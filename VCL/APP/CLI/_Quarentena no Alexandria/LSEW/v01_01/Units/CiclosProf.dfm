object FmCiclosProf: TFmCiclosProf
  Left = 339
  Top = 185
  Caption = 'CIC-CICLO-005 :: Gerenciamento de Professores'
  ClientHeight = 610
  ClientWidth = 1114
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 551
    Width = 1114
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtDiligencia: TBitBtn
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Dilig'#234'ncia'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtDiligenciaClick
    end
    object Panel2: TPanel
      Left = 976
      Top = 1
      Width = 137
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtLanctos: TBitBtn
      Left = 139
      Top = 5
      Width = 111
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Pagamento'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtLanctosClick
    end
    object BtLocCiclo: TBitBtn
      Left = 254
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Loc. &Ciclo'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtLocCicloClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1114
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Gerenciamento de Professores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 59
      Top = 1
      Width = 1054
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PainelPesquisa: TPanel
      Left = 1
      Top = 1
      Width = 58
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -33
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1114
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 337
      Width = 1112
      Height = 6
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
    end
    object Label5: TLabel
      Left = 330
      Top = 222
      Width = 71
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ComProfAlu'
    end
    object Label6: TLabel
      Left = 330
      Top = 601
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ComProfPrd'
    end
    object Label7: TLabel
      Left = 330
      Top = 980
      Width = 77
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ComProfSaq'
      FocusControl = DBEdit3
    end
    object Label8: TLabel
      Left = 330
      Top = 1359
      Width = 67
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ComProfAll'
      FocusControl = DBEdit4
    end
    object Label9: TLabel
      Left = 433
      Top = 231
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ComProfPrd'
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1112
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 6
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Professor:'
      end
      object Label2: TLabel
        Left = 457
        Top = 6
        Width = 69
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data inicial:'
      end
      object Label3: TLabel
        Left = 597
        Top = 6
        Width = 59
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data final:'
      end
      object Label4: TLabel
        Left = 737
        Top = 6
        Width = 160
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Per'#237'odo inicial do hist'#243'rico:'
      end
      object EdProfessor: TdmkEditCB
        Left = 10
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProfessorChange
        DBLookupComboBox = CBProfessor
        IgnoraDBLookupComboBox = False
      end
      object CBProfessor: TdmkDBLookupComboBox
        Left = 81
        Top = 25
        Width = 368
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEPROF'
        ListSource = DsProf
        TabOrder = 1
        dmkEditCB = EdProfessor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataI: TdmkEditDateTimePicker
        Left = 457
        Top = 25
        Width = 133
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39743.851871921310000000
        Time = 39743.851871921310000000
        TabOrder = 2
        OnClick = TPDataIClick
        OnChange = TPDataIClick
        OnExit = TPDataIExit
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataF: TdmkEditDateTimePicker
        Left = 597
        Top = 25
        Width = 133
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39743.851871921310000000
        Time = 39743.851871921310000000
        TabOrder = 3
        OnClick = TPDataFClick
        OnChange = TPDataFClick
        OnExit = TPDataFExit
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdMesAnoI: TdmkEdit
        Left = 737
        Top = 25
        Width = 157
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        TabOrder = 4
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
        OnExit = EdMesAnoIExit
      end
      object RadioGroup1: TRadioGroup
        Left = 901
        Top = 1
        Width = 185
        Height = 62
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo do gr'#225'fico: '
        ItemIndex = 0
        Items.Strings = (
          'Pelo hist'#243'rico'
          'Pelos '#250'ltimos 12 meses')
        TabOrder = 5
        Visible = False
      end
    end
    object GradeDiligencias: TDBGrid
      Left = 1
      Top = 164
      Width = 1112
      Height = 173
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsCiclosAula
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Ciclo'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'PRESENCA'
          Title.Caption = 'Presen'#231'a'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AlunosInsc'
          Title.Caption = 'Inscri'#231#245'es'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AlunosAula'
          Title.Caption = 'Presen'#231'as'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CIDADE_TXT'
          Title.Caption = 'Cidade'
          Width = 163
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTot'
          Title.Caption = 'Faturamento'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfVal'
          Title.Caption = 'Comiss'#227'o'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfDeb'
          Title.Caption = 'Desp. comiss'#227'o'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTSProfVal'
          Title.Caption = '13'#186
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTSProfDeb'
          Title.Caption = 'Desp. 13'#186
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfAlu'
          Title.Caption = 'Comis.Alu.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfPrd'
          Title.Caption = 'Comis.Prd.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfAll'
          Title.Caption = 'Comiss'#245'es'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfSaq'
          Title.Caption = 'Saques'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfPgt'
          Title.Caption = 'Pgt. comiss'#227'o'
          Visible = True
        end>
    end
    object DBGDespProf: TDBGrid
      Left = 1
      Top = 343
      Width = 1112
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      DataSource = DsDespProf
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Title.Caption = 'N'#186
          Width = 22
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Alignment = taRightJustify
          Title.Caption = 'D'#233'bito'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Carteira'
          Width = 175
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Conta (Plano de contas)'
          Width = 226
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID lancto.'
          Visible = True
        end>
    end
    object GradeHA: TStringGrid
      Left = 1
      Top = 65
      Width = 1112
      Height = 99
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      ColCount = 14
      DefaultColWidth = 48
      DefaultRowHeight = 18
      RowCount = 4
      TabOrder = 3
      OnDrawCell = GradeHADrawCell
    end
    object DBEdit3: TDBEdit
      Left = 330
      Top = 1162
      Width = 165
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'ComProfSaq'
      DataSource = DsCiclosAula
      TabOrder = 4
    end
    object DBEdit4: TDBEdit
      Left = 330
      Top = 1541
      Width = 165
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'ComProfAll'
      DataSource = DsCiclosAula
      TabOrder = 5
    end
  end
  object QrProf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CartPref,'
      'IF(tipo=0,RazaoSocial,Nome) NOMEPROF'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEPROF'
      '')
    Left = 16
    Top = 220
    object QrProfCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrProfCartPref: TIntegerField
      FieldName = 'CartPref'
      Origin = 'entidades.CartPref'
    end
    object QrProfNOMEPROF: TWideStringField
      FieldName = 'NOMEPROF'
      Required = True
      Size = 100
    end
  end
  object DsProf: TDataSource
    DataSet = QrProf
    Left = 44
    Top = 220
  end
  object QrCiclosAula: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCiclosAulaBeforeClose
    AfterScroll = QrCiclosAulaAfterScroll
    OnCalcFields = QrCiclosAulaCalcFields
    SQL.Strings = (
      'SELECT dtb.Nome MUNI, cia.*'
      'FROM ciclosaula cia'
      'LEFT JOIN dtb_munici dtb ON dtb.Codigo = cia.CodiCidade'
      'WHERE cia.Professor=:P0'
      'AND cia.Data BETWEEN :P1 AND :P2'
      'ORDER BY cia.Data')
    Left = 16
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCiclosAulaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ciclosaula.Codigo'
      DisplayFormat = '000000;-000000; '
    end
    object QrCiclosAulaControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'ciclosaula.Controle'
    end
    object QrCiclosAulaData: TDateField
      FieldName = 'Data'
      Origin = 'ciclosaula.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCiclosAulaAlunosInsc: TIntegerField
      FieldName = 'AlunosInsc'
      Origin = 'ciclosaula.AlunosInsc'
      DisplayFormat = '0;-0; '
    end
    object QrCiclosAulaAlunosAula: TIntegerField
      FieldName = 'AlunosAula'
      Origin = 'ciclosaula.AlunosAula'
      DisplayFormat = '0;-0; '
    end
    object QrCiclosAulaCidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'ciclosaula.Cidade'
      Size = 50
    end
    object QrCiclosAulaUF: TWideStringField
      FieldName = 'UF'
      Origin = 'ciclosaula.UF'
      Size = 2
    end
    object QrCiclosAulaLocalEnd: TWideStringField
      FieldName = 'LocalEnd'
      Origin = 'ciclosaula.LocalEnd'
      Size = 100
    end
    object QrCiclosAulaLocalVal: TFloatField
      FieldName = 'LocalVal'
      Origin = 'ciclosaula.LocalVal'
    end
    object QrCiclosAulaHotelNome: TWideStringField
      FieldName = 'HotelNome'
      Origin = 'ciclosaula.HotelNome'
      Size = 30
    end
    object QrCiclosAulaHotelCont: TWideStringField
      FieldName = 'HotelCont'
      Origin = 'ciclosaula.HotelCont'
    end
    object QrCiclosAulaHotelTele: TWideStringField
      FieldName = 'HotelTele'
      Origin = 'ciclosaula.HotelTele'
    end
    object QrCiclosAulaHotelVal: TFloatField
      FieldName = 'HotelVal'
      Origin = 'ciclosaula.HotelVal'
    end
    object QrCiclosAulaOnibusNome: TWideStringField
      FieldName = 'OnibusNome'
      Origin = 'ciclosaula.OnibusNome'
    end
    object QrCiclosAulaOnibusHora: TWideStringField
      FieldName = 'OnibusHora'
      Origin = 'ciclosaula.OnibusHora'
      Size = 25
    end
    object QrCiclosAulaObserva: TWideStringField
      FieldName = 'Observa'
      Origin = 'ciclosaula.Observa'
      Size = 255
    end
    object QrCiclosAulaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ciclosaula.Lk'
    end
    object QrCiclosAulaDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ciclosaula.DataCad'
    end
    object QrCiclosAulaDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ciclosaula.DataAlt'
    end
    object QrCiclosAulaUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ciclosaula.UserCad'
    end
    object QrCiclosAulaUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ciclosaula.UserAlt'
    end
    object QrCiclosAulaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'ciclosaula.AlterWeb'
    end
    object QrCiclosAulaAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ciclosaula.Ativo'
    end
    object QrCiclosAulaLocalNom: TWideStringField
      FieldName = 'LocalNom'
      Origin = 'ciclosaula.LocalNom'
      Size = 100
    end
    object QrCiclosAulaLocalPes: TWideStringField
      FieldName = 'LocalPes'
      Origin = 'ciclosaula.LocalPes'
      Size = 30
    end
    object QrCiclosAulaLocalTel: TWideStringField
      FieldName = 'LocalTel'
      Origin = 'ciclosaula.LocalTel'
    end
    object QrCiclosAulaValorUni: TFloatField
      FieldName = 'ValorUni'
      Origin = 'ciclosaula.ValorUni'
    end
    object QrCiclosAulaValorTot: TFloatField
      FieldName = 'ValorTot'
      Origin = 'ciclosaula.ValorTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComPromPer: TFloatField
      FieldName = 'ComPromPer'
      Origin = 'ciclosaula.ComPromPer'
    end
    object QrCiclosAulaComPromVal: TFloatField
      FieldName = 'ComPromVal'
      Origin = 'ciclosaula.ComPromVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComPromMin: TFloatField
      FieldName = 'ComPromMin'
      Origin = 'ciclosaula.ComPromMin'
    end
    object QrCiclosAulaComProfPer: TFloatField
      FieldName = 'ComProfPer'
      Origin = 'ciclosaula.ComProfPer'
    end
    object QrCiclosAulaComProfVal: TFloatField
      FieldName = 'ComProfVal'
      Origin = 'ciclosaula.ComProfVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfMin: TFloatField
      FieldName = 'ComProfMin'
      Origin = 'ciclosaula.ComProfMin'
    end
    object QrCiclosAulaDTSPromPer: TFloatField
      FieldName = 'DTSPromPer'
      Origin = 'ciclosaula.DTSPromPer'
    end
    object QrCiclosAulaDTSPromVal: TFloatField
      FieldName = 'DTSPromVal'
      Origin = 'ciclosaula.DTSPromVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaDTSPromMin: TFloatField
      FieldName = 'DTSPromMin'
      Origin = 'ciclosaula.DTSPromMin'
    end
    object QrCiclosAulaDTSProfPer: TFloatField
      FieldName = 'DTSProfPer'
      Origin = 'ciclosaula.DTSProfPer'
    end
    object QrCiclosAulaDTSProfVal: TFloatField
      FieldName = 'DTSProfVal'
      Origin = 'ciclosaula.DTSProfVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaDTSProfMin: TFloatField
      FieldName = 'DTSProfMin'
      Origin = 'ciclosaula.DTSProfMin'
    end
    object QrCiclosAulaPromotor: TIntegerField
      FieldName = 'Promotor'
      Origin = 'ciclosaula.Promotor'
    end
    object QrCiclosAulaDTSPromDeb: TFloatField
      FieldName = 'DTSPromDeb'
      Origin = 'ciclosaula.DTSPromDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComPromDeb: TFloatField
      FieldName = 'ComPromDeb'
      Origin = 'ciclosaula.ComPromDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaDTSProfDeb: TFloatField
      FieldName = 'DTSProfDeb'
      Origin = 'ciclosaula.DTSProfDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfDeb: TFloatField
      FieldName = 'ComProfDeb'
      Origin = 'ciclosaula.ComProfDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaPRESENCA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRESENCA'
      Size = 8
      Calculated = True
    end
    object QrCiclosAulaBonPromVal: TFloatField
      FieldName = 'BonPromVal'
      Origin = 'ciclosaula.BonPromVal'
    end
    object QrCiclosAulaBonPromWho: TIntegerField
      FieldName = 'BonPromWho'
      Origin = 'ciclosaula.BonPromWho'
    end
    object QrCiclosAulaProfessor: TIntegerField
      FieldName = 'Professor'
      Origin = 'ciclosaula.Professor'
    end
    object QrCiclosAulaComProfAlu: TFloatField
      FieldName = 'ComProfAlu'
      Origin = 'ciclosaula.ComProfAlu'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfPrd: TFloatField
      FieldName = 'ComProfPrd'
      Origin = 'ciclosaula.ComProfPrd'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfSaq: TFloatField
      FieldName = 'ComProfSaq'
      Origin = 'ciclosaula.ComProfSaq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfAll: TFloatField
      FieldName = 'ComProfAll'
      Origin = 'ciclosaula.ComProfAll'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfPgt: TFloatField
      FieldName = 'ComProfPgt'
      Origin = 'ciclosaula.ComProfPgt'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaMUNI: TWideStringField
      FieldName = 'MUNI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrCiclosAulaCodiCidade: TIntegerField
      FieldName = 'CodiCidade'
      Origin = 'ciclosaula.CodiCidade'
    end
    object QrCiclosAulaCIDADE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CIDADE_TXT'
      Size = 100
      Calculated = True
    end
  end
  object DsCiclosAula: TDataSource
    DataSet = QrCiclosAula
    Left = 44
    Top = 248
  end
  object QrDespProf: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDespProfCalcFields
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, '
      'lan.*'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID BETWEEN 700 AND 799'
      'AND lan.FatID_Sub=:P0')
    Left = 16
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDespProfNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrDespProfNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrDespProfSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrDespProfKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrDespProfData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespProfTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
      Required = True
    end
    object QrDespProfCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
      Required = True
    end
    object QrDespProfControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrDespProfSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      Required = True
    end
    object QrDespProfAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
    end
    object QrDespProfGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrDespProfQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrDespProfDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrDespProfNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
    end
    object QrDespProfDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDespProfCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDespProfCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespProfSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrDespProfDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
    end
    object QrDespProfSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrDespProfVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespProfFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrDespProfFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrDespProfFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrDespProfFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrDespProfID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
    end
    object QrDespProfID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Origin = 'lanctos.ID_Quit'
      Required = True
    end
    object QrDespProfID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrDespProfFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      Size = 1
    end
    object QrDespProfEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrDespProfBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrDespProfContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrDespProfCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrDespProfLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrDespProfCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrDespProfLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrDespProfOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrDespProfLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrDespProfPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrDespProfMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
      Required = True
    end
    object QrDespProfFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrDespProfCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrDespProfCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrDespProfForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrDespProfMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrDespProfMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrDespProfMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
    end
    object QrDespProfMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
    end
    object QrDespProfProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrDespProfDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrDespProfCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
    end
    object QrDespProfNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrDespProfVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrDespProfAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrDespProfICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
    end
    object QrDespProfICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
    end
    object QrDespProfDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 13
    end
    object QrDespProfDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrDespProfDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrDespProfDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrDespProfDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrDespProfUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrDespProfNFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrDespProfAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrDespProfExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrDespProfDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrDespProfCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrDespProfTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrDespProfReparcel: TIntegerField
      FieldName = 'Reparcel'
      Origin = 'lanctos.Reparcel'
      Required = True
    end
    object QrDespProfAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Origin = 'lanctos.Atrelado'
      Required = True
    end
    object QrDespProfPagMul: TFloatField
      FieldName = 'PagMul'
      Origin = 'lanctos.PagMul'
      Required = True
    end
    object QrDespProfPagJur: TFloatField
      FieldName = 'PagJur'
      Origin = 'lanctos.PagJur'
      Required = True
    end
    object QrDespProfSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Origin = 'lanctos.SubPgto1'
      Required = True
    end
    object QrDespProfMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
      Origin = 'lanctos.MultiPgto'
    end
    object QrDespProfLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrDespProfDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrDespProfDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrDespProfUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrDespProfUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrDespProfAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'lanctos.AlterWeb'
      Required = True
    end
    object QrDespProfAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'lanctos.Ativo'
      Required = True
    end
    object QrDespProfAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsDespProf: TDataSource
    DataSet = QrDespProf
    Left = 44
    Top = 276
  end
  object PMDiligencia: TPopupMenu
    OnPopup = PMDiligenciaPopup
    Left = 32
    Top = 432
    object Incluinovadiligncia1: TMenuItem
      Caption = '&Inclui nova dilig'#234'ncia'
      OnClick = Incluinovadiligncia1Click
    end
    object Alteradilignciaatual1: TMenuItem
      Caption = '&Altera dilig'#234'ncia atual'
      OnClick = Alteradilignciaatual1Click
    end
    object Excluidilignciaatual1: TMenuItem
      Caption = '&Exclui dilig'#234'ncia atual'
      Enabled = False
      OnClick = Excluidilignciaatual1Click
    end
  end
  object PMLanctos: TPopupMenu
    OnPopup = PMLanctosPopup
    Left = 136
    Top = 432
    object Incluilanamento1: TMenuItem
      Caption = '&Inclui lan'#231'amento'
      OnClick = Incluilanamento1Click
    end
    object Alteralanamento1: TMenuItem
      Caption = '&Altera lan'#231'amento'
      OnClick = Alteralanamento1Click
    end
    object Excluilanamento1: TMenuItem
      Caption = '&Exclui lan'#231'amento'
      OnClick = Excluilanamento1Click
    end
  end
  object frxDsCiclosAula: TfrxDBDataset
    UserName = 'frxDsCiclosAula'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Data=Data'
      'AlunosInsc=AlunosInsc'
      'AlunosAula=AlunosAula'
      'Cidade=Cidade'
      'UF=UF'
      'LocalEnd=LocalEnd'
      'LocalVal=LocalVal'
      'HotelNome=HotelNome'
      'HotelCont=HotelCont'
      'HotelTele=HotelTele'
      'HotelVal=HotelVal'
      'OnibusNome=OnibusNome'
      'OnibusHora=OnibusHora'
      'Observa=Observa'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'LocalNom=LocalNom'
      'LocalPes=LocalPes'
      'LocalTel=LocalTel'
      'ValorUni=ValorUni'
      'ValorTot=ValorTot'
      'ComPromPer=ComPromPer'
      'ComPromVal=ComPromVal'
      'ComPromMin=ComPromMin'
      'ComProfPer=ComProfPer'
      'ComProfVal=ComProfVal'
      'ComProfMin=ComProfMin'
      'DTSPromPer=DTSPromPer'
      'DTSPromVal=DTSPromVal'
      'DTSPromMin=DTSPromMin'
      'DTSProfPer=DTSProfPer'
      'DTSProfVal=DTSProfVal'
      'DTSProfMin=DTSProfMin'
      'Promotor=Promotor'
      'DTSPromDeb=DTSPromDeb'
      'ComPromDeb=ComPromDeb'
      'DTSProfDeb=DTSProfDeb'
      'ComProfDeb=ComProfDeb'
      'PRESENCA=PRESENCA'
      'BonPromVal=BonPromVal'
      'BonPromWho=BonPromWho'
      'Professor=Professor'
      'ComProfAlu=ComProfAlu'
      'ComProfPrd=ComProfPrd'
      'ComProfSaq=ComProfSaq'
      'ComProfAll=ComProfAll'
      'ComProfPgt=ComProfPgt'
      'MUNI=MUNI'
      'CodiCidade=CodiCidade'
      'CIDADE_TXT=CIDADE_TXT')
    DataSet = QrCiclosAula
    BCDToCurrency = False
    Left = 72
    Top = 248
  end
  object frxMovPeriodo: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39745.603151574100000000
    ReportOptions.LastChange = 39746.399532511600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  DTS, Com: Extended;                                  '
      '  '
      'procedure MeComAntOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Com := <frxDsAnt."Com">;        '
      'end;'
      ''
      'procedure MeDTSAntOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  DTS := <frxDsAnt."DTS">;'
      'end;'
      ''
      'procedure MeComAtuOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Com := Com + <frxDsCiclosAula."ComProfAll"> -      '
      
        '    <frxDsCiclosAula."ComProfSaq"> - <frxDsCiclosAula."ComProfPg' +
        't">;'
      
        '  MeComAtu.Text := FormatFloat('#39'#,###,##0.00'#39', Com);            ' +
        '                                                                ' +
        '       '
      'end;'
      ''
      'procedure MeDTSAtuOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  DTS := DTS + <frxDsCiclosAula."DTSProfVal"> - <frxDsCiclosAula' +
        '."DTSProfDeb">;'
      
        '  MeDTSAtu.Text := FormatFloat('#39'#,###,##0.00'#39', DTS);            ' +
        '                                                                ' +
        '       '
      'end;'
      ''
      'procedure MeComFimOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeComFim.Text := FormatFloat('#39'#,###,##0.00'#39', Com);            ' +
        '                                                                ' +
        '       '
      'end;'
      ''
      'procedure MeDTSFimOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeDTSFim.Text := FormatFloat('#39'#,###,##0.00'#39', DTS);            ' +
        '                                                                ' +
        '       '
      'end;'
      ''
      'begin'
      '  if <LogoBig1Existe> = True then'
      '  begin'
      '    Logo.LoadFromFile(<LogoBig1Caminho>);'
      '  end;          '
      'end.')
    OnGetValue = frxMovPeriodoGetValue
    Left = 104
    Top = 220
    Datasets = <
      item
        DataSet = frxDsAnt
        DataSetName = 'frxDsAnt'
      end
      item
        DataSet = frxDsCiclosAula
        DataSetName = 'frxDsCiclosAula'
      end
      item
        DataSet = frxDsDespProm
        DataSetName = 'frxDsDespProm'
      end
      item
        DataSet = frxDsGrafData
        DataSetName = 'frxDsGrafData'
      end
      item
        DataSet = frxDsGrafVal1
        DataSetName = 'frxDsGrafVal1'
      end
      item
        DataSet = frxDsGrafVal2
        DataSetName = 'frxDsGrafVal2'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 286.771831730000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Logo: TfrxPictureView
          Width = 718.110700000000000000
          Height = 94.488188980000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoNOME_ENT: TfrxMemoView
          Top = 147.401670000000000000
          Width = 718.110700000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_PROFESSOR]')
          ParentFont = False
        end
        object MeEndereco: TfrxMemoView
          Top = 160.629931020000000000
          Width = 718.110236220000000000
          Height = 56.692950000000010000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_ENDERECO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 226.771800000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Extrato de [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Top = 275.905690000000000000
          Width = 370.393940000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          Memo.UTF8W = (
            'SALDOS ANTERIORES')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeComAnt: TfrxMemoView
          Left = 548.031850000000000000
          Top = 275.905690000000000000
          Width = 170.078850000000000000
          Height = 10.866141730000000000
          OnBeforePrint = 'MeComAntOnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnt."Com"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeDTSAnt: TfrxMemoView
          Left = 464.882190000000000000
          Top = 147.401670000000000000
          Width = 170.078850000000000000
          Height = 10.866141730000000000
          Visible = False
          OnBeforePrint = 'MeDTSAntOnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnt."DTS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Top = 264.567100000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 56.692950000000010000
          Top = 264.567100000000000000
          Width = 200.315090000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'Cidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 257.008040000000000000
          Top = 264.567100000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Presen'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 294.803340000000000000
          Top = 264.567100000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Inscritos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 332.598640000000000000
          Top = 264.567100000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Presentes')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 548.031849999999900000
          Top = 264.567100000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Retiradas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 604.724800000000000000
          Top = 264.567100000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Pagamentos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 661.417750000000000000
          Top = 264.567100000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 464.882190000000000000
          Top = 136.063080000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '13'#186)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 521.575140000000100000
          Top = 136.063080000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Pagamentos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 578.268090000000000000
          Top = 136.063080000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo 13'#186)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 37.795300000000000000
          Top = 264.567100000000000000
          Width = 18.897650000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            'UF')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 464.882190000000000000
          Top = 219.212740000000000000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."D' +
              'TSPromVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 521.575140000000100000
          Top = 219.212740000000000000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."D' +
              'TSProfDeb">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeDTSAtu: TfrxMemoView
          Left = 578.268090000000000000
          Top = 219.212740000000000000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          Visible = False
          OnBeforePrint = 'MeDTSAtuOnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 464.882190000000000000
          Top = 241.889920000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."DTSProfVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 521.575140000000100000
          Top = 241.889920000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          Visible = False
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."DTSProfDeb">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeDTSFim: TfrxMemoView
          Left = 578.268090000000000000
          Top = 241.889920000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          Visible = False
          OnBeforePrint = 'MeDTSFimOnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 374.173470000000000000
          Top = 264.567100000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Comis.Alunos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 430.866420000000000000
          Top = 264.567100000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Comis.Prod.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 487.559370000000000000
          Top = 264.567100000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Comis.Cidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          Left = 374.173470000000000000
          Top = 275.905690000000000000
          Width = 170.078850000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 366.614410000000000000
        Width = 718.110700000000000000
        DataSet = frxDsCiclosAula
        DataSetName = 'frxDsCiclosAula'
        PrintIfDetailEmpty = True
        RowCount = 0
        object frxDsCiclosAulaData: TfrxMemoView
          Width = 37.795300000000000000
          Height = 11.338582677165400000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCiclosAula."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 56.692950000000010000
          Width = 200.315090000000000000
          Height = 11.338582680000000000
          DataField = 'CIDADE_TXT'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCiclosAula."CIDADE_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 257.008040000000000000
          Width = 37.795300000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclosAula."PRESENCA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 294.803340000000000000
          Width = 37.795300000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsCiclosAula."AlunosInsc">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 332.598640000000000000
          Width = 37.795300000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsCiclosAula."AlunosAula">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 548.031849999999900000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."C' +
              'omProfSaq">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 604.724800000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."C' +
              'omProfPgt">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeComAtu: TfrxMemoView
          Left = 661.417750000000000000
          Width = 56.692950000000000000
          Height = 11.338582680000000000
          OnBeforePrint = 'MeComAtuOnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 11.338582677165400000
          DataField = 'UF'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclosAula."UF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 374.173470000000000000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."C' +
              'omProfVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 430.866420000000000000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."C' +
              'omProfPrd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 487.559370000000000000
          Width = 56.692950000000010000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsCiclosAula."C' +
              'omProfAll">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 438.425480000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Width = 56.692950000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 56.692950000000010000
          Width = 200.315090000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 257.008040000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'0.00'#39', (IIF(SUM(<frxDsCiclosAula."AlunosInsc">) <>' +
              ' 0,'
            
              'SUM(<frxDsCiclosAula."AlunosAula">) / SUM(<frxDsCiclosAula."Alun' +
              'osInsc">) * 100'
            ',0)))]%')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 294.803340000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."AlunosInsc">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 332.598640000000000000
          Width = 37.795300000000000000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."AlunosAula">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 487.559370000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."ComProfAll">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 548.031849999999900000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."ComProfSaq">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 604.724800000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."ComProfPgt">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeComFim: TfrxMemoView
          Left = 661.417750000000000000
          Width = 56.692950000000000000
          Height = 10.866141730000000000
          OnBeforePrint = 'MeComFimOnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 430.866420000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."ComProfPrd">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 374.173470000000000000
          Width = 56.692950000000010000
          Height = 10.866141730000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCiclosAula."ComProfVal">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 79.370122680000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo37: TfrxMemoView
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Top = 45.354360000000000000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Inscritos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Top = 56.692949999999990000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Presentes')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 68.031540000000010000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          Memo.UTF8W = (
            'Presen'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 64.252010000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_01]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo44: TfrxMemoView
          Left = 113.385900000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_02]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo45: TfrxMemoView
          Left = 162.519790000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_03]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo46: TfrxMemoView
          Left = 211.653680000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_04]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo47: TfrxMemoView
          Left = 260.787570000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_05]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Left = 309.921460000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_06]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo49: TfrxMemoView
          Left = 359.055350000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_07]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Left = 408.189240000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_08]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo51: TfrxMemoView
          Left = 457.323130000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_09]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Left = 506.457020000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_10]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo53: TfrxMemoView
          Left = 555.590910000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_11]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Left = 604.724800000000000000
          Top = 34.015770000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_12]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo55: TfrxMemoView
          Left = 653.858690000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            '[MES_13]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo95: TfrxMemoView
          Top = 11.338590000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico Anual')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          Left = 64.252010000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_01]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo57: TfrxMemoView
          Left = 113.385900000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_02]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo58: TfrxMemoView
          Left = 162.519790000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_03]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo59: TfrxMemoView
          Left = 211.653680000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_04]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo60: TfrxMemoView
          Left = 260.787570000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_05]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo61: TfrxMemoView
          Left = 309.921460000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_06]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Left = 359.055350000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_07]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo63: TfrxMemoView
          Left = 408.189240000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_08]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Left = 457.323130000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_09]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo65: TfrxMemoView
          Left = 506.457020000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_10]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Left = 555.590910000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_11]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo67: TfrxMemoView
          Left = 604.724800000000000000
          Top = 45.354360000000000000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_12]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Left = 653.858690000000000000
          Top = 45.354360000000000000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INS_13]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo69: TfrxMemoView
          Left = 64.252010000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_01]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo70: TfrxMemoView
          Left = 113.385900000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_02]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo71: TfrxMemoView
          Left = 162.519790000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_03]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo72: TfrxMemoView
          Left = 211.653680000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_04]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo73: TfrxMemoView
          Left = 260.787570000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_05]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo74: TfrxMemoView
          Left = 309.921460000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_06]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Left = 359.055350000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_07]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo76: TfrxMemoView
          Left = 408.189240000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_08]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo77: TfrxMemoView
          Left = 457.323130000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_09]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo78: TfrxMemoView
          Left = 506.457020000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_10]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo79: TfrxMemoView
          Left = 555.590910000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_11]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo80: TfrxMemoView
          Left = 604.724800000000000000
          Top = 56.692949999999990000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_12]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo81: TfrxMemoView
          Left = 653.858690000000000000
          Top = 56.692949999999990000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ALU_13]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo82: TfrxMemoView
          Left = 64.252010000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_01]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo83: TfrxMemoView
          Left = 113.385900000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_02]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo84: TfrxMemoView
          Left = 162.519790000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_03]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo85: TfrxMemoView
          Left = 211.653680000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_04]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo86: TfrxMemoView
          Left = 260.787570000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_05]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo87: TfrxMemoView
          Left = 309.921460000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_06]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo88: TfrxMemoView
          Left = 359.055350000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_07]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo89: TfrxMemoView
          Left = 408.189240000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_08]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo90: TfrxMemoView
          Left = 457.323130000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_09]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo91: TfrxMemoView
          Left = 506.457020000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_10]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo92: TfrxMemoView
          Left = 555.590910000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_11]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo93: TfrxMemoView
          Left = 604.724800000000000000
          Top = 68.031540000000010000
          Width = 49.133890000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_12]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo94: TfrxMemoView
          Left = 653.858690000000000000
          Top = 68.031540000000010000
          Width = 64.252010000000000000
          Height = 11.338582680000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clSilver
          HAlign = haRight
          Memo.UTF8W = (
            '[PER_13]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 336.378170000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Chart2: TfrxChartView
          Top = 11.338590000000000000
          Width = 718.110236220000000000
          Height = 325.039580000000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E4272757368
            2E436F6C6F720707636C5768697465144261636B57616C6C2E42727573682E53
            74796C6507076273436C656172204261636B57616C6C2E42727573682E477261
            6469656E742E456E64436F6C6F7204C1C47500204261636B57616C6C2E427275
            73682E4772616469656E742E4D6964436F6C6F7204F9F99B00224261636B5761
            6C6C2E42727573682E4772616469656E742E5374617274436F6C6F7204C1C475
            001F4261636B57616C6C2E42727573682E4772616469656E742E56697369626C
            6509144261636B57616C6C2E50656E2E56697369626C6508144261636B57616C
            6C2E5472616E73706172656E7408124C6567656E642E4C6567656E645374796C
            6507086C73536572696573105469746C652E466F6E742E436F6C6F720707636C
            426C61636B115469746C652E466F6E742E48656967687402ED125469746C652E
            546578742E537472696E677301141000000048697374C3B37269636F20416E75
            616C0019426F74746F6D417869732E4461746554696D65466F726D617406074D
            4D2F5959595918426F74746F6D417869732E45786163744461746554696D6508
            15426F74746F6D417869732E4C6162656C5374796C65070774616C5465787418
            426F74746F6D417869732E5469746C652E43617074696F6E06054D455345530D
            4672616D652E56697369626C6508164C656674417869732E5469746C652E4361
            7074696F6E140E0000004EC2BA20444520414C554E4F53200656696577334408
            165669657733444F7074696F6E732E526F746174696F6E02000A426576656C4F
            75746572070662764E6F6E6505436F6C6F720709636C4D656E7542617211436F
            6C6F7250616C65747465496E646578020D000F54466173744C696E6553657269
            6573054D65736573134D61726B732E4172726F772E56697369626C6509194D61
            726B732E43616C6C6F75742E42727573682E436F6C6F720707636C426C61636B
            1B4D61726B732E43616C6C6F75742E4172726F772E56697369626C65090D4D61
            726B732E56697369626C65080B536572696573436F6C6F720706636C4E6F6E65
            0C53686F77496E4C6567656E6408055469746C650609496E73637269746F730D
            4C696E6550656E2E436F6C6F720706636C4E6F6E650D4C696E6550656E2E5769
            64746802020C5856616C7565732E4E616D650601580D5856616C7565732E4F72
            646572070B6C6F417363656E64696E670C5956616C7565732E4E616D65060159
            0D5956616C7565732E4F7264657207066C6F4E6F6E6500000F54466173744C69
            6E6553657269657309496E73637269746F73134D61726B732E4172726F772E56
            697369626C6509194D61726B732E43616C6C6F75742E42727573682E436F6C6F
            720707636C426C61636B1B4D61726B732E43616C6C6F75742E4172726F772E56
            697369626C6509114D61726B732E5472616E73706172656E74090D4D61726B73
            2E56697369626C65090B536572696573436F6C6F7203D500055469746C650609
            496E73637269746F730D4C696E6550656E2E436F6C6F7203D5000D4C696E6550
            656E2E5374796C650709707344617368446F740D4C696E6550656E2E57696474
            6802020C5856616C7565732E4E616D650601580D5856616C7565732E4F726465
            72070B6C6F417363656E64696E670C5956616C7565732E4E616D650601590D59
            56616C7565732E4F7264657207066C6F4E6F6E6500000F54466173744C696E65
            5365726965730950726573656E746573134D61726B732E4172726F772E566973
            69626C6509194D61726B732E43616C6C6F75742E42727573682E436F6C6F7207
            07636C426C61636B1B4D61726B732E43616C6C6F75742E4172726F772E566973
            69626C6509114D61726B732E5472616E73706172656E74090D4D61726B732E56
            697369626C65090B536572696573436F6C6F72040D2C4D00055469746C650609
            50726573656E7465730D4C696E6550656E2E436F6C6F72040D2C4D000D4C696E
            6550656E2E576964746802020C5856616C7565732E4E616D650601580D585661
            6C7565732E4F72646572070B6C6F417363656E64696E670C5956616C7565732E
            4E616D650601590D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 345
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtDBData
              DataSet = frxDsGrafData
              DataSetName = 'frxDsGrafData'
              SortOrder = soAscending
              TopN = 40
              TopNCaption = 'teste'
              XType = xtText
              Source1 = 'frxDsGrafData."Texto"'
              Source2 = 'frxDsGrafData."TextoInt"'
              XSource = 'frxDsGrafData."Texto"'
              YSource = 'frxDsGrafData."TextoInt"'
            end
            item
              InheritedName = 'TfrxSeriesItem3'
              DataType = dtDBData
              DataSet = frxDsGrafVal1
              DataSetName = 'frxDsGrafVal1'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsGrafVal1."Val01"'
              Source2 = 'frxDsGrafVal1."Val01"'
              XSource = 'frxDsGrafVal1."Val01"'
              YSource = 'frxDsGrafVal1."Val01"'
            end
            item
              InheritedName = 'TfrxSeriesItem4'
              DataType = dtDBData
              DataSet = frxDsGrafVal2
              DataSetName = 'frxDsGrafVal2'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsGrafVal2."Val02"'
              Source2 = 'frxDsGrafVal2."Val02"'
              XSource = 'frxDsGrafVal2."Val02"'
              YSource = 'frxDsGrafVal2."Val02"'
            end>
        end
      end
    end
  end
  object frxDsDespProm: TfrxDBDataset
    UserName = 'frxDsDespProm'
    CloseDataSource = False
    DataSet = QrDespProf
    BCDToCurrency = False
    Left = 72
    Top = 276
  end
  object QrAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(DTSPromVal-DTSPromDeb) DTS,'
      'SUM(ComProfAll-ComProfSaq-ComProfPgt) Com'
      'FROM ciclosaula'
      'WHERE Professor=:P0'
      'AND Data < :P1')
    Left = 104
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntDTS: TFloatField
      FieldName = 'DTS'
    end
    object QrAntCom: TFloatField
      FieldName = 'Com'
    end
  end
  object frxDsAnt: TfrxDBDataset
    UserName = 'frxDsAnt'
    CloseDataSource = False
    DataSet = QrAnt
    BCDToCurrency = False
    Left = 104
    Top = 276
  end
  object QrHistAnu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(AlunosInsc) AlunosInsc, '
      'SUM(AlunosAula) AlunosAula, MONTH(Data) + 0.0 Mes, Data'
      'FROM ciclosaula'
      'WHERE Professor=:P0'
      'AND Data BETWEEN :P1 AND :P2'
      'GROUP BY MONTH(Data)')
    Left = 72
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrHistAnuAlunosInsc: TFloatField
      FieldName = 'AlunosInsc'
    end
    object QrHistAnuAlunosAula: TFloatField
      FieldName = 'AlunosAula'
    end
    object QrHistAnuData: TDateField
      FieldName = 'Data'
    end
    object QrHistAnuMes: TFloatField
      FieldName = 'Mes'
    end
  end
  object frxDsGrafData: TfrxDBDataset
    UserName = 'frxDsGrafData'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Linha=Linha'
      'Texto=Texto'
      'Val01=Val01'
      'Val02=Val02'
      'Val03=Val03'
      'TextoInt=TextoInt'
      'Ativo=Ativo')
    DataSet = QrGrafData
    BCDToCurrency = False
    Left = 160
    Top = 220
  end
  object QrGrafData: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT *'
      'FROM grafico')
    Left = 132
    Top = 220
    object QrGrafDataLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'grafico.Linha'
    end
    object QrGrafDataTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'grafico.Texto'
      Size = 50
    end
    object QrGrafDataVal01: TFloatField
      FieldName = 'Val01'
      Origin = 'grafico.Val01'
    end
    object QrGrafDataVal02: TFloatField
      FieldName = 'Val02'
      Origin = 'grafico.Val02'
    end
    object QrGrafDataVal03: TFloatField
      FieldName = 'Val03'
      Origin = 'grafico.Val03'
    end
    object QrGrafDataTextoInt: TIntegerField
      FieldName = 'TextoInt'
    end
    object QrGrafDataAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'grafico.Ativo'
    end
  end
  object QrGrafVal1: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT *'
      'FROM grafico'
      'WHERE Ativo=1')
    Left = 132
    Top = 248
    object QrGrafVal1Linha: TIntegerField
      FieldName = 'Linha'
      Origin = 'grafico.Linha'
    end
    object QrGrafVal1Texto: TWideStringField
      FieldName = 'Texto'
      Origin = 'grafico.Texto'
      Size = 50
    end
    object QrGrafVal1Val01: TFloatField
      FieldName = 'Val01'
      Origin = 'grafico.Val01'
    end
    object QrGrafVal1Val02: TFloatField
      FieldName = 'Val02'
      Origin = 'grafico.Val02'
    end
    object QrGrafVal1Val03: TFloatField
      FieldName = 'Val03'
      Origin = 'grafico.Val03'
    end
    object QrGrafVal1Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'grafico.Ativo'
    end
  end
  object QrGrafVal2: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT *'
      'FROM grafico'
      'WHERE Ativo=1')
    Left = 132
    Top = 276
    object QrGrafVal2Linha: TIntegerField
      FieldName = 'Linha'
      Origin = 'grafico.Linha'
    end
    object QrGrafVal2Texto: TWideStringField
      FieldName = 'Texto'
      Origin = 'grafico.Texto'
      Size = 50
    end
    object QrGrafVal2Val01: TFloatField
      FieldName = 'Val01'
      Origin = 'grafico.Val01'
    end
    object QrGrafVal2Val02: TFloatField
      FieldName = 'Val02'
      Origin = 'grafico.Val02'
    end
    object QrGrafVal2Val03: TFloatField
      FieldName = 'Val03'
      Origin = 'grafico.Val03'
    end
    object QrGrafVal2Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'grafico.Ativo'
    end
  end
  object frxDsGrafVal1: TfrxDBDataset
    UserName = 'frxDsGrafVal1'
    CloseDataSource = False
    DataSet = QrGrafVal1
    BCDToCurrency = False
    Left = 160
    Top = 248
  end
  object frxDsGrafVal2: TfrxDBDataset
    UserName = 'frxDsGrafVal2'
    CloseDataSource = False
    DataSet = QrGrafVal2
    BCDToCurrency = False
    Left = 160
    Top = 276
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 60
    Top = 11
  end
  object frxMovPeriodo2: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39745.603151574100000000
    ReportOptions.LastChange = 40333.611422719900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  SALDO: Extended;                                  '
      ''
      'procedure MeComAntOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MeDTSAntOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MeComAtuOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MeDTSAtuOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MeComFimOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure MeDTSFimOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  SALDO := SALDO + <frxDsCiclos."ComisVal">;                    ' +
        '                                           '
      '  Memo4.Text := FormatFloat('#39'#,###,##0.00'#39', SALDO);  '
      'end;'
      ''
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin    '
      
        '  SALDO := SALDO + <frxDsProfLctos."VALOR">;                    ' +
        '                                           '
      '  Memo10.Text := FormatFloat('#39'#,###,##0.00'#39', SALDO);  '
      'end;'
      ''
      'procedure Memo18OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Memo18.Text := FormatFloat('#39'#,###,##0.00'#39', SALDO);            ' +
        '                                 '
      'end;'
      ''
      'begin'
      '  if <LogoBig1Existe> = True then'
      '  begin'
      '    Logo.LoadFromFile(<LogoBig1Caminho>);'
      '  end;          '
      'end.')
    OnGetValue = frxMovPeriodoGetValue
    Left = 660
    Top = 224
    Datasets = <
      item
        DataSet = frxDsAnt
        DataSetName = 'frxDsAnt'
      end
      item
        DataSet = frxDsCiclos
        DataSetName = 'frxDsCiclos'
      end
      item
        DataSet = frxDsCiclosAula
        DataSetName = 'frxDsCiclosAula'
      end
      item
        DataSet = frxDsDespProm
        DataSetName = 'frxDsDespProm'
      end
      item
        DataSet = frxDsGrafData
        DataSetName = 'frxDsGrafData'
      end
      item
        DataSet = frxDsGrafVal1
        DataSetName = 'frxDsGrafVal1'
      end
      item
        DataSet = frxDsGrafVal2
        DataSetName = 'frxDsGrafVal2'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsProfLctos
        DataSetName = 'frxDsProfLctos'
      end
      item
        DataSet = frxDsTotProdComis
        DataSetName = 'frxDsTotProdComis'
      end
      item
        DataSet = frxDsTotSaidas
        DataSetName = 'frxDsTotSaidas'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 275.905680240000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Logo: TfrxPictureView
          Width = 718.110700000000000000
          Height = 94.488188980000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoNOME_ENT: TfrxMemoView
          Top = 147.401670000000000000
          Width = 718.110700000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_PROFESSOR]')
          ParentFont = False
        end
        object MeEndereco: TfrxMemoView
          Top = 160.629931020000000000
          Width = 718.110236220000000000
          Height = 56.692950000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_ENDERECO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 226.771800000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Extrato de [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 260.787570000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 94.488188980000000000
          Top = 260.787570000000000000
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 529.133858270000000000
          Top = 260.787570000000000000
          Width = 94.488188980000000000
          Height = 15.118110236220500000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Lan'#231'amentos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 623.622047240000000000
          Top = 260.787570000000000000
          Width = 94.488188980000000000
          Height = 15.118110236220500000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        DataSet = frxDsCiclos
        DataSetName = 'frxDsCiclos'
        PrintIfDetailEmpty = True
        RowCount = 0
        object frxDsCiclosAulaData: TfrxMemoView
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."DataAcert"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 94.488250000000000000
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%g'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ciclo [frxDsCiclos."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 529.134200000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."ComisVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 623.622450000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'Memo4OnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 94.488250000000000000
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%g'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLMovV."NomeGra"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."CmisVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 623.622450000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110236220500000
        Top = 430.866420000000000000
        Width = 718.110700000000000000
        DataSet = frxDsProfLctos
        DataSetName = 'frxDsProfLctos'
        RowCount = 0
        object Memo7: TfrxMemoView
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsProfLctos
          DataSetName = 'frxDsProfLctos'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProfLctos."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 94.488250000000000000
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataField = 'Nome'
          DataSet = frxDsProfLctos
          DataSetName = 'frxDsProfLctos'
          DisplayFormat.FormatStr = '%g'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProfLctos."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 529.134200000000000000
          Width = 94.488188980000000000
          Height = 15.118110236220500000
          DataField = 'VALOR'
          DataSet = frxDsProfLctos
          DataSetName = 'frxDsProfLctos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProfLctos."VALOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 623.622450000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 37.795290240000000000
        Top = 468.661720000000000000
        Width = 718.110700000000000000
        object Memo16: TfrxMemoView
          Left = -0.000061020000000000
          Top = 22.677180000000020000
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'Extrato Resumido')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 434.645950000000000000
          Top = 22.677180000000020000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = 'dd.mmm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 604.724800000000000000
        Width = 718.110700000000000000
        object Memo17: TfrxMemoView
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%g'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total Geral')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 434.645950000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 529.134199999999900000
        Width = 718.110700000000000000
        DataSet = frxDsTotSaidas
        DataSetName = 'frxDsTotSaidas'
        RowCount = 0
        object Memo22: TfrxMemoView
          Left = 434.645950000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataField = 'VALOR'
          DataSet = frxDsTotSaidas
          DataSetName = 'frxDsTotSaidas'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotSaidas."VALOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%g'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total Comiss'#227'o Alunos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 566.929499999999900000
        Width = 718.110700000000000000
        DataSet = frxDsTotProdComis
        DataSetName = 'frxDsTotProdComis'
        RowCount = 0
        object Memo23: TfrxMemoView
          Left = 434.645950000000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          DataField = 'ComisVal'
          DataSet = frxDsTotProdComis
          DataSetName = 'frxDsTotProdComis'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotProdComis."ComisVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Width = 434.645669290000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosAula
          DataSetName = 'frxDsCiclosAula'
          DisplayFormat.FormatStr = '%g'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total [frxDsTotProdComis."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCiclos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCiclosAfterScroll
    SQL.Strings = (
      'SELECT cic.DataAcert, cic.Codigo Ciclo, cic.ComisVal,'
      'cic.ComisTrf, cic.ComisPgt, cic.Codigo,'
      'cic.Controle CicControle, cic.Professor CicProf'
      'FROM ciclos cic'
      'WHERE cic.Professor=:P0'
      'AND cic.DataSaida BETWEEN :P1 AND :P2'
      'AND cic.Codigo > 0'
      'GROUP BY cic.Codigo '
      'ORDER BY cic.DataSaida')
    Left = 688
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCiclosDataAcert: TDateField
      FieldName = 'DataAcert'
      Origin = 'ciclos.DataAcert'
    end
    object QrCiclosCiclo: TIntegerField
      FieldName = 'Ciclo'
      Origin = 'ciclos.Codigo'
    end
    object QrCiclosComisVal: TFloatField
      FieldName = 'ComisVal'
      Origin = 'ciclos.ComisVal'
    end
    object QrCiclosComisTrf: TFloatField
      FieldName = 'ComisTrf'
      Origin = 'ciclos.ComisTrf'
    end
    object QrCiclosComisPgt: TFloatField
      FieldName = 'ComisPgt'
      Origin = 'ciclos.ComisPgt'
    end
    object QrCiclosCicControle: TIntegerField
      FieldName = 'CicControle'
      Origin = 'ciclos.Controle'
    end
    object QrCiclosCicProf: TIntegerField
      FieldName = 'CicProf'
      Origin = 'ciclos.Professor'
    end
    object QrCiclosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxDsCiclos: TfrxDBDataset
    UserName = 'frxDsCiclos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataAcert=DataAcert'
      'Ciclo=Ciclo'
      'ComisVal=ComisVal'
      'ComisTrf=ComisTrf'
      'ComisPgt=ComisPgt'
      'CicControle=CicControle'
      'CicProf=CicProf'
      'Codigo=Codigo')
    DataSet = QrCiclos
    BCDToCurrency = False
    Left = 716
    Top = 224
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 12
    object A1: TMenuItem
      Caption = 'Modelo &A'
      OnClick = A1Click
    end
    object B1: TMenuItem
      Caption = 'Modelo &B'
      OnClick = B1Click
    end
  end
  object QrProfLctos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Data, con.Nome, '
      'IF(lan.Credito > 0, lan.Credito, - lan.Debito) VALOR'
      'FROM FTabLctALS lan'
      'LEFT JOIN ciclosaula aul ON aul.Controle =  lan.FatID_Sub'
      'LEFT JOIN ciclos cic ON cic.Codigo = aul.Codigo'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE aul.Codigo=0'
      'AND cic.Professor=:P0'
      'AND cic.DataSaida BETWEEN :P1 AND :P2')
    Left = 744
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrProfLctosData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
    end
    object QrProfLctosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrProfLctosVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object frxDsProfLctos: TfrxDBDataset
    UserName = 'frxDsProfLctos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Nome=Nome'
      'VALOR=VALOR')
    DataSet = QrProfLctos
    BCDToCurrency = False
    Left = 772
    Top = 224
  end
  object QrTotSaidas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(IF(lan.Credito > 0, lan.Credito, - lan.Debito)) VALOR'
      'FROM FTabLctALS lan'
      'LEFT JOIN ciclosaula aul ON aul.Controle =  lan.FatID_Sub'
      'LEFT JOIN ciclos cic ON cic.Codigo = aul.Codigo'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE aul.Codigo=0'
      'AND cic.Professor=:P0'
      'AND cic.DataSaida BETWEEN :P1 AND :P2')
    Left = 688
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrTotSaidasVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object frxDsTotSaidas: TfrxDBDataset
    UserName = 'frxDsTotSaidas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VALOR=VALOR')
    DataSet = QrTotSaidas
    BCDToCurrency = False
    Left = 716
    Top = 252
  end
  object QrTotProdComis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gra.Nome, mom.ComisVal'
      'FROM movim mom'
      'LEFT JOIN ciclos cic ON cic.Controle=mom.Controle'
      'LEFT JOIN ciclosaula aul ON aul.Codigo=cic.Codigo'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.SubCtrl=3'
      'AND cic.Professor=:P0'
      'AND cic.DataSaida BETWEEN :P1 AND :P2'
      'GROUP BY mom.Grade, mom.Cor, mom.Tam')
    Left = 744
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrTotProdComisNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grades.Nome'
      Size = 30
    end
    object QrTotProdComisComisVal: TFloatField
      FieldName = 'ComisVal'
      Origin = 'movim.ComisVal'
    end
  end
  object frxDsTotProdComis: TfrxDBDataset
    UserName = 'frxDsTotProdComis'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nome=Nome'
      'ComisVal=ComisVal')
    DataSet = QrTotProdComis
    BCDToCurrency = False
    Left = 772
    Top = 252
  end
end
