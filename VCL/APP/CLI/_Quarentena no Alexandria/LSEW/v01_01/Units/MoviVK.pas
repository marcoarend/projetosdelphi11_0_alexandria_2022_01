unit MoviVK;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, Variants, dmkGeral;

type
  TFmMoviVK = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBGrid2: TDBGrid;
    Panel3: TPanel;
    Label6: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    PnEdita: TPanel;
    Label1: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrGradeKit: TmySQLQuery;
    DsGradeKit: TDataSource;
    QrTot: TmySQLQuery;
    QrTotTOTAL: TFloatField;
    DsTot: TDataSource;
    QrLoc: TmySQLQuery;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    QrGradesCodUsu: TIntegerField;
    DsGrades: TDataSource;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    DsGradesTams: TDataSource;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesCorsCor: TIntegerField;
    DsGradesCors: TDataSource;
    StaticText2: TStaticText;
    CBProd: TdmkDBLookupComboBox;
    EdProd: TdmkEditCB;
    CBCor: TdmkDBLookupComboBox;
    EdCor: TdmkEditCB;
    CBTam: TdmkDBLookupComboBox;
    EdTam: TdmkEditCB;
    EdProdQtd: TdmkEdit;
    EdProdTot: TdmkEdit;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    BtConfItem: TBitBtn;
    QrGradeKitControle: TIntegerField;
    QrGradeKitConta: TIntegerField;
    QrGradeKitGrade: TIntegerField;
    QrGradeKitDataPedi: TDateField;
    QrGradeKitKit: TIntegerField;
    QrGradeKitCor: TIntegerField;
    QrGradeKitTam: TIntegerField;
    QrGradeKitQtd: TFloatField;
    QrGradeKitNOMEGRADE: TWideStringField;
    QrGradeKitNOMECOR: TWideStringField;
    QrGradeKitTOTAL: TFloatField;
    QrGradeKitPRECO: TFloatField;
    QrGradeKitNOMETAM: TWideStringField;
    QrGradeKitProduto: TIntegerField;
    QrGradeKitPOSIq: TFloatField;
    QrGradeKitVen: TFloatField;
    QrGradeKitFrete: TFloatField;
    QrGradeKitQTDKIT: TIntegerField;
    QrTotTOT: TFloatField;
    QrGradeKitCodigo: TIntegerField;
    Label2: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    BtDesItems: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGradeKitCalcFields(DataSet: TDataSet);
    procedure QrGradeKitAfterScroll(DataSet: TDataSet);
    procedure BtConfItemClick(Sender: TObject);
    procedure QrGradesAfterScroll(DataSet: TDataSet);
    procedure QrGradesBeforeClose(DataSet: TDataSet);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdProdQtdChange(Sender: TObject);
    procedure QrTotCalcFields(DataSet: TDataSet);
    procedure CBProdEnter(Sender: TObject);
    procedure BtDesItemsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdTamChange(Sender: TObject);
    procedure EdCorChange(Sender: TObject);
    procedure EdProdChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTam(Codigo, Cor: Integer);
    procedure ReopenCor(Codigo: Integer);
    procedure ReopenGradeKit(Conta: Integer);
    procedure ReopenTot();
    procedure LimpaCampos;
    function  VerificaProduto(Grade, Cor, Tam: Integer): Integer;
    function  QtdEmEstoque(Grade, Cor, Tam: Integer): Double;
    function  ProdutoEstaAtivo(Grade, Cor, Tam: Integer): Boolean;
    function  CalculaTotal(Grade, Cor, Tam, ListaPre: Integer; Qtd: Double): Double;
  public
    { Public declarations }
    FControle, FListaPre, FKit: Integer;
    FSQLStatus: String;
    FTrava: Boolean;
  end;

  var
  FmMoviVK: TFmMoviVK;

implementation

{$R *.DFM}

uses Module, MyListas, ModuleGeral, UCreate, ModuleProd, MoviV, MoviVEdit,
UnMyObjects, DmkDAC_PF;

function TFmMoviVK.QtdEmEstoque(Grade, Cor, Tam: Integer): Double;
begin
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT EstqQ');
  QrLoc.SQL.Add('FROM produtos');
  QrLoc.SQL.Add('WHERE Codigo=:PO');
  QrLoc.SQL.Add('AND Cor=:P1');
  QrLoc.SQL.Add('AND Tam=:P2');
  QrLoc.Params[0].AsInteger := Grade;
  QrLoc.Params[1].AsInteger := Cor;
  QrLoc.Params[2].AsInteger := Tam;
  QrLoc.Open;
  //
  Result := QrLoc.FieldByName('EstqQ').AsFloat;
end;

procedure TFmMoviVK.BtConfItemClick(Sender: TObject);
var
  Controle, Conta, Kit, Grade, Cor, Tam, Produto: Integer;
  Qtd, Preco: Double;
  DataPedi: String;
begin
  Controle := QrGradeKitControle.Value;
  Kit      := QrGradeKitKit.Value;
  Qtd      := EdProdQtd.ValueVariant;
  Preco    := 0;
  Grade    := EdProd.ValueVariant;
  Cor      := EdCor.ValueVariant;
  Tam      := EdTam.ValueVariant;
  Produto  := VerificaProduto(Grade, Cor, Tam);
  DataPedi := Geral.FDT(QrGradeKitDataPedi.Value, 1);
  //
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  if Grade = 0 then
  begin
    Application.MessageBox('Produto n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBProd.SetFocus;
    Exit;
  end;
  if Cor = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCor.SetFocus;
    Exit;
  end;
  if Tam = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBTam.SetFocus;
    Exit;
  end;
  if Qtd = 0 then
  begin
    Application.MessageBox('Quantidade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdProdQtd.SetFocus;
    Exit;
  end;
  //
  if Produto > 0 then
    //Preco := DmProd.ObtemPrecoProduto(FListaPre, Grade, Produto)
  else
    Preco := 0;
  //
  case Dmod.QrControleFatSEtqKit.Value of
    0: //N�o inclui se n�o tiver estoque
    begin
      if QtdEmEstoque(Grade, Cor, Tam) <= 0 then
      begin
        Geral.MensagemBox('Inclus�o abortada!' + sLineBreak +
          'Motivo: Este item n�o possui estoque', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    2: //Perguntar antes se n�o tiver estoque
    begin
      if Geral.MensagemBox('Este item n�o possui estoque!' + sLineBreak +
        'Deseja inclu�-lo mesmo assim?', 'Pergunta',
        MB_ICONQUESTION+MB_YESNOCANCEL) <> ID_YES
      then
        Exit;
    end;
  end;
  //
  if FSQLStatus = CO_ALTERACAO then
    Conta := QrGradeKitConta.Value
  else
    Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
        'movim', 'movim', 'conta');
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, FSQLStatus, 'movim', False,
  [
    'Motivo', 'Qtd', 'Val', 'Ven', 'Grade',
    'Cor', 'Tam', 'DataPedi', 'Kit', 'Controle'
  ], ['Conta'],
  [
    21, - Qtd, - Preco * Qtd, EdProdTot.ValueVariant, QrGradesCodigo.Value,
    Cor, Tam, DataPedi, Kit, Controle
  ], [Conta]) then
  begin
    PnEdita.Visible        := False;
    EdProd.ValueVariant    := 0;
    CBProd.KeyValue        := Null;
    EdCor.ValueVariant     := 0;
    CBCor.KeyValue         := Null;
    EdTam.ValueVariant     := 0;
    CBTam.KeyValue         := Null;
    EdProdQtd.ValueVariant := 0;
    EdProdTot.ValueVariant := 0;
    DBGrid2.Enabled        := True;
    //
    ReopenGradeKit(Conta);
    DBGrid2.SetFocus;
  end;
end;

procedure TFmMoviVK.BtDesItemsClick(Sender: TObject);
begin
  PnEdita.Visible        := False;
  EdProd.ValueVariant    := 0;
  CBProd.KeyValue        := Null;
  EdCor.ValueVariant     := 0;
  CBCor.KeyValue         := Null;
  EdTam.ValueVariant     := 0;
  CBTam.KeyValue         := Null;
  EdProdQtd.ValueVariant := 0;
  EdProdTot.ValueVariant := 0;
  DBGrid2.Enabled        := True;
  //
  DBGrid2.SetFocus;
end;

procedure TFmMoviVK.BtSaidaClick(Sender: TObject);
begin
  if not FTrava then
    FmMoviVEdit.FTotKit := QrTotTOT.Value;
  Close;
end;

procedure TFmMoviVK.CBProdEnter(Sender: TObject);
begin
  if FSQLStatus = CO_INCLUSAO then 
    LimpaCampos;
end;

procedure TFmMoviVK.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Conta, Grade, Cor, Tam: Integer;
begin
  if not FTrava then
  begin
    //Editar - Desabilitado o pre�o est� puxando errado
    if Key = VK_RETURN then
    begin
      Grade := QrGradeKitCodigo.Value;
      Cor   := QrGradeKitCor.Value;
      Tam   := QrGradeKitTam.Value; 
      //
      if QtdEmEstoque(Grade, Cor, Tam) <= 0 then
      begin
        Geral.MensagemBox('Edi��o abortada!' + sLineBreak +
          'Motivo: Este item n�o possui estoque', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      //
      if not ProdutoEstaAtivo(Grade, Cor, Tam) then
      begin
        Geral.MensagemBox('Edi��o abortada!' + sLineBreak +
          'Motivo: Este item n�o est� ativo', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;      
      //
      FSQLStatus             := CO_ALTERACAO;
      DBGrid2.Enabled        := False;
      EdProd.ValueVariant    := Grade;
      CBProd.KeyValue        := Grade;
      EdCor.ValueVariant     := Cor;
      CBCor.KeyValue         := Cor;
      EdTam.ValueVariant     := Tam;
      CBTam.KeyValue         := Tam;
      EdProdQtd.ValueVariant := QrGradeKitPOSIq.Value;
      EdProdTot.ValueVariant := QrGradeKitTOTAL.Value;
      PnEdita.Visible        := True;
      CBProd.SetFocus;
    end;
    //Excluir
    if (Key = VK_DELETE) and (ssCtrl in Shift) then
    begin
      Conta := QrGradeKitConta.Value;
      //
      if Conta > 0 then
      begin
        if Geral.MB_Pergunta('Confirma a exclus�o do registro?') = ID_YES then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM movim WHERE Conta=:P0');
          Dmod.QrUpd.Params[00].AsInteger := Conta;
          Dmod.QrUpd.ExecSQL;
          //
          ReopenGradeKit(0);
        end;
      end;
    end;
    //Incluir
    if (Key = VK_F4) then
    begin
      FSQLStatus             := CO_INCLUSAO;
      DBGrid2.Enabled        := False;
      EdProd.ValueVariant    := 0;
      CBProd.KeyValue        := Null;
      EdCor.ValueVariant     := 0;
      CBCor.KeyValue         := Null;
      EdTam.ValueVariant     := 0;
      CBTam.KeyValue         := Null;
      EdProdQtd.ValueVariant := 0;
      EdProdTot.ValueVariant := 0;
      PnEdita.Visible        := True;
      CBProd.SetFocus;
    end;
  end;
end;

function TFmMoviVK.CalculaTotal(Grade, Cor, Tam, ListaPre: Integer; Qtd: Double): Double;
var
  Preco: Double;
begin
  try
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Controle, EstqQ');
    QrLoc.SQL.Add('FROM produtos');
    QrLoc.SQL.Add('WHERE Codigo=:PO');
    QrLoc.SQL.Add('AND Cor=:P1');
    QrLoc.SQL.Add('AND Tam=:P2');
    QrLoc.Params[0].AsInteger := Grade;
    QrLoc.Params[1].AsInteger := Cor;
    QrLoc.Params[2].AsInteger := Tam;
    QrLoc.Open;
    //
    Preco := DmProd.ObtemPrecoProduto(ListaPre, Grade,
      QrLoc.FieldByName('Controle').AsInteger);
    //
    Result := Qtd * Preco;
  except
    Result := 0;
  end;
end;

procedure TFmMoviVK.EdCorChange(Sender: TObject);
begin
  ReopenTam(QrGradesCodigo.Value, EdCor.ValueVariant);
  //
  EdProdTot.ValueVariant := CalculaTotal(EdProd.ValueVariant,
    EdCor.ValueVariant, EdTam.ValueVariant, FListaPre, EdProdQtd.ValueVariant);
end;

procedure TFmMoviVK.EdProdChange(Sender: TObject);
begin
  EdProdTot.ValueVariant := CalculaTotal(EdProd.ValueVariant,
    EdCor.ValueVariant, EdTam.ValueVariant, FListaPre, EdProdQtd.ValueVariant);
end;

procedure TFmMoviVK.EdProdQtdChange(Sender: TObject);
begin
  EdProdTot.ValueVariant := CalculaTotal(EdProd.ValueVariant,
    EdCor.ValueVariant, EdTam.ValueVariant, FListaPre, EdProdQtd.ValueVariant);
end;

procedure TFmMoviVK.EdTamChange(Sender: TObject);
begin
  EdProdTot.ValueVariant := CalculaTotal(EdProd.ValueVariant,
    EdCor.ValueVariant, EdTam.ValueVariant, FListaPre, EdProdQtd.ValueVariant);
end;

procedure TFmMoviVK.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ReopenGradeKit(0);
  ReopenTot;
  QrGrades.Open;
end;

procedure TFmMoviVK.FormCreate(Sender: TObject);
begin
  PnEdita.Visible := False;
end;

procedure TFmMoviVK.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMoviVK.FormShow(Sender: TObject);
begin
  StaticText2.Visible := not FTrava;
end;

procedure TFmMoviVK.LimpaCampos;
begin
  EdProd.ValueVariant    := 0;
  CBProd.KeyValue        := Null;
  EdCor.ValueVariant     := 0;
  CBCor.KeyValue         := Null;
  EdTam.ValueVariant     := 0;
  CBTam.KeyValue         := Null;
  EdProdQtd.ValueVariant := 0;
  EdProdTot.ValueVariant := 0;
end;

function TFmMoviVK.ProdutoEstaAtivo(Grade, Cor, Tam: Integer): Boolean;
begin
  //True  = Ativo
  //False = Inativo
  //
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Ativo');
  QrLoc.SQL.Add('FROM produtos');
  QrLoc.SQL.Add('WHERE Codigo=:PO');
  QrLoc.SQL.Add('AND Cor=:P1');
  QrLoc.SQL.Add('AND Tam=:P2');
  QrLoc.Params[0].AsInteger := Grade;
  QrLoc.Params[1].AsInteger := Cor;
  QrLoc.Params[2].AsInteger := Tam;
  QrLoc.Open;
  //
  Result := Geral.IntToBool(QrLoc.FieldByName('Ativo').AsInteger);
end;

procedure TFmMoviVK.QrGradeKitAfterScroll(DataSet: TDataSet);
begin
  ReopenTot;
end;

procedure TFmMoviVK.QrGradeKitCalcFields(DataSet: TDataSet);
begin
  QrGradeKitPRECO.Value := QrGradeKitVen.Value / - QrGradeKitQtd.Value;
  QrGradeKitTOTAL.Value := - QrGradeKitPRECO.Value * QrGradeKitQtd.Value;
  QrGradeKitPOSIq.Value := - QrGradeKitQtd.Value;
end;

procedure TFmMoviVK.QrGradesAfterScroll(DataSet: TDataSet);
begin
  ReopenCor(QrGradesCodigo.Value);
  ReopenTam(QrGradesCodigo.Value, 0);
end;

procedure TFmMoviVK.QrGradesBeforeClose(DataSet: TDataSet);
begin
  QrGradesCors.Close;
  QrGradesTams.Close;
end;

procedure TFmMoviVK.QrTotCalcFields(DataSet: TDataSet);
begin
  QrTotTOT.Value := (QrGradeKitQTDKIT.Value * QrGradeKitFrete.Value) + QrTotTOTAL.Value;
end;

procedure TFmMoviVK.ReopenCor(Codigo: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Codigo;
  QrGradesCors.Open;
end;

procedure TFmMoviVK.ReopenGradeKit(Conta: Integer);
begin
  QrGradeKit.Close;
  QrGradeKit.Params[0].AsInteger := FControle;
  QrGradeKit.Params[1].AsInteger := FKit;
  QrGradeKit.Open;
  //
  if Conta > 0 then QrGradeKit.Locate('Conta', Conta, []);
end;

procedure TFmMoviVK.ReopenTam(Codigo, Cor: Integer);
var
  SQLCor: String;
begin
  if Cor <> 0 then
    SQLCor := 'AND pro.Cor=' + Geral.FF0(Cor)
  else
    SQLCor := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTams, Dmod.MyDB, [
    'SELECT tam.Nome NOMETAM, pro.Tam ',
    'FROM produtos pro ',
    'LEFT JOIN tamanhos tam ON tam.Codigo = pro.Tam ',
    'WHERE pro.Codigo=' + Geral.FF0(Codigo),
    SQLCor,
    'AND pro.Ativo=1 ',
    'GROUP BY pro.Tam ',
    'ORDER BY tam.Codigo ',
    '']);
end;

procedure TFmMoviVK.ReopenTot;
begin
  QrTot.Close;
  QrTot.Params[0].AsInteger := FControle;
  QrTot.Params[1].AsInteger := FKit;
  QrTot.Open;
end;

function TFmMoviVK.VerificaProduto(Grade, Cor, Tam: Integer): Integer;
begin
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Controle, EstqQ');
  QrLoc.SQL.Add('FROM produtos');
  QrLoc.SQL.Add('WHERE Codigo=:PO');
  QrLoc.SQL.Add('AND Cor=:P1');
  QrLoc.SQL.Add('AND Tam=:P2');
  QrLoc.Params[0].AsInteger := Grade;
  QrLoc.Params[1].AsInteger := Cor;
  QrLoc.Params[2].AsInteger := Tam;
  QrLoc.Open;
  //
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Controle').AsInteger
  else
    Result := 0;
end;

end.



