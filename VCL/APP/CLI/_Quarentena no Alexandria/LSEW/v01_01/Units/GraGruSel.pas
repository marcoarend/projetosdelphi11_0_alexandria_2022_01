unit GraGruSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, mySQLDbTables, Principal;

type
  TFmGraGruSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label19: TLabel;
    EdProd: TdmkEditCB;
    CBProd: TdmkDBLookupComboBox;
    Label30: TLabel;
    CBCor: TdmkDBLookupComboBox;
    EdCor: TdmkEditCB;
    Label31: TLabel;
    EdTam: TdmkEditCB;
    CBTam: TdmkDBLookupComboBox;
    Label34: TLabel;
    EdKit: TdmkEditCB;
    CBKit: TdmkDBLookupComboBox;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    QrGradesCodUsu: TIntegerField;
    DsGrades: TDataSource;
    DsGradesCors: TDataSource;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsAlterWeb: TSmallintField;
    QrGradesCorsAtivo: TSmallintField;
    DsGradesTams: TDataSource;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    QrGradeK: TmySQLQuery;
    QrGradeKCodigo: TIntegerField;
    QrGradeKNome: TWideStringField;
    DsGradeK: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGradesAfterScroll(DataSet: TDataSet);
    procedure QrGradesBeforeClose(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenGradeK();
    procedure ReopenGrades();
    procedure ReopenGradesCors(Codigo: Integer);
    procedure ReopenGradesTams(Codigo: Integer);
  public
    { Public declarations }
    FFinalidade: TFinalidade;
    FCodProd, FTam, FCor: Integer;
  end;

  var
  FmGraGruSel: TFmGraGruSel;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmGraGruSel.BtOKClick(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 0 then //Produtos
  begin
    FCodProd    := EdProd.ValueVariant;
    FCor        := EdCor.ValueVariant;
    FTam        := EdTam.ValueVariant;
    FFinalidade := istProdutos;
    //
    if MyObjects.FIC(FCodProd = 0, EdProd, 'Informe a mercadoria!') then Exit;
    //
    Close;
  end else
  if PageControl1.ActivePageIndex = 1 then //Kit
  begin
    FCodProd    := EdKit.ValueVariant;
    FCor        := 0;
    FTam        := 0;
    FFinalidade := istGradeK;
    //
    if MyObjects.FIC(FCodProd = 0, EdKit, 'Informe o Kit!') then Exit;
    //
    Close;
  end else
    Geral.MB_Aviso('N�o implementado!');
end;

procedure TFmGraGruSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraGruSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  //
  FCodProd := 0;
  FTam     := 0;
  FCor     := 0;
  //
  ReopenGradeK;
  ReopenGrades;
end;

procedure TFmGraGruSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraGruSel.QrGradesAfterScroll(DataSet: TDataSet);
begin
  ReopenGradesCors(QrGradesCodigo.Value);
  ReopenGradesTams(QrGradesCodigo.Value);
end;

procedure TFmGraGruSel.QrGradesBeforeClose(DataSet: TDataSet);
begin
  QrGradesCors.Close;
  QrGradesTams.Close;
end;

procedure TFmGraGruSel.ReopenGradeK;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradeK, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM gradek ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmGraGruSel.ReopenGrades;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGrades, Dmod.MyDB, [
    'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome ',
    'FROM produtos pro ',
    'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo ',
    'WHERE pro.Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmGraGruSel.ReopenGradesCors(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesCors, Dmod.MyDB, [
    'SELECT cor.Nome NOMECOR, grc.* ',
    'FROM gradescors grc ',
    'LEFT JOIN cores cor ON cor.Codigo=grc.Cor ',
    'WHERE grc.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Controle ',
    '']);
end;

procedure TFmGraGruSel.ReopenGradesTams(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTams, Dmod.MyDB, [
    'SELECT tam.Nome NOMETAM, grt.* ',
    'FROM gradestams grt ',
    'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam ',
    'WHERE grt.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Controle ',
    '']);
end;

end.
