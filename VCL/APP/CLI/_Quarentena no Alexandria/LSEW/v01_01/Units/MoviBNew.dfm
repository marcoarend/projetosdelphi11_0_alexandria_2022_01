object FmMoviBNew: TFmMoviBNew
  Left = 569
  Top = 308
  Caption = 'PRD-BALAN-003 :: Novo Per'#237'odo de Balan'#231'o'
  ClientHeight = 265
  ClientWidth = 420
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 49
    Width = 420
    Height = 157
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Label2: TLabel
      Left = 30
      Top = 34
      Width = 117
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'M'#234's do movimento:'
    end
    object Label3: TLabel
      Left = 266
      Top = 34
      Width = 115
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Ano do movimento:'
    end
    object Label1: TLabel
      Left = 30
      Top = 94
      Width = 153
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Inserindo estoque inicial...'
      Visible = False
    end
    object CBMes: TComboBox
      Left = 31
      Top = 55
      Width = 224
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'CBMes'
    end
    object CBAno: TComboBox
      Left = 265
      Top = 55
      Width = 117
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Text = 'CBAno'
    end
    object ProgressBar1: TProgressBar
      Left = 30
      Top = 113
      Width = 350
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 2
      Visible = False
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 420
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Novo Per'#237'odo de Balan'#231'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 417
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 206
    Width = 420
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 30
      Top = 5
      Width = 110
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 270
      Top = 5
      Width = 110
      Height = 49
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object QrLocPeriodo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Periodo '
      'FROM movib'
      'WHERE Periodo=:P0')
    Left = 128
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPeriodoPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Cor, Tam, EstqQ, EstqV'
      'FROM produtos'
      'WHERE EstqQ <> 0 AND EstqV <> 0')
    Left = 124
    Top = 176
    object QrEstqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstqCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrEstqTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrEstqEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrEstqEstqV: TFloatField
      FieldName = 'EstqV'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 4
  end
end
