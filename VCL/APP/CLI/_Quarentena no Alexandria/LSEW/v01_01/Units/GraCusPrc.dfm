object FmGraCusPrc: TFmGraCusPrc
  Left = 368
  Top = 194
  Caption = 'PRD-PRECO-001 :: Cadastro de Lista de Pre'#231'os de Grades'
  ClientHeight = 353
  ClientWidth = 816
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 816
    Height = 305
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 256
      Width = 814
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 9
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 705
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 814
      Height = 136
      Align = alTop
      TabOrder = 0
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 812
        Height = 48
        Align = alTop
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label9: TLabel
          Left = 66
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object EdCodigo: TdmkEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdNome: TdmkEdit
          Left = 66
          Top = 20
          Width = 655
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
      object Panel8: TPanel
        Left = 1
        Top = 49
        Width = 812
        Height = 67
        Align = alTop
        Caption = 'Panel8'
        TabOrder = 1
        object Label4: TLabel
          Left = 5
          Top = 3
          Width = 36
          Height = 13
          Caption = 'Moeda:'
        end
        object EdMoeda: TdmkEditCB
          Left = 5
          Top = 18
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Moeda'
          UpdCampo = 'Moeda'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBMoeda
          IgnoraDBLookupComboBox = False
        end
        object CBMoeda: TdmkDBLookupComboBox
          Left = 61
          Top = 18
          Width = 245
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsCambioMda
          TabOrder = 1
          dmkEditCB = EdMoeda
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CGAplicacao: TdmkCheckGroup
          Left = 312
          Top = 4
          Width = 409
          Height = 40
          Caption = ' Aplica'#231#227'o: '
          Columns = 4
          ItemIndex = 2
          Items.Strings = (
            'Ciclo'
            'Telemarketing'
            'Pontos de venda'
            'Lingerie')
          TabOrder = 2
          QryCampo = 'Aplicacao'
          UpdCampo = 'Aplicacao'
          UpdType = utYes
          Value = 4
          OldValor = 0
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 816
    Height = 305
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 814
      Height = 124
      Align = alTop
      TabOrder = 1
      object Panel6: TPanel
        Left = 1
        Top = 49
        Width = 812
        Height = 52
        Align = alTop
        TabOrder = 0
        object Label5: TLabel
          Left = 4
          Top = 3
          Width = 36
          Height = 13
          Caption = 'Moeda:'
        end
        object DBEdit2: TDBEdit
          Left = 60
          Top = 18
          Width = 246
          Height = 21
          DataField = 'NOMEMOEDA'
          DataSource = DsGraCusPrc
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 4
          Top = 18
          Width = 56
          Height = 21
          DataField = 'SIGLAMDA'
          DataSource = DsGraCusPrc
          TabOrder = 1
        end
        object dmkDBCheckGroup1: TdmkDBCheckGroup
          Left = 312
          Top = 4
          Width = 409
          Height = 40
          Caption = ' Aplica'#231#227'o: '
          Columns = 4
          DataField = 'Aplicacao'
          DataSource = DsGraCusPrc
          Items.Strings = (
            'Ciclo'
            'Telemarketing'
            'Pontos de venda'
            'Lingerie')
          ParentBackground = False
          TabOrder = 2
        end
      end
      object PainelData: TPanel
        Left = 1
        Top = 1
        Width = 812
        Height = 48
        Align = alTop
        Enabled = False
        TabOrder = 1
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 66
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object DBEdCodigo: TDBEdit
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsGraCusPrc
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 66
          Top = 20
          Width = 655
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsGraCusPrc
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 256
      Width = 814
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 344
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtInclui: TBitBtn
          Tag = 10
          Left = 6
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 190
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui entidade atual'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 98
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Atera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAlteraClick
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 48
    Align = alTop
    Caption = 
      '                              Cadastro de Lista de Pre'#231'os de Gra' +
      'des'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 733
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 507
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 40
    Top = 12
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraCusPrcBeforeOpen
    AfterOpen = QrGraCusPrcAfterOpen
    SQL.Strings = (
      'SELECT gcp.Codigo, gcp.Nome, gcp.Moeda, '#13
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMDA,'
      'gcp.Aplicacao'
      'FROM gracusprc gcp'
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda')
    Left = 12
    Top = 12
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraCusPrcMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrGraCusPrcNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcSIGLAMDA: TWideStringField
      FieldName = 'SIGLAMDA'
      Size = 5
    end
    object QrGraCusPrcAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
  object QrCambioMda: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Codusu, Sigla, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 392
    Top = 72
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodusu: TIntegerField
      FieldName = 'Codusu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 420
    Top = 72
  end
end
