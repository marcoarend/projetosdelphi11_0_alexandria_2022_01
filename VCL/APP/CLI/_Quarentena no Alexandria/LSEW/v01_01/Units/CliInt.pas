unit CliInt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, ComCtrls, dmkEdit,
  dmkEditCB, DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, dmkDBGrid,
  dmkLabel, dmkGeral, dmkPermissoes, UnDmkEnums;

type
  TFmCliInt = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    PnInclui: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PnControle: TPanel;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    Label1: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    DsCliInt: TDataSource;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntCliente: TIntegerField;
    QrCliIntNOMECLIENTE: TWideStringField;
    dmkDBGrid1: TdmkDBGrid;
    LaTipo: TdmkLabel;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    BitBtn3: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCliInt(Codigo: Integer);
  public
    { Public declarations }
  end;

  var
  FmCliInt: TFmCliInt;

implementation

uses Module, UMySQLModule, UnMyObjects, MyDBCheck, DmkDAC_PF, ModuleGeral,
  UnInternalConsts;

{$R *.DFM}


procedure TFmCliInt.BitBtn1Click(Sender: TObject);
begin
  QrEntidades.Close;
  QrEntidades.Open;
  //
  PnInclui.Visible   := True;
  PnControle.Visible := False;
  LaTipo.SQLType     := stIns;
end;

procedure TFmCliInt.BitBtn3Click(Sender: TObject);
var
  DtEncer, DtMorto: TDateTime;
  TabLctA, TabLctB, TabLctD: String;
begin
  if QrCliIntCliente.Value <> 0 then
  begin
    DModG.Def_EM_ABD(TMeuDB, QrCliIntCliente.Value, QrCliIntCodigo.Value,
      DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM ' + TabLctA);
    Dmod.QrAux.SQL.Add('WHERE CliInt=:P0');
    Dmod.QrAux.Params[0].AsInteger := QrCliIntCliente.Value;
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Aviso('EXCLUS�O CANCELADA! Existem ' +
        Geral.FF0(Dmod.QrAux.RecordCount) + ' lan�amentos atrelados � entidade ' +
        'selecionada para exclus�o!');
      Exit;
    end;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT * FROM carteiras ');
    Dmod.QrAux.SQL.Add('WHERE ForneceI=:P0');
    Dmod.QrAux.Params[0].AsInteger := QrCliIntCliente.Value;
    Dmod.QrAux.Open;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Aviso('EXCLUS�O CANCELADA! Existem ' +
        Geral.FF0(Dmod.QrAux.RecordCount) + ' carteiras atreladas � entidade ' +
        'selecionada para exclus�o!');
      Exit;
    end;
    //
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o do cliente interno n�' +
    Geral.FF0(QrCliIntCodigo.Value) + '?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cliint WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCliIntCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=0 WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCliIntCliente.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenCliInt(0);
  end;
end;

procedure TFmCliInt.BtOKClick(Sender: TObject);
var
  Entidade, CliInt: Integer;
begin
  Entidade := Geral.IMV(EdCliente.Text);
  //
  if Entidade = 0  then
  begin
    Geral.MB_Aviso('Defina a entidade!');
    Exit;
  end;
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT CliInt FROM entidades WHERE Codigo=:P0');
  Dmod.QrAux.Params[0].AsInteger := Entidade;
  Dmod.QrAux.Open;
  if Dmod.QrAux.FieldByName('CliInt').AsInteger = 0 then
  begin
    CliInt := UMyMod.BuscaEmLivreY_Def('CliInt', 'Codigo', stIns, 0);
    // o n�mero 1 � reservado para o dono do aplicativo
    if CliInt = 1 then CliInt := 2;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
      'cliint'], ['Codigo'], [Entidade], [Entidade], True)
    then if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cliint', False, [
      'Cliente'], ['Codigo'], [Entidade], [CliInt], True) then
    begin
      LaTipo.SQLType     := stLok;
      PnControle.Visible := True;
      PnInclui.Visible   := False;
      //
      DBCheck.VerificaEntiCliInt();
      //
      ReopenCliInt(CliInt);
    end;
  end else
    Geral.MB_Aviso('A entidade selecionada j� � um cliente interno!');
end;

procedure TFmCliInt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCliInt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCliInt.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  ReopenCliInt(0);
end;

procedure TFmCliInt.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCliInt.ReopenCliInt(Codigo: Integer);
begin
  QrCliInt.Close;
  QrCliInt.Open;
  //
  if Codigo <> 0  then
    QrCliInt.Locate('Codigo', Codigo, []);
end;

end.



