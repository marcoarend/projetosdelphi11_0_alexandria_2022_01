object FmMoviBIts: TFmMoviBIts
  Left = 347
  Top = 184
  Caption = 'Acerto de Balan'#231'o'
  ClientHeight = 496
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 400
    Align = alClient
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 27
      Width = 1006
      Height = 47
      Align = alTop
      TabOrder = 0
      object Label13: TLabel
        Left = 8
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Mercadoria:'
      end
      object Label14: TLabel
        Left = 388
        Top = 4
        Width = 19
        Height = 13
        Caption = 'Cor:'
      end
      object Label15: TLabel
        Left = 596
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label16: TLabel
        Left = 668
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label17: TLabel
        Left = 780
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Pre'#231'o:'
      end
      object Label18: TLabel
        Left = 892
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Total:'
      end
      object EdGrade: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdGradeChange
        DBLookupComboBox = CBGrade
        IgnoraDBLookupComboBox = False
      end
      object EdCorCod: TEdit
        Left = 388
        Top = 20
        Width = 32
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object EdCorNom: TEdit
        Left = 420
        Top = 20
        Width = 172
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object EdTamCod: TEdit
        Left = 596
        Top = 20
        Width = 32
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
      object EdTamNom: TEdit
        Left = 627
        Top = 20
        Width = 37
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object EdQtd: TEdit
        Left = 668
        Top = 20
        Width = 108
        Height = 21
        TabOrder = 6
        OnEnter = EdQtdEnter
        OnExit = EdQtdExit
      end
      object EdPrc: TEdit
        Left = 780
        Top = 20
        Width = 108
        Height = 21
        TabOrder = 7
        OnEnter = EdPrcEnter
        OnExit = EdPrcExit
      end
      object EdVal: TEdit
        Left = 892
        Top = 20
        Width = 108
        Height = 21
        TabOrder = 8
        OnEnter = EdValEnter
        OnExit = EdValExit
      end
      object CBGrade: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 321
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGrades
        TabOrder = 1
        dmkEditCB = EdGrade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GradeA: TStringGrid
      Left = 1
      Top = 196
      Width = 790
      Height = 203
      TabStop = False
      ColCount = 1
      DefaultColWidth = 18
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      TabOrder = 1
      OnClick = GradeAClick
      OnDrawCell = GradeADrawCell
      RowHeights = (
        18)
    end
    object GradeC: TStringGrid
      Left = 380
      Top = 76
      Width = 361
      Height = 133
      ColCount = 2
      DefaultColWidth = 18
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 2
      Visible = False
      RowHeights = (
        18
        18)
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 1
      Width = 1006
      Height = 26
      Align = alTop
      Alignment = taCenter
      BorderStyle = sbsSunken
      Caption = 'Informe o estoque real atual!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      ExplicitWidth = 246
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfItem: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfItemClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Acerto de Balan'#231'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.Tam, pro.Cor, pro.Ativo, mom.Conta'
      'FROM produtos pro'
      'LEFT JOIN tamanhos tam ON tam.Codigo=pro.Tam'
      'LEFT JOIN cores    cor ON cor.Codigo=pro.Cor'
      'LEFT JOIN movim    mom ON mom.Grade=pro.Codigo '
      '   AND mom.Tam=pro.Tam '
      '   AND mom.Cor=pro.Cor'
      '   AND mom.Controle=:P0'
      'WHERE pro.Codigo= :P1'
      'AND pro.Ativo=2'
      '/*Para poder ordenar!!!*/'
      'ORDER BY tam.Nome, cor.Nome')
    Left = 756
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProdutosTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrProdutosCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY NOMETAM')
    Left = 728
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY NOMECOR')
    Left = 700
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 644
    Top = 12
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 672
    Top = 12
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 12
  end
end
