object FmGraGruGrupo: TFmGraGruGrupo
  Left = 339
  Top = 185
  Caption = 'PRD-GRADE-010 :: Grupo'
  ClientHeight = 239
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 191
    Width = 566
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 7
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Confirma'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 454
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 16
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 566
    Height = 48
    Align = alTop
    Caption = 'Grupo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 482
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 483
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 48
    Width = 566
    Height = 131
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 11
      Top = 45
      Width = 70
      Height = 13
      Caption = 'Departamento:'
    end
    object Label12: TLabel
      Left = 11
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label13: TLabel
      Left = 11
      Top = 86
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object dmkEdCBGradeD: TdmkEditCB
      Left = 11
      Top = 61
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = dmkCBGradeD
    end
    object dmkCBGradeD: TdmkDBLookupComboBox
      Left = 69
      Top = 61
      Width = 418
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGradeD
      TabOrder = 2
      dmkEditCB = dmkEdCBGradeD
      UpdType = utYes
    end
    object dmkEdNome: TdmkEdit
      Left = 11
      Top = 102
      Width = 476
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object dmkEdCodigo: TdmkEdit
      Left = 11
      Top = 20
      Width = 71
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object PnNiveis: TPanel
    Left = 0
    Top = 179
    Width = 566
    Height = 12
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
  end
  object QrGradeD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graded'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrGradeDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradeDNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGradeD: TDataSource
    DataSet = QrGradeD
    Left = 36
    Top = 8
  end
end
