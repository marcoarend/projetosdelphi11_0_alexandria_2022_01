unit AlunoVis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Mask, DBCtrls,
  dmkDBEdit, dmkEdit, DB, mySQLDbTables, dmkGeral;

type
  TFmAlunoVis = class(TForm)
    PainelConfirma: TPanel;
    BtLocalizar: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label55: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqObserva: TWideStringField;
    QrPesqData: TDateField;
    QrPesqAluno: TWideStringField;
    QrPesqNOMEPRM: TWideStringField;
    QrPesqNOMEPRO: TWideStringField;
    QrPesqMUNI: TWideStringField;
    QrPesqUF: TWideStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QrPesqCodigo: TIntegerField;
    QrPesqControle: TIntegerField;
    QrPesqEntidade: TIntegerField;
    Label7: TLabel;
    LaDataExp: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure BtLocalizarClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesq(Conta: Integer);
  public
    { Public declarations }
    FConta: Integer;
  end;

  var
  FmAlunoVis: TFmAlunoVis;

implementation

uses Principal, Module, UnMyObjects;

{$R *.DFM}

procedure TFmAlunoVis.BtLocalizarClick(Sender: TObject);
begin
  FmPrincipal.FCodAlu  := QrPesqCodigo.Value;
  FmPrincipal.FCodAlu2 := QrPesqControle.Value;
  Close;
end;

procedure TFmAlunoVis.BtSaidaClick(Sender: TObject);
begin
  FmPrincipal.FCodAlu  := 0;
  FmPrincipal.FCodAlu2 := 0;
  Close;
end;

procedure TFmAlunoVis.FormActivate(Sender: TObject);
var
  DataExp: TDateTime;
begin
  MyObjects.CorIniComponente;
  ReopenPesq(FConta);
  //
  DataExp := Dmod.VerificaDataDeExpiracaoDoCurso(QrPesqEntidade.Value);
  LaDataExp.Caption := Geral.FDT(DataExp, 2);
end;

procedure TFmAlunoVis.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmAlunoVis.QrPesqAfterScroll(DataSet: TDataSet);
begin
  BtLocalizar.Enabled := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
end;

procedure TFmAlunoVis.ReopenPesq(Conta: Integer);
begin
  QrPesq.Close;
  QrPesq.Params[0].AsInteger := Conta;
  QrPesq.Open;
end;

end.
