unit PtosFatBal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkGeral, UnDmkEnums;

type
  TFmPtosFatBal = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    TbEstqPto: TmySQLTable;
    DsEstqPto: TDataSource;
    DBGrid1: TDBGrid;
    TbEstqPtoGrade: TIntegerField;
    TbEstqPtoGraGruX: TIntegerField;
    TbEstqPtoCor: TIntegerField;
    TbEstqPtoTam: TIntegerField;
    TbEstqPtoNo_Grade: TWideStringField;
    TbEstqPtoNo_Cor: TWideStringField;
    TbEstqPtoNo_Tam: TWideStringField;
    TbEstqPtoQtdeOld: TIntegerField;
    TbEstqPtoQtdeNew: TIntegerField;
    QrItens: TmySQLQuery;
    QrItensIDCtrl: TIntegerField;
    QrItensCodInn: TIntegerField;
    QrItensCodOut: TIntegerField;
    QrItensTipOut: TSmallintField;
    QrItensDataIns: TDateTimeField;
    QrItensEmpresa: TIntegerField;
    QrItensProduto: TIntegerField;
    QrItensQuanti: TFloatField;
    QrItensPrecoO: TFloatField;
    QrItensPrecoR: TFloatField;
    QrItensDescoP: TFloatField;
    QrItensStatus: TSmallintField;
    QrItensAlterWeb: TSmallintField;
    QrItensAtivo: TSmallintField;
    MeAvisos: TMemo;
    QrSum: TmySQLQuery;
    QrSumQuanti: TFloatField;
    QrSumValor: TFloatField;
    QrPediPrzIts: TmySQLQuery;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    TbEstqPtoCodUsu: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbEstqPtoBeforePost(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure InserirParcela(const ValorTot: Double; var ValorPar,
              ValorPto: Double; TabLctA: String);
  public
    { Public declarations }
  end;

  var
  FmPtosFatBal: TFmPtosFatBal;

implementation

{$R *.DFM}

uses Module, ModuleGeral, PtosFatCad, UMySQLModule, UnFinanceiro,
UnInternalConsts, UnBco_Rem, UnMyObjects;

procedure TFmPtosFatBal.BtOKClick(Sender: TObject);
var
  Codigo, Itens: Integer;
  QuantTot, ValorTot, ValorPar,
  ValorPto, ValorCon, ValorGer,
  ComisPto, ComisCon, ComisGer: Double;
  Encerrou: String;
begin
  Screen.Cursor := crHourGlass;
  Encerrou := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  BtOK.Enabled := False;
  MeAvisos.Lines.Clear;
  TbEstqPto.First;
  while not TbEstqPto.Eof do
  begin
    if TbEstqPtoQtdeNew.Value < TbEstqPtoQtdeOld.Value then
    begin
      Itens := TbEstqPtoQtdeOld.Value - TbEstqPtoQtdeNew.Value;
      //
      QrItens.Close;
      QrItens.Params[00].AsInteger := FmPtosFatCad.QrPtosFatCadPontoVda.Value;
      QrItens.Params[01].AsInteger := TbEstqPtoGraGruX.Value;
      QrItens.Params[02].AsInteger := Itens;
      QrItens.Open;
      //
      if QrItens.RecordCount > 0 then
      begin
        {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE ptosstqmov SET TipOut=1, ');
        Dmod.QrUpd.SQL.Add('Status=:P0, CodOut=:P1');
        Dmod.QrUpd.SQL.Add('WHERE IDCtrl=:Pa');
        Dmod.QrUpd.SQL.Add('');
        }
        QrItens.First;
        while not QrItens.Eof do
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosstqmov', False, [
          'CodOut', 'TipOut', 'Status'], ['IDCtrl'], [
          FmPtosFatCad.QrPtosFatCadCodigo.Value, 1, 60], [
          QrItensIDCtrl.Value], False);
          {
          Dmod.QrUpd.Params[00].AsInteger := 60;
          Dmod.QrUpd.Params[01].AsInteger := FmPtosFatCad.QrPtosFatCadCodigo.Value;
          //
          Dmod.QrUpd.Params[02].AsInteger := QrItensIDCtrl.Value;
          Dmod.QrUpd.ExecSQL;
          }//
          QrItens.Next;
        end;
        if QrItens.RecordCount <> Itens then
          MeAvisos.Lines.Add('Reduzido ' + FormatFloat('00000000',
            TbEstqPtoGraGruX.Value) +
            ': Itens a baixar = ' + FormatFloat('000000', Itens) +
            '  -  Itens baixados = ' + FormatFloat('000000', QrItens.RecordCount));
      end;
    end;
    //
    TbEstqPto.Next;
  end;
  //
  QrSum.Close;
  QrSum.Params[0].AsInteger := FmPtosFatCad.QrPtosFatCadCodigo.Value;
  QrSum.Open;
  //
  ComisPto := FmPtosFatCad.QrPtosFatCadComisPto.Value;
  ComisCon := FmPtosFatCad.QrPtosFatCadComisCon.Value;
  ComisGer := FmPtosFatCad.QrPtosFatCadComisGer.Value;
  //
  QuantTot := QrSumQuanti.Value;
  ValorTot := QrSumValor.Value;
  ValorPto := (Round(ValorTot * ComisPto)) / 100;
  ValorCon := (Round(ValorTot * ComisCon)) / 100;
  ValorGer := (Round(ValorTot * ComisGer)) / 100;
  Codigo   := FmPtosFatCad.QrPtosFatCadCodigo.Value;

  //Fazer faturamento!
  //Parei aqui!
  // Excluir faturamento atual se houver?
  QrPediPrzIts.Close;
  QrPediPrzIts.Params[0].AsInteger := FmPtosFatCad.QrPtosFatCadPediPrzCab.Value;
  QrPediPrzIts.Open;
  //
  ValorPar := 0;
  QrPediPrzIts.First;
  while not QrPediPrzIts.Eof do
  begin
    InserirParcela(ValorTot, ValorPar, ValorPto, FmPtosFatCad.FTabLctALS);
    //
    QrPediPrzIts.Next;
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosfatcad', False, [
  'Encerrou', 'QuantTot', 'ValorTot',
  'ValorPto', 'ValorCon', 'ValorGer'], ['Codigo'], [
  Encerrou, QuantTot, ValorTot,
  ValorPto, ValorCon, ValorGer], [Codigo], True);

  Screen.Cursor := crDefault;
  if MeAvisos.Text <> '' then
  begin
    MeAvisos.Visible := True;
    Application.MessageBox('H� avisos de inconsist�ncias!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end else
    Close;
end;

procedure TFmPtosFatBal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPtosFatBal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosFatBal.FormCreate(Sender: TObject);
begin
  // erro na vers�o 2.7.2 do DAC
  TbEstqPto.DataBase := DModG.MyPID_DB;
  TbEstqPto.Open;
end;

procedure TFmPtosFatBal.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPtosFatBal.InserirParcela(const ValorTot: Double; var ValorPar,
  ValorPto: Double; TabLctA: String);
var
  Agora: TDateTime;
  Credito: Double;
begin
  Agora := DModG.ObtemAgora();
  if QrPediPrzIts.RecNo = QrPediPrzIts.RecordCount then
    Credito := ValorTot - ValorPar
  else
    Credito := (Round(ValorTot * QrPediPrzItsPercent1.Value)) / 100;
  ValorPar := ValorPar + Credito;
  //
  FLAN_Documento     := UBco_Rem.ObtemProximoNossoNumero(
    FmPtosFatCad.QrPtosFatCadCNAB_Cfg.Value, True);
  //
  FLAN_Data          := Geral.FDT(Agora, 1);
  FLAN_Vencimento    := Geral.FDT(Agora + QrPediPrzItsDias.Value, 1);
  FLAN_DataCad       := Geral.FDT(Agora, 1);
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_Descricao     := Dmod.QrControleTxtVdaPto.Value;
  FLAN_Tipo          := FmPtosFatCad.QrPtosFatCadTIPOCART.Value;
  FLAN_Carteira      := FmPtosFatCad.QrPtosFatCadCarteira.Value;
  FLAN_Credito       := Credito;
  FLAN_Debito        := ValorPto;
  FLAN_Genero        := Dmod.QrControleCtaPtoVda.Value;
  FLAN_Cliente       := FmPtosFatCad.QrPtosFatCadPontoVda.Value;
  FLAN_Vendedor      := FmPtosFatCad.QrPtosFatCadConsultor.Value;
  FLAN_Account       := FmPtosFatCad.QrPtosFatCadGerente.Value;
  FLAN_CliInt        := FmPtosFatCad.QrPtosFatCadCliInt.Value; // Carteiras.ForneceI = Cliente interno
  FLAN_MoraDia       := FmPtosFatCad.QrPtosFatCadJurosMes.Value;
  FLAN_Multa         := 0;//FmPtosFatCad.QrPtosFatCadMultaPer.Value;
  FLAN_FatID         := 801;
  FLAN_FatID_Sub     := 0;
  FLAN_FatNum        := FmPtosFatCad.QrPtosFatCadCodigo.Value;
  FLAN_FatParcela    := QrPediPrzIts.RecNo;
  FLAN_Duplicata     := FormatFloat('000000', FmPtosFatCad.QrPtosFatCadCodUsu.Value) + '/' + FormatFloat('000', FLAN_FatParcela);
  //
  {
  FLAN_Depto         := 0;
  FLAN_Debito        := 0;
  FLAN_Mez           := '';
  FLAN_Doc2          := '';
  FLAN_SerieCH       := '';
  FLAN_SerieNF       := '';
  FLAN_NotaFiscal    := 0;
  FLAN_Cartao        := 0;
  FLAN_Linha         := 0;
  FLAN_Sub           := 0;
  FLAN_ID_Pgto       := 0;
  FLAN_Fornecedor    := 0;
  FLAN_ICMS_P        := 0;
  FLAN_ICMS_V        := 0;
  FLAN_DescoPor      := 0;
  FLAN_DescoVal      := 0;
  FLAN_ForneceI      := 0;
  FLAN_NFVal         := 0;
  FLAN_Unidade       := 0;
  FLAN_Qtde          := 0;
  FLAN_Emitente      := '';
  FLAN_CNPJCPF       := '';
  FLAN_Banco         := 0;
  FLAN_Agencia       := '';
  FLAN_ContaCorrente := '';
  FLAN_MultaVal      := 0;
  FLAN_MoraVal       := 0;
  FLAN_CtrlIni       := 0;
  FLAN_Nivel         := 0;
  FLAN_TipoCH        := 0;
  FLAN_Atrelado      := 0;
  FLAN_SubPgto1      := 0;
  FLAN_MultiPgto     := 0;
  FLAN_CNAB_Sit      := 0;
  }
  if FLAN_Tipo < 2 then
  begin
    FLAN_Sit         := 3;
    FLAN_Compensado  := FLAN_Vencimento;
  end;
  //
  //  parei aqui incluir lancto
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
    'Controle', TabLctA, LAN_CTOS, 'Controle');
  UFinanceiro.InsereLancamento(FmPtosFatCad.FTabLctALS);
end;

procedure TFmPtosFatBal.TbEstqPtoBeforePost(DataSet: TDataSet);
begin
  if TbEstqPtoQtdeNew.Value > TbEstqPtoQtdeOld.Value then
  begin
    Geral.MB_Aviso('A quantidade nova n�o pode ser superior � anterior!');
    TbEstqPtoQtdeNew.Value := TbEstqPtoQtdeOld.Value;
  end else
  if TbEstqPtoQtdeNew.Value < 0 then
  begin
    Application.MessageBox('A quantidade nova n�o pode ser negativa! ', 'Aviso',
      MB_OK+MB_ICONWARNING);
    TbEstqPtoQtdeNew.Value := 0;
  end;
end;

end.
