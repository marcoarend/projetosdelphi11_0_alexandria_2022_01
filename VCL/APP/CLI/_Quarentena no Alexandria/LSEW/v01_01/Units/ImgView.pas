unit ImgView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, JPEG, StdCtrls, dmkLabel;

type
  TFmImgView = class(TForm)
    Image1: TImage;
    IdHTTP1: TIdHTTP;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FImage: String;
  end;

var
  FmImgView: TFmImgView;

implementation

uses Module, UnMLAGeral, dmkGeral;

{$R *.dfm}

procedure TFmImgView.FormActivate(Sender: TObject);
var
  ImageMem : TMemoryStream;
  ImageJpg: TJPEGImage;
begin
  if ((Pos('http://', LowerCase(FImage)) > 0) or
    (Pos('www.', LowerCase(FImage)) > 0)) and (Length(FImage) > 0) then
  begin
    ImageMem := TMemoryStream.Create;
    ImageJpg := TJPEGImage.Create;
    try
      try
        if (Pos('http://', LowerCase(FImage)) > 0) then
          IdHTTP1.Get(FImage, ImageMem)
        else
          IdHTTP1.Get('http://' + FImage, ImageMem);
      except on e: EIdHTTPProtocolException do
        begin
          if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
          begin
            // N�o achou!
            Exit;
          end;
        end;
      end;
      ImageMem.Position := 0;
      ImageJpg.LoadFromStream(ImageMem);
      Image1.Picture.Assign(ImageJpg);
    finally
      ImageJpg.Free;
      ImageMem.Free;
    end;
  end else
  begin
    if Dmod.QrControlePrdFotoWeb.Value = 0 then
      Image1.Picture.LoadFromFile(FImage);
  end;
end;

procedure TFmImgView.FormCreate(Sender: TObject);
var
  Proxy: Integer;
begin
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
end;

procedure TFmImgView.Image1Click(Sender: TObject);
begin
  Close;
end;

end.
