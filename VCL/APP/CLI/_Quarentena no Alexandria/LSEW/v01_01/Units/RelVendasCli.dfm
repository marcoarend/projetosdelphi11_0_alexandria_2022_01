object FmRelVendasCli: TFmRelVendasCli
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-009 :: Relat'#243'rio de Vendas por Cliente'
  ClientHeight = 438
  ClientWidth = 834
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 390
    Width = 834
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtPesquisa: TBitBtn
      Tag = 22
      Left = 7
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 722
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 16
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 103
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      TabOrder = 2
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 834
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rio de vendas por cliente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 832
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 3
      ExplicitWidth = 830
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 834
    Height = 342
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 832
      Height = 60
      Align = alTop
      TabOrder = 0
      object CkPeriodo: TRadioGroup
        Left = 73
        Top = 1
        Width = 72
        Height = 58
        Align = alLeft
        Caption = 'Per'#237'odo em'
        Items.Strings = (
          'Dias'
          'Data')
        TabOrder = 1
        OnClick = CkPeriodoClick
      end
      object CkCliente: TRadioGroup
        Left = 1
        Top = 1
        Width = 72
        Height = 58
        Align = alLeft
        Caption = 'Clientes'
        Items.Strings = (
          'Ativos'
          'Inativos')
        TabOrder = 0
      end
      object PnPeriodo: TPanel
        Left = 145
        Top = 1
        Width = 236
        Height = 58
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object LaIni: TLabel
          Left = 10
          Top = 8
          Width = 17
          Height = 13
          Caption = 'De:'
        end
        object LaFin: TLabel
          Left = 128
          Top = 8
          Width = 6
          Height = 13
          Caption = 'a'
        end
        object EdDiasIni: TdmkEdit
          Left = 31
          Top = 4
          Width = 90
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdDiasFin: TdmkEdit
          Left = 141
          Top = 4
          Width = 90
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object TPDataFin: TDateTimePicker
          Left = 141
          Top = 29
          Width = 90
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.777157974500000000
          Time = 37636.777157974500000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object TPDataIni: TDateTimePicker
          Left = 31
          Top = 29
          Width = 90
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.777157974500000000
          Time = 37636.777157974500000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 61
      Width = 832
      Height = 280
      Align = alClient
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Cliente'
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Vendedor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'N'#186' de compras'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Valor m'#233'dio'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'TOTAL'
          Visible = True
        end>
    end
  end
end
