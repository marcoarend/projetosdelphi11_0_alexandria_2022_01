object FmPtosFatBal: TFmPtosFatBal
  Left = 339
  Top = 185
  Caption = 'PTO-VENDA-007 :: Faturamento de Ponto de Vendas por Invent'#225'rio'
  ClientHeight = 608
  ClientWidth = 871
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 549
    Width = 871
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 734
      Top = 1
      Width = 136
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 871
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Faturamento de Ponto de Vendas por Invent'#225'rio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 869
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 871
    Height = 490
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 869
      Height = 275
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsEstqPto
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodUsu'
          ReadOnly = True
          Title.Caption = 'Refer'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'No_Grade'
          Title.Caption = 'Grade'
          Width = 196
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'No_Cor'
          Title.Caption = 'Cor'
          Width = 195
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'No_Tam'
          Title.Caption = 'Tamanho'
          Width = 67
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeOld'
          Title.Caption = 'Qtd antes'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeNew'
          Title.Caption = 'Qtd nova'
          Visible = True
        end>
    end
    object MeAvisos: TMemo
      Left = 1
      Top = 276
      Width = 869
      Height = 213
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Courier New'
      Font.Style = []
      Lines.Strings = (
        'MeAvisos')
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 1
      Visible = False
    end
  end
  object TbEstqPto: TmySQLTable
    Database = DModG.DBDmk
    BeforePost = TbEstqPtoBeforePost
    TableName = 'estqpto'
    Left = 288
    Top = 52
    object TbEstqPtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'estqpto.CodUsu'
    end
    object TbEstqPtoGrade: TIntegerField
      FieldName = 'Grade'
      ReadOnly = True
    end
    object TbEstqPtoGraGruX: TIntegerField
      FieldName = 'GraGruX'
      ReadOnly = True
    end
    object TbEstqPtoCor: TIntegerField
      FieldName = 'Cor'
      ReadOnly = True
    end
    object TbEstqPtoTam: TIntegerField
      FieldName = 'Tam'
      ReadOnly = True
    end
    object TbEstqPtoNo_Grade: TWideStringField
      FieldName = 'No_Grade'
      ReadOnly = True
      Size = 30
    end
    object TbEstqPtoNo_Cor: TWideStringField
      FieldName = 'No_Cor'
      ReadOnly = True
      Size = 30
    end
    object TbEstqPtoNo_Tam: TWideStringField
      FieldName = 'No_Tam'
      ReadOnly = True
      Size = 10
    end
    object TbEstqPtoQtdeOld: TIntegerField
      FieldName = 'QtdeOld'
      ReadOnly = True
    end
    object TbEstqPtoQtdeNew: TIntegerField
      FieldName = 'QtdeNew'
    end
  end
  object DsEstqPto: TDataSource
    DataSet = TbEstqPto
    Left = 316
    Top = 52
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ptosstqmov'
      'WHERE Status=50'
      'AND Empresa=:P0'
      'AND Produto=:P1'
      'ORDER BY DataIns, IDCtrl'
      'Limit 0, :P2')
    Left = 288
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrItensIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrItensCodInn: TIntegerField
      FieldName = 'CodInn'
    end
    object QrItensCodOut: TIntegerField
      FieldName = 'CodOut'
    end
    object QrItensTipOut: TSmallintField
      FieldName = 'TipOut'
    end
    object QrItensDataIns: TDateTimeField
      FieldName = 'DataIns'
    end
    object QrItensEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItensProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrItensQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrItensPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrItensPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItensDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItensStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrItensAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrItensAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Quanti) Quanti, SUM(PrecoR*Quanti) Valor '
      'FROM ptosstqmov'
      'WHERE status=60'
      'AND TipOut=1'
      'AND CodOut=:P0')
    Left = 316
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrSumValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrPediPrzIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.*'
      'FROM pediprzits ppi'
      'LEFT JOIN ptosfatcad pfc ON pfc.PediPrzCab=ppi.Codigo'
      'WHERE pfc.Codigo=:P0'
      'ORDER BY ppi.Dias')
    Left = 288
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPediPrzItsPercent1: TFloatField
      FieldName = 'Percent1'
    end
  end
end
