unit GraCusPrc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, dmkDBGrid, dmkCheckBox,
  dmkCheckGroup, UnDmkProcFunc, UnDmkEnums;

type
  TFmGraCusPrc = class(TForm)
    PainelDados: TPanel;
    DsGraCusPrc: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    PainelEdit: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Panel4: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    QrCambioMda: TmySQLQuery;
    DsCambioMda: TDataSource;
    QrCambioMdaCodigo: TIntegerField;
    QrCambioMdaCodusu: TIntegerField;
    QrCambioMdaSigla: TWideStringField;
    QrCambioMdaNome: TWideStringField;
    DBEdit3: TDBEdit;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrGraCusPrcMoeda: TIntegerField;
    QrGraCusPrcNOMEMOEDA: TWideStringField;
    QrGraCusPrcSIGLAMDA: TWideStringField;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    QrGraCusPrcAplicacao: TIntegerField;
    Panel7: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label9: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    EdMoeda: TdmkEditCB;
    CBMoeda: TdmkDBLookupComboBox;
    CGAplicacao: TdmkCheckGroup;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraCusPrcAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraCusPrcBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmGraCusPrc: TFmGraCusPrc;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraCusPrc.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraCusPrc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraCusPrcCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraCusPrc.DefParams;
begin
  VAR_GOTOTABELA := 'gracusprc';
  VAR_GOTOMYSQLTABLE := QrGraCusPrc;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gcp.Codigo, gcp.Nome, gcp.Moeda, ');
  VAR_SQLx.Add('mda.Nome NOMEMOEDA, mda.Sigla SIGLAMDA, ');
  VAR_SQLx.Add('gcp.Aplicacao');
  VAR_SQLx.Add('FROM gracusprc gcp');
  VAR_SQLx.Add('LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE gcp.Codigo > -1000');
  //
  VAR_SQL1.Add('AND gcp.Codigo=:P0');
  //
  VAR_SQLa.Add('AND gcp.Nome Like :P0');
  //
end;

procedure TFmGraCusPrc.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmGraCusPrc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraCusPrc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraCusPrc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraCusPrc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraCusPrc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraCusPrc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraCusPrc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraCusPrc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraCusPrcCodigo.Value;
  Close;
end;

procedure TFmGraCusPrc.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGraCusPrc, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'gracusprc');
end;

procedure TFmGraCusPrc.BtConfirmaClick(Sender: TObject);
var
  Codigo, Moeda: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Moeda := EdMoeda.ValueVariant;
  if Moeda = 0 then
  begin
    Application.MessageBox('Defina a moeda.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('gracusprc', 'Codigo', LaTipo.SQLType,
    QrGraCusPrcCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmGraCusPrc, PainelEdita,
    'gracusprc', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGraCusPrc.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'gracusprc', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'gracusprc', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'gracusprc', 'Codigo');
end;

procedure TFmGraCusPrc.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrGraCusPrc, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'gracusprc');
end;

procedure TFmGraCusPrc.FormCreate(Sender: TObject);
begin
  CriaOForm;
  QrCambioMda.Open;
  Panel4.Align     := alClient;
  PainelEdit.Align := alClient;
end;

procedure TFmGraCusPrc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraCusPrcCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraCusPrc.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraCusPrc.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraCusPrcCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraCusPrc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmGraCusPrc.QrGraCusPrcAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraCusPrc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmGraCusPrc.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraCusPrcCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'gracusprc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraCusPrc.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmGraCusPrc.QrGraCusPrcBeforeOpen(DataSet: TDataSet);
begin
  QrGraCusPrcCodigo.DisplayFormat := FFormatFloat;
end;

end.



