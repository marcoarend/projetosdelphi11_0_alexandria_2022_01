object FmOpcoesLeSew: TFmOpcoesLeSew
  Left = 339
  Top = 185
  Caption = 'FER-OPCAO-002 :: Op'#231#245'es Espec'#237'ficas'
  ClientHeight = 722
  ClientWidth = 998
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 663
    Width = 998
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 860
      Top = 1
      Width = 137
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 998
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Op'#231#245'es Espec'#237'ficas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 996
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 998
    Height = 604
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object PageControl2: TPageControl
      Left = 1
      Top = 1
      Width = 996
      Height = 602
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet11
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cursos'
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 988
          Height = 571
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet5
          Align = alClient
          TabOrder = 0
          object TabSheet5: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Plano de contas no ciclo '
            object Label2: TLabel
              Left = 10
              Top = 10
              Width = 383
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Devolu'#231#227'o de empr'#233'stimos de professores (credita a empresa):'
            end
            object Label3: TLabel
              Left = 10
              Top = 128
              Width = 219
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Comiss'#227'o de promotores (provis'#227'o):'
            end
            object Label1: TLabel
              Left = 10
              Top = 59
              Width = 319
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Emprestar dinheiro a professores (debita a empresa):'
            end
            object Label4: TLabel
              Left = 10
              Top = 177
              Width = 222
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Comiss'#227'o de professores (provis'#227'o):'
            end
            object Label6: TLabel
              Left = 10
              Top = 246
              Width = 177
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '13'#186' de promotores (provis'#227'o):'
            end
            object Label5: TLabel
              Left = 10
              Top = 295
              Width = 180
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '13'#186' de professores (provis'#227'o):'
            end
            object Label0: TLabel
              Left = 10
              Top = 348
              Width = 87
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Descri'#231#227'o 13'#186':'
            end
            object EdCtaEmpPrfC: TdmkEditCB
              Left = 10
              Top = 30
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtaEmpPrfC
              IgnoraDBLookupComboBox = False
            end
            object CBCtaEmpPrfC: TdmkDBLookupComboBox
              Left = 84
              Top = 30
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas1
              TabOrder = 1
              dmkEditCB = EdCtaEmpPrfC
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBCtaEmpPrfD: TdmkDBLookupComboBox
              Left = 84
              Top = 79
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas2
              TabOrder = 3
              dmkEditCB = EdCtaEmpPrfD
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCtaEmpPrfD: TdmkEditCB
              Left = 10
              Top = 79
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtaEmpPrfD
              IgnoraDBLookupComboBox = False
            end
            object EdCtaComProm: TdmkEditCB
              Left = 10
              Top = 148
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtaComProm
              IgnoraDBLookupComboBox = False
            end
            object CBCtaComProm: TdmkDBLookupComboBox
              Left = 84
              Top = 148
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas3
              TabOrder = 5
              dmkEditCB = EdCtaComProm
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCtaComProf: TdmkEditCB
              Left = 10
              Top = 197
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtaComProf
              IgnoraDBLookupComboBox = False
            end
            object CBCtaComProf: TdmkDBLookupComboBox
              Left = 84
              Top = 197
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas4
              TabOrder = 7
              dmkEditCB = EdCtaComProf
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCta13oProm: TdmkEditCB
              Left = 10
              Top = 266
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCta13oProm
              IgnoraDBLookupComboBox = False
            end
            object CBCta13oProm: TdmkDBLookupComboBox
              Left = 84
              Top = 266
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas5
              TabOrder = 9
              dmkEditCB = EdCta13oProm
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCta13oProf: TdmkEditCB
              Left = 10
              Top = 315
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCta13oProf
              IgnoraDBLookupComboBox = False
            end
            object CBCta13oProf: TdmkDBLookupComboBox
              Left = 84
              Top = 315
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas6
              TabOrder = 11
              dmkEditCB = EdCta13oProf
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdNome13: TdmkEdit
              Left = 10
              Top = 367
              Width = 98
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Centros de custo especiais '
            ImageIndex = 1
            object Label7: TLabel
              Left = 10
              Top = 10
              Width = 252
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Despesas das comiss'#245'es de promotores:'
            end
            object Label8: TLabel
              Left = 10
              Top = 128
              Width = 255
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Despesas das comiss'#245'es de professores:'
            end
            object Label9: TLabel
              Left = 10
              Top = 59
              Width = 205
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Despesas do 13'#186' dos promotores:'
            end
            object Label10: TLabel
              Left = 10
              Top = 177
              Width = 208
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Despesas do 13'#186' dos professores:'
            end
            object EdCCuPromCom: TdmkEditCB
              Left = 10
              Top = 30
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCCuPromCom
              IgnoraDBLookupComboBox = False
            end
            object CBCCuPromCom: TdmkDBLookupComboBox
              Left = 84
              Top = 30
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCC1
              TabOrder = 1
              dmkEditCB = EdCCuPromCom
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBCCuPromDTS: TdmkDBLookupComboBox
              Left = 84
              Top = 79
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCC2
              TabOrder = 3
              dmkEditCB = EdCCuPromDTS
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCCuPromDTS: TdmkEditCB
              Left = 10
              Top = 79
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCCuPromDTS
              IgnoraDBLookupComboBox = False
            end
            object CBCCuProfCom: TdmkDBLookupComboBox
              Left = 84
              Top = 148
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCC3
              TabOrder = 5
              dmkEditCB = EdCCuProfCom
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCCuProfDTS: TdmkEditCB
              Left = 10
              Top = 197
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCCuProfDTS
              IgnoraDBLookupComboBox = False
            end
            object CBCCuProfDTS: TdmkDBLookupComboBox
              Left = 84
              Top = 197
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCC4
              TabOrder = 7
              dmkEditCB = EdCCuProfDTS
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCCuProfCom: TdmkEditCB
              Left = 10
              Top = 148
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCCuPromDTS
              IgnoraDBLookupComboBox = False
            end
          end
          object TabSheet7: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Rateio '
            ImageIndex = 2
            object Label11: TLabel
              Left = 10
              Top = 10
              Width = 487
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Caixa para transferir automaticamente o dinheiro retido em viage' +
                'm pelo professor:'
            end
            object Label12: TLabel
              Left = 10
              Top = 64
              Width = 213
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Comiss'#227'o de professores (retidas) :'
            end
            object Label13: TLabel
              Left = 10
              Top = 118
              Width = 441
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Texto para hist'#243'rico na reten'#231#227'o autom'#225'tica das comiss'#245'es  do pr' +
                'ofessor:'
            end
            object EdCxaPgtProf: TdmkEditCB
              Left = 10
              Top = 30
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCxaPgtProf
              IgnoraDBLookupComboBox = False
            end
            object CBCxaPgtProf: TdmkDBLookupComboBox
              Left = 84
              Top = 30
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCxaPgtProf
              TabOrder = 1
              dmkEditCB = EdCxaPgtProf
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCtaPagProf: TdmkEditCB
              Left = 10
              Top = 84
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtaPagProf
              IgnoraDBLookupComboBox = False
            end
            object CBCtaPagProf: TdmkDBLookupComboBox
              Left = 84
              Top = 84
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas4
              TabOrder = 3
              dmkEditCB = EdCtaPagProf
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdTxtPgtProf: TEdit
              Left = 10
              Top = 138
              Width = 548
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 4
            end
          end
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Ciclos '
            ImageIndex = 3
            object Label14: TLabel
              Left = 10
              Top = 10
              Width = 136
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Departamento padr'#227'o:'
            end
            object Label15: TLabel
              Left = 10
              Top = 69
              Width = 142
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Lista de pre'#231'os padr'#227'o:'
            end
            object Label30: TLabel
              Left = 10
              Top = 128
              Width = 134
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cliente interno padr'#227'o:'
            end
            object EdCBDepto: TdmkEditCB
              Left = 10
              Top = 30
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBDepto
              IgnoraDBLookupComboBox = False
            end
            object CBDepto: TdmkDBLookupComboBox
              Left = 84
              Top = 30
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCiclGradeD
              TabOrder = 1
              dmkEditCB = EdCBDepto
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCBListPrd: TdmkEditCB
              Left = 10
              Top = 89
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBListPrd
              IgnoraDBLookupComboBox = False
            end
            object CBListPrd: TdmkDBLookupComboBox
              Left = 84
              Top = 89
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCiclListPrd
              TabOrder = 3
              dmkEditCB = EdCBListPrd
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCiclCliInt: TdmkEditCB
              Left = 10
              Top = 149
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCiclCliInt
              IgnoraDBLookupComboBox = False
            end
            object CBCiclCliInt: TdmkDBLookupComboBox
              Left = 84
              Top = 149
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'NOMEENTI'
              ListSource = DsCiclCliInt
              TabOrder = 5
              dmkEditCB = EdCiclCliInt
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pontos de venda'
        ImageIndex = 1
        object Label16: TLabel
          Left = 10
          Top = 10
          Width = 136
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Departamento padr'#227'o:'
        end
        object Label18: TLabel
          Left = 10
          Top = 59
          Width = 327
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta do plano de contas para lan'#231'amento de vendas:'
        end
        object Label17: TLabel
          Left = 10
          Top = 108
          Width = 279
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Texto para hist'#243'rico de lan'#231'amentos na venda:'
        end
        object EdDeptoPto: TdmkEditCB
          Left = 10
          Top = 30
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBDeptoPto
          IgnoraDBLookupComboBox = False
        end
        object CBDeptoPto: TdmkDBLookupComboBox
          Left = 84
          Top = 30
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPtosGradeD
          TabOrder = 1
          dmkEditCB = EdDeptoPto
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBCtaPtoVda: TdmkDBLookupComboBox
          Left = 84
          Top = 79
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas7
          TabOrder = 2
          dmkEditCB = EdCtaPtoVda
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCtaPtoVda: TdmkEditCB
          Left = 10
          Top = 79
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCtaPtoVda
          IgnoraDBLookupComboBox = False
        end
        object EdTxtVdaPto: TdmkEdit
          Left = 10
          Top = 128
          Width = 548
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lingerie'
        ImageIndex = 2
        object Label36: TLabel
          Left = 10
          Top = 10
          Width = 136
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Departamento padr'#227'o:'
        end
        object Label37: TLabel
          Left = 10
          Top = 64
          Width = 135
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lista de pre'#231'o padr'#227'o:'
        end
        object Label39: TLabel
          Left = 10
          Top = 117
          Width = 200
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Carteira padr'#227'o para pagamento:'
        end
        object Label40: TLabel
          Left = 10
          Top = 171
          Width = 176
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta padr'#227'o do pagamento:'
        end
        object Label41: TLabel
          Left = 10
          Top = 225
          Width = 302
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Condi'#231#227'o de pagamento padr'#227'o para pagamento:'
        end
        object Label42: TLabel
          Left = 10
          Top = 282
          Width = 279
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Texto para hist'#243'rico de lan'#231'amentos na venda:'
        end
        object Label43: TLabel
          Left = 487
          Top = 282
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[CODIGO]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = Label43Click
        end
        object EdLinDepto: TdmkEditCB
          Left = 10
          Top = 30
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBLinDepto
          IgnoraDBLookupComboBox = False
        end
        object CBLinDepto: TdmkDBLookupComboBox
          Left = 84
          Top = 30
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsLinDepto
          TabOrder = 1
          dmkEditCB = EdLinDepto
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdLinGraPrc: TdmkEditCB
          Left = 10
          Top = 84
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBLinGraPrc
          IgnoraDBLookupComboBox = False
        end
        object CBLinGraPrc: TdmkDBLookupComboBox
          Left = 84
          Top = 84
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsLinGraPrc
          TabOrder = 3
          dmkEditCB = EdLinGraPrc
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBLinCart: TdmkDBLookupComboBox
          Left = 84
          Top = 137
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsLinCart
          TabOrder = 4
          dmkEditCB = EdLinCart
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdLinCart: TdmkEditCB
          Left = 10
          Top = 137
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBLinCart
          IgnoraDBLookupComboBox = False
        end
        object EdLinConta: TdmkEditCB
          Left = 10
          Top = 191
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBLinConta
          IgnoraDBLookupComboBox = False
        end
        object CBLinConta: TdmkDBLookupComboBox
          Left = 84
          Top = 191
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsLinConta
          TabOrder = 7
          dmkEditCB = EdLinConta
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBLinCPgto: TdmkDBLookupComboBox
          Left = 84
          Top = 245
          Width = 474
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsLinCPgto
          TabOrder = 8
          dmkEditCB = EdLinCPgto
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdLinCPgto: TdmkEditCB
          Left = 10
          Top = 245
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBLinCPgto
          IgnoraDBLookupComboBox = False
        end
        object EdLinTxt: TdmkEdit
          Left = 10
          Top = 303
          Width = 548
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 30
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGFatSEtqLin: TdmkRadioGroup
          Left = 10
          Top = 338
          Width = 548
          Height = 47
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Permite faturamento de item sem estoque suficiente: '
          Columns = 3
          ItemIndex = 2
          Items.Strings = (
            'N'#227'o permite'
            'Permite sem avisar'
            'Perguntar antes')
          TabOrder = 11
          UpdType = utYes
          OldValor = 0
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Venda de produtos'
        ImageIndex = 3
        object PageControl3: TPageControl
          Left = 0
          Top = 0
          Width = 988
          Height = 571
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet9
          Align = alClient
          TabOrder = 0
          object TabSheet9: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Venda de produtos'
            object Label27: TLabel
              Left = 495
              Top = 172
              Width = 279
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Texto para hist'#243'rico de lan'#231'amentos na venda:'
            end
            object LaEndRastre: TLabel
              Left = 417
              Top = 172
              Width = 69
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '[CODIGO]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = LaEndRastreClick
            end
            object Label25: TLabel
              Left = 4
              Top = 172
              Width = 175
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Endere'#231'o para rastreamento:'
            end
            object Label26: TLabel
              Left = 4
              Top = 197
              Width = 32
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'http://'
            end
            object Label28: TLabel
              Left = 4
              Top = 228
              Width = 368
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Texto para hist'#243'rico de lan'#231'amentos de vendas sem estoque:'
            end
            object Label31: TLabel
              Left = 4
              Top = 281
              Width = 200
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Carteira padr'#227'o para pagamento:'
            end
            object Label33: TLabel
              Left = 4
              Top = 335
              Width = 302
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Condi'#231#227'o de pagamento padr'#227'o para pagamento:'
            end
            object Label52: TLabel
              Left = 4
              Top = 389
              Width = 136
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Departamento padr'#227'o:'
            end
            object Label38: TLabel
              Left = 495
              Top = 335
              Width = 135
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Lista de pre'#231'o padr'#227'o:'
            end
            object Label32: TLabel
              Left = 495
              Top = 281
              Width = 176
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Conta padr'#227'o do pagamento:'
            end
            object Label29: TLabel
              Left = 495
              Top = 229
              Width = 164
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Caminho do logo PAC [F4]: '
            end
            object LaTxtVdaPro: TLabel
              Left = 906
              Top = 170
              Width = 69
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '[CODIGO]'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = LaTxtVdaProClick
            end
            object Label69: TLabel
              Left = 495
              Top = 389
              Width = 296
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Carteira padr'#227'o para confirma'#231#227'o de pagamento:'
            end
            object GroupBox2: TGroupBox
              Left = 492
              Top = 4
              Width = 484
              Height = 81
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Status 2'
              TabOrder = 0
              object Label21: TLabel
                Left = 10
                Top = 20
                Width = 40
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status:'
              end
              object Label22: TLabel
                Left = 396
                Top = 20
                Width = 24
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cor:'
              end
              object SpeedButton1: TSpeedButton
                Left = 450
                Top = 39
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton1Click
              end
              object EdStatus2: TdmkEditCB
                Left = 10
                Top = 39
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBStatus2
                IgnoraDBLookupComboBox = False
              end
              object CBStatus2: TdmkDBLookupComboBox
                Left = 84
                Top = 39
                Width = 307
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsStatus2
                TabOrder = 1
                dmkEditCB = EdStatus2
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdCor2: TdmkEdit
                Left = 396
                Top = 39
                Width = 50
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Enabled = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox1: TGroupBox
              Left = 4
              Top = 4
              Width = 483
              Height = 81
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Status 1'
              TabOrder = 1
              object Label19: TLabel
                Left = 10
                Top = 20
                Width = 40
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status:'
              end
              object Label20: TLabel
                Left = 396
                Top = 20
                Width = 24
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cor:'
              end
              object SpeedButton6: TSpeedButton
                Left = 450
                Top = 39
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton6Click
              end
              object EdStatus1: TdmkEditCB
                Left = 10
                Top = 39
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBStatus1
                IgnoraDBLookupComboBox = False
              end
              object CBStatus1: TdmkDBLookupComboBox
                Left = 84
                Top = 39
                Width = 307
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsStatus1
                TabOrder = 1
                dmkEditCB = EdStatus1
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdCor1: TdmkEdit
                Left = 396
                Top = 39
                Width = 50
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Enabled = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox3: TGroupBox
              Left = 4
              Top = 86
              Width = 483
              Height = 81
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Status 3'
              TabOrder = 2
              object Label23: TLabel
                Left = 10
                Top = 20
                Width = 40
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status:'
              end
              object Label24: TLabel
                Left = 396
                Top = 20
                Width = 24
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cor:'
              end
              object SpeedButton2: TSpeedButton
                Left = 450
                Top = 39
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton2Click
              end
              object EdStatus3: TdmkEditCB
                Left = 10
                Top = 39
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBStatus3
                IgnoraDBLookupComboBox = False
              end
              object CBStatus3: TdmkDBLookupComboBox
                Left = 84
                Top = 39
                Width = 307
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsStatus3
                TabOrder = 1
                dmkEditCB = EdStatus3
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdCor3: TdmkEdit
                Left = 396
                Top = 39
                Width = 50
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Enabled = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox4: TGroupBox
              Left = 492
              Top = 86
              Width = 484
              Height = 81
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Status 4'
              TabOrder = 3
              object Label34: TLabel
                Left = 10
                Top = 20
                Width = 40
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status:'
              end
              object Label35: TLabel
                Left = 396
                Top = 20
                Width = 24
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cor:'
              end
              object SpeedButton3: TSpeedButton
                Left = 450
                Top = 39
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton3Click
              end
              object EdStatus4: TdmkEditCB
                Left = 10
                Top = 39
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBStatus4
                IgnoraDBLookupComboBox = False
              end
              object CBStatus4: TdmkDBLookupComboBox
                Left = 84
                Top = 39
                Width = 307
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsStatus4
                TabOrder = 1
                dmkEditCB = EdStatus4
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdCor4: TdmkEdit
                Left = 396
                Top = 39
                Width = 50
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Enabled = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object EdTxtVdaPro: TdmkEdit
              Left = 492
              Top = 193
              Width = 484
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              MaxLength = 30
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEndRastre: TdmkEdit
              Left = 49
              Top = 193
              Width = 438
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              MaxLength = 255
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTxtVdaEsq: TdmkEdit
              Left = 4
              Top = 249
              Width = 483
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              MaxLength = 15
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVdaCart: TdmkEditCB
              Left = 4
              Top = 300
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBVdaCart
              IgnoraDBLookupComboBox = False
            end
            object CBVdaCart: TdmkDBLookupComboBox
              Left = 78
              Top = 300
              Width = 409
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsVdaCart
              TabOrder = 8
              dmkEditCB = EdVdaCart
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdVdaConPgto: TdmkEditCB
              Left = 4
              Top = 354
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBVdaConPgto
              IgnoraDBLookupComboBox = False
            end
            object CBVdaConPgto: TdmkDBLookupComboBox
              Left = 78
              Top = 354
              Width = 409
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsVdaConPgto
              TabOrder = 10
              dmkEditCB = EdVdaConPgto
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBPrdGradeD: TdmkDBLookupComboBox
              Left = 78
              Top = 409
              Width = 409
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPrdGradeD
              TabOrder = 11
              dmkEditCB = EdPrdGradeD
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPrdGradeD: TdmkEditCB
              Left = 4
              Top = 409
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBPrdGradeD
              IgnoraDBLookupComboBox = False
            end
            object EdVPGraPrc: TdmkEditCB
              Left = 495
              Top = 354
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 13
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBVPGraPrc
              IgnoraDBLookupComboBox = False
            end
            object CBVPGraPrc: TdmkDBLookupComboBox
              Left = 566
              Top = 354
              Width = 410
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsVPGraPrc
              TabOrder = 14
              dmkEditCB = EdVPGraPrc
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBVdaCont: TdmkDBLookupComboBox
              Left = 566
              Top = 300
              Width = 410
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsVdaCont
              TabOrder = 15
              dmkEditCB = EdVdaCont
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdVdaCont: TdmkEditCB
              Left = 495
              Top = 300
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBVdaCont
              IgnoraDBLookupComboBox = False
            end
            object EdLogoMoviV: TdmkEdit
              Left = 492
              Top = 249
              Width = 484
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              MaxLength = 15
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdLogoMoviVKeyDown
            end
            object EdMoviVCaPag: TdmkEditCB
              Left = 495
              Top = 409
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 18
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMoviVCaPag
              IgnoraDBLookupComboBox = False
            end
            object CBMoviVCaPag: TdmkDBLookupComboBox
              Left = 566
              Top = 409
              Width = 410
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCartMoviConf
              TabOrder = 19
              dmkEditCB = EdMoviVCaPag
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object RGFatSEtqKit: TdmkRadioGroup
              Left = 4
              Top = 443
              Width = 483
              Height = 47
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Permite faturamento de item sem estoque suficiente: '
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'N'#227'o permite'
                'Permite sem avisar'
                'Perguntar antes')
              TabOrder = 20
              UpdType = utYes
              OldValor = 0
            end
          end
          object TabSheet10: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Fotos'
            ImageIndex = 1
            object Label44: TLabel
              Left = 14
              Top = 10
              Width = 259
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Diret'#243'rio para armazenamento de imagens:'
            end
            object Label45: TLabel
              Left = 14
              Top = 64
              Width = 130
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Largura das imagens:'
            end
            object SpeedButton4: TSpeedButton
              Left = 500
              Top = 31
              Width = 26
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton4Click
            end
            object EdPrdFoto: TdmkEdit
              Left = 14
              Top = 31
              Width = 483
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              MaxLength = 30
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPrdFotoTam: TdmkEdit
              Left = 14
              Top = 85
              Width = 134
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              MaxLength = 30
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkPrdFotoWeb: TCheckBox
              Left = 158
              Top = 87
              Width = 174
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Armazenar fotos na WEB'
              TabOrder = 2
            end
          end
        end
      end
      object TabSheet11: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cartas'
        ImageIndex = 4
        object Label57: TLabel
          Left = 14
          Top = 73
          Width = 136
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Departamento padr'#227'o:'
        end
        object Label54: TLabel
          Left = 14
          Top = 132
          Width = 99
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Texto aprovado:'
        end
        object Label55: TLabel
          Left = 14
          Top = 186
          Width = 103
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Texto reprovado:'
        end
        object Label62: TLabel
          Left = 14
          Top = 240
          Width = 90
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Motivo padr'#227'o:'
        end
        object Label56: TLabel
          Left = 14
          Top = 302
          Width = 259
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Diret'#243'rio para armazenamento de imagens:'
        end
        object Label53: TLabel
          Left = 14
          Top = 358
          Width = 130
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Largura das imagens:'
        end
        object Label63: TLabel
          Left = 155
          Top = 358
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lucro aluno %:'
        end
        object SpeedButton5: TSpeedButton
          Left = 466
          Top = 325
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label74: TLabel
          Left = 14
          Top = 15
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente Interno:'
        end
        object EdAvaGradeD: TdmkEditCB
          Left = 14
          Top = 95
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvaGradeD
          IgnoraDBLookupComboBox = False
        end
        object CBAvaGradeD: TdmkDBLookupComboBox
          Left = 86
          Top = 95
          Width = 406
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPrdGradeD
          TabOrder = 3
          dmkEditCB = EdAvaGradeD
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBAvaTexApr: TdmkDBLookupComboBox
          Left = 86
          Top = 153
          Width = 406
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCartas1
          TabOrder = 4
          dmkEditCB = EdAvaTexApr
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAvaTexApr: TdmkEditCB
          Left = 14
          Top = 151
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvaTexApr
          IgnoraDBLookupComboBox = False
        end
        object EdAvaTexRep: TdmkEditCB
          Left = 14
          Top = 206
          Width = 68
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvaTexRep
          IgnoraDBLookupComboBox = False
        end
        object CBAvaTexRep: TdmkDBLookupComboBox
          Left = 86
          Top = 206
          Width = 406
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCartas2
          TabOrder = 7
          dmkEditCB = EdAvaTexRep
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBAvaMotivo: TdmkDBLookupComboBox
          Left = 86
          Top = 260
          Width = 406
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMotivo
          TabOrder = 8
          dmkEditCB = EdAvaMotivo
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAvaMotivo: TdmkEditCB
          Left = 14
          Top = 260
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvaMotivo
          IgnoraDBLookupComboBox = False
        end
        object EdAvaDir: TdmkEdit
          Left = 14
          Top = 325
          Width = 449
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 30
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdAvaFoto: TdmkEdit
          Left = 14
          Top = 379
          Width = 134
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          MaxLength = 30
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAvaLucro: TdmkEdit
          Left = 155
          Top = 379
          Width = 134
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          MaxLength = 30
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAvaEmpresa: TdmkEditCB
          Left = 14
          Top = 37
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvaEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBAvaEmpresa: TdmkDBLookupComboBox
          Left = 86
          Top = 37
          Width = 406
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NOMEENTI'
          ListSource = DsAvaEmpresa
          TabOrder = 1
          dmkEditCB = EdAvaEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object TabSheet12: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Web'
        ImageIndex = 5
        object Label64: TLabel
          Left = 533
          Top = 224
          Width = 139
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Caminho do logo WEB:'
        end
        object Label49: TLabel
          Left = 533
          Top = 282
          Width = 54
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'URL site:'
        end
        object Label50: TLabel
          Left = 770
          Top = 331
          Width = 193
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Exemplo: http://www.seusite.com'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = cl3DDkShadow
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object GroupBox5: TGroupBox
          Left = 4
          Top = 15
          Width = 522
          Height = 209
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Dados FTP'
          TabOrder = 0
          object Label46: TLabel
            Left = 10
            Top = 21
            Width = 54
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Servidor:'
          end
          object Label47: TLabel
            Left = 10
            Top = 74
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Usu'#225'rio:'
          end
          object Label48: TLabel
            Left = 265
            Top = 21
            Width = 62
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pasta raiz:'
          end
          object Label51: TLabel
            Left = 265
            Top = 74
            Width = 42
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Senha:'
          end
          object EdWeb_Raiz: TdmkEdit
            Left = 265
            Top = 44
            Width = 247
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_Raiz'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdWeb_FTPu: TdmkEdit
            Left = 10
            Top = 94
            Width = 247
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdWeb_FTPh: TdmkEdit
            Left = 10
            Top = 44
            Width = 247
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPh'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdWeb_FTPs: TdmkEdit
            Left = 265
            Top = 94
            Width = 247
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPSenha'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object BitBtn1: TBitBtn
            Tag = 553
            Left = 10
            Top = 149
            Width = 247
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Testar configura'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BitBtn1Click
          end
          object CkWeb_FTPpassivo: TCheckBox
            Left = 10
            Top = 123
            Width = 111
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Modo Passivo'
            TabOrder = 5
          end
        end
        object GroupBox6: TGroupBox
          Left = 533
          Top = 15
          Width = 434
          Height = 200
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Proxy'
          TabOrder = 1
          object Label58: TLabel
            Left = 9
            Top = 126
            Width = 42
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Senha:'
          end
          object Label59: TLabel
            Left = 9
            Top = 74
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Usu'#225'rio:'
          end
          object Label60: TLabel
            Left = 263
            Top = 74
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Sevidor:'
          end
          object Label61: TLabel
            Left = 263
            Top = 126
            Width = 35
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Porta:'
          end
          object CkProxUse: TdmkCheckBox
            Left = 9
            Top = 20
            Width = 145
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Usar conex'#227'o'
            TabOrder = 0
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object CkProxBAut: TdmkCheckBox
            Left = 9
            Top = 44
            Width = 145
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Autentica'#231#227'o b'#225'sica'
            TabOrder = 1
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object EdProxPass: TdmkEdit
            Left = 9
            Top = 146
            Width = 247
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPSenha'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdProxUser: TdmkEdit
            Left = 9
            Top = 94
            Width = 247
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdProxServ: TdmkEdit
            Left = 263
            Top = 94
            Width = 162
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdProxPort: TdmkEdit
            Left = 263
            Top = 146
            Width = 162
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Web_FTPu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object EdWebLogo: TdmkEdit
          Left = 533
          Top = 245
          Width = 434
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdWeb_MyURL: TdmkEdit
          Left = 533
          Top = 303
          Width = 434
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Web_MyURL'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object GroupBox7: TGroupBox
          Left = 4
          Top = 233
          Width = 522
          Height = 283
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Banco de dados WEB'
          TabOrder = 4
          object Label65: TLabel
            Left = 10
            Top = 20
            Width = 54
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Servidor:'
          end
          object Label66: TLabel
            Left = 10
            Top = 69
            Width = 50
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Usu'#225'rio:'
          end
          object Label67: TLabel
            Left = 265
            Top = 69
            Width = 42
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Senha:'
          end
          object Label68: TLabel
            Left = 265
            Top = 20
            Width = 103
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Banco de dados:'
          end
          object RGWeb_MySQL: TRadioGroup
            Left = 10
            Top = 122
            Width = 502
            Height = 88
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' MySQL no meu site: '
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o possuo, ou n'#227'o tenho acesso direto.'
              'Conectar somente quando eu necessitar.'
              'Conectar automaticamente ao iniciar.')
            TabOrder = 4
          end
          object EdWeb_Host: TdmkEdit
            Left = 10
            Top = 39
            Width = 247
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdWeb_DB: TdmkEdit
            Left = 265
            Top = 39
            Width = 247
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdWeb_Pwd: TdmkEdit
            Left = 265
            Top = 89
            Width = 247
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdWeb_User: TdmkEdit
            Left = 10
            Top = 89
            Width = 247
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object BtTestar: TBitBtn
            Tag = 10001
            Left = 10
            Top = 218
            Width = 247
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Testar configura'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = BtTestarClick
          end
        end
      end
      object TabSheet13: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Geral'
        ImageIndex = 6
        object GroupBox8: TGroupBox
          Left = 14
          Top = 5
          Width = 209
          Height = 129
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Validade do Curso'
          TabOrder = 0
          object Label70: TLabel
            Left = 10
            Top = 20
            Width = 34
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Anos:'
          end
          object Label71: TLabel
            Left = 10
            Top = 69
            Width = 193
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo do Produto para c'#225'lculo:'
          end
          object EdValCur: TdmkEdit
            Left = 10
            Top = 38
            Width = 187
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            MaxLength = 30
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdValCurPrd: TdmkEdit
            Left = 10
            Top = 87
            Width = 187
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            MaxLength = 30
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object TabSheet14: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Custo M'#233'dio'
        ImageIndex = 7
        object GroupBox9: TGroupBox
          Left = 14
          Top = 5
          Width = 401
          Height = 55
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Custo m'#233'dio'
          TabOrder = 0
          object Label72: TLabel
            Left = 10
            Top = 22
            Width = 132
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Considerar as '#250'ltimas '
          end
          object Label73: TLabel
            Left = 242
            Top = 22
            Width = 147
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'compras no custo m'#233'dio'
          end
          object EdMoviCCons: TdmkEdit
            Left = 143
            Top = 18
            Width = 92
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            MaxLength = 30
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 12
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas1: TDataSource
    DataSet = QrContas
    Left = 512
    Top = 8
  end
  object DsContas2: TDataSource
    DataSet = QrContas
    Left = 540
    Top = 8
  end
  object DsContas3: TDataSource
    DataSet = QrContas
    Left = 568
    Top = 8
  end
  object DsContas4: TDataSource
    DataSet = QrContas
    Left = 596
    Top = 8
  end
  object DsContas5: TDataSource
    DataSet = QrContas
    Left = 624
    Top = 8
  end
  object DsContas6: TDataSource
    DataSet = QrContas
    Left = 652
    Top = 8
  end
  object QrCentroCusto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM centrocusto'
      'ORDER BY Nome')
    Left = 40
    Top = 8
    object QrCentroCustoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCC1: TDataSource
    DataSet = QrCentroCusto
    Left = 684
    Top = 8
  end
  object DsCC2: TDataSource
    DataSet = QrCentroCusto
    Left = 712
    Top = 8
  end
  object DsCC3: TDataSource
    DataSet = QrCentroCusto
    Left = 740
    Top = 8
  end
  object DsCC4: TDataSource
    DataSet = QrCentroCusto
    Left = 768
    Top = 8
  end
  object QrCxaPgtProf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=0'
      'ORDER BY Nome')
    Left = 484
    Top = 316
    object QrCxaPgtProfCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCxaPgtProfNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCxaPgtProf: TDataSource
    DataSet = QrCxaPgtProf
    Left = 512
    Top = 316
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 480
    Top = 236
  end
  object QrCiclListPrd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Ativo = 1')
    Left = 484
    Top = 344
    object QrCiclListPrdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCiclListPrdNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCiclListPrd: TDataSource
    DataSet = QrCiclListPrd
    Left = 512
    Top = 344
  end
  object QrCiclGradeD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graded'
      'WHERE Ativo = 1'
      '')
    Left = 484
    Top = 372
    object QrCiclGradeDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCiclGradeDNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCiclGradeD: TDataSource
    DataSet = QrCiclGradeD
    Left = 512
    Top = 372
  end
  object QrPtosGradeD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graded'
      'WHERE Ativo = 1')
    Left = 544
    Top = 236
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPtosGradeD: TDataSource
    DataSet = QrPtosGradeD
    Left = 572
    Top = 236
  end
  object QrContas7: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome'
      '')
    Left = 544
    Top = 264
    object QrContas7Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContas7Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas7: TDataSource
    DataSet = QrContas7
    Left = 572
    Top = 264
  end
  object QrStatus2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM statusv'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 144
    Top = 8
    object QrStatus2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStatus2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsStatus2: TDataSource
    DataSet = QrStatus2
    Left = 172
    Top = 8
  end
  object ColorDialog1: TColorDialog
    Left = 572
    Top = 344
  end
  object QrStatus1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM statusv'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 88
    Top = 8
    object QrStatus1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStatus1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsStatus1: TDataSource
    DataSet = QrStatus1
    Left = 116
    Top = 8
  end
  object DsStatus3: TDataSource
    DataSet = QrStatus3
    Left = 228
    Top = 8
  end
  object QrStatus3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM statusv'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 200
    Top = 8
    object QrStatus3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStatus3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 569
    Top = 377
  end
  object QrCiclCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cli.Codigo, cli.Cliente,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI '
      'FROM cliint cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'WHERE cli.Ativo = 1')
    Left = 544
    Top = 292
    object QrCiclCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCiclCliIntCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCiclCliIntNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCiclCliInt: TDataSource
    DataSet = QrCiclCliInt
    Left = 572
    Top = 292
  end
  object QrVdaCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 704
    Top = 356
    object QrVdaCartCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVdaCartNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsVdaCart: TDataSource
    DataSet = QrVdaCart
    Left = 732
    Top = 356
  end
  object QrVdaCont: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'AND Credito = '#39'V'#39
      'ORDER BY Nome')
    Left = 704
    Top = 384
    object QrVdaContCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVdaContNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsVdaCont: TDataSource
    DataSet = QrVdaCont
    Left = 732
    Top = 384
  end
  object QrVdaConPgto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Nome'
      'FROM pediprzcab cab'
      'ORDER BY Nome')
    Left = 704
    Top = 412
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsVdaConPgto: TDataSource
    DataSet = QrVdaConPgto
    Left = 732
    Top = 412
  end
  object QrStatus4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM statusv'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 256
    Top = 8
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsStatus4: TDataSource
    DataSet = QrStatus4
    Left = 284
    Top = 8
  end
  object QrLinDepto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graded'
      'WHERE Ativo = 1')
    Left = 484
    Top = 288
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsLinDepto: TDataSource
    DataSet = QrLinDepto
    Left = 512
    Top = 289
  end
  object QrVPGraPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Ativo = 1'
      'AND Aplicacao & 2')
    Left = 352
    Top = 416
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsVPGraPrc: TDataSource
    DataSet = QrVPGraPrc
    Left = 380
    Top = 416
  end
  object DsLinGraPrc: TDataSource
    DataSet = QrLinGraPrc
    Left = 380
    Top = 444
  end
  object QrLinGraPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Ativo = 1'
      'AND Aplicacao & 4')
    Left = 352
    Top = 444
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField6: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrLinCPgto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Nome'
      'FROM pediprzcab cab'
      'ORDER BY Nome')
    Left = 628
    Top = 328
    object IntegerField7: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField7: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsLinCPgto: TDataSource
    DataSet = QrLinCPgto
    Left = 656
    Top = 328
  end
  object QrLinConta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'AND Credito = '#39'V'#39
      'ORDER BY Nome')
    Left = 628
    Top = 300
    object IntegerField8: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField8: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsLinConta: TDataSource
    DataSet = QrLinConta
    Left = 656
    Top = 300
  end
  object DsLinCart: TDataSource
    DataSet = QrLinCart
    Left = 656
    Top = 272
  end
  object QrLinCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 628
    Top = 272
    object IntegerField9: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField9: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrCartas1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cartag'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 127
    Top = 261
    object QrCartas1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartas1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCartas1: TDataSource
    DataSet = QrCartas1
    Left = 155
    Top = 261
  end
  object QrCartas2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cartag'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 127
    Top = 289
    object QrCartas2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartas2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCartas2: TDataSource
    DataSet = QrCartas2
    Left = 155
    Top = 289
  end
  object QrPrdGradeD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graded'
      'WHERE Ativo = 1')
    Left = 484
    Top = 400
    object IntegerField10: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField10: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPrdGradeD: TDataSource
    DataSet = QrPrdGradeD
    Left = 512
    Top = 400
  end
  object QrAvaGradeD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graded'
      'WHERE Ativo = 1')
    Left = 484
    Top = 428
    object IntegerField11: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField11: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAvaGradeD: TDataSource
    DataSet = QrAvaGradeD
    Left = 512
    Top = 428
  end
  object DsMotivo: TDataSource
    DataSet = QrMotivo
    Left = 572
    Top = 208
  end
  object QrMotivo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM motivos'
      'WHERE Codigo > 0')
    Left = 544
    Top = 208
    object QrMotivoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMotivoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrCartMoviConf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo = 1'
      'AND Ativo = 1 '
      'ORDER BY Nome')
    Left = 555
    Top = 421
    object IntegerField12: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField12: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCartMoviConf: TDataSource
    DataSet = QrCartMoviConf
    Left = 583
    Top = 421
  end
  object QrAvaEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END NOMEENTI '
      'FROM entidades'
      'WHERE Ativo = 1')
    Left = 500
    Top = 112
    object QrAvaEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAvaEmpresaNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsAvaEmpresa: TDataSource
    DataSet = QrAvaEmpresa
    Left = 528
    Top = 112
  end
end
