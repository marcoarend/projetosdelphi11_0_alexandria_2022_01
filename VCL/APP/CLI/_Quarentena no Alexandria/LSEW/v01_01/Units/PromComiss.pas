unit PromComiss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkMemo, dmkRadioGroup, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, Mask, DB, mySQLDbTables, dmkLabel,
  Variants, dmkGeral, dmkValUsu, ComCtrls, Grids, DBGrids, dmkDBGridDAC,
  dmkEditDateTimePicker, dmkDBGrid, UnDmkProcFunc, UnDmkEnums;

type
  TFmPromComiss = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    BtLoc: TBitBtn;
    Label1: TLabel;
    EdPromotor: TdmkEditCB;
    CBPromotor: TdmkDBLookupComboBox;
    TPDataI: TdmkEditDateTimePicker;
    TPDataF: TdmkEditDateTimePicker;
    DsCiclosAula: TDataSource;
    QrCiclosAula: TmySQLQuery;
    CkDataI: TCheckBox;
    CkDataF: TCheckBox;
    BtPesq: TBitBtn;
    QrCiclosAulaCodigo: TIntegerField;
    QrCiclosAulaControle: TIntegerField;
    QrCiclosAulaData: TDateField;
    QrCiclosAulaCidade: TWideStringField;
    QrCiclosAulaCodiCidade: TIntegerField;
    QrCiclosAulaUF: TWideStringField;
    QrCiclosAulaAlunosInsc: TIntegerField;
    QrCiclosAulaAlunosAula: TIntegerField;
    QrCiclosAulaNOMEPRM: TWideStringField;
    QrCiclosAulaMUNI: TWideStringField;
    QrCiclosAulaMUNICI_TXT: TWideStringField;
    PB1: TProgressBar;
    TbPromCom: TmySQLTable;
    DsPromCom: TDataSource;
    TbPromComCodigo: TIntegerField;
    TbPromComControle: TIntegerField;
    TbPromComData: TDateField;
    TbPromComCidade: TWideStringField;
    TbPromComUF: TWideStringField;
    TbPromComPromotor: TWideStringField;
    TbPromComInscritos: TIntegerField;
    TbPromComPresentes: TIntegerField;
    TbPromComComissao: TFloatField;
    TbPromComDPrmAluno: TFloatField;
    DBGrid1: TDBGrid;
    QrCiclosAulaComPromPer: TFloatField;
    QrCiclosAulaDebito: TFloatField;
    BtConfirma: TBitBtn;
    QrCiclosAulaCICCTRL: TIntegerField;
    QrCiclosAulaProfessor: TIntegerField;
    QrCiclosAulaRolComis: TIntegerField;
    TbPromComCICCTRL: TIntegerField;
    TbPromComProfessor: TIntegerField;
    TbPromComRolComis: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCiclosAulaCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure TbPromComNewRecord(DataSet: TDataSet);
    procedure BtLocClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    { Private declarations }
    FComiss: Double;
    PromCom: String;
    procedure MostraEdicao;
    procedure MontaTempTable;
    function CalculaTotalComisTmp: Double;
  public
    { Public declarations }
  end;

  var
  FmPromComiss: TFmPromComiss;

implementation

uses Entidade2, MyDBCheck, UnInternalConsts, UMySQLModule, Module, ModuleCiclos,
ModuleGeral, UCreate, Principal, UnMyObjects, CiclosProm;

{$R *.DFM}

procedure TFmPromComiss.BtConfirmaClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  Comissao: Double;
begin
  Screen.Cursor := crHourGlass;
  if TbPromCom.RecordCount > 0 then
  begin
    PB1.Position := 0;
    PB1.Max      := TbPromCom.RecordCount;
    PB1.Visible  := True;
    //
    TbPromCom.First;
    while not TbPromCom.Eof do
    begin
      Codigo   := TbPromComCodigo.Value;
      Controle := TbPromComControle.Value;
      Comissao := TbPromComComissao.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ciclosaula', False,
      [
        'ComPromPer', 'Codigo'
      ], ['Controle'], [
        Comissao, Codigo
      ], [Controle], False) then
      begin
        DmCiclos.CalculaCiclo(TbPromComCodigo.Value, TbPromComCICCTRL.Value,
          TbPromComProfessor.Value, TbPromComRolComis.Value, '',
          FmCiclosProm.FTabLctALS);
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
      end;
      TbPromCom.Next;
    end;
    PB1.Visible := False;    
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPromComiss.BtLocClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Codigo   := TbPromComCodigo.Value;
  Controle := TbPromComControle.Value;
  //
  if (Codigo <> 0) and (Controle <> 0) then
    FmPrincipal.GerenciaCiclos2(Codigo, Controle);
end;

procedure TFmPromComiss.BtPesqClick(Sender: TObject);
begin
  MontaTempTable;
end;

procedure TFmPromComiss.BtSaidaClick(Sender: TObject);
var
  Comissao: Double;
begin
  Comissao := CalculaTotalComisTmp;
  if FComiss <> Comissao then
  begin
    if Geral.MB_Pergunta('Existem dados que n�o foram salvos!' +
      sLineBreak + 'Deseja sair mesmo assim?') = ID_YES then
    Close;
  end else
    Close;
end;

function TFmPromComiss.CalculaTotalComisTmp: Double;
begin
  Result:= 0;
  if TbPromCom.State <> dsInactive then
  begin
    if TbPromCom.RecordCount > 0 then
    begin
      DModG.QrUpdPID2.Close;
      DModG.QrUpdPID2.SQL.Clear;
      DModG.QrUpdPID2.SQL.Add('SELECT SUM(Comissao) Comissao');
      DModG.QrUpdPID2.SQL.Add('FROM promcom');
      DModG.QrUpdPID2.Open;
      if DModG.QrUpdPID2.RecordCount > 0 then
        Result := DModG.QrUpdPID2.FieldByName('Comissao').Value;
      DModG.QrUpdPID2.Close;
    end;
  end;
end;

procedure TFmPromComiss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  MostraEdicao;
end;

procedure TFmPromComiss.FormCreate(Sender: TObject);
begin
  DmCiclos.QrPromotores.Open;
  PB1.Visible := False;
end;

procedure TFmPromComiss.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPromComiss.MontaTempTable;
  procedure InsereRegistro(Codigo, Controle, Inscritos, Presentes, CICCTRL,
    Professor, RolComis: Integer; Data: TDateTime; Cidade, UF, Prom: String;
    Comissao, DesProm: Double);
  begin
    // Insere na tabela tempor�ria
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + PromCom + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Codigo=:P00, Controle=:P01, Data=:P02, Cidade=:P03, ');
    DModG.QrUpdPID1.SQL.Add('UF=:P04, Promotor=:P05, Inscritos=:P06, Presentes=:P07, ');
    DModG.QrUpdPID1.SQL.Add('Comissao=:P08, DPrmAluno=:P09, CICCTRL=:P10, ');
    DModG.QrUpdPID1.SQL.Add('Professor=:P11, RolComis=:P12');
    DModG.QrUpdPID1.Params[00].AsInteger  := Codigo;
    DModG.QrUpdPID1.Params[01].AsInteger  := Controle;
    DModG.QrUpdPID1.Params[02].AsDateTime := Data;
    DModG.QrUpdPID1.Params[03].AsString   := Cidade;
    DModG.QrUpdPID1.Params[04].AsString   := UF;
    DModG.QrUpdPID1.Params[05].AsString   := Prom;
    DModG.QrUpdPID1.Params[06].AsInteger  := Inscritos;
    DModG.QrUpdPID1.Params[07].AsInteger  := Presentes;
    DModG.QrUpdPID1.Params[08].AsFloat    := Comissao;
    DModG.QrUpdPID1.Params[09].AsFloat    := DesProm;
    DModG.QrUpdPID1.Params[10].AsInteger  := CICCTRL;
    DModG.QrUpdPID1.Params[11].AsInteger  := Professor;
    DModG.QrUpdPID1.Params[12].AsInteger  := RolComis;
    UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  end;
var
  Codigo, Controle, Promotor, Inscritos, Presentes, CICCTRL, Professor,
  RolComis: Integer;
  Data: TDateTime;
  Cidade, UF, Prom: String;
  Comissao, DesProm: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    TbPromCom.Close;
    TbPromCom.Database := DModG.MyPID_DB;
    PromCom  := UCriar.RecriaTempTable('promcom', DModG.QrUpdPID1, False);
    Promotor := EdPromotor.ValueVariant;
    //
    QrCiclosAula.Close;
    QrCiclosAula.SQL.Clear;
    QrCiclosAula.SQL.Add('SELECT aul.Codigo, aul.Controle, aul.Data, aul.Cidade,');
    QrCiclosAula.SQL.Add('aul.CodiCidade, aul.UF, aul.AlunosInsc, aul.AlunosAula,');
    QrCiclosAula.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPRM,');
    QrCiclosAula.SQL.Add('dtb.Nome MUNI, aul.ComPromPer, SUM(lan.Debito) Debito,');
    QrCiclosAula.SQL.Add('cic.Controle CICCTRL, cic.Professor, cic.RolComis');
    QrCiclosAula.SQL.Add('FROM ciclosaula aul');
    QrCiclosAula.SQL.Add('LEFT JOIN ciclos cic ON cic.Codigo = aul.Codigo');
    QrCiclosAula.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici dtb ON dtb.Codigo = aul.CodiCidade');
    QrCiclosAula.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = aul.Promotor');
    QrCiclosAula.SQL.Add('LEFT JOIN ' + FmCiclosProm.FTabLctALS + ' lan ON lan.FatID = 703');
    QrCiclosAula.SQL.Add('                      AND lan.FatID_Sub = aul.Controle');
    QrCiclosAula.SQL.Add('                      AND lan.FatNum =  aul.Codigo');
    QrCiclosAula.SQL.Add('WHERE aul.Codigo > 0');
    if Promotor > 0 then
      QrCiclosAula.SQL.Add('AND aul.Promotor = ' + Geral.FF0(Promotor));
    QrCiclosAula.SQL.Add(dmkPF.SQL_Periodo('AND aul.Data ', TPDataI.Date,
                         TPDataF.Date, CkDataI.Checked, CkDataF.Checked));
    QrCiclosAula.SQL.Add('GROUP BY aul.Controle');
    QrCiclosAula.Open;
    //
    PB1.Visible  := True;
    PB1.Position := 0;
    PB1.Max      := QrCiclosAula.RecordCount;
    //
    QrCiclosAula.First;
    while not QrCiclosAula.Eof do
    begin
      Codigo     := QrCiclosAulaCodigo.Value;
      Controle   := QrCiclosAulaControle.Value;
      Data       := QrCiclosAulaData.Value;
      Prom       := QrCiclosAulaNOMEPRM.Value;
      Cidade     := QrCiclosAulaMUNICI_TXT.Value;
      UF         := QrCiclosAulaUF.Value;
      Inscritos  := QrCiclosAulaAlunosInsc.Value;
      Presentes  := QrCiclosAulaAlunosAula.Value;
      Comissao   := QrCiclosAulaComPromPer.Value;
      DesProm    := QrCiclosAulaDebito.Value;
      CICCTRL    := QrCiclosAulaCICCTRL.Value;
      Professor  := QrCiclosAulaProfessor.Value;
      RolComis   := QrCiclosAulaRolComis.Value;
      InsereRegistro(Codigo, Controle, Inscritos, Presentes, CICCTRL, Professor,
                     RolComis, Data, Cidade, UF, Prom, Comissao, DesProm);
      //
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      QrCiclosAula.Next;
    end;
    TbPromCom.Open;
    PB1.Visible := False;
    //
    FComiss := CalculaTotalComisTmp;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPromComiss.MostraEdicao;
begin
  TPDataI.Date := Date;
  TPDataF.Date := Date;
end;

procedure TFmPromComiss.QrCiclosAulaCalcFields(DataSet: TDataSet);
begin
  if Length(QrCiclosAulaCidade.Value) > 0 then
    QrCiclosAulaMUNICI_TXT.Value := QrCiclosAulaCidade.Value
  else
    QrCiclosAulaMUNICI_TXT.Value := QrCiclosAulaMUNI.Value;
end;

procedure TFmPromComiss.TbPromComNewRecord(DataSet: TDataSet);
begin
  TbPromCom.Cancel;
end;

end.
