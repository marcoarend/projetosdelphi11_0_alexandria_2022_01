object FmPediPrzCab: TFmPediPrzCab
  Left = 368
  Top = 194
  Caption = 'PED-PRAZO-001 :: Prazos de Pagamentos'
  ClientHeight = 456
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 408
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 359
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 92
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label15: TLabel
        Left = 636
        Top = 4
        Width = 66
        Height = 13
        Caption = '% juros / m'#234's:'
        FocusControl = DBEdit7
      end
      object Label16: TLabel
        Left = 712
        Top = 4
        Width = 71
        Height = 13
        Caption = '% desco. m'#225'x.:'
        FocusControl = DBEdit8
      end
      object Label19: TLabel
        Left = 560
        Top = 4
        Width = 40
        Height = 13
        Caption = '% Multa:'
        FocusControl = DBEdit7
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 409
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdJurosMes: TdmkEdit
        Left = 636
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'JurosMes'
        UpdCampo = 'JurosMes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdMaxDesco: TdmkEdit
        Left = 712
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'MaxDesco'
        UpdCampo = 'MaxDesco'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object CGAplicacao: TdmkCheckGroup
        Left = 4
        Top = 44
        Width = 781
        Height = 41
        Caption = ' Aplica'#231#227'o: '
        Columns = 3
        Items.Strings = (
          'Pontos de venda')
        TabOrder = 6
        QryCampo = 'Aplicacao'
        UpdCampo = 'Aplicacao'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object dmkEdit1: TdmkEdit
        Left = 560
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 408
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 156
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 560
        Top = 4
        Width = 40
        Height = 13
        Caption = '% Multa:'
        FocusControl = DBEdit7
      end
      object Label13: TLabel
        Left = 636
        Top = 4
        Width = 66
        Height = 13
        Caption = '% juros / m'#234's:'
        FocusControl = DBEdit7
      end
      object Label20: TLabel
        Left = 712
        Top = 4
        Width = 71
        Height = 13
        Caption = '% desco. m'#225'x.:'
        FocusControl = DBEdit8
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 20
        Width = 409
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsPediPrzCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 88
        Width = 781
        Height = 61
        Caption = ' Parcelamento: '
        TabOrder = 4
        object Label5: TLabel
          Left = 360
          Top = 16
          Width = 97
          Height = 13
          Caption = 'M'#233'dia dias - simples:'
          FocusControl = DBEdit3
        end
        object Label6: TLabel
          Left = 464
          Top = 16
          Width = 80
          Height = 13
          Caption = 'M'#233'dia dias - real:'
          FocusControl = DBEdit4
        end
        object Label10: TLabel
          Left = 568
          Top = 16
          Width = 69
          Height = 13
          Caption = 'M'#233'dia dias - 1:'
          FocusControl = DBEdit5
          Visible = False
        end
        object Label11: TLabel
          Left = 672
          Top = 16
          Width = 69
          Height = 13
          Caption = 'M'#233'dia dias - 2:'
          FocusControl = DBEdit6
          Visible = False
        end
        object Label4: TLabel
          Left = 12
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Parcelas:'
          FocusControl = DBEdit2
        end
        object Label14: TLabel
          Left = 64
          Top = 16
          Width = 77
          Height = 13
          Caption = 'Percentual total:'
          FocusControl = DBEdit9
        end
        object Label17: TLabel
          Left = 156
          Top = 16
          Width = 63
          Height = 13
          Caption = 'Percentual 1:'
          FocusControl = DBEdit10
          Visible = False
        end
        object Label18: TLabel
          Left = 248
          Top = 16
          Width = 63
          Height = 13
          Caption = 'Percentual 2:'
          FocusControl = DBEdit11
          Visible = False
        end
        object DBEdit3: TDBEdit
          Left = 360
          Top = 32
          Width = 100
          Height = 21
          DataField = 'MedDDSimpl'
          DataSource = DsPediPrzCab
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 464
          Top = 32
          Width = 100
          Height = 21
          DataField = 'MedDDReal'
          DataSource = DsPediPrzCab
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 568
          Top = 32
          Width = 100
          Height = 21
          DataField = 'MedDDPerc1'
          DataSource = DsPediPrzCab
          TabOrder = 2
          Visible = False
        end
        object DBEdit6: TDBEdit
          Left = 672
          Top = 32
          Width = 100
          Height = 21
          DataField = 'MedDDPerc2'
          DataSource = DsPediPrzCab
          TabOrder = 3
          Visible = False
        end
        object DBEdit2: TDBEdit
          Left = 12
          Top = 32
          Width = 49
          Height = 21
          DataField = 'Parcelas'
          DataSource = DsPediPrzCab
          TabOrder = 4
        end
        object DBEdit9: TDBEdit
          Left = 64
          Top = 32
          Width = 89
          Height = 21
          DataField = 'PercentT'
          DataSource = DsPediPrzCab
          TabOrder = 5
        end
        object DBEdit10: TDBEdit
          Left = 156
          Top = 32
          Width = 89
          Height = 21
          DataField = 'Percent1'
          DataSource = DsPediPrzCab
          TabOrder = 6
          Visible = False
        end
        object DBEdit11: TDBEdit
          Left = 248
          Top = 32
          Width = 89
          Height = 21
          DataField = 'Percent2'
          DataSource = DsPediPrzCab
          TabOrder = 7
          Visible = False
        end
      end
      object DBEdit7: TDBEdit
        Left = 636
        Top = 20
        Width = 72
        Height = 21
        DataField = 'JurosMes'
        DataSource = DsPediPrzCab
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 712
        Top = 20
        Width = 72
        Height = 21
        DataField = 'MaxDesco'
        DataSource = DsPediPrzCab
        TabOrder = 6
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 4
        Top = 44
        Width = 781
        Height = 41
        Caption = ' Aplica'#231#227'o: '
        Columns = 3
        DataField = 'Aplicacao'
        DataSource = DsPediPrzCab
        Items.Strings = (
          'Pontos de venda')
        ParentBackground = False
        TabOrder = 7
      end
      object DBEdit12: TDBEdit
        Left = 560
        Top = 20
        Width = 72
        Height = 21
        TabOrder = 3
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 359
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
        end
        object BtCondicoes: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Condi'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCondicoesClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCliente: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtClienteClick
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 236
      Width = 790
      Height = 123
      Align = alBottom
      TabOrder = 2
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 336
        Height = 121
        Align = alLeft
        DataSource = DsPediPrzIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Dias'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Percent1'
            Title.Caption = '% parcela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Percent2'
            Title.Caption = '% parcela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PERCENTT'
            Title.Caption = '% total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 337
        Top = 1
        Width = 452
        Height = 121
        Align = alClient
        DataSource = DsPediPrzCli
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Empresa'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_ENT'
            Title.Caption = 'Nome cliente'
            Width = 370
            Visible = True
          end>
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = '                              Prazos de Pagamentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 40
    Top = 12
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPediPrzCabBeforeOpen
    AfterOpen = QrPediPrzCabAfterOpen
    BeforeClose = QrPediPrzCabBeforeClose
    AfterScroll = QrPediPrzCabAfterScroll
    SQL.Strings = (
      'SELECT ppc.Codigo, ppc.CodUsu, ppc.Nome,'
      'ppc.MaxDesco, ppc.JurosMes, ppc.Parcelas,'
      'ppc.MedDDSimpl, ppc.MedDDReal, ppc.MedDDPerc1,'
      'ppc.MedDDPerc2, ppc.PercentT, ppc.Percent1,'
      'ppc.Percent2, ppc.Aplicacao, ppc.MultaPer'
      'FROM pediprzcab ppc')
    Left = 12
    Top = 12
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'pediprzcab.Codigo'
      Required = True
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'pediprzcab.CodUsu'
      Required = True
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'pediprzcab.Nome'
      Required = True
      Size = 50
    end
    object QrPediPrzCabMaxDesco: TFloatField
      FieldName = 'MaxDesco'
      Origin = 'pediprzcab.MaxDesco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabJurosMes: TFloatField
      FieldName = 'JurosMes'
      Origin = 'pediprzcab.JurosMes'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabParcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'pediprzcab.Parcelas'
      Required = True
      DisplayFormat = '0'
    end
    object QrPediPrzCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDPerc1: TFloatField
      FieldName = 'MedDDPerc1'
      Origin = 'pediprzcab.MedDDPerc1'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabMedDDPerc2: TFloatField
      FieldName = 'MedDDPerc2'
      Origin = 'pediprzcab.MedDDPerc2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediPrzCabPercentT: TFloatField
      FieldName = 'PercentT'
      Origin = 'pediprzcab.PercentT'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzCabPercent1: TFloatField
      FieldName = 'Percent1'
      Origin = 'pediprzcab.Percent1'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzCabPercent2: TFloatField
      FieldName = 'Percent2'
      Origin = 'pediprzcab.Percent2'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzCabAplicacao: TSmallintField
      FieldName = 'Aplicacao'
      Origin = 'pediprzcab.Aplicacao'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCondicoes
    CanUpd01 = BtItens
    Left = 68
    Top = 12
  end
  object PMCondicoes: TPopupMenu
    OnPopup = PMCondicoesPopup
    Left = 344
    Top = 376
    object Incluinovacondio1: TMenuItem
      Caption = '&Inclui nova condi'#231#227'o'
      OnClick = Incluinovacondio1Click
    end
    object Alteracondioatual1: TMenuItem
      Caption = '&Altera condi'#231#227'o atual'
      OnClick = Alteracondioatual1Click
    end
    object Excluicondioatual1: TMenuItem
      Caption = '&Exclui condi'#231#227'o atual'
      Enabled = False
    end
  end
  object QrPediPrzIts: TmySQLQuery
    Database = Dmod.MyDB
    DataSource = DsPediPrzCab
    SQL.Strings = (
      'SELECT ppi.Controle, ppi.Dias, '
      'ppi.Percent1, ppi.Percent2,'
      'ppi.Percent1 + ppi.Percent2 PERCENTT'
      'FROM pediprzits ppi'
      'WHERE ppi.Codigo=:P0'
      'ORDER BY Dias')
    Left = 524
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrPediPrzItsPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzItsPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrPediPrzItsPERCENTT: TFloatField
      FieldName = 'PERCENTT'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
  end
  object DsPediPrzIts: TDataSource
    DataSet = QrPediPrzIts
    Left = 552
    Top = 8
  end
  object QrPediPrzCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppe.Controle, ppe.Empresa, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM pediprzcli ppe'
      'LEFT JOIN entidades ent ON ent.Codigo=ppe.Empresa'
      'WHERE ppe.Codigo=:P0'
      'ORDER BY NO_ENT'
      '')
    Left = 584
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCliControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediPrzCliEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediPrzCliNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsPediPrzCli: TDataSource
    DataSet = QrPediPrzCli
    Left = 612
    Top = 8
  end
  object PMCliente: TPopupMenu
    Left = 392
    Top = 232
    object Incluicliente1: TMenuItem
      Caption = '&Inclui cliente'
      OnClick = Incluicliente1Click
    end
    object Retiracilente1: TMenuItem
      Caption = '&Retira cliente'
      OnClick = Retiracilente1Click
    end
  end
end
