unit VendaPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, Variants, dmkGeral;

type
  TFmVendaPesq = class(TForm)
    PainelConfirma: TPanel;
    BtReabre: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    GBPedido: TGroupBox;
    CkDataFimPed: TCheckBox;
    TPDataFimPed: TDateTimePicker;
    TPDataIniPed: TDateTimePicker;
    CkDataIniPed: TCheckBox;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdPedido: TdmkEdit;
    Label2: TLabel;
    EdTel: TdmkEdit;
    Label111: TLabel;
    Label3: TLabel;
    EdDoc: TdmkEdit;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTI: TWideStringField;
    DsCliente: TDataSource;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrPesqSALDO: TFloatField;
    QrPesqPOSIt: TFloatField;
    StaticText2: TStaticText;
    QrPesqNOMEENTI: TWideStringField;
    QrPesqFRETEA: TFloatField;
    QrPesqTotal: TFloatField;
    QrPesqFrete: TFloatField;
    QrPesqDescon: TFloatField;
    QrPesqPago: TFloatField;
    QrPesqNOMESTAT: TWideStringField;
    QrPesqCodigo: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrPesqIntTipo: TLargeintField;
    QrPesqData: TDateField;
    BtLocaliza: TBitBtn;
    QrPesqTipo: TWideStringField;
    QrPesqControle: TIntegerField;
    QrPesqNOMEVENDEDOR: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FControle, FCliente, FDepto: Integer;
  end;

  var
  FmVendaPesq: TFmVendaPesq;

implementation

{$R *.DFM}

uses Module, MyListas, Principal, UnMyObjects;

procedure TFmVendaPesq.BtLocalizaClick(Sender: TObject);
begin
  FmPrincipal.FIntTipo := QrPesqIntTipo.Value;
  FmPrincipal.FCodAlu  := QrPesqCodigo.Value;
  FmPrincipal.FCodAlu2 := QrPesqControle.Value;
  //
  if FDepto = QrPesqIntTipo.Value then 
    FCodigo := QrPesqCodigo.Value;
  Close;
end;

procedure TFmVendaPesq.BtReabreClick(Sender: TObject);
var
  Cliente, Pedido: Integer;
  Doc, Tel: String;
begin
  Cliente := EdCliente.ValueVariant;
  Pedido  := EdPedido.ValueVariant;
  Doc     := Geral.SoNumero_TT(EdDoc.ValueVariant);
  Tel     := MLAGeral.SoNumeroESinal_TT(EdTel.ValueVariant); 
  //
  if Length(Tel) > 0 then
  begin
    if (Length(Tel) - 1 = 8) or (Length(Tel) - 1 = 10) then
    begin
      case Length(Tel) - 1 of
         8: Tel := Copy(Tel, 2, 8);
        10: Tel := Tel;
      end;
    end else
    begin
      Tel := '';
      Application.MessageBox('O n�mero de telefone deve ter 8, 10 ou 11 d�gitos!',
      'Aviso', MB_OK+ MB_ICONWARNING);
      EdTel.SetFocus;
      Exit;
    end;
  end;
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT moc.Codigo, moc.Codigo Controle, moc.Cliente, 1 IntTipo, moc.DataPedi Data,');
  QrPesq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrPesq.SQL.Add('ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA,');
  QrPesq.SQL.Add('moc.Total, moc.Frete, moc.Descon, moc.Pago,');
  QrPesq.SQL.Add('sta.Nome NOMESTAT, grd.Nome Tipo, ');
  QrPesq.SQL.Add('CASE moc.UserCad');
  QrPesq.SQL.Add('WHEN -2 THEN "BOSS [Administrador]"');
  QrPesq.SQL.Add('WHEN -1 THEN "MASTER [Admin. DERMATEK]"');
  QrPesq.SQL.Add('WHEN  0 THEN "N�o definido"');
  QrPesq.SQL.Add('ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome END) END NOMEVENDEDOR ');
  QrPesq.SQL.Add('FROM moviv moc');
  QrPesq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente');
  QrPesq.SQL.Add('LEFT JOIN statusv sta ON sta.Codigo = moc.StatAtual');
  QrPesq.SQL.Add('LEFT JOIN graded grd ON grd.Codigo = moc.GradeD');
  QrPesq.SQL.Add('LEFT JOIN senhas sen ON sen.Numero = moc.UserCad');
  QrPesq.SQL.Add('LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario');
  QrPesq.SQL.Add('WHERE moc.Codigo > 0');
  if Cliente > 0 then
    QrPesq.SQL.Add('AND Cliente =' + IntToStr(Cliente));
  if Pedido > 0 then
    QrPesq.SQL.Add('AND moc.Codigo =' + IntToStr(Pedido));
  if Length(Tel) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.ETe1, ent.PTe1) LIKE "%' + Tel + '%")');
  if Length(Doc) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.CNPJ, ent.CPF) = ' + Doc + ')');
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND moc.DataPedi BETWEEN "' + Geral.FDT(TPDataIniPed.Date, 1) + '" AND "' + Geral.FDT(TPDataFimPed.Date, 1) +'"');
  if (CkDataIniPed.Checked = False) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND moc.DataPedi <= ' + Geral.FDT(TPDataFimPed.Date, 1));
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = False) then
    QrPesq.SQL.Add('AND moc.DataPedi >= ' + Geral.FDT(TPDataIniPed.Date, 1));

  QrPesq.SQL.Add('UNION');

  QrPesq.SQL.Add('SELECT moc.Codigo, moc.Codigo Controle, moc.Enti, 2 IntTipo, moc.DataCad Data,');
  QrPesq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrPesq.SQL.Add('ELSE ent.Nome END NOMEENTI, 0/1 FRETEA,');
  QrPesq.SQL.Add('0.0 Total, 0.0 Frete, 0.0 Descon, 0.0 Pago,');
  QrPesq.SQL.Add('IF(moc.Status = 0, "APROVADO", "REPROVADO") NOMESTAT, grd.Nome Tipo, ');
  QrPesq.SQL.Add('CASE moc.UserCad');
  QrPesq.SQL.Add('WHEN -2 THEN "BOSS [Administrador]"');
  QrPesq.SQL.Add('WHEN -1 THEN "MASTER [Admin. DERMATEK]"');
  QrPesq.SQL.Add('WHEN  0 THEN "N�o definido"');
  QrPesq.SQL.Add('ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome END) END NOMEVENDEDOR ');
  QrPesq.SQL.Add('FROM cartaval moc');
  QrPesq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = moc.Enti');
  QrPesq.SQL.Add('LEFT JOIN graded grd ON grd.Codigo = moc.GradeD');
  QrPesq.SQL.Add('LEFT JOIN senhas sen ON sen.Numero = moc.UserCad');
  QrPesq.SQL.Add('LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario');
  QrPesq.SQL.Add('WHERE moc.Codigo > 0');
  if Cliente > 0 then
    QrPesq.SQL.Add('AND Enti =' + IntToStr(Cliente));
  if Pedido > 0 then
    QrPesq.SQL.Add('AND moc.Codigo =' + IntToStr(Pedido));
  if Length(Tel) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.ETe1, ent.PTe1) LIKE "%' + Tel + '%")');
  if Length(Doc) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.CNPJ, ent.CPF) = ' + Doc + ')');
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND moc.DataCad BETWEEN "' + Geral.FDT(TPDataIniPed.Date, 1) + '" AND "' + Geral.FDT(TPDataFimPed.Date, 1) +'"');
  if (CkDataIniPed.Checked = False) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND moc.DataCad <= ' + Geral.FDT(TPDataFimPed.Date, 1));
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = False) then
    QrPesq.SQL.Add('AND moc.DataCad >= ' + Geral.FDT(TPDataIniPed.Date, 1));

  QrPesq.SQL.Add('UNION');

  QrPesq.SQL.Add('SELECT moc.Codigo, moc.Codigo Controle, moc.Cliente, 3 IntTipo, moc.DataPedi Data,');
  QrPesq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrPesq.SQL.Add('ELSE ent.Nome END NOMEENTI, 0/1 FRETEA,');
  QrPesq.SQL.Add('moc.Total, moc.Frete, moc.Descon, moc.Pago,');
  QrPesq.SQL.Add('"" NOMESTAT, grd.Nome Tipo, ');
  QrPesq.SQL.Add('CASE moc.UserCad');
  QrPesq.SQL.Add('WHEN -2 THEN "BOSS [Administrador]"');
  QrPesq.SQL.Add('WHEN -1 THEN "MASTER [Admin. DERMATEK]"');
  QrPesq.SQL.Add('WHEN  0 THEN "N�o definido"');
  QrPesq.SQL.Add('ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome END) END NOMEVENDEDOR ');
  QrPesq.SQL.Add('FROM movil moc');
  QrPesq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = moc.Cliente');
  QrPesq.SQL.Add('LEFT JOIN graded grd ON grd.Codigo = moc.GradeD');
  QrPesq.SQL.Add('LEFT JOIN senhas sen ON sen.Numero = moc.UserCad');
  QrPesq.SQL.Add('LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario');
  QrPesq.SQL.Add('WHERE moc.Codigo > 0');  
  if Cliente > 0 then
    QrPesq.SQL.Add('AND Cliente =' + IntToStr(Cliente));
  if Pedido > 0 then
    QrPesq.SQL.Add('AND moc.Codigo =' + IntToStr(Pedido));
  if Length(Tel) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.ETe1, ent.PTe1) LIKE "%' + Tel + '%")');
  if Length(Doc) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.CNPJ, ent.CPF) = ' + Doc + ')');
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND moc.DataPedi BETWEEN "' + Geral.FDT(TPDataIniPed.Date, 1) + '" AND "' + Geral.FDT(TPDataFimPed.Date, 1) +'"');
  if (CkDataIniPed.Checked = False) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND moc.DataPedi <= ' + Geral.FDT(TPDataFimPed.Date, 1));
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = False) then
    QrPesq.SQL.Add('AND moc.DataPedi >= ' + Geral.FDT(TPDataIniPed.Date, 1));

  QrPesq.SQL.Add('UNION');

  QrPesq.SQL.Add('SELECT alu.Codigo, alu.Conta Controle, alu.Entidade Cliente, 4 IntTipo, aul.Data,');
  QrPesq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrPesq.SQL.Add('ELSE ent.Nome END NOMEENTI, 0 FRETEA, 0 Total, 0 Frete,');
  QrPesq.SQL.Add('0 Descon, 0 Pago, "" NOMESTAT, grd.Nome Tipo, ');
  QrPesq.SQL.Add('CASE alu.UserCad');
  QrPesq.SQL.Add('WHEN -2 THEN "BOSS [Administrador]"');
  QrPesq.SQL.Add('WHEN -1 THEN "MASTER [Admin. DERMATEK]"');
  QrPesq.SQL.Add('WHEN  0 THEN "N�o definido"');
  QrPesq.SQL.Add('ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome END) END NOMEVENDEDOR ');
  QrPesq.SQL.Add('FROM ciclosalu alu');
  QrPesq.SQL.Add('LEFT JOIN ciclosaula aul ON aul.Controle = alu.Controle');
  QrPesq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = alu.Entidade');
  QrPesq.SQL.Add('LEFT JOIN graded grd ON grd.Codigo = alu.GradeD');
  QrPesq.SQL.Add('LEFT JOIN senhas sen ON sen.Numero = alu.UserCad');
  QrPesq.SQL.Add('LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario');
  QrPesq.SQL.Add('WHERE alu.Entidade > 0');
  if Cliente > 0 then
    QrPesq.SQL.Add('AND alu.Entidade =' + IntToStr(Cliente));
  if Pedido > 0 then
    QrPesq.SQL.Add('AND alu.Codigo =' + IntToStr(Pedido));
  if Length(Tel) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.ETe1, ent.PTe1) LIKE "%' + Tel + '%")');
  if Length(Doc) > 0 then
    QrPesq.SQL.Add('AND (IF(ent.Tipo=0, ent.CNPJ, ent.CPF) = ' + Doc + ')');
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND aul.Data BETWEEN "' + Geral.FDT(TPDataIniPed.Date, 1) + '" AND "' + Geral.FDT(TPDataFimPed.Date, 1) +'"');
  if (CkDataIniPed.Checked = False) and (CkDataFimPed.Checked = True) then
    QrPesq.SQL.Add('AND aul.Data <= ' + Geral.FDT(TPDataFimPed.Date, 1));
  if (CkDataIniPed.Checked = True) and (CkDataFimPed.Checked = False) then
    QrPesq.SQL.Add('AND aul.Data >= ' + Geral.FDT(TPDataIniPed.Date, 1));
  //
  QrPesq.Open;
end;

procedure TFmVendaPesq.BtSaidaClick(Sender: TObject);
begin
  FmPrincipal.FIntTipo := FDepto;
  FmPrincipal.FCodAlu  := FmPrincipal.FCodDepto;
  Close;
end;

procedure TFmVendaPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmVendaPesq.FormCreate(Sender: TObject);
begin
  QrCliente.Open;
  //
  CkDataIniPed.Checked := False;
  CkDataFimPed.Checked := False;
  TPDataIniPed.Date    := Date;
  TPDataFimPed.Date    := Date;
  //
  EdPedido.ValueVariant  := 0;
  EdTel.ValueVariant     := '';
  EdDoc.ValueVariant     := '';
end;

procedure TFmVendaPesq.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmVendaPesq.QrPesqCalcFields(DataSet: TDataSet);
begin
  QrPesqPOSIt.Value := QrPesqTotal.Value + QrPesqFrete.Value - QrPesqDescon.Value;
  QrPesqSALDO.Value := QrPesqPOSIt.Value - QrPesqPago.Value;
end;

end.



