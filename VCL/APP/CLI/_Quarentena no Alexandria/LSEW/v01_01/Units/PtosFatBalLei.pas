unit PtosFatBalLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkGeral, dmkEdit, Mask, DBCtrls, UnDmkEnums;

type
  TFmPtosFatBalLei = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    TbEstqPto: TmySQLTable;
    DsEstqPto: TDataSource;
    DBGrid1: TDBGrid;
    TbEstqPtoGrade: TIntegerField;
    TbEstqPtoGraGruX: TIntegerField;
    TbEstqPtoCor: TIntegerField;
    TbEstqPtoTam: TIntegerField;
    TbEstqPtoNo_Grade: TWideStringField;
    TbEstqPtoNo_Cor: TWideStringField;
    TbEstqPtoNo_Tam: TWideStringField;
    TbEstqPtoQtdeOld: TIntegerField;
    TbEstqPtoQtdeNew: TIntegerField;
    QrItens: TmySQLQuery;
    QrItensIDCtrl: TIntegerField;
    QrItensCodInn: TIntegerField;
    QrItensCodOut: TIntegerField;
    QrItensTipOut: TSmallintField;
    QrItensDataIns: TDateTimeField;
    QrItensEmpresa: TIntegerField;
    QrItensProduto: TIntegerField;
    QrItensQuanti: TFloatField;
    QrItensPrecoO: TFloatField;
    QrItensPrecoR: TFloatField;
    QrItensDescoP: TFloatField;
    QrItensStatus: TSmallintField;
    QrItensAlterWeb: TSmallintField;
    QrItensAtivo: TSmallintField;
    MeAvisos: TMemo;
    QrSum: TmySQLQuery;
    QrSumQuanti: TFloatField;
    QrSumValor: TFloatField;
    QrPediPrzIts: TmySQLQuery;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent1: TFloatField;
    TbEstqPtoCodUsu: TIntegerField;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeituraCodigoDeBarras: TEdit;
    EdQtdLei: TdmkEdit;
    BitBtn1: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    CkFixo: TCheckBox;
    DsItens: TDataSource;
    QrItem: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    SmallintField1: TSmallintField;
    DateTimeField1: TDateTimeField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    SmallintField2: TSmallintField;
    SmallintField3: TSmallintField;
    SmallintField4: TSmallintField;
    QrItensNO_GRADE: TWideStringField;
    QrItensNO_Cor: TWideStringField;
    QrItensNO_Tam: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraCodigoDeBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLeituraCodigoDeBarrasChange(Sender: TObject);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkFixoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FTamAll: Boolean;
    FGraGruX, FSequencia: Integer;
    procedure InserirParcela(const TabLctA: String; const ValorTot: Double;
              var ValorPar, ValorPto: Double);
    procedure InsereItem();
    function  ReopenItens(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmPtosFatBalLei: TFmPtosFatBalLei;

implementation

{$R *.DFM}

uses Module, ModuleGeral, PtosFatCad, UMySQLModule, UnFinanceiro,
UnInternalConsts, UnBco_Rem, UnMyObjects;

const
  FTamMax = 13; // EAN13
  FTamPrd = 12; // tudo � codigo do produto menos o d�gito (n�o tem lote e etc.)
  FTamMin = 1; // Digito verificador

procedure TFmPtosFatBalLei.BitBtn1Click(Sender: TObject);
begin
  InsereItem();
end;

procedure TFmPtosFatBalLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPtosFatBalLei.CkFixoClick(Sender: TObject);
begin
  EdQtdLei.Enabled := not CkFixo.Checked;
end;

procedure TFmPtosFatBalLei.EdLeituraCodigoDeBarrasChange(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(EdLeituraCodigoDeBarras.Text);
  if Tam = FTamMax then
  begin
    FGraGruX   := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 07, 6));
    FSequencia := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 13, 8));
    if ReopenItens() then
    begin
      if CkFixo.Checked then
      begin
        FTamAll := True;
        InsereItem();
        EdLeituraCodigoDeBarras.SetFocus;
        EdLeituraCodigoDeBarras.Text := '';
      end else EdQtdLei.SetFocus;
    end;
  end else if Tam = 0 then QrItens.Close;
end;

procedure TFmPtosFatBalLei.EdLeituraCodigoDeBarrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Txt: String;
  Tam: Integer;
begin
  if Key = VK_RETURN then
  begin
    if (Length(EdLeituraCodigoDeBarras.Text) <= FTamPrd)
    and (Length(EdLeituraCodigoDeBarras.Text) >  FTamMin) then
    begin
      Txt := EdLeituraCodigoDeBarras.Text;
      Tam := Length(Txt);
      Txt := Copy(Txt, 1, Tam-1);
      FGraGruX := Geral.IMV(Txt);
      if ReopenItens() then
      begin
        if CkFixo.Checked then
        begin
          FTamAll := True;
          InsereItem();
          EdLeituraCodigoDeBarras.SetFocus;
          EdLeituraCodigoDeBarras.Text := '';
        end else EdQtdLei.SetFocus;
      end;
    end;
  end;  
end;

procedure TFmPtosFatBalLei.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeituraCodigoDeBarras.Text = '') and FTamAll then
  begin
    FTamAll := False;
  end;
end;

procedure TFmPtosFatBalLei.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereItem();
end;

procedure TFmPtosFatBalLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosFatBalLei.FormCreate(Sender: TObject);
begin
  // erro na vers�o 2.7.2 do DAC
  TbEstqPto.DataBase := DModG.MyPID_DB;
  TbEstqPto.Open;
end;

procedure TFmPtosFatBalLei.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPtosFatBalLei.FormShow(Sender: TObject);
begin
  EdLeituraCodigoDeBarras.SetFocus;
end;

procedure TFmPtosFatBalLei.InsereItem;
  procedure AtualizaTmpTable(GraGruX: Integer);
  begin
    if (TbEstqPtoQtdeNew.Value - EdQtdLei.ValueVariant) < TbEstqPtoQtdeOld.Value then
    begin
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('UPDATE estqpto SET QtdeNew=:P0 WHERE GraGrux=:P1');
      DModG.QrUpdPID1.Params[0].AsInteger := TbEstqPtoQtdeNew.Value - EdQtdLei.ValueVariant;
      DModG.QrUpdPID1.Params[1].AsInteger := GraGruX;
      DModG.QrUpdPID1.ExecSQL;
    end;
  end;
var
  Codigo, GragruX: Integer;
  QuantTot, ValorTot, ValorPar,
  ValorPto, ValorCon, ValorGer,
  ComisPto, ComisCon, ComisGer: Double;
  Encerrou: String;
begin
  GragruX    := FGraGruX;
  FGraGruX   := 0;
  FSequencia := 0;
  //
  TbEstqPto.Locate('GraGrux', GragruX, []);
  //
  if (TbEstqPtoQtdeNew.Value - EdQtdLei.ValueVariant) > TbEstqPtoQtdeOld.Value then
  begin
    Geral.MensagemBox('A quantida nova n�o pode ser superior a quantidade anterior!',
      'Aviso', MB_OK+MB_ICONWARNING);
  end else
  if (TbEstqPtoQtdeNew.Value - EdQtdLei.ValueVariant) < 0 then
  begin
    Geral.MensagemBox('A quantidade nova n�o pode ser negativa!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end else
  begin
    Screen.Cursor := crHourGlass;
    Encerrou      := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    MeAvisos.Lines.Clear;
    //
    if GraGruX <> 0 then
    begin
      if (TbEstqPtoQtdeNew.Value - EdQtdLei.ValueVariant) < TbEstqPtoQtdeOld.Value then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosstqmov', False, [
          'CodOut', 'TipOut', 'Status'], ['IDCtrl'], [
          FmPtosFatCad.QrPtosFatCadCodigo.Value, 1, 60], [
          QrItensIDCtrl.Value], False);
        //
        AtualizaTmpTable(GragruX);
      end;
      //
      QrSum.Close;
      QrSum.Params[0].AsInteger := FmPtosFatCad.QrPtosFatCadCodigo.Value;
      QrSum.Open;
      //
      ComisPto := FmPtosFatCad.QrPtosFatCadComisPto.Value;
      ComisCon := FmPtosFatCad.QrPtosFatCadComisCon.Value;
      ComisGer := FmPtosFatCad.QrPtosFatCadComisGer.Value;
      //
      QuantTot := QrSumQuanti.Value;
      ValorTot := QrSumValor.Value;
      ValorPto := (Round(ValorTot * ComisPto)) / 100;
      ValorCon := (Round(ValorTot * ComisCon)) / 100;
      ValorGer := (Round(ValorTot * ComisGer)) / 100;
      Codigo   := FmPtosFatCad.QrPtosFatCadCodigo.Value;
      //
      QrPediPrzIts.Close;
      QrPediPrzIts.Params[0].AsInteger := FmPtosFatCad.QrPtosFatCadPediPrzCab.Value;
      QrPediPrzIts.Open;
      //
      ValorPar := 0;
      QrPediPrzIts.First;
      while not QrPediPrzIts.Eof do
      begin
        InserirParcela(FmPtosFatCad.FTabLctALS, ValorTot, ValorPar, ValorPto);
        //
        QrPediPrzIts.Next;
      end;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosfatcad', False, [
      'Encerrou', 'QuantTot', 'ValorTot',
      'ValorPto', 'ValorCon', 'ValorGer'], ['Codigo'], [
      Encerrou, QuantTot, ValorTot,
      ValorPto, ValorCon, ValorGer], [Codigo], True);

      if MeAvisos.Text <> '' then
      begin
        MeAvisos.Visible := True;
        Application.MessageBox('H� avisos de inconsist�ncias!', 'Aviso',
          MB_OK+MB_ICONWARNING);
      end;
    end;
    Screen.Cursor := crDefault;
    //
    EdLeituraCodigoDeBarras.Text := '';
    EdLeituraCodigoDeBarras.SetFocus;
    //
    TbEstqPto.Close;
    TbEstqPto.Open;
    if TbEstqPto.RecordCount > 0 then
      TbEstqPto.Locate('GraGrux', GragruX, []);
  end;
end;

procedure TFmPtosFatBalLei.InserirParcela(const TabLctA: String;
  const ValorTot: Double; var ValorPar, ValorPto: Double);
var
  Agora: TDateTime;
  Credito: Double;
begin
  Agora := DModG.ObtemAgora();
  if QrPediPrzIts.RecNo = QrPediPrzIts.RecordCount then
    Credito := ValorTot - ValorPar
  else
    Credito := (Round(ValorTot * QrPediPrzItsPercent1.Value)) / 100;
  ValorPar := ValorPar + Credito;
  //
  FLAN_Documento     := UBco_Rem.ObtemProximoNossoNumero(
    FmPtosFatCad.QrPtosFatCadCNAB_Cfg.Value, True);
  //
  FLAN_Data          := Geral.FDT(Agora, 1);
  FLAN_Vencimento    := Geral.FDT(Agora + QrPediPrzItsDias.Value, 1);
  FLAN_DataCad       := Geral.FDT(Agora, 1);
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_Descricao     := Dmod.QrControleTxtVdaPto.Value;
  FLAN_Tipo          := FmPtosFatCad.QrPtosFatCadTIPOCART.Value;
  FLAN_Carteira      := FmPtosFatCad.QrPtosFatCadCarteira.Value;
  FLAN_Credito       := Credito;
  FLAN_Debito        := ValorPto;
  FLAN_Genero        := Dmod.QrControleCtaPtoVda.Value;
  FLAN_Cliente       := FmPtosFatCad.QrPtosFatCadPontoVda.Value;
  FLAN_Vendedor      := FmPtosFatCad.QrPtosFatCadConsultor.Value;
  FLAN_Account       := FmPtosFatCad.QrPtosFatCadGerente.Value;
  FLAN_CliInt        := FmPtosFatCad.QrPtosFatCadCliInt.Value; // Carteiras.ForneceI = Cliente interno
  FLAN_MoraDia       := FmPtosFatCad.QrPtosFatCadJurosMes.Value;
  FLAN_Multa         := 0;//FmPtosFatCad.QrPtosFatCadMultaPer.Value;
  FLAN_FatID         := 801;
  FLAN_FatID_Sub     := 0;
  FLAN_FatNum        := FmPtosFatCad.QrPtosFatCadCodigo.Value;
  FLAN_FatParcela    := QrPediPrzIts.RecNo;
  FLAN_Duplicata     := FormatFloat('000000', FmPtosFatCad.QrPtosFatCadCodUsu.Value) + '/' + FormatFloat('000', FLAN_FatParcela);
  //
  {
  FLAN_Depto         := 0;
  FLAN_Debito        := 0;
  FLAN_Mez           := '';
  FLAN_Doc2          := '';
  FLAN_SerieCH       := '';
  FLAN_SerieNF       := '';
  FLAN_NotaFiscal    := 0;
  FLAN_Cartao        := 0;
  FLAN_Linha         := 0;
  FLAN_Sub           := 0;
  FLAN_ID_Pgto       := 0;
  FLAN_Fornecedor    := 0;
  FLAN_ICMS_P        := 0;
  FLAN_ICMS_V        := 0;
  FLAN_DescoPor      := 0;
  FLAN_DescoVal      := 0;
  FLAN_ForneceI      := 0;
  FLAN_NFVal         := 0;
  FLAN_Unidade       := 0;
  FLAN_Qtde          := 0;
  FLAN_Emitente      := '';
  FLAN_CNPJCPF       := '';
  FLAN_Banco         := 0;
  FLAN_Agencia       := '';
  FLAN_ContaCorrente := '';
  FLAN_MultaVal      := 0;
  FLAN_MoraVal       := 0;
  FLAN_CtrlIni       := 0;
  FLAN_Nivel         := 0;
  FLAN_TipoCH        := 0;
  FLAN_Atrelado      := 0;
  FLAN_SubPgto1      := 0;
  FLAN_MultiPgto     := 0;
  FLAN_CNAB_Sit      := 0;
  }
  if FLAN_Tipo < 2 then
  begin
    FLAN_Sit         := 3;
    FLAN_Compensado  := FLAN_Vencimento;
  end;
  //
  //  parei aqui incluir lancto
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                     TabLctA, LAN_CTOS, 'Controle');
  UFinanceiro.InsereLancamento(FmPtosFatCad.FTabLctALS);
end;

function TFmPtosFatBalLei.ReopenItens(): Boolean;
begin
  Result := False;
  QrItens.Close;
  if FGraGruX > 0 then
  begin
    QrItens.Params[00].AsInteger := FmPtosFatCad.QrPtosFatCadPontoVda.Value;
    QrItens.Params[01].AsInteger := FGraGruX;
    QrItens.Open;
    //
    if QrItens.RecordCount > 0 then
    begin
      Result := True;
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

end.
