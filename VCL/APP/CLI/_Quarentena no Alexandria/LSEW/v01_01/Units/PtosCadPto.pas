unit PtosCadPto;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, dmkCheckBox, UnDmkProcFunc, UnDmkEnums;

type
  TFmPtosCadPto = class(TForm)
    PainelDados: TPanel;
    DsPtosCadPto: TDataSource;
    QrPtosCadPto: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    DBEdNome: TDBEdit;
    EdNome: TdmkEdit;
    Label9: TLabel;
    QrPtosCadPtoCodigo: TIntegerField;
    QrPtosCadPtoConsultor: TIntegerField;
    QrPtosCadPtoGerente: TIntegerField;
    QrPtosCadPtoComisPonto: TFloatField;
    QrPtosCadPtoComisConsu: TFloatField;
    QrPtosCadPtoComisGeren: TFloatField;
    QrPtosCadPtoDdEntrega: TIntegerField;
    QrPtosCadPtoLimiCred: TFloatField;
    QrPtosCadPtoCodUsu: TIntegerField;
    QrPtosCadPtoNO_EntiPonto: TWideStringField;
    QrPtosCadPtoNO_Consultor: TWideStringField;
    QrPtosCadPtoNO_Gerente: TWideStringField;
    QrPtosCadPtoLk: TIntegerField;
    QrPtosCadPtoDataCad: TDateField;
    QrPtosCadPtoDataAlt: TDateField;
    QrPtosCadPtoUserCad: TIntegerField;
    QrPtosCadPtoUserAlt: TIntegerField;
    QrPtosCadPtoAlterWeb: TSmallintField;
    QrPtosCadPtoAtivo: TSmallintField;
    QrPtosCadPtoNome: TWideStringField;
    LaCliente: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    EdComisPonto: TdmkEdit;
    Label4: TLabel;
    EdConsultor: TdmkEditCB;
    Label5: TLabel;
    CBConsultor: TdmkDBLookupComboBox;
    EdComisConsu: TdmkEdit;
    Label6: TLabel;
    SpeedButton6: TSpeedButton;
    EdGerente: TdmkEditCB;
    Label10: TLabel;
    CBGerente: TdmkDBLookupComboBox;
    EdComisGeren: TdmkEdit;
    Label11: TLabel;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    QrCarteiras: TmySQLQuery;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    dmkValUsu1: TdmkValUsu;
    dmkValUsu2: TdmkValUsu;
    Label7: TLabel;
    EdLimiCred: TdmkEdit;
    CkAtivo: TdmkCheckBox;
    dmkEdit2: TdmkEdit;
    Label12: TLabel;
    QrConsultores: TmySQLQuery;
    DsConsultores: TDataSource;
    QrGerentes: TmySQLQuery;
    DsGerentes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesCodUsu: TIntegerField;
    QrClientesNO_ENT: TWideStringField;
    QrConsultoresCodigo: TIntegerField;
    QrConsultoresCodUsu: TIntegerField;
    QrConsultoresNO_ENT: TWideStringField;
    QrGerentesCodigo: TIntegerField;
    QrGerentesCodUsu: TIntegerField;
    QrGerentesNO_ENT: TWideStringField;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    QrPtosCadPtoCU_PONTO: TIntegerField;
    QrPtosCadPtoCU_CONSULTOR: TIntegerField;
    QrPtosCadPtoCU_GERENTE: TIntegerField;
    Label1: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    DsCarteiras: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    DsGraCusPrc: TDataSource;
    QrPtosCadPtoGraCusPrc: TIntegerField;
    QrPtosCadPtoCarteira: TIntegerField;
    QrPtosCadPtoNO_Carteira: TWideStringField;
    QrPtosCadPtoNO_GraCusPrc: TWideStringField;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label24: TLabel;
    EdPediPrzCab: TdmkEditCB;
    CBPediPrzCab: TdmkDBLookupComboBox;
    QrPtosCadPtoPediPrzCab: TIntegerField;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    DsPediPrzCab: TDataSource;
    dmkValUsu3: TdmkValUsu;
    dmkValUsu4: TdmkValUsu;
    QrPtosCadPtoNO_PediPrzCab: TWideStringField;
    Label25: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrPtosCadPtoCU_PediPrzCab: TIntegerField;
    Label26: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    QrCNAB_Cfg: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    Label27: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    QrPtosCadPtoCNAB_Cfg: TIntegerField;
    QrPtosCadPtoNO_CNAB_Cfg: TWideStringField;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPtosCadPtoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPtosCadPtoBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenEntidades();
    procedure ReopenPediPrzCab();
  public
    { Public declarations }
  end;

var
  FmPtosCadPto: TFmPtosCadPto;
const
  FFormatFloat = '00000';

implementation

uses Module, Principal, GraCusPrc, Carteiras, MyDBCheck, PediPrzCab, CNAB_Cfg,
PediPrzCab1, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPtosCadPto.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPtosCadPto.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPtosCadPtoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPtosCadPto.DefParams;
begin
  VAR_GOTOTABELA := 'ptoscadpto';
  VAR_GOTOMYSQLTABLE := QrPtosCadPto;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT pto.CodUsu CU_PONTO, cst.CodUsu');
  VAR_SQLx.Add('CU_CONSULTOR, ger.CodUsu CU_GERENTE,');
  VAR_SQLx.Add('IF(pto.Tipo=0,pto.RazaoSocial,pto.Nome) NO_EntiPonto,');
  VAR_SQLx.Add('IF(cst.Tipo=0,cst.RazaoSocial,cst.Nome) NO_Consultor,');
  VAR_SQLx.Add('IF(ger.Tipo=0,ger.RazaoSocial,ger.Nome) NO_Gerente,');
  VAR_SQLx.Add('car.Nome NO_Carteira, gcp.Nome NO_GraCusPrc,');
  VAR_SQLx.Add('ppc.Nome NO_PediPrzCab, ppc.CodUsu CU_PediPrzCab, ');
  VAR_SQLx.Add('cfg.Nome NO_CNAB_Cfg, pcp.*');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM ptoscadpto pcp');
  VAR_SQLx.Add('LEFT JOIN entidades pto ON pto.Codigo=pcp.Codigo');
  VAR_SQLx.Add('LEFT JOIN entidades cst ON cst.Codigo=pcp.Consultor');
  VAR_SQLx.Add('LEFT JOIN entidades ger ON ger.Codigo=pcp.Gerente');
  VAR_SQLx.Add('LEFT JOIN gracusprc gcp ON gcp.Codigo=pcp.GraCusPrc');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=pcp.Carteira');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pcp.PediPrzCab');
  VAR_SQLx.Add('LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pcp.CNAB_Cfg');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE pcp.Codigo > 0');
  //
  VAR_SQL1.Add('AND pcp.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pcp.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND pcp.Nome Like :P0');
  //
end;

procedure TFmPtosCadPto.EdCodigoChange(Sender: TObject);
begin
  ReopenPediPrzCab();
end;

procedure TFmPtosCadPto.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'ptoscadpto', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmPtosCadPto.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPtosCadPto.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmPtosCadPto.AlteraRegistro;
var
  PtosCadPto : Integer;
begin
  PtosCadPto := QrPtosCadPtoCodigo.Value;
  if QrPtosCadPtoCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PtosCadPto, Dmod.MyDB, 'PtosCadPto', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PtosCadPto, Dmod.MyDB, 'PtosCadPto', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPtosCadPto.IncluiRegistro;
var
  Cursor : TCursor;
  PtosCadPto : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PtosCadPto := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PtosCadPto', 'PtosCadPto', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PtosCadPto))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, PtosCadPto);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmPtosCadPto.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPtosCadPto.ReopenEntidades;
begin
  QrClientes.Close;
  QrClientes.Open;
  //
  QrConsultores.Close;
  QrConsultores.Open;
  //
  QrGerentes.Close;
  QrGerentes.Open;
  //
end;

procedure TFmPtosCadPto.ReopenPediPrzCab;
var
  CodUsu, Empresa: Integer;
begin
  Empresa := Geral.IMV(EdCodigo.Text);
  QrPediPrzCab.Close;
  QrPediPrzCab.Params[0].AsInteger := Empresa;
  QrPediPrzCab.Open;
  //
  CodUsu := Geral.IMV(EdPediPrzCab.Text);
  if not QrPediPrzCab.Locate('CodUsu', CodUsu, []) then
  begin
    EdPediPrzCab.ValueVariant := 0;
    CBPediPrzCab.KeyValue     := 0;
  end;
end;

procedure TFmPtosCadPto.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPtosCadPto.SpeedButton10Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPediPrzCab, FmPediPrzCab, afmoNegarComAviso) then
  begin
    FmPediPrzCab.ShowModal;
    FmPediPrzCab.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrPediPrzCab.Close;
    QrPediPrzCab.Open;
    EdPediPrzCab.ValueVariant := VAR_CADASTRO;
    CBPediPrzCab.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPtosCadPto.SpeedButton11Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrCNAB_Cfg.Close;
    QrCNAB_Cfg.Open;
    EdCNAB_Cfg.ValueVariant := VAR_CADASTRO;
    CBCNAB_Cfg.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPtosCadPto.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPtosCadPto.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPtosCadPto.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPtosCadPto.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPtosCadPto.SpeedButton5Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  FmPrincipal.CadastroDeEntidades(EdCodigo.ValueVariant);
  if VAR_ENTIDADE <> 0 then
  begin
    ReopenEntidades();
    if QrClientes.Locate('Codigo', VAR_ENTIDADE, []) then
    begin
      EdCodigo.ValueVariant := VAR_ENTIDADE;
      CBCodigo.KeyValue     := VAR_ENTIDADE;
    end;
  end;
end;

procedure TFmPtosCadPto.SpeedButton6Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  FmPrincipal.CadastroDeEntidades(EdConsultor.ValueVariant);
  if VAR_ENTIDADE <> 0 then
  begin
    ReopenEntidades();
    if QrConsultores.Locate('Codigo', VAR_ENTIDADE, []) then
    begin
      EdCodigo.ValueVariant := VAR_ENTIDADE;
      CBCodigo.KeyValue     := VAR_ENTIDADE;
    end;
  end;
end;

procedure TFmPtosCadPto.SpeedButton7Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  FmPrincipal.CadastroDeEntidades(EdGerente.ValueVariant);
  if VAR_ENTIDADE <> 0 then
  begin
    ReopenEntidades();
    if QrGerentes.Locate('Codigo', VAR_ENTIDADE, []) then
    begin
      EdCodigo.ValueVariant := VAR_ENTIDADE;
      CBCodigo.KeyValue     := VAR_ENTIDADE;
    end;
  end;
end;

procedure TFmPtosCadPto.SpeedButton8Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrGraCusPrc.Close;
    QrGraCusPrc.Open;
    EdGraCusPrc.ValueVariant := VAR_CADASTRO;
    CBGraCusPrc.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPtosCadPto.SpeedButton9Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrCarteiras.Close;
    QrCarteiras.Open;
    EdCarteira.ValueVariant := VAR_CADASTRO;
    CBCarteira.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmPtosCadPto.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPtosCadPto, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'ptoscadpto');
end;

procedure TFmPtosCadPto.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPtosCadPtoCodigo.Value;
  Close;
end;

procedure TFmPtosCadPto.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('ptoscadpto', 'Codigo', LaTipo.SQLType,
    QrPtosCadPtoCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPtosCadPto, PainelEdit,
    'ptoscadpto', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPtosCadPto.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'ptoscadpto', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ptoscadpto', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ptoscadpto', 'Codigo');
end;

procedure TFmPtosCadPto.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPtosCadPto, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'ptoscadpto');
end;

procedure TFmPtosCadPto.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  ReopenEntidades();
  QrCarteiras.Open;
  QrGraCusPrc.Open;
  QrCNAB_Cfg.Open;
  // reabrir conforme cliente
  //QrPediPrzCab.Open;
end;

procedure TFmPtosCadPto.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPtosCadPtoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPtosCadPto.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPtosCadPto.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPtosCadPtoCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPtosCadPto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPtosCadPto.QrPtosCadPtoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPtosCadPto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosCadPto.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPtosCadPtoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ptoscadpto', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPtosCadPto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPtosCadPto.QrPtosCadPtoBeforeOpen(DataSet: TDataSet);
begin
  QrPtosCadPtoCodigo.DisplayFormat := FFormatFloat;
end;

end.

