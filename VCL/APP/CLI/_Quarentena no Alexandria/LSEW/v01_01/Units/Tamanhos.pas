unit Tamanhos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, UnDmkProcFunc, UnDmkEnums;

type
  TFmTamanhos = class(TForm)
    PainelDados: TPanel;
    DsTamanhos: TDataSource;
    QrTamanhos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrTamanhosCodigo: TIntegerField;
    QrTamanhosCodUsu: TIntegerField;
    QrTamanhosNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTamanhosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTamanhosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmTamanhos: TFmTamanhos;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTamanhos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTamanhos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTamanhosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTamanhos.DefParams;
begin
  VAR_GOTOTABELA := 'tamanhos';
  VAR_GOTOMYSQLTABLE := QrTamanhos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome');
  VAR_SQLx.Add('FROM tamanhos');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTamanhos.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'tamanhos', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmTamanhos.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmTamanhos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmTamanhos.AlteraRegistro;
var
  Tamanhos : Integer;
begin
  Tamanhos := QrTamanhosCodigo.Value;
  if QrTamanhosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Tamanhos, Dmod.MyDB, 'Tamanhos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Tamanhos, Dmod.MyDB, 'Tamanhos', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmTamanhos.IncluiRegistro;
var
  Cursor : TCursor;
  Tamanhos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Tamanhos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Tamanhos', 'Tamanhos', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Tamanhos))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, Tamanhos);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmTamanhos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTamanhos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTamanhos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTamanhos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTamanhos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTamanhos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTamanhos.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTamanhos, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'tamanhos');
end;

procedure TFmTamanhos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTamanhosCodigo.Value;
  Close;
end;

procedure TFmTamanhos.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('tamanhos', 'Codigo', LaTipo.SQLType,
    QrTamanhosCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmTamanhos, PainelEdit,
    'tamanhos', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTamanhos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'tamanhos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tamanhos', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tamanhos', 'Codigo');
end;

procedure TFmTamanhos.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrTamanhos, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'tamanhos');
end;

procedure TFmTamanhos.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmTamanhos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTamanhosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTamanhos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTamanhos.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTamanhosCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmTamanhos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmTamanhos.QrTamanhosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTamanhos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmTamanhos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTamanhosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tamanhos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTamanhos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTamanhos.QrTamanhosBeforeOpen(DataSet: TDataSet);
begin
  QrTamanhosCodigo.DisplayFormat := FFormatFloat;
end;

end.

