unit MoviL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UmySQLModule, UnMySQLCuringa, mySQLDbTables,
  dmkPermissoes, dmkGeral, frxClass, dmkDBEdit, Grids, DBGrids, dmkDBGrid,
  ComCtrls, Menus, frxDBSet, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, dmkLabel,  UnDmkProcFunc, UnDmkEnums;

type
  TFmMoviL = class(TForm)
    QrMoviL: TmySQLQuery;
    DsMoviL: TDataSource;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtPagtos: TBitBtn;
    BtVenda: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PainelDados: TPanel;
    QrMoviLNOMGRACUSPRC: TWideStringField;
    QrMoviLNOMEENTI: TWideStringField;
    QrMoviLFRETEA: TFloatField;
    QrMoviLCodigo: TIntegerField;
    QrMoviLControle: TIntegerField;
    QrMoviLDataPedi: TDateField;
    QrMoviLDataReal: TDateField;
    QrMoviLCliente: TIntegerField;
    QrMoviLTotal: TFloatField;
    QrMoviLPago: TFloatField;
    QrMoviLLk: TIntegerField;
    QrMoviLDataCad: TDateField;
    QrMoviLDataAlt: TDateField;
    QrMoviLUserCad: TIntegerField;
    QrMoviLUserAlt: TIntegerField;
    QrMoviLAlterWeb: TSmallintField;
    QrMoviLAtivo: TSmallintField;
    QrMoviLGraCusPrc: TIntegerField;
    QrMoviLTransportadora: TIntegerField;
    QrMoviLFrete: TFloatField;
    QrMoviLObserv: TWideStringField;
    QrMoviLDescon: TFloatField;
    QrTotMovim: TmySQLQuery;
    QrTotMovimTOTITENS: TFloatField;
    DsTotMovim: TDataSource;
    PnTopo: TPanel;
    Panel8: TPanel;
    Panel19: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Panel1: TPanel;
    Label1: TLabel;
    Label26: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    EdDBDataVenda: TdmkDBEdit;
    EdDBDataPed: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    PnInfoCli: TPanel;
    Panel14: TPanel;
    Label2: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Panel15: TPanel;
    BtUltimo: TBitBtn;
    PCProd: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    DBGridProd: TdmkDBGrid;
    Panel6: TPanel;
    Panel9: TPanel;
    Image2: TImage;
    PnBottom: TPanel;
    Label22: TLabel;
    Label25: TLabel;
    Label24: TLabel;
    Label21: TLabel;
    Label18: TLabel;
    Panel4: TPanel;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    PMPagtos: TPopupMenu;
    IncluiPagtos1: TMenuItem;
    ExcluiPagtos1: TMenuItem;
    PMVenda: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    QrMoviLDATAREAL_TXT: TWideStringField;
    QrMoviLNOMECAD2: TWideStringField;
    QrMoviLNOMEALT2: TWideStringField;
    QrMoviLNOMECAD: TWideStringField;
    QrMoviLNOMEALT: TWideStringField;
    QrSenhas: TmySQLQuery;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasLk: TIntegerField;
    DsSenhas: TDataSource;
    QrHist: TmySQLQuery;
    QrHistDATAREAL_TXT: TWideStringField;
    QrHistPOSIt: TFloatField;
    QrHistTotal: TFloatField;
    QrHistFrete: TFloatField;
    QrHistDataReal: TDateField;
    QrHistNOMECAD2: TWideStringField;
    QrHistNOMECAD: TWideStringField;
    QrHistUserCad: TIntegerField;
    QrHistCodigo: TIntegerField;
    DsHist: TDataSource;
    QrMovim: TmySQLQuery;
    QrMovimNOMEGRA: TWideStringField;
    QrMovimNOMETAM: TWideStringField;
    QrMovimNOMECOR: TWideStringField;
    QrMovimControle: TIntegerField;
    QrMovimConta: TIntegerField;
    QrMovimGrade: TIntegerField;
    QrMovimCor: TIntegerField;
    QrMovimTam: TIntegerField;
    QrMovimQtd: TFloatField;
    QrMovimVal: TFloatField;
    QrMovimLk: TIntegerField;
    QrMovimDataCad: TDateField;
    QrMovimDataAlt: TDateField;
    QrMovimUserCad: TIntegerField;
    QrMovimUserAlt: TIntegerField;
    QrMovimDataPedi: TDateField;
    QrMovimDataReal: TDateField;
    QrMovimPRECO: TFloatField;
    QrMovimMotivo: TSmallintField;
    QrMovimVen: TFloatField;
    QrMovimPOSIq: TFloatField;
    QrMovimPOSIv: TFloatField;
    QrMovimVAZIO: TIntegerField;
    DsMovim: TDataSource;
    DBText1: TDBText;
    Label23: TLabel;
    QrMoviLPOSIt: TFloatField;
    QrMoviLTOTALVDA: TFloatField;
    Panel7: TPanel;
    StaticText3: TStaticText;
    GridPagtos: TDBGrid;
    QrTotPagtos: TmySQLQuery;
    QrTotPagtosCredito: TFloatField;
    QrTotProd: TmySQLQuery;
    QrTotProdTotal: TFloatField;
    QrPagtos: TmySQLQuery;
    QrPagtosSEQ: TIntegerField;
    QrPagtosData: TDateField;
    QrPagtosVencimento: TDateField;
    QrPagtosCredito: TFloatField;
    QrPagtosBanco: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosDocumento: TFloatField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosFatNum: TFloatField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosControle: TIntegerField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    DsPagtos: TDataSource;
    QrFutNeg: TmySQLQuery;
    QrFutNegConta: TIntegerField;
    QrFutNegGrade: TIntegerField;
    QrFutNegCor: TIntegerField;
    QrFutNegTam: TIntegerField;
    QrFutNegQtd: TFloatField;
    QrFutNegVal: TFloatField;
    QrFutNegEstqQ: TFloatField;
    QrFutNegEstqV: TFloatField;
    QrFutNegFutQtd: TFloatField;
    QrFutNegFutVal: TFloatField;
    QrFutNegNOMEGRADE: TWideStringField;
    QrFutNegNOMECOR: TWideStringField;
    QrFutNegNOMETAM: TWideStringField;
    frxDsFutNeg: TfrxDBDataset;
    frxFutNeg: TfrxReport;
    frxMoviL: TfrxDBDataset;
    QrMovim2: TmySQLQuery;
    QrMovim2Grade: TIntegerField;
    QrMovim2Cor: TIntegerField;
    QrMovim2Tam: TIntegerField;
    QrMovim2Val: TFloatField;
    QrMovim2Qtd: TFloatField;
    QrMovim2Conta: TIntegerField;
    QrLocFoto: TmySQLQuery;
    IdHTTP1: TIdHTTP;
    QrLocFotoNOMEFOTO: TWideStringField;
    QrPagtosAgencia: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMoviLAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBEdCodigoDblClick(Sender: TObject);
    procedure BtPagtosClick(Sender: TObject);
    procedure BtVendaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure QrMoviLCalcFields(DataSet: TDataSet);
    procedure QrMoviLAfterScroll(DataSet: TDataSet);
    procedure QrMovimCalcFields(DataSet: TDataSet);
    procedure Altera1Click(Sender: TObject);
    procedure IncluiPagtos1Click(Sender: TObject);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure QrMoviLBeforeClose(DataSet: TDataSet);
    procedure ExcluiPagtos1Click(Sender: TObject);
    procedure DBEdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrMovimAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenHist(Cliente: Integer);
    procedure ReopenPagtos;
  public
    { Public declarations }
    FTabLctALS: String;
    FPagto: Boolean;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTOTAIS;
    procedure ReopenMovim(Conta: Integer);
    procedure AtualizaTotVenda(Codigo, Controle: Integer);
  end;

var
  FmMoviL: TFmMoviL;

implementation

uses Module, VendaPesq, MyDBCheck, MoviLEdit, MoviLPagtos, ModuleProd,
ModuleGeral, Principal, UnMyObjects, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMoviL.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMoviL.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMoviLCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMoviL.DefParams;
begin
  VAR_GOTOTABELA := 'movil';
  VAR_GOTOmySQLTABLE := QrMoviL;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT prc.Nome NOMGRACUSPRC,');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  VAR_SQLx.Add('ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA, moc.*');
  VAR_SQLx.Add('FROM movil moc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente');
  VAR_SQLx.Add('LEFT JOIN gracusprc prc ON prc.Codigo = moc.GraCusPrc');
  VAR_SQLx.Add('WHERE moc.Codigo > 0');
  //
  VAR_SQL1.Add('AND moc.Codigo=:P0');
  //
  VAR_SQLa.Add('AND moc.Nome Like :P0');
  //
end;

procedure TFmMoviL.ExcluiPagtos1Click(Sender: TObject);
var
  FatNum, QtdX, ValX, CusX: Double;
  Codigo, Controle, FatParcela, FatID: Integer;
begin
  if QrPagtos.RecordCount > 0 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    FatParcela := QrPagtosFatParcela.Value;
    FatNum     := QrPagtosFatNum.Value;
    FatID      := QrPagtosFatID.Value;
    Codigo     := QrMoviLCodigo.Value;
    Controle   := QrMoviLControle.Value;
    //
    if MLAGeral.NaoPermiteExclusaoDeLancto(FatParcela, FatNum, FatID) then Exit;
    if Application.MessageBox('Confirma a exclus�o deste item de pagamento?',
    'Exclus�o de Pagamento', MB_YESNOCANCEL+MB_DEFBUTTON2+MB_ICONQUESTION) =
    ID_YES then
    begin
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + FTabLctALS + ' WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrPagtosControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctALS + ' SET FatParcela=FatParcela-1');
      Dmod.QrUpd.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
      Dmod.QrUpd.Params[0].AsInteger := FatParcela;
      Dmod.QrUpd.Params[1].AsInteger := FatID;
      Dmod.QrUpd.Params[2].AsFloat   := FatNum;
      Dmod.QrUpd.ExecSQL;
      //
      if QrMoviLDataReal.Value > 0 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE movil SET DataReal=:P0 WHERE Codigo=:P1');
        Dmod.QrUpd.Params[0].AsDate    := 0;
        Dmod.QrUpd.Params[1].AsInteger := Codigo;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE movim SET DataReal=:P0 WHERE Controle=:P1');
        Dmod.QrUpd.Params[0].AsDate    := 0;
        Dmod.QrUpd.Params[1].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
        //
        QrMovim2.Close;
        QrMovim2.Params[0].AsInteger := Controle;
        QrMovim2.Open;
        //
        DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
          QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
        'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, ',
        'la.Agencia, la.FatID, la.ContaCorrente, la.Documento, ',
        'la.Descricao, la.FatParcela, la.FatNum, ca.Nome NOMECARTEIRA, ',
        'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ca.Agencia1, ca.Conta1, ',
        'ca.TipoDoc, la.Controle, ca.Tipo CARTEIRATIPO ',
        'FROM ' + FTabLctALS + ' la ',
        'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
        'WHERE FatID=' + Geral.FF0(VAR_FATID_0810),
        'AND FatNum=' + Geral.FFI(FatNum),
        'ORDER BY la.FatParcela, la.Vencimento ',
        '']);
      AtualizaTotVenda(QrMoviLCodigo.Value, QrMoviLControle.Value);
      LocCod(QrMoviLCodigo.Value, QrMoviLCodigo.Value);
    end;
  end;
end;

procedure TFmMoviL.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMoviL.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMoviL.ReopenHist(Cliente: Integer);
begin
  QrHist.Close;
  QrHist.Params[0].AsInteger := Cliente;
  QrHist.Open;
  //
  if QrHist.RecordCount > 0 then
    BtUltimo.Enabled := True
  else
    BtUltimo.Enabled := False;
end;

procedure TFmMoviL.ReopenMovim(Conta: Integer);
begin
  QrMovim.Close;
  QrMovim.Params[0].AsInteger := QrMoviLControle.Value;
  QrMovim.Open;
  //
  if Conta > 0 then QrMovim.Locate('Conta', Conta, []);
end;

procedure TFmMoviL.ReopenPagtos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
    'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, ',
    'la.Agencia, la.FatID, la.ContaCorrente, la.Documento, ',
    'la.Descricao, la.FatParcela, la.FatNum, ca.Nome NOMECARTEIRA, ',
    'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ca.Agencia1, ca.Conta1, ',
    'ca.TipoDoc, la.Controle, ca.Tipo CARTEIRATIPO ',
    'FROM ' + FTabLctALS + ' la ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0810),
    'AND FatNum=' + Geral.FF0(QrMoviLControle.Value),
    'ORDER BY la.FatParcela, la.Vencimento ',
    '']);
end;

procedure TFmMoviL.ReopenTOTAIS;
begin
  QrTotMovim.Close;
  QrTotMovim.Params[0].AsInteger := QrMoviLControle.Value;
  QrTotMovim.Open;
end;

procedure TFmMoviL.DBEdCodigoDblClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMoviLCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMoviL.DBEdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{var
  Codigo: Integer;}
begin
  //Codigo  := 0;
  if (Key = VK_F4) then
  begin
    FmPrincipal.FDepto    := 3;
    FmPrincipal.FCodDepto := QrMoviLCodigo.Value;
    Close;
    {if DBCheck.CriaFm(TFmVendaPesq, FmVendaPesq, afmoNegarComAviso) then
    begin
      FmVendaPesq.FDepto := 3;
      FmVendaPesq.ShowModal;
      FmVendaPesq.Destroy;
      //
      Codigo := FmVendaPesq.FCodigo;
      if Codigo > 0 then
      begin
        LocCod(Codigo, Codigo);
      end;
    end;}
  end;
end;

procedure TFmMoviL.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMoviL.SbImprimeClick(Sender: TObject);
begin
  ReopenTOTAIS;
end;

procedure TFmMoviL.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMoviL.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMoviL.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMoviL.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMoviL.Altera1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrMoviLCodigo.Value;
  Controle := QrMoviLControle.Value;
  //
  if DBCheck.CriaFm(TFmMoviLEdit, FmMoviLEdit, afmoNegarComAviso) then
  begin
    FmMoviLEdit.LaTipo.SQLType := stUpd;
    FmMoviLEdit.FCodigo        := QrMoviLCodigo.Value;
    FmMoviLEdit.FControle      := QrMoviLControle.Value;
    //
    FmMoviLEdit.ShowModal;
    FmMoviLEdit.Destroy;
    FmMoviLEdit := nil;
    //
    AtualizaTotVenda(Codigo, Controle);
  end;
end;

procedure TFmMoviL.AtualizaTotVenda(Codigo, Controle: Integer);
var
  Total, Pago: Double;
begin
  //Calcula o total dos produtos
  QrTotProd.Close;
  QrTotProd.Params[0].AsInteger := Controle;
  QrTotProd.Open;
  //
  //Calcula total pagto
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotPagtos, Dmod.MyDB, [
    'SELECT SUM(Credito) Credito ',
    'FROM ' + FTabLctALS,
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0810),
    'AND FatNum=' + Geral.FF0(Controle),
    '']);
  //
  Total := QrTotProdTotal.Value;
  Pago  := QrTotPagtosCredito.Value;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'movil', False,
  [
    'Total', 'Pago', 'Controle'
  ], ['Codigo'],
  [
    Total, Pago, Controle
  ], [Codigo]) then
  begin
    LocCod(Codigo, Codigo);
  end;  
end;

procedure TFmMoviL.BtPagtosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagtos, BtPagtos);
end;

procedure TFmMoviL.BtSaidaClick(Sender: TObject);
begin
  VAR_COD               := QrMoviLCodigo.Value;
  FmPrincipal.FCodDepto := 0;
  Close;
end;

procedure TFmMoviL.BtVendaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVenda, BtVenda);
end;

procedure TFmMoviL.FormCreate(Sender: TObject);
var
  Proxy: Integer;
begin
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  QrSenhas.Open;
end;

procedure TFmMoviL.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmMoviL.QrMoviLAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMoviL.QrMoviLAfterScroll(DataSet: TDataSet);
begin
  ReopenHist(QrMoviLCliente.Value);
  ReopenMovim(0);
  ReopenPagtos;
  //
  if QrMoviLDataReal.Value > 0 then
  begin
    Altera1.Enabled       := False;
    IncluiPagtos1.Enabled := False;
  end else
  begin
    Altera1.Enabled       := True;
    IncluiPagtos1.Enabled := True;
  end;
end;

procedure TFmMoviL.QrMoviLBeforeClose(DataSet: TDataSet);
begin
  QrHist.Close;
  QrMovim.Close;
  QrPagtos.Close;
end;

procedure TFmMoviL.QrMoviLCalcFields(DataSet: TDataSet);
begin
  QrMoviLDATAREAL_TXT.Value := Geral.FDT(QrMoviLDataReal.Value, 2);
  QrMoviLPOSIt.Value        := QrMoviLTotal.Value * -1;
  QrMoviLTOTALVDA.Value     := QrMoviLTotal.Value + QrMoviLFrete.Value - QrMoviLDescon.Value;
  case QrMoviLUserCad.Value of
    -2: QrMoviLNOMECAD2.Value  := 'BOSS [Administrador]';
    -1: QrMoviLNOMECAD2.Value  := 'MASTER [Admin. DERMATEK]';
     0: QrMoviLNOMECAD2.Value  := 'N�o definido';
    else QrMoviLNOMECAD2.Value := QrMoviLNOMECAD.Value;
  end;
  case QrMoviLUserAlt.Value of
    -2: QrMoviLNOMEALT2.Value  := 'BOSS [Administrador]';
    -1: QrMoviLNOMEALT2.Value  := 'MASTER [Admin.DERMATEK]';
     0: QrMoviLNOMEALT2.Value  := '- - -';
    else QrMoviLNOMEALT2.Value := QrMoviLNOMEALT.Value;
  end;
end;

procedure TFmMoviL.QrMovimAfterScroll(DataSet: TDataSet);
var
  Produto: String;
begin
  QrLocFoto.Close;
  QrLocFoto.Params[0].AsInteger := QrMovimGrade.Value;
  QrLocFoto.Params[1].AsInteger := QrMovimCor.Value;
  QrLocFoto.Params[2].AsInteger := QrMovimTam.Value;
  QrLocFoto.Open;
  if QrLocFoto.RecordCount > 0 then
    Produto := QrLocFotoNOMEFOTO.Value
  else
    Produto := '';
  FmPrincipal.CarregaFoto(Image2, Produto);
  if FmMoviLEdit <> nil then
    FmPrincipal.CarregaFoto(FmMoviLEdit.Image2, Produto);
end;

procedure TFmMoviL.QrMovimCalcFields(DataSet: TDataSet);
begin
  if QrMovimQtd.Value = 0 then QrMovimPRECO.Value := 0 else
  QrMovimPRECO.Value := QrMovimVen.Value / -QrMovimQtd.Value;
  QrMovimPOSIq.Value := -QrMovimQtd.Value;
  QrMovimPOSIv.Value := QrMovimVen.Value;
end;

procedure TFmMoviL.QrPagtosCalcFields(DataSet: TDataSet);
begin
  QrPagtosSEQ.Value := QrPagtos.RecNo;
end;

procedure TFmMoviL.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviL.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
end;

procedure TFmMoviL.FormShow(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmMoviL.Inclui1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMoviLEdit, FmMoviLEdit, afmoNegarComAviso) then
  begin
    FmMoviLEdit.LaTipo.SQLType := stIns;
    //
    FmMoviLEdit.ShowModal;
    FmMoviLEdit.Destroy;
    FmMoviLEdit := nil;
  end;
end;

procedure TFmMoviL.IncluiPagtos1Click(Sender: TObject);
var
  Controle: Integer;
  QtdX, ValX, CusX: Double;
  DataReal: TDateTime;
begin
  Controle := QrMoviLControle.Value;
  //
  QrFutNeg.Close;
  QrFutNeg.Params[0].AsInteger := Controle;
  QrFutNeg.Open;
  if QrFutNeg.RecordCount > 0 then
  begin
    MyObjects.frxMostra(frxFutNeg, 'Estoque futuro negativo');
    Exit;
  end else
  begin
    if DBCheck.CriaFm(TFmMoviLPagtos, FmMoviLPagtos, afmoNegarComAviso) then
    begin
      FmMoviLPagtos.FTotValor := QrMoviLTOTALVDA.Value - QrMoviLPago.Value;
      FmMoviLPagtos.FDevedor  := QrMoviLCliente.Value;
      FmMoviLPagtos.FControle := QrMoviLControle.Value;
      //
      FmMoviLPagtos.ShowModal;
      FmMoviLPagtos.Destroy;
      AtualizaTotVenda(QrMoviLCodigo.Value, QrMoviLControle.Value);
      //

      if (QrMoviLPago.Value = QrMoviLTOTALVDA.Value) and
        (QrMoviLDataReal.Value = 0) then
      begin
        // Atualiza custo caso seje confirma��o de venda
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE movim SET Val=:P0 WHERE Controle=:P1 AND Conta=:P2 ');
        while not QrMovim2.Eof do
        begin
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          Dmod.QrUpdU.Params[00].AsFloat   := CusX * QrMovim2Qtd.Value;
          Dmod.QrUpdU.Params[01].Asinteger := Controle;
          Dmod.QrUpdU.Params[02].Asinteger := QrMovim2Conta.Value;
          Dmod.QrUpdU.ExecSQL;
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          QrMovim2.Next;
        end;

        // Confirma venda; todos itens ao mesmo tempo
        QrMovim2.Close;
        QrMovim2.Params[0].AsInteger := QrMoviLControle.Value;
        QrMovim2.Open;
        QrMovim2.First;

        DataReal := DModG.ObtemAgora();

        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE movim SET DataPedi=:P0, DataReal=:P1');
        Dmod.QrUpdU.SQL.Add('WHERE Controle=:P2');
        Dmod.QrUpdU.Params[00].AsString  := Geral.FDT(QrMoviLDataPedi.Value, 1);
        Dmod.QrUpdU.Params[01].AsString  := Geral.FDT(DataReal, 1);
        //
        Dmod.QrUpdU.Params[02].AsInteger := Controle;
        Dmod.QrUpdU.ExecSQL;

        //
        // Atualiza estoques
        QrMovim2.First;
        while not QrMovim2.Eof do
        begin
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          QrMovim2.Next;
        end;

        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE movil SET DataReal=:P0 WHERE Codigo=:P1');
        Dmod.QrUpd.Params[0].AsDate    := DataReal;
        Dmod.QrUpd.Params[1].AsInteger := QrMoviLCodigo.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      LocCod(QrMoviLCodigo.Value, QrMoviLCodigo.Value);
    end;
  end;
end;

end.




