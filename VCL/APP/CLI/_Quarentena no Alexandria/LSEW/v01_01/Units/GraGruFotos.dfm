object FmGraGruFotos: TFmGraGruFotos
  Left = 339
  Top = 185
  BorderStyle = bsSizeToolWin
  Caption = 'PRD-GRADE-016 :: Cadastro de fotos'
  ClientHeight = 467
  ClientWidth = 693
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnNiveis: TPanel
    Left = 0
    Top = 48
    Width = 693
    Height = 371
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 419
    Width = 693
    Height = 48
    Align = alBottom
    TabOrder = 0
    object LaProd: TLabel
      Left = 512
      Top = 12
      Width = 60
      Height = 24
      Caption = 'LaProd'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Panel2: TPanel
      Left = 581
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 18
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtCaptura: TBitBtn
      Left = 5
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Captura'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtCapturaClick
    end
    object BtUpload: TBitBtn
      Left = 189
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Salvar'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtUploadClick
    end
    object PBProgress: TProgressBar
      Left = 285
      Top = 16
      Width = 212
      Height = 17
      TabOrder = 3
    end
    object BtAbrir: TBitBtn
      Left = 97
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Abrir'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = BtAbrirClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 693
    Height = 48
    Align = alTop
    Caption = 'Fotos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 609
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 189
      ExplicitTop = 21
    end
    object LaTipo: TdmkLabel
      Left = 610
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 48
    Width = 693
    Height = 371
    Align = alClient
    TabOrder = 3
    object Image2: TImage
      Left = 351
      Top = 95
      Width = 336
      Height = 240
      Stretch = True
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 691
      Height = 86
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 0
      object TabSheet3: TTabSheet
        Caption = 'Mercadorias'
        object Label19: TLabel
          Left = 3
          Top = 9
          Width = 56
          Height = 13
          Caption = 'Mercadoria:'
        end
        object Label30: TLabel
          Left = 364
          Top = 9
          Width = 19
          Height = 13
          Caption = 'Cor:'
        end
        object Label31: TLabel
          Left = 522
          Top = 9
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
        end
        object EdProd: TdmkEditCB
          Left = 3
          Top = 25
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBProd
          IgnoraDBLookupComboBox = False
        end
        object CBProd: TdmkDBLookupComboBox
          Left = 61
          Top = 25
          Width = 290
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsGrades
          TabOrder = 1
          dmkEditCB = EdProd
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBCor: TdmkDBLookupComboBox
          Left = 364
          Top = 25
          Width = 145
          Height = 21
          KeyField = 'Cor'
          ListField = 'NOMECOR'
          ListSource = DsGradesCors
          TabOrder = 2
          dmkEditCB = EdCor
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCor: TdmkEditCB
          Left = 474
          Top = 1
          Width = 35
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCor
          IgnoraDBLookupComboBox = False
        end
        object CBTam: TdmkDBLookupComboBox
          Left = 522
          Top = 25
          Width = 145
          Height = 21
          KeyField = 'Tam'
          ListField = 'NOMETAM'
          ListSource = DsGradesTams
          TabOrder = 4
          dmkEditCB = EdTam
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdTam: TdmkEditCB
          Left = 632
          Top = 1
          Width = 35
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTam
          IgnoraDBLookupComboBox = False
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Kits'
        ImageIndex = 1
        object Label34: TLabel
          Left = 6
          Top = 9
          Width = 15
          Height = 13
          Caption = 'Kit:'
        end
        object EdKit: TdmkEditCB
          Left = 6
          Top = 25
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBKit
          IgnoraDBLookupComboBox = False
        end
        object CBKit: TdmkDBLookupComboBox
          Left = 65
          Top = 25
          Width = 607
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsGradeK
          TabOrder = 1
          dmkEditCB = EdKit
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 43
    Top = 9
  end
  object FTP: TIdFTP
    OnWorkEnd = FTPWorkEnd
    IPVersion = Id_IPv4
    Passive = True
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 12
    Top = 10
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 144
    Top = 9
  end
  object QrGradeK: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gradek'
      'WHERE Ativo = 1')
    Left = 380
    Top = 9
    object QrGradeKCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradek.Codigo'
    end
    object QrGradeKNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gradek.Nome'
      Size = 100
    end
  end
  object DsGradeK: TDataSource
    DataSet = QrGradeK
    Left = 408
    Top = 9
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradesBeforeClose
    AfterScroll = QrGradesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 436
    Top = 9
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 464
    Top = 9
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY Controle')
    Left = 492
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
      Size = 30
    end
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradescors.Codigo'
      Required = True
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradescors.Controle'
      Required = True
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'gradescors.Cor'
      Required = True
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradescors.Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradescors.DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradescors.DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradescors.UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradescors.UserAlt'
    end
    object QrGradesCorsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'gradescors.AlterWeb'
      Required = True
    end
    object QrGradesCorsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'gradescors.Ativo'
      Required = True
    end
  end
  object DsGradesCors: TDataSource
    DataSet = QrGradesCors
    Left = 520
    Top = 9
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY Controle')
    Left = 548
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradestams.Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradestams.Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'gradestams.Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradestams.Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradestams.DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradestams.DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradestams.UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradestams.UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsGradesTams: TDataSource
    DataSet = QrGradesTams
    Left = 576
    Top = 9
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 'Jpg (*.jpg)|*.jpg'
    Left = 185
    Top = 9
  end
end
