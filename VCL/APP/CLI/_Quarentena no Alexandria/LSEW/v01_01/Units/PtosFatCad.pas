unit PtosFatCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, ComCtrls, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, frxClass, frxDBSet, UnDmkProcFunc, UnDmkEnums;

type
  TFmPtosFatCad = class(TForm)
    PainelDados: TPanel;
    DsPtosFatCad: TDataSource;
    QrPtosFatCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    PMFatura: TPopupMenu;
    BtFatura: TBitBtn;
    QrPtosCadPto: TmySQLQuery;
    DsPtosCadPto: TDataSource;
    LaCliente: TLabel;
    EdPontoVda: TdmkEditCB;
    CBPontoVda: TdmkDBLookupComboBox;
    dmkLabel1: TdmkLabel;
    Label2: TLabel;
    EdConsultor: TdmkEditCB;
    CBConsultor: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrConsultores: TmySQLQuery;
    DsConsultores: TDataSource;
    dmkValUsu1: TdmkValUsu;
    EdEncerrou: TdmkEdit;
    QrPtosCadPtoCodigo: TIntegerField;
    QrPtosCadPtoCodUsu: TIntegerField;
    QrPtosCadPtoNome: TWideStringField;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dmkLabel5: TdmkLabel;
    Label5: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    PMItens: TPopupMenu;
    QrPtosFatGru: TmySQLQuery;
    DsPtosFatGru: TDataSource;
    PnGrids: TPanel;
    DBEdit10: TDBEdit;
    dmkLabel9: TdmkLabel;
    EdAbertura: TdmkEdit;
    dmkLabel10: TdmkLabel;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    TSItemAItem: TTabSheet;
    QrPtosFatGruCodigo: TIntegerField;
    QrPtosFatGruNome: TWideStringField;
    N1: TMenuItem;
    Cancelaritens1: TMenuItem;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    EdGerente: TdmkEditCB;
    CBGerente: TdmkDBLookupComboBox;
    DsGerentes: TDataSource;
    QrGerentes: TmySQLQuery;
    QrConsultoresCodigo: TIntegerField;
    QrConsultoresNO_ENT: TWideStringField;
    QrGerentesCodigo: TIntegerField;
    QrGerentesNO_ENT: TWideStringField;
    QrPtosFatCadCodigo: TIntegerField;
    QrPtosFatCadCodUsu: TIntegerField;
    QrPtosFatCadPontoVda: TIntegerField;
    QrPtosFatCadConsultor: TIntegerField;
    QrPtosFatCadGerente: TIntegerField;
    QrPtosFatCadAbertura: TDateTimeField;
    QrPtosFatCadEncerrou: TDateTimeField;
    QrPtosFatCadValorTot: TFloatField;
    QrPtosFatCadComisPto: TFloatField;
    QrPtosFatCadComisCon: TFloatField;
    QrPtosFatCadComisGer: TFloatField;
    QrPtosFatCadValorPto: TFloatField;
    QrPtosFatCadValorCon: TFloatField;
    QrPtosFatCadValorGer: TFloatField;
    QrPtosFatCadOndeCad: TSmallintField;
    QrPtosFatCadOndeEnc: TSmallintField;
    QrPtosFatCadLk: TIntegerField;
    QrPtosFatCadDataCad: TDateField;
    QrPtosFatCadDataAlt: TDateField;
    QrPtosFatCadUserCad: TIntegerField;
    QrPtosFatCadUserAlt: TIntegerField;
    QrPtosFatCadAlterWeb: TSmallintField;
    QrPtosFatCadAtivo: TSmallintField;
    QrPtosFatCadNO_Consultor: TWideStringField;
    QrPtosFatCadNO_Gerente: TWideStringField;
    EdComisCon: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    EdComisGer: TdmkEdit;
    Label12: TLabel;
    EdComisPto: TdmkEdit;
    QrPtosCadPtoConsultor: TIntegerField;
    QrPtosCadPtoGerente: TIntegerField;
    QrPtosCadPtoComisPonto: TFloatField;
    QrPtosCadPtoComisConsu: TFloatField;
    QrPtosCadPtoComisGeren: TFloatField;
    QrPtosFatCadCU_PontoVda: TIntegerField;
    QrPtosFatCadNO_PontoVda: TWideStringField;
    Label13: TLabel;
    DBEdit6: TDBEdit;
    Label14: TLabel;
    DBEdit11: TDBEdit;
    Label15: TLabel;
    DBEdit12: TDBEdit;
    Incluinovafatura1: TMenuItem;
    Alterafaturaatual1: TMenuItem;
    Excluifaturaatual1: TMenuItem;
    Incluiitenspeloestoque1: TMenuItem;
    Imprimeestoque1: TMenuItem;
    QrEstqPto: TmySQLQuery;
    QrEstqPtoGrade: TIntegerField;
    QrEstqPtoCor: TIntegerField;
    QrEstqPtoTam: TIntegerField;
    QrEstqPtoProduto: TIntegerField;
    QrEstqPtoQuanti: TFloatField;
    QrEstqPtoPRECOM: TFloatField;
    QrEstqPtoNO_GRADE: TWideStringField;
    QrEstqPtoNO_Cor: TWideStringField;
    QrEstqPtoNO_Tam: TWideStringField;
    DsEstqPto: TDataSource;
    frxEstqPto: TfrxReport;
    frxDsEstqPto: TfrxDBDataset;
    QrPtosFatIts: TmySQLQuery;
    QrPtosFatItsCodigo: TIntegerField;
    QrPtosFatItsNome: TWideStringField;
    QrPtosFatItsNO_Cor: TWideStringField;
    QrPtosFatItsNO_Tam: TWideStringField;
    QrPtosFatItsQuanti: TFloatField;
    QrPtosFatItsValor: TFloatField;
    DBGrid1: TDBGrid;
    DsPtosFatIts: TDataSource;
    DBGPtosFatIts: TDBGrid;
    Label16: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrPtosFatCadQuantTot: TFloatField;
    QrPtosFatCadNO_Carteira: TWideStringField;
    QrPtosFatCadCarteira: TIntegerField;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    QrPtosCadPtoCarteira: TIntegerField;
    Label18: TLabel;
    EdPediPrzCab: TdmkEditCB;
    CBPediPrzCab: TdmkDBLookupComboBox;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    DsPediPrzCab: TDataSource;
    dmkValUsu2: TdmkValUsu;
    QrPtosFatCadNO_PediPrzCab: TWideStringField;
    QrPtosFatCadPediPrzCab: TIntegerField;
    QrPtosCadPtoPediPrzCab: TIntegerField;
    QrPtosStqMov: TmySQLQuery;
    DsPtosStqMov: TDataSource;
    QrPtosStqMovCodigo: TIntegerField;
    QrPtosStqMovNome: TWideStringField;
    QrPtosStqMovNO_Cor: TWideStringField;
    QrPtosStqMovNO_Tam: TWideStringField;
    QrPtosStqMovQuanti: TFloatField;
    QrPtosStqMovPrecoR: TFloatField;
    QrPtosStqMovIDCtrl: TIntegerField;
    DBGPtosStqMov: TDBGrid;
    TabSheet1: TTabSheet;
    DBEdit15: TDBEdit;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    DsCNAB_Cfg: TDataSource;
    Label26: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    Label20: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    QrPtosFatCadCNAB_Cfg: TIntegerField;
    QrPtosFatCadNO_CNAB_Cfg: TWideStringField;
    QrPtosFatCadTIPOCART: TIntegerField;
    QrPtosFatCadJurosMes: TFloatField;
    QrPtosFatCadCliInt: TIntegerField;
    QrFatura: TmySQLQuery;
    DsFatura: TDataSource;
    DBGrid2: TDBGrid;
    QrFaturaControle: TIntegerField;
    QrFaturaData: TDateField;
    QrFaturaVencimento: TDateField;
    QrFaturaDocumento: TFloatField;
    QrFaturaDuplicata: TWideStringField;
    QrFaturaCompensado: TDateField;
    QrFaturaMoraDia: TFloatField;
    QrFaturaMulta: TFloatField;
    QrFaturaVendedor: TIntegerField;
    QrFaturaAccount: TIntegerField;
    QrFaturaCOMPENSADO_TXT: TWideStringField;
    BtBloqueto: TBitBtn;
    frxBloqA2: TfrxReport;
    QrCfg: TmySQLQuery;
    QrCfgLocalPag: TWideStringField;
    QrCfgNOMECED_IMP: TWideStringField;
    QrCfgNOMESAC_IMP: TWideStringField;
    frxDsCfg: TfrxDBDataset;
    QrCfgCedente: TIntegerField;
    QrCfgSacadAvali: TIntegerField;
    QrCfgEspecieTit: TWideStringField;
    QrCfgAceiteTit: TSmallintField;
    QrCfgNOMEBANCO: TWideStringField;
    frxDsFatura: TfrxDBDataset;
    QrCfgCartNum: TWideStringField;
    QrCfgEspecieVal: TWideStringField;
    QrCfgTipoCart: TSmallintField;
    QrCfgCedBanco: TIntegerField;
    QrCfgCedAgencia: TIntegerField;
    QrCfgCedConta: TWideStringField;
    QrCfgCedDAC_A: TWideStringField;
    QrCfgCedDAC_C: TWideStringField;
    QrCfgCedDAC_AC: TWideStringField;
    QrCfgCedNome: TWideStringField;
    QrCfgCedPosto: TIntegerField;
    QrFaturaBLOQUETO: TFloatField;
    QrCfgCodEmprBco: TWideStringField;
    QrCfgOperCodi: TWideStringField;
    QrCfgIDCobranca: TWideStringField;
    CkDesign: TCheckBox;
    CkZerado: TCheckBox;
    QrCfgDVB: TWideStringField;
    QrCfgTexto01: TWideStringField;
    QrCfgTexto02: TWideStringField;
    QrCfgTexto03: TWideStringField;
    QrCfgTexto04: TWideStringField;
    QrCfgTexto05: TWideStringField;
    QrCfgTexto06: TWideStringField;
    QrCfgTexto07: TWideStringField;
    QrCfgTexto08: TWideStringField;
    QrCfgTexto09: TWideStringField;
    QrCfgTexto10: TWideStringField;
    QrCfgAgContaCed: TWideStringField;
    QrCfgACEITETIT_TXT: TWideStringField;
    Label21: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBEdit22: TDBEdit;
    QrPtosCadPtoCNAB_Cfg: TIntegerField;
    QrEstqPtoCodUsu: TIntegerField;
    QrFaturaValor: TFloatField;
    QrFaturaCREDITO: TFloatField;
    QrCfgTipoCobranca: TIntegerField;
    QrCNAB_CfgCodLidrBco: TWideStringField;
    QrCNAB_CfgCedente: TIntegerField;
    QrCNAB_CfgSacadAvali: TIntegerField;
    QrCNAB_CfgCedBanco: TIntegerField;
    QrCNAB_CfgCedAgencia: TIntegerField;
    QrCNAB_CfgCedConta: TWideStringField;
    QrCNAB_CfgCedDAC_A: TWideStringField;
    QrCNAB_CfgCedDAC_C: TWideStringField;
    QrCNAB_CfgCedDAC_AC: TWideStringField;
    QrCNAB_CfgCedNome: TWideStringField;
    QrCNAB_CfgCedPosto: TIntegerField;
    QrCNAB_CfgSacAvaNome: TWideStringField;
    QrCNAB_CfgTipoCart: TSmallintField;
    QrCNAB_CfgCartNum: TWideStringField;
    QrCNAB_CfgCartCod: TWideStringField;
    QrCNAB_CfgEspecieTit: TWideStringField;
    QrCNAB_CfgAceiteTit: TSmallintField;
    QrCNAB_CfgInstrCobr1: TWideStringField;
    QrCNAB_CfgInstrCobr2: TWideStringField;
    QrCNAB_CfgInstrDias: TSmallintField;
    QrCNAB_CfgCodEmprBco: TWideStringField;
    QrCNAB_CfgJurosTipo: TSmallintField;
    QrCNAB_CfgJurosPerc: TFloatField;
    QrCNAB_CfgJurosDias: TSmallintField;
    QrCNAB_CfgMultaTipo: TSmallintField;
    QrCNAB_CfgMultaPerc: TFloatField;
    QrCNAB_CfgTexto01: TWideStringField;
    QrCNAB_CfgTexto02: TWideStringField;
    QrCNAB_CfgTexto03: TWideStringField;
    QrCNAB_CfgTexto04: TWideStringField;
    QrCNAB_CfgTexto05: TWideStringField;
    QrCNAB_CfgTexto06: TWideStringField;
    QrCNAB_CfgTexto07: TWideStringField;
    QrCNAB_CfgTexto08: TWideStringField;
    QrCNAB_CfgTexto09: TWideStringField;
    QrCNAB_CfgTexto10: TWideStringField;
    QrCNAB_CfgCNAB: TIntegerField;
    QrCNAB_CfgEnvEmeio: TSmallintField;
    QrCNAB_CfgDiretorio: TWideStringField;
    QrCNAB_CfgQuemPrint: TWideStringField;
    QrCNAB_Cfg_237Mens1: TWideStringField;
    QrCNAB_Cfg_237Mens2: TWideStringField;
    QrCNAB_CfgCodOculto: TWideStringField;
    QrCNAB_CfgSeqArq: TIntegerField;
    QrCNAB_CfgLk: TIntegerField;
    QrCNAB_CfgDataCad: TDateField;
    QrCNAB_CfgDataAlt: TDateField;
    QrCNAB_CfgUserCad: TIntegerField;
    QrCNAB_CfgUserAlt: TIntegerField;
    QrCNAB_CfgAlterWeb: TSmallintField;
    QrCNAB_CfgAtivo: TSmallintField;
    QrCNAB_CfgLastNosNum: TLargeintField;
    QrCNAB_CfgLocalPag: TWideStringField;
    QrCNAB_CfgEspecieVal: TWideStringField;
    QrCNAB_CfgOperCodi: TWideStringField;
    QrCNAB_CfgIDCobranca: TWideStringField;
    QrCNAB_CfgAgContaCed: TWideStringField;
    QrCNAB_CfgDirRetorno: TWideStringField;
    QrCNAB_CfgEspecieDoc: TWideStringField;
    QrCNAB_CfgMultaDias: TSmallintField;
    QrCNAB_CfgCartRetorno: TIntegerField;
    QrCNAB_CfgPosicoesBB: TSmallintField;
    QrCNAB_CfgIndicatBB: TWideStringField;
    QrCNAB_CfgTipoCobrBB: TWideStringField;
    QrCNAB_CfgVariacao: TIntegerField;
    QrCNAB_CfgComando: TIntegerField;
    QrCNAB_CfgDdProtesBB: TIntegerField;
    QrCNAB_CfgMsg40posBB: TWideStringField;
    QrCNAB_CfgQuemDistrb: TWideStringField;
    QrCNAB_CfgDesco1Cod: TSmallintField;
    QrCNAB_CfgDesco1Dds: TIntegerField;
    QrCNAB_CfgDesco1Fat: TFloatField;
    QrCNAB_CfgDesco2Cod: TSmallintField;
    QrCNAB_CfgDesco2Dds: TIntegerField;
    QrCNAB_CfgDesco2Fat: TFloatField;
    QrCNAB_CfgDesco3Cod: TSmallintField;
    QrCNAB_CfgDesco3Dds: TIntegerField;
    QrCNAB_CfgDesco3Fat: TFloatField;
    QrCNAB_CfgProtesCod: TSmallintField;
    QrCNAB_CfgProtesDds: TSmallintField;
    QrCNAB_CfgBxaDevCod: TSmallintField;
    QrCNAB_CfgBxaDevDds: TIntegerField;
    QrCNAB_CfgMoedaCod: TWideStringField;
    QrCNAB_CfgContrato: TFloatField;
    QrCNAB_CfgConvCartCobr: TWideStringField;
    QrCNAB_CfgConvVariCart: TWideStringField;
    QrCNAB_CfgInfNossoNum: TSmallintField;
    QrCNAB_CfgCartEmiss: TIntegerField;
    FaturamentoporLeitura1: TMenuItem;
    QrCfgEspecieDoc: TWideStringField;
    QrCfgCNAB: TIntegerField;
    QrCfgCtaCooper: TWideStringField;
    QrCfgCorresBco: TIntegerField;
    QrCfgLayoutRem: TWideStringField;
    QrCfgCorresAge: TIntegerField;
    QrCfgCorresCto: TWideStringField;
    QrCfgCART_IMP: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPtosFatCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPtosFatCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFaturaClick(Sender: TObject);
    procedure PMFaturaPopup(Sender: TObject);
    procedure QrPtosFatCadAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure QrPtosFatCadCalcFields(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure QrPtosFatGruAfterScroll(DataSet: TDataSet);
    procedure PMItensPopup(Sender: TObject);
    procedure EdPontoVdaChange(Sender: TObject);
    procedure Incluinovafatura1Click(Sender: TObject);
    procedure Alterafaturaatual1Click(Sender: TObject);
    procedure Imprimeestoque1Click(Sender: TObject);
    procedure frxEstqPtoGetValue(const VarName: string; var Value: Variant);
    procedure Incluiitenspeloestoque1Click(Sender: TObject);
    procedure QrPtosFatGruBeforeClose(DataSet: TDataSet);
    procedure QrPtosFatCadBeforeClose(DataSet: TDataSet);
    procedure Cancelaritens1Click(Sender: TObject);
    procedure QrFaturaCalcFields(DataSet: TDataSet);
    procedure BtBloquetoClick(Sender: TObject);
    procedure frxBloqA2GetValue(const VarName: string; var Value: Variant);
    function frxBloqA2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure QrCfgCalcFields(DataSet: TDataSet);
    procedure FaturamentoporLeitura1Click(Sender: TObject);
  private
    FNossoNumero, FEstqPto: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FTabLctALS: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPtosFatGru(_: Integer);
    procedure ReopenPtosStqMov(IDCtrl: Integer);
    procedure ReopenPtosFatIts();
    procedure ReopenPediPrzCab;
    procedure ReopenFatura(Controle: Integer);
  end;

var
  FmPtosFatCad: TFmPtosFatCad;
const
  FFormatFloat   = '00000';

implementation

uses Module, MyDBCheck, PtosCadPto, GraCusPrc, ModuleGeral, ModuleProd,
UnMyObjects, UCreate, Ponto, Principal, PtosFatBal, QuaisItens, UnBancos,
  ModuleBco, MyGlyfs, PtosFatBalLei, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPtosFatCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPtosFatCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPtosFatCadCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPtosFatCad.DefParams;
begin
  VAR_GOTOTABELA := 'PtosFatCad';
  VAR_GOTOMYSQLTABLE := QrPtosFatCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  //
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT pcp.CodUsu CU_PontoVda, pcp.Nome NO_PontoVda,');
  VAR_SQLx.Add('IF(con.Tipo=0,con.RazaoSocial,con.Nome) NO_Consultor,');
  VAR_SQLx.Add('IF(ger.Tipo=0,ger.RazaoSocial,ger.Nome) NO_Gerente,');
  VAR_SQLx.Add('cfg.Nome NO_CNAB_Cfg, ppc.Nome NO_PediPrzCab,');
  VAR_SQLx.Add('ppc.JurosMes, car.Nome NO_Carteira, ');
  VAR_SQLx.Add('car.Tipo TIPOCART, car.ForneceI CliInt, pfc.*');
  VAR_SQLx.Add('FROM ptosfatcad pfc');
  VAR_SQLx.Add('LEFT JOIN ptoscadpto pcp ON pcp.Codigo=pfc.PontoVda');
  VAR_SQLx.Add('LEFT JOIN entidades con ON con.Codigo=pfc.Consultor');
  VAR_SQLx.Add('LEFT JOIN entidades ger ON ger.Codigo=pfc.Gerente');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=pfc.Carteira');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pfc.PediPrzCab');
  VAR_SQLx.Add('LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pfc.CNAB_Cfg');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE pfc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND pfc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pfc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND pfc.Nome Like :P0');
  //
end;

procedure TFmPtosFatCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PtosFatCad', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmPtosFatCad.EdPontoVdaChange(Sender: TObject);
begin
  if LaTipo.SQLType = stIns then
  begin
    EdComisPto.ValueVariant   := QrPtosCadPtoComisPonto.Value;
    //
    EdConsultor.ValueVariant  := QrPtosCadPtoConsultor.Value;
    CBConsultor.KeyValue      := QrPtosCadPtoConsultor.Value;
    EdComisCon.ValueVariant   := QrPtosCadPtoComisConsu.Value;
    //
    EdGerente.ValueVariant    := QrPtosCadPtoGerente.Value;
    CBGerente.KeyValue        := QrPtosCadPtoGerente.Value;
    EdComisGer.ValueVariant   := QrPtosCadPtoComisGeren.Value;
    //
    EdCarteira.ValueVariant   := QrPtosCadPtoCarteira.Value;
    CBCarteira.KeyValue       := QrPtosCadPtoCarteira.Value;
    //
    EdPediPrzCab.ValueVariant := QrPtosCadPtoPediPrzCab.Value;
    CBPediPrzCab.KeyValue     := QrPtosCadPtoPediPrzCab.Value;
    //
    EdCNAB_Cfg.ValueVariant   := QrPtosCadPtoCNAB_Cfg.Value;
    CBCNAB_Cfg.KeyValue       := QrPtosCadPtoCNAB_Cfg.Value;
    //
  end;
  ReopenPediPrzCab;
end;

procedure TFmPtosFatCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPtosFatCad.PMItensPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrPtosFatCad.State <> dsInactive) and
    (QrPtosFatCadEncerrou.Value < 1);
  Incluiitenspeloestoque1.Enabled := Habilita;
  FaturamentoporLeitura1.Enabled  := Habilita;
end;

procedure TFmPtosFatCad.PMFaturaPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrPtosFatCad.State <> dsInactive) and
    (QrPtosFatCad.RecordCount > 0) and
    (QrPtosFatCadEncerrou.Value < 1);
  Alterafaturaatual1.Enabled := Habilita;
  //Excluifaturaatual1.Enabled := Habilita;
end;

procedure TFmPtosFatCad.Cancelaritens1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Codigo := QrPtosFatCadCodigo.Value;
  //
  if Codigo > 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DELETE FROM ' + FTabLctALS);
    DMod.QrUpd.SQL.Add('WHERE FatID=801');
    DMod.QrUpd.SQL.Add('AND FatNum=:P0');
    DMod.QrUpd.Params[0].AsInteger := QrPtosFatCadCodigo.Value;
    DMod.QrUpd.ExecSQL;
    //
    QrPtosStqMov.First;
    while not QrPtosStqMov.Eof do
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosstqmov', False, [
      'CodOut', 'TipOut', 'Status'], ['IDCtrl'], [0, 0, 50], [
      QrPtosStqMovIDCtrl.Value], False);
      QrPtosStqMov.Next;
    end;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosfatcad', False, [
    'Encerrou', 'QuantTot', 'ValorTot',
    'ValorPto', 'ValorCon', 'ValorGer'], ['Codigo'], [
    0, 0, 0, 0, 0, 0], [Codigo], True);
  end;
  LocCod(Codigo, Codigo);
  //
  Screen.Cursor := crDefault;
  //
{
  function RetiraItem(): Boolean;
  begin
    Result := False;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptosstqmov', False, [
    'CodOut', 'TipOut', 'Status'], ['IDCtrl'], [
    0, 0, 50], [QrPtosStqMovIDCtrl.Value], False) then Result := True;
  end;
var
  (*m,*) n: integer;
  q: TSelType;
begin
  // n�o pode !!! cancelar tudo ou nada!!!
  if PageControl1.ActivePageIndex <> TSItemAItem.PageIndex then
  begin
    Geral.MB_Aviso('Para retirar itens do faturamento, ' +
    'selecione a aba "' + Trim(TSItemAItem.Caption) + '".');
    Exit;
  end;
  q := istNenhum;
  if (QrPtosStqMov.State <> dsBrowse) or (QrPtosStqMov.RecordCount = 0) then
  begin
    Application.MessageBox('N�o h� item a ser exclu�do!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
      begin
        Application.MessageBox('Retirada cancelada pelo usu�rio!', 'Aviso',
          MB_OK+MB_ICONWARNING);
        q := istDesiste;
      end else q := FEscolha;
      Destroy;
    end;
  end;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;
  //m := 0;
  if (q = istSelecionados) and (DBGPtosStqMov.SelectedRows.Count < 2) then
    q := istAtual;
  case q of
    istAtual:
    begin
      if Application.MessageBox('Confirma a retirada do item selecionado?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        //if RetiraItem() then m := 1;
        RetiraItem();
    end;
    istSelecionados:
    begin
      if Geral.MB_Pergunta('Confirma a retitrada dos ' +
        Geral.FF0(DBGPtosStqMov.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
      begin
        with DBGPtosStqMov.DataSource.DataSet do
        for n := 0 to DBGPtosStqMov.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGPtosStqMov.SelectedRows.Items[n]));
          //if RetiraItem() then inc(m, 1);
          RetiraItem();
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MB_Pergunta('Confirma a retirada de todos os ' +
        Geral.FF0(QrPtosStqMov.RecordCount) + ' itens pesquisados?') = ID_YES then
      begin
        QrPtosStqMov.First;
        while not QrPtosStqMov.Eof do
        begin
          //if RetiraItem() then inc(m, 1);
          RetiraItem();
          QrPtosStqMov.Next;
        end;
      end;
    end;
  end;
  ReopenPtosStqMov(QrPtosStqMovIDCtrl.Value);
  Screen.Cursor := crDefault;
}
end;

procedure TFmPtosFatCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPtosFatCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPtosFatCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPtosFatCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPtosFatCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPtosFatCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPtosFatCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPtosFatCad.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPtosCadPto, FmPtosCadPto, afmoNegarComAviso) then
  begin
    FmPtosCadPto.ShowModal;
    FmPtosCadPto.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrPtosCadPto.Close;
      QrPtosCadPto.Open;
      if QrPtosCadPto.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdPontoVda.ValueVariant := VAR_CADASTRO;
        CBPontoVda.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPtosFatCad.SpeedButton6Click(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrGraCusPrc.Close;
      QrGraCusPrc.Open;
      if QrGraCusPrc.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdGraCusPrc.ValueVariant := VAR_CADASTRO;
        CBGraCusPrc.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
}
end;

procedure TFmPtosFatCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPtosFatCadCodigo.Value;
  Close;
end;

procedure TFmPtosFatCad.BtFaturaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFatura, BtFatura);
end;

procedure TFmPtosFatCad.Alterafaturaatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPtosFatCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'ptosfatcad');
  EdPontoVda.Enabled := QrPtosFatGru.RecordCount > 0;
  EdPontoVda.Enabled := QrPtosFatGru.RecordCount > 0;
end;

procedure TFmPtosFatCad.BtBloquetoClick(Sender: TObject);
var
  Msg, NossoNumero_Rem: String;
begin
  if QrFatura.RecordCount = 0 then Msg := 'N�o h� bloqueto para imprimir!'
  else
  if QrPtosFatCadCNAB_Cfg.Value = 0 then Msg := 'Configura��o CNAB n�o definida!'
  else
  begin
    Msg := '';
    //
    QrCfg.Close;
    QrCfg.Params[0].AsInteger := QrPtosFatCadCNAB_Cfg.Value;
    QrCfg.Open;
    //
    UBancos.GeraNossoNumero(QrCfgTipoCart.Value, QrCfgCedBanco.Value,
      QrCfgCedAgencia.Value, QrCfgCedPosto.Value, QrFaturaBLOQUETO.Value,
      QrCfgCedConta.Value, QrCfgCartNum.Value, QrCfgIDCobranca.Value,
      Geral.SoNumero_TT(QrCfgCodEmprBco.Value), QrFaturaVencimento.Value,
      QrCfgTipoCobranca.Value, QrCfgEspecieDoc.Value, QrCfgCNAB.Value,
      QrCfgCtaCooper.Value, QrCfgLayoutRem.Value, FNossoNumero, NossoNumero_Rem);
    //
    DModG.ReopenEndereco(QrPtosFatCadPontoVda.Value);
    //
    MyObjects.frxMostra(frxBloqA2, 'Bloqueto de ponto de venda');
  end;
  if Msg <> '' then
    Geral.MB_Aviso(Msg);
end;

procedure TFmPtosFatCad.BtConfirmaClick(Sender: TObject);
var
  Ponto, Codigo: Integer;
  ComisPto: Double;
begin
  Ponto := Geral.IMV(EdPontoVda.Text);
  if Ponto = 0 then
  begin
    Application.MessageBox('Defina uma ponto de venda!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdPontoVda.SetFocus;
    Exit;
  end;
  ComisPto := Geral.DMV(EdComisPto.Text);
  if ComisPto = 0 then
  begin
    Application.MessageBox('Defina a comiss�o do ponto de venda!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdComisPto.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('PtosFatCad', 'Codigo', LaTipo.SQLType,
    QrPtosFatCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPtosFatCad, PainelEdit,
    'PtosFatCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPtosFatCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PtosFatCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PtosFatCad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PtosFatCad', 'Codigo');
end;

procedure TFmPtosFatCad.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmPtosFatCad.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  {
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeL.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  }
  //
  PainelEdit.Align  := alClient;
  PnGrids.Align  := alClient;
  CriaOForm;
  QrPtosCadPto.Open;
  QrConsultores.Open;
  QrGerentes.Open;
  QrCarteiras.Open;
  QrCNAB_Cfg.Open;
  // reabrir conforme ponto de venda
  //QrPediPrzCab.Open;
  //
end;

procedure TFmPtosFatCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPtosFatCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPtosFatCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPtosFatCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPtosFatCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPtosFatCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPtosFatCad.QrCfgCalcFields(DataSet: TDataSet);
begin
  if QrCfgAceiteTit.Value = 1 then
    QrCfgACEITETIT_TXT.Value := 'S'
  else
    QrCfgACEITETIT_TXT.Value := 'N';
end;

procedure TFmPtosFatCad.QrFaturaCalcFields(DataSet: TDataSet);
begin
  QrFaturaBLOQUETO.Value := QrFaturaDocumento.Value;
  QrFaturaCOMPENSADO_TXT.Value := dmkPF.FDT_NULO(QrFaturaCompensado.Value, 2);
  QrFaturaCREDITO.Value := QrFaturaValor.Value - QrPtosFatCadValorPto.Value;
end;

procedure TFmPtosFatCad.QrPtosFatCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPtosFatCad.QrPtosFatCadAfterScroll(DataSet: TDataSet);
begin
  ReopenPtosFatGru(0);
  ReopenPtosStqMov(0);
  ReopenFatura(0);
end;

procedure TFmPtosFatCad.FaturamentoporLeitura1Click(Sender: TObject);
begin
  if QrPtosFatCadCarteira.Value = 0 then
  begin
    Application.MessageBox('Faturamento cancelado. Defina a carteira!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if QrPtosFatCadPediPrzCab.Value = 0 then
  begin
    Application.MessageBox('Faturamento cancelado. Defina a condi��o de pagamento!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if QrPtosFatCadCNAB_Cfg.Value = 0 then
  begin
    Application.MessageBox('Faturamento cancelado. Defina a configura��o CNAB!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  FEstqPto := UCriar.RecriaTempTable('EstqPto', DmodG.QrUpdPID1, False);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO estqpto');
  DmodG.QrUpdPID1.SQL.Add('SELECT grs.CodUsu, prd.Codigo Grade, psm.Produto GraGruX, prd.Cor, prd.Tam,');
  DmodG.QrUpdPID1.SQL.Add('grs.Nome NO_GRADE, cor.Nome NO_Cor, tam.Nome NO_Tam,');
  DmodG.QrUpdPID1.SQL.Add('SUM(psm.Quanti) QtdeOld, SUM(psm.Quanti)  QtdeNew');
  DmodG.QrUpdPID1.SQL.Add('FROM lesew.ptosstqmov psm');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.produtos prd ON prd.Controle=psm.Produto');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.grades grs ON grs.Codigo=prd.Codigo');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.cores cor ON cor.Codigo=prd.Cor');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.tamanhos tam ON tam.Codigo=prd.Tam');
  DmodG.QrUpdPID1.SQL.Add('WHERE psm.Status=50');
  DmodG.QrUpdPID1.SQL.Add('AND psm.Empresa=:P0');
  DmodG.QrUpdPID1.SQL.Add('GROUP BY psm.Produto');
  DmodG.QrUpdPID1.SQL.Add('ORDER BY NO_GRADE, NO_Cor, NO_Tam');
  DmodG.QrUpdPID1.Params[0].AsInteger := QrPtosFatCadPontoVda.Value;
  DmodG.QrUpdPID1.ExecSQL;
  //
  if DBCheck.CriaFm(TFmPtosFatBalLei, FmPtosFatBalLei, afmoNegarComAviso) then
  begin
    FmPtosFatBalLei.ShowModal;
    FmPtosFatBalLei.Destroy;
    //
    LocCod(QrPtosFatCadCodigo.Value, QrPtosFatCadCodigo.Value);
  end;
end;

procedure TFmPtosFatCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosFatCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPtosFatCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PtosFatCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPtosFatCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPtosFatCad.frxBloqA2GetValue(const VarName: string;
  var Value: Variant);
var
  //LocalEData, UH,
  DVB: String;
  V, M, J, C, P: Double;
begin
  //else
  {
  if AnsiCompareText(VarName, 'PERIODODATE_PBB') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.PrimeiroDiaDoPeriodo_Date(
    FmCondGer.QrPrevPeriodo.Value - 1 + DCond.QrCondPBB.Value)) + ' a ' +
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.UltimoDiaDoPeriodo_Date(
    FmCondGer.QrPrevPeriodo.Value - 1 + DCond.QrCondPBB.Value)) else
  if AnsiCompareText(VarName, 'PERIODODATE_PSB') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.PrimeiroDiaDoPeriodo_Date(
    FmCondGer.QrPrevPeriodo.Value - 1 + DCond.QrCondPSB.Value)) + ' a ' +
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.UltimoDiaDoPeriodo_Date(
    FmCondGer.QrPrevPeriodo.Value - 1 + DCond.QrCondPSB.Value))
  else if AnsiCompareText(VarName, 'VARF_FIM_PREVISAO') = 0 then
  begin
    Value := FmCondGer.FCompensaAltura;
  end
  else if AnsiCompareText(VarName, 'VARF_MESANO') = 0 then
    Value := MLAGeral.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value)
  else if AnsiCompareText(VarName, 'VARF_MESANOA') = 0 then
    Value := MLAGeral.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value + 1)
  else if AnsiCompareText(VarName, 'VARF_PROXIMOPERIODO') = 0 then
    Value := MLAGeral.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value+1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_1') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(1, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_2') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(2, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_3') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(3, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_4') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(4, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_5') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(5, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_6') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(6, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_7') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(7, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_8') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(8, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_9') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(9, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_10') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(10, 0)

  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_1') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(1, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_2') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(2, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_3') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(3, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_4') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(4, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_5') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(5, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_6') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(6, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_7') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(7, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_8') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(8, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_9') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(9, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_10') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(10, 1)

  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_1') = 0 then
    Value := FmCondGer.QrPrevAviso01.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_2') = 0 then
    Value := FmCondGer.QrPrevAviso02.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_3') = 0 then
    Value := FmCondGer.QrPrevAviso03.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_4') = 0 then
    Value := FmCondGer.QrPrevAviso04.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_5') = 0 then
    Value := FmCondGer.QrPrevAviso05.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_6') = 0 then
    Value := FmCondGer.QrPrevAviso06.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_7') = 0 then
    Value := FmCondGer.QrPrevAviso07.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_8') = 0 then
    Value := FmCondGer.QrPrevAviso08.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_9') = 0 then
    Value := FmCondGer.QrPrevAviso09.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_10') = 0 then
    Value := FmCondGer.QrPrevAviso10.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_11') = 0 then
    Value := FmCondGer.QrPrevAviso11.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_12') = 0 then
    Value := FmCondGer.QrPrevAviso12.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_13') = 0 then
    Value := FmCondGer.QrPrevAviso13.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_14') = 0 then
    Value := FmCondGer.QrPrevAviso14.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_15') = 0 then
    Value := FmCondGer.QrPrevAviso15.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_16') = 0 then
    Value := FmCondGer.QrPrevAviso16.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_17') = 0 then
    Value := FmCondGer.QrPrevAviso17.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_18') = 0 then
    Value := FmCondGer.QrPrevAviso18.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_19') = 0 then
    Value := FmCondGer.QrPrevAviso19.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_20') = 0 then
    Value := FmCondGer.QrPrevAviso20.Value

  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoAnt') = 0 then
    Value := FmCondGer.FResumo_SaldoAnt
  else if AnsiCompareText(VarName, 'VARF_Resumo_Receitas') = 0 then
    Value := FmCondGer.FResumo_Receitas
  else if AnsiCompareText(VarName, 'VARF_Resumo_Despesas') = 0 then
    Value := FmCondGer.FResumo_Despesas
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoMes') = 0 then
    Value := FmCondGer.FResumo_SaldoMes
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoTrf') = 0 then
    Value := FmCondGer.FResumo_SaldoTrf
} //Parei aqui
  //else
  if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    Value := FNossoNumero;
  end else if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    Value := UBancos.CodigoDeBarra_BoletoDeCobranca(QrCfgCedBanco.Value,
      QrCfgCedAgencia.Value, QrCfgCorresBco.Value, QrCfgCorresAge.Value,
      QrCfgCedDAC_A.Value, QrCfgCedPosto.Value, QrCfgCedConta.Value,
      QrCfgCedDAC_C.Value, QrCfgCorresCto.Value, 9, 3, 1, FNossoNumero,
      QrCfgCodEmprBco.Value, QrCfgCartNum.Value, QrCfgCART_IMP.Value,
      QrCfgIDCobranca.Value, QrCfgOperCodi.Value, QrFaturaVencimento.Value,
      QrFaturaCREDITO.Value, 0, 0, not CkZerado.Checked, QrCfgTipoCart.Value,
      QrCfgLayoutRem.Value);
  end else if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if QrCfgDVB.Value <> '?' then DVB := QrCfgDVB.Value else
    DVB := UBancos.DigitoVerificadorCodigoBanco(QrCfgCedBanco.Value);
    Value := FormatFloat('000', QrCfgCedBanco.Value) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    Value := '';
    Value := UBancos.LinhaDigitavel_BoletoDeCobranca(
      UBancos.CodigoDeBarra_BoletoDeCobranca(QrCfgCedBanco.Value,
      QrCfgCedAgencia.Value, QrCfgCorresBco.Value, QrCfgCorresAge.Value,
      QrCfgCedDAC_A.Value, QrCfgCedPosto.Value, QrCfgCedConta.Value,
      QrCfgCedDAC_C.Value, QrCfgCorresCto.Value, 9, 3, 1, FNossoNumero,
      QrCfgCodEmprBco.Value, QrCfgCartNum.Value, QrCfgCART_IMP.Value,
      QrCfgIDCobranca.Value, QrCfgOperCodi.Value, QrFaturaVencimento.Value,
      QrFaturaCREDITO.Value, 0, 0, not CkZerado.Checked, QrCfgTipoCart.Value,
      QrCfgLayoutRem.Value));
  end
{  else if AnsiCompareText(VarName, 'VARF_INADIMP_U') = 0 then
  begin
    FmCondGer.QrPendU.Close;
    FmCondGer.QrPendU.Params[0].AsInteger := FmCondGer.QrPrevCondCli.Value;
    FmCondGer.QrPendU.Params[1].AsInteger := DCond.QrBoletosApto.Value;
    FmCondGer.QrPendU.Open;
    Value := -FmCondGer.QrPendUSALDO.Value;
  end else if AnsiCompareText(VarName, 'VARF_INADIMP_T') = 0 then
    Value := -FmCondGer.QrPendTSALDO.Value
  else if AnsiCompareText(VarName, 'VARF_MBB') = 0 then
    Value := QrCfgMBB.Value
  else if AnsiCompareText(VarName, 'VARF_MRB') = 0 then
    Value := QrCfgMRB.Value
  else if AnsiCompareText(VarName, 'VARF_MSB') = 0 then
    Value := QrCfgMSB.Value
  else if AnsiCompareText(VarName, 'VARF_MSP') = 0 then
    Value := QrCfgMSP.Value
  else if AnsiCompareText(VarName, 'VARF_MIB') = 0 then
    Value := QrCfgMIB.Value
  else if AnsiCompareText(VarName, 'VARF_MPB') = 0 then
    Value := QrCfgMPB.Value
  else if AnsiCompareText(VarName, 'VARF_MAB') = 0 then
    Value := QrCfgMAB.Value
  else if AnsiCompareText(VarName, 'VARF_URL') = 0 then
    Value := Dmod.QrControleWeb_MyURL.Value
  else if AnsiCompareText(VarName, 'PERIODODATE_PBB_H1') = 0 then
    Value := FmCondGer.FormataPeriodo(FmCondGer.QrPrevPeriodo.Value - 1 + QrCfgPBB.Value,
      DCond.QrConfigBolTitBPerFmt.Value)
  else if AnsiCompareText(VarName, 'PERIODODATE_PBB_H2') = 0 then
    Value := FmCondGer.FormataPeriodo(FmCondGer.QrPrevPeriodo.Value - 1 + QrCfgPBB.Value,
      DCond.QrConfigBolTitRPerFmt.Value)
  else if AnsiCompareText(VarName, 'PERIODODATE_PSB_H1') = 0 then
    Value := FmCondGer.FormataPeriodo(FmCondGer.QrPrevPeriodo.Value - 1 + QrCfgPBB.Value,
      DCond.QrConfigBolTitCPerFmt.Value)
  else if AnsiCompareText(VarName, 'MeuLogoBolExiste') = 0 then
    Value := FileExists(DCond.QrConfigBolMeuLogoArq.Value)
}
  else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
    Value := FmMyGlyfs.LogoBancoExiste(QrCfgCedBanco.Value)
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
    Value := FmMyGlyfs.CaminhoLogoBanco(QrCfgCedBanco.Value)
{

  // frxCondRx
  else if AnsiCompareText(VarName, 'VAR_ENOMEDONO') = 0 then
    Value := DmodG.QrEnderecoNOME_ENT.Value
  else if AnsiCompareText(VarName, 'VAR_ECNPJ') = 0 then
    Value := DmodG.QrEnderecoCNPJ_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_ENOMERUA') = 0 then
    Value := DmodG.QrEnderecoNOMELOGRAD.Value + ' ' + DmodG.QrEnderecoRUA.Value
  else if AnsiCompareText(VarName, 'VAR_ENUMERO') = 0 then
    Value := 'N� ' + DmodG.QrEnderecoNUMERO_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_ECOMPL') = 0 then
    Value := 'Compl.: ' + DmodG.QrEnderecoCOMPL.Value
  else if AnsiCompareText(VarName, 'VAR_EBAIRRO') = 0 then
    Value := 'Bairro: ' + DmodG.QrEnderecoBAIRRO.Value
  else if AnsiCompareText(VarName, 'VAR_ECIDADE') = 0 then
    Value := 'Cidade: ' + DmodG.QrEnderecoCIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_EUF') = 0 then
    Value := 'UF: ' + DmodG.QrEnderecoNOMEUF.Value
  else if AnsiCompareText(VarName, 'VAR_ECEP') = 0 then
    Value := 'CEP: ' + Geral.FormataCEP_NT(DmodG.QrEnderecoCEP.Value)
  else if AnsiCompareText(VarName, 'VAR_EPAIS') = 0 then
    Value := 'Pa�s: ' + DmodG.QrEnderecoPais.Value
  else if AnsiCompareText(VarName, 'VAR_ETEL') = 0 then
    Value := 'Tel.: ' + DmodG.QrEnderecoTE1_TXT.Value
  //
  else if AnsiCompareText(VarName, 'VAR_EXTENSO') = 0 then
    Value := dmkPF.ExtensoMoney(Geral.FFT(
      DCond.QrBoletosSUB_TOT.Value, 2, siPositivo))
  else if AnsiCompareText(VarName, 'VAR_REFERENTE') = 0 then
  begin
    if Trim(DCond.QrInquilinoPrefixoUH.Value) <>  '' then UH :=
      DCond.QrInquilinoPrefixoUH.Value;
    if Trim(DCond.QrInquilinoUnidade.Value) <>  '' then UH :=
      UH + ' ' + DCond.QrInquilinoUnidade.Value;
    Value := UpperCase('Pagamento da quota condominial - ' +
      FormatDateTime('mmm"/"yy', DCond.QrBoletosVencto.Value) +
      ' - ' + UH + ' - Com vencimento em ' +
      FormatDateTime('dd" de "mmmm" de "yyyy', DCond.QrBoletosVencto.Value)
      + '.')
  end
  else if AnsiCompareText(VarName, 'VAR_BNOME') = 0 then
    Value := DmodG.QrEndereco2NOME_ENT.Value
  else if AnsiCompareText(VarName, 'VAR_BCNPJ') = 0 then
    Value := DmodG.QrEndereco2CNPJ_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_BNOMERUA') = 0 then
    Value := DmodG.QrEndereco2NOMELOGRAD.Value + ' ' + DmodG.QrEndereco2RUA.Value
  else if AnsiCompareText(VarName, 'VAR_BNUMERO') = 0 then
    Value := 'N� ' + DmodG.QrEndereco2NUMERO_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_BCOMPL') = 0 then
    Value := 'Compl.: ' + DmodG.QrEndereco2COMPL.Value
  else if AnsiCompareText(VarName, 'VAR_BBAIRRO') = 0 then
    Value := 'Bairro: ' + DmodG.QrEndereco2BAIRRO.Value
  else if AnsiCompareText(VarName, 'VAR_BCIDADE') = 0 then
    Value := 'Cidade: ' + DmodG.QrEndereco2CIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_BUF') = 0 then
    Value := 'UF: ' + DmodG.QrEndereco2NOMEUF.Value
  else if AnsiCompareText(VarName, 'VAR_BCEP') = 0 then
    Value := 'CEP: ' + Geral.FormataCEP_NT(DmodG.QrEndereco2CEP.Value)
  else if AnsiCompareText(VarName, 'VAR_BPAIS') = 0 then
    Value := 'Pa�s: ' + DmodG.QrEndereco2Pais.Value
  else if AnsiCompareText(VarName, 'VAR_BTEL') = 0 then
    Value := 'Tel.: ' + DmodG.QrEndereco2TE1_TXT.Value
  //
  else if AnsiCompareText(VarName, 'VAR_LOCALDATA') = 0 then
  begin
    LocalEData := MLAGeral.Maiusculas(
      FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Now),
      MLAGeral.EhMinusculas(DmodG.QrEndereco2CIDADE.Value, False));
    if DmodG.QrEndereco2CIDADE.Value <> '' then LocalEData :=
      DmodG.QrEndereco2CIDADE.Value + ', ' + LocalEData;
    //
    Value := LocalEData;
  end
  }
  // Parei aqui
  else
  begin
    V := QrFaturaValor.Value;
    M := QrFaturaMulta.Value;
    J := QrFaturaMoraDia.Value;
    C := QrFaturaCREDITO.Value;
    P := QrPtosFatCadComisPto.Value;
    if AnsiCompareText(VarName, 'VAR_INSTRUCAO_01') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto01.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_02') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto02.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_03') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto03.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_04') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto04.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_05') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto05.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_06') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto06.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_07') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto07.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_08') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto08.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_09') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto09.Value, V, M, J, C, P)
    else if AnsiCompareText(VarName, 'VAR_INSTRUCAO_10') = 0 then
      Value := DmBco.TraduzInstrucaoBloqueto(QrCfgTexto10.Value, V, M, J, C, P);
  end
end;

function TFmPtosFatCad.frxBloqA2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  {
  if MethodName = 'VARF_MBB' then
    Params := QrCfgMBB.Value else
  if MethodName = 'VARF_MRB' then
    Params := QrCfgMRB.Value else
  if MethodName = 'VARF_MSB' then
    Params := QrCfgMSB.Value else
  if MethodName = 'VARF_MSP' then
    Params := QrCfgMSP.Value else
  if MethodName = 'VARF_MIB' then
    Params := QrCfgMIB.Value else
  if MethodName = 'VARF_MPB' then
    Params := QrCfgMPB.Value else
  if MethodName = 'VARF_MAB' then
    Params := QrCfgMAB.Value
  }
end;

procedure TFmPtosFatCad.frxEstqPtoGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_NOMEPONTO' then
    Value := QrPtosFatCadNO_PontoVda.Value;
end;

procedure TFmPtosFatCad.Imprimeestoque1Click(Sender: TObject);
begin
  QrEstqPto.Close;
  QrEstqPto.Params[0].AsInteger := QrPtosFatCadPontoVda.Value;
  QrEstqPto.Open;
  //
  MyObjects.frxMostra(frxEstqPto, 'Estoque de ponto');
end;

procedure TFmPtosFatCad.Incluiitenspeloestoque1Click(Sender: TObject);
begin
  if QrPtosFatCadCarteira.Value = 0 then
  begin
    Application.MessageBox('Faturamento cancelado. Defina a carteira!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if QrPtosFatCadPediPrzCab.Value = 0 then
  begin
    Application.MessageBox('Faturamento cancelado. Defina a condi��o de pagamento!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if QrPtosFatCadCNAB_Cfg.Value = 0 then
  begin
    Application.MessageBox('Faturamento cancelado. Defina a configura��o CNAB!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  FEstqPto := UCriar.RecriaTempTable('EstqPto', DmodG.QrUpdPID1, False);
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO estqpto');
  DmodG.QrUpdPID1.SQL.Add('SELECT grs.CodUsu, prd.Codigo Grade, psm.Produto GraGruX, prd.Cor, prd.Tam,');
  DmodG.QrUpdPID1.SQL.Add('grs.Nome NO_GRADE, cor.Nome NO_Cor, tam.Nome NO_Tam,');
  DmodG.QrUpdPID1.SQL.Add('SUM(psm.Quanti) QtdeOld, SUM(psm.Quanti)  QtdeNew');
  DmodG.QrUpdPID1.SQL.Add('FROM lesew.ptosstqmov psm');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.produtos prd ON prd.Controle=psm.Produto');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.grades grs ON grs.Codigo=prd.Codigo');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.cores cor ON cor.Codigo=prd.Cor');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN lesew.tamanhos tam ON tam.Codigo=prd.Tam');
  DmodG.QrUpdPID1.SQL.Add('WHERE psm.Status=50');
  DmodG.QrUpdPID1.SQL.Add('AND psm.Empresa=:P0');
  DmodG.QrUpdPID1.SQL.Add('GROUP BY psm.Produto');
  DmodG.QrUpdPID1.SQL.Add('ORDER BY NO_GRADE, NO_Cor, NO_Tam');
  DmodG.QrUpdPID1.Params[0].AsInteger := QrPtosFatCadPontoVda.Value;
  DmodG.QrUpdPID1.ExecSQL;
  //
  if DBCheck.CriaFm(TFmPtosFatBal, FmPtosFatBal, afmoNegarComAviso) then
  begin
    FmPtosFatBal.ShowModal;
    FmPtosFatBal.Destroy;
    //
    LocCod(QrPtosFatCadCodigo.Value, QrPtosFatCadCodigo.Value);
  end;
end;

procedure TFmPtosFatCad.Incluinovafatura1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPtosFatCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'ptosfatcad');
  Agora := DModG.ObtemAgora();
  EdAbertura.ValueVariant :=  Agora;
  EdPontoVda.Enabled := True;
  EdPontoVda.Enabled := True;
end;

procedure TFmPtosFatCad.QrPtosFatCadBeforeClose(DataSet: TDataSet);
begin
  QrPtosFatGru.Close;
  QrPtosStqMov.Close;
  QrFatura.Close;
end;

procedure TFmPtosFatCad.QrPtosFatCadBeforeOpen(DataSet: TDataSet);
begin
  QrPtosFatCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPtosFatCad.QrPtosFatCadCalcFields(DataSet: TDataSet);
begin
{
  QrPtosFatCadDATAPED_TXT.Value := Geral.FDT(QrPtosFatCadDataPed.Value, 0);
  QrPtosFatCadDATALIB_TXT.Value := Geral.FDT(QrPtosFatCadDataLib.Value, 0);
  QrPtosFatCadDATAENV_TXT.Value := Geral.FDT(QrPtosFatCadDataEnv.Value, 0);
  QrPtosFatCadDATAREC_TXT.Value := Geral.FDT(QrPtosFatCadDataRec.Value, 0);
  //
  QrPtosFatCadNO_Status.Value :=
    DmProd.StatusPedido_Txt(QrPtosFatCadStatus.Value);
}
end;

procedure TFmPtosFatCad.QrPtosFatGruAfterScroll(DataSet: TDataSet);
begin
  ReopenPtosFatIts();
end;

procedure TFmPtosFatCad.QrPtosFatGruBeforeClose(DataSet: TDataSet);
begin
  QrPtosFatIts.Close;
end;

procedure TFmPtosFatCad.ReopenFatura(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatura, Dmod.MyDB, [
    'SELECT lan.Controle, lan.Data, lan.Vencimento, lan.Documento,',
    'lan.Duplicata, lan.Compensado, lan.Credito Valor, lan.MoraDia,',
    'lan.Multa, lan.Vendedor, lan.Account',
    'FROM ' + FTabLctALS + ' lan',
    'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0801),
    'AND lan.FatNum=' + Geral.FF0(QrPtosFatCadCodigo.Value),
    'ORDER BY lan.Vencimento, lan.FatParcela ',
    '']);
  //
  if Controle <> 0 then
    QrFatura.Locate('Controle', Controle, []);
end;

procedure TFmPtosFatCad.ReopenPediPrzCab;
var
  CodUsu, Empresa: Integer;
begin
  Empresa := Geral.IMV(EdPontoVda.Text);
  QrPediPrzCab.Close;
  if (Empresa <> 0) or
  ( (QrPtosCadPtoCodUsu.Value = 0) and (QrPtosCadPtoCodigo.Value <> 0)) then
  begin
    Empresa := QrPtosCadPtoCodigo.Value;
    QrPediPrzCab.Params[0].AsInteger := Empresa;
    QrPediPrzCab.Open;
    //
    CodUsu := Geral.IMV(EdPediPrzCab.Text);
    if not QrPediPrzCab.Locate('CodUsu', CodUsu, []) then
    begin
      EdPediPrzCab.ValueVariant := 0;
      CBPediPrzCab.KeyValue     := 0;
    end;
  end;
end;

procedure TFmPtosFatCad.ReopenPtosFatGru(_: Integer);
begin
  QrPtosFatGru.Close;
  QrPtosFatGru.Params[0].AsInteger := QrPtosFatCadCodigo.Value;
  QrPtosFatGru.Open;
end;

procedure TFmPtosFatCad.ReopenPtosFatIts();
begin
  QrPtosFatIts.Close;
  QrPtosFatIts.Params[00].AsInteger := QrPtosFatCadCodigo.Value;
  QrPtosFatIts.Params[01].AsInteger := QrPtosFatGruCodigo.Value;
  QrPtosFatIts.Open;
  //
end;

procedure TFmPtosFatCad.ReopenPtosStqMov(IDCtrl: Integer);
begin
  QrPtosStqMov.Close;
  QrPtosStqMov.Params[00].AsInteger := QrPtosFatCadCodigo.Value;
  QrPtosStqMov.Open;
  //
  if IDCtrl <> 0 then
    QrPtosStqMov.Locate('IDCtrl', IDCtrl, []);
end;

end.

