unit CiclosProm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB, mySQLDbTables, Grids,
  DBGrids, dmkGeral, Menus, MyDBCheck, UMySQLModule, frxClass, frxDBSet,
  Variants, WinSkinStore, WinSkinData, frxCross, dmkPermissoes,
  UnInternalConsts, DmkDAC_PF, DateUtils, UnDmkEnums;

type
  TFmCiclosProm = class(TForm)
    PainelConfirma: TPanel;
    BtDiligencia: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    EdPromotor: TdmkEditCB;
    CBPromotor: TdmkDBLookupComboBox;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    Label3: TLabel;
    QrProm: TmySQLQuery;
    DsProm: TDataSource;
    QrPromCodigo: TIntegerField;
    QrPromNOMEPROM: TWideStringField;
    GradeDiligencias: TDBGrid;
    QrCiclosAula: TmySQLQuery;
    DsCiclosAula: TDataSource;
    QrCiclosAulaCodigo: TIntegerField;
    QrCiclosAulaControle: TIntegerField;
    QrCiclosAulaData: TDateField;
    QrCiclosAulaAlunosInsc: TIntegerField;
    QrCiclosAulaAlunosAula: TIntegerField;
    QrCiclosAulaCidade: TWideStringField;
    QrCiclosAulaUF: TWideStringField;
    QrCiclosAulaLocalEnd: TWideStringField;
    QrCiclosAulaLocalVal: TFloatField;
    QrCiclosAulaHotelNome: TWideStringField;
    QrCiclosAulaHotelCont: TWideStringField;
    QrCiclosAulaHotelTele: TWideStringField;
    QrCiclosAulaHotelVal: TFloatField;
    QrCiclosAulaOnibusNome: TWideStringField;
    QrCiclosAulaOnibusHora: TWideStringField;
    QrCiclosAulaObserva: TWideStringField;
    QrCiclosAulaLk: TIntegerField;
    QrCiclosAulaDataCad: TDateField;
    QrCiclosAulaDataAlt: TDateField;
    QrCiclosAulaUserCad: TIntegerField;
    QrCiclosAulaUserAlt: TIntegerField;
    QrCiclosAulaAlterWeb: TSmallintField;
    QrCiclosAulaAtivo: TSmallintField;
    QrCiclosAulaLocalNom: TWideStringField;
    QrCiclosAulaLocalPes: TWideStringField;
    QrCiclosAulaLocalTel: TWideStringField;
    QrCiclosAulaValorUni: TFloatField;
    QrCiclosAulaValorTot: TFloatField;
    QrCiclosAulaComPromPer: TFloatField;
    QrCiclosAulaComPromVal: TFloatField;
    QrCiclosAulaComPromMin: TFloatField;
    QrCiclosAulaComProfPer: TFloatField;
    QrCiclosAulaComProfVal: TFloatField;
    QrCiclosAulaComProfMin: TFloatField;
    QrCiclosAulaDTSPromPer: TFloatField;
    QrCiclosAulaDTSPromVal: TFloatField;
    QrCiclosAulaDTSPromMin: TFloatField;
    QrCiclosAulaDTSProfPer: TFloatField;
    QrCiclosAulaDTSProfVal: TFloatField;
    QrCiclosAulaDTSProfMin: TFloatField;
    QrCiclosAulaPromotor: TIntegerField;
    DBGDespProm: TDBGrid;
    QrDespProm: TmySQLQuery;
    QrDespPromNOMECONTA: TWideStringField;
    QrDespPromNOMECARTEIRA: TWideStringField;
    QrDespPromSEQ: TIntegerField;
    QrDespPromKGT: TIntegerField;
    QrDespPromData: TDateField;
    QrDespPromTipo: TSmallintField;
    QrDespPromCarteira: TIntegerField;
    QrDespPromControle: TIntegerField;
    QrDespPromSub: TSmallintField;
    QrDespPromAutorizacao: TIntegerField;
    QrDespPromGenero: TIntegerField;
    QrDespPromQtde: TFloatField;
    QrDespPromDescricao: TWideStringField;
    QrDespPromNotaFiscal: TIntegerField;
    QrDespPromDebito: TFloatField;
    QrDespPromCredito: TFloatField;
    QrDespPromCompensado: TDateField;
    QrDespPromSerieCH: TWideStringField;
    QrDespPromDocumento: TFloatField;
    QrDespPromSit: TIntegerField;
    QrDespPromVencimento: TDateField;
    QrDespPromFatID: TIntegerField;
    QrDespPromFatID_Sub: TIntegerField;
    QrDespPromFatNum: TFloatField;
    QrDespPromFatParcela: TIntegerField;
    QrDespPromID_Pgto: TIntegerField;
    QrDespPromID_Quit: TIntegerField;
    QrDespPromID_Sub: TSmallintField;
    QrDespPromFatura: TWideStringField;
    QrDespPromEmitente: TWideStringField;
    QrDespPromBanco: TIntegerField;
    QrDespPromContaCorrente: TWideStringField;
    QrDespPromCNPJCPF: TWideStringField;
    QrDespPromLocal: TIntegerField;
    QrDespPromCartao: TIntegerField;
    QrDespPromLinha: TIntegerField;
    QrDespPromOperCount: TIntegerField;
    QrDespPromLancto: TIntegerField;
    QrDespPromPago: TFloatField;
    QrDespPromMez: TIntegerField;
    QrDespPromFornecedor: TIntegerField;
    QrDespPromCliente: TIntegerField;
    QrDespPromCliInt: TIntegerField;
    QrDespPromForneceI: TIntegerField;
    QrDespPromMoraDia: TFloatField;
    QrDespPromMulta: TFloatField;
    QrDespPromMoraVal: TFloatField;
    QrDespPromMultaVal: TFloatField;
    QrDespPromProtesto: TDateField;
    QrDespPromDataDoc: TDateField;
    QrDespPromCtrlIni: TIntegerField;
    QrDespPromNivel: TIntegerField;
    QrDespPromVendedor: TIntegerField;
    QrDespPromAccount: TIntegerField;
    QrDespPromICMS_P: TFloatField;
    QrDespPromICMS_V: TFloatField;
    QrDespPromDuplicata: TWideStringField;
    QrDespPromDepto: TIntegerField;
    QrDespPromDescoPor: TIntegerField;
    QrDespPromDescoVal: TFloatField;
    QrDespPromDescoControle: TIntegerField;
    QrDespPromUnidade: TIntegerField;
    QrDespPromNFVal: TFloatField;
    QrDespPromAntigo: TWideStringField;
    QrDespPromExcelGru: TIntegerField;
    QrDespPromDoc2: TWideStringField;
    QrDespPromCNAB_Sit: TSmallintField;
    QrDespPromTipoCH: TSmallintField;
    QrDespPromReparcel: TIntegerField;
    QrDespPromAtrelado: TIntegerField;
    QrDespPromPagMul: TFloatField;
    QrDespPromPagJur: TFloatField;
    QrDespPromSubPgto1: TIntegerField;
    QrDespPromMultiPgto: TIntegerField;
    QrDespPromLk: TIntegerField;
    QrDespPromDataCad: TDateField;
    QrDespPromDataAlt: TDateField;
    QrDespPromUserCad: TIntegerField;
    QrDespPromUserAlt: TIntegerField;
    QrDespPromAlterWeb: TSmallintField;
    QrDespPromAtivo: TSmallintField;
    DsDespProm: TDataSource;
    PMDiligencia: TPopupMenu;
    Incluinovadiligncia1: TMenuItem;
    Alteradilignciaatual1: TMenuItem;
    Excluidilignciaatual1: TMenuItem;
    BtLanctos: TBitBtn;
    PMLanctos: TPopupMenu;
    Incluilanamento1: TMenuItem;
    Alteralanamento1: TMenuItem;
    Excluilanamento1: TMenuItem;
    QrPromCartPref: TIntegerField;
    QrCiclosAulaDTSPromDeb: TFloatField;
    QrCiclosAulaComPromDeb: TFloatField;
    QrCiclosAulaDTSProfDeb: TFloatField;
    QrCiclosAulaComProfDeb: TFloatField;
    Splitter1: TSplitter;
    BtLocCiclo: TBitBtn;
    frxDsCiclosAula: TfrxDBDataset;
    frxMovPeriodo: TfrxReport;
    frxDsDespProm: TfrxDBDataset;
    PainelPesquisa: TPanel;
    SbImprime: TBitBtn;
    QrCiclosAulaPRESENCA: TWideStringField;
    QrAnt: TmySQLQuery;
    QrAntDTS: TFloatField;
    QrAntCom: TFloatField;
    frxDsAnt: TfrxDBDataset;
    QrCiclosAulaBonPromVal: TFloatField;
    QrCiclosAulaBonPromWho: TIntegerField;
    Label4: TLabel;
    EdMesAnoI: TdmkEdit;
    QrHistAnu: TmySQLQuery;
    QrHistAnuAlunosInsc: TFloatField;
    QrHistAnuAlunosAula: TFloatField;
    GradeHA: TStringGrid;
    frxDsGrafData: TfrxDBDataset;
    QrGrafData: TmySQLQuery;
    QrGrafDataLinha: TIntegerField;
    QrGrafDataTexto: TWideStringField;
    QrGrafDataVal01: TFloatField;
    QrGrafDataVal02: TFloatField;
    QrGrafDataVal03: TFloatField;
    QrGrafDataAtivo: TSmallintField;
    RadioGroup1: TRadioGroup;
    QrGrafVal1: TmySQLQuery;
    QrGrafVal2: TmySQLQuery;
    QrGrafVal1Linha: TIntegerField;
    QrGrafVal1Texto: TWideStringField;
    QrGrafVal1Val01: TFloatField;
    QrGrafVal1Val02: TFloatField;
    QrGrafVal1Val03: TFloatField;
    QrGrafVal1Ativo: TSmallintField;
    QrGrafVal2Linha: TIntegerField;
    QrGrafVal2Texto: TWideStringField;
    QrGrafVal2Val01: TFloatField;
    QrGrafVal2Val02: TFloatField;
    QrGrafVal2Val03: TFloatField;
    QrGrafVal2Ativo: TSmallintField;
    frxDsGrafVal1: TfrxDBDataset;
    frxDsGrafVal2: TfrxDBDataset;
    dmkPermissoes1: TdmkPermissoes;
    QrHistAnuMes: TFloatField;
    QrCiclosAulaCIDADE_TXT: TWideStringField;
    QrCiclosAulaMUNI: TWideStringField;
    BtComiss: TBitBtn;
    QrDespPromAgencia: TIntegerField;
    QrGrafDataTextoInt: TIntegerField;
    QrHistAnuData: TDateField;
    QrCiclosAulaCodiCidade: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPromotorChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure TPDataFClick(Sender: TObject);
    procedure BtDiligenciaClick(Sender: TObject);
    procedure QrCiclosAulaAfterScroll(DataSet: TDataSet);
    procedure Incluinovadiligncia1Click(Sender: TObject);
    procedure Alteradilignciaatual1Click(Sender: TObject);
    procedure Excluidilignciaatual1Click(Sender: TObject);
    procedure BtLanctosClick(Sender: TObject);
    procedure Incluilanamento1Click(Sender: TObject);
    procedure Alteralanamento1Click(Sender: TObject);
    procedure QrDespPromCalcFields(DataSet: TDataSet);
    procedure QrCiclosAulaBeforeClose(DataSet: TDataSet);
    procedure BtLocCicloClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxMovPeriodoGetValue(const VarName: string; var Value: Variant);
    procedure QrCiclosAulaCalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMesAnoIExit(Sender: TObject);
    procedure GradeHADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure Excluilanamento1Click(Sender: TObject);
    procedure BtComissClick(Sender: TObject);
    procedure TPDataIExit(Sender: TObject);
    procedure TPDataFExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PMDiligenciaPopup(Sender: TObject);
    procedure PMLanctosPopup(Sender: TObject);
  private
    { Private declarations }
    FPromotor, FPromAtu, FMesAnoIAtu: Integer;
    FDataI, FDataF: TDate;
    procedure AtualizaGradeHA();
    procedure ReabreHistoricoAnual();
    procedure ReopenDespProm(Controle: Integer);
  public
    { Public declarations }
    FTabLctALS: String;
    procedure ReopenCiclosAula(Controle: Integer);
  end;

  var
  FmCiclosProm: TFmCiclosProm;

implementation

{$R *.DFM}

uses Module, CiclosPro_Edit, UnFinanceiro, LctEdit, ModuleCiclos, ModuleGeral,
  Principal, UCreate, PromComiss, ModuleFin, UnDmkProcFunc, UnMyObjects;

procedure TFmCiclosProm.Alteradilignciaatual1Click(Sender: TObject);
begin
  if QrCiclosAula.RecordCount = 0 then
    Geral.MB_Aviso('N�o h� dilig�ncia selecionada!')
  else
    UMyMod.FormInsUpd_Show(TFmCiclosPro_Edit, FmCiclosPro_Edit, afmoNegarComAviso,
      QrCiclosAula, stUpd);
end;

procedure TFmCiclosProm.BtLanctosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLanctos, BtLanctos);
end;

procedure TFmCiclosProm.BtLocCicloClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  Codigo   := QrCiclosAulaCodigo.Value;
  Controle := QrCiclosAulaControle.Value;
  //
  if (Codigo <> 0) and (Controle <> 0) then
  begin
    FmPrincipal.GerenciaCiclos2(Codigo, Controle);
    //
    ReopenCiclosAula(Controle);
  end;
end;

procedure TFmCiclosProm.Alteralanamento1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  if QrCiclosAula.RecordCount = 0 then
    Geral.MB_Aviso('N�o h� dilig�ncia selecionada para fazer lan�amentos para o promotor!')
  else begin
    CtrlAula := QrCiclosAulaControle.Value;
{
//N�o existe mais
    if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
    afmoNegarComAviso, QrDespProm, (*FmPrincipal.QrCarteiras*) nil,
    tgrAltera, QrDespPromControle.Value, QrDespPromSub.Value,
    0(*Genero*), 0(*Juros*), 0(*Multa*), nil, 703, CtrlAula,
    -1(*FatNum*), QrPromCartPref.Value, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
    0(*ForneceI*), QrPromCodigo.Value(*Account*), 0(*Vendedor*),
    True(*LockCliInt*), False(*LockForneceI*), True(*LockAccount*),
    False(*LockVendedor*), QrCiclosAulaData.Value, 0, 0, 3, 0) > 0 then
}
    UFinanceiro.AlteracaoLancamento(nil, QrDespProm, lfCicloCurso,
      0(*OneAccount*), QrDespPromForneceI.Value(*OneCliInt*), FTabLctALS, True);
    //
    DmCiclos.CalculaDespPromTurma(CtrlAula, FTabLctALS);
    ReopenCiclosAula(QrCiclosAulaControle.Value);
    QrDespProm.Locate('Controle', FLAN_CONTROLE, [])
  end;
end;

procedure TFmCiclosProm.BtComissClick(Sender: TObject);
var
  Promotor: Integer;
begin
  Promotor := EdPromotor.ValueVariant;
  if DBCheck.CriaFm(TFmPromComiss, FmPromComiss, afmoNegarComAviso) then
  begin
    if Promotor > 0 then
    begin
      FmPromComiss.EdPromotor.ValueVariant := Promotor;
      FmPromComiss.CBPromotor.KeyValue     := Promotor;
    end;
    FmPromComiss.ShowModal;
    FmPromComiss.Destroy;
  end;
end;

procedure TFmCiclosProm.BtDiligenciaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDiligencia, BtDiligencia);
end;

procedure TFmCiclosProm.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosProm.EdMesAnoIExit(Sender: TObject);
begin
  AtualizaGradeHA;
end;

procedure TFmCiclosProm.EdPromotorChange(Sender: TObject);
begin
  FPromotor := EdPromotor.ValueVariant;
  //
  if FTabLctALS <> '' then
  begin
    ReopenCiclosAula(QrCiclosAulaControle.Value);
    AtualizaGradeHA;
  end;
end;

procedure TFmCiclosProm.Excluidilignciaatual1Click(Sender: TObject);
begin
  if QrDespProm.RecordCount > 0 then
    Geral.MB_Aviso('Exclus�o de dilig�ncia cancelada! Existem despesas atreladas a ela!')
  else
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCiclosAula, GradeDiligencias,
      'ciclosaula', ['Controle'], ['Controle'], istAtual, '');
end;

procedure TFmCiclosProm.Excluilanamento1Click(Sender: TObject);
begin
  UFinanceiro.ExcluiLct_Unico(FTabLctALS, Dmod.MyDB, QrDespPromData.Value,
    QrDespPromTipo.Value, QrDespPromCarteira.Value, QrDespPromControle.Value,
    QrDespPromSub.Value, dmkPF.MotivDel_ValidaCodigo(300), True);
  //
  ReopenDespProm(0);
end;

procedure TFmCiclosProm.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclosProm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := MLAGeral.FmIniConfigPath(Name);
  Geral.WriteAppKeyCU('DataI', Cam, Int(TPDataI.Date), ktDate);
  Geral.WriteAppKeyCU('DataF', Cam, Int(TPDataF.Date), ktDate);
  Geral.WriteAppKeyCU('Promotor', Cam, Geral.IMV(EdPromotor.Text), ktInteger);
  Geral.WriteAppKeyCU('MesAnoI', Cam, EdMesAnoI.Text, ktString);
  //Geral.WriteAppKeyCU('MesAnoF', Cam, EdMesAnoF.Text, ktString);
end;

procedure TFmCiclosProm.FormCreate(Sender: TObject);
var
  Cam, MesAnoI, MesAnoF: String;
  Promotor: Integer;
begin
  // fica saindo � toa da lista de datasets quando n�o abro o ModuleGeral na IDE
  // N�o usa mais
  //frxMovPeriodo.DataSets.Add(DModG.frxDsEndereco);
  //
  Cam := MLAGeral.FmIniConfigPath(Name);
  //
  Promotor := Geral.ReadAppKeyCU('Promotor', Cam, ktInteger, 0);
  EdPromotor.ValueVariant := Promotor;
  CBPromotor.KeyValue     := Promotor;
  //
  TPDataI.Date := Geral.ReadAppKeyCU('DataI', Cam, ktDate, Int(Date-30));
  TPDataF.Date := Geral.ReadAppKeyCU('DataF', Cam, ktDate, Int(Date));
  //
  MesAnoI := MLAGeral.MesEAnoDaData(MLAGeral.PrimeiroDiaDoAno(Date));
  MesAnoF := MLAGeral.MesEAnoDaData(MLAGeral.UltimoDiaDoAno(Date));
  EdMesAnoI.Text := Geral.ReadAppKeyCU('MesAnoI', Cam, ktString, MesAnoI);
  //EdMesAnoF.Text := Geral.ReadAppKeyCU('MesAnoF', Cam, ktString, MesAnoF);
  //
  QrProm.Open;
  //
  GradeHA.ColWidths[00] := 100;
  GradeHA.Cells[00,01] := 'Inscritos';
  GradeHA.Cells[00,02] := 'Presentes';
  GradeHA.Cells[00,03] := 'Presen�a';
  //
  GradeHA.Cells[13,00] := 'TOTAL';
  AtualizaGradeHA;
end;

procedure TFmCiclosProm.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosProm.FormShow(Sender: TObject);
begin
  ReopenCiclosAula(QrCiclosAulaControle.Value);
end;

procedure TFmCiclosProm.frxMovPeriodoGetValue(const VarName: string;
  var Value: Variant);
var
  Linha1, Linha2, Linha3: String;
begin
  if VarName = 'VARF_ENDERECO' then
    Value := DmodG.ObtemEnderecoEtiqueta3Linhas(QrPromCodigo.Value, Linha1, Linha2, Linha3)
  else if VarName = 'VARF_PROMOTOR' then
    Value := DmodG.QrEnderecoNOME_ENT.Value
  else if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(TPDataI.Date, TPDataF.Date, True, True,
    ''{TextoI}, ''{Agregador}, ''{TextoF})
  else if Copy(VarName, 1, 4) = 'MES_' then
    Value := GradeHA.Cells[Geral.IMV(Copy(VarName, 5, 2)), 00]
  else if Copy(VarName, 1, 4) = 'INS_' then
    Value := GradeHA.Cells[Geral.IMV(Copy(VarName, 5, 2)), 01]
  else if Copy(VarName, 1, 4) = 'ALU_' then
    Value := GradeHA.Cells[Geral.IMV(Copy(VarName, 5, 2)), 02]
  else if Copy(VarName, 1, 4) = 'PER_' then
    Value := GradeHA.Cells[Geral.IMV(Copy(VarName, 5, 2)), 03]
end;

procedure TFmCiclosProm.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    for i := 1 to 2 do
      for j := 1 to 2 do
        Cross.AddValue([i], [j], [GradeHA.Cells[i - 1, j - 1]]);
  end;
end;

procedure TFmCiclosProm.GradeHADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol = 0) or (ARow = 0) then
  begin
  end else
    with GradeHA.Canvas do
    begin
      if ACol in ([1,2,3,4,5,6,7,8,9,10,11,12,13]) then
        MyObjects.DesenhaTextoEmStringGrid(GradeHA, Rect, clBlack,
          Panel3.Color, taRightJustify,
          GradeHA.Cells[Acol, ARow], 1, 1, False)
      else
      if ACol in ([0]) then
        MyObjects.DesenhaTextoEmStringGrid(GradeHA, Rect, clBlack,
          Panel3.Color, taLeftJustify,
          GradeHA.Cells[Acol, ARow], 1, 1, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeHA, Rect, clBlack,
          Panel3.Color, taCenter,
          GradeHA.Cells[Acol, ARow], 1, 1, False);
    end;
end;

procedure TFmCiclosProm.Incluilanamento1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  if QrCiclosAula.RecordCount = 0 then
    Geral.MB_Aviso('N�o h� dilig�ncia selecionada para fazer lan�amentos para o promotor!')
  else
  begin
    CtrlAula := QrCiclosAulaControle.Value;
    //
    if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
      afmoNegarComAviso, QrDespProm, (*FmPrincipal.QrCarteiras*) nil,
      tgrInclui, QrDespPromControle.Value, QrDespPromSub.Value,
      0(*Genero*), 0(*Juros*), 0(*Multa*), nil, 703, CtrlAula,
      -1(*FatNum*), QrPromCartPref.Value, 0, 0, True,
      0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
      0(*ForneceI*), QrPromCodigo.Value(*Account*), 0(*Vendedor*),
      True(*LockCliInt*), False(*LockForneceI*), True(*LockAccount*),
      False(*LockVendedor*), QrCiclosAulaData.Value, 0, 0, 3, 0,
      FTabLctALS, 0, 0) > 0 then
    begin
      DmCiclos.CalculaDespPromTurma(CtrlAula, FTabLctALS);
      ReopenCiclosAula(QrCiclosAulaControle.Value);
      QrDespProm.Locate('Controle', FLAN_CONTROLE, [])
    end;
  end;
end;

procedure TFmCiclosProm.Incluinovadiligncia1Click(Sender: TObject);
begin
  if FPromotor = 0 then
    Geral.MB_Aviso('Defina o promotor!')
  else
    UMyMod.FormInsUpd_Show(TFmCiclosPro_Edit, FmCiclosPro_Edit,
      afmoNegarComAviso, QrCiclosAula, stIns);
end;

procedure TFmCiclosProm.PMDiligenciaPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCiclosAula.State <> dsInactive) and (QrCiclosAula.RecordCount > 0);
  Enab2 := QrCiclosAulaCodigo.Value = 0;
  //
  Incluinovadiligncia1.Enabled  := Enab;
  Alteradilignciaatual1.Enabled := Enab and Enab2;
  Excluidilignciaatual1.Enabled := Enab and Enab2;
end;

procedure TFmCiclosProm.PMLanctosPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrCiclosAula.State <> dsInactive) and (QrCiclosAula.RecordCount > 0);
  Enab2 := QrCiclosAulaCodigo.Value = 0;
  Enab3 := (QrDespProm.State <> dsInactive) and (QrDespProm.RecordCount > 0);
  //
  Incluilanamento1.Enabled := Enab and Enab2;
  Alteralanamento1.Enabled := Enab and Enab2 and Enab3;
  Excluilanamento1.Enabled := Enab and Enab2 and Enab3;
end;

procedure TFmCiclosProm.QrCiclosAulaAfterScroll(DataSet: TDataSet);
begin
  ReopenDespProm(0);
  //
  BtLocCiclo.Enabled := QrCiclosAulaCodigo.Value <> 0;
  BtLanctos.Enabled  := QrCiclosAulaCodigo.Value = 0;
end;

procedure TFmCiclosProm.QrCiclosAulaBeforeClose(DataSet: TDataSet);
begin
  QrDespProm.Close;
  //
  BtLocCiclo.Enabled := False;
  BtLanctos.Enabled  := False;
end;

procedure TFmCiclosProm.QrCiclosAulaCalcFields(DataSet: TDataSet);
begin
  if Length(QrCiclosAulaCidade.Value) > 0 then
    QrCiclosAulaCIDADE_TXT.Value := QrCiclosAulaCidade.Value
  else
    QrCiclosAulaCIDADE_TXT.Value := QrCiclosAulaMUNI.Value; 
  if QrCiclosAulaAlunosInsc.Value = 0 then
    QrCiclosAulaPRESENCA.Value := ''
  else
    QrCiclosAulaPRESENCA.Value := Geral.FFT(
    QrCiclosAulaAlunosAula.Value / QrCiclosAulaAlunosInsc.Value * 100, 2,
    siNegativo) + '%';
end;

procedure TFmCiclosProm.QrDespPromCalcFields(DataSet: TDataSet);
begin
  QrDespPromSEQ.Value := QrDespProm.RecNo;
  QrDesppromKGT.Value := 0;
end;

procedure TFmCiclosProm.TPDataFClick(Sender: TObject);
begin
  if FTabLctALS <> '' then
    ReopenCiclosAula(QrCiclosAulaControle.Value);
end;

procedure TFmCiclosProm.TPDataFExit(Sender: TObject);
begin
  AtualizaGradeHA;
end;

procedure TFmCiclosProm.TPDataIClick(Sender: TObject);
begin
  if FTabLctALS <> '' then
  begin
    ReopenCiclosAula(QrCiclosAulaControle.Value);
    //
    if TPDataI.Date <> 0 then
      EdMesAnoI.ValueVariant := MLAGeral.MesEAnoDaData(TPDataI.Date);
  end;
end;

procedure TFmCiclosProm.TPDataIExit(Sender: TObject);
begin
  AtualizaGradeHA;
end;

procedure TFmCiclosProm.ReopenCiclosAula(Controle: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    QrCiclosAula.Close;
    QrCiclosAula.SQL.Clear;
    QrCiclosAula.SQL.Add('SELECT dtb.Nome MUNI, cia.*');
    QrCiclosAula.SQL.Add('FROM ciclosaula cia');
    QrCiclosAula.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici dtb ON dtb.Codigo = cia.CodiCidade');
    QrCiclosAula.SQL.Add('WHERE cia.Promotor=:P0');
    QrCiclosAula.SQL.Add('AND cia.Data BETWEEN :P1 AND :P2');
    QrCiclosAula.SQL.Add('ORDER BY cia.Data');
    QrCiclosAula.Params[00].AsInteger := FPromotor;
    QrCiclosAula.Params[01].AsString  := Geral.FDT(TPDataI.Date, 1);
    QrCiclosAula.Params[02].AsString  := Geral.FDT(TPDataF.Date, 1);
    QrCiclosAula.Open;
    //
    if Controle <> 0 then
      QrCiclosAula.Locate('Controle', Controle, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCiclosProm.ReopenDespProm(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDespProm, Dmod.MyDB, [
    'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, ',
    'lan.* ',
    'FROM ' + FTabLctALS + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'LEFT JOIN contas con ON con.Codigo=lan.Genero ',
    'WHERE lan.FatID in (703,704) ',
    'AND lan.FatID_Sub=' + Geral.FF0(QrCiclosAulaControle.Value),
    '']);
  //
  if Controle <> 0 then
    QrDespProm.Locate('Controle', Controle, []);
end;

procedure TFmCiclosProm.SbImprimeClick(Sender: TObject);
begin
  if MyObjects.FIC(FPromotor = 0, nil, 'Defina o Promotor!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    QrAnt.Close;
    QrAnt.Params[00].AsInteger := FPromotor;
    QrAnt.Params[01].AsString  := Geral.FDT(TPDataI.Date, 1);
    QrAnt.Open;
    //
    DmodG.ReopenEndereco(QrPromCodigo.Value);
    ReabreHistoricoAnual();
  finally
    Screen.Cursor := crDefault;
  end;
  MyObjects.frxDefineDataSets(frxMovPeriodo, [
    DmodG.frxDsMaster,
    frxDsAnt,
    frxDsCiclosAula,
    frxDsDespProm,
    frxDsGrafData,
    frxDsGrafVal1,
    frxDsGrafVal2
    ]);
  MyObjects.frxMostra(frxMovPeriodo, 'Extrato de Promotor');
end;

procedure TFmCiclosProm.AtualizaGradeHA();
  function MontaDataIni(Data: TDate): TDate;
  var
    Dia, Mes, Ano: Word;
  begin
    DecodeDate(Data, Ano, Mes, Dia);
    //
    Result := EncodeDate(Ano, Mes, 1);
  end;
var
  DataI, DataF: String;
  Ano, Mes, Dia: Word;
  i: Integer;
  m: TDateTime;
  Mudou: Boolean;
  a1, a2, n1, n2, Presenca: Double;
  Diferenca: Integer;
begin
  Mudou := False;
  n1    := 0;
  n2    := 0;
  //
  if EdMesAnoI.ValueVariant = NULL then
    MyObjects.LimpaGrade(GradeHA, 1, 0, False)
  else
  begin
    if (FMesAnoIAtu <> Trunc(EdMesAnoI.ValueVariant)) or
      (FDataI <> TPDataI.Date) or
      (FDataF <> TPDataF.Date) then
    begin
      Mudou       := True;
      FMesAnoIAtu := Trunc(EdMesAnoI.ValueVariant);
      FDataI      := TPDataI.Date;
      FDataF      := TPDataF.Date;
      m           := EdMesAnoI.ValueVariant;
      //
      for i := 1 to 12 do
      begin
        GradeHA.Cells[i,00] := Uppercase(Geral.FDT(m, 18));
        m                   := IncMonth(m, 1);
      end;
    end;
    if FPromAtu <> FPromotor then
    begin
      FPromAtu := FPromotor;
      Mudou    := True;
    end;
    if Mudou then
    begin
      MyObjects.LimpaGrade(GradeHA, 1, 1, False);
      DecodeDate(EdMesAnoI.ValueVariant, Ano, Mes, Dia);
      //
      DataI := Geral.FDT(EncodeDate(Ano, Mes, 1), 1);
      DataF := Geral.FDT(IncMonth(EncodeDate(Ano, Mes, 1), 13)-1, 1);
      //
      QrHistAnu.Close;
      QrHistAnu.Params[00].AsInteger := FPromotor;
      QrHistAnu.Params[01].AsString  := DataI;
      QrHistAnu.Params[02].AsString  := DataF;
      QrHistAnu.Open;
      // N�o fechar!! usa na impress�o
      //
      if QrHistAnu.RecordCount > 0 then
      begin
        while not QrHistAnu.Eof do
        begin
          (* ERRO!
          i := Trunc(QrHistAnuMes.Value + 0.1) - 11;
          if i < 1 then i := i + 12;
          *)
          //
          Diferenca := Round(DaysBetween(MontaDataIni(TPDataI.Date), MontaDataIni(QrHistAnuData.Value)) / 30);
          i         := Diferenca + 1;
          //
          a1 := QrHistAnuAlunosInsc.Value;
          a2 := QrHistAnuAlunosAula.Value;
          if a1 = 0 then Presenca := 0 else Presenca := a2 / a1 * 100;
          GradeHA.Cells[i,01] := FormatFloat('0', a1);
          GradeHA.Cells[i,02] := FormatFloat('0', a2);
          GradeHA.Cells[i,03] := FormatFloat('0.00', Presenca);
          n1 := n1 + a1;
          n2 := n2 + a2;
          //Parei aqui
          QrHistAnu.Next;
        end;
        if n1 = 0 then
          Presenca := 0
        else
          Presenca := n2 / n1 * 100;
        //
        GradeHA.Cells[13,01] := FormatFloat('0', n1);
        GradeHA.Cells[13,02] := FormatFloat('0', n2);
        GradeHA.Cells[13,03] := FormatFloat('0.00', Presenca);
      end;
    end;
  end;
end;

procedure TFmCiclosProm.ReabreHistoricoAnual();
var
  i, k, n: Integer;
  Ano, Mes, Dia: Word;
  ai, ap, pp: Double;
  Data: TDateTime;
  GraficoTab: String;
begin
  n := 0;
  //
  if MyObjects.FIC(EdMesAnoI.ValueVariant = Null, EdMesAnoI,
    'Informe o per�odo inicial do hist�rico!') then Exit;
  //
  DecodeDate(EdMesAnoI.ValueVariant, Ano, Mes, Dia);
  //UCriar.RecriaTabelaLocal('Grafico', 1);
  GraficoTab := UCriar.RecriaTempTableNovo(ntrttGrafico, DmodG.QrUpdPID1, False, 0, 'Grafico');
  //
  DModG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DELETE FROM ' + GraficoTab);
  DmodG.QrUpdPID1.ExecSQL;
  // QrHistAnu j� est� aberto (configura a GradeHA)
  for i := 0 to 11 do
  begin
    Data := IncMonth(EdMesAnoI.ValueVariant, i);
    k    := Mes + i;
    //
    if k > 12 then
      k := k - 12;
    //
    //N�o est� funcionando por o Mes � float => if QrHistAnu.Locate('Mes', k, []) then
    if DmCiclos.LocalizaMesCiclosPromProf(QrHistAnu, k) then
    begin
      ai := QrHistAnuAlunosInsc.Value;
      ap := QrHistAnuAlunosAula.Value;
    end else begin
      ai := 0;
      ap := 0;
    end;
    if ai = 0 then pp := 0 else pp := ap / ai * 100;
    if (Data + 31 > Date) and (ai < 1) and (ap < 1) then
    begin
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, GraficoTab, False, [
        'Texto'
      ], ['Linha'], [
        Uppercase(Geral.FDT(Data, 18))
      ], [i + 1], False);
    end else begin
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, GraficoTab, False, [
        'Texto', 'Val01', 'Val02', 'Val03'
      ], ['Linha'], [
        Uppercase(Geral.FDT(Data, 18)), ai, ap, pp
      ], [i + 1], False);
      //
      n := i + 1;
    end;
  end;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + GraficoTab + ' SET Ativo=1 WHERE Linha <=:P0');
  DmodG.QrUpdPID1.Params[0].AsInteger := n;
  DmodG.QrUpdPID1.ExecSQL;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGrafData, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + GraficoTab,
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGrafVal1, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + GraficoTab,
  'WHERE Ativo=1 ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGrafVal2, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + GraficoTab,
  'WHERE Ativo=1 ',
  '']);
  //
end;

end.
