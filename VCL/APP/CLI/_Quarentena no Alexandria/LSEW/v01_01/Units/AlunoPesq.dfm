object FmAlunoPesq: TFmAlunoPesq
  Left = 339
  Top = 185
  Caption = 'CIC-ALUNO-002 :: Pesquisa  de Alunos e Clientes'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtPesq: TBitBtn
      Tag = 22
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtPesqClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtEdita: TBitBtn
      Tag = 11
      Left = 208
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Edita'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtEditaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Pesquisa  de Alunos e Clientes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 90
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object Label2: TLabel
        Left = 395
        Top = 4
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
      end
      object Label3: TLabel
        Left = 517
        Top = 4
        Width = 29
        Height = 13
        Caption = 'Serial:'
      end
      object Label4: TLabel
        Left = 8
        Top = 45
        Width = 45
        Height = 13
        Caption = 'Telefone:'
      end
      object Label5: TLabel
        Left = 238
        Top = 45
        Width = 20
        Height = 13
        Caption = 'Fax:'
      end
      object Label6: TLabel
        Left = 123
        Top = 45
        Width = 35
        Height = 13
        Caption = 'Celular:'
      end
      object EdNome: TdmkEdit
        Left = 8
        Top = 20
        Width = 385
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCNPJ: TdmkEdit
        Left = 395
        Top = 20
        Width = 120
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdSerial: TdmkEdit
        Left = 517
        Top = 20
        Width = 277
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object dmkEdTel: TdmkEdit
        Left = 8
        Top = 61
        Width = 113
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = dmkEdTelExit
      end
      object dmkEdFax: TdmkEdit
        Left = 238
        Top = 61
        Width = 113
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = dmkEdFaxExit
      end
      object dmkEdCel: TdmkEdit
        Left = 123
        Top = 61
        Width = 113
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = dmkEdCelExit
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 91
      Width = 1006
      Height = 306
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_ENT'
          Title.Caption = 'Nome'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJ_TXT'
          Title.Caption = 'CNPJ / CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TE1_TXT'
          Title.Caption = 'Telefone 1'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CEL_TXT'
          Title.Caption = 'Celular'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMELOGRAD'
          Title.Caption = 'Lograd.'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RUA'
          Title.Caption = 'Nome logradouro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NUMERO_TXT'
          Title.Caption = 'N'#250'mero'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPL'
          Title.Caption = 'Complemento'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Email'
          Title.Caption = 'Emeio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BAIRRO'
          Title.Caption = 'Bairro'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CIDADE'
          Title.Caption = 'Cidade'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CEP_TXT'
          Title.Caption = 'CEP'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEUF'
          Title.Caption = 'UF'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TE2_TXT'
          Title.Caption = 'Telefone 2'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TE3_TXT'
          Title.Caption = 'Telefone3'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FAX_TXT'
          Title.Caption = 'Fax'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Email'
          Title.Caption = 'Emeio'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsPesq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_ENT'
          Title.Caption = 'Nome'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJ_TXT'
          Title.Caption = 'CNPJ / CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TE1_TXT'
          Title.Caption = 'Telefone 1'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CEL_TXT'
          Title.Caption = 'Celular'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMELOGRAD'
          Title.Caption = 'Lograd.'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RUA'
          Title.Caption = 'Nome logradouro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NUMERO_TXT'
          Title.Caption = 'N'#250'mero'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPL'
          Title.Caption = 'Complemento'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Email'
          Title.Caption = 'Emeio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BAIRRO'
          Title.Caption = 'Bairro'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CIDADE'
          Title.Caption = 'Cidade'
          Width = 160
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CEP_TXT'
          Title.Caption = 'CEP'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEUF'
          Title.Caption = 'UF'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TE2_TXT'
          Title.Caption = 'Telefone 2'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TE3_TXT'
          Title.Caption = 'Telefone3'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FAX_TXT'
          Title.Caption = 'Fax'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Email'
          Title.Caption = 'Emeio'
          Visible = True
        end>
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.Respons1, en.ENatal, en.PNatal,'#13
      ' '
      'en.Cliente1, '#13
      'en.Cliente2, en. EUF, en.PUF, en.SSP, en.DataRG,'#13
      'alu.MD5Num, cab.Senha MD5Senha, cab.Forca MD5Forca,'
      #10
      ''
      ''
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOME_EN' +
        'T, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe3        ELSE en.PTe3    END TE3,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ECel        ELSE en.PCel    END CEL,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.EEmail      ELSE en.PEMail  END Emai' +
        'l'
      #10
      ''
      ''
      ''
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'LEFT JOIN md5alu      alu ON alu.Entidade=en.Codigo'
      'LEFT JOIN md5cab      cab ON cab.Codigo=alu.MD5Cab'
      ''
      'WHERE en.Cliente1="V"'
      'OR en.Cliente2="V"'
      '')
    Left = 16
    Top = 144
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPesqTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPesqRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrPesqNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrPesqCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrPesqIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrPesqNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrPesqRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrPesqNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrPesqCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrPesqBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrPesqCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrPesqNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrPesqNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrPesqPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrPesqLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrPesqCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrPesqTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrPesqFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrPesqCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrPesqNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrPesqE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrPesqCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesqFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesqTE2: TWideStringField
      FieldName = 'TE2'
    end
    object QrPesqTE3: TWideStringField
      FieldName = 'TE3'
    end
    object QrPesqCEL: TWideStringField
      FieldName = 'CEL'
    end
    object QrPesqTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE2_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE3_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrPesqENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrPesqPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrPesqCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrPesqCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrPesqEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrPesqPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrPesqSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrPesqDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrPesqMD5Num: TIntegerField
      FieldName = 'MD5Num'
    end
    object QrPesqMD5Forca: TSmallintField
      FieldName = 'MD5Forca'
    end
    object QrPesqMD5Senha: TWideStringField
      FieldName = 'MD5Senha'
      Required = True
      Size = 8
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 44
    Top = 144
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtEdita
    Left = 12
    Top = 12
  end
end
