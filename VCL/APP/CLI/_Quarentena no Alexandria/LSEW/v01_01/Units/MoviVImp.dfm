object FmMoviVImp: TFmMoviVImp
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-012 :: Impress'#227'o de Vendas de Mercadorias (Com pre'#231'os)'
  ClientHeight = 207
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 159
    Width = 713
    Height = 48
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 312
    ExplicitWidth = 398
    object BtOK: TBitBtn
      Tag = 5
      Left = 15
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 601
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 286
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 713
    Height = 48
    Align = alTop
    Caption = 'Impress'#227'o de Vendas de Mercadorias (Com pre'#231'os)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 398
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 711
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 547
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 713
    Height = 111
    Align = alClient
    TabOrder = 2
    ExplicitLeft = 1
    ExplicitTop = 46
    ExplicitWidth = 506
  end
  object frxPedidosV: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40983.754497303240000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if <VAR_IMGEXISTE> = True then Picture1.LoadFromFile(<VAR_IMG>' +
        ');      '
      'end.')
    OnGetValue = frxPedidosVGetValue
    Left = 68
    Top = 64
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMovim
        DataSetName = 'frxDsMovim'
      end
      item
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
      end
      item
        DataSet = frxDsMoviVK
        DataSetName = 'frxDsMoviVK'
      end
      item
        DataSet = frxDsMoviVKIts
        DataSetName = 'frxDsMoviVKIts'
      end
      item
        DataSet = frxDsMoviVStat
        DataSetName = 'frxDsMoviVStat'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 67.700835000000000000
        ParentFont = False
        Top = 200.315090000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
        RowCount = 0
        StartNewPage = True
        object Memo6: TfrxMemoView
          Top = 5.267717999999992000
          Width = 699.212598430000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Top = 48.803185000000000000
          Width = 45.354298980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 45.354360000000000000
          Top = 48.803185000000000000
          Width = 90.708658980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."Codigo"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 468.283767000000000000
          Top = 48.803185000000000000
          Width = 95.244070570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'STATUS DO PEDIDO:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 304.630118000000000000
          Top = 48.803185000000000000
          Width = 65.763760980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA VENDA:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 139.086704000000000000
          Top = 48.803185000000000000
          Width = 67.653525980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA PEDIDO:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 563.527923000000000000
          Top = 48.803185000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMoviVStat."NOMESTATUS"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 370.015987000000000000
          Top = 48.803185000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataField = 'DATAREAL_TXT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."DATAREAL_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 206.362338000000000000
          Top = 48.803185000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."DataPedi"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 45.354360000000000000
          Top = 30.236240000000000000
          Width = 652.724831000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMoviV."NOMEENTI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Top = 30.236240000000000000
          Width = 45.354298980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 332.598640000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMovim
        DataSetName = 'frxDsMovim'
        RowCount = 0
        object Memo15: TfrxMemoView
          Width = 306.141783540000000000
          Height = 18.897650000000000000
          DataField = 'NOMEGRA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMEGRA"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 306.141783300000000000
          Width = 148.535406950000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOR'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMECOR"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 454.677459000000000000
          Width = 96.755845950000000000
          Height = 18.897650000000000000
          DataField = 'NOMETAM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMETAM"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 551.212957000000000000
          Width = 72.566926690000000000
          Height = 18.897650000000000000
          DataField = 'POSIq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovim."POSIq"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 624.000000000000000000
          Width = 76.346456690000000000
          Height = 18.897650000000000000
          DataSet = frxDsMovim
          DataSetName = 'frxDsMovim'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovim."POSIv"]')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 427.086890000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviVK
        DataSetName = 'frxDsMoviVK'
        RowCount = 0
        object Memo26: TfrxMemoView
          Left = 28.346475000000000000
          Width = 454.299139860000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMoviVK."Nome"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 549.165709000000000000
          Width = 74.834645180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviVK."Qtd"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Width = 29.102234540000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'KIT:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 482.645981000000000000
          Width = 66.519679180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'QUANTIDADE:')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 510.236550000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviVKIts
        DataSetName = 'frxDsMoviVKIts'
        RowCount = 0
        object Memo30: TfrxMemoView
          Width = 302.740206540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviVKIts."NOMEGRADE"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 551.811380000000000000
          Width = 72.188974180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviVKIts."POSIq"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 302.740206300000000000
          Width = 148.535382540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviVKIts."NOMECOR"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 451.275882000000000000
          Width = 100.535351540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviVKIts."NOMETAM"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 624.000000000000000000
          Width = 76.346456690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviVKIts."POSIv"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 20.078744060000000000
        Top = 291.023810000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMoviV."Codigo"'
        object Memo11: TfrxMemoView
          Top = 1.181094059999998000
          Width = 306.141783540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 551.212957000000000000
          Top = 1.181094060000000000
          Width = 72.566926690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 306.141930000000000000
          Top = 1.181094059999998000
          Width = 148.535406950000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'COR')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 454.677459000000000000
          Top = 1.181094059999998000
          Width = 96.755845950000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'TAMANHO')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 624.000000000000000000
          Top = 1.181090160000000000
          Width = 75.590580470000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 31.188992000000000000
        Top = 374.173470000000000000
        Width = 699.213050000000000000
        object Memo39: TfrxMemoView
          Top = 7.000000000000000000
          Width = 623.622047240000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 623.622450000000000000
          Top = 7.181102360000000000
          Width = 76.346456690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovim."POSIv">)]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 468.661720000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMoviVKIts."VAZIO"'
        object Memo20: TfrxMemoView
          Width = 302.740206540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'MERCADORIA')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 550.944884330000000000
          Width = 72.566929133858300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 302.740353000000000000
          Width = 152.314912540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'COR')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 454.677165350000000000
          Width = 96.755821540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'TAMANHO')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 623.622450000000000000
          Width = 75.590580470000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.078737720000000000
        Top = 551.811380000000000000
        Width = 699.213050000000000000
        object Memo44: TfrxMemoView
          Top = 6.377953000000000000
          Width = 485.291338582677200000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 624.000000000000000000
          Top = 6.425196850000000000
          Width = 76.346456690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviVKIts."POSIv">) + <frxDsMoviVK."FRETE">]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 551.811023620000000000
          Top = 6.425196850000000000
          Width = 74.834645180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviVK."FRETE"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 485.669291340000000000
          Top = 6.425196850000000000
          Width = 66.519679180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Frete do Kit:')
          ParentFont = False
        end
      end
      object DetailData3: TfrxDetailData
        FillType = ftBrush
        Height = 117.165366540000000000
        Top = 600.945270000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
        RowCount = 0
        object Memo19: TfrxMemoView
          Left = 0.000000240000000000
          Top = 41.952783000000000000
          Width = 620.220482450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE FRETE')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 620.598826240000000000
          Top = 41.952783000000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Frete"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 60.850433000000000000
          Width = 620.598435450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DO PEDIDO')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 620.598826240000000000
          Top = 60.850433000000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_TOTPED]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 0.000000240000000000
          Top = 98.267716535433080000
          Width = 620.598435450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 620.598826480000000000
          Top = 98.267716540000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_TOTVDA]')
          ParentFont = False
        end
        object Rich1: TfrxRichView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Height = 18.897637800000000000
          StretchMode = smMaxHeight
          DataField = 'Observ'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6465
            666C616E67313034367B5C666F6E7474626C7B5C66305C666E696C204D532053
            616E732053657269663B7D7D0D0A7B5C2A5C67656E657261746F72204D736674
            6564697420352E34312E32312E323531303B7D5C766965776B696E64345C7563
            315C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Memo32: TfrxMemoView
          Width = 302.740206540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'OBSERVA'#199#213'ES:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Top = 79.370130000000000000
          Width = 620.598435450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 620.598826240000000000
          Top = 79.370130000000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_DESCO]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 120.944960000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          Width = 699.213050000000000000
          Height = 119.811055350000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo51: TfrxMemoView
          Left = 80.503937010000000000
          Top = 21.433086000000000000
          Width = 467.527581020000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 80.637848000000000000
          Top = 0.755906000000000000
          Width = 467.527861000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMoviV."NOMEENTI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 8.826778000000000000
          Top = 0.755906000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Destinat'#225'rio:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 555.590910000000000000
          Top = 9.653525000000000000
          Width = 124.724409450000000000
          Height = 102.047244090000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          Left = 80.370130000000000000
          Top = 36.669295000000000000
          Width = 467.527581020000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL2"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsMoviV: TfrxDBDataset
    UserName = 'frxDsMoviV'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEENTI=NOMEENTI'
      'Codigo=Codigo'
      'Controle=Controle'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'Cliente=Cliente'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DATAREAL_TXT=DATAREAL_TXT'
      'SALDO=SALDO'
      'Total=Total'
      'Pago=Pago'
      'POSIt=POSIt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'GraCusPrc=GraCusPrc'
      'Transportadora=Transportadora'
      'Frete=Frete'
      'NOMECAD2=NOMECAD2'
      'NOMEALT2=NOMEALT2'
      'NOMECAD=NOMECAD'
      'NOMEALT=NOMEALT'
      'FRETEA=FRETEA'
      'Observ=Observ'
      'Descon=Descon'
      'CodRast=CodRast'
      'StatAtual=StatAtual'
      'NOMESTATAT=NOMESTATAT'
      'NOMGRACUSPRC=NOMGRACUSPRC'
      'TOTALVDA=TOTALVDA')
    DataSet = FmMoviV.QrMoviV
    BCDToCurrency = False
    Left = 96
    Top = 64
  end
  object frxDsMovim: TfrxDBDataset
    UserName = 'frxDsMovim'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEGRA=NOMEGRA'
      'NOMETAM=NOMETAM'
      'NOMECOR=NOMECOR'
      'Controle=Controle'
      'Conta=Conta'
      'Grade=Grade'
      'Cor=Cor'
      'Tam=Tam'
      'Qtd=Qtd'
      'Val=Val'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'PRECO=PRECO'
      'Motivo=Motivo'
      'Ven=Ven'
      'POSIq=POSIq'
      'POSIv=POSIv'
      'VAZIO=VAZIO')
    DataSet = FmMoviV.QrMovim
    BCDToCurrency = False
    Left = 124
    Top = 64
  end
  object frxDsMoviVK: TfrxDBDataset
    UserName = 'frxDsMoviVK'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'GradeK=GradeK'
      'Qtd=Qtd'
      'Nome=Nome'
      'FRETE=FRETE'
      'Total=Total')
    DataSet = FmMoviV.QrMoviVK
    BCDToCurrency = False
    Left = 152
    Top = 64
  end
  object frxDsMoviVKIts: TfrxDBDataset
    UserName = 'frxDsMoviVKIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grade=Grade'
      'Tam=Tam'
      'Cor=Cor'
      'Qtd=Qtd'
      'Val=Val'
      'NOMEGRADE=NOMEGRADE'
      'NOMECOR=NOMECOR'
      'NOMETAM=NOMETAM'
      'PRECO=PRECO'
      'POSIq=POSIq'
      'POSIv=POSIv'
      'VAZIO=VAZIO')
    DataSet = FmMoviV.QrMoviVKIts
    BCDToCurrency = False
    Left = 180
    Top = 64
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'NUMERO=NUMERO'
      'UF=UF'
      'Lograd=Lograd'
      'CEP=CEP'
      'E_ALL2=E_ALL2')
    DataSet = FmMoviV.QrCli
    BCDToCurrency = False
    Left = 208
    Top = 64
  end
  object frxDsMoviVStat: TfrxDBDataset
    UserName = 'frxDsMoviVStat'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'StatusV=StatusV'
      'DiaHora=DiaHora'
      'NOMESTATUS=NOMESTATUS'
      'Cor=Cor'
      'STATUS_STR=STATUS_STR')
    DataSet = FmMoviV.QrMoviVStat
    BCDToCurrency = False
    Left = 236
    Top = 64
  end
  object dmkPermissoes2: TdmkPermissoes
    Left = 302
    Top = 80
  end
end
