object FmGraRel: TFmGraRel
  Left = 473
  Top = 173
  Caption = 'PRD-GRADE-006 :: Relat'#243'rios de Mercadorias'
  ClientHeight = 376
  ClientWidth = 592
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 328
    Width = 592
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 480
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 592
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rios de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 590
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 3
      ExplicitWidth = 588
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 592
    Height = 280
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 590
      Height = 278
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Estoque'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 582
          Height = 113
          Align = alTop
          TabOrder = 0
          object RGOrdem1: TRadioGroup
            Left = 1
            Top = 1
            Width = 100
            Height = 111
            Align = alLeft
            Caption = ' Ordem 1: '
            ItemIndex = 0
            Items.Strings = (
              'Departamento'
              'Grupo'
              'Mercadoria'
              'Cor'
              'Tamanho')
            TabOrder = 0
          end
          object RGOrdem2: TRadioGroup
            Left = 101
            Top = 1
            Width = 100
            Height = 111
            Align = alLeft
            Caption = ' Ordem 2: '
            ItemIndex = 1
            Items.Strings = (
              'Departamento'
              'Grupo'
              'Mercadoria'
              'Cor'
              'Tamanho')
            TabOrder = 1
          end
          object RGOrdem3: TRadioGroup
            Left = 201
            Top = 1
            Width = 100
            Height = 111
            Align = alLeft
            Caption = ' Ordem 3: '
            ItemIndex = 2
            Items.Strings = (
              'Departamento'
              'Grupo'
              'Mercadoria'
              'Cor'
              'Tamanho')
            TabOrder = 2
          end
          object RGOrdem4: TRadioGroup
            Left = 301
            Top = 1
            Width = 100
            Height = 111
            Align = alLeft
            Caption = ' Ordem 4: '
            ItemIndex = 3
            Items.Strings = (
              'Departamento'
              'Grupo'
              'Mercadoria'
              'Cor'
              'Tamanho')
            TabOrder = 3
          end
          object RGGrupos: TRadioGroup
            Left = 501
            Top = 1
            Width = 80
            Height = 111
            Align = alClient
            Caption = ' Agrupamentos'
            ItemIndex = 1
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4')
            TabOrder = 4
          end
          object RGOrdem5: TRadioGroup
            Left = 401
            Top = 1
            Width = 100
            Height = 111
            Align = alLeft
            Caption = ' Ordem 5: '
            ItemIndex = 4
            Items.Strings = (
              'Departamento'
              'Grupo'
              'Mercadoria'
              'Cor'
              'Tamanho')
            TabOrder = 5
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 113
          Width = 582
          Height = 137
          Align = alClient
          TabOrder = 1
          object GroupBox1: TGroupBox
            Left = 1
            Top = 1
            Width = 580
            Height = 44
            Align = alTop
            Caption = ' Status: '
            TabOrder = 0
            object CkAtivos: TCheckBox
              Left = 12
              Top = 16
              Width = 97
              Height = 17
              Caption = 'Ativos.'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object CkInativ: TCheckBox
              Left = 112
              Top = 16
              Width = 97
              Height = 17
              Caption = 'Inativos.'
              TabOrder = 1
            end
            object CkPositi: TCheckBox
              Left = 212
              Top = 16
              Width = 113
              Height = 17
              Caption = 'Somente positivos.'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Hist'#243'rico'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 582
          Height = 89
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 4
            Width = 56
            Height = 13
            Caption = 'Mercadoria:'
          end
          object Label2: TLabel
            Left = 12
            Top = 44
            Width = 19
            Height = 13
            Caption = 'Cor:'
          end
          object Label3: TLabel
            Left = 232
            Top = 44
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
          end
          object Label4: TLabel
            Left = 480
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object Label5: TLabel
            Left = 480
            Top = 44
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object EdGra: TdmkEditCB
            Left = 12
            Top = 20
            Width = 58
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBGra
          end
          object CBGra: TdmkDBLookupComboBox
            Left = 72
            Top = 20
            Width = 313
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGra
            TabOrder = 1
            dmkEditCB = EdGra
            UpdType = utYes
          end
          object EdCor: TdmkEditCB
            Left = 12
            Top = 60
            Width = 58
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBCor
          end
          object CBCor: TdmkDBLookupComboBox
            Left = 72
            Top = 60
            Width = 157
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCor
            TabOrder = 3
            dmkEditCB = EdCor
            UpdType = utYes
          end
          object EdTam: TdmkEditCB
            Left = 232
            Top = 60
            Width = 58
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBTam
          end
          object CBTam: TdmkDBLookupComboBox
            Left = 292
            Top = 60
            Width = 93
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTam
            TabOrder = 5
            dmkEditCB = EdTam
            UpdType = utYes
          end
          object TPIniHist: TDateTimePicker
            Left = 480
            Top = 20
            Width = 92
            Height = 21
            Date = 39142.466203159710000000
            Time = 39142.466203159710000000
            TabOrder = 6
          end
          object TPFimHist: TDateTimePicker
            Left = 480
            Top = 60
            Width = 92
            Height = 21
            Date = 39142.466203159710000000
            Time = 39142.466203159710000000
            TabOrder = 7
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 89
          Width = 582
          Height = 161
          Align = alClient
          TabOrder = 1
          object PB1: TProgressBar
            Left = 1
            Top = 1
            Width = 580
            Height = 16
            Align = alTop
            TabOrder = 0
            Visible = False
          end
        end
      end
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEstqCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, cor.Nome NOMECOR, tam.Nome NOMETAM,'
      'prd.Codigo, prd.Cor, prd.Tam, prd.Ativo, prd.PrecoC,'
      'prd.PrecoV, prd.PediQ, prd.EstqQ, prd.EstqV, prd.EstqC,'
      'gra.GradeG, grg.Nome NOMEGRG, grd.Nome NOMEDEPTO'
      ''
      'FROM produtos prd'
      'LEFT JOIN grades   gra ON gra.Codigo=prd.Codigo'
      'LEFT JOIN cores    cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'LEFT JOIN gradeG   grg ON grg.Codigo=gra.GradeG'
      'LEFT JOIN gradeD   grd ON grd.Codigo=grg.GradeD'
      ''
      'WHERE prd.Ativo in (1,2)'
      'AND prd.EstqQ > -10'
      ''
      'ORDER BY NOMEDEPTO, NOMEGRG, NOMEGRA, NOMECOR, NOMETAM')
    Left = 8
    Top = 288
    object QrEstqNOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Size = 30
    end
    object QrEstqNOMEGRG: TWideStringField
      FieldName = 'NOMEGRG'
      Origin = 'gradeg.Nome'
      Size = 30
    end
    object QrEstqNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Origin = 'grades.Nome'
      Size = 30
    end
    object QrEstqNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
    end
    object QrEstqNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
    object QrEstqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'produtos.Codigo'
      Required = True
    end
    object QrEstqCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'produtos.Cor'
      Required = True
    end
    object QrEstqTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'produtos.Tam'
      Required = True
    end
    object QrEstqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'produtos.Ativo'
      Required = True
    end
    object QrEstqPrecoC: TFloatField
      FieldName = 'PrecoC'
      Origin = 'produtos.PrecoC'
      Required = True
    end
    object QrEstqPrecoV: TFloatField
      FieldName = 'PrecoV'
      Origin = 'produtos.PrecoV'
      Required = True
    end
    object QrEstqPediQ: TFloatField
      FieldName = 'PediQ'
      Origin = 'produtos.PediQ'
      Required = True
    end
    object QrEstqEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'produtos.EstqQ'
      Required = True
    end
    object QrEstqEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'produtos.EstqV'
      Required = True
    end
    object QrEstqEstqC: TFloatField
      FieldName = 'EstqC'
      Origin = 'produtos.EstqC'
      Required = True
    end
    object QrEstqGradeG: TIntegerField
      FieldName = 'GradeG'
      Origin = 'grades.GradeG'
    end
    object QrEstqFutQt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FutQt'
      Calculated = True
    end
    object QrEstqNOMETOT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETOT'
      Size = 255
      Calculated = True
    end
  end
  object frxEstq: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39038.633847465300000000
    ReportOptions.LastChange = 39979.472536342590000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <MeuLogoExiste> = True then'
      '    Picture1.LoadFromFile(<MeuLogoCaminho>);'
      
        '  if <VARF_MAXGRUPO> > 0 then GroupHeader1.Visible := True else ' +
        'GroupHeader1.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 1 then GroupHeader2.Visible := True else ' +
        'GroupHeader2.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 2 then GroupHeader3.Visible := True else ' +
        'GroupHeader3.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 3 then GroupHeader4.Visible := True else ' +
        'GroupHeader4.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 4 then GroupHeader5.Visible := True else ' +
        'GroupHeader5.Visible := False;'
      '  '
      
        '  if <VARF_MAXGRUPO> > 0 then GroupFooter1.Visible := True else ' +
        'GroupFooter1.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 1 then GroupFooter2.Visible := True else ' +
        'GroupFooter2.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 2 then GroupFooter3.Visible := True else ' +
        'GroupFooter3.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 3 then GroupFooter4.Visible := True else ' +
        'GroupFooter4.Visible := False;'
      
        '  if <VARF_MAXGRUPO> > 4 then GroupFooter5.Visible := True else ' +
        'GroupFooter5.Visible := False;'
      '  '
      '  GroupHeader1.Condition := <VARF_GRUPO1>;'
      '  Memo20.Memo.Text := <VARF_HEADR1>;'
      '  Memo35.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GroupHeader2.Condition := <VARF_GRUPO2>;'
      '  Memo23.Memo.Text := <VARF_HEADR3>;'
      '  Memo34.Memo.Text := <VARF_FOOTR3>;'
      '  '
      '  GroupHeader3.Condition := <VARF_GRUPO3>;'
      '  Memo26.Memo.Text := <VARF_HEADR4>;'
      '  Memo33.Memo.Text := <VARF_FOOTR4>;'
      ''
      '  GroupHeader4.Condition := <VARF_GRUPO4>;'
      '  Memo29.Memo.Text := <VARF_HEADR5>;'
      '  Memo32.Memo.Text := <VARF_FOOTR5>;'
      '  '
      '  GroupHeader5.Condition := <VARF_GRUPO5>;'
      '  Memo49.Memo.Text := <VARF_HEADR5>;'
      '  Memo52.Memo.Text := <VARF_FOOTR5>;'
      ''
      'end.')
    OnGetValue = frxEstqGetValue
    Left = 8
    Top = 232
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Line21: TfrxLineView
        Top = 1130.079470000000000000
        Width = 797.480830000000000000
        ShowHint = False
        Frame.Style = fsDashDotDot
        Frame.Typ = [ftTop]
      end
      object ReportTitle1: TfrxReportTitle
        Height = 124.724490000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape2: TfrxShapeView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 105.826840000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Picture1: TfrxPictureView
          Left = 45.354360000000000000
          Top = 22.677180000000000000
          Width = 166.299212600000000000
          Height = 64.251968500000000000
          ShowHint = False
          DataField = 'Logo'
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Left = 215.433210000000000000
          Top = 45.354360000000000000
          Width = 532.913730000000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsDono."E_CUC"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 215.433210000000000000
          Top = 22.677180000000000000
          Width = 532.913730000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Top = 90.708720000000000000
          Width = 718.110700000000000000
          Height = 30.236230240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8 = (
            'Estoque de Mercadorias em [VARF_DATA_HORA]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 37.795300000000000000
        Top = 163.653649000000000000
        Width = 793.701300000000000000
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Descri'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Estoque qtde.')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 340.157700000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Valor estoque')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 476.220780000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Pre'#195#167'o compra')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 408.189240000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Custo estoque')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 544.252320000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Pre'#195#167'o venda')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 619.842920000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Pedido Qtd.')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 687.874460000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Estq Futuro')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 461.480613000000000000
        Width = 793.701300000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMETOT'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDsEstq."NOMETOT"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EstqQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."EstqQ"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EstqV'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."EstqV"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 476.220780000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'PrecoC'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."PrecoC"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 544.252320000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'PrecoV'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."PrecoV"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'PediQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."PediQ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 408.189240000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'EstqC'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."EstqC"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'FutQt'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsEstq."FutQt"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 259.275758000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsEstq."NOMEGRG"'
        object Memo20: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Depto. [frxDsEstq."NOMEDEPTO"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 671.244528000000000000
        Width = 793.701300000000000000
        object Memo21: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqV">,MasterData1)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sub-total [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."PediQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."FutQt">,MasterData1)]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 22.677180000000000000
        Top = 301.984447000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsEstq."NOMEGRA"'
        object Memo23: TfrxMemoView
          Left = 52.913420000000000000
          Top = 3.779530000000022000
          Width = 702.992580000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Grupo [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 628.535839000000000000
        Width = 793.701300000000000000
        object Memo24: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqV">,MasterData1)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sub-total [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."PediQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."FutQt">,MasterData1)]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 344.693136000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsEstq."NOMECOR"'
        object Memo26: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Mercad. [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 585.827150000000000000
        Width = 793.701300000000000000
        object Memo27: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqV">,MasterData1)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sub-total [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."PediQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."FutQt">,MasterData1)]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 383.622295000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsEstq."NOMETAM"'
        object Memo29: TfrxMemoView
          Left = 94.488250000000000000
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Cor [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 543.118461000000000000
        Width = 793.701300000000000000
        object Memo30: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqV">,MasterData1)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sub-total [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."PediQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."FutQt">,MasterData1)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 26.456710000000000000
        Top = 751.748517000000000000
        Width = 793.701300000000000000
        object Memo44: TfrxMemoView
          Left = 272.126160000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 340.157700000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqV">,MasterData1)]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 37.795300000000000000
          Top = 7.559060000000045000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'TOTAL GERAL')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 619.842920000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."PediQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 687.874460000000000000
          Top = 7.559060000000045000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."FutQt">,MasterData1)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 798.236736000000000000
        Width = 793.701300000000000000
        object Memo107: TfrxMemoView
          Left = 563.149970000000000000
          Top = 3.779530000000022000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object GroupHeader5: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 422.551454000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsEstq."NOMETAM"'
        object Memo49: TfrxMemoView
          Left = 111.118182000000000000
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Tamanho [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
      end
      object GroupFooter5: TfrxGroupFooter
        Height = 22.677180000000000000
        Top = 500.409772000000000000
        Width = 793.701300000000000000
        object Memo50: TfrxMemoView
          Left = 272.126160000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 340.157700000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."EstqV">,MasterData1)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 37.795300000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sub-total [frxDsEstq."NOMEGRG"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."PediQ">,MasterData1)]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsEstq."FutQt">,MasterData1)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEstq: TfrxDBDataset
    UserName = 'frxDsEstq'
    CloseDataSource = False
    DataSet = QrEstq
    BCDToCurrency = False
    Left = 8
    Top = 260
  end
  object QrGra: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grades'
      'ORDER BY Nome')
    Left = 9
    Top = 9
    object QrGraCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'grades.Codigo'
    end
    object QrGraNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grades.Nome'
      Size = 30
    end
  end
  object DsGra: TDataSource
    DataSet = QrGra
    Left = 37
    Top = 9
  end
  object DsCor: TDataSource
    DataSet = QrCor
    Left = 97
    Top = 9
  end
  object QrCor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cores'
      'ORDER BY Nome')
    Left = 69
    Top = 9
    object QrCorCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cores.Codigo'
    end
    object QrCorNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cores.Nome'
    end
  end
  object QrTam: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tamanhos'
      'ORDER BY Nome')
    Left = 129
    Top = 9
    object QrTamCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tamanhos.Codigo'
    end
    object QrTamNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsTam: TDataSource
    DataSet = QrTam
    Left = 157
    Top = 9
  end
  object QrProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Ativo'
      'FROM produtos'
      'WHERE Codigo=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2')
    Left = 64
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrProdAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'produtos.Ativo'
    end
  end
  object QrHist: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrHistCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM movim'
      'WHERE Grade=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2'
      'AND DataReal > 0'
      'AND DataReal BETWEEN :P3 AND :P4'
      ''
      'ORDER BY DataReal, Motivo')
    Left = 36
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrHistControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movim.Controle'
    end
    object QrHistConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
    end
    object QrHistGrade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
    end
    object QrHistCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'movim.Cor'
    end
    object QrHistTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'movim.Tam'
    end
    object QrHistQtd: TFloatField
      FieldName = 'Qtd'
      Origin = 'movim.Qtd'
    end
    object QrHistVal: TFloatField
      FieldName = 'Val'
      Origin = 'movim.Val'
    end
    object QrHistDataPedi: TDateField
      FieldName = 'DataPedi'
      Origin = 'movim.DataPedi'
    end
    object QrHistDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'movim.DataReal'
    end
    object QrHistMotivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'movim.Motivo'
    end
    object QrHistLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'movim.Lk'
    end
    object QrHistDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'movim.DataCad'
    end
    object QrHistDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'movim.DataAlt'
    end
    object QrHistUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'movim.UserCad'
    end
    object QrHistUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'movim.UserAlt'
    end
    object QrHistVen: TFloatField
      FieldName = 'Ven'
      Origin = 'movim.Ven'
    end
    object QrHistSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Origin = 'movim.SALDOQTD'
    end
    object QrHistSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Origin = 'movim.SALDOVAL'
    end
    object QrHistNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 30
      Calculated = True
    end
    object QrHistCUSTOMOV: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOMOV'
      Calculated = True
    end
    object QrHistSALDOCUS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDOCUS'
      Calculated = True
    end
  end
  object QrAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Val) Val, SUM(Qtd) Qtd, SUM(Ven) Ven '
      'FROM movim'
      'WHERE Grade=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2'
      'AND DataReal > 0'
      'AND DataReal BETWEEN :P3 AND :P4')
    Left = 92
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrAntVal: TFloatField
      FieldName = 'Val'
    end
    object QrAntQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrAntVen: TFloatField
      FieldName = 'Ven'
    end
  end
  object frxHist: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39038.633847465300000000
    ReportOptions.LastChange = 39038.633847465300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <MeuLogoExiste> = True then'
      '    Picture1.LoadFromFile(<MeuLogoCaminho>);'
      'end.')
    OnGetValue = frxEstqGetValue
    Left = 36
    Top = 232
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsHist
        DataSetName = 'frxDsHist'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Line21: TfrxLineView
        Top = 1130.079470000000000000
        Width = 797.480830000000000000
        ShowHint = False
        Frame.Style = fsDashDotDot
        Frame.Typ = [ftTop]
      end
      object ReportTitle1: TfrxReportTitle
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape2: TfrxShapeView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 128.504020000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Picture1: TfrxPictureView
          Left = 49.133890000000000000
          Top = 22.677180000000000000
          Width = 166.299212600000000000
          Height = 64.251968500000000000
          ShowHint = False
          DataField = 'Logo'
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Left = 219.212740000000000000
          Top = 45.354360000000000000
          Width = 529.134200000000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsDono."E_CUC"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 219.212740000000000000
          Top = 22.677180000000000000
          Width = 529.134200000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Top = 90.708720000000000000
          Width = 718.110700000000000000
          Height = 26.456700240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8 = (
            'Hist'#195#179'rico de Mercadoria')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Top = 117.165430000000000000
          Width = 718.110700000000000000
          Height = 22.677170240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[VARF_MERCADORIA]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 37.795300000000000000
        Top = 188.976500000000000000
        Width = 793.701300000000000000
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 52.913385826771700000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Data Mov.')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 328.819110000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Qtde. movim.')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Valor movim.')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 464.882190000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Custo m'#195#169'dio')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 90.708720000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Data Lan'#195#167'.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 143.622140000000000000
          Top = 18.897650000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Motivo do movimento')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Estq. acum.')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 619.842920000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Valor acum.')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 687.874460000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Custo m'#195#169'dio')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 366.614410000000000000
        Width = 793.701300000000000000
        object Memo107: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000020000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[VARF_DATA_HORA] P'#195#161'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 287.244280000000000000
        Width = 793.701300000000000000
        DataSet = frxDsHist
        DataSetName = 'frxDsHist'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 37.795300000000000000
          Width = 52.913385826771700000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsHist."DataReal"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 90.708720000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsHist."DataPedi"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 328.819110000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Qtd'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsHist."Qtd"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 396.850650000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Val'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsHist."Val"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 464.882190000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CUSTOMOV'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsHist."CUSTOMOV"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 143.622140000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMETIPO'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[frxDsHist."NOMETIPO"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 551.811380000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SALDOQTD'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsHist."SALDOQTD"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 619.842920000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SALDOVAL'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsHist."SALDOVAL"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 687.874460000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'SALDOCUS'
          DataSet = frxDsHist
          DataSetName = 'frxDsHist'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsHist."SALDOCUS"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsHist: TfrxDBDataset
    UserName = 'frxDsHist'
    CloseDataSource = False
    DataSet = QrHist
    BCDToCurrency = False
    Left = 36
    Top = 260
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
