unit CartAval;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral, dmkDBEdit,
  ColorGrd, ActnColorMaps, ActnMan, dmkCheckBox, dmkValUsu, dmkPermissoes,
  dmkEditCB, dmkDBLookupComboBox, dmkRadioGroup, dmkMemo, Variants, frxClass,
  frxDBSet, frxRich, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, dmkCheckGroup, URLMon, UnDmkProcFunc, UnDmkEnums;

type
  TFmCartAval = class(TForm)
    DsCartAval: TDataSource;
    QrCartAval: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtCarta: TBitBtn;
    BtMotivo: TBitBtn;
    PainelData: TPanel;
    PainelEdita: TPanel;
    Label9: TLabel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdCodigo: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    RGStatus: TdmkRadioGroup;
    Label10: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    MeTexto: TdmkMemo;
    LaTexto: TLabel;
    QrCartAvalCodigo: TIntegerField;
    QrCartAvalEnti: TIntegerField;
    QrCartAvalStatus: TIntegerField;
    QrCartAvalLk: TIntegerField;
    QrCartAvalDataCad: TDateField;
    QrCartAvalDataAlt: TDateField;
    QrCartAvalUserCad: TIntegerField;
    QrCartAvalUserAlt: TIntegerField;
    QrCartAvalAlterWeb: TSmallintField;
    QrCartAvalAtivo: TSmallintField;
    QrCartAvalTexto: TWideMemoField;
    QrCartAvalFoto1: TWideStringField;
    QrCartAvalFoto2: TWideStringField;
    QrCartAvalNOMEENTI: TWideStringField;
    QrEnti: TmySQLQuery;
    DsEnti: TDataSource;
    QrEntiCodigo: TIntegerField;
    QrEntiNOMEENTI: TWideStringField;
    QrCartAvalFoto3: TWideStringField;
    frxDsCarta1: TfrxDBDataset;
    frxCartAval: TfrxReport;
    QrCarta1: TmySQLQuery;
    QrCarta1Texto: TWideMemoField;
    QrCartAvalFoto4: TWideStringField;
    QrCarta2: TmySQLQuery;
    MemoField1: TWideMemoField;
    frxDsCarta2: TfrxDBDataset;
    QrCartAvalNOMECAD2: TWideStringField;
    QrCartAvalNOMECAD: TWideStringField;
    QrSenhas: TmySQLQuery;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasLk: TIntegerField;
    DsSenhas: TDataSource;
    frxVerso: TfrxReport;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    QrCliE_ALL2: TWideStringField;
    frxDsCartAval: TfrxDBDataset;
    IdHTTP1: TIdHTTP;
    EdDepto: TdmkEdit;
    QrCartAvalGradeD: TIntegerField;
    PMCarta: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    PMMotivo: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    DsCartAvalS: TDataSource;
    QrCartAvalS: TmySQLQuery;
    QrLoc: TmySQLQuery;
    QrLocControle: TIntegerField;
    QrCartAvalSCodigo: TIntegerField;
    QrCartAvalSControle: TIntegerField;
    QrCartAvalSMotivo: TIntegerField;
    QrCartAvalSDiaHora: TDateTimeField;
    QrCartAvalSNOMEMOTIVO: TWideStringField;
    QrCartAvalSMOTIVO_STR: TWideStringField;
    SBCliente: TSpeedButton;
    frxDsCli: TfrxDBDataset;
    QrSenhasFuncionario: TIntegerField;
    QrSenhasNOMEENTI: TWideStringField;
    QrCartAvalModelo: TWideStringField;
    QrCartAvalTecido: TWideStringField;
    QrCartAvalCor: TWideStringField;
    QrCartAvalTamanho: TWideStringField;
    QrCartAvalQtde: TIntegerField;
    PnDados: TPanel;
    EdModelo: TdmkEdit;
    Label5: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    Label13: TLabel;
    Label14: TLabel;
    dmkEdit3: TdmkEdit;
    Label15: TLabel;
    dmkEdit4: TdmkEdit;
    Label16: TLabel;
    QrCartAvalValTot: TFloatField;
    Label18: TLabel;
    LaDataExp2: TLabel;
    QrAvaEmpresa: TmySQLQuery;
    StringField2: TWideStringField;
    StringField3: TWideStringField;
    StringField7: TWideStringField;
    StringField24: TWideStringField;
    frxDsAvaEmpresa: TfrxDBDataset;
    QrAvaEmpresaCodigo: TIntegerField;
    QrAvaEmpresaTipo: TSmallintField;
    QrAvaEmpresaCodUsu: TIntegerField;
    QrAvaEmpresaIE: TWideStringField;
    QrAvaEmpresaNOME_ENT: TWideStringField;
    QrAvaEmpresaCNPJ_CPF: TWideStringField;
    QrAvaEmpresaIE_RG: TWideStringField;
    QrAvaEmpresaRUA: TWideStringField;
    QrAvaEmpresaCOMPL: TWideStringField;
    QrAvaEmpresaBAIRRO: TWideStringField;
    QrAvaEmpresaCIDADE: TWideStringField;
    QrAvaEmpresaNOMELOGRAD: TWideStringField;
    QrAvaEmpresaNOMEUF: TWideStringField;
    QrAvaEmpresaPais: TWideStringField;
    QrAvaEmpresaENDEREF: TWideStringField;
    QrAvaEmpresaTE1: TWideStringField;
    QrAvaEmpresaFAX: TWideStringField;
    QrAvaEmpresaCAD_FEDERAL: TWideStringField;
    QrAvaEmpresaCAD_ESTADUAL: TWideStringField;
    QrAvaEmpresaTE1_TXT: TWideStringField;
    QrAvaEmpresaFAX_TXT: TWideStringField;
    QrAvaEmpresaNUMERO_TXT: TWideStringField;
    QrAvaEmpresaE_ALL: TWideStringField;
    QrAvaEmpresaE_ALL2: TWideStringField;
    QrAvaEmpresaSite: TWideStringField;
    QrCliLograd: TFloatField;
    QrCliNUMERO: TFloatField;
    QrCliCEP: TFloatField;
    QrCliUF: TFloatField;
    QrCliSite: TWideStringField;
    QrAvaEmpresaNUMERO: TFloatField;
    QrAvaEmpresaUF: TFloatField;
    QrAvaEmpresaLograd: TFloatField;
    QrAvaEmpresaCEP: TFloatField;
    Panel6: TPanel;
    Panel7: TPanel;
    Img4: TImage;
    Img3: TImage;
    Img2: TImage;
    Img1: TImage;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    LaDataExp: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    PnAprov: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    DBEdModelo: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    PnReprov: TPanel;
    LaDBTexto: TLabel;
    MeDBTexto: TDBMemo;
    Panel4: TPanel;
    DBLookupListBox2: TDBLookupListBox;
    Label17: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCartAvalAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCartAvalBeforeOpen(DataSet: TDataSet);
    procedure BtCartaClick(Sender: TObject);
    procedure RGStatusClick(Sender: TObject);
    procedure QrCartAvalAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxCartAvalGetValue(const VarName: string; var Value: Variant);
    procedure QrCartAvalCalcFields(DataSet: TDataSet);
    procedure Img1Click(Sender: TObject);
    procedure Img2Click(Sender: TObject);
    procedure Img3Click(Sender: TObject);
    procedure Img4Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtMotivoClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure QrCartAvalSCalcFields(DataSet: TDataSet);
    procedure SBClienteClick(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure dmkDBEdit1DblClick(Sender: TObject);
    procedure dmkDBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEntidadeChange(Sender: TObject);
    procedure QrAvaEmpresaCalcFields(DataSet: TDataSet);
  private
    function  ObtemCaminhoFoto(FldFoto: String; Foto: Integer): String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure CarregaFoto(Foto1, Foto2, Foto3, Foto4: String);
    procedure AbreImg(Foto: TImage; Image: String);
    procedure ImprimeCarta();
    procedure DownloadWebFile(Url, Destino: String);
    procedure ReopenEntidade(Query: TmySQLQuery; Entidade: Integer);
  public
    { Public declarations }
    FCartAvalS: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCartAvalS(Controle: Integer);
    function  InsereMotivo(Codigo, Motivo: Integer; DataHora: String): Boolean;
  end;

var
  FmCartAval: TFmCartAval;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, Principal, ImgView, CartAvalS, ModuleGeral, Entidade2,
  VendaPesq, UnMyObjects, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCartAval.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCartAval.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCartAvalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCartAval.DefParams;
begin
  VAR_GOTOTABELA := 'cartaval';
  VAR_GOTOMYSQLTABLE := QrCartAval;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ava.*, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  VAR_SQLx.Add('ELSE ent.Nome END NOMEENTI');
  VAR_SQLx.Add('FROM cartaval ava');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = ava.Enti');
  VAR_SQLx.Add('WHERE ava.Codigo > 0');
  //
  VAR_SQL1.Add('AND ava.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ava.Nome Like :P0');
  //
end;

function TFmCartAval.ObtemCaminhoFoto(FldFoto: String; Foto: Integer): String;
var
  Web: Integer;
  Web_MyURL, AvaDir, Codigo_Txt, Foto_Txt: String;
begin
  Result     := '';
  Web        := Dmod.QrControlePrdFotoWeb.Value;
  AvaDir     := Dmod.QrControleAvaDir.Value;
  Codigo_Txt := Geral.FF0(QrCartAvalCodigo.Value);
  Foto_Txt   := Geral.FF0(Foto);
  //
  if Web = 1 then
    Result := FldFoto
  else
    Result := AvaDir + '\' + 'ava_' + Codigo_Txt + '_' + Foto_Txt + '.jpg';
end;

procedure TFmCartAval.ReopenEntidade(Query: TmySQLQuery; Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT en.Codigo, Tipo, CodUsu, IE, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF, ',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG, ',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA, ',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0.000 NUMERO, ',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL, ',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO, ',
    'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CIDADE, ',
    'CASE WHEN en.Tipo=0 THEN en.EUF         ELSE en.PUF      END + 0.000 UF, ',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD, ',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF, ',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais, ',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0.000 Lograd, ',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0.000 CEP, ',
    'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF, ',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1, ',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX, ',
    'CASE WHEN en.Tipo=0 THEN en.ESite       ELSE en.PSite    END Site, ',
    'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    '']);
end;

procedure TFmCartAval.dmkDBEdit1DblClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCartAvalCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCartAval.dmkDBEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{var
  Codigo: Integer;}
begin
  //Codigo  := 0;
  if (Key = VK_F4) then
  begin
    FmPrincipal.FDepto    := 2;
    FmPrincipal.FCodDepto := QrCartAvalCodigo.Value;
    Close;
    {if DBCheck.CriaFm(TFmVendaPesq, FmVendaPesq, afmoNegarComAviso) then
    begin
      FmVendaPesq.FDepto := 2;
      FmVendaPesq.ShowModal;
      FmVendaPesq.Destroy;
      //
      Codigo := FmVendaPesq.FCodigo;
      if Codigo > 0 then
      begin
        LocCod(Codigo, Codigo);
      end;
    end;}
  end;
end;

procedure TFmCartAval.DownloadWebFile(Url, Destino: String);
var
  ImageMem : TMemoryStream;
  ImageJpg: TJPEGImage;
begin
  if Url = '' then
    Exit
  else if (Destino = '') or (not DirectoryExists(ExtractFileDir(Destino))) then
    Exit;
  //
  ImageMem := TMemoryStream.Create;
  ImageJpg := TJPEGImage.Create;
  try
    try
      IdHTTP1.Get(Url, ImageMem);
    except on e: EIdHTTPProtocolException do
      begin
        if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
        begin
          // N�o achou!
          Exit;
        end;
      end;
    end;
    ImageMem.Position := 0;
    ImageJpg.LoadFromStream(ImageMem);
    ImageJpg.SaveToFile(Destino);
  finally
    ImageJpg.Free;
    ImageMem.Free;
  end;
end;

procedure TFmCartAval.EdEntidadeChange(Sender: TObject);
var
  Aluno: Integer;
  DataExp: TDateTime;
begin
  Aluno := EdEntidade.ValueVariant;
  if Aluno > 0 then
  begin
    DataExp := Dmod.VerificaDataDeExpiracaoDoCurso(Aluno);
    LaDataExp2.Caption := Geral.FDT(DataExp, 2);
  end;
end;

procedure TFmCartAval.CarregaFoto(Foto1, Foto2, Foto3, Foto4: String);

  procedure CarregaFoto(Foto: String; Img: TImage);
  var
    ImageMem: TMemoryStream;
    ImageJpg: TJPEGImage;
  begin
    if Length(Foto) > 0 then
    begin
      if (Pos('http://', LowerCase(Foto)) > 0) or (Pos('www.', LowerCase(Foto)) > 0) then
      begin
        ImageMem := TMemoryStream.Create;
        ImageJpg := TJPEGImage.Create;
        try
          try
            if (Pos('http://', LowerCase(Foto)) > 0) then
              IdHTTP1.Get(Foto, ImageMem)
            else
              IdHTTP1.Get('http://' + Foto, ImageMem);
          except on e: EIdHTTPProtocolException do
            begin
              if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
              begin
                // N�o achou!
                Exit;
              end;
            end;
          end;
          ImageMem.Position := 0;
          ImageJpg.LoadFromStream(ImageMem);
          Img.Picture.Assign(ImageJpg);
        finally
          ImageJpg.Free;
          ImageMem.Free;
        end;
      end else
      begin
        if FmPrincipal.CarregaImg(Foto) then
        begin
          Img.Picture.LoadFromFile(Foto);
        end else
        begin
          Img.Picture.Assign(nil);
          //Img.Picture := nil;
        end;
      end;
    end else
    begin
      Img.Picture.Assign(nil);
      //Img.Picture := nil;
    end;
  end;
begin
  CarregaFoto(Foto1, Img1);
  CarregaFoto(Foto2, Img2);
  CarregaFoto(Foto3, Img3);
  CarregaFoto(Foto4, Img4);
end;

procedure TFmCartAval.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCartAval.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCartAval.ReopenCartAvalS(Controle: Integer);
begin
  QrCartAvalS.Close;
  QrCartAvalS.Params[0].AsInteger := QrCartAvalCodigo.Value;
  QrCartAvalS.Open;
  //
  if Controle > 0 then QrCartAvalS.Locate('Controle', Controle, []);
end;

procedure TFmCartAval.RGStatusClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := RGStatus.ItemIndex > -1;
  LaTexto.Visible := Visi;
  MeTexto.Visible := Visi;
  //
  if RGStatus.ItemIndex = 0 then
  begin
    PnDados.Top     := 110;
    PnDados.Visible := True;
    LaTexto.Top     := 252;
    MeTexto.Top     := 270;
  end;
  if RGStatus.ItemIndex = 1 then
  begin
    PnDados.Top     := 223;
    PnDados.Visible := False;
    LaTexto.Top     := 110;
    MeTexto.Top     := 128;
  end; 
end;

procedure TFmCartAval.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCartAval.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCartAval.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCartAval.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCartAval.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCartAval.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO          := QrCartAvalCodigo.Value;
  FmPrincipal.FCodDepto := 0;
  Close;
end;

procedure TFmCartAval.BtConfirmaClick(Sender: TObject);
var
  Codigo, Status, Entidade: Integer;
  Texto: String;
  Foto1, Foto2, Foto3, Foto4: String;
begin
  SbImprime.Enabled := True;
  Entidade          := EdEntidade.ValueVariant;
  Status            := RGStatus.ItemIndex;
  Texto             := MeTexto.Text;
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Defina o aluno!') then Exit;
  if MyObjects.FIC(Status < 0, RGStatus, 'Defina o status!') then Exit;
  if MyObjects.FIC(Length(Texto) = 0, MeTexto, 'Defina um texto!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('cartaval', 'Codigo', LaTipo.SQLType,
              QrCartAvalCodigo.Value);
  //
  if LaTipo.SQLType = stIns then
    InsereMotivo(Codigo, Dmod.QrControleAvaMotivo.Value,
      FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora()));
  //
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmCartAval, PainelEdita,
    'cartaval', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    Screen.Cursor := crHourGlass;
    //
    LocCod(Codigo, Codigo);
    ReopenCartAvalS(FCartAvalS);
    MostraEdicao(False, CO_TRAVADO, 0);
    FmPrincipal.MostraGraGruFotos2(QrCartAvalCodigo.Value, istCartAval);
    //
    if QrCartAvalStatus.Value = 0 then
    begin
      if Geral.MB_Pergunta('Deseja incluir um novo or�amento?') = ID_YES then
      begin
        FmPrincipal.CadastroVendas(0, QrCartAvalEnti.Value);
        //
        if FmPrincipal.FMoviVTot > 0 then
        begin
          if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'cartaval', False,
          [
            'ValTot'
          ], ['Codigo'],
          [
            FmPrincipal.FMoviVTot
          ], [QrCartAvalCodigo.Value]) then
          begin
          end;
        end;
      end;
    end;
    LocCod(Codigo, Codigo);
    ImprimeCarta();
    //
    Foto1 := ObtemCaminhoFoto(QrCartAvalFoto1.Value, 1);
    Foto2 := ObtemCaminhoFoto(QrCartAvalFoto2.Value, 2);
    Foto3 := ObtemCaminhoFoto(QrCartAvalFoto3.Value, 3);
    Foto4 := ObtemCaminhoFoto(QrCartAvalFoto4.Value, 4);
    //
    CarregaFoto(Foto1, Foto2, Foto3, Foto4);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCartAval.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  SbImprime.Enabled := True;
  Codigo            := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cartaval', Codigo);
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cartaval', 'Codigo');
end;

procedure TFmCartAval.BtMotivoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMotivo, BtMotivo);
end;

procedure TFmCartAval.FormCreate(Sender: TObject);
var
  Proxy, GradeD: Integer;
begin
  Dmod.ReopenControle;
  //
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  //
  QrEnti.Open;
  CriaOForm;
  //
  GradeD               := Dmod.QrControleAvaGradeD.Value;
  PainelEdita.Align    := alClient;
  PainelDados.Align    := alClient;
  LaTexto.Visible      := False;
  LaTexto.Visible      := False;
  PnDados.Visible      := False;
  LaTexto.Visible      := False;
  MeTexto.Visible      := False;
  //
  if GradeD = 0 then
  begin
    Geral.MB_Aviso('Departamento padr�o n�o definido nas op��es do aplicativo!');
    Exit;
  end;
end;

procedure TFmCartAval.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCartAvalCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCartAval.SBClienteClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    if VAR_ENTIDADE <> 0 then
    begin
      QrEnti.Close;
      QrEnti.Open;
      //
      EdEntidade.ValueVariant := VAR_ENTIDADE;
      CBEntidade.KeyValue     := VAR_ENTIDADE;
    end;
  end;
end;

procedure TFmCartAval.SbImprimeClick(Sender: TObject);
begin
  ImprimeCarta();
end;

procedure TFmCartAval.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCartAval.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCartAval.QrAvaEmpresaCalcFields(DataSet: TDataSet);
begin
  QrAvaEmpresaTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrAvaEmpresaTE1.Value);
  QrAvaEmpresaFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrAvaEmpresaFax.Value);
  QrAvaEmpresaNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrAvaEmpresaRUA.Value, Trunc(QrAvaEmpresaNumero.Value), False);
  //
  QrAvaEmpresaE_ALL.Value := Uppercase(QrAvaEmpresaNOMELOGRAD.Value);
  if Trim(QrAvaEmpresaE_ALL.Value) <> '' then QrAvaEmpresaE_ALL.Value :=
    QrAvaEmpresaE_ALL.Value + ' ';
  QrAvaEmpresaE_ALL.Value := QrAvaEmpresaE_ALL.Value + Uppercase(QrAvaEmpresaRua.Value);
  if Trim(QrAvaEmpresaRua.Value) <> '' then QrAvaEmpresaE_ALL.Value :=
    QrAvaEmpresaE_ALL.Value + ', ' + QrAvaEmpresaNUMERO_TXT.Value;
  if Trim(QrAvaEmpresaCompl.Value) <>  '' then QrAvaEmpresaE_ALL.Value :=
    QrAvaEmpresaE_ALL.Value + ' ' + Uppercase(QrAvaEmpresaCompl.Value);
  if Trim(QrAvaEmpresaBairro.Value) <>  '' then QrAvaEmpresaE_ALL.Value :=
    QrAvaEmpresaE_ALL.Value + ' - ' + Uppercase(QrAvaEmpresaBairro.Value);
  if QrAvaEmpresaCEP.Value > 0 then QrAvaEmpresaE_ALL2.Value :=
    QrAvaEmpresaE_ALL2.Value + 'CEP ' + Geral.FormataCEP_NT(QrAvaEmpresaCEP.Value);
  if Trim(QrAvaEmpresaCidade.Value) <>  '' then QrAvaEmpresaE_ALL2.Value :=
    QrAvaEmpresaE_ALL2.Value + ' - ' + Uppercase(QrAvaEmpresaCidade.Value);
  if Trim(QrAvaEmpresaNOMEUF.Value) <>  '' then QrAvaEmpresaE_ALL2.Value :=
    QrAvaEmpresaE_ALL2.Value + ', ' + QrAvaEmpresaNOMEUF.Value;
end;

procedure TFmCartAval.QrCartAvalAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCartAval.QrCartAvalAfterScroll(DataSet: TDataSet);
var
  DataExp: TDateTime;
  Foto1, Foto2, Foto3, Foto4: String;  
begin
  Foto1 := ObtemCaminhoFoto(QrCartAvalFoto1.Value, 1);
  Foto2 := ObtemCaminhoFoto(QrCartAvalFoto2.Value, 2);
  Foto3 := ObtemCaminhoFoto(QrCartAvalFoto3.Value, 3);
  Foto4 := ObtemCaminhoFoto(QrCartAvalFoto4.Value, 4);
  //
  case QrCartAvalStatus.Value of
    0:
    begin
      PnAprov.Visible  := True;
      PnReprov.Visible := False;
    end;
    1:
    begin
      PnAprov.Visible  := False;
      PnReprov.Visible := True;
    end;
  end;
  //
  ReopenCartAvalS(0);
  CarregaFoto(Foto1, Foto2, Foto3, Foto4);
  //
  DataExp := Dmod.VerificaDataDeExpiracaoDoCurso(QrCartAvalEnti.Value);
  LaDataExp.Caption := Geral.FDT(DataExp, 2);
end;

procedure TFmCartAval.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCartAval.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCartAvalCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cartaval', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCartAval.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCartAval.frxCartAvalGetValue(const VarName: string;
  var Value: Variant);
var
  Caminho, Foto1,Foto2, Foto3, Foto4: String;
  Existe, EFoto1, EFoto2, EFoto3, EFoto4: Boolean;
begin
  if (Dmod.QrControlePrdFotoWeb.Value = 1) and
    (Length(Dmod.QrControleWebLogo.Value) > 0) then
  begin
    Caminho := Dmod.QrControleAvaDir.Value + '\' + 'magistral.jpg';
    if FileExists(Caminho) = True then
      Existe := True
    else
      Existe := False;
  end else
  begin
    Caminho := Dmod.QrControleMeuLogoPath.Value;
    if (Length(Caminho) > 0) and (FileExists(Caminho) = True) then
      Existe := True
    else
      Existe := False;
  end;
  if Dmod.QrControlePrdFotoWeb.Value = 0 then
  begin
    Foto1 := ObtemCaminhoFoto(QrCartAvalFoto1.Value, 1);
    if (Length(Foto1) > 0) and (FileExists(Foto1) = True) then
      EFoto1 := True
    else
      EFoto1 := False;
    Foto2 := ObtemCaminhoFoto(QrCartAvalFoto2.Value, 2);
    if (Length(Foto2) > 0) and (FileExists(Foto2) = True) then
      EFoto2 := True
    else
      EFoto2 := False;
    Foto3 := ObtemCaminhoFoto(QrCartAvalFoto3.Value, 3);
    if (Length(Foto3) > 0) and (FileExists(Foto3) = True) then
      EFoto3 := True
    else
      EFoto3 := False;
    Foto4 := ObtemCaminhoFoto(QrCartAvalFoto4.Value, 4);
    if (Length(Foto4) > 0) and (FileExists(Foto4) = True) then
      EFoto4 := True
    else
      EFoto4 := False;
  end else
  begin
    Foto1 := Dmod.QrControleAvaDir.Value + '\' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_1.jpg';
    if (Length(QrCartAvalFoto1.Value) > 0) and (FileExists(Foto1) = True) then
      EFoto1 := True
    else
      EFoto1 := False;
    Foto2 := Dmod.QrControleAvaDir.Value + '\' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_2.jpg';
    if (Length(QrCartAvalFoto2.Value) > 0) and (FileExists(Foto2) = True) then
      EFoto2 := True
    else
      EFoto2 := False;
    Foto3 := Dmod.QrControleAvaDir.Value + '\' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_3.jpg';
    if (Length(QrCartAvalFoto3.Value) > 0) and (FileExists(Foto3) = True) then
      EFoto3 := True
    else
      EFoto3 := False;
    Foto4 := Dmod.QrControleAvaDir.Value + '\' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_4.jpg';
    if (Length(QrCartAvalFoto4.Value) > 0) and (FileExists(Foto4) = True) then
      EFoto4 := True
    else
      EFoto4 := False;
  end;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE') = 0 then
    Value := Existe
  else if AnsiCompareText(VarName, 'VAR_IMG') = 0 then
    Value := Caminho;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE1') = 0 then
    Value := EFoto1
  else if AnsiCompareText(VarName, 'VAR_IMG1') = 0 then
    Value := Foto1;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE2') = 0 then
    Value := EFoto2
  else if AnsiCompareText(VarName, 'VAR_IMG2') = 0 then
    Value := Foto2;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE3') = 0 then
    Value := EFoto3
  else if AnsiCompareText(VarName, 'VAR_IMG3') = 0 then
    Value := Foto3;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE4') = 0 then
    Value := EFoto4
  else if AnsiCompareText(VarName, 'VAR_IMG4') = 0 then
    Value := Foto4
  else if AnsiCompareText(VarName, 'CIDADE') = 0 then
    Value := UMyMod.ObtemNomeMunici(Trunc(DmodG.QrDonoCODMUNICI.Value), DModG.QrAllUpd)
  else if AnsiCompareText(VarName, 'DATAATUAL') = 0 then
    Value := FormatDateTime('dd"/"mm"/"yyyy', QrCartAvalDataCad.Value)
  else if AnsiCompareText(VarName, 'ENTIDADE') = 0 then
    Value := QrCartAvalNOMEENTI.Value
  else if AnsiCompareText(VarName, 'TEXTO') = 0 then
    Value := QrCartAvalTexto.Value
  else if AnsiCompareText(VarName, 'USUARIO') = 0 then
    Value := QrCartAvalNOMECAD2.Value
  else if AnsiCompareText(VarName, 'MODELO') = 0 then
    Value := QrCartAvalModelo.Value
  else if AnsiCompareText(VarName, 'TECIDO') = 0 then
    Value := QrCartAvalTecido.Value
  else if AnsiCompareText(VarName, 'QUANTIDADE') = 0 then
    Value := QrCartAvalQtde.Value
  else if AnsiCompareText(VarName, 'COR') = 0 then
    Value := QrCartAvalCor.Value
  else if AnsiCompareText(VarName, 'TAMANHO') = 0 then
    Value := QrCartAvalTamanho.Value
  else if AnsiCompareText(VarName, 'PERCALUNO') = 0 then
    Value := Dmod.QrControleAvaPerc.Value
  else if AnsiCompareText(VarName, 'VALORCAMEN') = 0 then
    Value := FormatFloat('#,###,##0.00', QrCartAvalValTot.Value) 
  else if AnsiCompareText(VarName, 'VALORPGTO') = 0 then
    Value := FormatFloat('#,###,##0.00', QrCartAvalValTot.Value + (QrCartAvalValTot.Value * Dmod.QrControleAvaPerc.Value / 100));
{  //
  if AnsiCompareText(VarName, '') = 0 then
    Value :=
  else if AnsiCompareText(VarName, 'ENTIDADE') = 0 then
    Value := QrCartAvalNOMEENTI.Value
  else if AnsiCompareText(VarName, 'TEXTO') = 0 then
    Value := QrCartAvalTexto.Value; }
end;

procedure TFmCartAval.Img1Click(Sender: TObject);
begin
  AbreImg(Img1, ObtemCaminhoFoto(QrCartAvalFoto1.Value, 1));
end;

procedure TFmCartAval.Img2Click(Sender: TObject);
begin
  AbreImg(Img2, ObtemCaminhoFoto(QrCartAvalFoto2.Value, 2));
end;

procedure TFmCartAval.Img3Click(Sender: TObject);
begin
  AbreImg(Img3, ObtemCaminhoFoto(QrCartAvalFoto3.Value, 3));
end;

procedure TFmCartAval.Img4Click(Sender: TObject);
begin
  AbreImg(Img4, ObtemCaminhoFoto(QrCartAvalFoto4.Value, 4));
end;

procedure TFmCartAval.ImprimeCarta;
var
  i, CodText, Status: Integer;
  Url, Fonte, Destino: String;
  Existe: Boolean;
begin
  Screen.Cursor := crHourGlass;
  //
  Existe  := False;
  CodText := 0;
  Status  := QrCartAvalStatus.Value;
  case Status of
    0: CodText := Dmod.QrControleAvaTexApr.Value;
    1: CodText := Dmod.QrControleAvaTexRep.Value;
  end;
  QrCarta1.Close;
  QrCarta1.Params[0].AsInteger := CodText;
  QrCarta1.Open;
  //
  QrCarta2.Close;
  QrCarta2.Params[0].AsInteger := CodText;
  QrCarta2.Open;
  //
  ReopenEntidade(QrCli, QrCartAvalEnti.Value);
  ReopenEntidade(QrAvaEmpresa, Dmod.QrControleAvaEmpresa.Value);
  //
  if (Length(Dmod.QrControleWebLogo.Value) > 0)
    and (Dmod.QrControlePrdFotoWeb.Value = 1) then
  begin
    Fonte   := Dmod.QrControleWebLogo.Value;
    Destino := Dmod.QrControleAvaDir.Value + '\' + 'magistral.jpg';
    DownloadWebFile(Fonte, Destino);
  end;
  if Dmod.QrControlePrdFotoWeb.Value = 1 then
  begin
    for i := 1 to 4 do
    begin
      case i of
        1: Existe := Length(QrCartAvalFoto1.Value) > 0;
        2: Existe := Length(QrCartAvalFoto2.Value) > 0;
        3: Existe := Length(QrCartAvalFoto3.Value) > 0;
        4: Existe := Length(QrCartAvalFoto4.Value) > 0;
      end;
      if Existe then
      begin
        if Pos('http://', LowerCase(Dmod.QrControleWeb_MyURL.Value)) > 0 then
          Url := Dmod.QrControleWeb_MyURL.Value
        else
          Url := 'http://' + Dmod.QrControleWeb_MyURL.Value;

        Fonte   := Url + '/' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_' + IntToStr(i) + '.jpg';
        Destino := Dmod.QrControleAvaDir.Value + '\' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_' + IntToStr(i) + '.jpg';
        DownloadWebFile(Fonte, Destino);
      end;
    end;
  end;
  Screen.Cursor := crDefault;
  //
  MyObjects.frxMostra(frxCartAval, 'Carta de avalia��o');
  MyObjects.frxMostra(frxVerso, 'Carta de avalia��o');
  //
  //Exclui imagens se web
  if (Length(Dmod.QrControleWebLogo.Value) > 0)
    and (Dmod.QrControlePrdFotoWeb.Value = 1) then
  begin
    Screen.Cursor := crHourGlass;
    Destino := Dmod.QrControleAvaDir.Value + '\' + 'magistral.jpg';
    if FmPrincipal.VerificaImg(Destino) then
      DeleteFile(Destino);
    for i := 1 to 4 do
    begin
      Destino := Dmod.QrControleAvaDir.Value + '\' + 'ava_' + IntToStr(QrCartAvalCodigo.Value) + '_' + IntToStr(i) + '.jpg';
      if FmPrincipal.VerificaImg(Destino) then
        DeleteFile(Destino);
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCartAval.Inclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrCartAval, [PainelDados],
  [PainelEdita], EdEntidade, LaTipo, 'cartaval');
  EdDepto.ValueVariant := Dmod.QrControleAvaGradeD.Value;
  LaTexto.Visible      := False;
  MeTexto.Visible      := False;
  SbImprime.Enabled    := False;
end;

function TFmCartAval.InsereMotivo(Codigo, Motivo: Integer; DataHora: String): Boolean;
begin
  Result := False;
  FCartAvalS := UMyMod.BuscaEmLivreY_Def('cartavals', 'Controle', stIns, 0);
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'cartavals', False,
  [
    'Motivo', 'DiaHora', 'Codigo'
  ], ['Controle'],
  [
    Motivo, DataHora, Codigo
  ], [FCartAvalS]) then
  begin
    Result := True;
  end;
end;

procedure TFmCartAval.QrCartAvalBeforeOpen(DataSet: TDataSet);
begin
  QrCartAvalCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCartAval.QrCartAvalCalcFields(DataSet: TDataSet);
begin
  case QrCartAvalUserCad.Value of
    -2: QrCartAvalNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrCartAvalNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrCartAvalNOMECAD2.Value := 'N�o definido';
    else QrCartAvalNOMECAD2.Value := QrCartAvalNOMECAD.Value;
  end;
end;

procedure TFmCartAval.QrCartAvalSCalcFields(DataSet: TDataSet);
begin
  QrCartAvalSMOTIVO_STR.Value := Geral.FDT(QrCartAvalSDiaHora.Value, 107) + ' - ' + QrCartAvalSNOMEMOTIVO.Value;
end;

procedure TFmCartAval.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCliRUA.Value, Trunc(QrCliNumero.Value), False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL2.Value :=
    QrCliE_ALL2.Value + 'CEP ' + Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL2.Value :=
    QrCliE_ALL2.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL2.Value :=
    QrCliE_ALL2.Value + ', ' + QrCliNOMEUF.Value;
end;

procedure TFmCartAval.Altera1Click(Sender: TObject);
begin
  if QrCartAvalCodigo.Value > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCartAval, [PainelDados],
    [PainelEdita], EdEntidade, LaTipo, 'cartaval');
    EdDepto.ValueVariant := Dmod.QrControleAvaGradeD.Value;
    SbImprime.Enabled    := False;
  end;
end;

procedure TFmCartAval.BtCartaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCarta, BtCarta);
end;

procedure TFmCartAval.AbreImg(Foto: TImage; Image: String);
begin
  if (Length(Image) > 0) and (FileExists(Image)) then
  begin
    if DBCheck.CriaFm(TFmImgView, FmImgView, afmoNegarComAviso) then
    begin
      FmImgView.FImage              := Image;
      FmImgView.Height              := Foto.Picture.Height;
      FmImgView.Width               := Foto.Picture.Width;
      FmImgView.HorzScrollBar.Range := Foto.Picture.Width;
      FmImgView.VertScrollBar.Range := Foto.Picture.Height;
      FmImgView.ShowModal;
      FmImgView.Destroy;
    end;
  end;
end;

procedure TFmCartAval.MenuItem1Click(Sender: TObject);
begin
  if QrCartAval.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmCartAvalS, FmCartAvalS, afmoNegarComAviso) then
    begin
      FmCartAvalS.LaTipo.SQLType := stIns;
      FmCartAvalS.FCodigo        := QrCartAvalCodigo.Value;
      //
      FmCartAvalS.ShowModal;
      FmCartAvalS.Destroy;
    end;
  end else
  begin
    Geral.MB_Aviso('Voc� deve adicionar algum �tem antes de adicionar o motivo!');
  end;
end;

procedure TFmCartAval.MenuItem2Click(Sender: TObject);
var
  Controle: Integer; 
begin
  if QrCartAvalS.RecordCount > 0 then
  begin
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Controle');
    QrLoc.SQL.Add('FROM cartavals');
    QrLoc.SQL.Add('WHERE Codigo=:P0');
    QrLoc.SQL.Add('ORDER BY DiaHora DESC');
    QrLoc.SQL.Add('LIMIT 1');
    QrLoc.Params[0].AsInteger := QrCartAvalCodigo.Value;
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
    begin
      if QrLoc.FieldByName('Controle').Value > QrCartAvalSControle.Value then
      begin
        Geral.MB_Aviso('Exclus�o cancelada. Existem motivos posteriores a este!');
        Exit;
      end;
    end;
    Controle := QrCartAvalSControle.Value;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do motivo ' +
      QrCartAvalSNOMEMOTIVO.Value +'?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM cartavals WHERE Controle=:P0');
      Dmod.QrUpd.Params[00].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCartAvalS(0);
    end;
  end else
  begin
    Geral.MB_Aviso('N�o foi localizado nenhum motivo!');
  end;
end;

procedure TFmCartAval.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Visible := False;
    PainelEdita.Visible := True;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.ValueVariant   := FormatFloat('000', Codigo);
      EdEntidade.ValueVariant := '';
      CBEntidade.KeyValue     := Null;
      RGStatus.ItemIndex      := -1;
      MeTexto.Text            := '';      
      EdEntidade.SetFocus;
    end else begin
      EdCodigo.ValueVariant   := QrCartAvalCodigo.Value;
      EdEntidade.ValueVariant := QrCartAvalEnti.Value;
      CBEntidade.KeyValue     := QrCartAvalEnti.Value;
      RGStatus.ItemIndex      := QrCartAvalStatus.Value;
      MeTexto.Text            := QrCartAvalTexto.Value;
      EdEntidade.SetFocus;
    end;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

end.
