object FmPtosPedAnul: TFmPtosPedAnul
  Left = 339
  Top = 185
  Caption = 
    'PTO-VENDA-005 :: Cancelamento de Itens de Pedidos de Pontos de V' +
    'enda'
  ClientHeight = 494
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 794
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 682
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtTodos: TBitBtn
      Tag = 127
      Left = 299
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Marca todos itens'
      Caption = '&Todos'
      TabOrder = 2
      OnClick = BtTodosClick
      NumGlyphs = 2
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 395
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Desmarca todos itens'
      Caption = '&Nenhum'
      TabOrder = 3
      OnClick = BtNenhumClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 794
    Height = 48
    Align = alTop
    Caption = 'Cancelamento de Itens de Pedidos de Pontos de Venda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 792
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 794
    Height = 398
    Align = alClient
    TabOrder = 0
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 70
      Width = 792
      Height = 327
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Title.Caption = #252
          Title.Font.Charset = SYMBOL_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Wingdings'
          Title.Font.Style = []
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Produto'
          Title.Caption = 'Produto'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cor'
          Title.Caption = 'Cor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Tam'
          Title.Caption = 'Tamanho'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataIns'
          Title.Caption = 'Data inclus'#227'o item'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoR'
          Title.Caption = '$ venda'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quanti'
          Title.Caption = 'Qtde'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_STATUS'
          Title.Caption = 'Status item'
          Width = 156
          Visible = True
        end>
      Color = clWindow
      DataSource = DmProd.DsPedItsAnul
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Title.Caption = #252
          Title.Font.Charset = SYMBOL_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -11
          Title.Font.Name = 'Wingdings'
          Title.Font.Style = []
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Produto'
          Title.Caption = 'Produto'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cor'
          Title.Caption = 'Cor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Tam'
          Title.Caption = 'Tamanho'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataIns'
          Title.Caption = 'Data inclus'#227'o item'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoR'
          Title.Caption = '$ venda'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quanti'
          Title.Caption = 'Qtde'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_STATUS'
          Title.Caption = 'Status item'
          Width = 156
          Visible = True
        end>
    end
    object PnLeitura: TPanel
      Left = 1
      Top = 1
      Width = 792
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 329
        Height = 49
        Align = alLeft
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 4
          Width = 89
          Height = 13
          Caption = 'Leitura / digita'#231#227'o:'
        end
        object EdLeituraCodigoDeBarras: TEdit
          Left = 4
          Top = 20
          Width = 222
          Height = 21
          MaxLength = 20
          TabOrder = 0
          OnChange = EdLeituraCodigoDeBarrasChange
          OnKeyDown = EdLeituraCodigoDeBarrasKeyDown
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 232
          Top = 4
          Width = 90
          Height = 40
          Caption = '&OK'
          TabOrder = 1
          NumGlyphs = 2
        end
      end
      object Panel9: TPanel
        Left = 329
        Top = 0
        Width = 463
        Height = 49
        Align = alClient
        Enabled = False
        TabOrder = 1
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 86
          Height = 13
          Caption = 'Grupo de produto:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 264
          Top = 4
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 388
          Top = 4
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 257
          Height = 21
          DataField = 'NO_GRADE'
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 264
          Top = 20
          Width = 121
          Height = 21
          DataField = 'NO_COR'
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 388
          Top = 20
          Width = 69
          Height = 21
          DataField = 'NO_TAM'
          TabOrder = 2
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 50
      Width = 792
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Selecione os itens a serem cancelados!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object QrLoc: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT pia.IDCtrl, pia.Ativo'
      'FROM peditsanul pia'
      'LEFT JOIN lesew.ptosstqmov psm ON psm.IDCtrl=pia.IDCtrl'
      'LEFT JOIN lesew.produtos prd ON prd.Controle=psm.Produto'
      'WHERE psm.Produto=:P0'
      'AND pia.Ativo=0')
    Left = 244
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrLocAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
