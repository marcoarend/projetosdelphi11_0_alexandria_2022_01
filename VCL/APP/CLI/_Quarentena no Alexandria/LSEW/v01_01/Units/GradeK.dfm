object FmGradeK: TFmGradeK
  Left = 469
  Top = 171
  Caption = 'PRD-GRADE-015 :: Cadastro de kits'
  ClientHeight = 496
  ClientWidth = 964
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnItens: TPanel
    Left = 0
    Top = 48
    Width = 964
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 399
      Width = 962
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfItem: TBitBtn
        Tag = 14
        Left = 8
        Top = 6
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfItemClick
      end
      object Panel6: TPanel
        Left = 856
        Top = 1
        Width = 105
        Height = 46
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Sa'#237'da'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 962
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 2
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit7
      end
      object Label12: TLabel
        Left = 114
        Top = 5
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label5: TLabel
        Left = 415
        Top = 7
        Width = 51
        Height = 13
        Caption = 'Valor frete:'
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 20
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGradeK
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 114
        Top = 21
        Width = 296
        Height = 21
        DataField = 'Nome'
        DataSource = DsGradeK
        TabOrder = 1
      end
      object CkAtivo: TDBCheckBox
        Left = 493
        Top = 23
        Width = 62
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsGradeK
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit3: TDBEdit
        Left = 415
        Top = 21
        Width = 72
        Height = 21
        DataField = 'Frete'
        DataSource = DsGradeK
        TabOrder = 3
      end
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 49
      Width = 962
      Height = 77
      Align = alTop
      DataSource = DsGradeKIts
      Enabled = False
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEGRA'
          Title.Caption = 'Mercadoria'
          Width = 185
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Cor'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtd'
          Title.Caption = 'Quant.'
          Width = 65
          Visible = True
        end>
    end
    object GradeA: TStringGrid
      Left = 1
      Top = 251
      Width = 962
      Height = 148
      Align = alBottom
      ColCount = 1
      DefaultColWidth = 18
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      TabOrder = 4
      OnClick = GradeAClick
      OnDrawCell = GradeADrawCell
      RowHeights = (
        18)
    end
    object Panel4: TPanel
      Left = 1
      Top = 188
      Width = 962
      Height = 63
      Align = alBottom
      TabOrder = 0
      object Label13: TLabel
        Left = 8
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Mercadoria:'
      end
      object Label14: TLabel
        Left = 512
        Top = 4
        Width = 19
        Height = 13
        Caption = 'Cor:'
      end
      object Label15: TLabel
        Left = 772
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label16: TLabel
        Left = 844
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object LaStatus: TLabel
        Left = 879
        Top = 1
        Width = 82
        Height = 44
        Align = alRight
        Alignment = taCenter
        AutoSize = False
        Caption = 'Travado'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 707
      end
      object SpeedButton6: TSpeedButton
        Left = 486
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object EdGrade: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGradeChange
        DBLookupComboBox = CBGrade
        IgnoraDBLookupComboBox = False
      end
      object EdCorCod: TEdit
        Left = 512
        Top = 20
        Width = 32
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object EdCorNom: TEdit
        Left = 544
        Top = 20
        Width = 225
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object EdTamCod: TEdit
        Left = 772
        Top = 20
        Width = 32
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
      object EdTamNom: TEdit
        Left = 803
        Top = 20
        Width = 37
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object CBGrade: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 421
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGrades
        TabOrder = 1
        dmkEditCB = EdGrade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object StaticText2: TStaticText
        Left = 1
        Top = 45
        Width = 960
        Height = 17
        Align = alBottom
        Alignment = taCenter
        Caption = 
          'Clique na c'#233'lula correspondente da grade acima para definir a co' +
          'r e o tamanho.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        ExplicitWidth = 457
      end
      object EdQtd: TdmkEdit
        Left = 844
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdQtdEnter
      end
    end
    object GradeC: TStringGrid
      Left = 317
      Top = 256
      Width = 361
      Height = 133
      ColCount = 2
      DefaultColWidth = 18
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 5
      Visible = False
      RowHeights = (
        18
        18)
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 964
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 399
      Width = 962
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel7: TPanel
        Left = 796
        Top = 1
        Width = 165
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 68
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 962
      Height = 196
      Align = alTop
      TabOrder = 1
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label4: TLabel
        Left = 86
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label6: TLabel
        Left = 488
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Valor frete:'
      end
      object dmkEdNome: TdmkEdit
        Left = 85
        Top = 21
        Width = 398
        Height = 21
        MaxLength = 100
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkCkAtivo: TdmkCheckBox
        Left = 566
        Top = 23
        Width = 43
        Height = 17
        Caption = 'Ativo'
        TabOrder = 3
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 21
        Width = 71
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFrete: TdmkEdit
        Left = 487
        Top = 21
        Width = 72
        Height = 21
        Alignment = taRightJustify
        MaxLength = 100
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Frete'
        UpdCampo = 'Frete'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 964
    Height = 448
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PnControle: TPanel
      Left = 1
      Top = 399
      Width = 962
      Height = 48
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 492
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtItens: TBitBtn
          Tag = 290
          Left = 95
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtItensClick
        end
        object BtCompra: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Kit'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtCompraClick
        end
        object BtFotos: TBitBtn
          Tag = 102
          Left = 186
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fotos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFotosClick
        end
      end
    end
    object PnData: TPanel
      Left = 1
      Top = 1
      Width = 962
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 114
        Top = 5
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 415
        Top = 7
        Width = 51
        Height = 13
        Caption = 'Valor frete:'
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGradeK
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 114
        Top = 21
        Width = 296
        Height = 21
        DataField = 'Nome'
        DataSource = DsGradeK
        TabOrder = 1
      end
      object DBCheckBox1: TDBCheckBox
        Left = 493
        Top = 23
        Width = 62
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsGradeK
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit2: TDBEdit
        Left = 415
        Top = 21
        Width = 72
        Height = 21
        DataField = 'Frete'
        DataSource = DsGradeK
        TabOrder = 3
      end
    end
    object PnDItens: TPanel
      Left = 1
      Top = 49
      Width = 962
      Height = 248
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 962
        Height = 248
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Kits'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Splitter1: TSplitter
            Left = 624
            Top = 17
            Width = 10
            Height = 203
            Align = alRight
            ExplicitLeft = 631
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 0
            Width = 27
            Height = 17
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Itens'
            TabOrder = 0
          end
          object GradeItens: TDBGrid
            Left = 0
            Top = 17
            Width = 624
            Height = 203
            Align = alClient
            DataSource = DsGradeKIts
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 185
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 65
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tamanho'
                Width = 65
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtd'
                Title.Caption = 'Quant.'
                Width = 65
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 634
            Top = 17
            Width = 320
            Height = 203
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object Image2: TImage
              Left = 0
              Top = 17
              Width = 320
              Height = 186
              Align = alClient
              Center = True
              Proportional = True
              Stretch = True
              ExplicitLeft = 264
              ExplicitTop = 40
              ExplicitWidth = 105
              ExplicitHeight = 105
            end
            object StaticText9: TStaticText
              Left = 0
              Top = 0
              Width = 29
              Height = 17
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 'Foto'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 964
    Height = 48
    Align = alTop
    Caption = '                              Cadastro de kits'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 655
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 221
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 881
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsGradeK: TDataSource
    DataSet = QrGradeK
    Left = 260
    Top = 9
  end
  object QrGradeK: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGradeKBeforeOpen
    AfterOpen = QrGradeKAfterOpen
    BeforeClose = QrGradeKBeforeClose
    AfterScroll = QrGradeKAfterScroll
    SQL.Strings = (
      'SELECT grk.*, fot.Nome NOMEFOTO'
      'FROM gradek grk'
      'LEFT JOIN fotos fot ON fot.Codigo = grk.Foto'
      'WHERE grk.Codigo > 0'
      'AND grk.Ativo = 1')
    Left = 232
    Top = 9
    object QrGradeKCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGradeKNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrGradeKAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGradeKFrete: TFloatField
      FieldName = 'Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrGradeKLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradeKDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradeKDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradeKUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradeKUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradeKAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGradeKFoto: TIntegerField
      FieldName = 'Foto'
    end
    object QrGradeKNOMEFOTO: TWideStringField
      FieldName = 'NOMEFOTO'
      Size = 255
    end
  end
  object PMCompra: TPopupMenu
    OnPopup = PMCompraPopup
    Left = 380
    Top = 440
    object Incluinovacompra1: TMenuItem
      Caption = '&Inclui novo kit'
      OnClick = Incluinovacompra1Click
    end
    object Alteracompraatual1: TMenuItem
      Caption = '&Altera kit atual'
      OnClick = Alteracompraatual1Click
    end
    object Excluicompraatual1: TMenuItem
      Caption = '&Exclui kit atual'
      OnClick = Excluicompraatual1Click
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 456
    Top = 440
    object Incluiitensnacompraatual1: TMenuItem
      Caption = '&Inclui / Altera itens na compra atual'
      OnClick = Incluiitensnacompraatual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 476
    Top = 12
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 504
    Top = 12
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY NOMECOR')
    Left = 532
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrGradesCorsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGradesCorsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY NOMETAM')
    Left = 560
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrGradesTamsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGradesTamsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Incluinovacompra1
    CanIns02 = Incluiitensnacompraatual1
    CanUpd01 = Alteracompraatual1
    CanDel01 = Excluicompraatual1
    CanDel02 = Excluiitematual1
    Left = 412
    Top = 12
  end
  object DsGradeKIts: TDataSource
    DataSet = QrGradeKIts
    Left = 316
    Top = 9
  end
  object QrGradeKIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, its.*'
      'FROM gradekits its'
      'LEFT JOIN grades gra ON gra.Codigo=its.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=its.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=its.Cor'
      'WHERE its.Codigo=:P0')
    Left = 288
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradeKItsNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrGradeKItsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrGradeKItsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrGradeKItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGradeKItsGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrGradeKItsCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrGradeKItsTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrGradeKItsQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
      DisplayFormat = '#,###,###0.000'
    end
    object QrGradeKItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradeKItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradeKItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradeKItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradeKItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradeKItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGradeKItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGradeKItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.Tam, pro.Cor, pro.Ativo, gki.Controle'
      'FROM produtos pro'
      'LEFT JOIN tamanhos  tam ON tam.Codigo=pro.Tam'
      'LEFT JOIN cores     cor ON cor.Codigo=pro.Cor'
      'LEFT JOIN gradekits gki ON gki.Grade=pro.Codigo '
      '   AND gki.Tam=pro.Tam '
      '   AND gki.Cor=pro.Cor'
      '   AND gki.Controle=:P0'
      'WHERE pro.Codigo=:P1'
      'AND pro.Ativo=1'
      '/*Para poder ordenar!!!*/'
      'ORDER BY tam.Nome, cor.Nome')
    Left = 588
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProdutosTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrProdutosCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 525
    Top = 193
  end
end
