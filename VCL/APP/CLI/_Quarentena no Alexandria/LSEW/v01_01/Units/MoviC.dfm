object FmMoviC: TFmMoviC
  Left = 469
  Top = 171
  Caption = 'PRD-COMPR-001 :: Compras de Mercadorias'
  ClientHeight = 496
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnItens: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 399
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfItem: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfItemClick
      end
      object Panel6: TPanel
        Left = 900
        Top = 1
        Width = 105
        Height = 46
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Sa'#237'da'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 2
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit13
      end
      object Label8: TLabel
        Left = 112
        Top = 4
        Width = 61
        Height = 13
        Caption = 'Data pedido:'
      end
      object Label11: TLabel
        Left = 184
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Data entrada:'
      end
      object Label12: TLabel
        Left = 256
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label22: TLabel
        Left = 556
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Total:'
        FocusControl = DBEdit10
      end
      object Label23: TLabel
        Left = 632
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Pago:'
        FocusControl = DBEdit11
      end
      object Label24: TLabel
        Left = 708
        Top = 4
        Width = 39
        Height = 13
        Caption = 'SALDO:'
        FocusControl = DBEdit12
      end
      object DBEdit13: TDBEdit
        Left = 8
        Top = 20
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMoviC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit4: TDBEdit
        Left = 112
        Top = 20
        Width = 68
        Height = 21
        DataField = 'DataPedi'
        DataSource = DsMoviC
        TabOrder = 1
      end
      object DBEdit5: TDBEdit
        Left = 184
        Top = 20
        Width = 68
        Height = 21
        DataField = 'DATAREAL_TXT'
        DataSource = DsMoviC
        TabOrder = 2
      end
      object DBEdit6: TDBEdit
        Left = 256
        Top = 20
        Width = 296
        Height = 21
        DataField = 'NOMEENTI'
        DataSource = DsMoviC
        TabOrder = 3
      end
      object DBEdit10: TDBEdit
        Left = 556
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Total'
        DataSource = DsMoviC
        TabOrder = 4
      end
      object DBEdit11: TDBEdit
        Left = 632
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Pago'
        DataSource = DsMoviC
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 708
        Top = 20
        Width = 72
        Height = 21
        DataField = 'SALDO'
        DataSource = DsMoviC
        TabOrder = 6
      end
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 49
      Width = 1006
      Height = 77
      Align = alTop
      DataSource = DsMovim
      Enabled = False
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEGRA'
          Title.Caption = 'Mercadoria'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Cor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtd'
          Title.Caption = 'Quant.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRECO'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Val'
          Title.Caption = 'Total'
          Visible = True
        end>
    end
    object GradeA: TStringGrid
      Left = 1
      Top = 251
      Width = 1006
      Height = 148
      Align = alBottom
      ColCount = 1
      DefaultColWidth = 18
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      TabOrder = 4
      OnClick = GradeAClick
      OnDrawCell = GradeADrawCell
      RowHeights = (
        18)
    end
    object Panel4: TPanel
      Left = 1
      Top = 188
      Width = 1006
      Height = 63
      Align = alBottom
      TabOrder = 0
      object Label13: TLabel
        Left = 8
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Mercadoria:'
      end
      object Label14: TLabel
        Left = 424
        Top = 4
        Width = 19
        Height = 13
        Caption = 'Cor:'
      end
      object Label15: TLabel
        Left = 624
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label16: TLabel
        Left = 696
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label17: TLabel
        Left = 772
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Pre'#231'o:'
      end
      object Label18: TLabel
        Left = 848
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Total:'
      end
      object LaStatus: TLabel
        Left = 923
        Top = 1
        Width = 82
        Height = 44
        Align = alRight
        Alignment = taCenter
        AutoSize = False
        Caption = 'Travado'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 707
      end
      object SpeedButton6: TSpeedButton
        Left = 398
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object EdGrade: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGradeChange
        DBLookupComboBox = CBGrade
        IgnoraDBLookupComboBox = False
      end
      object EdCorCod: TEdit
        Left = 424
        Top = 20
        Width = 32
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object EdCorNom: TEdit
        Left = 456
        Top = 20
        Width = 164
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
      object EdTamCod: TEdit
        Left = 624
        Top = 20
        Width = 32
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
      object EdTamNom: TEdit
        Left = 655
        Top = 20
        Width = 37
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object EdQtd: TEdit
        Left = 696
        Top = 20
        Width = 72
        Height = 21
        TabOrder = 6
        OnEnter = EdQtdEnter
        OnExit = EdQtdExit
      end
      object EdPrc: TEdit
        Left = 772
        Top = 20
        Width = 72
        Height = 21
        TabOrder = 7
        OnEnter = EdPrcEnter
        OnExit = EdPrcExit
      end
      object EdVal: TEdit
        Left = 848
        Top = 20
        Width = 72
        Height = 21
        Enabled = False
        TabOrder = 8
        OnEnter = EdValEnter
        OnExit = EdValExit
      end
      object CBGrade: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 333
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGrades
        TabOrder = 1
        dmkEditCB = EdGrade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object StaticText2: TStaticText
        Left = 1
        Top = 45
        Width = 1004
        Height = 17
        Align = alBottom
        Alignment = taCenter
        Caption = 
          'Clique na c'#233'lula correspondente da grade acima para definir a co' +
          'r e o tamanho.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
      end
    end
    object GradeC: TStringGrid
      Left = 316
      Top = 256
      Width = 361
      Height = 133
      ColCount = 2
      DefaultColWidth = 18
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 5
      Visible = False
      RowHeights = (
        18
        18)
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 399
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 5
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel7: TPanel
        Left = 900
        Top = 1
        Width = 105
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 820
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 196
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 112
        Top = 4
        Width = 61
        Height = 13
        Caption = 'Data pedido:'
      end
      object Label3: TLabel
        Left = 184
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Data entrada:'
      end
      object Label4: TLabel
        Left = 256
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label25: TLabel
        Left = 188
        Top = 44
        Width = 48
        Height = 13
        Caption = '[F4] Copia'
      end
      object SpeedButton5: TSpeedButton
        Left = 645
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TEdit
        Left = 8
        Top = 20
        Width = 100
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '00'
      end
      object EdDataPedi: TEdit
        Left = 112
        Top = 20
        Width = 68
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '000000'
        OnExit = EdDataPediExit
      end
      object EdDataReal: TEdit
        Left = 184
        Top = 20
        Width = 68
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '000000'
        OnExit = EdDataRealExit
        OnKeyDown = EdDataRealKeyDown
      end
      object EdFornece: TdmkEditCB
        Left = 256
        Top = 20
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 320
        Top = 20
        Width = 324
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTI'
        ListSource = DsFornece
        TabOrder = 4
        dmkEditCB = EdFornece
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 448
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PnControle: TPanel
      Left = 1
      Top = 399
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 363
        Height = 46
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
        ExplicitWidth = 26
        ExplicitHeight = 13
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 536
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtPagtos: TBitBtn
          Tag = 187
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pagtos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtPagtosClick
        end
        object BtItens: TBitBtn
          Tag = 290
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtCompra: TBitBtn
          Tag = 30
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Compra'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCompraClick
        end
      end
    end
    object PnData: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 112
        Top = 4
        Width = 61
        Height = 13
        Caption = 'Data pedido:'
      end
      object Label5: TLabel
        Left = 184
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Data entrada:'
      end
      object Label6: TLabel
        Left = 256
        Top = 4
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label19: TLabel
        Left = 556
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Total:'
        FocusControl = DBEdit7
      end
      object Label20: TLabel
        Left = 632
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Pago:'
        FocusControl = DBEdit8
      end
      object Label21: TLabel
        Left = 708
        Top = 4
        Width = 39
        Height = 13
        Caption = 'SALDO:'
        FocusControl = DBEdit9
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMoviC
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 112
        Top = 20
        Width = 68
        Height = 21
        DataField = 'DataPedi'
        DataSource = DsMoviC
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 184
        Top = 20
        Width = 68
        Height = 21
        DataField = 'DATAREAL_TXT'
        DataSource = DsMoviC
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 256
        Top = 20
        Width = 296
        Height = 21
        DataField = 'NOMEENTI'
        DataSource = DsMoviC
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 556
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Total'
        DataSource = DsMoviC
        TabOrder = 4
      end
      object DBEdit8: TDBEdit
        Left = 632
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Pago'
        DataSource = DsMoviC
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 708
        Top = 20
        Width = 72
        Height = 21
        DataField = 'SALDO'
        DataSource = DsMoviC
        TabOrder = 6
      end
    end
    object PnPagtos: TPanel
      Left = 1
      Top = 299
      Width = 1006
      Height = 100
      Align = alBottom
      TabOrder = 2
      object StaticText4: TStaticText
        Left = 1
        Top = 1
        Width = 1004
        Height = 17
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'Pagamentos'
        TabOrder = 0
      end
      object GridPagtos: TDBGrid
        Left = 1
        Top = 18
        Width = 1004
        Height = 81
        Align = alClient
        DataSource = DsPagtos
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SEQ'
            Title.Caption = 'N'#186
            Width = 22
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECARTEIRA'
            Title.Caption = 'Carteira'
            Width = 101
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID lancto.'
            Visible = True
          end>
      end
    end
    object PnDItens: TPanel
      Left = 1
      Top = 49
      Width = 1006
      Height = 100
      Align = alTop
      TabOrder = 3
      object StaticText1: TStaticText
        Left = 1
        Top = 1
        Width = 1004
        Height = 17
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'Itens'
        TabOrder = 0
      end
      object GradeItens: TDBGrid
        Left = 1
        Top = 18
        Width = 1004
        Height = 81
        Align = alClient
        DataSource = DsMovim
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NOMEGRA'
            Title.Caption = 'Mercadoria'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECOR'
            Title.Caption = 'Cor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMETAM'
            Title.Caption = 'Tamanho'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtd'
            Title.Caption = 'Quant.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRECO'
            Title.Caption = 'Pre'#231'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Val'
            Title.Caption = 'Total'
            Visible = True
          end>
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                              Compras de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 3
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsMoviC: TDataSource
    DataSet = QrMoviC
    Left = 260
    Top = 25
  end
  object QrMoviC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMoviCBeforeOpen
    AfterOpen = QrMoviCAfterOpen
    BeforeClose = QrMoviCBeforeClose
    AfterScroll = QrMoviCAfterScroll
    OnCalcFields = QrMoviCCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, moc.* '
      'FROM movic moc'
      'LEFT JOIN entidades ent ON ent.Codigo=moc.Fornece'
      'WHERE moc.Codigo > 0')
    Left = 232
    Top = 25
    object QrMoviCNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrMoviCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMoviCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMoviCDataPedi: TDateField
      FieldName = 'DataPedi'
    end
    object QrMoviCDataReal: TDateField
      FieldName = 'DataReal'
    end
    object QrMoviCFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrMoviCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMoviCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMoviCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMoviCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMoviCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMoviCDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrMoviCSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviCTotal: TFloatField
      FieldName = 'Total'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviCPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMoviCAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTI'
      'FROM entidades en'
      'WHERE en.Fornece1="V"'
      'ORDER BY NOMEENTI')
    Left = 580
    Top = 20
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForneceNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 608
    Top = 20
  end
  object QrMovim: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovimCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.Controle= :P0')
    Left = 292
    Top = 25
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrMovimNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrMovimNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrMovimControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMovimConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMovimGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMovimCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMovimTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrMovimQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrMovimVal: TFloatField
      FieldName = 'Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMovimDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMovimDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMovimUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMovimUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMovimDataPedi: TDateField
      FieldName = 'DataPedi'
      Required = True
    end
    object QrMovimDataReal: TDateField
      FieldName = 'DataReal'
      Required = True
    end
    object QrMovimPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimVen: TFloatField
      FieldName = 'Ven'
      Required = True
    end
    object QrMovimMotivo: TSmallintField
      FieldName = 'Motivo'
      Required = True
    end
    object QrMovimSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Required = True
    end
    object QrMovimSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Required = True
    end
    object QrMovimAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMovimAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsMovim: TDataSource
    DataSet = QrMovim
    Left = 320
    Top = 25
  end
  object PMCompra: TPopupMenu
    OnPopup = PMCompraPopup
    Left = 592
    Top = 456
    object Incluinovacompra1: TMenuItem
      Caption = '&Inclui nova compra'
      OnClick = Incluinovacompra1Click
    end
    object Alteracompraatual1: TMenuItem
      Caption = '&Altera compra atual'
      OnClick = Alteracompraatual1Click
    end
    object Excluicompraatual1: TMenuItem
      Caption = '&Exclui compra atual'
      OnClick = Excluicompraatual1Click
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 668
    Top = 456
    object Incluiitensnacompraatual1: TMenuItem
      Caption = '&Inclui / Altera itens na compra atual'
      OnClick = Incluiitensnacompraatual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 636
    Top = 20
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 664
    Top = 20
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY NOMECOR')
    Left = 696
    Top = 29
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrGradesCorsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGradesCorsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY NOMETAM')
    Left = 724
    Top = 29
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrGradesTamsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGradesTamsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.Tam, pro.Cor, pro.Ativo, mom.Conta'
      'FROM produtos pro'
      'LEFT JOIN tamanhos tam ON tam.Codigo=pro.Tam'
      'LEFT JOIN cores    cor ON cor.Codigo=pro.Cor'
      'LEFT JOIN movim    mom ON mom.Grade=pro.Codigo '
      '   AND mom.Tam=pro.Tam '
      '   AND mom.Cor=pro.Cor'
      '   AND mom.Controle=:P0'
      'WHERE pro.Codigo= :P1'
      'AND pro.Ativo=1'
      '/*Para poder ordenar!!!*/'
      'ORDER BY tam.Nome, cor.Nome')
    Left = 752
    Top = 29
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProdutosTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrProdutosCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
  end
  object QrMovim2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, mom.Cor, mom.Tam'
      'FROM movim mom'
      'WHERE mom.Controle= :P0')
    Left = 348
    Top = 25
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovim2Grade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMovim2Cor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMovim2Tam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.EstqQ, pro.EstqV'
      'FROM produtos pro'
      'WHERE pro.Codigo= :P0'
      'AND pro.Cor= :P1'
      'AND pro.Tam= :P2')
    Left = 568
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqEstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
    end
    object QrEstqEstqV: TFloatField
      FieldName = 'EstqV'
      Required = True
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ' +
        'la.FatID, '
      
        'la.ContaCorrente, la.Documento, la.Descricao, la.FatParcela, la.' +
        'FatNum, '
      'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, '
      'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEI,'
      'ca.Tipo CARTEIRATIPO'
      'FROM FTabLctALS la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI'
      'WHERE FatID=500'
      'AND FatNum=:P0'
      'ORDER BY la.FatParcela, la.Vencimento')
    Left = 536
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPagtosBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPagtosAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPagtosConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPagtosTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPagtosCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPagtosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 564
    Top = 401
  end
  object QrSumP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Debito'
      'FROM FTabLctALS'
      'WHERE FatID=500'
      'AND FatNum=:P0'
      '')
    Left = 592
    Top = 401
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object PMPagtos: TPopupMenu
    OnPopup = PMPagtosPopup
    Left = 746
    Top = 453
    object IncluiPagtos1: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiPagtos1Click
    end
    object ExcluiPagtos1: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiPagtos1Click
    end
  end
  object QrSumM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Val) Val'
      'FROM movim mom'
      'WHERE mom.Controle= :P0')
    Left = 620
    Top = 401
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumMVal: TFloatField
      FieldName = 'Val'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Incluinovacompra1
    CanIns02 = Incluiitensnacompraatual1
    CanIns03 = IncluiPagtos1
    CanUpd01 = Alteracompraatual1
    CanDel01 = Excluicompraatual1
    CanDel02 = Excluiitematual1
    CanDel03 = ExcluiPagtos1
    Left = 412
    Top = 12
  end
  object frxPedidosC: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40046.434945740740000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    Left = 660
    Top = 184
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMoviC
        DataSetName = 'frxDsMoviC'
      end
      item
        DataSet = frxDsMovim
        DataSetName = 'frxDsMovim'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 61.984283460000000000
        ParentFont = False
        Top = 150.803247000000000000
        Width = 699.213050000000000000
        RowCount = 1
        StartNewPage = True
        object Shape2: TfrxShapeView
          Left = -0.000018310000000000
          Top = 0.685065000000000000
          Width = 699.213050000000000000
          Height = 59.338621000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo51: TfrxMemoView
          Left = 78.992177000000000000
          Top = 20.338621000000000000
          Width = 613.417719000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 570.709030000000000000
          Top = 1.440971000000000000
          Width = 121.700866000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 514.016080000000000000
          Top = 1.440971000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 80.637848000000000000
          Top = 1.440971000000000000
          Width = 433.512091000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMoviC."NOMEENTI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 8.826778000000000000
          Top = 1.440971000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Destinat'#225'rio:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 297.826964000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMovim
        DataSetName = 'frxDsMovim'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = -0.000000240000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          DataField = 'NOMEGRA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMEGRA"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 619.842920000000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovim."Val"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 226.771653300000000000
          Width = 148.535406950000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOR'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMECOR"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 375.307329000000000000
          Width = 168.566915950000000000
          Height = 18.897650000000000000
          DataField = 'NOMETAM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMETAM"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 543.874367000000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovim."Qtd"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 74.078788000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Top = 0.377953000000000000
          Width = 699.213050000000000000
          Height = 69.165399000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Picture1: TfrxPictureView
          Left = 10.960637000000000000
          Top = 1.133859000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 179.149722000000000000
          Top = 1.133859000000000000
          Width = 575.338590000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 571.086983000000000000
          Top = 13.984261000000000000
          Width = 122.928849000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 179.149722000000000000
          Top = 16.251979000000000000
          Width = 391.559308000000000000
          Height = 48.755912590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 44.976407000000000000
        Top = 232.819048000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMoviC."Codigo"'
        object Memo11: TfrxMemoView
          Top = 26.078744060000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 543.874367000000000000
          Top = 26.078744060000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 226.771800000000000000
          Top = 26.078744060000000000
          Width = 148.535406950000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'COR')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 375.307329000000000000
          Top = 26.078744060000000000
          Width = 168.566915950000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'TAMANHO')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 619.842920000000000000
          Top = 26.078757000000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'PRE'#199'O')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Top = 1.889765000000000000
          Width = 41.574768980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 42.196877000000000000
          Top = 1.889765000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviC."Codigo"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 304.630118000000000000
          Top = 1.889765000000000000
          Width = 65.763760980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA VENDA:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 139.086704000000000000
          Top = 1.889765000000000000
          Width = 67.653525980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA PEDIDO:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 370.015987000000000000
          Top = 1.889765000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviC."DataReal"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 206.362338000000000000
          Top = 1.889765000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviC."DataPedi"]')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 31.188992000000000000
        Top = 336.756123000000000000
        Width = 699.213050000000000000
        object Memo41: TfrxMemoView
          Left = 543.873974010000000000
          Top = 7.000000000000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovim."Qtd">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 619.842920000000000000
          Top = 7.000000000000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovim."Val">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 7.000000000000000000
          Width = 543.873976450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'SUBTOTAL')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 425.575078000000000000
        Width = 699.213050000000000000
        object Memo47: TfrxMemoView
          Top = 0.377953000000000000
          Width = 619.842529450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 619.842920240000000000
          Top = 0.377953000000000000
          Width = 75.968504180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviC."Total"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsMoviC: TfrxDBDataset
    UserName = 'frxDsMoviC'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEENTI=NOMEENTI'
      'Codigo=Codigo'
      'Controle=Controle'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'Fornece=Fornece'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DATAREAL_TXT=DATAREAL_TXT'
      'SALDO=SALDO'
      'Total=Total'
      'Pago=Pago'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSource = DsMoviC
    BCDToCurrency = False
    Left = 688
    Top = 184
  end
  object frxDsMovim: TfrxDBDataset
    UserName = 'frxDsMovim'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEGRA=NOMEGRA'
      'NOMETAM=NOMETAM'
      'NOMECOR=NOMECOR'
      'Controle=Controle'
      'Conta=Conta'
      'Grade=Grade'
      'Cor=Cor'
      'Tam=Tam'
      'Qtd=Qtd'
      'Val=Val'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'PRECO=PRECO'
      'Ven=Ven'
      'Motivo=Motivo'
      'SALDOQTD=SALDOQTD'
      'SALDOVAL=SALDOVAL'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSource = DsMovim
    BCDToCurrency = False
    Left = 716
    Top = 184
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    DataSet = QrCli
    BCDToCurrency = False
    Left = 772
    Top = 184
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 744
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrCliUF: TLargeintField
      FieldName = 'UF'
    end
    object QrCliLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TLargeintField
      FieldName = 'CEP'
    end
  end
end
