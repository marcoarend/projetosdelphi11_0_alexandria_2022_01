object FmGradesCodBarra: TFmGradesCodBarra
  Left = 339
  Top = 185
  Caption = 'PRD-GRADE-005 :: Impress'#227'o de C'#243'digo de Barras'
  ClientHeight = 450
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 391
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 837
      Top = 1
      Width = 137
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Impress'#227'o de C'#243'digo de Barras'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 973
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 332
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 15
      Top = 5
      Width = 72
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Mercadoria:'
    end
    object Label2: TLabel
      Left = 478
      Top = 5
      Width = 24
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cor:'
    end
    object Label3: TLabel
      Left = 748
      Top = 5
      Width = 61
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Tamanho:'
    end
    object Label4: TLabel
      Left = 15
      Top = 59
      Width = 169
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Configura'#231#227'o de impress'#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 448
      Top = 79
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      OnClick = SpeedButton1Click
    end
    object Label5: TLabel
      Left = 15
      Top = 113
      Width = 87
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Espa'#231'amento:'
    end
    object Label6: TLabel
      Left = 103
      Top = 113
      Width = 85
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Coluna inicial: '
    end
    object Label7: TLabel
      Left = 192
      Top = 113
      Width = 72
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Linha inicial:'
    end
    object LaRepeticoes: TLabel
      Left = 281
      Top = 113
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Repeti'#231#245'es:'
    end
    object Label8: TLabel
      Left = 15
      Top = 256
      Width = 164
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome e EAN13 do produto:'
      FocusControl = DBEdit1
    end
    object Label9: TLabel
      Left = 478
      Top = 59
      Width = 95
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Lista de pre'#231'os:'
    end
    object Label10: TLabel
      Left = 817
      Top = 256
      Width = 39
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pre'#231'o:'
    end
    object LaMunici: TLabel
      Left = 674
      Top = 166
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cidade:'
    end
    object LaUF: TLabel
      Left = 863
      Top = 166
      Width = 21
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'UF:'
    end
    object LaEntidade: TLabel
      Left = 15
      Top = 167
      Width = 57
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Entidade:'
    end
    object SpeedButton2: TSpeedButton
      Left = 646
      Top = 187
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      OnClick = SpeedButton2Click
    end
    object EdGra: TdmkEditCB
      Left = 15
      Top = 25
      Width = 71
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraChange
      DBLookupComboBox = CBGra
      IgnoraDBLookupComboBox = False
    end
    object CBGra: TdmkDBLookupComboBox
      Left = 89
      Top = 25
      Width = 385
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsGra
      TabOrder = 1
      dmkEditCB = EdGra
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCor: TdmkEditCB
      Left = 478
      Top = 25
      Width = 71
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdCorChange
      DBLookupComboBox = CBCor
      IgnoraDBLookupComboBox = False
    end
    object CBCor: TdmkDBLookupComboBox
      Left = 551
      Top = 25
      Width = 194
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCor
      TabOrder = 3
      dmkEditCB = EdCor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTam: TdmkEditCB
      Left = 748
      Top = 25
      Width = 72
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdTamChange
      DBLookupComboBox = CBTam
      IgnoraDBLookupComboBox = False
    end
    object CBTam: TdmkDBLookupComboBox
      Left = 822
      Top = 25
      Width = 115
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTam
      TabOrder = 5
      dmkEditCB = EdTam
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdConfImp: TdmkEditCB
      Left = 15
      Top = 79
      Width = 71
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConfImp
      IgnoraDBLookupComboBox = False
    end
    object CBConfImp: TdmkDBLookupComboBox
      Left = 89
      Top = 79
      Width = 355
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPage
      TabOrder = 7
      dmkEditCB = EdConfImp
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEspacamento: TdmkEdit
      Left = 15
      Top = 133
      Width = 83
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '3'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 3
      ValWarn = False
    end
    object EdColIni: TdmkEdit
      Left = 103
      Top = 133
      Width = 84
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdLinIni: TdmkEdit
      Left = 192
      Top = 133
      Width = 84
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdRepeticoes: TdmkEdit
      Left = 281
      Top = 133
      Width = 83
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object Progress: TProgressBar
      Left = 15
      Top = 231
      Width = 917
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 22
    end
    object CkInfoCod: TCheckBox
      Left = 734
      Top = 118
      Width = 188
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Informa c'#243'digo de cadastro.'
      Checked = True
      State = cbChecked
      TabOrder = 15
    end
    object DBEdit1: TDBEdit
      Left = 15
      Top = 276
      Width = 138
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'EAN13'
      DataSource = DsProduto
      TabOrder = 23
    end
    object CkGrade: TCheckBox
      Left = 734
      Top = 139
      Width = 70
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grade.'
      TabOrder = 16
    end
    object CkDesign: TCheckBox
      Left = 827
      Top = 138
      Width = 105
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Design mode.'
      TabOrder = 17
    end
    object DBEdit2: TDBEdit
      Left = 153
      Top = 276
      Width = 661
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'NOMEPROD'
      DataSource = DsProduto
      TabOrder = 24
    end
    object EdGraCusPrc: TdmkEditCB
      Left = 478
      Top = 79
      Width = 71
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdGraCusPrcChange
      DBLookupComboBox = CBGraCusPrc
      IgnoraDBLookupComboBox = False
    end
    object CBGraCusPrc: TdmkDBLookupComboBox
      Left = 551
      Top = 79
      Width = 386
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGraCusPrc
      TabOrder = 9
      dmkEditCB = EdGraCusPrc
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCustoPreco: TdmkEdit
      Left = 817
      Top = 276
      Width = 115
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 25
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object RGDescricao: TRadioGroup
      Left = 369
      Top = 108
      Width = 361
      Height = 61
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Descri'#231#227'o: '
      Columns = 2
      ItemIndex = 2
      Items.Strings = (
        'Nada'
        'Refer'#234'ncia'
        'Nome / cor / tam.'
        'Cod. ent. / Cidade / UF')
      TabOrder = 14
      OnClick = RGDescricaoClick
    end
    object EdMunici: TdmkEdit
      Left = 671
      Top = 187
      Width = 184
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 10
      TabOrder = 20
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdUF: TdmkEdit
      Left = 863
      Top = 187
      Width = 74
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 2
      TabOrder = 21
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 89
      Top = 187
      Width = 555
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsEntidade
      TabOrder = 19
      dmkEditCB = EdEntidade
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEntidade: TdmkEditCB
      Left = 15
      Top = 187
      Width = 71
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 18
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
    end
  end
  object DsTam: TDataSource
    DataSet = QrTam
    Left = 157
    Top = 9
  end
  object QrTam: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tamanhos'
      'ORDER BY Nome')
    Left = 129
    Top = 9
    object QrTamCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'tamanhos.Codigo'
    end
    object QrTamNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsCor: TDataSource
    DataSet = QrCor
    Left = 97
    Top = 9
  end
  object QrCor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cores'
      'ORDER BY Nome')
    Left = 69
    Top = 9
    object QrCorCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cores.Codigo'
    end
    object QrCorNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cores.Nome'
    end
  end
  object DsGra: TDataSource
    DataSet = QrGra
    Left = 37
    Top = 9
  end
  object QrGra: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM grades'
      'ORDER BY Nome')
    Left = 9
    Top = 9
    object QrGraCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'grades.Codigo'
    end
    object QrGraCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grades.Nome'
      Size = 30
    end
  end
  object QrPage: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pages')
    Left = 600
    Top = 12
    object QrPageNome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrPageObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrPageCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPagePagCol: TIntegerField
      FieldName = 'PagCol'
    end
    object QrPagePagLin: TIntegerField
      FieldName = 'PagLin'
    end
    object QrPagePagLef: TIntegerField
      FieldName = 'PagLef'
    end
    object QrPagePagTop: TIntegerField
      FieldName = 'PagTop'
    end
    object QrPagePagWid: TIntegerField
      FieldName = 'PagWid'
    end
    object QrPagePagHei: TIntegerField
      FieldName = 'PagHei'
    end
    object QrPagePagGap: TIntegerField
      FieldName = 'PagGap'
    end
    object QrPageBanCol: TIntegerField
      FieldName = 'BanCol'
    end
    object QrPageBanLin: TIntegerField
      FieldName = 'BanLin'
    end
    object QrPageBanWid: TIntegerField
      FieldName = 'BanWid'
    end
    object QrPageBanHei: TIntegerField
      FieldName = 'BanHei'
    end
    object QrPageBanLef: TIntegerField
      FieldName = 'BanLef'
    end
    object QrPageBanTop: TIntegerField
      FieldName = 'BanTop'
    end
    object QrPageBanGap: TIntegerField
      FieldName = 'BanGap'
    end
    object QrPageOrient: TIntegerField
      FieldName = 'Orient'
    end
    object QrPageDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPageDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPageUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrPageUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrPageLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPageColWid: TIntegerField
      FieldName = 'ColWid'
    end
    object QrPageBa2Hei: TIntegerField
      FieldName = 'Ba2Hei'
    end
    object QrPageFoSize: TSmallintField
      FieldName = 'FoSize'
    end
    object QrPageMemHei: TIntegerField
      FieldName = 'MemHei'
    end
    object QrPageEspaco: TSmallintField
      FieldName = 'Espaco'
    end
    object QrPageBarLef: TIntegerField
      FieldName = 'BarLef'
    end
    object QrPageBarTop: TIntegerField
      FieldName = 'BarTop'
    end
    object QrPageBarWid: TIntegerField
      FieldName = 'BarWid'
    end
    object QrPageBarHei: TIntegerField
      FieldName = 'BarHei'
    end
  end
  object DsPage: TDataSource
    DataSet = QrPage
    Left = 628
    Top = 12
  end
  object QrMinhaEtiq: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM minhaetiq'
      'ORDER BY Nome, Conta')
    Left = 656
    Top = 12
    object QrMinhaEtiqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'minhaetiq.Nome'
      Size = 100
    end
    object QrMinhaEtiqConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'minhaetiq.Conta'
    end
    object QrMinhaEtiqTexto1: TWideStringField
      FieldName = 'Texto1'
      Origin = 'minhaetiq.Texto1'
      Size = 100
    end
    object QrMinhaEtiqTexto2: TWideStringField
      FieldName = 'Texto2'
      Origin = 'minhaetiq.Texto2'
      Size = 100
    end
    object QrMinhaEtiqTexto3: TWideStringField
      FieldName = 'Texto3'
      Origin = 'minhaetiq.Texto3'
      Size = 100
    end
    object QrMinhaEtiqTexto4: TWideStringField
      FieldName = 'Texto4'
      Origin = 'minhaetiq.Texto4'
      Size = 100
    end
  end
  object QrProduto: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrProdutoAfterScroll
    OnCalcFields = QrProdutoCalcFields
    SQL.Strings = (
      'SELECT prd.Controle, gra.Nome NOMEGRA,'
      'cor.Nome NOMECOR, tam.Nome NOMETAM,'
      'ggv.CustoPreco'
      'FROM produtos prd'
      'LEFT JOIN grades   gra ON gra.Codigo=prd.Codigo'
      'LEFT JOIN cores    cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.tam'
      
        'LEFT JOIN gragruval ggv ON ggv.GraGruX=prd.Controle AND ggv.GraC' +
        'usPrc=:P0'
      'WHERE prd.Codigo=:P1'
      'AND prd.Cor=:P2'
      'AND prd.Tam=:P3')
    Left = 436
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrProdutoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProdutoNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrProdutoNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrProdutoNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrProdutoNOMEPROD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPROD'
      Size = 255
      Calculated = True
    end
    object QrProdutoEAN13: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN13'
      Size = 13
      Calculated = True
    end
    object QrProdutoCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
  object DsProduto: TDataSource
    DataSet = QrProduto
    Left = 464
    Top = 128
  end
  object frxEtiquetas: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.525010486100000000
    ReportOptions.LastChange = 39721.525010486100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure BarCodeOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  BarCode.Visible := <VARF_BARCODE_VISI>;                       ' +
        '                         '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxEtiquetasGetValue
    Left = 684
    Top = 12
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMinhaEtiq
        DataSetName = 'frxDsMinhaEtiq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      DataSet = frxDsMinhaEtiq
      DataSetName = 'frxDsMinhaEtiq'
    end
  end
  object frxDsMinhaEtiq: TfrxDBDataset
    UserName = 'frxDsMinhaEtiq'
    CloseDataSource = False
    DataSet = QrMinhaEtiq
    BCDToCurrency = False
    Left = 656
    Top = 40
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 712
    Top = 12
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 252
    Top = 9
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'ORDER BY Nome'
      '')
    Left = 548
    Top = 100
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 576
    Top = 100
  end
  object QrGraGruVal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Cor, prd.Tam, ggv.CustoPreco'
      'FROM gragruval ggv'#13
      'LEFT JOIN produtos prd ON prd.Controle=ggv.GraGruX'
      'WHERE ggv.Codigo=:P0'
      'AND ggv.GraCusPrc=:P1')
    Left = 608
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGruValCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrGraGruValTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      Required = True
    end
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTI'
      'FROM entidades en'
      'ORDER BY NOMEENTI')
    Left = 189
    Top = 9
    object QrEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadeNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsEntidade: TDataSource
    DataSet = QrEntidade
    Left = 217
    Top = 9
  end
end
