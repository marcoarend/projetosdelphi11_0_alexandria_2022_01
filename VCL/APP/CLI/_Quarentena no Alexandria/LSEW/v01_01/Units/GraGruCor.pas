unit GraGruCor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, MyDBCheck, DBCtrls, UnGOTOy,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu,
  dmkLabel, Mask, dmkGeral, UnInternalConsts, Variants, UnDmkEnums;

type
  TFmGraGruCor = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    PnEdita: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnNiveis: TPanel;
    Label3: TLabel;
    dmkEdCBGradesCors: TdmkEditCB;
    dmkCBGradesCors: TdmkDBLookupComboBox;
    dmkEdControle: TdmkEdit;
    Label12: TLabel;
    QrGradesCors: TmySQLQuery;
    DsGradesCors: TDataSource;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsNome: TWideStringField;
    SpeedButton2: TSpeedButton;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruCor: TFmGraGruCor;

implementation

uses Module, UMySQLModule, UnMySQLCuringa, GraGru, Cores, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruCor.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Cor: Integer;
begin
  Codigo   := FmGraGru.QrGradesCodigo.Value;
  Cor := dmkEdCBGradesCors.ValueVariant;
  //
  // Localiza se a cor j� foi incluida
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Controle');
  QrLoc.SQL.Add('FROM gradescors');
  QrLoc.SQL.Add('WHERE Codigo=:P0');
  QrLoc.SQL.Add('AND Cor=:P1');
  QrLoc.Params[0].AsInteger := Codigo;
  QrLoc.Params[1].AsInteger := Cor;
  QrLoc.Open;
  //
  if QrLoc.RecordCount > 0 then
  begin
    Application.MessageBox('Esta cor j� foi adicionada.', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdCBGradesCors.SetFocus;
    Exit;
  end;
  //
  if Cor = 0 then
  begin
    Application.MessageBox('Defina a cor.', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdCBGradesCors.SetFocus;
    Exit;
  end;
  Controle := UMyMod.BuscaEmLivreY_Def_Old('gradescors', 'Controle', LaTipo.Caption, FmGraGru.QrGradesCorsControle.Value);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'gradescors', False,
  [
    'Cor', 'Codigo'
  ], ['Controle'], [
    dmkEdCBGradesCors.ValueVariant, Codigo
  ], [Controle]) then
  begin
    FmGraGru.QrGradesCors.Close;
    FmGraGru.QrGradesCors.Open;
    VAR_COD := Controle;
    //
    Application.MessageBox('Inclus�o conclu�da', 'Aviso', MB_OK+MB_ICONINFORMATION);
    dmkEdCBGradesCors.ValueVariant := 0;
    dmkCBGradesCors.KeyValue       := NULL;
    dmkEdCBGradesCors.SetFocus;
  end;
end;

procedure TFmGraGruCor.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := FmGraGru.QrGradesCorsControle.Value;
  Close;
end;

procedure TFmGraGruCor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    dmkEdCBGradesCors.ValueVariant := 0;
    dmkCBGradesCors.KeyValue       := NULL;
  end else
  begin
    dmkEdControle.ValueVariant     := FmGraGru.QrGradesCorsControle.Value;
    dmkEdCBGradesCors.ValueVariant := FmGraGru.QrGradesCorsCor.Value;
    dmkCBGradesCors.KeyValue       := FmGraGru.QrGradesCorsCor.Value;
  end;
  dmkEdCBGradesCors.SetFocus;
end;

procedure TFmGraGruCor.FormCreate(Sender: TObject);
begin
  QrGradesCors.Open;
end;

procedure TFmGraGruCor.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGraGruCor.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCores, FmCores, afmoNegarComAviso) then
  begin
    FmCores.ShowModal;
    FmCores.Destroy;
    //
    if VAR_COD > 0 then
    begin
      QrGradesCors.Close;
      QrGradesCors.Open;
      //
      dmkEdCBGradesCors.ValueVariant := VAR_COD;
      dmkCBGradesCors.KeyValue       := VAR_COD;
    end;
  end;
end;

end.


