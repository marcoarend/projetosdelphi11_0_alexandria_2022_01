unit OpcoesLeSew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DBCtrls, Db, mySQLDbTables,
  dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkGeral, dmkPermissoes, ClipBrd,
  ExtDlgs, Mask, dmkDBEdit, dmkCheckBox, dmkRadioGroup, UnDmkProcFunc, UnDmkEnums;

type
  TFmOpcoesLeSew = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas1: TDataSource;
    Panel1: TPanel;
    DsContas2: TDataSource;
    DsContas3: TDataSource;
    DsContas4: TDataSource;
    DsContas5: TDataSource;
    DsContas6: TDataSource;
    QrCentroCusto: TmySQLQuery;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    DsCC1: TDataSource;
    DsCC2: TDataSource;
    DsCC3: TDataSource;
    DsCC4: TDataSource;
    QrCxaPgtProf: TmySQLQuery;
    DsCxaPgtProf: TDataSource;
    QrCxaPgtProfCodigo: TIntegerField;
    QrCxaPgtProfNome: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    QrCiclListPrd: TmySQLQuery;
    DsCiclListPrd: TDataSource;
    QrCiclListPrdCodigo: TIntegerField;
    QrCiclListPrdNome: TWideStringField;
    QrCiclGradeD: TmySQLQuery;
    DsCiclGradeD: TDataSource;
    QrPtosGradeD: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsPtosGradeD: TDataSource;
    QrContas7: TmySQLQuery;
    DsContas7: TDataSource;
    QrContas7Codigo: TIntegerField;
    QrContas7Nome: TWideStringField;
    QrStatus2: TmySQLQuery;
    DsStatus2: TDataSource;
    ColorDialog1: TColorDialog;
    QrStatus1: TmySQLQuery;
    DsStatus1: TDataSource;
    DsStatus3: TDataSource;
    QrStatus3: TmySQLQuery;
    QrStatus1Codigo: TIntegerField;
    QrStatus1Nome: TWideStringField;
    QrStatus2Codigo: TIntegerField;
    QrStatus2Nome: TWideStringField;
    QrStatus3Codigo: TIntegerField;
    QrStatus3Nome: TWideStringField;
    OpenPictureDialog1: TOpenPictureDialog;
    QrCiclCliInt: TmySQLQuery;
    DsCiclCliInt: TDataSource;
    QrCiclGradeDCodigo: TIntegerField;
    QrCiclGradeDNome: TWideStringField;
    QrCiclCliIntCodigo: TIntegerField;
    QrCiclCliIntCliente: TIntegerField;
    QrCiclCliIntNOMEENTI: TWideStringField;
    QrVdaCart: TmySQLQuery;
    DsVdaCart: TDataSource;
    QrVdaCartCodigo: TIntegerField;
    QrVdaCartNome: TWideStringField;
    QrVdaCont: TmySQLQuery;
    DsVdaCont: TDataSource;
    QrVdaContCodigo: TIntegerField;
    QrVdaContNome: TWideStringField;
    QrVdaConPgto: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVdaConPgto: TDataSource;
    QrStatus4: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsStatus4: TDataSource;
    QrLinDepto: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    DsLinDepto: TDataSource;
    QrVPGraPrc: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField5: TWideStringField;
    DsVPGraPrc: TDataSource;
    DsLinGraPrc: TDataSource;
    QrLinGraPrc: TmySQLQuery;
    IntegerField6: TIntegerField;
    StringField6: TWideStringField;
    QrLinCPgto: TmySQLQuery;
    IntegerField7: TIntegerField;
    StringField7: TWideStringField;
    DsLinCPgto: TDataSource;
    QrLinConta: TmySQLQuery;
    IntegerField8: TIntegerField;
    StringField8: TWideStringField;
    DsLinConta: TDataSource;
    DsLinCart: TDataSource;
    QrLinCart: TmySQLQuery;
    IntegerField9: TIntegerField;
    StringField9: TWideStringField;
    QrCartas1: TmySQLQuery;
    DsCartas1: TDataSource;
    QrCartas2: TmySQLQuery;
    DsCartas2: TDataSource;
    QrCartas2Codigo: TIntegerField;
    QrCartas1Codigo: TIntegerField;
    QrCartas1Nome: TWideStringField;
    QrCartas2Nome: TWideStringField;
    QrPrdGradeD: TmySQLQuery;
    IntegerField10: TIntegerField;
    StringField10: TWideStringField;
    DsPrdGradeD: TDataSource;
    QrAvaGradeD: TmySQLQuery;
    IntegerField11: TIntegerField;
    StringField11: TWideStringField;
    DsAvaGradeD: TDataSource;
    DsMotivo: TDataSource;
    QrMotivo: TmySQLQuery;
    QrMotivoCodigo: TIntegerField;
    QrMotivoNome: TWideStringField;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label0: TLabel;
    EdCtaEmpPrfC: TdmkEditCB;
    CBCtaEmpPrfC: TdmkDBLookupComboBox;
    CBCtaEmpPrfD: TdmkDBLookupComboBox;
    EdCtaEmpPrfD: TdmkEditCB;
    EdCtaComProm: TdmkEditCB;
    CBCtaComProm: TdmkDBLookupComboBox;
    EdCtaComProf: TdmkEditCB;
    CBCtaComProf: TdmkDBLookupComboBox;
    EdCta13oProm: TdmkEditCB;
    CBCta13oProm: TdmkDBLookupComboBox;
    EdCta13oProf: TdmkEditCB;
    CBCta13oProf: TdmkDBLookupComboBox;
    EdNome13: TdmkEdit;
    TabSheet6: TTabSheet;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EdCCuPromCom: TdmkEditCB;
    CBCCuPromCom: TdmkDBLookupComboBox;
    CBCCuPromDTS: TdmkDBLookupComboBox;
    EdCCuPromDTS: TdmkEditCB;
    CBCCuProfCom: TdmkDBLookupComboBox;
    EdCCuProfDTS: TdmkEditCB;
    CBCCuProfDTS: TdmkDBLookupComboBox;
    EdCCuProfCom: TdmkEditCB;
    TabSheet7: TTabSheet;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdCxaPgtProf: TdmkEditCB;
    CBCxaPgtProf: TdmkDBLookupComboBox;
    EdCtaPagProf: TdmkEditCB;
    CBCtaPagProf: TdmkDBLookupComboBox;
    EdTxtPgtProf: TEdit;
    TabSheet8: TTabSheet;
    Label14: TLabel;
    Label15: TLabel;
    Label30: TLabel;
    EdCBDepto: TdmkEditCB;
    CBDepto: TdmkDBLookupComboBox;
    EdCBListPrd: TdmkEditCB;
    CBListPrd: TdmkDBLookupComboBox;
    EdCiclCliInt: TdmkEditCB;
    CBCiclCliInt: TdmkDBLookupComboBox;
    Label16: TLabel;
    EdDeptoPto: TdmkEditCB;
    CBDeptoPto: TdmkDBLookupComboBox;
    Label18: TLabel;
    CBCtaPtoVda: TdmkDBLookupComboBox;
    EdCtaPtoVda: TdmkEditCB;
    EdTxtVdaPto: TdmkEdit;
    Label17: TLabel;
    Label36: TLabel;
    EdLinDepto: TdmkEditCB;
    CBLinDepto: TdmkDBLookupComboBox;
    Label37: TLabel;
    EdLinGraPrc: TdmkEditCB;
    CBLinGraPrc: TdmkDBLookupComboBox;
    Label39: TLabel;
    CBLinCart: TdmkDBLookupComboBox;
    EdLinCart: TdmkEditCB;
    Label40: TLabel;
    EdLinConta: TdmkEditCB;
    CBLinConta: TdmkDBLookupComboBox;
    Label41: TLabel;
    CBLinCPgto: TdmkDBLookupComboBox;
    EdLinCPgto: TdmkEditCB;
    Label42: TLabel;
    EdLinTxt: TdmkEdit;
    Label43: TLabel;
    PageControl3: TPageControl;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    GroupBox2: TGroupBox;
    Label21: TLabel;
    Label22: TLabel;
    SpeedButton1: TSpeedButton;
    EdStatus2: TdmkEditCB;
    CBStatus2: TdmkDBLookupComboBox;
    EdCor2: TdmkEdit;
    GroupBox1: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    SpeedButton6: TSpeedButton;
    EdStatus1: TdmkEditCB;
    CBStatus1: TdmkDBLookupComboBox;
    EdCor1: TdmkEdit;
    GroupBox3: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    SpeedButton2: TSpeedButton;
    EdStatus3: TdmkEditCB;
    CBStatus3: TdmkDBLookupComboBox;
    EdCor3: TdmkEdit;
    GroupBox4: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    SpeedButton3: TSpeedButton;
    EdStatus4: TdmkEditCB;
    CBStatus4: TdmkDBLookupComboBox;
    EdCor4: TdmkEdit;
    Label27: TLabel;
    EdTxtVdaPro: TdmkEdit;
    EdEndRastre: TdmkEdit;
    LaEndRastre: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label28: TLabel;
    EdTxtVdaEsq: TdmkEdit;
    Label31: TLabel;
    EdVdaCart: TdmkEditCB;
    CBVdaCart: TdmkDBLookupComboBox;
    Label33: TLabel;
    EdVdaConPgto: TdmkEditCB;
    CBVdaConPgto: TdmkDBLookupComboBox;
    Label52: TLabel;
    CBPrdGradeD: TdmkDBLookupComboBox;
    EdPrdGradeD: TdmkEditCB;
    EdVPGraPrc: TdmkEditCB;
    CBVPGraPrc: TdmkDBLookupComboBox;
    Label38: TLabel;
    CBVdaCont: TdmkDBLookupComboBox;
    EdVdaCont: TdmkEditCB;
    Label32: TLabel;
    Label29: TLabel;
    EdLogoMoviV: TdmkEdit;
    LaTxtVdaPro: TLabel;
    Label44: TLabel;
    EdPrdFoto: TdmkEdit;
    Label45: TLabel;
    EdPrdFotoTam: TdmkEdit;
    CkPrdFotoWeb: TCheckBox;
    SpeedButton4: TSpeedButton;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    GroupBox5: TGroupBox;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label51: TLabel;
    EdWeb_Raiz: TdmkEdit;
    EdWeb_FTPu: TdmkEdit;
    EdWeb_FTPh: TdmkEdit;
    EdWeb_FTPs: TdmkEdit;
    GroupBox6: TGroupBox;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    CkProxUse: TdmkCheckBox;
    CkProxBAut: TdmkCheckBox;
    EdProxPass: TdmkEdit;
    EdProxUser: TdmkEdit;
    EdProxServ: TdmkEdit;
    EdProxPort: TdmkEdit;
    Label64: TLabel;
    EdWebLogo: TdmkEdit;
    Label49: TLabel;
    EdWeb_MyURL: TdmkEdit;
    GroupBox7: TGroupBox;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    RGWeb_MySQL: TRadioGroup;
    EdWeb_Host: TdmkEdit;
    EdWeb_DB: TdmkEdit;
    EdWeb_Pwd: TdmkEdit;
    EdWeb_User: TdmkEdit;
    Label50: TLabel;
    Label57: TLabel;
    EdAvaGradeD: TdmkEditCB;
    CBAvaGradeD: TdmkDBLookupComboBox;
    Label54: TLabel;
    CBAvaTexApr: TdmkDBLookupComboBox;
    EdAvaTexApr: TdmkEditCB;
    Label55: TLabel;
    EdAvaTexRep: TdmkEditCB;
    CBAvaTexRep: TdmkDBLookupComboBox;
    CBAvaMotivo: TdmkDBLookupComboBox;
    EdAvaMotivo: TdmkEditCB;
    Label62: TLabel;
    Label56: TLabel;
    EdAvaDir: TdmkEdit;
    Label53: TLabel;
    EdAvaFoto: TdmkEdit;
    EdAvaLucro: TdmkEdit;
    Label63: TLabel;
    Label69: TLabel;
    EdMoviVCaPag: TdmkEditCB;
    CBMoviVCaPag: TdmkDBLookupComboBox;
    QrCartMoviConf: TmySQLQuery;
    IntegerField12: TIntegerField;
    StringField12: TWideStringField;
    DsCartMoviConf: TDataSource;
    SpeedButton5: TSpeedButton;
    TabSheet13: TTabSheet;
    GroupBox8: TGroupBox;
    Label70: TLabel;
    EdValCur: TdmkEdit;
    EdValCurPrd: TdmkEdit;
    Label71: TLabel;
    TabSheet14: TTabSheet;
    GroupBox9: TGroupBox;
    Label72: TLabel;
    EdMoviCCons: TdmkEdit;
    Label73: TLabel;
    Label74: TLabel;
    EdAvaEmpresa: TdmkEditCB;
    CBAvaEmpresa: TdmkDBLookupComboBox;
    QrAvaEmpresa: TmySQLQuery;
    DsAvaEmpresa: TDataSource;
    QrAvaEmpresaCodigo: TIntegerField;
    QrAvaEmpresaNOMEENTI: TWideStringField;
    RGFatSEtqKit: TdmkRadioGroup;
    RGFatSEtqLin: TdmkRadioGroup;
    BtTestar: TBitBtn;
    BitBtn1: TBitBtn;
    CkWeb_FTPpassivo: TCheckBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure LaEndRastreClick(Sender: TObject);
    procedure LaTxtVdaProClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdLogoMoviVKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton3Click(Sender: TObject);
    procedure Label43Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
    FmOpcoesLeSew: TFmOpcoesLeSew;

implementation

uses Module, UnInternalConsts, UMySQLModule, UnMyObjects, DmkDAC_PF, UnFTP;

{$R *.DFM}

procedure TFmOpcoesLeSew.BitBtn1Click(Sender: TObject);
const
  Avisa = True;
begin
  UFTP.TestaConexaoFTP(Self, EdWeb_FTPh.Text, EdWeb_FTPu.Text, EdWeb_FTPs.Text,
    CkWeb_FTPpassivo.Checked, Avisa);
end;

procedure TFmOpcoesLeSew.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesLeSew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  EdWeb_Raiz.CharCase  := ecNormal;
  EdWeb_FTPu.CharCase  := ecNormal;
  EdWeb_FTPh.CharCase  := ecNormal;
  EdWeb_MyURL.CharCase := ecNormal;
  EdWeb_FTPs.CharCase  := ecNormal;
  EdPrdFoto.CharCase   := ecNormal;
  EdAvaDir.CharCase    := ecNormal;
  EdProxUser.CharCase  := ecNormal;
  EdProxPass.CharCase  := ecNormal;
  EdWebLogo.CharCase   := ecNormal;
  EdWeb_Host.CharCase  := ecNormal;
  EdWeb_DB.CharCase    := ecNormal;
  EdWeb_User.CharCase  := ecNormal;
  EdWeb_Pwd.CharCase   := ecNormal;
end;

procedure TFmOpcoesLeSew.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmOpcoesLeSew.FormShow(Sender: TObject);
begin
  EdEndRastre.CharCase := ecNormal;
end;

procedure TFmOpcoesLeSew.Label43Click(Sender: TObject);
begin
  Clipboard.AsText := Label43.Caption;
  EdLinTxt.PasteFromClipboard;
  EdLinTxt.SetFocus;
end;

procedure TFmOpcoesLeSew.LaEndRastreClick(Sender: TObject);
begin
  Clipboard.AsText := LaEndRastre.Caption;
  EdEndRastre.PasteFromClipboard;
  EdEndRastre.SetFocus;
end;

procedure TFmOpcoesLeSew.LaTxtVdaProClick(Sender: TObject);
begin
  Clipboard.AsText := LaTxtVdaPro.Caption;
  EdTxtVdaPro.PasteFromClipboard;
  EdTxtVdaPro.SetFocus;
end;

procedure TFmOpcoesLeSew.SpeedButton1Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
    EdCor2.Color := ColorDialog1.Color;
end;

procedure TFmOpcoesLeSew.SpeedButton2Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
    EdCor3.Color := ColorDialog1.Color;
end;

procedure TFmOpcoesLeSew.SpeedButton3Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
    EdCor4.Color := ColorDialog1.Color;
end;

procedure TFmOpcoesLeSew.SpeedButton4Click(Sender: TObject);
var
  Arquivo: String;
begin
  MyObjects.DefineDiretorio(FmOpcoesLeSew, EdPrdFoto);
end;

procedure TFmOpcoesLeSew.SpeedButton5Click(Sender: TObject);
var
  Arquivo: String;
begin
  MyObjects.DefineDiretorio(FmOpcoesLeSew, EdAvaDir);
end;

procedure TFmOpcoesLeSew.SpeedButton6Click(Sender: TObject);
begin
  if ColorDialog1.Execute then
    EdCor1.Color := ColorDialog1.Color;
end;

procedure TFmOpcoesLeSew.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //
  QrCartMoviConf.Open;
  QrStatus1.Open;
  QrStatus2.Open;
  QrStatus3.Open;
  QrStatus4.Open;
  QrContas.Open;
  QrContas7.Open;
  QrCentroCusto.Open;
  QrCxaPgtProf.Open;
  QrCiclListPrd.Open;
  QrCiclGradeD.Open;
  QrPtosGradeD.Open;
  QrCiclCliInt.Open;
  QrVdaCart.Open;
  QrVdaCont.Open;
  QrVdaConPgto.Open;
  QrLinDepto.Open;
  QrVPGraPrc.Open;
  QrLinGraPrc.Open;
  QrLinCart.Open;
  QrLinConta.Open;
  QrLinCPgto.Open;
  QrCartas1.Open;
  QrCartas2.Open;
  QrPrdGradeD.Open;
  QrAvaGradeD.Open;
  QrMotivo.Open;
  QrAvaEmpresa.Open;
  //
  EdCtaEmpPrfC.ValueVariant := Dmod.QrControleCtaEmpPrfC.Value;
  CBCtaEmpPrfC.KeyValue     := Dmod.QrControleCtaEmpPrfC.Value;
  //
  EdCtaEmpPrfD.ValueVariant := Dmod.QrControleCtaEmpPrfD.Value;
  CBCtaEmpPrfD.KeyValue     := Dmod.QrControleCtaEmpPrfD.Value;
  //
  EdCtaComProm.ValueVariant := Dmod.QrControleCtaComProm.Value;
  CBCtaComProm.KeyValue     := Dmod.QrControleCtaComProm.Value;
  //
  EdCtaComProf.ValueVariant := Dmod.QrControleCtaComProf.Value;
  CBCtaComProf.KeyValue     := Dmod.QrControleCtaComProf.Value;
  //
  EdCta13oProm.ValueVariant := Dmod.QrControleCta13oProm.Value;
  CBCta13oProm.KeyValue     := Dmod.QrControleCta13oProm.Value;
  //
  EdCta13oProf.ValueVariant := Dmod.QrControleCta13oProf.Value;
  CBCta13oProf.KeyValue     := Dmod.QrControleCta13oProf.Value;
  //
  EdNome13.ValueVariant     := Dmod.QrControleNome13.Value;
  //
  EdCCuPromCom.ValueVariant := Dmod.QrControleCCuPromCom.Value;
  CBCCuPromCom.KeyValue     := Dmod.QrControleCCuPromCom.Value;
  //
  EdCCuPromDTS.ValueVariant := Dmod.QrControleCCuPromDTS.Value;
  CBCCuPromDTS.KeyValue     := Dmod.QrControleCCuPromDTS.Value;
  //
  EdCCuProfCom.ValueVariant := Dmod.QrControleCCuProfCom.Value;
  CBCCuProfCom.KeyValue     := Dmod.QrControleCCuProfCom.Value;
  //
  EdCCuProfDTS.ValueVariant := Dmod.QrControleCCuProfDTS.Value;
  CBCCuProfDTS.KeyValue     := Dmod.QrControleCCuProfDTS.Value;
  //
  EdCxaPgtProf.ValueVariant := Dmod.QrControleCxaPgtProf.Value;
  CBCxaPgtProf.KeyValue     := Dmod.QrControleCxaPgtProf.Value;
  //
  EdCtaPagProf.ValueVariant := Dmod.QrControleCtaPagProf.Value;
  CBCtaPagProf.KeyValue     := Dmod.QrControleCtaPagProf.Value;
  //
  EdTxtPgtProf.Text         := Dmod.QrControleTxtPgtProf.Value;
  //
  EdCBDepto.ValueVariant    := Dmod.QrControleCiclGradeD.Value;
  CBDepto.KeyValue          := Dmod.QrControleCiclGradeD.Value;
  EdCBListPrd.ValueVariant  := Dmod.QrControleCiclListPrd.Value;
  CBListPrd.KeyValue        := Dmod.QrControleCiclListPrd.Value;
  EdDeptoPto.ValueVariant   := Dmod.QrControlePtosGradeD.Value;
  CBDeptoPto.KeyValue       := Dmod.QrControlePtosGradeD.Value;
  //
  EdTxtVdaPto.Text          := Dmod.QrControleTxtVdaPto.Value;
  EdCtaPtoVda.ValueVariant  := Dmod.QrControleCtaPtoVda.Value;
  //
  EdStatus1.ValueVariant    := Dmod.QrControleVPStat1.Value;
  CBStatus1.KeyValue        := Dmod.QrControleVPStat1.Value;
  EdCor1.Color              := Dmod.QrControleVPStat1Cor.Value;
  //
  EdStatus2.ValueVariant    := Dmod.QrControleVPStat2.Value;
  CBStatus2.KeyValue        := Dmod.QrControleVPStat2.Value;
  EdCor2.Color              := Dmod.QrControleVPStat2Cor.Value;
  //
  EdStatus3.ValueVariant    := Dmod.QrControleVPStat3.Value;
  CBStatus3.KeyValue        := Dmod.QrControleVPStat3.Value;
  EdCor3.Color              := Dmod.QrControleVPStat3Cor.Value;
  //
  EdStatus4.ValueVariant    := Dmod.QrControleVPStat4.Value;
  CBStatus4.KeyValue        := Dmod.QrControleVPStat4.Value;
  EdCor4.Color              := Dmod.QrControleVPStat4Cor.Value;
  //
  EdEndRastre.ValueVariant  := Dmod.QrControleEndRastre.Value;
  EdTxtVdaPro.ValueVariant  := Dmod.QrControleTxtVdaPro.Value;
  //
  EdTxtVdaEsq.ValueVariant  := Dmod.QrControleTxtVdaEsq.Value;
  EdLogoMoviV.ValueVariant  := Dmod.QrControleLogoMoviV.Value;
  //
  EdCiclCliInt.ValueVariant := Dmod.QrControleCiclCliInt.Value;
  CBCiclCliInt.KeyValue     := Dmod.QrControleCiclCliInt.Value;
  //
  EdVdaCart.ValueVariant    := Dmod.QrControleVdaCart.Value;
  CBVdaCart.KeyValue        := Dmod.QrControleVdaCart.Value;
  //
  EdVdaCont.ValueVariant    := Dmod.QrControleVdaConta.Value;
  CBVdaCont.KeyValue        := Dmod.QrControleVdaConta.Value;
  //
  EdVdaConPgto.ValueVariant := Dmod.QrControleVdaConPgto.Value;
  CBVdaConPgto.KeyValue     := Dmod.QrControleVdaConPgto.Value;
  //
  EdLinDepto.ValueVariant   := Dmod.QrControleLinDepto.Value;
  CBLinDepto.KeyValue       := Dmod.QrControleLinDepto.Value;
  //
  EdVPGraPrc.ValueVariant   := Dmod.QrControleVPGraPrc.Value;
  CBVPGraPrc.KeyValue       := Dmod.QrControleVPGraPrc.Value;
  //
  EdLinGraPrc.ValueVariant  := Dmod.QrControleLinGraPrc.Value;
  CBLinGraPrc.KeyValue      := Dmod.QrControleLinGraPrc.Value;
  EdLinCart.ValueVariant    := Dmod.QrControleLinCart.Value;
  CBLinCart.KeyValue        := Dmod.QrControleLinCart.Value;
  EdLinConta.ValueVariant   := Dmod.QrControleLinConta.Value;
  CBLinConta.KeyValue       := Dmod.QrControleLinConta.Value;
  EdLinCPgto.ValueVariant   := Dmod.QrControleLinCPgto.Value;
  CBLinCPgto.KeyValue       := Dmod.QrControleLinCPgto.Value;
  EdLinTxt.ValueVariant     := Dmod.QrControleLinTxt.Value;
  //
  EdPrdFoto.ValueVariant    := Dmod.QrControlePrdFoto.Value;
  EdPrdFotoTam.ValueVariant := Dmod.QrControlePrdFotoTam.Value;
  CkPrdFotoWeb.Checked      := MLAGeral.IntToBool(Dmod.QrControlePrdFotoWeb.Value);
  //
  EdWeb_FTPh.ValueVariant   := Dmod.QrControleWeb_FTPh.Value;
  EdWeb_Raiz.ValueVariant   := Dmod.QrControleWeb_Raiz.Value;
  EdWeb_FTPu.ValueVariant   := Dmod.QrControleWeb_FTPu.Value;
  EdWeb_FTPs.ValueVariant   := Dmod.QrControleWeb_FTPs.Value;
  EdWeb_MyURL.ValueVariant  := Dmod.QrControleWeb_MyURL.Value;
  CkWeb_FTPpassivo.Checked  := Geral.IntToBool(Dmod.QrControlePassivo.Value);
  //
  EdAvaTexApr.ValueVariant  := Dmod.QrControleAvaTexApr.Value;
  CBAvaTexApr.KeyValue      := Dmod.QrControleAvaTexApr.Value;
  EdAvaTexRep.ValueVariant  := Dmod.QrControleAvaTexRep.Value;
  CBAvaTexRep.KeyValue      := Dmod.QrControleAvaTexRep.Value;
  EdAvaFoto.ValueVariant    := Dmod.QrControleAvaFoto.Value;
  EdAvaDir.ValueVariant     := Dmod.QrControleAvaDir.Value;
  //
  EdPrdGradeD.ValueVariant  := Dmod.QrControlePrdGradeD.Value;
  CBPrdGradeD.KeyValue      := Dmod.QrControlePrdGradeD.Value;
  //
  EdAvaGradeD.ValueVariant  := Dmod.QrControleAvaGradeD.Value;
  CBAvaGradeD.KeyValue      := Dmod.QrControleAvaGradeD.Value;
  //
  EdAvaEmpresa.ValueVariant := Dmod.QrControleAvaEmpresa.Value;
  CBAvaEmpresa.KeyValue     := Dmod.QrControleAvaEmpresa.Value;
  //
  CkProxBAut.Checked        := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
  EdProxUser.ValueVariant   := Dmod.QrControleProxUser.Value;
  EdProxServ.ValueVariant   := Dmod.QrControleProxServ.Value;
  EdProxPass.ValueVariant   := Dmod.QrControleProxPass.Value;
  EdProxPort.ValueVariant   := Dmod.QrControleProxPort.Value;
  //
  EdAvaMotivo.ValueVariant  := Dmod.QrControleAvaMotivo.Value;
  CBAvaMotivo.KeyValue      := Dmod.QrControleAvaMotivo.Value;
  //
  EdAvaLucro.ValueVariant   := Dmod.QrControleAvaPerc.Value;
  EdWebLogo.ValueVariant    := Dmod.QrControleWebLogo.Value;
  //
  EdWeb_Host.ValueVariant   := Dmod.QrControleWeb_Host.Value;
  EdWeb_DB.ValueVariant     := Dmod.QrControleWeb_DB.Value;
  EdWeb_User.ValueVariant   := Dmod.QrControleWeb_User.Value;
  EdWeb_Pwd.ValueVariant    := Dmod.QrControleWeb_Pwd.Value;
  RGWeb_MySQL.ItemIndex     := Dmod.QrControleWeb_MySQL.Value;
  //
  EdMoviVCaPag.ValueVariant := Dmod.QrControleMoviVCaPag.Value;
  CBMoviVCaPag.KeyValue     := Dmod.QrControleMoviVCaPag.Value;
  //
  CkProxUse.Checked         := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
  HKEY_LOCAL_MACHINE);
  //
  EdValCur.ValueVariant     := Dmod.QrControleValCur.Value;
  EdValCurPrd.ValueVariant  := Dmod.QrControleValCurPrd.Value;
  //
  EdMoviCCons.ValueVariant := Dmod.QrControleMoviCCons.Value;
  //
  RGFatSEtqKit.ItemIndex := Dmod.QrControleFatSEtqKit.Value;
  RGFatSEtqLin.ItemIndex := Dmod.QrControleFatSEtqLin.Value;
end;

procedure TFmOpcoesLeSew.BtOKClick(Sender: TObject);
var
  Proxy, Cor1, Cor2, Cor3, Cor4: Integer;
begin
  Cor1 := ColorToRGB(EdCor1.Color);
  Cor2 := ColorToRGB(EdCor2.Color);
  Cor3 := ColorToRGB(EdCor3.Color);
  Cor4 := ColorToRGB(EdCor4.Color);
  //
  if CkProxUse.Checked = True then
    Proxy := 1
  else
    Proxy := 0;
  //
  Geral.WriteAppKey('Proxy', Application.Title, Proxy, ktInteger, HKEY_LOCAL_MACHINE);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False,
  [
    'CtaEmpPrfC', 'CtaEmpPrfD',
    'CtaComProm', 'CtaComProf',
    'Cta13oProm', 'Cta13oProf',
    'Nome13', 'CCuPromCom',
    'CCuPromDTS', 'CCuProfCom',
    'CCuProfDTS', 'CxaPgtProf',
    'CtaPagProf', 'TxtPgtProf',
    'CiclGradeD', 'CiclListPrd',
    'PtosGradeD', 'TxtVdaPto',
    'CtaPtoVda', 'VPStat1',
    'VPStat2', 'VPStat3',
    'VPStat1Cor', 'VPStat2Cor',
    'VPStat3Cor', 'EndRastre',
    'TxtVdaPro', 'TxtVdaEsq',
    'LogoMoviV', 'CiclCliInt',
    'VdaCart', 'VdaConta',
    'VdaConPgto', 'VPStat4',
    'VPStat4Cor', 'LinDepto',
    'LinGraPrc', 'VPGraPrc',
    'LinCart', 'LinConta',
    'LinCPgto', 'LinTxt',
    'PrdFoto', 'Web_MyURL',
    'Web_FTPh', 'Web_FTPs',
    'Web_FTPu', 'Web_Raiz', 'Passivo',
    'PrdFotoWeb', 'PrdFotoTam',
    'AvaTexApr', 'AvaTexRep',
    'AvaFoto', 'AvaDir',
    'PrdGradeD', 'AvaGradeD',
    'ProxBAut', 'ProxUser',
    'ProxServ', 'ProxPass',
    'ProxPort', 'AvaMotivo',
    'AvaPerc', 'WebLogo',
    'Web_Host', 'Web_User',
    'Web_Pwd', 'Web_DB',
    'Web_MySQL', 'MoviVCaPag',
    'ValCur', 'ValCurPrd',
    'MoviCCons', 'AvaEmpresa',
    'FatSEtqLin', 'FatSEtqKit'
  ], ['Codigo'], [
    EdCtaEmpPrfC.Text, EdCtaEmpPrfD.Text,
    EdCtaComProm.Text, EdCtaComProf.Text,
    EdCta13oProm.Text, EdCta13oProf.Text,
    EdNome13.Text, EdCCuPromCom.Text,
    EdCCuPromDTS.Text, EdCCuProfCom.Text,
    EdCCuProfDTS.Text, EdCxaPgtProf.Text,
    EdCtaPagProf.Text, EdTxtPgtProf.Text,
    EdCBDepto.ValueVariant, EdCBListPrd.ValueVariant,
    EdDeptoPto.ValueVariant, EdTxtVdaPto.Text, EdCtaPtoVda.ValueVariant,
    EdStatus1.ValueVariant, EdStatus2.ValueVariant,
    EdStatus3.ValueVariant, Cor1, Cor2, Cor3, EdEndRastre.ValueVariant,
    EdTxtVdaPro.ValueVariant, EdTxtVdaEsq.ValueVariant,
    EdLogoMoviV.ValueVariant,
    EdCiclCliInt.ValueVariant, EdVdaCart.ValueVariant,
    EdVdaCont.ValueVariant, EdVdaConPgto.ValueVariant,
    EdStatus4.ValueVariant, Cor4, EdLinDepto.ValueVariant,
    EdLinGraPrc.ValueVariant, EdVPGraPrc.ValueVariant,
    EdLinCart.ValueVariant, EdLinConta.ValueVariant,
    EdLinCPgto.ValueVariant, EdLinTxt.ValueVariant,
    EdPrdFoto.ValueVariant, EdWeb_MyURL.ValueVariant,
    EdWeb_FTPh.ValueVariant, EdWeb_FTPs.ValueVariant,
    EdWeb_FTPu.ValueVariant, EdWeb_Raiz.ValueVariant, Geral.BoolToInt(CkWeb_FTPpassivo.Checked),
    MLAGeral.BoolToInt(CkPrdFotoWeb.Checked), EdPrdFotoTam.ValueVariant,
    EdAvaTexApr.ValueVariant, EdAvaTexRep.ValueVariant,
    EdAvaFoto.ValueVariant, EdAvaDir.ValueVariant,
    EdPrdGradeD.ValueVariant, EdAvaGradeD.ValueVariant,
    CkProxBAut.Checked, EdProxUser.ValueVariant,
    EdProxServ.ValueVariant, EdProxPass.ValueVariant,
    EdProxPort.ValueVariant, EdAvaMotivo.ValueVariant,
    EdAvaLucro.ValueVariant, EdWebLogo.ValueVariant,
    EdWeb_Host.ValueVariant, EdWeb_User.ValueVariant,
    EdWeb_Pwd.ValueVariant, EdWeb_DB.ValueVariant,
    RGWeb_MySQL.ItemIndex, EdMoviVCaPag.ValueVariant,
    EdValCur.ValueVariant, EdValCurPrd.ValueVariant,
    EdMoviCCons.ValueVariant, EdAvaEmpresa.ValueVariant,
    RGFatSEtqLin.ItemIndex, RGFatSEtqKit.ItemIndex
  ], ['1'], False) then
  begin
    Dmod.QrControle.Close;
    Dmod.QrControle.Open;
    //
    Close;
  end;
end;

procedure TFmOpcoesLeSew.BtTestarClick(Sender: TObject);
const
  Avisa = True;
begin
  UnDmkDAC_PF.TestarConexaoMySQL(Self, EdWeb_Host.Text, EdWeb_User.Text,
    EdWeb_Pwd.Text, EdWeb_DB.Text, 3306, Avisa);
end;

procedure TFmOpcoesLeSew.EdLogoMoviVKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdLogoMoviV.ValueVariant :=
      OpenPictureDialog1.FileName;
  end;
end;

// Parei aqui ! restaurar backup para teste
end.
