unit WUsers;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral, dmkDBEdit,
  ColorGrd, ActnColorMaps, ActnMan, dmkCheckBox, dmkDBLookupComboBox, dmkEditCB,
  dmkRadioGroup, Variants, ComCtrls, UnDmkProcFunc, UnDmkEnums;

type
  TFmWUsers = class(TForm)
    DsWUsers: TDataSource;
    QrWUsers: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PainelData: TPanel;
    PainelEdita: TPanel;
    Label10: TLabel;
    Label9: TLabel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkEdNome: TdmkEdit;
    CkContinuar: TCheckBox;
    EdCodigo: TdmkEdit;
    QrLoc: TmySQLQuery;
    dmkCkAtivo: TdmkCheckBox;
    Label7: TLabel;
    dmkEdCBCliente: TdmkEditCB;
    dmkCBCliente: TdmkDBLookupComboBox;
    Label3: TLabel;
    dmkEdUsuario: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    dmkEdEmail: TdmkEdit;
    dmkRGTipo: TdmkRadioGroup;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNome: TWideStringField;
    EdSenha: TEdit;
    EdSenha2: TEdit;
    QrWUsersCodigo: TAutoIncField;
    QrWUsersUsername: TWideStringField;
    QrWUsersPassword: TWideStringField;
    QrWUsersPersonalName: TWideStringField;
    QrWUsersLoginID: TWideStringField;
    QrWUsersEntidade: TIntegerField;
    QrWUsersTipo: TSmallintField;
    QrWUsersLk: TIntegerField;
    QrWUsersDataCad: TDateField;
    QrWUsersDataAlt: TDateField;
    QrWUsersUserCad: TIntegerField;
    QrWUsersUserAlt: TIntegerField;
    QrWUsersAlterWeb: TSmallintField;
    QrWUsersAtivo: TSmallintField;
    QrWUsersSENHA: TWideStringField;
    QrLocCliente: TmySQLQuery;
    QrWUsersNOMECLIENTE: TWideStringField;
    QrLocClienteCLIENTE: TWideStringField;
    Label1: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label2: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label8: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label11: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    Label13: TLabel;
    dmkDBEdit6: TdmkDBEdit;
    CkAtivo: TDBCheckBox;
    Label12: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    QrLocEmail: TmySQLQuery;
    QrLocEmailSTREMAIL: TWideStringField;
    QrWUsersEmail: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWUsersAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWUsersBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure dmkEdCBClienteExit(Sender: TObject);
    procedure dmkEdUsuarioExit(Sender: TObject);
    procedure EdSenhaExit(Sender: TObject);
    procedure EdSenha2Exit(Sender: TObject);
    procedure dmkEdEmailExit(Sender: TObject);
    procedure QrWUsersCalcFields(DataSet: TDataSet);
    procedure dmkEdEmailKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    //
  public
    { Public declarations }
  end;

var
  FmWUsers: TFmWUsers;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWUsers.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWUsers.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWUsersCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWUsers.DefParams;
begin
  VAR_GOTOTABELA := 'wusers';
  VAR_GOTOMYSQLTABLE := QrWUsers;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  //
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT wus.*, AES_DECRYPT(Password, "XidvztYQhR") SENHA');
  VAR_SQLx.Add('FROM wusers wus');
  VAR_SQLx.Add('WHERE wus.Codigo > 0');
  //
  VAR_SQL1.Add('AND wus.Codigo=:P0');
  //
  VAR_SQLa.Add('AND wus.Nome Like :P0');
  //
end;

procedure TFmWUsers.dmkEdCBClienteExit(Sender: TObject);
var
  Codigo : Integer;
begin
  if (dmkEdCBCliente.ValueVariant <> 0) and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Entidade');
    QrLoc.SQL.Add('FROM wusers');
    QrLoc.SQL.Add('WHERE Entidade LIKE :P0');
    QrLoc.Params[0].AsString := dmkEdCBCliente.ValueVariant;
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Este cliente possui "'+ Geral.FF0(QrLoc.RecordCount)
        + '" senha(s) cadastrada(s). Deseja continuar?') = ID_NO then
      begin
        Codigo := Geral.IMV(EdCodigo.Text);
        if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'wusers', Codigo);
        MostraEdicao(False, CO_TRAVADO, 0);
        UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wusers', 'Codigo');
      end;
    end;
  end;
end;

procedure TFmWUsers.dmkEdEmailExit(Sender: TObject);
begin
  if (dmkEdEmail.ValueVariant <> '') and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Email');
    QrLoc.SQL.Add('FROM wusers');
    QrLoc.SQL.Add('WHERE Email LIKE :P0');
    QrLoc.Params[0].AsString := dmkEdEmail.ValueVariant;
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Este email j� foi cadastrado.', 'Aviso', MB_OK+MB_ICONWARNING);
      dmkEdEmail.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmWUsers.dmkEdEmailKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F5 then
  begin
    QrLocEmail.Close;
    QrLocEmail.Params[0].AsInteger := dmkEdCBCliente.ValueVariant;
    QrLocEmail.Open;
    //
    if QrLocEmail.RecordCount > 0 then
      dmkEdEmail.ValueVariant := QrLocEmailSTREMAIL.Value;
  end;
end;

procedure TFmWUsers.EdSenha2Exit(Sender: TObject);
begin
  if (EdSenha.Text <> EdSenha2.Text)
    and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    Application.MessageBox('O campo Senha e o campo Confirmar senha devem conter o mesmo texto.', 'Aviso', MB_OK+MB_ICONWARNING);
    EdSenha.Text  := '';
    EdSenha2.Text := '';
    EdSenha.SetFocus;
    Exit;
  end;
end;

procedure TFmWUsers.EdSenhaExit(Sender: TObject);
begin
  if (EdSenha.Text <> '') and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Password');
    QrLoc.SQL.Add('FROM wusers');
    QrLoc.SQL.Add('WHERE Password LIKE :P0');
    QrLoc.Params[0].AsString := EdSenha.Text;
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Esta senha j� foi cadastrada.', 'Aviso', MB_OK+MB_ICONWARNING);
      EdSenha.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmWUsers.dmkEdUsuarioExit(Sender: TObject);
begin
  if (dmkEdUsuario.ValueVariant <> '') and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Username');
    QrLoc.SQL.Add('FROM wusers');
    QrLoc.SQL.Add('WHERE Username LIKE :P0');
    QrLoc.Params[0].AsString := dmkEdUsuario.ValueVariant;
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Este usu�rio j� foi cadastrado.', 'Aviso', MB_OK+MB_ICONWARNING);
      dmkEdUsuario.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmWUsers.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWUsers.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWUsers.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWUsers.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWUsers.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWUsers.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWUsers.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWUsers.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWUsersCodigo.Value;
  Close;
end;

procedure TFmWUsers.BtConfirmaClick(Sender: TObject);
var
  Codigo, Cliente, Tipo : Integer;
  PName, User, Pass, Pass2, Email : String;
begin
  Tipo    := 0;
  Cliente := dmkEdCBCliente.ValueVariant;
  PName   := dmkEdNome.ValueVariant;
  User    := dmkEdUsuario.ValueVariant;
  Pass    := EdSenha.Text;
  Pass2   := EdSenha2.Text;
  Email   := dmkEdEmail.ValueVariant;
  case dmkRGTipo.ItemIndex of
    0: Tipo := 1;
    1: Tipo := 2;
    2: Tipo := 3;
    3: Tipo := 4;
    4: Tipo := 9;
  end;
  //
  if Cliente = 0 then
  begin
    Application.MessageBox('Defina um Cliente.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    dmkEdCBCliente.SetFocus;
    Exit;
  end;
  if Length(User) = 0 then
  begin
    Application.MessageBox('Defina um Usu�rio.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    dmkEdUsuario.SetFocus;
    Exit;
  end;
  if Length(Pass) = 0  then
  begin
    Application.MessageBox('Defina uma Senha.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    EdSenha.SetFocus;
    Exit;
  end;
  if Length(Email) = 0 then
  begin
    Application.MessageBox('Defina um email.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    dmkEdEmail.SetFocus;
    Exit;
  end;
  if Pass <> Pass2 then
  begin
    Application.MessageBox('O campo "Senha" e o campo "Confirmar senha" devem conter o mesmo texto.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    EdSenha.Text  := '';
    EdSenha2.Text := '';
    EdSenha.SetFocus;
    Exit;
  end;
  if Tipo < 0 then
  begin
    Application.MessageBox('Defina o Tipo.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    dmkRGTipo.SetFocus;
    Exit;
  end;
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, VAR_LIVRES, 'controle', 'senhas', 'usuario', 'numero')
  else
    Codigo := QrWUsersCodigo.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('INSERT INTO wusers SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE wusers SET ');
  Dmod.QrUpd.SQL.Add('Entidade=:P0, PersonalName=:P1, Username=:P2, ');
  Dmod.QrUpd.SQL.Add('Password=AES_ENCRYPT(:P3, :P4), Email=:P5, ');
  Dmod.QrUpd.SQL.Add('Tipo=:P6, Ativo=:P7, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpd.Params[00].AsInteger  := Cliente;
  Dmod.QrUpd.Params[01].AsString   := PName;
  Dmod.QrUpd.Params[02].AsString   := User;
  Dmod.QrUpd.Params[03].AsString   := Pass;
  Dmod.QrUpd.Params[04].AsString   := 'XidvztYQhR';
  Dmod.QrUpd.Params[05].AsString   := Email;
  Dmod.QrUpd.Params[06].AsInteger  := Tipo;
  Dmod.QrUpd.Params[07].AsInteger  := MLAGeral.BTI(dmkCkAtivo.Checked);
  //
  Dmod.QrUpd.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[09].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[10].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  QrWUsers.Close;
  QrWUsers.Open;
  //
  if CkContinuar.Checked then
  begin
    Application.MessageBox('Senha cadastrada com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    MostraEdicao(True, CO_INCLUSAO, 0);
  end else begin
    MostraEdicao(False, CO_TRAVADO, 0);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWUsers.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'wusers', Codigo);
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'wusers', 'Codigo');
end;

procedure TFmWUsers.FormCreate(Sender: TObject);
begin
  VAR_LIVRES := 'Livres';
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  //
  QrClientes.Open;
  EdSenha.CharCase  := ecNormal;
  EdSenha2.CharCase := ecNormal;
end;

procedure TFmWUsers.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWUsersCodigo.Value,LaRegistro.Caption);
end;

procedure TFmWUsers.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWUsers.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmWUsers.QrWUsersAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWUsers.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmWUsers.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWUsersCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wusers', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWUsers.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmWUsers.QrWUsersBeforeOpen(DataSet: TDataSet);
begin
  QrWUsersCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWUsers.QrWUsersCalcFields(DataSet: TDataSet);
begin
  QrLocCliente.Close;
  QrLocCliente.Params[0].AsInteger := QrWUsersEntidade.Value;
  QrLocCliente.Open;
  //
  QrWUsersNOMECLIENTE.Value := QrLocClienteCLIENTE.Value;
end;

procedure TFmWUsers.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, CO_INCLUSAO, 0);
end;

procedure TFmWUsers.BtAlteraClick(Sender: TObject);
begin
  if QrWUsersCodigo.Value > 0 then
  begin
    MostraEdicao(True, CO_ALTERACAO, 0);
  end;
end;

procedure TFmWUsers.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Visible := False;
    PainelEdita.Visible := True;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.ValueVariant       := FormatFloat('000', Codigo);
      dmkEdCBCliente.ValueVariant := 0;
      dmkCBCliente.KeyValue       := NULL;
      dmkEdNome.ValueVariant      := '';
      dmkEdUsuario.ValueVariant   := '';
      EdSenha.Text                := '';
      EdSenha2.Text               := '';
      dmkEdEmail.ValueVariant     := '';
      dmkRGTipo.ItemIndex         := 0;
      dmkCkAtivo.Checked          := True;
    end else begin
      EdCodigo.ValueVariant       := QrWUsersCodigo.Value;
      dmkEdCBCliente.ValueVariant := QrWUsersEntidade.Value;
      dmkCBCliente.KeyValue       := QrWUsersEntidade.Value;
      dmkEdNome.ValueVariant      := QrWUsersPersonalName.Value;
      dmkEdUsuario.ValueVariant   := QrWUsersUsername.Value;
      EdSenha.Text                := '';
      EdSenha2.Text               := '';
      dmkEdEmail.ValueVariant     := QrWUsersEmail.Value;
      dmkRGTipo.ItemIndex         := QrWUsersTipo.Value;
      dmkCkAtivo.Checked          := MLAGeral.ITB(QrWUsersAtivo.Value);
    end;
    dmkEdCBCliente.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption); 
end;

end.
