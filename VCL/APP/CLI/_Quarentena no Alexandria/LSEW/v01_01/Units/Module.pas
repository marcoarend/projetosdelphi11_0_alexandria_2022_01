unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule, dmkGeral,
  mySQLDbTables, UnGOTOy, Winsock, MySQLBatch, frxClass, frxDBSet,
  ABSMain, Variants, UnGrl_Vars;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrSomaM: TMySQLQuery;
    QrSomaMValor: TFloatField;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleUFPadrao: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrControleTravaCidade: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TIntegerField;
    QrControleEntraSemValor: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrControleMyPagTip: TIntegerField;
    QrControleMyPagCar: TIntegerField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrControleDono: TIntegerField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoNUMERO: TLargeintField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrEnderecoRespons1: TWideStringField;
    QrAgoraAGORA: TDateTimeField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoNF: TWideStringField;
    QrMasterENumero: TFloatField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrMaster2Limite: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrSenhasFuncionario: TIntegerField;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasNOMEDOBANCO: TWideStringField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasDIFERENCA: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTIPOPRAZO: TWideStringField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TSmallintField;
    QrCarteirasUserAlt: TSmallintField;
    QrCarteirasNOMEPAGREC: TWideStringField;
    QrCarteirasPagRec: TSmallintField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoTipo: TSmallintField;
    QrDonoTE1: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoNATAL_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoE_CUC: TWideStringField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrDonoE_LN2: TWideStringField;
    QrDonoAPELIDO: TWideStringField;
    QrDonoNUMERO: TFloatField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    QrEstqR: TmySQLQuery;
    QrEstqRQtd: TFloatField;
    QrEstqRVal: TFloatField;
    QrEstqP: TmySQLQuery;
    QrEstqPQtd: TFloatField;
    QrSel: TmySQLQuery;
    QrSelMovix: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TSmallintField;
    QrControleModeloNF: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleEntiGrupos: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrControleCores: TIntegerField;
    QrControleGradeG: TIntegerField;
    QrControleGrades: TIntegerField;
    QrControleGradesCors: TIntegerField;
    QrControleGradesTams: TIntegerField;
    QrControleTamanhos: TIntegerField;
    QrControleProdutos: TIntegerField;
    QrControleMoviC: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrUpdW: TmySQLQuery;
    QrControleMovix: TIntegerField;
    frxDsDono: TfrxDBDataset;
    frxDsMaster: TfrxDBDataset;
    QrControleEquiGru: TIntegerField;
    QrControleEquiCom: TIntegerField;
    QrControleEquiComIts: TIntegerField;
    QrControleCiclos: TIntegerField;
    QrControleCursos: TIntegerField;
    QrControleCursosIts: TIntegerField;
    QrControleMoviB: TIntegerField;
    QrControleMovim: TIntegerField;
    DsOptImpConfig: TDataSource;
    QrOptImpConfig: TmySQLQuery;
    QrOptImpConfigCodigo: TIntegerField;
    QrOptImpConfigNome: TWideStringField;
    QrOptImpConfigTipo: TIntegerField;
    QrOptImpConfigNOMETIPO: TWideStringField;
    QrControleContVen: TIntegerField;
    QrControleContCom: TIntegerField;
    QrControleMoviV: TIntegerField;
    QrControleEntiCtas: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleEquiConTrf: TIntegerField;
    QrControleCtaEmpPrfC: TIntegerField;
    QrControleCtaEmpPrfD: TIntegerField;
    QrControleCiclosAula: TIntegerField;
    QrControleCliInt: TIntegerField;
    QrControleContasLnk: TIntegerField;
    QrControleNome13: TWideStringField;
    QrControleCtaComProf: TIntegerField;
    QrControleCtaComProm: TIntegerField;
    QrControleCta13oProf: TIntegerField;
    QrControleCta13oProm: TIntegerField;
    QrControleCCuPromCom: TIntegerField;
    QrControleCCuPromDTS: TIntegerField;
    QrControleCCuProfCom: TIntegerField;
    QrControleCCuProfDTS: TIntegerField;
    QrControleLogoBig1: TWideStringField;
    QrControleSecuritStr: TWideStringField;
    QrControleSenhasIts: TIntegerField;
    QrControleCxaPgtProf: TIntegerField;
    QrControleCtaPagProf: TIntegerField;
    QrControleTxtPgtProf: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrSerialUC: TmySQLQuery;
    QrSerialUF: TmySQLQuery;
    QrSerialUFControle: TIntegerField;
    QrSerialUCCodigo: TIntegerField;
    QrSerialUCSenha: TWideStringField;
    QrSerialMC: TmySQLQuery;
    QrSerialMCCodigo: TIntegerField;
    QrSerialMCSenha: TWideStringField;
    QrSerialMCNumIni: TIntegerField;
    QrSerialMCNumFim: TIntegerField;
    QrDuplMD5: TmySQLQuery;
    QrDuplMD5Entidade: TIntegerField;
    QrControleCiclGradeD: TIntegerField;
    QrControleCiclListPrd: TIntegerField;
    QrSenhasIP_Default: TWideStringField;
    QrControlePtosGradeD: TIntegerField;
    QrControleCasasProd: TIntegerField;
    QrDonoLograd: TFloatField;
    QrDonoCEP: TFloatField;
    QrControleTxtVdaPto: TWideStringField;
    QrControleCtaPtoVda: TIntegerField;
    QrControleMD5Cab: TIntegerField;
    QrControleMD5Fxa: TIntegerField;
    QrControleCambioCot: TIntegerField;
    QrControleCambioMda: TIntegerField;
    QrControleCiclosAval: TIntegerField;
    QrControleGradeD: TIntegerField;
    QrControleGradeK: TIntegerField;
    QrControleGradeKIts: TIntegerField;
    QrControleGraGruVal: TIntegerField;
    QrControleMoviVStat: TIntegerField;
    QrControleStatusV: TIntegerField;
    QrControleEntiContat: TIntegerField;
    QrControleEntiMail: TIntegerField;
    QrControleEntiTel: TIntegerField;
    QrControleMoviVK: TIntegerField;
    QrControleEntiCargos: TIntegerField;
    QrControleEntiTipCto: TIntegerField;
    QrControleCarteirasU: TIntegerField;
    QrControleParamsNFs: TIntegerField;
    QrControleContasU: TIntegerField;
    QrControleEntDefAtr: TIntegerField;
    QrControleEntAtrCad: TIntegerField;
    QrControleEntAtrIts: TIntegerField;
    QrControleBalTopoNom: TIntegerField;
    QrControleBalTopoTit: TIntegerField;
    QrControleBalTopoPer: TIntegerField;
    QrControleNCMs: TIntegerField;
    QrControleLctoEndoss: TIntegerField;
    QrControleEntiRespon: TIntegerField;
    QrControleEntiCfgRel: TIntegerField;
    QrControlePtosCadPto: TIntegerField;
    QrControlePediPrzIts: TIntegerField;
    QrControlePediPrzCli: TIntegerField;
    QrControlePtosFatCad: TIntegerField;
    QrControlePtosPedCad: TIntegerField;
    QrControlePtosPedIts: TIntegerField;
    QrControlePtosStqMov: TIntegerField;
    QrControleVendaProS: TIntegerField;
    QrControleVPStat1: TIntegerField;
    QrControleVPStat2: TIntegerField;
    QrControleVPStat3: TIntegerField;
    QrControleVPStat1Cor: TIntegerField;
    QrControleVPStat2Cor: TIntegerField;
    QrControleVPStat3Cor: TIntegerField;
    QrControleEndRastre: TWideStringField;
    QrControleTxtVdaPro: TWideStringField;
    QrControleTxtVdaEsq: TWideStringField;
    QrControleLogoMoviV: TWideStringField;
    QrControleCiclCliInt: TIntegerField;
    QrControleCustomFR3: TIntegerField;
    QrControleVdaCart: TIntegerField;
    QrControleVdaConta: TIntegerField;
    QrControleVdaConPgto: TIntegerField;
    QrControleVPStat4: TIntegerField;
    QrControleVPStat4Cor: TIntegerField;
    QrControleContasLnkIts: TIntegerField;
    QrControleLinDepto: TIntegerField;
    QrControleMoviL: TIntegerField;
    QrControleVPGraPrc: TIntegerField;
    QrControleLinGraPrc: TIntegerField;
    QrControleLinCart: TIntegerField;
    QrControleLinConta: TIntegerField;
    QrControleLinCPgto: TIntegerField;
    QrControleLinTxt: TWideStringField;
    QrControleWeb_MyURL: TWideStringField;
    QrControleWeb_FTPu: TWideStringField;
    QrControleWeb_FTPs: TWideStringField;
    QrControleWeb_FTPh: TWideStringField;
    QrControleWeb_Raiz: TWideStringField;
    QrControlePrdFoto: TWideStringField;
    QrControleEventosCad: TSmallintField;
    QrControlePrdFotoWeb: TSmallintField;
    QrControlePrdFotoTam: TIntegerField;
    QrControleAvaTexApr: TIntegerField;
    QrControleAvaTexRep: TIntegerField;
    QrControleAvaDir: TWideStringField;
    QrControleCartAval: TIntegerField;
    QrControleAvaFoto: TIntegerField;
    QrControlePrdGradeD: TIntegerField;
    QrControleAvaGradeD: TIntegerField;
    QrControleProxBAut: TSmallintField;
    QrControleProxPass: TWideStringField;
    QrControleProxPort: TIntegerField;
    QrControleProxServ: TWideStringField;
    QrControleProxUser: TWideStringField;
    QrControleCartAvalS: TIntegerField;
    QrControleAvaMotivo: TIntegerField;
    QrControleAvaPerc: TIntegerField;
    QrControleWebLogo: TWideStringField;
    QrControleFotos: TIntegerField;
    QrEnderecoNO_TIPO_DOC: TWideStringField;
    QrControleCNAB_Cfg: TIntegerField;
    QrControleCNAB_Lei: TIntegerField;
    QrControleCNAB_Lot: TIntegerField;
    QrControleWeb_MySQL: TSmallintField;
    QrControleWeb_Page: TWideStringField;
    QrControleWeb_Host: TWideStringField;
    QrControleWeb_User: TWideStringField;
    QrControleWeb_Pwd: TWideStringField;
    QrControleWeb_DB: TWideStringField;
    MyDBn: TmySQLDatabase;
    QrWeb: TmySQLQuery;
    QrControleMoviVCaPag: TIntegerField;
    QrUpdZ: TmySQLQuery;
    QrControleValCur: TIntegerField;
    QrControleValCurPrd: TIntegerField;
    QrControleMoviCCons: TIntegerField;
    QrControleAvaEmpresa: TIntegerField;
    QrMasterUsaAccMngr: TSmallintField;
    QrCarteirasAgencia1: TIntegerField;
    QrControleFatSEtqKit: TSmallintField;
    QrControleFatSEtqLin: TSmallintField;
    QrControlePassivo: TSmallintField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure QrControleCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FStrFmtPrc: String;
    FProdDelete: Integer;
    function  Privilegios(Usuario : Integer) : Boolean;
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte;
              TabLctA: String);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);
    procedure ReopenControle();

    function  ConcatenaRegistros(Matricula: Integer): String;
    function  BuscaProximoMovix: Integer;
    function  VerificaSenhaEntidade(const Senha: String; var Grupo: Integer;
              var Numero: Integer; const MyKey: Integer): Boolean;
    function  SerialDuplicadoEntidade(SerialG, SerialN: Integer): Boolean;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function  ConectarAoSite(var IP: String): Integer;
    function  ConexaoRemota(Aviso: Integer): Boolean;
    function  VerificaDataDeExpiracaoDoCurso(Entidade: Integer): TDateTime;
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenParamsEspecificos(Empresa: Integer);
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses Principal, SenhaBoss, Servidor, VerifiDB, MyDBCheck, InstallMySQL41,
  LeSew_MLA, MyListas, uEncrypt, UnitMD5, ServiceManager, VerifiDBTerceiros,
  ModuleGeral, ModuleFin, UnLic_Dmk, UnMyObjects, DmkDAC_PF;

{$R *.DFM}

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica, VerificaDBTerc: Boolean;
begin
  VerificaDBTerc := False;
  //
  if MyDB.Connected then
    Geral.MB_Aviso('MyDB est� connectado antes da configura��o!');
  if MyLocDataBase.Connected then
    Geral.MB_Aviso('MyLocDataBase est� connectado antes da configura��o!');
  if MyDBn.Connected then
    Geral.MB_Aviso('MyDBn est� connectado antes da configura��o!');
  //
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_PORTA   := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  //
  if not GOTOy.OMySQLEstaInstalado(FmLeSew_MLA.LaAviso1, FmLeSew_MLA.LaAviso2,
    FmLeSew_MLA.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmLeSew_MLA.LaAviso1, FmLeSew_MLA.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmLeSew_MLA.BtEntra.Visible := False;
    //
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
        Geral.MB_Erro('M�quina cliente sem rede!');
        //Application.Terminate;
      end;
    end;
  except
    Geral.MB_Erro('Falha ao obter par�metros do sistema!');
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    MyObjects.CriaForm_AcessoTotal(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  (*
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  *)
  //
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql', VAR_IP, VAR_PORTA, VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);

  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados ' + TMeuDB +
              ' n�o existe e deve ser criado. Confirma a cria��o?');
    //
    if Resp = ID_YES then
    begin
      DBCheck.CriaDatabase(QrAux, TMeuDB);
      //
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados local ' + TLocDB +
              ' n�o existe e deve ser criado. Confirma a cria��o?');
    //
    if Resp = ID_YES then
    begin
      DBCheck.CriaDatabase(QrAuxL, TLocDB);
      MyLocDatabase.DatabaseName := TLocDB;
    end else
    if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, ' +
              'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e ' +
              'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do ' +
              'banco de dados. Confirma a Verifica��o?');
    //
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  //MyObjects.ConfiguracoesIniciais(1, MyDB.DatabaseName);
  MyList.ConfiguracoesIniciais(1, Application.Name);
  //
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados Geral!');
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MB_Erro('Imposs�vel criar Modulo Financeiro!');
    Application.Terminate;
    Exit;
  end;
  if Verifica then
  begin
    MyObjects.CriaForm_AcessoTotal(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      MyObjects.CriaForm_AcessoTotal(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
        //
        VerificaDBTerc := True;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  if VerificaDBTerc then
  begin
    Application.CreateForm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros);
    FmVerifiDBTerceiros.FVerifi := True;
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.FVerifi := False;
    FmVerifiDBTerceiros.Destroy;
  end;
  //
  Lic_Dmk.LiberaUso5;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DROP TABLE IF EXISTS lanctos ',
    '']);
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte;
  TabLctA: String);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM ' + TabLctA);
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM ' + TabLctA);
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDmod.ReopenControle;
begin
  QrControle.Close;
  QrControle.Open;
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

function TDmod.VerificaDataDeExpiracaoDoCurso(Entidade: Integer): TDateTime;
var
  Data, Data1, Data2: TDateTime;
  Dia, Mes, AnoI, AnoF: Word;
  Produto: Integer;
begin
  Result := 0;
  Produto := QrControleValCurPrd.Value;
  if Produto = 0 then
  begin
    Geral.MensagemBox('Produto n�o definido nas op��es do aplicativo!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  // Verifica nas turmas
  QrUpd.Close;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('SELECT MAX(aul.Data) Data');
  QrUpd.SQL.Add('FROM ciclosalu alu');
  QrUpd.SQL.Add('LEFT JOIN ciclosaula aul ON aul.Controle = alu.Controle');
  QrUpd.SQL.Add('WHERE alu.Entidade=:P0');
  QrUpd.Params[0].AsInteger := Entidade;
  QrUpd.Open;
  //
  if QrUpd.FieldByName('Data').Value <> Null then
    Data1 := QrUpd.FieldByName('Data').Value
  else
    Data1 := 0;
  //
  // Verifica nas vendas
  QrUpd.Close;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('SELECT MAX(mov.DataReal) Data');
  QrUpd.SQL.Add('FROM produtos prd');
  QrUpd.SQL.Add('LEFT JOIN movim mom ON  mom.Cor = prd.Cor');
  QrUpd.SQL.Add('                    AND mom.Tam = prd.Tam');
  QrUpd.SQL.Add('                    AND mom.Grade = prd.Codigo');
  QrUpd.SQL.Add('LEFT JOIN moviv mov ON mom.Controle = mov.Controle');
  QrUpd.SQL.Add('WHERE mov.Cliente=:P0');
  QrUpd.SQL.Add('AND prd.Codigo=:P1');
  QrUpd.Params[0].AsInteger := Entidade;
  QrUpd.Params[1].AsInteger := Produto;
  QrUpd.Open;
  //
  if QrUpd.FieldByName('Data').Value <> Null then
    Data2 := QrUpd.FieldByName('Data').Value
  else
    Data2 := 0;
  //
  QrUpd.Close;
  if Data1 > Data2 then
    Data := Data1
  else
    Data := Data2;
  if Data > 0 then
  begin
    DecodeDate(Data, AnoI, Mes, Dia);
    AnoF := AnoI + QrControleValCur.Value;
    Data := EncodeDate(AnoF, Mes, Dia);
    //
    Result := Data;
  end;
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  QrControle.Close;
  QrControle.Open;
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CONTVEN         := QrControleContVen.Value;
  VAR_CONTCOM         := QrControleContCom.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  Qr??.Open;
  //
  Registros := MLAGeral.Registros(Qr??);
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


function TDmod.ConectarAoSite(var IP: String): Integer;
var
  BD, PW, ID: String;
begin
  Result := -2;
  if Dmod.QrControleWeb_MySQL.Value > 0 then
  begin
    if not MyDBn.Connected then
    begin
      IP := Dmod.QrControleWeb_Host.Value;
      ID := Dmod.QrControleWeb_User.Value;
      PW := Dmod.QrControleWeb_Pwd.Value;
      BD := Dmod.QrControleWeb_DB.Value;
      if MLAGeral.FaltaInfo(4,
        [IP,'Servidor',ID,'Usu�rio',PW,'Senha',BD,'Banco de dados'],
        'Conex�o ao MySQL no meu site') then Exit;
      //Exit;
      //
      MyDBn.Connected    := False;
      MyDBn.UserPassword := PW;
      MyDBn.UserName     := ID;
      MyDBn.Host         := IP;
      MyDBn.DatabaseName := BD; // J� deve existir (configurado pelo hospedeiro)
      try
        MyDBn.Connect;
      finally
        ;
      end;
      Result := MLAGeral.BoolToInt(MyDBn.Connected);
    end else Result := 1;
    //
  end;
  //
end;

function TDmod.ConexaoRemota(Aviso: Integer): Boolean;
var
  IP, Txt: String;
  rtrn: Integer;
begin
  rtrn := -3;
  try
    rtrn := ConectarAoSite(IP);
  finally
    if rtrn = 0 then
    begin
      case Aviso of
        0: ;// nada
        1: Txt := 'N�o h� conex�o com o servidor ' + IP + '!';
        2: Txt := 'N�o foi poss�vel se conectar ao servidor ' + IP + '!';
        else Txt := 'O aplicativo n�o est� conectado ao servidor!';
      end;
      if Txt <> '' then
        Geral.MB_Aviso(Txt);
    end;
  end;
  if rtrn < 0 then rtrn := 0;
  Result := MLAGeral.IntToBool(rtrn);
end;

procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  Result := '';
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDMod.Privilegios(Usuario: Integer) : Boolean;
begin
  FM_MASTER := 'F';
  Dmod.QrPerfis.Close;
  Dmod.QrPerfis.Params[0].AsInteger := Usuario;
  Dmod.QrPerfis.Open;
  Result := Dmod.QrPerfis.RecordCount > 0;
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEnderecoRUA.Value, QrEnderecoNumero.Value, False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' + Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value := Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then Natal := QrEnderecoENatal.Value
  else Natal := QrEnderecoPNatal.Value;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

function TDmod.BuscaProximoMovix: Integer;
begin
  QrSel.Close;
  QrSel.Open;
  Result := QrSelMovix.Value;
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FStrFmtPrc := MLAGeral.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  //
  VAR_MEULOGOPATH := QrControleMeuLogoPath.Value;
  if FileExists(VAR_MEULOGOPATH) then VAR_MEULOGOEXISTE := True
  else VAR_MEULOGOEXISTE := False;
  //
  VAR_LOGONFPATH := QrControleLogoNF.Value;
  if FileExists(VAR_LOGONFPATH) then VAR_LOGONFEXISTE := True
  else VAR_LOGONFEXISTE := False;
  //
  VAR_LOGOBIG1PATH := QrControleLogoBig1.Value;
  if FileExists(VAR_LOGOBIG1PATH) then VAR_LOGOBIG1EXISTE := True
  else VAR_LOGOBIG1EXISTE := False;
  //
  QrDono.Close;
  QrDono.Open;
end;

procedure TDmod.QrControleCalcFields(DataSet: TDataSet);
begin
  QrControleCasasProd.Value := 2;  // Pre�o?
end;

function TDMod.VerificaSenhaEntidade(const Senha: String; var Grupo: Integer;
var Numero: Integer; const MyKey: Integer): Boolean;
var
  i: integer;
  //Tipo: Byte;
  sDecrypted: AnsiString;
  Crypt, NumTxt, Chave: String;
  NumInt: Integer;
  Achou: Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  Grupo  := 0;
  Numero := 0;
  Crypt := UnEncrypt.LimpaStringUEncrypt(Senha);
  if Length(Crypt) = 32 then
  begin
    // MD5
    QrSerialMC.Close;
    QrSerialMC.Open;
    Achou := False;
    while not QrSerialMC.Eof do
    begin
      if Achou then
      begin
        Result := True;
        Screen.Cursor := crDefault;
        Exit;
      end;
      for i := QrSerialMCNumIni.Value to QrSerialMCNumFim.Value do
      begin
        if UnMD5.StrMD5(FormatFloat('00000000000', i) + QrSerialMCSenha.Value) =
        Crypt then
        begin
          Achou := True;
          Grupo  := QrSerialMCCodigo.Value;
          Numero := i;
          Break;
        end;
      end;
      QrSerialMC.Next;
    end;
    Result := Achou;
  end else begin
    // uEncrypt
    sDecrypted := UnEncrypt.Decrypt(Crypt, MyKey);
    NumTxt := Copy(sDecrypted, 1, 4);
    Chave  := Copy(sDecrypted, 5, Length(sDecrypted));
    //verifica s� � um n�mero mesmo
    NumInt := Geral.IMV(NumTxt);
    if Numtxt = FormatFloat('0000', NumInt) then
    begin
      // Procura por grupo de senhas
      QrSerialUC.Close;
      QrSerialUC.Params[0].AsString := Chave;
      QrSerialUC.Open;
      if QrSerialUC.RecordCount > 0 then
      begin
        // Verifica se o n�mero se encontra em alguma faixa
        QrSerialUF.Close;
        QrSerialUF.Params[00].AsInteger := QrSerialUCCodigo.Value;
        QrSerialUF.Params[01].AsInteger := NumInt;
        QrSerialUF.Open;
        if QrSerialUFControle.Value > 0 then
        begin
          // Confirma a genuidade do serial
          Result := True;
          Grupo  := QrSerialUCCodigo.Value;
          Numero := NumInt;
        end;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TDMod.SerialDuplicadoEntidade(SerialG, SerialN: Integer): Boolean;
begin
  //Result := true;
  QrDuplMD5.Close;
  QrDuplMD5.Params[00].AsInteger := SerialG;
  QrDuplMD5.Params[01].AsInteger := SerialN;
  QrDuplMD5.Open;
  //
  Result := QrDuplMD5.RecordCount > 0;
  //n�o fechar QrDuplMD5
  //QrDuplMD5.Close;
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Geral.MB_Aviso('Este balancete n�o possui dados industriais ' +
    'adicionais neste aplicativo!');
end;

end.



