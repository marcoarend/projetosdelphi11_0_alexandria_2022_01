unit CiclosAval;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral, dmkDBEdit,
  ColorGrd, ActnColorMaps, ActnMan, dmkCheckBox, dmkPermissoes, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmCiclosAval = class(TForm)
    DsCiclosAval: TDataSource;
    QrCiclosAval: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PainelData: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    dmkDBEdCodigo: TdmkDBEdit;
    dmkDBEdNome: TdmkDBEdit;
    PainelEdita: TPanel;
    Label10: TLabel;
    Label9: TLabel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkEdNome: TdmkEdit;
    dmkEdCodigo: TdmkEdit;
    QrLocCiclosAval: TmySQLQuery;
    dmkCkAtivo: TdmkCheckBox;
    CkAtivo: TDBCheckBox;
    QrLocCiclosAvalNome: TWideStringField;
    Label3: TLabel;
    dmkDBEdAlunosMin: TdmkDBEdit;
    dmkDBEdAlunosMax: TdmkDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    dmkEdAlunosMin: TdmkEdit;
    Label6: TLabel;
    dmkEdAlunosMax: TdmkEdit;
    QrCiclosAvalCodigo: TIntegerField;
    QrCiclosAvalNome: TWideStringField;
    QrCiclosAvalAlunosMin: TIntegerField;
    QrCiclosAvalAlunosMax: TIntegerField;
    QrCiclosAvalLk: TIntegerField;
    QrCiclosAvalDataCad: TDateField;
    QrCiclosAvalDataAlt: TDateField;
    QrCiclosAvalUserCad: TIntegerField;
    QrCiclosAvalUserAlt: TIntegerField;
    QrCiclosAvalAlterWeb: TSmallintField;
    QrCiclosAvalAtivo: TSmallintField;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCiclosAvalAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCiclosAvalBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure dmkEdNomeExit(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    //
  public
    { Public declarations }
  end;

var
  FmCiclosAval: TFmCiclosAval;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCiclosAval.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCiclosAval.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCiclosAvalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCiclosAval.DefParams;
begin
  VAR_GOTOTABELA := 'ciclosaval';
  VAR_GOTOMYSQLTABLE := QrCiclosAval;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ciclosaval');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCiclosAval.dmkEdNomeExit(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    QrLocCiclosAval.SQL.Clear;
    QrLocCiclosAval.SQL.Add('SELECT Nome FROM ciclosaval WHERE Nome LIKE :P0');
    QrLocCiclosAval.Params[00].AsString := dmkEdNome.ValueVariant;
    QrLocCiclosAval.Open;
    if QrLocCiclosAval.RecordCount > 0 then
    begin
      Application.MessageBox('Esta avalia��o j� foi cadastrada!', 'Avisos', MB_OK+MB_ICONWARNING);
      dmkEdNome.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmCiclosAval.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCiclosAval.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCiclosAval.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCiclosAval.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCiclosAval.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCiclosAval.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCiclosAval.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCiclosAval.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCiclosAvalCodigo.Value;
  Close;
end;

procedure TFmCiclosAval.BtConfirmaClick(Sender: TObject);
var
  Codigo, AlunosMin, AlunosMax : Integer;
  Nome : String;
begin
  Nome      := dmkEdNome.ValueVariant;
  AlunosMin := dmkEdAlunosMin.ValueVariant;
  AlunosMax := dmkEdAlunosMax.ValueVariant;
  //
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdNome.SetFocus;
    Exit;
  end;
  if AlunosMax = 0 then
  begin
    Application.MessageBox('Defina o n�mero m�ximo de alunos!', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdAlunosMax.SetFocus;
    Exit;
  end;
  if AlunosMin > AlunosMax then
  begin
    Application.MessageBox('O n�mero m�ximo de alunos deve ser superior ao n�mero m�nimo de alunos!',
    'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdAlunosMin.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('CiclosAval', 'Codigo', LaTipo.SQLType,
    QrCiclosAvalCodigo.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCiclosAval, LaTipo.SQLType, 'CiclosAval', Codigo,
  Dmod.QrUpd) then
  begin
    MostraEdicao(False, CO_TRAVADO, 0);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCiclosAval.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(dmkEdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'ciclosaval', Codigo);
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ciclosaval', 'Codigo');
end;

procedure TFmCiclosAval.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  //
  QrCiclosAval.Open;
end;

procedure TFmCiclosAval.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCiclosAvalCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCiclosAval.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCiclosAval.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCiclosAval.QrCiclosAvalAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  if QrCiclosAval.RecordCount > 0 then
    BtAltera.Enabled := True
  else
    BtAltera.Enabled := False;
end;

procedure TFmCiclosAval.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclosAval.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCiclosAvalCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ciclosaval', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCiclosAval.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCiclosAval.QrCiclosAvalBeforeOpen(DataSet: TDataSet);
begin
  QrCiclosAvalCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCiclosAval.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrCiclosAval, [PainelDados],
  [PainelEdita], dmkEdNome, LaTipo, 'ciclosaval');
  //
  dmkCkAtivo.Checked  := True;
end;

procedure TFmCiclosAval.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCiclosAval, [PainelDados],
  [PainelEdita], dmkEdNome, LaTipo, 'ciclosaval');
  //
  if QrCiclosAvalAtivo.Value = 0 then
    dmkCkAtivo.Checked := False
  else dmkCkAtivo.Checked := True;
end;

procedure TFmCiclosAval.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Visible := False;
    PainelEdita.Visible := True;
    if Status = CO_INCLUSAO then
    begin
      dmkEdCodigo.ValueVariant := FormatFloat('000', Codigo);
      dmkEdNome.ValueVariant   := '';
      dmkCkAtivo.Checked    := true;
    end else begin
      dmkEdCodigo.ValueVariant := QrCiclosAvalCodigo.Value;
      dmkEdNome.ValueVariant   := QrCiclosAvalNome.Value;
      if QrCiclosAvalAtivo.Value = 0 then
        dmkCkAtivo.Checked  := false
      else
        dmkCkAtivo.Checked  := true;
    end;
    dmkEdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

end.


