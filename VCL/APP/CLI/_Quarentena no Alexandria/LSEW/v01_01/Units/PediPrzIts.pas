unit PediPrzIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, MyDBCheck, Mask, DBCtrls,
  Grids, DBGrids, dmkDBGrid, DB, mySQLDbTables, dmkLabel, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkGeral, dmkValUsu, dmkDBEdit, UnDmkEnums;

type
  TFmPediPrzIts = class(TForm)
    PnControla: TPanel;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    LaTipo: TdmkLabel;
    EdPercent1: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    Label6: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrSum1: TmySQLQuery;
    Label7: TLabel;
    DBEdPercent: TDBEdit;
    Label8: TLabel;
    EdPercent2: TdmkEdit;
    Label9: TLabel;
    EdDias: TdmkEdit;
    DBGrid1: TDBGrid;
    QrSum1Parcelas: TLargeintField;
    QrSum1MedDDSimpl: TFloatField;
    QrSum1MedDDReal: TFloatField;
    QrSum1MedDDPerc1: TFloatField;
    QrSum1MedDDPerc2: TFloatField;
    QrSum1PercentT: TFloatField;
    QrSum1Percent1: TFloatField;
    QrSum1Percent2: TFloatField;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBEdPercentChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaPercent(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmPediPrzIts: TFmPediPrzIts;

implementation

uses UnMySQLCuringa, Module, UnInternalConsts, UMySQLModule, PediPrzCab,
UnMyObjects;

{$R *.DFM}

procedure TFmPediPrzIts.AtualizaPercent(Controle: Integer);
var
  Codigo: Integer;
begin
  Codigo := FmPediPrzCab.QrPediPrzCabCodigo.Value;
  QrSum1.Close;
  QrSum1.Params[0].AsInteger := Codigo;
  QrSum1.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE pediprzcab SET Parcelas=:P0, ');
  Dmod.QrUpd.SQL.Add('PercentT=:P1, Percent1=:P2, Percent2=:P3, ');
  Dmod.QrUpd.SQL.Add('MedDDSimpl=:P4, MedDDReal=:P5, ');
  Dmod.QrUpd.SQL.Add('MedDDPerc1=:P6, MedDDPerc2=:P7 ');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := QrSum1Parcelas.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSum1PercentT.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSum1Percent1.Value;
  Dmod.QrUpd.Params[03].AsFloat   := QrSum1Percent2.Value;
  Dmod.QrUpd.Params[04].AsFloat   := QrSum1MedDDSimpl.Value;
  Dmod.QrUpd.Params[05].AsFloat   := QrSum1MedDDReal.Value;
  Dmod.QrUpd.Params[06].AsFloat   := QrSum1MedDDPerc1.Value;
  Dmod.QrUpd.Params[07].AsFloat   := QrSum1MedDDPerc2.Value;
  //
  Dmod.QrUpd.Params[08].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  FmPediPrzCab.LocCod(Codigo, Codigo);
  FmPediPrzCab.QrPediPrzIts.Locate('Controle', Controle, []);
end;

procedure TFmPediPrzIts.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, FmPediPrzIts, PnEdita, FmPediPrzCab.QrPediPrzIts,
  [PnControla], [PnEdita], EdDias, LaTipo, 'pediprzits');
end;

procedure TFmPediPrzIts.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('pediprzits', 'Controle', LaTipo.SQLType,
    Geral.IMV(EdControle.Text));
  EdControle.ValueVariant := Controle;
  UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPediPrzIts, PnEdita, 'pediprzits',
    Controle, Dmod.QrUpd, [PnEdita], [PnControla], LaTipo, True);
  AtualizaPercent(Controle);
end;

procedure TFmPediPrzIts.BtDesisteClick(Sender: TObject);
begin
  PnControla.Visible := True;
  PnEdita.Visible    := False;
end;

procedure TFmPediPrzIts.BtExcluiClick(Sender: TObject);
begin
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da parcela?', 'pediprzits',
  'Controle', FmPediPrzCab.QrPediPrzItsControle.Value, Dmod.MyDB);
  AtualizaPercent(0);
end;

procedure TFmPediPrzIts.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, FmPediPrzIts, PnEdita, FmPediPrzCab.QrPediPrzIts,
  [PnControla], [PnEdita], EdDias, LaTipo, 'pediprzits');
end;

procedure TFmPediPrzIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediPrzIts.DBEdPercentChange(Sender: TObject);
begin
  if FmPediPrzCab.QrPediPrzCabPercentT.Value = 100 then
    DBEdPercent.Font.Color := clBlue else
    DBEdPercent.Font.Color := clRed;
end;

procedure TFmPediPrzIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPediPrzIts.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (FmPediPrzCab.QrPediPrzCabPercentT.Value <> 100) and
     (FmPediPrzCab.QrPediPrzCabPercentT.Value <>   0) then
  begin
    Application.MessageBox('Antes de fechar a janela, o percentual total de ' +
    'fibras deve ser 100%', 'Aviso', MB_OK+MB_ICONWARNING);
    CanClose := False;
  end;
end;

procedure TFmPediPrzIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
