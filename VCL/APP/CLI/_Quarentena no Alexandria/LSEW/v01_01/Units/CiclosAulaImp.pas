unit CiclosAulaImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, mySQLDbTables,
  ComCtrls, dmkGeral, UMySQLModule, UnInternalConsts, UnDmkEnums;

type
  TFmCiclosAulaImp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCursosWEB: TmySQLQuery;
    DsCursosWEB: TDataSource;
    DBGrid1: TDBGrid;
    QrCursosWEBCodigo: TLargeintField;
    QrCursosWEBCidade: TWideStringField;
    QrCursosWEBPromotor: TWideStringField;
    QrCursosWEBData: TWideStringField;
    QrCursosWEBAlunos: TLargeintField;
    PB2: TProgressBar;
    PB1: TProgressBar;
    QrCursosWEBCodProm: TWordField;
    QrCursosWEBUF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function ObtemListaIdsCiclosAula(): String;
    function VerificaCidade(Cidade: String): Integer;
    function VerificaPromotor(Promotor: Integer): Integer;
  public
    { Public declarations }
    FCodigo, FProfessor, FPromotor: Integer;
    FData, FCidade, FUF: String;
    procedure ReopenCursosWEB(Promotor: Integer; Data, Cidade, UF: String);
  end;

  var
  FmCiclosAulaImp: TFmCiclosAulaImp;

implementation

uses Module, Ciclos2, UnMyObjects, ModuleGeral, UnDmkProcFunc, DmkDAC_PF;

{$R *.DFM}

procedure TFmCiclosAulaImp.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Cidade, Alunos, Professor, Promotor, IdWeb: Integer;
  CPF, UF, DataCur, Nome, Telefone, Celular: String;
  Data: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  //
  PB1.Visible  := True;
  PB1.Position := 0;
  PB1.Max      := 2;
  //
  Codigo    := FCodigo;
  Controle  := UMyMod.BuscaEmLivreY_Def('ciclosaula', 'Controle', stIns, 0);
  Cidade    := VerificaCidade(QrCursosWEBCidade.Value);
  UF        := QrCursosWEBUF.Value;
  Alunos    := QrCursosWEBAlunos.Value;
  Promotor  := VerificaPromotor(QrCursosWEBCodProm.Value);
  Professor := FProfessor;
  Data      := StrToDate(QrCursosWEBData.Value);
  DataCur   := Geral.FDT(Data, 1);
  IdWeb     := QrCursosWEBCodigo.Value;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'ciclosaula', False,
  [
    'CodiCidade', 'UF', 'AlunosInsc', 'Professor',
    'Promotor', 'Data', 'IdWeb', 'Codigo'
  ], ['Controle'], [
    Cidade, UF, Alunos, Professor,
    Promotor, DataCur, IdWeb, Codigo
  ], [Controle]) then
  begin
    PB1.Position := PB1.Position + 1;
    //
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('SELECT ID, Nome, Telefone, Celular, CPF');
    Dmod.QrWeb.SQL.Add('FROM intranet_alunos');
    Dmod.QrWeb.SQL.Add('WHERE curso=:P0');
    Dmod.QrWeb.SQL.Add('ORDER BY Nome');
    Dmod.QrWeb.Params[0].AsInteger := QrCursosWEBCodigo.Value;
    Dmod.QrWeb.Open;
    if Dmod.QrWeb.RecordCount > 0 then
    begin
      PB2.Visible  := True;
      PB2.Position := 0;
      PB2.Max      := Dmod.QrWeb.RecordCount;
      //
      Dmod.QrWeb.First;
      while not Dmod.QrWeb.Eof do
      begin
        Conta    := UMyMod.BuscaEmLivreY_Def('ciclosalu', 'Conta', stIns, 0);
        Nome     := UpperCase(Dmod.QrWeb.FieldByName('Nome').Value);
        Telefone := Dmod.QrWeb.FieldByName('Telefone').Value;
        Telefone := Geral.FormataTelefone_TT(Geral.SoNumero_TT(Telefone));
        Telefone := MlaGeral.SoNumeroESinal_TT(Telefone);
        Celular  := Dmod.QrWeb.FieldByName('Celular').Value;
        Celular  := Geral.FormataTelefone_TT(Geral.SoNumero_TT(Celular));
        Celular  := MLAGeral.SoNumeroESinal_TT(Celular);
        CPF      := Dmod.QrWeb.FieldByName('CPF').Value;
        CPF      := MLAGeral.FormataCNPJ_TFT(CPF);
        //
        if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'ciclosalu', False,
        [
          'Nome', 'Telefone', 'Celular', 'FCurso', 'CPF', 'GradeD',
          'Codigo', 'Controle'
        ], ['Conta'], [
          Nome, Telefone, Celular, 0, CPF, Dmod.QrControleCiclGradeD.Value,
          Codigo, Controle
        ], [Conta]) then
        begin
          PB2.Position := PB2.Position + 1;
          PB2.Update;
          Application.ProcessMessages;
          //
          Dmod.QrWeb.Next;
        end;
      end;
    end;
    FmCiclos2.LocCod(Codigo, Codigo);
    //FmCiclos2.ReopenCiclosAula(Controle);
  end;
  //
  PB1.Visible := False;
  PB2.Visible := False;
  //
  Screen.Cursor := crDefault;
  //
  VAR_CADASTRO := Controle;
  Close;
end;

procedure TFmCiclosAulaImp.BtSaidaClick(Sender: TObject);
begin
  FmCiclos2.FDesiste := True;
  VAR_CADASTRO := 0;
  Close;
end;

procedure TFmCiclosAulaImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ReopenCursosWEB(FPromotor, FData, FCidade, FUF);
end;

procedure TFmCiclosAulaImp.FormCreate(Sender: TObject);
begin
  FmCiclos2.FDesiste := False;
end;

procedure TFmCiclosAulaImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmCiclosAulaImp.ObtemListaIdsCiclosAula: String;
var
  Qry: TmySQLQuery;
begin
  Result := '';
  Qry    := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT IdWeb ',
      'FROM ciclosaula ',
      'WHERE IdWeb <> 0 ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      if Qry.RecNo <> Qry.RecordCount then
        Result := Result + ', ';
      //
      Result := Result + Geral.FF0(Qry.FieldByName('IdWeb').AsInteger);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmCiclosAulaImp.ReopenCursosWEB(Promotor: Integer; Data, Cidade,
  UF: String);
var
  Res: String;
begin
  Res := ObtemListaIdsCiclosAula();
  //
  QrCursosWEB.Close;
  QrCursosWEB.SQL.Clear;
  QrCursosWEB.SQL.Add('SELECT cur.id Codigo, cid.nome Cidade, ufs.uf UF, usr.nome Promotor,');
  QrCursosWEB.SQL.Add('DATE_FORMAT(cur.data_cur, "%d/%m/%Y") Data, COUNT(alu.Id) Alunos, ');
  QrCursosWEB.SQL.Add('usr.id CodProm, cur.*');
  QrCursosWEB.SQL.Add('FROM intranet_curso cur');
  QrCursosWEB.SQL.Add('LEFT JOIN dados_cidades cid ON cur.cidade = cid.id');
  QrCursosWEB.SQL.Add('LEFT JOIN dados_estados ufs ON ufs.id = cur.estado');
  QrCursosWEB.SQL.Add('LEFT JOIN intranet_usuarios usr ON cur.id_user = usr.id');
  QrCursosWEB.SQL.Add('LEFT JOIN intranet_alunos alu ON alu.Curso = cur.Id');
  QrCursosWEB.SQL.Add('WHERE cur.id NOT IN ');
  QrCursosWEB.SQL.Add('("' + Res + '") ');
  //
  if Length(Data) > 0 then
    QrCursosWEB.SQL.Add('AND cur.data_cur="' + Data +'"');
  if Promotor > 0 then
    QrCursosWEB.SQL.Add('AND usr.id=' + IntToStr(Promotor) + '');
  if Length(Cidade) > 0 then
    QrCursosWEB.SQL.Add('AND cid.nome LIKE "' + Cidade + '"');
  if Length(UF) > 0 then
    QrCursosWEB.SQL.Add('AND ufs.uf LIKE "' + UF + '"');
  QrCursosWEB.SQL.Add('GROUP BY alu.Curso');
  QrCursosWEB.Open;
end;

function TFmCiclosAulaImp.VerificaCidade(Cidade: String): Integer;
begin
  DmodG.QrAllUpd.Close;
  DModG.QrAllUpd.SQL.Clear;
  DmodG.QrAllUpd.SQL.Add('SELECT Codigo, Nome');
  DmodG.QrAllUpd.SQL.Add('FROM dtb_munici');
  DmodG.QrAllUpd.SQL.Add('WHERE Nome LIKE "' + Cidade + '"');
  DmodG.QrAllUpd.Open;
  //
  if DmodG.QrAllUpd.RecordCount > 0 then
    Result := DmodG.QrAllUpd.FieldByName('Codigo').Value
  else
    Result := 0;    
  //
  DmodG.QrAllUpd.Close;
end;

function TFmCiclosAulaImp.VerificaPromotor(Promotor: Integer): Integer;
begin
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT Entidade');
  Dmod.QrUpd.SQL.Add('FROM promotores');
  Dmod.QrUpd.SQL.Add('WHERE CodUsu=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Promotor;
  Dmod.QrUpd.Open;
  //
  if Dmod.QrUpd.RecordCount > 0 then
    Result := Dmod.QrUpd.FieldByName('Entidade').Value
  else
    Result := 0;
  //
  Dmod.QrUpd.Close;
end;

end.
