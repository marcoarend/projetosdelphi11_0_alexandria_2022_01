unit GraRel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables,
  frxClass, frxDBSet, DBCtrls, Grids, DBGrids, Mask, dmkPermissoes,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmGraRel = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    CkAtivos: TCheckBox;
    CkInativ: TCheckBox;
    CkPositi: TCheckBox;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGGrupos: TRadioGroup;
    QrEstq: TmySQLQuery;
    QrEstqNOMEGRA: TWideStringField;
    QrEstqNOMECOR: TWideStringField;
    QrEstqNOMETAM: TWideStringField;
    QrEstqCodigo: TIntegerField;
    QrEstqCor: TIntegerField;
    QrEstqTam: TIntegerField;
    QrEstqAtivo: TSmallintField;
    QrEstqPrecoC: TFloatField;
    QrEstqPrecoV: TFloatField;
    QrEstqPediQ: TFloatField;
    QrEstqEstqQ: TFloatField;
    QrEstqEstqV: TFloatField;
    QrEstqEstqC: TFloatField;
    QrEstqGradeG: TIntegerField;
    QrEstqNOMEGRG: TWideStringField;
    frxEstq: TfrxReport;
    frxDsEstq: TfrxDBDataset;
    QrEstqFutQt: TFloatField;
    QrEstqNOMETOT: TWideStringField;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    Label1: TLabel;
    EdGra: TdmkEditCB;
    CBGra: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCor: TdmkEditCB;
    CBCor: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdTam: TdmkEditCB;
    CBTam: TdmkDBLookupComboBox;
    Panel6: TPanel;
    QrGra: TmySQLQuery;
    QrGraCodigo: TIntegerField;
    QrGraNome: TWideStringField;
    DsGra: TDataSource;
    DsCor: TDataSource;
    QrCor: TmySQLQuery;
    QrCorCodigo: TIntegerField;
    QrCorNome: TWideStringField;
    QrTam: TmySQLQuery;
    DsTam: TDataSource;
    QrTamCodigo: TIntegerField;
    QrTamNome: TWideStringField;
    QrProd: TmySQLQuery;
    QrHist: TmySQLQuery;
    QrProdAtivo: TSmallintField;
    QrHistControle: TIntegerField;
    QrHistConta: TIntegerField;
    QrHistGrade: TIntegerField;
    QrHistCor: TIntegerField;
    QrHistTam: TIntegerField;
    QrHistQtd: TFloatField;
    QrHistVal: TFloatField;
    QrHistDataPedi: TDateField;
    QrHistDataReal: TDateField;
    QrHistMotivo: TSmallintField;
    QrHistLk: TIntegerField;
    QrHistDataCad: TDateField;
    QrHistDataAlt: TDateField;
    QrHistUserCad: TIntegerField;
    QrHistUserAlt: TIntegerField;
    QrHistVen: TFloatField;
    TPIniHist: TDateTimePicker;
    Label4: TLabel;
    TPFimHist: TDateTimePicker;
    Label5: TLabel;
    QrAnt: TmySQLQuery;
    QrAntVal: TFloatField;
    QrAntQtd: TFloatField;
    QrAntVen: TFloatField;
    QrHistNOMETIPO: TWideStringField;
    frxHist: TfrxReport;
    frxDsHist: TfrxDBDataset;
    QrHistCUSTOMOV: TFloatField;
    QrHistSALDOCUS: TFloatField;
    PB1: TProgressBar;
    QrHistSALDOQTD: TFloatField;
    QrHistSALDOVAL: TFloatField;
    dmkPermissoes1: TdmkPermissoes;
    QrEstqNOMEDEPTO: TWideStringField;
    RGOrdem5: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxEstqGetValue(const VarName: String; var Value: Variant);
    procedure QrEstqCalcFields(DataSet: TDataSet);
    procedure QrHistCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure RelatorioEstoque;
    procedure HistoricoMercadoria;
  public
    { Public declarations }
  end;

  var
  FmGraRel: TFmGraRel;

implementation

{$R *.DFM}

uses Module, ModuleProd, UnMyObjects;

procedure TFmGraRel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraRel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmGraRel.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGraRel.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  QrGra.Open;
  QrCor.Open;
  QrTam.Open;
  TPIniHist.Date := Date - 30;
  TPFimHist.Date := IncMonth(Date, 60);
end;

procedure TFmGraRel.BtOKClick(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: RelatorioEstoque;
    1: HistoricoMercadoria;
    else Application.MessageBox('Relat�rio n�o implementado', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmGraRel.RelatorioEstoque;
const
  Ordens: array[0..4] of String = ('NOMEDEPTO', 'NOMEGRG', 'NOMEGRA', 'NOMECOR', 'NOMETAM');
  Campos: array[0..4] of String = ('NOMEDEPTO', 'NOMEGRG', 'NOMEGRA', 'NOMECOR', 'NOMETAM');
  Header: array[0..4] of String = ('Departamento', 'Grupo', 'Mercadoria', 'Cor', 'Tamanho');
  Footer: array[0..4] of String = ('Sub-total departamento', 'Sub-total grupo', 'Sub-total mercadoria', 'Sub-total cor', 'Sub-total tamanho');
var
  Ordem, Ativo1, Ativo2: String;
begin
  Ativo1 := MLAGeral.EscolhaDe2Str(CkInativ.Checked, '0', '-1000');
  Ativo2 := MLAGeral.EscolhaDe2Str(CkAtivos.Checked, '1', '-1000');
  Ordem := 'ORDER BY ' + Ordens[RGOrdem1.ItemIndex] + ', '
                       + Ordens[RGOrdem2.ItemIndex] + ', '
                       + Ordens[RGOrdem3.ItemIndex] + ', '
                       + Ordens[RGOrdem4.ItemIndex] + ', '
                       + Ordens[RGOrdem5.ItemIndex];
  QrEstq.Close;
  QrEstq.SQL.Clear;
  QrEstq.SQL.Add('SELECT gra.Nome NOMEGRA, cor.Nome NOMECOR, tam.Nome NOMETAM,');
  QrEstq.SQL.Add('prd.Codigo, prd.Cor, prd.Tam, prd.Ativo, prd.PrecoC,');
  QrEstq.SQL.Add('prd.PrecoV, prd.PediQ, prd.EstqQ, prd.EstqV, prd.EstqC,');
  QrEstq.SQL.Add('gra.GradeG, grg.Nome NOMEGRG, grd.Nome NOMEDEPTO');
  QrEstq.SQL.Add('FROM produtos prd');
  QrEstq.SQL.Add('LEFT JOIN grades   gra ON gra.Codigo=prd.Codigo');
  QrEstq.SQL.Add('LEFT JOIN cores    cor ON cor.Codigo=prd.Cor');
  QrEstq.SQL.Add('LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam');
  QrEstq.SQL.Add('LEFT JOIN gradeg   grg ON grg.Codigo=gra.GradeG');
  QrEstq.SQL.Add('LEFT JOIN graded   grd ON grd.Codigo=grg.GradeD');
  QrEstq.SQL.Add('');
  QrEstq.SQL.Add('WHERE prd.Ativo in ('+Ativo1+','+Ativo2+')');
  if CkPositi.Checked then
    QrEstq.SQL.Add('AND prd.EstqQ > 0');
  QrEstq.SQL.Add(Ordem);
  //MLAGeral.LeMeuSQLy(QrEstq, '', nil, True, True);
  QrEstq.Open;
  //
  frxEstq.Variables['VARF_MAXGRUPO'] := RGGrupos.ItemIndex;

  frxEstq.Variables['VARF_GRUPO1']   := ''''+'frxDsEstq."'+Ordens[RGOrdem1.ItemIndex]+'"''';
  frxEstq.Variables['VARF_HEADR1']   := ''''+Header[RGOrdem1.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem1.ItemIndex]+'"]''';
  frxEstq.Variables['VARF_FOOTR1']   := ''''+Footer[RGOrdem1.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frxEstq.Variables['VARF_GRUPO2']   := ''''+'frxDsEstq."'+Ordens[RGOrdem2.ItemIndex]+'"''';
  frxEstq.Variables['VARF_HEADR2']   := ''''+Header[RGOrdem2.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem2.ItemIndex]+'"]''';
  frxEstq.Variables['VARF_FOOTR2']   := ''''+Footer[RGOrdem2.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem2.ItemIndex]+'"]''';

  frxEstq.Variables['VARF_GRUPO3']   := ''''+'frxDsEstq."'+Ordens[RGOrdem3.ItemIndex]+'"''';
  frxEstq.Variables['VARF_HEADR3']   := ''''+Header[RGOrdem3.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem3.ItemIndex]+'"]''';
  frxEstq.Variables['VARF_FOOTR3']   := ''''+Footer[RGOrdem1.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frxEstq.Variables['VARF_GRUPO4']   := ''''+'frxDsEstq."'+Ordens[RGOrdem4.ItemIndex]+'"''';
  frxEstq.Variables['VARF_HEADR4']   := ''''+Header[RGOrdem4.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem4.ItemIndex]+'"]''';
  frxEstq.Variables['VARF_FOOTR4']   := ''''+Footer[RGOrdem1.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  frxEstq.Variables['VARF_GRUPO5']   := ''''+'frxDsEstq."'+Ordens[RGOrdem5.ItemIndex]+'"''';
  frxEstq.Variables['VARF_HEADR5']   := ''''+Header[RGOrdem5.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem5.ItemIndex]+'"]''';
  frxEstq.Variables['VARF_FOOTR5']   := ''''+Footer[RGOrdem1.ItemIndex]+': [frxDsEstq."'+Ordens[RGOrdem1.ItemIndex]+'"]''';

  MyObjects.frxMostra(frxEstq, 'Estoque de Mercadorias');
end;

procedure TFmGraRel.frxEstqGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_DATA_HORA') = 0 then
    Value := Geral.FDT(Now, 8)
  else if AnsiCompareText(VarName, 'VARF_MERCADORIA') = 0 then
    Value := CBGra.Text + ' ' + CBCor.Text + ' ' + CBTam.Text;
end;

procedure TFmGraRel.QrEstqCalcFields(DataSet: TDataSet);
begin
  QrEstqFutQt.Value := QrEstqEstqQ.Value + QrEstqPediQ.Value;
  QrEstqNOMETOT.Value := QrEstqNOMEGRA.Value + ' ' + QrEstqNOMECOR.Value + ' ' +
    QrEstqNOMETAM.Value;
end;

procedure TFmGraRel.HistoricoMercadoria;
var
  BalIni, IniHist, FimHist: TDateTime;
  Qtd, Val: Double;
begin
  Screen.Cursor := crHourGlass;
  QrProd.Close;
  QrProd.Params[00].AsInteger := Geral.IMV(EdGra.Text);
  QrProd.Params[01].AsInteger := Geral.IMV(EdCor.Text);
  QrProd.Params[02].AsInteger := Geral.IMV(EdTam.Text);
  QrProd.Open;
  if QrProd.RecordCount = 0 then
  begin
    Application.MessageBox('Mercadoria / cor / tamanho n�o cadastrada!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  IniHist := Int(TPIniHist.Date);
  FimHist := Int(TPFimHist.Date);
  BalIni := Int(MLAGeral.PrimeiraDataDoPeriodo(DmProd.VerificaBalanco));
  QrAnt.Close;
  if BalIni < IniHist then
  begin
    QrAnt.Params[00].AsInteger := Geral.IMV(EdGra.Text);
    QrAnt.Params[01].AsInteger := Geral.IMV(EdCor.Text);
    QrAnt.Params[02].AsInteger := Geral.IMV(EdTam.Text);
    QrAnt.Params[03].AsString  := Geral.FDT(BalIni, 1);
    QrAnt.Params[04].AsString  := Geral.FDT(IniHist-1, 1);
    QrAnt.Open;
    //
    Qtd := QrAntQtd.Value;
    Val := QrAntVal.Value;
  end else begin
    Qtd := 0;
    Val := 0;
  end;
  QrHist.Close;
  QrHist.Params[00].AsInteger := Geral.IMV(EdGra.Text);
  QrHist.Params[01].AsInteger := Geral.IMV(EdCor.Text);
  QrHist.Params[02].AsInteger := Geral.IMV(EdTam.Text);
  QrHist.Params[03].AsString  := Geral.FDT(IniHist, 1);
  QrHist.Params[04].AsString  := Geral.FDT(FimHist, 1);
  QrHist.Open;
  PB1.Visible := True;
  PB1.Max := QrHist.RecordCount;
  PB1.Position := 0;
  QrHist.First;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE movim SET SALDOQTD=:P0, SALDOVAL=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Conta=:P3');
  Dmod.QrUpd.SQL.Add('');
  while not QrHist.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Qtd := Qtd + QrHistQtd.Value;
    Val := Val + QrHistVal.Value;
    Dmod.QrUpd.Params[00].AsFloat   := Qtd;
    Dmod.QrUpd.Params[01].AsFloat   := Val;
    Dmod.QrUpd.Params[02].AsInteger := QrHistConta.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrHist.Next;
  end;
  // reabrir para atualizar SALDOQTD e SALDOVAL
  QrHist.Close;
  QrHist.Open;
  PB1.Visible := False;
  MyObjects.frxMostra(frxHist, 'Hist�rico do movimento de mercadoria');
end;

procedure TFmGraRel.QrHistCalcFields(DataSet: TDataSet);
begin
  QrHistNOMETIPO.Value := MLAGeral.TipoMovim_Txt(QrHistMotivo.Value);
  if QrHistQtd.Value = 0 then QrHistCUSTOMOV.Value := 0 else
    QrHistCUSTOMOV.Value := QrHistVal.Value / QrHistQtd.Value;
  if QrHistSALDOQTD.Value = 0 then QrHistSALDOCUS.Value := 0 else
    QrHistSALDOCUS.Value := QrHistSALDOVAL.Value / QrHistSALDOQTD.Value;
end;

end.
