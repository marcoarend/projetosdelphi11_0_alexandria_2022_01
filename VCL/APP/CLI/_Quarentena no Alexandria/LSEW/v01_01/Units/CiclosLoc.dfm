object FmCiclosLoc: TFmCiclosLoc
  Left = 339
  Top = 185
  Caption = 'CIC-CICLO-012 :: Localizar ciclo'
  ClientHeight = 320
  ClientWidth = 816
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 272
    Width = 816
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 185
    ExplicitWidth = 554
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 704
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 442
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 48
    Align = alTop
    Caption = 'Localizar Ciclo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 554
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 814
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 816
    Height = 224
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 554
    ExplicitHeight = 137
    object PainelDados: TPanel
      Left = 1
      Top = 1
      Width = 814
      Height = 50
      Align = alTop
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 5
      object Label10: TLabel
        Left = 8
        Top = 4
        Width = 47
        Height = 13
        Caption = 'Professor:'
      end
      object EdProfessor: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdProfessorChange
        DBLookupComboBox = CBProfessor
      end
      object CBProfessor: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 441
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEPROFE'
        ListSource = DmCiclos.DsProfessores
        TabOrder = 1
        dmkEditCB = EdProfessor
        UpdType = utYes
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 51
      Width = 814
      Height = 172
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Ciclo'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPRF'
          Title.Caption = 'Professor'
          Width = 260
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataSaida'
          Title.Caption = 'Data de sa'#237'da'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataChega'
          Title.Caption = 'Data de retorno'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataAcert'
          Title.Caption = 'Data de acerto'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'STATUS_TXT'
          Title.Caption = 'Etapa atual do ciclo'
          Width = 150
          Visible = True
        end>
      Color = clWindow
      DataSource = DsCiclos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dmkDBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Ciclo'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEPRF'
          Title.Caption = 'Professor'
          Width = 260
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataSaida'
          Title.Caption = 'Data de sa'#237'da'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataChega'
          Title.Caption = 'Data de retorno'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataAcert'
          Title.Caption = 'Data de acerto'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'STATUS_TXT'
          Title.Caption = 'Etapa atual do ciclo'
          Width = 150
          Visible = True
        end>
    end
  end
  object QrCiclos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCiclosCalcFields
    SQL.Strings = (
      'SELECT cic.*, '
      'IF(prf.Tipo=0, prf.RazaoSocial, prf.Nome) NOMEPRF,'
      'IF(prm.Tipo=0, prm.RazaoSocial, prm.Nome) NOMEPRM, '
      'prf.CartPref CART_PROF, prm.CartPref CART_PROM,'
      'prf.RolComis ROLCOMIS_PRF'
      'FROM ciclos cic'
      'LEFT JOIN entidades prf ON prf.Codigo=cic.Professor'
      'LEFT JOIN entidades prm ON prm.Codigo=cic.Promotor'
      'WHERE cic.Professor=:P0')
    Left = 520
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCiclosSTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Calculated = True
    end
    object QrCiclosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ciclos.Codigo'
      Required = True
    end
    object QrCiclosDataSaida: TDateField
      FieldName = 'DataSaida'
      Origin = 'ciclos.DataSaida'
      Required = True
    end
    object QrCiclosDataChega: TDateField
      FieldName = 'DataChega'
      Origin = 'ciclos.DataChega'
      Required = True
    end
    object QrCiclosDataAcert: TDateField
      FieldName = 'DataAcert'
      Origin = 'ciclos.DataAcert'
      Required = True
    end
    object QrCiclosAlunosInsc: TIntegerField
      FieldName = 'AlunosInsc'
      Origin = 'ciclos.AlunosInsc'
      Required = True
    end
    object QrCiclosAlunosAula: TIntegerField
      FieldName = 'AlunosAula'
      Origin = 'ciclos.AlunosAula'
      Required = True
    end
    object QrCiclosStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'ciclos.Status'
      Required = True
    end
    object QrCiclosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'ciclos.Controle'
      Required = True
    end
    object QrCiclosAlunosPrc: TFloatField
      FieldName = 'AlunosPrc'
      Origin = 'ciclos.AlunosPrc'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAlunosVal: TFloatField
      FieldName = 'AlunosVal'
      Origin = 'ciclos.AlunosVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAlunosPgt: TFloatField
      FieldName = 'AlunosPgt'
      Origin = 'ciclos.AlunosPgt'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProdVal: TFloatField
      FieldName = 'ProdVal'
      Origin = 'ciclos.ProdVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProdPgt: TFloatField
      FieldName = 'ProdPgt'
      Origin = 'ciclos.ProdPgt'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProdMrg: TFloatField
      FieldName = 'ProdMrg'
      Origin = 'ciclos.ProdMrg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosDespesas: TFloatField
      FieldName = 'Despesas'
      Origin = 'ciclos.Despesas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosNOMEPRF: TWideStringField
      FieldName = 'NOMEPRF'
      Size = 100
    end
    object QrCiclosTransfCre: TFloatField
      FieldName = 'TransfCre'
      Origin = 'ciclos.TransfCre'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosTransfDeb: TFloatField
      FieldName = 'TransfDeb'
      Origin = 'ciclos.TransfDeb'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosCurso: TIntegerField
      FieldName = 'Curso'
      Origin = 'ciclos.Curso'
      Required = True
    end
    object QrCiclosRatDebVar: TFloatField
      FieldName = 'RatDebVar'
      Origin = 'ciclos.RatDebVar'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosRatTrfFix: TFloatField
      FieldName = 'RatTrfFix'
      Origin = 'ciclos.RatTrfFix'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosRatTrfVar: TFloatField
      FieldName = 'RatTrfVar'
      Origin = 'ciclos.RatTrfVar'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosComisVal: TFloatField
      FieldName = 'ComisVal'
      Origin = 'ciclos.ComisVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosSdoProfIni: TFloatField
      FieldName = 'SdoProfIni'
      Origin = 'ciclos.SdoProfIni'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosSdoProfFim: TFloatField
      FieldName = 'SdoProfFim'
      Origin = 'ciclos.SdoProfFim'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosComisPgt: TFloatField
      FieldName = 'ComisPgt'
      Origin = 'ciclos.ComisPgt'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosComisTrf: TFloatField
      FieldName = 'ComisTrf'
      Origin = 'ciclos.ComisTrf'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProfVal: TFloatField
      FieldName = 'ProfVal'
      Origin = 'ciclos.ProfVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosEmprestC: TFloatField
      FieldName = 'EmprestC'
      Origin = 'ciclos.EmprestC'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosEmprestD: TFloatField
      FieldName = 'EmprestD'
      Origin = 'ciclos.EmprestD'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosDespProm: TFloatField
      FieldName = 'DespProm'
      Origin = 'ciclos.DespProm'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosDespProf: TFloatField
      FieldName = 'DespProf'
      Origin = 'ciclos.DespProf'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosValAluDil: TFloatField
      FieldName = 'ValAluDil'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosValAluVen: TFloatField
      FieldName = 'ValAluVen'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProfessor: TIntegerField
      FieldName = 'Professor'
    end
    object QrCiclosRolComis: TIntegerField
      FieldName = 'RolComis'
    end
    object QrCiclosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCiclosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCiclosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCiclosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCiclosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCiclosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCiclosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCiclosPromotor: TIntegerField
      FieldName = 'Promotor'
    end
    object QrCiclosNOMEPRM: TWideStringField
      FieldName = 'NOMEPRM'
      Size = 100
    end
    object QrCiclosCART_PROF: TIntegerField
      FieldName = 'CART_PROF'
    end
    object QrCiclosCART_PROM: TIntegerField
      FieldName = 'CART_PROM'
    end
    object QrCiclosROLCOMIS_PRF: TIntegerField
      FieldName = 'ROLCOMIS_PRF'
    end
  end
  object DsCiclos: TDataSource
    DataSet = QrCiclos
    Left = 548
    Top = 56
  end
end
