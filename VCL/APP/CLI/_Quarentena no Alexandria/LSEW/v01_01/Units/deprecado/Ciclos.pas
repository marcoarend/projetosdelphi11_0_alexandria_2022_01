unit Ciclos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkLabel, frxClass, frxDBSet, dmkGeral, dmkCheckBox, ComCtrls,
  dmkEditDateTimePicker, Variants, dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TEmpresta = (empVai, empVolta);
  //TValAluCalc = (vacAlunos, vacPreco, vacTotal);
  TFmCiclos = class(TForm)
    PainelDados: TPanel;
    DsCiclos: TDataSource;
    QrCiclos: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtProdutos: TBitBtn;
    BtCiclos: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TEdit;
    PainelData: TPanel;
    Label1: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PMCiclos: TPopupMenu;
    PainelItens: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BitBtn2: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    Label7: TLabel;
    QrCiclosIts: TmySQLQuery;
    DsCiclosIts: TDataSource;
    QrCiclosItsCodigo: TIntegerField;
    QrCiclosItsControle: TIntegerField;
    QrCiclosItsNomeImp: TWideStringField;
    QrCiclosItsGenero: TIntegerField;
    QrCiclosItsValor: TFloatField;
    QrCiclosItsLk: TIntegerField;
    QrCiclosItsDataCad: TDateField;
    QrCiclosItsDataAlt: TDateField;
    QrCiclosItsUserCad: TIntegerField;
    QrCiclosItsUserAlt: TIntegerField;
    QrCiclosItsAlterWeb: TSmallintField;
    QrCiclosItsAtivo: TSmallintField;
    PMVende: TPopupMenu;
    LaTipo: TdmkLabel;
    frxCiclo_A: TfrxReport;
    frxDsCiclosIts: TfrxDBDataset;
    frxDsCiclos: TfrxDBDataset;
    EdPromotor: TdmkEditCB;
    Label6: TLabel;
    Label10: TLabel;
    EdProfessor: TdmkEditCB;
    CBProfessor: TdmkDBLookupComboBox;
    TPDataSaida: TdmkEditDateTimePicker;
    TPDataChega: TdmkEditDateTimePicker;
    TPDataAcert: TdmkEditDateTimePicker;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Excluicicloatual1: TMenuItem;
    Alteracicloatual1: TMenuItem;
    Incluinovociclo1: TMenuItem;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    QrCiclosDATASAIDA_TXT: TWideStringField;
    QrCiclosDATACHEGA_TXT: TWideStringField;
    QrCiclosDATAACERT_TXT: TWideStringField;
    QrCiclosSTATUS_TXT: TWideStringField;
    Label11: TLabel;
    DBEdit8: TDBEdit;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    ExcluiitemdemercadoriaVenda1: TMenuItem;
    N1: TMenuItem;
    Avaarparaprximaetapa1: TMenuItem;
    Voltarparaetapaanterior1: TMenuItem;
    PMLevados: TPopupMenu;
    Incluinovaconsignao1: TMenuItem;
    Excluiconsignaodemercadoria1: TMenuItem;
    PMDevolve: TPopupMenu;
    DesfazTODOprocessodeconsignao1: TMenuItem;
    Devolvemercadoriasconsignadas1: TMenuItem;
    Incluialteravendademercadoria1: TMenuItem;
    BtVolta: TBitBtn;
    BtAvanca: TBitBtn;
    BtDespesas: TBitBtn;
    BtTransf: TBitBtn;
    DBEdit9: TDBEdit;
    N2: TMenuItem;
    Incluipagamento1: TMenuItem;
    Manual1: TMenuItem;
    Automtico1: TMenuItem;
    QrCiclosCodigo: TIntegerField;
    QrCiclosDataSaida: TDateField;
    QrCiclosDataChega: TDateField;
    QrCiclosDataAcert: TDateField;
    QrCiclosDespesas: TFloatField;
    QrCiclosAlunosInsc: TIntegerField;
    QrCiclosAlunosAula: TIntegerField;
    QrCiclosStatus: TSmallintField;
    QrCiclosControle: TIntegerField;
    QrCiclosPromotor: TIntegerField;
    QrCiclosAlunosPrc: TFloatField;
    QrCiclosAlunosVal: TFloatField;
    QrCiclosProdVal: TFloatField;
    QrCiclosProdPgt: TFloatField;
    QrCiclosNOMEPRF: TWideStringField;
    QrCiclosNOMEPRM: TWideStringField;
    N3: TMenuItem;
    CalculaEReabre1: TMenuItem;
    QrCiclosProdMrg: TFloatField;
    GroupBox1: TGroupBox;
    DBEdit12: TDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit13: TDBEdit;
    Label19: TLabel;
    DBEdit14: TDBEdit;
    GroupBox2: TGroupBox;
    Label15: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label16: TLabel;
    Label20: TLabel;
    DBEdit15: TDBEdit;
    Label21: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    QrCiclosAlunosPgt: TFloatField;
    Label23: TLabel;
    DBEdProdif: TDBEdit;
    QrCiclosPRODDIF: TFloatField;
    Excluipagamentos1: TMenuItem;
    QrCiclosCART_PROF: TIntegerField;
    DBEdAlunosDif: TDBEdit;
    Label24: TLabel;
    QrCiclosALUNOSDIF: TFloatField;
    QrCiclosTransfCre: TFloatField;
    QrCiclosTransfDeb: TFloatField;
    GroupBox4: TGroupBox;
    Label29: TLabel;
    DBEdit18: TDBEdit;
    Label30: TLabel;
    DBEdit19: TDBEdit;
    QrCiclosTRANSFDIF: TFloatField;
    Label31: TLabel;
    DBEdit20: TDBEdit;
    BtRateio: TBitBtn;
    PMRateio: TPopupMenu;
    Incluireceitadecurso1: TMenuItem;
    Alterareceitadecurso1: TMenuItem;
    Excluireceitadecurso1: TMenuItem;
    Rateiavalores1: TMenuItem;
    N4: TMenuItem;
    GroupBox5: TGroupBox;
    Label32: TLabel;
    DBEdit21: TDBEdit;
    QrCiclosVALORRATEIO: TFloatField;
    Label36: TLabel;
    EdCurso: TdmkEditCB;
    CBCurso: TdmkDBLookupComboBox;
    SpeedButton7: TSpeedButton;
    QrCiclosCurso: TIntegerField;
    QrCiclosRatDebVar: TFloatField;
    QrCiclosRatTrfFix: TFloatField;
    QrCiclosRatTrfVar: TFloatField;
    QrCiclosRATEIODIF: TFloatField;
    QrCiclosComisVal: TFloatField;
    GroupBox6: TGroupBox;
    QrCiclosTOT_RECEITAS: TFloatField;
    Label40: TLabel;
    DBEdit28: TDBEdit;
    QrCiclosTOT_CREDITOS: TFloatField;
    Label41: TLabel;
    DBEdit29: TDBEdit;
    QrCiclosTOT_RECE_LIQ: TFloatField;
    Label42: TLabel;
    DBEdit30: TDBEdit;
    QrCiclosTOT_SALD_LIQ: TFloatField;
    Label43: TLabel;
    DBEdit31: TDBEdit;
    QrCiclosSdoProfIni: TFloatField;
    QrCiclosSdoProfFim: TFloatField;
    PMTransf: TPopupMenu;
    Incluitransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    GroupBox7: TGroupBox;
    DBEdit27: TDBEdit;
    Label39: TLabel;
    QrCiclosComisPgt: TFloatField;
    Label33: TLabel;
    DBEdit22: TDBEdit;
    QrCiclosCOMISDIF: TFloatField;
    DBEdComisDif: TDBEdit;
    Label37: TLabel;
    BtEmpresta: TBitBtn;
    PMEmpresta: TPopupMenu;
    Incluiemprstimo1: TMenuItem;
    Alteraemprstimoatual1: TMenuItem;
    Excluiemprstimos1: TMenuItem;
    Emprestaafuncionrio1: TMenuItem;
    Devoluodefuncionrio1: TMenuItem;
    QrCiclosEmprestC: TFloatField;
    QrCiclosEmprestD: TFloatField;
    QrCiclosEMPRES_SDOATU: TFloatField;
    QrCiclosEMPRES_SDOFIM: TFloatField;
    QrCiclosEMPRES_SDOVER: TFloatField;
    QrCiclosAPAGAR_PROF: TFloatField;
    GroupBox8: TGroupBox;
    Label34: TLabel;
    DBEdit23: TDBEdit;
    Label38: TLabel;
    DBEdit25: TDBEdit;
    DBEdit24: TDBEdit;
    Label35: TLabel;
    QrCiclosComisTrf: TFloatField;
    Label44: TLabel;
    DBEdit26: TDBEdit;
    DBEdit32: TDBEdit;
    Label45: TLabel;
    QrCiclosPAGOAPROF: TFloatField;
    QrCiclosProfVal: TFloatField;
    QrCiclosEMPRES_SDOATU_NEG: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    DBGL: TDBGrid;
    TabSheet4: TTabSheet;
    DBGT: TDBGrid;
    TabSheet5: TTabSheet;
    DBGV: TDBGrid;
    DBGPagtosV_: TDBGrid;
    TabSheet6: TTabSheet;
    DBGDespProf: TDBGrid;
    TabSheet7: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet8: TTabSheet;
    DBGEmprestimos: TDBGrid;
    TabSheet9: TTabSheet;
    DBGRateio: TDBGrid;
    DsCiclosAula: TDataSource;
    QrCiclosAula: TmySQLQuery;
    N5: TMenuItem;
    urmas1: TMenuItem;
    Incluiturma1: TMenuItem;
    Alteraturma1: TMenuItem;
    Excluiturma1: TMenuItem;
    QrCiclosAulaCodigo: TIntegerField;
    QrCiclosAulaControle: TIntegerField;
    QrCiclosAulaData: TDateField;
    QrCiclosAulaAlunosInsc: TIntegerField;
    QrCiclosAulaAlunosAula: TIntegerField;
    QrCiclosAulaCidade: TWideStringField;
    QrCiclosAulaUF: TWideStringField;
    QrCiclosAulaLocalEnd: TWideStringField;
    QrCiclosAulaLocalVal: TFloatField;
    QrCiclosAulaHotelNome: TWideStringField;
    QrCiclosAulaHotelCont: TWideStringField;
    QrCiclosAulaHotelTele: TWideStringField;
    QrCiclosAulaHotelVal: TFloatField;
    QrCiclosAulaOnibusNome: TWideStringField;
    QrCiclosAulaOnibusHora: TWideStringField;
    QrCiclosAulaObserva: TWideStringField;
    QrCiclosAulaLk: TIntegerField;
    QrCiclosAulaDataCad: TDateField;
    QrCiclosAulaDataAlt: TDateField;
    QrCiclosAulaUserCad: TIntegerField;
    QrCiclosAulaUserAlt: TIntegerField;
    QrCiclosAulaAlterWeb: TSmallintField;
    QrCiclosAulaAtivo: TSmallintField;
    QrCiclosAulaLocalNom: TWideStringField;
    QrCiclosAulaLocalPes: TWideStringField;
    QrCiclosAulaLocalTel: TWideStringField;
    QrCiclosAulaValorUni: TFloatField;
    QrCiclosAulaValorTot: TFloatField;
    PMDespesas: TPopupMenu;
    Incluidespesaspromotor1: TMenuItem;
    Alteradespesapromotor1: TMenuItem;
    Excluidespesaspromotor1: TMenuItem;
    CBPromotor: TdmkDBLookupComboBox;
    QrCiclosCART_PROM: TIntegerField;
    QrCiclosDespProm: TFloatField;
    QrCiclosDespProf: TFloatField;
    QrCiclosProfessor: TIntegerField;
    QrCiclosROLCOMIS_PRF: TIntegerField;
    DBEdit33: TDBEdit;
    Label25: TLabel;
    DBEdit34: TDBEdit;
    Label26: TLabel;
    Panel4: TPanel;
    Panel10: TPanel;
    Splitter1: TSplitter;
    Panel11: TPanel;
    Panel12: TPanel;
    DBGDespProm: TDBGrid;
    Panel13: TPanel;
    DBGrid2: TDBGrid;
    Promotor1: TMenuItem;
    Professor1: TMenuItem;
    Incluidespesasprofessor1: TMenuItem;
    Alteradespesaprofessor1: TMenuItem;
    Excluidespesasprofessor1: TMenuItem;
    QrCiclosAulaComPromPer: TFloatField;
    QrCiclosAulaComPromVal: TFloatField;
    Label27: TLabel;
    EdRolComis: TdmkEditCB;
    CBRolComis: TdmkDBLookupComboBox;
    SpeedButton8: TSpeedButton;
    QrCiclosRolComis: TIntegerField;
    QrCiclosAulaComPromMin: TFloatField;
    QrCiclosAulaComProfPer: TFloatField;
    QrCiclosAulaComProfVal: TFloatField;
    QrCiclosAulaComProfMin: TFloatField;
    QrCiclosAulaDTSPromPer: TFloatField;
    QrCiclosAulaDTSPromVal: TFloatField;
    QrCiclosAulaDTSPromMin: TFloatField;
    QrCiclosAulaDTSProfPer: TFloatField;
    QrCiclosAulaDTSProfVal: TFloatField;
    QrCiclosAulaDTSProfMin: TFloatField;
    QrCiclosAulaPromotor: TIntegerField;
    QrCiclosAulaDTSPromDeb: TFloatField;
    QrCiclosAulaComPromDeb: TFloatField;
    QrCiclosAulaDTSProfDeb: TFloatField;
    QrCiclosAulaComProfDeb: TFloatField;
    QrCiclosAulaBonPromVal: TFloatField;
    QrCiclosAulaBonPromWho: TIntegerField;
    QrCiclosAulaProfessor: TIntegerField;
    Panel14: TPanel;
    GradeTurmas: TDBGrid;
    DBGrid3: TDBGrid;
    Splitter2: TSplitter;
    DBGrid4: TDBGrid;
    Splitter3: TSplitter;
    QrCiclosAulaComProfAlu: TFloatField;
    QrCiclosAulaComProfPrd: TFloatField;
    QrCiclosAulaComProfSaq: TFloatField;
    QrCiclosAulaComProfAll: TFloatField;
    QrCiclosValAluDil: TFloatField;
    QrCiclosValAluVen: TFloatField;
    GroupBox3: TGroupBox;
    Label28: TLabel;
    DBEdit35: TDBEdit;
    Label46: TLabel;
    DBEdit36: TDBEdit;
    QrCiclosVALALUDIF: TFloatField;
    Label47: TLabel;
    DBEdVALALUDIF: TDBEdit;
    QrRetido: TmySQLQuery;
    QrRetidoValor: TFloatField;
    Label48: TLabel;
    DBEdit37: TDBEdit;
    DsRetido: TDataSource;
    frxDsRetido: TfrxDBDataset;
    frxCiclo_B: TfrxReport;
    PMImprime: TPopupMenu;
    FonteGrande1: TMenuItem;
    FonteMdia1: TMenuItem;
    N6: TMenuItem;
    Rateio1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    QrCiclosAulaComProfPgt: TFloatField;
    QrCiclosAulaCodiCidade: TIntegerField;
    QrCiclosAulaMUNI: TWideStringField;
    DBEdCodigo: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtCiclosClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCiclosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCiclosBeforeOpen(DataSet: TDataSet);
    procedure Crianovogrupo1Click(Sender: TObject);
    procedure Alteragrupoatual1Click(Sender: TObject);
    procedure Excluigrupoatual1Click(Sender: TObject);
    procedure BtProdutosClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrCiclosAfterScroll(DataSet: TDataSet);
    procedure PMCiclosPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrCiclosCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Avaarparaprximaetapa1Click(Sender: TObject);
    procedure Voltarparaetapaanterior1Click(Sender: TObject);
    procedure Incluinovaconsignao1Click(Sender: TObject);
    procedure Excluiconsignaodemercadoria1Click(Sender: TObject);
    procedure Incluialteravendademercadoria1Click(Sender: TObject);
    procedure Devolvemercadoriasconsignadas1Click(Sender: TObject);
    procedure DesfazTODOprocessodeconsignao1Click(Sender: TObject);
    procedure BtVoltaClick(Sender: TObject);
    procedure BtAvancaClick(Sender: TObject);
    procedure ExcluiitemdemercadoriaVenda1Click(Sender: TObject);
    procedure BtDespesasClick(Sender: TObject);
    procedure Manual1Click(Sender: TObject);
    procedure CalculaEReabre1Click(Sender: TObject);
    procedure Automtico1Click(Sender: TObject);
    procedure DBEdProdifChange(Sender: TObject);
    procedure Excluipagamentos1Click(Sender: TObject);
    procedure EdAlunosAulaEnter(Sender: TObject);
    procedure EdAlunosPrcEnter(Sender: TObject);
    procedure EdAlunosValEnter(Sender: TObject);
    procedure EdAlunosAulaExit(Sender: TObject);
    procedure EdAlunosPrcExit(Sender: TObject);
    procedure EdAlunosValExit(Sender: TObject);
    procedure DBEdAlunosDifChange(Sender: TObject);
    procedure BtTransfClick(Sender: TObject);
    procedure Incluireceitadecurso1Click(Sender: TObject);
    procedure PMRateioPopup(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure Incluitransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure Excluireceitadecurso1Click(Sender: TObject);
    procedure BtRateioClick(Sender: TObject);
    procedure Rateiavalores1Click(Sender: TObject);
    procedure DBEdComisDifChange(Sender: TObject);
    procedure Alterareceitadecurso1Click(Sender: TObject);
    procedure BtEmprestaClick(Sender: TObject);
    procedure Devoluodefuncionrio1Click(Sender: TObject);
    procedure Alteraemprstimoatual1Click(Sender: TObject);
    procedure Excluiemprstimos1Click(Sender: TObject);
    procedure Emprestaafuncionrio1Click(Sender: TObject);
    procedure Incluiturma1Click(Sender: TObject);
    procedure Alteraturma1Click(Sender: TObject);
    procedure Excluiturma1Click(Sender: TObject);
    procedure Incluidespesaspromotor1Click(Sender: TObject);
    procedure Alteradespesapromotor1Click(Sender: TObject);
    procedure Excluidespesaspromotor1Click(Sender: TObject);
    procedure QrCiclosAulaAfterScroll(DataSet: TDataSet);
    procedure QrCiclosAulaBeforeClose(DataSet: TDataSet);
    procedure QrCiclosBeforeClose(DataSet: TDataSet);
    procedure Incluidespesasprofessor1Click(Sender: TObject);
    procedure Excluidespesasprofessor1Click(Sender: TObject);
    procedure Alteradespesaprofessor1Click(Sender: TObject);
    procedure PMTransfPopup(Sender: TObject);
    procedure PMVendePopup(Sender: TObject);
    procedure DBEdVALALUDIFChange(Sender: TObject);
    procedure frxCiclo_AGetValue(const VarName: string; var Value: Variant);
    procedure FonteGrande1Click(Sender: TObject);
    procedure FonteMdia1Click(Sender: TObject);
    procedure Rateio1Click(Sender: TObject);
  private
    //FValAluCalc: TValAluCalc;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenMovimL(Conta: Integer);
    procedure ReopenMovimT(Conta: Integer);
    procedure ReopenMovimV(Conta: Integer);
    procedure ReopenMovimD(Conta: Integer);
    procedure ReopenCiclosAula(Controle: Integer);
    procedure ReopenTransfDia(Controle: Integer);
    procedure ReopenDespProm(Controle: Integer);
    procedure ReopenDespProf(Controle: Integer);
    procedure ReopenPagtosV(Controle: Integer);
    procedure ReopenTransf(Controle: Integer);
    procedure ReopenRateio(Controle: Integer);
    procedure ReopenAuto(Controle: Integer);
    procedure ReopenEmprestimos(Controle: Integer);
    procedure ReopenRetido();
    procedure CadastraEquiGru(Edit: TdmkEditCB; DBCB: TdmkDBLookupComboBox);
    procedure MostraCiclosMovV_Venda;
    procedure MostraCiclosMovV_Levar;
    procedure LocalizaGridEtapaAtual;
    procedure VoltaEtapa();
    procedure AvancaEtapa();
    procedure CalculaEReabre();
    //procedure CalculaValorAlunos();
    procedure Emprestimo(Tipo: TEmpresta; Valor: Double);

    function ItensTrazidosIgualAItensLevados(Avisa: Boolean): Word;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCiclosIts(Controle: Integer);
  end;

var
  FmCiclos: TFmCiclos;
const
  FFormatFloat = '00000';
  FMaxStatusedit = 3;

implementation

uses Module, UnFinanceiro, Entidades, MyDBCheck, CiclosMovV, UnInternalConsts3,
ModuleProd, ModuleCiclos, Principal, LctEdit, EquiGru, CiclosRat, CiclosAula,
MyGlyfs, ModuleFin, MeuDBUses, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCiclos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCiclos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCiclosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCiclos.DefParams;
begin
  VAR_GOTOTABELA := 'ciclos';
  VAR_GOTOMYSQLTABLE := QrCiclos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cic.*,');
  VAR_SQLx.Add('IF(prf.Tipo=0, prf.RazaoSocial, prf.Nome) NOMEPRF, ');
  VAR_SQLx.Add('IF(prm.Tipo=0, prm.RazaoSocial, prm.Nome) NOMEPRM, ');
  VAR_SQLx.Add('prf.CartPref CART_PROF, prm.CartPref CART_PROM, ');
  VAR_SQLx.Add('prf.RolComis ROLCOMIS_PRF');
  VAR_SQLx.Add('FROM ciclos cic');
  VAR_SQLx.Add('LEFT JOIN entidades prf ON prf.Codigo=cic.Professor');
  VAR_SQLx.Add('LEFT JOIN entidades prm ON prm.Codigo=cic.Promotor');
  VAR_SQLx.Add('WHERE cic.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND cic.Codigo=:P0');
  //
  VAR_SQLa.Add('');//AND Nome Like :P0');
  //
end;

procedure TFmCiclos.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelItens.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text             := FormatFloat(FFormatFloat, Codigo);
        EdPromotor.Text           := '';
        CBPromotor.KeyValue       := Null;
        EdProfessor.Text          := '';
        CBProfessor.KeyValue      := Null;
        EdRolComis.Text           := '';
        CBRolComis.KeyValue       := Null;
        EdCurso.Text              := '';
        CBCurso.KeyValue          := Null;
        TPDataSaida.Date          := 0;
        TPDataChega.Date          := 0;
        TPDataAcert.Date          := 0;
      end else begin
        EdCodigo.Text             := DBEdCodigo.Text;
        EdPromotor.ValueVariant   := QrCiclosPromotor.Value;
        CBPromotor.KeyValue       := QrCiclosPromotor.Value;
        EdProfessor.ValueVariant  := QrCiclosProfessor.Value;
        CBProfessor.KeyValue      := QrCiclosProfessor.Value;
        EdRolComis.ValueVariant   := QrCiclosRolComis.Value;
        CBRolComis.KeyValue       := QrCiclosRolComis.Value;
        EdCurso.ValueVariant      := QrCiclosCurso.Value;
        CBCurso.KeyValue          := QrCiclosCurso.Value;
        TPDataSaida.Date          := QrCiclosDataSaida.Value;
        TPDataChega.Date          := QrCiclosDataChega.Value;
        TPDataAcert.Date          := QrCiclosDataAcert.Value;
      end;
      EdPromotor.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmCiclos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCiclos.AlteraRegistro;
var
  Ciclos : Integer;
begin
  Ciclos := QrCiclosCodigo.Value;
  if not UMyMod.SelLockY(Ciclos, Dmod.MyDB, 'ciclos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Ciclos, Dmod.MyDB, 'ciclos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCiclos.IncluiRegistro;
var
  Cursor : TCursor;
  Ciclos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Ciclos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
    'ciclos', 'ciclos', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Ciclos))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, Ciclos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCiclos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCiclos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCiclos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCiclos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCiclos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCiclos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCiclos.BtCiclosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCiclos, BtCiclos);
end;

procedure TFmCiclos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCiclosCodigo.Value;
  Close;
end;

procedure TFmCiclos.BtConfirmaClick(Sender: TObject);
var
  Codigo, Promotor, Professor, Controle: Integer;
  DataSaida, DataChega, DataAcert: String;
begin
  Promotor := EdPromotor.ValueVariant;
  if Promotor = 0 then
  begin
    Application.MessageBox('Defina o grupo de promotores!', 'Erro',
      MB_OK+MB_ICONERROR);
    EdPromotor.SetFocus;
    Exit;
  end;
  //
  Professor := EdProfessor.ValueVariant;
  if Professor = 0 then
  begin
    Application.MessageBox('Defina o professor!.', 'Erro', MB_OK+MB_ICONERROR);
    EdProfessor.SetFocus;
    Exit;
  end;
  //
  DataSaida := Geral.FDT(TPDataSaida.Date, 1);
  DataChega := Geral.FDT(TPDataChega.Date, 1);
  DataAcert := Geral.FDT(TPDataAcert.Date, 1);
  Codigo := Geral.IMV(EdCodigo.Text);
  {if CkEncerrado.Checked then Status := 9 else
  if TPDataAcert.Date > 1 then Status := 3 else
  if TPDataChega.Date > 1 then Status := 2 else
  if TPDataSaida.Date > 1 then Status := 1 else
  Status := 0;
  }
  if LaTipo.SQLType = stIns then
    Controle := Dmod.BuscaProximoMovix
  else
    Controle := QrCiclosControle.Value;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'ciclos', False,
  [
    'Professor', 'Promotor', 'DataSaida', 'DataChega', 'DataAcert',
    //'AlunosInsc', 'AlunosAula', 'AlunosPrc', 'AlunosVal',
    'Curso
    ', 'RolComis', 'Controle'
  ], ['Codigo'],
  [
    Professor, Promotor, DataSaida, DataChega, DataAcert,
    //EdAlunosInsc.ValueVariant, EdAlunosAula.ValueVariant,
    //EdAlunosPrc.ValueVariant, EdAlunosVal.ValueVariant,
    EdCurso.ValueVariant, EdRolComis.ValueVariant, Controle
  ], [Codigo]) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ciclos', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmCiclos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'ciclos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ciclos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ciclos', 'Codigo');
end;

procedure TFmCiclos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  PageControl2.Align := alClient;
  PageControl1.Align := alClient;
  CriaOForm;
  //
  DmCiclos.QrPromotores.Close;
  DmCiclos.QrPromotores.Open;
  DmCiclos.QrProfessores.Close;
  DmCiclos.QrProfessores.Open;
  DmCiclos.QrCursos.Close;
  DmCiclos.QrCursos.Open;
  DmCiclos.QrEquiCom.Close;
  DmCiclos.QrEquiCom.Open;
  //
  DBGrid3.Height := 100;
  DBGrid4.Height := 100;
end;

procedure TFmCiclos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCiclosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCiclos.SbNomeClick(Sender: TObject);
begin
  FmPrincipal.MostraCiclosLoc;
  if FmPrincipal.FCiclo > 0 then
    LocCod(FmPrincipal.FCiclo, FmPrincipal.FCiclo);
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCiclos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCiclos.QrCiclosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCiclos.FonteGrande1Click(Sender: TObject);
begin
  DmCiclos.PreparaImpressaoCiclo(
    QrCiclosCodigo.Value, QrCiclosControle.Value, QrCiclosProfessor.Value);
  MyObjects.frxMostra(frxCiclo_A, 'Ciclo de Cursos');
end;

procedure TFmCiclos.FonteMdia1Click(Sender: TObject);
begin
  DmCiclos.PreparaImpressaoCiclo(
    QrCiclosCodigo.Value, QrCiclosControle.Value, QrCiclosProfessor.Value);
  MyObjects.frxMostra(frxCiclo_B, 'Ciclo de Cursos');
end;

procedure TFmCiclos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclos.SbQueryClick(Sender: TObject);
begin
  FmPrincipal.MostraCiclosLoc;
  if FmPrincipal.FCiclo > 0 then
    LocCod(FmPrincipal.FCiclo, FmPrincipal.FCiclo);
{  LocCod(QrCiclosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ciclos', Dmod.MyDB, CO_VAZIO));}
end;

procedure TFmCiclos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
  Panel10.Height := Trunc(Panel4.Height / 2);
end;

procedure TFmCiclos.frxCiclo_AGetValue(const VarName: string; var Value: Variant);
begin
  if VarName = 'VAR_PRODCOMIS' then
  begin
    DmCiclos.QrProdAlu.Close;
    DmCiclos.QrProdAlu.Params[0].AsInteger := QrCiclosRolComis.Value;
    DmCiclos.QrProdAlu.Open;
    Value := '';
    while not DmCiclos.QrProdAlu.Eof do
    begin
     Value := Value + ', ' + DmCiclos.QrProdAluNome.Value;
     DmCiclos.QrProdAlu.Next;
    end;
    Value := Copy(Value, 2, Length(Value));
  end;
end;

procedure TFmCiclos.QrCiclosBeforeClose(DataSet: TDataSet);
begin
  QrCiclosAula.Close;
  DmCiclos.QrMovimL.Close;
  DmCiclos.QrMovimT.Close;
  DmCiclos.QrMovimV.Close;
  DmCiclos.QrDespProf.Close;
  DmCiclos.QrDespProm.Close;
  DmCiclos.QrTransfAll.Close;
  DmCiclos.QrTransfDia.Close;
  DmCiclos.QrRateio.Close;
  DmCiclos.QrAuto.Close;
  DmCiclos.QrEmprestimos.Close;
  QrRetido.Close;
  //
end;

procedure TFmCiclos.QrCiclosBeforeOpen(DataSet: TDataSet);
begin
  QrCiclosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCiclos.Crianovogrupo1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCiclos.Alteragrupoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCiclos.Excluigrupoatual1Click(Sender: TObject);
begin
  ShowMessage('A��o n�o implementada');
  //UMyMod.SQLDel1(Dmod.QrUpd, QrCiclos, 'Ciclos', 'Codigo',
  //QrCiclosCodigo.Value, True, 'Confirma a exclus�o do ciclo ' +
  //IntToStr(QrCiclosCodigo.Value) + '"?');
end;

procedure TFmCiclos.BtProdutosClick(Sender: TObject);
begin
  PageControl2.ActivePageIndex := QrCiclosStatus.Value;
  case QrCiclosStatus.Value of
    0:
    begin
      MyObjects.MostraPopUpDeBotao(PMLevados, BtProdutos);
    end;
    1:
    begin
      MyObjects.MostraPopUpDeBotao(PMDevolve, BtProdutos);
    end;
    2:
    begin
      if (PageControl1.ActivePageIndex = 0) and
         (PageControl2.ActivePageIndex = 2) then
      else
        PageControl1.ActivePageIndex := 1;
      MyObjects.MostraPopUpDeBotao(PMVende, BtProdutos);
    end
    else begin
      Application.MessageBox('A etapa atual n�o permite editar mercadorias!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
end;

procedure TFmCiclos.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCiclos.BitBtn1Click(Sender: TObject);
{var
  Genero, Codigo, Controle: Integer;
  }
begin
  {
  Codigo := QrCiclosCodigo.Value;
  Genero := Geral.IMV(EdGenero.Text);
  //
  if (Codigo = 0) then
  begin
    Application.MessageBox('Defina o curso!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  if (Genero = 0) then
  begin
    Application.MessageBox(PChar('Defina a conta do plano de contas!'),
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  //
  Controle := UMyMod.BuscaEmLivreY_Def('Ciclosits', 'Controle',
    LaTipo.SQLType, QrCiclosItsControle.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'Ciclosits', False,
  [
    'Codigo', 'Genero', 'Valor', 'NomeImp'
  ], ['Controle'],
  [
  Codigo, Genero, EdValor.ValueVariant, EdNomeImp.Text
  ], [Controle], True) then
  begin
    MostraEdicao(0, stLok, 0);
    RecalculaTotal(Codigo);
    ReopenCiclosIts(Controle);
  end;
  }
end;

procedure TFmCiclos.QrCiclosAfterScroll(DataSet: TDataSet);
begin
  ReopenCiclosIts(0);
  ReopenPagtosV(0);
  ReopenTransf(0);
  ReopenRateio(0);
  ReopenAuto(0);
  ReopenEmprestimos(0);
  ReopenRetido();
  LocalizaGridEtapaAtual;
  BtVolta.Enabled  := QrCiclosStatus.Value > 0;
  BtAvanca.Enabled := QrCiclosStatus.Value < 9;
end;

procedure TFmCiclos.QrCiclosAulaAfterScroll(DataSet: TDataSet);
begin
  ReopenDespProf(0);
  ReopenDespProm(0);
  ReopenTransfDia(0);
  ReopenMovimD(0);
end;

procedure TFmCiclos.QrCiclosAulaBeforeClose(DataSet: TDataSet);
begin
  DmCiclos.QrDespProm.Close;
  DmCiclos.QrMovimD.Close;
end;

procedure TFmCiclos.ReopenCiclosIts(Controle: Integer);
begin
  ReopenCiclosAula(QrCiclosAulaControle.Value);
  ReopenMovimL(DmCiclos.QrMovimLConta.Value);
  ReopenMovimT(DmCiclos.QrMovimTConta.Value);
  ReopenMovimV(DmCiclos.QrMovimVConta.Value);
end;

procedure TFmCiclos.ReopenCiclosAula(Controle: Integer);
begin
  QrCiclosAula.Close;
  QrCiclosAula.Params[0].AsInteger := QrCiclosCodigo.Value;
  QrCiclosAula.Open;
  //
  QrCiclosAula.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenMovimL(Conta: Integer);
begin
  DmCiclos.QrMovimL.Close;
  DmCiclos.QrMovimL.Params[0].AsInteger := QrCiclosControle.Value;
  DmCiclos.QrMovimL.Open;
  //
  DmCiclos.QrMovimL.Locate('Conta', Conta, []);
end;

procedure TFmCiclos.ReopenMovimT(Conta: Integer);
begin
  DmCiclos.QrMovimT.Close;
  DmCiclos.QrMovimT.Params[0].AsInteger := QrCiclosControle.Value;
  DmCiclos.QrMovimT.Open;
  //
  DmCiclos.QrMovimT.Locate('Conta', Conta, []);
end;

procedure TFmCiclos.ReopenMovimV(Conta: Integer);
begin
  DmCiclos.QrMovimV.Close;
  DmCiclos.QrMovimV.Params[0].AsInteger := QrCiclosControle.Value;
  DmCiclos.QrMovimV.Open;
  //
  DmCiclos.QrMovimV.Locate('Conta', Conta, []);
end;

procedure TFmCiclos.ReopenMovimD(Conta: Integer);
begin
  DmCiclos.QrMovimD.Close;
  DmCiclos.QrMovimD.Params[00].AsInteger := QrCiclosControle.Value;
  DmCiclos.QrMovimD.Params[01].AsInteger := QrCiclosAulaControle.Value;
  DmCiclos.QrMovimD.Open;
  //
  DmCiclos.QrMovimD.Locate('Conta', Conta, []);
end;

procedure TFmCiclos.ReopenDespProf(Controle: Integer);
begin
  DmCiclos.QrDespProf.Close;
  DmCiclos.QrDespProf.Params[00].AsInteger := QrCiclosAulaControle.Value;
  DmCiclos.QrDespProf.Params[01].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrDespProf.Open;
  //
  DmCiclos.QrDespProf.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenTransfDia(Controle: Integer);
begin
  DmCiclos.QrTransfDia.Close;
  DmCiclos.QrTransfDia.Params[00].AsInteger := QrCiclosAulaControle.Value;
  DmCiclos.QrTransfDia.Params[01].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrTransfDia.Open;
  //
  DmCiclos.QrTransfDia.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenDespProm(Controle: Integer);
begin
  DmCiclos.QrDespProm.Close;
  DmCiclos.QrDespProm.Params[00].AsInteger := QrCiclosAulaControle.Value;
  DmCiclos.QrDespProm.Params[01].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrDespProm.Open;
  //
  DmCiclos.QrDespProm.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenAuto(Controle: Integer);
begin
  DmCiclos.QrAuto.Close;
  DmCiclos.QrAuto.Params[0].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrAuto.Open;
  //
  DmCiclos.QrAuto.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenEmprestimos(Controle: Integer);
begin
  DmCiclos.QrEmprestimos.Close;
  DmCiclos.QrEmprestimos.Params[0].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrEmprestimos.Open;
  //
  DmCiclos.QrEmprestimos.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenPagtosV(Controle: Integer);
begin
  {
  DmCiclos.QrPagtosV.Close;
  DmCiclos.QrPagtosV.Params[0].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrPagtosV.Open;
  //
  DmCiclos.QrPagtosV.Locate('Controle', Controle, []);
  }
end;

procedure TFmCiclos.ReopenTransf(Controle: Integer);
begin
  DmCiclos.QrTransfAll.Close;
  DmCiclos.QrTransfAll.Params[0].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrTransfAll.Open;
  //
  DmCiclos.QrTransfAll.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenRateio(Controle: Integer);
begin
  DmCiclos.QrRateio.Close;
  DmCiclos.QrRateio.Params[0].AsInteger := QrCiclosCodigo.Value;
  DmCiclos.QrRateio.Open;
  //
  DmCiclos.QrRateio.Locate('Controle', Controle, []);
end;

procedure TFmCiclos.ReopenRetido();
begin
  QrRetido.Close;
  QrRetido.Params[0].AsInteger := QrCiclosCodigo.Value;
  QrRetido.Open;
end;

procedure TFmCiclos.MostraCiclosMovV_Venda;
begin
  PageControl2.ActivePageIndex := 2;
  if (QrCiclosDataAcert.Value < 1) or (QrCiclosDataSaida.Value < 1)
  or (QrCiclosDataChega.Value < 1) then
  begin
    Application.MessageBox(PChar('Venda cancelada! Data de sa�da e/ou data ' +
    'de chegada e/ou data de acerto n�o definida(s)!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrCiclosRolComis.Value = 0  then
  begin
    Application.MessageBox(PChar('Venda cancelada! Ciclo sem rol de ' +
    'comiss�es definido! Para definir v� no cadastro da entidade na aba ' +
    'Miscel�nea'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //if DmCiclos.ProfessorDiferenteDe1(QrCiclosProfessor.Value) then Exit;
  //
  if DBCheck.CriaFm(TFmCiclosMovV, FmCiclosMovV, afmoNegarComAviso) then
  with FmCiclosMovV do
  begin
    DBGProd.DataSource := DmCiclos.DsMovimD;
    FQrMovim  := DmCiclos.QrMovimD;
    FControle := QrCiclosControle.Value;
    FSubCtrl  := 3; // Vender
    FMotivo   := 23; //Efetiva��o de venda consignada
    FSubCta   := QrCiclosAulaControle.Value;
    FDataPedi := QrCiclosDataSaida.Value;
    FDataReal := QrCiclosDataAcert.Value;
    FRolComis := QrCiclosRolComis.Value;
    //
    EdRolComis.ValueVariant := QrCiclosRolComis.Value;
    CBRolComis.KeyValue     := QrCiclosRolComis.Value;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmCiclos.PMCiclosPopup(Sender: TObject);
begin
  UMyMod.HabilitaMenuItemInt([Alteracicloatual1, Excluicicloatual1],
    QrCiclos, 'Codigo', 1, 0);
  Alteracicloatual1.Enabled :=
    Alteracicloatual1.Enabled and (QrCiclosStatus.Value < 9);
end;

procedure TFmCiclos.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmCiclos.QrCiclosCalcFields(DataSet: TDataSet);
begin
  QrCiclosDATASAIDA_TXT.Value := Geral.FDT(QrCiclosDataSaida.Value, 15);
  QrCiclosDATACHEGA_TXT.Value := Geral.FDT(QrCiclosDataChega.Value, 15);
  QrCiclosDATAACERT_TXT.Value := Geral.FDT(QrCiclosDataAcert.Value, 15);
  case QrCiclosStatus.Value of
    0: QrCiclosSTATUS_TXT.Value := 'Ciclo aberto';
    1: QrCiclosSTATUS_TXT.Value := 'Curso em andamento';
    2: QrCiclosSTATUS_TXT.Value := 'Retorno de viagem';
    3: QrCiclosSTATUS_TXT.Value := 'Acerto em andamento';
    //
    9: QrCiclosSTATUS_TXT.Value := 'Ciclo encerrado';
  end;
  QrCiclosPRODDIF.Value     := QrCiclosProdPgt.Value    - QrCiclosProdVal.Value (*+ QrCiclosAlunosVal.Value*);
  QrCiclosALUNOSDIF.Value   := QrCiclosAlunosPgt.Value  - QrCiclosAlunosVal.Value;
  QrCiclosTRANSFDIF.Value   := QrCiclosTransfDeb.Value  - QrCiclosTransfCre.Value;
  QrCiclosVALORRATEIO.Value := QrCiclosAlunosVal.Value  - QrCiclosDespesas.Value;
  QrCiclosRATEIODIF.Value   := (QrCiclosAlunosPgt.Value - QrCiclosDespesas.Value)
    - (QrCiclosRatTrfFix.Value + QrCiclosRatTrfVar.Value);

  //

  QrCiclosTOT_RECEITAS.Value  := QrCiclosAlunosVal.Value    + (*ERRO*) QrCiclosProdVal.Value;
  QrCiclosTOT_CREDITOS.Value  := QrCiclosTOT_RECEITAS.Value + QrCiclosTransfCre.Value;
  QrCiclosTOT_RECE_LIQ.Value  := QrCiclosTOT_CREDITOS.Value - QrCiclosDespesas.Value;
  QrCiclosTOT_SALD_LIQ.Value  := QrCiclosTOT_RECE_LIQ.Value - QrCiclosTransfDeb.Value;
  //
  QrCiclosCOMISDIF.Value      := QrCiclosComisTrf.Value     - QrCiclosComisVal.Value;
  QrCiclosEMPRES_SDOATU.Value := QrCiclosEmprestC.Value     - QrCiclosEmprestD.Value;
  QrCiclosEMPRES_SDOFIM.Value := QrCiclosSdoProfIni.Value   + QrCiclosEMPRES_SDOATU.Value;
  if QrCiclosStatus.Value > FMaxStatusedit then
    QrCiclosEMPRES_SDOVER.Value := QrCiclosSdoProfFim.Value
  else
    QrCiclosEMPRES_SDOVER.Value := QrCiclosEMPRES_SDOFIM.Value;
  QrCiclosAPAGAR_PROF.Value     := QrCiclosComisVal.Value - QrCiclosTOT_SALD_LIQ.Value;
  QrCiclosPAGOAPROF.Value       := QrCiclosComisPgt.Value - QrCiclosTOT_SALD_LIQ.Value;
  QrCiclosEMPRES_SDOATU_NEG.Value := QrCiclosEMPRES_SDOATU.Value * -1;
  //
  QrCiclosVALALUDIF.Value       := QrCiclosValAluVen.Value - QrCiclosValAluDil.Value;
end;

procedure TFmCiclos.SpeedButton5Click(Sender: TObject);
begin
  //CadastraEquiGru(EdGruProm, CBGruProm);
end;

procedure TFmCiclos.SpeedButton6Click(Sender: TObject);
begin
  CadastraEquiGru(EdProfessor, CBProfessor);
  {VAR_ENTIDADE := 0;
  FmPrincipal.CadastroEntidades;
  if VAR_ENTIDADE <> 0 then
  begin
    DmCiclos.QrProfessor.Close;
    DmCiclos.QrProfessor.Open;
    //
    EdProfessor.ValueVariant := VAR_ENTIDADE;
    CBProfessor.KeyValue     := VAR_ENTIDADE;
  end;
  }
end;

procedure TFmCiclos.CadastraEquiGru(Edit: TdmkEditCB; DBCB: TdmkDBLookupComboBox);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEquiGru, FmEquiGru, afmoNegarComAviso) then
  begin
    FmEquiGru.ShowModal;
    FmEquiGru.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      //DmCiclos.QrProfessor.Close;
      //DmCiclos.QrProfessor.Open;
      //
      DmCiclos.QrPromotores.Close;
      DmCiclos.QrPromotores.Open;
      //
      Edit.Text := IntToStr(VAR_CADASTRO);
      DBCB.KeyValue := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmCiclos.Avaarparaprximaetapa1Click(Sender: TObject);
begin
  AvancaEtapa;
end;

procedure TFmCiclos.BtAvancaClick(Sender: TObject);
begin
  AvancaEtapa();
end;

procedure TFmCiclos.AvancaEtapa();
var
  Continua: Word;
  Status: Integer;
begin
  Continua := ID_NO;
  Status := QrCiclosStatus.Value + 1;
  case QrCiclosStatus.Value of
    0:
    begin
      if QrCiclosDataSaida.Value < 2 then
        Application.MessageBox(PChar('A data de sa�da do professor n�o foi ' +
        'informada!'), 'Aviso', MB_OK+MB_ICONWARNING)
      else
        if DmCiclos.QrMovimL.RecordCount > 0 then Continua := ID_YES else
        Continua :=
          Application.MessageBox(PChar('N�o foi definida nenhuma consigna��o ' +
          ' de mercadoria! Deseja continuar sem defin�-la?'), 'Pergunta',
          MB_YESNOCANCEL+MB_ICONQUESTION);
    end;
    1:
    begin
      if (QrCiclosDataChega.Value < 2)
      or (QrCiclosDataChega.Value < QrCiclosDataSaida.Value) then
        Application.MessageBox(PChar('A data de chegada do professor n�o foi ' +
        'informada ou � anterior a data de sa�da!'), 'Aviso', MB_OK+MB_ICONWARNING)
      else
      if (QrCiclosDataAcert.Value < 2)
      or (QrCiclosDataAcert.Value < QrCiclosDataChega.Value) then
        Application.MessageBox(PChar('A data de acerto n�o foi ' +
        'informada ou � anterior a data de chegada!'), 'Aviso', MB_OK+MB_ICONWARNING)
      else
      Continua := ItensTrazidosIgualAItensLevados(True);
    end;
    2:
    begin
      {
      if QrCiclosPRODDIF.Value <> 0 then
      Application.MessageBox(PChar('H� diverg�ncia no pagamento das ' +
      'mercadorias vendidas!'), 'Aviso', MB_OK+MB_ICONWARNING) else
      }
      Continua := ID_YES;
    end;
    3:
    begin
      if QrCiclosALUNOSDIF.Value <> 0 then
      Application.MessageBox(PChar('H� diverg�ncia no pagamento das ' +
      'frequ�ncias nos cursos!'), 'Aviso', MB_OK+MB_ICONWARNING) else
      if QrCiclosPRODDIF.Value <> 0 then
      Application.MessageBox(PChar('H� diverg�ncia no recebimento das ' +
      'vendas de mercadorias!'+#13#10+'Verifique se o rateio est� correto e ' +
      'se no cadastro do rol de comiss�es do professor todas mercadorias aqui ' +
      'vendidas possuem uma conta de cr�dito, para que a transfer�ncia tenha ' +
      'ocorrido corretamente no rateio!'), 'Aviso', MB_OK+MB_ICONWARNING) else
      {
      if QrCiclosCOMISDIF.Value <> 0 then
      Application.MessageBox(PChar('H� diverg�ncia no pagamento das ' +
      'comiss�es ao professor!'), 'Aviso', MB_OK+MB_ICONWARNING) else
      }
      if QrCiclosVALALUDIF.Value <> 0 then
      Application.MessageBox(PChar('H� diverg�ncia nas comiss�es do professor ' +
      'sobre os alunos das dilig�ncias para as vendas!'), 'Aviso', MB_OK+MB_ICONWARNING) else
      if DmCiclos.PodeEncerrarCiclo(QrCiclosCodigo.Value, QrCiclosProfessor.Value) then
        Continua := ID_YES;
    end;
    // N�o impedir ir ao nove
    4..8: Continua := ID_YES;
  end;
  if QrCiclosStatus.Value >= FMaxStatusedit then
  begin
    Status := 9;
    DmCiclos.SaldosProfessor(QrCiclosCodigo.Value, QrCiclosProfessor.Value,
      QrCiclosEMPRES_SDOATU.Value);
  end;
  if Continua = ID_YES then
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ciclos', False,
    ['status'], ['codigo'], [Status], [QrCiclosCodigo.Value], True) then
    LocCod(QrCiclosCodigo.Value, QrCiclosCodigo.Value);
end;

procedure TFmCiclos.Voltarparaetapaanterior1Click(Sender: TObject);
begin
  VoltaEtapa();
end;

procedure TFmCiclos.BtVoltaClick(Sender: TObject);
begin
  VoltaEtapa();
end;

procedure TFmCiclos.VoltaEtapa();
var
  Status: Integer;
  Continua: Word;
  Msg: String;
begin
  Continua := ID_NO;
  Msg := '';
  case QrCiclosStatus.Value of
    0: Msg := 'N�o h� etapa anterior. Esta � a primeira etapa!';
    1:
    begin
      if DmCiclos.QrMovimT.RecordCount > 0 then Msg := 'O retorno � etapa anterior s� ' +
        'pode ocorrer ap�s a exclus�o do retorno das mercadorias!'
      else
        Continua := ID_YES;
    end;
    2:
    begin
      if DmCiclos.QrMovimV.RecordCount > 0 then Msg := 'O retorno � etapa anterior s� ' +
        'pode ocorrer ap�s a exclus�o das mercadorias vendidas!'
      else
        Continua := ID_YES;
    end;
    9:
    begin
      if DmCiclos.PodeReabrirCiclo(QrCiclosCodigo.Value, QrCiclosProfessor.Value) then
      Continua := ID_YES;

      {
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT Codigo');
      Dmod.QrAux.SQL.Add('FROM ciclos');
      Dmod.QrAux.SQL.Add('WHERE Codigo > :P0');
      Dmod.QrAux.SQL.Add('AND Status=9');
      Dmod.QrAux.Params[0].AsInteger := QrCiclosCodigo.Value;
      Dmod.QrAux.Open;
      case Dmod.QrAux.RecordCount of
        0: Continua := ID_YES;
        1: Msg := 'H� um ciclo posterior encerrado impedindo a altera��o deste ciclo!';
        else Msg := 'H� ' + FormatFloat('0', Dmod.QrAux.RecordCount) +
           ' ciclos posteriores j� encerrados impedindo a altera��o deste ciclo!';
      end;
      }
    end;
    else Continua := ID_YES;
  end;
  if Msg <> '' then
    Application.MessageBox(PChar(Msg), 'Aviso', MB_OK+MB_ICONWARNING);
  if Continua <> ID_YES then Exit;
  if QrCiclosStatus.Value > 0 then
  begin
    if QrCiclosStatus.Value = 9 then
      Status := FMaxStatusedit
    else
      Status := QrCiclosStatus.Value -1;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ciclos', False,
    ['status'], ['codigo'], [Status], [QrCiclosCodigo.Value], True) then
    LocCod(QrCiclosCodigo.Value, QrCiclosCodigo.Value);
  end;
end;

procedure TFmCiclos.Incluinovaconsignao1Click(Sender: TObject);
begin
  MostraCiclosMovV_Levar;
end;

procedure TFmCiclos.MostraCiclosMovV_Levar;
begin
  if QrCiclosDataSaida.Value < 1 then
  begin
    Application.MessageBox('Venda cancelada! Data de sa�da n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DBCheck.CriaFm(TFmCiclosMovV, FmCiclosMovV, afmoNegarComAviso) then
  with FmCiclosMovV do
  begin
    DBGProd.DataSource := DmCiclos.DsMovimL;
    FQrMovim  := DmCiclos.QrMovimL;
    FControle := QrCiclosControle.Value;
    FSubCtrl  := 1; // Levar
    FMotivo   := 22; //Sa�da para venda consignada
    FDataPedi := QrCiclosDataSaida.Value;
    FDataReal := QrCiclosDataSaida.Value;
    EdPrc.Enabled := False;
    EdCua.Enabled := False;
    EdVen.Enabled := False;
    EdVla.Enabled := False;
    //
    EdRolComis.ValueVariant := QrCiclosRolComis.Value;
    CBRolComis.KeyValue     := QrCiclosRolComis.Value;
    ShowModal;
    Destroy;
  end;
end;


procedure TFmCiclos.Excluiconsignaodemercadoria1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmCiclos.QrMovimL, DBGL,
    'movim', ['conta'], ['conta'], istPergunta, '');
end;

procedure TFmCiclos.Incluialteravendademercadoria1Click(Sender: TObject);
begin
  MostraCiclosMovV_Venda;
  CalculaEReabre();
end;

procedure TFmCiclos.LocalizaGridEtapaAtual;
begin
  if QrCiclosStatus.Value < 2 then
    PageControl2.ActivePageIndex := QrCiclosStatus.Value;
end;

function TFmCiclos.ItensTrazidosIgualAItensLevados(Avisa: Boolean): Word;
begin
  if (DmCiclos.QrMovimL.RecordCount = DmCiclos.QrMovimT.RecordCount) then
    Result := ID_YES
  else begin
    Result := ID_NO;
    if Avisa then Application.MessageBox(PChar('Itens de consigna��o ' +
    'devolvidos n�o conferem com os levados!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCiclos.Devolvemercadoriasconsignadas1Click(Sender: TObject);
var
  Motivo, SubCtrl, Conta, DataChega: Integer;
  DataPedi, DataReal: String;
  QtdX, ValX, CusX: Double;
begin
  Conta := 0;
  if DmCiclos.QrMovimL.RecordCount = 0 then
  begin
    Application.MessageBox('N�o existem mercadorias consignadas para serem devolvidas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DataChega := Trunc(QrCiclosDataChega.Value);
  if (DataChega < 2) or (DataChega < Trunc(QrCiclosDataSaida.Value)) then
  begin
    Application.MessageBox(PChar('Data de retorno do grupo de professores n�o ' +
    'definida ou anterior a data de sa�da!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if DmCiclos.QrMovimT.RecordCount > 0 then
  begin
    Application.MessageBox('As mercadorias consignadas j� foram devolvidas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if Application.MessageBox('Confirma devolu��o das mercadorias consignadas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Motivo   := 12;
    SubCtrl  := 2;
    DataPedi := Geral.FDT(QrCiclosDataChega.Value, 1);
    DataReal := Geral.FDT(QrCiclosDataChega.Value, 1);
    DmCiclos.QrMovimL.Close;
    DmCiclos.QrMovimL.Open;
    DmCiclos.QrMovimL.First;
    while not DmCiclos.QrMovimL.Eof do
    begin
      Conta := UMyMod.BuscaEmLivreY_Def('movim', 'conta', stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'movim', False, [
        'Motivo', 'Qtd', 'Val', 'Ven', 'Grade', 'Cor', 'Tam', 'Controle',
        'SubCtrl', 'DataPedi', 'DataReal'
      ], ['Conta'], [
        Motivo, -DmCiclos.QrMovimLQtd.Value, -DmCiclos.QrMovimLVal.Value, DmCiclos.QrMovimLVen.Value,
        DmCiclos.QrMovimLGrade.Value, DmCiclos.QrMovimLCor.Value, DmCiclos.QrMovimLTam.Value,
        DmCiclos.QrMovimLControle.Value, SubCtrl, DataPedi, DataReal
      ], [Conta], True);
      DmProd.AtualizaEstoqueMercadoria(
        DmCiclos.QrMovimLGrade.Value, DmCiclos.QrMovimLCor.Value, DmCiclos.QrMovimLTam.Value, 
        QrCiclosDataChega.Value > 1, True, QtdX, ValX, CusX);
      DmCiclos.QrMovimL.Next;
    end;
  end;
  ReopenMovimT(Conta);
end;

procedure TFmCiclos.DesfazTODOprocessodeconsignao1Click(Sender: TObject);
begin
  //UMyMod.SQLDel2(Dmod.QrUpd, DmCiclos.QrMovimT, 'movim', '', ['Controle', 'SubCtrl'],
  //[DmCiclos.QrMovimTControle.Value, 2], False, '');
  // Excluir tudo
  UMyMod.SQLDel1(Dmod.QrUpd, DmCiclos.QrMovimT, 'movim', 'Controle',
  QrCiclosControle.Value, True,
  'Deseja realmente desfazer todo processo de consigna��o?', True);
  LocCod(QrCiclosCodigo.Value, QrCiclosCodigo.Value);
end;

procedure TFmCiclos.ExcluiitemdemercadoriaVenda1Click(Sender: TObject);
var
  Grade, Cor, Tam: Integer;
  QtdX, ValX, CusX: Double;
  Exclui: Boolean;
  Query: TmySQLQuery;
begin
  {
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, Query, DBGV,
    'movim', ['Conta'], ['Conta'], istPergunta);
  }
  if (PageControl1.ActivePageIndex = 0) and
  (PageControl2.ActivePageIndex = 2) then
    Query := DmCiclos.QrMovimV
  else
  if (PageControl1.ActivePageIndex = 1) then
    Query := DmCiclos.QrMovimD
  else begin
    Application.MessageBox('N�o h� query selecionada para exclus�o de mercadoria!',
    'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if (Query.State <> dsBrowse) or (Query.RecordCount = 0) then Exit;
  if Query.RecordCount = 0 then Exit;
  if Query.FieldByName('DataReal').AsDateTime > 1 then
    if DmProd.ImpedePeloBalanco(Query.FieldByName('DataReal').AsDateTime) then Exit;
  //
  Exclui := True;
  Grade := Query.FieldByName('Grade').AsInteger;
  Cor   := Query.FieldByName('Cor').AsInteger;
  Tam   := Query.FieldByName('Tam').AsInteger;
  DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, True, True, QtdX, ValX, CusX);
  if Exclui then
  begin
    if Application.MessageBox('Confirma a exclus�o do item selecionado?',
    'Confirma��o de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM movim ');
      Dmod.QrUpd.SQL.Add('WHERE Conta=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Query.FieldByName('Conta').Value;
      Dmod.QrUpd.ExecSQL;
      //
      DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, True, True, QtdX, ValX, CusX);
      Screen.Cursor := crDefault;
    end;
  end;
  QrCiclosROLCOMIS_PRF.Value;
  DmCiclos.CalculaVenComDia(
    QrCiclosControle.Value,
    QrCiclosAulaControle.Value,
    QrCiclosRolComis.Value);
  CalculaEReabre();
end;

procedure TFmCiclos.BtDespesasClick(Sender: TObject);
begin
  if QrCiclosStatus.Value > 2 then
  begin
    Application.MessageBox(PChar('Nesta etapa n�o � mais permitido incluir, ' +
    'editar ou excluir despesas!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else begin
    PageControl1.ActivePageIndex := 1;
    MyObjects.MostraPopUpDeBotao(PMDespesas, BtDespesas);
  end;
end;

procedure TFmCiclos.Incluidespesasprofessor1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  if QrCiclosAula.RecordCount = 0 then
    Application.MessageBox(
    'N�o h� turma cadastrada para lan�ar despesas para o promotor!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    CtrlAula := QrCiclosAulaControle.Value;
    if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
    afmoNegarComAviso, DmCiclos.QrDespProf, (*FmPrincipal.QrCarteiras*) nil,
    tgrInclui, DmCiclos.QrDespProfControle.Value, DmCiclos.QrDespProfSub.Value,
    0(*Genero*), 0(*Juros*), 0(*Multa*), nil, 701, CtrlAula(*FatID_Sub*),
    QrCiclosCodigo.Value, QrCiclosCART_PROF.Value, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
    0(*ForneceI*), QrCiclosProfessor.Value(*Account*), 0(*Vendedor*),
    True(*LockCliInt*), False(*LockForneceI*), True(*LockAccount*),
    False(*LockVendedor*), QrCiclosAulaData.Value, 0, 0, 0, 3, 0) > 0 then
    begin
      DmCiclos.CalculaDespProfTurma(CtrlAula);
      CalculaEReabre();
      QrCiclosAula.Locate('Controle', CtrlAula, []);
      ReopenDespProf(FLAN_CONTROLE);
    end;
  end;
end;

procedure TFmCiclos.Incluidespesaspromotor1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  if QrCiclosAula.RecordCount = 0 then
    Application.MessageBox(
    'N�o h� turma cadastrada para lan�ar despesas para o promotor!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    CtrlAula := QrCiclosAulaControle.Value;
    if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
    afmoNegarComAviso, DmCiclos.QrDespProm, (*FmPrincipal.QrCarteiras*) nil,
    tgrInclui, DmCiclos.QrDespPromControle.Value, DmCiclos.QrDespPromSub.Value,
    0(*Genero*), 0(*Juros*), 0(*Multa*), nil, 703, CtrlAula,
    QrCiclosCodigo.Value, QrCiclosCART_PROM.Value, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
    0(*ForneceI*), QrCiclosPromotor.Value(*Account*), 0(*Vendedor*),
    True(*LockCliInt*), False(*LockForneceI*), True(*LockAccount*),
    False(*LockVendedor*), QrCiclosAulaData.Value, 0, 0, 0, 3, 0) > 0 then
    begin
      DmCiclos.CalculaDespPromTurma(CtrlAula);
      CalculaEReabre();
      QrCiclosAula.Locate('Controle', CtrlAula, []);
      ReopenDespProm(FLAN_CONTROLE);
    end;
  end;
end;

procedure TFmCiclos.Excluidespesasprofessor1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  CtrlAula := QrCiclosAulaControle.Value;
  if CtrlAula = 0 then
  begin
    Application.MessageBox(
    'N�o h� lan�amento de despesa de promotor para a turma selecionada!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end else begin
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmCiclos.QrDespProf, DBGDespProf,
      'lanctos', ['controle', 'sub'], ['controle', 'sub'], istPergunta, '');
    DmCiclos.CalculaDespProfTurma(CtrlAula);
    CalculaEReabre();
    QrCiclosAula.Locate('Controle', CtrlAula, []);
    ReopenDespProf(FLAN_CONTROLE);
  end;
end;

procedure TFmCiclos.Excluidespesaspromotor1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  CtrlAula := QrCiclosAulaControle.Value;
  if CtrlAula = 0 then
  begin
    Application.MessageBox(
    'N�o h� lan�amento de despesa de promotor para a turma selecionada!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end else begin
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmCiclos.QrDespProm, DBGDespProm,
      'lanctos', ['controle', 'sub'], ['controle', 'sub'], istPergunta, '');
    DmCiclos.CalculaDespPromTurma(QrCiclosAulaControle.Value);
    CalculaEReabre();
    QrCiclosAula.Locate('Controle', CtrlAula, []);
  end;
end;

procedure TFmCiclos.Manual1Click(Sender: TObject);
//var
  //Terceiro, Cod, Genero, Carteira, Parcelas, Periodic, Dias: Integer;
  //Valor: Double;
begin
  {
  if (QrCiclos.State <> dsBrowse) or (QrCiclos.RecordCount = 0) then Exit;
  IC3_ED_FatNum := QrCiclosCodigo.Value;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  VAR_FATID     := 721;
  Cod           := QrCiclosControle.Value;
  Terceiro      := 0;// Cliente
  Valor         := QrCiclosProdVal.Value - QrCiclosProdPgt.Value;
  if Valor < 0 then Valor := 0;
  Genero        := Dmod.QrControleContVen.Value;
  Carteira      := QrCiclosCART_PROF.Value;
  Parcelas      := 0;
  Periodic      := 0;
  Dias          := 0;
  UCash.Pagto(DmCiclos.QrPagtosV, tpCred, Cod, Terceiro, VAR_FATID, Genero, CO_INCLUSAO,
    'Recebimento de Mercadorias', Valor, VAR_USUARIO, 0, Dmod.QrDonoCodigo.Value,
    mmNenhum, 0, 0, True, True, Carteira, Parcelas, Periodic, Dias);
  //
  CalculaEReabre();
  }
end;

procedure TFmCiclos.CalculaEReabre1Click(Sender: TObject);
begin
  CalculaEReabre();
end;

procedure TFmCiclos.Automtico1Click(Sender: TObject);
begin
  //
  CalculaEReabre();
end;

procedure TFmCiclos.CalculaEReabre();
begin
  DmCiclos.CalculaCiclo(QrCiclosCodigo.Value, QrCiclosControle.Value,
    QrCiclosProfessor.Value, QrCiclosRolComis.Value, 'FmCiclos');
end;

procedure TFmCiclos.Excluipagamentos1Click(Sender: TObject);
begin
  {
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmCiclos.QrPagtosV, DBGPagtosV,
    'lanctos', ['controle', 'sub'], ['controle', 'sub'], istPergunta);
  CalculaEReabre();
  }
end;

procedure TFmCiclos.EdAlunosAulaEnter(Sender: TObject);
begin
  //FValAluCalc := vacAlunos;
end;

procedure TFmCiclos.EdAlunosAulaExit(Sender: TObject);
begin
  //CalculaValorAlunos();
end;

procedure TFmCiclos.EdAlunosPrcEnter(Sender: TObject);
begin
  //FValAluCalc := vacPreco;
end;

procedure TFmCiclos.EdAlunosValEnter(Sender: TObject);
begin
  //FValAluCalc := vacTotal;
end;

procedure TFmCiclos.EdAlunosPrcExit(Sender: TObject);
begin
  //CalculaValorAlunos();
end;

procedure TFmCiclos.EdAlunosValExit(Sender: TObject);
begin
  //CalculaValorAlunos();
end;

{
procedure TFmCiclos.CalculaValorAlunos();
var
  Alunos: Integer;
  Preco, Total: Double;
begin
  Alunos := EdAlunosAula.ValueVariant;
  Preco  := EdAlunosPrc.ValueVariant;
  Total  := EdAlunosVal.ValueVariant;
  //
  case FValAluCalc of
    vacAlunos: Total := Alunos * Preco;
    vacPreco : Total := Alunos * Preco;
    vacTotal : if Total = 0 then Preco := 0 else Preco := Total / Alunos;
  end;
  EdAlunosAula.ValueVariant := Alunos;
  EdAlunosPrc.ValueVariant  := Preco;
  EdAlunosVal.ValueVariant  := Total;
end;
}

procedure TFmCiclos.DBEdProdifChange(Sender: TObject);
begin
  if QrCiclosPRODDIF.Value < 0 then DBEdProdif.Font.Color := clRed     else
  if QrCiclosPRODDIF.Value > 0 then DBEdProdif.Font.Color := clFuchsia else
                                    DBEdProdif.Font.Color := clSilver;
end;

procedure TFmCiclos.DBEdAlunosDifChange(Sender: TObject);
begin
  if QrCiclosALUNOSDIF.Value < 0 then DBEdAlunosDif.Font.Color := clRed     else
  if QrCiclosALUNOSDIF.Value > 0 then DBEdAlunosDif.Font.Color := clFuchsia else
                                      DBEdAlunosDif.Font.Color := clSilver;
end;

procedure TFmCiclos.BtTransfClick(Sender: TObject);
begin
  if QrCiclosStatus.Value > 3 then
    Application.MessageBox('A etapa atual n�o permite mais transfer�ncias!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    //PageControl2.ActivePageIndex := 4;
    if (PageControl1.ActivePageIndex = 0) and
       (PageControl2.ActivePageIndex = 4) then
    else
      PageControl1.ActivePageIndex := 1;
    MyObjects.MostraPopUpDeBotao(PMTransf, BtTransf);
  end;
end;

procedure TFmCiclos.Incluireceitadecurso1Click(Sender: TObject);
var
  Valor: Double;
begin
  Valor := -QrCiclosALUNOSDIF.Value;
  if Valor < 0 then Valor := 0;
  if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
  afmoNegarComAviso, DmCiclos.QrAuto, (*FmPrincipal.QrCarteiras*) nil,
  tgrInclui, DmCiclos.QrAutoControle.Value, DmCiclos.QrAutoSub.Value,
  0 (*conta (plano)*), 0(*Juros*), 0(*Multa*), nil, 711, 0(*FatID_Sub*),
  QrCiclosCodigo.Value, QrCiclosCART_PROF.Value, Valor, 0, True,
  0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), True(*LockAccount*), False(*LockVendedor*),
  0, 0, 0, 0, 3, 0) > 0 then
    ReopenAuto(FLAN_CONTROLE);
  CalculaEReabre();
end;

procedure TFmCiclos.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeCursos;
  if VAR_CADASTRO <> 0 then
  begin
    DmCiclos.QrCursos.Close;
    DmCiclos.QrCursos.Open;
    //
    EdCurso.ValueVariant := VAR_CADASTRO;
    CBCurso.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmCiclos.Incluitransferncia1Click(Sender: TObject);
begin
  UFinanceiro.CriarTransferCart(0, DmCiclos.QrTransfDia, nil, True,
    Dmod.QrControleDono.Value, 700, QrCiclosAulaControle.Value,
    QrCiclosCodigo.Value);
  CalculaEReabre();
  ReopenTransf(VAR_LANCTO2);
end;

procedure TFmCiclos.Alteratransferncia1Click(Sender: TObject);
begin
  UFinanceiro.CriarTransferCart(1, DmCiclos.QrTransfDia, nil, True,
    Dmod.QrControleDono.Value, 700, QrCiclosAulaControle.Value,
    QrCiclosCodigo.Value);
  CalculaEReabre();
  ReopenTransf(VAR_LANCTO2);
end;

procedure TFmCiclos.Alteraturma1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmCiclosAula, FmCiclosAula,
    afmoNegarComAviso, QrCiclosAula, stUpd);
end;

procedure TFmCiclos.Excluitransferncia1Click(Sender: TObject);
begin
  if (PageControl1.ActivePageIndex = 0) and
     (PageControl2.ActivePageIndex = 4) then
  UFinanceiro.CriarTransferCart(2, DmCiclos.QrTransfAll, nil, True,
    Dmod.QrControleDono.Value, 700, QrCiclosAulaControle.Value,
    QrCiclosCodigo.Value)
  else
  UFinanceiro.CriarTransferCart(2, DmCiclos.QrTransfDia, nil, True,
    Dmod.QrControleDono.Value, 700, QrCiclosAulaControle.Value,
    QrCiclosCodigo.Value);
  CalculaEReabre();
  ReopenTransf(VAR_LANCTO2);
end;

procedure TFmCiclos.Excluiturma1Click(Sender: TObject);
begin
  if DmCiclos.QrDespProm.RecordCount > 0 then
    Application.MessageBox(PChar('Exclus�o de turma cancelada! Existem ' +
    'despesas de promotor atreladas a ela!'), 'Aviso', MB_OK+MB_ICONWARNING)
  else
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCiclosAula, GradeTurmas,
      'ciclosaula', ['Controle'], ['Controle'], istAtual, '');
end;

procedure TFmCiclos.BtRateioClick(Sender: TObject);
begin
  if QrCiclosStatus.Value <> 3 then
  begin
    Application.MessageBox(PChar('A etapa atual n�o permite ratear as ' +
    'receitas com alunos entre as contas!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
    MyObjects.MostraPopUpDeBotao(PMRateio, BtRateio);
end;

procedure TFmCiclos.PMRateioPopup(Sender: TObject);
begin
  Rateiavalores1.Enabled        := True;
  //Incluireceitadecurso1.Enabled := False;
  Alterareceitadecurso1.Enabled := False;
  Excluireceitadecurso1.Enabled := False;
  if QrCiclosStatus.Value < 9 then
  begin
    //Incluireceitadecurso1.Enabled := True;
    Alterareceitadecurso1.Enabled := True;
    Excluireceitadecurso1.Enabled := True;
  end else Rateiavalores1.Enabled := True;
end;

procedure TFmCiclos.PMTransfPopup(Sender: TObject);
begin
  if (PageControl1.ActivePageIndex = 0) and
  (PageControl2.ActivePageIndex = 4) then
  begin
    Incluitransferncia1.Enabled := False;
    Alteratransferncia1.Enabled := False;
    Excluitransferncia1.Enabled := (DmCiclos.DsTransfAll.State <> dsInactive)
      and (DmCiclos.QrTransfAll.RecordCount > 0);
  end else begin
    Incluitransferncia1.Enabled := (QrCiclosAula.State <> dsInactive)
      and (QrCiclosAula.RecordCount > 0);
    Alteratransferncia1.Enabled := (DmCiclos.DsTransfDia.State <> dsInactive)
      and (DmCiclos.QrTransfDia.RecordCount > 0);
    Excluitransferncia1.Enabled := (DmCiclos.DsTransfDia.State <> dsInactive)
      and (DmCiclos.QrTransfDia.RecordCount > 0);
  end;
end;

procedure TFmCiclos.PMVendePopup(Sender: TObject);
begin
  if (PageControl1.ActivePageIndex = 0) and
  (PageControl2.ActivePageIndex = 2) then
  begin
    Incluialteravendademercadoria1.Enabled := False;
    ExcluiitemdemercadoriaVenda1.Enabled := (DmCiclos.QrMovimV.State <> dsInactive)
      and (DmCiclos.QrMovimV.RecordCount > 0);
  end else begin
    Incluialteravendademercadoria1.Enabled := QrCiclosAula.RecordCount > 0;
    ExcluiitemdemercadoriaVenda1.Enabled := (DmCiclos.QrMovimD.State <> dsInactive)
      and (DmCiclos.QrMovimD.RecordCount > 0);
  end;
end;

procedure TFmCiclos.Rateiavalores1Click(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMCurso, BtCurso);
  if QrCiclosStatus.Value <> 3 then
  begin
    Application.MessageBox(PChar('A etapa atual n�o permite ratear as ' +
    'receitas com alunos entre as contas!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
  if QrCiclosProfessor.Value = 0 then
  begin
    Application.MessageBox(PChar('Rateio cancelado! N�o h� professor definido '+
    'para o ciclo!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
  if QrCiclospromotor.Value = 0 then
  begin
    Application.MessageBox(PChar('Rateio cancelado! N�o h� promotor definido '+
    'para o ciclo!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
  if Dmod.QrControleCxaPgtProf.Value = 0 then
  begin
    Application.MessageBox(PChar('Rateio cancelado! N�o h� caixa definido '+
    'para transferir automaticamente o dinheiro sacado pelo professor!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
  if Dmod.QrControleCtaPagProf.Value = 0 then
  begin
    Application.MessageBox(PChar('Rateio cancelado! N�o h� conta definida '+
    'para pagamento da comiss�o do professor!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end else
  begin
    PageControl2.ActivePageIndex := 6;
    {//DmCiclos.RateiaCreditosEDebitosDeMercadorias(
    DmCiclos.RateiaCreditosParaContas(
      QrCiclosCodigo.Value, QrCiclosCurso.Value,
      QrCiclosProfessor.Value, QrCiclosAlunosAula.Value,
      QrCiclosVALORRATEIO.Value, QrCiclosDespesas.Value,
      QrCiclosAlunosVal.Value, QrCiclosDataChega.Value);
    }
    if DBCheck.CriaFm(TFmCiclosRat, FmCiclosRat, afmoNegarComAviso) then
    begin
      FmCiclosRat.FRolComis    := QrCiclosRolComis.Value;
      FmCiclosRat.FDataEmis    := QrCiclosDataChega.Value;
      FmCiclosRat.FProfessor   := QrCiclosProfessor.Value;
      FmCiclosRat.FPromotor    := QrCiclosPromotor.Value;
      FmCiclosRat.FCiclo       := QrCiclosCodigo.Value;
      //FmCiclosRat.FCurso       := QrCiclosCurso.Value;
      FmCiclosRat.FQtdAlunos   := QrCiclosAlunosAula.Value;
      FmCiclosRat.FValorRateio := QrCiclosVALORRATEIO.Value;
      FmCiclosRat.FDespesas    := QrCiclosDespProf.Value;
      FmCiclosRat.FValorAlunos := QrCiclosAlunosVal.Value;
      FmCiclosRat.FMovimCtrl   := QrCiclosControle.Value;
      FmCiclosRat.FPagoAProf   := QrCiclosPAGOAPROF.Value;
      FmCiclosRat.FCxaPgtProf  := Dmod.QrControleCxaPgtProf.Value;
      FmCiclosRat.FCtaPagProf  := Dmod.QrControleCtaPagProf.Value;
      FmCiclosRat.FTabLctA     := Fazer;
      //
      FmCiclosRat.ShowModal;
      FmCiclosRat.Destroy;
    end;
    CalculaEReabre;
  end;
end;

procedure TFmCiclos.Rateio1Click(Sender: TObject);
begin
  FmMeuDBUses.ImprimeDBGrid(DBGRateio, 'Itens Rateados no ciclo ' +
    IntToStr(QrCiclosCodigo.Value));
end;

procedure TFmCiclos.Excluireceitadecurso1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmCiclos.QrAuto, DBGRateio,
    'lanctos', ['controle', 'sub'], ['controle', 'sub'], istPergunta, '');
  CalculaEReabre();
end;

procedure TFmCiclos.DBEdComisDifChange(Sender: TObject);
begin
  if QrCiclosCOMISDIF.Value < 0 then DBEdComisDif.Font.Color := clRed     else
  if QrCiclosCOMISDIF.Value > 0 then DBEdComisDif.Font.Color := clFuchsia else
                                    DBEdComisDif.Font.Color := clSilver;
end;

procedure TFmCiclos.DBEdVALALUDIFChange(Sender: TObject);
begin
  if QrCiclosVALALUDIF.Value < 0 then DBEdVALALUDIF.Font.Color := clRed     else
  if QrCiclosVALALUDIF.Value > 0 then DBEdVALALUDIF.Font.Color := clFuchsia else
                                      DBEdVALALUDIF.Font.Color := clSilver;
end;

procedure TFmCiclos.Alterareceitadecurso1Click(Sender: TObject);
begin
{
//N�o existe mais
  if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
  afmoNegarComAviso, DmCiclos.QrAuto, (*FmPrincipal.QrCarteiras*) nil,
  tgrAltera, DmCiclos.QrAutoControle.Value, DmCiclos.QrAutoSub.Value,
  DmCiclos.QrAutoGenero.Value, 0(*Juros*), 0(*Multa*), nil,
  DmCiclos.QrAutoFatID.Value, DmCiclos.QrAutoFatID_Sub.Value,
  QrCiclosCodigo.Value, QrCiclosCART_PROF.Value, 0, 0, True,
  0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), True(*LockAccount*), False(*LockVendedor*), 0, 0, 0, 3, 0) > 0 then
}
  UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfCicloCurso,
    0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True);
  //
  ReopenAuto(FLAN_CONTROLE);
  CalculaEReabre();
end;

procedure TFmCiclos.BtEmprestaClick(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 5;
  if QrCiclosStatus.Value < 9 then
    MyObjects.MostraPopUpDeBotao(PMEmpresta, BtEmpresta)
  else
    Application.MessageBox(PChar('A etapa atual n�o permite gerenciar ' +
    'empr�stimos!'), 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmCiclos.Emprestaafuncionrio1Click(Sender: TObject);
begin
  Emprestimo(empVai, 0);
end;

procedure TFmCiclos.Devoluodefuncionrio1Click(Sender: TObject);
begin
  Emprestimo(empVolta, 0);
end;

procedure TFmCiclos.Alteradespesaprofessor1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  CtrlAula := QrCiclosAulaControle.Value;
  if CtrlAula = 0 then
  begin
    Application.MessageBox(
    'N�o h� lan�amento de despesa de promotor para a turma selecionada!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end else begin
{
//N�o existe mais
    if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
    afmoNegarComAviso, DmCiclos.QrDespProf, (*FmPrincipal.QrCarteiras*) nil,
    tgrAltera, DmCiclos.QrDespProfControle.Value, DmCiclos.QrDespProfSub.Value,
    DmCiclos.QrDespProfGenero.Value, 0(*Juros*), 0(*Multa*), nil, 701, 0(*FatID_Sub*),
    QrCiclosCodigo.Value, QrCiclosCART_PROF.Value, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*),
    True(*LockCliInt*), False(*LockForneceI*), True(*LockAccount*),
    False(*LockVendedor*), 0, 0, 0, 3, 0) > 0 then
}
    UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfCicloCurso,
      0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True);
    //
    DmCiclos.CalculaDespProfTurma(CtrlAula);
    CalculaEReabre();
    QrCiclosAula.Locate('Controle', CtrlAula, []);
    ReopenDespProf(FLAN_CONTROLE);
  end;
end;

procedure TFmCiclos.Alteradespesapromotor1Click(Sender: TObject);
var
  CtrlAula: Integer;
begin
  CtrlAula := QrCiclosAulaControle.Value;
  if CtrlAula = 0 then
  begin
    Application.MessageBox(
    'N�o h� lan�amento de despesa de promotor para a turma selecionada!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end else begin
{
//N�o existe mais
    if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
    afmoNegarComAviso, DmCiclos.QrDespProm, (*FmPrincipal.QrCarteiras*) nil,
    tgrAltera, DmCiclos.QrDespPromControle.Value, DmCiclos.QrDespPromSub.Value,
    DmCiclos.QrDespPromGenero.Value, 0(*Juros*), 0(*Multa*), nil, 703, 0(*FatID_Sub*),
    QrCiclosCodigo.Value, QrCiclosCART_PROM.Value, 0, 0, True,
    0(*Cliente*), 0(*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), True(*LockAccount*), False(*LockVendedor*), 0, 0, 0, 3, 0) > 0 then
}
    UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfCicloCurso,
      0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True);
    //
    DmCiclos.CalculaDespPromTurma(QrCiclosAulaControle.Value);
    CalculaEReabre();
    QrCiclosAula.Locate('Controle', CtrlAula, []);
    ReopenDespProm(FLAN_CONTROLE);
  end;
end;

procedure TFmCiclos.Alteraemprstimoatual1Click(Sender: TObject);
var
  Valor: Double;
  Campo: String;
  Controle, Sub: Integer;
begin
  if DmCiclos.QrEmprestimosCredito.Value > 0 then
  begin
    Valor := DmCiclos.QrEmprestimosCredito.Value;
    Campo := 'Credito';
  end else begin
    Valor := DmCiclos.QrEmprestimosDebito.Value;
    Campo := 'Debito';
  end;
  if dmkPF.ObtemValorDouble(Valor, 2) then
  begin
    Controle := DmCiclos.QrEmprestimosControle.Value;
    Sub      := DmCiclos.QrEmprestimosSub.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'lanctos', False, [Campo],
    ['Controle', 'Sub'], [Valor], [Controle, Sub], True);
    CalculaEReabre;
    DmCiclos.QrEmprestimos.Locate('Controle', FLAN_Controle, []);
  end;
end;

procedure TFmCiclos.Emprestimo(Tipo: TEmpresta; Valor: Double);
var
  Data, Vencto, Compen: String;
  Conta: Integer;
  Credito, Debito: Double;
begin
  //if DmCiclos.ProfessorDiferenteDe1(QrCiclosProfessor.Value) then Exit;
  //
  if Dmod.QrControleCtaEmpPrfC.Value = 0 then
  begin
    if Application.MessageBox(PChar('N�o h� cadastro de conta padr�o para ' +
    'empr�stimo a professor! Deseja cadastr�-lo agora?'), 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      FmPrincipal.Especficos1Click(Self);
    if Dmod.QrControleCtaEmpPrfC.Value = 0 then Exit;
  end;
  Conta   := 0;
  Credito := 0;
  Debito  := 0;
  case Tipo of
    empVai:   Conta := Dmod.QrControleCtaEmpPrfD.Value;
    empVolta: Conta := Dmod.QrControleCtaEmpPrfC.Value;
  end;
  if dmkPF.ObtemValorDouble(Valor, 2) then
  begin
    case Tipo of
      empVai:   Debito  := Valor;
      empVolta: Credito := Valor;
    end;
    DmCiclos.QrConta.Close;
    DmCiclos.QrConta.Params[0].AsInteger := Conta;
    DmCiclos.QrConta.Open;
    //
    Data := Geral.FDT(QrCiclosDataAcert.Value, 1);
    Vencto := Data;
    Compen := Data;
    //
    DmCiclos.QrEnt.Close;
    DmCiclos.QrEnt.Params[0].AsInteger := QrCiclosProfessor.Value;
    DmCiclos.QrEnt.Open;
    DmCiclos.InsereLanctoEmprestimo(Data, DmCiclos.QrContaNome.Value, Vencto,
    Compen, DmCiclos.QrEntTIPOCART.Value, DmCiclos.QrEntCartPref.Value,
    Conta, 731, 1, QrCiclosCodigo.Value,
    QrCiclosProfessor.Value, 0, Credito, Debito);
    //
    CalculaEReabre();
    DmCiclos.QrEmprestimos.Locate('Controle', FLAN_Controle, []);
  end;
end;

procedure TFmCiclos.Excluiemprstimos1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmCiclos.QrEmprestimos, DBGEmprestimos,
    'lanctos', ['controle', 'sub'], ['controle', 'sub'], istPergunta, '');
  //
  CalculaEReabre();
end;

procedure TFmCiclos.Incluiturma1Click(Sender: TObject);
begin
  if QrCiclosRolComis.Value = 0 then
    Application.MessageBox('N�o h� rol de comiss�es definido neste ciclo!',
    'Aviso', MB_OK+MB_ICONWARNING);
  //
  UMyMod.FormInsUpd_Show(TFmCiclosAula, FmCiclosAula, afmoNegarComAviso,
  QrCiclosAula, stIns);
end;

end.



