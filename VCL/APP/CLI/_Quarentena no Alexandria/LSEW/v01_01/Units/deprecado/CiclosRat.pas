unit CiclosRat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, ComCtrls, Db,
  mySQLDbTables, dmkPermissoes, dmkGeral, UnDmkEnums;

type
  TFmCiclosRat = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBGrid2: TDBGrid;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    STAvisos: TStaticText;
    PB1: TProgressBar;
    QrProds: TmySQLQuery;
    QrComProd: TmySQLQuery;
    QrComProdGenero: TIntegerField;
    QrComProdPerceGene: TFloatField;
    QrComProdCredDeb: TSmallintField;
    QrComProdEhComProf: TSmallintField;
    QrComProdRateio: TSmallintField;
    QrProdsGrade: TIntegerField;
    QrProdsValor: TFloatField;
    QrImpede3: TmySQLQuery;
    QrRat: TmySQLQuery;
    QrRatGenero: TIntegerField;
    QrRatPerceGene: TFloatField;
    QrRatCredDeb: TSmallintField;
    QrRatEhComProf: TSmallintField;
    QrRatRateio: TSmallintField;
    QrRatNOMECONTA: TWideStringField;
    QrSum: TmySQLQuery;
    QrSumTotal: TFloatField;
    QrComProdCompoe: TSmallintField;
    QrRatCompoe: TSmallintField;
    QrRatFATID: TIntegerField;
    QrRatFATID_SUB: TIntegerField;
    QrRatCartIdx: TSmallintField;
    QrTrf: TmySQLQuery;
    QrTrfCarteira: TIntegerField;
    QrTrfValor: TFloatField;
    QrClc: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrRatCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FProfC, FProfT, FPromC, FPromT, FCarteira, FTipoCart: Integer;
    procedure ObtemCarteiraETipoCart(CartIdx: Integer);
    procedure TransferirDinheiroEntreCarteiras(Valor: Double;
              CartOrig, CartDest, FatID: Integer);
  public
    { Public declarations }
    FDataEmis: TDateTime;
    FTabLctA: String;
    FCtaPagProf, FCxaPgtProf, FProfessor, FPromotor, FCiclo, FMovimCtrl, FRolComis: Integer;
    FPagoAProf, FQtdAlunos, FValorRateio, FDespesas, FValorAlunos: Double;
    procedure ExecutaRateio(RolComis, Professor, Promotor, Ciclo, MovimCtrl,
      CxaPgtProf, CtaPagProf: Integer; QtdAlunos, ValorRateio, Despesas,
      ValorAlunos, PagoAProf: Double; DataEmis: TDateTime);
  end;

  var
  FmCiclosRat: TFmCiclosRat;

implementation

uses ModuleCiclos, Module, UMySQLModule, Transfer2, UnFinanceiro, ModuleFin,
  UnMyObjects, UnInternalConsts;

{$R *.DFM}

procedure TFmCiclosRat.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosRat.ExecutaRateio(RolComis, Professor, Promotor, Ciclo, MovimCtrl,
  CxaPgtProf, CtaPagProf: Integer; QtdAlunos, ValorRateio, Despesas,
  ValorAlunos, PagoAProf: Double; DataEmis: TDateTime);
var
  Fator, Total, Saldo, Valor: Double;
  Data, Vencto, Compen: String;
  //PromT, PromC, ProfT, ProfC, TipoCart, Carteira: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Data := Geral.FDT(FDataEmis, 1);
    Vencto := Data;
    Compen := Data;
    //
    DmCiclos.QrEnt.Close;
    DmCiclos.QrEnt.Params[0].AsInteger := FPromotor;
    DmCiclos.QrEnt.Open;
    //
    FPromT := DmCiclos.QrEntTIPOCART.Value;
    FPromC := DmCiclos.QrEntCartPref.Value;
    //
    DmCiclos.QrEnt.Close;
    DmCiclos.QrEnt.Params[0].AsInteger := FProfessor;
    DmCiclos.QrEnt.Open;
    FProfT := DmCiclos.QrEntTIPOCART.Value;
    FProfC := DmCiclos.QrEntCartPref.Value;
    //
    //if DmCiclos.ProfessorDiferenteDe1(FGruProf) then Exit;
    //
    // Exclui o que h� para refazer caso necess�rio...
    if DmCiclos.QrAuto.RecordCount > 0 then
    begin
      STAvisos.Caption := 'Excluindo lan�amentos existentes...';
      Application.ProcessMessages;
      //
      // ... exclui receitas ...
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0711, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... exclui vendas ...
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0721, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... e despesas...
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0702, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... e transferencias entre carteiras de acerto entre professor e promotor...
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0750, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... e transferencias entre carteiras para pagamento da comiss�o de
      //professores sobre alunos (devolu��o do dinheiro o qual o professor reteu em viagem)
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0751, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... e transferencias entre carteiras para devolu��o da comiss�o de
      //professores sobre alunos (devolu��o do dinheiro o qual o professor reteu em viagem)
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0752, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... e o pagamento da comiss�o de professores sobre alunos (valor retido em viagem)
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0706, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // ... e a reten��o da comiss�o de professores sobre alunos e produtos (diferen�a do que ele n�o reteu)
      UFinanceiro.ExcluiLct_FatNum(Dmod.QrAux, VAR_FATID_0712, FCiclo,
        Dmod.QrControleCiclCliInt.Value, FCarteira,
        CO_MOTVDEL_311_EXCLUILCTFATURAMENTO, False, FTabLctA);
      // Agora, coloca os c�digos de controle na tabele livres para reutiliz�-los
      DmCiclos.QrAuto.First;
      while not DmCiclos.QrAuto.Eof do
      begin
        UMyMod.PoeEmLivreY_Double(Dmod.MyDB, 'livres', 'lanctos',
          DmCiclos.QrAutoControle.Value);
        DmCiclos.QrAuto.Next;
      end;
    end;
    {
    if FCurso = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox('Rateio cancelado. N�o foi definido o curso!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    }
    QrImpede3.Close;
    QrImpede3.Params[00].AsInteger := FRolComis;
    QrImpede3.Open;
    if QrImpede3.RecordCount > 1 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(PChar('Rateio cancelado. H� ' + IntToStr(
      QrImpede3.RecordCount) +' formas de rateio p�s-fixado, quando o m�ximo ' +
      'permitido � 1 (um) !'), 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    // Mercadorias vendidas
    STAvisos.Caption := 'Incluindo novos lan�amentos ...';
    Update;
    Application.ProcessMessages;
    //
    QrProds.Close;
    QrProds.Params[0].AsInteger := FMovimCtrl;
    QrProds.Open;
    PB1.Position := 0;
    PB1.Max := QrProds.RecordCount;
    Update;
    Application.ProcessMessages;
    //
    QrProds.First;
    // Inclui lanctos mercadoria a mercadoria
    while not QrProds.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Max := QrProds.RecordCount;
      Update;
      Application.ProcessMessages;
      //
      //////////////////////////////////////////////////////////////////////////
      ////////////////      C R � D I T O S      /////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      //
      Saldo := QrProdsValor.Value;
      //
      ////////////  CR�DITO P�S-FIXADO
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de cr�dito p�s-fixado...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 3; // P�s-fixado
      QrRat.Params[01].AsInteger := 1; // Cr�dito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        Valor := FDespesas;
        // impedir lan�ar de novo.
        FDespesas := 0;
        //
        ObtemCarteiraETipoCart(QrRatCartIdx.Value);
        DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
        FTipoCart, FCarteira, QrRatGenero.Value, QrRatFATID.Value,
        QrRatFATID_SUB.Value, FCiclo, FProfessor, 0, Valor, 0);
        //
        Saldo := Saldo - Valor;
      end;
      //
      ////////////  CR�DITO PR�-FIXADO
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de cr�dito pr�-fixado ...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 1; // Pr�-fixado
      QrRat.Params[01].AsInteger := 1; // Cr�dito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        QrRat.First;
        while not QrRat.Eof do
        begin
          Valor := Round(QrRatPerceGene.Value * QrProdsValor.Value) / 100;
          //
          ObtemCarteiraETipoCart(QrRatCartIdx.Value);
          DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
          FTipoCart, FCarteira,
          QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo,
          FProfessor, 0, Valor, 0);
          //
          Saldo := Saldo - Valor;
          QrRat.Next;
        end;
      end;
      //
      ////////////  CR�DITO POR C�LCULO NAS TURMAS
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de cr�dito calculado nas turmas ...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 4; // Pr�-fixado
      QrRat.Params[01].AsInteger := 1; // Cr�dito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        QrRat.First;
        while not QrRat.Eof do
        begin
          {QrClc.Close;
          QrClc.Params[0].AsInteger := QrRatGenero.Value;
          QrClc.Open;}
          //
          Valor := Round(QrRatPerceGene.Value * QrProdsValor.Value) / 100;
          //
          ObtemCarteiraETipoCart(QrRatCartIdx.Value);
          DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
          FTipoCart, FCarteira,
          QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo,
          FProfessor, 0, Valor, 0);
          //
          Saldo := Saldo - Valor;
          QrRat.Next;
        end;
      end;
      //
      ////////////  CR�DITO VARI�VEL
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de cr�dito vari�vel...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 2; // Vari�vel
      QrRat.Params[01].AsInteger := 1; // Cr�dito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        QrSum.Close;
        QrSum.Params[00].AsInteger := 2; // Vari�vel
        QrSum.Params[01].AsInteger := 1; // Cr�dito
        QrSum.Params[02].AsInteger := FRolComis;
        QrSum.Params[03].AsInteger := QrProdsGrade.Value;
        QrSum.Open;
        if QrSumTotal.Value > 0 then
          Fator := 100 / QrSumTotal.Value
        else Fator := 0;
        Total := Saldo;
        QrRat.First;
        while not QrRat.Eof do
        begin
          if QrRat.RecNo = QrRat.RecordCount then
            Valor := Saldo
          else
            Valor := Round(QrRatPerceGene.Value * Fator * Total) / 100;
          //
          ObtemCarteiraETipoCart(QrRatCartIdx.Value);
          DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
          FTipoCart, FCarteira,
          QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo,
          FProfessor, 0, Valor, 0);
          //
          Saldo := Saldo - Valor;
          QrRat.Next;
        end;
      end;
      //
      //////////////////////////////////////////////////////////////////////////
      ////////////////      D � B I T O S      /////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      //
      Saldo := QrProdsValor.Value;
      //
      ////////////  D�BITO P�S-FIXADO
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de d�bito p�s-fixado...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 3; // P�s-fixado
      QrRat.Params[01].AsInteger := 2; // D�dito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        // N � O   T E M
        Application.MessageBox(PChar('Verifique o rol de comiss�es selecionado'+
        '! Ele possui ' + IntToStr(QrRat.RecordCount) + ' itens de d�bito ' +
        'p�s-fixado, o que n�o � permitido!'), 'Aviso', MB_OK+MB_ICONWARNING);
        {
        Valor := FDespesas;
        // impedir lan�ar de novo.
        FDespesas := 0;
        //
        DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
        DmCiclos.QrLocGProfTIPOCART.Value, DmCiclos.QrLocGProfCartPadr.Value,
        QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo, 0, Valor);
        //
        Saldo := Saldo - Valor;
        }
      end;
      //
      ////////////  D�BITO PR�-FIXADO
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de d�bito pr�-fixado ...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 1; // Pr�-fixado
      QrRat.Params[01].AsInteger := 2; // D�bito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        QrRat.First;
        while not QrRat.Eof do
        begin
          Valor := Round(QrRatPerceGene.Value * QrProdsValor.Value) / 100;
          //
          ObtemCarteiraETipoCart(QrRatCartIdx.Value);
          DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
          FTipoCart, FCarteira,
          QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo,
          0, FProfessor, 0, Valor);
          //
          Saldo := Saldo - Valor;
          QrRat.Next;
        end;
      end;
      //
      ////////////  CR�DITO POR C�LCULO NAS TURMAS
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de d�bito calculado nas turmas ...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 4; // Pr�-fixado
      QrRat.Params[01].AsInteger := 2; // Cr�dito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        QrRat.First;
        while not QrRat.Eof do
        begin
          Valor := Round(QrRatPerceGene.Value * QrProdsValor.Value) / 100;
          //
          ObtemCarteiraETipoCart(QrRatCartIdx.Value);
          DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
          FTipoCart, FCarteira,
          QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo,
          0, FProfessor, 0, Valor);
          //
          Saldo := Saldo - Valor;
          QrRat.Next;
        end;
      end;
      //
      ////////////  D�BITO VARI�VEL
      //
      STAvisos.Caption := 'Incluindo novos lan�amentos de d�bito vari�vel...';
      Update;
      Application.ProcessMessages;
      //
      QrRat.Close;
      QrRat.Params[00].AsInteger := 2; // Vari�vel
      QrRat.Params[01].AsInteger := 2; // D�bito
      QrRat.Params[02].AsInteger := FRolComis;
      QrRat.Params[03].AsInteger := QrProdsGrade.Value;
      QrRat.Open;
      if QrRat.RecordCount > 0 then
      begin
        QrSum.Close;
        QrSum.Params[00].AsInteger := 2; // Vari�vel
        QrSum.Params[01].AsInteger := 2; // D�bito
        QrSum.Params[02].AsInteger := FRolComis;
        QrSum.Params[03].AsInteger := QrProdsGrade.Value;
        QrSum.Open;
        if QrSumTotal.Value > 0 then
          Fator := 100 / QrSumTotal.Value
        else Fator := 0;
        Total := Saldo;
        QrRat.First;
        while not QrRat.Eof do
        begin
          if QrRat.RecNo = QrRat.RecordCount then
            Valor := Saldo
          else
            Valor := Round(QrRatPerceGene.Value * Fator * Total) / 100;
          //
          ObtemCarteiraETipoCart(QrRatCartIdx.Value);
          DmCiclos.InsereLanctoRateio(Data, QrRatNOMECONTA.Value, Vencto, Compen,
          FTipoCart, FCarteira,
          QrRatGenero.Value, QrRatFATID.Value, QrRatFATID_SUB.Value, FCiclo,
          FProfessor, 0, 0, Valor);
          //
          Saldo := Saldo - Valor;
          QrRat.Next;
        end;
      end;
      //
      QrProds.Next;
    end;
    //
    QrTrf.Close;
    QrTrf.Params[00].AsInteger := FCiclo;
    QrTrf.Params[01].AsInteger := FProfC;
    QrTrf.Open;
    QrTrf.First;
    while not QrTrf.Eof do
    begin
      TransferirDinheiroEntreCarteiras(QrTrfValor.Value, QrTrfCarteira.Value,
        FProfC, 750);
      //
      QrTrf.Next;
    end;
    //
    // Acertar devolu��o da do dinheiro que ficou com o professor transferindo
    // entra contas, e pagando a ele a comiss�o referente ao mesmo valor
    {
    TransferirDinheiroEntreCarteiras(FPagoAProf, FCxaPgtProf, FProfC, 751);
    TransferirDinheiroEntreCarteiras(FPagoAProf, FProfC, FCxaPgtProf, 752);
    }
    // Pagamento do professor
    {
    DmCiclos.InsereLanctoRateio(Data, Dmod.QrControleTxtPgtProf.Value,
      Vencto, Compen, FTipoCart, FCarteira, Dmod.QrControleCtaPagProf.Value,
      706, 0, FCiclo, FProfessor, 0, 0, FPagoAProf);
    }
    if FPagoAProf < 0 then
    begin
      ObtemCarteiraETipoCart(0);
      DmCiclos.InsereLanctoRateio(Data, Dmod.QrControleTxtPgtProf.Value,
        Vencto, Compen, FTipoCart, FCarteira, Dmod.QrControleCtaPagProf.Value,
        712, 0, FCiclo, FProfessor, 0, 0, -FPagoAProf)
    end else
    begin
      ObtemCarteiraETipoCart(1);
      DmCiclos.InsereLanctoRateio(Data, Dmod.QrControleTxtPgtProf.Value,
        Vencto, Compen, FTipoCart, FCarteira, Dmod.QrControleCtaPagProf.Value,
        712, 0, FCiclo, FProfessor, 0, FPagoAProf, 0);
    end;
    //
  finally
    Screen.Cursor := crDefault;
    STAvisos.Caption := '...';
    PB1.Position := 0;
    Application.MessageBox('Rateio finalizado!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
    Close;
  end;
end;

procedure TFmCiclosRat.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclosRat.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosRat.BtOKClick(Sender: TObject);
begin
  ExecutaRateio(FRolComis, FProfessor, FPromotor, FCiclo, FMovimCtrl,
    FCxaPgtProf, FCtaPagProf, FQtdAlunos, FValorRateio, FDespesas, FValorAlunos,
    FPagoAProf, FDataEmis);
end;


procedure TFmCiclosRat.TransferirDinheiroEntreCarteiras(Valor: Double;
CartOrig, CartDest, FatID: Integer);
begin
  UFinanceiro.CriarTransferCart(0, nil(*QrLanctos*), nil(*QrCarteiras*),
    False, Dmod.QrControleDono.Value, 0, 0, 0);
  FmTransfer2.FFatID  := FatID;
  FmTransfer2.FFatNum := FCiclo;
  if Valor > 0 then
  begin
    FmTransfer2.EdOrigem.ValueVariant  := CartOrig;
    FmTransfer2.CBOrigem.KeyValue      := CartOrig;
    FmTransfer2.EdDestino.ValueVariant := CartDest;
    FmTransfer2.CBDestino.KeyValue     := CartDest;
    FmTransfer2.EdValor.Text           := Geral.FFT(-Valor, 2, siPositivo);
  end else begin
    FmTransfer2.EdDestino.ValueVariant := CartOrig;
    FmTransfer2.CBDestino.KeyValue     := CartOrig;
    FmTransfer2.EdOrigem.ValueVariant  := CartDest;
    FmTransfer2.CBOrigem.KeyValue      := CartDest;
    FmTransfer2.EdValor.Text           := Geral.FFT(Valor, 2, siPositivo);
  end;
  FmTransfer2.TPData.Date := FDataEmis;
  //
  FmTransfer2.BtConfirmaClick(Self);

  FmTransfer2.Close;
end;

procedure TFmCiclosRat.QrRatCalcFields(DataSet: TDataSet);
begin
  case QrRatCredDeb.Value of
    1:
    begin
      if QrRatCompoe.Value = 1 then
        QrRatFATID.Value   := 711
      else
        QrRatFATID.Value   := 721;
      QrRatFATID_SUB.Value := QrRatCompoe.Value;
    end;
    2:
    begin
      QrRatFATID.Value     := 702;
      QrRatFATID_SUB.Value := QrRatEhComProf.Value;
    end;
    else begin
      // Erro! o que fazer?
      Application.MessageBox(PChar('ERRO. Situa��o de "CredDeb" n�o ' +
      'prevista no rateio de valores!'), 'ERRO', MB_OK+MB_ICONERROR);
      QrRatFATID.Value     := 0;
      QrRatFATID_SUB.Value := 0;
    end;
  end;
end;

procedure TFmCiclosRat.ObtemCarteiraETipoCart(CartIdx: Integer);
begin
  //case QrRatCartIdx.Value of
  case CartIdx of
    0: // Professor
    begin
      FCarteira := FProfC;
      FTipoCart := FProfT;
    end;
    1: // Promotor
    begin
      FCarteira := FPromC;
      FTipoCart := FPromT;
    end;
    else // Erro
    begin
      FCarteira := 0;
      FTipoCart := 0;
    end;
  end;
end;

end.

