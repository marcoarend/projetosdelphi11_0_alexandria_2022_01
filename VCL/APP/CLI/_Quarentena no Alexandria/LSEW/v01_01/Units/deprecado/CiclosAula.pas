unit CiclosAula;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Mask, DBCtrls, dmkEdit, dmkRadioGroup,
  dmkCheckBox, dmkDBEdit, dmkLabel, dmkMemo, ExtDlgs, ComCtrls, dmkGeral,
  dmkEditDateTimePicker, DB, mySQLDbTables, dmkPermissoes, dmkDBLookupComboBox,
  dmkEditCB, UnDmkEnums;

type
  TValAluCalc = (vacAlunos, vacPreco, vacTotal,
    vacComPromPer, vacComPromVal, vacComProfPer, vacComProfVal,
    vacDTSPromPer, vacDTSPromVal, vacDTSProfPer, vacDTSProfVal);
  TFmCiclosAula = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Panel4: TPanel;
    dmkDBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    dmkEdit1: TdmkEdit;
    EdInscritos: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdAlunosAula: TdmkEdit;
    EdCidade: TdmkEdit;
    Label5: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label6: TLabel;
    EdUF: TdmkEdit;
    Label7: TLabel;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    dmkEdit8: TdmkEdit;
    dmkEdit6: TdmkEdit;
    Label8: TLabel;
    dmkEdit7: TdmkEdit;
    Label9: TLabel;
    dmkEdit9: TdmkEdit;
    Label11: TLabel;
    dmkEdit10: TdmkEdit;
    Label12: TLabel;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    dmkEdit11: TdmkEdit;
    dmkEdit12: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    dmkEdit13: TdmkEdit;
    dmkEdit14: TdmkEdit;
    Label16: TLabel;
    GroupBox3: TGroupBox;
    Label17: TLabel;
    dmkEdit15: TdmkEdit;
    Label18: TLabel;
    dmkEdit16: TdmkEdit;
    Memo1: TdmkMemo;
    Label19: TLabel;
    Label20: TLabel;
    EdValorUni: TdmkEdit;
    Label21: TLabel;
    EdValorTot: TdmkEdit;
    GroupBox4: TGroupBox;
    Label22: TLabel;
    EdComPromPer: TdmkEdit;
    EdComPromVal: TdmkEdit;
    Label23: TLabel;
    Label24: TLabel;
    EdDTSPromPer: TdmkEdit;
    EdDTSPromVal: TdmkEdit;
    Label25: TLabel;
    GroupBox5: TGroupBox;
    EdComProfPer: TdmkEdit;
    EdComProfVal: TdmkEdit;
    EdDTSProfPer: TdmkEdit;
    EdDTSProfVal: TdmkEdit;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    EdComPromMin: TdmkEdit;
    Label30: TLabel;
    EdDTSPromMin: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    EdComProfMin: TdmkEdit;
    EdDTSProfMin: TdmkEdit;
    Label33: TLabel;
    CkContinuar: TCheckBox;
    EdPromotor: TdmkEdit;
    Label34: TLabel;
    EdProfessor: TdmkEdit;
    Label35: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    EdCodiCidade: TdmkEditCB;
    CBCodiCidade: TdmkDBLookupComboBox;
    Label36: TLabel;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure dmkEdit7Exit(Sender: TObject);
    procedure EdAlunosAulaEnter(Sender: TObject);
    procedure EdValorUniEnter(Sender: TObject);
    procedure EdValorTotEnter(Sender: TObject);
    procedure EdAlunosAulaExit(Sender: TObject);
    procedure EdValorUniExit(Sender: TObject);
    procedure EdValorTotExit(Sender: TObject);
    procedure EdComPromPerEnter(Sender: TObject);
    procedure EdComPromPerExit(Sender: TObject);
    procedure EdComPromValEnter(Sender: TObject);
    procedure EdComPromValExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDTSPromPerEnter(Sender: TObject);
    procedure EdDTSPromPerExit(Sender: TObject);
    procedure EdDTSPromValEnter(Sender: TObject);
    procedure EdDTSPromValExit(Sender: TObject);
    procedure EdComProfPerEnter(Sender: TObject);
    procedure EdComProfPerExit(Sender: TObject);
    procedure EdComProfValEnter(Sender: TObject);
    procedure EdComProfValExit(Sender: TObject);
    procedure EdDTSProfPerEnter(Sender: TObject);
    procedure EdDTSProfPerExit(Sender: TObject);
    procedure EdDTSProfValEnter(Sender: TObject);
    procedure EdDTSProfValExit(Sender: TObject);
  private
    { Private declarations }
    FValAluCalc: TValAluCalc;
    FOnCreate: Boolean;
    procedure CalculaValorAlunos();
  public
    { Public declarations }
  end;

  var
  FmCiclosAula: TFmCiclosAula;

implementation

uses UnMyVCLref, UmySQlModule, UnFinanceiro, Module, UnInternalConsts,
 ModuleCiclos, UnMyObjects;

{$R *.DFM}


procedure TFmCiclosAula.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosAula.EdValorUniEnter(Sender: TObject);
begin
  FValAluCalc := vacAlunos;
end;

procedure TFmCiclosAula.EdValorUniExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdValorTotEnter(Sender: TObject);
begin
  FValAluCalc := vacAlunos;
end;

procedure TFmCiclosAula.EdValorTotExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdAlunosAulaEnter(Sender: TObject);
begin
  FValAluCalc := vacAlunos;
end;

procedure TFmCiclosAula.EdAlunosAulaExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdComProfPerEnter(Sender: TObject);
begin
  FValAluCalc := vacComProfPer;
end;

procedure TFmCiclosAula.EdComProfPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdComProfValEnter(Sender: TObject);
begin
  FValAluCalc := vacComProfVal;
end;

procedure TFmCiclosAula.EdComProfValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdComPromPerEnter(Sender: TObject);
begin
  FValAluCalc := vacComPromPer;
end;

procedure TFmCiclosAula.EdComPromPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdComPromValEnter(Sender: TObject);
begin
  FValAluCalc := vacComPromVal;
end;

procedure TFmCiclosAula.EdComPromValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdDTSProfPerEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSProfPer;
end;

procedure TFmCiclosAula.EdDTSProfPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdDTSProfValEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSProfVal;
end;

procedure TFmCiclosAula.EdDTSProfValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdDTSPromPerEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSPromPer;
end;

procedure TFmCiclosAula.EdDTSPromPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.EdDTSPromValEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSPromVal;
end;

procedure TFmCiclosAula.EdDTSPromValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula.dmkEdit7Exit(Sender: TObject);
begin
  dmkedit7.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(dmkedit7.Text));
end;

procedure TFmCiclosAula.FormActivate(Sender: TObject);
  procedure SetaPecentualEMinimo(EdPer, EdMin: TdmkEdit; Rol, Cta: Integer);
  begin
    DmCiclos.QrCtaSew.Close;
    DmCiclos.QrCtaSew.Params[00].AsInteger := Rol;
    DmCiclos.QrCtaSew.Params[01].AsInteger := Cta;
    DmCiclos.QrCtaSew.Open;
    if DmCiclos.QrCtaSew.RecordCount > 1 then
    Application.MessageBox(PChar('A conta ' + IntToStr(Cta) + ' possui ' +
    IntToStr(DmCiclos.QrCtaSew.RecordCount) + ' cadastros no rol de comiss�es '
    + IntToStr(Rol) + '. Apenas o primeiro ser� usado para setar o percentual '
    + 'e o m�nimo!'), 'Aviso', MB_OK+MB_ICONWARNING);
    EdPer.ValueVariant := DmCiclos.QrCtaSewPerceGene.Value;
    EdMin.ValueVariant := DmCiclos.QrCtaSewMinQtde.Value;
  end;
var
  Rol, Cta: Integer;
begin
  MyObjects.CorIniComponente;
  EdPromotor.ValueVariant  := FmCiclos.QrCiclosPromotor.Value;
  EdProfessor.ValueVariant := FmCiclos.QrCiclosProfessor.Value;
  if FOnCreate then
  begin
    FOncreate := False;
    if LaTipo.SQLType = stIns then
    begin
      Rol := FmCiclos.QrCiclosRolComis.Value;
      //
      Cta := Dmod.QrControleCtaComProm.Value;
      SetaPecentualEMinimo(EdComPromPer, EdComPromMin, Rol, Cta);
      //
      Cta := Dmod.QrControleCta13oProm.Value;
      SetaPecentualEMinimo(EdDTSPromPer, EdDTSPromMin, Rol, Cta);
      //
      Cta := Dmod.QrControleCtaComProf.Value;
      SetaPecentualEMinimo(EdComProfPer, EdComProfMin, Rol, Cta);
      //
      Cta := Dmod.QrControleCta13oProf.Value;
      SetaPecentualEMinimo(EdDTSProfPer, EdDTSProfMin, Rol, Cta);
      //
      DmCiclos.QrCurso.Close;
      DmCiclos.QrCurso.Params[0].AsInteger := FmCiclos.QrCiclosCurso.Value;
      DmCiclos.QrCurso.Open;
      EdValorUni.ValueVariant := DmCiclos.QrCursoValor.Value;
      //
    end;
  end;
end;

procedure TFmCiclosAula.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosAula.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  if not Geral.ConfereUFeMunici_IBGE(EdUF.Text, EdCodiCidade.ValueVariant, 'Turma') then Exit;
  //
  Controle :=
    UMyMod.BuscaEmLivreY_Def('ciclosaula', 'controle', LaTipo.SQLType,
      FmCiclos.QrCiclosItsControle.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCiclosAula, LaTipo.SQLType, 'ciclosaula', Controle,
  Dmod.QrUpd) then
  begin
    DmCiclos.CalculaCiclo(FmCiclos.QrCiclosCodigo.Value,
      FmCiclos.QrCiclosControle.Value, FmCiclos.QrCiclosProfessor.Value,
      FmCiclos.QrCiclosRolComis.Value, 'FmCiclos');
    FmCiclos.ReopenCiclosIts(Controle);
    if CkContinuar.Checked then
    begin
      TPData.Date                   := TPData.Date + 1;
      EdInscritos.ValueVariant      := 0;
      EdAlunosAula.ValueVariant     := 0;
      EdCidade.Text                 := '';
      //
      CalculaValorAlunos();
      TPData.SetFocus;
    end else Close;
  end;
end;

procedure TFmCiclosAula.CalculaValorAlunos();
var
  Alunos: Integer;
  Preco, Total,
  ComPromPer, ComPromVal, DTSPromPer, DTSPromVal,
  ComProfPer, ComProfVal, DTSProfPer, DTSProfVal: Double;
  //
  Calc1: Boolean;
begin
  Alunos     := EdAlunosAula.ValueVariant;
  Preco      := EdValorUni.ValueVariant;
  Total      := EdValorTot.ValueVariant;
  ComPromPer := EdComPromPer.ValueVariant;
  ComPromVal := EdComPromVal.ValueVariant;
  DTSPromPer := EdDTSPromPer.ValueVariant;
  DTSPromVal := EdDTSPromVal.ValueVariant;
  ComProfPer := EdComProfPer.ValueVariant;
  ComProfVal := EdComProfVal.ValueVariant;
  DTSProfPer := EdDTSProfPer.ValueVariant;
  DTSProfVal := EdDTSProfVal.ValueVariant;
  //
  Calc1 := True;
  case FValAluCalc of
    vacAlunos: Total := Alunos * Preco;
    vacPreco : Total := Alunos * Preco;
    vacTotal : if Total = 0 then Preco := 0 else Preco := Total / Alunos;
    else Calc1 := False;
  end;
  if Calc1 then
  begin
    EdAlunosAula.ValueVariant := Alunos;
    EdValorUni.ValueVariant   := Preco;
    EdValorTot.ValueVariant   := Total;
  end;
  //
  if FValAluCalc = vacComPromVal then
  begin
    if Total = 0 then ComPromPer := 0 else
      ComPromPer := ComPromVal / Total * 100;
  end else begin
    if Alunos < EdComPromMin.ValueVariant then ComPromVal := 0
    else ComPromVal := Total * ComPromPer / 100;
  end;
  EdComPromPer.ValueVariant := ComPromPer;
  EdComPromVal.ValueVariant := ComPromVal;
  //
  if FValAluCalc = vacDTSPromVal then
  begin
    if Total = 0 then DTSPromPer := 0 else
      DTSPromPer := DTSPromVal / Total * 100;
  end else begin
    if Alunos < EdDTSPromMin.ValueVariant then DTSPromVal := 0
    else DTSPromVal := Total * DTSPromPer / 100;
  end;
  EdDTSPromPer.ValueVariant := DTSPromPer;
  EdDTSPromVal.ValueVariant := DTSPromVal;
  //
  //
  if FValAluCalc = vacComProfVal then
  begin
    if Total = 0 then ComProfPer := 0 else
      ComProfPer := ComProfVal / Total * 100;
  end else begin
    if Alunos < EdComProfMin.ValueVariant then ComProfVal := 0
    else ComProfVal := Total * ComProfPer / 100;
  end;
  EdComProfPer.ValueVariant := ComProfPer;
  EdComProfVal.ValueVariant := ComProfVal;
  //
  if FValAluCalc = vacDTSProfVal then
  begin
    if Total = 0 then DTSProfPer := 0 else
      DTSProfPer := DTSProfVal / Total * 100;
  end else begin
    if Alunos < EdDTSProfMin.ValueVariant then DTSProfVal := 0
    else DTSProfVal := Total * DTSProfPer / 100;
  end;
  EdDTSProfPer.ValueVariant := DTSProfPer;
  EdDTSProfVal.ValueVariant := DTSProfVal;
  //
end;

procedure TFmCiclosAula.FormCreate(Sender: TObject);
begin
  TPData.Date := Date;
  FOnCreate   := True;
  //
  QrMunici.Open;
end;

end.
