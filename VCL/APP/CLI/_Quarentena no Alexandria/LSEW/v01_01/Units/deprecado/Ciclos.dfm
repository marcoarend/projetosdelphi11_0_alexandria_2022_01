object FmCiclos: TFmCiclos
  Left = 255
  Top = 165
  Caption = 'CIC-CICLO-001 :: Ciclo de Cursos'
  ClientHeight = 567
  ClientWidth = 1192
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelItens: TPanel
    Left = 0
    Top = 48
    Width = 1192
    Height = 519
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 1190
      Height = 196
      Align = alTop
      TabOrder = 0
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 1188
        Height = 52
        Align = alTop
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 470
      Width = 1190
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel7: TPanel
        Left = 1081
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1192
    Height = 519
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 470
      Width = 1190
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1081
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1190
      Height = 196
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 4
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label6: TLabel
        Left = 58
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Promotor:'
      end
      object Label10: TLabel
        Left = 56
        Top = 44
        Width = 47
        Height = 13
        Caption = 'Professor:'
      end
      object Label12: TLabel
        Left = 580
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Data sa'#237'da:'
      end
      object Label13: TLabel
        Left = 580
        Top = 84
        Width = 59
        Height = 13
        Caption = 'Data acerto:'
      end
      object Label14: TLabel
        Left = 580
        Top = 45
        Width = 62
        Height = 13
        Caption = 'Data retorno:'
      end
      object SpeedButton5: TSpeedButton
        Left = 553
        Top = 20
        Width = 21
        Height = 21
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 553
        Top = 60
        Width = 21
        Height = 21
        OnClick = SpeedButton6Click
      end
      object Label36: TLabel
        Left = 56
        Top = 124
        Width = 30
        Height = 13
        Caption = 'Curso:'
        Visible = False
      end
      object SpeedButton7: TSpeedButton
        Left = 553
        Top = 140
        Width = 21
        Height = 21
        Visible = False
        OnClick = SpeedButton7Click
      end
      object Label27: TLabel
        Left = 56
        Top = 84
        Width = 86
        Height = 13
        Caption = 'Rol de comiss'#245'es:'
      end
      object SpeedButton8: TSpeedButton
        Left = 553
        Top = 100
        Width = 21
        Height = 21
        OnClick = SpeedButton7Click
      end
      object EdCodigo: TEdit
        Left = 4
        Top = 20
        Width = 48
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '00'
      end
      object EdPromotor: TdmkEditCB
        Left = 56
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPromotor
        IgnoraDBLookupComboBox = False
      end
      object EdProfessor: TdmkEditCB
        Left = 56
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProfessor
        IgnoraDBLookupComboBox = False
      end
      object CBProfessor: TdmkDBLookupComboBox
        Left = 112
        Top = 60
        Width = 441
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEPROFE'
        ListSource = DmCiclos.DsProfessores
        TabOrder = 3
        dmkEditCB = EdProfessor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataSaida: TdmkEditDateTimePicker
        Left = 580
        Top = 20
        Width = 100
        Height = 21
        Date = 39676.412710682900000000
        Time = 39676.412710682900000000
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataChega: TdmkEditDateTimePicker
        Left = 580
        Top = 61
        Width = 100
        Height = 21
        Date = 39676.412710682900000000
        Time = 39676.412710682900000000
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataAcert: TdmkEditDateTimePicker
        Left = 580
        Top = 100
        Width = 100
        Height = 21
        Date = 39676.412710682900000000
        Time = 39676.412710682900000000
        TabOrder = 8
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdCurso: TdmkEditCB
        Left = 56
        Top = 140
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCurso
        IgnoraDBLookupComboBox = False
      end
      object CBCurso: TdmkDBLookupComboBox
        Left = 112
        Top = 140
        Width = 441
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DmCiclos.DsCursos
        TabOrder = 5
        Visible = False
        dmkEditCB = EdCurso
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBPromotor: TdmkDBLookupComboBox
        Left = 112
        Top = 20
        Width = 441
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEPROMO'
        ListSource = DmCiclos.DsPromotores
        TabOrder = 9
        dmkEditCB = EdPromotor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdRolComis: TdmkEditCB
        Left = 56
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRolComis
        IgnoraDBLookupComboBox = False
      end
      object CBRolComis: TdmkDBLookupComboBox
        Left = 112
        Top = 100
        Width = 441
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DmCiclos.DsEquiCom
        TabOrder = 11
        dmkEditCB = EdRolComis
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1192
    Height = 519
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 470
      Width = 1190
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 251
        Height = 46
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
        ExplicitWidth = 26
        ExplicitHeight = 13
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 424
        Top = 1
        Width = 765
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtProdutos: TBitBtn
          Tag = 30
          Left = 178
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Mercad.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtProdutosClick
        end
        object BtCiclos: TBitBtn
          Tag = 322
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ciclo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCiclosClick
        end
        object Panel2: TPanel
          Left = 656
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtVolta: TBitBtn
          Tag = 10033
          Left = 96
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtVoltaClick
        end
        object BtAvanca: TBitBtn
          Tag = 10034
          Left = 136
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtAvancaClick
        end
        object BtDespesas: TBitBtn
          Tag = 10044
          Left = 270
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Despesas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtDespesasClick
        end
        object BtTransf: TBitBtn
          Tag = 327
          Left = 362
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Transfer.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtTransfClick
        end
        object BtRateio: TBitBtn
          Tag = 324
          Left = 454
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Rateio'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BtRateioClick
        end
        object BtEmpresta: TBitBtn
          Tag = 10046
          Left = 546
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Empr'#233'st.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          OnClick = BtEmprestaClick
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1190
      Height = 208
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 2
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 8
        Top = 41
        Width = 45
        Height = 13
        Caption = 'Promotor:'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 296
        Top = 41
        Width = 47
        Height = 13
        Caption = 'Professor:'
        FocusControl = DBEdit3
      end
      object Label4: TLabel
        Left = 59
        Top = 2
        Width = 71
        Height = 13
        Caption = 'Data de sa'#237'da:'
        FocusControl = DBEdit5
      end
      object Label5: TLabel
        Left = 289
        Top = 2
        Width = 77
        Height = 13
        Caption = 'Data de retorno:'
        FocusControl = DBEdit6
      end
      object Label8: TLabel
        Left = 519
        Top = 2
        Width = 74
        Height = 13
        Caption = 'Data do acerto:'
        FocusControl = DBEdit7
      end
      object Label11: TLabel
        Left = 589
        Top = 40
        Width = 97
        Height = 13
        Caption = 'Etapa atual do ciclo:'
        FocusControl = DBEdit8
      end
      object Label48: TLabel
        Left = 1004
        Top = 160
        Width = 71
        Height = 13
        Caption = 'Retido viagem:'
        FocusControl = DBEdit37
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 57
        Width = 32
        Height = 21
        DataField = 'Promotor'
        DataSource = DsCiclos
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 40
        Top = 57
        Width = 253
        Height = 21
        DataField = 'NOMEPRM'
        DataSource = DsCiclos
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 296
        Top = 57
        Width = 32
        Height = 21
        DataField = 'Professor'
        DataSource = DsCiclos
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 328
        Top = 57
        Width = 258
        Height = 21
        DataField = 'NOMEPRF'
        DataSource = DsCiclos
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 59
        Top = 18
        Width = 227
        Height = 21
        DataField = 'DATASAIDA_TXT'
        DataSource = DsCiclos
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 289
        Top = 18
        Width = 227
        Height = 21
        DataField = 'DATACHEGA_TXT'
        DataSource = DsCiclos
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 519
        Top = 18
        Width = 230
        Height = 21
        DataField = 'DATAACERT_TXT'
        DataSource = DsCiclos
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 609
        Top = 57
        Width = 140
        Height = 21
        DataField = 'STATUS_TXT'
        DataSource = DsCiclos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 589
        Top = 57
        Width = 21
        Height = 21
        DataField = 'Status'
        DataSource = DsCiclos
        TabOrder = 8
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 84
        Width = 317
        Height = 60
        Caption = ' Venda de Mercadorias: '
        TabOrder = 9
        object Label17: TLabel
          Left = 8
          Top = 16
          Width = 34
          Height = 13
          Caption = 'Venda:'
          FocusControl = DBEdit12
        end
        object Label18: TLabel
          Left = 84
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Margem:'
          FocusControl = DBEdit13
        end
        object Label19: TLabel
          Left = 160
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Recebido:'
          FocusControl = DBEdit14
        end
        object Label23: TLabel
          Left = 236
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Diferen'#231'a:'
          FocusControl = DBEdProdif
        end
        object DBEdit12: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ProdVal'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 84
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ProdMrg'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdit14: TDBEdit
          Left = 160
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ProdPgt'
          DataSource = DsCiclos
          TabOrder = 2
        end
        object DBEdProdif: TDBEdit
          Left = 236
          Top = 32
          Width = 72
          Height = 21
          DataField = 'PRODDIF'
          DataSource = DsCiclos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnChange = DBEdProdifChange
        end
      end
      object GroupBox2: TGroupBox
        Left = 328
        Top = 84
        Width = 421
        Height = 60
        Caption = ' Alunos (curso) e recebimentos de alunos: '
        TabOrder = 10
        object Label15: TLabel
          Left = 8
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Inscri'#231#245'es:'
          FocusControl = DBEdit10
        end
        object Label16: TLabel
          Left = 64
          Top = 16
          Width = 47
          Height = 13
          Caption = 'Cursados:'
          FocusControl = DBEdit11
        end
        object Label20: TLabel
          Left = 120
          Top = 16
          Width = 34
          Height = 13
          Caption = 'Pre'#231'o: '
          FocusControl = DBEdit15
        end
        object Label21: TLabel
          Left = 188
          Top = 16
          Width = 40
          Height = 13
          Caption = 'Receita:'
          FocusControl = DBEdit16
        end
        object Label22: TLabel
          Left = 264
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Recebido:'
          FocusControl = DBEdit17
        end
        object Label24: TLabel
          Left = 340
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Diferen'#231'a:'
          FocusControl = DBEdAlunosDif
        end
        object DBEdit10: TDBEdit
          Left = 8
          Top = 32
          Width = 52
          Height = 21
          DataField = 'AlunosInsc'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit11: TDBEdit
          Left = 64
          Top = 32
          Width = 52
          Height = 21
          DataField = 'AlunosAula'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdit15: TDBEdit
          Left = 120
          Top = 32
          Width = 64
          Height = 21
          DataField = 'AlunosPrc'
          DataSource = DsCiclos
          TabOrder = 2
        end
        object DBEdit16: TDBEdit
          Left = 188
          Top = 32
          Width = 72
          Height = 21
          DataField = 'AlunosVal'
          DataSource = DsCiclos
          TabOrder = 3
        end
        object DBEdit17: TDBEdit
          Left = 264
          Top = 32
          Width = 72
          Height = 21
          DataField = 'AlunosPgt'
          DataSource = DsCiclos
          TabOrder = 4
        end
        object DBEdAlunosDif: TDBEdit
          Left = 340
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ALUNOSDIF'
          DataSource = DsCiclos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnChange = DBEdAlunosDifChange
        end
      end
      object GroupBox4: TGroupBox
        Left = 752
        Top = 84
        Width = 241
        Height = 60
        Caption = ' Transfer'#234'ncias: '
        TabOrder = 11
        object Label29: TLabel
          Left = 8
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Cr'#233'ditos:'
          FocusControl = DBEdit18
        end
        object Label30: TLabel
          Left = 84
          Top = 16
          Width = 39
          Height = 13
          Caption = 'D'#233'bitos:'
          FocusControl = DBEdit19
        end
        object Label31: TLabel
          Left = 160
          Top = 16
          Width = 39
          Height = 13
          Caption = 'L'#237'quido:'
          FocusControl = DBEdit20
        end
        object DBEdit18: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'TransfCre'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit19: TDBEdit
          Left = 84
          Top = 32
          Width = 72
          Height = 21
          DataField = 'TransfDeb'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdit20: TDBEdit
          Left = 160
          Top = 32
          Width = 72
          Height = 21
          DataField = 'TRANSFDIF'
          DataSource = DsCiclos
          TabOrder = 2
        end
      end
      object GroupBox5: TGroupBox
        Left = 752
        Top = 12
        Width = 241
        Height = 66
        Caption = ' Despesas: '
        TabOrder = 12
        object Label32: TLabel
          Left = 8
          Top = 16
          Width = 47
          Height = 13
          Caption = 'Professor:'
          FocusControl = DBEdit21
        end
        object Label25: TLabel
          Left = 84
          Top = 16
          Width = 45
          Height = 13
          Caption = 'Promotor:'
          FocusControl = DBEdit33
        end
        object Label26: TLabel
          Left = 160
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Total:'
          FocusControl = DBEdit34
        end
        object DBEdit21: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'DespProf'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit33: TDBEdit
          Left = 84
          Top = 32
          Width = 72
          Height = 21
          DataField = 'DespProm'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdit34: TDBEdit
          Left = 160
          Top = 32
          Width = 72
          Height = 21
          DataField = 'Despesas'
          DataSource = DsCiclos
          TabOrder = 2
        end
      end
      object GroupBox6: TGroupBox
        Left = 404
        Top = 144
        Width = 345
        Height = 61
        Caption = ' TOTAIS: '
        TabOrder = 13
        object Label40: TLabel
          Left = 4
          Top = 16
          Width = 45
          Height = 13
          Caption = 'Receitas:'
          FocusControl = DBEdit28
        end
        object Label41: TLabel
          Left = 88
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Cr'#233'ditos:'
          FocusControl = DBEdit29
        end
        object Label42: TLabel
          Left = 172
          Top = 16
          Width = 56
          Height = 13
          Caption = 'Receita liq.:'
          FocusControl = DBEdit30
        end
        object Label43: TLabel
          Left = 256
          Top = 16
          Width = 54
          Height = 13
          Caption = 'Cred - D'#233'b:'
          FocusControl = DBEdit31
        end
        object DBEdit28: TDBEdit
          Left = 4
          Top = 32
          Width = 80
          Height = 21
          DataField = 'TOT_RECEITAS'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit29: TDBEdit
          Left = 88
          Top = 32
          Width = 80
          Height = 21
          DataField = 'TOT_CREDITOS'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdit30: TDBEdit
          Left = 172
          Top = 32
          Width = 80
          Height = 21
          DataField = 'TOT_RECE_LIQ'
          DataSource = DsCiclos
          TabOrder = 2
        end
        object DBEdit31: TDBEdit
          Left = 256
          Top = 32
          Width = 80
          Height = 21
          DataField = 'TOT_SALD_LIQ'
          DataSource = DsCiclos
          TabOrder = 3
        end
      end
      object GroupBox7: TGroupBox
        Left = 8
        Top = 144
        Width = 393
        Height = 61
        Caption = ' Comiss'#227'o do professor e pagamento: '
        TabOrder = 14
        object Label39: TLabel
          Left = 8
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Comiss'#227'o:'
          FocusControl = DBEdit27
        end
        object Label33: TLabel
          Left = 84
          Top = 16
          Width = 57
          Height = 13
          Caption = 'Repassado:'
          FocusControl = DBEdit22
        end
        object Label37: TLabel
          Left = 160
          Top = 16
          Width = 59
          Height = 13
          Caption = 'Dif. repasse:'
          FocusControl = DBEdComisDif
        end
        object Label44: TLabel
          Left = 236
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Real receb.:'
          FocusControl = DBEdit26
        end
        object Label45: TLabel
          Left = 312
          Top = 16
          Width = 69
          Height = 13
          Caption = 'Saldo a pagar:'
          FocusControl = DBEdit32
        end
        object DBEdit27: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ComisVal'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit22: TDBEdit
          Left = 84
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ComisTrf'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdComisDif: TDBEdit
          Left = 160
          Top = 32
          Width = 72
          Height = 21
          DataField = 'COMISDIF'
          DataSource = DsCiclos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnChange = DBEdComisDifChange
        end
        object DBEdit26: TDBEdit
          Left = 236
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ProfVal'
          DataSource = DsCiclos
          TabOrder = 3
        end
        object DBEdit32: TDBEdit
          Left = 312
          Top = 32
          Width = 72
          Height = 21
          DataField = 'PAGOAPROF'
          DataSource = DsCiclos
          TabOrder = 4
        end
      end
      object GroupBox8: TGroupBox
        Left = 752
        Top = 144
        Width = 241
        Height = 61
        Caption = ' Pend'#234'ncias professor: '
        TabOrder = 15
        object Label34: TLabel
          Left = 8
          Top = 16
          Width = 60
          Height = 13
          Caption = 'Sdo anterior:'
          FocusControl = DBEdit23
        end
        object Label38: TLabel
          Left = 84
          Top = 16
          Width = 65
          Height = 13
          Caption = 'Sdo empr'#233'st.:'
          FocusControl = DBEdit25
        end
        object Label35: TLabel
          Left = 160
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Sdo final:'
          FocusControl = DBEdit24
        end
        object DBEdit23: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'SdoProfIni'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit25: TDBEdit
          Left = 84
          Top = 32
          Width = 72
          Height = 21
          DataField = 'EMPRES_SDOATU'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdit24: TDBEdit
          Left = 160
          Top = 32
          Width = 72
          Height = 21
          DataField = 'EMPRES_SDOVER'
          DataSource = DsCiclos
          TabOrder = 2
        end
      end
      object GroupBox3: TGroupBox
        Left = 996
        Top = 12
        Width = 85
        Height = 141
        Caption = ' Comis. alunos: '
        TabOrder = 17
        object Label28: TLabel
          Left = 8
          Top = 16
          Width = 54
          Height = 13
          Caption = 'Dilig'#234'ncias:'
          FocusControl = DBEdit35
        end
        object Label46: TLabel
          Left = 8
          Top = 56
          Width = 39
          Height = 13
          Caption = 'Vendas:'
          FocusControl = DBEdit36
        end
        object Label47: TLabel
          Left = 8
          Top = 96
          Width = 49
          Height = 13
          Caption = 'Diferen'#231'a:'
          FocusControl = DBEdVALALUDIF
        end
        object DBEdit35: TDBEdit
          Left = 8
          Top = 32
          Width = 72
          Height = 21
          DataField = 'ValAluDil'
          DataSource = DsCiclos
          TabOrder = 0
        end
        object DBEdit36: TDBEdit
          Left = 8
          Top = 72
          Width = 72
          Height = 21
          DataField = 'ValAluVen'
          DataSource = DsCiclos
          TabOrder = 1
        end
        object DBEdVALALUDIF: TDBEdit
          Left = 8
          Top = 112
          Width = 72
          Height = 21
          DataField = 'VALALUDIF'
          DataSource = DsCiclos
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnChange = DBEdVALALUDIFChange
        end
      end
      object DBEdit37: TDBEdit
        Left = 1004
        Top = 176
        Width = 72
        Height = 21
        DataField = 'Valor'
        DataSource = DsRetido
        TabOrder = 16
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 18
        Width = 48
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCiclos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 1
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 18
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 224
      Width = 1190
      Height = 246
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Ciclo professor'
        object PageControl2: TPageControl
          Left = 0
          Top = 20
          Width = 1182
          Height = 198
          ActivePage = TabSheet8
          Align = alBottom
          TabOrder = 0
          object TabSheet3: TTabSheet
            Caption = 'Itens levados'
            object DBGL: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 170
              Align = alClient
              DataSource = DmCiclos.DsMovimL
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEGRA'
                  Title.Caption = 'Mercadoria'
                  Width = 278
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECOR'
                  Title.Caption = 'Cor'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMETAM'
                  Title.Caption = 'Tamanho'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'POSIq'
                  Title.Caption = 'Quant.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PRECO'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'POSIv'
                  Title.Caption = 'Total'
                  Visible = True
                end>
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Devolu'#231#227'o dos itens'
            ImageIndex = 1
            object DBGT: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 170
              Align = alClient
              DataSource = DmCiclos.DsMovimT
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEGRA'
                  Title.Caption = 'Mercadoria'
                  Width = 278
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECOR'
                  Title.Caption = 'Cor'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMETAM'
                  Title.Caption = 'Tamanho'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'POSIq'
                  Title.Caption = 'Quant.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PRECO'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'POSIv'
                  Title.Caption = 'Total'
                  Visible = True
                end>
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Itens utilizados'
            ImageIndex = 2
            object DBGV: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 130
              Align = alClient
              DataSource = DmCiclos.DsMovimV
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Grade'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEGRA'
                  Title.Caption = 'Mercadoria'
                  Width = 168
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cor'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECOR'
                  Title.Caption = 'Cor'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tam'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMETAM'
                  Title.Caption = 'Tamanho'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'POSIq'
                  Title.Caption = 'Quant.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PRECO'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'POSIv'
                  Title.Caption = 'Total'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComisTipNOME'
                  Title.Caption = 'Tipo'
                  Width = 10
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComisFat'
                  Title.Caption = 'Fator'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ComisVal'
                  Title.Caption = 'Comiss'#227'o'
                  Visible = True
                end>
            end
            object DBGPagtosV_: TDBGrid
              Left = 0
              Top = 130
              Width = 1174
              Height = 40
              Align = alBottom
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Visible = False
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Valor'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECARTEIRA'
                  Title.Caption = 'Carteira'
                  Width = 101
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 69
                  Visible = True
                end>
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Despesas de viagem'
            ImageIndex = 3
            object DBGDespProf: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 170
              Align = alClient
              DataSource = DmCiclos.DsDespProf
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Valor'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECARTEIRA'
                  Title.Caption = 'Carteira'
                  Width = 175
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Conta (Plano de contas)'
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Transfer'#234'ncias'
            ImageIndex = 4
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 170
              Align = alClient
              DataSource = DmCiclos.DsTransfAll
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'credito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'D'#233'bito'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECART'
                  Title.Caption = 'Carteira'
                  Width = 175
                  Visible = True
                end>
            end
          end
          object TabSheet8: TTabSheet
            Caption = 'Empr'#233'stimos a professores'
            ImageIndex = 6
            object DBGEmprestimos: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 170
              Align = alClient
              DataSource = DmCiclos.DsEmprestimos
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Cr'#233'dito'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'D'#233'bito'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECARTEIRA'
                  Title.Caption = 'Carteira'
                  Width = 175
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Conta (Plano de contas)'
                  Width = 266
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatID'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatID_Sub'
                  Title.Caption = 'Sub'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
          end
          object TabSheet9: TTabSheet
            Caption = 'Rateios (Lan'#231'amentos autom'#225'ticos a cr'#233'dito e d'#233'bito)'
            ImageIndex = 7
            object DBGRateio: TDBGrid
              Left = 0
              Top = 0
              Width = 1174
              Height = 170
              Align = alClient
              DataSource = DmCiclos.DsAuto
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Cr'#233'dito'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'D'#233'bito'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECARTEIRA'
                  Title.Caption = 'Carteira'
                  Width = 175
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Conta (Plano de contas)'
                  Width = 266
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatID'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FatID_Sub'
                  Title.Caption = 'Sub'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Turmas e despesas'
        ImageIndex = 1
        object Panel4: TPanel
          Left = 669
          Top = 0
          Width = 513
          Height = 218
          Align = alRight
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 1
            Top = 113
            Width = 511
            Height = 3
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 42
            ExplicitWidth = 58
          end
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 511
            Height = 112
            Align = alTop
            TabOrder = 0
            object Panel13: TPanel
              Left = 1
              Top = 1
              Width = 509
              Height = 19
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Despesas do PROFESSOR no dia selecionado'
              TabOrder = 0
            end
            object DBGrid2: TDBGrid
              Left = 1
              Top = 20
              Width = 509
              Height = 91
              Align = alClient
              DataSource = DmCiclos.DsDespProf
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Valor'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECARTEIRA'
                  Title.Caption = 'Carteira'
                  Width = 175
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Conta (Plano de contas)'
                  Width = 226
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
          end
          object Panel11: TPanel
            Left = 1
            Top = 116
            Width = 511
            Height = 101
            Align = alClient
            TabOrder = 1
            object Panel12: TPanel
              Left = 1
              Top = 1
              Width = 509
              Height = 19
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Despesas do PROMOTOR no dia selecionado'
              TabOrder = 0
            end
            object DBGDespProm: TDBGrid
              Left = 1
              Top = 20
              Width = 509
              Height = 80
              Align = alClient
              DataSource = DmCiclos.DsDespProm
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Valor'
                  Width = 70
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Qtde'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECARTEIRA'
                  Title.Caption = 'Carteira'
                  Width = 175
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Conta (Plano de contas)'
                  Width = 226
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end>
            end
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 669
          Height = 218
          Align = alClient
          TabOrder = 1
          object Splitter2: TSplitter
            Left = 1
            Top = 114
            Width = 667
            Height = 3
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = 1
            ExplicitWidth = 491
          end
          object Splitter3: TSplitter
            Left = 1
            Top = 45
            Width = 667
            Height = 3
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = 1
            ExplicitWidth = 491
          end
          object GradeTurmas: TDBGrid
            Left = 1
            Top = 1
            Width = 667
            Height = 44
            Align = alClient
            DataSource = DsCiclosAula
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MUNI'
                Title.Caption = 'Cidade'
                Width = 124
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AlunosInsc'
                Title.Caption = 'Incritos'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AlunosAula'
                Title.Caption = 'Presen'#231'a'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorTot'
                Title.Caption = 'Valor'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComProfDeb'
                Title.Caption = 'D.Prf.Comis.'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTSProfDeb'
                Title.Caption = 'D.Prf.13'#186
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComPromDeb'
                Title.Caption = 'D.Prm.Comis.'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTSPromDeb'
                Title.Caption = 'D.Prm.13'#186
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComProfAlu'
                Title.Caption = 'Comis.Aluno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComProfPrd'
                Title.Caption = 'Comis.Prod'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComProfAll'
                Title.Caption = 'Comiss'#245'es'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComProfSaq'
                Title.Caption = 'Saque'
                Visible = True
              end>
          end
          object DBGrid3: TDBGrid
            Left = 1
            Top = 117
            Width = 667
            Height = 100
            Align = alBottom
            DataSource = DmCiclos.DsTransfDia
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'SEQ'
                Title.Caption = 'N'#186
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'credito'
                Title.Alignment = taRightJustify
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Alignment = taRightJustify
                Title.Caption = 'D'#233'bito'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECART'
                Title.Caption = 'Carteira'
                Width = 183
                Visible = True
              end>
          end
          object DBGrid4: TDBGrid
            Left = 1
            Top = 48
            Width = 667
            Height = 66
            Align = alBottom
            DataSource = DmCiclos.DsMovimD
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Grade'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 168
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cor'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tam'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tamanho'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Quant.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'Total'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComisTipNOME'
                Title.Caption = 'Tipo'
                Width = 10
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComisFat'
                Title.Caption = 'Fator'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ComisVal'
                Title.Caption = 'Comiss'#227'o'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1192
    Height = 48
    Align = alTop
    Caption = '                              Ciclo de Cursos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 883
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 233
      ExplicitTop = 3
      ExplicitWidth = 705
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 1109
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 932
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCiclos: TDataSource
    DataSet = QrCiclos
    Left = 476
    Top = 8
  end
  object QrCiclos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCiclosBeforeOpen
    AfterOpen = QrCiclosAfterOpen
    BeforeClose = QrCiclosBeforeClose
    AfterScroll = QrCiclosAfterScroll
    OnCalcFields = QrCiclosCalcFields
    SQL.Strings = (
      'SELECT cic.*, '
      'IF(prf.Tipo=0, prf.RazaoSocial, prf.Nome) NOMEPRF,'
      'IF(prm.Tipo=0, prm.RazaoSocial, prm.Nome) NOMEPRM, '
      'prf.CartPref CART_PROF, prm.CartPref CART_PROM,'
      'prf.RolComis ROLCOMIS_PRF'
      'FROM ciclos cic'
      'LEFT JOIN entidades prf ON prf.Codigo=cic.Professor'
      'LEFT JOIN entidades prm ON prm.Codigo=cic.Promotor'
      'WHERE cic.Codigo > 0')
    Left = 448
    Top = 8
    object QrCiclosDATASAIDA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATASAIDA_TXT'
      Size = 100
      Calculated = True
    end
    object QrCiclosDATACHEGA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATACHEGA_TXT'
      Size = 100
      Calculated = True
    end
    object QrCiclosDATAACERT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAACERT_TXT'
      Size = 100
      Calculated = True
    end
    object QrCiclosSTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Calculated = True
    end
    object QrCiclosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ciclos.Codigo'
      Required = True
    end
    object QrCiclosDataSaida: TDateField
      FieldName = 'DataSaida'
      Origin = 'ciclos.DataSaida'
      Required = True
    end
    object QrCiclosDataChega: TDateField
      FieldName = 'DataChega'
      Origin = 'ciclos.DataChega'
      Required = True
    end
    object QrCiclosDataAcert: TDateField
      FieldName = 'DataAcert'
      Origin = 'ciclos.DataAcert'
      Required = True
    end
    object QrCiclosAlunosInsc: TIntegerField
      FieldName = 'AlunosInsc'
      Origin = 'ciclos.AlunosInsc'
      Required = True
    end
    object QrCiclosAlunosAula: TIntegerField
      FieldName = 'AlunosAula'
      Origin = 'ciclos.AlunosAula'
      Required = True
    end
    object QrCiclosStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'ciclos.Status'
      Required = True
    end
    object QrCiclosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'ciclos.Controle'
      Required = True
    end
    object QrCiclosPromotor: TIntegerField
      FieldName = 'Promotor'
      Origin = 'ciclos.Promotor'
      Required = True
    end
    object QrCiclosProfessor: TIntegerField
      FieldName = 'Professor'
      Origin = 'ciclos.Professor'
      Required = True
    end
    object QrCiclosAlunosPrc: TFloatField
      FieldName = 'AlunosPrc'
      Origin = 'ciclos.AlunosPrc'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAlunosVal: TFloatField
      FieldName = 'AlunosVal'
      Origin = 'ciclos.AlunosVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAlunosPgt: TFloatField
      FieldName = 'AlunosPgt'
      Origin = 'ciclos.AlunosPgt'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProdVal: TFloatField
      FieldName = 'ProdVal'
      Origin = 'ciclos.ProdVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProdPgt: TFloatField
      FieldName = 'ProdPgt'
      Origin = 'ciclos.ProdPgt'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProdMrg: TFloatField
      FieldName = 'ProdMrg'
      Origin = 'ciclos.ProdMrg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosPRODDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRODDIF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosDespesas: TFloatField
      FieldName = 'Despesas'
      Origin = 'ciclos.Despesas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosNOMEPRF: TWideStringField
      FieldName = 'NOMEPRF'
      Size = 100
    end
    object QrCiclosNOMEPRM: TWideStringField
      FieldName = 'NOMEPRM'
      Size = 100
    end
    object QrCiclosCART_PROF: TIntegerField
      FieldName = 'CART_PROF'
      Origin = 'entidades.CartPref'
    end
    object QrCiclosALUNOSDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ALUNOSDIF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosTransfCre: TFloatField
      FieldName = 'TransfCre'
      Origin = 'ciclos.TransfCre'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosTransfDeb: TFloatField
      FieldName = 'TransfDeb'
      Origin = 'ciclos.TransfDeb'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosTRANSFDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TRANSFDIF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosVALORRATEIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORRATEIO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosCurso: TIntegerField
      FieldName = 'Curso'
      Origin = 'ciclos.Curso'
      Required = True
    end
    object QrCiclosRatDebVar: TFloatField
      FieldName = 'RatDebVar'
      Origin = 'ciclos.RatDebVar'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosRatTrfFix: TFloatField
      FieldName = 'RatTrfFix'
      Origin = 'ciclos.RatTrfFix'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosRatTrfVar: TFloatField
      FieldName = 'RatTrfVar'
      Origin = 'ciclos.RatTrfVar'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosRATEIODIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RATEIODIF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosComisVal: TFloatField
      FieldName = 'ComisVal'
      Origin = 'ciclos.ComisVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosTOT_RECEITAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_RECEITAS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosTOT_CREDITOS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_CREDITOS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosTOT_RECE_LIQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_RECE_LIQ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosTOT_SALD_LIQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_SALD_LIQ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosSdoProfIni: TFloatField
      FieldName = 'SdoProfIni'
      Origin = 'ciclos.SdoProfIni'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosSdoProfFim: TFloatField
      FieldName = 'SdoProfFim'
      Origin = 'ciclos.SdoProfFim'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosComisPgt: TFloatField
      FieldName = 'ComisPgt'
      Origin = 'ciclos.ComisPgt'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosComisTrf: TFloatField
      FieldName = 'ComisTrf'
      Origin = 'ciclos.ComisTrf'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosProfVal: TFloatField
      FieldName = 'ProfVal'
      Origin = 'ciclos.ProfVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosCOMISDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COMISDIF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosEmprestC: TFloatField
      FieldName = 'EmprestC'
      Origin = 'ciclos.EmprestC'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosEmprestD: TFloatField
      FieldName = 'EmprestD'
      Origin = 'ciclos.EmprestD'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosEMPRES_SDOATU: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EMPRES_SDOATU'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosEMPRES_SDOFIM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EMPRES_SDOFIM'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosEMPRES_SDOVER: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EMPRES_SDOVER'
      Calculated = True
    end
    object QrCiclosAPAGAR_PROF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'APAGAR_PROF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosPAGOAPROF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PAGOAPROF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCiclosEMPRES_SDOATU_NEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EMPRES_SDOATU_NEG'
      Calculated = True
    end
    object QrCiclosCART_PROM: TIntegerField
      FieldName = 'CART_PROM'
      Origin = 'entidades.CartPref'
    end
    object QrCiclosDespProm: TFloatField
      FieldName = 'DespProm'
      Origin = 'ciclos.DespProm'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosDespProf: TFloatField
      FieldName = 'DespProf'
      Origin = 'ciclos.DespProf'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosROLCOMIS_PRF: TIntegerField
      FieldName = 'ROLCOMIS_PRF'
      Origin = 'entidades.RolComis'
    end
    object QrCiclosRolComis: TIntegerField
      FieldName = 'RolComis'
      Origin = 'ciclos.RolComis'
      Required = True
    end
    object QrCiclosValAluDil: TFloatField
      FieldName = 'ValAluDil'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosValAluVen: TFloatField
      FieldName = 'ValAluVen'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosVALALUDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALALUDIF'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object PMCiclos: TPopupMenu
    OnPopup = PMCiclosPopup
    Left = 488
    Top = 492
    object Incluinovociclo1: TMenuItem
      Caption = '&Inclui novo ciclo'
      OnClick = Crianovogrupo1Click
    end
    object Alteracicloatual1: TMenuItem
      Caption = '&Altera ciclo atual'
      Enabled = False
      OnClick = Alteragrupoatual1Click
    end
    object Excluicicloatual1: TMenuItem
      Caption = '&Exclui ciclo atual'
      Enabled = False
      OnClick = Excluigrupoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Avaarparaprximaetapa1: TMenuItem
      Caption = 'Avan'#231'ar para &Pr'#243'xima etapa'
      OnClick = Avaarparaprximaetapa1Click
    end
    object Voltarparaetapaanterior1: TMenuItem
      Caption = '&Voltar para etapa anterior'
      OnClick = Voltarparaetapaanterior1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Calculaereabre1: TMenuItem
      Caption = '&Calcula e reabre'
      OnClick = Calculaereabre1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object urmas1: TMenuItem
      Caption = 'Turmas'
      object Incluiturma1: TMenuItem
        Caption = '&Inclui nova turma'
        OnClick = Incluiturma1Click
      end
      object Alteraturma1: TMenuItem
        Caption = '&Altera turma atual'
        OnClick = Alteraturma1Click
      end
      object Excluiturma1: TMenuItem
        Caption = '&Exclui turma atual'
        OnClick = Excluiturma1Click
      end
    end
  end
  object QrCiclosIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cui.* '
      'FROM ciclosits cui'
      'LEFT JOIN contas cta ON cta.Codigo=cui.Genero'
      'WHERE cui.Codigo=:P0')
    Left = 532
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCiclosItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCiclosItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCiclosItsNomeImp: TWideStringField
      FieldName = 'NomeImp'
      Size = 50
    end
    object QrCiclosItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCiclosItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCiclosItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCiclosItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCiclosItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCiclosItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCiclosItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCiclosItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCiclosIts: TDataSource
    DataSet = QrCiclosIts
    Left = 560
    Top = 8
  end
  object PMVende: TPopupMenu
    OnPopup = PMVendePopup
    Left = 664
    Top = 492
    object Incluialteravendademercadoria1: TMenuItem
      Caption = '&Inclui / altera venda de mercadoria'
      OnClick = Incluialteravendademercadoria1Click
    end
    object ExcluiitemdemercadoriaVenda1: TMenuItem
      Caption = '&Exclui item de mercadoria (Venda)'
      OnClick = ExcluiitemdemercadoriaVenda1Click
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object Incluipagamento1: TMenuItem
      Caption = 'Inclui pagamento'
      Enabled = False
      Visible = False
      object Automtico1: TMenuItem
        Caption = '&Autom'#225'tico'
        Enabled = False
        OnClick = Automtico1Click
      end
      object Manual1: TMenuItem
        Caption = '&Manual'
        OnClick = Manual1Click
      end
    end
    object Excluipagamentos1: TMenuItem
      Caption = '&Exclui pagamento(s)'
      Enabled = False
      Visible = False
      OnClick = Excluipagamentos1Click
    end
  end
  object frxCiclo_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39673.632371770800000000
    ReportOptions.LastChange = 39673.632371770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture2.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    OnGetValue = frxCiclo_AGetValue
    Left = 616
    Top = 8
    Datasets = <
      item
        DataSet = frxDsCiclos
        DataSetName = 'frxDsCiclos'
      end
      item
        DataSet = DmCiclos.frxDsDespesas
        DataSetName = 'frxDsDespesas'
      end
      item
        DataSet = DmCiclos.frxDsLMovV
        DataSetName = 'frxDsLMovV'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRetido
        DataSetName = 'frxDsRetido'
      end
      item
        DataSet = DmCiclos.frxDsTransfC
        DataSetName = 'frxDsTransfC'
      end
      item
        DataSet = DmCiclos.frxDsTransfD
        DataSetName = 'frxDsTransfD'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 219.212740000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDsCiclosCodigo: TfrxMemoView
          Left = 211.653680000000000000
          Top = 26.456710000000000000
          Width = 393.070817320000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CICLO DE CURSOS N'#186' [frxDsCiclos."Codigo"]')
          ParentFont = False
        end
        object frxDsCiclosNome: TfrxMemoView
          Left = 188.976500000000000000
          Top = 71.811070000000000000
          Width = 453.543297320000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."NOMEPRF"]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 37.795300000000000000
          Width = 170.078740160000000000
          Height = 68.031496060000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 211.653567720000000000
          Width = 393.070817320000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 75.590600000000000000
          Top = 71.811070000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Professor:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 75.590600000000000000
          Top = 90.708720000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Promotor:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 188.976500000000000000
          Top = 90.708720000000000000
          Width = 453.543297320000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."NOMEPRM"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 75.590600000000000000
          Top = 109.606370000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data da sa'#237'da:')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCiclosDATASAIDA_TXT: TfrxMemoView
          Left = 188.976500000000000000
          Top = 109.606370000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          DataField = 'DATASAIDA_TXT'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."DATASAIDA_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 75.590600000000000000
          Top = 128.504020000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data da chegada:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 188.976500000000000000
          Top = 128.504020000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."DATACHEGA_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 75.590600000000000000
          Top = 147.401670000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data do acerto:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Top = 147.401670000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."DATAACERT_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object frxDsCiclosAlunosInsc: TfrxMemoView
          Left = 177.637910000000000000
          Top = 196.535560000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'AlunosInsc'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."AlunosInsc"]')
          ParentFont = False
        end
        object frxDsCiclosAlunosAula: TfrxMemoView
          Left = 287.244280000000000000
          Top = 196.535560000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'AlunosAula'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."AlunosAula"]')
          ParentFont = False
        end
        object frxDsCiclosAlunosVal: TfrxMemoView
          Left = 468.661720000000000000
          Top = 196.535560000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita cursos:')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 75.590600000000000000
          Top = 196.535560000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Alunos inscritos:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 215.433210000000000000
          Top = 196.535560000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Presen'#231'as:')
          ParentFont = False
        end
        object frxDsCiclosAlunosVal1: TfrxMemoView
          Left = 566.929499999999900000
          Top = 196.535560000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."AlunosVal"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 75.590600000000000000
          Top = 177.637910000000000000
          Width = 566.929500000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CURSOS')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 325.039580000000000000
          Top = 196.535560000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Comiss'#227'o:')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 393.071120000000000000
          Top = 196.535560000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."ComisVal"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsLMovV
        DataSetName = 'frxDsLMovV'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLMovV."NomeGra"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 294.803340000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'LevaQtd'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."LevaQtd"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 362.834880000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'RETORNOU'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."RETORNOU"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 430.866420000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'VendQtd'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."VendQtd"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 566.929500000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'VendVal'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."VendVal"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 498.897960000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'CmisVal'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."CmisVal"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 1194.331480000000000000
        Width = 718.110700000000000000
        object Memo9: TfrxMemoView
          Top = 3.779530000000022000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795290240000000000
        Top = 294.803340000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLMovV."KGT"'
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Top = 22.677180000000020000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da mercadoria')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 294.803340000000000000
          Top = 22.677180000000020000
          Width = 68.031540000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 362.834880000000000000
          Top = 22.677180000000020000
          Width = 68.031540000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Retorno')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 430.866420000000000000
          Top = 22.677180000000020000
          Width = 68.031540000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Usado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677180000000020000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 498.897960000000000000
          Top = 22.677180000000020000
          Width = 68.031540000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comiss'#227'o')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'MERCADORIAS')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 385.512060000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."LevaQtd">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 362.834880000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."RETORNOU">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 430.866420000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."VendQtd">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."VendVal">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 498.897960000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."CmisVal">)]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795290240000000000
        Top = 616.063390000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsDespesas."KGT"'
        object Memo30: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DESPESAS')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 75.590600000000000000
          Top = 22.677180000000020000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta (do plano de contas)')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 574.488560000000000000
          Top = 22.677180000000020000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 672.756340000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsDespesas
        DataSetName = 'frxDsDespesas'
        RowCount = 0
        object Memo31: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          DataField = 'NOMECONTA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDespesas."NOMECONTA"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Debito'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."Debito"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 706.772110000000000000
        Width = 718.110700000000000000
        object Memo33: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DESPESAS  ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."Debito">)]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 427.086890000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo40: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 491.338900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE RECEITAS  ')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 566.929499999999900000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_RECEITAS"]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 468.661720000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsTransfC."KGT"'
        object Memo42: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TRANSFER'#202'NCIAS A CR'#201'DITO')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 151.181200000000000000
          Top = 22.677180000000020000
          Width = 423.307360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 574.488560000000000000
          Top = 22.677180000000020000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 75.590600000000000000
          Top = 22.677180000000020000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 525.354670000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsTransfC
        DataSetName = 'frxDsTransfC'
        RowCount = 0
        object Memo46: TfrxMemoView
          Left = 151.181200000000000000
          Width = 423.307360000000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = DmCiclos.frxDsTransfC
          DataSetName = 'frxDsTransfC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfC."Descricao"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfC
          DataSetName = 'frxDsTransfC'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTransfC."Credito"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = DmCiclos.frxDsTransfC
          DataSetName = 'frxDsTransfC'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfC."Data"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 559.370440000000000000
        Width = 718.110700000000000000
        object Memo49: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE TRANSFER'#202'NCIAS A CR'#201'DITO')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTransfC."Credito">)]'
            '')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE CR'#201'DITOS  ')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_CREDITOS"]')
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 748.346940000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo53: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'RECEITA L'#205'QUIDA  ')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 574.488560000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_RECE_LIQ"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 37.795290240000000000
        Top = 789.921770000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsTransfD."KGT"'
        object Memo55: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TRANSFER'#202'NCIAS A D'#201'BITO')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 151.181200000000000000
          Top = 22.677180000000020000
          Width = 423.307360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 574.488560000000000000
          Top = 22.677180000000020000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 75.590600000000000000
          Top = 22.677180000000020000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 846.614720000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsTransfD
        DataSetName = 'frxDsTransfD'
        RowCount = 0
        object Memo61: TfrxMemoView
          Left = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfD."Data"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 151.181200000000000000
          Width = 423.307360000000000000
          Height = 15.118110240000000000
          DataField = 'Descricao'
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfD."Descricao"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTransfD."Debito"]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 880.630490000000000000
        Width = 718.110700000000000000
        object Memo62: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE TRANSFER'#202'NCIAS A D'#201'BITO')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTransfD."Debito">)]')
          ParentFont = False
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Height = 120.944960000000000000
        Top = 922.205320000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo64: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          Memo.UTF8W = (
            'SALDO A RECEBER DO PROFESSOR DO CICLO:  ')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 574.488560000000000000
          Top = 3.779530000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_SALD_LIQ"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 75.590600000000000000
          Top = 22.677180000000020000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'COMISS'#213'ES DO PROFESSOR NO CICLO:  ')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 574.488560000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."ComisVal"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 75.590600000000000000
          Top = 41.574830000000020000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'SUB-TOTAL A PAGAR AO PROFESSOR NO CICLO:  ')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 574.488560000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."ProfVal"]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 75.590600000000000000
          Top = 60.472480000000010000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'PEND'#202'NCIAS (RETIRADO - DEVOLVIDO) DO PROFESSOR NO CICLO:  ')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 574.488560000000000000
          Top = 60.472480000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."EMPRES_SDOATU_NEG"]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370129999999900000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          Memo.UTF8W = (
            'SALDO PAGO AO PROFESSOR (SUB-TOTAL - PEND'#202'NCIAS):  ')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 574.488560000000000000
          Top = 79.370130000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."PAGOAPROF"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 75.590600000000000000
          Top = 98.267780000000010000
          Width = 498.897960000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'COMISS'#195'O RETIDA PELO PROFESSOR EM VIAGEM ([VAR_PRODCOMIS]):  ')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 574.488560000000000000
          Top = 98.267780000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRetido."Valor"]')
          ParentFont = False
        end
      end
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 1062.047930000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo59: TfrxMemoView
          Left = 75.590600000000000000
          Top = 15.118120000000090000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo anterior')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 272.126160000000000000
          Top = 15.118120000000090000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."SdoProfIni"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            'PEND'#202'NCIAS')
          ParentFont = False
        end
        object frxDsCiclosSdoProfFim: TfrxMemoView
          Left = 272.126160000000000000
          Top = 60.472479999999910000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."SdoProfFim"]')
          ParentFont = False
        end
        object frxDsCiclosPGT_PROFESSOR: TfrxMemoView
          Left = 272.126160000000000000
          Top = 30.236239999999960000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."EmprestD"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 75.590600000000000000
          Top = 30.236239999999960000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Retirado a mais neste ciclo')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 75.590600000000000000
          Top = 60.472479999999910000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo final do ciclo')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 377.953000000000000000
          Top = 56.692950000000110000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura professor')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 272.126160000000000000
          Top = 45.354360000000040000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."EmprestC"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 75.590600000000000000
          Top = 45.354360000000040000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Devolvido neste ciclo')
          ParentFont = False
        end
      end
    end
  end
  object frxDsCiclosIts: TfrxDBDataset
    UserName = 'frxDsCiclosIts'
    CloseDataSource = False
    DataSet = QrCiclosIts
    BCDToCurrency = False
    Left = 588
    Top = 8
  end
  object frxDsCiclos: TfrxDBDataset
    UserName = 'frxDsCiclos'
    CloseDataSource = False
    DataSet = QrCiclos
    BCDToCurrency = False
    Left = 504
    Top = 8
  end
  object PMLevados: TPopupMenu
    Left = 664
    Top = 436
    object Incluinovaconsignao1: TMenuItem
      Caption = '&Inclui / altera consigna'#231#227'o'
      OnClick = Incluinovaconsignao1Click
    end
    object Excluiconsignaodemercadoria1: TMenuItem
      Caption = '&Exclui consigna'#231#227'o'
      OnClick = Excluiconsignaodemercadoria1Click
    end
  end
  object PMDevolve: TPopupMenu
    Left = 664
    Top = 464
    object Devolvemercadoriasconsignadas1: TMenuItem
      Caption = '&Devolve mercadorias consignadas'
      OnClick = Devolvemercadoriasconsignadas1Click
    end
    object DesfazTODOprocessodeconsignao1: TMenuItem
      Caption = 'Desfaz &TODO processo de consigna'#231#227'o'
      OnClick = DesfazTODOprocessodeconsignao1Click
    end
  end
  object PMRateio: TPopupMenu
    OnPopup = PMRateioPopup
    Left = 925
    Top = 484
    object Rateiavalores1: TMenuItem
      Caption = '&Rateia valores'
      OnClick = Rateiavalores1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Incluireceitadecurso1: TMenuItem
      Caption = '&Inclui receita de curso'
      Enabled = False
      OnClick = Incluireceitadecurso1Click
    end
    object Alterareceitadecurso1: TMenuItem
      Caption = '&Altera receita de curso'
      Enabled = False
      OnClick = Alterareceitadecurso1Click
    end
    object Excluireceitadecurso1: TMenuItem
      Caption = '&Exclui receita de curso'
      OnClick = Excluireceitadecurso1Click
    end
  end
  object PMTransf: TPopupMenu
    OnPopup = PMTransfPopup
    Left = 845
    Top = 484
    object Incluitransferncia1: TMenuItem
      Caption = '&Inclui transfer'#234'ncia'
      OnClick = Incluitransferncia1Click
    end
    object Alteratransferncia1: TMenuItem
      Caption = '&Altera transfer'#234'ncia'
      OnClick = Alteratransferncia1Click
    end
    object Excluitransferncia1: TMenuItem
      Caption = '&Exclui transfer'#234'ncia'
      OnClick = Excluitransferncia1Click
    end
  end
  object PMEmpresta: TPopupMenu
    Left = 1012
    Top = 484
    object Incluiemprstimo1: TMenuItem
      Caption = '&Inclui empr'#233'stimo'
      object Emprestaafuncionrio1: TMenuItem
        Caption = '&Empresta ao professor (sai do caixa)'
        OnClick = Emprestaafuncionrio1Click
      end
      object Devoluodefuncionrio1: TMenuItem
        Caption = '&Devolu'#231#227'o de professor (entra no caixa)'
        OnClick = Devoluodefuncionrio1Click
      end
    end
    object Alteraemprstimoatual1: TMenuItem
      Caption = '&Altera empr'#233'stimo selecionado'
      OnClick = Alteraemprstimoatual1Click
    end
    object Excluiemprstimos1: TMenuItem
      Caption = '&Exclui empr'#233'stimo(s)'
      OnClick = Excluiemprstimos1Click
    end
  end
  object DsCiclosAula: TDataSource
    DataSet = QrCiclosAula
    Left = 672
    Top = 8
  end
  object QrCiclosAula: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCiclosAulaBeforeClose
    AfterScroll = QrCiclosAulaAfterScroll
    SQL.Strings = (
      'SELECT dtb.Nome MUNI, aul.* '
      'FROM ciclosaula aul'
      'LEFT JOIN dtb_munici dtb ON dtb.Codigo = aul.CodiCidade'
      'WHERE aul.Codigo=:P0'
      'ORDER BY aul.Data, aul.Controle')
    Left = 644
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCiclosAulaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ciclosaula.Codigo'
    end
    object QrCiclosAulaControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'ciclosaula.Controle'
    end
    object QrCiclosAulaData: TDateField
      FieldName = 'Data'
      Origin = 'ciclosaula.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCiclosAulaAlunosInsc: TIntegerField
      FieldName = 'AlunosInsc'
      Origin = 'ciclosaula.AlunosInsc'
    end
    object QrCiclosAulaAlunosAula: TIntegerField
      FieldName = 'AlunosAula'
      Origin = 'ciclosaula.AlunosAula'
    end
    object QrCiclosAulaValorUni: TFloatField
      FieldName = 'ValorUni'
      Origin = 'ciclosaula.ValorUni'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAulaValorTot: TFloatField
      FieldName = 'ValorTot'
      Origin = 'ciclosaula.ValorTot'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAulaCidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'ciclosaula.Cidade'
      Size = 50
    end
    object QrCiclosAulaUF: TWideStringField
      FieldName = 'UF'
      Origin = 'ciclosaula.UF'
      Size = 2
    end
    object QrCiclosAulaLocalEnd: TWideStringField
      FieldName = 'LocalEnd'
      Origin = 'ciclosaula.LocalEnd'
      Size = 100
    end
    object QrCiclosAulaLocalVal: TFloatField
      FieldName = 'LocalVal'
      Origin = 'ciclosaula.LocalVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAulaHotelNome: TWideStringField
      FieldName = 'HotelNome'
      Origin = 'ciclosaula.HotelNome'
      Size = 30
    end
    object QrCiclosAulaHotelCont: TWideStringField
      FieldName = 'HotelCont'
      Origin = 'ciclosaula.HotelCont'
    end
    object QrCiclosAulaHotelTele: TWideStringField
      FieldName = 'HotelTele'
      Origin = 'ciclosaula.HotelTele'
    end
    object QrCiclosAulaHotelVal: TFloatField
      FieldName = 'HotelVal'
      Origin = 'ciclosaula.HotelVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCiclosAulaOnibusNome: TWideStringField
      FieldName = 'OnibusNome'
      Origin = 'ciclosaula.OnibusNome'
    end
    object QrCiclosAulaOnibusHora: TWideStringField
      FieldName = 'OnibusHora'
      Origin = 'ciclosaula.OnibusHora'
      Size = 25
    end
    object QrCiclosAulaObserva: TWideStringField
      FieldName = 'Observa'
      Origin = 'ciclosaula.Observa'
      Size = 255
    end
    object QrCiclosAulaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ciclosaula.Lk'
    end
    object QrCiclosAulaDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ciclosaula.DataCad'
    end
    object QrCiclosAulaDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ciclosaula.DataAlt'
    end
    object QrCiclosAulaUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ciclosaula.UserCad'
    end
    object QrCiclosAulaUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ciclosaula.UserAlt'
    end
    object QrCiclosAulaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'ciclosaula.AlterWeb'
    end
    object QrCiclosAulaAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ciclosaula.Ativo'
    end
    object QrCiclosAulaLocalNom: TWideStringField
      FieldName = 'LocalNom'
      Origin = 'ciclosaula.LocalNom'
      Size = 100
    end
    object QrCiclosAulaLocalPes: TWideStringField
      FieldName = 'LocalPes'
      Origin = 'ciclosaula.LocalPes'
      Size = 30
    end
    object QrCiclosAulaLocalTel: TWideStringField
      FieldName = 'LocalTel'
      Origin = 'ciclosaula.LocalTel'
    end
    object QrCiclosAulaComPromPer: TFloatField
      FieldName = 'ComPromPer'
      Origin = 'ciclosaula.ComPromPer'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrCiclosAulaComPromVal: TFloatField
      FieldName = 'ComPromVal'
      Origin = 'ciclosaula.ComPromVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComPromMin: TFloatField
      FieldName = 'ComPromMin'
      Origin = 'ciclosaula.ComPromMin'
    end
    object QrCiclosAulaComProfPer: TFloatField
      FieldName = 'ComProfPer'
      Origin = 'ciclosaula.ComProfPer'
    end
    object QrCiclosAulaComProfVal: TFloatField
      FieldName = 'ComProfVal'
      Origin = 'ciclosaula.ComProfVal'
    end
    object QrCiclosAulaComProfMin: TFloatField
      FieldName = 'ComProfMin'
      Origin = 'ciclosaula.ComProfMin'
    end
    object QrCiclosAulaDTSPromPer: TFloatField
      FieldName = 'DTSPromPer'
      Origin = 'ciclosaula.DTSPromPer'
    end
    object QrCiclosAulaDTSPromVal: TFloatField
      FieldName = 'DTSPromVal'
      Origin = 'ciclosaula.DTSPromVal'
    end
    object QrCiclosAulaDTSPromMin: TFloatField
      FieldName = 'DTSPromMin'
      Origin = 'ciclosaula.DTSPromMin'
    end
    object QrCiclosAulaDTSProfPer: TFloatField
      FieldName = 'DTSProfPer'
      Origin = 'ciclosaula.DTSProfPer'
    end
    object QrCiclosAulaDTSProfVal: TFloatField
      FieldName = 'DTSProfVal'
      Origin = 'ciclosaula.DTSProfVal'
    end
    object QrCiclosAulaDTSProfMin: TFloatField
      FieldName = 'DTSProfMin'
      Origin = 'ciclosaula.DTSProfMin'
    end
    object QrCiclosAulaPromotor: TIntegerField
      FieldName = 'Promotor'
      Origin = 'ciclosaula.Promotor'
    end
    object QrCiclosAulaDTSPromDeb: TFloatField
      FieldName = 'DTSPromDeb'
      Origin = 'ciclosaula.DTSPromDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComPromDeb: TFloatField
      FieldName = 'ComPromDeb'
      Origin = 'ciclosaula.ComPromDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaDTSProfDeb: TFloatField
      FieldName = 'DTSProfDeb'
      Origin = 'ciclosaula.DTSProfDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfDeb: TFloatField
      FieldName = 'ComProfDeb'
      Origin = 'ciclosaula.ComProfDeb'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaBonPromVal: TFloatField
      FieldName = 'BonPromVal'
    end
    object QrCiclosAulaBonPromWho: TIntegerField
      FieldName = 'BonPromWho'
    end
    object QrCiclosAulaProfessor: TIntegerField
      FieldName = 'Professor'
    end
    object QrCiclosAulaComProfAlu: TFloatField
      FieldName = 'ComProfAlu'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfPrd: TFloatField
      FieldName = 'ComProfPrd'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfSaq: TFloatField
      FieldName = 'ComProfSaq'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfAll: TFloatField
      FieldName = 'ComProfAll'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCiclosAulaComProfPgt: TFloatField
      FieldName = 'ComProfPgt'
    end
    object QrCiclosAulaCodiCidade: TIntegerField
      FieldName = 'CodiCidade'
    end
    object QrCiclosAulaMUNI: TWideStringField
      FieldName = 'MUNI'
      Size = 100
    end
  end
  object PMDespesas: TPopupMenu
    Left = 748
    Top = 484
    object Promotor1: TMenuItem
      Caption = 'Promotor'
      object Incluidespesaspromotor1: TMenuItem
        Caption = '&Inclui despesa(s) promotor'
        OnClick = Incluidespesaspromotor1Click
      end
      object Alteradespesapromotor1: TMenuItem
        Caption = '&Altera despesa promotor'
        OnClick = Alteradespesapromotor1Click
      end
      object Excluidespesaspromotor1: TMenuItem
        Caption = '&Exclui despesa(s) promotor'
        OnClick = Excluidespesaspromotor1Click
      end
    end
    object Professor1: TMenuItem
      Caption = 'Professor'
      object Incluidespesasprofessor1: TMenuItem
        Caption = '&Inclui despesa(s) professor'
        OnClick = Incluidespesasprofessor1Click
      end
      object Alteradespesaprofessor1: TMenuItem
        Caption = '&Altera despesa professor'
        OnClick = Alteradespesaprofessor1Click
      end
      object Excluidespesasprofessor1: TMenuItem
        Caption = '&Exclui despesa(s) professor'
        OnClick = Excluidespesasprofessor1Click
      end
    end
  end
  object QrRetido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito-Debito) Valor'
      'FROM lanctos'
      'WHERE FatID=706'
      'AND FatNum=:P0')
    Left = 700
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRetidoValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsRetido: TDataSource
    DataSet = QrRetido
    Left = 728
    Top = 8
  end
  object frxDsRetido: TfrxDBDataset
    UserName = 'frxDsRetido'
    CloseDataSource = False
    DataSet = QrRetido
    BCDToCurrency = False
    Left = 756
    Top = 8
  end
  object frxCiclo_B: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39673.632371770800000000
    ReportOptions.LastChange = 39673.632371770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture2.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    OnGetValue = frxCiclo_AGetValue
    Left = 616
    Top = 36
    Datasets = <
      item
        DataSet = frxDsCiclos
        DataSetName = 'frxDsCiclos'
      end
      item
        DataSet = DmCiclos.frxDsDespesas
        DataSetName = 'frxDsDespesas'
      end
      item
        DataSet = DmCiclos.frxDsLMovV
        DataSetName = 'frxDsLMovV'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRetido
        DataSetName = 'frxDsRetido'
      end
      item
        DataSet = DmCiclos.frxDsTransfC
        DataSetName = 'frxDsTransfC'
      end
      item
        DataSet = DmCiclos.frxDsTransfD
        DataSetName = 'frxDsTransfD'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 188.976490240000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDsCiclosCodigo: TfrxMemoView
          Left = 211.653680000000000000
          Top = 26.456710000000000000
          Width = 393.070817320000000000
          Height = 18.897650000000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CICLO DE CURSOS N'#186' [frxDsCiclos."Codigo"]')
          ParentFont = False
        end
        object frxDsCiclosNome: TfrxMemoView
          Left = 188.976500000000000000
          Top = 71.811070000000000000
          Width = 453.543297320000000000
          Height = 15.118110236220500000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."NOMEPRF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture2: TfrxPictureView
          Left = 37.795300000000000000
          Width = 170.078740160000000000
          Height = 68.031496060000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 211.653567720000000000
          Width = 393.070817320000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 75.590600000000000000
          Top = 71.811070000000000000
          Width = 113.385900000000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Professor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Promotor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 188.976500000000000000
          Top = 86.929190000000000000
          Width = 453.543297320000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."NOMEPRM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 75.590600000000000000
          Top = 102.047310000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data da sa'#237'da:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsCiclosDATASAIDA_TXT: TfrxMemoView
          Left = 188.976500000000000000
          Top = 102.047310000000000000
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataField = 'DATASAIDA_TXT'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."DATASAIDA_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 75.590600000000000000
          Top = 117.165430000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data da chegada:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 188.976500000000000000
          Top = 117.165430000000000000
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."DATACHEGA_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 75.590600000000000000
          Top = 132.283550000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data do acerto:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Top = 132.283550000000000000
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCiclos."DATAACERT_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object frxDsCiclosAlunosInsc: TfrxMemoView
          Left = 177.637910000000000000
          Top = 173.858380000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'AlunosInsc'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."AlunosInsc"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsCiclosAlunosAula: TfrxMemoView
          Left = 287.244280000000000000
          Top = 173.858380000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'AlunosAula'
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."AlunosAula"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsCiclosAlunosVal: TfrxMemoView
          Left = 468.661720000000000000
          Top = 173.858380000000000000
          Width = 98.267780000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Receita cursos:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 75.590600000000000000
          Top = 173.858380000000000000
          Width = 102.047310000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Alunos inscritos:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 215.433210000000000000
          Top = 173.858380000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Presen'#231'as:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsCiclosAlunosVal1: TfrxMemoView
          Left = 566.929500000000000000
          Top = 173.858380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."AlunosVal"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 75.590600000000000000
          Top = 158.740260000000000000
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CURSOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 325.039580000000000000
          Top = 173.858380000000000000
          Width = 68.031540000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Comiss'#227'o:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 393.071120000000000000
          Top = 173.858380000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCiclos."ComisVal"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 325.039580000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsLMovV
        DataSetName = 'frxDsLMovV'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Width = 219.212740000000000000
          Height = 13.228346456692900000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLMovV."NomeGra"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 294.803340000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataField = 'LevaQtd'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."LevaQtd"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 362.834880000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataField = 'RETORNOU'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."RETORNOU"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 430.866420000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataField = 'VendQtd'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."VendQtd"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 566.929500000000000000
          Width = 75.590551180000000000
          Height = 13.228346456692900000
          DataField = 'VendVal'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."VendVal"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 498.897960000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataField = 'CmisVal'
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLMovV."CmisVal"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 1167.874770000000000000
        Width = 718.110700000000000000
        object Memo9: TfrxMemoView
          Top = 3.779530000000022000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125996460000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsLMovV."KGT"'
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 219.212740000000000000
          Height = 13.228346460000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da mercadoria')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 294.803340000000000000
          Top = 18.897650000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sa'#237'da')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 362.834880000000000000
          Top = 18.897650000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Retorno')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 430.866420000000000000
          Top = 18.897650000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Usado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 498.897960000000000000
          Top = 18.897650000000000000
          Width = 68.031540000000000000
          Height = 13.228346460000000000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Comiss'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'MERCADORIAS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 362.834880000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 219.212740000000000000
          Height = 13.228346456692900000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 294.803340000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."LevaQtd">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 362.834880000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataSet = frxDsCiclosIts
          DataSetName = 'frxDsCiclosIts'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."RETORNOU">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 430.866420000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."VendQtd">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 13.228346456692900000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."VendVal">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 498.897960000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataSet = DmCiclos.frxDsLMovV
          DataSetName = 'frxDsLMovV'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLMovV."CmisVal">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125996460000000000
        Top = 604.724800000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsDespesas."KGT"'
        object Memo30: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DESPESAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 661.417750000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsDespesas
        DataSetName = 'frxDsDespesas'
        RowCount = 0
        object Memo31: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 13.228346456692900000
          DataField = 'NOMECONTA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDespesas."NOMECONTA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataField = 'Debito'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDespesas."Debito"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 699.213050000000000000
        Width = 718.110700000000000000
        object Memo33: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DESPESAS  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsDespesas."Debito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 20.787401574803100000
        Top = 404.409710000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo40: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 491.338900000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE RECEITAS  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 75.590551180000000000
          Height = 13.228346456692900000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_RECEITAS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125996460000000000
        Top = 449.764070000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsTransfC."KGT"'
        object Memo42: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TRANSFER'#202'NCIAS A CR'#201'DITO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 423.307360000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 506.457020000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsTransfC
        DataSetName = 'frxDsTransfC'
        RowCount = 0
        object Memo46: TfrxMemoView
          Left = 151.181200000000000000
          Width = 423.307360000000000000
          Height = 13.228346460000000000
          DataField = 'Descricao'
          DataSet = DmCiclos.frxDsTransfC
          DataSetName = 'frxDsTransfC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfC."Descricao"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = DmCiclos.frxDsTransfC
          DataSetName = 'frxDsTransfC'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTransfC."Credito"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          DataField = 'Data'
          DataSet = DmCiclos.frxDsTransfC
          DataSetName = 'frxDsTransfC'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfC."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 35.905511811023600000
        Top = 544.252320000000100000
        Width = 718.110700000000000000
        object Memo49: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE TRANSFER'#202'NCIAS A CR'#201'DITO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTransfC."Credito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE CR'#201'DITOS  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_CREDITOS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        Height = 20.787401574803100000
        Top = 740.787880000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo53: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 498.897960000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            'RECEITA L'#205'QUIDA  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 574.488560000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_RECE_LIQ"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125996460000000000
        Top = 786.142240000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsTransfD."KGT"'
        object Memo55: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 566.929500000000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TRANSFER'#202'NCIAS A D'#201'BITO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 423.307360000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 75.590600000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 842.835190000000000000
        Width = 718.110700000000000000
        DataSet = DmCiclos.frxDsTransfD
        DataSetName = 'frxDsTransfD'
        RowCount = 0
        object Memo61: TfrxMemoView
          Left = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 13.228346456692900000
          DataField = 'Data'
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfD."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 151.181200000000000000
          Width = 423.307360000000000000
          Height = 13.228346456692900000
          DataField = 'Descricao'
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTransfD."Descricao"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTransfD."Debito"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 880.630490000000000000
        Width = 718.110700000000000000
        object Memo62: TfrxMemoView
          Left = 75.590600000000000000
          Width = 498.897960000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE TRANSFER'#202'NCIAS A D'#201'BITO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 574.488560000000000000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTransfD."Debito">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 922.205320000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo64: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000022000
          Width = 498.897960000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          Memo.UTF8W = (
            'SALDO A RECEBER DO PROFESSOR DO CICLO:  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 574.488560000000000000
          Top = 3.779530000000022000
          Width = 68.031496060000000000
          Height = 13.228346456692900000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."TOT_SALD_LIQ"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 75.590600000000000000
          Top = 17.007874015748030000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'COMISS'#213'ES DO PROFESSOR NO CICLO:  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 574.488560000000000000
          Top = 17.007874015748030000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."ComisVal"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 75.590600000000000000
          Top = 30.236239999999960000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          Memo.UTF8W = (
            'SUB-TOTAL A PAGAR AO PROFESSOR NO CICLO:  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 574.488560000000000000
          Top = 30.236239999999960000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."ProfVal"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 75.590600000000000000
          Top = 43.464566929133870000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'PEND'#202'NCIAS (RETIRADO - DEVOLVIDO) DO PROFESSOR NO CICLO:  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 574.488560000000000000
          Top = 43.464566929133870000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."EMPRES_SDOATU_NEG"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 75.590600000000000000
          Top = 56.692949999999990000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          Memo.UTF8W = (
            'SALDO PAGO AO PROFESSOR (SUB-TOTAL - PEND'#202'NCIAS):  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 574.488560000000000000
          Top = 56.692949999999990000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clNavy
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."PAGOAPROF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 75.590600000000000000
          Top = 69.921259842519710000
          Width = 498.897960000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'COMISS'#195'O RETIDA PELO PROFESSOR EM VIAGEM ([VAR_PRODCOMIS]):  ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 574.488560000000000000
          Top = 69.921259842519710000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRetido."Valor"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 1031.811690000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo59: TfrxMemoView
          Left = 75.590600000000000000
          Top = 13.228346456692860000
          Width = 196.535560000000000000
          Height = 13.228346460000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo anterior')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 272.126160000000000000
          Top = 13.228346456692860000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."SdoProfIni"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.566929130000000000
          Height = 13.228346456692920000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clGray
          HAlign = haCenter
          Memo.UTF8W = (
            'PEND'#202'NCIAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsCiclosSdoProfFim: TfrxMemoView
          Left = 272.126160000000000000
          Top = 52.913420000000090000
          Width = 68.031540000000010000
          Height = 13.228346460000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."SdoProfFim"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsCiclosPGT_PROFESSOR: TfrxMemoView
          Left = 272.126160000000000000
          Top = 26.456709999999930000
          Width = 68.031540000000010000
          Height = 13.228346460000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."EmprestD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 75.590600000000000000
          Top = 26.456709999999930000
          Width = 196.535560000000000000
          Height = 13.228346460000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Retirado a mais neste ciclo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 75.590600000000000000
          Top = 52.913420000000090000
          Width = 196.535560000000000000
          Height = 13.228346460000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo final do ciclo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 377.953000000000000000
          Top = 52.913420000000090000
          Width = 264.567100000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura professor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 272.126160000000000000
          Top = 39.685039370078810000
          Width = 68.031540000000010000
          Height = 13.228346460000000000
          DataSet = frxDsCiclos
          DataSetName = 'frxDsCiclos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCiclos."EmprestC"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 75.590600000000000000
          Top = 39.685039370078810000
          Width = 196.535560000000000000
          Height = 13.228346460000000000
          DataSet = DmCiclos.frxDsTransfD
          DataSetName = 'frxDsTransfD'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Devolvido neste ciclo')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 8
    Top = 20
    object FonteGrande1: TMenuItem
      Caption = 'Fonte &Grande'
      OnClick = FonteGrande1Click
    end
    object FonteMdia1: TMenuItem
      Caption = 'Fonte &M'#233'dia'
      OnClick = FonteMdia1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Rateio1: TMenuItem
      Caption = '&Rateio'
      OnClick = Rateio1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
