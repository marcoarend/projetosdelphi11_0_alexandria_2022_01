unit GradesCodBarra;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, DB, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, Mask, frxClass, frxDBSet,
  frxBarcod, frxBarcode, dmkPermissoes, dmkGeral, Variants;

type
  TFmGradesCodBarra = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsTam: TDataSource;
    QrTam: TmySQLQuery;
    QrTamCodigo: TIntegerField;
    QrTamNome: TWideStringField;
    DsCor: TDataSource;
    QrCor: TmySQLQuery;
    QrCorCodigo: TIntegerField;
    QrCorNome: TWideStringField;
    DsGra: TDataSource;
    QrGra: TmySQLQuery;
    QrGraCodigo: TIntegerField;
    QrGraNome: TWideStringField;
    Label1: TLabel;
    EdGra: TdmkEditCB;
    CBGra: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCor: TdmkEditCB;
    CBCor: TdmkDBLookupComboBox;
    EdTam: TdmkEditCB;
    Label3: TLabel;
    CBTam: TdmkDBLookupComboBox;
    QrPage: TmySQLQuery;
    QrPageNome: TWideStringField;
    QrPageObserva: TWideMemoField;
    QrPageCodigo: TIntegerField;
    QrPagePagCol: TIntegerField;
    QrPagePagLin: TIntegerField;
    QrPagePagLef: TIntegerField;
    QrPagePagTop: TIntegerField;
    QrPagePagWid: TIntegerField;
    QrPagePagHei: TIntegerField;
    QrPagePagGap: TIntegerField;
    QrPageBanCol: TIntegerField;
    QrPageBanLin: TIntegerField;
    QrPageBanWid: TIntegerField;
    QrPageBanHei: TIntegerField;
    QrPageBanLef: TIntegerField;
    QrPageBanTop: TIntegerField;
    QrPageBanGap: TIntegerField;
    QrPageOrient: TIntegerField;
    QrPageDataCad: TDateField;
    QrPageDataAlt: TDateField;
    QrPageUserCad: TSmallintField;
    QrPageUserAlt: TSmallintField;
    QrPageLk: TIntegerField;
    QrPageColWid: TIntegerField;
    QrPageBa2Hei: TIntegerField;
    QrPageFoSize: TSmallintField;
    QrPageMemHei: TIntegerField;
    QrPageEspaco: TSmallintField;
    DsPage: TDataSource;
    Label4: TLabel;
    EdConfImp: TdmkEditCB;
    CBConfImp: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    EdEspacamento: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdColIni: TdmkEdit;
    EdLinIni: TdmkEdit;
    Label7: TLabel;
    LaRepeticoes: TLabel;
    EdRepeticoes: TdmkEdit;
    Progress: TProgressBar;
    QrMinhaEtiq: TmySQLQuery;
    QrMinhaEtiqNome: TWideStringField;
    QrMinhaEtiqConta: TIntegerField;
    QrMinhaEtiqTexto1: TWideStringField;
    QrMinhaEtiqTexto2: TWideStringField;
    QrMinhaEtiqTexto3: TWideStringField;
    QrMinhaEtiqTexto4: TWideStringField;
    CkInfoCod: TCheckBox;
    QrProduto: TmySQLQuery;
    QrProdutoControle: TIntegerField;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DsProduto: TDataSource;
    frxEtiquetas: TfrxReport;
    frxDsMinhaEtiq: TfrxDBDataset;
    CkGrade: TCheckBox;
    CkDesign: TCheckBox;
    frxBarCodeObject1: TfrxBarCodeObject;
    QrProdutoNOMEGRA: TWideStringField;
    QrProdutoNOMECOR: TWideStringField;
    QrProdutoNOMETAM: TWideStringField;
    QrProdutoNOMEPROD: TWideStringField;
    QrProdutoEAN13: TWideStringField;
    DBEdit2: TDBEdit;
    QrPageBarLef: TIntegerField;
    QrPageBarTop: TIntegerField;
    QrPageBarWid: TIntegerField;
    QrPageBarHei: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    Label9: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    QrGraGruVal: TmySQLQuery;
    QrGraGruValCor: TIntegerField;
    QrGraGruValTam: TIntegerField;
    QrGraGruValCustoPreco: TFloatField;
    QrProdutoCustoPreco: TFloatField;
    EdCustoPreco: TdmkEdit;
    Label10: TLabel;
    RGDescricao: TRadioGroup;
    QrGraCodUsu: TIntegerField;
    EdMunici: TdmkEdit;
    LaMunici: TLabel;
    EdUF: TdmkEdit;
    LaUF: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    LaEntidade: TLabel;
    EdEntidade: TdmkEditCB;
    SpeedButton2: TSpeedButton;
    QrEntidade: TmySQLQuery;
    DsEntidade: TDataSource;
    QrEntidadeCodigo: TIntegerField;
    QrEntidadeNOMEENTI: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraChange(Sender: TObject);
    procedure EdCorChange(Sender: TObject);
    procedure EdTamChange(Sender: TObject);
    procedure frxEtiquetasGetValue(const VarName: string; var Value: Variant);
    procedure QrProdutoCalcFields(DataSet: TDataSet);
    procedure QrProdutoAfterScroll(DataSet: TDataSet);
    procedure EdGraCusPrcChange(Sender: TObject);
    procedure RGDescricaoClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    FMinhaEtiq: String;
    FContaCol,
    Imp_LinhasEtiqHei, Imp_LinhasEtiq: Integer;
    Imp_Espacos: String;
  {
  Imp_Espacos: String;
  Imp_LinhasEtiq, Imp_LinhasEtiqHei, FContaCol: Integer;
  pOrient: TPrinterOrientation;
  }
    //procedure ReopenMinhaEtqIts;
    procedure ImprimeEtiqueta;
    procedure ReopenProduto;
    procedure AjustaComponentes(Tipo: Integer);
    function AdicionaEspacosEmBranco(TamTot, Tam: Integer): String;
  public
    { Public declarations }
  end;

  var
  FmGradesCodBarra: TFmGradesCodBarra;

implementation


uses Principal, UnInternalConsts, Module, UCreate, ModuleGeral, UnMyObjects;

{$R *.DFM}

function TFmGradesCodBarra.AdicionaEspacosEmBranco(TamTot, Tam: Integer): String;
var
  i: Integer;
  Texto: String;
begin
  Texto := '';
  Tam   := TamTot - Tam;
  if Tam >  0 then
  begin
    for i := 0 to Tam do
    begin
      Texto := Texto + ' ';
    end;
  end;
  Result := Texto;
end;

procedure TFmGradesCodBarra.AjustaComponentes(Tipo: Integer);
begin
  if RGDescricao.ItemIndex = 3 then
  begin
    LaEntidade.Visible   := True;
    EdEntidade.Visible   := True;
    CBEntidade.Visible   := True;
    SpeedButton2.Visible := True;
    LaMunici.Visible     := True;
    LaUF.Visible         := True;
    EdMunici.Visible     := True;
    EdUF.Visible         := True;
    LaEntidade.Top       := 135;
    LaMunici.Top         := 135;
    LaUF.Top             := 135;
    EdEntidade.Top       := 152;
    CBEntidade.Top       := 152;
    SpeedButton2.Top     := 152;
    EdMunici.Top         := 152;
    EdUF.Top             := 152;
    Progress.Top         := 188;
    Label8.Top           := 208;
    Label10.Top          := 208;
    DBEdit1.Top          := 224;
    DBEdit2.Top          := 224;
    EdCustoPreco.Top     := 224;
  end else
  begin
    LaEntidade.Visible      := False;
    EdEntidade.Visible      := False;
    CBEntidade.Visible      := False;
    SpeedButton2.Visible    := False;
    LaMunici.Visible        := False;
    LaUF.Visible            := False;
    EdMunici.Visible        := False;
    EdUF.Visible            := False;
    EdEntidade.ValueVariant := 0;
    CBEntidade.KeyValue     := Null;
    EdMunici.ValueVariant   := '';
    EdUF.ValueVariant       := '';
    LaEntidade.Top          := 208;
    LaMunici.Top            := 208;
    LaUF.Top                := 208;
    EdEntidade.Top          := 224;
    CBEntidade.Top          := 224;
    SpeedButton2.Top        := 224; 
    SpeedButton2.Top        := 224;
    EdMunici.Top            := 224;
    EdUF.Top                := 224;
    Progress.Top            := 152;
    Label8.Top              := 188;
    Label10.Top             := 188;
    DBEdit1.Top             := 204;
    DBEdit2.Top             := 204;
    EdCustoPreco.Top        := 204;
  end;
end;

procedure TFmGradesCodBarra.BtOKClick(Sender: TObject);
var
  K, I,Repet, Conta, Col, Lin, MaxCol, ConfImp: Integer;
  Nome, Texto1, Texto2, Preco: String;
  CustoPreco: Double;
begin
  Imp_LinhasEtiqHei := 1;
  Imp_Espacos := Geral.CompletaString(
  ' ', ' ', Geral.IMV(EdEspacamento.Text), taRightJustify, True);
  Imp_LinhasEtiq := 1;//QrMultiEtqLinhas.Value;
  //
  Progress.Update;
  // Etiquetas (endereco ou diversos)
  ConfImp := Geral.IMV(EdConfImp.Text);
  if ConfImp = 0 then
  begin
    Application.MessageBox('Defina a "Configura��o de impress�o"!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    EdConfImp.SetFocus;
    Exit;
  end;
  MaxCol := QrPageBanCol.Value;
  Col := Geral.IMV(EdColIni.Text) - 1;
  if (Col >= MaxCol) {and (RGConsulta.ItemIndex > 0)} then
  begin
    Application.MessageBox('Coluna inicial fora dos par�metros!', 'Erro',
    MB_OK+MB_ICONERROR);
    EdColIni.SetFocus;
    Exit;
  end;
  CustoPreco := EdCustoPreco.ValueVariant;
  if (Geral.IMV(EdGraCusPrc.Text) <> 0) and (CustoPreco = 0) then
  begin
    Geral.MB_Aviso('Produto n�o possui pre�o v�lido para a lista selecionada!');
    EdGraCusPrc.SetFocus;
    Exit;
  end;
  Lin := Geral.IMV(EdLinIni.Text) - 1;
  Conta := (Lin * QrPageBanCol.Value) + Col;
  Repet := Geral.IMV(EdRepeticoes.Text);
  FMinhaEtiq := UCriar.RecriaTempTable('minhaetiq', DmodG.QrUpdPID1, False);
  ////
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FMinhaEtiq + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Nome=:P0, Conta=:P1, Texto1=:P2, Texto2=:P3 ');
  Progress.Position := 0;
  Progress.Max := Conta;
  for I := 1 to Conta do
  begin
    Progress.Position := Progress.Position + 1;
    DmodG.QrUpdPID1.Params[00].AsString  := CO_VAZIO;
    DmodG.QrUpdPID1.Params[01].AsInteger := Conta;
    DmodG.QrUpdPID1.Params[02].AsString  := CO_VAZIO;
    DmodG.QrUpdPID1.Params[03].AsString  := CO_VAZIO;
    DmodG.QrUpdPID1.ExecSQL;
  end;
  Texto1 := QrProdutoEAN13.Value;
  if EdGraCusPrc.ValueVariant > 0 then
    Preco := 'Pre�o: ' + Dmod.QrControleMoeda.Value + ' ' + EdCustoPreco.Text
  else
    Preco := '';
  for K := 1 to Repet do
  begin
    Conta := Conta + 1;
    Progress.Position := Progress.Position + 1;
    Texto2 := Preco;
    case RGDescricao.ItemIndex of
      0:
      begin
        Nome   := Preco;
        Texto2 := '';
      end;
      1: Nome := 'Ref. ' + EdGra.Text;
      2: Nome := QrProdutoNOMEPROD.Value;
      3: Nome := EdMunici.ValueVariant + ' - ' + EdUF.ValueVariant +
                   AdicionaEspacosEmBranco(13, Length(EdMunici.ValueVariant) + 5) +
                   AdicionaEspacosEmBranco(5, Length(IntToStr(EdEntidade.ValueVariant))) +
                   IntToStr(EdEntidade.ValueVariant);
    end;
    DmodG.QrUpdPID1.Params[00].AsString  := Nome;
    DmodG.QrUpdPID1.Params[01].AsInteger := Conta;
    DmodG.QrUpdPID1.Params[02].AsString  := Texto1;
    DmodG.QrUpdPID1.Params[03].AsString  := Texto2;
    DmodG.QrUpdPID1.ExecSQL;
  end;
  Progress.Max := Progress.Max * 4{Linhas};
  Progress.Position := 0;
  //EtiquetasDiversas;
  QrMinhaEtiq.Close;
  QrMinhaEtiq.Database := DModG.MyPID_DB;
  QrMinhaEtiq.Open;
  ImprimeEtiqueta;
  Progress.Position := 0;
end;

procedure TFmGradesCodBarra.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGradesCodBarra.EdCorChange(Sender: TObject);
begin
  ReopenProduto;
end;

procedure TFmGradesCodBarra.EdGraChange(Sender: TObject);
begin
  ReopenProduto;
end;

procedure TFmGradesCodBarra.EdGraCusPrcChange(Sender: TObject);
begin
  ReopenProduto;
end;

procedure TFmGradesCodBarra.EdTamChange(Sender: TObject);
begin
  ReopenProduto;
end;

procedure TFmGradesCodBarra.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmGradesCodBarra.FormCreate(Sender: TObject);
begin
  QrGra.Open;
  QrCor.Open;
  QrTam.Open;
  QrPage.Open;
  QrGraCusPrc.Open;
  QrEntidade.Open;
  if QrPage.RecordCount = 1 then
  begin
    EdConfimp.ValueVariant := QrPageCodigo.Value;
    CBConfimp.KeyValue     := QrPageCodigo.Value;
  end;
  AjustaComponentes(0);
end;

procedure TFmGradesCodBarra.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGradesCodBarra.frxEtiquetasGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_BARCODE_VISI' then
    Value := QrMinhaEtiqTexto1.Value <> ''
  else
  if VarName = 'VARF_BARCODE_CODI' then
    Value := Imp_Espacos + QrMinhaEtiqTexto1.Value
  else
  if VarName = 'VARF_BARCODE_VALR' then
    Value := Imp_Espacos + QrMinhaEtiqTexto2.Value
  else
  if VarName = 'VARF_BARCODE_NOME' then
  begin
    //if RGDescricao.ItemIndex <> 0 then
      Value := Imp_Espacos + QrMinhaEtiqNome.Value
    //else
      //Value := QrMinhaEtiqTexto1.Value
  end else
  if VarName = 'VARF_BARCODE_BARC' then
    Value := QrMinhaEtiqTexto1.Value
end;

procedure TFmGradesCodBarra.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CriaPages;
  QrPage.Close;
  QrPage.Open;
  EdConfimp.Text := IntToStr(VAR_CONFIPAGE);
  CBConfimp.KeyValue := VAR_CONFIPAGE;
end;

procedure TFmGradesCodBarra.SpeedButton2Click(Sender: TObject);
begin
  FmPrincipal.CadastroEntidades;
  QrEntidade.Close;
  QrEntidade.Open;
  EdEntidade.ValueVariant := VAR_CADASTRO;
  CBEntidade.KeyValue     := VAR_CADASTRO;
end;

procedure TFmGradesCodBarra.ImprimeEtiqueta;
var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  BARLEF, BARTOP, (*BARWID,*) BARHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  BarCode: TfrxBarCodeView;
  //Align, i,
  L, T, W, H: Integer;
  Fator: Double;
begin
  // Configura p�gina
  while frxEtiquetas.PagesCount > 0 do
    frxEtiquetas.Pages[0].Free;
  Page := TfrxReportPage.Create(frxEtiquetas);
  Page.CreateUniqueName;
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  Page.Height       := Trunc(QrPagePagHei.Value / VAR_DOTIMP);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  H := Trunc(QrPagePagTop.Value / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  // Configura Banda
  BANLEF := Trunc(QrPageBanLef.Value / VAR_DOTIMP);
  BANTOP := Trunc(QrPageBanTop.Value / VAR_DOTIMP);
  BANWID := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  BANHEI := Trunc(QrPageBanHei.Value / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  //BA2HEI := Trunc(QrPageBa2Hei.Value / VAR_DOTIMP / (QrPageEspaco.Value + 1));
  //for i := 1 to 5 do
  //begin
    // Configura memo da Linha 1

  // Left:
  // Left:
  Fator := Trunc(FContaCol / Imp_LinhasEtiqHei);
  MEMLEF := Trunc((QrPageBanLef.Value / VAR_DOTIMP) + (Fator *
     ((QrPageColWid.Value / VAR_DOTIMP)+(QrPageBanGap.Value / VAR_DOTIMP))));
  if FContaCol >= (QrPageBanCol.Value * Imp_LinhasEtiqHei) then FContaCol := 0;
  BARLEF := MEMLEF + Trunc(QrPageBarLef.Value / VAR_DOTIMP);
  FContaCol := FContaCol + 1;
  if FContaCol >= (QrPageBanCol.Value * Imp_LinhasEtiqHei) then FContaCol := 0;

  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  BARHEI := Trunc(QrPageBarHei.Value / VAR_DOTIMP);

  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  BARTOP := Trunc(QrPageBarTop.Value / VAR_DOTIMP);

  // Width := QrPageColWid.Value / VAR_DOTIMP;
  //BARWID := Trunc(QrPageBarWid.Value / VAR_DOTIMP);
  //
  BarCode               := TfrxBarCodeView.Create(Band);
  BarCode.Name          := 'BarCode';
  BarCode.Visible       := True;
  BarCode.OnBeforePrint := 'BarCodeOnBeforePrint';
  BarCode.Left          := BARLEF;
  BarCode.Top           := BARTOP;
  BarCode.Height        := BARHEI;
  BarCode.Font.Size     := QrPageFoSize.Value;
  BarCode.BarType       := bcCodeEan13;
  BarCode.Expression    := '<VARF_BARCODE_BARC>';
  if CkGrade.Checked then
    BarCode.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //



  // MEMO 1 - Nome do produto
  //
  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  MEMHEI := Trunc(QrPageMemHei.Value / VAR_DOTIMP);
  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  MEMTOP := BARTOP + BARHEI;//Trunc(((i-1) * QrPageMemHei.Value) / VAR_DOTIMP);
  // Width := QrPageColWid.Value / VAR_DOTIMP;
  MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  //
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  //Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Name   := 'Courier New';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_BARCODE_NOME]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //



  // MEMO 2 - Pre�o do produto
  //
  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  MEMHEI := Trunc(QrPageMemHei.Value / VAR_DOTIMP);
  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  MEMTOP := MEMTOP + MEMHEI;//Trunc(((i-1) * QrPageMemHei.Value) / VAR_DOTIMP);
  // Width := QrPageColWid.Value / VAR_DOTIMP;
  MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  //
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  //Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Name   := 'Courier New';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_BARCODE_VALR]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //



  //////////////////////////////////////////////////////////////////////////////
  if CkDesign.Checked then
    frxEtiquetas.DesignReport
  else
    MyObjects.frxMostra(frxEtiquetas, 'Etiquetas de C�digo de Barras');
end;

procedure TFmGradesCodBarra.QrProdutoAfterScroll(DataSet: TDataSet);
begin
  EdCustoPreco.ValueVariant := QrProdutoCustoPreco.Value;
end;

procedure TFmGradesCodBarra.QrProdutoCalcFields(DataSet: TDataSet);
var
  EAN13: String;
begin
  QrProdutoNOMEPROD.Value :=
  QrProdutoNOMEGRA.Value + ' ' +
  QrProdutoNOMECOR.Value + ' ' +
  QrProdutoNOMETAM.Value;
  //
  EAN13 := FormatFloat('000000000000', QrProdutoControle.Value);
  QrProdutoEAN13.Value := EAN13 + Geral.ModuloEAN13(EAN13);
end;

procedure TFmGradesCodBarra.ReopenProduto;
var
  Gra, Cor, Tam, GCP: Integer;
begin
  Gra := Geral.IMV(EdGra.Text);
  if Gra <> 0 then
    Gra := QrGraCodigo.Value;
  Cor := Geral.IMV(EdCor.Text);
  Tam := Geral.IMV(EdTam.Text);
  GCP := Geral.IMV(EdGraCusPrc.Text);
  //
  QrProduto.Close;
  QrProduto.Params[00].AsInteger := GCP;
  QrProduto.Params[01].AsInteger := Gra;
  QrProduto.Params[02].AsInteger := Cor;
  QrProduto.Params[03].AsInteger := Tam;
  QrProduto.Open;
  //
end;


procedure TFmGradesCodBarra.RGDescricaoClick(Sender: TObject);
begin
  AjustaComponentes(RGDescricao.ItemIndex);
end;

end.


