unit VendaConf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, UnFinanceiro, frxClass, frxDBSet, dmkGeral, UnDmkEnums;

type
  TFmVendaConf = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    QrFutNeg: TmySQLQuery;
    QrFutNegConta: TIntegerField;
    QrFutNegGrade: TIntegerField;
    QrFutNegCor: TIntegerField;
    QrFutNegTam: TIntegerField;
    QrFutNegQtd: TFloatField;
    QrFutNegVal: TFloatField;
    QrFutNegEstqQ: TFloatField;
    QrFutNegEstqV: TFloatField;
    QrFutNegFutQtd: TFloatField;
    QrFutNegFutVal: TFloatField;
    QrFutNegNOMEGRADE: TWideStringField;
    QrFutNegNOMECOR: TWideStringField;
    QrFutNegNOMETAM: TWideStringField;
    frxDsFutNeg: TfrxDBDataset;
    frxFutNeg: TfrxReport;
    QrMovim2: TmySQLQuery;
    QrMovim2Grade: TIntegerField;
    QrMovim2Cor: TIntegerField;
    QrMovim2Tam: TIntegerField;
    QrMovim2Val: TFloatField;
    QrMovim2Qtd: TFloatField;
    QrMovim2Conta: TIntegerField;
    QrLoc: TmySQLQuery;
    PBVenda: TProgressBar;
    QrLocCodigo: TIntegerField;
    QrLocControle: TIntegerField;
    QrLocDataReal: TDateField;
    MCData: TMonthCalendar;
    Panel3: TPanel;
    Label69: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteira: TmySQLQuery;
    IntegerField12: TIntegerField;
    StringField12: TWideStringField;
    DsCarteira: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxFutNegGetValue(const VarName: string; var Value: Variant);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure AlteraDescricao(Lancto, Tipo: Integer; DescriDmod, DescriLcto: String);
    procedure QuitaItemLanctoAutomatico(Data: TDateTime);
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmVendaConf: TFmVendaConf;

implementation

{$R *.DFM}

uses Module, MyListas, ModuleGeral, ModuleFin, ModuleProd, MoviV, UnMyObjects;

procedure TFmVendaConf.AlteraDescricao(Lancto, Tipo: Integer; DescriDmod, DescriLcto: String);
var
  Descri, NDescri: String;
  Tam: Integer;
begin
  NDescri := DescriDmod;
  Descri := Trim(DescriLcto);
  Tam     := Length(NDescri);
  //
  case Tipo of
    0:
    begin
      if Copy(Descri, 1, Tam) <> NDescri then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lanctos SET Descricao=:P0 WHERE Controle=:P1');
        Dmod.QrUpd.Params[0].AsString  := NDescri + ' ' + Descri;
        Dmod.QrUpd.Params[1].AsInteger :=  Lancto;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
    1:
    begin
      if Copy(Descri, 1, Tam) = NDescri then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lanctos SET Descricao=:P0 WHERE Controle=:P1');
        Dmod.QrUpd.Params[0].AsString  := Copy(Descri, Tam + 2);
        Dmod.QrUpd.Params[1].AsInteger := Lancto;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
  end;
end;

procedure TFmVendaConf.BtOKClick(Sender: TObject);
var
  FatNum, QtdX, ValX, CusX: Double;
begin
  FatNum := DmodFin.QrLctosFatNum.Value;
  //
  QrFutNeg.Close;
  QrFutNeg.Params[0].AsFloat := FatNum;
  QrFutNeg.Open;
  //
  QrLoc.Close;
  QrLoc.Params[0].AsFloat := FatNum;
  QrLoc.Open;
  //
  if QrLoc.RecordCount > 0 then
  begin
    FCodigo   := QrLoc.FieldByName('Codigo').Value;
    FControle := QrLoc.FieldByName('Controle').Value;
    //
    if QrLocDataReal.Value = 0 then
    begin
      if QrFutNeg.RecordCount > 0 then
      begin
        AlteraDescricao(DmodFin.QrLctosControle.Value, 0, Trim(Dmod.QrControleTxtVdaEsq.Value), DmodFin.QrLctosDescricao.Value);
    
        DmodFin.VeSeReabreLct(FmSelfGer.TPDataIni, FmSelfGer.TPDataFim,
          FmSelfGer.CBUH.KeyValue, DmodFin.QrLctosControle.Value,
          DmodFin.QrLctosSub.Value, DmodFin.QrCarts, DmodFin.QrLctos);
        //
        MyObjects.frxMostra(frxFutNeg, 'Estoque futuro negativo');
        Close;
      end else
      begin
        if DmProd.ImpedePeloBalanco(MCData.Date) then
        begin
          Application.MessageBox(PChar('Inclus�o anulada!' + #13#10 +
            'Motivo: A data selecionada � inferior a data do �ltimo balan�o!'), 'Aviso',
            MB_OK+MB_ICONWARNING);
          exit;
        end;

        PBVenda.Visible  := True;
        //
        QrMovim2.Close;
        QrMovim2.Params[0].AsFloat := FatNum;
        QrMovim2.Open;
        QrMovim2.First;
        //
        PBVenda.Max      := QrMovim2.RecordCount;
        PBVenda.Position := 0;

        // Atualiza custo caso seje confirma��o de venda
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE movim SET Val=:P0 WHERE Controle=:P1 AND Conta=:P2 ');
        while not QrMovim2.Eof do
        begin
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          Dmod.QrUpdU.Params[00].AsFloat   := CusX * QrMovim2Qtd.Value;
          Dmod.QrUpdU.Params[01].AsFloat   := FatNum;
          Dmod.QrUpdU.Params[02].Asinteger := QrMovim2Conta.Value;
          Dmod.QrUpdU.ExecSQL;
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          QrMovim2.Next;
          //
          PBVenda.Position := PBVenda.Position + 1;
        end;

        // Confirma venda;
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE moviv SET DataReal=:P0 WHERE Controle=:P1');
        Dmod.QrUpdU.Params[00].AsString  := Geral.FDT(MCData.Date, 1);
        Dmod.QrUpdU.Params[01].AsFloat   := FatNum;
        Dmod.QrUpdU.ExecSQL;

        // Confirma venda; todos itens ao mesmo tempo
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE movim SET DataReal=:P0 WHERE Controle=:P1');
        Dmod.QrUpdU.Params[00].AsString  := Geral.FDT(MCData.Date, 1);
        Dmod.QrUpdU.Params[01].AsFloat   := FatNum;
        Dmod.QrUpdU.ExecSQL;

        // Atualiza estoques
        PBVenda.Max      := QrMovim2.RecordCount;
        PBVenda.Position := 0;
        //
        QrMovim2.First;
        while not QrMovim2.Eof do
        begin
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          QrMovim2.Next;
          //
          PBVenda.Position := PBVenda.Position + 1;
        end;

        FmMoviV.AtualizaStatus(FCodigo, FControle, 0, 3);

        AlteraDescricao(DmodFin.QrLctosControle.Value, 1, Trim(Dmod.QrControleTxtVdaEsq.Value), DmodFin.QrLctosDescricao.Value);

        QuitaItemLanctoAutomatico(MCData.Date);
        {UFinanceiro.QuitacaoAutomaticaDmk(FmSelfGer.dmkDBGLct,
          DmodFin.QrLctos, DmodFin.QrCarts);}

        DmodFin.VeSeReabreLct(FmSelfGer.TPDataIni, FmSelfGer.TPDataFim,
          FmSelfGer.CBUH.KeyValue, DmodFin.QrLctosControle.Value,
          DmodFin.QrLctosSub.Value, DmodFin.QrCarts, DmodFin.QrLctos);

        if Application.MessageBox(PChar('Deseja localizar a venda?'),
          'Pergunta', MB_YESNO+MB_ICONQUESTION) = ID_YES
        then begin
          Close;          
          if DBCheck.CriaFm(TFmMoviV, FmMoviV, afmoNegarComAviso) then
          begin
            FmMoviV.LocCod(FCodigo, FCodigo);
            FmMoviV.ImprimeVenda(True);
            //
            FmMoviV.ShowModal;
            FmMoviV.Destroy;
          end;
        end;
      end;
    end else
    begin
      Application.MessageBox(PChar('Inclus�o anulada!' + #13#10 +
        'Motivo: Venda j� confirmada!'), 'Aviso',
        MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
end;

procedure TFmVendaConf.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVendaConf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmVendaConf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodFin.VeSeReabreLct(FmSelfGer.TPDataIni, FmSelfGer.TPDataFim,
    FmSelfGer.CBUH.KeyValue, DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosSub.Value, DmodFin.QrCarts, DmodFin.QrLctos);
end;

procedure TFmVendaConf.FormCreate(Sender: TObject);
begin
  MCData.Date := Date;
  QrCarteira.Close;
  QrCarteira.Params[0].AsString := VAR_LIB_EMPRESAS;
  QrCarteira.Open;
  //
  EdCarteira.ValueVariant := Dmod.QrControleMoviVCaPag.Value;
  CBCarteira.KeyValue     := Dmod.QrControleMoviVCaPag.Value;
end;

procedure TFmVendaConf.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmVendaConf.frxFutNegGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_CODMOVIV') = 0 then Value := FCodigo;
end;

procedure TFmVendaConf.QuitaItemLanctoAutomatico(Data: TDateTime);
var
  Controle2: Integer;
  Txt: String;
begin
  if DmodFin.QrLctosSit.Value > 0 then
  begin
    case DmodFin.QrLctosSit.Value of
      1: Txt := 'j� tem pagamentos parciais atrelados a ele';
      2: Txt := 'j� est� quitado';
      3: Txt := 'j� est� compensado';
      else Txt := 'j� possui algum tipo de atrelamento desconhecido';
    end;
    Application.MessageBox(PChar('O lan�amento n� ' + IntToStr(
    DmodFin.QrLctosControle.Value) + ' n�o ser� compensada pois ' +
    Txt + '!'), 'Aviso', MB_OK + MB_ICONWARNING);
    Exit;
  end;
  Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    VAR_LCT, VAR_LCT, 'Controle');
  //
  if DmodFin.QrLctosSit.Value > 0 then
  begin
    Application.MessageBox(PChar('O lan�amento n� ' +
    IntToStr(DmodFin.QrLctosControle.Value) +
    ' j� possui pagamento parcial ou total, portanto n�o ser� quitado!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Sit', 'Compensado', 'ID_Quit'], ['Controle', 'Sub', 'Tipo'], [3,
    FormatDateTime(VAR_FORMATDATE, Date), Controle2], [
    DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value, 2], True, '');
  //
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_Data       := Geral.FDT(MCData.Date, 1);
  FLAN_Vencimento := Geral.FDT(DmodFin.QrLctosVencimento.Value, 1);
  FLAN_DataCad    := Geral.FDT(Date, 1);
  FLAN_Mez        := MLAGeral.ITS_NULL(DmodFin.QrLctosMez.Value);
  FLAN_Descricao  := DmodFin.QrLctosDescricao.Value;
  FLAN_Compensado := Geral.FDT(MCData.Date, 1);
  FLAN_Duplicata  := DmodFin.QrLctosDuplicata.Value;
  FLAN_Doc2       := DmodFin.QrLctosDoc2.Value;
  FLAN_SerieCH    := DmodFin.QrLctosSerieCH.Value;

  FLAN_Documento  := Trunc(DmodFin.QrLctosDocumento.Value);
  FLAN_Tipo       := 1;
  FLAN_Carteira   := EdCarteira.ValueVariant;
  FLAN_Credito    := DmodFin.QrLctosCredito.Value;
  FLAN_Debito     := DmodFin.QrLctosDebito.Value;
  FLAN_Genero     := DmodFin.QrLctosGenero.Value;
  FLAN_SerieNF    := DmodFin.QrLctosSerieNF.Value;
  FLAN_NotaFiscal := DmodFin.QrLctosNotaFiscal.Value;
  FLAN_Sit        := 3;
  FLAN_Cartao     := 0;
  FLAN_Linha      := 0;
  FLAN_Fornecedor := DmodFin.QrLctosFornecedor.Value;
  FLAN_Cliente    := DmodFin.QrLctoscliente.Value;
  FLAN_MoraDia    := DmodFin.QrLctosMoraDia.Value;
  FLAN_Multa      := DmodFin.QrLctosMulta.Value;

  FLAN_Vendedor   := DmodFin.QrLctosVendedor.Value;
  FLAN_Account    := DmodFin.QrLctosAccount.Value;
  FLAN_ICMS_P     := DmodFin.QrLctosICMS_P.Value;
  FLAN_ICMS_V     := DmodFin.QrLctosICMS_V.Value;
  FLAN_CliInt     := DmodFin.QrLctosCliInt.Value;
  FLAN_Depto      := DmodFin.QrLctosDepto.Value;
  FLAN_DescoPor   := DmodFin.QrLctosDescoPor.Value;
  FLAN_ForneceI   := DmodFin.QrLctosForneceI.Value;
  FLAN_DescoVal   := DmodFin.QrLctosDescoVal.Value;
  FLAN_NFVal      := DmodFin.QrLctosNFVal.Value;
  FLAN_Unidade    := DmodFin.QrLctosUnidade.Value;
  FLAN_Qtde       := DmodFin.QrLctosQtde.Value;
  FLAN_FatID      := DmodFin.QrLctosFatID.Value;
  FLAN_FatID_Sub  := DmodFin.QrLctosFatID_Sub.Value;
  FLAN_FatNum     := DmodFin.QrLctosFatNum.Value;
  FLAN_FatParcela := DmodFin.QrLctosFatParcela.Value;
  FLAN_MultaVal   := DmodFin.QrLctosMultaVal.Value;
  FLAN_MoraVal    := DmodFin.QrLctosMoraVal.Value;
  FLAN_ID_Pgto    := DmodFin.QrLctosControle.Value;
  FLAN_Controle   := Round(Controle2);
  //
  UFinanceiro.InsereLancamento();
end;

end.

