object FmCiclosAulaImp: TFmCiclosAulaImp
  Left = 339
  Top = 185
  Caption = 'CIC-CICLO-009 :: Importar turma WEB'
  ClientHeight = 510
  ClientWidth = 1216
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 450
    Width = 1216
    Height = 60
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 1078
      Top = 1
      Width = 137
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1216
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Importar turma WEB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1214
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1216
    Height = 391
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 1214
      Height = 347
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsCursosWEB
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Promotor'
          Width = 350
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cidade'
          Width = 350
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Alunos'
          Title.Caption = 'Total de alunos'
          Visible = True
        end>
    end
    object PB2: TProgressBar
      Left = 1
      Top = 369
      Width = 1214
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      Visible = False
    end
    object PB1: TProgressBar
      Left = 1
      Top = 348
      Width = 1214
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      Visible = False
    end
  end
  object QrCursosWEB: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      
        'SELECT cur.id Codigo, cid.nome Cidade, ufs.uf UF, usr.nome Promo' +
        'tor, '
      'cur.data_cur Data, COUNT(alu.Id) Alunos, usr.id CodProm'
      'FROM intranet_curso cur'
      'LEFT JOIN dados_cidades cid ON cur.cidade = cid.id'
      'LEFT JOIN dados_estados ufs ON ufs.id = cur.estado'
      'LEFT JOIN intranet_usuarios usr ON cur.id_user = usr.id '
      'LEFT JOIN intranet_alunos alu ON alu.Curso = cur.Id'
      'WHERE cur.Id > 0'
      'AND usr.id=:P1'
      'AND cid.nome LIKE :P2'
      'GROUP BY alu.Curso'
      '')
    Left = 159
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCursosWEBCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrCursosWEBCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 50
    end
    object QrCursosWEBPromotor: TWideStringField
      FieldName = 'Promotor'
      Size = 250
    end
    object QrCursosWEBData: TWideStringField
      FieldName = 'Data'
      Size = 250
    end
    object QrCursosWEBAlunos: TLargeintField
      FieldName = 'Alunos'
      Required = True
    end
    object QrCursosWEBCodProm: TWordField
      FieldName = 'CodProm'
      Required = True
    end
    object QrCursosWEBUF: TWideStringField
      FieldName = 'UF'
      Size = 10
    end
  end
  object DsCursosWEB: TDataSource
    DataSet = QrCursosWEB
    Left = 187
    Top = 65
  end
end
