unit Cores;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UmySQLModule, UnMySQLCuringa, mySQLDbTables, dmkPermissoes,
  dmkGeral, dmkLabel,  UnDmkProcFunc;

type
  TFmCores = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    EdCodigo: TEdit;
    EdNome: TEdit;
    QrCores: TmySQLQuery;
    DsCores: TDataSource;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrCoresCodigo: TIntegerField;
    QrCoresLk: TIntegerField;
    QrCoresDataCad: TDateField;
    QrCoresDataAlt: TDateField;
    QrCoresUserCad: TIntegerField;
    QrCoresUserAlt: TIntegerField;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCoresNome: TWideStringField;
    QrCoresAlterWeb: TSmallintField;
    QrCoresAtivo: TSmallintField;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCoresAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCoresAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmCores: TFmCores;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCores.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCores.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCoresCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCores.DefParams;
begin
  VAR_GOTOTABELA := 'cores';
  VAR_GOTOmySQLTABLE := QrCores;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cores');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCores.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Enabled := True;
    DBEdCodigo.Visible := False;
    DBEdNome.Visible := False;
    EdCodigo.Visible := True;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    PainelControle.Visible:=False;
    PainelConfirma.Visible:=True;
    if Status = CO_INCLUSAO then
      EdCodigo.Text := FormatFloat('000', Codigo)
    else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Enabled := False;
    DBEdCodigo.Visible := True;
    DBEdNome.Visible := True;
    EdCodigo.Visible := False;
    EdNome.Visible := False;
    PainelControle.Visible:=True;
    PainelConfirma.Visible:=False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmCores.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCores.AlteraRegistro;
var
  Cores : Integer;
begin
  Cores := QrCoresCodigo.Value;
  if QrCoresCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Cores, Dmod.MyDB, 'cores', 'codigo') then
  begin
    try
      UMyMod.UpdLockY(Cores, Dmod.MyDB, 'cores', 'codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCores.IncluiRegistro;
var
  Cursor : TCursor;
  Cores : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Cores := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle', 'cores', 'cores', 'codigo');
    MostraEdicao(True, CO_INCLUSAO, Cores);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCores.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCores.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmCores.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCores.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCores.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCores.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCores.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCores.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCores.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := QrCoresCodigo.Value;
  Close;
end;

procedure TFmCores.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO cores SET ');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, DataCad=:P1, UserCad=:P2, Codigo=:P3');
    Dmod.QrUpdU.SQL.Add('');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE cores SET ');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, DataAlt=:P1, UserAlt=:P2 WHERE Codigo=:P3');
  end;
  Dmod.QrUpdU.Params[00].AsString := Nome;
  Dmod.QrUpdU.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnLockY(Codigo, Dmod.MyDB, 'cores', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmCores.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'cores', Codigo);
  UMyMod.UpdUnLockY(Codigo, Dmod.MyDB, 'cores', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnLockY(Codigo, Dmod.MyDB, 'cores', 'Codigo');
end;

procedure TFmCores.FormCreate(Sender: TObject);
begin
  VAR_COD := 0;
  EdCodigo.Left := 16;
  EdNome.Left := 16;
  CriaOForm;
end;

procedure TFmCores.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCoresCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCores.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCores.QrCoresAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCores.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCores.QrCoresAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrCoresCodigo.Value, False);
end;

procedure TFmCores.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCoresCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cores', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCores.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
end;

end.




