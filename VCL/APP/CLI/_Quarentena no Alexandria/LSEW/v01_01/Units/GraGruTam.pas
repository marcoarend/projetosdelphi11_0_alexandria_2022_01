unit GraGruTam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, MyDBCheck, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu,
  dmkLabel, Mask, dmkGeral, UnInternalConsts, Variants, UnDmkEnums;

type
  TFmGraGruTam = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    PnEdita: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnNiveis: TPanel;
    Label3: TLabel;
    dmkEdCBGradesTams: TdmkEditCB;
    dmkCBGradesTams: TdmkDBLookupComboBox;
    dmkEdControle: TdmkEdit;
    Label12: TLabel;
    QrGradesTams: TmySQLQuery;
    DsGradesTams: TDataSource;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsNome: TWideStringField;
    SpeedButton2: TSpeedButton;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruTam: TFmGraGruTam;

implementation

uses Module, UMySQLModule, UnMySQLCuringa, GraGru, Tamanhos, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruTam.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Tam: Integer;
begin
  Codigo   := FmGraGru.QrGradesCodigo.Value;
  Tam := dmkEdCBGradesTams.ValueVariant;
  // Localiza se a cor j� foi incluida
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Controle');
  QrLoc.SQL.Add('FROM gradestams');
  QrLoc.SQL.Add('WHERE Codigo=:P0');
  QrLoc.SQL.Add('AND Tam=:P1');
  QrLoc.Params[0].AsInteger := Codigo;
  QrLoc.Params[1].AsInteger := Tam;
  QrLoc.Open;
  //
  if QrLoc.RecordCount > 0 then
  begin
    Application.MessageBox('Este tamanho j� foi adicionado.', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdCBGradesTams.SetFocus;
    Exit;
  end;
  //
  if Tam = 0 then
  begin
    Application.MessageBox('Defina o tamanho.', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdCBGradesTams.SetFocus;
    Exit;
  end;
  Controle := UMyMod.BuscaEmLivreY_Def_Old('gradestams', 'Controle', LaTipo.Caption, FmGraGru.QrGradesTamsControle.Value);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'gradestams', False,
  [
    'Tam', 'Codigo'
  ], ['Controle'], [
    dmkEdCBGradesTams.ValueVariant, Codigo
  ], [Controle]) then
  begin
    FmGraGru.QrGradesTams.Close;
    FmGraGru.QrGradesTams.Open;
    VAR_COD := Controle;
    //
    Application.MessageBox('Inclus�o conclu�da', 'Aviso', MB_OK+MB_ICONINFORMATION);
    dmkEdCBGradesTams.ValueVariant := 0;
    dmkCBGradesTams.KeyValue       := NULL;
    dmkEdCBGradesTams.SetFocus;
  end;
end;

procedure TFmGraGruTam.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := FmGraGru.QrGradesTamsControle.Value;
  Close;
end;

procedure TFmGraGruTam.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    dmkEdCBGradesTams.ValueVariant := 0;
    dmkCBGradesTams.KeyValue       := NULL;
  end else
  begin
    dmkEdControle.ValueVariant     := FmGraGru.QrGradesTamsControle.Value;
    dmkEdCBGradesTams.ValueVariant := FmGraGru.QrGradesTamsTam.Value;
    dmkCBGradesTams.KeyValue       := FmGraGru.QrGradesTamsTam.Value;
  end;
  dmkEdCBGradesTams.SetFocus;
end;

procedure TFmGraGruTam.FormCreate(Sender: TObject);
begin
  QrGradesTams.Open;
end;

procedure TFmGraGruTam.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGraGruTam.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTamanhos, FmTamanhos, afmoNegarComAviso) then
  begin
    FmTamanhos.ShowModal;
    FmTamanhos.Destroy;
    //
    if VAR_COD > 0 then
    begin
      QrGradesTams.Close;
      QrGradesTams.Open;
      //
      dmkEdCBGradesTams.ValueVariant := VAR_COD;
      dmkCBGradesTams.KeyValue       := VAR_COD;
    end;
  end;
end;

end.


