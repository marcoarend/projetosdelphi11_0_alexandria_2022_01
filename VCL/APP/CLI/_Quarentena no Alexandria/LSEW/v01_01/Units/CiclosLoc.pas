unit CiclosLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, Controls, dmkDBGrid;

type
  TFmCiclosLoc = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PainelDados: TPanel;
    Label10: TLabel;
    EdProfessor: TdmkEditCB;
    CBProfessor: TdmkDBLookupComboBox;
    dmkDBGrid1: TdmkDBGrid;
    QrCiclos: TmySQLQuery;
    QrCiclosSTATUS_TXT: TWideStringField;
    QrCiclosCodigo: TIntegerField;
    QrCiclosDataSaida: TDateField;
    QrCiclosDataChega: TDateField;
    QrCiclosDataAcert: TDateField;
    QrCiclosAlunosInsc: TIntegerField;
    QrCiclosAlunosAula: TIntegerField;
    QrCiclosStatus: TSmallintField;
    QrCiclosControle: TIntegerField;
    QrCiclosAlunosPrc: TFloatField;
    QrCiclosAlunosVal: TFloatField;
    QrCiclosAlunosPgt: TFloatField;
    QrCiclosProdVal: TFloatField;
    QrCiclosProdPgt: TFloatField;
    QrCiclosProdMrg: TFloatField;
    QrCiclosDespesas: TFloatField;
    QrCiclosNOMEPRF: TWideStringField;
    QrCiclosTransfCre: TFloatField;
    QrCiclosTransfDeb: TFloatField;
    QrCiclosCurso: TIntegerField;
    QrCiclosRatDebVar: TFloatField;
    QrCiclosRatTrfFix: TFloatField;
    QrCiclosRatTrfVar: TFloatField;
    QrCiclosComisVal: TFloatField;
    QrCiclosSdoProfIni: TFloatField;
    QrCiclosSdoProfFim: TFloatField;
    QrCiclosComisPgt: TFloatField;
    QrCiclosComisTrf: TFloatField;
    QrCiclosProfVal: TFloatField;
    QrCiclosEmprestC: TFloatField;
    QrCiclosEmprestD: TFloatField;
    QrCiclosDespProm: TFloatField;
    QrCiclosDespProf: TFloatField;
    QrCiclosValAluDil: TFloatField;
    QrCiclosValAluVen: TFloatField;
    QrCiclosProfessor: TIntegerField;
    QrCiclosRolComis: TIntegerField;
    DsCiclos: TDataSource;
    QrCiclosLk: TIntegerField;
    QrCiclosDataCad: TDateField;
    QrCiclosDataAlt: TDateField;
    QrCiclosUserCad: TIntegerField;
    QrCiclosUserAlt: TIntegerField;
    QrCiclosAlterWeb: TSmallintField;
    QrCiclosAtivo: TSmallintField;
    QrCiclosPromotor: TIntegerField;
    QrCiclosNOMEPRM: TWideStringField;
    QrCiclosCART_PROF: TIntegerField;
    QrCiclosCART_PROM: TIntegerField;
    QrCiclosROLCOMIS_PRF: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCiclosCalcFields(DataSet: TDataSet);
    procedure EdProfessorChange(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCiclos(Professor: Integer);
  public
    { Public declarations }
  end;

  var
  FmCiclosLoc: TFmCiclosLoc;

implementation

uses ModuleCiclos, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmCiclosLoc.BtOKClick(Sender: TObject);
begin
  FmPrincipal.FCiclo := QrCiclosCodigo.Value;
  Close;
end;

procedure TFmCiclosLoc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosLoc.dmkDBGrid1DblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmCiclosLoc.EdProfessorChange(Sender: TObject);
begin
  ReopenCiclos(EdProfessor.ValueVariant);
end;

procedure TFmCiclosLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclosLoc.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosLoc.QrCiclosCalcFields(DataSet: TDataSet);
begin
  case QrCiclosStatus.Value of
    0: QrCiclosSTATUS_TXT.Value := 'Ciclo aberto';
    1: QrCiclosSTATUS_TXT.Value := 'Curso em andamento';
    2: QrCiclosSTATUS_TXT.Value := 'Retorno de viagem';
    3: QrCiclosSTATUS_TXT.Value := 'Acerto em andamento';
    //
    9: QrCiclosSTATUS_TXT.Value := 'Ciclo encerrado';
  end;
end;

procedure TFmCiclosLoc.ReopenCiclos(Professor: Integer);
begin
  QrCiclos.Close;
  QrCiclos.Params[0].AsInteger := Professor;
  QrCiclos.Open;
end;

end.
