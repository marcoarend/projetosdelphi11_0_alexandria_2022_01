object FmCiclosAval: TFmCiclosAval
  Left = 368
  Top = 194
  Caption = 'CIC-CICLO-008 :: Avalia'#231#227'o por m'#233'dia de alunos'
  ClientHeight = 251
  ClientWidth = 712
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 712
    Height = 203
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Label10: TLabel
      Left = 17
      Top = 50
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label9: TLabel
      Left = 17
      Top = 10
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label5: TLabel
      Left = 17
      Top = 93
      Width = 87
      Height = 13
      Caption = 'M'#237'nimo de alunos:'
    end
    object Label6: TLabel
      Left = 123
      Top = 93
      Width = 88
      Height = 13
      Caption = 'M'#225'ximo de alunos:'
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 154
      Width = 710
      Height = 48
      Align = alBottom
      TabOrder = 5
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 601
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object dmkEdNome: TdmkEdit
      Left = 17
      Top = 67
      Width = 469
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = dmkEdNomeExit
    end
    object dmkEdCodigo: TdmkEdit
      Left = 17
      Top = 25
      Width = 71
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object dmkCkAtivo: TdmkCheckBox
      Left = 229
      Top = 111
      Width = 45
      Height = 17
      Caption = 'Ativo'
      TabOrder = 4
      QryCampo = 'Ativo'
      UpdCampo = 'Ativo'
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object dmkEdAlunosMin: TdmkEdit
      Left = 17
      Top = 110
      Width = 100
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'AlunosMin'
      UpdCampo = 'AlunosMin'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = dmkEdNomeExit
    end
    object dmkEdAlunosMax: TdmkEdit
      Left = 123
      Top = 110
      Width = 100
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'AlunosMax'
      UpdCampo = 'AlunosMax'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = dmkEdNomeExit
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 712
    Height = 203
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelControle: TPanel
      Left = 1
      Top = 154
      Width = 710
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 127
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 87
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 7
          Top = 3
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 240
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 710
      Height = 136
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label2: TLabel
        Left = 6
        Top = 49
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label1: TLabel
        Left = 6
        Top = 7
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 6
        Top = 90
        Width = 87
        Height = 13
        Caption = 'M'#237'nimo de alunos:'
      end
      object Label4: TLabel
        Left = 112
        Top = 90
        Width = 88
        Height = 13
        Caption = 'M'#225'ximo de alunos:'
      end
      object dmkDBEdCodigo: TdmkDBEdit
        Left = 6
        Top = 22
        Width = 72
        Height = 21
        DataField = 'Codigo'
        DataSource = DsCiclosAval
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdNome: TdmkDBEdit
        Left = 6
        Top = 64
        Width = 469
        Height = 21
        DataField = 'Nome'
        DataSource = DsCiclosAval
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object CkAtivo: TDBCheckBox
        Left = 218
        Top = 107
        Width = 62
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsCiclosAval
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdAlunosMin: TdmkDBEdit
        Left = 6
        Top = 106
        Width = 100
        Height = 21
        DataField = 'AlunosMin'
        DataSource = DsCiclosAval
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdAlunosMax: TdmkDBEdit
        Left = 112
        Top = 106
        Width = 100
        Height = 21
        DataField = 'AlunosMax'
        DataSource = DsCiclosAval
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 712
    Height = 48
    Align = alTop
    Caption = '                              Avalia'#231#227'o por m'#233'dia de alunos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object LaTipo: TdmkLabel
      Left = 629
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 403
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 221
      ExplicitTop = -1
      ExplicitWidth = 401
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCiclosAval: TDataSource
    DataSet = QrCiclosAval
    Left = 580
    Top = 8
  end
  object QrCiclosAval: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCiclosAvalBeforeOpen
    AfterOpen = QrCiclosAvalAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM ciclosaval'
      'WHERE Codigo > 0')
    Left = 552
    Top = 8
    object QrCiclosAvalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCiclosAvalNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCiclosAvalAlunosMin: TIntegerField
      FieldName = 'AlunosMin'
    end
    object QrCiclosAvalAlunosMax: TIntegerField
      FieldName = 'AlunosMax'
    end
    object QrCiclosAvalLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCiclosAvalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCiclosAvalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCiclosAvalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCiclosAvalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCiclosAvalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCiclosAvalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrLocCiclosAval: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome '
      'FROM ciclosaval'
      'WHERE Nome LIKE :P0')
    Left = 480
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCiclosAvalNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 240
    Top = 11
  end
end
