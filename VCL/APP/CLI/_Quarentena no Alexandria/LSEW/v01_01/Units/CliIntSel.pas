unit CliIntSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, dmkDBGrid, dmkEdit, UnInternalConsts, dmkDBLookupComboBox, dmkEditCB,
  dmkGeral;

type
  TFmCliIntSel = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    Panel3: TPanel;
    Panel4: TPanel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkDBGrid1: TdmkDBGrid;
    QrCliIntCodigo: TIntegerField;
    QrCliIntCliente: TIntegerField;
    QrCliIntLk: TIntegerField;
    QrCliIntDataCad: TDateField;
    QrCliIntDataAlt: TDateField;
    QrCliIntUserCad: TIntegerField;
    QrCliIntUserAlt: TIntegerField;
    QrCliIntAlterWeb: TSmallintField;
    QrCliIntAtivo: TSmallintField;
    QrCliIntNOMECLIENTE: TWideStringField;
    QrCliIntCODENT: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEntInt: Integer;
    procedure ReopenCliInt();
  end;

  var
  FmCliIntSel: TFmCliIntSel;

implementation

uses Module, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmCliIntSel.BtSaidaClick(Sender: TObject);
begin
  FEntInt := 0;
  Close;
end;

procedure TFmCliIntSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCliIntSel.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCliIntSel.EdNomeChange(Sender: TObject);
begin
  ReopenCliInt;
end;

procedure TFmCliIntSel.dmkDBGrid1DblClick(Sender: TObject);
begin
  if QrCliInt.RecordCount > 0 then
  begin
    EdCliInt.Text     := FormatFloat('0', QrCliIntCODENT.Value);
    CBCliInt.KeyValue := QrCliIntCODENT.Value;
    BtOKClick(Self);
  end;
end;

procedure TFmCliIntSel.BtOKClick(Sender: TObject);
begin
  FEntInt := Geral.IMV(EdCliInt.Text);
  Close;
end;

procedure TFmCliIntSel.FormCreate(Sender: TObject);
begin
  ReopenCliInt;
end;

procedure TFmCliIntSel.ReopenCliInt();
begin
  QrCliInt.Close;
  QrCliInt.Params[0].AsString := '%' + EdNome.Text + '%';
  QrCliInt.Open;
end;

end.
