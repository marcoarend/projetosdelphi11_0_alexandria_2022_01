unit ModuleProd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, Grids, dmkEdit, dmkLabel, dmkGeral, UnMLAGeral, Buttons,
  stdctrls, extctrls, Variants, UnDmkEnums;

type
  TDmProd = class(TDataModule)
    QrLocta: TmySQLQuery;
    QrLoctaConta: TIntegerField;
    QrProdutos: TmySQLQuery;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsNOMECOR: TWideStringField;
    QrEstq: TmySQLQuery;
    QrEstqEstqQ: TFloatField;
    QrEstqEstqV: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrCustoProd: TmySQLQuery;
    QrCustoProdCusto: TFloatField;
    QrProdutosTam: TIntegerField;
    QrProdutosCor: TIntegerField;
    QrProdutosAtivo: TSmallintField;
    QrProdutosConta: TIntegerField;
    QrComisPrd: TmySQLQuery;
    QrProdutosControle: TIntegerField;
    QrGraGruVal: TmySQLQuery;
    QrProdutos2: TmySQLQuery;
    QrProdutos2Cor: TIntegerField;
    QrProdutos2Tam: TIntegerField;
    QrProdutos2Controle: TIntegerField;
    QrProdutos2Ativo: TSmallintField;
    QrGraGruValCor: TIntegerField;
    QrGraGruValTam: TIntegerField;
    QrGraGruValCustoPreco: TFloatField;
    QrProdutos2EstqQ: TFloatField;
    QrLocProd: TmySQLQuery;
    QrLocProdCodigo: TIntegerField;
    QrLocProdGraCusPrc: TIntegerField;
    QrLocProdCustoPreco: TFloatField;
    QrPtosPedPrc: TmySQLQuery;
    QrPtosPedPrcCor: TIntegerField;
    QrPtosPedPrcTam: TIntegerField;
    QrPtosPedPrcQuanti: TFloatField;
    QrPtosPedPrcPrecoR: TFloatField;
    QrPtosPedPrcDescoP: TFloatField;
    QrPtosPedPrcPrecoO: TFloatField;
    QrPedItsAnul: TmySQLQuery;
    DsPedItsAnul: TDataSource;
    QrPedItsAnulIDCtrl: TIntegerField;
    QrPedItsAnulDataIns: TDateTimeField;
    QrPedItsAnulStatus: TSmallintField;
    QrPedItsAnulQuanti: TFloatField;
    QrPedItsAnulPrecoR: TFloatField;
    QrPedItsAnulNO_Produto: TWideStringField;
    QrPedItsAnulNO_Cor: TWideStringField;
    QrPedItsAnulCU_Tam: TIntegerField;
    QrPedItsAnulNO_Tam: TWideStringField;
    QrPedItsAnulNO_STATUS: TWideStringField;
    QrPedItsAnulAtivo: TSmallintField;
    QrPediItsMoM: TmySQLQuery;
    QrPediItsMoMAtivo: TSmallintField;
    QrPediItsMoMGrade: TIntegerField;
    QrPediItsMoMTam: TIntegerField;
    QrPediItsMoMCor: TIntegerField;
    QrPediItsMoMID_Loc: TIntegerField;
    QrPediItsMoMID_Net: TIntegerField;
    QrDelMomPto: TmySQLQuery;
    QrSelMomPto: TmySQLQuery;
    QrSelMomPtoConta: TIntegerField;
    QrSelMomPtoGrade: TIntegerField;
    QrSelMomPtoTam: TIntegerField;
    QrSelMomPtoCor: TIntegerField;
    QrMoviV: TmySQLQuery;
    QrMoviVNOMEENTI: TWideStringField;
    QrMoviVCodigo: TIntegerField;
    QrMoviVControle: TIntegerField;
    QrMoviVDataPedi: TDateField;
    QrMoviVDataReal: TDateField;
    QrMoviVCliente: TIntegerField;
    QrMoviVLk: TIntegerField;
    QrMoviVDataCad: TDateField;
    QrMoviVDataAlt: TDateField;
    QrMoviVUserCad: TIntegerField;
    QrMoviVUserAlt: TIntegerField;
    QrMoviVDATAREAL_TXT: TWideStringField;
    QrMoviVSALDO: TFloatField;
    QrMoviVTotal: TFloatField;
    QrMoviVPago: TFloatField;
    QrMoviVPOSIt: TFloatField;
    QrMoviVAlterWeb: TSmallintField;
    QrMoviVAtivo: TSmallintField;
    QrMoviVGraCusPrc: TIntegerField;
    QrMoviVTransportadora: TIntegerField;
    QrMoviVFrete: TFloatField;
    QrMoviVNOMECAD2: TWideStringField;
    QrMoviVNOMEALT2: TWideStringField;
    QrMoviVNOMECAD: TWideStringField;
    QrMoviVNOMEALT: TWideStringField;
    QrMoviVFRETEA: TFloatField;
    QrMoviVObserv: TWideStringField;
    DsMoviV: TDataSource;
    QrPedItsAnulProduto: TIntegerField;
    procedure QrPedItsAnulCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function  ObtemCustoProduto(Grade, Cor, Tam: Integer): Double;
    function ObtemPrecoProduto(GraCusPrc, Grade, Controle: Integer): Double;
    function  LocalizaConta(Controle, Kit: Integer; Grade, Cor, Tam: String): Integer;
    function  ImpedePeloBalanco(Data: TDateTime): Boolean;
    function  VerificaBalanco: Integer;
    function  AtualizaEstoqueMercadoria(Grade, Cor, Tam: Integer;
              Real, Pedido: Boolean; var Qtd, Val, Cus: Double): Integer;

    procedure ReopenGradesTams(Grade: Integer);
    procedure ReopenGradesCors(Grade: Integer);
    procedure ConfigGrades(Grade, Controle, SubCtrl, SubCta: Integer; GradeA, GradeC, GradeX: TStringGrid);
    procedure ConfigGrades1(Grades, Tabela: Integer;
              GradeA, GradeX, GradeC, GradeP, GradeE: TStringGrid);
    procedure AtualizaGradeA(Grade, Controle, SubCtrl, SubCta: Integer; GradeA: TStringGrid);
    procedure AtualizaGradeX(Grade: Integer; GradeX: TStringGrid);
    procedure LimpaEdits(EdTamCod, EdCorCod, EdTamNom, EdCorNom: TdmkEdit);
    procedure CalculaPreco(Como: TCalcVal; EdQtd, EdPrc, EdVen: TdmkEdit;
              RGComisTip: TRadioGroup; EdComisFat, EdComisVal: TdmkEdit);
    procedure CalculaComisProd(Como: TCalcVal; RGComisTip: TRadioGroup;
              EdQtd, EdVen, EdComisFat, EdComisVal: TdmkEdit);
    procedure SelecionaItemGradeA(Grade, Cor, Tam: Integer;
              GradeA, GradeC, GradeX: TStringGrid; EdTamCod, EdCorCod, EdTamNom, EdCorNom,
              EdQtd, EdQta, EdCua, EdVla, EdVen, EdPrc: TdmkEdit; LaStatus: TdmkLabel;
              QrMovim: TmySQLQuery; BtConfItem: TBitBtn; RolComis: Integer;
              RGComisTip: TRadioGroup; EdComisFat, EdComisVal: TdmkEdit);
    procedure AtualizaGradesAtivos1(Tam: Integer; GradeA, GradeC, GradeE: TStringGrid);
    procedure AtualizaGradesPrecos1(GraGru1, GraCusPrc: Integer; GradeP: TStringGrid);
    procedure ReopenGraGruVal(GraGru1, GraCusPrc: Integer);
    procedure ReopenProdutos(Grade, Controle, SubCtrl, SubCta: Integer);
    procedure ReopenProdutos2(GraGruT: Integer);
    //  junho 2009
    procedure ReopenPtosPedPrc(Pedido: Integer);
    function  PreparaGrades(Grade: Integer; StrGrids: array of TStringGrid): Boolean;
    procedure AtualizaGradesAtivos0(Grade: Integer; GradeA, GradeC: TStringGrid);
    //procedure ReopenGraGruVal(GraGru1, GraCusPrc: Integer);
    // Pedidos de mercadorias para pontos de venta
    // Selecionar quantidades e definir descontos e pre�os c/ custo financeiro
    procedure ConfigGrades001(Grade, Lista: Integer; GradeA, GradeX, GradeC,
              GradeP_Orig, GradeP_Real, GradeQ, GradeD: TStringGrid;
              CustoFin: Double; LaTipo: TdmkLabel);
    // Pedidos de mercadorias para pontos de venta
    // Mostrar quantidades selecionadas
    procedure ConfigGrades002(Grade: Integer; GradeA, GradeX, GradeC,
              GradeP_Orig, GradeP_Real, GradeQ, GradeD: TStringGrid; Pedido: Integer);
    // Mostra pre�os da lista padr�o em StringGrids
    procedure AtualizaGradesPrecos001(Grade, GraCusPrc: Integer;
              GradeC, GradeP_Orig, GradeP_Real: TStringGrid; CustoFin: Double);
    // Mostra pre�os de pedido
    procedure AtualizaGradesPrecos002(Grade: Integer; GradeC, GradeQ,
              GradeD, GradeP_Orig, GradeP_Real: TStringGrid; Pedido: Integer);
    function StatusPedido_Txt(Status: Integer): String;
    procedure ReopenPedItsAnul(Status: Integer);
    procedure CancelaItensPedido();
    procedure GeraBaixaEmMovimDePonto(Motivo, Controle, IDCtrl, Grade, Cor, Tam:
              Integer; Qtd, Val: Double; DataPedi: String);
    procedure DeleteItemMovimPonto(IDCtrl: Integer);
    // fim junho 2009
  end;

var
  DmProd: TDmProd;

implementation

uses Module, UnInternalConsts, ModuleGeral, UMySQLModule, Principal;

{$R *.DFM}

function TDmProd.VerificaBalanco: Integer;
begin
  QrPeriodoBal.Close;
  QrPeriodoBal.Open;
  Result := QrPeriodoBalPeriodo.Value;
  QrPeriodoBal.Close;
end;

/////////////////////////////////////

function TDmProd.ImpedePeloBalanco(Data: TDateTime): Boolean;
var
  Dia: TDateTime;
begin
  Result := True;
  Dia := StrToDate(MLAGeral.PrimeiroDiaAposPeriodo(VerificaBalanco-1, dtSystem3));
  if Data >= Dia then Result := False else
  Application.MessageBox('Data inv�lida. J� existe balan�o com data posterior.',
  'Erro', MB_OK+MB_ICONERROR);
  // USO:
//  if FmPrincipal.ImpedePeloBalanco(TPData.Date) then Exit;
end;

/////////////////////////////////////////////////

function TDmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam: Integer;
  Real, Pedido: Boolean; var Qtd, Val, Cus: Double): Integer;
var
  MyCursor: TCursor;
begin
  Qtd := 0;
  Val := 0;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  if VAR_PERIODOBAL = '' then
    VAR_PERIODOBAL := MLAGeral.PrimeiroDiaDoPeriodo(VerificaBalanco, dtSystem);
  if Real then
  begin
    Dmod.QrEstqR.Close;
    Dmod.QrEstqR.Params[00].AsInteger := Grade;
    Dmod.QrEstqR.Params[01].AsInteger := Cor;
    Dmod.QrEstqR.Params[02].AsInteger := Tam;
    Dmod.QrEstqR.Params[03].AsString  := VAR_PERIODOBAL;
    Dmod.QrEstqR.Open;
    Qtd := Dmod.QrEstqRQtd.Value;
    Val := Dmod.QrEstqRVal.Value;
    if Qtd = 0 then Cus := 0 else Cus := Val / Qtd;
  end else Cus := 0;
  //
  if Pedido then
  begin
    Dmod.QrEstqP.Close;
    Dmod.QrEstqP.Params[00].AsInteger := Grade;
    Dmod.QrEstqP.Params[01].AsInteger := Cor;
    Dmod.QrEstqP.Params[02].AsInteger := Tam;
    Dmod.QrEstqP.Params[03].AsString  := VAR_PERIODOBAL;
    Dmod.QrEstqP.Open;
  end;
  //
  if Real and not Pedido then
  begin
    Dmod.QrUpdY.SQL.Clear;
    Dmod.QrUpdY.SQL.Add('UPDATE produtos SET ');
    Dmod.QrUpdY.SQL.Add('EstqQ=:P0, EstqV=:P1, EstqC=:P2');
    Dmod.QrUpdY.SQL.Add('WHERE Codigo=:Pa AND Cor=:Pb AND Tam=:Pc');
    Dmod.QrUpdY.Params[00].AsFloat   := Qtd;
    Dmod.QrUpdY.Params[01].AsFloat   := Val;
    Dmod.QrUpdY.Params[02].AsFloat   := Cus;
    //
    Dmod.QrUpdY.Params[03].AsInteger := Grade;
    Dmod.QrUpdY.Params[04].AsInteger := Cor;
    Dmod.QrUpdY.Params[05].AsInteger := Tam;
    Dmod.QrUpdY.ExecSQL;
    Result := 1;
  end else if Pedido and not Real then
  begin
    Dmod.QrUpdY.SQL.Clear;
    Dmod.QrUpdY.SQL.Add('UPDATE produtos SET ');
    Dmod.QrUpdY.SQL.Add('PediQ=:P0 ');
    Dmod.QrUpdY.SQL.Add('WHERE Codigo=:Pa AND Cor=:Pb AND Tam=:Pc');
    Dmod.QrUpdY.Params[00].AsFloat   := Dmod.QrEstqPQtd.Value;
    //
    Dmod.QrUpdY.Params[01].AsInteger := Grade;
    Dmod.QrUpdY.Params[02].AsInteger := Cor;
    Dmod.QrUpdY.Params[03].AsInteger := Tam;
    Dmod.QrUpdY.ExecSQL;
    Result := 2;
  end else if Real and Pedido then
  begin
    Dmod.QrUpdY.SQL.Clear;
    Dmod.QrUpdY.SQL.Add('UPDATE produtos SET ');
    Dmod.QrUpdY.SQL.Add('EstqQ=:P0, EstqV=:P1, EstqC=:P2, PediQ=:P3');
    Dmod.QrUpdY.SQL.Add('WHERE Codigo=:Pa AND Cor=:Pb AND Tam=:Pc');
    Dmod.QrUpdY.Params[00].AsFloat   := Qtd;
    Dmod.QrUpdY.Params[01].AsFloat   := Val;
    Dmod.QrUpdY.Params[02].AsFloat   := Cus;
    Dmod.QrUpdY.Params[03].AsFloat   := Dmod.QrEstqPQtd.Value;
    //
    Dmod.QrUpdY.Params[04].AsInteger := Grade;
    Dmod.QrUpdY.Params[05].AsInteger := Cor;
    Dmod.QrUpdY.Params[06].AsInteger := Tam;
    Dmod.QrUpdY.ExecSQL;
    Result := 3;
  end else Result := 0;
  Screen.Cursor := MyCursor;
end;

function TDmProd.LocalizaConta(Controle, Kit: Integer; Grade, Cor, Tam: String): Integer;
begin
  QrLocta.Close;
  QrLocta.Params[00].AsInteger := Controle;
  QrLocta.Params[01].AsString  := Grade;
  QrLocta.Params[02].AsString  := Cor;
  QrLocta.Params[03].AsString  := Tam;
  QrLocta.Params[04].AsInteger := Kit;
  QrLocta.Open;
  Result := QrLoctaConta.Value;
  if Result = 0 then Application.MessageBox('Conta n�o localizada!', 'Erro',
  MB_OK+MB_ICONERROR);
end;

procedure TDmProd.AtualizaGradeX(Grade: Integer; GradeX: TStringGrid);
var
  c, l: Integer;
begin
  ReopenProdutos2(Grade);
  QrProdutos2.First;
  while not QrProdutos2.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutos2Tam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutos2Cor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutos2Controle.Value > 0 then
        begin
          GradeX.Cells[c, l] := FormatFloat('0', QrProdutos2Controle.Value);
        end else
        begin
          GradeX.Cells[c, l] := '';
        end;
      end;
    end;
    QrProdutos2.Next;
  end;
end;

procedure TDmProd.AtualizaGradeA(Grade, Controle, SubCtrl, SubCta: Integer; GradeA: TStringGrid);
var
  c, l: Integer;
begin
  ReopenProdutos(Grade, Controle, SubCtrl, SubCta);
  QrProdutos.First;
  while not QrProdutos.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutosTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutosCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutosConta.Value > 0 then
        begin
          GradeA.Cells[c, l] := '2';
        end else
        begin
          GradeA.Cells[c, l] := '1';
        end;
      end;
    end;
    QrProdutos.Next;
  end;
end;

procedure TDmProd.ReopenPedItsAnul(Status: Integer);
begin
  QrPedItsAnul.Close;
  QrPedItsAnul.SQL.Clear;
  QrPedItsAnul.SQL.Add('SELECT pia.Ativo, psm.IDCtrl, psm.DataIns, psm.Status,');
  QrPedItsAnul.SQL.Add('psm.Quanti, psm.PrecoR, grs.Nome NO_Produto, psm.Produto,');
  QrPedItsAnul.SQL.Add('cor.Nome NO_Cor, tam.CodUsu CU_Tam, tam.Nome NO_Tam');
  QrPedItsAnul.SQL.Add('FROM peditsanul pia');
  QrPedItsAnul.SQL.Add('LEFT JOIN lesew.ptosstqmov psm ON psm.IDCtrl=pia.IDCtrl');
  QrPedItsAnul.SQL.Add('LEFT JOIN lesew.produtos prd ON prd.Controle=psm.Produto');
  QrPedItsAnul.SQL.Add('LEFT JOIN lesew.grades grs ON grs.Codigo=prd.codigo');
  QrPedItsAnul.SQL.Add('LEFT JOIN lesew.cores cor ON cor.Codigo=prd.Cor');
  QrPedItsAnul.SQL.Add('LEFT JOIN lesew.tamanhos tam ON tam.Codigo=prd.Tam');
  if Status < 2 then
    QrPedItsAnul.SQL.Add('WHERE Ativo=' + FormatFloat('0', Status));
  QrPedItsAnul.SQL.Add('ORDER BY NO_Produto, NO_Cor, CU_Tam, NO_Tam');
  QrPedItsAnul.Open;
end;

procedure TDmProd.ReopenProdutos(Grade, Controle, SubCtrl, SubCta: Integer);
begin
  QrProdutos.Close;
  QrProdutos.Params[00].AsInteger := Controle;
  QrProdutos.Params[01].AsInteger := SubCtrl;
  QrProdutos.Params[02].AsInteger := SubCta;
  QrProdutos.Params[03].AsInteger := Grade;
  QrProdutos.Open;
end;

procedure TDmProd.ReopenGradesCors(Grade: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Grade;
  QrGradesCors.Open;
end;

procedure TDmProd.ReopenGradesTams(Grade: Integer);
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Grade;
  QrGradesTams.Open;
end;

procedure TDmProd.ConfigGrades(Grade, Controle, SubCtrl, SubCta: Integer; GradeA, GradeC, GradeX: TStringGrid);
var
  c, l, g: Integer;
begin

  for c := 0 to GradeA.ColCount - 1 do
    for l := 0 to GradeA.ColCount - 1 do
      GradeA.Cells[c, l] := '';
  //g := Geral.IMV(EdGrade.Text);
  g := Grade;
  if g > 0 then
  begin
    ReopenGradesCors(g);
    ReopenGradesTams(g);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    GradeA.ColCount  := c;
    GradeA.RowCount  := l;
    GradeA.FixedCols := 1;
    GradeA.FixedRows := 1;
    //
    GradeC.ColCount  := c;
    GradeC.RowCount  := l;
    //
    GradeX.ColCount  := c;
    GradeX.RowCount  := l;
    //
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      GradeA.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      GradeC.Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
      GradeX.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      GradeA.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      GradeC.Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value);
      GradeX.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      QrGradesTams.Next;
    end;
    AtualizaGradeA(g, Controle, SubCtrl, SubCta, GradeA);
    AtualizaGradeX(g, GradeX);
  end else begin
    GradeA.ColCount := 1;
    GradeA.RowCount := 1;
  end;
end;

procedure TDmProd.ConfigGrades001(Grade, Lista: Integer; GradeA, GradeX, GradeC,
  GradeP_Orig, GradeP_Real, GradeQ, GradeD: TStringGrid; CustoFin: Double;
  LaTipo: TdmkLabel);
begin
  if PreparaGrades(
  Grade, [GradeA, GradeX, GradeC, GradeP_Orig, GradeP_Real, GradeQ, GradeD]) then
  begin
    AtualizaGradesAtivos0(Grade, GradeA, GradeC);
    AtualizaGradesPrecos001(Grade, Lista, GradeC, GradeP_Orig, GradeP_Real, CustoFin);
  end;
end;

procedure TDmProd.ConfigGrades002(Grade: Integer; GradeA, GradeX, GradeC,
  GradeP_Orig, GradeP_Real, GradeQ, GradeD: TStringGrid; Pedido: Integer);
begin
  if PreparaGrades(
  Grade, [GradeA, GradeX, GradeC, GradeP_Orig, GradeP_Real, GradeQ, GradeD]) then
  begin
    AtualizaGradesAtivos0(Grade, GradeA, GradeC);
    AtualizaGradesPrecos002(Grade, GradeC, GradeQ, GradeD,
      GradeP_Orig, GradeP_Real, Pedido);
  end;
end;

procedure TDmProd.LimpaEdits(EdTamCod, EdCorCod, EdTamNom, EdCorNom: TdmkEdit);
begin
  EdTamCod.Text := '';
  EdCorCod.Text := '';
  //
  EdTamNom.Text := '';
  EdCorNom.Text := '';
  //
end;

procedure TDmProd.CalculaPreco(Como: TCalcVal; EdQtd, EdPrc, EdVen: TdmkEdit;
RGComisTip: TRadioGroup; EdComisFat, EdComisVal: TdmkEdit);
var
  Qtd, Prc, Ven: Double;
begin
  Qtd := Geral.DMV(EdQtd.Text);
  Prc := Geral.DMV(EdPrc.Text);
  Ven := Geral.DMV(EdVen.Text);
  case Como of
    tcvQtd: Ven := Qtd * Prc;
    tcvPrc: Ven := Qtd * Prc;
    tcvVal: if Qtd = 0 then Prc := 0 else Prc := Ven / Qtd;
  end;
  EdQtd.Text := Geral.FFT(Qtd, 2, siPositivo);
  //if EdPrc.Enabled then
  EdPrc.Text := Geral.FFT(Prc, 2, siPositivo);
  EdVen.Text := Geral.FFT(Ven, 2, siPositivo);
  if (RGComisTip <> nil) and (EdComisFat <> nil) and (EdComisVal <> nil) then
  CalculaComisProd(Como, RGComisTip, EdQtd, EdVen, EdComisFat, EdComisVal);
end;

procedure TDmProd.CancelaItensPedido();
begin
  Screen.Cursor := crHourGlass;
  try
    // reabrir somente itens selecionados
    QrPediItsMoM.Close;
    QrPediItsMoM.Open;
    //
    if QrPediItsMoM.RecordCount > 0 then
    begin
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE ptosstqmov ');
      Dmod.QrUpdM.SQL.Add('SET Status=-1');
      Dmod.QrUpdM.SQL.Add('WHERE IDCtrl=:P0');
      //
      QrPediItsMoM.First;
      while not QrPediItsMoM.Eof do
      begin
        if QrPediItsMoMID_Loc.Value > 0 then
        begin
          DeleteItemMovimPonto(QrPediItsMoMID_Loc.Value);
          //
          // ??Dmod.QrUpd.ExecSQL;
        end;
        // Cancela item no pedido
        Dmod.QrUpdM.Params[0].AsInteger := QrPediItsMoMID_Net.Value;
        Dmod.QrUpdM.ExecSQL;
        QrPediItsMoM.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDmProd.CalculaComisProd(Como: TCalcVal; RGComisTip: TRadioGroup;
EdQtd, EdVen, EdComisFat, EdComisVal: TdmkEdit);
  function CalcComisVal(ComisTip: Integer; Qtd, Ven, ComisFat: Double): Double;
  begin
    case ComisTip of
      // $
      1: Result := Round(Qtd * ComisFat * 100) / 100;
      // %
      2: Result := Round(Ven * ComisFat) / 100;
      else Result := 0;
    end;
  end;
  function CalcComisFat(ComisTip: Integer; Qtd, Ven, ComisVal: Double): Double;
  begin
    case ComisTip of
      1: // $
      begin
        if Qtd = 0 then Result := 0 else
        Result := Trunc(ComisVal * Qtd * 100)   / 100;
      end;
      2: // %
      begin
        if Ven = 0 then Result := 0 else
        Result := Round(ComisVal / Ven * 10000) / 100
      end;
      else Result := 0;
    end;
  end;
var
  Qtd, Ven, ComisFat, ComisVal: Double;
  ComisTip: Integer;
begin
  Ven      := Geral.DMV(EdVen.Text);
  Qtd      := Geral.DMV(EdQtd.Text);
  ComisTip := RGComisTip.ItemIndex;
  ComisFat := Geral.DMV(EdComisFat.Text);
  ComisVal := Geral.DMV(EdComisVal.Text);
  if Como = tcvComisVal then
    ComisFat := CalcComisFat(ComisTip, Qtd, Ven, ComisVal)
  else
    ComisVal := CalcComisVal(ComisTip, Qtd, Ven, ComisFat);
  //
  EdComisFat.ValueVariant := ComisFat;
  EdComisVal.ValueVariant := ComisVal;
end;

procedure TDmProd.SelecionaItemGradeA(Grade, Cor, Tam: Integer;
GradeA, GradeC, GradeX: TStringGrid; EdTamCod, EdCorCod, EdTamNom, EdCorNom,
EdQtd, EdQta, EdCua, EdVla, EdVen, EdPrc: TdmkEdit; LaStatus: TdmkLabel;
QrMovim: TmySQLQuery; BtConfItem: TBitBtn; RolComis: Integer;
RGComisTip: TRadioGroup; EdComisFat, EdComisVal: TdmkEdit);
var
  Status, GraCusPrc, Coluna, Linha, Controle: Integer;
begin
  GraCusPrc := Dmod.QrControleCiclListPrd.Value;
  if GradeA.Cells[GradeA.Col, GradeA.Row] <> '' then
  begin
    EdTamCod.Text := GradeC.Cells[GradeA.Col, 0];
    EdCorCod.Text := GradeC.Cells[0, GradeA.Row];
    //
    EdTamNom.Text := GradeA.Cells[GradeA.Col, 0];
    EdCorNom.Text := GradeA.Cells[0, GradeA.Row];
    //
  end else
    LimpaEdits(EdTamCod, EdCorCod, EdTamNom, EdCorNom);
  Status := Geral.IMV(GradeA.Cells[GradeA.Col, GradeA.Row]);
  //Grade := Geral.IMV(EdGrade.Text);
  Cor := Geral.IMV(EdCorCod.Text);
  Tam := Geral.IMV(EdTamCod.Text);
  case Status of
    0: LaStatus.SQLType := stLok;
    1:
    begin
      LaStatus.SQLType := stIns;
      EdQta.Text := '0,000';
      EdCua.Text := '0,000000';
      EdVla.Text := '0,00';
      //
      Coluna := GradeA.Col;
      Linha  := GradeA.Row;
      Controle := Geral.IMV(GradeX.Cells[Coluna, Linha]);
      //
      EdPrc.ValueVariant := ObtemPrecoProduto(GraCusPrc, Grade, Controle);
      EdQtd.ValueVariant := 0;
      EdVen.ValueVariant := 0;
      EdComisVal.ValueVariant := 0;
    end else begin
      LaStatus.SQLType := stUpd;
      if QrMovim.Locate('Grade;Cor;Tam', VarArrayOf([Grade, Cor, Tam]), []) then
      begin
        EdQtd.Text := Geral.FFT(QrMovim.FieldByName('Qtd').AsFloat, 3, siPositivo);
        EdQta.Text := Geral.FFT(QrMovim.FieldByName('Qtd').AsFloat, 3, siPositivo);
        EdCua.Text := Geral.FFT(QrMovim.FieldByName('Val').AsFloat /
                                   QrMovim.FieldByName('Qtd').AsFloat, 6, siPositivo);
        EdVla.Text := Geral.FFT(QrMovim.FieldByName('Val').AsFloat, 2, siPositivo);
        EdVen.Text := Geral.FFT(QrMovim.FieldByName('Ven').AsFloat, 2, siPositivo);
        RGComisTip.ItemIndex    := QrMovim.FieldByName('ComisTip').AsInteger;
        EdComisFat.ValueVariant := QrMovim.FieldByName('ComisFat').AsFloat;
        EdComisVal.ValueVariant := QrMovim.FieldByName('ComisVal').AsFloat;
        if EdPrc.Enabled = True then
          CalculaPreco(tcvVal, EdQtd, EdPrc, EdVen,
          RGComisTip, EdComisFat, EdComisVal);
      end else
        LimpaEdits(EdTamCod, EdCorCod, EdTamNom, EdCorNom);
    end;
  end;
  // Pre�o de custo em caso de consigna��o!
  if (EdPrc.Enabled = False) and (Status > 0) then
  begin
    EdPrc.Text := FormatFloat('#,###,##0.000000',
      ObtemCustoProduto(Grade, Cor, Tam));
    CalculaPreco(tcvPrc, EdQtd, EdPrc, EdVen,
    RGComisTip, EdComisFat, EdComisVal);
  end
  // Configura comiss�o se inclus�o de venda
  else if (EdPrc.Enabled = True) and (Status = 1) then
  begin
    {QrComisPrd.Close;
    QrComisPrd.Params[00].AsInteger := RolComis;
    QrComisPrd.Params[01].AsInteger := Grade;
    QrComisPrd.Params[02].AsInteger := Cor;
    QrComisPrd.Params[03].AsInteger := Tam;
    QrComisPrd.Open;
    //
    RGComisTip.ItemIndex    := QrComisPrdComisTip.Value;
    EdComisFat.ValueVariant := QrComisPrdComisQtd.Value;
    }
  end;
  BtConfItem.Enabled := Geral.IntToBool_0(Status);
  EdQtd.SetFocus;
end;

function TDmProd.StatusPedido_Txt(Status: Integer): String;
begin
  case Status of
    -3: Result := '-3: EXTRAVIADO NO RETORNO';
    -2: Result := '-2: EXTRAVIADO NO ENVIO';
    -1: Result := '-1: CANCELADO';
     0: Result := '00: PEDIDO - INCLUINDO';
    10: Result := '10: PEDIDO - INCLU�DO';
    20: Result := '20: PEDIDO - ACEITO';
    30: Result := '30: PEDIDO - PROCESSANDO';
    40: Result := '40: ENVIO - EM VIAGEM';
    50: Result := '50: ENVIO - RECEBIDO';
    60: Result := '60: ITEM VENDIDO';
    // confirmar
    70: Result := '70: DEVOLU��O - INCLUINDO';
    80: Result := '80: DEVOLU��O - EM VIAGEM';
    90: Result := '90: DEVOLU��O - RECEBIDO';
    else Result := FormatFloat('00', Status) + ': STATUS INDEFINIDO';
  end;
end;

function TDmProd.ObtemCustoProduto(Grade, Cor, Tam: Integer): Double;
begin
  QrCustoProd.Close;
  QrCustoProd.Params[00].AsInteger := Grade;
  QrCustoProd.Params[01].AsInteger := Cor;
  QrCustoProd.Params[02].AsInteger := Tam;
  QrCustoProd.Open;
  //
  Result := QrCustoProdCusto.Value;
end;

function TDmProd.ObtemPrecoProduto(GraCusPrc, Grade, Controle: Integer): Double;
begin
  QrLocProd.Close;
  QrLocProd.Params[00].AsInteger := Grade;
  QrLocProd.Params[01].AsInteger := GraCusPrc;
  QrLocProd.Params[02].AsInteger := Controle;
  QrLocProd.Open;
  //
  Result := QrLocProdCustoPreco.Value;
end;

procedure TDmProd.AtualizaGradesAtivos0(Grade: Integer; GradeA, GradeC: TStringGrid);
var
  c, l: Integer;
begin
  ReopenProdutos2(Grade);
  QrProdutos2.First;
  while not QrProdutos2.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutos2Tam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutos2Cor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutos2Controle.Value > 0 then
        begin
          GradeA.Cells[c, l] := IntToStr(QrProdutos2Ativo.Value);
          GradeC.Cells[c, l] := FormatFloat('0', QrProdutos2Controle.Value);
        end else begin
          GradeA.Cells[c, l] := '';
          GradeC.Cells[c, l] := '';
        end;
      end;
    end;
    QrProdutos2.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGradesAtivos1(Tam: Integer; GradeA, GradeC, GradeE: TStringGrid);
var
  c, l: Integer;
begin
  ReopenProdutos2(Tam);
  QrProdutos2.First;
  while not QrProdutos2.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutos2Tam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutos2Cor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutos2Controle.Value > 0 then
        begin
          GradeA.Cells[c, l] := IntToStr(QrProdutos2Ativo.Value);
          GradeE.Cells[c, l] := FloatToStr(QrProdutos2EstqQ.Value);
          GradeC.Cells[c, l] := FormatFloat('0', QrProdutos2Controle.Value);
        end else begin
          GradeA.Cells[c, l] := '';
          GradeE.Cells[c, l] := '';
          GradeC.Cells[c, l] := '';
        end;
      end;
    end;
    QrProdutos2.Next;
  end;
  //
end;

procedure TDmProd.ConfigGrades1(Grades, Tabela: Integer;
GradeA, GradeX, GradeC, GradeP, GradeE: TStringGrid);
var
  c, l: Integer;
begin
  for c := 0 to GradeA.ColCount - 1 do
    for l := 0 to GradeA.RowCount - 1 do
    begin
      if GradeA <> nil then GradeA.Cells[c, l] := '';
      if GradeC <> nil then GradeC.Cells[c, l] := '';
      if GradeP <> nil then GradeP.Cells[c, l] := '';
      if GradeE <> nil then GradeE.Cells[c, l] := '';
      if GradeX <> nil then GradeX.Cells[c, l] := '';
    end;
  if Grades > -1 then
  begin
    ReopenGradesCors(Grades);
    ReopenGradesTams(Grades);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    if GradeA <> nil then
    begin
      GradeA.ColCount  := c;
      GradeA.RowCount  := l;
      GradeA.FixedCols := 1;
      GradeA.FixedRows := 1;
    end;
    //
    if GradeC <> nil then
    begin
      GradeC.ColCount  := c;
      GradeC.RowCount  := l;
      GradeC.FixedCols := 1;
      GradeC.FixedRows := 1;
    end;
    //
    if GradeP <> nil then
    begin
      GradeP.ColCount  := c;
      GradeP.RowCount  := l;
      GradeP.FixedCols := 1;
      GradeP.FixedRows := 1;
    end;
    //
    if GradeE <> nil then
    begin
      GradeE.ColCount  := c;
      GradeE.RowCount  := l;
      GradeE.FixedCols := 1;
      GradeE.FixedRows := 1;
    end;
    //
    if GradeX <> nil then
    begin
      GradeX.ColCount  := c;
      GradeX.RowCount  := l;
    end;
    //
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      if GradeA <> nil then
        GradeA.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeC <> nil then
        GradeC.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeP <> nil then
        GradeP.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeE <> nil then
        GradeE.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      if GradeX <> nil then
        GradeX.Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      if GradeA <> nil then
        GradeA.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeC <> nil then
        GradeC.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeP <> nil then
        GradeP.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeE <> nil then
        GradeE.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      if GradeX <> nil then
        GradeX.Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value);
      QrGradesTams.Next;
    end;
    AtualizaGradesAtivos1(Grades, GradeA, GradeC, GradeE);
    AtualizaGradesPrecos1(Grades, Tabela, GradeP);
  end else begin
    GradeA.ColCount := 1;
    GradeA.RowCount := 1;
    //
    GradeC.ColCount := 1;
    GradeC.RowCount := 1;
    //
    GradeP.ColCount := 1;
    GradeP.RowCount := 1;
    //
    GradeE.ColCount := 1;
    GradeE.RowCount := 1;
    //
    GradeX.ColCount := 1;
    GradeX.RowCount := 1;
    //
  end;
end;

procedure TDmProd.DataModuleCreate(Sender: TObject);
begin
  QrDelMomPto.SQL.Clear;
  QrDelMomPto.SQL.Add('DELETE FROM movim ');
  QrDelMomPto.SQL.Add('WHERE Motivo in (' + FmPrincipal.FMotivosPtos + ') ');
  QrDelMomPto.SQL.Add('AND IDCtrl=:P0');
  //
  QrSelMomPto.Close;
  QrSelMomPto.SQL.Clear;
  QrSelMomPto.SQL.Add('SELECT mom.Conta, mom.Grade, mom.Tam, mom.Cor');
  QrSelMomPto.SQL.Add('FROM movim  mom');
  QrSelMomPto.SQL.Add('WHERE mom.Motivo in (' + FmPrincipal.FMotivosPtos + ') ');
  QrSelMomPto.SQL.Add('AND mom.IDCtrl=:P0');
  //
end;

procedure TDmProd.DeleteItemMovimPonto(IDCtrl: Integer);
var
  Qtd, Val, Cus: Double;
begin
  QrSelMomPto.Close;
  QrSelMomPto.Params[0].AsInteger := IDCtrl;
  QrSelMomPto.Open;
  //
  QrDelMomPto.Params[0].AsInteger := IDCtrl;
  QrDelMomPto.ExecSQL;
  //
  if QrSelMomPto.RecordCount > 0 then
  begin
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'movim', QrSelMomPtoConta.Value);
    //
    AtualizaEstoqueMercadoria(QrSelMomPtoGrade.Value,
      QrSelMomPtoCor.Value, QrSelMomPtoTam.Value,
      True, True, Qtd, Val, Cus);
  end;
end;

procedure TDmProd.GeraBaixaEmMovimDePonto(Motivo, Controle, IDCtrl, Grade,
Cor, Tam: Integer; Qtd, Val: Double; DataPedi: String);
var
  Conta: Integer;
  Cus: Double;
begin
  DeleteItemMovimPonto(IDCtrl);
  Conta := UMyMod.BuscaEmLivreY_Def('movim', 'Conta', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'movim', False, [
  'Controle', 'Grade', 'Cor', 'Tam', 'Qtd', 'Val',
  'DataPedi', 'Motivo', 'IDCtrl'], ['Conta'], [
  Controle, Grade, Cor, Tam, Qtd, Val,
  DataPedi, Motivo, IDCtrl], [Conta], False) then
  begin
    QrSelMomPto.Close;
    QrSelMomPto.Params[0].AsInteger := IDCtrl;
    QrSelMomPto.Open;
    //
    AtualizaEstoqueMercadoria(QrSelMomPtoGrade.Value,
      QrSelMomPtoCor.Value, QrSelMomPtoTam.Value,
      True, True, Qtd, Val, Cus);
  end;
end;

procedure TDmProd.AtualizaGradesPrecos001(Grade, GraCusPrc: Integer; GradeC,
  GradeP_Orig, GradeP_Real: TStringGrid; CustoFin: Double);
var
  c, l: Integer;
  Fator: Double;
  Valor: String;
begin
  Fator := (CustoFin / 100) + 1;
  ReopenGraGruVal(Grade, GraCusPrc);
  QrGraGruVal.First;
  for c := 1 to GradeP_Orig.ColCount - 1 do
    for l := 1 to GradeP_Orig.RowCount - 1 do
    begin
      GradeP_Orig.Cells[c, l] := '';
      GradeP_Real.Cells[c, l] := '';
    end;
  //
  while not QrGraGruVal.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrGraGruValTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrGraGruValCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        Valor := FormatFloat(Dmod.FStrFmtPrc, QrGraGruValCustoPreco.Value * Fator);
        if GradeP_Orig <> nil then
          GradeP_Orig.Cells[c, l] := Valor;
        if GradeP_Real <> nil then
          GradeP_Real.Cells[c, l] := Valor;
      end;
    end;
    QrGraGruVal.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGradesPrecos002(Grade: Integer; GradeC, GradeQ,
  GradeD, GradeP_Orig, GradeP_Real: TStringGrid; Pedido: Integer);
var
  c, l: Integer;
  Valor: String;
begin
  ReopenPtosPedPrc(Pedido);
  QrPtosPedPrc.First;
  for c := 1 to GradeP_Orig.ColCount - 1 do
    for l := 1 to GradeP_Orig.RowCount - 1 do
    begin
      GradeP_Orig.Cells[c, l] := '';
      GradeP_Real.Cells[c, l] := '';
    end;
  //
  while not QrPtosPedPrc.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrPtosPedPrcTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrPtosPedPrcCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        //
        Valor := FormatFloat(Dmod.FStrFmtPrc, QrPtosPedPrcPrecoO.Value);
        if GradeP_Orig <> nil then
          GradeP_Orig.Cells[c, l] := Valor;
        //
        Valor := FormatFloat(Dmod.FStrFmtPrc, QrPtosPedPrcPrecoR.Value);
        if GradeP_Real <> nil then
          GradeP_Real.Cells[c, l] := Valor;
        //
        Valor := FormatFloat(Dmod.FStrFmtPrc, QrPtosPedPrcDescoP.Value);
        if GradeD <> nil then
          GradeD.Cells[c, l] := Valor;
        //
        Valor := Geral.FFT(QrPtosPedPrcQuanti.Value,
          Dmod.QrControleCasasProd.Value, siNegativo);
        if GradeQ <> nil then
          GradeQ.Cells[c, l] := Valor;
      end;
    end;
    QrPtosPedPrc.Next;
  end;
  //
end;

procedure TDmProd.AtualizaGradesPrecos1(GraGru1, GraCusPrc: Integer; GradeP: TStringGrid);
var
  c, l: Integer;
  StrFmt: String;
begin
  if GradeP = nil then Exit;
  //
  StrFmt := MLAGeral.StrFmt_Double(2);
  ReopenGraGruVal(GraGru1, GraCusPrc);
  QrGraGruVal.First;
  for c := 1 to GradeP.ColCount - 1 do
    for l := 1 to GradeP.RowCount - 1 do
      GradeP.Cells[c, l] := '';
  //
  while not QrGraGruVal.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrGraGruValTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrGraGruValCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        GradeP.Cells[c, l] := FormatFloat(StrFmt, QrGraGruValCustoPreco.Value);
      end;
    end;
    QrGraGruVal.Next;
  end;
  //
end;

procedure TDmProd.ReopenGraGruVal(GraGru1, GraCusPrc: Integer);
begin
  QrGraGruVal.Close;
  QrGraGruVal.Params[00].AsInteger := GraGru1;
  QrGraGruVal.Params[01].AsInteger := GraCusPrc;
  QrGraGruVal.Open;
end;

procedure TDmProd.ReopenProdutos2(GraGruT: Integer);
begin
  QrProdutos2.Close;
  QrProdutos2.Params[00].AsInteger := GraGruT;
  QrProdutos2.Open;
end;

procedure TDmProd.ReopenPtosPedPrc(Pedido: Integer);
begin
  QrPtosPedPrc.Close;
  QrPtosPedPrc.Params[0].AsInteger := Pedido;
  QrPtosPedPrc.Open;
end;

function TDmProd.PreparaGrades(Grade: Integer; StrGrids: array of TStringGrid): Boolean;
var
  Cols, Rows, i, j, k, c, l: Integer;
begin
  Result := False;
  j := High(StrGrids);
  k := Low(StrGrids);
  //
  Cols := StrGrids[0].ColCount;
  Rows := StrGrids[0].RowCount;
  //
  for c := 0 to Cols - 1 do
    for l := 0 to Rows - 1 do
      for i := k to j do
        if StrGrids[i] <> nil then
          StrGrids[i].Cells[c,l] := '';
  if Grade > -1 then
  begin
    ReopenGradesCors(Grade);
    ReopenGradesTams(Grade);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        StrGrids[i].Cells[c,l] := '';
        StrGrids[i].ColCount  := c;
        StrGrids[i].RowCount  := l;
        StrGrids[i].FixedCols := 1;
        StrGrids[i].FixedRows := 1;
      end;
    end;
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      for i := k to j do
      begin
        if StrGrids[i] <> nil then
        begin
          if StrGrids[i].Name = 'GradeX' then
            StrGrids[i].Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value)
          else
            StrGrids[i].Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
        end;
      end;
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      for i := k to j do
        if StrGrids[i] <> nil then
        begin
          if StrGrids[i].Name = 'GradeX' then
            StrGrids[i].Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value)
          else
            StrGrids[i].Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
        end;
      QrGradesTams.Next;
    end;
    Result := True;
  end else begin
    for i := k to j do
    begin
      if StrGrids[i] <> nil then
      begin
        StrGrids[i].ColCount  := 0;
        StrGrids[i].RowCount  := 0;
      end;
    end;
  end;
end;

procedure TDmProd.QrPedItsAnulCalcFields(DataSet: TDataSet);
begin
  QrPedItsAnulNO_STATUS.Value := StatusPedido_Txt(QrPedItsAnulStatus.Value);
end;

end.

