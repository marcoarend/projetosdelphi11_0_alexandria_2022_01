object FmMasterSelFilial: TFmMasterSelFilial
  Left = 339
  Top = 185
  Caption = '@MF-SELEC-001 :: Sele'#231#227'o de Filial'
  ClientHeight = 156
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 108
    Width = 470
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 358
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 470
    Height = 48
    Align = alTop
    Caption = 'Sele'#231#227'o de Filial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 468
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 470
    Height = 60
    Align = alClient
    TabOrder = 0
    object dmkLabel1: TdmkLabel
      Left = 16
      Top = 12
      Width = 23
      Height = 13
      Caption = 'Filial:'
      UpdType = utYes
      SQLType = stNil
    end
    object EdFilial: TdmkEditCB
      Left = 16
      Top = 28
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBFilial
    end
    object CBFilial: TdmkDBLookupComboBox
      Left = 72
      Top = 28
      Width = 389
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DsFiliais
      TabOrder = 1
      dmkEditCB = EdFilial
      UpdType = utYes
    end
  end
  object QrFiliais: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL'
      'FROM entidades'
      'WHERE Codigo<-10'
      'ORDER BY NOMEFILIAL')
    Left = 228
    Top = 56
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 256
    Top = 56
  end
end
