unit MoviVImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, UnFinanceiro, frxClass, frxDBSet, dmkGeral,
  dmkPermissoes, UnDmkProcFunc;

type
  TFmMoviVImp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    frxPedidosV: TfrxReport;
    frxDsMoviV: TfrxDBDataset;
    frxDsMovim: TfrxDBDataset;
    frxDsMoviVK: TfrxDBDataset;
    frxDsMoviVKIts: TfrxDBDataset;
    frxDsCli: TfrxDBDataset;
    frxDsMoviVStat: TfrxDBDataset;
    dmkPermissoes2: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxPedidosVGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmMoviVImp: TFmMoviVImp;

implementation

{$R *.DFM}

uses Module, MyListas, ModuleGeral, ModuleFin, ModuleProd, MoviV, UnMyObjects;

procedure TFmMoviVImp.BtOKClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxPedidosV, 'Pedidos');
end;

procedure TFmMoviVImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviVImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviVImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMoviVImp.frxPedidosVGetValue(const VarName: string;
  var Value: Variant);
var
  Caminho: String;
  Existe: Boolean;
begin
  Caminho := Dmod.QrControleLogoMoviV.Value;
  if (Length(Caminho) > 0) and (FileExists(Caminho) = True) then
    Existe := True
  else
    Existe := False;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE') = 0 then
    Value := Existe
  else if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
    Value := dmkPF.ParValueCodTxt('Cliente: ', FmMoviV.QrMoviVNOMEENTI.Value, FmMoviV.QrMoviVCliente.Value)
  else if AnsiCompareText(VarName, 'VAR_IMG') = 0 then
    Value := Caminho
  else if AnsiCompareText(VarName, 'VAR_TOTPED') = 0 then
    Value := FmMoviV.QrTotMovimTOTITENS.Value + FmMoviV.QrTotMoviVKTOTFRETE.Value +
      FmMoviV.QrTotMoviVKItsTOTKITITS.Value
  else if AnsiCompareText(VarName, 'VAR_DESCO') = 0 then
    Value := FmMoviV.QrMoviVDescon.Value
  else if AnsiCompareText(VarName, 'VAR_TOTVDA') = 0 then
    Value := FmMoviV.QrTotMovimTOTITENS.Value + FmMoviV.QrTotMoviVKTOTFRETE.Value +
      FmMoviV.QrTotMoviVKItsTOTKITITS.Value + FmMoviV.QrMoviVFrete.Value -
      FmMoviV.QrMoviVDescon.Value;
end;

end.

