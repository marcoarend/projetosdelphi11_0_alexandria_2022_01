unit PtosPedCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, ComCtrls, dmkEditDateTimePicker, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, UnDmkProcFunc, UnDmkEnums;

type
  TFmPtosPedCad = class(TForm)
    PainelDados: TPanel;
    DsPtosPedCad: TDataSource;
    QrPtosPedCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    PMPedido: TPopupMenu;
    BtPedido: TBitBtn;
    QrPtosCadPto: TmySQLQuery;
    DsPtosCadPto: TDataSource;
    LaCliente: TLabel;
    EdPontoVda: TdmkEditCB;
    CBPontoVda: TdmkDBLookupComboBox;
    dmkLabel1: TdmkLabel;
    dmkLabel2: TdmkLabel;
    dmkLabel3: TdmkLabel;
    dmkLabel4: TdmkLabel;
    Label2: TLabel;
    EdGraCusPrc: TdmkEditCB;
    CBGraCusPrc: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    QrGraCusPrc: TmySQLQuery;
    DsGraCusPrc: TDataSource;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    EdDataPed: TdmkEdit;
    EdDataLib: TdmkEdit;
    EdDataEnv: TdmkEdit;
    EdDataRec: TdmkEdit;
    Incluinovopedido1: TMenuItem;
    Alterapedidoatual1: TMenuItem;
    Excluipedidoatual1: TMenuItem;
    QrPtosCadPtoCodigo: TIntegerField;
    QrPtosCadPtoCodUsu: TIntegerField;
    QrPtosCadPtoNome: TWideStringField;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    dmkLabel6: TdmkLabel;
    dmkLabel7: TdmkLabel;
    dmkLabel8: TdmkLabel;
    dmkLabel5: TdmkLabel;
    QrPtosPedCadCodigo: TIntegerField;
    QrPtosPedCadCodUsu: TIntegerField;
    QrPtosPedCadPontoVda: TIntegerField;
    QrPtosPedCadDataPed: TDateTimeField;
    QrPtosPedCadDataLib: TDateTimeField;
    QrPtosPedCadDataEnv: TDateTimeField;
    QrPtosPedCadDataRec: TDateTimeField;
    QrPtosPedCadGraCusPrc: TIntegerField;
    QrPtosPedCadValor: TFloatField;
    QrPtosPedCadLk: TIntegerField;
    QrPtosPedCadDataCad: TDateField;
    QrPtosPedCadDataAlt: TDateField;
    QrPtosPedCadUserCad: TIntegerField;
    QrPtosPedCadUserAlt: TIntegerField;
    QrPtosPedCadAlterWeb: TSmallintField;
    QrPtosPedCadAtivo: TSmallintField;
    QrPtosPedCadNO_PontoVda: TWideStringField;
    QrPtosPedCadNO_GraCusPrc: TWideStringField;
    Label5: TLabel;
    DBEdit7: TDBEdit;
    QrPtosPedCadCU_PontoVda: TIntegerField;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    QrPtosPedCadDATALIB_TXT: TWideStringField;
    QrPtosPedCadDATAPED_TXT: TWideStringField;
    QrPtosPedCadDATAENV_TXT: TWideStringField;
    QrPtosPedCadDATAREC_TXT: TWideStringField;
    PMItens: TPopupMenu;
    Incluiitensporgrade1: TMenuItem;
    QrPtosPedGru: TmySQLQuery;
    DsPtosPedGru: TDataSource;
    PnGrids: TPanel;
    DBGrid1: TDBGrid;
    DBEdit10: TDBEdit;
    dmkLabel9: TdmkLabel;
    EdAbertura: TdmkEdit;
    dmkLabel10: TdmkLabel;
    QrPtosPedCadAbertura: TDateTimeField;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    TabSheet4: TTabSheet;
    GradeD: TStringGrid;
    TabSheet1: TTabSheet;
    GradeL: TStringGrid;
    TabSheet2: TTabSheet;
    GradeF: TStringGrid;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrPtosPedGruCodigo: TIntegerField;
    QrPtosPedGruNome: TWideStringField;
    IncluiitensporLeitura1: TMenuItem;
    BtVolta: TBitBtn;
    BtAvanca: TBitBtn;
    QrPtosPedCadStatus: TSmallintField;
    DBEdit11: TDBEdit;
    dmkLabel11: TdmkLabel;
    QrPtosPedCadNO_Status: TWideStringField;
    QrPtosPedCadOndeCad: TSmallintField;
    TabSheet7: TTabSheet;
    N1: TMenuItem;
    Cancelaritens1: TMenuItem;
    QrPtosStqMov: TmySQLQuery;
    QrPtosStqMovIDCtrl: TIntegerField;
    QrPtosStqMovDataIns: TDateTimeField;
    QrPtosStqMovStatus: TSmallintField;
    QrPtosStqMovQuanti: TFloatField;
    QrPtosStqMovPrecoR: TFloatField;
    QrPtosStqMovNO_Produto: TWideStringField;
    QrPtosStqMovNO_Cor: TWideStringField;
    QrPtosStqMovCU_Tam: TIntegerField;
    QrPtosStqMovNO_Tam: TWideStringField;
    QrPtosStqMovNO_Status: TWideStringField;
    QrPtosStqMovGrade: TIntegerField;
    QrPtosStqMovProduto: TIntegerField;
    DsPtosStqMov: TDataSource;
    dmkDBGHist: TdmkDBGrid;
    QrPtosStqMovCor: TIntegerField;
    QrPtosStqMovTam: TIntegerField;
    TabSheet8: TTabSheet;
    DBGrid2: TDBGrid;
    QrMovEnv: TmySQLQuery;
    DsMovEnv: TDataSource;
    QrMovEnvControle: TIntegerField;
    QrMovEnvConta: TIntegerField;
    QrMovEnvGrade: TIntegerField;
    QrMovEnvCor: TIntegerField;
    QrMovEnvTam: TIntegerField;
    QrMovEnvQtd: TFloatField;
    QrMovEnvVal: TFloatField;
    QrMovEnvVen: TFloatField;
    QrMovEnvDataPedi: TDateField;
    QrMovEnvDataReal: TDateField;
    QrMovEnvMotivo: TSmallintField;
    QrMovEnvSALDOQTD: TFloatField;
    QrMovEnvSALDOVAL: TFloatField;
    QrMovEnvLk: TIntegerField;
    QrMovEnvDataCad: TDateField;
    QrMovEnvDataAlt: TDateField;
    QrMovEnvUserCad: TIntegerField;
    QrMovEnvUserAlt: TIntegerField;
    QrMovEnvAlterWeb: TSmallintField;
    QrMovEnvAtivo: TSmallintField;
    QrMovEnvSubCtrl: TIntegerField;
    QrMovEnvComisTip: TSmallintField;
    QrMovEnvComisFat: TFloatField;
    QrMovEnvComisVal: TFloatField;
    QrMovEnvSubCta: TIntegerField;
    QrMovEnvKit: TIntegerField;
    QrMovEnvIDCtrl: TIntegerField;
    QrPtosCadPtoGraCusPrc: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPtosPedCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPtosPedCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtPedidoClick(Sender: TObject);
    procedure PMPedidoPopup(Sender: TObject);
    procedure QrPtosPedCadAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Incluinovopedido1Click(Sender: TObject);
    procedure Alterapedidoatual1Click(Sender: TObject);
    procedure QrPtosPedCadCalcFields(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluiitensporgrade1Click(Sender: TObject);
    procedure QrPtosPedGruAfterScroll(DataSet: TDataSet);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeLDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure IncluiitensporLeitura1Click(Sender: TObject);
    procedure QrPtosPedGruBeforeClose(DataSet: TDataSet);
    procedure BtVoltaClick(Sender: TObject);
    procedure BtAvancaClick(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrPtosStqMovCalcFields(DataSet: TDataSet);
    procedure dmkDBGHistDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdPontoVdaChange(Sender: TObject);
    procedure Cancelaritens1Click(Sender: TObject);
  private
    FPedItsAnul: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReconfiguraGradeQ();
    procedure AvancaEtapa();
    procedure VoltaEtapa();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPtosPedGru(_: Integer);
    procedure ReopenPtosStqMov(IDCtrl: Integer);
    procedure ReopenMovEnv(IDCtrl: Integer);
    procedure IncluiItensPorGrade(SQLType: TSQLType);
    procedure IncluiItensPorLeitura(SQLType: TSQLType);
  end;

var
  FmPtosPedCad: TFmPtosPedCad;
const
  FFormatFloat   = '00000';

implementation

uses Module, MyDBCheck, PtosCadPto, GraCusPrc, ModuleGeral, PtosPedGru,
  ModuleProd, UnMyObjects, PtosPedLei, UCreate, PtosPedAnul, Ponto, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPtosPedCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPtosPedCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPtosPedCadCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPtosPedCad.DefParams;
begin
  VAR_GOTOTABELA := 'ptospedcad';
  VAR_GOTOMYSQLTABLE := QrPtosPedCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ppc.*, pcp.Nome NO_PontoVda, pcp.CodUsu CU_PontoVda,');
  VAR_SQLx.Add('gcp.Nome NO_GraCusPrc');
  VAR_SQLx.Add('FROM ptospedcad ppc');
  VAR_SQLx.Add('LEFT JOIN ptoscadpto pcp ON pcp.Codigo=ppc.PontoVda');
  VAR_SQLx.Add('LEFT JOIN gracusprc gcp ON gcp.Codigo=ppc.GraCusPrc');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE ppc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND ppc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND ppc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ppc.Nome Like :P0');
  //
end;

procedure TFmPtosPedCad.dmkDBGHistDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'NO_Status') then
  begin
    if      QrPtosStqMovStatus.Value < -1 then Cor := clGray
    else if QrPtosStqMovStatus.Value = -1 then Cor := clRed
    else if QrPtosStqMovStatus.Value < 40 then Cor := clBlue
    else if QrPtosStqMovStatus.Value = 50 then Cor := clGreen
    else if QrPtosStqMovStatus.Value < 90 then Cor := clPurple
    else Cor := clBlack;
    with dmkDBGHist.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmPtosPedCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'ptospedcad', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmPtosPedCad.EdPontoVdaChange(Sender: TObject);
begin
  if (LaTipo.SQLType = stIns) then
  begin
    EdGraCusPrc.ValueVariant := QrPtosCadPtoGraCusPrc.Value;
    CBGraCusPrc.KeyValue     := QrPtosCadPtoGraCusPrc.Value;
  end;
end;

procedure TFmPtosPedCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPtosPedCad.PMItensPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrPtosPedCad.State <> dsInactive) and
    (QrPtosPedCadStatus.Value = 0);
  Incluiitensporgrade1.Enabled := Habilita;
  IncluiitensporLeitura1.Enabled := Habilita;
  //
  Habilita :=
    (QrPtosPedGru.State <> dsInactive) and
    (QrPtosPedGru.RecordCount > 0);
  Cancelaritens1.Enabled := Habilita;
end;

procedure TFmPtosPedCad.PMPedidoPopup(Sender: TObject);
begin
  Alterapedidoatual1.Enabled :=
    (QrPtosPedCad.State <> dsInactive) and
    (QrPtosPedCad.RecordCount > 0) and
    (QrPtosPedCadStatus.Value < 10);
end;

procedure TFmPtosPedCad.Cancelaritens1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    FPedItsAnul := UCriar.RecriaTempTable('peditsanul', DmodG.QrUpdPID1, False);
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FPedItsAnul);
    //QrUpdPID1.SQL.Add('SELECT IDCtrl, IF(Status<0,1,0) Ativo');
    DmodG.QrUpdPID1.SQL.Add('SELECT IDCtrl, 0 Ativo');
    DmodG.QrUpdPID1.SQL.Add('FROM lesew.ptosstqmov');
    DmodG.QrUpdPID1.SQL.Add('WHERE CodInn=:P0');
    DmodG.QrUpdPID1.SQL.Add('AND Status>=0');
    DmodG.QrUpdPID1.Params[0].AsInteger := QrPtosPedCadCodigo.Value;
    DmodG.QrUpdPID1.ExecSQL;
    //
    DmProd.ReopenPedItsAnul(2);
  finally
    Screen.Cursor := crDefault;
  end;
  if DmProd.QrPedItsAnul.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� itens a serem cancelados!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DBCheck.CriaFm(TFmPtosPedAnul, FmPtosPedAnul, afmoNegarComAviso) then
  begin
    FmPtosPedAnul.ShowModal;
    FmPtosPedAnul.Destroy;
    ReopenPtosStqMov(QrPtosStqMovIDCtrl.Value);
    ReopenMovEnv(QrMovEnvIDCtrl.Value);
  end;
end;

procedure TFmPtosPedCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPtosPedCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPtosPedCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPtosPedCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPtosPedCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPtosPedCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPtosPedCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPtosPedCad.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmPtosCadPto, FmPtosCadPto, afmoNegarComAviso) then
  begin
    FmPtosCadPto.ShowModal;
    FmPtosCadPto.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrPtosCadPto.Close;
      QrPtosCadPto.Open;
      if QrPtosCadPto.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdPontoVda.ValueVariant := VAR_CADASTRO;
        CBPontoVda.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPtosPedCad.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrGraCusPrc.Close;
      QrGraCusPrc.Open;
      if QrGraCusPrc.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdGraCusPrc.ValueVariant := VAR_CADASTRO;
        CBGraCusPrc.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmPtosPedCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPtosPedCadCodigo.Value;
  Close;
end;

procedure TFmPtosPedCad.BtVoltaClick(Sender: TObject);
begin
  VoltaEtapa();
end;

procedure TFmPtosPedCad.VoltaEtapa();
var
  Status: Integer;
  Continua: Word;
  Msg: String;
begin
  Status   := 0;
  Continua := ID_NO;
  Msg := '';
  case QrPtosPedCadStatus.Value of
    0: Msg := 'N�o h� etapa anterior. Esta � a primeira etapa!';
    10:
    begin
      Status := 0;
      Continua := ID_YES;
    end;
    20:
    begin
      Status := 10;
      Continua := ID_YES;
    end;
    30:
    begin
      Status := 20;
      Continua := ID_YES;
    end;
    40:
    begin
      QrMovEnv.First;
      while not QrMovEnv.Eof do
      begin
        DmProd.DeleteItemMovimPonto(QrMovEnvIDCtrl.Value);
        QrMovEnv.Next;
      end;
      Status := 30;
      Continua := ID_YES;
    end;
    50:
    begin
      Status := 40;
      Continua := ID_YES;
    end;
    {
    9:
    begin
      if DmCiclos.PodeReabrirCiclo(QrCiclosCodigo.Value, QrCiclosProfessor.Value) then
      Continua := ID_YES;
    end;
    else Continua := ID_YES;
    }
  end;
  if Msg <> '' then
    Geral.MB_Aviso(Msg);
  //
  if Continua <> ID_YES then Exit;
  if QrPtosPedCadStatus.Value > 0 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE ptosstqmov SET Status=:P0');
    DMod.QrUpd.SQL.Add('WHERE CodInn=:Pa AND Status=:Pb');
    DMod.QrUpd.Params[00].AsInteger := Status;
    DMod.QrUpd.Params[01].AsInteger := QrPtosPedCadCodigo.Value;
    DMod.QrUpd.Params[02].AsInteger := QrPtosPedCadStatus.Value;
    DMod.QrUpd.ExecSQL;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptospedcad', False,
    ['status'], ['codigo'], [Status], [QrPtosPedCadCodigo.Value], True) then
    LocCod(QrPtosPedCadCodigo.Value, QrPtosPedCadCodigo.Value);
  end;
end;

procedure TFmPtosPedCad.BtPedidoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPedido, BtPedido);
end;

procedure TFmPtosPedCad.Alterapedidoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPtosPedCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'ptospedcad');
  EdPontoVda.Enabled := QrPtosPedGru.RecordCount > 0;
  EdPontoVda.Enabled := QrPtosPedGru.RecordCount > 0;
end;

procedure TFmPtosPedCad.BtAvancaClick(Sender: TObject);
begin
  AvancaEtapa();
end;

procedure TFmPtosPedCad.AvancaEtapa();
var
  Continua: Word;
  Status: Integer;
  Agora: String;
  //
  Motivo, Controle, IDCtrl, Grade, Cor, Tam: Integer;
  Qtd, Val: Double;
  DataPedi: String;
begin
  Status := QrPtosPedCadStatus.Value;
  Agora := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  Continua := ID_NO;
  case QrPtosPedCadStatus.Value of
    0:
    begin
      Status := 10;
      if QrPtosPedGru.RecordCount = 0 then
        Geral.MB_Aviso('Avan�o abortado! N�o foi inclu�da nenhuma mercadoria!')
      else if QrPtosPedCadOndeCad.Value > 0 then
      begin
        Continua := Geral.MB_Pergunta('Este pedido foi criado na web! Deseja ' +
          'encerrar a inclus�o de itens neste aplicativo assim mesmo?');
      end
      else
        Continua := ID_YES;
      if Continua = ID_YES then
      begin
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptospedcad', False,
        ['DataPed'], ['Codigo'], [Agora], [QrPtosPedCadCodigo.Value], True) then
          Continua := ID_NO;
      end;
    end;
    10:
    begin
      Status := 20;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptospedcad', False,
      ['DataLib'], ['Codigo'], [Agora], [QrPtosPedCadCodigo.Value], True) then
        Continua := ID_YES;
    end;
    20:
    begin
      Status := 30;
      Continua := ID_YES;
    end;
    30:
    begin
      Status := 40;
      DataPedi := Geral.FDT(QrPtosPedCadDataPed.Value, 1);
      Motivo   := 25; // //25- Sa�da por pedido para ponto de venda
      Qtd      := 1;
      Controle := -QrPtosPedCadCodigo.Value; // Negativo para autom�ticos
      //
      QrPtosStqMov.First;
      while not QrPtosStqMov.Eof do
      begin
        if QrPtosStqMovStatus.Value = 30 then
        begin
          IDCtrl   := QrPtosStqMovIDCtrl.Value;
          Grade    := QrPtosStqMovGrade.Value;
          Cor      := QrPtosStqMovCor.Value;
          Tam      := QrPtosStqMovTam.Value;
          Val      := QrPtosStqMovPrecoR.Value;
          DmProd.GeraBaixaEmMovimDePonto(Motivo, Controle, IDCtrl, Grade,
            Cor, Tam, Qtd, Val, DataPedi);
        end;    
        //
        QrPtosStqMov.Next;
      end;
      Continua := ID_YES;
    end;
    40:
    begin
      Status := 50;
      Continua := ID_YES;
    end;
    {
    // N�o impedir ir ao nove
    40..90: Continua := ID_YES;
    }
  end;
  {
  if QrPtosPedCadStatus.Value >= FMaxStatusedit then
    Status := 40;
  }
  if Status <> QrPtosPedCadStatus.Value then
  begin
    if Continua = ID_YES then
    begin
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('UPDATE ptosstqmov SET Status=:P0');
      DMod.QrUpd.SQL.Add('WHERE CodInn=:Pa AND Status=:Pb');
      DMod.QrUpd.Params[00].AsInteger := Status;
      DMod.QrUpd.Params[01].AsInteger := QrPtosPedCadCodigo.Value;
      DMod.QrUpd.Params[02].AsInteger := QrPtosPedCadStatus.Value;
      DMod.QrUpd.ExecSQL;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ptospedcad', False,
      ['Status'], ['Codigo'], [Status], [QrPtosPedCadCodigo.Value], True) then
      LocCod(QrPtosPedCadCodigo.Value, QrPtosPedCadCodigo.Value);
    end;
  end;
end;

procedure TFmPtosPedCad.BtConfirmaClick(Sender: TObject);
var
  Ponto, GraCusPrc, Codigo: Integer;
begin
  Ponto := Geral.IMV(EdPontoVda.Text);
  if Ponto = 0 then
  begin
    Application.MessageBox('Defina uma ponto de venda!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdPontoVda.SetFocus;
    Exit;
  end;
  GraCusPrc := Geral.IMV(EdGraCusPrc.Text);
  if GraCusPrc = 0 then
  begin
    Application.MessageBox('Defina uma lista de pre�os!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdGraCusPrc.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('ptospedcad', 'Codigo', LaTipo.SQLType,
    QrPtosPedCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPtosPedCad, PainelEdit,
    'ptospedcad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPtosPedCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'ptospedcad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ptospedcad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ptospedcad', 'Codigo');
end;

procedure TFmPtosPedCad.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmPtosPedCad.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeL.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  //
  PainelEdit.Align  := alClient;
  PnGrids.Align  := alClient;
  CriaOForm;
  QrPtosCadPto.Open;
  QrGraCusPrc.Open;
  //
end;

procedure TFmPtosPedCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPtosPedCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPtosPedCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPtosPedCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPtosPedCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPtosPedCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPtosPedCad.QrPtosPedCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPtosPedCad.QrPtosPedCadAfterScroll(DataSet: TDataSet);
begin
  ReopenPtosPedGru(0);
end;

procedure TFmPtosPedCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosPedCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPtosPedCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ptospedcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPtosPedCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPtosPedCad.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmPtosPedCad.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPtosPedCad.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc + ' %', 0, 0, False);
end;

procedure TFmPtosPedCad.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, GradeL, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmPtosPedCad.GradeLDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeL, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, True);
end;

procedure TFmPtosPedCad.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmPtosPedCad.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, nil, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPtosPedCad.Incluiitensporgrade1Click(Sender: TObject);
begin
  IncluiItensPorGrade(stIns);
end;

procedure TFmPtosPedCad.IncluiitensporLeitura1Click(Sender: TObject);
begin
  IncluiItensPorLeitura(stIns);
end;

procedure TFmPtosPedCad.IncluiItensPorLeitura(SQLType: TSQLType);
begin
  //DmPtosPed.ReopenParamsEmp(QrPtosPedEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPtosPedLei, FmPtosPedLei, afmoNegarComAviso) then
  begin
    FmPtosPedLei.LaTipo.SQLType := SQLType;
    FmPtosPedLei.ShowModal;
    FmPtosPedLei.Destroy;
    ReopenPtosPedGru(0);
    //ReopenPtosPedGru(QrPtosPedGruNivel1.Value);
  end;
end;

procedure TFmPtosPedCad.IncluiItensPorGrade(SQLType: TSQLType);
begin
  //DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPtosPedGru, FmPtosPedGru, afmoNegarComAviso) then
  begin
    FmPtosPedGru.LaTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmPtosPedGru.PnSeleciona.Enabled    := False;
      {
      FmPtosPedGru.EdGraGru1.ValueVariant := QrPediVdaGruNivel1.Value;
      FmPtosPedGru.CBGraGru1.KeyValue     := QrPediVdaGruNivel1.Value;
      }
    end;
    FmPtosPedGru.ShowModal;
    FmPtosPedGru.Destroy;
    ReopenPtosStqMov(QrPtosStqMovIDCtrl.Value);
    ReopenMovEnv(QrMovEnvIDCtrl.Value);
  end;
end;

procedure TFmPtosPedCad.Incluinovopedido1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPtosPedCad, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'ptospedcad');
  Agora := DModG.ObtemAgora();
  EdAbertura.ValueVariant :=  Agora;
  EdPontoVda.Enabled := True;
  EdPontoVda.Enabled := True;
end;

procedure TFmPtosPedCad.QrPtosPedCadBeforeOpen(DataSet: TDataSet);
begin
  QrPtosPedCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPtosPedCad.QrPtosPedCadCalcFields(DataSet: TDataSet);
begin
  QrPtosPedCadDATAPED_TXT.Value := Geral.FDT(QrPtosPedCadDataPed.Value, 0);
  QrPtosPedCadDATALIB_TXT.Value := Geral.FDT(QrPtosPedCadDataLib.Value, 0);
  QrPtosPedCadDATAENV_TXT.Value := Geral.FDT(QrPtosPedCadDataEnv.Value, 0);
  QrPtosPedCadDATAREC_TXT.Value := Geral.FDT(QrPtosPedCadDataRec.Value, 0);
  //
  QrPtosPedCadNO_Status.Value :=
    DmProd.StatusPedido_Txt(QrPtosPedCadStatus.Value);
end;

procedure TFmPtosPedCad.QrPtosPedGruAfterScroll(DataSet: TDataSet);
begin
  ReconfiguraGradeQ();
end;

procedure TFmPtosPedCad.QrPtosPedGruBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades(
    [GradeQ, GradeD, GradeL, GradeF, GradeC, GradeA, GradeX], 0, 0, True);
end;

procedure TFmPtosPedCad.QrPtosStqMovCalcFields(DataSet: TDataSet);
begin
  QrPtosStqMovNO_Status.Value :=
    DmProd.StatusPedido_Txt(QrPtosStqMovStatus.Value);
end;

procedure TFmPtosPedCad.ReconfiguraGradeQ();
var
  Grade, Pedido: Integer;
begin
  Grade  := QrPtosPedGruCodigo.Value;
  Pedido := QrPtosPedCadCodigo.Value;
  DmProd.ConfigGrades002(Grade,
   GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD, Pedido);
end;

procedure TFmPtosPedCad.ReopenMovEnv(IDCtrl: Integer);
begin
  QrMovEnv.Close;
  QrMovEnv.SQL.Clear;
  QrMovEnv.SQL.Add('SELECT mom.*');
  QrMovEnv.SQL.Add('FROM movim mom');
  QrMovEnv.SQL.Add('WHERE mom.Motivo in (' + FmPrincipal.FMotivosPtos + ')');
  QrMovEnv.SQL.Add('AND mom.Controle=' + FormatFloat('0', -QrPtosPedCadCodigo.Value));
  QrMovEnv.Open;
end;

procedure TFmPtosPedCad.ReopenPtosPedGru(_: Integer);
begin
  QrPtosPedGru.Close;
  QrPtosPedGru.Params[0].AsInteger := QrPtosPedCadCodigo.Value;
  QrPtosPedGru.Open;
  //
  ReopenPtosStqMov(0);
  ReopenMovEnv(0);
end;

procedure TFmPtosPedCad.ReopenPtosStqMov(IDCtrl: Integer);
begin
  QrPtosStqMov.Close;
  QrPtosStqMov.Params[0].AsInteger := QrPtosPedCadCodigo.Value;
  QrPtosStqMov.Open;
  //
  if IDCtrl <> 0 then
    QrPtosStqMov.Locate('IDCtrl', IDCtrl, []);
end;

{
Parei aqui

lista de pre�o padrao por cliente

terminar steps?
reverter cancelados?
atualizar estoques de movim quando excluidos no 40 > 30 > 40
  - estoques PediQ ou EstqQ?
Relat�rio de estoque de ponto (40,50,70,80)
Fazer venda de ponto
Fazer devolu��o de ponto

}
end.

