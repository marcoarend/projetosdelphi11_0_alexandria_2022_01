unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*) TypInfo, StdCtrls,
  ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg, mySQLDbTables,
  Buttons, WinSkinStore, WinSkinData, Mask, DBCtrls, frxBarcode, frxClass,
  dmkEdit, dmkGeral, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, Sockets, Variants, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TRGBArray = array[Word] of TRGBTriple;
  pRGBArray = ^TRGBArray;

  TcpCalc = (cpJurosMes, cpMulta);
  TFmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Sair1: TMenuItem;
    Logoffde1: TMenuItem;
    Cadastros1: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    Ferramentas1: TMenuItem;
    Imagemdefundo1: TMenuItem;
    Entidades1: TMenuItem;
    BD1: TMenuItem;
    Verifica1: TMenuItem;
    VCLSkin1: TMenuItem;
    Timer1: TTimer;
    Backup1: TMenuItem;
    Diversos1: TMenuItem;
    Panel1: TPanel;
    BtEntidades2: TBitBtn;
    BtProtecao: TBitBtn;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    QrView1: TmySQLQuery;
    QrView1Codigo: TIntegerField;
    QrView1Campo: TIntegerField;
    QrView1Topo: TIntegerField;
    QrView1MEsq: TIntegerField;
    QrView1Larg: TIntegerField;
    QrView1FTam: TIntegerField;
    QrView1Negr: TIntegerField;
    QrView1Ital: TIntegerField;
    QrView1Lk: TIntegerField;
    QrView1DataCad: TDateField;
    QrView1DataAlt: TDateField;
    QrView1UserCad: TIntegerField;
    QrView1UserAlt: TIntegerField;
    QrView1Altu: TIntegerField;
    QrView1Pref: TWideStringField;
    QrView1Sufi: TWideStringField;
    QrView1Padr: TWideStringField;
    QrTopos1: TmySQLQuery;
    QrTopos1TopR: TIntegerField;
    QrView1AliV: TIntegerField;
    QrView1AliH: TIntegerField;
    QrView1TopR: TIntegerField;
    QrView1PrEs: TIntegerField;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    Seleciona1: TMenuItem;
    Limpa1: TMenuItem;
    Opes2: TMenuItem;
    BtPlaCtas: TBitBtn;
    PMPlaCtas: TPopupMenu;
    Plano3: TMenuItem;
    Conjutos2: TMenuItem;
    Grupos3: TMenuItem;
    Subgrupos3: TMenuItem;
    Contas3: TMenuItem;
    Todosnveis2: TMenuItem;
    N12: TMenuItem;
    Listaplanos2: TMenuItem;
    N11: TMenuItem;
    Saldoinicialdeconta1: TMenuItem;
    Listagem2: TMenuItem;
    Simples2: TMenuItem;
    Transfernciaentrecontas1: TMenuItem;
    Financeiros1: TMenuItem;
    Planodecontas1: TMenuItem;
    Plano1: TMenuItem;
    Conjuntos1: TMenuItem;
    Grupos1: TMenuItem;
    Subgrupos1: TMenuItem;
    Contas1: TMenuItem;
    Todosnveis1: TMenuItem;
    N3: TMenuItem;
    Listaplanos1: TMenuItem;
    N4: TMenuItem;
    Saldoinicialdeconta2: TMenuItem;
    Listagem1: TMenuItem;
    Simples1: TMenuItem;
    N6: TMenuItem;
    Carteiras1: TMenuItem;
    Transfernciaentrecontas2: TMenuItem;
    BtMercad: TBitBtn;
    BtImpMercad: TBitBtn;
    BitBtn1: TBitBtn;
    Impresses1: TMenuItem;
    Impressesdemercadorias1: TMenuItem;
    Entradademercadorias1: TMenuItem;
    BtSdoContas: TBitBtn;
    Saldodecontas1: TMenuItem;
    BtVersao: TBitBtn;
    Verificanovaverso1: TMenuItem;
    N8: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Bancos1: TMenuItem;
    TimerIdle: TTimer;
    BitBtn9: TBitBtn;
    N9: TMenuItem;
    Atualizasaldodecontas1: TMenuItem;
    Planodecontas2: TMenuItem;
    Mercadorias2: TMenuItem;
    Cores2: TMenuItem;
    Tamanhos2: TMenuItem;
    N10: TMenuItem;
    N13: TMenuItem;
    Inventrio1: TMenuItem;
    Cursos1: TMenuItem;
    EQuipes1: TMenuItem;
    Comisses1: TMenuItem;
    Cursos2: TMenuItem;
    Ciclos1: TMenuItem;
    Mercadorias3: TMenuItem;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Cadastro1: TMenuItem;
    Especficos1: TMenuItem;
    N5: TMenuItem;
    BtFinancas: TBitBtn;
    ClientesInternos1: TMenuItem;
    Button1: TButton;
    Linksdecontas1: TMenuItem;
    GerenciaPromotores1: TMenuItem;
    Centrosdecustos1: TMenuItem;
    BitBtn5: TBitBtn;
    Etiquetas1: TMenuItem;
    Cdigodebarras1: TMenuItem;
    Auxiliares1: TMenuItem;
    ClculosdeMduloseBloquetos1: TMenuItem;
    GerenciaProfessores1: TMenuItem;
    BitBtn6: TBitBtn;
    Senhasalunos1: TMenuItem;
    Panel2: TPanel;
    Label3: TLabel;
    TempoI: TLabel;
    Label5: TLabel;
    TempoE: TLabel;
    TempoT: TLabel;
    Label9: TLabel;
    TempoF: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Letras: TLabel;
    CkRandom: TCheckBox;
    Button10: TButton;
    Button9: TButton;
    Button8: TButton;
    Button7: TButton;
    Edit4: TEdit;
    Button6: TButton;
    Button5: TButton;
    Button4: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    EdContaStr: TEdit;
    EdFatorCript: TEdit;
    Label2: TLabel;
    Label7: TLabel;
    EdChave: TdmkEdit;
    Memo1: TMemo;
    Button11: TButton;
    Edit5: TEdit;
    PMAlunos: TPopupMenu;
    Cadastra1: TMenuItem;
    Pesquisa1: TMenuItem;
    Avaliaopormdiadealunos1: TMenuItem;
    N15: TMenuItem;
    CiclosAval1: TMenuItem;
    Grade1: TMenuItem;
    N16: TMenuItem;
    Listadepreos1: TMenuItem;
    Statis1: TMenuItem;
    N17: TMenuItem;
    Kits1: TMenuItem;
    QrConsertaProd: TmySQLQuery;
    QrConsertaProdAtivo: TSmallintField;
    Relatriodevendas1: TMenuItem;
    WEB1: TMenuItem;
    Usurios1: TMenuItem;
    Pontosdevenda1: TMenuItem;
    Cadastro2: TMenuItem;
    Pedidos1: TMenuItem;
    CorreesnoBD1: TMenuItem;
    Entidades3: TMenuItem;
    amanhos1: TMenuItem;
    Prazos1: TMenuItem;
    Faturamento1: TMenuItem;
    CNAB1: TMenuItem;
    Configuraes1: TMenuItem;
    BitBtn2: TBitBtn;
    Cdigoparagrades1: TMenuItem;
    Memo3: TMemo;
    N18: TMenuItem;
    Prazos2: TMenuItem;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    BtAtualizar: TBitBtn;
    QrPesq: TmySQLQuery;
    ProgressBar1: TProgressBar;
    QrPesq2: TmySQLQuery;
    GroupBox2: TGroupBox;
    BitBtn7: TBitBtn;
    ProgressBar2: TProgressBar;
    QrPesq3: TmySQLQuery;
    QrPesq4: TmySQLQuery;
    QrPesq4StatusV: TIntegerField;
    QrPesq4DiaHora: TDateTimeField;
    GroupBox3: TGroupBox;
    BitBtn8: TBitBtn;
    ProgressBar3: TProgressBar;
    QrPesq5: TmySQLQuery;
    QrPesq5Total: TFloatField;
    Vendadeprodutos1: TMenuItem;
    GroupBox4: TGroupBox;
    BitBtn10: TBitBtn;
    ProgressBar4: TProgressBar;
    QrCliInt: TmySQLQuery;
    QrCliIntCliente: TIntegerField;
    RelatriodeVendasporClientes1: TMenuItem;
    N19: TMenuItem;
    Vendasdekits1: TMenuItem;
    N14: TMenuItem;
    Venda1: TMenuItem;
    BTLinVenda: TButton;
    Avaliao1: TMenuItem;
    BtAval: TButton;
    extos1: TMenuItem;
    extos2: TMenuItem;
    Grupodetexto1: TMenuItem;
    BtAlunos: TBitBtn;
    Motivos1: TMenuItem;
    Fotos1: TMenuItem;
    BitBtn11: TBitBtn;
    Promotores1: TMenuItem;
    IdHTTP1: TIdHTTP;
    SPC1: TMenuItem;
    Configuraesdeconsulta1: TMenuItem;
    Modalidadesdeconsultas1: TMenuItem;
    ConsultaSPC1: TMenuItem;
    N21: TMenuItem;
    SockSPC: TTcpClient;
    BtRat: TBitBtn;
    PB5: TProgressBar;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    MyDBTMP: TmySQLDatabase;
    QrTmpMoviM: TmySQLQuery;
    QrLocMoviM: TmySQLQuery;
    QrLocMoviMControle: TIntegerField;
    QrLocMoviMConta: TIntegerField;
    QrLocMoviMGrade: TIntegerField;
    QrLocMoviMCor: TIntegerField;
    QrLocMoviMTam: TIntegerField;
    QrLocMoviMQtd: TFloatField;
    QrLocMoviMVal: TFloatField;
    QrLocMoviMVen: TFloatField;
    QrLocMoviMDataPedi: TDateField;
    QrLocMoviMDataReal: TDateField;
    QrLocMoviMMotivo: TSmallintField;
    QrLocMoviMSALDOQTD: TFloatField;
    QrLocMoviMSALDOVAL: TFloatField;
    QrLocMoviMLk: TIntegerField;
    QrLocMoviMDataCad: TDateField;
    QrLocMoviMDataAlt: TDateField;
    QrLocMoviMUserCad: TIntegerField;
    QrLocMoviMUserAlt: TIntegerField;
    QrLocMoviMAlterWeb: TSmallintField;
    QrLocMoviMAtivo: TSmallintField;
    QrLocMoviMSubCtrl: TIntegerField;
    QrLocMoviMComisTip: TSmallintField;
    QrLocMoviMComisFat: TFloatField;
    QrLocMoviMComisVal: TFloatField;
    QrLocMoviMSubCta: TIntegerField;
    QrLocMoviMKit: TIntegerField;
    QrLocMoviMIDCtrl: TIntegerField;
    QrTmpMoviMControle: TIntegerField;
    QrTmpMoviMConta: TIntegerField;
    QrTmpMoviMGrade: TIntegerField;
    QrTmpMoviMCor: TIntegerField;
    QrTmpMoviMTam: TIntegerField;
    QrTmpMoviMQtd: TFloatField;
    QrTmpMoviMVal: TFloatField;
    QrTmpMoviMVen: TFloatField;
    QrTmpMoviMDataPedi: TDateField;
    QrTmpMoviMDataReal: TDateField;
    QrTmpMoviMMotivo: TSmallintField;
    QrTmpMoviMSALDOQTD: TFloatField;
    QrTmpMoviMSALDOVAL: TFloatField;
    QrTmpMoviMLk: TIntegerField;
    QrTmpMoviMDataCad: TDateField;
    QrTmpMoviMDataAlt: TDateField;
    QrTmpMoviMUserCad: TIntegerField;
    QrTmpMoviMUserAlt: TIntegerField;
    QrTmpMoviMAlterWeb: TSmallintField;
    QrTmpMoviMAtivo: TSmallintField;
    QrTmpMoviMSubCtrl: TIntegerField;
    QrTmpMoviMComisTip: TSmallintField;
    QrTmpMoviMComisFat: TFloatField;
    QrTmpMoviMComisVal: TFloatField;
    QrTmpMoviMSubCta: TIntegerField;
    QrTmpMoviMKit: TIntegerField;
    QrTmpMoviMIDCtrl: TIntegerField;
    QrLocProd: TmySQLQuery;
    QrLocProdControle: TIntegerField;
    QrLocProdCodigo: TIntegerField;
    QrLocProdCor: TIntegerField;
    QrLocProdTam: TIntegerField;
    QrLocProdPrecoC: TFloatField;
    QrLocProdPrecoV: TFloatField;
    QrLocProdPediQ: TFloatField;
    QrLocProdEstqQ: TFloatField;
    QrLocProdEstqV: TFloatField;
    QrLocProdEstqC: TFloatField;
    QrLocProdLk: TIntegerField;
    QrLocProdDataCad: TDateField;
    QrLocProdDataAlt: TDateField;
    QrLocProdUserCad: TIntegerField;
    QrLocProdUserAlt: TIntegerField;
    QrLocProdAlterWeb: TSmallintField;
    QrLocProdAtivo: TSmallintField;
    QrLocProdFoto: TIntegerField;
    ImgPrincipal: TImage;
    Ajuda1: TMenuItem;
    CentraldeSuporte1: TMenuItem;
    VerificaBDPblicas1: TMenuItem;
    N2: TMenuItem;
    Sobre1: TMenuItem;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    TmSuporte: TTimer;
    TySuporte: TTrayIcon;
    BalloonHint1: TBalloonHint;
    procedure Sair1Click(Sender: TObject);
    procedure Logoffde1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Entidades1Click(Sender: TObject);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Verifica1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Diversos1Click(Sender: TObject);
    procedure BtEntidades2Click(Sender: TObject);
    procedure BtProtecaoClick(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Seleciona1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure BtPlaCtasClick(Sender: TObject);
    procedure Plano3Click(Sender: TObject);
    procedure Conjutos2Click(Sender: TObject);
    procedure Grupos3Click(Sender: TObject);
    procedure Subgrupos3Click(Sender: TObject);
    procedure Contas3Click(Sender: TObject);
    procedure Todosnveis2Click(Sender: TObject);
    procedure Listaplanos2Click(Sender: TObject);
    procedure Listagem2Click(Sender: TObject);
    procedure Simples2Click(Sender: TObject);
    procedure Transfernciaentrecontas1Click(Sender: TObject);
    procedure Plano1Click(Sender: TObject);
    procedure Conjuntos1Click(Sender: TObject);
    procedure Grupos1Click(Sender: TObject);
    procedure Subgrupos1Click(Sender: TObject);
    procedure Contas1Click(Sender: TObject);
    procedure Todosnveis1Click(Sender: TObject);
    procedure Listaplanos1Click(Sender: TObject);
    procedure Listagem1Click(Sender: TObject);
    procedure Simples1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Transfernciaentrecontas2Click(Sender: TObject);
    procedure BtImpMercadClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Impressesdemercadorias1Click(Sender: TObject);
    procedure Entradademercadorias1Click(Sender: TObject);
    procedure BtSdoContasClick(Sender: TObject);
    procedure Saldodecontas1Click(Sender: TObject);
    procedure BtVersaoClick(Sender: TObject);
    procedure Verificanovaverso1Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure Bancos1Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure Atualizasaldodecontas1Click(Sender: TObject);
    procedure Planodecontas2Click(Sender: TObject);
    procedure Tamanhos2Click(Sender: TObject);
    procedure Inventrio1Click(Sender: TObject);
    procedure BtAlunosClick(Sender: TObject);
    procedure Cursos1Click(Sender: TObject);
    procedure EQuipes1Click(Sender: TObject);
    procedure Ciclos1Click(Sender: TObject);
    procedure Mercadorias3Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure Especficos1Click(Sender: TObject);
    procedure BtFinancasClick(Sender: TObject);
    procedure ClientesInternos1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Linksdecontas1Click(Sender: TObject);
    procedure GerenciaPromotores1Click(Sender: TObject);
    procedure Centrosdecustos1Click(Sender: TObject);
    procedure Etiquetas1Click(Sender: TObject);
    procedure Cdigodebarras1Click(Sender: TObject);
    procedure ClculosdeMduloseBloquetos1Click(Sender: TObject);
    procedure GerenciaProfessores1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Senhasalunos1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Cadastra1Click(Sender: TObject);
    procedure Pesquisa1Click(Sender: TObject);
    procedure Avaliaopormdiadealunos1Click(Sender: TObject);
    procedure CiclosAval1Click(Sender: TObject);
    procedure Grade1Click(Sender: TObject);
    procedure Listadepreos1Click(Sender: TObject);
    procedure Statis1Click(Sender: TObject);
    procedure Kits1Click(Sender: TObject);
    procedure Cores2Click(Sender: TObject);
    procedure BtMercadClick(Sender: TObject);
    procedure Relatriodevendas1Click(Sender: TObject);
    procedure Usurios1Click(Sender: TObject);
    procedure Cadastro2Click(Sender: TObject);
    procedure Pedidos1Click(Sender: TObject);
    procedure Entidades3Click(Sender: TObject);
    procedure amanhos1Click(Sender: TObject);
    procedure Faturamento1Click(Sender: TObject);
    procedure Configuraes1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Cdigoparagrades1Click(Sender: TObject);
    procedure Backup1Click(Sender: TObject);
    procedure Prazos1Click(Sender: TObject);
    procedure Prazos2Click(Sender: TObject);
    procedure BtAtualizarClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure Vendadeprodutos1Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure RelatriodeVendasporClientes1Click(Sender: TObject);
    procedure Venda1Click(Sender: TObject);
    procedure BTLinVendaClick(Sender: TObject);
    procedure Avaliao1Click(Sender: TObject);
    procedure BtAvalClick(Sender: TObject);
    procedure extos1Click(Sender: TObject);
    procedure Grupodetexto1Click(Sender: TObject);
    procedure Motivos1Click(Sender: TObject);
    procedure Fotos1Click(Sender: TObject);
    procedure Comisses1Click(Sender: TObject);
    procedure Promotores1Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure Configuraesdeconsulta1Click(Sender: TObject);
    procedure Modalidadesdeconsultas1Click(Sender: TObject);
    procedure ConsultaSPC1Click(Sender: TObject);
    procedure BtRatClick(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure VCLSkin1Click(Sender: TObject);
    procedure CentraldeSuporte1Click(Sender: TObject);
    procedure VerificaBDPblicas1Click(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure TmSuporteTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure CadastroTamanhos;
    procedure CadastroCompras;
    procedure CadastroProdutos;
    procedure RelatoriosEstoque;
    procedure RelatorioVendas;
    procedure SaldoDeContas;
    procedure AtzSdoContas;
    procedure Inventario;
    procedure CadastroDeGrades;
    procedure CadastroDeKits;
    procedure CadastroDeStatus;
    procedure CadastroCores;
    procedure CadastroLista;
    procedure MostraBackup3;
    procedure MostraMoviL(Codigo: Integer);
    procedure MostraPediPrzCab1;
    procedure MostraPediPrzCab;
    procedure MostraRelVendasCli;
    procedure MostraCartAval(Codigo: Integer);
    procedure MostraCartas;
    procedure MostraCartaG;
    procedure CadastroMotivos;
  public
    { Public declarations }
    FIntTipo, //1 = Venda de kit; 2 = Carta de avalia��o; 3 = Venda lingerie
    FCodAlu, FCodAlu2, FCliInt_, FEntInt, FDepto, FCodDepto,
    FTipoNovoEnti, FCiclo: Integer;
    FLDataIni, FLDataFim: TDateTime;
    Fmy_key_uEncrypt: Integer;
    FMotivosPtos: String;
    FMoviVTot: Double;
    //
    procedure ShowHint(Sender: TObject);
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure HabilitaAplicativo; // AcoesIniciais

    procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
    procedure CadastroEntidades;
    procedure CadastroEntidades1;
    procedure CadastroVendas(Codigo, Cliente: Integer);
    procedure CadastroVendasEdit(Codigo, Controle, Cliente: Integer; Tipo: TSQLType);

    procedure SelecionaImagemdefundo;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; AbrirEmAba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade1: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    //
    function  CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure CadastroDeContasSdoLista();
    procedure CadastroDeCarteira;
    procedure GerenciaCiclos();
    procedure MostraCiclosLoc();
    procedure GerenciaCiclos2(Codigo, Controle: Integer);
    procedure CadastroDeCursos();
    procedure CadastroDePlano();
    procedure CadastroDeConjutos();
    procedure CadastroDeGrupos;
    procedure CadastroDeSubGrupos;
    procedure CadastroDeContas;
    procedure CadastroDeNiveisPlano();
    procedure CadastroDeEntidades(Entidade: Integer);
    procedure CadastroEquiCom(Codigo: Integer);
    //
    procedure ImpressaoDoPlanoDeContas;
    procedure TransfereEntreContas(Entidade, CtaOrig, CtaDest: Integer;
              Data: TDateTime; Valor: Double);
    procedure MostraCarteiras(Carteira: Integer);
    // Cashier
    function  CartaoDeFatura: Integer;
    function  CompensacaoDeFatura: String;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    function  CarregaImg(Foto: String): Boolean;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    procedure CadastroDeContasNiv();
    // Fim Cashier
    procedure RetornoCNAB;
    procedure CriaPages;
    procedure CriaMinhasEtiquetas;
    procedure CadastroWUsers;
    procedure DefineVarsCliInt(CliInt: Integer);
    procedure MostraVendaConf(Sender: TObject);
    procedure MostraVendaPesq(Depto: Integer);
    procedure MostraAlunoVis(Conta: Integer);
    procedure MostraGraGruFotos(Produto: Integer; Janela: String);
    procedure MostraPromotores(Codigo: Integer);
    procedure MostraPromoWeb;
    procedure MostraCiclosAulaImp(Codigo, Professor, Promotor: Integer; Data, Cidade, UF: String);
    procedure CarregaFotoKit(Imagem: TImage; Produto: String);
    procedure CarregaFoto(Imagem: TImage; Produto: String);
    //
    //N�o usa procedure BMPtoJPG(BMPpic, JPGpic: string);
    function LoadJPEGPictureFile(Bitmap: TBitmap; FilePath, FileName: string): Boolean;
    function SaveJPEGPictureFile(Bitmap: TBitmap; FilePath, FileName: string;
      Quality: Integer): Boolean;
    function VerificaImg(Caminho: String): Boolean;
    procedure ResizeImage(FileName: string; MaxWidth: Integer);
    procedure SmoothResize(Src, Dst: TBitmap);
    procedure ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
    end;
    
var
  FmPrincipal: TFmPrincipal;

implementation

uses LeSew_MLA, Module, MyListas, Entidades, MyDBCheck, MyGlyfs, MyVCLSkin,
  VerifiDB, Opcoes, Carteiras, UCreate, Backup3, Conjuntos, Grupos, SubGrupos,
  Contas, PlaCtas, ContasSdoImp, ContasSdoAll, ContasSdoUni, ContasSdoTrf,
  ContasHistSdo, ContasHistAtz2, ContasNiv, SelfGer, ModuleFin, Plano, Tamanhos,
  Ciclos, Cores, GraRel, MoviC, MoviB, CalcPercent, NovaVersao, Bancos,
  EntiRapido, EquiGru, EquiCom, Cursos, OpcoesLeSew, CliInt, ContasLnk,
  CiclosProm, MalaDireta, Pages, MultiEtiq, UnLic_Dmk, GradesCodBarra,
  CalcMod10e11, CiclosProf, UnitMD5, MD5Cab, About, EZCrypt, uEncrypt,
  AlunoPesq, UnFinanceiro, CiclosAval, CiclosAvalImp, GraGru, GraCusPrc,
  StatusV, GradeK, CliIntSel, RelVendas, WUsers, PtosCadPto, PtosPedCad,
  UMySQLModule, ModuleGeral, PediPrzCab1, PtosFatCad, CNAB_Cfg, UnMyObjects,
  Entidade2, MoviV, VendaConf, RelVendasCli, MoviL, CartAval, Cartas, CartaG,
  VendaPesq, Motivos, MoviVEdit, Ciclos2, Promotores, CiclosAulaImp, PromoWeb,
  PediPrzCab, SPC_Modali, SPC_Config, SPC_Pesq, AlunoVis, CiclosRat,
  CiclosAula2, ModuleCiclos, CentroCusto, CiclosLoc, ModuleProd, LinkRankSkin,
  UnDmkWeb, VerifiDBTerceiros, ObtemFoto4;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

procedure TFmPrincipal.HabilitaAplicativo;
  procedure AvisaSetarOpcoes;
  var
    Caminho: String;
  begin
    Caminho := 'Dermatek\Syndic\Opcoes';
    if not Geral.ReadAppKey('JaSetado', Caminho, ktBoolean, False,
    HKEY_LOCAL_MACHINE) then
    begin
      if Geral.MensagemBox('O aplicativo detectou que as op��es do '+
      'aplicativo n�o foram configuradas ainda. Deseja configur�-las agora?',
      'Aviso de Op��es', MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) =
      ID_YES then Especficos1Click(Self);
    end;
  end;
  procedure NovoItem(var Seq: Integer; const Item: integer; const Local, Acao: String);
  begin
    Inc(Seq, 1);
    Dmod.QrUpdL.Params[00].AsInteger := Seq;
    Dmod.QrUpdL.Params[01].AsInteger := Item;  // Mostra Janela
    Dmod.QrUpdL.Params[02].AsString  := Local;
    Dmod.QrUpdL.Params[03].AsString  := Acao;
    Dmod.QrUpdL.ExecSQL;
  end;
const
  InfoSeq = 6;
begin
  try
    if ZZTerminate then
      Exit;
    Application.ProcessMessages;
    Screen.Cursor := crHourGlass;
{
    if Dmod.QrControleInfoSeq.Value < InfoSeq then
    begin
      UCriar.RecriaTabelaLocal('InfoSeq', 1);
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('INSERT INTO infoseq SET ');
      Dmod.QrUpdL.SQL.Add('Linha=:P0, Item=:P1, Local=:P2, Acao=:P3');
      Dmod.QrUpdL.SQL.Add('');
      Application.CreateForm(TFmInfoSeq, FmInfoSeq);
      i := 0;
      if Dmod.QrControleInfoSeq.Value < 1 then NovoItem(i, 1,
        'Ferramentas > Op��es > Espec�ficas: Impress�es',
        'Impressora de cheques');
      if Dmod.QrControleInfoSeq.Value < 1 then NovoItem(i, 2,
        'Gerencia Clientes: Bloqueto: Atraso pagamento',
        '% Multa e % de juros');
      if Dmod.QrControleInfoSeq.Value < 2 then NovoItem(i, 1,
        'Ferramentas > Op��es > Espec�ficas: Impress�es',
        'Bloqueto Condominial');
      if Dmod.QrControleInfoSeq.Value < 3 then NovoItem(i, 2,
        'Gerencia Clientes: Ag�ncia/C�digo cedente bloqueto:',
        'Informar a ag�ncia / c�digo cedente exatamente como consta no boleto. ' +
        'Caso n�o tenha, solicitar ao banco correspondente');
      if Dmod.QrControleInfoSeq.Value < 4 then NovoItem(i, 2,
        'Gerencia Clientes: Orelha > Parecer do conselho; ' +
        'Bot�o: Condom. > Parecer do conselho',
        'Editar o texto do parecer do conselho fiscal do condom�nio.');
      if Dmod.QrControleInfoSeq.Value < 5 then NovoItem(i, 3,
        'Ferramentas > Op��es > Diversos: "Orelha" Cobran�a (CNAB); ',
        'Informar a conta de Juros de mora p�s pagamento a menor');
      if Dmod.QrControleInfoSeq.Value < 6 then NovoItem(i, 3,
        'Ferramentas > Op��es > Diversos: "Orelha" Cobran�a (CNAB); ',
        'Informar a conta de tarifa de cobran�a de bloqueto.');
      if i > 0 then
      begin
        FmInfoSeq.QrInfoSeq.Close;
        FmInfoSeq.QrInfoSeq.Open;
        FmInfoSeq.ShowModal;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE controle SET InfoSeq=:P0');
        Dmod.QrUpd.Params[0].AsInteger := InfoSeq;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrControle.Close;
        Dmod.QrControle.Open;
      end;
      FmInfoSeq.Destroy;
    end;
    BitBtn2.Visible := not MLAGeral.IntInConjunto(1, Dmod.QrControleAtzCritic.Value);
    PertoChekP.VerificaPertochek;
    UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
    VerificaOrelhas;
    //
    DBGErrRepa.DataSource         := Dmod.DsErrRepa;
    dmkDBGErrRepaIts.DataSource   := Dmod.DsErrRepaIts;
    DBGErrRepaSum.DataSource      := Dmod.DsErrRepaSum;
    //
    DBGEmeioProt.DataSource       := Dmod.DsEmeioProt;
    DBGEmeioProtPak.DataSource    := Dmod.DsEmeioProtPak;
    DBGEmeioProtPakIts.DataSource := Dmod.DsEmeioProtPakIts;
    DBEdEmeioProtTot.DataSource   := Dmod.DsEmeioProtTot;
    //
}
    if DModG <> nil then
    begin
      DmodG.AtualizaEntiConEnt();
      //
      DModG.MyPID_DB_Cria();
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      if DmkWeb.RemoteConnection then
      begin
        if VerificaNovasVersoes(True) then
          DmkWeb.MostraBalloonHintMenuTopo(BtVersao, BalloonHint1,
            'H� uma nova vers�o!', 'Clique aqui para atualizar.');
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.Logoffde1Click(Sender: TObject);
begin
  FmPrincipal.Enabled := False;
  //
  FmLeSew_MLA.Show;
  FmLeSew_MLA.EdLogin.Text := '';
  FmLeSew_MLA.EdSenha.Text := '';
  FmLeSew_MLA.EdLogin.SetFocus;
end;

procedure TFmPrincipal.Faturamento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPtosFatCad, FmPtosFatCad, afmoNegarComAviso) then
  begin
    FmPtosFatCad.ShowModal;
    FmPtosFatCad.Destroy;
  end;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  if not MyObjects.CorIniComponente() then Hide else Show;
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Trim(StatusBar.Panels[3].Text) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then
    Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  Imagem(*, Avisa*): String;
begin
  TabSheet2.TabVisible := False;
  //
  VAR_TYPE_LOG     := ttlCliIntLog;
  Fmy_key_uEncrypt := 83735;
  FMotivosPtos := '15,25,26'; // Motivos de pontos de venda
  VAR_USA_TAG_BITBTN        := True;
  FTipoNovoEnti             := 0;
  VAR_LOGOFFDE              := Logoffde1;
  VAR_STLOGIN               := StatusBar.Panels[01];
  StatusBar.Panels[3].Text  := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL            := StatusBar.Panels[05];
  VAR_STDATALICENCA         := StatusBar.Panels[07];
  //VAR_STAVISOS            := StatusBar.Panels[09];
  VAR_SKINUSANDO            := StatusBar.Panels[09];
  VAR_STDATABASES           := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT        := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP                   := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE             := 1;
  Application.OnMessage     := MyObjects.FormMsg;
  Imagem                    := Geral.ReadAppKey('ImagemFundo',
    Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then ImgPrincipal.Picture.LoadFromFile(Imagem);
  //
  VAR_CAD_POPUP := PMGeral;
  //MLAGeral.CopiaItensDeMenu(PMGeral, Cadastros1, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  //
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\VistaXP-VISTAXPB2.skn';
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString, HKEY_LOCAL_MACHINE);
  end;
end;

procedure TFmPrincipal.Senhasalunos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMD5Cab, FmMD5Cab, afmoNegarComAviso) then
  begin
    FmMD5Cab.ShowModal;
    FmMD5Cab.Destroy;
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFmPrincipal.Entidades1Click(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.Prazos1Click(Sender: TObject);
begin
  MostraPediPrzCab;
end;

procedure TFmPrincipal.Prazos2Click(Sender: TObject);
begin
  MostraPediPrzCab1;
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
  except
    Result := False
  end;
end;

procedure TFmPrincipal.Promotores1Click(Sender: TObject);
begin
  MostraPromotores(0);
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  (*
  if aName = 'TFmObtemFoto4' then
    DoSkin := False;
  *)
end;

procedure TFmPrincipal.CadastroEntidades;
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroEntidades1;
begin
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroEquiCom(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEquiCom, FmEquiCom, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then FmEquiCom.LocCod(Codigo, Codigo);
    FmEquiCom.ShowModal;
    FmEquiCom.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroLista;
begin
  if DBCheck.CriaFm(TFmGraCusPrc, FmGraCusPrc, afmoNegarComAviso) then
  begin
    FmGraCusPrc.ShowModal;
    FmGraCusPrc.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroMotivos;
begin
  if DBCheck.CriaFm(TFmMotivos, FmMotivos, afmoNegarComAviso) then
  begin
    FmMotivos.ShowModal;
    FmMotivos.Destroy;
  end;
end;

procedure TFmPrincipal.Verifica1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaBDPblicas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.Fotos1Click(Sender: TObject);
begin
  MostraGraGruFotos(0, 'FmGradeK');
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmLeSew_MLA.Show;
  Enabled := False;
  FmLeSew_MLA.Refresh;
  FmLeSew_MLA.EdSenha.Text := FmLeSew_MLA.EdSenha.Text+'*';
  FmLeSew_MLA.EdSenha.Refresh;
  FmLeSew_MLA.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmLeSew_MLA.EdSenha.Text := FmLeSew_MLA.EdSenha.Text+'*';
  FmLeSew_MLA.EdSenha.Refresh;
  FmLeSew_MLA.ReloadSkin;
  FmLeSew_MLA.EdLogin.Text := '';
  FmLeSew_MLA.EdLogin.PasswordChar := 'l';
  FmLeSew_MLA.EdSenha.Text := '';
  FmLeSew_MLA.EdSenha.Refresh;
  FmLeSew_MLA.EdLogin.ReadOnly := False;
  FmLeSew_MLA.EdSenha.ReadOnly := False;
  FmLeSew_MLA.EdLogin.SetFocus;
  //FmLeSew_MLA.ReloadSkin;
  FmLeSew_MLA.Refresh;
end;

procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrCliIntLog, Dmod.MyDB, [
  'SELECT DISTINCT eci.CodEnti',
  'FROM enticliint eci',
  'LEFT JOIN senhas snh ON snh.Funcionario=eci.AccManager',
  'LEFT JOIN senhasits sei ON eci.CodEnti=sei.Empresa',
  'WHERE eci.CodCliInt=' + FormatFloat('0', CliInt),
  'AND ',
  '(',
  '  eci.AccManager=0',
  '  OR ',
  '  snh.Numero=' + FormatFloat('0', VAR_USUARIO),
  '  OR',
  '  sei.Numero=' + FormatFloat('0', VAR_USUARIO),
  '  OR',
     FormatFloat('0', VAR_USUARIO) + '<0',
  ')',
  '']);
  //
  FEntInt := DmodG.QrCliIntLogCodEnti.Value;
  if FEntInt = 0 then
    FCliInt_ := 0
  else
    FCliInt_ := CliInt;
  //
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntLogCodEnti.Value);
  if VAR_LIB_EMPRESAS = '' then VAR_LIB_EMPRESAS := '-100000000';
  //
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
end;

procedure TFmPrincipal.Diversos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.BtEntidades2Click(Sender: TObject);
begin
  CadastroDeEntidades(0);
end;

procedure TFmPrincipal.BtProtecaoClick(Sender: TObject);
begin
  DModG.MostraBackup3();
  {if DBCheck.CriaFm(TFmBackup3, FmBackup3, afmoNegarComAviso) then
  begin
    FmBackup3.ShowModal;
    FmBackup3.Destroy;
  end;}
end;

procedure TFmPrincipal.BtRatClick(Sender: TObject);
var
  Conta: Integer;
  Nome: String;
begin
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('SELECT Conta, Nome');
  Dmod.QrUpdM.SQL.Add('FROM ciclosalu');
  Dmod.QrUpdM.Open;
  //
  PB5.Visible  := True;
  PB5.Position := 0;
  PB5.Max      := Dmod.QrUpdM.RecordCount;
  //
  if Dmod.QrUpdM.RecordCount > 0 then
  begin
    Dmod.QrUpdM.First;
    while not Dmod.QrUpdM.Eof do
    begin
      Conta := Dmod.QrUpdM.FieldByName('Conta').Value;
      Nome  := UpperCase(Dmod.QrUpdM.FieldByName('Nome').Value);
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ciclosalu SET Nome=:P0 WHERE Conta=:P1');
      Dmod.QrUpd.Params[0].AsString  := Nome;
      Dmod.QrUpd.Params[1].AsInteger := Conta;
      Dmod.QrUpd.ExecSQL;
      //
      PB5.Position := PB5.Position + 1;
      PB5.Update;
      Application.ProcessMessages;
      //
      Dmod.QrUpdM.Next;
    end;
  end;
  Dmod.QrUpdM.Close;
  PB5.Visible := False;
  //
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('SELECT ent.Nome, alu.Entidade');
  Dmod.QrUpdM.SQL.Add('FROM ciclosalu alu');
  Dmod.QrUpdM.SQL.Add('LEFT JOIN entidades ent ON alu.Entidade = ent.Codigo');
  Dmod.QrUpdM.SQL.Add('WHERE alu.Entidade > 0');
  Dmod.QrUpdM.Open;
  if Dmod.QrUpdM.RecordCount > 0 then
  begin
    PB5.Position := 0;
    PB5.Max      := Dmod.QrUpdM.RecordCount;
    PB5.Visible  := True;
    Dmod.QrUpdM.First;
    while not Dmod.QrUpdM.Eof do
    begin
      Conta := Dmod.QrUpdM.FieldByName('Entidade').Value;
      Nome  := Dmod.QrUpdM.FieldByName('Nome').Value;
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE entidades SET Nome=:P0 WHERE Codigo=:P1');
      Dmod.QrUpd.Params[0].AsString  := UpperCase(Nome);
      Dmod.QrUpd.Params[1].AsInteger := Conta;
      Dmod.QrUpd.ExecSQL;
      //
      PB5.Position := PB5.Position + 1;
      PB5.Update;
      Application.ProcessMessages;
      //
      Dmod.QrUpdM.Next;
    end;
  end;
  PB5.Visible := False;
  Dmod.QrUpdM.Close;
{var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT cic.Codigo');
  Dmod.QrUpd.SQL.Add('FROM ciclos cic');
  Dmod.QrUpd.SQL.Add('LEFT JOIN ciclosaula aul ON aul.Codigo = cic.Codigo');
  Dmod.QrUpd.SQL.Add('WHERE aul.Controle > 0');
  Dmod.QrUpd.SQL.Add('GROUP BY cic.Codigo');
  Dmod.QrUpd.Open;
  if Dmod.QrUpd.RecordCount > 0 then
  begin
    ProgressBar5.Max      := Dmod.QrUpd.RecordCount;
    ProgressBar5.Position := 0;
    ProgressBar5.Visible  := True;
    //
    Codigo := Dmod.QrUpd.FieldByName('Codigo').Value;
    while not Dmod.QrUpd.Eof do
    begin
      RolComis  := Dmod.QrUpd.FieldByName('RolComis').Value;
      DataEmis  := Dmod.QrUpd.FieldByName('DataChega').Value;
      Professor := Dmod.QrUpd.




      if DBCheck.CriaFm(TFmCiclosRat, FmCiclosRat, afmoNegarComAviso) then
      begin
        FmCiclosRat.FRolComis    := QrCiclosRolComis.Value;
        FmCiclosRat.FDataEmis    := QrCiclosDataChega.Value;
        FmCiclosRat.FProfessor   := QrCiclosProfessor.Value;
        FmCiclosRat.FPromotor    := QrCiclosPromotor.Value;
        FmCiclosRat.FCiclo       := QrCiclosCodigo.Value;
        //FmCiclosRat.FCurso       := QrCiclosCurso.Value;
        FmCiclosRat.FQtdAlunos   := QrCiclosAlunosAula.Value;
        FmCiclosRat.FValorRateio := QrCiclosVALORRATEIO.Value;
        FmCiclosRat.FDespesas    := QrCiclosDespProf.Value;
        FmCiclosRat.FValorAlunos := QrCiclosAlunosVal.Value;
        FmCiclosRat.FMovimCtrl   := QrCiclosControle.Value;
        FmCiclosRat.FPagoAProf   := QrCiclosPAGOAPROF.Value;
        FmCiclosRat.FCxaPgtProf  := Dmod.QrControleCxaPgtProf.Value;
        FmCiclosRat.FCtaPagProf  := Dmod.QrControleCtaPagProf.Value;
        //
        FmCiclosRat.ShowModal;
        FmCiclosRat.Destroy;
      end;
      //
      ProgressBar5.Position := ProgressBar5.Position + 1;
      ProgressBar5.Update;
      Application.ProcessMessages;
      //
      Dmod.QrUpd.Next;
    end;
  end;
  Screen.Cursor := crDefault;}
end;

procedure TFmPrincipal.CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  CadastroEntidades1;
end;

procedure TFmPrincipal.Entidades3Click(Sender: TObject);
begin
  UMyMod.AtualizaEntidadesCodigoToCodUsu('Entidades');
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; AbrirEmAba: Boolean);
begin
  // Confirmar dono como cliente interno!
  if Entidade = -1 then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=1 WHERE Codigo=-1');
    DMod.QrUpd.ExecSQL;
  end;
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.Pedidos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPtosPedCad, FmPtosPedCad, afmoNegarComAviso) then
  begin
    FmPtosPedCad.ShowModal;
    FmPtosPedCad.Destroy;
  end;
end;

procedure TFmPrincipal.Pesquisa1Click(Sender: TObject);
begin
{  if DBCheck.CriaFm(TFmAlunoPesq, FmAlunoPesq, afmoNegarComAviso) then
  begin
    FmAlunoPesq.ShowModal;
    FmAlunoPesq.Destroy;
  end;}
end;

procedure TFmPrincipal.Seleciona1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.Limpa1Click(Sender: TObject);
begin
  ImgPrincipal.Picture := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
end;

procedure TFmPrincipal.Linksdecontas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasLnk, FmContasLnk, afmoNegarComAviso) then
  begin
    FmContasLnk.ShowModal;
    FmContasLnk.Destroy;
  end;
end;

procedure TFmPrincipal.amanhos1Click(Sender: TObject);
begin
  UMyMod.AtualizaEntidadesCodigoToCodUsu('Tamanhos');
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.BtPlaCtasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPlaCtas, BtPlaCtas);
end;

procedure TFmPrincipal.Plano3Click(Sender: TObject);
begin
  CadastroDePlano();
end;

procedure TFmPrincipal.Conjutos2Click(Sender: TObject);
begin
  CadastroDeConjutos();
end;

procedure TFmPrincipal.ConsultaSPC1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Pesq, FmSPC_Pesq, afmoNegarComAviso) then
  begin
    FmSPC_Pesq.ShowModal;
    FmSPC_Pesq.Destroy;
  end;
end;

procedure TFmPrincipal.Grupos3Click(Sender: TObject);
begin
  CadastroDeGrupos();
end;

procedure TFmPrincipal.Subgrupos3Click(Sender: TObject);
begin
  CadastroDeSubGrupos;
end;

procedure TFmPrincipal.Contas3Click(Sender: TObject);
begin
  CadastroDeContas;
end;

procedure TFmPrincipal.Cores2Click(Sender: TObject);
begin
  CadastroCores;
end;

procedure TFmPrincipal.Todosnveis2Click(Sender: TObject);
begin
  CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.Listaplanos2Click(Sender: TObject);
begin
  ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.Listagem2Click(Sender: TObject);
begin
  CadastroDeContasSdoLista();
end;

procedure TFmPrincipal.Simples2Click(Sender: TObject);
begin
  CadastroDeContasSdoSimples(0, 0);
end;

procedure TFmPrincipal.Statis1Click(Sender: TObject);
begin
  Cadastrodestatus;
end;

procedure TFmPrincipal.Transfernciaentrecontas1Click(Sender: TObject);
begin
  TransfereEntreContas(0, 0, 0, Date, 0);
end;

procedure TFmPrincipal.CadastroDePlano();
begin
  if DBCheck.CriaFm(TFmPlano, FmPlano, afmoNegarComAviso) then
  begin
    FmPlano.ShowModal;
    FmPlano.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeConjutos();
begin
  if DBCheck.CriaFm(TFmConjuntos, FmConjuntos, afmoNegarComAviso) then
  begin
    FmConjuntos.ShowModal;
    FmConjuntos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeGrades;
begin
  if DBCheck.CriaFm(TFmGraGru, FmGraGru, afmoNegarComAviso) then
  begin
    FmGraGru.ShowModal;
    FmGraGru.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeKits;
begin
  if DBCheck.CriaFm(TFmGradeK, FmGradeK, afmoNegarComAviso) then
  begin
    FmGradeK.ShowModal;
    FmGradeK.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeGrupos;
begin
  if DBCheck.CriaFm(TFmGrupos, FmGrupos, afmoNegarComAviso) then
  begin
    FmGrupos.ShowModal;
    FmGrupos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeStatus;
begin
  if DBCheck.CriaFm(TFmStatusV, FmStatusV, afmoNegarComAviso) then
  begin
    FmStatusV.ShowModal;
    FmStatusV.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeSubGrupos;
begin
  if DBCheck.CriaFm(TFmSubgrupos, FmSubgrupos, afmoNegarComAviso) then
  begin
    FmSubgrupos.ShowModal;
    FmSubgrupos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeContas;
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeNiveisPlano();
begin
  if DBCheck.CriaFm(TFmPlaCtas, FmPlaCtas, afmoNegarComAviso) then
  begin
    FmPlaCtas.ShowModal;
    FmPlaCtas.Destroy;
  end;
end;

procedure TFmPrincipal.ImpressaoDoPlanoDeContas;
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeContasSdoLista();
begin
  if DBCheck.CriaFm(TFmContasSdoAll, FmContasSdoAll, afmoNegarComAviso) then
  begin
    FmContasSdoAll.ShowModal;
    FmContasSdoAll.Destroy;
  end;
end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmContasSdoUni, FmContasSdoUni, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
    begin
      FmContasSdoUni.EdEntidade.Text := IntToStr(Entidade);
      FmContasSdoUni.CBEntidade.KeyValue := Entidade;
    end;
    if Conta <> 0 then
    begin
      FmContasSdoUni.EdCodigo.Text := IntToStr(Conta);
      FmContasSdoUni.CBCodigo.KeyValue := Conta;
    end;
    FmContasSdoUni.ShowModal;
    Result := FmContasSdoUni.FExecutou;
    FmContasSdoUni.Destroy;
  end;
end;

procedure TFmPrincipal.TransfereEntreContas(Entidade, CtaOrig, CtaDest: Integer;
  Data: TDateTime; Valor: Double);
begin
  {
  if DBCheck.CriaFm(TFmContasSdoTrf, FmContasSdoTrf, afmoNegarComAviso) then
  begin
    if (Entidade > 0)
    and (FmContasSdoTrf.QrEntidades.Locate('Codigo', Entidade, [])) then
    begin
      FmContasSdoTrf.EdEntidade.Text := IntToStr(Entidade);
      FmContasSdoTrf.CBEntidade.KeyValue := Entidade;
    end;
    if CtaOrig > 0 then
    begin
      FmContasSdoTrf.EdCtaOrig.Text := IntToStr(CtaOrig);
      FmContasSdoTrf.CBCtaOrig.KeyValue := CtaOrig;
    end;
    if CtaDest > 0 then
    begin
      FmContasSdoTrf.EdCtaDest.Text := IntToStr(CtaDest);
      FmContasSdoTrf.CBCtaDest.KeyValue := CtaDest;
    end;
    if Data > 0 then FmContasSdoTrf.TPDataT.Date := Data;
    FmContasSdoTrf.ShowModal;
    FmContasSdoTrf.Destroy;
  end;
  }
  UFinanceiro.CriarTransferCtas(0, DmodFin.QrLctos, DmodFin.QrCarts,
    True, -1, 0, 0, 0);
  Dmod.RecalcSaldoCarteira(DmodFin.QrCartsTipo.Value, DmodFin.QrCartsCodigo.Value, 1);
end;

procedure TFmPrincipal.Modalidadesdeconsultas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Modali, FmSPC_Modali, afmoNegarComAviso) then
  begin
    FmSPC_Modali.ShowModal;
    FmSPC_Modali.Destroy;
  end;
end;

procedure TFmPrincipal.MostraAlunoVis(Conta: Integer);
begin
  if DBCheck.CriaFm(TFmAlunoVis, FmAlunoVis, afmoNegarComAviso) then
  begin
    FmAlunoVis.FConta := Conta;
    FmAlunoVis.ShowModal;
    FmAlunoVis.Destroy;
    if (FCodAlu <> 0) and (FCodAlu2 <> 0) then
      FmPrincipal.GerenciaCiclos2(FCodAlu, FCodAlu2);
  end;
end;

procedure TFmPrincipal.MostraBackup3;
begin
  DModG.MostraBackup3();
  {if DBCheck.CriaFm(TFmBackup3, FmBackup3, afmoNegarComAviso) then
  begin
    FmBackup3.ShowModal;
    FmBackup3.Destroy;
  end;}
end;

procedure TFmPrincipal.MostraMoviL(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmMoviL, FmMoviL, afmoNegarComAviso) then
  begin
    if Codigo > 0 then
      FmMoviL.LocCod(Codigo, Codigo);
    FmMoviL.ShowModal;
    FmMoviL.Destroy;
    //
    if FCodDepto > 0 then
      MostraVendaPesq(FDepto);
  end;
end;

procedure TFmPrincipal.MostraCartaG;
begin
  if DBCheck.CriaFm(TFmCartaG, FmCartaG, afmoNegarComAviso) then
  begin
    FmCartaG.FTipo := 7;
    FmCartaG.ShowModal;
    FmCartaG.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCartas;
begin
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 7;
    FmCartas.ShowModal;
    FmCartas.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCartAval(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCartAval, FmCartAval, afmoNegarComAviso) then
  begin
    if Codigo > 0 then
      FmCartAval.LocCod(Codigo, Codigo);
    FmCartAval.ShowModal;
    FmCartAval.Destroy;
    //
    if FCodDepto > 0 then
      MostraVendaPesq(FDepto);
  end;
end;

procedure TFmPrincipal.MostraCarteiras(Carteira: Integer);
begin
  // n�o � necess�rio
end;

procedure TFmPrincipal.MostraCiclosAulaImp(Codigo, Professor, Promotor: Integer; Data, Cidade, UF: String);
begin
  if DBCheck.CriaFm(TFmCiclosAulaImp, FmCiclosAulaImp, afmoNegarComAviso) then
  begin
    FmCiclosAulaImp.FCodigo    := Codigo;
    FmCiclosAulaImp.FPromotor  := Promotor;
    FmCiclosAulaImp.FProfessor := Professor;
    FmCiclosAulaImp.FData      := Data;
    FmCiclosAulaImp.FCidade    := Cidade;
    FmCiclosAulaImp.FUF        := UF;
    //
    FmCiclosAulaImp.ShowModal;
    FmCiclosAulaImp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCiclosLoc();
begin
  FCiclo := 0;
  if DBCheck.CriaFm(TFmCiclosLoc, FmCiclosLoc, afmoNegarComAviso) then
  begin
    FmCiclosLoc.ShowModal;
    FmCiclosLoc.Destroy;
  end;
end;

procedure TFmPrincipal.MostraGraGruFotos(Produto: Integer; Janela: String);
begin
  (*
  if DBCheck.CriaFm(TFmGraGruFotos, FmGraGruFotos, afmoNegarComAviso) then
  begin
    FmGraGruFotos.FJanela  := Janela;
    FmGraGruFotos.FProduto := Produto;
    FmGraGruFotos.ShowModal;
    FmGraGruFotos.Destroy;
  end;
  *)
  Geral.MB_Aviso('Fazer o GraGruFotos!');
end;

procedure TFmPrincipal.MostraPediPrzCab;
begin
  if DBCheck.CriaFm(TFmPediPrzCab, FmPediPrzCab, afmoNegarComAviso) then
  begin
    FmPediPrzCab.ShowModal;
    FmPediPrzCab.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPediPrzCab1;
begin
  if DBCheck.CriaFm(TFmPediPrzCab1, FmPediPrzCab1, afmoNegarComAviso) then
  begin
    FmPediPrzCab1.ShowModal;
    FmPediPrzCab1.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPromotores(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPromotores, FmPromotores, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then FmPromotores.LocCod(Codigo, Codigo);
    FmPromotores.ShowModal;
    FmPromotores.Destroy;
  end;
end;

procedure TFmPrincipal.MostraPromoWeb;
begin
  if DBCheck.CriaFm(TFmPromoWeb, FmPromoWeb, afmoNegarComAviso) then
  begin
    FmPromoWeb.ShowModal;
    FmPromoWeb.Destroy;
  end;
end;

procedure TFmPrincipal.MostraRelVendasCli;
begin
  if DBCheck.CriaFm(TFmRelVendasCli, FmRelVendasCli, afmoNegarComAviso) then
  begin
    FmRelVendasCli.ShowModal;
    FmRelVendasCli.Destroy;
  end;
end;

procedure TFmPrincipal.MostraVendaConf(Sender: TObject);
begin
  if DmodFin.QrLctosFatID.Value = 510 then
  begin
    if DmodFin.QrLctosCompensado.Value > 0 then
    begin
      Geral.MensagemBox('A��o cancelada! Lan�amento j� quitado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if DmodFin.QrLctosFatNum.Value = 0 then
    begin
      Geral.MensagemBox('A��o cancelada! Faturamento n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if DBCheck.CriaFm(TFmVendaConf, FmVendaConf, afmoNegarComAviso) then
    begin
      FmVendaConf.ShowModal;
      FmVendaConf.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.MostraVendaPesq(Depto: Integer);
begin
  if DBCheck.CriaFm(TFmVendaPesq, FmVendaPesq, afmoNegarComAviso) then
  begin
    FmVendaPesq.FDepto := Depto;
    FmVendaPesq.ShowModal;
    FmVendaPesq.Destroy;
    //
    case FIntTipo of
      1: CadastroVendas(FCodAlu, 0);
      2: MostraCartAval(FCodAlu);
      3: MostraMoviL(FCodAlu);
      4: MostraAlunoVis(FCodAlu2);
    end;
  end;
end;

procedure TFmPrincipal.Motivos1Click(Sender: TObject);
begin
  CadastroMotivos;
end;

procedure TFmPrincipal.Plano1Click(Sender: TObject);
begin
  CadastroDePlano();
end;

procedure TFmPrincipal.Configuraes1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TFmPrincipal.Configuraesdeconsulta1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Config, FmSPC_Config, afmoNegarComAviso) then
  begin
    FmSPC_Config.ShowModal;
    FmSPC_Config.Destroy;
  end;
end;

procedure TFmPrincipal.Conjuntos1Click(Sender: TObject);
begin
  CadastroDeConjutos();
end;

procedure TFmPrincipal.Grade1Click(Sender: TObject);
begin
  CadastroDeGrades;
end;

procedure TFmPrincipal.Grupodetexto1Click(Sender: TObject);
begin
  MostraCartaG;
end;

procedure TFmPrincipal.Grupos1Click(Sender: TObject);
begin
  CadastroDeGrupos;
end;

procedure TFmPrincipal.Subgrupos1Click(Sender: TObject);
begin
  CadastroDeSubGrupos;
end;

procedure TFmPrincipal.Contas1Click(Sender: TObject);
begin
  CadastroDeContas;
end;

procedure TFmPrincipal.Todosnveis1Click(Sender: TObject);
begin
  CadastroDeNiveisPlano();
end;

procedure TFmPrincipal.Listaplanos1Click(Sender: TObject);
begin
  ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.Listadepreos1Click(Sender: TObject);
begin
  CadastroLista;
end;

procedure TFmPrincipal.Listagem1Click(Sender: TObject);
begin
  CadastroDeContasSdoLista();
end;

procedure TFmPrincipal.Simples1Click(Sender: TObject);
begin
  CadastroDeContasSdoSimples(0, 0);
end;

procedure TFmPrincipal.Carteiras1Click(Sender: TObject);
begin
  CadastroDeCarteira;
end;

procedure TFmPrincipal.Cdigodebarras1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGradesCodBarra, FmGradesCodBarra, afmoNegarComAviso) then
  begin
    FmGradesCodBarra.ShowModal;
    FmGradesCodBarra.Destroy;
  end;
end;

procedure TFmPrincipal.Cdigoparagrades1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a atribui��o ao c�digo ' +
  '(refer�ncia) das mercadorias o n�mero do ID + 1000?', 'Pergunta',
  MB_YESNOCANCEL) = ID_Yes then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE grades SET CodUsu=Codigo+1000');
    Dmod.QrUpd.ExecSQL;
    Screen.Cursor := crDefault;
    //
    Geral.MensagemBox('Altera��o finalizada!', 'Aviso', MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmPrincipal.CentraldeSuporte1Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte;
  {$ENDIF}
end;

procedure TFmPrincipal.Centrosdecustos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCentroCusto, FmCentroCusto, afmoNegarComAviso) then
  begin
    FmCentroCusto.ShowModal;
    FmCentroCusto.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeCarteira;
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal.Transfernciaentrecontas2Click(Sender: TObject);
begin
  TransfereEntreContas(0, 0, 0, Date, 0);
end;

procedure TFmPrincipal.Usurios1Click(Sender: TObject);
begin
  CadastroWUsers;
end;

procedure TFmPrincipal.CadastroTamanhos;
begin
  if DBCheck.CriaFm(TFmTamanhos, FmTamanhos, afmoNegarComAviso) then
  begin
    FmTamanhos.ShowModal;
    FmTamanhos.Destroy;
  end;
end;

procedure TFmPrincipal.Avaliao1Click(Sender: TObject);
begin
  MostraCartAval(0);
end;

procedure TFmPrincipal.Avaliaopormdiadealunos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCiclosAval, FmCiclosAval, afmoNegarComAviso) then
  begin
    FmCiclosAval.ShowModal;
    FmCiclosAval.Destroy;
  end;
end;

procedure TFmPrincipal.BtImpMercadClick(Sender: TObject);
begin
  RelatoriosEstoque;
end;

procedure TFmPrincipal.BTLinVendaClick(Sender: TObject);
begin
  MostraMoviL(0);
end;

procedure TFmPrincipal.BtMercadClick(Sender: TObject);
begin
  CadastroProdutos;
end;

procedure TFmPrincipal.RelatoriosEstoque;
begin
  if DBCheck.CriaFm(TFmGraRel, FmGraRel, afmoNegarComAviso) then
  begin
    FmGraRel.ShowModal;
    FmGraRel.Destroy;
  end;
end;

procedure TFmPrincipal.RelatorioVendas;
begin
  if DBCheck.CriaFm(TFmRelVendas, FmRelVendas, afmoNegarComAviso) then
  begin
    FmRelVendas.ShowModal;
    FmRelVendas.Destroy;
  end;
end;

procedure TFmPrincipal.Relatriodevendas1Click(Sender: TObject);
begin
  RelatorioVendas;
end;

procedure TFmPrincipal.RelatriodeVendasporClientes1Click(Sender: TObject);
begin
  MostraRelVendasCli;
end;

procedure TFmPrincipal.BitBtn10Click(Sender: TObject);
var
  GraCusPrc: String;
  Codigo: Integer;
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Codigo, GraCusPrc');
  QrPesq.SQL.Add('FROM moviv');
  QrPesq.SQL.Add('WHERE GraCusPrc = 0');
  QrPesq.Open;
  //
  GraCusPrc := '';
  if InputQuery('Codigo da lista de pre�o', 'Informe o c�digo da lista de pre�o:', GraCusPrc) then
    GraCusPrc := Trim(GraCusPrc);
  if (QrPesq.RecordCount > 0) and (Length(GraCusPrc) > 0) then
  begin
    ProgressBar4.Max      := QrPesq.RecordCount;
    ProgressBar4.Position := 0;
    //
    QrPesq.First;
    while not QrPesq.Eof do
    begin
      ProgressBar4.Position := ProgressBar4.Position + 1;
      //
      Codigo := QrPesq.FieldByName('Codigo').Value;
      //
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE moviv SET GraCusPrc=:P0 WHERE Codigo=:P1');
      Dmod.QrUpdU.Params[0].AsInteger := Geral.IMV(GraCusPrc);
      Dmod.QrUpdU.Params[1].AsInteger := Codigo;
      Dmod.QrUpdU.ExecSQL;
      //
      QrPesq.Next;      
    end;
  end;
end;

procedure TFmPrincipal.BitBtn11Click(Sender: TObject);
begin
  GerenciaCiclos2(0, 0);
end;

procedure TFmPrincipal.BitBtn12Click(Sender: TObject);
var
  CPF, UF: String;
  Codigo, Munici, Conta: Integer;
begin
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('SELECT alu.Conta, alu.CPF, alu.Nome,');
  Dmod.QrUpdM.SQL.Add('alu.Telefone, alu.Celular, aul.CodiCidade, euf.Codigo UF');
  Dmod.QrUpdM.SQL.Add('FROM ciclosalu alu');
  Dmod.QrUpdM.SQL.Add('LEFT JOIN ciclosaula aul ON aul.Controle = alu.Controle');
  Dmod.QrUpdM.SQL.Add('LEFT JOIN ufs euf          ON euf.Nome=aul.UF');
  Dmod.QrUpdM.SQL.Add('WHERE CPF <> ""');
  Dmod.QrUpdM.Open;
  if Dmod.QrUpdM.RecordCount > 0 then
  begin
    PB5.Max      := Dmod.QrUpdM.RecordCount;
    PB5.Position := 0;
    PB5.Visible  := True;
    //
    Dmod.QrUpdM.First;
    while not Dmod.QrUpdM.Eof do
    begin
      Conta  := Dmod.QrUpdM.FieldByName('Conta').Value;
      if Dmod.QrUpdM.FieldByName('CPF').Value <> Null then
        CPF := Dmod.QrUpdM.FieldByName('CPF').Value
      else
        CPF := '';
      if Dmod.QrUpdM.FieldByName('CodiCidade').Value <> Null then
        Munici := Dmod.QrUpdM.FieldByName('CodiCidade').Value
      else
        Munici := 0;
      if Dmod.QrUpdM.FieldByName('UF').Value <> Null then
        UF := Dmod.QrUpdM.FieldByName('UF').Value
      else
        UF := '';
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('SELECT Codigo FROM entidades WHERE CPF = "'+ CPF +'"');
      Dmod.QrUpd.Open;
      if Dmod.QrUpd.RecordCount > 0 then
      begin
        Codigo := Dmod.QrUpd.FieldByName('Codigo').Value;
        if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'entidades', False,
        [
          'PCodMunici', 'PUF', 'PCodiPais'
        ], ['Codigo'], [
          Munici, UF, 1058
        ], [Codigo]) then
        begin
          if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'ciclosalu', False,
          [
            'Entidade'
          ], ['Conta'], [
            Codigo
          ], [Conta]) then
          begin
          end;
        end;
      end;
      //
      Application.ProcessMessages;
      PB5.Position := PB5.Position + 1;
      PB5.Update;
      //
      Dmod.QrUpdM.Next;
    end;
  end;
  PB5.Visible := False;
  Dmod.QrUpdM.Close;
end;

procedure TFmPrincipal.BitBtn13Click(Sender: TObject);
var
  Codigo, Controle, RolComis, Turma: Integer;
begin
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('SELECT mom.SubCta, mom.Controle, cic.Codigo, cic.RolComis');
  Dmod.QrUpdM.SQL.Add('FROM movim mom');
  Dmod.QrUpdM.SQL.Add('LEFT JOIN ciclos cic ON cic.Controle = mom.Controle');
  Dmod.QrUpdM.SQL.Add('WHERE mom.SubCtrl=3');
  Dmod.QrUpdM.SQL.Add('AND mom.SubCta = 0');
  Dmod.QrUpdM.SQL.Add('GROUP BY mom.Controle');
  Dmod.QrUpdM.Open;
  if Dmod.QrUpdM.RecordCount > 0 then
  begin
    PB5.Visible  := True;
    PB5.Max      := Dmod.QrUpdM.RecordCount;
    PB5.Position := 0;
    //
    while not Dmod.QrUpdM.Eof do
    begin
      Codigo   := Dmod.QrUpdM.FieldByName('Codigo').Value;
      Controle := Dmod.QrUpdM.FieldByName('Controle').Value;
      RolComis := Dmod.QrUpdM.FieldByName('RolComis').Value;
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('SELECT Controle FROM ciclosaula WHERE Codigo=:P0 LIMIT 1');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.Open;
      if Dmod.QrUpd.RecordCount > 0 then
      begin
        Turma := Dmod.QrUpd.FieldByName('Controle').Value;
        //
        Dmod.QrUpdZ.Close;
        Dmod.QrUpdZ.SQL.Clear;
        Dmod.QrUpdZ.SQL.Add('UPDATE movim SET SubCta=:P0 WHERE Controle=:P1');
        Dmod.QrUpdZ.Params[0].AsInteger := Turma;
        Dmod.QrUpdZ.Params[1].AsInteger := Controle;
        Dmod.QrUpdZ.ExecSQL;
        //
        DmCiclos.CalculaVenComDia(Controle, Turma, RolComis);
      end;
      PB5.Position := PB5.Position + 1;
      PB5.Update;
      Application.ProcessMessages;
      //
      Dmod.QrUpdM.Next;
    end;
  end;
  PB5.Visible := False;
end;

procedure TFmPrincipal.BitBtn14Click(Sender: TObject);
var
  Conta: Integer;
  Database: String;
begin
  Database := '';
  if InputQuery('Nome do banco de dados', 'Nome do banco de dados:', Database) then
  begin
    MyDBTMP.DatabaseName := Database;
    MyDBTMP.Connect;
    //
    Screen.Cursor := crHourGlass;
    //
    QrTmpMoviM.Close;
    QrTmpMoviM.SQL.Clear;
    QrTmpMoviM.SQL.Add('SELECT * FROM movim');
    QrTmpMoviM.Open;
    if QrTmpMoviM.RecordCount > 0 then
    begin
      PB5.Visible  := True;
      PB5.Position := 0;
      PB5.Max      := QrTmpMoviM.RecordCount;
      //
      QrTmpMoviM.First;
      while not QrTmpMoviM.Eof do
      begin
        Conta := QrTmpMoviM.FieldByName('Conta').Value;
        //
        QrLocMoviM.Close;
        QrLocMoviM.SQL.Clear;
        QrLocMoviM.SQL.Add('SELECT * FROM movim WHERE Conta=:P0');
        QrLocMoviM.Params[0].AsInteger := Conta;
        QrLocMoviM.Open;
        if QrLocMoviM.RecordCount = 0 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO movim SET Controle=:P00, Conta=:P01, ');
          Dmod.QrUpd.SQL.Add('Grade=:P02, Cor=:P03, Tam=:P04, Qtd=:P05, Val=:P06, ');
          Dmod.QrUpd.SQL.Add('Ven=:P07, DataPedi=:P08, DataReal=:P09, Motivo=:P10, ');
          Dmod.QrUpd.SQL.Add('SALDOQTD=:P11, SALDOVAL=:P12, Lk=:P13, DataCad=:P14, ');
          Dmod.QrUpd.SQL.Add('DataAlt=:P15, UserCad=:P16, UserAlt=:P17, AlterWeb=:P18, ');
          Dmod.QrUpd.SQL.Add('Ativo=:P19, SubCtrl=:P20, ComisTip=:P21, ComisFat=:P22, ');
          Dmod.QrUpd.SQL.Add('ComisVal=:P23, SubCta=:P24, Kit=:P25, IDCtrl=:P26');
          Dmod.QrUpd.Params[00].AsInteger := QrTmpMoviMControle.Value;
          Dmod.QrUpd.Params[01].AsInteger := QrTmpMoviMConta.Value;
          Dmod.QrUpd.Params[02].AsInteger := QrTmpMoviMGrade.Value;
          Dmod.QrUpd.Params[03].AsInteger := QrTmpMoviMCor.Value;
          Dmod.QrUpd.Params[04].AsInteger := QrTmpMoviMTam.Value;
          Dmod.QrUpd.Params[05].AsFloat   := QrTmpMoviMQtd.Value;
          Dmod.QrUpd.Params[06].AsFloat   := QrTmpMoviMVal.Value;
          Dmod.QrUpd.Params[07].AsFloat   := QrTmpMoviMVen.Value;
          Dmod.QrUpd.Params[08].AsDate    := QrTmpMoviMDataPedi.Value;
          Dmod.QrUpd.Params[09].AsDate    := QrTmpMoviMDataReal.Value;
          Dmod.QrUpd.Params[10].AsInteger := QrTmpMoviMMotivo.Value;
          Dmod.QrUpd.Params[11].AsFloat   := QrTmpMoviMSALDOQTD.Value;
          Dmod.QrUpd.Params[12].AsFloat   := QrTmpMoviMSALDOVAL.Value;
          Dmod.QrUpd.Params[13].AsInteger := QrTmpMoviMLk.Value;
          Dmod.QrUpd.Params[14].AsDate    := QrTmpMoviMDataCad.Value;
          Dmod.QrUpd.Params[15].AsDate    := QrTmpMoviMDataAlt.Value;
          Dmod.QrUpd.Params[16].AsInteger := QrTmpMoviMUserCad.Value;
          Dmod.QrUpd.Params[17].AsInteger := QrTmpMoviMUserAlt.Value;
          Dmod.QrUpd.Params[18].AsInteger := QrTmpMoviMAlterWeb.Value;
          Dmod.QrUpd.Params[19].AsInteger := QrTmpMoviMAtivo.Value;
          Dmod.QrUpd.Params[20].AsInteger := QrTmpMoviMSubCtrl.Value;
          Dmod.QrUpd.Params[21].AsInteger := QrTmpMoviMComisTip.Value;
          Dmod.QrUpd.Params[22].AsFloat   := QrTmpMoviMComisFat.Value;
          Dmod.QrUpd.Params[23].AsFloat   := QrTmpMoviMComisVal.Value;
          Dmod.QrUpd.Params[24].AsInteger := QrTmpMoviMSubCta.Value;
          Dmod.QrUpd.Params[25].AsInteger := QrTmpMoviMKit.Value;
          Dmod.QrUpd.Params[26].AsInteger := QrTmpMoviMIDCtrl.Value;
          Dmod.QrUpd.ExecSQL;
        end;
        //
        PB5.Position := PB5.Position + 1;
        PB5.Update;
        Application.ProcessMessages;
        QrTmpMoviM.Next;
      end;
      PB5.Visible := False;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  CadastroCompras;
end;

procedure TFmPrincipal.BitBtn2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroCompras;
begin
  if DBCheck.CriaFm(TFmMoviC, FmMoviC, afmoNegarComAviso) then
  begin
    FmMoviC.ShowModal;
    FmMoviC.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroProdutos;
begin
  if DBCheck.CriaFm(TFmGraGru, FmGraGru, afmoNegarComAviso) then
  begin
    FmGraGru.ShowModal;
    FmGraGru.Destroy;
  end;
end;

procedure TFmPrincipal.CarregaFotoKit(Imagem: TImage; Produto: String);
var
  ImageMem : TMemoryStream;
  ImageJpg: TJPEGImage;
  Proxy: Integer;
begin
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  if Length(Produto) > 0 then
  begin
    if Dmod.QrControlePrdFotoWeb.Value = 1 then
    begin
      ImageMem := TMemoryStream.Create;
      ImageJpg := TJPEGImage.Create;
      try
        try
          IdHTTP1.Get('http://' + Produto, ImageMem);
        except on e: EIdHTTPProtocolException do
          begin
            if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
            begin
              // N�o achou!
              Exit;
            end;
          end;
        end;
        ImageMem.Position := 0;
        ImageJpg.LoadFromStream(ImageMem);
        Imagem.Picture.Assign(ImageJpg);
      finally
        ImageJpg.Free;
        ImageMem.Free;
      end;
    end else
    begin
      if FmPrincipal.CarregaImg(Produto) then
        Imagem.Picture.LoadFromFile(Produto)
      else
        Imagem.Picture := nil;
    end;
  end else
    Imagem.Picture := nil;
end;

procedure TFmPrincipal.CarregaFoto(Imagem: TImage; Produto: String);
var
  ImageMem : TMemoryStream;
  ImageJpg: TJPEGImage;
  Proxy: Integer;
begin
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  if Length(Produto) > 0 then
  begin
    if Dmod.QrControlePrdFotoWeb.Value = 1 then
    begin
      ImageMem := TMemoryStream.Create;
      ImageJpg := TJPEGImage.Create;
      try
        try
          IdHTTP1.Get('http://' + Produto, ImageMem);
        except on e: EIdHTTPProtocolException do
          begin
            if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
            begin
              // N�o achou!
              Exit;
            end;
          end;
        end;
        ImageMem.Position := 0;
        ImageJpg.LoadFromStream(ImageMem);
        Imagem.Picture.Assign(ImageJpg);
      finally
        ImageJpg.Free;
        ImageMem.Free;
      end;
    end else
    begin
      if CarregaImg(Produto) then
        Imagem.Picture.LoadFromFile(Produto)
      else
        Imagem.Picture := nil;
    end;
  end else
    Imagem.Picture := nil;
end;

function TFmPrincipal.CarregaImg(Foto: String): Boolean;
begin
  if VerificaImg(Foto) then
    Result := True
  else
    Result := False;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  Result := 0;//FmFaturas.QrFaturasCodigo.Value;
end;

procedure TFmPrincipal.Comisses1Click(Sender: TObject);
begin
  CadastroEquiCom(0);
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  Result := '';//FormatDateTime(VAR_FORMATDATE, FmFaturas.QrFaturasData.Value);
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  if MyObjects.CriaForm_AcessoTotal(TFmCalcPercent, FmCalcPercent) then
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.Impressesdemercadorias1Click(Sender: TObject);
begin
  RelatoriosEstoque;
end;

procedure TFmPrincipal.Entradademercadorias1Click(Sender: TObject);
begin
  CadastroCompras;
end;

procedure TFmPrincipal.SaldoDeContas;
begin
  if DBCheck.CriaFm(TFmContasHistSdo, FmContasHistSdo, afmoNegarComAviso) then
  begin
    FmContasHistSdo.ShowModal;
    FmContasHistSdo.Destroy;
  end;
end;

procedure TFmPrincipal.BtSdoContasClick(Sender: TObject);
begin
  SaldoDeContas;
end;

procedure TFmPrincipal.Saldodecontas1Click(Sender: TObject);
begin
  SaldoDeContas;
end;

procedure TFmPrincipal.Button10Click(Sender: TObject);
const
  my_key = 13456;
var
  i, k: integer;
  ti, tf, te, tt: TTime;
  sEncrypted, sDecrypted: AnsiString;
  TextoSenha: String;
begin
  Memo1.lines.Clear;
  ti := Now();
  Screen.Cursor := crHourGlass;
  k := 0;
  for i := 0 to 9999 do
  begin
    if CkRandom.Checked then
    begin
      Randomize;
      TextoSenha := Copy(dmkPF.PWDGenerateSecutityString, 1, 8);
    end else TextoSenha := Edit4.Text;
    //
    sEncrypted := UnEncrypt.Encrypt(FormatFloat('0000', i) + TextoSenha, my_key);
    //sDecrypted := UnEncrypt.Decrypt(sEncrypted, my_key);

    sDecrypted := UnEncrypt.Decrypt(sEncrypted, my_key);

    Memo1.Lines.Add(sEncrypted + '  ::  ' + sDecrypted);
    if Length(sEncrypted) > k then
      k := Length(sEncrypted);
  end;
  tf := Now();
  tt := tf - ti;
  te := 0;//te - ti;
  TempoI.Caption := FormatDateTime('h:nn:ss:zzz', ti);
  TempoF.Caption := FormatDateTime('h:nn:ss:zzz', tf);
  TempoE.Caption := FormatDateTime('h:nn:ss:zzz', te);
  TempoT.Caption := FormatDateTime('h:nn:ss:zzz', tt);
  Letras.Caption := IntToStr(k);
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
(*
var
  IniDir, Arquivo: String;
*)
begin
  Application.CreateForm(TFmObtemFoto4, FmObtemFoto4);
  FmObtemFoto4.ShowModal;
  FmObtemFoto4.Destroy;
  (*
  IniDir := ExtractFileDir(Application.ExeName);
  Arquivo := '';
  if MyObjects.FileOpenDialog(FmPrincipal, IniDir, Arquivo,
  'Selecione o arquivo de imagem', '', [], Arquivo) then
  begin
    ImgPrincipal.Picture.LoadFromFile(Arquivo);
    Geral.WriteAppKey('ImagemFundo', Application.Title,
      Arquivo, ktString, HKEY_LOCAL_MACHINE);
  end;
  *)
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
begin
  Edit2.Text := UnMD5.StrMD5(Edit1.Text);
  Edit3.Text := UnMD5.StrMD5(Edit2.Text);
end;

procedure TFmPrincipal.BtVersaoClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.Button4Click(Sender: TObject);
var
  FatorCript: Integer;
begin
  FatorCript := Geral.IMV(EdFatorCript.Text);
  Edit2.Text := MLAGeral.GeraTextoCript(Edit1.Text, FatorCript, 9823, 3469);
  Edit3.Text := MLAGeral.AbreTextoCript(Edit2.Text, FatorCript, 9823, 3469);
end;

procedure TFmPrincipal.Button5Click(Sender: TObject);
begin
  Edit2.Text := MLAGeral.GeraCript(Edit1.Text, 227, 24, 9823, 3469);
  Edit3.Text := '';
end;

procedure TFmPrincipal.Button6Click(Sender: TObject);
var
  i: integer;
  ti, tf, te, tt: TTime;
begin
  ti := Now();
  te := 0;
  Screen.Cursor := crHourGlass;
  for i := 0 to 9999 do
  begin
    if UnMD5.StrMD5(FormatFloat('0000', i) + Edit4.Text) = Edit2.Text then
      te := Now();
  end;
  tf := Now();
  tt := tf - ti;
  te := te - ti;
  TempoI.Caption := FormatDateTime('h:nn:ss:zzz', ti);
  TempoF.Caption := FormatDateTime('h:nn:ss:zzz', tf);
  TempoE.Caption := FormatDateTime('h:nn:ss:zzz', te);
  TempoT.Caption := FormatDateTime('h:nn:ss:zzz', tt);
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.Button7Click(Sender: TObject);
var
   Fatores: TWordTriple;
begin
  Fatores[0] := 127;
  Fatores[1] := 134;
  Fatores[2] := 150;
  Edit2.Text := TextEncrypt(Edit1.Text, Fatores);
  Edit3.text := TextDecrypt(Edit2.Text, Fatores)
end;

procedure TFmPrincipal.Button8Click(Sender: TObject);
begin
  Randomize;
  Edit2.Text := dmkPF.PWDExEncode(Edit1.Text, 'ABCDEFGHIJKLMNOP');
  Edit3.Text := dmkPF.PWDExDecode(Edit2.Text, 'ABCDEFGHIJKLMNOP');
end;

procedure TFmPrincipal.Button9Click(Sender: TObject);
var
  sEncrypted, sDecrypted :AnsiString;
begin
  // Encrypt a string
  sEncrypted := UnEncrypt.Encrypt(Edit1.Text, Fmy_key_uEncrypt);
  Edit2.text := sEncrypted;
  EdChave.Text := MLAGeral.ParticionaTexto(sEncrypted, 4);


  // Decrypt the string
  sDecrypted := UnEncrypt.Decrypt(sEncrypted, Fmy_key_uEncrypt);
  Edit3.Text := sDecrypted;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'LeSew', 'LeSew',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO, CO_DMKID_APP,
    DModG.ObtemAgora(), Memo3, dtExec, Versao, False, ApenasVerifica,
    BalloonHint1);
end;

procedure TFmPrincipal.Verificanovaverso1Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
var
  Dia: Integer;
begin
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes then
      Application.Terminate;
  end else
    Application.Terminate;
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    nil, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.Backup1Click(Sender: TObject);
begin
  MostraBackup3;
end;

procedure TFmPrincipal.Bancos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.AtzSdoContas;
begin
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := 0;
    FmContasHistAtz2.FGenero  := 0;
    FmContasHistAtz2.FPeriodo := -23999;
    //
    FmContasHistAtz2.ShowModal;
    FmContasHistAtz2.Destroy;
  end;
end;

procedure TFmPrincipal.BitBtn9Click(Sender: TObject);
begin
  AtzSdoContas;
end;

procedure TFmPrincipal.Atualizasaldodecontas1Click(Sender: TObject);
begin
  AtzSdoContas;
end;

procedure TFmPrincipal.Planodecontas2Click(Sender: TObject);
begin
  ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.Tamanhos2Click(Sender: TObject);
begin
  CadastroTamanhos;
end;

procedure TFmPrincipal.Inventario;
begin
  if DBCheck.CriaFm(TFmMoviB, FmMoviB, afmoNegarComAviso) then
  begin
    FmMoviB.ShowModal;
    FmMoviB.Destroy;
  end;
end;

procedure TFmPrincipal.Inventrio1Click(Sender: TObject);
begin
  Inventario;
end;

procedure TFmPrincipal.Kits1Click(Sender: TObject);
begin
  CadastroDeKits;
end;

procedure TFmPrincipal.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

procedure TFmPrincipal.BtAlunosClick(Sender: TObject);
begin
  MostraVendaPesq(0);
  //MyObjects.MostraPopUpDeBotao(PMAlunos, BtAlunos);
end;

procedure TFmPrincipal.BtAtualizarClick(Sender: TObject);
var
  Codigo, StatusV: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Codigo');
  QrPesq.SQL.Add('FROM moviv');
  QrPesq.SQL.Add('WHERE StatAtual = 0');
  QrPesq.Open;
  //
  if QrPesq.RecordCount > 0 then
  begin
    ProgressBar1.Max      := QrPesq.RecordCount;
    ProgressBar1.Position := 0;
    //
    QrPesq.First;
    while not QrPesq.Eof do
    begin
      Codigo := QrPesq.FieldByName('Codigo').Value;
      //
      QrPesq4.Close;
      QrPesq4.Params[0].AsInteger := Codigo;
      QrPesq4.Open;
      //
      StatusV := QrPesq4StatusV.Value;
      //
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE moviv SET StatAtual=:P0 WHERE Codigo=:P1');
      Dmod.QrUpdU.Params[0].AsInteger := StatusV;
      Dmod.QrUpdU.Params[1].AsInteger := Codigo;
      Dmod.QrUpdU.ExecSQL;
      //
      ProgressBar1.Position := ProgressBar1.Position + 1;
      QrPesq.Next;
    end;
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT Codigo');
    QrPesq.SQL.Add('FROM moviv');
    QrPesq.SQL.Add('WHERE StatAtual = 0');
    QrPesq.Open;
    if QrPesq.RecordCount > 0 then
    begin
      QrPesq.First;
      while not QrPesq.Eof do
      begin
        Codigo := QrPesq.FieldByName('Codigo').Value;
        //
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE moviv SET StatAtual=:P0 WHERE Codigo=:P1');
        Dmod.QrUpdU.Params[0].AsInteger := Dmod.QrControleVPStat1.Value;
        Dmod.QrUpdU.Params[1].AsInteger := Codigo;
        Dmod.QrUpdU.ExecSQL;
        //
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('UPDATE moviv SET StatAtual=:P0 WHERE Codigo=:P1');
        Dmod.QrUpdU.Params[0].AsInteger := Dmod.QrControleVPStat1.Value;
        Dmod.QrUpdU.Params[1].AsInteger := Codigo;
        Dmod.QrUpdU.ExecSQL;
        //
        QrPesq.Next;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
  ShowMessage('Atualiza��o finalizada!');
end;

procedure TFmPrincipal.BtAvalClick(Sender: TObject);
begin
  MostraCartAval(0);
end;

procedure TFmPrincipal.EQuipes1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEquiGru, FmEquiGru, afmoNegarComAviso) then
  begin
    FmEquiGru.ShowModal;
    FmEquiGru.Destroy;
  end;
end;

procedure TFmPrincipal.Ciclos1Click(Sender: TObject);
begin
  GerenciaCiclos;
end;

procedure TFmPrincipal.CiclosAval1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCiclosAvalImp, FmCiclosAvalImp, afmoNegarComAviso) then
  begin
    FmCiclosAvalImp.ShowModal;
    FmCiclosAvalImp.Destroy;
  end;
end;

procedure TFmPrincipal.ClculosdeMduloseBloquetos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCalcMod10e11, FmCalcMod10e11, afmoNegarComAviso) then
  begin
    FmCalcMod10e11.ShowModal;
    FmCalcMod10e11.Destroy;
  end;
end;

procedure TFmPrincipal.ClientesInternos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCliInt, FmCliInt, afmoNegarComAviso) then
  begin
    FmCliInt.ShowModal;
    FmCliInt.Destroy;
  end;
end;

procedure TFmPrincipal.Mercadorias3Click(Sender: TObject);
begin
  (*
  if DBCheck.CriaFm(TFmOpcoesM, FmOpcoesM, afmoNegarComAviso) then
  begin
    FmOpcoesM.ShowModal;
    FmOpcoesM.Destroy;
  end;
  *)
end;

procedure TFmPrincipal.BitBtn3Click(Sender: TObject);
begin
  CadastroVendas(0, 0);
end;

procedure TFmPrincipal.CadastroVendas(Codigo, Cliente: Integer);
begin
  if DBCheck.CriaFm(TFmMoviV, FmMoviV, afmoNegarComAviso) then
  begin
    FmMoviV.FCliente := Cliente;
    if Codigo > 0 then
      FmMoviV.LocCod(Codigo, Codigo);
    FmMoviV.ShowModal;
    FmMoviV.Destroy;
    //
    if FCodDepto > 0 then
      MostraVendaPesq(FDepto);
  end;
end;

procedure TFmPrincipal.CadastroVendasEdit(Codigo, Controle, Cliente: Integer;
  Tipo: TSQLType);
begin
  if DBCheck.CriaFm(TFmMoviVEdit, FmMoviVEdit, afmoNegarComAviso) then
  begin
    FmMoviVEdit.LaTipo.SQLType := Tipo;
    FmMoviVEdit.FCodigo        := Codigo;
    FmMoviVEdit.FControle      := Controle;
    FmMoviVEdit.FCliente       := Cliente;
    //
    FmMoviVEdit.ShowModal;
    FmMoviVEdit.Destroy;
    FmMoviVEdit := nil;
  end;
end;

procedure TFmPrincipal.VCLSkin1Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.Venda1Click(Sender: TObject);
begin
  MostraMoviL(0);
end;

procedure TFmPrincipal.Vendadeprodutos1Click(Sender: TObject);
begin
  CadastroVendas(0, 0);
end;

procedure TFmPrincipal.BitBtn4Click(Sender: TObject);
begin
  GerenciaCiclos;
end;

procedure TFmPrincipal.BitBtn5Click(Sender: TObject);
var
  CliInt: Integer;
begin
  CliInt := Dmod.QrControleCiclCliInt.Value;
  if CliInt > 0 then
  begin
    QrCliInt.Close;
    QrCliInt.Params[0].AsInteger := CliInt;
    QrCliInt.Open;
    //
    DefineVarsCliInt(QrCliIntCliente.Value);
  end;
  //
  if DBCheck.CriaFm(TFmCiclosProm, FmCiclosProm, afmoNegarComAviso) then
  begin
    FmCiclosProm.ShowModal;
    FmCiclosProm.Destroy;
    FmCiclosProm := nil;
  end;
end;

procedure TFmPrincipal.BitBtn6Click(Sender: TObject);
var
  CliInt: Integer;
begin
  CliInt := Dmod.QrControleCiclCliInt.Value;
  if CliInt > 0 then
  begin
    QrCliInt.Close;
    QrCliInt.Params[0].AsInteger := CliInt;
    QrCliInt.Open;
    //
    DefineVarsCliInt(QrCliIntCliente.Value);
  end;
  //
  if DBCheck.CriaFm(TFmCiclosProf, FmCiclosProf, afmoNegarComAviso) then
  begin
    FmCiclosProf.ShowModal;
    FmCiclosProf.Destroy;
    FmCiclosProf := nil;
  end;
end;

procedure TFmPrincipal.BitBtn7Click(Sender: TObject);
var
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Codigo, Controle');
  QrPesq.SQL.Add('FROM moviv mov');
  QrPesq.Open;
  //
  ProgressBar2.Max      := QrPesq.RecordCount;
  ProgressBar2.Position := 0;
  //
  if QrPesq.RecordCount > 0 then
  begin
    QrPesq.First;
    while not QrPesq.Eof do
    begin
      Controle := QrPesq.FieldByName('Controle').Value;
      //
      QrPesq2.Close;
      QrPesq2.SQL.Clear;
      QrPesq2.SQL.Add('SELECT mom.Kit, mom.Controle, mov.Codigo');
      QrPesq2.SQL.Add('FROM movim mom');
      QrPesq2.SQL.Add('LEFT JOIN moviv mov ON mov.Controle = mom.Controle');
      QrPesq2.SQL.Add('WHERE mom.Kit > 0');
      QrPesq2.SQL.Add('AND mom.Controle=:P0');
      QrPesq2.Params[0].AsInteger := Controle;
      QrPesq2.Open;
      //
      if QrPesq2.RecordCount > 0 then
      begin
        QrPesq2.First;
        while not QrPesq2.Eof do
        begin
          QrPesq3.Close;
          QrPesq3.SQL.Clear;
          QrPesq3.SQL.Add('SELECT Controle');
          QrPesq3.SQL.Add('FROM movivk');
          QrPesq3.SQL.Add('WHERE Codigo=:P0');
          QrPesq3.SQL.Add('AND GradeK=:P1');
          QrPesq3.Params[0].AsInteger := QrPesq2.FieldByName('Codigo').Value;
          QrPesq3.Params[1].AsInteger := QrPesq2.FieldByName('Kit').Value;
          QrPesq3.Open;
          //
          if QrPesq3.RecordCount > 0 then
          begin
            QrPesq3.First;
            while not QrPesq3.Eof do
            begin
              Dmod.QrUpdU.SQL.Clear;
              Dmod.QrUpdU.SQL.Add('UPDATE movim SET Kit=:P0 WHERE Kit=:P1 AND Controle=:P2');
              Dmod.QrUpdU.Params[0].AsInteger := QrPesq3.FieldByName('Controle').Value;
              Dmod.QrUpdU.Params[1].AsInteger := QrPesq2.FieldByName('Kit').Value;
              Dmod.QrUpdU.Params[2].AsInteger := QrPesq2.FieldByName('Controle').Value;
              Dmod.QrUpdU.ExecSQL;
              //
              QrPesq3.Next;
            end;
          end;
          //
          QrPesq2.Next;
        end;
      end;
      ProgressBar2.Position := ProgressBar2.Position + 1;
      //
      QrPesq.Next;
    end;
  end;
  Screen.Cursor := crDefault;
  ShowMessage('Atualiza��o finalizada!');
end;

procedure TFmPrincipal.BitBtn8Click(Sender: TObject);
var
  Controle: Integer;
  Total: Double;
begin
  Screen.Cursor := crHourGlass;
  //
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Controle');
  QrPesq.SQL.Add('FROM movivk');
  QrPesq.Open;
  //
  if QrPesq.RecordCount > 0 then
  begin
    ProgressBar3.Position := 0;
    ProgressBar3.Max      := QrPesq.RecordCount;
    //
    QrPesq.First;
    while not QrPesq.Eof do
    begin
      Controle := QrPesq.FieldByName('Controle').Value;
      //
      QrPesq5.Close;
      QrPesq5.Params[0].AsInteger := Controle;
      QrPesq5.Open;
      //
      Total := QrPesq5Total.Value;
      //
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE movivk SET Total=:P0 WHERE Controle=:P1');
      Dmod.QrUpdU.Params[0].AsFloat   := Total;
      Dmod.QrUpdU.Params[1].AsInteger := Controle;
      Dmod.QrUpdU.ExecSQL;
      //
      ProgressBar3.Position := ProgressBar3.Position + 1;
      QrPesq.Next;
    end;
  end;
  Screen.Cursor := crDefault;
  ShowMessage('Atualiza��o finalizada!');
end;

procedure TFmPrincipal.GerenciaCiclos();
var
  CliInt: Integer;
begin
  CliInt := Dmod.QrControleCiclCliInt.Value;
  if CliInt > 0 then
  begin
    QrCliInt.Close;
    QrCliInt.Params[0].AsInteger := CliInt;
    QrCliInt.Open;
    //
    DefineVarsCliInt(QrCliIntCliente.Value);
  end;
  //
  if DBCheck.CriaFm(TFmCiclos, FmCiclos, afmoNegarComAviso) then
  begin
    FmCiclos.ShowModal;
    FmCiclos.Destroy;
  end;
end;

procedure TFmPrincipal.GerenciaCiclos2(Codigo, Controle: Integer);
var
  CliInt: Integer;
begin
  CliInt := Dmod.QrControleCiclCliInt.Value;
  if CliInt > 0 then
  begin
    QrCliInt.Close;
    QrCliInt.Params[0].AsInteger := CliInt;
    QrCliInt.Open;
    //
    DefineVarsCliInt(QrCliIntCliente.Value);
  end;
  //
  if DBCheck.CriaFm(TFmCiclos2, FmCiclos2, afmoNegarComAviso) then
  begin
    if (Codigo > 0) and (Controle > 0) then
    begin
      FmCiclos2.LocCod(Codigo, Codigo);
      FmCiclos2.ReopenCiclosAula(Controle);
      FmCiclos2.PageControl1.ActivePageIndex := 1;
    end;
    FmCiclos2.ShowModal;
    FmCiclos2.Destroy;
  end;
end;

procedure TFmPrincipal.GerenciaProfessores1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCiclosProf, FmCiclosProf, afmoNegarComAviso) then
  begin
    FmCiclosProf.ShowModal;
    FmCiclosProf.Destroy;
    //FmCiclosProf := nil;
  end;
end;

procedure TFmPrincipal.GerenciaPromotores1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCiclosProm, FmCiclosProm, afmoNegarComAviso) then
  begin
    FmCiclosProm.ShowModal;
    FmCiclosProm.Destroy;
    FmCiclosProm := nil;
  end;
end;

procedure TFmPrincipal.Cursos1Click(Sender: TObject);
begin
  CadastroDeCursos;
end;

procedure TFmPrincipal.Cadastra1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiRapido, FmEntiRapido, afmoNegarComAviso) then
  begin
    FmEntiRapido.LaTipo.SQLType := stIns;
    FmEntiRapido.ShowModal;
    FmEntiRapido.Destroy;
  end;
end;

procedure TFmPrincipal.Cadastro1Click(Sender: TObject);
begin
  CadastroDeCursos;
end;

procedure TFmPrincipal.Cadastro2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPtosCadPto, FmPtosCadPto, afmoNegarComAviso) then
  begin
    FmPtosCadPto.ShowModal;
    FmPtosCadPto.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeCursos();
begin
  if DBCheck.CriaFm(TFmCursos, FmCursos, afmoNegarComAviso) then
  begin
    FmCursos.ShowModal;
    FmCursos.Destroy;
  end;
end;

procedure TFmPrincipal.Especficos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOpcoesLeSew, FmOpcoesLeSew, afmoNegarComAviso) then
  begin
    FmOpcoesLeSew.ShowModal;
    FmOpcoesLeSew.Destroy;
  end;
end;

procedure TFmPrincipal.Etiquetas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.extos1Click(Sender: TObject);
begin
  MostraCartas;
end;

procedure TFmPrincipal.BtFinancasClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if DBCheck.CriaFm(TFmCliIntSel, FmCliIntSel, afmoNegarComAviso) then
  begin
    case FmCliIntSel.QrCliInt.RecordCount of
      0: Geral.MensagemBox('N�o h� cliente interno cadastrado!', 'Aviso',
         MB_OK+MB_ICONWARNING);
      1:
      begin
        DefineVarsCliInt(FmCliIntSel.QrCliIntCodigo.Value);
      end else begin
        Screen.Cursor := crDefault;
        FmCliIntSel.ShowModal;
        DefineVarsCliInt(FmCliIntSel.FEntInt);
      end;
    end;
    FmCliIntSel.Destroy;
    Application.ProcessMessages;
    if VAR_LIB_EMPRESAS <> '0' then
    begin
      DmodFin.ReabreCarteiras(0, DmodFin.QrCarts, DmodFin.QrCartSum, 'TFmPrincipal.BtFinancasClick()');
      if DBCheck.CriaFm(TFmSelfGer, FmSelfGer, afmoNegarComAviso) then
      begin
        FmSelfGer.BtEspecificos.Visible := True;
        FmSelfGer.BtEspecificos.Caption := 'Conf Vda.';
        FmSelfGer.BtEspecificos.OnClick := MostraVendaConf; 
        {FmSelfGer.BtEspecificos.Visible := True;
        FmSelfGer.BtEspecificos.Caption := 'Conf Vda';
        MenuItem := TMenuItem.Create(FmSelfGer);
        MenuItem.Caption := 'Confirma��o de venda';
        MenuItem.OnClick := MostraVendaConf;
        FmSelfGer.PMEspecificos.Items.Add(MenuItem);}
        //
        FmSelfGer.ShowModal;
        FmSelfGer.Destroy;
        Application.OnHint := ShowHint;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade por enquanto
  {
  if DBCheck.CriaFm(TFmCNAB_Ret2, FmCNAB_Ret2, afmoNegarComAviso) then
  begin
    FmCNAB_Ret2.ShowModal;
    FmCNAB_Ret2.Destroy;
  end;
  }
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade1: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  //MLAGeral.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  //Apenas compatibilidade usado no Syndi2
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
//  Compatibilidade
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
//  Compatibilidade
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
//  Compatibilidade
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.CadastroDeEntidades(Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then FmEntidade2.LocCod(Entidade, Entidade);
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.CriaPages;
begin
  if DBCheck.CriaFm(TFmPages, FmPages, afmoNegarComAviso) then
  begin
    FmPages.ShowModal;
    FmPages.Destroy;
  end;
end;

procedure TFmPrincipal.CriaMinhasEtiquetas;
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
end;

procedure TFmPrincipal.Button11Click(Sender: TObject);
var
  Grupo, Numero: Integer;
begin
  Dmod.VerificaSenhaEntidade(EdChave.Text, Grupo, Numero,
    FmPrincipal.Fmy_key_uEncrypt);
  Edit5.Text := FloatToStr(Grupo) + ' - ' + FloatToStr(Numero);
end;

procedure TFmPrincipal.CadastroCores;
begin
  if DBCheck.CriaFm(TFmCores, FmCores, afmoNegarComAviso) then
  begin
    FmCores.ShowModal;
    FmCores.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroWUsers;
begin
  if DBCheck.CriaFm(TFmWUsers, FmWUsers, afmoNegarComAviso) then
  begin
    FmWUsers.ShowModal;
    FmWUsers.Destroy;
  end;
end;

(*
N�o usa
procedure TFmPrincipal.BMPtoJPG(BMPpic, JPGpic: string);
var
  Bitmap: TBitmap;
  JpegImg: TJpegImage;
begin
  Bitmap := TBitmap.Create;
  try
   Bitmap.LoadFromFile(BMPpic) ;
   JpegImg := TJpegImage.Create;
   try
    JpegImg.Assign(Bitmap) ;
    JpegImg.SaveToFile(JPGpic) ;
   finally
    JpegImg.Free
   end;
  finally
   Bitmap.Free
  end;
end;
*)

function TFmPrincipal.LoadJPEGPictureFile(Bitmap: TBitmap; FilePath, FileName: string): Boolean;
var
  JPEGImage: TJPEGImage;
begin
  if (FileName = '') then
    Result := False
  else
  begin
    try
      JPEGImage := TJPEGImage.Create;
      try
        JPEGImage.LoadFromFile(FilePath + FileName);
        Bitmap.Assign(JPEGImage);
      finally
        JPEGImage.Free;
      end;
    except
    end;
    Result := True;
  end;
end;

function TFmPrincipal.SaveJPEGPictureFile(Bitmap: TBitmap; FilePath, FileName: string;
  Quality: Integer): Boolean;
begin
  Result := True;
  try
    if ForceDirectories(FilePath) then
    begin
      with TJPegImage.Create do
      begin
        try
          Assign(Bitmap);
          CompressionQuality := Quality;
          SaveToFile(FilePath + FileName);
        finally
          Free;
        end;
      end;
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TFmPrincipal.SmoothResize(Src, Dst: TBitmap);
var
  x, y: Integer;
  xP, yP: Integer;
  xP2, yP2: Integer;
  SrcLine1, SrcLine2: pRGBArray;
  t3: Integer;
  z, z2, iz2: Integer;
  DstLine: pRGBArray;
  DstGap: Integer;
  w1, w2, w3, w4: Integer;
begin
  Src.PixelFormat := pf24Bit;
  Dst.PixelFormat := pf24Bit;

  if (Src.Width = Dst.Width) and (Src.Height = Dst.Height) then
    Dst.Assign(Src)
  else
  begin
    DstLine := Dst.ScanLine[0];
    DstGap := Integer(Dst.ScanLine[1]) - Integer(DstLine);

    xP2 := MulDiv(pred(Src.Width), $10000, Dst.Width);
    yP2 := MulDiv(pred(Src.Height), $10000, Dst.Height);
    yP := 0;

    for y := 0 to pred(Dst.Height) do begin
      xP := 0;

      SrcLine1 := Src.ScanLine[yP shr 16];

      if (yP shr 16 < pred(Src.Height)) then
        SrcLine2 := Src.ScanLine[succ(yP shr 16)]
      else
        SrcLine2 := Src.ScanLine[yP shr 16];

      z2 := succ(yP and $FFFF);
      iz2 := succ((not yp) and $FFFF);
      for x := 0 to pred(Dst.Width) do begin
        t3 := xP shr 16;
        z := xP and $FFFF;
        w2 := MulDiv(z, iz2, $10000);
        w1 := iz2 - w2;
        w4 := MulDiv(z, z2, $10000);
        w3 := z2 - w4;
        DstLine[x].rgbtRed := (SrcLine1[t3].rgbtRed * w1 +
          SrcLine1[t3 + 1].rgbtRed * w2 +
          SrcLine2[t3].rgbtRed * w3 + SrcLine2[t3 + 1].rgbtRed * w4) shr 16;
        DstLine[x].rgbtGreen :=
          (SrcLine1[t3].rgbtGreen * w1 + SrcLine1[t3 + 1].rgbtGreen * w2 +

          SrcLine2[t3].rgbtGreen * w3 + SrcLine2[t3 + 1].rgbtGreen * w4) shr 16;
        DstLine[x].rgbtBlue := (SrcLine1[t3].rgbtBlue * w1 +
          SrcLine1[t3 + 1].rgbtBlue * w2 +
          SrcLine2[t3].rgbtBlue * w3 +
          SrcLine2[t3 + 1].rgbtBlue * w4) shr 16;
        Inc(xP, xP2);
      end;
      Inc(yP, yP2);
      DstLine := pRGBArray(Integer(DstLine) + DstGap);
    end;
  end;
end;

procedure TFmPrincipal.Sobre1Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

function TFmPrincipal.VerificaImg(Caminho: String): Boolean;
begin
  Result := True;
  if Length(Caminho) > 0 then
  begin
    if not FileExists(Caminho) then
      Result := False;
  end;
end;

procedure TFmPrincipal.ResizeImage(FileName: string; MaxWidth: Integer);
var
  OldBitmap: TBitmap;
  NewBitmap: TBitmap;
  JPEGImage: TJPEGImage;
begin
  JPEGImage := TJPEGImage.Create;
  JPEGImage.LoadFromFile(FileName);
  OldBitmap := TBitmap.Create;
  try
    OldBitmap.Assign(JPEGImage);
    begin
      NewBitmap := TBitmap.Create;
      try
        NewBitmap.Width := MaxWidth;
        NewBitmap.Height := MulDiv(MaxWidth, OldBitmap.Height, OldBitmap.Width);
        SmoothResize(OldBitmap, NewBitmap);
        RenameFile(FileName, ChangeFileExt(FileName, '.$$$'));
        if SaveJPEGPictureFile(NewBitmap, ExtractFilePath(FileName),
          ExtractFileName(FileName), MaxWidth * 2) then
          DeleteFile(ChangeFileExt(FileName, '.$$$'))
        else
          RenameFile(ChangeFileExt(FileName, '.$$$'), FileName);
      finally
        NewBitmap.Free;
      end;
    end;
  finally
    OldBitmap.Free;
  end;
end;

end.
