object FmCiclosAula2: TFmCiclosAula2
  Left = 343
  Top = 217
  Caption = 'CIC-CICLO-002 :: Turma'
  ClientHeight = 608
  ClientWidth = 889
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 560
    Width = 889
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 777
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtImportar: TBitBtn
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Importar WEB'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImportarClick
    end
    object BtTodos: TBitBtn
      Tag = 127
      Left = 295
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Todos'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtTodosClick
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 387
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Nenhum'
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BtNenhumClick
    end
    object BtLanctos: TBitBtn
      Left = 480
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Lan'#231'amento'
      NumGlyphs = 2
      TabOrder = 5
      OnClick = BtLanctosClick
    end
    object BtVerificar: TBitBtn
      Left = 203
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Verificar'
      NumGlyphs = 2
      TabOrder = 6
      OnClick = BtVerificarClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 889
    Height = 48
    Align = alTop
    Caption = 'Turma'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 805
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 706
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 806
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 889
    Height = 512
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 889
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Ciclo:'
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 715
        Height = 21
        DataField = 'NOMEPRF'
        DataSource = FmCiclos2.DsCiclos
        TabOrder = 0
      end
      object dmkDBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Color = clInactiveCaption
        DataField = 'Codigo'
        DataSource = FmCiclos2.DsCiclos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 48
      Width = 889
      Height = 464
      Align = alClient
      TabOrder = 0
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 887
        Height = 247
        Align = alTop
        TabOrder = 0
        object Label19: TLabel
          Left = 7
          Top = 154
          Width = 168
          Height = 13
          Caption = 'Observa'#231#245'es: (m'#225'x 255 caracteres)'
        end
        object SpeedButton5: TSpeedButton
          Left = 746
          Top = 62
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object Label6: TLabel
          Left = 381
          Top = 46
          Width = 45
          Height = 13
          Caption = 'Promotor:'
        end
        object Label105: TLabel
          Left = 8
          Top = 46
          Width = 36
          Height = 13
          Caption = 'Cidade:'
        end
        object Label8: TLabel
          Left = 743
          Top = 2
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object Label110: TLabel
          Left = 495
          Top = 3
          Width = 25
          Height = 13
          Caption = 'Pa'#237's:'
        end
        object Label2: TLabel
          Left = 8
          Top = 6
          Width = 42
          Height = 13
          Caption = 'Controle:'
        end
        object Label35: TLabel
          Left = 67
          Top = 6
          Width = 33
          Height = 13
          Caption = 'Profes:'
        end
        object Label3: TLabel
          Left = 224
          Top = 6
          Width = 42
          Height = 13
          Caption = 'Inscritos:'
        end
        object Label4: TLabel
          Left = 283
          Top = 6
          Width = 53
          Height = 13
          Caption = 'Presen'#231'as:'
        end
        object Label20: TLabel
          Left = 344
          Top = 6
          Width = 56
          Height = 13
          Caption = 'Valor aluno:'
        end
        object Label21: TLabel
          Left = 418
          Top = 6
          Width = 50
          Height = 13
          Caption = 'Valor total:'
        end
        object Label5: TLabel
          Left = 110
          Top = 6
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label7: TLabel
          Left = 661
          Top = 227
          Width = 123
          Height = 13
          Caption = 'Pr = Presen'#231'a  Pg = Pago'
        end
        object Memo1: TdmkMemo
          Left = 7
          Top = 170
          Width = 777
          Height = 47
          TabOrder = 16
          QryCampo = 'Observa'
          UpdCampo = 'Observa'
          UpdType = utYes
        end
        object GroupBox4: TGroupBox
          Left = 8
          Top = 91
          Width = 386
          Height = 55
          Caption = ' Promotor: '
          TabOrder = 14
          object Label22: TLabel
            Left = 11
            Top = 14
            Width = 58
            Height = 13
            Caption = '% comiss'#227'o:'
          end
          object Label23: TLabel
            Left = 107
            Top = 14
            Width = 56
            Height = 13
            Caption = '$ comiss'#227'o:'
          end
          object Label24: TLabel
            Left = 201
            Top = 14
            Width = 30
            Height = 13
            Caption = '% 13'#186':'
          end
          object Label25: TLabel
            Left = 299
            Top = 14
            Width = 28
            Height = 13
            Caption = '$ 13'#186':'
          end
          object Label30: TLabel
            Left = 81
            Top = 14
            Width = 24
            Height = 13
            Caption = 'Min*:'
          end
          object Label31: TLabel
            Left = 271
            Top = 14
            Width = 24
            Height = 13
            Caption = 'Min*:'
          end
          object EdComPromPer: TdmkEdit
            Left = 7
            Top = 28
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'ComPromPer'
            UpdCampo = 'ComPromPer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdComPromPerEnter
            OnExit = EdComPromPerExit
          end
          object EdComPromVal: TdmkEdit
            Left = 107
            Top = 28
            Width = 74
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ComPromVal'
            UpdCampo = 'ComPromVal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdComPromValEnter
            OnExit = EdComPromValExit
          end
          object EdDTSPromPer: TdmkEdit
            Left = 193
            Top = 28
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'DTSPromPer'
            UpdCampo = 'DTSPromPer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdDTSPromPerEnter
            OnExit = EdDTSPromPerExit
          end
          object EdDTSPromVal: TdmkEdit
            Left = 297
            Top = 28
            Width = 74
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'DTSPromVal'
            UpdCampo = 'DTSPromVal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdDTSPromValEnter
            OnExit = EdDTSPromValExit
          end
          object EdComPromMin: TdmkEdit
            Left = 81
            Top = 28
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ComPromMin'
            UpdCampo = 'ComPromMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDTSPromMin: TdmkEdit
            Left = 271
            Top = 28
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DTSPromMin'
            UpdCampo = 'DTSPromMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox5: TGroupBox
          Left = 397
          Top = 91
          Width = 386
          Height = 55
          Caption = ' Professor: '
          TabOrder = 15
          object Label26: TLabel
            Left = 8
            Top = 14
            Width = 58
            Height = 13
            Caption = '% comiss'#227'o:'
          end
          object Label27: TLabel
            Left = 108
            Top = 14
            Width = 56
            Height = 13
            Caption = '$ comiss'#227'o:'
          end
          object Label28: TLabel
            Left = 198
            Top = 14
            Width = 30
            Height = 13
            Caption = '% 13'#186':'
          end
          object Label29: TLabel
            Left = 298
            Top = 14
            Width = 28
            Height = 13
            Caption = '$ 13'#186':'
          end
          object Label32: TLabel
            Left = 82
            Top = 14
            Width = 24
            Height = 13
            Caption = 'Min*:'
          end
          object Label33: TLabel
            Left = 272
            Top = 14
            Width = 24
            Height = 13
            Caption = 'Min*:'
          end
          object EdComProfPer: TdmkEdit
            Left = 8
            Top = 28
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'ComProfPer'
            UpdCampo = 'ComProfPer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdComProfPerEnter
            OnExit = EdComProfPerExit
          end
          object EdComProfVal: TdmkEdit
            Left = 108
            Top = 28
            Width = 74
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ComProfVal'
            UpdCampo = 'ComProfVal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdComProfValEnter
            OnExit = EdComProfValExit
          end
          object EdDTSProfPer: TdmkEdit
            Left = 198
            Top = 28
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'DTSProfPer'
            UpdCampo = 'DTSProfPer'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdDTSProfPerEnter
            OnExit = EdDTSProfPerExit
          end
          object EdDTSProfVal: TdmkEdit
            Left = 298
            Top = 28
            Width = 74
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'DTSProfVal'
            UpdCampo = 'DTSProfVal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnEnter = EdDTSProfValEnter
            OnExit = EdDTSProfValExit
          end
          object EdComProfMin: TdmkEdit
            Left = 82
            Top = 28
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ComProfMin'
            UpdCampo = 'ComProfMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdDTSProfMin: TdmkEdit
            Left = 272
            Top = 28
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'DTSProfMin'
            UpdCampo = 'DTSProfMin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object CBPromotor: TdmkDBLookupComboBox
          Left = 436
          Top = 62
          Width = 309
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEPROMO'
          ListSource = DmCiclos.DsPromotores
          TabOrder = 13
          dmkEditCB = EdPromotor
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPromotor: TdmkEditCB
          Left = 380
          Top = 62
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Promotor'
          UpdCampo = 'Promotor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPromotor
          IgnoraDBLookupComboBox = False
        end
        object CBCodMunici: TdmkDBLookupComboBox
          Left = 64
          Top = 62
          Width = 313
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DmCiclos.DsMunici
          TabOrder = 11
          dmkEditCB = EdCodMunici
          QryCampo = 'CodiCidade'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCodMunici: TdmkEditCB
          Left = 8
          Top = 62
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodiCidade'
          UpdCampo = 'CodiCidade'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCodMunici
          IgnoraDBLookupComboBox = False
        end
        object CBCodiPais: TdmkDBLookupComboBox
          Left = 550
          Top = 20
          Width = 191
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DmCiclos.DsBacen_Pais1
          TabOrder = 8
          dmkEditCB = EdCodiPais
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCodiPais: TdmkEditCB
          Left = 495
          Top = 20
          Width = 55
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCodiPais
          IgnoraDBLookupComboBox = False
        end
        object dmkEdit1: TdmkEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Color = clBackground
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaption
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdProfessor: TdmkEdit
          Left = 67
          Top = 20
          Width = 40
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBackground
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clInactiveCaption
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Professor'
          UpdCampo = 'Professor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAlunosAula: TdmkEdit
          Left = 284
          Top = 20
          Width = 58
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AlunosAula'
          UpdCampo = 'AlunosAula'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnEnter = EdAlunosAulaEnter
          OnExit = EdAlunosAulaExit
        end
        object EdInscritos: TdmkEdit
          Left = 223
          Top = 19
          Width = 58
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'AlunosInsc'
          UpdCampo = 'AlunosInsc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdValorUni: TdmkEdit
          Left = 344
          Top = 20
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorUni'
          UpdCampo = 'ValorUni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnEnter = EdValorUniEnter
          OnExit = EdValorUniExit
        end
        object EdValorTot: TdmkEdit
          Left = 418
          Top = 20
          Width = 74
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorTot'
          UpdCampo = 'ValorTot'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnEnter = EdValorTotEnter
          OnExit = EdValorTotExit
        end
        object CkContinuar: TCheckBox
          Left = 8
          Top = 223
          Width = 118
          Height = 17
          Caption = 'Continuar inserindo.'
          TabOrder = 17
        end
        object TPData: TdmkEditDateTimePicker
          Left = 110
          Top = 20
          Width = 112
          Height = 21
          Date = 39729.574278784720000000
          Time = 39729.574278784720000000
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'Data'
          UpdCampo = 'Data'
          UpdType = utYes
        end
        object EdUF: TdmkEdit
          Left = 743
          Top = 20
          Width = 24
          Height = 21
          CharCase = ecUpperCase
          MaxLength = 2
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'UF'
          UpdCampo = 'UF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdUFExit
        end
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 248
        Width = 887
        Height = 215
        Align = alClient
        DataSource = FmCiclos2.DsCiclosAlu
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGrid1CellClick
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'FCurso'
            Title.Caption = 'Pr'
            Width = 41
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pago'
            Title.Caption = 'Pg'
            Width = 15
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SEQ'
            Title.Caption = 'Sequencia'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'STATUS'
            Title.Caption = 'Status'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 250
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Telefone'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Celular'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            ReadOnly = True
            Title.Caption = 'ID'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entidade'
            Visible = True
          end>
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 24
    Top = 12
  end
  object PMLanctos: TPopupMenu
    OnPopup = PMLanctosPopup
    Left = 512
    Top = 572
    object Incluilanamento1: TMenuItem
      Caption = '&Inclui lan'#231'amento'
      OnClick = Incluilanamento1Click
    end
    object Alteralanamento1: TMenuItem
      Caption = '&Altera lan'#231'amento'
    end
    object Excluilanamento1: TMenuItem
      Caption = '&Exclui lan'#231'amento'
    end
  end
end
