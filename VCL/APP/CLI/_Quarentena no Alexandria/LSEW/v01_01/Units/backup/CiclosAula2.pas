unit CiclosAula2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Mask, DBCtrls, dmkEdit, dmkRadioGroup,
  dmkCheckBox, dmkDBEdit, dmkLabel, dmkMemo, ExtDlgs, ComCtrls, dmkGeral,
  dmkEditDateTimePicker, DB, mySQLDbTables, dmkPermissoes, dmkDBLookupComboBox,
  dmkEditCB, Grids, DBGrids, MyVCLSkin, Menus, Variants, UnDmkEnums, dmkDBGrid,
  dmkDBGridZTO;

type
  TValAluCalc = (vacAlunos, vacPreco, vacTotal,
    vacComPromPer, vacComPromVal, vacComProfPer, vacComProfVal,
    vacDTSPromPer, vacDTSPromVal, vacDTSProfPer, vacDTSProfVal);
  TFmCiclosAula2 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Memo1: TdmkMemo;
    Label19: TLabel;
    GroupBox4: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    EdComPromPer: TdmkEdit;
    EdComPromVal: TdmkEdit;
    EdDTSPromPer: TdmkEdit;
    EdDTSPromVal: TdmkEdit;
    EdComPromMin: TdmkEdit;
    EdDTSPromMin: TdmkEdit;
    GroupBox5: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    EdComProfPer: TdmkEdit;
    EdComProfVal: TdmkEdit;
    EdDTSProfPer: TdmkEdit;
    EdDTSProfVal: TdmkEdit;
    EdComProfMin: TdmkEdit;
    EdDTSProfMin: TdmkEdit;
    SpeedButton5: TSpeedButton;
    CBPromotor: TdmkDBLookupComboBox;
    EdPromotor: TdmkEditCB;
    Label6: TLabel;
    CBCodMunici: TdmkDBLookupComboBox;
    EdCodMunici: TdmkEditCB;
    Label105: TLabel;
    Label8: TLabel;
    CBCodiPais: TdmkDBLookupComboBox;
    EdCodiPais: TdmkEditCB;
    Label110: TLabel;
    dmkEdit1: TdmkEdit;
    Label2: TLabel;
    EdProfessor: TdmkEdit;
    Label35: TLabel;
    Label3: TLabel;
    EdAlunosAula: TdmkEdit;
    Label4: TLabel;
    EdInscritos: TdmkEdit;
    EdValorUni: TdmkEdit;
    Label20: TLabel;
    Label21: TLabel;
    EdValorTot: TdmkEdit;
    CkContinuar: TCheckBox;
    BtImportar: TBitBtn;
    Label5: TLabel;
    TPData: TdmkEditDateTimePicker;
    DBGrid1: TDBGrid;
    DBEdit1: TDBEdit;
    dmkDBEdCodigo: TdmkDBEdit;
    Label1: TLabel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    EdUF: TdmkEdit;
    BtLanctos: TBitBtn;
    PMLanctos: TPopupMenu;
    Incluilanamento1: TMenuItem;
    Alteralanamento1: TMenuItem;
    Excluilanamento1: TMenuItem;
    BtVerificar: TBitBtn;
    Label7: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdAlunosAulaEnter(Sender: TObject);
    procedure EdValorUniEnter(Sender: TObject);
    procedure EdValorTotEnter(Sender: TObject);
    procedure EdAlunosAulaExit(Sender: TObject);
    procedure EdValorUniExit(Sender: TObject);
    procedure EdValorTotExit(Sender: TObject);
    procedure EdComPromPerEnter(Sender: TObject);
    procedure EdComPromPerExit(Sender: TObject);
    procedure EdComPromValEnter(Sender: TObject);
    procedure EdComPromValExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDTSPromPerEnter(Sender: TObject);
    procedure EdDTSPromPerExit(Sender: TObject);
    procedure EdDTSPromValEnter(Sender: TObject);
    procedure EdDTSPromValExit(Sender: TObject);
    procedure EdComProfPerEnter(Sender: TObject);
    procedure EdComProfPerExit(Sender: TObject);
    procedure EdComProfValEnter(Sender: TObject);
    procedure EdComProfValExit(Sender: TObject);
    procedure EdDTSProfPerEnter(Sender: TObject);
    procedure EdDTSProfPerExit(Sender: TObject);
    procedure EdDTSProfValEnter(Sender: TObject);
    procedure EdDTSProfValExit(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdUFExit(Sender: TObject);
    procedure BtLanctosClick(Sender: TObject);
    procedure Incluilanamento1Click(Sender: TObject);
    procedure PMLanctosPopup(Sender: TObject);
    procedure BtVerificarClick(Sender: TObject);
    function VerificaCPFDuplic(CPF: string): Boolean;
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FValAluCalc: TValAluCalc;
    FOnCreate: Boolean;
    procedure CalculaValorAlunos();
    procedure UpdateAdiciona(FCurso, Conta: Integer; Todos: Boolean);
    procedure UpdatePago(Pago, Conta: Integer; Todos: Boolean);
    procedure AtualizaTotalDeAlunos;
    procedure MostraEdicao(SQLType: TSQLType);
  public
    { Public declarations }
    procedure CadastraEntidade(CPF, Nome, Tel, Cel, CodUF: String; CodMuni, CodPais, ID: Integer);
  end;

  var
  FmCiclosAula2: TFmCiclosAula2;

implementation

uses UnMyVCLref, UmySQlModule, UnFinanceiro, Module, UnInternalConsts,
 ModuleCiclos, Ciclos2, Principal, ModuleGeral, LctEdit, MyDBCheck, UnMyObjects;

{$R *.DFM}


procedure TFmCiclosAula2.BtSaidaClick(Sender: TObject);
begin
  FmCiclos2.FImportarWEB := False;
  FmCiclos2.FContinuar   := False;
  VAR_CADASTRO := dmkEdit1.ValueVariant;
  Close;
end;

procedure TFmCiclosAula2.BtTodosClick(Sender: TObject);
begin
  UpdateAdiciona(1, FmCiclos2.TbCiclosAluConta.Value, True);
  AtualizaTotalDeAlunos;
end;

procedure TFmCiclosAula2.BtVerificarClick(Sender: TObject);
begin
  FmCiclos2.ReopenTbCiclosAlu(True);
end;

procedure TFmCiclosAula2.EdValorUniEnter(Sender: TObject);
begin
  FValAluCalc := vacAlunos;
end;

procedure TFmCiclosAula2.EdValorUniExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdValorTotEnter(Sender: TObject);
begin
  FValAluCalc := vacAlunos;
end;

procedure TFmCiclosAula2.EdValorTotExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdAlunosAulaEnter(Sender: TObject);
begin
  FValAluCalc := vacAlunos;
end;

procedure TFmCiclosAula2.EdAlunosAulaExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdComProfPerEnter(Sender: TObject);
begin
  FValAluCalc := vacComProfPer;
end;

procedure TFmCiclosAula2.EdComProfPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdComProfValEnter(Sender: TObject);
begin
  FValAluCalc := vacComProfVal;
end;

procedure TFmCiclosAula2.EdComProfValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdComPromPerEnter(Sender: TObject);
begin
  FValAluCalc := vacComPromPer;
end;

procedure TFmCiclosAula2.EdComPromPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdComPromValEnter(Sender: TObject);
begin
  FValAluCalc := vacComPromVal;
end;

procedure TFmCiclosAula2.EdComPromValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdDTSProfPerEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSProfPer;
end;

procedure TFmCiclosAula2.EdDTSProfPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdDTSProfValEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSProfVal;
end;

procedure TFmCiclosAula2.EdDTSProfValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdDTSPromPerEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSPromPer;
end;

procedure TFmCiclosAula2.EdDTSPromPerExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.EdDTSPromValEnter(Sender: TObject);
begin
  FValAluCalc := vacDTSPromVal;
end;

procedure TFmCiclosAula2.EdDTSPromValExit(Sender: TObject);
begin
  CalculaValorAlunos();
end;

procedure TFmCiclosAula2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FOnCreate then
  begin
    FOncreate := False;
    MostraEdicao(LaTipo.SQLType);
  end;
end;

procedure TFmCiclosAula2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosAula2.Incluilanamento1Click(Sender: TObject);
var
  CtrlAlu: Integer;
begin
  CtrlAlu := FmCiclos2.TbCiclosAluConta.Value;
  if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfCicloCurso,
  afmoNegarComAviso, nil, (*FmPrincipal.QrCarteiras*) nil,
  tgrInclui, FmCiclos2.TbCiclosAluConta.Value, 0,
  0(*Genero*), 0(*Juros*), 0(*Multa*), nil, 703, CtrlAlu,
  -1(*FatNum*), 0, 0, 0, True, FmCiclos2.TbCiclosAluEntidade.Value (*Cliente*),
  0 (*Fornecedor*), Dmod.QrControleDono.Value(*cliInt*), 0 (*ForneceI*),
  0 (*Account*), 0 (*Vendedor*), True(*LockCliInt*), False(*LockForneceI*),
  True(*LockAccount*), False(*LockVendedor*), FmCiclos2.QrCiclosAulaData.Value,
  0, 0, 3, 0, FmCiclos2.FTabLctALS, 0, 0) > 0 then
  begin
  end;
end;

procedure TFmCiclosAula2.MostraEdicao(SQLType: TSQLType);
  procedure SetaPecentualEMinimo(EdPer, EdMin: TdmkEdit; Rol, Cta: Integer);
  begin
    DmCiclos.QrCtaSew.Close;
    DmCiclos.QrCtaSew.Params[00].AsInteger := Rol;
    DmCiclos.QrCtaSew.Params[01].AsInteger := Cta;
    DmCiclos.QrCtaSew.Open;
    if DmCiclos.QrCtaSew.RecordCount > 1 then
    Application.MessageBox(PChar('A conta ' + IntToStr(Cta) + ' possui ' +
    IntToStr(DmCiclos.QrCtaSew.RecordCount) + ' cadastros no rol de comiss�es '
    + IntToStr(Rol) + '. Apenas o primeiro ser� usado para setar o percentual '
    + 'e o m�nimo!'), 'Aviso', MB_OK+MB_ICONWARNING);
    EdPer.ValueVariant := DmCiclos.QrCtaSewPerceGene.Value;
    EdMin.ValueVariant := DmCiclos.QrCtaSewMinQtde.Value;
  end;
var
  Rol, Cta: Integer;
begin
  CkContinuar.Checked := FmCiclos2.FContinuar;
  if SQLType = stIns then
  begin
    Rol := FmCiclos2.QrCiclosRolComis.Value;
    //
    Cta := Dmod.QrControleCtaComProm.Value;
    SetaPecentualEMinimo(EdComPromPer, EdComPromMin, Rol, Cta);
    //
    Cta := Dmod.QrControleCta13oProm.Value;
    SetaPecentualEMinimo(EdDTSPromPer, EdDTSPromMin, Rol, Cta);
    //
    Cta := Dmod.QrControleCtaComProf.Value;
    SetaPecentualEMinimo(EdComProfPer, EdComProfMin, Rol, Cta);
    //
    Cta := Dmod.QrControleCta13oProf.Value;
    SetaPecentualEMinimo(EdDTSProfPer, EdDTSProfMin, Rol, Cta);
    //
    DmCiclos.QrCurso.Close;
    DmCiclos.QrCurso.Params[0].AsInteger := FmCiclos2.QrCiclosCurso.Value;
    DmCiclos.QrCurso.Open;
    EdValorUni.ValueVariant := DmCiclos.QrCursoValor.Value;
    //
    EdProfessor.ValueVariant := FmCiclos2.QrCiclosProfessor.Value;
    //
    BtImportar.Visible  := True;
    BtTodos.Visible     := False;
    BtNenhum.Visible    := False;
    BtLanctos.Visible   := False;
    BtVerificar.Visible := False;
    DBGrid1.Visible     := False;
  end else
  if SQLType = stUpd then
  begin
    dmkEdit1.ValueVariant     := FmCiclos2.QrCiclosAulaControle.Value;
    EdProfessor.ValueVariant  := FmCiclos2.QrCiclosAulaProfessor.Value;
    TPData.Date               := FmCiclos2.QrCiclosAulaData.Value;
    EdInscritos.ValueVariant  := FmCiclos2.QrCiclosAulaAlunosInsc.Value;
    EdAlunosAula.ValueVariant := FmCiclos2.QrCiclosAulaAlunosAula.Value;
    EdValorUni.ValueVariant   := FmCiclos2.QrCiclosAulaValorUni.Value;
    EdValorTot.ValueVariant   := FmCiclos2.QrCiclosAulaValorTot.Value;
    EdUF.ValueVariant         := FmCiclos2.QrCiclosAulaUF.Value;
    EdCodMunici.ValueVariant  := FmCiclos2.QrCiclosAulaCodiCidade.Value;
    CBCodMunici.KeyValue      := FmCiclos2.QrCiclosAulaCodiCidade.Value;
    EdPromotor.ValueVariant   := FmCiclos2.QrCiclosAulaPromotor.Value;
    CBPromotor.KeyValue       := FmCiclos2.QrCiclosAulaPromotor.Value;
    EdComPromPer.ValueVariant := FmCiclos2.QrCiclosAulaComPromPer.Value;
    EdComPromMin.ValueVariant := FmCiclos2.QrCiclosAulaComPromMin.Value;
    EdComPromVal.ValueVariant := FmCiclos2.QrCiclosAulaComPromVal.Value;
    EdDTSPromPer.ValueVariant := FmCiclos2.QrCiclosAulaDTSPromPer.Value;
    EdDTSPromMin.ValueVariant := FmCiclos2.QrCiclosAulaDTSPromMin.Value;
    EdDTSPromVal.ValueVariant := FmCiclos2.QrCiclosAulaDTSPromVal.Value;
    EdComProfPer.ValueVariant := FmCiclos2.QrCiclosAulaComProfPer.Value;
    EdComProfMin.ValueVariant := FmCiclos2.QrCiclosAulaComProfPer.Value;
    EdComProfVal.ValueVariant := FmCiclos2.QrCiclosAulaComProfVal.Value;
    EdDTSProfPer.ValueVariant := FmCiclos2.QrCiclosAulaDTSProfPer.Value;
    EdDTSProfMin.ValueVariant := FmCiclos2.QrCiclosAulaDTSProfMin.Value;
    EdDTSProfVal.ValueVariant := FmCiclos2.QrCiclosAulaDTSProfVal.Value;
    Memo1.Text                := FmCiclos2.QrCiclosAulaObserva.Value;
    //
    BtImportar.Visible  := False;
    BtTodos.Visible     := True;
    BtNenhum.Visible    := True;
    BtVerificar.Visible := True;
    BtLanctos.Visible   := True;
    DBGrid1.Visible     := True;
  end;
end;

procedure TFmCiclosAula2.PMLanctosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  //Enab := FmCiclos2.TbCiclosAluEntidade.Value > 0;
  Enab := (FmCiclos2.TbCiclosAlu.State <> dsInactive) or
    (FmCiclos2.TbCiclosAlu.RecordCount > 0);
  Incluilanamento1.Enabled := Enab;
  Alteralanamento1.Enabled := False;
  Excluilanamento1.Enabled := False;
end;

procedure TFmCiclosAula2.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroDeEntidades(EdPromotor.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPromotor, CBPromotor, DmCiclos.QrPromotores, VAR_CADASTRO);
    //
    EdPromotor.SetFocus;
  end;
end;

procedure TFmCiclosAula2.UpdateAdiciona(FCurso, Conta: Integer;
  Todos: Boolean);
begin
  Screen.Cursor := crHourGlass;
  //
  if not Todos then
  begin
    if FmCiclos2.TbCiclosAluFCurso.Value = 0 then
    begin
      UpdatePago(1, Conta, False);
      FCurso := 1
    end else
      FCurso := 0;
  end;
  //
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ciclosalu SET FCurso=:P0');
  Dmod.QrUpd.Params[0].AsInteger := FCurso;
  if not Todos then
    Dmod.QrUpd.SQL.Add('WHERE Conta=' + IntToStr(Conta));
  if Todos then
    Dmod.QrUpd.SQL.Add('WHERE Controle =' + IntToStr(FmCiclos2.TbCiclosAluControle.Value));
  Dmod.QrUpd.ExecSQL;
  //
  FmCiclos2.TbCiclosAlu.Close;
  FmCiclos2.TbCiclosAlu.Open;
  FmCiclos2.TbCiclosAlu.Locate('Conta', Conta, []);
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCiclosAula2.UpdatePago(Pago, Conta: Integer; Todos: Boolean);
begin
  Screen.Cursor := crHourGlass;
  //
  if not Todos then
  begin
    if FmCiclos2.TbCiclosAluPago.Value = 0 then
      Pago := 1
    else
      Pago := 0;
  end;
  //
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ciclosalu SET Pago=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Pago;
  if not Todos then
    Dmod.QrUpd.SQL.Add('WHERE Conta=' + IntToStr(Conta));
  if Todos then
    Dmod.QrUpd.SQL.Add('WHERE Controle =' + IntToStr(FmCiclos2.TbCiclosAluControle.Value));
  Dmod.QrUpd.ExecSQL;
  //
  FmCiclos2.TbCiclosAlu.Close;
  FmCiclos2.TbCiclosAlu.Open;
  FmCiclos2.TbCiclosAlu.Locate('Conta', Conta, []);
  //
  Screen.Cursor := crDefault;
end;

function TFmCiclosAula2.VerificaCPFDuplic(CPF: string): Boolean;
begin
  Result := False;
  //
  Dmod.QrUpdU.Close;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('SELECT CPF');
  Dmod.QrUpdU.SQL.Add('FROM entidades');
  Dmod.QrUpdU.SQL.Add('WHERE CPF="' + Geral.SoNumero_TT(CPF) + '"');
  Dmod.QrUpdU.Open;
  if Dmod.QrUpdU.FieldByName('CPF').AsString <> '' then
    Result := True;
  //
  Dmod.QrUpdU.Close;
end;

procedure TFmCiclosAula2.AtualizaTotalDeAlunos;
begin
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT COUNT(Controle) Total');
  Dmod.QrUpd.SQL.Add('FROM ciclosalu');
  Dmod.QrUpd.SQL.Add('WHERE FCurso = 1');
  Dmod.QrUpd.SQL.Add('AND Codigo=:P0');
  Dmod.QrUpd.SQL.Add('AND Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := FmCiclos2.QrCiclosCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := FmCiclos2.QrCiclosAulaControle.Value;
  Dmod.QrUpd.Open;
  if Dmod.QrUpd.RecordCount > 0 then
    EdAlunosAula.ValueVariant := Dmod.QrUpd.FieldByName('Total').Value
  else
    EdAlunosAula.ValueVariant := 0;
  Dmod.QrUpd.Close;
end;

procedure TFmCiclosAula2.BtImportarClick(Sender: TObject);
begin
  FmCiclos2.FData        := Geral.FDT(TPData.Date, 1);
  FmCiclos2.FCidade      := Trim(CBCodMunici.Text);
  FmCiclos2.FUF          := Trim(EdUF.ValueVariant);
  FmCiclos2.FImportarWEB := True;
  //
  Close;
end;

procedure TFmCiclosAula2.BtLanctosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLanctos, BtLanctos);
end;

procedure TFmCiclosAula2.BtNenhumClick(Sender: TObject);
begin
  UpdateAdiciona(0, FmCiclos2.TbCiclosAluConta.Value, True);
  AtualizaTotalDeAlunos;
end;

procedure TFmCiclosAula2.BtOKClick(Sender: TObject);
var
  Controle, Promotor, CodiCidade: Integer;
begin
  Promotor   := EdPromotor.ValueVariant;
  CodiCidade := EdCodMunici.ValueVariant;
  //
  if MyObjects.FIC(Promotor = 0, EdPromotor, 'Promotor n�o definido!') then Exit;
  if MyObjects.FIC(CodiCidade = 0, EdCodMunici, 'Cidade n�o definida!') then Exit;
  //
  if not Geral.ConfereUFeMunici_IBGE(EdUF.Text, EdCodMunici.ValueVariant, 'Turma') then Exit;
  //
  Controle :=
    UMyMod.BuscaEmLivreY_Def('ciclosaula', 'controle', LaTipo.SQLType,
      FmCiclos2.QrCiclosAulaControle.Value);
  if UMyMod.ExecSQLInsUpdFm(FmCiclosAula2, LaTipo.SQLType, 'ciclosaula', Controle,
  Dmod.QrUpd) then
  begin
    if FmCiclos2.TbCiclosAlu.State <> dsInactive then
    begin
      Screen.Cursor := crHourGlass;
      //
      FmCiclos2.TbCiclosAlu.First;
      while not FmCiclos2.TbCiclosAlu.Eof do
      begin
        if FmCiclos2.TbCiclosAluFCurso.Value = 1 then
          CadastraEntidade(FmCiclos2.TbCiclosAluCPF.Value, FmCiclos2.TbCiclosAluNome.Value,
            FmCiclos2.TbCiclosAluTelefone.Value, FmCiclos2.TbCiclosAluCelular.Value,
            EdUF.ValueVariant, EdCodMunici.ValueVariant, EdCodiPais.ValueVariant,
            FmCiclos2.TbCiclosAluConta.Value);
        FmCiclos2.TbCiclosAlu.Next;
      end;
      Screen.Cursor := crHourGlass;
    end;
    DmCiclos.CalculaCiclo(FmCiclos2.QrCiclosCodigo.Value,
      FmCiclos2.QrCiclosControle.Value, EdProfessor.ValueVariant,
      FmCiclos2.QrCiclosRolComis.Value, 'FmCiclos2', FmCiclos2.FTabLctALS);
    FmCiclos2.ReopenCiclosIts(Controle);
    //
    FmCiclos2.FContinuar := CkContinuar.Checked;
    VAR_CADASTRO := Controle;
    Close;
  end;
end;

procedure TFmCiclosAula2.CadastraEntidade(CPF, Nome, Tel, Cel, CodUF: String; CodMuni, CodPais, ID: Integer);
var
  Codigo, UF: Integer;
  Telefone, Doc, a, b: String;
begin
  Doc := Geral.SoNumero_TT(CPF);
  Doc := Trim(Doc);
  UF  := 0;
  //
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT Codigo');
  Dmod.QrUpd.SQL.Add('FROM ufs');
  Dmod.QrUpd.SQL.Add('WHERE Nome = "'+ CodUF +'"');
  Dmod.QrUpd.Open;
  if Dmod.QrUpd.RecordCount > 0 then
    UF := Dmod.QrUpd.FieldByName('Codigo').Value;
  Dmod.QrUpd.Close;
  //
  a := Geral.FormataCNPJ_TFT(Doc);
  b := Geral.CalculaCNPJCPF(Doc);
  //
  if (a = b) and (Length(Doc) > 0) then
  begin
    if not VerificaCPFDuplic(Doc) then
    begin
      Codigo   := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', stIns, 0);
      Telefone := MlaGeral.SoNumeroESinal_TT(Tel);
      if Length(Telefone) = 0 then
        Telefone := MLAGeral.SoNumeroESinal_TT(Cel);
      //
      if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'entidades', False,
      [
        'Nome', 'CPF', 'PTe1', 'Cliente1', 'Cliente2',
        'PCodMunici', 'PUF', 'PCodiPais', 'CodUsu'
      ], ['Codigo'], [
        Nome, Doc, Telefone, 'V', 'V',
        CodMuni, UF, CodPais, Codigo
      ], [Codigo]) then
      begin
        if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'ciclosalu', False,
        [
          'Entidade'
        ], ['Conta'], [
          Codigo
        ], [ID]) then
        begin
        end;
      end;
    end;
  end;
end;

procedure TFmCiclosAula2.CalculaValorAlunos();
var
  Alunos: Integer;
  Preco, Total,
  ComPromPer, ComPromVal, DTSPromPer, DTSPromVal,
  ComProfPer, ComProfVal, DTSProfPer, DTSProfVal: Double;
  //
  Calc1: Boolean;
begin
  Alunos     := EdAlunosAula.ValueVariant;
  Preco      := EdValorUni.ValueVariant;
  Total      := EdValorTot.ValueVariant;
  ComPromPer := EdComPromPer.ValueVariant;
  ComPromVal := EdComPromVal.ValueVariant;
  DTSPromPer := EdDTSPromPer.ValueVariant;
  DTSPromVal := EdDTSPromVal.ValueVariant;
  ComProfPer := EdComProfPer.ValueVariant;
  ComProfVal := EdComProfVal.ValueVariant;
  DTSProfPer := EdDTSProfPer.ValueVariant;
  DTSProfVal := EdDTSProfVal.ValueVariant;
  //
  Calc1 := True;
  case FValAluCalc of
    vacAlunos: Total := Alunos * Preco;
    vacPreco : Total := Alunos * Preco;
    vacTotal : if Total = 0 then Preco := 0 else Preco := Total / Alunos;
    else Calc1 := False;
  end;
  if Calc1 then
  begin
    EdAlunosAula.ValueVariant := Alunos;
    EdValorUni.ValueVariant   := Preco;
    EdValorTot.ValueVariant   := Total;
  end;
  //
  if FValAluCalc = vacComPromVal then
  begin
    if Total = 0 then ComPromPer := 0 else
      ComPromPer := ComPromVal / Total * 100;
  end else begin
    if Alunos < EdComPromMin.ValueVariant then ComPromVal := 0
    else ComPromVal := Total * ComPromPer / 100;
  end;
  EdComPromPer.ValueVariant := ComPromPer;
  EdComPromVal.ValueVariant := ComPromVal;
  //
  if FValAluCalc = vacDTSPromVal then
  begin
    if Total = 0 then DTSPromPer := 0 else
      DTSPromPer := DTSPromVal / Total * 100;
  end else begin
    if Alunos < EdDTSPromMin.ValueVariant then DTSPromVal := 0
    else DTSPromVal := Total * DTSPromPer / 100;
  end;
  EdDTSPromPer.ValueVariant := DTSPromPer;
  EdDTSPromVal.ValueVariant := DTSPromVal;
  //
  //
  if FValAluCalc = vacComProfVal then
  begin
    if Total = 0 then ComProfPer := 0 else
      ComProfPer := ComProfVal / Total * 100;
  end else begin
    if Alunos < EdComProfMin.ValueVariant then ComProfVal := 0
    else ComProfVal := Total * ComProfPer / 100;
  end;
  EdComProfPer.ValueVariant := ComProfPer;
  EdComProfVal.ValueVariant := ComProfVal;
  //
  if FValAluCalc = vacDTSProfVal then
  begin
    if Total = 0 then DTSProfPer := 0 else
      DTSProfPer := DTSProfVal / Total * 100;
  end else begin
    if Alunos < EdDTSProfMin.ValueVariant then DTSProfVal := 0
    else DTSProfVal := Total * DTSProfPer / 100;
  end;
  EdDTSProfPer.ValueVariant := DTSProfPer;
  EdDTSProfVal.ValueVariant := DTSProfVal;
  //
end;

procedure TFmCiclosAula2.DBGrid1CellClick(Column: TColumn);
begin
  if Column.FieldName = 'FCurso' then
  begin
    UpdateAdiciona(0, FmCiclos2.TbCiclosAluConta.Value, False);
    AtualizaTotalDeAlunos;
  end;
  if Column.FieldName = 'Pago' then
    UpdatePago(0, FmCiclos2.TbCiclosAluConta.Value, False);
end;

procedure TFmCiclosAula2.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Color: TColor;
begin
  if FmCiclos2.TbCiclosAluSTATUS.Value = 'Ok' then
    Color := clBlue
  else
    Color := clRed;
  //
  if (Column.FieldName = 'STATUS') then
  begin
    with DBGrid1.Canvas do
    begin
      Font.Color := Color;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
  if Column.FieldName = 'FCurso' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, FmCiclos2.TbCiclosAluFCurso.Value);
  if Column.FieldName = 'Pago' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, FmCiclos2.TbCiclosAluPago.Value);
end;

procedure TFmCiclosAula2.EdUFExit(Sender: TObject);
var
  UF: String;
begin
  UF := EdUF.ValueVariant;
  DmCiclos.ReopenMunici(UF);
end;

procedure TFmCiclosAula2.FormCreate(Sender: TObject);
begin
  TPData.Date            := Date;
  FOnCreate              := True;
  //
  UMyMod.AbreQuery(DmCiclos.QrBacen_Pais, DModG.AllID_DB);
  DmCiclos.ReopenMunici('');
  //
  EdCodiPais.ValueVariant := 1058;
  CBCodiPais.KeyValue     := 1058;
end;

end.
