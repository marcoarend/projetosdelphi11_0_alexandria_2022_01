unit GraGruFotos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, MyDBCheck, DBCtrls, UnGOTOy,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu,
  dmkLabel, Mask, dmkGeral, UnInternalConsts, Variants, tscap32_rt, Menus,
  ComCtrls, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, JPEG, IdExplicitTLSClientServerBase, IdFTP, dmkDBEdit, DBCGrids,
  Grids, DBGrids, dmkDBGrid, ExtDlgs, UnDmkProcFunc;

type
  TFmGraGruFotos = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnNiveis: TPanel;
    tsCap32Dialogs1: TtsCap32Dialogs;
    tsCap32PopupMenu1: TtsCap32PopupMenu;
    IdHTTP1: TIdHTTP;
    FTP: TIdFTP;
    QrLoc: TmySQLQuery;
    BtCaptura: TBitBtn;
    BtUpload: TBitBtn;
    LaProd: TLabel;
    PBProgress: TProgressBar;
    QrGradeK: TmySQLQuery;
    QrGradeKCodigo: TIntegerField;
    QrGradeKNome: TWideStringField;
    DsGradeK: TDataSource;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    QrGradesCodUsu: TIntegerField;
    DsGrades: TDataSource;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsAlterWeb: TSmallintField;
    QrGradesCorsAtivo: TSmallintField;
    DsGradesCors: TDataSource;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    DsGradesTams: TDataSource;
    PnEdita: TPanel;
    Image2: TImage;
    tsCap321: TtsCap32;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    Label19: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    EdProd: TdmkEditCB;
    CBProd: TdmkDBLookupComboBox;
    CBCor: TdmkDBLookupComboBox;
    EdCor: TdmkEditCB;
    CBTam: TdmkDBLookupComboBox;
    EdTam: TdmkEditCB;
    TabSheet4: TTabSheet;
    Label34: TLabel;
    EdKit: TdmkEditCB;
    CBKit: TdmkDBLookupComboBox;
    BtAbrir: TBitBtn;
    OpenPictureDialog1: TOpenPictureDialog;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtUploadClick(Sender: TObject);
    procedure FTPWork(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Integer);
    procedure FTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Integer);
    procedure FTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure QrGradesAfterScroll(DataSet: TDataSet);
    procedure QrGradesBeforeClose(DataSet: TDataSet);
    procedure BtCapturaClick(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
  private
    { Private declarations }
    FUpload: Boolean;
    FCodProd, FFotoAtu: Integer;
    procedure ReopenGradesCors(Codigo: Integer);
    procedure ReopenGradesTams(Codigo: Integer);
    procedure CapturaWebCam;
  public
    FJanela, FArquivo: String;
    FProduto: Integer;
    FCapturou: Boolean;
  end;

  var
  FmGraGruFotos: TFmGraGruFotos;

implementation

uses Module, UMySQLModule, UnMySQLCuringa, GraGru, Cores, Principal,
  UnMyObjects;

{$R *.DFM}

procedure TFmGraGruFotos.BtAbrirClick(Sender: TObject);
var
  Tam: Integer;
  Dir, Nome, Cam: String;
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
      FJanela := 'FmProdutos';
      if (EdProd.ValueVariant = 0) or (EdCor.ValueVariant = 0) or
        (EdTam.ValueVariant = 0) then
      begin
        Application.MessageBox('Produto n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
        CBProd.SetFocus;
        Exit;
      end;
    end;
    1:
    begin
      FJanela := 'FmGradeK';
      if EdKit.ValueVariant = 0 then
      begin
        Application.MessageBox('Kit n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
        CBKit.SetFocus;
        Exit;
      end;
    end;
  end;
  if OpenPictureDialog1.Execute then
  begin
    FArquivo := OpenPictureDialog1.FileName;
    FFotoAtu := 0;
    FCodProd := UMyMod.BuscaEmLivreY_Def('Fotos', 'Codigo', stIns, 0);
    //
    Tam  := Dmod.QrControlePrdFotoTam.Value;
    Dir  := Dmod.QrControlePrdFoto.Value + '\';
    Nome := 'prd_' + IntToStr(FCodProd);
    Cam  := Dir + Nome;
    //
    if ForceDirectories(Dir) then
    begin
      if DeleteFile(Cam + '.jpg') then
      begin
      end;
      //
      if CopyFile(PChar(FArquivo), PChar(Cam + '.jpg'), True) then
      begin
        FmPrincipal.ResizeImage(Cam + '.jpg', Tam);
        if FmPrincipal.VerificaImg(Cam + '.jpg') then
          DeleteFile(Cam + '.bmp');
        Image2.Picture.LoadFromFile(Cam + '.jpg');
        BtUpload.Visible := True;
      end;
    end;
  end;
end;

procedure TFmGraGruFotos.BtCapturaClick(Sender: TObject);
begin
  FArquivo := '';
  CapturaWebCam;
end;

procedure TFmGraGruFotos.BtSaidaClick(Sender: TObject);
begin
  tscap321.Connected := Not tscap321.Connected;
  Close;
end;

procedure TFmGraGruFotos.BtUploadClick(Sender: TObject);
var
  Tabela, Campo, Cam, Foto, PrdFoto, DirUpl, ArqUpl: String;
  Controle, Web, Cor, Tam: Integer;
begin
  Controle := 0;
  Web      := Dmod.QrControlePrdFotoWeb.Value;
  if FFotoAtu > 0 then
    Campo := 'Foto' + IntToStr(FFotoAtu)
  else
    Campo := 'Foto';
  //
  if FJanela = 'FmCartAval' then
  begin
    Controle  := FProduto;
    Cam       := Dmod.QrControleAvaDir.Value + '\ava_' + IntToStr(Controle) + '_' + IntToStr(FFotoAtu) + '.jpg';
    Tabela    := 'cartaval';
    FCapturou := False;
  end;
  if FJanela = 'FmGradeK' then
  begin
    Controle := FCodProd;
    Cam      := Dmod.QrControlePrdFoto.Value + '\prd_' + IntToStr(FCodProd) + '.jpg';
    Tabela   := 'gradek';
  end;
  if FJanela = 'FmProdutos' then
  begin
    Controle := FCodProd;
    Cam      := Dmod.QrControlePrdFoto.Value + '\prd_' + IntToStr(FCodProd) + '.jpg';
    Tabela   := 'produtos';
  end;
  if not FmPrincipal.VerificaImg(Cam) then
  begin
    Application.MessageBox(PChar('Foto n�o localizada!' + #13#10 +
      'Capture uma foto e tente novamente.'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  // Faz o upload
  if Web = 1 then
  begin
    FUpload := False;
    if FTP.Connected then
      FTP.Disconnect;
    FTP.Host     := Dmod.QrControleWeb_FTPh.Value;
    FTP.Username := Dmod.QrControleWeb_FTPu.Value;
    FTP.Password := Dmod.QrControleWeb_FTPs.Value;
    FTP.Connect;
    FTP.ChangeDir(Dmod.QrControleWeb_Raiz.Value + '/');

    pbProgress.Position := 0;
    pbProgress.Visible := true;

    if FJanela = 'FmCartAval' then
    begin
      DirUpl := Dmod.QrControleAvaDir.Value + '/ava_' + IntToStr(Controle) + '_' + IntToStr(FFotoAtu) + '.jpg';
      ArqUpl := 'ava_' + IntToStr(Controle) + '_' + IntToStr(FFotoAtu) + '.jpg';
    end;
    if FJanela = 'FmGradeK' then
    begin
      DirUpl := Dmod.QrControlePrdFoto.Value + '/prd_' + IntToStr(Controle) + '.jpg';
      if Length(FArquivo) = 0 then
        ArqUpl := 'prd_' + IntToStr(FCodProd) + '.jpg'
      else
        ArqUpl := FArquivo;
    end;
    if FJanela = 'FmProdutos' then
    begin
      DirUpl := Dmod.QrControlePrdFoto.Value + '/prd_' + IntToStr(Controle) + '.jpg';
      if Length(FArquivo) = 0 then
        ArqUpl := 'prd_' + IntToStr(FCodProd) + '.jpg'
      else
        ArqUpl := FArquivo;
    end;
    //
    FTP.Put(DirUpl, ArqUpl);
    FTP.Disconnect;
    //
    FArquivo := '';
    //
    if FUpload then
    begin
      if FmPrincipal.VerificaImg(Cam) then
        DeleteFile(Cam);
      if FJanela = 'FmCartAval' then
        Foto := Dmod.QrControleWeb_MyURL.Value + '/ava_' + IntToStr(Controle) + '_' + IntToStr(FFotoAtu) + '.jpg';
      if FJanela = 'FmGradeK' then
        Foto := Dmod.QrControleWeb_MyURL.Value + '/prd_' + IntToStr(FCodProd) + '.jpg';
      if FJanela = 'FmProdutos' then
        Foto := Dmod.QrControleWeb_MyURL.Value + '/prd_' + IntToStr(FCodProd) + '.jpg';
    end;
  end else
  begin
    if FJanela = 'FmCartAval' then
      Foto := Dmod.QrControleAvaDir.Value + '\ava_' + IntToStr(Controle) + '_' + IntToStr(FFotoAtu) + '.jpg';
    if FJanela = 'FmGradeK' then
      Foto := Dmod.QrControlePrdFoto.Value + '\prd_' + IntToStr(FCodProd) + '.jpg';
    if FJanela = 'FmProdutos' then
      Foto := Dmod.QrControlePrdFoto.Value + '\prd_' + IntToStr(FCodProd) + '.jpg';
  end;
  if FJanela = 'FmCartAval' then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + Tabela +' SET '+ Campo +'=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[0].AsString  := Foto;
    Dmod.QrUpd.Params[1].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
  end else
  begin
    if Dmod.QrControlePrdFotoWeb.Value = 0 then
      PrdFoto := dmkPF.DuplicaBarras(Foto)
    else
      PrdFoto := Foto;
    if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'fotos', False,
    [
      'Nome'
    ], ['Codigo'],
    [
      PrdFoto
    ], [FCodProd]) then
    begin
      if FJanela = 'FmGradeK' then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE ' + Tabela +' SET '+ Campo +'=:P0 WHERE Codigo=:P1');
        Dmod.QrUpd.Params[0].AsInteger := FCodProd;
        Dmod.QrUpd.Params[1].AsInteger := EdKit.ValueVariant;
        Dmod.QrUpd.ExecSQL;
      end;
      if FJanela = 'FmProdutos' then
      begin
        Cor := EdCor.ValueVariant;
        Tam := EdTam.ValueVariant;
        //
        if (Cor > 0) and (Tam = 0) then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE produtos SET Foto=:P0');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Cor=:P2');
          Dmod.QrUpd.Params[0].AsInteger := FCodProd;
          Dmod.QrUpd.Params[1].AsInteger := QrGradesCodigo.Value;
          Dmod.QrUpd.Params[2].AsInteger := Cor;
          Dmod.QrUpd.ExecSQL;
        end
        else if (Cor = 0) and (Tam > 0) then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE produtos SET Foto=:P0');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Tam=:P2');
          Dmod.QrUpd.Params[0].AsInteger := FCodProd;
          Dmod.QrUpd.Params[1].AsInteger := QrGradesCodigo.Value;
          Dmod.QrUpd.Params[2].AsInteger := Tam;
          Dmod.QrUpd.ExecSQL;
        end
        else if (Cor = 0) and (Tam = 0) then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE produtos SET Foto=:P0');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
          Dmod.QrUpd.Params[0].AsInteger := FCodProd;
          Dmod.QrUpd.Params[1].AsInteger := QrGradesCodigo.Value;
          Dmod.QrUpd.ExecSQL;
        end
        else if (Cor > 0) and (Tam > 0) then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE produtos SET Foto=:P0');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND Cor=:P2 AND Tam=:P3');
          Dmod.QrUpd.Params[0].AsInteger := FCodProd;
          Dmod.QrUpd.Params[1].AsInteger := QrGradesCodigo.Value;
          Dmod.QrUpd.Params[2].AsInteger := Cor;
          Dmod.QrUpd.Params[3].AsInteger := Tam;
          Dmod.QrUpd.ExecSQL;
        end;
      end;
    end;
  end;
  //
  if FFotoAtu = 4 then
    Close
  else
  begin
    Application.MessageBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    if FJanela <> 'FmCartAval' then
    begin
      PageControl1.Enabled := True;
      case PageControl1.ActivePageIndex of
        0: CBProd.SetFocus;
        1: CBKit.SetFocus;
      end;
    end;
  end;
  BtUpload.Visible := False;
end;

procedure TFmGraGruFotos.CapturaWebCam;
var
  Cam, Dir, Nome: String;
  Tam: Integer;
begin
  Tam := 0;
  if FJanela = 'FmCartAval' then
  begin
    if not FCapturou then
    begin
      FFotoAtu  := FFotoAtu + 1;
      FCapturou := True;
    end;
    Tam       := Dmod.QrControleAvaFoto.Value;
    Dir       := Dmod.QrControleAvaDir.Value + '\';
    Nome      := 'ava_' + IntToStr(FProduto) + '_' + IntToStr(FFotoAtu);
    Cam       := Dir + Nome;
  end else
  begin
    case PageControl1.ActivePageIndex of
      0:
      begin
        FJanela := 'FmProdutos';
        if (EdProd.ValueVariant = 0) or (EdCor.ValueVariant = 0) or
          (EdTam.ValueVariant = 0) then
        begin
          Application.MessageBox('Produto n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
          CBProd.SetFocus;
          Exit;
        end;
      end;
      1:
      begin
        FJanela := 'FmGradeK';
        if EdKit.ValueVariant = 0 then
        begin
          Application.MessageBox('Kit n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
          CBKit.SetFocus;
          Exit;
        end;
      end;
    end;
  end;
  PageControl1.Enabled := False;
  if FJanela = 'FmGradeK' then
  begin
    FFotoAtu := 0;
    FCodProd := UMyMod.BuscaEmLivreY_Def('Fotos', 'Codigo', stIns, 0);
    Tam      := Dmod.QrControlePrdFotoTam.Value;
    Dir      := Dmod.QrControlePrdFoto.Value + '\';
    Nome     := 'prd_' + IntToStr(FCodProd);
    Cam      := Dir + Nome;
  end;
  if FJanela = 'FmProdutos' then
  begin
    FFotoAtu := 0;
    FCodProd := UMyMod.BuscaEmLivreY_Def('Fotos', 'Codigo', stIns, 0);
    Tam      := Dmod.QrControlePrdFotoTam.Value;
    Dir      := Dmod.QrControlePrdFoto.Value + '\';
    Nome     := 'prd_' + IntToStr(FCodProd);
    Cam      := Dir + Nome;
  end;
  tsCap321.SaveAsBMP := Cam + '.bmp';
  FmPrincipal.BMPtoJPG(Cam + '.bmp', Cam + '.jpg');
  FmPrincipal.ResizeImage(Cam + '.jpg', Tam);
  if FmPrincipal.VerificaImg(Cam + '.jpg') then
    DeleteFile(Cam + '.bmp');
  Image2.Picture.LoadFromFile(Cam + '.jpg');
  BtUpload.Visible := True;
end;

procedure TFmGraGruFotos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  LaProd.Caption := IntToStr(FProduto);
  //
  if FJanela = 'FmCartAval' then
  begin
    tsCap321.Top         := 0;
    Image2.Top           := 0;
    PageControl1.Visible := False;
    BtAbrir.Visible      := False;
    BtAbrir.Left         := 189;
    BtUpload.Left        := 97;
  end else
  begin
    tsCap321.Top         := 95;
    Image2.Top           := 95;
    PageControl1.Visible := True;
    BtAbrir.Visible      := True;
    BtAbrir.Left         := 97;
    BtUpload.Left        := 189;
  end;
  BtUpload.Visible := False;
end;

procedure TFmGraGruFotos.FormCreate(Sender: TObject);
var
  Proxy: Integer;
begin
  FArquivo := '';
  QrGrades.Open;
  QrGradeK.Open;
  PageControl1.ActivePageIndex := 0;
  FJanela                      := 'FmProdutos';
  PBProgress.Visible           := False;
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  FFotoAtu := 0;
  tsCap321.Connected           := Not tsCap321.Connected;
  if tsCap321.Connected then
  begin
    if ((Length(Dmod.QrControlePrdFoto.Value) = 0 ) or
      (Dmod.QrControlePrdFotoTam.Value = 0)) then
    begin
      Application.MessageBox(PChar('Configura��es n�o definidas!' + #13#10 +
        'V� nas op��es espec�ficas do aplicativo na aba fotos.'), 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end else
    Close;
end;

procedure TFmGraGruFotos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGraGruFotos.FTPWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Integer);
begin
  Application.ProcessMessages;
  pbProgress.Position := AWorkCount;
end;

procedure TFmGraGruFotos.FTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Integer);
begin
  Screen.Cursor := crHourGlass;
  pbProgress.Max := AWorkCountMax;
  BtSaida.Enabled   := False;
end;

procedure TFmGraGruFotos.FTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
var
  Controle: Integer;
  Cam: String;
begin
  Controle := FProduto;
  Cam      := Dmod.QrControlePrdFoto.Value + '\prd_' + IntToStr(Controle) + '.jpg';
  //
  pbProgress.Visible := false;
  Screen.Cursor      := crDefault;
  BtSaida.Enabled    := True;
  FUpload            := True;
end;

procedure TFmGraGruFotos.QrGradesAfterScroll(DataSet: TDataSet);
begin
  ReopenGradesCors(QrGradesCodigo.Value);
  ReopenGradesTams(QrGradesCodigo.Value);
end;

procedure TFmGraGruFotos.QrGradesBeforeClose(DataSet: TDataSet);
begin
  QrGradesTams.Close;
  QrGradesCors.Close;
end;

procedure TFmGraGruFotos.ReopenGradesCors(Codigo: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Codigo;
  QrGradesCors.Open;
end;

procedure TFmGraGruFotos.ReopenGradesTams(Codigo: Integer);
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Codigo;
  QrGradesTams.Open;
end;

end.


