object FmEntiRapido: TFmEntiRapido
  Left = 371
  Top = 198
  Caption = 'CIC-ALUNO-001 :: Cadastro de Alunos e Clientes'
  ClientHeight = 610
  ClientWidth = 850
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 213
    Width = 850
    Height = 338
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 1
    object Label31: TLabel
      Left = 290
      Top = 5
      Width = 109
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome logradouro:'
      FocusControl = EdRua
    end
    object Label32: TLabel
      Left = 610
      Top = 5
      Width = 51
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'N'#250'mero:'
      FocusControl = EdNumero
    end
    object Label33: TLabel
      Left = 694
      Top = 5
      Width = 87
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Complemento:'
      FocusControl = EdCompl
    end
    object Label34: TLabel
      Left = 5
      Top = 54
      Width = 39
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Bairro:'
      FocusControl = EdBairro
    end
    object Label39: TLabel
      Left = 5
      Top = 5
      Width = 30
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'CEP:'
      FocusControl = EdCEP
    end
    object Label97: TLabel
      Left = 133
      Top = 5
      Width = 100
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Tipo logradouro:'
    end
    object Label1: TLabel
      Left = 271
      Top = 54
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cidade:'
      FocusControl = EdCidade
    end
    object Label7: TLabel
      Left = 610
      Top = 54
      Width = 21
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'UF:'
    end
    object Label3: TLabel
      Left = 5
      Top = 103
      Width = 190
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'N'#250'mero serial (senha de aluno):'
      FocusControl = EdSerial
    end
    object Label4: TLabel
      Left = 679
      Top = 103
      Width = 120
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valida'#231#227'o do serial:'
    end
    object Label8: TLabel
      Left = 684
      Top = 54
      Width = 30
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pa'#237's:'
      FocusControl = EdPais
    end
    object BtCEP: TButton
      Left = 101
      Top = 25
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      TabOrder = 1
      TabStop = False
      OnClick = BtCEPClick
    end
    object EdRua: TEdit
      Left = 290
      Top = 25
      Width = 317
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 3
    end
    object EdNumero: TEdit
      Left = 612
      Top = 25
      Width = 78
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 4
    end
    object EdCompl: TEdit
      Left = 694
      Top = 25
      Width = 149
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 5
    end
    object EdBairro: TEdit
      Left = 5
      Top = 74
      Width = 262
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 6
    end
    object EdCEP: TEdit
      Left = 5
      Top = 25
      Width = 95
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
      OnExit = EdCEPExit
    end
    object CBLograd: TDBLookupComboBox
      Left = 133
      Top = 25
      Width = 154
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsListaLograd
      TabOrder = 2
    end
    object EdCidade: TEdit
      Left = 271
      Top = 74
      Width = 336
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 7
    end
    object CBUF: TDBLookupComboBox
      Left = 610
      Top = 74
      Width = 71
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsUFs
      TabOrder = 8
    end
    object EdSerial: TEdit
      Left = 5
      Top = 123
      Width = 671
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 10
      OnExit = EdSerialExit
    end
    object EdSerialG: TdmkEdit
      Left = 679
      Top = 123
      Width = 71
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSerialN: TdmkEdit
      Left = 748
      Top = 123
      Width = 94
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 12
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdPais: TEdit
      Left = 684
      Top = 74
      Width = 159
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 9
    end
    object GroupBox1: TGroupBox
      Left = 5
      Top = 153
      Width = 838
      Height = 168
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Ajuda '#224' senha de 32 caracteres: '
      TabOrder = 13
      object GroupBox2: TGroupBox
        Left = 2
        Top = 18
        Width = 196
        Height = 120
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Contra-senha: '
        TabOrder = 0
        object Label10: TLabel
          Left = 84
          Top = 44
          Width = 7
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '_'
        end
        object Label14: TLabel
          Left = 10
          Top = 25
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lote:'
        end
        object Label15: TLabel
          Left = 98
          Top = 25
          Width = 68
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Sequ'#234'ncia:'
        end
        object EdMD5Cab: TdmkEdit
          Left = 10
          Top = 44
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdMD5CabExit
        end
        object EdSequencia: TdmkEdit
          Left = 98
          Top = 44
          Width = 84
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdMD5CabExit
        end
      end
      object GBSenha: TGroupBox
        Left = 198
        Top = 18
        Width = 638
        Height = 120
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Senha do aluno: '
        Enabled = False
        TabOrder = 1
        object Label9: TLabel
          Left = 15
          Top = 25
          Width = 26
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '1'#186'/4:'
        end
        object Label11: TLabel
          Left = 138
          Top = 25
          Width = 26
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '2'#186'/4:'
        end
        object Label12: TLabel
          Left = 261
          Top = 25
          Width = 26
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '3'#186'/4:'
        end
        object Label13: TLabel
          Left = 384
          Top = 25
          Width = 26
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '4'#186'/4:'
        end
        object La_4_1: TLabel
          Left = 15
          Top = 74
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = 'Faltam 8 caracteres'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object La_4_2: TLabel
          Left = 138
          Top = 74
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = 'Faltam 8 caracteres'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object La_4_3: TLabel
          Left = 261
          Top = 74
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = 'Faltam 8 caracteres'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object La_4_4: TLabel
          Left = 384
          Top = 74
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = 'Faltam 8 caracteres'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object LaQuarto1: TLabel
          Left = 15
          Top = 94
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = '?'
        end
        object LaQuarto2: TLabel
          Left = 138
          Top = 94
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = '?'
        end
        object LaQuarto3: TLabel
          Left = 261
          Top = 94
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = '?'
        end
        object LaQuarto4: TLabel
          Left = 384
          Top = 94
          Width = 123
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taCenter
          AutoSize = False
          Caption = '?'
        end
        object Ed_4_2: TdmkEdit
          Left = 138
          Top = 44
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Ed_4_1Change
        end
        object Ed_4_1: TdmkEdit
          Left = 15
          Top = 44
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Ed_4_1Change
        end
        object Ed_4_4: TdmkEdit
          Left = 384
          Top = 44
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Ed_4_1Change
        end
        object Ed_4_3: TdmkEdit
          Left = 261
          Top = 44
          Width = 123
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = Ed_4_1Change
        end
      end
      object PnAviso: TPanel
        Left = 2
        Top = 138
        Width = 834
        Height = 28
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Informe a contra senha!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 551
    Width = 850
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 17
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel2: TPanel
      Left = 703
      Top = 1
      Width = 146
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 17
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 850
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Cadastro de Alunos e Clientes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 747
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 748
      Top = 1
      Width = 101
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stNil
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 49
    Width = 850
    Height = 164
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 0
    object Label25: TLabel
      Left = 103
      Top = 5
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome:'
      FocusControl = EdNome
    end
    object Label29: TLabel
      Left = 5
      Top = 108
      Width = 74
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'CNPJ / CPF:'
      FocusControl = EdCNPJCPF
    end
    object Label30: TLabel
      Left = 103
      Top = 54
      Width = 51
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'I.E. / RG:'
      FocusControl = EdIERG
    end
    object Label2: TLabel
      Left = 433
      Top = 54
      Width = 37
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Email:'
      FocusControl = EdEmeio
    end
    object Label43: TLabel
      Left = 148
      Top = 108
      Width = 132
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Telefone Residencial:'
    end
    object Label102: TLabel
      Left = 231
      Top = 54
      Width = 52
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Emissor:'
      FocusControl = EdSSP
    end
    object Label118: TLabel
      Left = 300
      Top = 54
      Width = 56
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Emiss'#227'o:'
    end
    object Label6: TLabel
      Left = 561
      Top = 108
      Width = 45
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Celular:'
      FocusControl = EdCel
    end
    object Label44: TLabel
      Left = 286
      Top = 108
      Width = 121
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Telefone Comercial:'
    end
    object Label105: TLabel
      Left = 423
      Top = 108
      Width = 106
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Telefone Contato:'
    end
    object Label5: TLabel
      Left = 704
      Top = 108
      Width = 25
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Fax:'
      FocusControl = EdFax
    end
    object EdNome: TEdit
      Left = 103
      Top = 25
      Width = 627
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      CharCase = ecUpperCase
      TabOrder = 1
    end
    object EdCNPJCPF: TEdit
      Left = 5
      Top = 128
      Width = 139
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 7
      OnExit = EdCNPJCPFExit
    end
    object EdIERG: TEdit
      Left = 103
      Top = 74
      Width = 123
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 3
    end
    object RGTipo: TRadioGroup
      Left = 5
      Top = 5
      Width = 95
      Height = 95
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Cadastro: '
      ItemIndex = 0
      Items.Strings = (
        '?'
        'Empresa'
        'Pessoal')
      TabOrder = 0
    end
    object EdEmeio: TEdit
      Left = 433
      Top = 74
      Width = 297
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 6
    end
    object EdTe1: TEdit
      Left = 148
      Top = 128
      Width = 134
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 8
      OnExit = EdTe1Exit
    end
    object EdTe2: TEdit
      Left = 286
      Top = 128
      Width = 134
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 9
      OnExit = EdTe2Exit
    end
    object RGTipoEnti: TRadioGroup
      Left = 738
      Top = 0
      Width = 105
      Height = 105
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Entidade: '
      ItemIndex = 0
      Items.Strings = (
        '?'
        'Aluno'
        'Cliente'
        'Ambos')
      TabOrder = 2
    end
    object EdSSP: TEdit
      Left = 231
      Top = 74
      Width = 66
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 10
      TabOrder = 4
    end
    object TPDataRG: TdmkEditDateTimePicker
      Left = 300
      Top = 74
      Width = 130
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 1.992729421297550000
      Time = 1.992729421297550000
      TabOrder = 5
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdTe3: TEdit
      Left = 423
      Top = 128
      Width = 135
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 10
      OnExit = EdTe3Exit
    end
    object EdCel: TEdit
      Left = 561
      Top = 128
      Width = 139
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 11
      OnExit = EdCelExit
    end
    object EdFax: TEdit
      Left = 704
      Top = 128
      Width = 139
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 12
      OnExit = EdFaxExit
    end
  end
  object QrDuplic2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Cliente1, Cliente2'
      'FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 68
    Top = 6
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDuplic2Cliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrDuplic2Cliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 40
    Top = 6
  end
  object QrListaLograd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM listalograd'
      'ORDER BY Nome')
    Left = 12
    Top = 6
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades '
      'WHERE Codigo=:P0')
    Left = 96
    Top = 6
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrUFs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM ufs'
      'ORDER BY Nome')
    Left = 125
    Top = 5
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 153
    Top = 6
  end
  object PMCEP: TPopupMenu
    Left = 48
    Top = 178
    object Descobrir1: TMenuItem
      Caption = '&Descobrir'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = '&Verificar'
      OnClick = Verificar1Click
    end
  end
  object QrMD5Cab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM md5cab'
      'WHERE Codigo=:P0')
    Left = 388
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMD5CabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMD5CabSenha: TWideStringField
      FieldName = 'Senha'
      Size = 8
    end
    object QrMD5CabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrMD5CabForca: TSmallintField
      FieldName = 'Forca'
    end
    object QrMD5CabQuartos: TSmallintField
      FieldName = 'Quartos'
    end
  end
end
