unit MD5Imp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, DB, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, Mask, frxClass, frxDBSet,
  frxBarcod, frxBarcode, dmkPermissoes;

type
  TFmMD5Imp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrPage: TmySQLQuery;
    QrPageNome: TWideStringField;
    QrPageObserva: TWideMemoField;
    QrPageCodigo: TIntegerField;
    QrPagePagCol: TIntegerField;
    QrPagePagLin: TIntegerField;
    QrPagePagLef: TIntegerField;
    QrPagePagTop: TIntegerField;
    QrPagePagWid: TIntegerField;
    QrPagePagHei: TIntegerField;
    QrPagePagGap: TIntegerField;
    QrPageBanCol: TIntegerField;
    QrPageBanLin: TIntegerField;
    QrPageBanWid: TIntegerField;
    QrPageBanHei: TIntegerField;
    QrPageBanLef: TIntegerField;
    QrPageBanTop: TIntegerField;
    QrPageBanGap: TIntegerField;
    QrPageOrient: TIntegerField;
    QrPageDataCad: TDateField;
    QrPageDataAlt: TDateField;
    QrPageUserCad: TSmallintField;
    QrPageUserAlt: TSmallintField;
    QrPageLk: TIntegerField;
    QrPageColWid: TIntegerField;
    QrPageBa2Hei: TIntegerField;
    QrPageFoSize: TSmallintField;
    QrPageMemHei: TIntegerField;
    QrPageEspaco: TSmallintField;
    DsPage: TDataSource;
    Label4: TLabel;
    EdConfImp: TdmkEditCB;
    CBConfImp: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    EdEspacamento: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdColIni: TdmkEdit;
    EdLinIni: TdmkEdit;
    Label7: TLabel;
    LaRepeticoes: TLabel;
    EdRepeticoes: TdmkEdit;
    Progress: TProgressBar;
    QrMinhaEtiq: TmySQLQuery;
    QrMinhaEtiqNome: TWideStringField;
    QrMinhaEtiqConta: TIntegerField;
    QrMinhaEtiqTexto1: TWideStringField;
    QrMinhaEtiqTexto2: TWideStringField;
    QrMinhaEtiqTexto3: TWideStringField;
    QrMinhaEtiqTexto4: TWideStringField;
    CkInfoCod: TCheckBox;
    frxEtiquetas: TfrxReport;
    frxDsMinhaEtiq: TfrxDBDataset;
    CkGrade: TCheckBox;
    CkDesign: TCheckBox;
    QrPageBarLef: TIntegerField;
    QrPageBarTop: TIntegerField;
    QrPageBarWid: TIntegerField;
    QrPageBarHei: TIntegerField;
    Label1: TLabel;
    EdNumIni: TdmkEdit;
    EdNumFim: TdmkEdit;
    Label2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxEtiquetasGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FContaCol,
    Imp_LinhasEtiqHei, Imp_LinhasEtiq: Integer;
    Imp_Espacos: String;
  {
  Imp_Espacos: String;
  Imp_LinhasEtiq, Imp_LinhasEtiqHei, FContaCol: Integer;
  pOrient: TPrinterOrientation;
  }
    //procedure ReopenMinhaEtqIts;
    procedure ImprimeEtiqueta;
  public
    { Public declarations }
  end;

  var
  FmMD5Imp: TFmMD5Imp;

implementation


uses Principal, UnInternalConsts, Module, UCreate, MD5Cab, UnitMD5, uEncrypt,
dmkGeral, UnMyObjects;

{$R *.DFM}

procedure TFmMD5Imp.BtOKClick(Sender: TObject);
var
  k, i, Conta, Col, Lin, MaxCol, ConfImp: Integer;
  MD5i, MD5p, Titulo: String;
begin
  Screen.Cursor := crHourGlass;
  try
  Imp_LinhasEtiqHei := 1;
  Imp_Espacos := Geral.CompletaString(
  ' ', ' ', Geral.IMV(EdEspacamento.Text), taRightJustify, True);
  Imp_LinhasEtiq := 1;//QrMultiEtqLinhas.Value;
  //
  Progress.Update;
  // Etiquetas (endereco ou diversos)
  ConfImp := Geral.IMV(EdConfImp.Text);
  if ConfImp = 0 then
  begin
    Application.MessageBox('Defina a "Configura��o de impress�o"!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    EdConfImp.SetFocus;
    Exit;
  end;
  MaxCol := QrPageBanCol.Value;
  Col := Geral.IMV(EdColIni.Text) - 1;
  if (Col >= MaxCol) (*and (RGConsulta.ItemIndex > 0)*) then
  begin
    Application.MessageBox('Coluna inicial fora dos par�metros!', 'Erro',
    MB_OK+MB_ICONERROR);
    EdColIni.SetFocus;
    Exit;
  end;
  Lin := Geral.IMV(EdLinIni.Text) - 1;
  Conta := (Lin * QrPageBanCol.Value) + Col;
  UCriar.RecriaTabelaLocal('MinhaEtiq', 1);
  ////
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO minhaetiq SET ');
  Dmod.QrUpdL.SQL.Add('Nome=:P0, Conta=:P1, Texto1=:P2 ');
  Progress.Position := 0;
  Progress.Max := Conta + EdNumFim.ValueVariant + 1 - EdNumIni.ValueVariant;
  for i  := 1 to Conta do
  begin
    Progress.Position := Progress.Position + 1;
    Dmod.QrUpdL.Params[0].AsString  := CO_VAZIO;
    Dmod.QrUpdL.Params[1].AsInteger := Conta;
    Dmod.QrUpdL.Params[2].AsString  := CO_VAZIO;
    Dmod.QrUpdL.ExecSQL;
  end;
  for k := EdNumIni.ValueVariant to EdNumFim.ValueVariant do
  begin
    Progress.Position := Progress.Position + 1;
    case FmMD5Cab.QrMD5CabForca.Value of
      0:
      begin
        MD5i := UnEncrypt.Encrypt(FormatFloat('0000', k) +
          FmMD5Cab.QrMD5CabSenha.Value, FmPrincipal.Fmy_key_uEncrypt);
        MD5p := MLAGeral.ParticionaTexto(MD5i, 4);
      end;
      1:
      begin
        MD5i := UnMD5.StrMD5(FormatFloat('00000000000', k) +
          FmMD5Cab.QrMD5CabSenha.Value);
        MD5p := MLAGeral.ParticionaTexto(MD5i, 8);
      end;
    end;
    //
    Titulo := IntToStr(FmMD5Cab.QrMD5CabCodigo.Value) + ' - ' + IntToStr(k);
    Conta := Conta + 1;
    Progress.Position := Progress.Position + 1;
    Dmod.QrUpdL.Params[0].AsString  := Titulo;
    Dmod.QrUpdL.Params[1].AsInteger := Conta;
    Dmod.QrUpdL.Params[2].AsString  := MD5p;
    Dmod.QrUpdL.ExecSQL;
  end;
  Progress.Max := Progress.Max * 4 (*Linhas*);
  Progress.Position := 0;
  //EtiquetasDiversas;
  QrMinhaEtiq.Close;
  QrMinhaEtiq.Open;
  ImprimeEtiqueta;
  Progress.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMD5Imp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMD5Imp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  EdNumIni.ValMin := IntToStr(FmMD5Cab.QrMD5FxaNumIni.Value);
  EdNumIni.ValMax := IntToStr(FmMD5Cab.QrMD5FxaNumFim.Value);
  EdNumIni.ValueVariant := IntToStr(FmMD5Cab.QrMD5FxaNumIni.Value);
  //
  EdNumFim.ValMin := IntToStr(FmMD5Cab.QrMD5FxaNumIni.Value);
  EdNumFim.ValMax := IntToStr(FmMD5Cab.QrMD5FxaNumFim.Value);
  EdNumFim.ValueVariant := IntToStr(FmMD5Cab.QrMD5FxaNumFim.Value);
end;

procedure TFmMD5Imp.FormCreate(Sender: TObject);
begin
  QrPage.Open;
  if QrPage.RecordCount = 1 then
  begin
    EdConfimp.ValueVariant := QrPageCodigo.Value;
    CBConfimp.KeyValue     := QrPageCodigo.Value;
  end;
end;

procedure TFmMD5Imp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMD5Imp.frxEtiquetasGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_DONO' then
  begin
    if QrMinhaEtiqTexto1.Value = '' then
      Value := ''
    else
      Value := Imp_Espacos + Dmod.QrMasterEm.Value
  end
  else
  if VarName = 'VARF_TXT1' then
    Value := Imp_Espacos + QrMinhaEtiqTexto1.Value
  else
  if VarName = 'VARF_NOME' then
  begin
    if not CkInfoCod.Checked then
      Value := ''
    else
      Value := Imp_Espacos + QrMinhaEtiqNome.Value;
  end
end;

procedure TFmMD5Imp.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CriaPages;
  QrPage.Close;
  QrPage.Open;
  EdConfimp.Text := IntToStr(VAR_CONFIPAGE);
  CBConfimp.KeyValue := VAR_CONFIPAGE;
end;

procedure TFmMD5Imp.ImprimeEtiqueta;
var
  Page: TfrxReportPage;
  // Topo em branco
  Head: TfrxPageHeader;
  // Banda (MasterData)
  Band: TfrxMasterData;
  BANLEF, BANTOP, BANWID, BANHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;
  // Memo
  Memo: TfrxMemoView;
  //Align, i,
  L, T, W, H, k, TopoIni: Integer;
  Fator: Double;
begin
  Fator := 0;
  k     := -1;
  // Configura p�gina
  while frxEtiquetas.PagesCount > 0 do
    frxEtiquetas.Pages[0].Free;
  Page := TfrxReportPage.Create(frxEtiquetas);
  Page.CreateUniqueName;
  Page.LeftMargin   := 0;//QrPagePagLef.Value / VAR_DOTIMP;
  Page.RightMargin  := 0;
  Page.TopMargin    := 0;//QrPagePagTop.Value / VAR_DOTIMP;
  Page.BottomMargin := 0;
  Page.Height       := Trunc(QrPagePagHei.Value / VAR_DOTIMP);
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L := 0;
  T := 0;
  W := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  H := Trunc(QrPagePagTop.Value / VAR_DOTIMP);
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  // Configura Banda
  BANLEF := Trunc(QrPageBanLef.Value / VAR_DOTIMP);
  BANTOP := Trunc(QrPageBanTop.Value / VAR_DOTIMP);
  BANWID := Trunc(QrPageBanWid.Value / VAR_DOTIMP);
  BANHEI := Trunc(QrPageBanHei.Value / VAR_DOTIMP);
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  Band.Columns     := QrPageBanCol.Value;
  Band.ColumnGap   := Trunc(QrPageBanGap.Value / VAR_DOTIMP);
  Band.ColumnWidth := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  Band.Font.size   := QrPageFoSize.Value;
  Band.DataSet     := frxDsMinhaEtiq;
  //
  //////////////////////////////////////////////////////////////////////////////
  FContaCol := 0;
  // Top := Banda1.Top + <VARF_BA2HEI> + <VARF_TOP000>;
  TopoIni    := 0;
  // Height:= QrPageMemHei.Value / VAR_DOTIMP;
  MEMHEI := Trunc(QrPageMemHei.Value / VAR_DOTIMP);
  // Width := QrPageColWid.Value / VAR_DOTIMP;
  MEMWID := Trunc(QrPageColWid.Value / VAR_DOTIMP);
  MEMLEF := Trunc((QrPageBanLef.Value / VAR_DOTIMP) + (Fator *
       ((QrPageColWid.Value / VAR_DOTIMP)+(QrPageBanGap.Value / VAR_DOTIMP))));
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_DONO]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_TXT1]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  inc(k, 1);
  MEMTOP := TopoIni + (k * MEMHEI);
  Memo := TfrxMemoView.Create(Band);
  Memo.CreateUniqueName;
  Memo.Visible := True;
  Memo.SetBounds(MEMLEF, MEMTOP, MEMWID, MEMHEI);
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := QrPageFoSize.Value;
  Memo.Memo.Text   := '[VARF_NOME]';
  if CkGrade.Checked then
    Memo.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  if CkDesign.Checked then
    frxEtiquetas.DesignReport
  else
    MyObjects.frxMostra(frxEtiquetas, 'Etiquetas de C�digo de Barras');
end;

end.


