unit GraGruProd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, dmkLabel, Mask, dmkGeral,
  UnInternalConsts, dmkCheckBox;

type
  TFmGraGruProd = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    PnEdita: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnNiveis: TPanel;
    Label3: TLabel;
    dmkEdCBGradeG: TdmkEditCB;
    dmkCBGradeG: TdmkDBLookupComboBox;
    dmkEdNome: TdmkEdit;
    dmkEdCodigo: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    QrGradeG: TmySQLQuery;
    DsGradeG: TDataSource;
    QrGradeGCodigo: TIntegerField;
    QrGradeGNome: TWideStringField;
    Label1: TLabel;
    EdCodUsu: TdmkEdit;
    CkAtivo: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruProd: TFmGraGruProd;

implementation

uses Module, UMySQLModule, UnMySQLCuringa, GraGru, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruProd.BtOKClick(Sender: TObject);
var
  Codigo, Grupo, Ativo: Integer;
  Nome: String;
begin
  Nome  := dmkEdNome.ValueVariant;
  Grupo := dmkEdCBGradeG.ValueVariant;
  Ativo := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, dmkEdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Grupo = 0, dmkEdCBGradeG, 'Defina o grupo!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def_Old('grades', 'Codigo', LaTipo.Caption, FmGraGru.QrGradesCodigo.Value);
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'grades', False,
    ['Nome', 'GradeG', 'CodUsu', 'Ativo'], ['Codigo'],
    [dmkEdNome.ValueVariant, dmkEdCBGradeG.ValueVariant, EdCodUsu.ValueVariant, Ativo], [Codigo]) then
  begin
    FmGraGru.QrGrades.Close;
    FmGraGru.QrGrades.Open;
    VAR_COD := Codigo;
    //
    Close;
  end;
end;

procedure TFmGraGruProd.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := FmGraGru.QrGradesCodigo.Value;
  Close;
end;

procedure TFmGraGruProd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    dmkEdCodigo.ValueVariant := 0;
    dmkEdNome.ValueVariant   := '';
    CkAtivo.Checked          := True;
  end else
  begin
    dmkEdCodigo.ValueVariant   := FmGraGru.QrGradesCodigo.Value;
    EdCodUsu.ValueVariant      := FmGraGru.QrGradesCodUsu.Value;
    dmkEdNome.ValueVariant     := FmGraGru.QrGradesNome.Value;
    dmkEdCBGradeG.ValueVariant := FmGraGru.QrGradesGradeG.Value;
    dmkCBGradeG.KeyValue       := FmGraGru.QrGradesGradeG.Value;
    CkAtivo.Checked            := Geral.IntToBool(FmGraGru.QrGradesAtivo.Value);
  end;
  EdCodUsu.SetFocus;
end;

procedure TFmGraGruProd.FormCreate(Sender: TObject);
begin
  QrGradeG.Open;
end;

procedure TFmGraGruProd.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
