object FmAlunoVis: TFmAlunoVis
  Left = 339
  Top = 185
  Caption = 'CIC-CICLO-011 :: Visualiza'#231#227'o de Aluno'
  ClientHeight = 392
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 344
    Width = 496
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 340
    object Label7: TLabel
      Left = 116
      Top = 19
      Width = 64
      Height = 16
      Caption = 'Expira'#231#227'o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaDataExp: TLabel
      Left = 183
      Top = 19
      Width = 75
      Height = 16
      Caption = '24/05/2010'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtLocalizar: TBitBtn
      Tag = 22
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Localizar'
      TabOrder = 0
      OnClick = BtLocalizarClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 384
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 496
    Height = 48
    Align = alTop
    Caption = 'Visualiza'#231#227'o de Aluno'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 494
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 496
    Height = 296
    Align = alClient
    Enabled = False
    TabOrder = 0
    object Label55: TLabel
      Left = 8
      Top = 12
      Width = 30
      Height = 13
      Caption = 'Aluno:'
    end
    object Label1: TLabel
      Left = 410
      Top = 12
      Width = 55
      Height = 13
      Caption = 'Data curso:'
    end
    object Label2: TLabel
      Left = 8
      Top = 53
      Width = 36
      Height = 13
      Caption = 'Cidade:'
    end
    object Label3: TLabel
      Left = 445
      Top = 53
      Width = 17
      Height = 13
      Caption = 'UF:'
    end
    object Label4: TLabel
      Left = 8
      Top = 94
      Width = 47
      Height = 13
      Caption = 'Professor:'
    end
    object Label5: TLabel
      Left = 8
      Top = 133
      Width = 45
      Height = 13
      Caption = 'Promotor:'
    end
    object Label6: TLabel
      Left = 8
      Top = 174
      Width = 66
      Height = 13
      Caption = 'Observa'#231#245'es:'
    end
    object DBMemo1: TDBMemo
      Left = 8
      Top = 190
      Width = 477
      Height = 89
      DataField = 'Observa'
      DataSource = DsPesq
      TabOrder = 0
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 28
      Width = 400
      Height = 21
      DataField = 'Aluno'
      DataSource = DsPesq
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Left = 410
      Top = 28
      Width = 75
      Height = 21
      DataField = 'Data'
      DataSource = DsPesq
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Left = 8
      Top = 69
      Width = 434
      Height = 21
      DataField = 'MUNI'
      DataSource = DsPesq
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 445
      Top = 69
      Width = 40
      Height = 21
      DataField = 'UF'
      DataSource = DsPesq
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 8
      Top = 109
      Width = 477
      Height = 21
      DataField = 'NOMEPRO'
      DataSource = DsPesq
      TabOrder = 5
    end
    object DBEdit6: TDBEdit
      Left = 8
      Top = 149
      Width = 477
      Height = 21
      DataField = 'NOMEPRM'
      DataSource = DsPesq
      TabOrder = 6
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPesqAfterScroll
    SQL.Strings = (
      'SELECT alu.Codigo, alu.Controle, alu.Entidade, aul.Data, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Aluno,'
      'IF(prm.Tipo=0, prm.RazaoSocial, prm.Nome) NOMEPRM,'
      'IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome) NOMEPRO,'
      'dtb.Nome MUNI, aul.UF, aul.Observa'
      'FROM ciclosalu alu'
      'LEFT JOIN ciclosaula aul ON aul.Controle = alu.Controle'
      'LEFT JOIN ciclos cic ON cic.Codigo = alu.Codigo'
      'LEFT JOIN dtb_munici dtb ON dtb.Codigo = aul.CodiCidade'
      'LEFT JOIN entidades ent ON ent.Codigo=alu.Entidade'
      'LEFT JOIN entidades prm ON prm.Codigo=aul.Promotor'
      'LEFT JOIN entidades pro ON pro.Codigo=cic.Professor '
      'WHERE alu.Conta=:P0')
    Left = 8
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqObserva: TWideStringField
      FieldName = 'Observa'
      Size = 255
    end
    object QrPesqData: TDateField
      FieldName = 'Data'
    end
    object QrPesqAluno: TWideStringField
      FieldName = 'Aluno'
      Size = 100
    end
    object QrPesqNOMEPRM: TWideStringField
      FieldName = 'NOMEPRM'
      Size = 100
    end
    object QrPesqNOMEPRO: TWideStringField
      FieldName = 'NOMEPRO'
      Size = 100
    end
    object QrPesqMUNI: TWideStringField
      FieldName = 'MUNI'
      Size = 100
    end
    object QrPesqUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 36
    Top = 9
  end
end
