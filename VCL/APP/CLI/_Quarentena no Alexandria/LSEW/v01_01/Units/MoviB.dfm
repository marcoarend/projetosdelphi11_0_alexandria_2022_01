object FmMoviB: TFmMoviB
  Left = 340
  Top = 174
  Caption = 'PRD-BALAN-001 :: Balan'#231'os de Mercadorias'
  ClientHeight = 635
  ClientWidth = 975
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 985
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnDados: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 517
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelConfirma: TPanel
      Left = 1
      Top = 457
      Width = 973
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      Visible = False
      object BtTrava: TBitBtn
        Tag = 14
        Left = 10
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Trava'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtTravaClick
      end
      object BtIncluiIts: TBitBtn
        Tag = 10
        Left = 399
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Insere item'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Insere'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiItsClick
      end
      object BtAlteraIts: TBitBtn
        Tag = 11
        Left = 512
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Edita item'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Edita'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = False
        OnClick = BtAlteraItsClick
      end
      object BtExcluiIts: TBitBtn
        Tag = 12
        Left = 625
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Exclui item'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Visible = False
        OnClick = BtExcluiItsClick
      end
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 187
        Top = 5
        Width = 148
        Height = 49
        Hint = 'Confirma / cancela balan'#231'o|Confirma ou cancela o balan'#231'o atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faz / Desfaz'
        TabOrder = 4
        Visible = False
        OnClick = BtRefreshClick
      end
      object BtPrivativo: TBitBtn
        Tag = 182
        Left = 738
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 
          'Zera estoque de todas mercadorias|Exclui todos itens de balan'#231'o,' +
          ' e depois zera estoques negativos.'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Zera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        Visible = False
        OnClick = BtPrivativoClick
      end
    end
    object PainelDados1: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 62
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 522
        Top = 7
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Balan'#231'o Qtde:'
      end
      object Label2: TLabel
        Left = 635
        Top = 7
        Width = 88
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Balan'#231'o Valor:'
      end
      object RGMerca: TRadioGroup
        Left = 0
        Top = 2
        Width = 164
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Ordem das Mercadorias: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Nome'
          'Codigo')
        TabOrder = 0
        OnClick = RGMercaClick
      end
      object RGGrupo: TRadioGroup
        Left = 167
        Top = 2
        Width = 351
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Odem complementar: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Codigo do Grupo'
          'Listagem das Mercador.')
        TabOrder = 1
        OnClick = RGGrupoClick
      end
      object DBEdit1: TDBEdit
        Left = 522
        Top = 27
        Width = 109
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Qtd'
        DataSource = DsMoviB
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 635
        Top = 27
        Width = 110
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Val'
        DataSource = DsMoviB
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 753
        Top = 30
        Width = 120
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Confirmado.'
        DataSource = DsMoviB
        TabOrder = 4
        ValueChecked = 'V'
        ValueUnchecked = 'F'
        Visible = False
      end
    end
    object PainelDados2: TPanel
      Left = 1
      Top = 432
      Width = 973
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      Visible = False
      object Progress1: TProgressBar
        Left = 577
        Top = 1
        Width = 394
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Position = 50
        Smooth = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 281
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvLowered
        Caption = 'Aguarde...    Atualizando estoque...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 1
      end
      object Panel2: TPanel
        Left = 282
        Top = 1
        Width = 74
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Registros:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 2
      end
      object PnRegistros: TPanel
        Left = 356
        Top = 1
        Width = 93
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 3
      end
      object Panel4: TPanel
        Left = 449
        Top = 1
        Width = 59
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Tempo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 4
      end
      object PnTempo: TPanel
        Left = 508
        Top = 1
        Width = 69
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0:00:00  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 5
      end
    end
    object DBGItens: TDBGrid
      Left = 1
      Top = 63
      Width = 973
      Height = 369
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsMovim
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnKeyDown = DBGItensKeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEGRUPO'
          Title.Caption = 'Grupo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGRADE'
          Title.Caption = 'Mercadoria'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Cor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtd'
          Title.Caption = 'Quantidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Val'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUSTO'
          Title.Caption = 'Custo'
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                              Balan'#231'os de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 873
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 595
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 576
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object LaRegistro: TdmkLabel
      Left = 213
      Top = 1
      Width = 31
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = '[N]: 0'
      UpdType = utYes
      SQLType = stNil
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 212
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 158
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 108
        Top = 5
        Width = 50
        Height = 49
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 59
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 10
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 396
      Top = 1
      Width = 578
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 458
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 231
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 118
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 5
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
      end
    end
  end
  object DsMoviB: TDataSource
    DataSet = QrMoviB
    Left = 588
    Top = 9
  end
  object DsMovim: TDataSource
    DataSet = QrMovim
    Left = 644
    Top = 9
  end
  object DsArtigo: TDataSource
    DataSet = QrArtigo
    Left = 36
    Top = 221
  end
  object QrMoviB: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMoviBAfterOpen
    AfterScroll = QrMoviBAfterScroll
    OnCalcFields = QrMoviBCalcFields
    SQL.Strings = (
      'SELECT * FROM movib')
    Left = 560
    Top = 9
    object QrMoviBPeriodo2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Periodo2'
      Size = 50
      Calculated = True
    end
    object QrMoviBPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrMoviBQtd: TFloatField
      FieldName = 'Qtd'
      DisplayFormat = '#,###,##0.000'
    end
    object QrMoviBVal: TFloatField
      FieldName = 'Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMoviBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMoviBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMoviBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMoviBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMoviBControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrMovim: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMovimAfterOpen
    OnCalcFields = QrMovimCalcFields
    SQL.Strings = (
      'SELECT grg.Nome NOMEGRUPO, gra.Nome NOMEGRADE, '
      'tam.Nome NOMETAM, cor.Nome NOMECOR, '
      'pro.Qtd, pro.Val'
      'FROM movim pro'
      'LEFT JOIN grades   gra ON gra.Codigo=pro.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=pro.Tam'
      'LEFT JOIN cores    cor ON cor.Codigo=pro.Cor'
      'LEFT JOIN gradeg   grg ON grg.Codigo=gra.GradeG'
      'WHERE Controle= :P0'
      '/*Para poder ordenar!!!*/'
      'ORDER BY NOMEGRUPO, NOMEGRADE, NOMECOR, NOMETAM')
    Left = 616
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 30
    end
    object QrMovimNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 30
    end
    object QrMovimNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
    object QrMovimNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrMovimQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrMovimVal: TFloatField
      FieldName = 'Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimCUSTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object QrArtigo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 8
    Top = 221
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArtigoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArtigoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM movib')
    Left = 64
    Top = 253
    object QrMaxPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtd) Qtd, SUM(Val) Val'
      'FROM movim'
      'WHERE Controle=:P0')
    Left = 36
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrSomaVal: TFloatField
      FieldName = 'Val'
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, EstqQ, EstqV'
      'FROM produtos'
      'WHERE EstqQ<>0'
      'OR EstqV<>0'
      '')
    Left = 448
    Top = 144
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
  end
  object QrAtualiza: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Cor, Tam'
      'FROM produtos')
    Left = 8
    Top = 252
    object QrAtualizaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtualizaCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrAtualizaTam: TIntegerField
      FieldName = 'Tam'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanIns02 = BtIncluiIts
    CanUpd01 = BtAltera
    CanUpd02 = BtAlteraIts
    CanDel01 = BtExclui
    CanDel02 = BtExcluiIts
    Left = 260
    Top = 12
  end
end
