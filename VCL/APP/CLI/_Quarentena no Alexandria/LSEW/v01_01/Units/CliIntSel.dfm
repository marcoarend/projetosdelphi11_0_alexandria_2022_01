object FmCliIntSel: TFmCliIntSel
  Left = 339
  Top = 185
  Caption = 'CAD-CLIIN-002 :: Clientes Internos'
  ClientHeight = 320
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 272
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Clientes internos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 585
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliInt
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 697
      Height = 21
      KeyField = 'CODENT'
      ListField = 'NOMECLIENTE'
      ListSource = DsCliInt
      TabOrder = 1
      dmkEditCB = EdCliInt
      UpdType = utYes
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 167
    Align = alClient
    TabOrder = 3
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 44
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 12
        Top = 4
        Width = 147
        Height = 13
        Caption = 'Pesquisa por descri'#231#227'o parcial:'
      end
      object EdNome: TdmkEdit
        Left = 12
        Top = 20
        Width = 761
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdNomeChange
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 45
      Width = 782
      Height = 121
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'CODENT'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIENTE'
          Title.Caption = 'Raz'#227'o Social'
          Width = 615
          Visible = True
        end>
      Color = clWindow
      DataSource = DsCliInt
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dmkDBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'CODENT'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIENTE'
          Title.Caption = 'Raz'#227'o Social'
          Width = 615
          Visible = True
        end>
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cli.*, ent.Codigo CODENT,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLIENTE'
      'FROM cliint cli'
      'LEFT JOIN entidades ent ON ent.Codigo=cli.Cliente'
      'WHERE IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0'
      'ORDER BY NOMECLIENTE')
    Left = 636
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliIntCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCliIntLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCliIntDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCliIntDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCliIntUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCliIntUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCliIntAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCliIntAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCliIntNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrCliIntCODENT: TIntegerField
      FieldName = 'CODENT'
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 664
    Top = 200
  end
end
