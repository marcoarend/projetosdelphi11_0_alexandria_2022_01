unit MoviV;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UmySQLModule, UnMySQLCuringa, mySQLDbTables, dmkPermissoes,
  dmkGeral, frxClass, dmkDBEdit, Grids, DBGrids, dmkDBGrid, ComCtrls, Menus,
  frxDBSet, Shellapi, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, dmkLabel, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmMoviV = class(TForm)
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtPagtos: TBitBtn;
    BtVenda: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PainelDados: TPanel;
    PnTopo: TPanel;
    Panel8: TPanel;
    Panel19: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Panel1: TPanel;
    Label1: TLabel;
    Label26: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    EdDBDataVenda: TdmkDBEdit;
    EdDBDataPed: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    PnInfoCli: TPanel;
    Panel14: TPanel;
    Label2: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Panel15: TPanel;
    BtUltimo: TBitBtn;
    PnBottom: TPanel;
    Label22: TLabel;
    Label25: TLabel;
    Label24: TLabel;
    Label21: TLabel;
    Label18: TLabel;
    Panel4: TPanel;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    PMVenda: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    DBText1: TDBText;
    Label23: TLabel;
    QrMoviV: TmySQLQuery;
    QrMoviVNOMEENTI: TWideStringField;
    QrMoviVCodigo: TIntegerField;
    QrMoviVControle: TIntegerField;
    QrMoviVDataPedi: TDateField;
    QrMoviVDataReal: TDateField;
    QrMoviVCliente: TIntegerField;
    QrMoviVLk: TIntegerField;
    QrMoviVDataCad: TDateField;
    QrMoviVDataAlt: TDateField;
    QrMoviVUserCad: TIntegerField;
    QrMoviVUserAlt: TIntegerField;
    QrMoviVDATAREAL_TXT: TWideStringField;
    QrMoviVSALDO: TFloatField;
    QrMoviVTotal: TFloatField;
    QrMoviVPago: TFloatField;
    QrMoviVPOSIt: TFloatField;
    QrMoviVAlterWeb: TSmallintField;
    QrMoviVAtivo: TSmallintField;
    QrMoviVGraCusPrc: TIntegerField;
    QrMoviVTransportadora: TIntegerField;
    QrMoviVFrete: TFloatField;
    QrMoviVNOMECAD2: TWideStringField;
    QrMoviVNOMEALT2: TWideStringField;
    QrMoviVNOMECAD: TWideStringField;
    QrMoviVNOMEALT: TWideStringField;
    QrMoviVFRETEA: TFloatField;
    QrMoviVObserv: TWideStringField;
    QrMoviVDescon: TFloatField;
    QrMoviVCodRast: TWideStringField;
    QrMoviVStatAtual: TIntegerField;
    QrMoviVNOMESTATAT: TWideStringField;
    DsMoviV: TDataSource;
    QrMovim: TmySQLQuery;
    QrMovimNOMEGRA: TWideStringField;
    QrMovimNOMETAM: TWideStringField;
    QrMovimNOMECOR: TWideStringField;
    QrMovimControle: TIntegerField;
    QrMovimConta: TIntegerField;
    QrMovimGrade: TIntegerField;
    QrMovimCor: TIntegerField;
    QrMovimTam: TIntegerField;
    QrMovimQtd: TFloatField;
    QrMovimVal: TFloatField;
    QrMovimLk: TIntegerField;
    QrMovimDataCad: TDateField;
    QrMovimDataAlt: TDateField;
    QrMovimUserCad: TIntegerField;
    QrMovimUserAlt: TIntegerField;
    QrMovimDataPedi: TDateField;
    QrMovimDataReal: TDateField;
    QrMovimPRECO: TFloatField;
    QrMovimMotivo: TSmallintField;
    QrMovimVen: TFloatField;
    QrMovimPOSIq: TFloatField;
    QrMovimPOSIv: TFloatField;
    QrMovimVAZIO: TIntegerField;
    DsMovim: TDataSource;
    QrMoviVK: TmySQLQuery;
    QrMoviVKCodigo: TIntegerField;
    QrMoviVKControle: TIntegerField;
    QrMoviVKGradeK: TIntegerField;
    QrMoviVKQtd: TIntegerField;
    QrMoviVKNome: TWideStringField;
    QrMoviVKFRETE: TFloatField;
    QrMoviVKTotal: TFloatField;
    DsMoviVK: TDataSource;
    QrSenhas: TmySQLQuery;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasLk: TIntegerField;
    DsSenhas: TDataSource;
    QrMoviVNOMGRACUSPRC: TWideStringField;
    QrHist: TmySQLQuery;
    QrHistDATAREAL_TXT: TWideStringField;
    QrHistPOSIt: TFloatField;
    QrHistTotal: TFloatField;
    QrHistFrete: TFloatField;
    QrHistDataReal: TDateField;
    QrHistNOMECAD2: TWideStringField;
    QrHistNOMECAD: TWideStringField;
    QrHistUserCad: TIntegerField;
    QrHistCodigo: TIntegerField;
    DsHist: TDataSource;
    QrMoviVTOTALVDA: TFloatField;
    QrMoviVStat: TmySQLQuery;
    QrMoviVStatCodigo: TIntegerField;
    QrMoviVStatControle: TIntegerField;
    QrMoviVStatStatusV: TIntegerField;
    QrMoviVStatDiaHora: TDateTimeField;
    QrMoviVStatNOMESTATUS: TWideStringField;
    QrMoviVStatCor: TIntegerField;
    QrMoviVStatSTATUS_STR: TWideStringField;
    DsMoviVStat: TDataSource;
    PMPagtos: TPopupMenu;
    IncluiPagtos1: TMenuItem;
    ExcluiPagtos1: TMenuItem;
    QrTotPagtos: TmySQLQuery;
    QrTotPagtosCredito: TFloatField;
    QrTotKit: TmySQLQuery;
    QrTotKitTotal: TFloatField;
    QrTotProd: TmySQLQuery;
    QrTotProdTotal: TFloatField;
    QrPagtos: TmySQLQuery;
    QrPagtosSEQ: TIntegerField;
    QrPagtosData: TDateField;
    QrPagtosVencimento: TDateField;
    QrPagtosCredito: TFloatField;
    QrPagtosBanco: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosDocumento: TFloatField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosFatNum: TFloatField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosControle: TIntegerField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrPagtosCompensado: TDateField;
    DsPagtos: TDataSource;
    LaRastrea: TLabel;
    dmkDBEdit13: TdmkDBEdit;
    BtRast: TBitBtn;
    QrTotMovim: TmySQLQuery;
    QrTotMovimTOTITENS: TFloatField;
    QrTotMoviVK: TmySQLQuery;
    QrTotMoviVKTOTFRETE: TFloatField;
    QrTotMoviVKIts: TmySQLQuery;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    QrCliNUMERO: TLargeintField;
    QrCliUF: TLargeintField;
    QrCliLograd: TLargeintField;
    QrCliCEP: TLargeintField;
    QrCliE_ALL2: TWideStringField;
    frxDsMoviV: TfrxDBDataset;
    frxDsMovim: TfrxDBDataset;
    frxDsMoviVK: TfrxDBDataset;
    frxDsMoviVKIts: TfrxDBDataset;
    frxDsCli: TfrxDBDataset;
    frxDsMoviVStat: TfrxDBDataset;
    frxPedidosV: TfrxReport;
    QrTotMoviVKItsTOTKITITS: TFloatField;
    QrMoviVKIts: TmySQLQuery;
    QrMoviVKItsGrade: TIntegerField;
    QrMoviVKItsTam: TIntegerField;
    QrMoviVKItsCor: TIntegerField;
    QrMoviVKItsQtd: TFloatField;
    QrMoviVKItsVal: TFloatField;
    QrMoviVKItsNOMEGRADE: TWideStringField;
    QrMoviVKItsNOMECOR: TWideStringField;
    QrMoviVKItsNOMETAM: TWideStringField;
    QrMoviVKItsPRECO: TFloatField;
    QrMoviVKItsPOSIq: TFloatField;
    QrMoviVKItsPOSIv: TFloatField;
    QrMoviVKItsVAZIO: TIntegerField;
    DsMoviVKIts: TDataSource;
    QrMovim2: TmySQLQuery;
    QrMovim2Grade: TIntegerField;
    QrMovim2Cor: TIntegerField;
    QrMovim2Tam: TIntegerField;
    QrMovim2Val: TFloatField;
    QrMovim2Qtd: TFloatField;
    QrMovim2Conta: TIntegerField;
    QrLoc: TmySQLQuery;
    QrFutNeg: TmySQLQuery;
    QrFutNegConta: TIntegerField;
    QrFutNegGrade: TIntegerField;
    QrFutNegCor: TIntegerField;
    QrFutNegTam: TIntegerField;
    QrFutNegQtd: TFloatField;
    QrFutNegVal: TFloatField;
    QrFutNegEstqQ: TFloatField;
    QrFutNegEstqV: TFloatField;
    QrFutNegFutQtd: TFloatField;
    QrFutNegFutVal: TFloatField;
    QrFutNegNOMEGRADE: TWideStringField;
    QrFutNegNOMECOR: TWideStringField;
    QrFutNegNOMETAM: TWideStringField;
    frxDsFutNeg: TfrxDBDataset;
    frxFutNeg: TfrxReport;
    QrMoviVKNOMEFOTO: TWideStringField;
    QrLocFoto: TmySQLQuery;
    QrLocFotoNOMEFOTO: TWideStringField;
    LaDataExp: TLabel;
    Label12: TLabel;
    QrPagtosAgencia: TIntegerField;
    PMImprime: TPopupMenu;
    Vendaatual1: TMenuItem;
    Vendaatualcompreo1: TMenuItem;
    N1: TMenuItem;
    dmkPermissoes2: TdmkPermissoes;
    PCProd: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    DBGridProd: TdmkDBGrid;
    Panel6: TPanel;
    Panel9: TPanel;
    Image2: TImage;
    Panel7: TPanel;
    Label17: TLabel;
    DBText2: TDBText;
    Label19: TLabel;
    DBLookupListBox2: TDBLookupListBox;
    TabSheet2: TTabSheet;
    Panel10: TPanel;
    Panel11: TPanel;
    Image3: TImage;
    Panel12: TPanel;
    Label8: TLabel;
    DBText3: TDBText;
    Label11: TLabel;
    DBLookupListBox1: TDBLookupListBox;
    Panel13: TPanel;
    DBGridKit: TdmkDBGrid;
    StaticText2: TStaticText;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBEdCodigoDblClick(Sender: TObject);
    procedure BtVendaClick(Sender: TObject);
    procedure QrMoviVCalcFields(DataSet: TDataSet);
    procedure QrMovimCalcFields(DataSet: TDataSet);
    procedure QrMoviVAfterScroll(DataSet: TDataSet);
    procedure QrMoviVBeforeClose(DataSet: TDataSet);
    procedure QrHistCalcFields(DataSet: TDataSet);
    procedure BtUltimoClick(Sender: TObject);
    procedure QrMoviVStatCalcFields(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure IncluiPagtos1Click(Sender: TObject);
    procedure BtPagtosClick(Sender: TObject);
    procedure ExcluiPagtos1Click(Sender: TObject);
    procedure LaRastreaClick(Sender: TObject);
    procedure BtRastClick(Sender: TObject);
    procedure DBEdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure frxPedidosVGetValue(const VarName: string; var Value: Variant);
    procedure QrMoviVKAfterScroll(DataSet: TDataSet);
    procedure QrMoviVKBeforeClose(DataSet: TDataSet);
    procedure PMPagtosPopup(Sender: TObject);
    procedure DBGridKitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDBDataVendaDblClick(Sender: TObject);
    procedure frxFutNegGetValue(const VarName: string; var Value: Variant);
    procedure QrMovimAfterScroll(DataSet: TDataSet);
    procedure Vendaatual1Click(Sender: TObject);
    procedure Vendaatualcompreo1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FFoto: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenStatusV(Controle: Integer);
    procedure ReopenPagtos;
    procedure ReopenTOTAIS;
    procedure ReopenMoviVKIts;
    procedure ReopenCli(Cliente: Integer);
  public
    { Public declarations }
    FImprime, FPagto: Boolean;
    FCliente: Integer;
    FTabLctALS: String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenMovim(Conta: Integer);
    procedure ReopenMoviVK(Controle: Integer);
    procedure AtualizaTotVenda(Codigo, Controle: Integer);
    procedure ImprimeVenda(Imprime: Boolean);
    function AtualizaStatus(Codigo, Controle, FControle, Status: Integer): Integer;
    function ReopenHist(Cliente: Integer): Boolean;
  end;

var
  FmMoviV: TFmMoviV;

implementation

uses Module, VendaPesq, MyDBCheck, ModuleProd, ModuleGeral, MoviVEdit,
MoviVPagto, MoviVK, GraCusPrc, Principal, MoviVImp, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMoviV.LaRastreaClick(Sender: TObject);
var
  CodRast, Rastre, Link: String;
  PosIni, PosFim: Integer;
begin
  Rastre  := Dmod.QrControleEndRastre.Value;
  CodRast := QrMoviVCodRast.Value;
  //
  if (Length(Rastre) > 0) and (Length(CodRast) > 0) then
  begin
    Link := 'http://';
    //
    PosIni := Pos('[', Rastre);
    if PosIni > 0 then
    begin
      Link := Link + Trim(Copy(Rastre, 1, PosIni - 1));
      Link := Link + CodRast;
      //
      PosFim := Pos(']', Rastre);
      if PosFim > 0 then
        Link := Link + Trim(Copy(Rastre, PosFim + 1));
    end else
      Link := Link + Rastre;
    ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
  end;
end;

procedure TFmMoviV.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMoviV.PMPagtosPopup(Sender: TObject);
begin
  ExcluiPagtos1.Enabled := QrPagtos.RecordCount > 0;
end;

procedure TFmMoviV.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMoviVCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmMoviV.Vendaatual1Click(Sender: TObject);
begin
  FFoto := False;
  ImprimeVenda(False);
end;

procedure TFmMoviV.Vendaatualcompreo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMoviVImp, FmMoviVImp, afmoNegarComAviso) then
  begin
    ReopenCli(QrMoviVCliente.Value);
    //
    ReopenTOTAIS;
    //
    FmMoviVImp.ShowModal;
    FmMoviVImp.Destroy;
    //
    FFoto := True;
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMoviV.DefParams;
begin
  VAR_GOTOTABELA := 'moviv';
  VAR_GOTOmySQLTABLE := QrMoviV;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT prc.Nome NOMGRACUSPRC, stv.Nome NOMESTATAT,');
  VAR_SQLx.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  VAR_SQLx.Add('ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA, moc.*');
  VAR_SQLx.Add('FROM moviv moc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente');
  VAR_SQLx.Add('LEFT JOIN statusv stv ON stv.Codigo=moc.StatAtual');
  VAR_SQLx.Add('LEFT JOIN gracusprc prc ON prc.Codigo = moc.GraCusPrc');
  VAR_SQLx.Add('WHERE moc.Codigo > 0');
  //
  VAR_SQL1.Add('AND moc.Codigo=:P0');
  //
  VAR_SQLa.Add('AND moc.Nome Like :P0');
  //
end;

procedure TFmMoviV.EdDBDataVendaDblClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  QtdX, ValX, CusX: Double;
  Hoje: TDate;
begin
  if QrMoviVDataReal.Value = 0 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //    
    Codigo   := QrMoviVCodigo.Value;
    Controle := QrMoviVControle.Value;
    Hoje     := Date;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE moviv SET DataReal=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[0].AsDate    := Hoje;
    Dmod.QrUpd.Params[1].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE movim SET DataReal=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsDate    := Hoje;
    Dmod.QrUpd.Params[1].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    QrFutNeg.Close;
    QrFutNeg.Params[0].AsInteger := Controle;
    QrFutNeg.Open;
    if QrFutNeg.RecordCount > 0 then
    begin
      MyObjects.frxMostra(frxFutNeg, 'Estoque futuro negativo');
      Exit;
    end;
    //
    QrMovim2.Close;
    QrMovim2.Params[0].AsInteger := Controle;
    QrMovim2.Open;
    QrMovim2.First;
    while not QrMovim2.Eof do
    begin
      DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
        QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
      QrMovim2.Next;
    end;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMoviV.ExcluiPagtos1Click(Sender: TObject);
var
  FatNum, QtdX, ValX, CusX: Double;
  FatParcela, FatID, Codigo, Controle: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  FatParcela := QrPagtosFatParcela.Value;
  FatNum     := QrPagtosFatNum.Value;
  FatID      := QrPagtosFatID.Value;
  Codigo     := QrMoviVCodigo.Value;
  Controle   := QrMoviVControle.Value;
  //
  if MLAGeral.NaoPermiteExclusaoDeLancto(FatParcela, FatNum, FatID) then Exit;
  if Application.MessageBox('Confirma a exclus�o deste item de pagamento?',
  'Exclus�o de Pagamento', MB_YESNOCANCEL+MB_DEFBUTTON2+MB_ICONQUESTION) =
  ID_YES then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DELETE FROM ' + FTabLctALS);
    DMod.QrUpd.SQL.Add('WHERE FatID=510');
    DMod.QrUpd.SQL.Add('AND FatNum=:P0');
    DMod.QrUpd.Params[0].AsFloat := FatNum;
    DMod.QrUpd.ExecSQL;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
      'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, ',
      'la.Agencia, la.FatID, la.ContaCorrente, la.Documento, ',
      'la.Descricao, la.FatParcela, la.FatNum, ca.Nome NOMECARTEIRA, ',
      'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ca.Agencia1, ca.Conta1, ',
      'ca.TipoDoc, la.Controle, ca.Tipo CARTEIRATIPO, la.Compensado',
      'FROM ' + FTabLctALS + ' la',
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
      'WHERE FatID=' + Geral.FF0(VAR_FATID_0510),
      'AND FatNum=' + Geral.FFI(FatNum),
      'ORDER BY la.FatParcela, la.Vencimento',
      '']);
    //
    if QrMoviVDataReal.Value > 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE moviv SET DataReal=:P0 WHERE Codigo=:P1');
      Dmod.QrUpd.Params[0].AsDate    := 0;
      Dmod.QrUpd.Params[1].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE movim SET DataReal=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.Params[0].AsDate    := 0;
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      QrMovim2.Close;
      QrMovim2.Params[0].AsInteger := Controle;
      QrMovim2.Open;
      if QrMovim2.RecordCount > 0 then
      begin
        QrMovim2.First;
        while not QrMovim2.Eof do
        begin
          DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
            QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
          //
          QrMovim2.Next;
        end;
      end;
    end;
    AtualizaStatus(Codigo, Controle, 0, 1);
    AtualizaTotVenda(Codigo, Controle);
  end;
end;

procedure TFmMoviV.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMoviV.ReopenCli(Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCli, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CodUsu, en.IE, mue.Nome CIDADE, ',
  'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT, ',
  'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF, ',
  'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG, ',
  'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA, ',
  'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUMERO, ',
  'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL, ',
  'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO, ',
  'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, ',
  'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD, ',
  'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF, ',
  'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pais, ',
  'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Lograd, ',
  'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP, ',
  'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF, ',
  'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1, ',
  'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX, ',
  'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL ',
  'FROM entidades en ',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo = IF (en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
  'WHERE en.Codigo=' + Geral.FF0(Cliente),
  '']);
end;

function TFmMoviV.ReopenHist(Cliente: Integer): Boolean;
begin
  QrHist.Close;
  QrHist.Params[0].AsInteger := Cliente;
  QrHist.Open;
  //
  if QrHist.RecordCount > 0 then
    Result := True
  else
    Result := False;
end;

procedure TFmMoviV.ReopenMovim(Conta: Integer);
begin
  QrMovim.Close;
  QrMovim.Params[0].AsInteger := QrMoviVControle.Value;
  QrMovim.Open;
  //
  if Conta > 0 then QrMovim.Locate('Conta', Conta, []);
end;

procedure TFmMoviV.ReopenMoviVK(Controle: Integer);
begin
  QrMoviVK.Close;
  QrMoviVK.Params[0].AsInteger := QrMoviVCodigo.Value;
  QrMoviVK.Open;
  //
  if Controle > 0 then
    QrMoviVK.Locate('Controle', Controle, []);
end;

procedure TFmMoviV.ReopenMoviVKIts;
begin
  QrMoviVKIts.Close;
  QrMoviVKIts.Params[0].AsInteger := QrMoviVCodigo.Value;
  QrMoviVKIts.Params[1].AsInteger := QrMoviVKControle.Value;
  QrMoviVKIts.Open;
end;

procedure TFmMoviV.ReopenStatusV(Controle: Integer);
begin
  QrMoviVStat.Close;
  QrMoviVStat.Params[0].AsInteger := QrMoviVCodigo.Value;
  QrMoviVStat.Open;
  //
  if Controle > 0 then
      QrMoviVStat.Locate('Controle', Controle, []);
end;

procedure TFmMoviV.ReopenTOTAIS;
begin
  QrTotMovim.Close;
  QrTotMovim.Params[0].AsInteger := QrMoviVControle.Value;
  QrTotMovim.Open;
  //
  QrTotMoviVK.Close;
  QrTotMoviVK.Params[0].AsInteger := QrMoviVCodigo.Value;
  QrTotMoviVK.Open;
  //
  QrTotMoviVKIts.Close;
  QrTotMoviVKIts.Params[0].AsInteger := QrMoviVCodigo.Value;
  QrTotMoviVKIts.Open;
end;

procedure TFmMoviV.DBEdCodigoDblClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMoviVCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMoviV.DBEdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{var
  Codigo: Integer;}
begin
  //Codigo  := 0;
  if (Key = VK_F4) then
  begin
    FmPrincipal.FDepto    := 1;
    FmPrincipal.FCodDepto := QrMoviVCodigo.Value;
    Close;
    {if DBCheck.CriaFm(TFmVendaPesq, FmVendaPesq, afmoNegarComAviso) then
    begin
      FmVendaPesq.FDepto := 1;
      FmVendaPesq.ShowModal;
      FmVendaPesq.Destroy;
      Codigo := FmVendaPesq.FCodigo;
      if Codigo > 0 then
      begin
        LocCod(Codigo, Codigo);
      end;
    end;}
  end;
end;

procedure TFmMoviV.DBGridKitKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Kit, GraCusPrc: Integer;
begin
  if (Key = VK_F4) then
  begin
    Kit       := FmMoviV.QrMoviVKControle.Value;
    GraCusPrc := FmMoviV.QrMoviVGraCusPrc.Value;
    //
    if DBCheck.CriaFm(TFmMoviVK, FmMoviVK, afmoNegarComAviso) then
    begin
      FmMoviVK.FControle := FmMoviV.QrMoviVControle.Value;
      FmMoviVK.FKit      := Kit;
      FmMoviVK.FListaPre := GraCusPrc;
      FmMoviVK.FTrava    := True;
      //
      FmMoviVK.ShowModal;
      FmMoviVK.Destroy;
    end;
  end;
end;

procedure TFmMoviV.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMoviV.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmMoviV.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMoviV.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMoviV.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMoviV.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMoviV.Altera1Click(Sender: TObject);
begin
  FmPrincipal.CadastroVendasEdit(QrMoviVCodigo.Value, QrMoviVControle.Value, 0, stUpd);
end;

procedure TFmMoviV.BtPagtosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagtos, BtPagtos);
end;

procedure TFmMoviV.BtRastClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  CodRast: String;
begin
  CodRast := QrMoviVCodRast.Value;
  if InputQuery('C�digo de rastreamento', 'Informe o c�digo de rastreamento:', CodRast) then
  begin
    if Length(CodRast) > 0 then
    begin
      Codigo   := QrMoviVCodigo.Value;
      Controle := QrMoviVControle.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE moviv SET CodRast=:P0 WHERE Codigo=:P1');
      Dmod.QrUpd.Params[0].AsString  := CodRast;
      Dmod.QrUpd.Params[1].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      if Length(QrMoviVCodRast.Value) = 0 then
        AtualizaStatus(Codigo, Controle, 0, 4);
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmMoviV.BtSaidaClick(Sender: TObject);
begin
  VAR_COD            := QrMoviVCodigo.Value;
  FmPrincipal.FCodDepto := 0;
  FmPrincipal.FMoviVTot := QrMoviVTOTALVDA.Value;
  Close;
end;

procedure TFmMoviV.BtUltimoClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrHistCodigo.Value;
  LocCod(Codigo, Codigo);
end;

procedure TFmMoviV.BtVendaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVenda, BtVenda);
end;

procedure TFmMoviV.FormCreate(Sender: TObject);
begin
  PCProd.ActivePageIndex := 0;
  FFoto := True;
end;

procedure TFmMoviV.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmMoviV.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCliRUA.Value, QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL2.Value :=
    QrCliE_ALL2.Value + 'CEP ' + Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL2.Value :=
    QrCliE_ALL2.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL2.Value :=
    QrCliE_ALL2.Value + ', ' + QrCliNOMEUF.Value;
end;

procedure TFmMoviV.QrHistCalcFields(DataSet: TDataSet);
begin
  QrHistDATAREAL_TXT.Value := Geral.FDT(QrHistDataReal.Value, 2);
  QrHistPOSIt.Value        := QrHistTotal.Value + QrHistFrete.Value;
  case QrHistUserCad.Value of
    -2: QrHistNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrHistNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrHistNOMECAD2.Value := 'N�o definido';
    else QrHistNOMECAD2.Value := QrHistNOMECAD.Value;
  end;
end;

procedure TFmMoviV.QrMovimAfterScroll(DataSet: TDataSet);
var
  Produto: String;
begin
  QrLocFoto.Close;
  QrLocFoto.Params[0].AsInteger := QrMovimGrade.Value;
  QrLocFoto.Params[1].AsInteger := QrMovimCor.Value;
  QrLocFoto.Params[2].AsInteger := QrMovimTam.Value;
  QrLocFoto.Open;
  if QrLocFoto.RecordCount > 0 then
    Produto := QrLocFotoNOMEFOTO.Value
  else
    Produto := '';
  FmPrincipal.CarregaFoto(Image2, Produto);
  if FmMoviVEdit <> nil then
    FmPrincipal.CarregaFoto(FmMoviVEdit.Image2, Produto);
end;

procedure TFmMoviV.QrMovimCalcFields(DataSet: TDataSet);
begin
  if QrMovimQtd.Value = 0 then QrMovimPRECO.Value := 0 else
  QrMovimPRECO.Value := QrMovimVen.Value / -QrMovimQtd.Value;
  QrMovimPOSIq.Value := -QrMovimQtd.Value;
  QrMovimPOSIv.Value := QrMovimVen.Value;
end;

procedure TFmMoviV.QrMoviVAfterScroll(DataSet: TDataSet);
var
  DataExp: TDateTime;
  Stat1: Integer;
begin
  ReopenMovim(0);
  ReopenMoviVK(0);
  ReopenStatusV(0);
  ReopenPagtos;
  BtUltimo.Enabled := ReopenHist(QrMoviVCliente.Value);
  //
  DBText3.Font.Color := QrMoviVStatCor.Value;
  DBText2.Font.Color := QrMoviVStatCor.Value;
  //
  Stat1 := Dmod.QrControleVPStat1.Value;
  //
  Altera1.Enabled       := QrMoviVStatAtual.Value = Stat1;
  IncluiPagtos1.Enabled := QrMoviVStatAtual.Value = Stat1;
  BtRast.Visible        := (QrMoviVStatAtual.Value = Dmod.QrControleVPStat3.Value) or
    (QrMoviVStatAtual.Value = Dmod.QrControleVPStat4.Value);
  //
  DataExp := Dmod.VerificaDataDeExpiracaoDoCurso(QrMoviVCliente.Value);
  LaDataExp.Caption := Geral.FDT(DataExp, 2);
end;

procedure TFmMoviV.QrMoviVBeforeClose(DataSet: TDataSet);
begin
  QrMovim.Close;
  QrMoviVK.Close;
  QrMoviVStat.Close;
  QrHist.Close;
  QrPagtos.Close;
end;

procedure TFmMoviV.QrMoviVCalcFields(DataSet: TDataSet);
begin
  QrMoviVDATAREAL_TXT.Value := Geral.FDT(QrMoviVDataReal.Value, 2);
  QrMoviVPOSIt.Value        := QrMoviVTotal.Value * -1;
  QrMoviVSALDO.Value        := QrMoviVFRETEA.Value + QrMoviVPOSIt.Value + QrMoviVPago.Value;
  QrMoviVTOTALVDA.Value     := QrMoviVTotal.Value + QrMoviVFrete.Value - QrMoviVDescon.Value;
  //
  case QrMoviVUserCad.Value of
    -2: QrMoviVNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrMoviVNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrMoviVNOMECAD2.Value := 'N�o definido';
    else QrMoviVNOMECAD2.Value := QrMoviVNOMECAD.Value;
  end;
  case QrMoviVUserAlt.Value of
    -2: QrMoviVNOMEALT2.Value := 'BOSS [Administrador]';
    -1: QrMoviVNOMEALT2.Value := 'MASTER [Admin.DERMATEK]';
     0: QrMoviVNOMEALT2.Value := '- - -';
    else QrMoviVNOMEALT2.Value := QrMoviVNOMEALT.Value;
  end;
end;

procedure TFmMoviV.QrMoviVKAfterScroll(DataSet: TDataSet);
begin
  ReopenMoviVKIts;
  FmPrincipal.CarregaFotoKit(Image3, QrMoviVKNOMEFOTO.Value);
  if FmMoviVEdit <> nil then
    FmPrincipal.CarregaFoto(FmMoviVEdit.Image3, QrMoviVKNOMEFOTO.Value);
end;

procedure TFmMoviV.QrMoviVKBeforeClose(DataSet: TDataSet);
begin
  QrMoviVKIts.Close;
end;

procedure TFmMoviV.QrMoviVStatCalcFields(DataSet: TDataSet);
begin
  QrMoviVStatSTATUS_STR.Value := Geral.FDT(QrMoviVStatDiaHora.Value, 107) + ' - ' + QrMoviVStatNOMESTATUS.Value;
end;

procedure TFmMoviV.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviV.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
end;

procedure TFmMoviV.FormShow(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmMoviV.frxFutNegGetValue(const VarName: string; var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_CODMOVIV') = 0 then Value := QrMoviVCodigo.Value;
end;

procedure TFmMoviV.frxPedidosVGetValue(const VarName: string;
  var Value: Variant);
var
  Caminho: String;
  Existe: Boolean;
begin
  Caminho := Dmod.QrControleLogoMoviV.Value;
  if (Length(Caminho) > 0) and (FileExists(Caminho) = True) then
    Existe := True
  else
    Existe := False;
  if AnsiCompareText(VarName, 'VAR_IMGEXISTE') = 0 then
    Value := Existe
  else if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
    Value := dmkPF.ParValueCodTxt('Cliente: ', QrMoviVNOMEENTI.Value, QrMoviVCliente.Value)
  else if AnsiCompareText(VarName, 'VAR_IMG') = 0 then
    Value := Caminho
  else if AnsiCompareText(VarName, 'VAR_TOTPED') = 0 then
    Value := QrTotMovimTOTITENS.Value + QrTotMoviVKTOTFRETE.Value +
      QrTotMoviVKItsTOTKITITS.Value
  else if AnsiCompareText(VarName, 'VAR_TOTVDA') = 0 then
    Value := QrTotMovimTOTITENS.Value + QrTotMoviVKTOTFRETE.Value +
      QrTotMoviVKItsTOTKITITS.Value + QrMoviVFrete.Value;
end;

procedure TFmMoviV.ImprimeVenda(Imprime: Boolean);
begin
  if not Imprime then
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  ReopenCli(QrMoviVCliente.Value);
  //
  ReopenTOTAIS;
  //
  MyObjects.frxMostra(frxPedidosV, 'Pedidos');
  FFoto := True;
end;

procedure TFmMoviV.Inclui1Click(Sender: TObject);
begin
  FmPrincipal.CadastroVendasEdit(0, 0, FCliente, stIns);
end;

procedure TFmMoviV.IncluiPagtos1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  if (FmMoviV.QrMoviV.State <> dsBrowse) or (FmMoviV.QrMoviV.RecordCount = 0) then Exit;
  if DBCheck.CriaFm(TFmMoviVPagto, FmMoviVPagto, afmoNegarComAviso) then
  begin
    FmMoviVPagto.ShowModal;
    FmMoviVPagto.Destroy;
    //
    Codigo   := FmMoviV.QrMoviVCodigo.Value;
    Controle := FmMoviV.QrMoviVControle.Value;
    if FPagto then
    begin
      AtualizaTotVenda(Codigo, Controle);      
      AtualizaStatus(Codigo, Controle, 0, 2);
    end;
    LocCod(Codigo, Codigo);
  end;
end;

function TFmMoviV.AtualizaStatus(Codigo, Controle, FControle, Status: Integer): Integer;
var
  SControle, Stat, StatCor: Integer;
  DataHora: TDateTime;
begin
  Result  := 0;
  Stat    := 0;
  StatCor := 0;
  //
  case Status of
    1:
    begin
      Stat    := Dmod.QrControleVPStat1.Value;
      StatCor := Dmod.QrControleVPStat1Cor.Value;
    end;
    2:
    begin
      Stat    := Dmod.QrControleVPStat2.Value;
      StatCor := Dmod.QrControleVPStat2Cor.Value;
    end;
    3:
    begin
      Stat    := Dmod.QrControleVPStat3.Value;
      StatCor := Dmod.QrControleVPStat3Cor.Value;
    end;
    4:
    begin
      Stat    := Dmod.QrControleVPStat4.Value;
      StatCor := Dmod.QrControleVPStat4Cor.Value;
    end;
  end;
  //
  DataHora  := DModG.ObtemAgora();
  SControle := UMyMod.BuscaEmLivreY_Def('movivstat', 'Controle', stIns, FControle);
  //
  //Atualiza os antigos
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE movivstat SET Ultimo=0 WHERE Codigo=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'movivstat', False,
  [
    'StatusV', 'Cor', 'DiaHora', 'Ultimo', 'Codigo'
  ], ['Controle'],
  [
    Stat, StatCor, DataHora, 1, Codigo
  ], [SControle]) then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE moviv SET StatAtual=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsInteger := Stat;
    Dmod.QrUpd.Params[1].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    Result := StatCor;
  end;
end;

procedure TFmMoviV.AtualizaTotVenda(Codigo, Controle: Integer);
var
  Total, Pago: Double;
begin
  //Calcula o total dos produtos
  QrTotProd.Close;
  QrTotProd.Params[0].AsInteger := Controle;
  QrTotProd.Open;
  //
  //Calcula total dos kits
  QrTotKit.Close;
  QrTotKit.Params[0].AsInteger := Codigo;
  QrTotKit.Open;
  //
  //Calcula total pagto
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotPagtos, Dmod.MyDB, [
    'SELECT SUM(Credito) Credito ',
    'FROM ' + FTabLctALS,
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0510),
    'AND FatNum=' + Geral.FF0(Controle),
    '']);
  Total := QrTotProdTotal.Value + QrTotKitTotal.Value;
  Pago  := QrTotPagtosCredito.Value;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'moviv', False,
    ['Total', 'Pago'], ['Codigo'], [Total, Pago], [Codigo]) then
  begin
    FmMoviV.LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMoviV.ReopenPagtos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
    'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, ',
    'la.Agencia, la.FatID, la.ContaCorrente, la.Documento, ',
    'la.Descricao, la.FatParcela, la.FatNum, ca.Nome NOMECARTEIRA, ',
    'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ca.Agencia1, ca.Conta1, ',
    'ca.TipoDoc, la.Controle, ca.Tipo CARTEIRATIPO, la.Compensado ',
    'FROM ' + FTabLctALS + ' la ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0510),
    'AND FatNum=' + Geral.FF0(QrMoviVControle.Value),
    'ORDER BY la.FatParcela, la.Vencimento ',
    '']);
end;

end.




