unit GradeK;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  ComCtrls, Grids, DBGrids, Menus, Variants, dmkPermissoes, dmkEdit, dmkLabel,
  dmkCheckBox, dmkDBEdit, dmkDBLookupComboBox, dmkEditCB, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, UnDmkProcFunc, UnDmkEnums;

type
  TFmGradeK = class(TForm)
    PainelDados: TPanel;
    DsGradeK: TDataSource;
    QrGradeK: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PnControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtItens: TBitBtn;
    BtCompra: TBitBtn;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    PMCompra: TPopupMenu;
    Incluinovacompra1: TMenuItem;
    Alteracompraatual1: TMenuItem;
    Excluicompraatual1: TMenuItem;
    PMItens: TPopupMenu;
    Incluiitensnacompraatual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    PnItens: TPanel;
    Panel2: TPanel;
    BtConfItem: TBitBtn;
    Panel1: TPanel;
    Label7: TLabel;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    DBGrid2: TDBGrid;
    GradeA: TStringGrid;
    Panel4: TPanel;
    Label13: TLabel;
    EdGrade: TdmkEditCB;
    Label14: TLabel;
    EdCorCod: TEdit;
    EdCorNom: TEdit;
    Label15: TLabel;
    EdTamCod: TEdit;
    EdTamNom: TEdit;
    Label16: TLabel;
    QrGrades: TmySQLQuery;
    DsGrades: TDataSource;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    CBGrade: TdmkDBLookupComboBox;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    GradeC: TStringGrid;
    LaStatus: TLabel;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    DBEdit6: TDBEdit;
    QrGradesTamsAlterWeb: TSmallintField;
    QrGradesTamsAtivo: TSmallintField;
    QrGradesCorsAlterWeb: TSmallintField;
    QrGradesCorsAtivo: TSmallintField;
    StaticText2: TStaticText;
    SpeedButton6: TSpeedButton;
    dmkPermissoes1: TdmkPermissoes;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label4: TLabel;
    dmkEdNome: TdmkEdit;
    Panel7: TPanel;
    BtDesiste: TBitBtn;
    LaTipo: TdmkLabel;
    QrGradeKCodigo: TIntegerField;
    QrGradeKNome: TWideStringField;
    dmkCkAtivo: TdmkCheckBox;
    QrGradeKAtivo: TSmallintField;
    EdCodigo: TdmkEdit;
    CkAtivo: TDBCheckBox;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DsGradeKIts: TDataSource;
    QrGradeKIts: TmySQLQuery;
    QrProdutos: TmySQLQuery;
    QrProdutosTam: TIntegerField;
    QrProdutosCor: TIntegerField;
    QrProdutosAtivo: TSmallintField;
    QrGradeKItsNOMEGRA: TWideStringField;
    QrGradeKItsNOMETAM: TWideStringField;
    QrGradeKItsNOMECOR: TWideStringField;
    QrGradeKItsCodigo: TIntegerField;
    QrGradeKItsGrade: TIntegerField;
    QrGradeKItsCor: TIntegerField;
    QrGradeKItsTam: TIntegerField;
    QrGradeKItsQtd: TFloatField;
    QrGradeKItsLk: TIntegerField;
    QrGradeKItsDataCad: TDateField;
    QrGradeKItsDataAlt: TDateField;
    QrGradeKItsUserCad: TIntegerField;
    QrGradeKItsUserAlt: TIntegerField;
    QrGradeKItsAlterWeb: TSmallintField;
    QrGradeKItsAtivo: TSmallintField;
    QrGradeKItsControle: TIntegerField;
    QrProdutosControle: TIntegerField;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    QrGradeKFrete: TFloatField;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    EdFrete: TdmkEdit;
    EdQtd: TdmkEdit;
    QrGradesCodUsu: TIntegerField;
    BtFotos: TBitBtn;
    PnDItens: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    StaticText1: TStaticText;
    GradeItens: TDBGrid;
    QrGradeKLk: TIntegerField;
    QrGradeKDataCad: TDateField;
    QrGradeKDataAlt: TDateField;
    QrGradeKUserCad: TIntegerField;
    QrGradeKUserAlt: TIntegerField;
    QrGradeKAlterWeb: TSmallintField;
    IdHTTP1: TIdHTTP;
    QrGradeKFoto: TIntegerField;
    QrGradeKNOMEFOTO: TWideStringField;
    Panel8: TPanel;
    Image2: TImage;
    StaticText9: TStaticText;
    Splitter1: TSplitter;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGradeKAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrGradeKAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGradeKBeforeOpen(DataSet: TDataSet);
    procedure BtCompraClick(Sender: TObject);
    procedure Incluinovacompra1Click(Sender: TObject);
    procedure Alteracompraatual1Click(Sender: TObject);
    procedure Excluicompraatual1Click(Sender: TObject);
    procedure Incluiitensnacompraatual1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtConfItemClick(Sender: TObject);
    procedure EdGradeChange(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeAClick(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure PMCompraPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure EdQtdEnter(Sender: TObject);
    procedure BtFotosClick(Sender: TObject);
    procedure QrGradeKBeforeClose(DataSet: TDataSet);
  private
    FDataReal: TDateTime;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenGradeKIts(Controle: Integer);
    procedure ReopenGradesTams(Grade: Integer);
    procedure ReopenGradesCors(Grade: Integer);
    procedure ConfigGrades;
    procedure AtualizaGradeA(Grade: Integer);
    procedure LimpaEdits;
    procedure ReopenProdutos(Grade: Integer);
    procedure CarregaFoto;
  public
    { Public declarations }
    FSeq: Integer;
    FQtd, FPrc, FVal: Double;
  end;

var
  FmGradeK: TFmGradeK;
const
  FFormatFloat = '00000';

implementation

uses Module, MyVCLSkin, UnInternalConsts3, MyDBCheck, Entidades, GraGru,
  ModuleProd, Principal, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGradeK.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGradeK.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGradeKCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGradeK.DefParams;
begin
  VAR_GOTOTABELA := 'gradek';
  VAR_GOTOMYSQLTABLE := QrGradeK;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('');
  VAR_SQLx.Add('SELECT grk.*, fot.Nome NOMEFOTO');
  VAR_SQLx.Add('FROM gradek grk');
  VAR_SQLx.Add('LEFT JOIN fotos fot ON fot.Codigo = grk.Foto');
  VAR_SQLx.Add('WHERE grk.Codigo > 0');
  VAR_SQLx.Add('AND grk.Ativo = 1');
  //
  VAR_SQL1.Add('AND grk.Codigo=:P0');
  //
  VAR_SQLa.Add('');//AND Nome Like :P0');
  //
end;

procedure TFmGradeK.MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PnControle.Visible     := True;
      //
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnItens.Visible        := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PnControle.Visible     := False;
      if Status = CO_INCLUSAO then
      begin
        FDataReal              := 0;
        EdCodigo.Text          := '';
        dmkEdNome.ValueVariant := '';
        EdFrete.ValueVariant   := 0;
        dmkCkAtivo.Checked     := True;
      end else begin
        EdCodigo.Text          := IntToStr(QrGradeKCodigo.Value);
        dmkEdNome.ValueVariant := QrGradeKNome.Value;
        EdFrete.ValueVariant   := QrGradeKFrete.Value;
        dmkCkAtivo.Checked     := MLAGeral.IntToBool(QrGradeKAtivo.Value);
      end;
      dmkEdNome.SetFocus;
    end;
    2:
    begin
      if (QrGradeK.State <> dsBrowse) or (QrGradeK.RecordCount = 0) then Exit;
      PnItens.Visible        := True;
      PainelDados.Visible    := False;
      PnControle.Visible     := False;
      if Status = CO_INCLUSAO then
      begin

      end else begin

      end;
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmGradeK.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGradeK.QueryPrincipalAfterOpen;
begin

end;

procedure TFmGradeK.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGradeK.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGradeK.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGradeK.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGradeK.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGradeK.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGradeKCodigo.Value;
  Close;
end;

procedure TFmGradeK.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Frete: Double;
  Nome: String;
begin
  Nome    := dmkEdNome.ValueVariant;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    dmkEdNome.SetFocus;
    Exit;
  end;
  Frete := EdFrete.ValueVariant;
  if Frete = 0 then
  begin
    Application.MessageBox('Defina o valor do frete.', 'Falta de informa��es', MB_OK+MB_ICONWARNING);
    EdFrete.ValueVariant;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('gradek', 'Codigo', LaTipo.SQLType,
    QrGradeKCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmGradeK, PainelEdita,
    'gradek', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, true) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmGradeK.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'gradeg', Codigo);
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'gradeg', 'Codigo');
end;

procedure TFmGradeK.BtFotosClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrGradeKCodigo.Value;
  //
  FmPrincipal.MostraGraGruSel(0, QrGradeKCodigo.Value, 1);
  //
  LocCod(Codigo, Codigo);
  CarregaFoto;
end;

procedure TFmGradeK.FormCreate(Sender: TObject);
var
  Proxy: Integer;
begin
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  //
  FSeq                         := 0;
  FDataReal                    := 0;
  PageControl1.ActivePageIndex := 0;
  PainelEdita.Align            := alClient;
  PainelDados.Align            := alClient;
  PainelEdit.Align             := alClient;
  PnDItens.Align               := alClient;
  GradeA.Align                 := alClient;
  GradeA.ColWidths[0]          := 100;
  FQtd                         := 0;
  FPrc                         := 0;
  FVal                         := 0;
  CriaOForm;
  QrGrades.Open;
end;

procedure TFmGradeK.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGradeKCodigo.Value,LaRegistro.Caption);
end;

procedure TFmGradeK.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGradeK.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrGradeKCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmGradeK.QrGradeKAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGradeK.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'gradek', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmGradeK.QrGradeKAfterScroll(DataSet: TDataSet);
begin
  ReopenGradeKIts(0);
  CarregaFoto;
end;

procedure TFmGradeK.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGradeKCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'gradek', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGradeK.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmGradeK.QrGradeKBeforeClose(DataSet: TDataSet);
begin
  QrGradeKIts.Close;
end;

procedure TFmGradeK.QrGradeKBeforeOpen(DataSet: TDataSet);
begin
  QrGradeKCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGradeK.ReopenGradeKIts(Controle: Integer);
begin
  QrGradeKIts.Close;
  QrGradeKIts.Params[0].AsInteger := QrGradeKCodigo.Value;
  QrGradeKIts.Open;
  //
  if Controle > 0 then QrGradeKIts.Locate('Controle', Controle, []);
end;

procedure TFmGradeK.BtCompraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCompra, BtCompra);
end;

procedure TFmGradeK.Incluinovacompra1Click(Sender: TObject);
begin
//  MostraEdicao(1, CO_INCLUSAO, 0);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrGradeK, [PainelDados],
  [PainelEdita], dmkEdNome, LaTipo, 'gradek');
end;

procedure TFmGradeK.Alteracompraatual1Click(Sender: TObject);
{var
  GradeK : Integer;}
begin
  if QrGradeKCodigo.Value > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrGradeK, [PainelDados],
    [PainelEdita], dmkEdNome, LaTipo, 'gradek');
  end;
{  GradeK := QrGradeKCodigo.Value;
  if not UMyMod.SelLockY(GradeK, Dmod.MyDB, 'gradek', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GradeK, Dmod.MyDB, 'gradek', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;}
end;

procedure TFmGradeK.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmGradeK.Incluiitensnacompraatual1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmGradeK.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmGradeK.EdGradeChange(Sender: TObject);
begin
  ConfigGrades;
  LimpaEdits;
end;

procedure TFmGradeK.EdQtdEnter(Sender: TObject);
begin
  FQtd := Geral.DMV(EdQtd.Text);
end;

procedure TFmGradeK.ReopenGradesCors(Grade: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Grade;
  QrGradesCors.Open;
end;

procedure TFmGradeK.ReopenGradesTams(Grade: Integer);
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Grade;
  QrGradesTams.Open;
end;

procedure TFmGradeK.CarregaFoto;
var
  Foto: String;
  ImageMem : TMemoryStream;
  ImageJpg: TJPEGImage;
begin
  Foto := QrGradeKNOMEFOTO.Value;
  //
  if Length(Foto) > 0 then
  begin
    if (Pos('http://', LowerCase(Foto)) > 0) or (Pos('www.', LowerCase(Foto)) > 0) then
    begin
      ImageMem := TMemoryStream.Create;
      ImageJpg := TJPEGImage.Create;
      try
        try
          if (Pos('http://', LowerCase(Foto)) > 0) then
            IdHTTP1.Get(Foto, ImageMem)
          else
            IdHTTP1.Get('http://' + Foto, ImageMem);
        except on e: EIdHTTPProtocolException do
          begin
            if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
            begin
              // N�o achou!
              Exit;
            end;
          end;
        end;
        ImageMem.Position := 0;
        ImageJpg.LoadFromStream(ImageMem);
        Image2.Picture.Assign(ImageJpg);
      finally
        ImageJpg.Free;
        ImageMem.Free;
      end;
    end else
    begin
      if FmPrincipal.CarregaImg(Foto) then
        Image2.Picture.LoadFromFile(Foto)
      else
        Image2.Picture := nil;
    end;
  end else
    Image2.Picture := nil;
end;

procedure TFmGradeK.ConfigGrades;
var
  c, l{, ci, li}, g: Integer;
begin
  for c := 0 to GradeA.ColCount - 1 do
    for l := 0 to GradeA.ColCount - 1 do
      GradeA.Cells[c, l] := '';
  g := Geral.IMV(EdGrade.Text);
  if g > 0 then
  begin
    // c�digo real
    g := QrGradesCodigo.Value;
    ReopenGradesCors(g);
    ReopenGradesTams(g);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    GradeA.ColCount  := c;
    GradeA.RowCount  := l;
    GradeA.FixedCols := 1;
    GradeA.FixedRows := 1;
    //
    GradeC.ColCount  := c;
    GradeC.RowCount  := l;
    //
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      GradeA.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      GradeC.Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      GradeA.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      GradeC.Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value);
      QrGradesTams.Next;
    end;
    AtualizaGradeA(g);
  end else begin
    GradeA.ColCount := 1;
    GradeA.RowCount := 1;
  end;
end;

procedure TFmGradeK.AtualizaGradeA(Grade: Integer);
var
  c, l: Integer;
begin
  ReopenProdutos(Grade);
  QrProdutos.First;
  while not QrProdutos.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutosTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutosCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutosControle.Value > 0 then
          GradeA.Cells[c, l] := '2'
        else
          GradeA.Cells[c, l] := '1';
      end;
    end;
    QrProdutos.Next;
  end;
end;

procedure TFmGradeK.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol <> 0) and (ARow <> 0) then
    MeuVCLSkin.DrawGrid3(GradeA, Rect, 1, Geral.IMV(GradeA.Cells[ACol, ARow]));
end;

procedure TFmGradeK.GradeAClick(Sender: TObject);
var
  Status: Integer;
begin
  if GradeA.Cells[GradeA.Col, GradeA.Row] <> '' then
  begin
    EdTamCod.Text := GradeC.Cells[GradeA.Col, 0];
    EdCorCod.Text := GradeC.Cells[0, GradeA.Row];
    //
    EdTamNom.Text := GradeA.Cells[GradeA.Col, 0];
    EdCorNom.Text := GradeA.Cells[0, GradeA.Row];
  end else LimpaEdits;
  Status := Geral.IMV(GradeA.Cells[GradeA.Col, GradeA.Row]);
  case Status of
    0:   LaStatus.Caption := CO_TRAVADO;
    1: LaStatus.Caption := CO_INCLUSAO;
    else LaStatus.Caption := CO_ALTERACAO;
  end;
  BtConfItem.Enabled := Geral.IntToBool_0(Status);
  EdQtd.SetFocus;
end;

procedure TFmGradeK.LimpaEdits;
begin
  EdTamCod.Text := '';
  EdCorCod.Text := '';
  //
  EdTamNom.Text := '';
  EdCorNom.Text := '';
  //
end;

procedure TFmGradeK.BtConfItemClick(Sender: TObject);
var
  Codigo, Controle, Grade, Cor, Tam: Integer;
  Qtd: Double;
begin
  if LaStatus.Caption = CO_TRAVADO then Exit;
  Qtd := Geral.DMV(EdQtd.Text);
  Codigo := QrGradeKCodigo.Value;
  Grade := QrGradesCodigo.Value;
  Cor := Geral.IMV(EdCorCod.Text);
  Tam := Geral.IMV(EdTamCod.Text);
  //
  if Grade = 0 then
  begin
    Application.MessageBox('Selecione a mercadoria.',
      'Aviso', MB_OK+MB_ICONWARNING);
    EdGrade.SetFocus;
    Exit;
  end;
  // C�digo real
  Grade := QrGradesCodigo.Value;
  //
  if Qtd = 0 then
  begin
    Application.MessageBox('A quantidade deve ser superior a 0.',
      'Aviso', MB_OK+MB_ICONWARNING);
    EdQtd.SetFocus;
    Exit;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  if LaStatus.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO gradekits SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'gradekits', 'gradekits', 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE gradekits SET ');
    Controle := QrGradeKItsControle.Value;
  end;
  Dmod.QrUpd.SQL.Add('Grade=:P0, Cor=:P1, Tam=:P2, Qtd=:P3, ');
  //
  if LaStatus.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd ')
  else Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  //
  Dmod.QrUpd.Params[00].AsInteger := Grade;
  Dmod.QrUpd.Params[01].AsInteger := Cor;
  Dmod.QrUpd.Params[02].AsInteger := Tam;
  Dmod.QrUpd.Params[03].AsFloat   := Qtd;
  //
  Dmod.QrUpd.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[06].AsInteger := Codigo;
  Dmod.QrUpd.Params[07].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  ReopenGradeKIts(Controle);
  AtualizaGradeA(Grade);
  LaStatus.Caption := CO_TRAVADO;
  EdQtd.Text := '';
  EdGrade.SetFocus;
  BtConfItem.Enabled := False;
end;

procedure TFmGradeK.Excluiitematual1Click(Sender: TObject);
begin
  if (QrGradeK.State <> dsBrowse) or (QrGradeK.RecordCount = 0) then Exit;
  if Application.MessageBox('Confirma a exclus�o do item selecionado?',
  'Confirma��o de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM gradekits ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrGradeKItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrGradeKIts.Close;
    QrGradeKIts.Open;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGradeK.Excluicompraatual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrGradeKIts.RecordCount > 0 then
  begin
    Application.MessageBox('Exclus�o cancelada. Existem �tens cadastrados neste kit.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox('Confirma a exclus�o do kit?',
  'Confirma��o de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Codigo := QrGradeKCodigo.Value;
    // 
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM gradek ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    QrGradeK.Close;
    QrGradeK.Open;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGradeK.PMCompraPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := not ((QrGradeK.State <> dsBrowse) or (QrGradeK.RecordCount = 0));
  Alteracompraatual1.Enabled := Enab;
  Excluicompraatual1.Enabled := Enab;
end;

procedure TFmGradeK.PMItensPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := not ((QrGradeK.State <> dsBrowse) or (QrGradeK.RecordCount = 0));
  Incluiitensnacompraatual1.Enabled := Enab;
end;

procedure TFmGradeK.SpeedButton6Click(Sender: TObject);
begin
  VAR_COD := 0;
  if DBCheck.CriaFm(TFmGraGru, FmGraGru, afmoNegarComAviso) then
  begin
    FmGraGru.ShowModal;
    FmGraGru.Destroy;
    //
    if VAR_COD <> 0 then
    begin
      QrGrades.Close;
      QrGrades.Open;
      //
      if QrGrades.Locate('Codigo', VAR_COD, []) then
      begin
        EdGrade.Text := IntToStr(QrGradesCodUsu.Value);
        CBGrade.KeyValue := QrGradesCodUsu.Value;
      end;
    end;
  end;
end;

procedure TFmGradeK.ReopenProdutos(Grade: Integer);
begin
  QrProdutos.Close;
  QrProdutos.Params[00].AsInteger := QrGradeKCodigo.Value;
  QrProdutos.Params[01].AsInteger := Grade;
  QrProdutos.Open;
end;

end.




