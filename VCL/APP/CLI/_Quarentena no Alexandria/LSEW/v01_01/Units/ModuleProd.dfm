object DmProd: TDmProd
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 500
  Width = 757
  object QrLocta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta'
      'FROM movim'
      'WHERE Controle=:P0'
      'AND Grade=:P1'
      'AND Cor=:P2'
      'AND Tam=:P3'
      'AND Kit=:P4')
    Left = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrLoctaConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.Controle, pro.Tam, pro.Cor, pro.Ativo, mom.Conta'
      'FROM produtos pro'
      'LEFT JOIN tamanhos tam ON tam.Codigo=pro.Tam'
      'LEFT JOIN cores    cor ON cor.Codigo=pro.Cor'
      'LEFT JOIN movim    mom ON mom.Grade=pro.Codigo '
      '   AND mom.Tam=pro.Tam '
      '   AND mom.Cor=pro.Cor'
      '   AND mom.Controle=:P0'
      '   AND mom.SubCtrl=:P1'
      '   AND mom.SubCta=:P2'
      'WHERE pro.Ativo=1'
      'AND pro.Codigo=:P2'
      '/*Para poder ordenar!!!*/'
      'ORDER BY tam.CodUsu, tam.Nome, cor.Nome')
    Left = 28
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrProdutosTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'produtos.Tam'
      Required = True
    end
    object QrProdutosCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'produtos.Cor'
      Required = True
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'produtos.Ativo'
      Required = True
    end
    object QrProdutosConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
      Required = True
    end
    object QrProdutosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'produtos.Controle'
      Required = True
    end
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY tam.CodUsu, NOMETAM')
    Left = 28
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY NOMECOR')
    Left = 28
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.EstqQ, pro.EstqV'
      'FROM produtos pro'
      'WHERE pro.Codigo= :P0'
      'AND pro.Cor= :P1'
      'AND pro.Tam= :P2')
    Left = 28
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqEstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
    end
    object QrEstqEstqV: TFloatField
      FieldName = 'EstqV'
      Required = True
    end
  end
  object QrPeriodoBal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 28
    Top = 240
    object QrPeriodoBalPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrCustoProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (EstqV / EstqQ) Custo'
      'FROM produtos'
      'WHERE Codigo=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2')
    Left = 28
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCustoProdCusto: TFloatField
      FieldName = 'Custo'
    end
  end
  object QrComisPrd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eci.ComisTip, eci.ComisQtd '
      'FROM equicomits eci'
      'LEFT JOIN produtos prd ON prd.Codigo=eci.Produto'
      'WHERE eci.Codigo=:P0'
      'AND prd.Codigo=:P1'
      'AND prd.Cor=:P2'
      'AND prd.Tam=:P3')
    Left = 28
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object QrGraGruVal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Cor, prd.Tam, ggv.CustoPreco'
      'FROM gragruval ggv'#13
      'LEFT JOIN produtos prd ON prd.Controle=ggv.GraGruX'
      'WHERE ggv.Codigo=:P0'
      'AND ggv.GraCusPrc=:P1')
    Left = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGruValCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrGraGruValTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      Required = True
    end
  end
  object QrProdutos2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Cor, prd.Tam, '
      'prd.Controle, prd.Ativo, prd.EstqQ'
      'FROM produtos prd'
      'LEFT JOIN grades gra ON prd.Codigo=gra.Codigo'
      'WHERE prd.Codigo=:P0')
    Left = 112
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutos2Cor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrProdutos2Tam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrProdutos2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrProdutos2Ativo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutos2EstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
    end
  end
  object QrLocProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, GraCusPrc, CustoPreco'
      'FROM gragruval'
      'WHERE Codigo=:P0'
      'AND GraCusPrc=:P1'
      'AND GraGruX=:P2')
    Left = 28
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocProdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocProdGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrLocProdCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
  object QrPtosPedPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Cor, prd.Tam, SUM(psm.Quanti) Quanti,'
      'SUM(psm.PrecoR) / SUM(psm.Quanti) PrecoR,'
      'SUM(psm.PrecoO) / SUM(psm.Quanti) PrecoO,'
      'SUM(psm.DescoP) / SUM(psm.Quanti) DescoP'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'WHERE psm.CodInn=:P0'
      'GROUP BY prd.Cor, prd.Tam')
    Left = 112
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPtosPedPrcCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrPtosPedPrcTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrPtosPedPrcQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrPtosPedPrcPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrPtosPedPrcDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrPtosPedPrcPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
  end
  object QrPedItsAnul: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrPedItsAnulCalcFields
    SQL.Strings = (
      'SELECT pia.Ativo, psm.IDCtrl, psm.DataIns, psm.Status,'
      'psm.Quanti, psm.PrecoR, grs.Nome NO_Produto, psm.Produto,'
      'cor.Nome NO_Cor, tam.CodUsu CU_Tam, tam.Nome NO_Tam'
      'FROM peditsanul pia'
      'LEFT JOIN lesew.ptosstqmov psm ON psm.IDCtrl=pia.IDCtrl'
      'LEFT JOIN lesew.produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN lesew.grades grs ON grs.Codigo=prd.codigo'
      'LEFT JOIN lesew.cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN lesew.tamanhos tam ON tam.Codigo=prd.Tam'
      'ORDER BY NO_Produto, NO_Cor, CU_Tam, NO_Tam'
      '')
    Left = 112
    Top = 144
    object QrPedItsAnulAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrPedItsAnulIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      EditFormat = '000'
    end
    object QrPedItsAnulDataIns: TDateTimeField
      FieldName = 'DataIns'
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrPedItsAnulStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrPedItsAnulQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrPedItsAnulPrecoR: TFloatField
      FieldName = 'PrecoR'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPedItsAnulNO_Produto: TWideStringField
      FieldName = 'NO_Produto'
      Size = 30
    end
    object QrPedItsAnulNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 100
    end
    object QrPedItsAnulCU_Tam: TIntegerField
      FieldName = 'CU_Tam'
    end
    object QrPedItsAnulNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 100
    end
    object QrPedItsAnulNO_STATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_STATUS'
      Size = 100
      Calculated = True
    end
    object QrPedItsAnulProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
  end
  object DsPedItsAnul: TDataSource
    DataSet = QrPedItsAnul
    Left = 112
    Top = 192
  end
  object QrPediItsMoM: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT pia.Ativo, prd.Codigo Grade, prd.Tam, prd.Cor, '
      'mom.IDCtrl ID_Loc, pia.IDCtrl ID_Net '
      'FROM peditsanul pia'
      'LEFT JOIN lesew.movim mom ON mom.IDCtrl=pia.IDCtrl '
      '  AND mom.Motivo in (15,25,26)'
      'LEFT JOIN lesew.ptosstqmov psm ON psm.IDCtrl=pia.IDCtrl'
      'LEFT JOIN lesew.produtos prd ON prd.Controle=psm.Produto'
      'WHERE pia.Ativo=1')
    Left = 112
    Top = 240
    object QrPediItsMoMAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPediItsMoMGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrPediItsMoMTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrPediItsMoMCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrPediItsMoMID_Loc: TIntegerField
      FieldName = 'ID_Loc'
    end
    object QrPediItsMoMID_Net: TIntegerField
      FieldName = 'ID_Net'
      Required = True
    end
  end
  object QrDelMomPto: TmySQLQuery
    Database = Dmod.MyDB
    Left = 112
    Top = 288
  end
  object QrSelMomPto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Conta, mom.Grade, mom.Tam, mom.Cor '
      'FROM movim  mom'
      'WHERE mom.Motivo in (15,25,26)'
      'AND mom.IDCtrl>0'
      '')
    Left = 112
    Top = 336
    object QrSelMomPtoConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrSelMomPtoGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrSelMomPtoTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrSelMomPtoCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
  end
  object QrMoviV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA, moc.* '
      'FROM moviv moc'
      'LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente'
      'WHERE moc.Codigo > 0')
    Left = 648
    Top = 9
    object QrMoviVNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrMoviVCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMoviVControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMoviVDataPedi: TDateField
      FieldName = 'DataPedi'
    end
    object QrMoviVDataReal: TDateField
      FieldName = 'DataReal'
    end
    object QrMoviVCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrMoviVLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMoviVDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMoviVDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMoviVUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMoviVUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMoviVDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrMoviVSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviVTotal: TFloatField
      FieldName = 'Total'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviVAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMoviVAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMoviVGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Required = True
    end
    object QrMoviVTransportadora: TIntegerField
      FieldName = 'Transportadora'
    end
    object QrMoviVFrete: TFloatField
      FieldName = 'Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrMoviVNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrMoviVNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrMoviVNOMEALT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEALT'
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserAlt'
      Size = 100
      Lookup = True
    end
    object QrMoviVFRETEA: TFloatField
      FieldName = 'FRETEA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
  end
  object DsMoviV: TDataSource
    DataSet = QrMoviV
    Left = 676
    Top = 9
  end
end
