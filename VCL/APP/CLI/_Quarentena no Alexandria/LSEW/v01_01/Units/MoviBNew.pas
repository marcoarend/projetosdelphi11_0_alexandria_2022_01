unit MoviBNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, UnMLAGeral, DBCtrls, UnGOTOy, UnInternalConsts, UnMsgInt, StdCtrls,
  Db, (*DBTables,*) Buttons, UMySQLModule, mySQLDbTables, ComCtrls, dmkPermissoes,
  dmkGeral;

type
  TFmMoviBNew = class(TForm)
    QrLocPeriodo: TmySQLQuery;
    QrLocPeriodoPeriodo: TIntegerField;
    Panel1: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label2: TLabel;
    CBMes: TComboBox;
    Label3: TLabel;
    CBAno: TComboBox;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    QrEstq: TmySQLQuery;
    QrEstqCodigo: TIntegerField;
    QrEstqCor: TIntegerField;
    QrEstqTam: TIntegerField;
    QrEstqEstqQ: TFloatField;
    QrEstqEstqV: TFloatField;
    dmkPermissoes1: TdmkPermissoes;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMoviBNew: TFmMoviBNew;

implementation

uses Module, Principal, ModuleProd, MoviB, UnMyObjects, UnDmkEnums;

{$R *.DFM}

procedure TFmMoviBNew.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia : Word;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    Add(IntToStr(Ano-2));
    Add(IntToStr(Ano-1));
    Add(IntToStr(Ano));
    Add(IntToStr(Ano+1));
    Add(IntToStr(Ano+2));
  end;
  CBAno.ItemIndex := 2;
  CBMes.ItemIndex := (Mes - 1);
  //QrTerceiros.Open;
end;

procedure TFmMoviBNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviBNew.BtConfirmaClick(Sender: TObject);
var
  Ano, Mes, Dia : word;
  Periodo, Atual, Controle, Conta: Integer;
begin
  Ano := Geral.IMV(CBAno.Text);
  if Ano = 0 then Exit;
  Mes := CBMes.ItemIndex + 1;
  Periodo := ((Ano - 2000) * 12) + Mes;
  QrLocPeriodo.Close;
  QrLocPeriodo.Params[0].AsInteger := Periodo;
  QrLocPeriodo.Open;
  if GOTOy.Registros(QrLocPeriodo) > 0 then
  begin
    Geral.MB_Aviso('Este per�odo de balan�o j� existe!');
    Exit;
  end;
  // A - Impede criar do m�s futuro (pela data do sistema)
  DecodeDate(Date, Ano, Mes, Dia);
  Atual := ((Ano - 2000) * 12) + Mes;
  if Periodo > Atual then
  begin
    Application.MessageBox('M�s inv�lido. [Futuro]', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  // fim A
  // B - Impede criar m�s anterior ao m�ximo
  if Periodo < DmProd.VerificaBalanco then
  begin
    Application.MessageBox('M�s inv�lido. J� existe m�s posterior ao desejado.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //Fim B
  Controle := Dmod.BuscaProximoMovix;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO movib SET Periodo=:P0, Controle=:P1');
  Dmod.QrUpdM.Params[00].AsFloat   := Periodo;
  Dmod.QrUpdM.Params[01].AsInteger := Controle;
  Dmod.QrUpdM.ExecSQL;
  QrLocPeriodo.Close;
  VAR_PERIODO := Periodo;
  FmMoviB.DefParams;
  FmMoviB.LocCod(FmMoviB.QrMoviBPeriodo.Value, Periodo);
  FmMoviB.MostraEdicao(True, CO_INCLUSAO, 0);
  UMyMod.UpdLockY(Periodo, Dmod.MyDB, 'movib', 'periodo');
  VAR_PERIODOBAL := MLAGeral.PrimeiroDiaDoPeriodo(
    DmProd.VerificaBalanco, dtSystem);
  // Coloca estoque inicial novo igual ao do final antigo!
  QrEstq.Open;
  if QrEstq.RecordCount > 0 then
  begin
    ProgressBar1.Position := 0;
    ProgressBar1.Max := QrEstq.RecordCount;
    ProgressBar1.Visible := True;
    Label1.Visible := True;
    Application.ProcessMessages;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO movim SET Motivo=1, '); // In�cio de balanco
    Dmod.QrUpd.SQL.Add('Controle=:P0, Conta=:P1, Grade=:P2, Cor=:P3, Tam=:P4, ');
    Dmod.QrUpd.SQL.Add('Qtd=:P5, Val=:P6, DataReal=:P7');
    while not QrEstq.Eof do
    begin
      Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle', 'movim',
        'movim', 'conta');
      Dmod.QrUpd.Params[00].AsInteger := Controle;
      Dmod.QrUpd.Params[01].AsInteger := Conta;
      Dmod.QrUpd.Params[02].AsInteger := QrEstqCodigo.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrEstqCor.Value;
      Dmod.QrUpd.Params[04].AsInteger := QrEstqTam.Value;
      Dmod.QrUpd.Params[05].AsFloat   := QrEstqEstqQ.Value;
      Dmod.QrUpd.Params[06].AsFloat   := QrEstqEstqV.Value;
      Dmod.QrUpd.Params[07].AsString  := VAR_PERIODOBAL;
      Dmod.QrUpd.ExecSQL;
      QrEstq.Next;
    end;
  end;
  Close;
end;

procedure TFmMoviBNew.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviBNew.FormPaint(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

