unit MD5Alu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkGeral, dmkPermissoes, UnDmkEnums;

type
  TFmMD5Alu = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdAluno: TdmkEditCB;
    CBAluno: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrAlunos: TmySQLQuery;
    QrAlunosCodigo: TIntegerField;
    QrAlunosNOMEALUNO: TWideStringField;
    DsAlunos: TDataSource;
    EdSerial: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdSerialG: TdmkEdit;
    EdSerialN: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdSerialGChange(Sender: TObject);
    procedure EdSerialAfterExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenAlunos;
  public
    { Public declarations }
  end;

  var
  FmMD5Alu: TFmMD5Alu;

implementation

uses Module, UMySQLModule, MD5Cab, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmMD5Alu.BtOKClick(Sender: TObject);
var
  Entidade, SerialG, SerialN: Integer;
begin
  SerialG  := Geral.IMV(EdSerialG.Text);
  SerialN  := Geral.IMV(EdSerialN.Text);
  Entidade := Geral.IMV(EdAluno.Text);
  if Dmod.SerialDuplicadoEntidade(SerialG, SerialN) then
  begin
    Geral.MB_Aviso('Inclus�o de entidade abortada! O serial ' +
      'j� est� cadastrado para a entidade n�mero ' +
      Geral.FF0(Dmod.QrDuplMD5Entidade.Value) + '!');
    EdSerial.SetFocus;
    Exit;
  end;
  if SerialG > 0 then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'md5alu', False, ['MD5Cab',
    'MD5Num'], ['Entidade'], [SerialG, SerialN], [Entidade], True) then
    begin
      FmMD5Cab.ReopenMD5Alu(Entidade);
      Close;
    end;
  end;
end;

procedure TFmMD5Alu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMD5Alu.EdSerialAfterExit(Sender: TObject);
var
  Grupo, Numero: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    EdSerialG.ValueVariant := 0;
    EdSerialN.ValueVariant := 0;
    Dmod.VerificaSenhaEntidade(EdSerial.Text, Grupo, Numero,
      FmPrincipal.Fmy_key_uEncrypt);
    EdSerialG.ValueVariant := Grupo;
    EdSerialN.ValueVariant := Numero;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMD5Alu.EdSerialGChange(Sender: TObject);
begin
  BtOK.Enabled := EdSerialG.ValueVariant > 0;  
end;

procedure TFmMD5Alu.FormActivate(Sender: TObject);
begin
  EdSerial.CharCase := ecNormal;
  MyObjects.CorIniComponente;
end;

procedure TFmMD5Alu.FormCreate(Sender: TObject);
begin
  ReopenAlunos;
end;

procedure TFmMD5Alu.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMD5Alu.ReopenAlunos;
begin
  QrAlunos.Close;
  QrAlunos.Open;
end;

end.
