object FmPtosFatBalLei: TFmPtosFatBalLei
  Left = 339
  Top = 185
  Caption = 'PTO-VENDA-008 :: Faturamento de Pontos de Vendas - por Leitura'
  ClientHeight = 494
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 794
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 682
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object CkFixo: TCheckBox
      Left = 4
      Top = 16
      Width = 125
      Height = 17
      Caption = 'Quantidade fixa.'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = CkFixoClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 794
    Height = 48
    Align = alTop
    Caption = 'Faturamento de Pontos de Vendas - por Leitura'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 792
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 794
    Height = 349
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 792
      Height = 174
      Align = alClient
      DataSource = DsEstqPto
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodUsu'
          ReadOnly = True
          Title.Caption = 'Refer'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'No_Grade'
          Title.Caption = 'Grade'
          Width = 196
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'No_Cor'
          Title.Caption = 'Cor'
          Width = 195
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'No_Tam'
          Title.Caption = 'Tamanho'
          Width = 67
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeOld'
          Title.Caption = 'Qtd antes'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeNew'
          Title.Caption = 'Qtd nova'
          Visible = True
        end>
    end
    object MeAvisos: TMemo
      Left = 1
      Top = 175
      Width = 792
      Height = 173
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      Lines.Strings = (
        'MeAvisos')
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 1
      Visible = False
    end
  end
  object PnLeitura: TPanel
    Left = 0
    Top = 48
    Width = 794
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 3
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 329
      Height = 49
      Align = alLeft
      TabOrder = 0
      object Label3: TLabel
        Left = 4
        Top = 4
        Width = 89
        Height = 13
        Caption = 'Leitura / digita'#231#227'o:'
      end
      object LaQtdeLei: TLabel
        Left = 188
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Qtde:'
      end
      object EdLeituraCodigoDeBarras: TEdit
        Left = 4
        Top = 20
        Width = 180
        Height = 21
        MaxLength = 20
        TabOrder = 0
        OnChange = EdLeituraCodigoDeBarrasChange
        OnKeyDown = EdLeituraCodigoDeBarrasKeyDown
      end
      object EdQtdLei: TdmkEdit
        Left = 188
        Top = 20
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = True
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnEnter = EdQtdLeiEnter
        OnKeyDown = EdQtdLeiKeyDown
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 232
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
    object Panel9: TPanel
      Left = 329
      Top = 0
      Width = 465
      Height = 49
      Align = alClient
      Enabled = False
      TabOrder = 1
      object Label4: TLabel
        Left = 4
        Top = 4
        Width = 86
        Height = 13
        Caption = 'Grupo de produto:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 264
        Top = 4
        Width = 19
        Height = 13
        Caption = 'Cor:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 388
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 20
        Width = 257
        Height = 21
        DataField = 'NO_GRADE'
        DataSource = DsItens
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 264
        Top = 20
        Width = 121
        Height = 21
        DataField = 'NO_COR'
        DataSource = DsItens
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 388
        Top = 20
        Width = 69
        Height = 21
        DataField = 'NO_TAM'
        DataSource = DsItens
        TabOrder = 2
      end
    end
  end
  object TbEstqPto: TmySQLTable
    Database = DModG.DBDmk
    TableName = 'estqpto'
    Left = 288
    Top = 156
    object TbEstqPtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'estqpto.CodUsu'
    end
    object TbEstqPtoGrade: TIntegerField
      FieldName = 'Grade'
      ReadOnly = True
    end
    object TbEstqPtoGraGruX: TIntegerField
      FieldName = 'GraGruX'
      ReadOnly = True
    end
    object TbEstqPtoCor: TIntegerField
      FieldName = 'Cor'
      ReadOnly = True
    end
    object TbEstqPtoTam: TIntegerField
      FieldName = 'Tam'
      ReadOnly = True
    end
    object TbEstqPtoNo_Grade: TWideStringField
      FieldName = 'No_Grade'
      ReadOnly = True
      Size = 30
    end
    object TbEstqPtoNo_Cor: TWideStringField
      FieldName = 'No_Cor'
      ReadOnly = True
      Size = 30
    end
    object TbEstqPtoNo_Tam: TWideStringField
      FieldName = 'No_Tam'
      ReadOnly = True
      Size = 10
    end
    object TbEstqPtoQtdeOld: TIntegerField
      FieldName = 'QtdeOld'
      ReadOnly = True
    end
    object TbEstqPtoQtdeNew: TIntegerField
      FieldName = 'QtdeNew'
      ReadOnly = True
    end
  end
  object DsEstqPto: TDataSource
    DataSet = TbEstqPto
    Left = 316
    Top = 156
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT grs.Nome NO_GRADE, cor.Nome NO_Cor, '
      'tam.Nome NO_Tam, mov.* '
      'FROM ptosstqmov mov'
      'LEFT JOIN produtos prd ON prd.Controle=mov.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN lesew.tamanhos tam ON tam.Codigo=prd.Tam'
      'WHERE mov.Status=50'
      'AND mov.Empresa=:P0'
      'AND mov.Produto=:P1'
      'ORDER BY mov.DataIns, mov.IDCtrl')
    Left = 344
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItensIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrItensCodInn: TIntegerField
      FieldName = 'CodInn'
    end
    object QrItensCodOut: TIntegerField
      FieldName = 'CodOut'
    end
    object QrItensTipOut: TSmallintField
      FieldName = 'TipOut'
    end
    object QrItensDataIns: TDateTimeField
      FieldName = 'DataIns'
    end
    object QrItensEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrItensProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrItensQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrItensPrecoO: TFloatField
      FieldName = 'PrecoO'
    end
    object QrItensPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItensDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItensStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrItensAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrItensAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrItensNO_GRADE: TWideStringField
      FieldName = 'NO_GRADE'
      Size = 30
    end
    object QrItensNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrItensNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 10
    end
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Quanti) Quanti, SUM(PrecoR*Quanti) Valor '
      'FROM ptosstqmov'
      'WHERE status=60'
      'AND TipOut=1'
      'AND CodOut=:P0')
    Left = 316
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrSumValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrPediPrzIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.*'
      'FROM pediprzits ppi'
      'LEFT JOIN ptosfatcad pfc ON pfc.PediPrzCab=ppi.Codigo'
      'WHERE pfc.Codigo=:P0'
      'ORDER BY ppi.Dias')
    Left = 288
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPediPrzItsPercent1: TFloatField
      FieldName = 'Percent1'
    end
  end
  object DsItens: TDataSource
    DataSet = QrItens
    Left = 372
    Top = 184
  end
  object QrItem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ptosstqmov'
      'WHERE Status=50'
      'AND Empresa=:P0'
      'AND Produto=:P1'
      'ORDER BY DataIns, IDCtrl'
      'Limit 0, :P2'
      '')
    Left = 288
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'IDCtrl'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodInn'
    end
    object IntegerField3: TIntegerField
      FieldName = 'CodOut'
    end
    object SmallintField1: TSmallintField
      FieldName = 'TipOut'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DataIns'
    end
    object IntegerField4: TIntegerField
      FieldName = 'Empresa'
    end
    object IntegerField5: TIntegerField
      FieldName = 'Produto'
    end
    object FloatField1: TFloatField
      FieldName = 'Quanti'
    end
    object FloatField2: TFloatField
      FieldName = 'PrecoO'
    end
    object FloatField3: TFloatField
      FieldName = 'PrecoR'
    end
    object FloatField4: TFloatField
      FieldName = 'DescoP'
    end
    object SmallintField2: TSmallintField
      FieldName = 'Status'
    end
    object SmallintField3: TSmallintField
      FieldName = 'AlterWeb'
    end
    object SmallintField4: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
