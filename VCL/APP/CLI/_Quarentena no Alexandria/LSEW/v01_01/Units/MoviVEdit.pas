unit MoviVEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkEditDateTimePicker, jpeg, dmkMemo, dmkGeral,
  Variants, UrlMon, Menus, Shellapi, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, UnDmkEnums;

type
  TFmMoviVEdit = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    LaTipo: TdmkLabel;
    PnTopo: TPanel;
    PnBottom: TPanel;
    Panel2: TPanel;
    BtConfPedi: TBitBtn;
    Label9: TLabel;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTI: TWideStringField;
    DsCliente: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    PnInfoCli: TPanel;
    Panel14: TPanel;
    Label2: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Panel15: TPanel;
    BtUltimo: TBitBtn;
    PnDados: TPanel;
    PCProd: TPageControl;
    TabSheet1: TTabSheet;
    Panel11: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    EdProd: TdmkEditCB;
    CBProd: TdmkDBLookupComboBox;
    EdProdQtd: TdmkEdit;
    EdProdTot: TdmkEdit;
    BtConfItem: TBitBtn;
    EdCor: TdmkEditCB;
    CBCor: TdmkDBLookupComboBox;
    EdTam: TdmkEditCB;
    CBTam: TdmkDBLookupComboBox;
    EdProdVal: TdmkEdit;
    EdEstQ: TdmkEdit;
    Panel5: TPanel;
    DBGridProd: TdmkDBGrid;
    StaticText1: TStaticText;
    Panel6: TPanel;
    Panel9: TPanel;
    Image2: TImage;
    Panel12: TPanel;
    Label32: TLabel;
    Label33: TLabel;
    DBText2: TDBText;
    Panel13: TPanel;
    DBLookupListBox2: TDBLookupListBox;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    EdKit: TdmkEditCB;
    CBKit: TdmkDBLookupComboBox;
    BtConfKit: TBitBtn;
    EdTotKit: TdmkEdit;
    EdQtdeKit: TdmkEdit;
    PBKit: TProgressBar;
    Panel8: TPanel;
    Panel10: TPanel;
    Image3: TImage;
    Panel16: TPanel;
    Label37: TLabel;
    Label38: TLabel;
    DBText1: TDBText;
    Panel17: TPanel;
    DBLookupListBox1: TDBLookupListBox;
    Panel18: TPanel;
    StaticText2: TStaticText;
    DBGridKit: TdmkDBGrid;
    EdNomeCad: TdmkEdit;
    EdDataPed: TdmkEdit;
    EdDataVenda: TdmkEdit;
    Label10: TLabel;
    Label3: TLabel;
    Label26: TLabel;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CBListaPre: TdmkDBLookupComboBox;
    EdListaPre: TdmkEditCB;
    SBCliente: TSpeedButton;
    Label84: TLabel;
    EdNomeAlt: TdmkEdit;
    Label85: TLabel;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    QrGradesCodUsu: TIntegerField;
    DsGrades: TDataSource;
    DsGradesCors: TDataSource;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesCorsCor: TIntegerField;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    DsGradesTams: TDataSource;
    QrLoc: TmySQLQuery;
    EdCodigo: TdmkEdit;
    DsGradeK: TDataSource;
    QrGradeK: TmySQLQuery;
    QrGradeKCodigo: TIntegerField;
    QrGradeKNome: TWideStringField;
    QrLoc2: TmySQLQuery;
    BtDesiste: TBitBtn;
    PnVdaBot: TPanel;
    Label21: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label22: TLabel;
    Label18: TLabel;
    LaTotal: TLabel;
    Label23: TLabel;
    EdObs: TdmkEdit;
    EdSubTotal: TdmkEdit;
    EdFrete: TdmkEdit;
    EdDesc: TdmkEdit;
    EdPago: TdmkEdit;
    Label12: TLabel;
    LaDataExp: TLabel;
    QrMoviVUpd: TmySQLQuery;
    QrTot: TmySQLQuery;
    QrTotTOTAL: TFloatField;
    QrTotTOT: TFloatField;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure BtConfPediClick(Sender: TObject);
    procedure QrGradesBeforeClose(DataSet: TDataSet);
    procedure QrGradesAfterScroll(DataSet: TDataSet);
    procedure EdProdChange(Sender: TObject);
    procedure EdCorChange(Sender: TObject);
    procedure EdTamChange(Sender: TObject);
    procedure EdProdQtdChange(Sender: TObject);
    procedure CBClienteExit(Sender: TObject);
    procedure BtConfItemClick(Sender: TObject);
    procedure CBProdExit(Sender: TObject);
    procedure DBGridProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBKitExit(Sender: TObject);
    procedure EdKitChange(Sender: TObject);
    procedure EdQtdeKitChange(Sender: TObject);
    procedure BtConfKitClick(Sender: TObject);
    procedure EdFreteChange(Sender: TObject);
    procedure EdDescChange(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure DBGridKitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteChange(Sender: TObject);
    procedure BtUltimoClick(Sender: TObject);
    procedure EdDescExit(Sender: TObject);
    procedure DBGridProdCellClick(Column: TColumn);
    procedure DBGridKitCellClick(Column: TColumn);
    procedure QrTotCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure AtualizaStatus(Codigo, Controle, Status: Integer);
    procedure ReopenGradesCors(Codigo: Integer);
    procedure ReopenGradesTams(Codigo, Cor: Integer);
    procedure VerificaProduto();
    procedure VerificaKit();
    procedure AtualizaTotKit(TotKit: Double; Controle: Integer);
    procedure AtualizaTotVenda(Codigo, Controle: Integer);
    procedure MostraMoviVKMsg(Tipo: Integer; Lista: TStringList);
  public
    { Public declarations }
    FCliente, FCodigo, FControle, FConta: Integer;
    FTotKit: Double;
  end;

  var
  FmMoviVEdit: TFmMoviVEdit;

implementation

{$R *.DFM}

uses Module, Entidade2, MoviV, ModuleGeral, ModuleProd, MoviVK, Principal,
MoviVKMsg, UnMyObjects, DmkDAC_PF;

procedure TFmMoviVEdit.AtualizaStatus(Codigo, Controle, Status: Integer);
var
  Cor: Integer;
begin
  Cor := FmMoviV.AtualizaStatus(Codigo, Controle, 0, Status);                
  DBText1.Font.Color := Cor;
  DBText2.Font.Color := Cor;
end;

procedure TFmMoviVEdit.AtualizaTotKit(TotKit: Double; Controle: Integer);
begin
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_ALTERACAO, 'movivk', False,
  [
    'Total'
  ], ['Controle'],
  [
    TotKit
  ], [Controle]) then
  begin
    FmMoviV.ReopenMoviVK(Controle);
  end;
end;

procedure TFmMoviVEdit.AtualizaTotVenda(Codigo, Controle: Integer);
begin
  FmMoviV.AtualizaTotVenda(Codigo, Controle);
  //
  EdSubTotal.ValueVariant := FmMoviV.QrMoviVTotal.Value;
  EdPago.ValueVariant     := FmMoviV.QrMoviVPago.Value;
  LaTotal.Caption         := Geral.TFT(EdSubTotal.ValueVariant  + EdFrete.ValueVariant
    - EdDesc.ValueVariant, 2, siNegativo);
end;

procedure TFmMoviVEdit.BtConfItemClick(Sender: TObject);
var
  Prod, Grade, Cor, Tam, Conta, Controle, Codigo: Integer;
  Qtd, Preco: Double;
begin
  Prod     := EdProd.ValueVariant;
  Grade    := QrGradesCodigo.Value;
  Cor      := EdCor.ValueVariant;
  Tam      := EdTam.ValueVariant;
  Qtd      := EdProdQtd.ValueVariant;
  //
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  if Prod = 0 then
  begin
    Application.MessageBox('Produto n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBProd.SetFocus;
    Exit;
  end;
  if Cor = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCor.SetFocus;
    Exit;
  end;
  if Tam = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBTam.SetFocus;
    Exit;
  end;
  if Qtd = 0 then
  begin
    Application.MessageBox('Quantidade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdProdQtd.SetFocus;
    Exit;
  end;
  case Dmod.QrControleFatSEtqKit.Value of
    0: //N�o inclui se n�o tiver estoque
    begin
      if EdEstQ.ValueVariant <= 0 then
      begin
        Geral.MensagemBox('Inclus�o abortada!' + sLineBreak +
          'Motivo: Este item n�o possui estoque', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    2: //Perguntar antes se n�o tiver estoque
    begin
      if Geral.MensagemBox('Este item n�o possui estoque!' + sLineBreak +
        'Deseja inclu�-lo mesmo assim?', 'Pergunta',
        MB_ICONQUESTION+MB_YESNOCANCEL) <> ID_YES
      then
        Exit;
    end;
  end;
  Conta    := UMyMod.BuscaEmLivreY_Def('movim', 'Conta', stIns, FConta);
  Controle := FmMoviV.QrMoviVControle.Value;
  Codigo   := FmMoviV.QrMoviVCodigo.Value;
  Preco    := DmProd.ObtemPrecoProduto(EdListaPre.ValueVariant, Grade, Controle);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'movim', False,
  [
    'Motivo', 'Qtd', 'Val', 'Ven', 'Grade',
    'Cor', 'Tam', 'DataPedi', 'Controle'
  ], ['Conta'],
  [
    21, - Qtd, - Preco * Qtd, EdProdTot.ValueVariant, QrGradesCodigo.Value,
    Cor, Tam, FmMoviV.QrMoviVDataPedi.Value, Controle
  ], [Conta]) then
  begin
    AtualizaTotVenda(Codigo, Controle);
    //MostraEdicao(1, CO_INCLUSAO, 0); //N�o zerar os campos
    EdProd.SetFocus;
  end;
end;

procedure TFmMoviVEdit.BtConfKitClick(Sender: TObject);
var
  Codigo, Controle, Conta, Kit, Grade, Cor, Tam, Produto: Integer;
  Total, Ven, Qtd, Preco: Double;
  Inativos, SemEstoque: TStringList;
  Continuar: Boolean;
begin
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  Kit        := EdKit.ValueVariant;
  Qtd        := EdQtdeKit.ValueVariant;
  Total      := EdTotKit.ValueVariant;
  Preco      := 0;
  Inativos   := TStringList.Create;
  SemEstoque := TStringList.Create;
  //
  if EdTotKit.ValueVariant = 0 then
  begin
    Application.MessageBox('Valor do kit n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdKit.SetFocus;
    Exit;
  end;
  if Kit = 0 then
  begin
    Application.MessageBox('Kit n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBKit.SetFocus;
    Exit;
  end;
  if Qtd = 0 then
  begin
    Application.MessageBox('Quantidade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdQtdeKit.SetFocus;
    Exit;
  end;
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Codigo, Controle,');
  QrLoc.SQL.Add('Grade, Cor, Tam, Qtd');
  QrLoc.SQL.Add('FROM gradekits');
  QrLoc.SQL.Add('WHERE Codigo=:P0');
  QrLoc.Params[0].AsInteger := Kit;
  QrLoc.Open;
  //
  if QrLoc.RecordCount > 0 then
  begin
    PBKit.Visible  := True;
    PBKit.Max      := QrLoc.RecordCount;
    PBKit.Position := 0;
    //
    Codigo   := FmMoviV.QrMoviVCodigo.Value;
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
      'movivk', 'movivk', 'controle');
    if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'movivk', False,
    [
      'GradeK', 'Qtd', 'Total', 'Codigo'
    ], ['Controle'],
    [
      Kit, Qtd, Total, Codigo
    ], [Controle]) then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
        'movim', 'movim', 'conta');
        //
        Grade := QrLoc.FieldByName('Grade').AsInteger;
        Cor   := QrLoc.FieldByName('Cor').AsInteger;
        Tam   := QrLoc.FieldByName('Tam').AsInteger;
        //
        //Verifica o produto
        QrLoc2.Close;
        QrLoc2.SQL.Clear;
        QrLoc2.SQL.Add('SELECT pro.Ativo, pro.Controle, pro.EstqQ,');
        QrLoc2.SQL.Add('pro.Codigo, pro.Cor, pro.Tam, cor.Nome NOMECOR,');
        QrLoc2.SQL.Add('tam.Nome NOMETAM, gra.Nome NOMEGRA');
        QrLoc2.SQL.Add('FROM produtos pro');
        QrLoc2.SQL.Add('LEFT JOIN cores cor ON cor.Codigo = pro.Cor');
        QrLoc2.SQL.Add('LEFT JOIN tamanhos tam ON tam.Codigo = pro.Tam');
        QrLoc2.SQL.Add('LEFT JOIN grades gra ON gra.Codigo = pro.Codigo');
        QrLoc2.SQL.Add('WHERE pro.Codigo=:P0');
        QrLoc2.SQL.Add('AND pro.Cor=:P1');
        QrLoc2.SQL.Add('AND pro.Tam=:P2');
        QrLoc2.Params[0].AsInteger := Grade;
        QrLoc2.Params[1].AsInteger := Cor;
        QrLoc2.Params[2].AsInteger := Tam;
        QrLoc2.Open;
        //
        //Verifica se todos produtos do Kit est�o ativos
        if QrLoc2.RecordCount > 0 then
        begin
          if QrLoc2.FieldByName('Ativo').AsInteger = 0 then
          begin
            Inativos.Add(QrLoc2.FieldByName('Controle').Value);
            //
            PBKit.Position := PBKit.Position + 1;
          end else
          begin
            if QrLoc2.FieldByName('EstqQ').AsFloat <= 0 then
            begin
              if Dmod.QrControleFatSEtqKit.Value = 0 then
              begin
                SemEstoque.Add(Geral.FF0(QrLoc2.FieldByName('Controle').AsInteger));
                //
                PBKit.Position := PBKit.Position + 1;
              end;
            end else
            begin
              Continuar := True;
              if Dmod.QrControleFatSEtqKit.Value = 2 then
              begin
                if QrLoc2.FieldByName('EstqQ').AsFloat <= 0 then
                begin
                  if Geral.MensagemBox('O produto ' +
                    QrLoc2.FieldByName('NOMEGRA').AsString + ' ' +
                    QrLoc2.FieldByName('NOMECOR').AsString + ' ' +
                    QrLoc2.FieldByName('NOMETAM').AsString +
                    ' n�o possui estoque!' + sLineBreak +
                    'Deseja inclu�-lo assim mesmo?', 'Pergunta',
                    MB_ICONQUESTION+MB_YESNOCANCEL) <> ID_YES
                  then
                    Continuar := False;
                end;
              end;
              if Continuar then
              begin
                Produto := QrLoc2.FieldByName('Controle').AsInteger;
                Preco   := DmProd.ObtemPrecoProduto(EdListaPre.ValueVariant,
                  QrLoc.FieldByName('Grade').AsInteger, Produto);
                Ven     := Preco * QrLoc.FieldByName('Qtd').AsFloat * Qtd;
                //
                if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'movim', False,
                [
                  'Motivo', 'Kit', 'Qtd', 'Val', 'Ven', 'Grade',
                  'Cor', 'Tam', 'DataPedi', 'Controle'
                ], ['Conta'],
                [
                  21, Controle, - QrLoc.FieldByName('Qtd').AsFloat * Qtd, 0, Ven,
                  Grade, Cor, Tam, FmMoviV.QrMoviVDataPedi.Value, FmMoviV.QrMoviVControle.Value
                ], [Conta]) then
                begin
                  PBKit.Position := PBKit.Position + 1;
                end;
              end;
            end;
          end;
        end;
        QrLoc.Next;
      end;
    end;
    PBKit.Visible  := False;
    //
    FmMoviV.ReopenMoviVK(FmMoviV.QrMoviVControle.Value);
    //
    QrTot.Close;
    QrTot.Params[0].AsInteger := FmMoviV.QrMoviVControle.Value;
    QrTot.Params[1].AsInteger := FmMoviV.QrMoviVKControle.Value;
    QrTot.Open;
    //
    AtualizaTotKit(QrTotTOT.Value, FmMoviV.QrMoviVKControle.Value);
    AtualizaTotVenda(FmMoviV.QrMoviVCodigo.Value, FmMoviV.QrMoviVControle.Value);
    //MostraEdicao(2, CO_INCLUSAO, 0); N�o zerar os campos
    EdKit.SetFocus;
    if Inativos.Count > 0 then
      MostraMoviVKMsg(0, Inativos);
    if SemEstoque.Count > 0 then
      MostraMoviVKMsg(1, SemEstoque);
  end else
  begin
    Application.MessageBox('O kit selecionado n�o possui itens!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBKit.SetFocus;
    Exit;
  end;
  Inativos.Free;
  SemEstoque.Free;
end;

procedure TFmMoviVEdit.BtConfPediClick(Sender: TObject);
var
  Codigo, Controle, Cliente, GraCusPrc, GradeD: Integer;
  DataPed: String;
begin
  Cliente   := EdCliente.ValueVariant;
  GraCusPrc := EdListaPre.ValueVariant;
  DataPed   := Geral.FDT(EdDataPed.ValueVariant, 1);
  GradeD    := Dmod.QrControlePrdGradeD.Value;
  //
  if GradeD = 0 then
  begin
    Application.MessageBox('Departamento padr�o n�o definido nas op��es do aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Cliente = 0 then
  begin
    Application.MessageBox('Cliente n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdCliente.SetFocus;
    Exit;
  end;
  if GraCusPrc = 0 then
  begin
    Application.MessageBox('Lista de pre�o n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdListaPre.SetFocus;
    Exit;
  end;
  if LaTipo.SQLType = stIns then
  begin
    Codigo   := UMyMod.BuscaEmLivreY_Def('moviv', 'Codigo', LaTipo.SQLType, FCodigo);
    Controle := Dmod.BuscaProximoMovix;
  end else
  begin
    Codigo   := FmMoviV.QrMoviVCodigo.Value;
    Controle := FmMoviV.QrMoviVControle.Value;
  end;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'moviv', False,
  [
    'Cliente', 'GraCusPrc', 'Frete', 'Descon',
    'Observ', 'DataPedi', 'GradeD', 'Controle'
  ], ['Codigo'],
  [
    Cliente, GraCusPrc, EdFrete.ValueVariant, EdDesc.ValueVariant,
    EdObs.ValueVariant, DataPed, GradeD, Controle
  ], [Codigo]) then
  begin
    FmMoviV.LocCod(Codigo, Codigo);
    if LaTipo.SQLType = stIns then
    begin
      AtualizaStatus(Codigo, Controle, 1);
      MostraEdicao(0, CO_ALTERACAO, 0);
      LaTipo.SQLType := stUpd;
      EdProd.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmMoviVEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviVEdit.BtUltimoClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := FmMoviV.QrHistCodigo.Value;
  FmMoviV.LocCod(Codigo, Codigo);
  Close;  
end;

procedure TFmMoviVEdit.CBClienteExit(Sender: TObject);
begin
  if LaTipo.SQLType = stUpd then
  begin
    PCProd.ActivePageIndex := 0;
    EdProd.SetFocus;
  end;
end;

procedure TFmMoviVEdit.CBKitExit(Sender: TObject);
begin
  if CBKit.KeyValue < 1 then
    EdObs.SetFocus;
end;

procedure TFmMoviVEdit.CBProdExit(Sender: TObject);
begin
  if CBProd.KeyValue < 1 then
    EdObs.SetFocus;
end;

procedure TFmMoviVEdit.DBGridKitCellClick(Column: TColumn);
begin
  FmPrincipal.CarregaFotoKit(Image3, FmMoviV.QrMoviVKNOMEFOTO.Value);
end;

procedure TFmMoviVEdit.DBGridKitKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  procedure ExcluiKit(Codigo, Controle, Kit: Integer);
  begin
    Screen.Cursor := crHourGlass;
    //
    QrMoviVUpd.Close;
    QrMoviVUpd.SQL.Clear;
    QrMoviVUpd.SQL.Add('SELECT mom.Conta');
    QrMoviVUpd.SQL.Add('FROM movim mom');
    QrMoviVUpd.SQL.Add('LEFT JOIN moviv mov ON mov.Controle = mom.Controle');
    QrMoviVUpd.SQL.Add('WHERE mov.Codigo=:P0');
    QrMoviVUpd.SQL.Add('AND mom.Controle=:P1');
    QrMoviVUpd.SQL.Add('AND mom.Kit=:P2');
    QrMoviVUpd.SQL.Add('AND mom.Kit > 0');
    QrMoviVUpd.Params[0].AsInteger := Codigo;
    QrMoviVUpd.Params[1].AsInteger := Controle;
    QrMoviVUpd.Params[2].AsInteger := Kit;
    QrMoviVUpd.Open;
    if QrMoviVUpd.RecordCount > 0 then
    begin
      QrMoviVUpd.First;
      while not QrMoviVUpd.Eof do
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM movim WHERE Conta=:P0');
        Dmod.QrUpd.Params[00].AsInteger := QrMoviVUpd.FieldByName('Conta').AsInteger;
        Dmod.QrUpd.ExecSQL;
        //
        QrMoviVUpd.Next;
      end;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM movivk WHERE Controle=:P0');
    Dmod.QrUpd.Params[00].AsInteger := Kit;
    Dmod.QrUpd.ExecSQL;
    //
    Screen.Cursor := crDefault;
  end;
var
  Codigo, Controle, MoviVK, Kit, GraCusPrc: Integer;
begin
  // Editar
  Kit       := FmMoviV.QrMoviVKControle.Value;
  GraCusPrc := FmMoviV.QrMoviVGraCusPrc.Value;
  //
  if Kit > 0 then
  begin
    if (Key = VK_F4) then
    begin
      if DBCheck.CriaFm(TFmMoviVK, FmMoviVK, afmoNegarComAviso) then
      begin
        FmMoviVK.FControle := FmMoviV.QrMoviVControle.Value;
        FmMoviVK.FKit      := Kit;
        FmMoviVK.FListaPre := GraCusPrc;
        FmMoviVK.FTrava    := False;
        //
        FmMoviVK.ShowModal;
        FmMoviVK.Destroy;
        AtualizaTotKit(FTotKit, FmMoviV.QrMoviVKControle.Value);
      end;
      Controle := FmMoviV.QrMoviVControle.Value;
      AtualizaTotVenda(FmMoviV.QrMoviVCodigo.Value, Controle);
      FmMoviV.ReopenMoviVK(Controle);
    end;
  end;
  if (Key = VK_DELETE) and (ssCtrl in Shift) then
  begin
    Codigo   := FmMoviV.QrMoviVCodigo.Value;
    Controle := FmMoviV.QrMoviVControle.Value;
    MoviVK   := FmMoviV.QrMoviVKControle.Value;
    if MoviVK > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do registro?') = ID_YES then
      begin
        ExcluiKit(Codigo, Controle, MoviVK);
        //
        FmMoviV.ReopenMoviVK(0);
        AtualizaTotVenda(FmMoviV.QrMoviVCodigo.Value, FmMoviV.QrMoviVControle.Value);
      end;
    end;
  end;
end;

procedure TFmMoviVEdit.DBGridProdCellClick(Column: TColumn);
var
  Produto: String;
begin
  FmMoviV.QrLocFoto.Close;
  FmMoviV.QrLocFoto.Params[0].AsInteger := FmMoviV.QrMovimGrade.Value;
  FmMoviV.QrLocFoto.Params[1].AsInteger := FmMoviV.QrMovimCor.Value;
  FmMoviV.QrLocFoto.Params[2].AsInteger := FmMoviV.QrMovimTam.Value;
  FmMoviV.QrLocFoto.Open;
  if FmMoviV.QrLocFoto.RecordCount > 0 then
    Produto := FmMoviV.QrLocFotoNOMEFOTO.Value
  else
    Produto := '';
  FmPrincipal.CarregaFoto(Image2, Produto);
end;

procedure TFmMoviVEdit.DBGridProdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Conta: Integer;
begin
  // Excluir
  if (Key = VK_DELETE) and (ssCtrl in Shift) then
  begin
    Conta := FmMoviV.QrMovimConta.Value;
    //
    if Conta > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do registro?') = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM movim WHERE Conta=:P0');
        Dmod.QrUpd.Params[00].AsInteger := Conta;
        Dmod.QrUpd.ExecSQL;
        //
        AtualizaTotVenda(FmMoviV.QrMoviVCodigo.Value, FmMoviV.QrMoviVControle.Value);
        FmMoviV.ReopenMovim(0);
      end;
    end;
  end;
end;

procedure TFmMoviVEdit.EdClienteChange(Sender: TObject);
var
  DataExp: TDateTime;
  Cliente: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  //
  if Cliente > 0 then
  begin
    BtUltimo.Enabled := FmMoviV.ReopenHist(Cliente);
    //
    DataExp := Dmod.VerificaDataDeExpiracaoDoCurso(Cliente);
    LaDataExp.Caption := Geral.FDT(DataExp, 2);
  end;
end;

procedure TFmMoviVEdit.EdCorChange(Sender: TObject);
begin
  ReopenGradesTams(QrGradesCodigo.Value, EdCor.ValueVariant);
  //
  VerificaProduto;
end;

procedure TFmMoviVEdit.EdDescChange(Sender: TObject);
begin
  AtualizaTotVenda(FmMoviV.QrMoviVCodigo.Value, FmMoviV.QrMoviVControle.Value);
end;

procedure TFmMoviVEdit.EdDescExit(Sender: TObject);
begin
  BtConfPedi.SetFocus;
end;

procedure TFmMoviVEdit.EdFreteChange(Sender: TObject);
begin
  AtualizaTotVenda(FmMoviV.QrMoviVCodigo.Value, FmMoviV.QrMoviVControle.Value);
end;

procedure TFmMoviVEdit.EdKitChange(Sender: TObject);
begin
  VerificaKit;
end;

procedure TFmMoviVEdit.EdProdChange(Sender: TObject);
begin
  VerificaProduto;
end;

procedure TFmMoviVEdit.EdProdQtdChange(Sender: TObject);
begin
  VerificaProduto;
end;

procedure TFmMoviVEdit.EdQtdeKitChange(Sender: TObject);
begin
  VerificaKit;
end;

procedure TFmMoviVEdit.EdTamChange(Sender: TObject);
begin
  VerificaProduto;
end;

procedure TFmMoviVEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  MostraEdicao(0, LaTipo.Caption, 0);
end;

procedure TFmMoviVEdit.FormCreate(Sender: TObject);
begin
  QrCliente.Open;
  QrGraCusPrc.Open;
  QrGrades.Open;
  QrGradeK.Open;
  //
  PCProd.ActivePageIndex := 0;
  LaDataExp.Caption := '';
end;

procedure TFmMoviVEdit.MostraEdicao(Mostra: Integer; Status: String;
  Codigo: Integer);
var
  Total: String;
begin
  case Mostra of
    0: //Cabe�alho da venda
    begin
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.ValueVariant    := 0;
        EdDataPed.ValueVariant   := Date;
        EdDataVenda.ValueVariant := '';
        if FCliente = 0 then
        begin
          EdCliente.ValueVariant   := 0;
          CBCliente.KeyValue       := Null;
        end else
        begin
          EdCliente.ValueVariant  := FCliente;
          CBCliente.KeyValue      := FCliente;
        end;
        EdListaPre.ValueVariant  := Dmod.QrControleVPGraPrc.Value;
        CBListaPre.KeyValue      := Dmod.QrControleVPGraPrc.Value;
        EdSubTotal.ValueVariant  := 0;
        EdFrete.ValueVariant     := 0;
        EdObs.ValueVariant       := '';
        EdDesc.ValueVariant      := 0;
        EdPago.ValueVariant      := 0;
        LaTotal.Caption          := Geral.TFT('', 2, siNegativo);
        EdNomeCad.ValueVariant   := '';
        EdNomeAlt.ValueVariant   := '';
        //
        EdListaPre.Enabled       := True;
        CBListaPre.Enabled       := True;
        PnVdaBot.Visible         := False;
        PnDados.Visible          := False;
        PnBottom.Align           := alTop;
      end else
      begin
        EdCodigo.ValueVariant    := FmMoviV.QrMoviVCodigo.Value;
        EdDataPed.ValueVariant   := FmMoviV.QrMoviVDataPedi.Value;
        EdDataVenda.ValueVariant := FmMoviV.QrMoviVDATAREAL_TXT.Value; 
        EdCliente.ValueVariant   := FmMoviV.QrMoviVCliente.Value; 
        CBCliente.KeyValue       := FmMoviV.QrMoviVCliente.Value; 
        EdListaPre.ValueVariant  := FmMoviV.QrMoviVGraCusPrc.Value;
        CBListaPre.KeyValue      := FmMoviV.QrMoviVGraCusPrc.Value; 
        EdSubTotal.ValueVariant  := FmMoviV.QrMoviVTotal.Value;
        EdFrete.ValueVariant     := FmMoviV.QrMoviVFrete.Value;
        EdObs.ValueVariant       := FmMoviV.QrMoviVObserv.Value;
        EdDesc.ValueVariant      := FmMoviV.QrMoviVDescon.Value;
        EdPago.ValueVariant      := FmMoviV.QrMoviVPago.Value;
        Total                    := FloatToStr(FmMoviV.QrMoviVTOTALVDA.Value);
        LaTotal.Caption          := Geral.TFT(Total, 2, siNegativo);
        EdNomeCad.ValueVariant   := FmMoviV.QrMoviVNOMECAD2.Value;
        EdNomeAlt.ValueVariant   := FmMoviV.QrMoviVNOMEALT2.Value;
        //
        EdDataPed.Enabled        := False;
        EdDataVenda.Enabled      := False;
        EdNomeCad.Enabled        := False;
        EdNomeAlt.Enabled        := False;
        EdListaPre.Enabled       := False;
        CBListaPre.Enabled       := False;
        EdSubTotal.Enabled       := False;
        EdPago.Enabled           := False;
        PnVdaBot.Visible         := True;
        PnDados.Visible          := True;
        PnBottom.Align           := alBottom;
      end;
      EdCliente.SetFocus;
    end;
    1: //Itens da venda
    begin
      if Status = CO_INCLUSAO then
      begin
        EdProd.ValueVariant    := 0;
        CBProd.KeyValue        := Null;
        EdCor.ValueVariant     := 0;
        CBCor.KeyValue         := Null;
        EdTam.ValueVariant     := 0;
        CBTam.KeyValue         := Null;
        EdProdQtd.ValueVariant := 0;
        EdProdTot.ValueVariant := 0;
        EdEstQ.ValueVariant    := 0;
        EdProdVal.ValueVariant := 0;
      end;
      EdProd.SetFocus;
    end;
    2:
    begin
      if Status = CO_INCLUSAO then
      begin
        EdKit.ValueVariant     := 0;
        CBKit.KeyValue         := Null;
        EdQtdeKit.ValueVariant := 0;
        EdTotKit.ValueVariant  := 0;
      end;
      EdKit.SetFocus;
    end;
  end;
end;

procedure TFmMoviVEdit.MostraMoviVKMsg(Tipo: Integer; Lista: TStringList);
begin
  if DBCheck.CriaFm(TFmMoviVKMsg, FmMoviVKMsg, afmoNegarComAviso) then
  begin
    //Tipo 0 = Inativos => 1 = Sem estoque
    FmMoviVKMsg.FTipo  := Tipo;
    FmMoviVKMsg.FLista := Lista;
    FmMoviVKMsg.ShowModal;
    FmMoviVKMsg.Destroy;
  end;
end;

procedure TFmMoviVEdit.QrGradesAfterScroll(DataSet: TDataSet);
begin
  ReopenGradesCors(QrGradesCodigo.Value);
  ReopenGradesTams(QrGradesCodigo.Value, 0);
end;

procedure TFmMoviVEdit.QrGradesBeforeClose(DataSet: TDataSet);
begin
  QrGradesTams.Close;
  QrGradesCors.Close;
end;

procedure TFmMoviVEdit.QrTotCalcFields(DataSet: TDataSet);
begin
  QrTotTOT.Value := (FmMoviV.QrMoviVKQtd.Value * FmMoviV.QrMoviVKFRETE.Value) + QrTotTOTAL.Value;
end;

procedure TFmMoviVEdit.ReopenGradesCors(Codigo: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Codigo;
  QrGradesCors.Open;
end;

procedure TFmMoviVEdit.ReopenGradesTams(Codigo, Cor: Integer);
var
  SQLCor: String;
begin
  if Cor <> 0 then
    SQLCor := 'AND pro.Cor=' + Geral.FF0(Cor)
  else
    SQLCor := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGradesTams, Dmod.MyDB, [
    'SELECT tam.Nome NOMETAM, pro.Tam ',
    'FROM produtos pro ',
    'LEFT JOIN tamanhos tam ON tam.Codigo = pro.Tam ',
    'WHERE pro.Codigo=' + Geral.FF0(Codigo),
    SQLCor,
    'AND pro.Ativo=1 ',
    'GROUP BY pro.Tam ',
    'ORDER BY tam.Codigo ',
    '']);
end;

procedure TFmMoviVEdit.SBClienteClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  //
  DModG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);  
  //
  if VAR_ENTIDADE <> 0 then
  begin
    QrCliente.Close;
    QrCliente.Open;
    //
    EdCliente.ValueVariant := VAR_ENTIDADE;
    CBCliente.KeyValue     := VAR_ENTIDADE;
  end;
end;

procedure TFmMoviVEdit.VerificaKit;
var
  Kit, Controle: Integer;
  PrecoIni, PrecoFin, Qtd: Double;
begin
  if (EdKit.ValueVariant > 0) then
  begin
    Kit      := EdKit.ValueVariant;
    PrecoFin := 0;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT pro.Controle, its.Codigo, its.Grade,');
    QrLoc.SQL.Add('its.Cor, its.Tam, its.Qtd, gra.Frete, fot.Nome NOMEFOTO');
    QrLoc.SQL.Add('FROM gradekits its');
    QrLoc.SQL.Add('LEFT JOIN gradek gra ON gra.Codigo = its.Codigo');
    QrLoc.SQL.Add('LEFT JOIN fotos fot ON fot.Codigo = gra.Foto');
    QrLoc.SQL.Add('LEFT JOIN produtos pro ON pro.Codigo = its.Grade');
    QrLoc.SQL.Add('  AND pro.Cor = its.Cor');
    QrLoc.SQL.Add('  AND pro.Tam = its.Tam');
    QrLoc.SQL.Add('WHERE its.Codigo=:P0');
    QrLoc.SQL.Add('AND pro.Ativo = 1');
    QrLoc.Params[0].AsInteger := Kit;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        Controle := QrLoc.FieldByName('Controle').AsInteger;
        Qtd      := QrLoc.FieldByName('Qtd').AsFloat;
        PrecoIni := DmProd.ObtemPrecoProduto(EdListaPre.ValueVariant, QrLoc.FieldByName('Grade').AsInteger, Controle);
        //
        PrecoFin := PrecoFin + (Qtd * PrecoIni);
        //
        QrLoc.Next;
      end;
      PrecoFin := (PrecoFin + QrLoc.FieldByName('Frete').AsFloat) * EdQtdeKit.ValueVariant;
      EdTotKit.ValueVariant := PrecoFin;
      if Length(QrLoc.FieldByName('NOMEFOTO').AsString) > 0 then
        FmPrincipal.CarregaFotoKit(Image3, QrLoc.FieldByName('NOMEFOTO').AsString);
    end;
  end;
end;

procedure TFmMoviVEdit.VerificaProduto;
var
  Prod, Cor, Tam: Integer;
  Produto: String;
begin
  if (EdProd.ValueVariant > 0) and (EdCor.ValueVariant > 0) and (EdTam.ValueVariant > 0) then
  begin
    Prod := QrGradesCodigo.Value;
    Cor  := QrGradesCorsCor.Value;
    Tam  := QrGradesTamsTam.Value;
    //
    if (Prod > 0) and (Cor > 0) and (Tam > 0) then
    begin
      QrLoc.Close;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT Ativo, Controle, EstqQ, Codigo, Cor, Tam');
      QrLoc.SQL.Add('FROM produtos');
      QrLoc.SQL.Add('WHERE Codigo=:PO');
      QrLoc.SQL.Add('AND Cor=:P1');
      QrLoc.SQL.Add('AND Tam=:P2');
      QrLoc.Params[0].AsInteger := Prod;
      QrLoc.Params[1].AsInteger := Cor;
      QrLoc.Params[2].AsInteger := Tam;
      QrLoc.Open;
      //
      if QrLoc.RecordCount > 0 then
      begin
        if QrLoc.FieldByName('Ativo').AsInteger = 1 then
        begin
          EdProdVal.ValueVariant :=  DmProd.ObtemPrecoProduto(EdListaPre.ValueVariant, QrGradesCodigo.Value,
            QrLoc.FieldByName('Controle').AsInteger);
          EdEstQ.ValueVariant    := QrLoc.FieldByName('EstqQ').AsFloat;
          //
          if EdProdVal.ValueVariant > 0 then
            EdProdTot.ValueVariant := EdProdVal.ValueVariant * EdProdQtd.ValueVariant
          else
            EdProdTot.ValueVariant := 0;
          //
          FmMoviV.QrLocFoto.Close;
          FmMoviV.QrLocFoto.Params[0].AsInteger := QrLoc.FieldByName('Codigo').AsInteger;
          FmMoviV.QrLocFoto.Params[1].AsInteger := QrLoc.FieldByName('Cor').AsInteger;
          FmMoviV.QrLocFoto.Params[2].AsInteger := QrLoc.FieldByName('Tam').AsInteger;
          FmMoviV.QrLocFoto.Open;
          if FmMoviV.QrLocFoto.RecordCount > 0 then
            Produto := FmMoviV.QrLocFotoNOMEFOTO.Value
          else
            Produto := '';
          FmPrincipal.CarregaFoto(Image2, Produto);
        end else
        begin
          EdProd.ValueVariant := 0;
          CBProd.KeyValue     := Null;
          EdCor.ValueVariant  := 0;
          CBCor.KeyValue      := Null;
          EdTam.ValueVariant  := 0;
          CBTam.KeyValue      := Null;
        end;
      end;
    end;
  end;
end;

end.



