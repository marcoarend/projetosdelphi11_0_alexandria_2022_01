object FmGraGru: TFmGraGru
  Left = 339
  Top = 185
  Caption = 'PRD-GRADE-008 :: Grade de produtos'
  ClientHeight = 576
  ClientWidth = 1162
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Splitter1: TSplitter
    Left = 0
    Top = 297
    Width = 1162
    Height = 6
    Cursor = crVSplit
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1162
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Grade de produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1160
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object PnTopo: TPanel
    Left = 0
    Top = 59
    Width = 1162
    Height = 238
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 432
      Top = 1
      Width = 6
      Height = 235
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
    end
    object PnGradeD: TPanel
      Left = 1
      Top = 1
      Width = 431
      Height = 235
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
      object StaticText1: TStaticText
        Left = 1
        Top = 1
        Width = 110
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'Departamentos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 22
        Width = 429
        Height = 212
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsGradeD
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end>
      end
    end
    object PnGradeG: TPanel
      Left = 438
      Top = 1
      Width = 723
      Height = 235
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      object Splitter4: TSplitter
        Left = 487
        Top = 48
        Width = 7
        Height = 186
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
      end
      object StaticText2: TStaticText
        Left = 1
        Top = 1
        Width = 141
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'Grupos de produtos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 1
        Top = 22
        Width = 720
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object dmkEdGradeGPesq: TdmkEdit
          Left = 0
          Top = 0
          Width = 720
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = dmkEdGradeGPesqChange
        end
      end
      object dmkDBGrid2: TdmkDBGrid
        Left = 1
        Top = 48
        Width = 486
        Height = 186
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 370
            Visible = True
          end>
        Color = clWindow
        DataSource = DsGradeG
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 370
            Visible = True
          end>
      end
      object Panel1: TPanel
        Left = 494
        Top = 48
        Width = 227
        Height = 186
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        TabOrder = 3
        object Image2: TImage
          Left = 1
          Top = 22
          Width = 225
          Height = 142
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Center = True
          Proportional = True
          Stretch = True
        end
        object StaticText9: TStaticText
          Left = 1
          Top = 1
          Width = 35
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Foto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object StaticText10: TStaticText
          Left = 1
          Top = 164
          Width = 198
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Sel. o prod. na aba C'#243'digos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  object PnCenter: TPanel
    Left = 0
    Top = 303
    Width = 1162
    Height = 214
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 432
      Top = 1
      Width = 6
      Height = 212
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
    end
    object PnGrades: TPanel
      Left = 1
      Top = 1
      Width = 431
      Height = 212
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
      object StaticText3: TStaticText
        Left = 1
        Top = 1
        Width = 121
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSunken
        Caption = 'Produtos (grade)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object Panel3: TPanel
        Left = 1
        Top = 22
        Width = 429
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object dmkEdGradesPesq: TdmkEdit
          Left = 0
          Top = 0
          Width = 428
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = dmkEdGradesPesqChange
        end
      end
      object dmkDBGrid3: TdmkDBGrid
        Left = 1
        Top = 48
        Width = 429
        Height = 162
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CodUsu'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Width = 45
            Visible = True
          end>
        Color = clWindow
        DataSource = DsGrades
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodUsu'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Width = 45
            Visible = True
          end>
      end
    end
    object PnGradesDados: TPanel
      Left = 438
      Top = 1
      Width = 723
      Height = 212
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 721
        Height = 210
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 720
        ExplicitHeight = 209
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ativos'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 627
          ExplicitHeight = 0
          object GradeA: TStringGrid
            Left = 0
            Top = 21
            Width = 710
            Height = 154
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnClick = GradeAClick
            OnDrawCell = GradeADrawCell
            RowHeights = (
              18
              18)
          end
          object StaticText4: TStaticText
            Left = 0
            Top = 0
            Width = 459
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Clique na c'#233'lula correspondente para ativar / desativar o produt' +
              'o.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Estoque'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 627
          ExplicitHeight = 0
          object GradeE: TStringGrid
            Left = 0
            Top = 21
            Width = 710
            Height = 154
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            TabOrder = 0
            OnDrawCell = GradeEDrawCell
            RowHeights = (
              18
              18)
          end
          object StaticText5: TStaticText
            Left = 0
            Top = 0
            Width = 16
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = '   '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pre'#231'os'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 627
          ExplicitHeight = 0
          object dmkDBGrid4: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 322
            Height = 175
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 160
                Visible = True
              end>
            Color = clWindow
            DataSource = DsGraCusPrc
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 160
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 322
            Top = 0
            Width = 388
            Height = 175
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            TabOrder = 1
            object StaticText6: TStaticText
              Left = 1
              Top = 1
              Width = 448
              Height = 20
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 'Clique na c'#233'lula correspondente para editar o pre'#231'o do produto.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object GradeP: TStringGrid
              Left = 1
              Top = 22
              Width = 385
              Height = 152
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 1
              OnClick = GradePClick
              OnDrawCell = GradePDrawCell
            end
          end
        end
        object TabSheet4: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digos'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 627
          ExplicitHeight = 0
          object GradeC: TStringGrid
            Left = 0
            Top = 42
            Width = 710
            Height = 133
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            RowCount = 2
            TabOrder = 0
            OnClick = GradeCClick
            OnDrawCell = GradeCDrawCell
            OnKeyDown = GradeCKeyDown
            ColWidths = (
              65
              65)
            RowHeights = (
              18
              19)
          end
          object StaticText7: TStaticText
            Left = 0
            Top = 21
            Width = 525
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Para excluir uma cor / tamanho clique na c'#233'lula e precione a tec' +
              'la "Delete".'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object StaticText8: TStaticText
            Left = 0
            Top = 0
            Width = 603
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
              'dente na guia "Ativos".'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
          end
        end
        object TabSheet5: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'GradeX'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 627
          ExplicitHeight = 0
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 710
            Height = 175
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
            TabOrder = 0
            RowHeights = (
              18
              19)
          end
        end
      end
    end
  end
  object PnBottom: TPanel
    Left = 0
    Top = 517
    Width = 1162
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel4: TPanel
      Left = 994
      Top = 1
      Width = 167
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 44
        Top = 5
        Width = 118
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtDepto: TBitBtn
      Left = 5
      Top = 5
      Width = 118
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Depto.'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDeptoClick
    end
    object BtGrupo: TBitBtn
      Left = 124
      Top = 5
      Width = 118
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'G&rupo'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtGrupoClick
    end
    object BtGrade: TBitBtn
      Tag = 30
      Left = 244
      Top = 5
      Width = 118
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Grade'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtGradeClick
    end
    object BtCor: TBitBtn
      Tag = 29
      Left = 363
      Top = 5
      Width = 118
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Cor'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = BtCorClick
    end
    object BtTamanho: TBitBtn
      Tag = 300
      Left = 482
      Top = 5
      Width = 119
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Tamanho'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = BtTamanhoClick
    end
    object BtFotos: TBitBtn
      Tag = 102
      Left = 601
      Top = 6
      Width = 118
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Fotos'
      NumGlyphs = 2
      TabOrder = 6
      OnClick = BtFotosClick
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 10
    Top = 9
  end
  object QrGradeD: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradeDBeforeClose
    AfterScroll = QrGradeDAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM graded'
      'WHERE Codigo > 0')
    Left = 38
    Top = 9
    object QrGradeDCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'graded.Codigo'
    end
    object QrGradeDNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'graded.Nome'
      Size = 30
    end
    object QrGradeDLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'graded.Lk'
    end
    object QrGradeDDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'graded.DataCad'
    end
    object QrGradeDDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'graded.DataAlt'
    end
    object QrGradeDUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'graded.UserCad'
    end
    object QrGradeDUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'graded.UserAlt'
    end
    object QrGradeDAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'graded.AlterWeb'
    end
    object QrGradeDAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'graded.Ativo'
    end
  end
  object DsGradeD: TDataSource
    DataSet = QrGradeD
    Left = 66
    Top = 9
  end
  object PMGrade: TPopupMenu
    Left = 254
    Top = 413
    object Incluinovagrade1: TMenuItem
      Caption = '&Inclui nova grade de mecadoria'
      OnClick = Incluinovagrade1Click
    end
    object Alteranomedagrade1: TMenuItem
      Caption = '&Altera grade de mercadoria'
      OnClick = Alteranomedagrade1Click
    end
    object Excluigradedemercadoria1: TMenuItem
      Caption = '&Exclui grade de mercadoria'
      OnClick = Excluigradedemercadoria1Click
    end
  end
  object PMCor: TPopupMenu
    Left = 349
    Top = 413
    object Incluinovacorgradeatual1: TMenuItem
      Caption = '&Inclui nova cor '#224' grade atual'
      OnClick = Incluinovacorgradeatual1Click
    end
    object Alteraacorselecionada1: TMenuItem
      Caption = '&Altera a cor selecionada'
      OnClick = Alteraacorselecionada1Click
    end
  end
  object PMTam: TPopupMenu
    Left = 443
    Top = 414
    object Incluinovotamanhogradeatual1: TMenuItem
      Caption = '&Inclui novo tamanho '#224' grade atual'
      OnClick = Incluinovotamanhogradeatual1Click
    end
    object Alteratamanhoselecionado1: TMenuItem
      Caption = '&Altera tamanho selecionado'
      OnClick = Alteratamanhoselecionado1Click
    end
  end
  object PMGrupo: TPopupMenu
    Left = 150
    Top = 413
    object MenuItem1: TMenuItem
      Caption = '&Inclui novo grupo'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = '&Altera grupo atual'
      OnClick = MenuItem2Click
    end
  end
  object PMDepto: TPopupMenu
    Left = 46
    Top = 413
    object MenuItem3: TMenuItem
      Caption = '&Inclui novo depto.'
      OnClick = MenuItem3Click
    end
    object MenuItem4: TMenuItem
      Caption = '&Altera depto. atual'
      OnClick = MenuItem4Click
    end
  end
  object QrGradeG: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradeGBeforeClose
    AfterScroll = QrGradeGAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM gradeg'
      'WHERE GradeD=:P0'
      'AND Nome LIKE :P1')
    Left = 94
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGradeGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradeGLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradeGDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradeGDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradeGUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradeGUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradeGNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGradeGAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGradeGAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrGradeGGradeD: TIntegerField
      FieldName = 'GradeD'
    end
  end
  object DsGradeG: TDataSource
    DataSet = QrGradeG
    Left = 122
    Top = 9
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradesBeforeClose
    AfterScroll = QrGradesAfterScroll
    SQL.Strings = (
      'SELECT grs.* '
      'FROM grades grs'
      'WHERE grs.GradeG=:P0'
      'AND Nome LIKE :P1')
    Left = 150
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGradesGradeG: TIntegerField
      FieldName = 'GradeG'
      Required = True
    end
    object QrGradesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGradesAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
      MaxValue = 1
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 178
    Top = 9
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY Controle')
    Left = 206
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradestams.Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradestams.Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'gradestams.Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradestams.Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradestams.DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradestams.DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradestams.UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradestams.UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsGradesTams: TDataSource
    DataSet = QrGradesTams
    Left = 234
    Top = 9
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY Controle')
    Left = 262
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
  end
  object DsGradesCors: TDataSource
    DataSet = QrGradesCors
    Left = 290
    Top = 9
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pro.Tam, pro.Cor, pro.Ativo, pro.EstqQ, pro.PrecoV'
      'FROM produtos pro'
      'LEFT JOIN tamanhos tam ON tam.Codigo=pro.Tam'
      'LEFT JOIN cores    cor ON cor.Codigo=pro.Cor'
      'WHERE pro.Codigo= :P0'
      '/*Para poder ordenar!!!*/'
      'ORDER BY tam.Nome, cor.Nome')
    Left = 318
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutosTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrProdutosCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
      Required = True
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 346
    Top = 9
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 402
    Top = 9
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrGraCusPrcAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT gcp.Codigo, gcp.Nome,'
      'mda.Nome NOMEMOEDA, mda.Sigla SIGLAMOEDA'
      'FROM gracusprc gcp'
      'LEFT JOIN cambiomda mda ON mda.Codigo=gcp.Moeda')
    Left = 374
    Top = 9
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraCusPrcNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrGraCusPrcSIGLAMOEDA: TWideStringField
      FieldName = 'SIGLAMOEDA'
      Size = 5
    end
  end
  object QrLocPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CustoPreco, Controle'
      'FROM gragruval'
      'WHERE GragruX=:P0'
      'AND Codigo=:P1'
      'AND GraCusPrc=:P2')
    Left = 430
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocPrcCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
    object QrLocPrcControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrGraGruVal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT val.Tam, val.Cor, val.Ativo, val.CustoPreco'
      'FROM gragruval val'
      'LEFT JOIN tamanhos tam ON tam.Codigo=val.Tam'
      'LEFT JOIN cores    cor ON cor.Codigo=val.Cor'
      'WHERE val.Codigo= :P0'
      'AND GraCusPrc=:P1'
      '/*Para poder ordenar!!!*/'
      'ORDER BY tam.Nome, cor.Nome')
    Left = 458
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGraGruValCustoPreco: TFloatField
      FieldName = 'CustoPreco'
      Required = True
    end
    object QrGraGruValTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrGraGruValCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrGraGruValAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsGraGruVal: TDataSource
    DataSet = QrGraGruVal
    Left = 486
    Top = 9
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 514
    Top = 9
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 525
    Top = 193
  end
end
