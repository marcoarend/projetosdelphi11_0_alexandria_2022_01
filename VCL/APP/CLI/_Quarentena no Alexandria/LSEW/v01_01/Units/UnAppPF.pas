unit UnAppPF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, mySQLDBTables,
  UnInternalConsts2, ComCtrls, dmkGeral, UnDmkEnums, SHDocVw;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // O U T R O S
    function  AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
              Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects;

{ TUnAppPF }

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
begin
  //Compatibilidade
  Result := False;
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

end.
