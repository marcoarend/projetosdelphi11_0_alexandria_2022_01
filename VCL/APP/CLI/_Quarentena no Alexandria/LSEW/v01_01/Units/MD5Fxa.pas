unit MD5Fxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, Mask,
  dmkDBEdit, MyDBCheck, dmkGeral, dmkPermissoes, UnDmkEnums;

type
  TFmMD5Fxa = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    Label1: TLabel;
    DBEdit1: TdmkDBEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EdNumIni: TdmkEdit;
    EdNumFim: TdmkEdit;
    Label3: TLabel;
    EdControle: TdmkEdit;
    Label4: TLabel;
    QrPesq: TmySQLQuery;
    QrPesqFIM: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMD5Fxa: TFmMD5Fxa;

implementation

uses Module, UMySQLModule, UnInternalConsts, MD5Cab, UnitMD5, UnMyObjects;

{$R *.DFM}

procedure TFmMD5Fxa.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := FmMD5Cab.QrMD5CabCodigo.Value;
  if LaTipo.SQLType = stIns then
    EdControle.ValueVariant := UMyMod.BuscaEmLivreY_Def('md5fxa', 'Controle', stIns, 0);
  if UMyMod.ExecSQLInsUpdFm(FmMD5Fxa, LaTipo.SQLType, 'md5fxa',
  Codigo, Dmod.QrUpd) then
  begin
    FmMD5Cab.ReopenMD5Fxa(EdControle.ValueVariant);
    Close;
  end;
end;

procedure TFmMD5Fxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMD5Fxa.FormActivate(Sender: TObject);
var
  FxaIni: Integer;
begin
  MyObjects.CorIniComponente;
  if LaTipo.SQLType = stIns then
  begin
    QrPesq.Close;
    QrPesq.Params[0].AsInteger := FmMD5Cab.QrMD5CabCodigo.Value;
    QrPesq.Open;
    //
    if (QrPesqFIM.Value = 0) and (FmMD5Cab.QrMD5Fxa.RecordCount = 0) then
    FxaIni := 0 else FxaIni := QrPesqFIM.Value + 1;
    EdNumIni.ValueVariant := FxaIni;
  end;
end;

procedure TFmMD5Fxa.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
