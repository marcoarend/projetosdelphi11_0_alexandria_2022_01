unit CartAvalS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, dmkGeral, UnDmkEnums;

type
  TFmCartAvalS = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    QrMotivos: TmySQLQuery;
    DsMotivos: TDataSource;
    TPData: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    TPHora: TDateTimePicker;
    EdMotivo: TdmkEditCB;
    CBMotivo: TdmkDBLookupComboBox;
    QrMotivosCodigo: TIntegerField;
    QrMotivosNome: TWideStringField;
    LaTipo: TdmkLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmCartAvalS: TFmCartAvalS;

implementation

{$R *.DFM}

uses Module, MyListas, Motivos, CartAval, UnMyObjects;

procedure TFmCartAvalS.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := FmCartAval.QrCartAvalCodigo.Value;
  Close;
end;

procedure TFmCartAvalS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCartAvalS.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCartAvalS.FormCreate(Sender: TObject);
begin
  TPData.Date := Now();
  TPHora.Time := Now();
  //
  QrMotivos.Open;
end;

procedure TFmCartAvalS.SpeedButton1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMotivos, FmMotivos, afmoNegarComAviso) then
  begin
    FmMotivos.ShowModal;
    FmMotivos.Destroy;
    QrMotivos.Close;
    QrMotivos.Open;
    //
    if VAR_COD > 0 then
    begin
      EdMotivo.ValueVariant := VAR_COD;
      CBMotivo.KeyValue     := VAR_COD;
    end;
  end;
end;

procedure TFmCartAvalS.BtOKClick(Sender: TObject);
var
  DataHora: String;
  Codigo, Motivo: Integer;
begin
  Codigo   := FCodigo;
  DataHora := Geral.FDT(Int(TPData.Date) + TPHora.Time - Int(TPHora.Time), 9);
  Motivo   := EdMotivo.ValueVariant;
  //
  if Motivo = 0 then
  begin
    Application.MessageBox('Defina o motivo!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdMotivo.SetFocus;
    Exit;
  end;
  if FmCartAval.InsereMotivo(Codigo, Motivo, DataHora) then
  begin
    FmCartAval.ReopenCartAvalS(FmCartAval.FCartAvalS);
    Close;
  end;
end;

end.

