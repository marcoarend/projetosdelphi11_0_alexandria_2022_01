unit Promotores;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkDBLookupComboBox,
  dmkEditCB, Variants, UnDmkProcFunc, UnDmkEnums;

type
  TFmPromotores = class(TForm)
    PainelDados: TPanel;
    DsPromotores: TDataSource;
    QrPromotores: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    QrPromotoresCodigo: TIntegerField;
    QrPromotoresCodUsu: TIntegerField;
    QrPromotoresEquicom: TIntegerField;
    QrPromotoresEntidade: TIntegerField;
    QrPromotoresNOMEPRM: TWideStringField;
    Label27: TLabel;
    EdPromotor: TdmkEditCB;
    CBPromotor: TdmkDBLookupComboBox;
    SpeedButton8: TSpeedButton;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    BtSincronizar: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPromotoresAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPromotoresBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure LocCod(Atual, Codigo: Integer);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtSincronizarClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmPromotores: TFmPromotores;
const
  FFormatFloat = '00000';

implementation

uses Module, Principal, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPromotores.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPromotores.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPromotoresCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPromotores.DefParams;
begin
  VAR_GOTOTABELA := 'promotores';
  VAR_GOTOMYSQLTABLE := QrPromotores;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pro.Codigo, pro.CodUsu, pro.Entidade, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPRM');
  VAR_SQLx.Add('FROM promotores pro');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = pro.Entidade');
  VAR_SQLx.Add('WHERE pro.Codigo > 0');
  //
  VAR_SQL1.Add('AND pro.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pro.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPromotores.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'promotores', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmPromotores.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant   := FormatFloat(FFormatFloat, Codigo);
        EdCodUsu.ValueVariant   := 0;
        EdPromotor.ValueVariant := 0;
        CBPromotor.KeyValue     := Null;
        //
      end else begin
        EdCodigo.ValueVariant   := QrPromotoresCodigo.Value;
        EdCodUsu.ValueVariant   := QrPromotoresCodUsu.Value;
        EdPromotor.ValueVariant := QrPromotoresEntidade.Value;
        CBPromotor.KeyValue     := QrPromotoresEntidade.Value;
        //
      end;
      EdCodUsu.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmPromotores.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPromotores.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPromotores.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPromotores.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPromotores.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPromotores.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPromotores.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPromotores.SpeedButton8Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeEntidades(EdPromotor.ValueVariant);
  //
  QrPromotores.Close;
  QrPromotores.Open;
  CBPromotor.SetFocus;
end;

procedure TFmPromotores.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPromotores, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'promotores');
end;

procedure TFmPromotores.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPromotoresCodigo.Value;
  Close;
end;

procedure TFmPromotores.BtSincronizarClick(Sender: TObject);
begin
  if Dmod.ConexaoRemota(1) then
  begin
    if Geral.MensagemBox('Foram localizados novos dados na WEB! Deseja adicion�-los?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      FmPrincipal.MostraPromoWeb;
      //
      QrEntidades.Close;
      QrEntidades.Open;
      //
      LocCod(VAR_CADASTRO, VAR_CADASTRO);
    end;
  end;
end;

procedure TFmPromotores.BtConfirmaClick(Sender: TObject);
var
  Codigo, Entidade: Integer;
begin
  Entidade  := EdPromotor.ValueVariant;
  if Entidade = 0 then
  begin
    Geral.MensagemBox('Promotor n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBPromotor.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('promotores', 'Codigo', LaTipo.SQLType,
    QrPromotoresCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmPromotores, PainelEdit,
    'promotores', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPromotores.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'promotores', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'promotores', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'promotores', 'Codigo');
end;

procedure TFmPromotores.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrPromotores, [PainelDados],
  [PainelEdita], EdCodUsu, LaTipo, 'promotores');
end;

procedure TFmPromotores.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  //
  QrEntidades.Close;
  QrEntidades.Open;
end;

procedure TFmPromotores.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPromotoresCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPromotores.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPromotores.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPromotoresCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPromotores.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmPromotores.QrPromotoresAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPromotores.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPromotores.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPromotoresCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'promotores', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPromotores.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPromotores.QrPromotoresBeforeOpen(DataSet: TDataSet);
begin
  QrPromotoresCodigo.DisplayFormat := FFormatFloat;
end;

end.

