object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 523
  Width = 814
  object QrFields: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 428
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrMaster: TmySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    AfterCancel = QrControleAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha, ma.*'
      'FROM entidades te, controle cm, ufs uf, master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
      Origin = 'master.UsaAccMngr'
    end
  end
  object QrRecCountX: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  artigosgrupos')
    Left = 392
    Top = 247
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrUser: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 12
    Top = 95
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
  end
  object QrSB: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 232
    Top = 7
  end
  object QrSB2: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 51
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.lanctos.Controle'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 232
    Top = 51
  end
  object QrSB3: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 204
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 232
    Top = 95
  end
  object QrDuplicIntX: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 159
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrDuplicStrX: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrInsLogX: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalY: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 272
    Top = 7
    object QrDataBalYData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrSenha: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 8
    Top = 239
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNTEACH.senhas.Numero'
    end
    object QrSenhaSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNTEACH.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNTEACH.senhas.Perfil'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNTEACH.senhas.Lk'
    end
  end
  object QrMaster2: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 13
    Top = 191
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMaster2Em: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMaster2CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
      Required = True
    end
    object QrMaster2Licenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
      Required = True
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrMaster2MasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
  end
  object QrSomaM: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM FTabLctALS')
    Left = 448
    Top = 91
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
  end
  object QrProduto: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo, Controla'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 120
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
  end
  object QrVendas: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtossits'
      'WHERE Produto=:P0')
    Left = 92
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVendasPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtossits.Pecas'
    end
    object QrVendasValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtossits.Total'
    end
  end
  object QrEntrada: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtoscits'
      'WHERE Produto=:P0'
      '')
    Left = 64
    Top = 287
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntradaPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtoscits.Pecas'
    end
    object QrEntradaValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtoscits.Total'
    end
  end
  object QrUpdM: TmySQLQuery
    Database = MyDB
    Left = 448
    Top = 47
  end
  object QrUpdU: TmySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object QrUpdL: TmySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 95
  end
  object QrUpdY: TmySQLQuery
    Database = MyDB
    Left = 64
    Top = 239
  end
  object QrLivreY: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo FROM livres')
    Left = 60
    Top = 191
    object QrLivreYCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrCountY: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  entidades')
    Left = 464
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocY: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 464
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrIdx: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrMas: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrAux: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrSQL: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object MyDB: TmySQLDatabase
    DatabaseName = 'lesew'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=lesew'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 12
    Top = 3
  end
  object MyLocDatabase: TmySQLDatabase
    DatabaseName = 'loclesew'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=loclesew'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 156
    Top = 7
  end
  object QrSB4: TmySQLQuery
    Database = MyLocDatabase
    Left = 200
    Top = 143
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 232
    Top = 143
  end
  object QrPriorNext: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAuxL: TmySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 51
  end
  object QrSoma1: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(TotalC*Tipo) TOTAL, '
      'SUM(Qtde*Tipo) Qtde '
      'FROM mov'
      'WHERE Produto=:P0')
    Left = 260
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoma1TOTAL: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TOTAL'
    end
    object QrSoma1Qtde: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Qtde'
    end
  end
  object QrControle: TmySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    OnCalcFields = QrControleCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 12
    Top = 287
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
      Origin = 'controle.LastBco'
    end
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Origin = 'controle.Moeda'
      Size = 4
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
      Origin = 'controle.UFPadrao'
    end
    object QrControleContVen: TIntegerField
      FieldName = 'ContVen'
      Origin = 'controle.ContVen'
    end
    object QrControleContCom: TIntegerField
      FieldName = 'ContCom'
      Origin = 'controle.ContCom'
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
      Origin = 'controle.CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
      Origin = 'controle.CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
      Origin = 'controle.CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
      Origin = 'controle.CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
      Origin = 'controle.CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
      Origin = 'controle.CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
      Origin = 'controle.CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
      Origin = 'controle.CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
      Origin = 'controle.CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
      Origin = 'controle.CartEmA'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Origin = 'controle.ContraSenha'
      Size = 50
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
      Origin = 'controle.TravaCidade'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
      Origin = 'controle.MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'controle.Multa'
    end
    object QrControleMensalSempre: TIntegerField
      FieldName = 'MensalSempre'
      Origin = 'controle.MensalSempre'
    end
    object QrControleEntraSemValor: TIntegerField
      FieldName = 'EntraSemValor'
      Origin = 'controle.EntraSemValor'
    end
    object QrControleMyPagTip: TIntegerField
      FieldName = 'MyPagTip'
      Origin = 'controle.MyPagTip'
    end
    object QrControleMyPagCar: TIntegerField
      FieldName = 'MyPagCar'
      Origin = 'controle.MyPagCar'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
      Origin = 'controle.CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
      Origin = 'controle.IdleMinutos'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
      Origin = 'controle.MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
      Origin = 'controle.MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
      Origin = 'controle.MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
      Origin = 'controle.MyPgDias'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Origin = 'controle.MeuLogoPath'
      Size = 255
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Origin = 'controle.LogoNF'
      Size = 255
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
      Origin = 'controle.VendaDiasPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
      Origin = 'controle.VendaPeriPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
      Origin = 'controle.VendaParcPg'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
      Origin = 'controle.VendaCartPg'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
      Origin = 'controle.CNABCtaTar'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
      Origin = 'controle.CNABCtaMul'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
      Origin = 'controle.CNABCtaJur'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
      Origin = 'controle.VerBcoTabs'
    end
    object QrControleLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'controle.Lk'
    end
    object QrControleDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'controle.DataCad'
    end
    object QrControleDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'controle.DataAlt'
    end
    object QrControleUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'controle.UserCad'
    end
    object QrControleUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'controle.UserAlt'
    end
    object QrControleCNAB_Rem: TIntegerField
      FieldName = 'CNAB_Rem'
      Origin = 'controle.CNAB_Rem'
    end
    object QrControleCNAB_Rem_I: TIntegerField
      FieldName = 'CNAB_Rem_I'
      Origin = 'controle.CNAB_Rem_I'
    end
    object QrControlePreEmMsgIm: TIntegerField
      FieldName = 'PreEmMsgIm'
      Origin = 'controle.PreEmMsgIm'
    end
    object QrControlePreEmMsg: TIntegerField
      FieldName = 'PreEmMsg'
      Origin = 'controle.PreEmMsg'
    end
    object QrControlePreEmail: TIntegerField
      FieldName = 'PreEmail'
      Origin = 'controle.PreEmail'
    end
    object QrControleContasMes: TIntegerField
      FieldName = 'ContasMes'
      Origin = 'controle.ContasMes'
    end
    object QrControleMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
      Origin = 'controle.MultiPgto'
    end
    object QrControleImpObs: TIntegerField
      FieldName = 'ImpObs'
      Origin = 'controle.ImpObs'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
      Origin = 'controle.ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
      Origin = 'controle.ProLaINSSp'
    end
    object QrControleVerSalTabs: TIntegerField
      FieldName = 'VerSalTabs'
      Origin = 'controle.VerSalTabs'
    end
    object QrControleCNAB_CaD: TIntegerField
      FieldName = 'CNAB_CaD'
      Origin = 'controle.CNAB_CaD'
    end
    object QrControleCNAB_CaG: TIntegerField
      FieldName = 'CNAB_CaG'
      Origin = 'controle.CNAB_CaG'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
      Origin = 'controle.AtzCritic'
    end
    object QrControleContasTrf: TIntegerField
      FieldName = 'ContasTrf'
      Origin = 'controle.ContasTrf'
    end
    object QrControleSomaIts: TIntegerField
      FieldName = 'SomaIts'
      Origin = 'controle.SomaIts'
    end
    object QrControleContasAgr: TIntegerField
      FieldName = 'ContasAgr'
      Origin = 'controle.ContasAgr'
    end
    object QrControleConfJanela: TIntegerField
      FieldName = 'ConfJanela'
      Origin = 'controle.ConfJanela'
    end
    object QrControleDataPesqAuto: TSmallintField
      FieldName = 'DataPesqAuto'
      Origin = 'controle.DataPesqAuto'
    end
    object QrControleAtividad: TSmallintField
      FieldName = 'Atividad'
      Origin = 'controle.Atividad'
    end
    object QrControleCidades: TSmallintField
      FieldName = 'Cidades'
      Origin = 'controle.Cidades'
    end
    object QrControleChequez: TSmallintField
      FieldName = 'Chequez'
      Origin = 'controle.Chequez'
    end
    object QrControlePaises: TSmallintField
      FieldName = 'Paises'
      Origin = 'controle.Paises'
    end
    object QrControleCambiosData: TDateField
      FieldName = 'CambiosData'
      Origin = 'controle.CambiosData'
    end
    object QrControleCambiosUsuario: TIntegerField
      FieldName = 'CambiosUsuario'
      Origin = 'controle.CambiosUsuario'
    end
    object QrControlereg10: TSmallintField
      FieldName = 'reg10'
      Origin = 'controle.reg10'
    end
    object QrControlereg11: TSmallintField
      FieldName = 'reg11'
      Origin = 'controle.reg11'
    end
    object QrControlereg50: TSmallintField
      FieldName = 'reg50'
      Origin = 'controle.reg50'
    end
    object QrControlereg51: TSmallintField
      FieldName = 'reg51'
      Origin = 'controle.reg51'
    end
    object QrControlereg53: TSmallintField
      FieldName = 'reg53'
      Origin = 'controle.reg53'
    end
    object QrControlereg54: TSmallintField
      FieldName = 'reg54'
      Origin = 'controle.reg54'
    end
    object QrControlereg56: TSmallintField
      FieldName = 'reg56'
      Origin = 'controle.reg56'
    end
    object QrControlereg60: TSmallintField
      FieldName = 'reg60'
      Origin = 'controle.reg60'
    end
    object QrControlereg75: TSmallintField
      FieldName = 'reg75'
      Origin = 'controle.reg75'
    end
    object QrControlereg88: TSmallintField
      FieldName = 'reg88'
      Origin = 'controle.reg88'
    end
    object QrControlereg90: TSmallintField
      FieldName = 'reg90'
      Origin = 'controle.reg90'
    end
    object QrControleNumSerieNF: TSmallintField
      FieldName = 'NumSerieNF'
      Origin = 'controle.NumSerieNF'
    end
    object QrControleSerieNF: TSmallintField
      FieldName = 'SerieNF'
      Origin = 'controle.SerieNF'
    end
    object QrControleModeloNF: TSmallintField
      FieldName = 'ModeloNF'
      Origin = 'controle.ModeloNF'
    end
    object QrControleControlaNeg: TSmallintField
      FieldName = 'ControlaNeg'
      Origin = 'controle.ControlaNeg'
    end
    object QrControleFamilias: TSmallintField
      FieldName = 'Familias'
      Origin = 'controle.Familias'
    end
    object QrControleFamiliasIts: TSmallintField
      FieldName = 'FamiliasIts'
      Origin = 'controle.FamiliasIts'
    end
    object QrControleAskNFOrca: TSmallintField
      FieldName = 'AskNFOrca'
      Origin = 'controle.AskNFOrca'
    end
    object QrControlePreviewNF: TSmallintField
      FieldName = 'PreviewNF'
      Origin = 'controle.PreviewNF'
    end
    object QrControleOrcaRapido: TSmallintField
      FieldName = 'OrcaRapido'
      Origin = 'controle.OrcaRapido'
    end
    object QrControleDistriDescoItens: TSmallintField
      FieldName = 'DistriDescoItens'
      Origin = 'controle.DistriDescoItens'
    end
    object QrControleBalType: TSmallintField
      FieldName = 'BalType'
      Origin = 'controle.BalType'
    end
    object QrControleOrcaOrdem: TSmallintField
      FieldName = 'OrcaOrdem'
      Origin = 'controle.OrcaOrdem'
    end
    object QrControleOrcaLinhas: TSmallintField
      FieldName = 'OrcaLinhas'
      Origin = 'controle.OrcaLinhas'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
      Origin = 'controle.OrcaLFeed'
    end
    object QrControleOrcaModelo: TSmallintField
      FieldName = 'OrcaModelo'
      Origin = 'controle.OrcaModelo'
    end
    object QrControleOrcaRodaPos: TSmallintField
      FieldName = 'OrcaRodaPos'
      Origin = 'controle.OrcaRodaPos'
    end
    object QrControleOrcaRodape: TSmallintField
      FieldName = 'OrcaRodape'
      Origin = 'controle.OrcaRodape'
    end
    object QrControleOrcaCabecalho: TSmallintField
      FieldName = 'OrcaCabecalho'
      Origin = 'controle.OrcaCabecalho'
    end
    object QrControleCoresRel: TSmallintField
      FieldName = 'CoresRel'
      Origin = 'controle.CoresRel'
    end
    object QrControleCFiscalPadr: TWideStringField
      FieldName = 'CFiscalPadr'
      Origin = 'controle.CFiscalPadr'
    end
    object QrControleSitTribPadr: TWideStringField
      FieldName = 'SitTribPadr'
      Origin = 'controle.SitTribPadr'
    end
    object QrControleCFOPPadr: TWideStringField
      FieldName = 'CFOPPadr'
      Origin = 'controle.CFOPPadr'
    end
    object QrControleAvisosCxaEdit: TSmallintField
      FieldName = 'AvisosCxaEdit'
      Origin = 'controle.AvisosCxaEdit'
    end
    object QrControleChConfCab: TIntegerField
      FieldName = 'ChConfCab'
      Origin = 'controle.ChConfCab'
    end
    object QrControleImpDOS: TIntegerField
      FieldName = 'ImpDOS'
      Origin = 'controle.ImpDOS'
    end
    object QrControleUnidadePadrao: TIntegerField
      FieldName = 'UnidadePadrao'
      Origin = 'controle.UnidadePadrao'
    end
    object QrControleProdutosV: TIntegerField
      FieldName = 'ProdutosV'
      Origin = 'controle.ProdutosV'
    end
    object QrControleCartDespesas: TIntegerField
      FieldName = 'CartDespesas'
      Origin = 'controle.CartDespesas'
    end
    object QrControleReserva: TSmallintField
      FieldName = 'Reserva'
      Origin = 'controle.Reserva'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Size = 18
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
      Origin = 'controle.VerWeb'
    end
    object QrControleErroHora: TIntegerField
      FieldName = 'ErroHora'
      Origin = 'controle.ErroHora'
    end
    object QrControleSenhas: TIntegerField
      FieldName = 'Senhas'
      Origin = 'controle.Senhas'
    end
    object QrControleSalarios: TIntegerField
      FieldName = 'Salarios'
      Origin = 'controle.Salarios'
    end
    object QrControleEntidades: TIntegerField
      FieldName = 'Entidades'
      Origin = 'controle.Entidades'
    end
    object QrControleUFs: TIntegerField
      FieldName = 'UFs'
      Origin = 'controle.UFs'
    end
    object QrControleListaECivil: TIntegerField
      FieldName = 'ListaECivil'
      Origin = 'controle.ListaECivil'
    end
    object QrControlePerfis: TIntegerField
      FieldName = 'Perfis'
      Origin = 'controle.Perfis'
    end
    object QrControleUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'controle.Usuario'
    end
    object QrControleContas: TIntegerField
      FieldName = 'Contas'
      Origin = 'controle.Contas'
    end
    object QrControleCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Origin = 'controle.CentroCusto'
    end
    object QrControleDepartamentos: TIntegerField
      FieldName = 'Departamentos'
      Origin = 'controle.Departamentos'
    end
    object QrControleDividas: TIntegerField
      FieldName = 'Dividas'
      Origin = 'controle.Dividas'
    end
    object QrControleDividasIts: TIntegerField
      FieldName = 'DividasIts'
      Origin = 'controle.DividasIts'
    end
    object QrControleDividasPgs: TIntegerField
      FieldName = 'DividasPgs'
      Origin = 'controle.DividasPgs'
    end
    object QrControleCarteiras: TIntegerField
      FieldName = 'Carteiras'
      Origin = 'controle.Carteiras'
    end
    object QrControleCartaG: TIntegerField
      FieldName = 'CartaG'
      Origin = 'controle.CartaG'
    end
    object QrControleCartas: TIntegerField
      FieldName = 'Cartas'
      Origin = 'controle.Cartas'
    end
    object QrControleConsignacao: TIntegerField
      FieldName = 'Consignacao'
      Origin = 'controle.Consignacao'
    end
    object QrControleGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'controle.Grupo'
    end
    object QrControleSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = 'controle.SubGrupo'
    end
    object QrControleConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'controle.Conjunto'
    end
    object QrControlePlano: TIntegerField
      FieldName = 'Plano'
      Origin = 'controle.Plano'
    end
    object QrControleInflacao: TIntegerField
      FieldName = 'Inflacao'
      Origin = 'controle.Inflacao'
    end
    object QrControlekm: TIntegerField
      FieldName = 'km'
      Origin = 'controle.km'
    end
    object QrControlekmMedia: TIntegerField
      FieldName = 'kmMedia'
      Origin = 'controle.kmMedia'
    end
    object QrControlekmIts: TIntegerField
      FieldName = 'kmIts'
      Origin = 'controle.kmIts'
    end
    object QrControleFatura: TIntegerField
      FieldName = 'Fatura'
      Origin = 'controle.Fatura'
    end
    object QrControleLanctos: TLargeintField
      FieldName = 'Lanctos'
      Origin = 'controle.Lanctos'
    end
    object QrControleEntiGrupos: TIntegerField
      FieldName = 'EntiGrupos'
      Origin = 'controle.EntiGrupos'
    end
    object QrControleAparencias: TIntegerField
      FieldName = 'Aparencias'
      Origin = 'controle.Aparencias'
    end
    object QrControlePages: TIntegerField
      FieldName = 'Pages'
      Origin = 'controle.Pages'
    end
    object QrControleMultiEtq: TIntegerField
      FieldName = 'MultiEtq'
      Origin = 'controle.MultiEtq'
    end
    object QrControleEntiTransp: TIntegerField
      FieldName = 'EntiTransp'
      Origin = 'controle.EntiTransp'
    end
    object QrControleExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'controle.ExcelGru'
    end
    object QrControleExcelGruImp: TIntegerField
      FieldName = 'ExcelGruImp'
      Origin = 'controle.ExcelGruImp'
    end
    object QrControleMediaCH: TIntegerField
      FieldName = 'MediaCH'
      Origin = 'controle.MediaCH'
    end
    object QrControleImprime: TIntegerField
      FieldName = 'Imprime'
      Origin = 'controle.Imprime'
    end
    object QrControleImprimeBand: TIntegerField
      FieldName = 'ImprimeBand'
      Origin = 'controle.ImprimeBand'
    end
    object QrControleImprimeView: TIntegerField
      FieldName = 'ImprimeView'
      Origin = 'controle.ImprimeView'
    end
    object QrControleComProdPerc: TIntegerField
      FieldName = 'ComProdPerc'
      Origin = 'controle.ComProdPerc'
    end
    object QrControleComProdEdit: TIntegerField
      FieldName = 'ComProdEdit'
      Origin = 'controle.ComProdEdit'
    end
    object QrControleComServPerc: TIntegerField
      FieldName = 'ComServPerc'
      Origin = 'controle.ComServPerc'
    end
    object QrControleComServEdit: TIntegerField
      FieldName = 'ComServEdit'
      Origin = 'controle.ComServEdit'
    end
    object QrControlePadrPlacaCar: TWideStringField
      FieldName = 'PadrPlacaCar'
      Origin = 'controle.PadrPlacaCar'
      Size = 100
    end
    object QrControleServSMTP: TWideStringField
      FieldName = 'ServSMTP'
      Origin = 'controle.ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TWideStringField
      FieldName = 'NomeMailOC'
      Origin = 'controle.NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TWideStringField
      FieldName = 'DonoMailOC'
      Origin = 'controle.DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TWideStringField
      FieldName = 'MailOC'
      Origin = 'controle.MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TWideMemoField
      FieldName = 'CorpoMailOC'
      Origin = 'controle.CorpoMailOC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TWideStringField
      FieldName = 'ConexaoDialUp'
      Origin = 'controle.ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TWideStringField
      FieldName = 'MailCCCega'
      Origin = 'controle.MailCCCega'
      Size = 80
    end
    object QrControleMoedaVal: TFloatField
      FieldName = 'MoedaVal'
      Origin = 'controle.MoedaVal'
    end
    object QrControleTela1: TIntegerField
      FieldName = 'Tela1'
      Origin = 'controle.Tela1'
    end
    object QrControleChamarPgtoServ: TIntegerField
      FieldName = 'ChamarPgtoServ'
      Origin = 'controle.ChamarPgtoServ'
    end
    object QrControleFormUsaTam: TIntegerField
      FieldName = 'FormUsaTam'
      Origin = 'controle.FormUsaTam'
    end
    object QrControleFormHeight: TIntegerField
      FieldName = 'FormHeight'
      Origin = 'controle.FormHeight'
    end
    object QrControleFormWidth: TIntegerField
      FieldName = 'FormWidth'
      Origin = 'controle.FormWidth'
    end
    object QrControleFormPixEsq: TIntegerField
      FieldName = 'FormPixEsq'
      Origin = 'controle.FormPixEsq'
    end
    object QrControleFormPixDir: TIntegerField
      FieldName = 'FormPixDir'
      Origin = 'controle.FormPixDir'
    end
    object QrControleFormPixTop: TIntegerField
      FieldName = 'FormPixTop'
      Origin = 'controle.FormPixTop'
    end
    object QrControleFormPixBot: TIntegerField
      FieldName = 'FormPixBot'
      Origin = 'controle.FormPixBot'
    end
    object QrControleFormFoAlt: TIntegerField
      FieldName = 'FormFoAlt'
      Origin = 'controle.FormFoAlt'
    end
    object QrControleFormFoPro: TFloatField
      FieldName = 'FormFoPro'
      Origin = 'controle.FormFoPro'
    end
    object QrControleFormUsaPro: TIntegerField
      FieldName = 'FormUsaPro'
      Origin = 'controle.FormUsaPro'
    end
    object QrControleFormSlides: TIntegerField
      FieldName = 'FormSlides'
      Origin = 'controle.FormSlides'
    end
    object QrControleFormNeg: TIntegerField
      FieldName = 'FormNeg'
      Origin = 'controle.FormNeg'
    end
    object QrControleFormIta: TIntegerField
      FieldName = 'FormIta'
      Origin = 'controle.FormIta'
    end
    object QrControleFormSub: TIntegerField
      FieldName = 'FormSub'
      Origin = 'controle.FormSub'
    end
    object QrControleFormExt: TIntegerField
      FieldName = 'FormExt'
      Origin = 'controle.FormExt'
    end
    object QrControleFormFundoTipo: TIntegerField
      FieldName = 'FormFundoTipo'
      Origin = 'controle.FormFundoTipo'
    end
    object QrControleFormFundoBMP: TWideStringField
      FieldName = 'FormFundoBMP'
      Origin = 'controle.FormFundoBMP'
      Size = 255
    end
    object QrControleServInterv: TIntegerField
      FieldName = 'ServInterv'
      Origin = 'controle.ServInterv'
    end
    object QrControleServAntecip: TIntegerField
      FieldName = 'ServAntecip'
      Origin = 'controle.ServAntecip'
    end
    object QrControleAdiLancto: TIntegerField
      FieldName = 'AdiLancto'
      Origin = 'controle.AdiLancto'
    end
    object QrControleContaSal: TIntegerField
      FieldName = 'ContaSal'
      Origin = 'controle.ContaSal'
    end
    object QrControleContaVal: TIntegerField
      FieldName = 'ContaVal'
      Origin = 'controle.ContaVal'
    end
    object QrControlePronomeE: TWideStringField
      FieldName = 'PronomeE'
      Origin = 'controle.PronomeE'
    end
    object QrControlePronomeM: TWideStringField
      FieldName = 'PronomeM'
      Origin = 'controle.PronomeM'
    end
    object QrControlePronomeF: TWideStringField
      FieldName = 'PronomeF'
      Origin = 'controle.PronomeF'
    end
    object QrControlePronomeA: TWideStringField
      FieldName = 'PronomeA'
      Origin = 'controle.PronomeA'
    end
    object QrControleSaudacaoE: TWideStringField
      FieldName = 'SaudacaoE'
      Origin = 'controle.SaudacaoE'
      Size = 50
    end
    object QrControleSaudacaoM: TWideStringField
      FieldName = 'SaudacaoM'
      Origin = 'controle.SaudacaoM'
      Size = 50
    end
    object QrControleSaudacaoF: TWideStringField
      FieldName = 'SaudacaoF'
      Origin = 'controle.SaudacaoF'
      Size = 50
    end
    object QrControleSaudacaoA: TWideStringField
      FieldName = 'SaudacaoA'
      Origin = 'controle.SaudacaoA'
      Size = 50
    end
    object QrControleNiver: TSmallintField
      FieldName = 'Niver'
      Origin = 'controle.Niver'
    end
    object QrControleNiverddA: TSmallintField
      FieldName = 'NiverddA'
      Origin = 'controle.NiverddA'
    end
    object QrControleNiverddD: TSmallintField
      FieldName = 'NiverddD'
      Origin = 'controle.NiverddD'
    end
    object QrControleLastPassD: TDateTimeField
      FieldName = 'LastPassD'
      Origin = 'controle.LastPassD'
    end
    object QrControleMultiPass: TIntegerField
      FieldName = 'MultiPass'
      Origin = 'controle.MultiPass'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'controle.Codigo'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'controle.AlterWeb'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'controle.Ativo'
    end
    object QrControleCores: TIntegerField
      FieldName = 'Cores'
      Origin = 'controle.Cores'
    end
    object QrControleGradeG: TIntegerField
      FieldName = 'GradeG'
      Origin = 'controle.GradeG'
    end
    object QrControleGrades: TIntegerField
      FieldName = 'Grades'
      Origin = 'controle.Grades'
    end
    object QrControleGradesCors: TIntegerField
      FieldName = 'GradesCors'
      Origin = 'controle.GradesCors'
    end
    object QrControleGradesTams: TIntegerField
      FieldName = 'GradesTams'
      Origin = 'controle.GradesTams'
    end
    object QrControleTamanhos: TIntegerField
      FieldName = 'Tamanhos'
      Origin = 'controle.Tamanhos'
    end
    object QrControleProdutos: TIntegerField
      FieldName = 'Produtos'
      Origin = 'controle.Produtos'
    end
    object QrControleMoviC: TIntegerField
      FieldName = 'MoviC'
      Origin = 'controle.MoviC'
    end
    object QrControleMovix: TIntegerField
      FieldName = 'Movix'
      Origin = 'controle.Movix'
    end
    object QrControleEquiGru: TIntegerField
      FieldName = 'EquiGru'
      Origin = 'controle.EquiGru'
    end
    object QrControleEquiCom: TIntegerField
      FieldName = 'EquiCom'
      Origin = 'controle.EquiCom'
    end
    object QrControleEquiComIts: TIntegerField
      FieldName = 'EquiComIts'
      Origin = 'controle.EquiComIts'
    end
    object QrControleCiclos: TIntegerField
      FieldName = 'Ciclos'
      Origin = 'controle.Ciclos'
    end
    object QrControleCursos: TIntegerField
      FieldName = 'Cursos'
      Origin = 'controle.Cursos'
    end
    object QrControleCursosIts: TIntegerField
      FieldName = 'CursosIts'
      Origin = 'controle.CursosIts'
    end
    object QrControleMoviB: TIntegerField
      FieldName = 'MoviB'
      Origin = 'controle.MoviB'
    end
    object QrControleMovim: TIntegerField
      FieldName = 'Movim'
      Origin = 'controle.Movim'
    end
    object QrControleMoviV: TIntegerField
      FieldName = 'MoviV'
      Origin = 'controle.MoviV'
    end
    object QrControleEntiCtas: TIntegerField
      FieldName = 'EntiCtas'
      Origin = 'controle.EntiCtas'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
      Origin = 'controle.MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MyPerMulta'
      Origin = 'controle.MyPerMulta'
      Calculated = True
    end
    object QrControleEquiConTrf: TIntegerField
      FieldName = 'EquiConTrf'
      Origin = 'controle.EquiConTrf'
    end
    object QrControleCtaEmpPrfC: TIntegerField
      FieldName = 'CtaEmpPrfC'
      Origin = 'controle.CtaEmpPrfC'
    end
    object QrControleCtaEmpPrfD: TIntegerField
      FieldName = 'CtaEmpPrfD'
      Origin = 'controle.CtaEmpPrfD'
    end
    object QrControleCiclosAula: TIntegerField
      FieldName = 'CiclosAula'
      Origin = 'controle.CiclosAula'
    end
    object QrControleCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'controle.CliInt'
    end
    object QrControleContasLnk: TIntegerField
      FieldName = 'ContasLnk'
      Origin = 'controle.ContasLnk'
    end
    object QrControleNome13: TWideStringField
      FieldName = 'Nome13'
      Origin = 'controle.Nome13'
      Size = 15
    end
    object QrControleCtaComProf: TIntegerField
      FieldName = 'CtaComProf'
      Origin = 'controle.CtaComProf'
    end
    object QrControleCtaComProm: TIntegerField
      FieldName = 'CtaComProm'
      Origin = 'controle.CtaComProm'
    end
    object QrControleCta13oProf: TIntegerField
      FieldName = 'Cta13oProf'
      Origin = 'controle.Cta13oProf'
    end
    object QrControleCta13oProm: TIntegerField
      FieldName = 'Cta13oProm'
      Origin = 'controle.Cta13oProm'
    end
    object QrControleCCuPromCom: TIntegerField
      FieldName = 'CCuPromCom'
      Origin = 'controle.CCuPromCom'
    end
    object QrControleCCuPromDTS: TIntegerField
      FieldName = 'CCuPromDTS'
      Origin = 'controle.CCuPromDTS'
    end
    object QrControleCCuProfCom: TIntegerField
      FieldName = 'CCuProfCom'
      Origin = 'controle.CCuProfCom'
    end
    object QrControleCCuProfDTS: TIntegerField
      FieldName = 'CCuProfDTS'
      Origin = 'controle.CCuProfDTS'
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Origin = 'controle.LogoBig1'
      Size = 255
    end
    object QrControleSecuritStr: TWideStringField
      FieldName = 'SecuritStr'
      Origin = 'controle.SecuritStr'
      Size = 32
    end
    object QrControleSenhasIts: TIntegerField
      FieldName = 'SenhasIts'
      Origin = 'controle.SenhasIts'
    end
    object QrControleCxaPgtProf: TIntegerField
      FieldName = 'CxaPgtProf'
      Origin = 'controle.CxaPgtProf'
    end
    object QrControleCtaPagProf: TIntegerField
      FieldName = 'CtaPagProf'
      Origin = 'controle.CtaPagProf'
    end
    object QrControleTxtPgtProf: TWideStringField
      FieldName = 'TxtPgtProf'
      Origin = 'controle.TxtPgtProf'
      Size = 50
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
      Origin = 'controle.MoedaBr'
    end
    object QrControleCiclGradeD: TIntegerField
      FieldName = 'CiclGradeD'
      Origin = 'controle.CiclGradeD'
    end
    object QrControleCiclListPrd: TIntegerField
      FieldName = 'CiclListPrd'
      Origin = 'controle.CiclListPrd'
    end
    object QrControlePtosGradeD: TIntegerField
      FieldName = 'PtosGradeD'
      Origin = 'controle.PtosGradeD'
    end
    object QrControleCasasProd: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CasasProd'
      Origin = 'controle.CasasProd'
      Calculated = True
    end
    object QrControleTxtVdaPto: TWideStringField
      FieldName = 'TxtVdaPto'
      Origin = 'controle.TxtVdaPto'
      Size = 50
    end
    object QrControleCtaPtoVda: TIntegerField
      FieldName = 'CtaPtoVda'
      Origin = 'controle.CtaPtoVda'
    end
    object QrControleMD5Cab: TIntegerField
      FieldName = 'MD5Cab'
      Origin = 'controle.MD5Cab'
    end
    object QrControleMD5Fxa: TIntegerField
      FieldName = 'MD5Fxa'
      Origin = 'controle.MD5Fxa'
    end
    object QrControleCambioCot: TIntegerField
      FieldName = 'CambioCot'
      Origin = 'controle.CambioCot'
    end
    object QrControleCambioMda: TIntegerField
      FieldName = 'CambioMda'
      Origin = 'controle.CambioMda'
    end
    object QrControleCiclosAval: TIntegerField
      FieldName = 'CiclosAval'
      Origin = 'controle.CiclosAval'
    end
    object QrControleGradeD: TIntegerField
      FieldName = 'GradeD'
      Origin = 'controle.GradeD'
    end
    object QrControleGradeK: TIntegerField
      FieldName = 'GradeK'
      Origin = 'controle.GradeK'
    end
    object QrControleGradeKIts: TIntegerField
      FieldName = 'GradeKIts'
      Origin = 'controle.GradeKIts'
    end
    object QrControleGraGruVal: TIntegerField
      FieldName = 'GraGruVal'
      Origin = 'controle.GraGruVal'
    end
    object QrControleMoviVStat: TIntegerField
      FieldName = 'MoviVStat'
      Origin = 'controle.MoviVStat'
    end
    object QrControleStatusV: TIntegerField
      FieldName = 'StatusV'
      Origin = 'controle.StatusV'
    end
    object QrControleEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'controle.EntiContat'
    end
    object QrControleEntiMail: TIntegerField
      FieldName = 'EntiMail'
      Origin = 'controle.EntiMail'
    end
    object QrControleEntiTel: TIntegerField
      FieldName = 'EntiTel'
      Origin = 'controle.EntiTel'
    end
    object QrControleMoviVK: TIntegerField
      FieldName = 'MoviVK'
      Origin = 'controle.MoviVK'
    end
    object QrControleEntiCargos: TIntegerField
      FieldName = 'EntiCargos'
      Origin = 'controle.EntiCargos'
    end
    object QrControleEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'controle.EntiTipCto'
    end
    object QrControleCarteirasU: TIntegerField
      FieldName = 'CarteirasU'
      Origin = 'controle.CarteirasU'
    end
    object QrControleParamsNFs: TIntegerField
      FieldName = 'ParamsNFs'
      Origin = 'controle.ParamsNFs'
    end
    object QrControleContasU: TIntegerField
      FieldName = 'ContasU'
      Origin = 'controle.ContasU'
    end
    object QrControleEntDefAtr: TIntegerField
      FieldName = 'EntDefAtr'
      Origin = 'controle.EntDefAtr'
    end
    object QrControleEntAtrCad: TIntegerField
      FieldName = 'EntAtrCad'
      Origin = 'controle.EntAtrCad'
    end
    object QrControleEntAtrIts: TIntegerField
      FieldName = 'EntAtrIts'
      Origin = 'controle.EntAtrIts'
    end
    object QrControleBalTopoNom: TIntegerField
      FieldName = 'BalTopoNom'
      Origin = 'controle.BalTopoNom'
    end
    object QrControleBalTopoTit: TIntegerField
      FieldName = 'BalTopoTit'
      Origin = 'controle.BalTopoTit'
    end
    object QrControleBalTopoPer: TIntegerField
      FieldName = 'BalTopoPer'
      Origin = 'controle.BalTopoPer'
    end
    object QrControleNCMs: TIntegerField
      FieldName = 'NCMs'
      Origin = 'controle.NCMs'
    end
    object QrControleLctoEndoss: TIntegerField
      FieldName = 'LctoEndoss'
      Origin = 'controle.LctoEndoss'
    end
    object QrControleEntiRespon: TIntegerField
      FieldName = 'EntiRespon'
      Origin = 'controle.EntiRespon'
    end
    object QrControleEntiCfgRel: TIntegerField
      FieldName = 'EntiCfgRel'
      Origin = 'controle.EntiCfgRel'
    end
    object QrControlePtosCadPto: TIntegerField
      FieldName = 'PtosCadPto'
      Origin = 'controle.PtosCadPto'
    end
    object QrControlePediPrzIts: TIntegerField
      FieldName = 'PediPrzIts'
      Origin = 'controle.PediPrzIts'
    end
    object QrControlePediPrzCli: TIntegerField
      FieldName = 'PediPrzCli'
      Origin = 'controle.PediPrzCli'
    end
    object QrControlePtosFatCad: TIntegerField
      FieldName = 'PtosFatCad'
      Origin = 'controle.PtosFatCad'
    end
    object QrControlePtosPedCad: TIntegerField
      FieldName = 'PtosPedCad'
      Origin = 'controle.PtosPedCad'
    end
    object QrControlePtosPedIts: TIntegerField
      FieldName = 'PtosPedIts'
      Origin = 'controle.PtosPedIts'
    end
    object QrControlePtosStqMov: TIntegerField
      FieldName = 'PtosStqMov'
      Origin = 'controle.PtosStqMov'
    end
    object QrControleVendaProS: TIntegerField
      FieldName = 'VendaProS'
      Origin = 'controle.VendaProS'
    end
    object QrControleVPStat1: TIntegerField
      FieldName = 'VPStat1'
      Origin = 'controle.VPStat1'
    end
    object QrControleVPStat2: TIntegerField
      FieldName = 'VPStat2'
      Origin = 'controle.VPStat2'
    end
    object QrControleVPStat3: TIntegerField
      FieldName = 'VPStat3'
      Origin = 'controle.VPStat3'
    end
    object QrControleVPStat1Cor: TIntegerField
      FieldName = 'VPStat1Cor'
      Origin = 'controle.VPStat1Cor'
    end
    object QrControleVPStat2Cor: TIntegerField
      FieldName = 'VPStat2Cor'
      Origin = 'controle.VPStat2Cor'
    end
    object QrControleVPStat3Cor: TIntegerField
      FieldName = 'VPStat3Cor'
      Origin = 'controle.VPStat3Cor'
    end
    object QrControleEndRastre: TWideStringField
      FieldName = 'EndRastre'
      Origin = 'controle.EndRastre'
      Size = 50
    end
    object QrControleTxtVdaPro: TWideStringField
      FieldName = 'TxtVdaPro'
      Origin = 'controle.TxtVdaPro'
      Size = 50
    end
    object QrControleTxtVdaEsq: TWideStringField
      FieldName = 'TxtVdaEsq'
      Origin = 'controle.TxtVdaEsq'
      Size = 15
    end
    object QrControleLogoMoviV: TWideStringField
      FieldName = 'LogoMoviV'
      Origin = 'controle.LogoMoviV'
      Size = 255
    end
    object QrControleCiclCliInt: TIntegerField
      FieldName = 'CiclCliInt'
      Origin = 'controle.CiclCliInt'
    end
    object QrControleCustomFR3: TIntegerField
      FieldName = 'CustomFR3'
      Origin = 'controle.CustomFR3'
    end
    object QrControleVdaCart: TIntegerField
      FieldName = 'VdaCart'
      Origin = 'controle.VdaCart'
    end
    object QrControleVdaConta: TIntegerField
      FieldName = 'VdaConta'
      Origin = 'controle.VdaConta'
    end
    object QrControleVdaConPgto: TIntegerField
      FieldName = 'VdaConPgto'
      Origin = 'controle.VdaConPgto'
    end
    object QrControleVPStat4: TIntegerField
      FieldName = 'VPStat4'
      Origin = 'controle.VPStat4'
    end
    object QrControleVPStat4Cor: TIntegerField
      FieldName = 'VPStat4Cor'
      Origin = 'controle.VPStat4Cor'
    end
    object QrControleContasLnkIts: TIntegerField
      FieldName = 'ContasLnkIts'
      Origin = 'controle.ContasLnkIts'
    end
    object QrControleLinDepto: TIntegerField
      FieldName = 'LinDepto'
      Origin = 'controle.LinDepto'
    end
    object QrControleMoviL: TIntegerField
      FieldName = 'MoviL'
      Origin = 'controle.MoviL'
    end
    object QrControleVPGraPrc: TIntegerField
      FieldName = 'VPGraPrc'
      Origin = 'controle.VPGraPrc'
    end
    object QrControleLinGraPrc: TIntegerField
      FieldName = 'LinGraPrc'
      Origin = 'controle.LinGraPrc'
    end
    object QrControleLinCart: TIntegerField
      FieldName = 'LinCart'
      Origin = 'controle.LinCart'
    end
    object QrControleLinConta: TIntegerField
      FieldName = 'LinConta'
      Origin = 'controle.LinConta'
    end
    object QrControleLinCPgto: TIntegerField
      FieldName = 'LinCPgto'
      Origin = 'controle.LinCPgto'
    end
    object QrControleLinTxt: TWideStringField
      FieldName = 'LinTxt'
      Origin = 'controle.LinTxt'
      Size = 50
    end
    object QrControleWeb_MyURL: TWideStringField
      FieldName = 'Web_MyURL'
      Origin = 'controle.Web_MyURL'
      Size = 255
    end
    object QrControleWeb_FTPu: TWideStringField
      FieldName = 'Web_FTPu'
      Origin = 'controle.Web_FTPu'
      Size = 50
    end
    object QrControleWeb_FTPs: TWideStringField
      FieldName = 'Web_FTPs'
      Origin = 'controle.Web_FTPs'
      Size = 50
    end
    object QrControleWeb_FTPh: TWideStringField
      FieldName = 'Web_FTPh'
      Origin = 'controle.Web_FTPh'
      Size = 255
    end
    object QrControleWeb_Raiz: TWideStringField
      FieldName = 'Web_Raiz'
      Origin = 'controle.Web_Raiz'
      Size = 50
    end
    object QrControlePrdFoto: TWideStringField
      FieldName = 'PrdFoto'
      Origin = 'controle.PrdFoto'
      Size = 255
    end
    object QrControleEventosCad: TSmallintField
      FieldName = 'EventosCad'
      Origin = 'controle.EventosCad'
    end
    object QrControlePrdFotoWeb: TSmallintField
      FieldName = 'PrdFotoWeb'
      Origin = 'controle.PrdFotoWeb'
    end
    object QrControlePrdFotoTam: TIntegerField
      FieldName = 'PrdFotoTam'
      Origin = 'controle.PrdFotoTam'
    end
    object QrControleAvaTexApr: TIntegerField
      FieldName = 'AvaTexApr'
      Origin = 'controle.AvaTexApr'
    end
    object QrControleAvaTexRep: TIntegerField
      FieldName = 'AvaTexRep'
      Origin = 'controle.AvaTexRep'
    end
    object QrControleAvaDir: TWideStringField
      FieldName = 'AvaDir'
      Origin = 'controle.AvaDir'
      Size = 255
    end
    object QrControleCartAval: TIntegerField
      FieldName = 'CartAval'
      Origin = 'controle.CartAval'
    end
    object QrControleAvaFoto: TIntegerField
      FieldName = 'AvaFoto'
      Origin = 'controle.AvaFoto'
    end
    object QrControlePrdGradeD: TIntegerField
      FieldName = 'PrdGradeD'
      Origin = 'controle.PrdGradeD'
    end
    object QrControleAvaGradeD: TIntegerField
      FieldName = 'AvaGradeD'
      Origin = 'controle.AvaGradeD'
    end
    object QrControleProxBAut: TSmallintField
      FieldName = 'ProxBAut'
      Origin = 'controle.ProxBAut'
    end
    object QrControleProxPass: TWideStringField
      FieldName = 'ProxPass'
      Origin = 'controle.ProxPass'
      Size = 100
    end
    object QrControleProxPort: TIntegerField
      FieldName = 'ProxPort'
      Origin = 'controle.ProxPort'
    end
    object QrControleProxServ: TWideStringField
      FieldName = 'ProxServ'
      Origin = 'controle.ProxServ'
      Size = 100
    end
    object QrControleProxUser: TWideStringField
      FieldName = 'ProxUser'
      Origin = 'controle.ProxUser'
      Size = 100
    end
    object QrControleCartAvalS: TIntegerField
      FieldName = 'CartAvalS'
      Origin = 'controle.CartAvalS'
    end
    object QrControleAvaMotivo: TIntegerField
      FieldName = 'AvaMotivo'
      Origin = 'controle.AvaMotivo'
    end
    object QrControleAvaPerc: TIntegerField
      FieldName = 'AvaPerc'
      Origin = 'controle.AvaPerc'
    end
    object QrControleWebLogo: TWideStringField
      FieldName = 'WebLogo'
      Origin = 'controle.WebLogo'
      Size = 255
    end
    object QrControleFotos: TIntegerField
      FieldName = 'Fotos'
      Origin = 'controle.Fotos'
    end
    object QrControleCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Origin = 'controle.CNAB_Cfg'
    end
    object QrControleCNAB_Lei: TIntegerField
      FieldName = 'CNAB_Lei'
      Origin = 'controle.CNAB_Lei'
    end
    object QrControleCNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
      Origin = 'controle.CNAB_Lot'
    end
    object QrControleWeb_MySQL: TSmallintField
      FieldName = 'Web_MySQL'
      Origin = 'controle.Web_MySQL'
    end
    object QrControleWeb_Page: TWideStringField
      FieldName = 'Web_Page'
      Origin = 'controle.Web_Page'
      Size = 255
    end
    object QrControleWeb_Host: TWideStringField
      FieldName = 'Web_Host'
      Origin = 'controle.Web_Host'
      Size = 255
    end
    object QrControleWeb_User: TWideStringField
      FieldName = 'Web_User'
      Origin = 'controle.Web_User'
      Size = 30
    end
    object QrControleWeb_Pwd: TWideStringField
      FieldName = 'Web_Pwd'
      Origin = 'controle.Web_Pwd'
      Size = 30
    end
    object QrControleWeb_DB: TWideStringField
      FieldName = 'Web_DB'
      Origin = 'controle.Web_DB'
      Size = 50
    end
    object QrControleMoviVCaPag: TIntegerField
      FieldName = 'MoviVCaPag'
      Origin = 'controle.MoviVCaPag'
    end
    object QrControleValCur: TIntegerField
      FieldName = 'ValCur'
      Origin = 'controle.ValCur'
    end
    object QrControleValCurPrd: TIntegerField
      FieldName = 'ValCurPrd'
      Origin = 'controle.ValCurPrd'
    end
    object QrControleMoviCCons: TIntegerField
      FieldName = 'MoviCCons'
      Origin = 'controle.MoviCCons'
    end
    object QrControleAvaEmpresa: TIntegerField
      FieldName = 'AvaEmpresa'
      Origin = 'controle.AvaEmpresa'
    end
    object QrControleFatSEtqKit: TSmallintField
      FieldName = 'FatSEtqKit'
      Origin = 'controle.FatSEtqKit'
    end
    object QrControleFatSEtqLin: TSmallintField
      FieldName = 'FatSEtqLin'
      Origin = 'controle.FatSEtqLin'
    end
    object QrControlePassivo: TSmallintField
      FieldName = 'Passivo'
    end
  end
  object QrEstoque: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, produtosmits pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM>=:P1'
      'GROUP BY pm.Tipo')
    Left = 428
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstoqueTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrMin: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Min(Periodo) Periodo'
      'FROM balancos')
    Left = 372
    Top = 325
    object QrMinPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'DBMBWET.balancos.Periodo'
    end
  end
  object QrBalancosIts: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,'
      'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G'
      'FROM balancosits'
      'WHERE Produto=:P0'
      'AND Periodo=:P1'
      '')
    Left = 400
    Top = 324
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMBWET.balancosits.EstqQ'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMBWET.balancosits.EstqV'
    end
    object QrBalancosItsEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrBalancosItsEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
  end
  object QrEstqperiodo: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, produtosmits pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM BETWEEN :P1 AND :P2'
      'GROUP BY pm.Tipo')
    Left = 456
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqperiodoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqperiodoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstqperiodoValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrTerminal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 4
  end
  object QrNTI: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 48
  end
  object ZZDB: TmySQLDatabase
    DatabaseName = 'lesew'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=lesew'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 96
    Top = 3
  end
  object QrPerfis: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 300
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrTerceiro: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 196
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyLocDatabase
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrAgora: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 60
    Top = 95
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminais: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrEndereco: TmySQLQuery
    Database = MyDB
    OnCalcFields = QrEnderecoCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 60
    Top = 342
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderecoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnderecoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEnderecoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrEnderecoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderecoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEnderecoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEnderecoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderecoNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrEnderecoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderecoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderecoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEnderecoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderecoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderecoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEnderecoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrEnderecoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderecoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEnderecoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderecoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderecoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEnderecoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEnderecoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEnderecoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEnderecoE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEnderecoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEnderecoNO_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Calculated = True
    end
  end
  object QrBoss: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 340
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrBSit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 368
    Top = 376
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSenhas: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.Funcionario, se.IP_Default'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 396
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
      Origin = 'senhas.Funcionario'
    end
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Origin = 'senhas.Senha'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'senhas.Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'senhas.Perfil'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Origin = 'senhas.IP_Default'
      Size = 40
    end
  end
  object QrSSit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 424
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 544
    Top = 41
  end
  object QrCarteiras: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT DISTINCT ca.*, '
      'CASE WHEN ba.Codigo=0 THEN "" ELSE ba.Nome END NOMEDOBANCO, '
      
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFOR' +
        'NECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI'
      'WHERE ca.Codigo>0')
    Left = 516
    Top = 41
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
      DisplayFormat = '000'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 128
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 128
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 128
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCarteirasNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Origin = 'carteiras.Nome'
      FixedChar = True
      Size = 100
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCarteirasTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCarteirasUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCarteirasUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCarteirasNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCarteirasPagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Required = True
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Required = True
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      DisplayFormat = '000'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
      DisplayFormat = '000000'
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Required = True
      Size = 100
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
  end
  object QrDono: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX,' +
        ' '
      'IF(en.Tipo=0,en.ELograd ,en.PLograd) + 0.000 Lograd,'
      'IF(en.Tipo=0,en.ECEP ,en.PCEP) + 0.000 CEP'
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 12
    Top = 330
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrDonoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDonoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrDonoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrDonoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrDonoECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrDonoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TWideStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 60
    end
    object QrDonoRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 60
    end
    object QrDonoLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrDonoCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object QrEstqR: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Qtd) Qtd, SUM(Val) Val'
      'FROM movim'
      'WHERE Grade= :P0'
      'AND Cor= :P1'
      'AND Tam= :P2'
      'AND DataReal>= :P3')
    Left = 516
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEstqRQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrEstqRVal: TFloatField
      FieldName = 'Val'
    end
  end
  object QrEstqP: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Qtd) Qtd'
      'FROM movim'
      'WHERE Grade= :P0'
      'AND Cor= :P1'
      'AND Tam= :P2'
      'AND DataPedi >= :P3 '
      'AND DataReal < 2')
    Left = 544
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEstqPQtd: TFloatField
      FieldName = 'Qtd'
    end
  end
  object QrSel: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'UNLOCK TABLES;'
      'UPDATE controle SET Movix=Movix+1;'
      'LOCK TABLES controle READ;'
      'SELECT Movix FROM controle;'
      'UNLOCK TABLES;')
    Left = 236
    Top = 292
    object QrSelMovix: TIntegerField
      FieldName = 'Movix'
    end
  end
  object QrUpdW: TmySQLQuery
    Database = MyDB
    Left = 484
    Top = 7
  end
  object frxDsDono: TfrxDBDataset
    UserName = 'frxDsDono'
    CloseDataSource = False
    DataSet = QrDono
    BCDToCurrency = False
    Left = 12
    Top = 381
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    Left = 40
    Top = 144
  end
  object DsOptImpConfig: TDataSource
    DataSet = QrOptImpConfig
    Left = 517
    Top = 381
  end
  object QrOptImpConfig: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT oi.*, im.Nome '
      'FROM optimpconfig oi'
      'LEFT JOIN imprime im ON im.Codigo=oi.Codigo'
      'ORDER BY Codigo')
    Left = 489
    Top = 381
    object QrOptImpConfigCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOptImpConfigNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOptImpConfigTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrOptImpConfigNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 40
      Calculated = True
    end
  end
  object QrSerialUC: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo, Senha '
      'FROM md5cab'
      'WHERE Ativo=1'
      'AND Forca=0'
      'AND Senha = BINARY :P0'
      '')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSerialUCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSerialUCSenha: TWideStringField
      FieldName = 'Senha'
      Size = 8
    end
  end
  object QrSerialUF: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM md5fxa'
      'WHERE Codigo=:P0'
      'AND :P1 BETWEEN NumIni AND NumFim')
    Left = 708
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSerialUFControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSerialMC: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, Cab.Senha, fxa.NumIni, fxa.NumFim'
      'FROM md5fxa fxa'
      'LEFT JOIN md5cab cab ON cab.Codigo=fxa.Codigo'
      'WHERE cab.Ativo=1'
      'AND cab.Forca=1'
      '')
    Left = 708
    Top = 104
    object QrSerialMCCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSerialMCSenha: TWideStringField
      FieldName = 'Senha'
      Required = True
      Size = 8
    end
    object QrSerialMCNumIni: TIntegerField
      FieldName = 'NumIni'
      Required = True
    end
    object QrSerialMCNumFim: TIntegerField
      FieldName = 'NumFim'
      Required = True
    end
  end
  object QrDuplMD5: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Entidade'
      'FROM md5alu'
      'WHERE MD5Cab=:P0'
      'AND MD5Num=:P1')
    Left = 708
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplMD5Entidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object MyDBn: TmySQLDatabase
    DatabaseName = 'magistral'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'DatabaseName=magistral')
    DatasetOptions = []
    Left = 112
    Top = 55
  end
  object QrWeb: TmySQLQuery
    Database = MyDBn
    Left = 112
    Top = 108
  end
  object QrUpdZ: TmySQLQuery
    Database = MyDB
    Left = 432
    Top = 3
  end
end
