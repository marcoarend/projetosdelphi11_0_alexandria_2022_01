object FmLeSew_MLA: TFmLeSew_MLA
  Left = 399
  Top = 261
  Caption = 'LeSew'
  ClientHeight = 183
  ClientWidth = 414
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 414
    Height = 76
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 80
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 224
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label3: TLabel
      Left = 8
      Top = 24
      Width = 43
      Height = 13
      Caption = 'Terminal:'
    end
    object LaSenhas: TLabel
      Left = 80
      Top = 62
      Width = 80
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaSenhasClick
      OnMouseEnter = LaSenhasMouseEnter
      OnMouseLeave = LaSenhasMouseLeave
    end
    object LaConexao: TLabel
      Left = 268
      Top = 62
      Width = 96
      Height = 13
      Cursor = crHandPoint
      Caption = 'Gerenciar Conex'#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaConexaoClick
      OnMouseEnter = LaConexaoMouseEnter
      OnMouseLeave = LaConexaoMouseLeave
    end
    object EdLogin: TEdit
      Left = 80
      Top = 40
      Width = 140
      Height = 20
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 0
      OnKeyDown = EdLoginKeyDown
    end
    object EdSenha: TEdit
      Left = 224
      Top = 40
      Width = 140
      Height = 20
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
      OnKeyDown = EdSenhaKeyDown
    end
    object EdTerminal: TEdit
      Left = 8
      Top = 40
      Width = 65
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      OnKeyDown = EdLoginKeyDown
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 135
    Width = 414
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtEntra: TBitBtn
      Tag = 14
      Left = 28
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtEntraClick
    end
    object BtSair: TBitBtn
      Tag = 15
      Left = 304
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Fechar'
      TabOrder = 1
      OnClick = BtSairClick
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 76
    Width = 414
    Height = 59
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 410
      Height = 42
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 25
        Width = 410
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 128
    Top = 8
    object Close1: TMenuItem
      Caption = '&Close'
      OnClick = Close1Click
    end
  end
end
