unit MoviBIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBCtrls, Db, mySQLDbTables,
  dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, UnDmkEnums;

type
  TFmMoviBIts = class(TForm)
    PainelConfirma: TPanel;
    BtConfItem: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel4: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdGrade: TdmkEditCB;
    EdCorCod: TEdit;
    EdCorNom: TEdit;
    EdTamCod: TEdit;
    EdTamNom: TEdit;
    EdQtd: TEdit;
    EdPrc: TEdit;
    EdVal: TEdit;
    CBGrade: TdmkDBLookupComboBox;
    GradeA: TStringGrid;
    GradeC: TStringGrid;
    StaticText1: TStaticText;
    QrProdutos: TmySQLQuery;
    QrProdutosTam: TIntegerField;
    QrProdutosCor: TIntegerField;
    QrProdutosAtivo: TSmallintField;
    QrProdutosConta: TIntegerField;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    DsGrades: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    QrGradesCodUsu: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdGradeChange(Sender: TObject);
    procedure EdQtdEnter(Sender: TObject);
    procedure EdQtdExit(Sender: TObject);
    procedure EdPrcEnter(Sender: TObject);
    procedure EdPrcExit(Sender: TObject);
    procedure EdValEnter(Sender: TObject);
    procedure EdValExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeAClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtConfItemClick(Sender: TObject);
  private
    { Private declarations }
    FQtd, FPrc, FVal: Double;
    procedure ReopenGradesTams(Grade: Integer);
    procedure ReopenGradesCors(Grade: Integer);
    procedure ReopenProdutos(Grade: Integer);
    procedure ConfigGrades;
    procedure AtualizaGradeA(Grade: Integer);
    procedure LimpaEdits;
    procedure CalculaPreco(Como: TCalcVal);
    //procedure AtualizaValores(MoviC, Movim, Pagto: Integer);

  public
    { Public declarations }
    FMoviBControle: Integer;
  end;

  var
  FmMoviBIts: TFmMoviBIts;

implementation

{$R *.DFM}

uses UnInternalConsts, MyVCLSkin, Module, UMySQLModule, MoviB, ModuleProd,
UnMyObjects;

procedure TFmMoviBIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviBIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviBIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMoviBIts.EdGradeChange(Sender: TObject);
begin
  ConfigGrades;
  LimpaEdits;
end;

procedure TFmMoviBIts.ReopenGradesCors(Grade: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Grade;
  QrGradesCors.Open;
end;

procedure TFmMoviBIts.ReopenGradesTams(Grade: Integer);
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Grade;
  QrGradesTams.Open;
end;

procedure TFmMoviBIts.ReopenProdutos(Grade: Integer);
begin
  QrProdutos.Close;
  QrProdutos.Params[00].AsInteger := FMoviBControle;
  QrProdutos.Params[01].AsInteger := Grade;
  QrProdutos.Open;
end;

procedure TFmMoviBIts.ConfigGrades;
var
  c, l{, ci, li}, g: Integer;
begin
  for c := 0 to GradeA.ColCount - 1 do
    for l := 0 to GradeA.ColCount - 1 do
      GradeA.Cells[c, l] := '';
  g := Geral.IMV(EdGrade.Text);
  if g > 0 then
  begin
    g := QrGradesCodigo.Value;
    ReopenGradesCors(g);
    ReopenGradesTams(g);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    GradeA.ColCount  := c;
    GradeA.RowCount  := l;
    GradeA.FixedCols := 1;
    GradeA.FixedRows := 1;
    //
    GradeC.ColCount  := c;
    GradeC.RowCount  := l;
    //
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      GradeA.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      GradeC.Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      GradeA.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      GradeC.Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value);
      QrGradesTams.Next;
    end;
    AtualizaGradeA(g);
  end else begin
    GradeA.ColCount := 1;
    GradeA.RowCount := 1;
  end;
end;

procedure TFmMoviBIts.AtualizaGradeA(Grade: Integer);
var
  c, l: Integer;
begin
  ReopenProdutos(Grade);
  QrProdutos.First;
  while not QrProdutos.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutosTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutosCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutosConta.Value > 0 then
          GradeA.Cells[c, l] := '2'
        else
          GradeA.Cells[c, l] := '1';
      end;
    end;
    QrProdutos.Next;
  end;
end;

procedure TFmMoviBIts.LimpaEdits;
begin
  EdTamCod.Text := '';
  EdCorCod.Text := '';
  //
  EdTamNom.Text := '';
  EdCorNom.Text := '';
  //
end;

procedure TFmMoviBIts.CalculaPreco(Como: TCalcVal);
var
  Qtd, Prc, Val: Double;
begin
  Qtd := Geral.DMV(EdQtd.Text);
  Prc := Geral.DMV(EdPrc.Text);
  Val := Geral.DMV(EdVal.Text);
  case Como of
    tcvQtd: Val := Qtd * Prc;
    tcvPrc: Val := Qtd * Prc;
    tcvVal: if Qtd = 0 then Prc := 0 else Prc := Val / Qtd;
  end;
  EdQtd.Text := Geral.FFT(Qtd, 2, siPositivo);
  EdPrc.Text := Geral.FFT(Prc, 6, siPositivo);
  EdVal.Text := Geral.FFT(Val, 2, siPositivo);
end;

procedure TFmMoviBIts.EdQtdEnter(Sender: TObject);
begin
  FQtd := Geral.DMV(EdQtd.Text);
end;

procedure TFmMoviBIts.EdQtdExit(Sender: TObject);
begin
  EdQtd.Text := Geral.TFT(EdQtd.Text, 3, siNegativo);
  if Geral.DMV(EdQtd.Text) <> FQtd then CalculaPreco(tcvQtd);
end;

procedure TFmMoviBIts.EdPrcEnter(Sender: TObject);
begin
  FPrc := Geral.DMV(EdPrc.Text);
end;

procedure TFmMoviBIts.EdPrcExit(Sender: TObject);
begin
  EdPrc.Text := Geral.TFT(EdPrc.Text, 6, siPositivo);
  if Geral.DMV(EdPrc.Text) <> FPrc then CalculaPreco(tcvPrc);
end;

procedure TFmMoviBIts.EdValEnter(Sender: TObject);
begin
  FVal := Geral.DMV(EdVal.Text);
end;

procedure TFmMoviBIts.EdValExit(Sender: TObject);
begin
  EdVal.Text := Geral.TFT(EdVal.Text, 2, siNegativo);
  if Geral.DMV(EdVal.Text) <> FVal then CalculaPreco(tcvVal);
end;

procedure TFmMoviBIts.FormCreate(Sender: TObject);
begin
  GradeA.Align        := alClient;
  GradeA.ColWidths[0] := 100;
  FQtd := 0;
  FPrc := 0;
  FVal := 0;
  QrGrades.Open;
end;

procedure TFmMoviBIts.GradeAClick(Sender: TObject);
var
  //Status: Integer;
  QtdX, ValX, CusX: Double;
begin
//  Status := 0;
  if (GradeA.Col <> 0) or (GradeA.Row <> 0) then
  begin
    EdTamCod.Text := GradeC.Cells[GradeA.Col, 0];
    EdCorCod.Text := GradeC.Cells[0, GradeA.Row];
    //
    EdTamNom.Text := GradeA.Cells[GradeA.Col, 0];
    EdCorNom.Text := GradeA.Cells[0, GradeA.Row];
    //
    DmProd.AtualizaEstoqueMercadoria(QrGradesCodigo.Value, Geral.IMV(
      EdCorCod.Text), Geral.IMV(EdTamCod.Text), True, True, QtdX, ValX, CusX);
    EdPrc.Text := Geral.FFT(CusX, 6, siPositivo);
  end else LimpaEdits;
  //BtConfItem.Enabled := Geral.IntToBool_0(Status);
  EdQtd.SetFocus;
end;

procedure TFmMoviBIts.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol <> 0) and (ARow <> 0) then
    MeuVCLSkin.DrawGrid3(GradeA, Rect, 1, Geral.IMV(GradeA.Cells[ACol, ARow]));
end;

procedure TFmMoviBIts.BtConfItemClick(Sender: TObject);
var
  Conta, Gra, Cor, Tam: Integer;
  Qtd, Val: Double;
  QtdX, ValX, CusX: Double;
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO movim SET Motivo=2, '); // Ajuste de balanco
  Dmod.QrUpd.SQL.Add('Controle=:P0, Conta=:P1, Grade=:P2, Cor=:P3, Tam=:P4, ');
  Dmod.QrUpd.SQL.Add('Qtd=:P5, Val=:P6, DataReal=:P7');
  //
  Gra := QrGradesCodigo.Value;
  Cor := Geral.IMV(EdCorCod.Text);
  Tam := Geral.IMV(EdTamCod.Text);
  Qtd := Geral.DMV(EdQtd.Text);
  Val := Geral.DMV(EdVal.Text);
  //
  DmProd.AtualizaEstoqueMercadoria(Gra, Cor, Tam, True, True, QtdX, ValX, CusX);
  Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle', 'movim',
    'movim', 'conta');
  //
  Dmod.QrUpd.Params[00].AsInteger := FMoviBControle;
  Dmod.QrUpd.Params[01].AsInteger := Conta;
  Dmod.QrUpd.Params[02].AsInteger := Gra;
  Dmod.QrUpd.Params[03].AsInteger := Cor;
  Dmod.QrUpd.Params[04].AsInteger := Tam;
  Dmod.QrUpd.Params[05].AsFloat   := Qtd - QtdX;
  Dmod.QrUpd.Params[06].AsFloat   := Val - ValX;
  Dmod.QrUpd.Params[07].AsString  := Geral.FDT(Date, 1);
  Dmod.QrUpd.ExecSQL;
  //
  EdQtd.Text := '0,000';
  EdQtd.SetFocus;
  EdPrc.Text := '0,000000';
  EdVal.Text := '0,00';
  ConfigGrades;
  FmMoviB.ReindexaTabela(True);
  Screen.Cursor := crDefault;
end;

end.

