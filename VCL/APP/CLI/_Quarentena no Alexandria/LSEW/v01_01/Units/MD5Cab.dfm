object FmMD5Cab: TFmMD5Cab
  Left = 368
  Top = 194
  Caption = 'CIC-CRYPT-001 :: Cadastro de Senhas de Alunos'
  ClientHeight = 456
  ClientWidth = 708
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 708
    Height = 408
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 706
      Height = 100
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 4
        Top = 52
        Width = 135
        Height = 13
        Caption = 'Senha (de 5 a 8 caracteres):'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMD5Cab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 64
        Top = 20
        Width = 633
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsMD5Cab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object RadioGroup1: TDBRadioGroup
        Left = 148
        Top = 44
        Width = 241
        Height = 45
        Caption = ' Tipo de senha: '
        Columns = 2
        DataField = 'Forca'
        DataSource = DsMD5Cab
        Items.Strings = (
          '12 a 16 caracteres'
          '32 carateres')
        ParentBackground = True
        TabOrder = 2
        Values.Strings = (
          '0'
          '1')
      end
      object RadioGroup2: TDBRadioGroup
        Left = 544
        Top = 44
        Width = 153
        Height = 45
        Caption = ' Status: '
        Columns = 2
        DataField = 'Ativo'
        DataSource = DsMD5Cab
        Items.Strings = (
          'Encerrado'
          'Aberto')
        ParentBackground = True
        TabOrder = 3
        Values.Strings = (
          '0'
          '1')
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 68
        Width = 137
        Height = 21
        DataField = 'Senha'
        DataSource = DsMD5Cab
        TabOrder = 4
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 392
        Top = 44
        Width = 149
        Height = 45
        Caption = ' Quartos obrigat. (32 carac.): '
        Columns = 5
        DataField = 'Quartos'
        DataSource = DsMD5Cab
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4')
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 359
      Width = 706
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 236
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Faixa'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
        end
        object BtGrupo: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtGrupoClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtAlunos: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Alunos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAlunosClick
        end
      end
    end
    object PanelGrids: TPanel
      Left = 1
      Top = 240
      Width = 706
      Height = 119
      Align = alBottom
      TabOrder = 2
      object DBGImp: TDBGrid
        Left = 1
        Top = 1
        Width = 280
        Height = 117
        Align = alLeft
        DataSource = DsMD5Fxa
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Controle'
            Title.Alignment = taCenter
            Title.Caption = 'Faixa'
            Width = 40
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumIni'
            Title.Alignment = taCenter
            Title.Caption = 'In'#237'cio'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumFim'
            Title.Alignment = taCenter
            Title.Caption = 'Final'
            Width = 100
            Visible = True
          end>
      end
      object DBGAlu: TDBGrid
        Left = 281
        Top = 1
        Width = 424
        Height = 117
        Align = alClient
        DataSource = DsMD5Alu
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Entidade'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEALU'
            Title.Caption = 'Nome aluno'
            Width = 296
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MD5Num'
            Title.Caption = 'N'#250'mero'
            Width = 43
            Visible = True
          end>
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 708
    Height = 408
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 359
      Width = 706
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 597
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 706
      Height = 108
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label9: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 4
        Top = 52
        Width = 135
        Height = 13
        Caption = 'Senha (de 5 a 8 caracteres):'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 64
        Top = 20
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdSenha: TdmkEdit
        Left = 4
        Top = 68
        Width = 137
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Senha'
        UpdCampo = 'Senha'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGForca: TdmkRadioGroup
        Left = 148
        Top = 44
        Width = 241
        Height = 45
        Caption = ' Tipo de senha: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          '12 a 16 caracteres'
          '32 carateres')
        TabOrder = 3
        QryCampo = 'Forca'
        UpdCampo = 'Forca'
        UpdType = utYes
        OldValor = 0
      end
      object RGAtivo: TdmkRadioGroup
        Left = 544
        Top = 44
        Width = 153
        Height = 45
        Caption = ' Status: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Encerrado'
          'Aberto')
        TabOrder = 5
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        OldValor = 0
      end
      object RGQuartos: TdmkRadioGroup
        Left = 392
        Top = 44
        Width = 149
        Height = 45
        Caption = ' Quartos obrigat. (32 carac.): '
        Columns = 5
        ItemIndex = 4
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4')
        TabOrder = 4
        QryCampo = 'Quartos'
        UpdCampo = 'Quartos'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 708
    Height = 48
    Align = alTop
    Caption = '                              Cadastro de Senhas de Alunos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 625
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 399
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsMD5Cab: TDataSource
    DataSet = QrMD5Cab
    Left = 40
    Top = 12
  end
  object QrMD5Cab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMD5CabBeforeOpen
    AfterOpen = QrMD5CabAfterOpen
    BeforeClose = QrMD5CabBeforeClose
    AfterScroll = QrMD5CabAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Senha, Forca, Ativo, Quartos'
      'FROM md5cab')
    Left = 12
    Top = 12
    object QrMD5CabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMD5CabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrMD5CabSenha: TWideStringField
      FieldName = 'Senha'
      Size = 8
    end
    object QrMD5CabForca: TSmallintField
      FieldName = 'Forca'
    end
    object QrMD5CabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMD5CabQuartos: TSmallintField
      FieldName = 'Quartos'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtGrupo
    CanUpd01 = BtItens
    Left = 72
    Top = 12
  end
  object QrMD5Fxa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mdi.Controle, mdi.NumIni, mdi.NumFim'
      'FROM md5fxa mdi'
      'WHERE mdi.Codigo=:P0'
      'ORDER BY mdi.NumIni, mdi.NumFim')
    Left = 636
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMD5FxaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000'
    end
    object QrMD5FxaNumIni: TIntegerField
      FieldName = 'NumIni'
      Required = True
      DisplayFormat = '000000'
    end
    object QrMD5FxaNumFim: TIntegerField
      FieldName = 'NumFim'
      Required = True
      DisplayFormat = '000000'
    end
  end
  object DsMD5Fxa: TDataSource
    DataSet = QrMD5Fxa
    Left = 664
    Top = 4
  end
  object PMGrupo: TPopupMenu
    Left = 264
    Top = 368
    object Incluinovogrupodesenhas1: TMenuItem
      Caption = '&Inclui novo grupo de senhas'
      OnClick = Incluinovogrupodesenhas1Click
    end
    object Alteragrupodesenhas1: TMenuItem
      Caption = '&Altera grupo de senhas'
      OnClick = Alteragrupodesenhas1Click
    end
  end
  object PMItens: TPopupMenu
    Left = 352
    Top = 368
    object Incluinovasequnciadesenhas1: TMenuItem
      Caption = '&Inclui nova faixa de senhas'
      OnClick = Incluinovasequnciadesenhas1Click
    end
    object Imprimeafaixaatual1: TMenuItem
      Caption = 'Imprime a faixa &atual'
      OnClick = Imprimeafaixaatual1Click
    end
  end
  object QrMD5Alu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT alu.Entidade, alu.MD5Num, alu.MD5Cab, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEALU'
      'FROM md5alu alu'
      'LEFT JOIN entidades ent ON ent.Codigo=alu.Entidade'
      'WHERE alu.MD5Cab=:P0')
    Left = 636
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMD5AluEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrMD5AluMD5Num: TIntegerField
      Alignment = taCenter
      FieldName = 'MD5Num'
      Required = True
      DisplayFormat = '0000'
    end
    object QrMD5AluMD5Cab: TIntegerField
      FieldName = 'MD5Cab'
      Required = True
    end
    object QrMD5AluNOMEALU: TWideStringField
      FieldName = 'NOMEALU'
      Size = 100
    end
  end
  object DsMD5Alu: TDataSource
    DataSet = QrMD5Alu
    Left = 664
    Top = 32
  end
  object PMAlunos: TPopupMenu
    Left = 444
    Top = 368
    object Adicionanovoaluno1: TMenuItem
      Caption = '&Adiciona novo aluno'
      OnClick = Adicionanovoaluno1Click
    end
    object Retiraalunoatual1: TMenuItem
      Caption = '&Retira aluno atual'
      OnClick = Retiraalunoatual1Click
    end
  end
end
