object FmMoviLEdit: TFmMoviLEdit
  Left = 339
  Top = 185
  Caption = 'LIN-VENDA-002 :: Edi'#231#227'o de venda de lingerie'
  ClientHeight = 844
  ClientWidth = 1161
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1161
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Venda de mercadorias'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Berlin Sans FB'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1057
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 1058
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 258
    Width = 1161
    Height = 517
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 1
    object PCProd: TPageControl
      Left = 1
      Top = 1
      Width = 1159
      Height = 515
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 1158
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mercadorias'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1151
          Height = 108
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1148
          object Label12: TLabel
            Left = 4
            Top = 55
            Width = 72
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mercadoria:'
          end
          object Label15: TLabel
            Left = 837
            Top = 55
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade:'
          end
          object Label16: TLabel
            Left = 967
            Top = 55
            Width = 34
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Total:'
          end
          object Label8: TLabel
            Left = 837
            Top = 4
            Width = 53
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Estoque:'
          end
          object Label11: TLabel
            Left = 967
            Top = 4
            Width = 35
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor:'
          end
          object Label13: TLabel
            Left = 453
            Top = 55
            Width = 24
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cor:'
          end
          object Label14: TLabel
            Left = 647
            Top = 55
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tamanho:'
          end
          object Label17: TLabel
            Left = 4
            Top = 5
            Width = 109
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Leitura / digita'#231#227'o:'
          end
          object EdProd: TdmkEditCB
            Left = 4
            Top = 75
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdProdChange
            DBLookupComboBox = CBProd
            IgnoraDBLookupComboBox = False
          end
          object CBProd: TdmkDBLookupComboBox
            Left = 76
            Top = 75
            Width = 361
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsGrades
            TabOrder = 1
            OnExit = CBProdExit
            dmkEditCB = EdProd
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProdQtd: TdmkEdit
            Left = 837
            Top = 75
            Width = 123
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdProdQtdChange
          end
          object EdProdTot: TdmkEdit
            Left = 967
            Top = 75
            Width = 123
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object BtConfItem: TBitBtn
            Tag = 14
            Left = 1098
            Top = 52
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            OnClick = BtConfItemClick
          end
          object EdCor: TdmkEditCB
            Left = 588
            Top = 46
            Width = 43
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCorChange
            DBLookupComboBox = CBCor
            IgnoraDBLookupComboBox = False
          end
          object CBCor: TdmkDBLookupComboBox
            Left = 453
            Top = 75
            Width = 178
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Cor'
            ListField = 'NOMECOR'
            ListSource = DsGradesCors
            TabOrder = 3
            dmkEditCB = EdCor
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTam: TdmkEditCB
            Left = 786
            Top = 46
            Width = 44
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdTamChange
            DBLookupComboBox = CBTam
            IgnoraDBLookupComboBox = False
          end
          object CBTam: TdmkDBLookupComboBox
            Left = 651
            Top = 75
            Width = 179
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Tam'
            ListField = 'NOMETAM'
            ListSource = DsGradesTams
            TabOrder = 5
            dmkEditCB = EdTam
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProdVal: TdmkEdit
            Left = 967
            Top = 25
            Width = 123
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdEstQ: TdmkEdit
            Left = 837
            Top = 25
            Width = 123
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdLeituraCodigoDeBarras: TEdit
            Left = 4
            Top = 25
            Width = 433
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            MaxLength = 20
            TabOrder = 11
            OnChange = EdLeituraCodigoDeBarrasChange
            OnKeyDown = EdLeituraCodigoDeBarrasKeyDown
          end
          object CkFixo: TCheckBox
            Left = 453
            Top = 25
            Width = 154
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade fixa.'
            TabOrder = 12
            OnClick = CkFixoClick
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 108
          Width = 839
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 368
          object StaticText1: TStaticText
            Left = 0
            Top = 351
            Width = 148
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Ctrl + Delete para excluir'
            TabOrder = 0
          end
          object DBGridProd: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 839
            Height = 351
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tam'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
            Color = clWindow
            DataSource = FmMoviL.DsMovim
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGridProdCellClick
            OnKeyDown = DBGridProdKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tam'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 844
          Top = 108
          Width = 307
          Height = 376
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          ExplicitLeft = 841
          ExplicitHeight = 368
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 308
            Height = 224
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Image2: TImage
              Left = 6
              Top = 0
              Width = 296
              Height = 209
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Stretch = True
            end
          end
        end
      end
    end
  end
  object PnTopo: TPanel
    Left = 0
    Top = 59
    Width = 1161
    Height = 135
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 0
    object Panel8: TPanel
      Left = 1
      Top = 70
      Width = 1159
      Height = 63
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 1158
      object Panel19: TPanel
        Left = 0
        Top = 0
        Width = 400
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 5
          Top = 7
          Width = 73
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pedido:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -23
          Font.Name = 'Berlin Sans FB'
          Font.Style = []
          ParentFont = False
        end
        object EdCodigo: TdmkEdit
          Left = 94
          Top = 9
          Width = 123
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1159
      Height = 69
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1158
      object Label1: TLabel
        Left = 265
        Top = 7
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Cliente:'
      end
      object SBCliente: TSpeedButton
        Left = 794
        Top = 4
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SBClienteClick
      end
      object Label26: TLabel
        Left = 223
        Top = 38
        Width = 88
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lista de pre'#231'o:'
      end
      object Label84: TLabel
        Left = 830
        Top = 7
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro por:'
      end
      object Label85: TLabel
        Left = 833
        Top = 38
        Width = 77
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Alterado por:'
      end
      object Label10: TLabel
        Left = 11
        Top = 7
        Width = 78
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data pedido:'
      end
      object Label3: TLabel
        Left = 11
        Top = 38
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data venda:'
      end
      object EdCliente: TdmkEditCB
        Left = 318
        Top = 4
        Width = 68
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 388
        Top = 4
        Width = 400
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTI'
        ListSource = DsCliente
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdListaPre: TdmkEditCB
        Left = 318
        Top = 34
        Width = 68
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBListaPre
        IgnoraDBLookupComboBox = False
      end
      object CBListaPre: TdmkDBLookupComboBox
        Left = 388
        Top = 34
        Width = 400
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 3
        OnExit = CBListaPreExit
        dmkEditCB = EdListaPre
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDataPed: TdmkEdit
        Left = 94
        Top = 4
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDate
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '12/11/2009'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 40129d
        ValWarn = False
      end
      object EdNomeCad: TdmkEdit
        Left = 916
        Top = 4
        Width = 234
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdNomeCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdNomeCad'
        ValWarn = False
      end
      object EdNomeAlt: TdmkEdit
        Left = 916
        Top = 34
        Width = 234
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdNomeCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdNomeCad'
        ValWarn = False
      end
      object EdDataVenda: TdmkEdit
        Left = 94
        Top = 34
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnInfoCli: TPanel
    Left = 0
    Top = 194
    Width = 1161
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Panel14: TPanel
      Left = 0
      Top = 0
      Width = 843
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 281
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        Caption = 'Informa'#231#245'es sobre o cliente'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -23
        Font.Name = 'Berlin Sans FB, 14'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label6: TLabel
        Left = 500
        Top = 36
        Width = 97
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastrado por:'
      end
      object Label5: TLabel
        Left = 238
        Top = 36
        Width = 122
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor '#250'ltima compra:'
      end
      object Label4: TLabel
        Left = 10
        Top = 36
        Width = 93
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #218'ltima compra: '
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 598
        Top = 32
        Width = 234
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMECAD2'
        DataSource = DsHist
        Enabled = False
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 369
        Top = 32
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'POSIt'
        DataSource = DsHist
        Enabled = False
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 102
        Top = 32
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DATAREAL_TXT'
        DataSource = DsHist
        Enabled = False
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel15: TPanel
      Left = 843
      Top = 0
      Width = 318
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object Label7: TLabel
        Left = 2
        Top = 36
        Width = 89
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Departamento:'
      end
      object CBGradeD: TdmkDBLookupComboBox
        Left = 96
        Top = 32
        Width = 209
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGradeD
        TabOrder = 0
        dmkEditCB = EdGradeD
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGradeD: TdmkEditCB
        Left = 236
        Top = 21
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGradeDChange
        DBLookupComboBox = CBGradeD
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object PnBottom: TPanel
    Left = 0
    Top = 775
    Width = 1161
    Height = 69
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object Panel2: TPanel
      Left = 921
      Top = 1
      Width = 238
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 123
        Top = 9
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
      object BtConfPedi: TBitBtn
        Tag = 14
        Left = 5
        Top = 9
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfPediClick
      end
    end
    object PnVdaBot: TPanel
      Left = 1
      Top = 1
      Width = 915
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label21: TLabel
        Left = 9
        Top = 10
        Width = 52
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Subtotal:'
      end
      object Label24: TLabel
        Left = 9
        Top = 39
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Obs.:'
      end
      object Label25: TLabel
        Left = 212
        Top = 10
        Width = 34
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Frete:'
      end
      object Label22: TLabel
        Left = 400
        Top = 11
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Desconto:'
      end
      object Label18: TLabel
        Left = 620
        Top = 10
        Width = 36
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pago:'
      end
      object LaTotal: TLabel
        Left = 662
        Top = 33
        Width = 123
        Height = 27
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'TOTAL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -23
        Font.Name = 'Berlin Sans FB'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 571
        Top = 33
        Width = 73
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'TOTAL:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -23
        Font.Name = 'Berlin Sans FB'
        Font.Style = []
        ParentFont = False
      end
      object EdObs: TdmkEdit
        Left = 47
        Top = 34
        Width = 517
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'dmkEdit1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'dmkEdit1'
        ValWarn = False
      end
      object EdSubTotal: TdmkEdit
        Left = 66
        Top = 5
        Width = 124
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdSubTotalChange
      end
      object EdFrete: TdmkEdit
        Left = 250
        Top = 5
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdSubTotalChange
      end
      object EdDesc: TdmkEdit
        Left = 468
        Top = 5
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdSubTotalChange
        OnExit = EdDescExit
      end
      object EdPago: TdmkEdit
        Left = 662
        Top = 5
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTI'
      'FROM entidades en'
      'WHERE en.Cliente1="V"'
      'ORDER BY NOMEENTI')
    Left = 12
    Top = 9
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClienteNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 40
    Top = 9
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Aplicacao & 8'
      'ORDER BY Nome')
    Left = 68
    Top = 9
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 96
    Top = 9
  end
  object DsHist: TDataSource
    DataSet = QrHist
    Left = 152
    Top = 9
  end
  object QrHist: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrHistCalcFields
    SQL.Strings = (
      'SELECT Codigo, DataReal, Total, '
      'Frete, UserCad'
      'FROM movil'
      'WHERE Cliente=:P0'
      'AND DataReal > 0'
      'ORDER BY DataReal DESC'
      'LIMIT 1')
    Left = 124
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrHistDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrHistPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrHistTotal: TFloatField
      FieldName = 'Total'
      Origin = 'moviv.Total'
    end
    object QrHistFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'moviv.Frete'
    end
    object QrHistDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'moviv.DataReal'
    end
    object QrHistNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrHistNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrHistUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'moviv.UserCad'
    end
    object QrHistCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM senhas')
    Left = 180
    Top = 9
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'senhas.Numero'
    end
    object QrSenhasSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'senhas.Senha'
      Size = 30
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'senhas.Perfil'
    end
    object QrSenhasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'senhas.Lk'
    end
  end
  object DsSenhas: TDataSource
    Left = 208
    Top = 9
  end
  object DsGradesCors: TDataSource
    DataSet = QrGradesCors
    Left = 376
    Top = 9
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY Controle')
    Left = 348
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
      Size = 30
    end
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradescors.Codigo'
      Required = True
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradescors.Controle'
      Required = True
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'gradescors.Cor'
      Required = True
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradescors.Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradescors.DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradescors.DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradescors.UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradescors.UserAlt'
    end
    object QrGradesCorsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'gradescors.AlterWeb'
      Required = True
    end
    object QrGradesCorsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'gradescors.Ativo'
      Required = True
    end
  end
  object DsGradesTams: TDataSource
    DataSet = QrGradesTams
    Left = 320
    Top = 9
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY Controle')
    Left = 292
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradestams.Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradestams.Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'gradestams.Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradestams.Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradestams.DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradestams.DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradestams.UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradestams.UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 264
    Top = 9
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradesBeforeClose
    AfterScroll = QrGradesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'LEFT JOIN gradeg grg  ON grg.Codigo=gra.GradeG'
      'LEFT JOIN graded grd ON grd.Codigo=grg.GradeD  '
      'WHERE pro.Ativo = 1'
      'AND grd.Codigo=:P0'
      'ORDER BY Nome')
    Left = 236
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 404
    Top = 9
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 460
    Top = 9
  end
  object QrItem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Codigo, gra.CodUsu, prd.Cor, prd.Tam'
      'FROM produtos prd '
      'LEFT JOIN grades gra ON gra.Codigo=prd.Codigo'
      'WHERE prd.Controle=:P0')
    Left = 432
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItemCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrItemTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrItemCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
  end
  object QrGradeD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM graded'
      'WHERE Codigo > 0')
    Left = 488
    Top = 9
    object QrGradeDCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'graded.Codigo'
    end
    object QrGradeDNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'graded.Nome'
      Size = 30
    end
    object QrGradeDLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'graded.Lk'
    end
    object QrGradeDDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'graded.DataCad'
    end
    object QrGradeDDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'graded.DataAlt'
    end
    object QrGradeDUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'graded.UserCad'
    end
    object QrGradeDUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'graded.UserAlt'
    end
    object QrGradeDAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'graded.AlterWeb'
    end
    object QrGradeDAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'graded.Ativo'
    end
  end
  object DsGradeD: TDataSource
    DataSet = QrGradeD
    Left = 516
    Top = 9
  end
end
