object FmPtosPedCad: TFmPtosPedCad
  Left = 368
  Top = 194
  Caption = 'PTO-VENDA-002 :: Cadastro de Pedidos de Pontos de Vendas'
  ClientHeight = 561
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 502
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 109
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      ExplicitWidth = 1238
      object Label1: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 79
        Top = 5
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 182
        Top = 5
        Width = 37
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome'
        FocusControl = DBEdit2
      end
      object dmkLabel6: TdmkLabel
        Left = 1078
        Top = 5
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data liberado:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel7: TdmkLabel
        Left = 650
        Top = 54
        Width = 84
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data enviado:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel8: TdmkLabel
        Left = 793
        Top = 54
        Width = 89
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data recebido:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel5: TdmkLabel
        Left = 935
        Top = 5
        Width = 78
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data pedido:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label5: TLabel
        Left = 5
        Top = 54
        Width = 95
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lista de pre'#231'os:'
        FocusControl = DBEdit7
      end
      object dmkLabel9: TdmkLabel
        Left = 793
        Top = 5
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abertura:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel11: TdmkLabel
        Left = 935
        Top = 54
        Width = 40
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Status:'
        UpdType = utYes
        SQLType = stNil
      end
      object DBEdCodigo: TDBEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPtosPedCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 79
        Top = 25
        Width = 98
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CodUsu'
        DataSource = DsPtosPedCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 251
        Top = 25
        Width = 538
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_PontoVda'
        DataSource = DsPtosPedCad
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 935
        Top = 25
        Width = 139
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DATAPED_TXT'
        DataSource = DsPtosPedCad
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 1078
        Top = 25
        Width = 138
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DATALIB_TXT'
        DataSource = DsPtosPedCad
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 650
        Top = 74
        Width = 138
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DATAENV_TXT'
        DataSource = DsPtosPedCad
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 793
        Top = 74
        Width = 137
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DATAREC_TXT'
        DataSource = DsPtosPedCad
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 74
        Top = 74
        Width = 572
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GraCusPrc'
        DataSource = DsPtosPedCad
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 182
        Top = 25
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CU_PontoVda'
        DataSource = DsPtosPedCad
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 5
        Top = 74
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'GraCusPrc'
        DataSource = DsPtosPedCad
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 793
        Top = 25
        Width = 137
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Abertura'
        DataSource = DsPtosPedCad
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 935
        Top = 74
        Width = 282
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Status'
        DataSource = DsPtosPedCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 11
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 442
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitWidth = 1238
      object LaRegistro: TdmkLabel
        Left = 213
        Top = 1
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 660
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
        end
        object BtPedido: TBitBtn
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Pedido'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtPedidoClick
        end
        object Panel2: TPanel
          Left = 443
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtVolta: TBitBtn
          Tag = 10033
          Left = 236
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtVoltaClick
        end
        object BtAvanca: TBitBtn
          Tag = 10034
          Left = 290
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtAvancaClick
        end
      end
    end
    object PnGrids: TPanel
      Left = 1
      Top = 177
      Width = 1239
      Height = 265
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      ExplicitWidth = 1238
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 350
        Height = 263
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        DataSource = DsPtosPedGru
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 351
        Top = 1
        Width = 887
        Height = 263
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet7
        Align = alClient
        MultiLine = True
        TabOrder = 1
        ExplicitWidth = 886
        object TabSheet5: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Quantidade '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeQ: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeQDrawCell
          end
        end
        object TabSheet4: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Desconto '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeD: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeDDrawCell
          end
        end
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Pre'#231'o da lista '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeL: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeLDrawCell
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Valores '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeF: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeFDrawCell
          end
        end
        object TabSheet6: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' C'#243'digos '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 1
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 1
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeCDrawCell
            RowHeights = (
              18)
          end
        end
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Ativos '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeA: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeADrawCell
            RowHeights = (
              18
              18)
          end
        end
        object TabSheet9: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' X '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeXDrawCell
            RowHeights = (
              18
              18)
          end
        end
        object TabSheet7: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Hist'#243'rico dos itens '
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object dmkDBGHist: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Produto'
                Title.Caption = 'Reduzido'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Produto'
                Title.Caption = 'Produto'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Cor'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Tam'
                Title.Caption = 'Tamanho'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataIns'
                Title.Caption = 'Data inclus'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quanti'
                Title.Caption = 'Qtde'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoR'
                Title.Caption = '$ Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Status'
                Title.Caption = 'Status'
                Width = 300
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPtosStqMov
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = dmkDBGHistDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Produto'
                Title.Caption = 'Reduzido'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Produto'
                Title.Caption = 'Produto'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Cor'
                Title.Caption = 'Cor'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Tam'
                Title.Caption = 'Tamanho'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataIns'
                Title.Caption = 'Data inclus'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quanti'
                Title.Caption = 'Qtde'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoR'
                Title.Caption = '$ Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Status'
                Title.Caption = 'Status'
                Width = 300
                Visible = True
              end>
          end
        end
        object TabSheet8: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Produtos enviados  '
          ImageIndex = 8
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 876
            Height = 228
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsMovEnv
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 502
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 442
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      ExplicitWidth = 1238
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1104
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 109
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 1238
      object Label7: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 79
        Top = 5
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object LaCliente: TLabel
        Left = 182
        Top = 5
        Width = 98
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ponto de venda:'
      end
      object dmkLabel1: TdmkLabel
        Left = 1078
        Top = 5
        Width = 78
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data pedido:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 793
        Top = 54
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data liberado:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel3: TdmkLabel
        Left = 935
        Top = 54
        Width = 84
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data enviado:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel4: TdmkLabel
        Left = 1078
        Top = 54
        Width = 89
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data recebido:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label2: TLabel
        Left = 5
        Top = 54
        Width = 95
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lista de pre'#231'os:'
      end
      object SpeedButton5: TSpeedButton
        Left = 901
        Top = 25
        Width = 26
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 763
        Top = 74
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object dmkLabel10: TdmkLabel
        Left = 935
        Top = 5
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abertura:'
        UpdType = utYes
        SQLType = stNil
      end
      object EdCodigo: TdmkEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 79
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdPontoVda: TdmkEditCB
        Left = 182
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPontoVdaChange
        DBLookupComboBox = CBPontoVda
        IgnoraDBLookupComboBox = False
      end
      object CBPontoVda: TdmkDBLookupComboBox
        Left = 252
        Top = 25
        Width = 650
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPtosCadPto
        TabOrder = 3
        dmkEditCB = EdPontoVda
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGraCusPrc: TdmkEditCB
        Left = 5
        Top = 74
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraCusPrc'
        UpdCampo = 'GraCusPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraCusPrc
        IgnoraDBLookupComboBox = False
      end
      object CBGraCusPrc: TdmkDBLookupComboBox
        Left = 75
        Top = 74
        Width = 689
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 6
        dmkEditCB = EdGraCusPrc
        QryCampo = 'GraCusPrc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDataPed: TdmkEdit
        Left = 1078
        Top = 25
        Width = 138
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 4
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'DataPed'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDataLib: TdmkEdit
        Left = 793
        Top = 74
        Width = 137
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'DataLib'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDataEnv: TdmkEdit
        Left = 935
        Top = 74
        Width = 138
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 8
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'DataEnv'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDataRec: TdmkEdit
        Left = 1078
        Top = 74
        Width = 138
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'DataRec'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAbertura: TdmkEdit
        Left = 935
        Top = 25
        Width = 138
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 10
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 
      '                              Cadastro de Pedidos de Pontos de V' +
      'endas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 1138
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 860
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsPtosPedCad: TDataSource
    DataSet = QrPtosPedCad
    Left = 40
    Top = 12
  end
  object QrPtosPedCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPtosPedCadBeforeOpen
    AfterOpen = QrPtosPedCadAfterOpen
    AfterScroll = QrPtosPedCadAfterScroll
    OnCalcFields = QrPtosPedCadCalcFields
    SQL.Strings = (
      'SELECT ppc.*, pcp.Nome NO_PontoVda, pcp.CodUsu CU_PontoVda,'
      'gcp.Nome NO_GraCusPrc'
      'FROM ptospedcad ppc'
      'LEFT JOIN ptoscadpto pcp ON pcp.Codigo=ppc.PontoVda'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=ppc.GraCusPrc'
      '')
    Left = 12
    Top = 12
    object QrPtosPedCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ptospedcad.Codigo'
      Required = True
    end
    object QrPtosPedCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'ptospedcad.CodUsu'
      Required = True
    end
    object QrPtosPedCadPontoVda: TIntegerField
      FieldName = 'PontoVda'
      Origin = 'ptospedcad.PontoVda'
      Required = True
    end
    object QrPtosPedCadDataPed: TDateTimeField
      FieldName = 'DataPed'
      Origin = 'ptospedcad.DataPed'
      Required = True
    end
    object QrPtosPedCadDataLib: TDateTimeField
      FieldName = 'DataLib'
      Origin = 'ptospedcad.DataLib'
      Required = True
    end
    object QrPtosPedCadDataEnv: TDateTimeField
      FieldName = 'DataEnv'
      Origin = 'ptospedcad.DataEnv'
      Required = True
    end
    object QrPtosPedCadDataRec: TDateTimeField
      FieldName = 'DataRec'
      Origin = 'ptospedcad.DataRec'
      Required = True
    end
    object QrPtosPedCadGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Origin = 'ptospedcad.GraCusPrc'
      Required = True
    end
    object QrPtosPedCadValor: TFloatField
      FieldName = 'Valor'
      Origin = 'ptospedcad.Valor'
      Required = True
    end
    object QrPtosPedCadLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ptospedcad.Lk'
    end
    object QrPtosPedCadDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ptospedcad.DataCad'
    end
    object QrPtosPedCadDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ptospedcad.DataAlt'
    end
    object QrPtosPedCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ptospedcad.UserCad'
    end
    object QrPtosPedCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ptospedcad.UserAlt'
    end
    object QrPtosPedCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'ptospedcad.AlterWeb'
      Required = True
    end
    object QrPtosPedCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ptospedcad.Ativo'
      Required = True
    end
    object QrPtosPedCadNO_PontoVda: TWideStringField
      FieldName = 'NO_PontoVda'
      Origin = 'ptoscadpto.Nome'
      Size = 50
    end
    object QrPtosPedCadNO_GraCusPrc: TWideStringField
      FieldName = 'NO_GraCusPrc'
      Origin = 'gracusprc.Nome'
      Required = True
      Size = 30
    end
    object QrPtosPedCadCU_PontoVda: TIntegerField
      FieldName = 'CU_PontoVda'
      Origin = 'ptoscadpto.CodUsu'
      Required = True
    end
    object QrPtosPedCadDATALIB_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'DATALIB_TXT'
      Size = 30
      Calculated = True
    end
    object QrPtosPedCadDATAPED_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'DATAPED_TXT'
      Size = 30
      Calculated = True
    end
    object QrPtosPedCadDATAENV_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'DATAENV_TXT'
      Size = 30
      Calculated = True
    end
    object QrPtosPedCadDATAREC_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'DATAREC_TXT'
      Size = 30
      Calculated = True
    end
    object QrPtosPedCadAbertura: TDateTimeField
      FieldName = 'Abertura'
      Required = True
    end
    object QrPtosPedCadStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrPtosPedCadOndeCad: TSmallintField
      FieldName = 'OndeCad'
    end
    object QrPtosPedCadNO_Status: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'NO_Status'
      Size = 50
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtPedido
    CanUpd01 = BtItens
    Left = 68
    Top = 12
  end
  object PMPedido: TPopupMenu
    OnPopup = PMPedidoPopup
    Left = 552
    Top = 380
    object Incluinovopedido1: TMenuItem
      Caption = '&Inclui novo pedido'
      OnClick = Incluinovopedido1Click
    end
    object Alterapedidoatual1: TMenuItem
      Caption = '&Altera pedido atual'
      OnClick = Alterapedidoatual1Click
    end
    object Excluipedidoatual1: TMenuItem
      Caption = '&Exclui pedido atual'
      Enabled = False
    end
  end
  object QrPtosCadPto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, GraCusPrc'
      'FROM ptoscadpto'
      'ORDER BY Nome')
    Left = 772
    Top = 12
    object QrPtosCadPtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPtosCadPtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPtosCadPtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPtosCadPtoGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object DsPtosCadPto: TDataSource
    DataSet = QrPtosCadPto
    Left = 800
    Top = 12
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Aplicacao & 4 <> 0'
      'ORDER BY Nome')
    Left = 828
    Top = 12
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 856
    Top = 12
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdPontoVda
    Panel = PainelEdita
    QryCampo = 'PontoVda'
    UpdCampo = 'PontoVda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 96
    Top = 12
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 632
    Top = 380
    object Incluiitensporgrade1: TMenuItem
      Caption = 'Inclui itens por &Grade'
      OnClick = Incluiitensporgrade1Click
    end
    object IncluiitensporLeitura1: TMenuItem
      Caption = 'Inclui itens por &Leitura'
      OnClick = IncluiitensporLeitura1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Cancelaritens1: TMenuItem
      Caption = '&Cancelar itens'
      OnClick = Cancelaritens1Click
    end
  end
  object QrPtosPedGru: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPtosPedGruBeforeClose
    AfterScroll = QrPtosPedGruAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT grs.Codigo, grs.Nome'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'WHERE psm.CodInn=:P0'
      'ORDER BY grs.Nome')
    Left = 124
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPtosPedGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPtosPedGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPtosPedGru: TDataSource
    DataSet = QrPtosPedGru
    Left = 152
    Top = 12
  end
  object QrPtosStqMov: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPtosStqMovCalcFields
    SQL.Strings = (
      'SELECT psm.IDCtrl, psm.DataIns, psm.Status, '
      'psm.Produto, psm.Quanti, psm.PrecoR,'
      'grs.Codigo Grade, grs.Nome NO_Produto, '
      'cor.Nome NO_Cor, tam.CodUsu CU_Tam, '
      'tam.Nome NO_Tam, prd.Cor, prd.Tam'
      'FROM ptosstqmov psm '
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.codigo'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'WHERE psm.CodInn=:P0'
      'ORDER BY NO_Produto, NO_Cor, CU_Tam, NO_Tam')
    Left = 180
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPtosStqMovIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrPtosStqMovDataIns: TDateTimeField
      FieldName = 'DataIns'
      Required = True
    end
    object QrPtosStqMovStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrPtosStqMovQuanti: TFloatField
      FieldName = 'Quanti'
      Required = True
    end
    object QrPtosStqMovPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPtosStqMovGrade: TIntegerField
      FieldName = 'Grade'
    end
    object QrPtosStqMovNO_Produto: TWideStringField
      FieldName = 'NO_Produto'
      Size = 30
    end
    object QrPtosStqMovNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 100
    end
    object QrPtosStqMovCU_Tam: TIntegerField
      FieldName = 'CU_Tam'
    end
    object QrPtosStqMovNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 100
    end
    object QrPtosStqMovNO_Status: TWideStringField
      DisplayWidth = 100
      FieldKind = fkCalculated
      FieldName = 'NO_Status'
      Size = 100
      Calculated = True
    end
    object QrPtosStqMovProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrPtosStqMovCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrPtosStqMovTam: TIntegerField
      FieldName = 'Tam'
    end
  end
  object DsPtosStqMov: TDataSource
    DataSet = QrPtosStqMov
    Left = 208
    Top = 12
  end
  object QrMovEnv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.* '
      'FROM movim mom'
      'WHERE mom.Motivo in (15,25,26)'
      'AND mom.Controle=:P0')
    Left = 236
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovEnvControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMovEnvConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMovEnvGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMovEnvCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMovEnvTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrMovEnvQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrMovEnvVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrMovEnvVen: TFloatField
      FieldName = 'Ven'
      Required = True
    end
    object QrMovEnvDataPedi: TDateField
      FieldName = 'DataPedi'
      Required = True
    end
    object QrMovEnvDataReal: TDateField
      FieldName = 'DataReal'
      Required = True
    end
    object QrMovEnvMotivo: TSmallintField
      FieldName = 'Motivo'
      Required = True
    end
    object QrMovEnvSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Required = True
    end
    object QrMovEnvSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Required = True
    end
    object QrMovEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMovEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMovEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMovEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMovEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMovEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMovEnvAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMovEnvSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
      Required = True
    end
    object QrMovEnvComisTip: TSmallintField
      FieldName = 'ComisTip'
      Required = True
    end
    object QrMovEnvComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
    end
    object QrMovEnvComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
    end
    object QrMovEnvSubCta: TIntegerField
      FieldName = 'SubCta'
      Required = True
    end
    object QrMovEnvKit: TIntegerField
      FieldName = 'Kit'
      Required = True
    end
    object QrMovEnvIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
  end
  object DsMovEnv: TDataSource
    DataSet = QrMovEnv
    Left = 264
    Top = 12
  end
end
