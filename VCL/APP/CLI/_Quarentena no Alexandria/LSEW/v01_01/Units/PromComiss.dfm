object FmPromComiss: TFmPromComiss
  Left = 339
  Top = 185
  Caption = 'CIC-CICLO-010 :: Comiss'#245'es de Promotores'
  ClientHeight = 558
  ClientWidth = 980
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 498
    Width = 980
    Height = 60
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 842
      Top = 1
      Width = 136
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtLoc: TBitBtn
      Left = 10
      Top = 5
      Width = 111
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Loc. &Ciclo'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtLocClick
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 123
      Top = 5
      Width = 111
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtConfirmaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 980
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Comiss'#245'es de Promotores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 877
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 878
      Top = 1
      Width = 100
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 980
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 10
      Top = 7
      Width = 58
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Promotor:'
    end
    object EdPromotor: TdmkEditCB
      Left = 10
      Top = 30
      Width = 69
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPromotor
      IgnoraDBLookupComboBox = False
    end
    object CBPromotor: TdmkDBLookupComboBox
      Left = 81
      Top = 30
      Width = 368
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOMEPROMO'
      ListSource = DmCiclos.DsPromotores
      TabOrder = 1
      dmkEditCB = EdPromotor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPDataI: TdmkEditDateTimePicker
      Left = 457
      Top = 30
      Width = 133
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 39743.851871921310000000
      Time = 39743.851871921310000000
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object TPDataF: TdmkEditDateTimePicker
      Left = 597
      Top = 30
      Width = 133
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 39743.851871921310000000
      Time = 39743.851871921310000000
      TabOrder = 3
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object CkDataI: TCheckBox
      Left = 457
      Top = 6
      Width = 110
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data inicial:'
      TabOrder = 4
    end
    object CkDataF: TCheckBox
      Left = 597
      Top = 6
      Width = 111
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data final:'
      TabOrder = 5
    end
    object BtPesq: TBitBtn
      Tag = 22
      Left = 735
      Top = 6
      Width = 111
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Pesquisar'
      NumGlyphs = 2
      TabOrder = 6
      OnClick = BtPesqClick
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 123
    Width = 980
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 3
    Visible = False
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 144
    Width = 980
    Height = 354
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    DataSource = DsPromCom
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        ReadOnly = True
        Title.Caption = 'Ciclo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        ReadOnly = True
        Title.Caption = 'ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cidade'
        ReadOnly = True
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UF'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Promotor'
        ReadOnly = True
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Inscritos'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Presentes'
        ReadOnly = True
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DPrmAluno'
        ReadOnly = True
        Title.Caption = 'Desp. Prm. '
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Comissao'
        Title.Caption = 'Comiss'#227'o %'
        Visible = True
      end>
  end
  object DsCiclosAula: TDataSource
    DataSet = QrCiclosAula
    Left = 40
    Top = 10
  end
  object QrCiclosAula: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCiclosAulaCalcFields
    SQL.Strings = (
      'SELECT aul.Codigo, aul.Controle, aul.Data, aul.Cidade, '
      'aul.CodiCidade, aul.UF, aul.AlunosInsc, aul.AlunosAula,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPRM, '
      'dtb.Nome MUNI, aul.ComPromPer, SUM(lan.Debito) Debito,'
      'cic.Controle CICCTRL, cic.Professor, cic.RolComis'
      'FROM ciclosaula aul'
      'LEFT JOIN ciclos cic ON cic.Codigo = aul.Codigo'
      'LEFT JOIN dtb_munici dtb ON dtb.Codigo = aul.CodiCidade'
      'LEFT JOIN entidades ent ON ent.Codigo = aul.Promotor'
      'LEFT JOIN FTabLctALS lan ON lan.FatID = 703'
      
        '                                    AND lan.FatID_Sub = aul.Cont' +
        'role'
      '                                    AND lan.FatNum =  aul.Codigo'
      'WHERE aul.Codigo > 0'
      'GROUP BY aul.Controle')
    Left = 12
    Top = 9
    object QrCiclosAulaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCiclosAulaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCiclosAulaData: TDateField
      FieldName = 'Data'
    end
    object QrCiclosAulaCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 50
    end
    object QrCiclosAulaCodiCidade: TIntegerField
      FieldName = 'CodiCidade'
    end
    object QrCiclosAulaUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCiclosAulaAlunosInsc: TIntegerField
      FieldName = 'AlunosInsc'
    end
    object QrCiclosAulaAlunosAula: TIntegerField
      FieldName = 'AlunosAula'
    end
    object QrCiclosAulaNOMEPRM: TWideStringField
      FieldName = 'NOMEPRM'
      Size = 100
    end
    object QrCiclosAulaMUNI: TWideStringField
      FieldName = 'MUNI'
      Size = 100
    end
    object QrCiclosAulaMUNICI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MUNICI_TXT'
      Size = 100
      Calculated = True
    end
    object QrCiclosAulaComPromPer: TFloatField
      FieldName = 'ComPromPer'
    end
    object QrCiclosAulaDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrCiclosAulaCICCTRL: TIntegerField
      FieldName = 'CICCTRL'
    end
    object QrCiclosAulaProfessor: TIntegerField
      FieldName = 'Professor'
    end
    object QrCiclosAulaRolComis: TIntegerField
      FieldName = 'RolComis'
    end
  end
  object TbPromCom: TmySQLTable
    Database = Dmod.MyDBn
    Filtered = True
    OnNewRecord = TbPromComNewRecord
    TableName = 'promcom'
    Left = 68
    Top = 10
    object TbPromComCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbPromComControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbPromComData: TDateField
      FieldName = 'Data'
    end
    object TbPromComCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object TbPromComUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object TbPromComPromotor: TWideStringField
      FieldName = 'Promotor'
      Size = 100
    end
    object TbPromComInscritos: TIntegerField
      FieldName = 'Inscritos'
    end
    object TbPromComPresentes: TIntegerField
      FieldName = 'Presentes'
    end
    object TbPromComComissao: TFloatField
      FieldName = 'Comissao'
      DisplayFormat = '#,###,####0.0000;-#,###,####0.0000;'
    end
    object TbPromComDPrmAluno: TFloatField
      FieldName = 'DPrmAluno'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00;'
    end
    object TbPromComCICCTRL: TIntegerField
      FieldName = 'CICCTRL'
    end
    object TbPromComProfessor: TIntegerField
      FieldName = 'Professor'
    end
    object TbPromComRolComis: TIntegerField
      FieldName = 'RolComis'
    end
  end
  object DsPromCom: TDataSource
    DataSet = TbPromCom
    Left = 96
    Top = 10
  end
end
