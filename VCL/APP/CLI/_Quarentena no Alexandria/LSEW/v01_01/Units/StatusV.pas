unit StatusV;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral, dmkDBEdit,
  ColorGrd, ActnColorMaps, ActnMan, dmkCheckBox, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP,
  dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TFmStatusV = class(TForm)
    DsStatusV: TDataSource;
    QrStatusV: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PainelData: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    PainelEdita: TPanel;
    Label10: TLabel;
    Label9: TLabel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    dmkEdNome: TdmkEdit;
    EdCodigo: TdmkEdit;
    dmkCkAtivo: TdmkCheckBox;
    CkAtivo: TDBCheckBox;
    QrStatusVCodigo: TIntegerField;
    QrStatusVNome: TWideStringField;
    QrStatusVLk: TIntegerField;
    QrStatusVDataCad: TDateField;
    QrStatusVDataAlt: TDateField;
    QrStatusVUserCad: TIntegerField;
    QrStatusVUserAlt: TIntegerField;
    QrStatusVAlterWeb: TSmallintField;
    QrStatusVAtivo: TSmallintField;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStatusVAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStatusVBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    //
  public
    { Public declarations }
  end;

var
  FmStatusV: TFmStatusV;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStatusV.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStatusV.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStatusVCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStatusV.DefParams;
begin
  VAR_GOTOTABELA := 'statusv';
  VAR_GOTOMYSQLTABLE := QrStatusV;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM statusv');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmStatusV.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStatusV.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStatusV.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmStatusV.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStatusV.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStatusV.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStatusV.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStatusV.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStatusVCodigo.Value;
  Close;
end;

procedure TFmStatusV.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome    := dmkEdNome.ValueVariant;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Falta de informações', MB_OK+MB_ICONWARNING);
    dmkEdNome.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('statusv', 'Codigo', LaTipo.SQLType,
    QrStatusVCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmStatusV, PainelEdita,
    'statusv', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, true) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmStatusV.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'statusv', Codigo);
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'statusv', 'Codigo');
end;

procedure TFmStatusV.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  //
end;

procedure TFmStatusV.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStatusVCodigo.Value,LaRegistro.Caption);
end;

procedure TFmStatusV.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStatusV.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmStatusV.QrStatusVAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmStatusV.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmStatusV.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStatusVCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'statusv', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStatusV.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmStatusV.QrStatusVBeforeOpen(DataSet: TDataSet);
begin
  QrStatusVCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmStatusV.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrStatusV, [PainelDados],
  [PainelEdita], dmkEdNome, LaTipo, 'statusv');
  //
  dmkCkAtivo.Checked       := True;
end;

procedure TFmStatusV.BtAlteraClick(Sender: TObject);
begin
  if QrStatusVCodigo.Value > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrStatusV, [PainelDados],
    [PainelEdita], dmkEdNome, LaTipo, 'statusv');
    //
    dmkCkAtivo.Checked := MLAGeral.IntToBool(QrStatusVAtivo.Value);
  end;
end;

procedure TFmStatusV.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Visible := False;
    PainelEdita.Visible := True;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.ValueVariant := FormatFloat('000', Codigo);
      dmkEdNome.ValueVariant    := '';
      dmkCkAtivo.Checked    := true;
    end else begin
      EdCodigo.ValueVariant  := QrStatusVCodigo.Value;
      dmkEdNome.ValueVariant := QrStatusVNome.Value;
      dmkCkAtivo.Checked     := MLAGeral.IntToBool(QrStatusVAtivo.Value);
    end;
    dmkEdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

end.


