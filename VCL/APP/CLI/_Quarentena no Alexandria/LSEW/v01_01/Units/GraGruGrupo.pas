unit GraGruGrupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, dmkLabel, Mask, dmkGeral,
  UnInternalConsts;

type
  TFmGraGruGrupo = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    PnEdita: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnNiveis: TPanel;
    Label3: TLabel;
    dmkEdCBGradeD: TdmkEditCB;
    dmkCBGradeD: TdmkDBLookupComboBox;
    dmkEdNome: TdmkEdit;
    dmkEdCodigo: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    QrGradeD: TmySQLQuery;
    DsGradeD: TDataSource;
    QrGradeDCodigo: TIntegerField;
    QrGradeDNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruGrupo: TFmGraGruGrupo;

implementation

uses Module, UMySQLModule, UnMySQLCuringa, GraGru, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruGrupo.BtOKClick(Sender: TObject);
var
  Codigo, Depto: Integer;
  Nome: String;
begin
  Nome := dmkEdNome.ValueVariant;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Falta de informações', MB_OK+MB_ICONWARNING);
    dmkEdNome.SetFocus;
    Exit;
  end;
  Depto := dmkEdCBGradeD.ValueVariant;
  if Depto = 0 then
  begin
    Application.MessageBox('Defina o departamento.', 'Falta de informações', MB_OK+MB_ICONWARNING);
    dmkEdCBGradeD.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def_Old('gradeg', 'Codigo', LaTipo.Caption, FmGraGru.QrGradeGCodigo.Value);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'gradeg', False,
  [
    'Nome', 'GradeD'
  ], ['Codigo'], [
    dmkEdNome.ValueVariant, dmkEdCBGradeD.ValueVariant
  ], [Codigo]) then
  begin
    FmGraGru.QrGradeG.Close;
    FmGraGru.QrGradeG.Open;
    VAR_COD := Codigo;
    //
    Close;
  end;
end;

procedure TFmGraGruGrupo.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := FmGraGru.QrGradeGCodigo.Value;
  Close;
end;

procedure TFmGraGruGrupo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    dmkEdCodigo.ValueVariant := 0;
    dmkEdNome.ValueVariant   := '';
  end else
  begin
    dmkEdCodigo.ValueVariant   := FmGraGru.QrGradeGCodigo.Value;
    dmkEdNome.ValueVariant     := FmGraGru.QrGradeGNome.Value;
    dmkEdCBGradeD.ValueVariant := FmGraGru.QrGradeGGradeD.Value;
    dmkCBGradeD.KeyValue       := FmGraGru.QrGradeGGradeD.Value;
  end;
  dmkEdNome.SetFocus;
end;

procedure TFmGraGruGrupo.FormCreate(Sender: TObject);
begin
  QrGradeD.Open;
end;

procedure TFmGraGruGrupo.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
