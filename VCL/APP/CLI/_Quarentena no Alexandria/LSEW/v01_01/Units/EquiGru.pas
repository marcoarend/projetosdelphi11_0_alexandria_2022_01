unit EquiGru;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkLabel, frxClass, frxDBSet, dmkGeral, Variants, UnDmkProcFunc, UnDmkEnums;

type
  TFmEquiGru = class(TForm)
    PainelDados: TPanel;
    DsEquiGru: TDataSource;
    QrEquiGru: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtEquiEnt: TBitBtn;
    BtEquiGru: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TEdit;
    PMEquiGru: TPopupMenu;
    PainelItens: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BitBtn2: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    Panel10: TPanel;
    LaProfProm: TLabel;
    QrEquiEnt: TmySQLQuery;
    DsEquiEnt: TDataSource;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    PMEquiEnt: TPopupMenu;
    QrProfPromo: TmySQLQuery;
    DsProfPromo: TDataSource;
    QrProfPromoCodigo: TIntegerField;
    LaTipo: TdmkLabel;
    QrSoma: TmySQLQuery;
    QrSomaValor: TFloatField;
    frxPrecoCurso: TfrxReport;
    frxDsEquiEnt: TfrxDBDataset;
    frxDsEquiGru: TfrxDBDataset;
    QrEquiGruCodigo: TIntegerField;
    QrEquiGruNome: TWideStringField;
    QrEquiGruTipo: TSmallintField;
    QrEquiEntControle: TIntegerField;
    QrEquiEntEntidade: TIntegerField;
    QrProfPromoNOMEENTI: TWideStringField;
    Incluinovaequipe1: TMenuItem;
    Alteraequipeatual1: TMenuItem;
    Excluiequipeatual1: TMenuItem;
    Retiraentidade1: TMenuItem;
    Alteraentidade1: TMenuItem;
    Incluientidade1: TMenuItem;
    QrEquiEntNOMEENTI: TWideStringField;
    QrEquiEntTEL1ENTI: TWideStringField;
    QrEquiEntCELUENTI: TWideStringField;
    QrEquiEntTEL1ENTI_TXT: TWideStringField;
    QrEquiEntCELUENTI_TXT: TWideStringField;
    QrEquiEntEquipe: TIntegerField;
    RGTipo: TRadioGroup;
    Label3: TLabel;
    Panel4: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Label4: TLabel;
    EdRolComis: TdmkEditCB;
    CBRolComis: TdmkDBLookupComboBox;
    QrRolComis: TmySQLQuery;
    DsRolComis: TDataSource;
    QrRolComisCodigo: TIntegerField;
    QrRolComisNome: TWideStringField;
    QrEquiEntNOMEROLCOMIS: TWideStringField;
    QrEquiEntRolComis: TIntegerField;
    CBCartPadr: TdmkDBLookupComboBox;
    EdCartPadr: TdmkEditCB;
    Label5: TLabel;
    QrEquiGruCartPadr: TIntegerField;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrEquiGruNOMECARTEIRA: TWideStringField;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtEquiGruClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEquiGruAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEquiGruBeforeOpen(DataSet: TDataSet);
    procedure Crianovogrupo1Click(Sender: TObject);
    procedure Alteragrupoatual1Click(Sender: TObject);
    procedure Excluigrupoatual1Click(Sender: TObject);
    procedure BtEquiEntClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrEquiGruAfterScroll(DataSet: TDataSet);
    procedure Incluiitemdereceita1Click(Sender: TObject);
    procedure Alteraitemdereceitaatual1Click(Sender: TObject);
    procedure PMEquiGruPopup(Sender: TObject);
    procedure Excluiitemdereceitaatual1Click(Sender: TObject);
    procedure PMEquiEntPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrEquiEntCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenEquiEnt(Controle: Integer);
  public
    { Public declarations }
  end;

var
  FmEquiGru: TFmEquiGru;
const
  FFormatFloat = '00000';

implementation

uses Module, UnFinanceiro, Carteiras, MyDBCheck, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEquiGru.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEquiGru.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEquiGruCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEquiGru.DefParams;
begin
  VAR_GOTOTABELA := 'equigru';
  VAR_GOTOMYSQLTABLE := QrEquiGru;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT car.Nome NOMECARTEIRA, eqg.*');
  VAR_SQLx.Add('FROM equigru eqg');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=eqg.CartPadr');
  VAR_SQLx.Add('WHERE eqg.Codigo > 0');
  //
  VAR_SQL1.Add('AND eqg.Codigo=:P0');
  //
  VAR_SQLa.Add('AND eqg.Nome Like :P0');
  //
end;

procedure TFmEquiGru.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelItens.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text           := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text             := '';
        RGTipo.ItemIndex        := 0;
        EdCartPadr.Text         := '';
        CBCartPadr.KeyValue     := Null;
      end else begin
        EdCodigo.Text           := DBEdCodigo.Text;
        EdNome.Text             := DBEdNome.Text;
        RGTipo.ItemIndex        := QrEquiGruTipo.Value;
        EdCartPadr.ValueVariant := QrEquiGruCartPadr.Value;
        CBCartPadr.KeyValue     := QrEquiGruCartPadr.Value;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      if (SQLType = stIns) and (QrEquiEnt.RecordCount > 0) then
      begin
        Application.MessageBox('O tipo de grupo "Professor" n�o pode ter em seu ' +
        'cadastro mais de uma entidade!', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      QrProfPromo.Close;
      QrProfPromo.SQL.Clear;
      QrProfPromo.SQL.Add('SELECT en.Codigo,');
      QrProfPromo.SQL.Add('IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTI');
      QrProfPromo.SQL.Add('FROM entidades en');
      case QrEquiGruTipo.Value of
        1: QrProfPromo.SQL.Add('WHERE en.Fornece2="V"'); // Promotor
        2: QrProfPromo.SQL.Add('WHERE en.Fornece3="V"'); // Professor
        else QrProfPromo.SQL.Add('WHERE en.Fornece2="V" OR en.Fornece3="V"');
      end;
      QrProfPromo.SQL.Add('AND en.Codigo NOT IN (');
      QrProfPromo.SQL.Add('  SELECT Entidade');
      QrProfPromo.SQL.Add('  FROM equient');
      QrProfPromo.SQL.Add('  WHERE Codigo=' +
        FormatFloat('0', QrEquiEntEntidade.Value));
      QrProfPromo.SQL.Add(')');
      QrProfPromo.SQL.Add('ORDER BY NOMEENTI');
      QrProfPromo.Open;
      //
      PainelItens.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      //
      if SQLType = stIns then
      begin
        EdEntidade.Text        := '';
        CBEntidade.KeyValue    := Null;
        EdRolComis.Text        := '';
        CBRolComis.KeyValue    := Null;
      end else begin
        EdEntidade.Text        := IntToStr(QrEquiEntEntidade.Value);
        CBEntidade.KeyValue    := QrEquiEntEntidade.Value;
        EdRolComis.Text        := IntToStr(QrEquiEntRolComis.Value);
        CBRolComis.KeyValue    := QrEquiEntRolComis.Value;
      end;
      //
      EdEntidade.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmEquiGru.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEquiGru.AlteraRegistro;
var
  EquiGru : Integer;
begin
  EquiGru := QrEquiGruCodigo.Value;
  if not UMyMod.SelLockY(EquiGru, Dmod.MyDB, 'equigru', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(EquiGru, Dmod.MyDB, 'equigru', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEquiGru.IncluiRegistro;
var
  Cursor : TCursor;
  EquiGru : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    EquiGru := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
    'equigru', 'equigru', 'Codigo');
    if Length(FormatFloat(FFormatFloat, EquiGru))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, EquiGru);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmEquiGru.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEquiGru.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEquiGru.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEquiGru.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEquiGru.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEquiGru.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEquiGru.BtEquiGruClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEquiGru, BtEquiGru);
end;

procedure TFmEquiGru.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEquiGruCodigo.Value;
  Close;
end;

procedure TFmEquiGru.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if (RGTipo.ItemIndex = 2) and (QrEquiEnt.RecordCount > 1) and
  (LaTipo.SQLType = stUpd) then
  begin
    Application.MessageBox('O tipo de grupo "Professor" n�o pode ter em seu ' +
    'cadastro mais de uma entidade!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if EdCartPadr.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina uma carteira preferencial.', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'equigru', False,
  [
    'Nome', 'Tipo', 'CartPadr'
  ], ['Codigo'],
  [
  EdNome.Text, RGTipo.ItemIndex, EdCartPadr.ValueVariant
  ], [Codigo]) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'equigru', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmEquiGru.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'equigru', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'equigru', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'equigru', 'Codigo');
end;

procedure TFmEquiGru.FormCreate(Sender: TObject);
begin
  QrRolComis.Open;
  Panel4.Align      := alClient;
  Panel8.Align      := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  dmkDBGrid1.Align  := alClient;
  CriaOForm;
  QrCarteiras.Open;
end;

procedure TFmEquiGru.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEquiGruCodigo.Value,LaRegistro.Caption);
end;

procedure TFmEquiGru.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEquiGru.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmEquiGru.QrEquiGruAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEquiGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEquiGru.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEquiGruCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'equigru', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEquiGru.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmEquiGru.QrEquiGruBeforeOpen(DataSet: TDataSet);
begin
  QrEquiGruCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEquiGru.Crianovogrupo1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmEquiGru.Alteragrupoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmEquiGru.Excluigrupoatual1Click(Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrEquiGru, 'equigru', 'Codigo',
  QrEquiGruCodigo.Value, True, 'Confirma a exclus�o da equipe "' +
  QrEquiGruNome.Value + '"?', True);
end;

procedure TFmEquiGru.BtEquiEntClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEquiEnt, BtEquiEnt);
end;

procedure TFmEquiGru.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmEquiGru.BitBtn1Click(Sender: TObject);
var
  Entidade, Codigo, Controle, RolComis: Integer;
begin
  Codigo   := QrEquiGruCodigo.Value;
  Entidade := Geral.IMV(EdEntidade.Text);
  RolComis := Geral.IMV(EdRolComis.Text);
  //
  if (Codigo = 0) then
  begin
    Application.MessageBox('Defina a equipe!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  if (Entidade = 0) then
  begin
    Geral.MB_Aviso('Defina a entidade!');
    Exit;
  end;
  //
  if (RolComis = 0) then
  begin
    Geral.MB_Aviso('Defina o rol de comiss�es!');
    Exit;
  end;
  //
  //
  Controle := UMyMod.BuscaEmLivreY_Def('equient', 'Controle',
    LaTipo.SQLType, QrEquiEntControle.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'equient', False,
  [
    'Codigo', 'Entidade', 'RolComis'
  ], ['Controle'],
  [
  Codigo, Entidade, RolComis
  ], [Controle], True) then
  begin
    MostraEdicao(0, stLok, 0);
    //RecalculaTotal(Codigo);
    ReopenEquiEnt(Controle);
  end;
end;

procedure TFmEquiGru.QrEquiGruAfterScroll(DataSet: TDataSet);
begin
  ReopenEquiEnt(0);
end;

procedure TFmEquiGru.ReopenEquiEnt(Controle: Integer);
begin
  QrEquiEnt.Close;
  QrEquiEnt.Params[0].AsInteger := QrEquiGruCodigo.Value;
  QrEquiEnt.Open;
  //
  if Controle > 0 then
    QrEquiEnt.Locate('Controle', Controle, []);
end;

procedure TFmEquiGru.Incluiitemdereceita1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmEquiGru.Alteraitemdereceitaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmEquiGru.Excluiitemdereceitaatual1Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Application.MessageBox('Confirma a retirada o item selecionado?',
  'Pergunta', MB_YESNOCANCEL) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM equient WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrEquiEntControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    //RecalculaTotal(QrEquiEntEquipe.Value);
    Prox := UMyMod.ProximoRegistro(QrEquiEnt, 'Controle',
      QrEquiEntControle.Value);
    ReopenEquiEnt(Prox);
    Screen.Cursor := crDefault;
  end;
  //
end;

procedure TFmEquiGru.PMEquiGruPopup(Sender: TObject);
begin
  UMyMod.HabilitaMenuItemInt([Alteraequipeatual1, Excluiequipeatual1],
    QrEquiGru, 'Codigo', 1, 0);
  Excluiequipeatual1.Enabled :=
    Excluiequipeatual1.Enabled and (QrEquiEnt.RecordCount = 0);
end;

procedure TFmEquiGru.PMEquiEntPopup(Sender: TObject);
begin
  UMyMod.HabilitaMenuItemInt([Alteraentidade1, Retiraentidade1],
    QrEquiEnt, 'Controle', 1, 0);
end;

procedure TFmEquiGru.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxPrecoCurso, 'Composi��o de pre�o de curso');
end;

procedure TFmEquiGru.QrEquiEntCalcFields(DataSet: TDataSet);
begin
  QrEquiEntTEL1ENTI_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEquiEntTEL1ENTI.Value);
  QrEquiEntCELUENTI_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEquiEntCELUENTI.Value);
end;

procedure TFmEquiGru.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      QrCarteiras.Close;
      QrCarteiras.Open;
      EdCartPadr.ValueVariant := VAR_CADASTRO;
      CBCartPadr.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

end.



