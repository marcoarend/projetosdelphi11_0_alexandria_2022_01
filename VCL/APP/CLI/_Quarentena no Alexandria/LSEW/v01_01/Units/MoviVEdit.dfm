object FmMoviVEdit: TFmMoviVEdit
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-002 :: Vendas de Mercadorias'
  ClientHeight = 817
  ClientWidth = 1161
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1161
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Venda de mercadorias'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Berlin Sans FB'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1057
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 1058
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
  end
  object PnTopo: TPanel
    Left = 0
    Top = 59
    Width = 1161
    Height = 99
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 1
    object Label9: TLabel
      Left = 4
      Top = 64
      Width = 73
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pedido:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -23
      Font.Name = 'Berlin Sans FB'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 11
      Top = 9
      Width = 78
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data pedido:'
    end
    object Label3: TLabel
      Left = 11
      Top = 38
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data venda:'
    end
    object Label26: TLabel
      Left = 223
      Top = 38
      Width = 88
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Lista de pre'#231'o:'
    end
    object Label1: TLabel
      Left = 265
      Top = 9
      Width = 44
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Caption = 'Cliente:'
    end
    object SBCliente: TSpeedButton
      Left = 794
      Top = 5
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBClienteClick
    end
    object Label84: TLabel
      Left = 830
      Top = 9
      Width = 81
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cadastro por:'
    end
    object Label85: TLabel
      Left = 833
      Top = 38
      Width = 77
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Alterado por:'
    end
    object EdNomeCad: TdmkEdit
      Left = 916
      Top = 5
      Width = 234
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'EdNomeCad'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'EdNomeCad'
      ValWarn = False
    end
    object EdDataPed: TdmkEdit
      Left = 92
      Top = 5
      Width = 123
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfDate
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0d
      ValWarn = False
    end
    object EdDataVenda: TdmkEdit
      Left = 92
      Top = 34
      Width = 123
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCliente: TdmkEditCB
      Left = 318
      Top = 4
      Width = 68
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 388
      Top = 4
      Width = 400
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliente
      TabOrder = 5
      OnExit = CBClienteExit
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CBListaPre: TdmkDBLookupComboBox
      Left = 388
      Top = 34
      Width = 400
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGraCusPrc
      TabOrder = 7
      dmkEditCB = EdListaPre
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdListaPre: TdmkEditCB
      Left = 318
      Top = 34
      Width = 68
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBListaPre
      IgnoraDBLookupComboBox = False
    end
    object EdNomeAlt: TdmkEdit
      Left = 916
      Top = 34
      Width = 234
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'EdNomeCad'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'EdNomeCad'
      ValWarn = False
    end
    object EdCodigo: TdmkEdit
      Left = 92
      Top = 64
      Width = 123
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnBottom: TPanel
    Left = 0
    Top = 748
    Width = 1161
    Height = 69
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Panel2: TPanel
      Left = 921
      Top = 1
      Width = 238
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfPedi: TBitBtn
        Tag = 14
        Left = 6
        Top = 9
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfPediClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 124
        Top = 9
        Width = 111
        Height = 49
        Hint = 'Desiste da inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
    object PnVdaBot: TPanel
      Left = 1
      Top = 1
      Width = 877
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label21: TLabel
        Left = 9
        Top = 10
        Width = 52
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Subtotal:'
      end
      object Label24: TLabel
        Left = 9
        Top = 39
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Obs.:'
      end
      object Label25: TLabel
        Left = 212
        Top = 10
        Width = 34
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Frete:'
      end
      object Label22: TLabel
        Left = 400
        Top = 11
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Desconto:'
      end
      object Label18: TLabel
        Left = 620
        Top = 10
        Width = 36
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pago:'
      end
      object LaTotal: TLabel
        Left = 662
        Top = 33
        Width = 123
        Height = 27
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'TOTAL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -23
        Font.Name = 'Berlin Sans FB'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 571
        Top = 33
        Width = 73
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'TOTAL:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -23
        Font.Name = 'Berlin Sans FB'
        Font.Style = []
        ParentFont = False
      end
      object EdObs: TdmkEdit
        Left = 47
        Top = 34
        Width = 517
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'dmkEdit1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'dmkEdit1'
        ValWarn = False
      end
      object EdSubTotal: TdmkEdit
        Left = 66
        Top = 5
        Width = 124
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFrete: TdmkEdit
        Left = 252
        Top = 5
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdFreteChange
      end
      object EdDesc: TdmkEdit
        Left = 468
        Top = 5
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdDescChange
        OnExit = EdDescExit
      end
      object EdPago: TdmkEdit
        Left = 662
        Top = 5
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnInfoCli: TPanel
    Left = 0
    Top = 158
    Width = 1161
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Panel14: TPanel
      Left = 0
      Top = 0
      Width = 814
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 281
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        Caption = 'Informa'#231#245'es sobre o cliente'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -23
        Font.Name = 'Berlin Sans FB, 14'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label6: TLabel
        Left = 500
        Top = 36
        Width = 97
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastrado por:'
      end
      object Label5: TLabel
        Left = 238
        Top = 36
        Width = 122
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor '#250'ltima compra:'
      end
      object Label4: TLabel
        Left = 10
        Top = 36
        Width = 93
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #218'ltima compra: '
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 598
        Top = 32
        Width = 234
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMECAD2'
        DataSource = FmMoviV.DsHist
        Enabled = False
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 369
        Top = 32
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'POSIt'
        DataSource = FmMoviV.DsHist
        Enabled = False
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 100
        Top = 32
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DATAREAL_TXT'
        DataSource = FmMoviV.DsHist
        Enabled = False
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel15: TPanel
      Left = 814
      Top = 0
      Width = 347
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object Label12: TLabel
        Left = 10
        Top = 36
        Width = 74
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Expira'#231#227'o:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object LaDataExp: TLabel
        Left = 96
        Top = 34
        Width = 91
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '24/05/2010'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -17
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtUltimo: TBitBtn
        Left = 194
        Top = 9
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&'#218'ltima compra'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtUltimoClick
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 222
    Width = 1161
    Height = 526
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 4
    object PCProd: TPageControl
      Left = 1
      Top = 1
      Width = 1159
      Height = 524
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 1158
      ExplicitHeight = 525
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mercadorias'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 1151
          Height = 108
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1148
          object Label19: TLabel
            Left = 9
            Top = 55
            Width = 72
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mercadoria:'
          end
          object Label20: TLabel
            Left = 837
            Top = 55
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade:'
          end
          object Label27: TLabel
            Left = 967
            Top = 55
            Width = 34
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Total:'
          end
          object Label28: TLabel
            Left = 837
            Top = 4
            Width = 53
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Estoque:'
          end
          object Label29: TLabel
            Left = 967
            Top = 4
            Width = 35
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Valor:'
          end
          object Label30: TLabel
            Left = 453
            Top = 55
            Width = 24
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cor:'
          end
          object Label31: TLabel
            Left = 647
            Top = 55
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tamanho:'
          end
          object EdProd: TdmkEditCB
            Left = 9
            Top = 75
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdProdChange
            DBLookupComboBox = CBProd
            IgnoraDBLookupComboBox = False
          end
          object CBProd: TdmkDBLookupComboBox
            Left = 80
            Top = 75
            Width = 357
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsGrades
            TabOrder = 1
            OnExit = CBProdExit
            dmkEditCB = EdProd
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProdQtd: TdmkEdit
            Left = 837
            Top = 75
            Width = 123
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 1
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdProdQtdChange
          end
          object EdProdTot: TdmkEdit
            Left = 967
            Top = 75
            Width = 123
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object BtConfItem: TBitBtn
            Tag = 14
            Left = 1094
            Top = 52
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            OnClick = BtConfItemClick
          end
          object EdCor: TdmkEditCB
            Left = 588
            Top = 46
            Width = 43
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCorChange
            DBLookupComboBox = CBCor
            IgnoraDBLookupComboBox = False
          end
          object CBCor: TdmkDBLookupComboBox
            Left = 453
            Top = 75
            Width = 178
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Cor'
            ListField = 'NOMECOR'
            ListSource = DsGradesCors
            TabOrder = 3
            dmkEditCB = EdCor
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTam: TdmkEditCB
            Left = 783
            Top = 46
            Width = 43
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdTamChange
            DBLookupComboBox = CBTam
            IgnoraDBLookupComboBox = False
          end
          object CBTam: TdmkDBLookupComboBox
            Left = 647
            Top = 75
            Width = 179
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Tam'
            ListField = 'NOMETAM'
            ListSource = DsGradesTams
            TabOrder = 5
            dmkEditCB = EdTam
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProdVal: TdmkEdit
            Left = 967
            Top = 25
            Width = 123
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdEstQ: TdmkEdit
            Left = 837
            Top = 25
            Width = 123
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 108
          Width = 839
          Height = 385
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 378
          object DBGridProd: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 839
            Height = 361
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tamanho'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
            Color = clWindow
            DataSource = FmMoviV.DsMovim
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGridProdCellClick
            OnKeyDown = DBGridProdKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tamanho'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 361
            Width = 148
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'Ctrl + Delete para excluir'
            TabOrder = 1
          end
        end
        object Panel6: TPanel
          Left = 844
          Top = 108
          Width = 307
          Height = 385
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          ExplicitLeft = 841
          ExplicitHeight = 378
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 307
            Height = 224
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 308
            object Image2: TImage
              Left = 6
              Top = 1
              Width = 296
              Height = 209
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Stretch = True
            end
          end
          object Panel12: TPanel
            Left = 0
            Top = 224
            Width = 307
            Height = 74
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 308
            object Label32: TLabel
              Left = 0
              Top = 0
              Width = 118
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'Status do pedido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
            end
            object Label33: TLabel
              Left = 0
              Top = 49
              Width = 56
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'Hist'#243'rico:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object DBText2: TDBText
              Left = 0
              Top = 18
              Width = 308
              Height = 31
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              DataField = 'NOMESTATAT'
              DataSource = FmMoviV.DsMoviV
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -27
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
            end
          end
          object Panel13: TPanel
            Left = 0
            Top = 298
            Width = 307
            Height = 87
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            ExplicitWidth = 308
            ExplicitHeight = 80
            object DBLookupListBox2: TDBLookupListBox
              Left = 0
              Top = 0
              Width = 307
              Height = 84
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Enabled = False
              KeyField = 'Controle'
              ListField = 'STATUS_STR'
              ListSource = FmMoviV.DsMoviVStat
              TabOrder = 0
              ExplicitWidth = 308
              ExplicitHeight = 52
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Kits'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1151
          Height = 79
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1148
          object Label34: TLabel
            Left = 9
            Top = 6
            Width = 17
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Kit:'
          end
          object Label35: TLabel
            Left = 967
            Top = 6
            Width = 34
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Total:'
          end
          object Label36: TLabel
            Left = 837
            Top = 6
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade:'
          end
          object EdKit: TdmkEditCB
            Left = 9
            Top = 26
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdKitChange
            DBLookupComboBox = CBKit
            IgnoraDBLookupComboBox = False
          end
          object CBKit: TdmkDBLookupComboBox
            Left = 81
            Top = 26
            Width = 747
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGradeK
            TabOrder = 1
            OnExit = CBKitExit
            dmkEditCB = EdKit
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtConfKit: TBitBtn
            Tag = 14
            Left = 1094
            Top = 6
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtConfKitClick
          end
          object EdTotKit: TdmkEdit
            Left = 967
            Top = 26
            Width = 123
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            Enabled = False
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdQtdeKit: TdmkEdit
            Left = 837
            Top = 26
            Width = 123
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdQtdeKitChange
          end
          object PBKit: TProgressBar
            Left = 1
            Top = 57
            Width = 1149
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 5
            Visible = False
            ExplicitWidth = 1146
          end
        end
        object Panel8: TPanel
          Left = 844
          Top = 79
          Width = 307
          Height = 414
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          ExplicitLeft = 841
          ExplicitHeight = 407
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 307
            Height = 224
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 308
            object Image3: TImage
              Left = 7
              Top = 7
              Width = 296
              Height = 210
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Stretch = True
            end
          end
          object Panel16: TPanel
            Left = 0
            Top = 224
            Width = 307
            Height = 74
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 308
            object Label37: TLabel
              Left = 0
              Top = 0
              Width = 118
              Height = 19
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'Status do pedido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
            end
            object Label38: TLabel
              Left = 0
              Top = 49
              Width = 56
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'Hist'#243'rico:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object DBText1: TDBText
              Left = 0
              Top = 18
              Width = 308
              Height = 31
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              DataField = 'NOMESTATAT'
              DataSource = FmMoviV.DsMoviV
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -27
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
            end
          end
          object Panel17: TPanel
            Left = 0
            Top = 298
            Width = 307
            Height = 116
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            ExplicitWidth = 308
            ExplicitHeight = 109
            object DBLookupListBox1: TDBLookupListBox
              Left = 0
              Top = 0
              Width = 307
              Height = 116
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Enabled = False
              KeyField = 'Controle'
              ListField = 'STATUS_STR'
              ListSource = FmMoviV.DsMoviVStat
              TabOrder = 0
              ExplicitWidth = 308
              ExplicitHeight = 100
            end
          end
        end
        object Panel18: TPanel
          Left = 0
          Top = 79
          Width = 844
          Height = 414
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 2
          ExplicitWidth = 841
          ExplicitHeight = 407
          object StaticText2: TStaticText
            Left = 1
            Top = 389
            Width = 241
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'F4 para editar - Ctrl + Delete para excluir'
            TabOrder = 0
          end
          object DBGridKit: TdmkDBGrid
            Left = 1
            Top = 1
            Width = 838
            Height = 388
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Kit'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtd'
                Title.Caption = 'Qtde.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FRETE'
                Title.Caption = 'Frete'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Total'
                Title.Caption = 'TOTAL'
                Width = 100
                Visible = True
              end>
            Color = clWindow
            DataSource = FmMoviV.DsMoviVK
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBGridKitCellClick
            OnKeyDown = DBGridKitKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Kit'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtd'
                Title.Caption = 'Qtde.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FRETE'
                Title.Caption = 'Frete'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Total'
                Title.Caption = 'TOTAL'
                Width = 100
                Visible = True
              end>
          end
        end
      end
    end
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTI'
      'FROM entidades en'
      'WHERE en.Cliente1="V"'
      'ORDER BY NOMEENTI')
    Left = 12
    Top = 9
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClienteNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 40
    Top = 9
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Aplicacao & 2'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 68
    Top = 9
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 96
    Top = 9
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradesBeforeClose
    AfterScroll = QrGradesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 124
    Top = 9
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 152
    Top = 9
  end
  object DsGradesCors: TDataSource
    DataSet = QrGradesCors
    Left = 208
    Top = 9
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, pro.Cor'
      'FROM produtos pro'
      'LEFT JOIN cores cor ON cor.Codigo = pro.Cor'
      'WHERE pro.Codigo=:P0'
      'AND pro.Ativo=1'
      'GROUP BY pro.Cor'
      'ORDER BY NOMECOR')
    Left = 180
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
      Size = 30
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'gradescors.Cor'
      Required = True
    end
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, pro.Tam'
      'FROM produtos pro'
      'LEFT JOIN tamanhos tam ON tam.Codigo = pro.Tam'
      'WHERE pro.Codigo=:P0'
      'AND pro.Cor=:P1'
      'AND pro.Ativo=1'
      'GROUP BY pro.Tam'
      'ORDER BY tam.Codigo')
    Left = 236
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'gradestams.Tam'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsGradesTams: TDataSource
    DataSet = QrGradesTams
    Left = 264
    Top = 9
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 292
    Top = 9
  end
  object DsGradeK: TDataSource
    DataSet = QrGradeK
    Left = 348
    Top = 9
  end
  object QrGradeK: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gradek'
      'WHERE Ativo = 1')
    Left = 320
    Top = 9
    object QrGradeKCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradek.Codigo'
    end
    object QrGradeKNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gradek.Nome'
      Size = 100
    end
  end
  object QrLoc2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 376
    Top = 9
  end
  object QrMoviVUpd: TmySQLQuery
    Database = Dmod.MyDB
    Left = 404
    Top = 9
  end
  object QrTot: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTotCalcFields
    SQL.Strings = (
      'SELECT SUM(Ven) TOTAL'
      'FROM movim'
      'WHERE Controle=:P0'
      'AND Kit=:P1')
    Left = 168
    Top = 385
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTotTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTotTOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
end
