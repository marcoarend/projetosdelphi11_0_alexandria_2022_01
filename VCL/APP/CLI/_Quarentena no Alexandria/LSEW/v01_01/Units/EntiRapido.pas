unit EntiRapido;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, ComCtrls, Grids,
  DBGrids, dmkGeral, dmkEdit, Variants, dmkLabel, dmkEditDateTimePicker, Menus,
  UnDmkEnums;

type
  TFmEntiRapido = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label31: TLabel;
    EdRua: TEdit;
    Label32: TLabel;
    EdNumero: TEdit;
    Label33: TLabel;
    EdCompl: TEdit;
    Label34: TLabel;
    EdBairro: TEdit;
    Label39: TLabel;
    EdCEP: TEdit;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    CBLograd: TDBLookupComboBox;
    Label97: TLabel;
    Panel1: TPanel;
    EdNome: TEdit;
    Label25: TLabel;
    EdCNPJCPF: TEdit;
    Label29: TLabel;
    Label30: TLabel;
    EdIERG: TEdit;
    DsListaLograd: TDataSource;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    RGTipo: TRadioGroup;
    EdCidade: TEdit;
    Label1: TLabel;
    EdEmeio: TEdit;
    Label2: TLabel;
    Label43: TLabel;
    EdTe1: TEdit;
    EdTe2: TEdit;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    CBUF: TDBLookupComboBox;
    Label7: TLabel;
    QrUFs: TmySQLQuery;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    DsUFs: TDataSource;
    Label3: TLabel;
    EdSerial: TEdit;
    Label4: TLabel;
    EdSerialG: TdmkEdit;
    EdSerialN: TdmkEdit;
    QrDuplic2Cliente2: TWideStringField;
    QrDuplic2Cliente1: TWideStringField;
    RGTipoEnti: TRadioGroup;
    LaTipo: TdmkLabel;
    Label102: TLabel;
    EdSSP: TEdit;
    Label118: TLabel;
    TPDataRG: TdmkEditDateTimePicker;
    EdTe3: TEdit;
    Label6: TLabel;
    EdCel: TEdit;
    Label44: TLabel;
    Label105: TLabel;
    Label5: TLabel;
    EdFax: TEdit;
    BtCEP: TButton;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    EdPais: TEdit;
    Label8: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EdMD5Cab: TdmkEdit;
    Label10: TLabel;
    EdSequencia: TdmkEdit;
    GBSenha: TGroupBox;
    Ed_4_2: TdmkEdit;
    Ed_4_1: TdmkEdit;
    Ed_4_4: TdmkEdit;
    Ed_4_3: TdmkEdit;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    QrMD5Cab: TmySQLQuery;
    QrMD5CabCodigo: TIntegerField;
    QrMD5CabSenha: TWideStringField;
    QrMD5CabNome: TWideStringField;
    QrMD5CabForca: TSmallintField;
    QrMD5CabQuartos: TSmallintField;
    La_4_1: TLabel;
    La_4_2: TLabel;
    La_4_3: TLabel;
    La_4_4: TLabel;
    PnAviso: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    LaQuarto1: TLabel;
    LaQuarto2: TLabel;
    LaQuarto3: TLabel;
    LaQuarto4: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdTe1Exit(Sender: TObject);
    procedure EdTe2Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdSerialExit(Sender: TObject);
    procedure EdCelExit(Sender: TObject);
    procedure EdTe3Exit(Sender: TObject);
    procedure EdFaxExit(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure BtCEPClick(Sender: TObject);
    procedure EdMD5CabExit(Sender: TObject);
    procedure Ed_4_1Change(Sender: TObject);
  private
    { Private declarations }
    function InsereEntidade(): Integer;
    function InsereMD5Alu(Aluno: Integer): Boolean;
    procedure VerificaSenhaPorAjuda();
    procedure ConfiguraAviso(Index: Integer);
  public
    { Public declarations }
    FAviso, FCodiEnti: Integer;
    FAlunoPesq: Boolean;
    procedure LocalizaSenhaAluno();
  end;

var
  FmEntiRapido: TFmEntiRapido;

implementation

uses Module, Entidades, ResIntStrings, UnFinanceiro, Principal, AlunoPesq,
MyDBCheck, EntiCEP, UnitMD5, UnMsgInt, UnMyObjects;

{$R *.DFM}

procedure TFmEntiRapido.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRapido.ConfiguraAviso(Index: Integer);
begin
  case Index of
    0: PnAviso.Caption := 'Informe a contra-senha!';
    1: PnAviso.Caption := 'Contra senha inv�lida, tipo n�o � de 32 caracteres!';
    2: PnAviso.Caption := 'Contra senha inv�lida ou n�o cadastrada';
    3: PnAviso.Caption := '';
    //...
    9: PnAviso.Caption := 'SENHA CONFERE!!!';
  end;
  if Index = 9 then
    PnAviso.Font.Color := clNavy
  else
    PnAviso.Font.Color := clred;
end;

procedure TFmEntiRapido.Descobrir1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    FmEntiCEP.ShowModal;
    if FmEntiCEP.CkCEP.Checked then EdCEP.Text               := FmEntiCEP.FCEP;
    if FmEntiCEP.CkTipoLograd.Checked then CBLograd.KeyValue := FmEntiCEP.FLogrTip;
    if FmEntiCEP.CkLogradouro.Checked then EdRua.Text        := FmEntiCEP.FLogrTxt;
    if FmEntiCEP.CkBairroIni.Checked then EdBairro.Text      := FmEntiCEP.FBairroI;
    if FmEntiCEP.CkBairroFim.Checked then EdBairro.Text      := FmEntiCEP.FBairroF;
    if FmEntiCEP.CkCidade.Checked then EdCidade.Text         := FmEntiCEP.FLocalTxt;
    if FmEntiCEP.CkUF.Checked then CBUF.KeyValue             := FmEntiCEP.FUF_Txt;
    if FmEntiCEP.CkBrasil.Checked then EdPais.Text           := FmEntiCEP.FPais;
    FmEntiCEP.Destroy;
    EdNumero.SetFocus;
  end;
end;

procedure TFmEntiRapido.EdSerialExit(Sender: TObject);
begin
  LocalizaSenhaAluno();
end;

procedure TFmEntiRapido.LocalizaSenhaAluno();
var
  Grupo, Numero: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    EdSerialG.ValueVariant := 0;
    EdSerialN.ValueVariant := 0;
    Dmod.VerificaSenhaEntidade(EdSerial.Text, Grupo, Numero,
      FmPrincipal.Fmy_key_uEncrypt);
    EdSerialG.ValueVariant := Grupo;
    EdSerialN.ValueVariant := Numero;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEntiRapido.BtCEPClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, BtCEP);
end;

procedure TFmEntiRapido.Verificar1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      EdCEPLoc.Text   := EdCEP.Text;
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmEntiRapido.VerificaSenhaPorAjuda;
  procedure AtualizaQuarto(Ed: TdmkEdit; La, Quarto: TLabel; var Itens: Integer);
  var
    X: Integer;
  begin
    X := Length(Ed.Text);
    if X < 8 then
    begin
      X := 8 - X;
      if X = 1 then
        La.Caption := 'Falta um caracter'
      else
        La.Caption := 'Faltam ' + IntToStr(X) + ' caracteres';
    end else begin
      if Ed.Text = Quarto.Caption then
        La.Caption := CO_OK
      else
        La.Caption := CO_ERRO
      // Parei aqui
    end;
    if La.Caption = CO_OK then
    begin
      Itens := Itens + 1;
      La.Font.Color := clBlue;
    end else
      La.Font.Color := clRed;
  end;
  var
    K: Integer;
begin
  K := 0;
  AtualizaQuarto(Ed_4_1, La_4_1, LaQuarto1, K);
  AtualizaQuarto(Ed_4_2, La_4_2, LaQuarto1, K);
  AtualizaQuarto(Ed_4_3, La_4_3, LaQuarto1, K);
  AtualizaQuarto(Ed_4_4, La_4_4, LaQuarto1, K);
  //
  if K >= QrMD5CabQuartos.Value then
  begin
    EdSerialG.Text := EdMD5Cab.Text;
    EdSerialN.Text := EdSequencia.Text;
    //
    ConfiguraAviso(9);
  end;
end;

procedure TFmEntiRapido.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEntiRapido.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdSerial.CharCase := ecNormal;
  EdEmeio.CharCase  := ecNormal;
end;

procedure TFmEntiRapido.EdCNPJCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Erro(SMLA_NUMEROINVALIDO2);
      EdCNPJCPF.SetFocus;
    end else
      EdCNPJCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else
    EdCNPJCPF.Text := CO_VAZIO;
end;

procedure TFmEntiRapido.EdMD5CabExit(Sender: TObject);
var
  Codigo, Sequencia: Integer;
  Serial: String;
begin
  //
  Ed_4_1.Text := '';
  Ed_4_2.Text := '';
  Ed_4_3.Text := '';
  Ed_4_4.Text := '';
  //
  LaQuarto1.Caption := '';
  LaQuarto2.Caption := '';
  LaQuarto3.Caption := '';
  LaQuarto4.Caption := '';
  //
  Codigo    := Geral.IMV(EdMD5Cab.ValueVariant);
  Sequencia := Geral.IMV(EdSequencia.ValueVariant);
  if Codigo > 0 then
  begin
    QrMD5Cab.Close;
    QrMD5Cab.Params[0].AsInteger := Codigo;
    QrMD5Cab.Open;
    //
    GBSenha.Enabled := (QrMD5Cab.RecordCount > 0) and (QrMD5CabForca.Value = 1)
      and (EdSequencia.Text <> '');
    //
    if GBSenha.Enabled then
    begin
      DMod.QrSerialUF.Close;
      DMod.QrSerialUF.Params[00].AsInteger := Codigo;
      DMod.QrSerialUF.Params[01].AsInteger := Sequencia;
      DMod.QrSerialUF.Open;
      //
      if DMod.QrSerialUFControle.Value > 0 then
      begin
        ConfiguraAviso(3);
        Serial := UnMD5.StrMD5(FormatFloat('00000000000', Sequencia) +
          QrMD5CabSenha.Value);
        LaQuarto1.Caption := Copy(Serial, 01, 8);
        LaQuarto2.Caption := Copy(Serial, 09, 8);
        LaQuarto3.Caption := Copy(Serial, 17, 8);
        LaQuarto4.Caption := Copy(Serial, 25, 8);
      end else
      //
    end else ConfiguraAviso(2);
      ConfiguraAviso(1);
  end else ConfiguraAviso(0);
end;

procedure TFmEntiRapido.EdFaxExit(Sender: TObject);
begin
  EdFax.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdFax.Text));
end;

procedure TFmEntiRapido.EdCelExit(Sender: TObject);
begin
  EdCel.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdCel.Text));
end;

procedure TFmEntiRapido.EdCEPExit(Sender: TObject);
begin
  EdCEP.Text := MLAGeral.FormataCEP_TT(EdCEP.Text);
end;

procedure TFmEntiRapido.EdTe1Exit(Sender: TObject);
begin
  EdTe1.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTe1.Text));
end;

procedure TFmEntiRapido.EdTe2Exit(Sender: TObject);
begin
  EdTe2.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTe2.Text));
end;

procedure TFmEntiRapido.EdTe3Exit(Sender: TObject);
begin
  EdTe3.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTe3.Text));
end;

procedure TFmEntiRapido.Ed_4_1Change(Sender: TObject);
begin
  VerificaSenhaPorAjuda();
end;

procedure TFmEntiRapido.FormCreate(Sender: TObject);
begin
  QrListaLograd.Open;
  QrUFs.Open;
end;

procedure TFmEntiRapido.BtConfirmaClick(Sender: TObject);
var
  Aluno, SerialG, SerialN: Integer;
begin
  SerialG  := Geral.IMV(EdSerialG.Text);
  SerialN  := Geral.IMV(EdSerialN.Text);
  if (Trim(EdSerial.Text) <> '') and (EdSerialG.ValueVariant = 0) then
  begin
    Geral.MB_Aviso('Inclus�o de entidade abortada! O serial informado n�o � v�lido!');
    EdSerial.SetFocus;
    Exit;
  end;
  if RGTipoEnti.ItemIndex < 1 then
  begin
    Geral.MB_Aviso('Defina o tipo de entidade!');
    RGTipoEnti.SetFocus;
    Exit;
  end;
  if Dmod.SerialDuplicadoEntidade(SerialG, SerialN) and
  (Dmod.QrDuplMD5Entidade.Value <> FCodiEnti) then
  begin
    Geral.MB_Aviso('Inclus�o de entidade abortada! O serial ' +
      'j� est� cadastrado para a entidade n�mero ' +
      IntToStr(Dmod.QrDuplMD5Entidade.Value) + '!');
    EdSerial.SetFocus;
    Exit;
  end;
  Aluno := InsereEntidade;
  if Aluno <> 0 then
  begin
    if InsereMD5Alu(Aluno) = True then
    begin
      if SerialG = 0 then
        Geral.MB_Aviso('Entidade cadastrada com sucesso!')
      else
        Geral.MB_Aviso('Entidade e serial cadastrados com sucesso!');
      //
      EdSerial.Text := '';
      EdSerialG.ValueVariant := 0;
      EdSerialN.ValueVariant := 0;
    end else
    // somente ap�s InsereMD5Alu
    EdNome.Text          := '';
    EdEmeio.Text         := '';
    EdCNPJCPF.Text       := '';
    EdIERG.Text          := '';
    EdTe1.Text           := '';
    EdCel.Text           := '';
    EdNome.SetFocus;
    EdMD5Cab.ValueVariant    := 0;
    EdSequencia.ValueVariant := 0;
    if (LaTipo.SQLType = stUpd) and (FAlunoPesq) then
    begin
      FmAlunoPesq.QrPesq.Close;
      FmAlunoPesq.QrPesq.Open;
      FmAlunoPesq.QrPesq.Locate('Codigo', Aluno, []);
      Close;
    end;
  end;
end;

function TFmEntiRapido.InsereMD5Alu(Aluno: Integer): Boolean;
var
  MD5Cab, MD5Num: Integer;
begin
  MD5Cab  := Geral.IMV(EdSerialG.Text);
  MD5Num  := Geral.IMV(EdSerialN.Text);
  //
  if MD5Cab > 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM md5alu WHERE Entidade=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Aluno;
    Dmod.QrUpd.ExecSQL;
    //
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'md5alu', False, ['MD5Cab',
    'MD5Num'], ['Entidade'], [MD5Cab, MD5Num], [Aluno], True) then
    begin
      Result := False;
      //
      Geral.MB_Erro('A entidade ' + EdNome.Text + '" C�digo ' +
        IntToStr(Aluno) + ' foi cadastradia com sucesso, mas n�o foi ' +
        'poss�vel adicionar o serial a ela! Informe o serial e a entidade ao ' +
        'seu superior!');
    end else
      Result := True;
  end else
    Result := True;
end;

function TFmEntiRapido.InsereEntidade(): Integer;
  procedure ConfirmaQueEntidadeEhAluno(Entidade: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET Cliente2="V"');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=' + IntToStr(Entidade));
    Dmod.QrUpd.ExecSQL;
  end;
var
  PUF, EUF, Veiculo, Codigo, Motivo, ELograd, PLograd, CLograd,
  LLograd, CasasApliDesco, Tipo, ENumero, PNumero: Integer;

  Fantasia, Respons2, Mae, NIRE, Simples, Atividade, Apelido, CPF_Pai, SSP,
  CidadeNatal, EstCivil, Nacionalid, EPais, Ete2, Respons1, Pai,
  FormaSociet, IEST, CPF_Conjuge, DataRG, UFNatal, Ete3, EFax, EContato,
  PPais, Pte2, PFax, PContato, EEmail, Pte3, PEmail, Responsavel,
  Sexo, Profissao, Cargo, DiaRecibo, AjudaEmpV, AjudaEmpP, Cliente3, Fornece5,
  Cadastro, Observacoes, CRua, CCompl, CCidade, CCEP, CTel, CFax, LRua,
  LCompl, LCidade, LCEP, LTel, LFax, Situacao, Grupo, ConjugeNatal,
  Nome1, Nome2, Nome3, Natal4, CreditosL, CreditosF2, CreditosD, CreditosU,
  RazaoSocial, Nome, CPF, CNPJ, IE, RG, ERua, PRua, EBairro, PBairro, ECompl,
  PCompl, ECidade, PCidade, ECEP, PCEP, ETe1, PTe1, ECel, PCel, ENatal, PNatal,
  Cliente1, Cliente2, Fornece1, Fornece2, Fornece3, Fornece4, Terceiro, Mensal,
  Agenda, SenhaQuer, Recibo, Cliente4, Fornece6, Informacoes, CNumero, CBairro,
  CUF, CPais, CCel, CContato, LNumero, LBairro, LUF, LPais, LCel, LContato,
  Nivel, Account, Natal1, Natal2, CreditosI, CreditosV, QuantI1, QuantI3,
  QuantN1, QuantN3, Senha1, TempA, TempD, Agencia, FatorCompra, AdValorem,
  DMaisC, Empresa, SCB, EAtividad, PCidadeCod, ECidadeCod, ConjugeNome,
  Natal3, QuantI2, QuantI4, QuantN2, Banco, ContaCorrente, DMaisD, CBE,
  PPaisCod, Antigo, Contab, PastaTxtFTP, PastaPwdFTP, Nome4, PAtividad,
  EPaisCod, CUF2, MSN1, Protestar, MultaCodi, MultaDias, MultaValr, MultaPerc,
  MultaTiVe, JuroSacado, CPMF, CPF_Resp1, Corrido, CliInt: String;

  AjudaV, AjudaP, LimiCred, Comissao, Desco: Double;
  //
begin
  Result := 0;
  Tipo := RGTipo.ItemIndex -1;
  if Tipo = -1 then
  begin
    Geral.MB_Aviso('Defina o tipo de cadastro!');
    Exit;
  end;
  if (Length(Geral.SoNumero1a9_TT(EdCNPJCPF.Text))>0)
  then begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    if Tipo = 0 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo, Cliente1, Cliente2 FROM entidades');
      QrDuplic2.SQL.Add('WHERE CNPJ="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo, Cliente1, Cliente2 FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
    end;
    QrDuplic2.Open;
    if GOTOy.Registros(QrDuplic2) > 0 then
    begin
      if (Uppercase(QrDuplic2Cliente1.Value) = 'V')
      or (Uppercase(QrDuplic2Cliente2.Value) = 'V') then
      begin
        {
        if Geral.MB_Pergunta('Esta entidade j� foi cadastrada com o c�digo n� '+
          IntToStr(QrDuplic2Codigo.Value)+'. Deseja utilizar este n�mero?') = ID_YES then
        begin
          Result := QrDuplic2Codigo.Value;
          ConfirmaQueEntidadeEhAluno(Result);
        end;
        }
      end else
      begin
        Geral.MB_Aviso('J� existe uma entidade cadastrada com ' +
          'o CPF/CNPJ indicado, mas o tipo de cadastro � restrito. ' +
          'Contate seu superior para ter acesso ao cadastro!');
      end;
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      //if LaTipo.SQLType = stIns then
        //Exit;
    end;
    QrDuplic2.Close;
  end else begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    if Tipo = 0 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo, Cliente1, Cliente2 FROM entidades');
      QrDuplic2.SQL.Add('WHERE RazaoSocial="'+EdNome.Text+'"');
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo, Cliente1, Cliente2 FROM entidades');
      QrDuplic2.SQL.Add('WHERE Nome="'+EdNome.Text+'"');
    end;
    QrDuplic2.Open;
    if (GOTOy.Registros(QrDuplic2) > 0) and (LaTipo.SQLType = stIns) then
    begin
      if Geral.MB_Pergunta('J� existe um cadastro para "' + EdNome.Text +
        '" sob o n�mero ' + IntToStr(QrDuplic2Codigo.Value) + '!') = ID_YES then
      begin
        Result := QrDuplic2Codigo.Value;
        ConfirmaQueEntidadeEhAluno(Result);
        Screen.Cursor := crDefault;
        Exit;
      end;
      QrDuplic2.Close;
    end;
    QrDuplic2.Close;
  end;
  if Tipo = 0 then
  begin
    RazaoSocial := EdNome.Text;
    Nome    := '';
    CNPJ    := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
    CPF     := '';
    IE      := EdIERG.Text;
    RG      := '';
    ERua    := EdRua.Text;
    PRua    := '';
    ENumero    := Geral.IMV(EdNumero.Text);
    PNumero    := 0;
    ECompl  := EdCompl.Text;
    PCompl  := '';
    EBairro := EdBairro.Text;
    PBairro := '';
    ECidade := EdCidade.Text;
    PCidade := '';
    ECEP    := Geral.SoNumero_TT(EdCEP.Text);
    PCEP    := '';
    ETe1    := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
    PTe1    := '';
    ETe2    := MlaGeral.SoNumeroESinal_TT(EdTe2.Text);
    PTe2    := '';
    ETe3    := MlaGeral.SoNumeroESinal_TT(EdTe3.Text);
    PTe3    := '';
    ECel    := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
    PCel    := '';
    EFax    := MlaGeral.SoNumeroESinal_TT(EdFax.Text);
    PFax    := '';
    if CBUF.KeyValue = Null then EUF := 0 else EUF := CBUF.KeyValue;
    PUF     := 0;
    EEmail  := EdEmeio.Text;
    PEmail  := '';
    if CBLograd.KeyValue <> Null then
      ELograd := CBLograd.KeyValue
    else
      ELograd := 0;
    PLograd := 0;
    EPais   := EdPais.Text;
    PPais   := '';
  end else begin
    Nome  := EdNome.Text;
    RazaoSocial := '';
    CPF   := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
    CNPJ  := '';
    RG    := EdIERG.Text;
    IE    := '';
    PRua    := EdRua.Text;
    ERua    := '';
    PNumero    := Geral.IMV(EdNumero.Text);
    ENumero    := 0;
    PCompl  := EdCompl.Text;
    ECompl  := '';
    PBairro := EdBairro.Text;
    EBairro := '';
    PCidade := EdCidade.Text;
    ECidade := '';
    PCEP    := Geral.SoNumero_TT(EdCEP.Text);
    ECEP    := '';
    PTe1    := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
    ETe1    := '';
    PTe2    := MlaGeral.SoNumeroESinal_TT(EdTe2.Text);
    ETe2    := '';
    PTe3    := MlaGeral.SoNumeroESinal_TT(EdTe3.Text);
    ETe3    := '';
    PCel    := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
    ECel    := '';
    PFax    := MlaGeral.SoNumeroESinal_TT(EdFax.Text);
    EFAx    := '';
    if CBUF.KeyValue = Null then PUF := 0 else PUF := CBUF.KeyValue;
    EUF     := 0;
    EEmail  := '';
    PEmail  := EdEmeio.Text;
    ELograd := 0;
    if CBLograd.KeyValue <> Null then
      PLograd := CBLograd.KeyValue
    else
      PLograd := 0;
    PPais   := EdPais.Text;
    EPais   := '';
  end;
  SSP      := EdSSP.Text;
  DataRG   := Geral.FDT(TPDataRG.Date, 1);
  Mensal   := 'F';
  Veiculo  := 0;
  LimiCred := 0;
  Comissao := 0;
  Desco    := 0;
  CasasApliDesco := 0;
  Motivo := 0;
  CLograd := 0;
  LLograd := 0;
  if RGTipoEnti.ItemIndex in ([2,3]) then Cliente1 := 'V' else Cliente1 := 'F';
  if RGTipoEnti.ItemIndex in ([1,3]) then Cliente2 := 'V' else Cliente2 := 'F';
  Cliente3 := 'F';
  Cliente4 := 'F';
  Fornece1 := 'F';
  Fornece2 := 'F';
  Fornece3 := 'F';
  Fornece4 := 'F';
  Fornece5 := 'F';
  Fornece6 := 'F';
  Terceiro := 'F';
  AjudaV := 0;
  AjudaP := 0;
  Agenda := 'F';
  SenhaQuer := 'F';
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  ///
  if LaTipo.SQLType = stIns then
  begin
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Entidades',
            'Entidades', 'Codigo');
  end else begin
    Codigo := FCodiEnti;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'entidades', False, [
    'RazaoSocial', 'Fantasia', 'Respons1', 'Respons2', 'Pai',
    'Mae', 'CNPJ', 'IE', 'NIRE', 'FormaSociet', 'Simples',
    'IEST', 'Atividade', 'Nome', 'Apelido', 'CPF', 'CPF_Pai',
    'CPF_Conjuge', 'RG', 'SSP', 'DataRG', 'CidadeNatal',
    'EstCivil', 'UFNatal', 'Nacionalid', 'ELograd', 'ERua',
    'ENumero', 'ECompl', 'EBairro', 'ECidade', 'EUF', 'ECEP',
    'EPais', 'ETe1', 'Ete2', 'Ete3', 'ECel', 'EFax', 'EEmail',
    'EContato', 'ENatal', 'PLograd', 'PRua', 'PNumero', 'PCompl',
    'PBairro', 'PCidade', 'PUF', 'PCEP', 'PPais', 'PTe1', 'Pte2',
    'Pte3', 'PCel', 'PFax', 'PEmail', 'PContato', 'PNatal',
    'Sexo', 'Responsavel', 'Profissao', 'Cargo', 'Recibo',
    'DiaRecibo', 'AjudaEmpV', 'AjudaEmpP', 'Cliente1', 'Cliente2',
    'Cliente3', 'Cliente4', 'Fornece1', 'Fornece2', 'Fornece3',
    'Fornece4', 'Fornece5', 'Fornece6', 'Terceiro', 'Cadastro',
    'Informacoes', 'Veiculo', 'Mensal', 'Observacoes',
    'Tipo', 'CLograd', 'CRua', 'CNumero', 'CCompl', 'CBairro',
    'CCidade', 'CUF', 'CCEP', 'CPais', 'CTel', 'CCel', 'CFax',
    'CContato', 'LLograd', 'LRua', 'LNumero', 'LCompl', 'LBairro',
    'LCidade', 'LUF', 'LCEP', 'LPais', 'LTel', 'LCel', 'LFax',
    'LContato', 'Comissao', 'Situacao', 'Nivel', 'Grupo',
    'Account', 'ConjugeNome', 'ConjugeNatal', 'Nome1',
    'Natal1', 'Nome2', 'Natal2', 'Nome3', 'Natal3', 'Nome4',
    'Natal4', 'CreditosI', 'CreditosL', 'CreditosF2',
    'CreditosD', 'CreditosU', 'CreditosV', 'Motivo', 'QuantI1',
    'QuantI2', 'QuantI3', 'QuantI4', 'QuantN1', 'QuantN2',
    'QuantN3', 'Agenda', 'SenhaQuer', 'Senha1', 'LimiCred',
    'Desco', 'CasasApliDesco', 'TempA', 'TempD', 'Banco',
    'Agencia', 'ContaCorrente', 'FatorCompra', 'AdValorem',
    'DMaisC', 'DMaisD', 'Empresa', 'CBE', 'SCB', 'PAtividad',
    'EAtividad', 'PCidadeCod', 'ECidadeCod', 'PPaisCod',
    'EPaisCod', 'Antigo', 'CUF2', 'Contab', 'MSN1', 'PastaTxtFTP',
    'PastaPwdFTP', 'Protestar', 'MultaCodi', 'MultaDias',
    'MultaValr', 'MultaPerc', 'MultaTiVe', 'JuroSacado', 'CPMF',
    'Corrido', 'CPF_Resp1', 'CliInt'
  ], ['Codigo'], [
    RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ,
    IE, NIRE, FormaSociet, Simples, IEST, Atividade, Nome,
    Apelido, CPF, CPF_Pai, CPF_Conjuge, RG, SSP, DataRG,
    CidadeNatal, EstCivil, UFNatal, Nacionalid, ELograd, ERua,
    ENumero, ECompl, EBairro, ECidade, EUF, ECEP, EPais, ETe1,
    Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PLograd,
    PRua, PNumero, PCompl, PBairro, PCidade, PUF, PCEP, PPais,
    PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal,
    Sexo, Responsavel, Profissao, Cargo, Recibo, DiaRecibo,
    AjudaEmpV, AjudaEmpP, Cliente1, Cliente2, Cliente3, Cliente4,
    Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6,
    Terceiro, Cadastro, Informacoes, Veiculo, Mensal,
    Observacoes, Tipo, CLograd, CRua, CNumero, CCompl, CBairro,
    CCidade, CUF, CCEP, CPais, CTel, CCel, CFax, CContato,
    LLograd, LRua, LNumero, LCompl, LBairro, LCidade, LUF,
    LCEP, LPais, LTel, LCel, LFax, LContato, Comissao,
    Situacao, Nivel, Grupo, Account, ConjugeNome,
    ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3,
    Natal3, Nome4, Natal4, CreditosI, CreditosL, CreditosF2,
    CreditosD, CreditosU, CreditosV, Motivo, QuantI1, QuantI2,
    QuantI3, QuantI4, QuantN1, QuantN2, QuantN3, Agenda,
    SenhaQuer, Senha1, LimiCred, Desco, CasasApliDesco, TempA,
    TempD, Banco, Agencia, ContaCorrente, FatorCompra, AdValorem,
    DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad, EAtividad,
    PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Antigo, CUF2,
    Contab, MSN1, PastaTxtFTP, PastaPwdFTP, Protestar, MultaCodi,
    MultaDias, MultaValr, MultaPerc, MultaTiVe, JuroSacado,
    CPMF, Corrido, CPF_Resp1, CliInt
  ], [Codigo], True) then
  begin
    QrPesq.Close;
    QrPesq.Params[0].AsInteger := Codigo;
    QrPesq.Open;
    //
    if QrPesqCodigo.Value = Codigo then Result := Codigo;
  end;
  Screen.Cursor := crDefault;
end;

end.



