object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 675
  ClientWidth = 1326
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object QrV_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsv'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 644
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrV_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrV_InfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object frxDsV_: TfrxDBDataset
    UserName = 'frxDsV_'
    CloseDataSource = False
    DataSet = QrV_
    BCDToCurrency = False
    Left = 672
    Top = 440
  end
  object QrXVol_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrXVol_qVol: TFloatField
      FieldName = 'qVol'
    end
    object QrXVol_esp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrXVol_marca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrXVol_nVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrXVol_pesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrXVol_pesoB: TFloatField
      FieldName = 'pesoB'
    end
  end
  object frxDsXvol_: TfrxDBDataset
    UserName = 'frxDsXvol_'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 672
    Top = 412
  end
  object QrCTeYIts_: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeyits')
    Left = 644
    Top = 384
    object QrCTeYIts_Seq1: TIntegerField
      FieldName = 'Seq1'
    end
    object QrCTeYIts_nDup1: TWideStringField
      FieldName = 'nDup1'
      Size = 60
    end
    object QrCTeYIts_dVenc1: TDateField
      FieldName = 'dVenc1'
    end
    object QrCTeYIts_vDup1: TFloatField
      FieldName = 'vDup1'
    end
    object QrCTeYIts_Seq2: TIntegerField
      FieldName = 'Seq2'
    end
    object QrCTeYIts_nDup2: TWideStringField
      FieldName = 'nDup2'
      Size = 60
    end
    object QrCTeYIts_dVenc2: TDateField
      FieldName = 'dVenc2'
    end
    object QrCTeYIts_vDup2: TFloatField
      FieldName = 'vDup2'
    end
    object QrCTeYIts_Seq3: TIntegerField
      FieldName = 'Seq3'
    end
    object QrCTeYIts_nDup3: TWideStringField
      FieldName = 'nDup3'
      Size = 60
    end
    object QrCTeYIts_dVenc3: TDateField
      FieldName = 'dVenc3'
    end
    object QrCTeYIts_vDup3: TFloatField
      FieldName = 'vDup3'
    end
    object QrCTeYIts_Seq4: TIntegerField
      FieldName = 'Seq4'
    end
    object QrCTeYIts_nDup4: TWideStringField
      FieldName = 'nDup4'
      Size = 60
    end
    object QrCTeYIts_dVenc4: TDateField
      FieldName = 'dVenc4'
    end
    object QrCTeYIts_vDup4: TFloatField
      FieldName = 'vDup4'
    end
    object QrCTeYIts_Seq5: TIntegerField
      FieldName = 'Seq5'
    end
    object QrCTeYIts_nDup5: TWideStringField
      FieldName = 'nDup5'
      Size = 60
    end
    object QrCTeYIts_dVenc5: TDateField
      FieldName = 'dVenc5'
    end
    object QrCTeYIts_vDup5: TFloatField
      FieldName = 'vDup5'
    end
    object QrCTeYIts_Linha: TIntegerField
      FieldName = 'Linha'
    end
    object QrCTeYIts_xVenc1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc1'
      Size = 10
      Calculated = True
    end
    object QrCTeYIts_xVenc2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc2'
      Size = 10
      Calculated = True
    end
    object QrCTeYIts_xVenc3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc3'
      Size = 10
      Calculated = True
    end
    object QrCTeYIts_xVenc4: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc4'
      Size = 10
      Calculated = True
    end
    object QrCTeYIts_xVenc5: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc5'
      Size = 10
      Calculated = True
    end
  end
  object frxDsCTeYIts_: TfrxDBDataset
    UserName = 'frxDsCTeYIts_'
    CloseDataSource = False
    DataSet = QrCTeYIts_
    BCDToCurrency = False
    Left = 672
    Top = 384
  end
  object QrY_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nDup, dVenc, vDup'
      'FROM nfecaby'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrY_nDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrY_dVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrY_vDup: TFloatField
      FieldName = 'vDup'
    end
  end
  object frxDsY_: TfrxDBDataset
    UserName = 'frxDsY_'
    CloseDataSource = False
    DataSet = QrY_
    BCDToCurrency = False
    Left = 672
    Top = 356
  end
  object QrO_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 644
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrO_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrO_IPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrO_IPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrO_IPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrO_IPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrO_IPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrO_IPI_CST: TWideStringField
      FieldName = 'IPI_CST'
      Size = 2
    end
    object QrO_IPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrO_IPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrO_IPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrO_IPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrO_IPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object frxDsO_: TfrxDBDataset
    UserName = 'frxDsO_'
    CloseDataSource = False
    DataSet = QrO_
    BCDToCurrency = False
    Left = 672
    Top = 328
  end
  object QrN_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 644
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrN_nItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrN_ICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrN_ICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrN_ICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrN_ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrN_ICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrN_ICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrN_ICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrN_ICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrN_ICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrN_ICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrN_ICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrN_ICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrN_ICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
  end
  object frxDsN_: TfrxDBDataset
    UserName = 'frxDsN_'
    CloseDataSource = False
    DataSet = QrN_
    BCDToCurrency = False
    Left = 672
    Top = 300
  end
  object QrI_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrI_prod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrI_prod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrI_prod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrI_prod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrI_prod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrI_prod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrI_prod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrI_prod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrI_prod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrI_prod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrI_prod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrI_prod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrI_prod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrI_prod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrI_prod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrI_prod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrI_prod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrI_prod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrI_Tem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrI_nItem: TIntegerField
      FieldName = 'nItem'
    end
  end
  object frxDsI_: TfrxDBDataset
    UserName = 'frxDsI_'
    CloseDataSource = False
    DataSet = QrI_
    BCDToCurrency = False
    Left = 672
    Top = 272
  end
  object QrA_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 644
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrA_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrA_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrA_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrA_LoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrA_versao: TFloatField
      FieldName = 'versao'
    end
    object QrA_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrA_ide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrA_ide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrA_ide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrA_ide_indPag: TSmallintField
      FieldName = 'ide_indPag'
    end
    object QrA_ide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrA_ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrA_ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrA_ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrA_ide_dSaiEnt: TDateField
      FieldName = 'ide_dSaiEnt'
    end
    object QrA_ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrA_ide_cMunFG: TIntegerField
      FieldName = 'ide_cMunFG'
    end
    object QrA_ide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrA_ide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrA_ide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrA_ide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrA_ide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrA_ide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrA_emit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrA_emit_CPF: TWideStringField
      FieldName = 'emit_CPF'
      Size = 11
    end
    object QrA_emit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrA_emit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrA_emit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrA_emit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrA_emit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrA_emit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrA_emit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrA_emit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrA_emit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrA_emit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrA_emit_cPais: TIntegerField
      FieldName = 'emit_cPais'
    end
    object QrA_emit_xPais: TWideStringField
      FieldName = 'emit_xPais'
      Size = 60
    end
    object QrA_emit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 10
    end
    object QrA_emit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrA_emit_IEST: TWideStringField
      FieldName = 'emit_IEST'
      Size = 14
    end
    object QrA_emit_IM: TWideStringField
      FieldName = 'emit_IM'
      Size = 15
    end
    object QrA_emit_CNAE: TWideStringField
      FieldName = 'emit_CNAE'
      Size = 7
    end
    object QrA_dest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrA_dest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrA_dest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrA_dest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrA_dest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrA_dest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrA_dest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrA_dest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrA_dest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrA_dest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrA_dest_CEP: TWideStringField
      FieldName = 'dest_CEP'
      Size = 8
    end
    object QrA_dest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrA_dest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrA_dest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 10
    end
    object QrA_dest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrA_dest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrA_ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrA_ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrA_ICMSTot_vBCST: TFloatField
      FieldName = 'ICMSTot_vBCST'
    end
    object QrA_ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrA_ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrA_ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrA_ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrA_ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrA_ICMSTot_vII: TFloatField
      FieldName = 'ICMSTot_vII'
    end
    object QrA_ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrA_ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrA_ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrA_ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrA_ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrA_ISSQNtot_vServ: TFloatField
      FieldName = 'ISSQNtot_vServ'
    end
    object QrA_ISSQNtot_vBC: TFloatField
      FieldName = 'ISSQNtot_vBC'
    end
    object QrA_ISSQNtot_vISS: TFloatField
      FieldName = 'ISSQNtot_vISS'
    end
    object QrA_ISSQNtot_vPIS: TFloatField
      FieldName = 'ISSQNtot_vPIS'
    end
    object QrA_ISSQNtot_vCOFINS: TFloatField
      FieldName = 'ISSQNtot_vCOFINS'
    end
    object QrA_RetTrib_vRetPIS: TFloatField
      FieldName = 'RetTrib_vRetPIS'
    end
    object QrA_RetTrib_vRetCOFINS: TFloatField
      FieldName = 'RetTrib_vRetCOFINS'
    end
    object QrA_RetTrib_vRetCSLL: TFloatField
      FieldName = 'RetTrib_vRetCSLL'
    end
    object QrA_RetTrib_vBCIRRF: TFloatField
      FieldName = 'RetTrib_vBCIRRF'
    end
    object QrA_RetTrib_vIRRF: TFloatField
      FieldName = 'RetTrib_vIRRF'
    end
    object QrA_RetTrib_vBCRetPrev: TFloatField
      FieldName = 'RetTrib_vBCRetPrev'
    end
    object QrA_RetTrib_vRetPrev: TFloatField
      FieldName = 'RetTrib_vRetPrev'
    end
    object QrA_ModFrete: TSmallintField
      FieldName = 'ModFrete'
    end
    object QrA_Transporta_CNPJ: TWideStringField
      FieldName = 'Transporta_CNPJ'
      Size = 14
    end
    object QrA_Transporta_CPF: TWideStringField
      FieldName = 'Transporta_CPF'
      Size = 11
    end
    object QrA_Transporta_XNome: TWideStringField
      FieldName = 'Transporta_XNome'
      Size = 60
    end
    object QrA_Transporta_IE: TWideStringField
      FieldName = 'Transporta_IE'
      Size = 14
    end
    object QrA_Transporta_XEnder: TWideStringField
      FieldName = 'Transporta_XEnder'
      Size = 60
    end
    object QrA_Transporta_XMun: TWideStringField
      FieldName = 'Transporta_XMun'
      Size = 60
    end
    object QrA_Transporta_UF: TWideStringField
      FieldName = 'Transporta_UF'
      Size = 2
    end
    object QrA_RetTransp_vServ: TFloatField
      FieldName = 'RetTransp_vServ'
    end
    object QrA_RetTransp_vBCRet: TFloatField
      FieldName = 'RetTransp_vBCRet'
    end
    object QrA_RetTransp_PICMSRet: TFloatField
      FieldName = 'RetTransp_PICMSRet'
    end
    object QrA_RetTransp_vICMSRet: TFloatField
      FieldName = 'RetTransp_vICMSRet'
    end
    object QrA_RetTransp_CFOP: TWideStringField
      FieldName = 'RetTransp_CFOP'
      Size = 4
    end
    object QrA_RetTransp_CMunFG: TWideStringField
      FieldName = 'RetTransp_CMunFG'
      Size = 7
    end
    object QrA_VeicTransp_Placa: TWideStringField
      FieldName = 'VeicTransp_Placa'
      Size = 8
    end
    object QrA_VeicTransp_UF: TWideStringField
      FieldName = 'VeicTransp_UF'
      Size = 2
    end
    object QrA_VeicTransp_RNTC: TWideStringField
      FieldName = 'VeicTransp_RNTC'
    end
    object QrA_Cobr_Fat_NFat: TWideStringField
      FieldName = 'Cobr_Fat_NFat'
      Size = 60
    end
    object QrA_Cobr_Fat_vOrig: TFloatField
      FieldName = 'Cobr_Fat_vOrig'
    end
    object QrA_Cobr_Fat_vDesc: TFloatField
      FieldName = 'Cobr_Fat_vDesc'
    end
    object QrA_Cobr_Fat_vLiq: TFloatField
      FieldName = 'Cobr_Fat_vLiq'
    end
    object QrA_InfAdic_InfAdFisco: TWideMemoField
      FieldName = 'InfAdic_InfAdFisco'
      BlobType = ftWideMemo
      Size = 255
    end
    object QrA_InfAdic_InfCpl: TWideMemoField
      FieldName = 'InfAdic_InfCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrA_Exporta_UFEmbarq: TWideStringField
      FieldName = 'Exporta_UFEmbarq'
      Size = 2
    end
    object QrA_Exporta_XLocEmbarq: TWideStringField
      FieldName = 'Exporta_XLocEmbarq'
      Size = 60
    end
    object QrA_Compra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Size = 17
    end
    object QrA_Compra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Size = 60
    end
    object QrA_Compra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Size = 60
    end
    object QrA_Status: TIntegerField
      FieldName = 'Status'
    end
    object QrA_infProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrA_infProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrA_infProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrA_infProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrA_infProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrA_infProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrA_infProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrA_infProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrA__Ativo_: TSmallintField
      FieldName = '_Ativo_'
    end
    object QrA_Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrA_DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrA_DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrA_UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrA_UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrA_AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrA_Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrA_IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrA_infCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 30
    end
    object QrA_infCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrA_infCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrA_infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrA_infCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrA_infCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrA_infCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrA_infCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrA_infCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrA_infCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrA_ID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ID_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrA_EMIT_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_FONE_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IE_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_IEST_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IEST_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_EMIT_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_CNPJ_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_DEST_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CNPJ_CPF_TXT'
      Size = 100
      Calculated = True
    end
    object QrA_DEST_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrA_DEST_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_CEP_TXT'
      Calculated = True
    end
    object QrA_DEST_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_FONE_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_DEST_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEST_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_TRANSPORTA_CNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_TRANSPORTA_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TRANSPORTA_IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrA_DOC_SEM_VLR_JUR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_JUR'
      Size = 255
      Calculated = True
    end
    object QrA_DOC_SEM_VLR_FIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_FIS'
      Size = 255
      Calculated = True
    end
  end
  object frxDsA_: TfrxDBDataset
    UserName = 'frxDsA_'
    CloseDataSource = False
    DataSet = QrA_
    BCDToCurrency = False
    Left = 672
    Top = 244
  end
  object QrCTeXMLI_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT xml.Codigo, xml.ID, xml.Valor, lay.Pai, lay.Descricao, la' +
        'y.Campo'
      'FROM nfexmli xml'
      'LEFT JOIN nfelayi lay ON lay.ID=xml.ID AND lay.Codigo=xml.Codigo'
      'WHERE xml.FatID=:P0'
      'AND xml.FatNum=:P1'
      'AND xml.Empresa=:P2')
    Left = 764
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeXMLI_Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrCTeXMLI_ID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrCTeXMLI_Valor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
    object QrCTeXMLI_Pai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrCTeXMLI_Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrCTeXMLI_Campo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
  end
  object frxDsCTeXMLI_: TfrxDBDataset
    UserName = 'frxDsCTeXMLI_'
    CloseDataSource = False
    DataSet = QrCTeXMLI_
    BCDToCurrency = False
    Left = 792
    Top = 284
  end
  object QrCTeCabA_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,'
      'ICMSTot_vProd, ICMSTot_vNF, '
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'IDCtrl, versao'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades cli ON '
      '/*  IF(nfa.dest_CNPJ<>'#39#39',nfa.dest_CNPJ=cli.CNPJ,'
      '  nfa.dest_CPF=cli.CPF)*/'
      'cli.Codigo=nfa.CodInfoDest'
      'ORDER BY nfa.DataCad DESC, nfa.ide_nNF DESC')
    Left = 724
    Top = 316
    object QrCTeCabA_NO_Cli: TWideStringField
      FieldName = 'NO_Cli'
      Size = 100
    end
    object QrCTeCabA_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCabA_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCabA_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCabA_LoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrCTeCabA_Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrCTeCabA_ide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrCTeCabA_ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCTeCabA_ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrCTeCabA_ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCTeCabA_ide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
    end
    object QrCTeCabA_ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCTeCabA_ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCTeCabA_cStat: TFloatField
      FieldName = 'cStat'
    end
    object TStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrCTeCabA_xMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrCTeCabA_dhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrCTeCabA_IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCTeCabA_versao: TFloatField
      FieldName = 'versao'
    end
  end
  object DsCTeCabA_: TDataSource
    DataSet = QrCTeCabA_
    Left = 752
    Top = 316
  end
  object frxDsCTeCabA_: TfrxDBDataset
    UserName = 'frxDsCTeCabA_'
    CloseDataSource = False
    DataSet = QrCTeCabA_
    BCDToCurrency = False
    Left = 780
    Top = 316
  end
  object QrCTeCabAMsg_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabamsg '
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 816
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeCabAMsg_FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCabAMsg_FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCabAMsg_Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCabAMsg_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeCabAMsg_Solicit: TIntegerField
      FieldName = 'Solicit'
    end
    object QrCTeCabAMsg_Id: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrCTeCabAMsg_tpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrCTeCabAMsg_verAplic: TWideStringField
      FieldName = 'verAplic'
      Size = 30
    end
    object QrCTeCabAMsg_dhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrCTeCabAMsg_nProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrCTeCabAMsg_digVal: TWideStringField
      FieldName = 'digVal'
      Size = 28
    end
    object QrCTeCabAMsg_cStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrCTeCabAMsg_xMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
  end
  object DsCTeCabAMsg_: TDataSource
    DataSet = QrCTeCabAMsg_
    Left = 844
    Top = 316
  end
  object frxA4A_001: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 41318.876849131900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child1.Visible := True;                                       ' +
        '           '
      'end;'
      ''
      'procedure Child1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Child1.Visible := False;  '
      'end;'
      ''
      'procedure MeFlowToHidOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if MeFlowToHid.Memo.Text <> '#39#39' then'
      '  begin                '
      '    Page3.Visible := True;'
      '    MeFlowToSee.Memo.Text := MeFlowToHid.Memo.Text;'
      '  end;              '
      'end;'
      ''
      'var'
      '  Altura: Extended;                                      '
      'procedure Me_ICMS_CSTOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '(*  if <frxDsN."ICMS_CST"> = 0 then'
      
        '    Me_ICMS_CST.Memo.Text := FormatFloat('#39'000'#39', <frxDsN."ICMS_CS' +
        'OSN">)  '
      '  else'
      
        '    Me_ICMS_CST.Memo.Text := FormatFloat('#39'000'#39', <frxDsN."ICMS_CS' +
        'T">);                                                      '
      '*)end;'
      ''
      'begin'
      '  if <LogoNFeExiste> = True then'
      '  begin              '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '(*'
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '    Picture3.LoadFromFile(<LogoNFePath>);'
      '*)  end;'
      '  //        '
      '(*  Altura := <NFeItsLin>;'
      
        '  Line1.Top := Altura;                                          ' +
        '    '
      
        '  Me_prod_cProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_xProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_NCM.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_CST.Height := Altura;                                 ' +
        '                       '
      
        '  Me_prod_CFOP.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_uCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_qCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_vUnCom.Height := Altura;                              ' +
        '                          '
      
        '  Me_prod_vProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_ICMS_vBC.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      
        '  Me_IPI_vIPI.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      '  Me_ICMS_pICMS.Height := Altura;'
      '  Me_IPI_pIPI.Height := Altura;'
      '  DD_prod.Height := Altura;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeRazao_2.Font.Size := <NFeFTRazao>;'
      '  MeRazao_3.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeEnder_2.Font.Size := <NFeFTEnder>;'
      '  MeEnder_3.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      '  MeFones_2.Font.Size := <NFeFTFones>;'
      '  MeFones_3.Font.Size := <NFeFTFones>;'
      '  //        '
      '*)end.')
    Left = 952
    Top = 268
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object Memo112: TfrxMemoView
        Left = 30.236240000000000000
        Top = 962.646260000000000000
        Width = 86.551181100000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DADOS ADICIONAIS')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 978.520275750000000000
        Width = 445.984251970000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INFORMA'#199#213'ES COMPLEMENTARES')
        ParentFont = False
      end
      object MeFlowToSor: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 987.969094650000000000
        Width = 445.984251970000000000
        Height = 106.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'frxDsA."InfAdic_InfAdFisco"'
          'frxDsA."InfAdic_InfCpl"'
          'frxDsA."InfCpl_totTrib"')
        ParentFont = False
        Formats = <
          item
          end
          item
          end
          item
          end>
      end
      object Memo115: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 978.520275750000000000
        Width = 287.244094490000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RESERVADO AO FISCO')
        ParentFont = False
      end
      object Memo116: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 991.748624650000000000
        Width = 287.244094490000000000
        Height = 102.803147170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        ParentFont = False
      end
      object MeFlowToHid: TfrxMemoView
        Left = 30.236240000000000000
        Top = 1099.843230000000000000
        Width = 733.228820000000000000
        Height = 7.559060000000000000
        OnAfterData = 'MeFlowToHidOnAfterData'
        OnAfterPrint = 'MeFlowToHidOnAfterPrint'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 80.503937010000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo13: TfrxMemoView
          Tag = 2
          Left = 30.236227800000000000
          Top = 7.558815910000000000
          Width = 733.228331810000000000
          Height = 287.244094488189000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -128
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '   PREVIEW')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 400.630180000000000000
          Top = 20.787396690000000000
          Width = 230.551181100000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 631.181117010000000000
          Top = 20.787396690000000000
          Width = 132.283440160000000000
          Height = 56.692908500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoDat: TfrxMemoView
          Left = 30.236240000000000000
          Top = 49.133853390000000000
          Width = 181.417322830000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'RG')
          ParentFont = False
        end
        object MeCanhotoIde: TfrxMemoView
          Left = 211.653579920000000000
          Top = 20.787396690000000000
          Width = 188.976377950000000000
          Height = 56.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ASSINATURA / CARIMBO')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo1: TfrxMemoView
          Left = 634.960647010000000000
          Top = 23.677180000000000000
          Width = 124.724490000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CT-e')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 634.850811100000000000
          Top = 39.551195750000000000
          Width = 22.677180000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 657.527991100000000000
          Top = 39.551195750000000000
          Width = 102.047310000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nCT">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 635.071281100000000000
          Top = 55.803164250000000000
          Width = 37.795300000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 672.866581100000000000
          Top = 55.803164250000000000
          Width = 86.929190000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 30.236240000000000000
          Top = 7.559050240000000000
          Width = 733.228427010000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'DECLARO QUE RECEBI OS VOLUMES DESTE CONHECIMENTO EM PERFEITO EST' +
              'ADO PELO QUE DOU POR CUMPRIDO O PRESENTE CONTRATO DE TRANSPORTE')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.787396690000000000
          Width = 181.417322830000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 404.409710000000000000
          Top = 20.787396690000000000
          Width = 226.771800000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'T'#201'RMINO DA PRESTA'#199#195'O - DATA / HORA')
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 404.409710000000000000
          Top = 47.244089610000000000
          Width = 226.771800000000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IN'#205'CIO DA PRESTA'#199#195'O - DATA / HORA')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 195.023622050000000000
        Top = 120.944960000000000000
        Width = 793.701300000000000000
        object Memo227: TfrxMemoView
          Left = 306.141930000000000000
          Top = 71.811070000000000000
          Width = 457.322932280000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Left = 306.141930000000000000
          Top = 11.338590000000000000
          Width = 389.291375200000000000
          Height = 30.236220470000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 11.338582680000000000
          Width = 275.905511810000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 32.125981810000000000
          Top = 15.118100470000000000
          Width = 68.031496060000000000
          Height = 68.031496060000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          Left = 306.141930000000000000
          Top = 170.078740160000000000
          Width = 457.322854170000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 695.433224650000000000
          Top = 11.338582680000000000
          Width = 68.031342280000000000
          Height = 30.236220472440900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 306.141634650000000000
          Top = 109.606299210000000000
          Width = 457.322932280000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Chave de acesso para consulta de autenticidade no site www.cte.f' +
              'azenda.gov.br ou na Sefaz Autorizadora')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 306.141732283464600000
          Top = 116.165430000000000000
          Width = 453.543307090000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 103.937081100000000000
          Top = 11.338590000000000000
          Width = 200.315090000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 103.937081100000000000
          Top = 37.795300000000000000
          Width = 200.315090000000000000
          Height = 34.015752910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 103.937081100000000000
          Top = 71.811033390000000000
          Width = 200.315090000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 147.401574800000000000
          Width = 275.905414170000000000
          Height = 45.354330708661420000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 154.960629920000000000
          Width = 275.149508660000000000
          Height = 34.015748031496060000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 306.141634650000000000
          Top = 132.283464570000000000
          Width = 457.322932280000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 306.141634650000000000
          Top = 177.637795280000000000
          Width = 453.543402280000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 30.236240000000000000
          Top = 5.669291338582680000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo192: TfrxMemoView
          Left = 306.141685900000000000
          Top = 22.677167800000000000
          Width = 389.291375200000000000
          Height = 20.787391810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR DO CONHECIMENTO'
            'DE TRANSPORTE ELETR'#212'NICO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 697.323291100000000000
          Top = 5.669291338582680000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo147: TfrxMemoView
          Left = 306.141930000000000000
          Top = 11.338590000000000000
          Width = 389.291375200000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DACTE')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo123: TfrxMemoView
          Left = 695.433520000000000000
          Top = 11.338590000000000000
          Width = 68.031325200000000000
          Height = 13.228331810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'MODAL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 695.433520000000000000
          Top = 24.566929133858300000
          Width = 68.031325200000000000
          Height = 13.228331810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RODOVI'#193'RIO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          Left = 306.141930000000000000
          Top = 41.574803150000000000
          Width = 457.322834650000000000
          Height = 30.236220470000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          Left = 306.141930000000000000
          Top = 41.574830000000000000
          Width = 69.921259840000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'MODELO')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 376.062955510000000000
          Top = 41.574830000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#201'RIE')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 402.519648430000000000
          Top = 41.574830000000000000
          Width = 86.929133860000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 489.448782280000000000
          Top = 41.574830000000000000
          Width = 35.905511810000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'FL')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 525.354670000000000000
          Top = 41.574830000000000000
          Width = 117.165354330000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DATA E HORA DE EMISS'#195'O')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Left = 642.520100000000000000
          Top = 41.574830000000000000
          Width = 120.944879450000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'INSC. SUFRAMA DESTINAT'#193'RIO')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 306.141930000000000000
          Top = 51.023622050000000000
          Width = 69.921259840000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_mod"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo206: TfrxMemoView
          Left = 376.062955510000000000
          Top = 51.023622050000000000
          Width = 26.456692910000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_serie"]')
          ParentFont = False
        end
        object Memo207: TfrxMemoView
          Left = 402.519648430000000000
          Top = 51.023622050000000000
          Width = 86.929133860000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nCT">)]')
          ParentFont = False
        end
        object Memo208: TfrxMemoView
          Left = 489.448782280000000000
          Top = 51.023622050000000000
          Width = 35.905511810000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
        end
        object Memo209: TfrxMemoView
          Left = 525.354670000000000000
          Top = 51.023622050000000000
          Width = 117.165354330000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"] [frxDsA."ide_hEmi"]')
          ParentFont = False
        end
        object Memo225: TfrxMemoView
          Left = 642.520100000000000000
          Top = 51.023622050000000000
          Width = 120.944879450000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_ISUF"]')
          ParentFont = False
        end
        object Memo193: TfrxMemoView
          Left = 30.236240000000000000
          Top = 86.929190000000000000
          Width = 137.952755910000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TIPO DO CT-E')
          ParentFont = False
        end
        object Memo194: TfrxMemoView
          Left = 168.188805510000000000
          Top = 86.929190000000000000
          Width = 137.952755910000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TIPO DO SERVI'#199'O')
          ParentFont = False
        end
        object Memo195: TfrxMemoView
          Left = 30.236240000000000000
          Top = 96.377952760000000000
          Width = 137.952755910000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Left = 168.188805510000000000
          Top = 96.377952760000000000
          Width = 137.952755910000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo197: TfrxMemoView
          Left = 30.236240000000000000
          Top = 117.165430000000000000
          Width = 137.952755910000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOMADOR DO SERVI'#199'O')
          ParentFont = False
        end
        object Memo198: TfrxMemoView
          Left = 168.188805510000000000
          Top = 117.165430000000000000
          Width = 137.952755910000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'FORMA DE PAGAMENTO')
          ParentFont = False
        end
        object Memo199: TfrxMemoView
          Left = 30.236240000000000000
          Top = 126.614192760000000000
          Width = 137.952755910000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo226: TfrxMemoView
          Left = 168.188805510000000000
          Top = 126.614192760000000000
          Width = 137.952755910000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 323.149501340000000000
          Top = 136.165430000000000000
          Width = 277.000000000000000000
          Height = 30.236220470000000000
          BarType = bcCode128C
          DataField = 'Id'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object BarCode2: TfrxBarCodeView
          Left = 323.149821100000000000
          Top = 75.590600000000000000
          Width = 79.000000000000000000
          Height = 30.236215590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
      end
      object Memo86: TfrxMemoView
        Left = 30.236240000000000000
        Top = 914.646260000000000000
        Width = 733.228346460000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'C'#193'LCULO DO ISSQN')
        ParentFont = False
        VAlign = vaCenter
      end
      object Line6: TfrxLineView
        Left = 30.236220470000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 68.031496060000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 279.685010080000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line14: TfrxLineView
        Left = 317.480285670000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 336.377923460000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 359.055088820000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 396.850364410000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 445.984540000000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 491.338553390000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 536.692884090000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 582.047214800000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 627.401545510000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 672.755876220000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 695.433041570000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 718.110206930000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Memo12: TfrxMemoView
        Tag = 2
        Left = -30.236240000000000000
        Top = 351.496290000000000000
        Width = 793.701300000000000000
        Height = 559.370440000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -117
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'SEM VALOR')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
      object Memo146: TfrxMemoView
        Left = 81.259901100000000000
        Top = 351.496263150000000000
        Width = 559.370244720000000000
        Height = 21.543307090000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'frxDsA."DOC_SEM_VLR_JUR"'
          'frxDsA."DOC_SEM_VLR_FIS"')
        ParentFont = False
        Formats = <
          item
          end
          item
          end>
      end
      object Line11: TfrxLineView
        Left = 763.465060000000000000
        Top = 695.433520000000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Memo200: TfrxMemoView
        Left = 278.684924650000000000
        Top = 407.433258820000000000
        Width = 302.362204720000000000
        Height = 51.023634250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'frxDsA."DOC_SEM_VLR_JUR"')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 571.086853390000000000
        Width = 229.039350550000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O ESTADUAL')
        ParentFont = False
      end
      object Memo23: TfrxMemoView
        Tag = 1
        Left = 259.275590550000000000
        Top = 571.086853390000000000
        Width = 243.401574800000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
        ParentFont = False
      end
      object Memo24: TfrxMemoView
        Tag = 1
        Left = 502.677165350000000000
        Top = 571.086853390000000000
        Width = 260.787401570000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'CNPJ')
        ParentFont = False
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 242.000000000000000000
      PaperSize = 256
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 260.788314490000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 366.614173230000000000
          Height = 94.488188980000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 366.614173230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'REMETENTE :[frxDsA."REMET_NOME"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Tag = 1
          Left = 30.236220470000000000
          Top = 200.827101180000000000
          Width = 124.724409450000000000
          Height = 12.094485750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FATURA / DUPLICATA')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 213.543914880000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Tag = 1
          Left = 79.370149530000000000
          Top = 213.543914880000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Top = 213.543914880000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          Left = 177.637929530000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Tag = 1
          Left = 226.771839060000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Tag = 1
          Left = 275.905729060000000000
          Top = 213.544195590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Tag = 1
          Left = 325.039599530000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Tag = 1
          Left = 374.173509060000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Tag = 1
          Left = 423.307399060000000000
          Top = 213.544195590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Tag = 1
          Left = 472.441269530000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Tag = 1
          Left = 521.575179060000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Tag = 1
          Left = 570.709069060000000000
          Top = 213.544195590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Tag = 1
          Left = 619.842939530000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Tag = 1
          Left = 668.976849060000000000
          Top = 213.544195590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Tag = 1
          Left = 718.110739060000000000
          Top = 213.544195590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 366.614173230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'REMETENTE :[frxDsA."REMET_ENDERECO"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 37.795300000000000000
          Width = 230.551093230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO: [frxDsA."REMET_XMUN_TXT"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 56.692950000000000000
          Width = 230.551093230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."REMET_CNPJ_CPF_TXT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 75.590600000000000000
          Width = 230.551093230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PA'#205'S: [frxDsA."REMET_XPAIS_TXT"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 260.787570000000000000
          Top = 37.795300000000000000
          Width = 136.062843230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP: [frxDsA."REMET_CEP_TXT"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 260.787570000000000000
          Top = 56.692950000000000000
          Width = 136.062843230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Inscri'#231#227'o Estadual: [frxDsA."REMET_IE_TXT"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 260.787570000000000000
          Top = 75.590600000000000000
          Width = 136.062843230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 396.850650000000000000
          Width = 366.614173230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'REMETENTE :[frxDsA."REMET_NOME"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 98.267780000000000000
          Width = 366.614173230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'REMETENTE :[frxDsA."REMET_NOME"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 117.165430000000000000
          Width = 366.614173230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'REMETENTE :[frxDsA."REMET_ENDERECO"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 136.063080000000000000
          Width = 230.551093230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO: [frxDsA."REMET_XMUN_TXT"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 154.960730000000000000
          Width = 230.551093230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."REMET_CNPJ_CPF_TXT"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 173.858380000000000000
          Width = 230.551093230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PA'#205'S: [frxDsA."REMET_XPAIS_TXT"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 260.787570000000000000
          Top = 136.063080000000000000
          Width = 136.062843230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP: [frxDsA."REMET_CEP_TXT"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 260.787570000000000000
          Top = 154.960730000000000000
          Width = 136.062843230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Inscri'#231#227'o Estadual: [frxDsA."REMET_IE_TXT"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 260.787570000000000000
          Top = 173.858380000000000000
          Width = 136.062843230000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 208.251968510000000000
        Top = 672.756340000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData2OnAfterPrint'
        RowCount = 1
        object Memo46: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 15.874015750000000000
          Width = 128.503937007874000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DOS PRODUTOS')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 15.874015750000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 15.874015750000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 15.874015750000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS ST')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 15.874015750000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS SUBSTITUI'#199#195'O')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DO IPI')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 48.000000000000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO FRETE')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO SEGURO')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS DESPESAS ACESS'#211'RIAS')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 48.000000000000000000
          Width = 128.503937010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DA NOTA')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 143.622047244094000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#193'LCULO DO IMPOSTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 15.874015750000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR APROXIMADO TRIBUTOS')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 638.740570000000000000
        Width = 793.701300000000000000
        RowCount = 0
        object Memo149: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'frxDsCTeYIts."nDup1"')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Tag = 1
          Left = 79.370149530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'frxDsCTeYIts."xVenc1"')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup1">)')
          ParentFont = False
        end
      end
      object DD_prod: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 32.125984250000000000
        Top = 944.882500000000000000
        Width = 793.701300000000000000
        RowCount = 0
        object Me_prod_cProd: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'frxDsI."prod_cProd"')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Tag = 1
          Left = 30.236240000000000000
          Top = 32.125984250000000000
          Width = 733.228820000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object Header1: TfrxHeader
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 903.307670000000000000
        Width = 793.701300000000000000
        object Memo131: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 211.653543310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Tag = 1
          Left = 279.685046690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Tag = 1
          Left = 317.480327170000000000
          Width = 18.897635350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Top = 8.692476459999970000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Top = 8.692476459999970000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VAL APROX'
            'TRIBUTOS')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 313.700990000000000000
        Visible = False
        Width = 793.701300000000000000
        OnAfterPrint = 'Child1OnAfterPrint'
        object Memo171: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 257.007903310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Tag = 1
          Left = 325.039406690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Tag = 1
          Left = 362.834687170000000000
          Width = 18.897635350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476460000020000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476460000020000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object PageHeader2: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Line9: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line10: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
    object Page3: TfrxReportPage
      Visible = False
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        RowCount = 0
        object MeFlowToSee: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 733.228820000000000000
          Height = 7.559060000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 7.559059999999990000
          Width = 733.228531970000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INFORMA'#199#213'ES COMPLEMENTARES (COMPLEMENTO DA PRIMEIRA FOLHA)')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Line4: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line5: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
  end
  object QrI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsi'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 56
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrIprod_cProd: TWideStringField
      FieldName = 'prod_cProd'
      Size = 60
    end
    object QrIprod_cEAN: TWideStringField
      FieldName = 'prod_cEAN'
      Size = 14
    end
    object QrIprod_xProd: TWideStringField
      FieldName = 'prod_xProd'
      Size = 120
    end
    object QrIprod_NCM: TWideStringField
      FieldName = 'prod_NCM'
      Size = 8
    end
    object QrIprod_EXTIPI: TWideStringField
      FieldName = 'prod_EXTIPI'
      Size = 3
    end
    object QrIprod_genero: TSmallintField
      FieldName = 'prod_genero'
    end
    object QrIprod_CFOP: TIntegerField
      FieldName = 'prod_CFOP'
    end
    object QrIprod_uCom: TWideStringField
      FieldName = 'prod_uCom'
      Size = 6
    end
    object QrIprod_qCom: TFloatField
      FieldName = 'prod_qCom'
    end
    object QrIprod_vUnCom: TFloatField
      FieldName = 'prod_vUnCom'
    end
    object QrIprod_vProd: TFloatField
      FieldName = 'prod_vProd'
    end
    object QrIprod_cEANTrib: TWideStringField
      FieldName = 'prod_cEANTrib'
      Size = 14
    end
    object QrIprod_uTrib: TWideStringField
      FieldName = 'prod_uTrib'
      Size = 6
    end
    object QrIprod_qTrib: TFloatField
      FieldName = 'prod_qTrib'
    end
    object QrIprod_vUnTrib: TFloatField
      FieldName = 'prod_vUnTrib'
    end
    object QrIprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
    end
    object QrIprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
    end
    object QrIprod_vDesc: TFloatField
      FieldName = 'prod_vDesc'
    end
    object QrITem_IPI: TSmallintField
      FieldName = 'Tem_IPI'
    end
    object QrInItem: TIntegerField
      FieldName = 'nItem'
    end
  end
  object QrN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsn'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrNICMS_Orig: TSmallintField
      FieldName = 'ICMS_Orig'
    end
    object QrNICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
      DisplayFormat = '000.###'
    end
    object QrNICMS_modBC: TSmallintField
      FieldName = 'ICMS_modBC'
    end
    object QrNICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrNICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrNICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrNICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrNICMS_modBCST: TSmallintField
      FieldName = 'ICMS_modBCST'
    end
    object QrNICMS_pMVAST: TFloatField
      FieldName = 'ICMS_pMVAST'
    end
    object QrNICMS_pRedBCST: TFloatField
      FieldName = 'ICMS_pRedBCST'
    end
    object QrNICMS_vBCST: TFloatField
      FieldName = 'ICMS_vBCST'
    end
    object QrNICMS_pICMSST: TFloatField
      FieldName = 'ICMS_pICMSST'
    end
    object QrNICMS_vICMSST: TFloatField
      FieldName = 'ICMS_vICMSST'
    end
    object QrNICMS_CSOSN: TIntegerField
      FieldName = 'ICMS_CSOSN'
    end
  end
  object QrO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitso'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrOnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrOIPI_clEnq: TWideStringField
      FieldName = 'IPI_clEnq'
      Size = 5
    end
    object QrOIPI_CNPJProd: TWideStringField
      FieldName = 'IPI_CNPJProd'
      Size = 14
    end
    object QrOIPI_cSelo: TWideStringField
      FieldName = 'IPI_cSelo'
      Size = 60
    end
    object QrOIPI_qSelo: TFloatField
      FieldName = 'IPI_qSelo'
    end
    object QrOIPI_cEnq: TWideStringField
      FieldName = 'IPI_cEnq'
      Size = 3
    end
    object QrOIPI_CST: TSmallintField
      FieldName = 'IPI_CST'
    end
    object QrOIPI_vBC: TFloatField
      FieldName = 'IPI_vBC'
    end
    object QrOIPI_qUnid: TFloatField
      FieldName = 'IPI_qUnid'
    end
    object QrOIPI_vUnid: TFloatField
      FieldName = 'IPI_vUnid'
    end
    object QrOIPI_pIPI: TFloatField
      FieldName = 'IPI_pIPI'
    end
    object QrOIPI_vIPI: TFloatField
      FieldName = 'IPI_vIPI'
    end
  end
  object QrY: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nDup, dVenc, vDup'
      'FROM nfecaby'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 56
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrYnDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrYdVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrYvDup: TFloatField
      FieldName = 'vDup'
    end
  end
  object QrCTeYIts: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeyits'
      'ORDER BY dVenc1')
    Left = 56
    Top = 340
    object QrCTeYItsSeq1: TIntegerField
      FieldName = 'Seq1'
    end
    object QrCTeYItsnDup1: TWideStringField
      FieldName = 'nDup1'
      Size = 60
    end
    object QrCTeYItsdVenc1: TDateField
      FieldName = 'dVenc1'
    end
    object QrCTeYItsvDup1: TFloatField
      FieldName = 'vDup1'
    end
    object QrCTeYItsSeq2: TIntegerField
      FieldName = 'Seq2'
    end
    object QrCTeYItsnDup2: TWideStringField
      FieldName = 'nDup2'
      Size = 60
    end
    object QrCTeYItsdVenc2: TDateField
      FieldName = 'dVenc2'
    end
    object QrCTeYItsvDup2: TFloatField
      FieldName = 'vDup2'
    end
    object QrCTeYItsSeq3: TIntegerField
      FieldName = 'Seq3'
    end
    object QrCTeYItsnDup3: TWideStringField
      FieldName = 'nDup3'
      Size = 60
    end
    object QrCTeYItsdVenc3: TDateField
      FieldName = 'dVenc3'
    end
    object QrCTeYItsvDup3: TFloatField
      FieldName = 'vDup3'
    end
    object QrCTeYItsSeq4: TIntegerField
      FieldName = 'Seq4'
    end
    object QrCTeYItsnDup4: TWideStringField
      FieldName = 'nDup4'
      Size = 60
    end
    object QrCTeYItsdVenc4: TDateField
      FieldName = 'dVenc4'
    end
    object QrCTeYItsvDup4: TFloatField
      FieldName = 'vDup4'
    end
    object QrCTeYItsSeq5: TIntegerField
      FieldName = 'Seq5'
    end
    object QrCTeYItsnDup5: TWideStringField
      FieldName = 'nDup5'
      Size = 60
    end
    object QrCTeYItsdVenc5: TDateField
      FieldName = 'dVenc5'
    end
    object QrCTeYItsvDup5: TFloatField
      FieldName = 'vDup5'
    end
    object QrCTeYItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrCTeYItsxVenc1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc1'
      Size = 10
      Calculated = True
    end
    object QrCTeYItsxVenc2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc2'
      Size = 10
      Calculated = True
    end
    object QrCTeYItsxVenc3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc3'
      Size = 10
      Calculated = True
    end
    object QrCTeYItsxVenc4: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc4'
      Size = 10
      Calculated = True
    end
    object QrCTeYItsxVenc5: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xVenc5'
      Size = 10
      Calculated = True
    end
  end
  object QrXVol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecabxvol'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 56
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrXVolqVol: TFloatField
      FieldName = 'qVol'
    end
    object QrXVolesp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrXVolmarca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrXVolnVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrXVolpesoL: TFloatField
      FieldName = 'pesoL'
    end
    object QrXVolpesoB: TFloatField
      FieldName = 'pesoB'
    end
  end
  object QrV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsv'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrVnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrVInfAdProd: TWideMemoField
      FieldName = 'InfAdProd'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMnItem: TIntegerField
      FieldName = 'nItem'
    end
    object QrMiTotTrib: TSmallintField
      FieldName = 'iTotTrib'
    end
    object QrMvTotTrib: TFloatField
      FieldName = 'vTotTrib'
    end
  end
  object mySQLQuery1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfeitsm'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND nItem=:P3')
    Left = 56
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'FatID'
    end
    object IntegerField2: TIntegerField
      FieldName = 'FatNum'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Empresa'
    end
    object IntegerField4: TIntegerField
      FieldName = 'nItem'
    end
    object SmallintField1: TSmallintField
      FieldName = 'iTotTrib'
    end
    object FloatField1: TFloatField
      FieldName = 'vTotTrib'
    end
  end
  object frxDsI: TfrxDBDataset
    UserName = 'frxDsI'
    CloseDataSource = False
    DataSet = QrI
    BCDToCurrency = False
    Left = 84
    Top = 228
  end
  object frxDsN: TfrxDBDataset
    UserName = 'frxDsN'
    CloseDataSource = False
    FieldAliases.Strings = (
      'nItem=nItem'
      'ICMS_Orig=ICMS_Orig'
      'ICMS_CST=ICMS_CST'
      'ICMS_modBC=ICMS_modBC'
      'ICMS_pRedBC=ICMS_pRedBC'
      'ICMS_vBC=ICMS_vBC'
      'ICMS_pICMS=ICMS_pICMS'
      'ICMS_vICMS=ICMS_vICMS'
      'ICMS_modBCST=ICMS_modBCST'
      'ICMS_pMVAST=ICMS_pMVAST'
      'ICMS_pRedBCST=ICMS_pRedBCST'
      'ICMS_vBCST=ICMS_vBCST'
      'ICMS_pICMSST=ICMS_pICMSST'
      'ICMS_vICMSST=ICMS_vICMSST'
      'ICMS_CSOSN=ICMS_CSOSN')
    DataSet = QrN
    BCDToCurrency = False
    Left = 84
    Top = 256
  end
  object frxDsO: TfrxDBDataset
    UserName = 'frxDsO'
    CloseDataSource = False
    DataSet = QrO
    BCDToCurrency = False
    Left = 84
    Top = 284
  end
  object frxDsY: TfrxDBDataset
    UserName = 'frxDsY'
    CloseDataSource = False
    DataSet = QrY
    BCDToCurrency = False
    Left = 84
    Top = 312
  end
  object frxDsCTeYIts: TfrxDBDataset
    UserName = 'frxDsCTeYIts'
    CloseDataSource = False
    DataSet = QrCTeYIts
    BCDToCurrency = False
    Left = 84
    Top = 340
  end
  object frxDsXvol: TfrxDBDataset
    UserName = 'frxDsXvol'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 84
    Top = 368
  end
  object frxDsV: TfrxDBDataset
    UserName = 'frxDsV'
    CloseDataSource = False
    DataSet = QrV
    BCDToCurrency = False
    Left = 84
    Top = 396
  end
  object frxDsM: TfrxDBDataset
    UserName = 'frxDsM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'nItem=nItem'
      'iTotTrib=iTotTrib'
      'vTotTrib=vTotTrib')
    DataSet = QrM
    BCDToCurrency = False
    Left = 84
    Top = 424
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDsM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'nItem=nItem'
      'iTotTrib=iTotTrib'
      'vTotTrib=vTotTrib')
    DataSet = mySQLQuery1
    BCDToCurrency = False
    Left = 84
    Top = 452
  end
end
