object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxReport1: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42360.538390300930000000
    ReportOptions.LastChange = 42360.538390300930000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 244
    Top = 144
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Memo17: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Width = 366.614173230000000000
        Height = 94.488188980000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        ParentFont = False
        VAlign = vaCenter
      end
      object Me_E04_Titu: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Width = 366.614173230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DESTENTE :')
        ParentFont = False
      end
      object Memo27: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 18.897650000000000000
        Width = 366.614173230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'ENDERE'#199'O :')
        ParentFont = False
      end
      object Memo28: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 37.795300000000000000
        Width = 230.551093230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'MUNIC'#205'PIO: ')
        ParentFont = False
      end
      object Memo29: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 56.692950000000000000
        Width = 230.551093230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'CNPJ / CPF:')
        ParentFont = False
      end
      object Memo30: TfrxMemoView
        Tag = 1
        Left = 41.574830000000000000
        Top = 75.590600000000000000
        Width = 192.755793230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'PA'#205'S: ')
        ParentFont = False
      end
      object Memo35: TfrxMemoView
        Tag = 1
        Left = 234.330860000000000000
        Top = 37.795300000000000000
        Width = 136.062843230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'CEP: ')
        ParentFont = False
      end
      object Memo36: TfrxMemoView
        Tag = 1
        Left = 234.330860000000000000
        Top = 56.692950000000000000
        Width = 136.062843230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCR. EST: ')
        ParentFont = False
      end
      object Memo37: TfrxMemoView
        Tag = 1
        Left = 234.330860000000000000
        Top = 75.590600000000000000
        Width = 136.062843230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'FONE:')
        ParentFont = False
      end
      object Memo14: TfrxMemoView
        Left = 52.913420000000000000
        Width = 313.700990000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_NOME"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 52.913420000000000000
        Top = 18.897650000000000000
        Width = 313.700990000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_ENDERECO"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 52.913420000000000000
        Top = 37.795300000000000000
        Width = 177.637910000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_XMUN_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        Left = 52.913420000000000000
        Top = 56.692950000000000000
        Width = 177.637910000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_CNPJ_CPF_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 75.590600000000000000
        Width = 37.795063230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'UF:')
        ParentFont = False
      end
      object Memo47: TfrxMemoView
        Left = 15.118120000000000000
        Top = 75.590600000000000000
        Width = 26.456710000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."DEST_UF"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo53: TfrxMemoView
        Left = 64.252010000000000000
        Top = 75.590600000000000000
        Width = 166.299320000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_XPAIS_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo55: TfrxMemoView
        Left = 275.905690000000000000
        Top = 37.795300000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_CEP_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo57: TfrxMemoView
        Left = 275.905690000000000000
        Top = 56.692950000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_IE_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo59: TfrxMemoView
        Left = 275.905690000000000000
        Top = 75.590600000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_FONE_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo1: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 98.267780000000000000
        Width = 366.614173230000000000
        Height = 94.488188980000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 98.267780000000000000
        Width = 366.614173230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RECEBENTE :')
        ParentFont = False
      end
      object Memo3: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 117.165430000000000000
        Width = 366.614173230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'ENDERE'#199'O :')
        ParentFont = False
      end
      object Memo4: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 136.063080000000000000
        Width = 230.551093230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'MUNIC'#205'PIO: ')
        ParentFont = False
      end
      object Memo5: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 154.960730000000000000
        Width = 230.551093230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'CNPJ / CPF:')
        ParentFont = False
      end
      object Memo6: TfrxMemoView
        Tag = 1
        Left = 41.574830000000000000
        Top = 173.858380000000000000
        Width = 192.755793230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'PA'#205'S: ')
        ParentFont = False
      end
      object Memo7: TfrxMemoView
        Tag = 1
        Left = 234.330860000000000000
        Top = 136.063080000000000000
        Width = 136.062843230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'CEP: ')
        ParentFont = False
      end
      object Memo8: TfrxMemoView
        Tag = 1
        Left = 234.330860000000000000
        Top = 154.960730000000000000
        Width = 136.062843230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCR. EST: ')
        ParentFont = False
      end
      object Memo9: TfrxMemoView
        Tag = 1
        Left = 234.330860000000000000
        Top = 173.858380000000000000
        Width = 136.062843230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'FONE:')
        ParentFont = False
      end
      object Memo10: TfrxMemoView
        Left = 52.913420000000000000
        Top = 98.267780000000000000
        Width = 313.700990000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_NOME"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        Left = 52.913420000000000000
        Top = 117.165430000000000000
        Width = 313.700990000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_ENDERECO"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        Left = 52.913420000000000000
        Top = 136.063080000000000000
        Width = 177.637910000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_XMUN_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 52.913420000000000000
        Top = 154.960730000000000000
        Width = 177.637910000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_CNPJ_CPF_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        Tag = 1
        Left = 3.779530000000000000
        Top = 173.858380000000000000
        Width = 37.795063230000000000
        Height = 18.897637800000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'UF:')
        ParentFont = False
      end
      object Memo16: TfrxMemoView
        Left = 15.118120000000000000
        Top = 173.858380000000000000
        Width = 26.456710000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."RECEB_UF"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 64.252010000000000000
        Top = 173.858380000000000000
        Width = 166.299320000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_XPAIS_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        Left = 275.905690000000000000
        Top = 136.063080000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_CEP_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        Left = 275.905690000000000000
        Top = 154.960730000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."RECEB_IE_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        Left = 275.905690000000000000
        Top = 173.858380000000000000
        Width = 90.708720000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsA."DEST_FONE_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
    end
  end
end
