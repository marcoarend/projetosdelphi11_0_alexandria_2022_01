unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule,
  mySQLDbTables, UnGOTOy, Winsock, extctrls, MySQLBatch, comctrls, stdctrls,
  frxClass, frxDBSet, dmkGeral, TypInfo, UnDmkProcFunc;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterENumero: TIntegerField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrMasterECEP: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrMasterLogo2: TBlobField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrMasterLimite: TSmallintField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrUserSets: TmySQLQuery;
    QrUserSetsDefinicao: TWideStringField;
    QrUserSetsAbilitado: TSmallintField;
    QrUserSetsVisivel: TSmallintField;
    QrUserSetsNomeForm: TWideStringField;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrMasterSolicitaSenha: TSmallintField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLNumero: TIntegerField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrMaster2SolicitaSenha: TSmallintField;
    QrMaster2Limite: TSmallintField;
    QrMaster2SitSenha: TSmallintField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrUpdZ: TmySQLQuery;
    frxDsMaster: TfrxDBDataset;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasFuncionario: TIntegerField;
    QrSenhasIP_Default: TWideStringField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrMasterUsaAccMngr: TSmallintField;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControleContasU: TIntegerField;
    QrControleEntDefAtr: TIntegerField;
    QrControleEntAtrCad: TIntegerField;
    QrControleEntAtrIts: TIntegerField;
    QrControleBalTopoNom: TIntegerField;
    QrControleBalTopoTit: TIntegerField;
    QrControleBalTopoPer: TIntegerField;
    QrControleNCMs: TIntegerField;
    QrControleParamsNFs: TIntegerField;
    QrControleCambioCot: TIntegerField;
    QrControleCambioMda: TIntegerField;
    QrControleMoedaBr: TIntegerField;
    QrControleSecuritStr: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleEquiCom: TIntegerField;
    QrControleContasLnk: TIntegerField;
    QrControleLastBco: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleEquiGru: TIntegerField;
    QrControleCNAB_Rem: TIntegerField;
    QrControleCNAB_Rem_I: TIntegerField;
    QrControlePreEmMsgIm: TIntegerField;
    QrControlePreEmMsg: TIntegerField;
    QrControlePreEmail: TIntegerField;
    QrControleContasMes: TIntegerField;
    QrControleMultiPgto: TIntegerField;
    QrControleImpObs: TIntegerField;
    QrControleProLaINSSr: TFloatField;
    QrControleProLaINSSp: TFloatField;
    QrControleVerSalTabs: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNAB_CaD: TIntegerField;
    QrControleCNAB_CaG: TIntegerField;
    QrControleAtzCritic: TIntegerField;
    QrControleContasTrf: TIntegerField;
    QrControleSomaIts: TIntegerField;
    QrControleContasAgr: TIntegerField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleLogoNF: TWideStringField;
    QrControleConfJanela: TIntegerField;
    QrControleDataPesqAuto: TSmallintField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleAtividad: TSmallintField;
    QrControleCidades: TSmallintField;
    QrControleChequez: TSmallintField;
    QrControlePaises: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleCambiosData: TDateField;
    QrControleCambiosUsuario: TIntegerField;
    QrControlereg10: TSmallintField;
    QrControlereg11: TSmallintField;
    QrControlereg50: TSmallintField;
    QrControlereg51: TSmallintField;
    QrControlereg53: TSmallintField;
    QrControlereg54: TSmallintField;
    QrControlereg56: TSmallintField;
    QrControlereg60: TSmallintField;
    QrControlereg75: TSmallintField;
    QrControlereg88: TSmallintField;
    QrControlereg90: TSmallintField;
    QrControleNumSerieNF: TSmallintField;
    QrControleSerieNF: TIntegerField;
    QrControleModeloNF: TSmallintField;
    QrControleMyPagTip: TSmallintField;
    QrControleMyPagCar: TSmallintField;
    QrControleControlaNeg: TSmallintField;
    QrControleFamilias: TSmallintField;
    QrControleFamiliasIts: TSmallintField;
    QrControleAskNFOrca: TSmallintField;
    QrControlePreviewNF: TSmallintField;
    QrControleOrcaRapido: TSmallintField;
    QrControleDistriDescoItens: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleMensalSempre: TSmallintField;
    QrControleBalType: TSmallintField;
    QrControleOrcaOrdem: TSmallintField;
    QrControleOrcaLinhas: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleOrcaModelo: TSmallintField;
    QrControleOrcaRodaPos: TSmallintField;
    QrControleOrcaRodape: TSmallintField;
    QrControleOrcaCabecalho: TSmallintField;
    QrControleCoresRel: TSmallintField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleCFiscalPadr: TWideStringField;
    QrControleSitTribPadr: TWideStringField;
    QrControleCFOPPadr: TWideStringField;
    QrControleAvisosCxaEdit: TSmallintField;
    QrControleTravaCidade: TSmallintField;
    QrControleChConfCab: TIntegerField;
    QrControleImpDOS: TIntegerField;
    QrControleUnidadePadrao: TIntegerField;
    QrControleProdutosV: TIntegerField;
    QrControleCartDespesas: TIntegerField;
    QrControleReserva: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleVersao: TIntegerField;
    QrControleVerWeb: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrControleDono: TIntegerField;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleErroHora: TIntegerField;
    QrControleSenhas: TIntegerField;
    QrControleSenhasIts: TIntegerField;
    QrControleSalarios: TIntegerField;
    QrControleEntidades: TIntegerField;
    QrControleEntiCtas: TIntegerField;
    QrControleUFs: TIntegerField;
    QrControleListaECivil: TIntegerField;
    QrControlePerfis: TIntegerField;
    QrControleUsuario: TIntegerField;
    QrControleContas: TIntegerField;
    QrControleCentroCusto: TIntegerField;
    QrControleDepartamentos: TIntegerField;
    QrControleDividas: TIntegerField;
    QrControleDividasIts: TIntegerField;
    QrControleDividasPgs: TIntegerField;
    QrControleCarteiras: TIntegerField;
    QrControleCarteirasU: TIntegerField;
    QrControleCartaG: TIntegerField;
    QrControleCartas: TIntegerField;
    QrControleConsignacao: TIntegerField;
    QrControleGrupo: TIntegerField;
    QrControleSubGrupo: TIntegerField;
    QrControleConjunto: TIntegerField;
    QrControlePlano: TIntegerField;
    QrControleInflacao: TIntegerField;
    QrControlekm: TIntegerField;
    QrControlekmMedia: TIntegerField;
    QrControlekmIts: TIntegerField;
    QrControleFatura: TIntegerField;
    QrControleLanctos: TLargeintField;
    QrControleLctoEndoss: TIntegerField;
    QrControleEntiGrupos: TIntegerField;
    QrControleEntiContat: TIntegerField;
    QrControleEntiCargos: TIntegerField;
    QrControleEntiMail: TIntegerField;
    QrControleEntiTel: TIntegerField;
    QrControleEntiTipCto: TIntegerField;
    QrControleAparencias: TIntegerField;
    QrControlePages: TIntegerField;
    QrControleMultiEtq: TIntegerField;
    QrControleEntiTransp: TIntegerField;
    QrControleEntiRespon: TIntegerField;
    QrControleEntiCfgRel: TIntegerField;
    QrControleExcelGru: TIntegerField;
    QrControleExcelGruImp: TIntegerField;
    QrControleMediaCH: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrControleImprime: TIntegerField;
    QrControleImprimeBand: TIntegerField;
    QrControleImprimeView: TIntegerField;
    QrControleComProdPerc: TIntegerField;
    QrControleComProdEdit: TIntegerField;
    QrControleComServPerc: TIntegerField;
    QrControleComServEdit: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControlePadrPlacaCar: TWideStringField;
    QrControleServSMTP: TWideStringField;
    QrControleNomeMailOC: TWideStringField;
    QrControleDonoMailOC: TWideStringField;
    QrControleMailOC: TWideStringField;
    QrControleCorpoMailOC: TWideMemoField;
    QrControleConexaoDialUp: TWideStringField;
    QrControleMailCCCega: TWideStringField;
    QrControleContVen: TIntegerField;
    QrControleContCom: TIntegerField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrControleMoedaVal: TFloatField;
    QrControleTela1: TIntegerField;
    QrControleChamarPgtoServ: TIntegerField;
    QrControleFormUsaTam: TIntegerField;
    QrControleFormHeight: TIntegerField;
    QrControleFormWidth: TIntegerField;
    QrControleFormPixEsq: TIntegerField;
    QrControleFormPixDir: TIntegerField;
    QrControleFormPixTop: TIntegerField;
    QrControleFormPixBot: TIntegerField;
    QrControleFormFoAlt: TIntegerField;
    QrControleFormFoPro: TFloatField;
    QrControleFormUsaPro: TIntegerField;
    QrControleFormSlides: TIntegerField;
    QrControleFormNeg: TIntegerField;
    QrControleFormIta: TIntegerField;
    QrControleFormSub: TIntegerField;
    QrControleFormExt: TIntegerField;
    QrControleFormFundoTipo: TIntegerField;
    QrControleFormFundoBMP: TWideStringField;
    QrControleServInterv: TIntegerField;
    QrControleServAntecip: TIntegerField;
    QrControleAdiLancto: TIntegerField;
    QrControleContaSal: TIntegerField;
    QrControleContaVal: TIntegerField;
    QrControlePronomeE: TWideStringField;
    QrControlePronomeM: TWideStringField;
    QrControlePronomeF: TWideStringField;
    QrControlePronomeA: TWideStringField;
    QrControleSaudacaoE: TWideStringField;
    QrControleSaudacaoM: TWideStringField;
    QrControleSaudacaoF: TWideStringField;
    QrControleSaudacaoA: TWideStringField;
    QrControleNiver: TSmallintField;
    QrControleNiverddA: TSmallintField;
    QrControleNiverddD: TSmallintField;
    QrControleLastPassD: TDateTimeField;
    QrControleMultiPass: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrControleCustomFR3: TIntegerField;
    QrControleanotacoes: TIntegerField;
    QrControleContasLnkIts: TIntegerField;
    QrControleEventosCad: TSmallintField;
    QrControleImprimeEmpr: TIntegerField;
    QrControleCartTalCH: TIntegerField;
    QrControleXcelCfgIts: TIntegerField;
    QrControleXcelCfgCab: TIntegerField;
    QrControlePlanRelCab: TIntegerField;
    QrControlePlanRelIts: TIntegerField;
    QrControleBLQ_TopoAvisoV: TIntegerField;
    QrControleBLQ_MEsqAvisoV: TIntegerField;
    QrControleBLQ_AltuAvisoV: TIntegerField;
    QrControleBLQ_LargAvisoV: TIntegerField;
    QrControleBLQ_TopoDestin: TIntegerField;
    QrControleBLQ_MEsqDestin: TIntegerField;
    QrControleBLQ_AltuDestin: TIntegerField;
    QrControleBLQ_LargDestin: TIntegerField;
    QrUpd2: TmySQLQuery;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QrDono_CalcFields(DataSet: TDataSet);
    procedure QrEndereco_CalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FProdDelete: Integer;

    function  TabelasQueNaoQueroCriar(): String;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure VerificaSenha(Index: Integer; Login, Senha: String);

    function ConcatenaRegistros(Matricula: Integer): String;
    function Privilegios(Usuario : Integer) : Boolean;
    procedure DefineUserSets_Painel(NomeForm, Definicao: String;
              Painel: TPanel);
    //procedure AtualizaContasMensais(PeriodoIni, PeriodoFim, Genero: Integer;
    //          PB1, PB2: TProgressBar);
    //procedure AtualizaContasMensais2(PeriodoIni, PeriodoFim, Genero: Integer;
    //          PB1, PB2, PB3: TProgressBar; ST1, ST2, ST3: TStaticText);
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function CarregaPronunciamento(Compo: TComponent; Unico: Integer = -1): String;
    procedure ReopenControle();
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    procedure ReopenParamsEspecificos(Empresa: Integer);

  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal, SenhaBoss, Servidor, VerifiDB, MyDBCheck, MyListas,
  Logidek_Dmk, UCreate, ModuleGeral, ModuleFin, UnLic_Dmk, DmkDAC_PF,
  ModuleCTe_0000, ModuleMDFe_0000;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

procedure TDmod.LocCod(Atual, Codigo: Integer);
begin
{:::
  FmPrincipal.LocCod(Atual, Codigo);
}
end;

procedure TDmod.Va(Para: TVaiPara);
begin
{:::
  Dmod.DefParams;
  //FmPrincipal.StatusBar.Panels[13].Text :=
    GOTOy.Go(Para, Dmod.QrCarteirasCodigo.Value, '');
    //FmPrincipal.StatusBar.Panels[13].Text[2]);
}
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica: Boolean;
//var
  //KConnected: Boolean;
  //PError: array[0..255] of Char;

begin
  MLAGeral.CriaLog(CO_ARQLOG, 'In�cio cria��o data module.', False);
  if MyDB.Connected then
    Application.MessageBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Application.MessageBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  MLAGeral.CriaLog(CO_ARQLOG, 'Databases verificadas se pr�-conectadas.', False);
  ////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  if not GOTOy.OMySQLEstaInstalado(
  FmLogidek_Dmk.LaAviso1, FmLogidek_Dmk.LaAviso2, FmLogidek_Dmk.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmLogidek_Dmk.LaAviso1, FmLogidek_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmLogidek_Dmk.BtEntra.Visible := False;
    //
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  MLAGeral.CriaLog(CO_ARQLOG, 'Instalacao do mysql verificada.', False);
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  MLAGeral.CriaLog(CO_ARQLOG, 'XXX_SQLx criados.', False);
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Application.MessageBox(PChar('M�quina cliente sem rede.'), 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    MLAGeral.CriaLog(CO_ARQLOG, 'XXX_SQLx N�O criados.', False);
    Application.MessageBox(PChar('Teste'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    MLAGeral.CriaLog(CO_ARQLOG, 'Aplicativo ser� finalizado', False);
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Vari�veis de data e hora definidos.', False);
  //////////////////////////////////////////////////////////////////////////////
{
  MyDB.Host := VAR_IP;
  MyDB.Port := VAR_PORTA;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
}
  //
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql', VAR_IP, VAR_PORTA, 'root',
  VAR_BDSENHA,(*Desconecta*)True, (*Configura*)True, (*Conecta*)False);

  MLAGeral.CriaLog(CO_ARQLOG, 'SHOW DATABASES Iniciado', False);
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  UMyMod.AbreQuery(QrAux, MyDB);
  MLAGeral.CriaLog(CO_ARQLOG, 'SHOW DATABASES OK', False);
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MLAGeral.CriaLog(CO_ARQLOG, 'DB Definido', False);
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    MLAGeral.CriaLog(CO_ARQLOG, 'DB definido como vazio', False);
    Resp := Application.MessageBox(PChar('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'), 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      MLAGeral.CriaLog(CO_ARQLOG, 'DB ser� criado.', False);
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      //
      MLAGeral.CriaLog(CO_ARQLOG, 'DB foi criado.', False);
      //
      if MyDB.Connected then
        MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      MLAGeral.CriaLog(CO_ARQLOG, 'DB n�o criado. Aplicativo ser� finalizado', False);
      Application.MessageBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  MyLocDatabase.Port := VAR_PORTA;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if (MyLocDatabase.DataBaseName = CO_VAZIO) and (TLocDB <> '') then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'),
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Verifica��o vers�o iniciada.', False);
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Application.MessageBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  try
    MLAGeral.CriaLog(CO_ARQLOG, 'Tabela master ser� aberta', False);
    QrMaster.Open;
    MLAGeral.CriaLog(CO_ARQLOG, 'Tabela master foi aberta', False);
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    Verifica := True;
    //raise;
  end;
  //
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  //FmPrincipal.VerificaTerminal;
  MLAGeral.CriaLog(CO_ARQLOG, 'Configura��es iniciais iniciadas', False);
  MyList.ConfiguracoesIniciais(1, MyDB.DatabaseName);
  MLAGeral.CriaLog(CO_ARQLOG, 'Cria��o data module geral.', False);
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados Geral'), 'Erro', MB_OK+MB_ICONERROR);
    raise;
    Application.Terminate;
    Exit;
  end;
  MLAGeral.CriaLog(CO_ARQLOG, 'Final cria��o data module geral.', False);


  try
    Application.CreateForm(TDmodFin, DmodFin);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados Financeiro'), 'Erro', MB_OK+MB_ICONERROR);
    raise;
    Application.Terminate;
    Exit;
  end;
  MLAGeral.CriaLog(CO_ARQLOG, 'Final cria��o data module financeiro.', False);

  //

  if Verifica then
  begin
    MLAGeral.CriaLog(CO_ARQLOG, 'Verifica��o das tabelas iniciada.', False);
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
    QrMaster.Open;
    MLAGeral.CriaLog(CO_ARQLOG, 'Tabela master foi aberta', False);
    VAR_EMPRESANOME := QrMasterEm.Value;
  end;

  MLAGeral.CriaLog(CO_ARQLOG, 'Libera��o iniciada', False);
  //if VAR_SERVIDOR = 2 then GOTOy.LiberaUso;
  if VAR_USUARIO = -1 then
    Geral.MB_Info('Lic_Dmk');
  //Lic_Dmk.LiberaUso5;
end;

procedure TDmod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      QrSenha.Open;
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        QrPerfis.Open;
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value;
  //
  ReopenControle();
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_FORMUSATAM      := QrControleFormUsaTam.Value;
  VAR_FORMHEIGHT      := QrControleFormHeight.Value;
  VAR_FORMWIDTH       := QrControleFormWidth.Value;
  VAR_FORMPIXESQ      := QrControleFormPixEsq.Value;
  VAR_FORMPIXDIR      := QrControleFormPixDir.Value;
  VAR_FORMPIXTOP      := QrControleFormPixTop.Value;
  VAR_FORMPIXBOT      := QrControleFormPixBot.Value;
  VAR_FORMFOALT       := QrControleFormFoAlt.Value;
  VAR_FORMFOPRO       := QrControleFormFoPro.Value;
  VAR_FORMUSAPRO      := QrControleFormUsaPro.Value;
  VAR_FORMSLIDES      := QrControleFormSlides.Value;
  VAR_FORMNEG         := QrControleFormNeg.Value;
  VAR_FORMITA         := QrControleFormIta.Value;
  VAR_FORMSUB         := QrControleFormSub.Value;
  VAR_FORMEXT         := QrControleFormExt.Value;
  VAR_FORMFUNDOTIPO   := QrControleFormFundoTipo.Value;
  VAR_FORMFUNDOBMP    := QrControleFormFundoBMP.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM Carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM Lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM Lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE Carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
end;

procedure TDmod.ReopenControle();
begin
  QrControle.Close;
  QrControle.Open;
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  DmCTe_0000.ReopenOpcoesCTe(DmCTe_0000.QrOpcoesCTe, Empresa, True);
  DmMDFe_0000.ReopenOpcoesMDFe(DmMDFe_0000.QrOpcoesMDFe, Empresa, True);
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  Qr??.Open;
  //
  Registros := MLAGeral.Registros(Qr??);
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;

procedure TDmod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDmod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

function TDmod.TabelasQueNaoQueroCriar(): String;
begin
  //
end;

procedure TDmod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDmod.DataModuleDestroy(Sender: TObject);
begin
  WSACleanup;
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  QrPerfis.Close;
  if Usuario > -1000 then
  begin
    QrPerfis.SQL.Clear;
    QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    QrPerfis.SQL.Add('FROM perfisits pit');
    QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    QrPerfis.SQL.Add('AND pip.Codigo='+IntToStr(Usuario)); // Condi��o do LEFT JOIN
  end;
  QrPerfis.Open;
  if QrPerfis.RecordCount > 0 then Result := True;
end;

procedure TDmod.DefineUserSets_Painel(NomeForm, Definicao: String;
 Painel: TPanel);
begin
  QrUserSets.Close;
  QrUserSets.Params[0].AsString := NomeForm;
  QrUserSets.Params[1].AsString := Definicao;
  QrUserSets.Open;
  //
  if QrUserSets.RecordCount > 0 then
  begin
    if QrUserSetsAbilitado.Value = 0 then Painel.Enabled := False;
    if QrUserSetsVisivel.Value = 0 then Painel.Visible := False;
  end else Application.MessageBox(PChar('"DefineUserSets_Painel" n�o definido '+
  'para "'+Definicao+'" em "'+NomeForm+'".'), 'Erro', MB_OK+MB_ICONERROR);
end;

/////////////////////////////////////////////////////////////////////////////////////
procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
  DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
  CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  // Compatibilidade
end;

procedure TDmod.QrDono_CalcFields(DataSet: TDataSet);
{
var
  Natal: TDateTime;
}
begin
{
  //  erro mysql 5.1.x
  if QrDonoTipo.Value = 0 then
  begin
    //QrDonoLograd.Value := QrDonoELograd.Value
    //QrDonoUF.Value  := QrDonoEUF.Value;
    QrDonoCEP.Value := QrDonoECEP.Value;
  end else begin
    //QrDonoLograd.Value := QrDonoPLograd.Value;
    //QrDonoUF.Value  := QrDonoPUF.Value;
    QrDonoCEP.Value := QrDonoPCEP.Value;
  end;
  //  fim erro mysql 5.1.x
  QrDonoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoTE2_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe2.Value);
  QrDonoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' +Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value :=Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
  //
  if Trim(QrDonoTE1.Value) <> '' then
    QrDonoFONES.Value := QrDonoTE1_TXT.Value;
  if Trim(QrDonoTe2.Value) <> '' then
    QrDonoFONES.Value := QrDonoFONES.Value + ' - ' + QrDonoTE2_TXT.Value;
}
end;

procedure TDmod.QrEndereco_CalcFields(DataSet: TDataSet);
{
var
  Natal: TDateTime;
}
begin
{
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(Trunc(QrEnderecoNumero.Value), False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
}
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
{  erro!
  DModG.QrDono.CLose;
  DModG.QrDono.Open;
}
end;

function TDmod.CarregaPronunciamento(Compo: TComponent; Unico: Integer): String;
//function TDmod.CarregaItensPronunciamento(Compo: TComponent): String;
const
  MaxItens = 5;
  Itens: array [-2..MaxItens] of String = ('Texto Livre', 'INDEFINIDO',
    'Leitura sugerida', 'Leitura opcional',
    'M�sica', 'Leitura b�blica', 'Credo / Ora��o', 'A Definir');
var
  PropInfo: PPropInfo;
  I: integer;
  //Lista: TStringList;
begin
  if Compo <> nil then
  begin
    {
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Items');
    if PropInfo <> nil then
    begin
      TRadioGroup(Compo).Items.Clear;
      TRadioGroup(Compo).Items.Add('Outros');
      TRadioGroup(Compo).Items.Add('Cheque');
      TRadioGroup(Compo).Items.Add('DOC');
      TRadioGroup(Compo).Items.Add('TED');
      TRadioGroup(Compo).Items.Add('Esp�cie');
      TRadioGroup(Compo).Items.Add('Bloqueto');
      TRadioGroup(Compo).Items.Add('Duplicata');
      TRadioGroup(Compo).Items.Add('TEF');
      TRadioGroup(Compo).Items.Add('D�bito c/c');
    end;
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Values');
    if PropInfo <> nil then
    begin
      TDBRadioGroup(Compo).Values.Clear;
      TDBRadioGroup(Compo).Values.Add('0');
      TDBRadioGroup(Compo).Values.Add('1');
      TDBRadioGroup(Compo).Values.Add('2');
      TDBRadioGroup(Compo).Values.Add('3');
      TDBRadioGroup(Compo).Values.Add('4');
      TDBRadioGroup(Compo).Values.Add('5');
      TDBRadioGroup(Compo).Values.Add('6');
      TDBRadioGroup(Compo).Values.Add('7');
      TDBRadioGroup(Compo).Values.Add('8');
    end;
    }
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Items');
    //PropInfo := GetPropInfo(TComponent(Objeto).ClassInfo, 'CharCase');
    if PropInfo <> nil then
    begin
      {Lista := '';
      for I := 0 to MaxItens do
        Lista := Lista + Itens[I] + #13#10;
      Lista := TStringList.Create;
      SetPropValue(Compo, 'Items', Lista.Text);
      }
      TRadioGroup(Compo).Items.Clear;
      for I := 0 to MaxItens do
        TRadioGroup(Compo).Items.Add(Itens[I]);
    end;
    PropInfo := GetPropInfo(Compo.ClassInfo, 'SQL');
    if PropInfo <> nil then
    begin
      for I := 0 to MaxItens do
        Result := Result + '"' + Itens[I] + '",';
      Result := Result + '"?"';
    end;
  end else
  begin
    if Unico = -1 then
      Result := ''
    else
      Result := Itens[Unico];
  end;
end;

end.

