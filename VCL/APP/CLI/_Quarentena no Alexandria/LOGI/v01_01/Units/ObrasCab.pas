unit ObrasCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkMemo, ComCtrls, dmkEditDateTimePicker, UnDmkEnums;

type
  TFmObrasCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrObrasCab: TmySQLQuery;
    DsObrasCab: TDataSource;
    QrCadastro_Com_Itens_ITS: TmySQLQuery;
    DsCadastro_Com_Itens_ITS: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrObrasCabCodigo: TIntegerField;
    QrObrasCabNome: TWideStringField;
    QrObrasCabLocalObra: TWideStringField;
    Label3: TLabel;
    DBMemo1: TDBMemo;
    MeLocalObra: TdmkMemo;
    Label4: TLabel;
    TPDtaJobIni: TdmkEditDateTimePicker;
    Label5: TLabel;
    TPDtaJobFim: TdmkEditDateTimePicker;
    Label6: TLabel;
    QrObrasCabDtaJobIni: TDateField;
    QrObrasCabDtaJobFim: TDateField;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrObrasCabDTAJOBFIM_TXT: TWideStringField;
    Label11: TLabel;
    EdSigla: TdmkEdit;
    QrObrasCabSigla: TWideStringField;
    Label12: TLabel;
    DBEdit3: TDBEdit;
    Label13: TLabel;
    EdCodigoObra: TdmkEdit;
    EdArt: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    QrObrasCabCodigoObra: TWideStringField;
    QrObrasCabArt: TWideStringField;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QrObrasCabDTAJOBINI_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrObrasCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrObrasCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrObrasCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrObrasCabCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    //procedure ReopenCadastro_Com_Itens_ITS(Controle?: Integer);

  end;

var
  FmObrasCab: TFmObrasCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck(*, Cadastro_Com_Itens_ITS*);

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmObrasCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmObrasCab.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrCadastro_Com_Itens_ITSCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrCadastro_Com_Itens_ITSNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
*)
end;

procedure TFmObrasCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrObrasCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrObrasCab, QrCadastro_Com_Itens_ITS);
end;

procedure TFmObrasCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrObrasCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCadastro_Com_Itens_ITS);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCadastro_Com_Itens_ITS);
end;

procedure TFmObrasCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrObrasCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmObrasCab.DefParams;
begin
  VAR_GOTOTABELA := 'obrascab';
  VAR_GOTOMYSQLTABLE := QrObrasCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM obrascab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

procedure TFmObrasCab.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmObrasCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmObrasCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmObrasCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmObrasCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrCadastro_Com_Itens_ITSControle.Value) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCadastro_Com_Itens_ITS,
      QrCadastro_Com_Itens_ITSControle, QrCadastro_Com_Itens_ITSControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
}
end;

{
procedure TFmObrasCab.ReopenCadastro_Com_Itens_ITS(Controle?: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCadastro_Com_Itens_ITS, Dmod.MyDB, [
  'SELECT * ',
  'FROM cadastro_com_itens_its ',
  'WHERE Codigo=' + Geral.FF0(QrObrasCabCodigo?.Value),
  '']);
  //
  QrCadastro_Com_Itens_ITS.Locate('Controle?, Controle, []);
end;
}

procedure TFmObrasCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmObrasCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmObrasCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmObrasCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmObrasCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmObrasCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmObrasCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrObrasCabCodigo.Value;
  Close;
end;

procedure TFmObrasCab.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmObrasCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrObrasCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'obrascab');
end;

procedure TFmObrasCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Sigla: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Sigla := EdSigla.ValueVariant;
  if MyObjects.FIC(Length(Sigla) = 0, EdSigla, 'Defina uma sigla de no m�imo 10 caracteres!') then Exit;
  //
  Codigo := EdCodigo.ValueVariant;
  //Codigo := UMyMod.BuscaEmLivreY_Def('obrascab', 'Codigo', ImgTipo.SQLType,
    //QrObrasCabCodigo.Value);
  Codigo := UMyMod.BPGS1I32('obrascab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'obrascab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmObrasCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'obrascab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'obrascab', 'Codigo');
end;

procedure TFmObrasCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmObrasCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmObrasCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmObrasCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrObrasCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmObrasCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmObrasCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrObrasCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmObrasCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmObrasCab.QrObrasCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmObrasCab.QrObrasCabAfterScroll(DataSet: TDataSet);
begin
  //ReopenCadastro_Com_Itens_ITS(0);
end;

procedure TFmObrasCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrObrasCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmObrasCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrObrasCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'obrascab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmObrasCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmObrasCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrObrasCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'obrascab');
end;

procedure TFmObrasCab.QrObrasCabBeforeOpen(DataSet: TDataSet);
begin
  QrObrasCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmObrasCab.QrObrasCabCalcFields(DataSet: TDataSet);
begin
  QrObrasCabDTAJOBFIM_TXT.Value := Geral.FDT(QrObrasCabDtaJobFim.Value, 2);
  QrObrasCabDTAJOBINI_TXT.Value := Geral.FDT(QrObrasCabDtaJobIni.Value, 2);
end;

end.

