unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinData, WinSkinStore, Mask, DBCtrls, ImgList,
  contnrs, frxClass, frxDBSet, dmkGeral, Variants, MyDBCheck, dmkDBGrid,
  dmkEdit, OleCtrls, SHDocVw, (*dmkPopOutFntCBox,*) ValEdit, AdvToolBar,
  AdvShapeButton, AdvGlowButton, AdvToolBarStylers, AdvOfficeHint,
  AdvPreviewMenu, AdvMenus, Vcl.Imaging.pngimage, AdvMetroTaskDialog,
  {$IfNDef _tmp_EhLgistic}                   {$EndIf}
  NFSe_PF_0000, NFSe_PF_0201, DMKpnfsConversao,
  UnDmkEnums, DmkDAC_PF;

const
  TMaxArtes = 127;

type
  TcpCalc = (cpJurosMes, cpMulta);
  //
  TLetraBmp = record
    Topo, Esqu, Base, Dire: Integer;
    Imagem: TImage;
  end;
  TFonteBMP = record
    Nome: ShortString;  // NomeFonte + NomeArte
    Letras: array[1..TMaxArtes] of TLetraBmp;
  end;
  TArteBMP = record
    Nome: ShortString;  // NomeFonte + NomeArte
    Fontes: array[1..TMaxArtes] of TFonteBMP;
  end;
  TArtesBMP = array[1..TMaxArtes] of TArteBMP;
  //
  TFmPrincipal = class(TForm)
    Timer1: TTimer;
    PageControl1: TPageControl;
    StatusBar: TStatusBar;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AdvPreviewMenu1: TAdvPreviewMenu;
    AdvOfficeHint1: TAdvOfficeHint;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    Memo3: TMemo;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    APMExtratos: TAdvPopupMenu;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    ResultadosMensais1: TMenuItem;
    ReceitaseDespesas1: TMenuItem;
    Prestaodecontas1: TMenuItem;
    APMPesquisas: TAdvPopupMenu;
    Emqualquerconta1: TMenuItem;
    Contascontroladas1: TMenuItem;
    Contassazonais1: TMenuItem;
    LaAviso2: TLabel;
    LaAviso3: TLabel;
    TmSuporte: TTimer;
    TySuporte: TTrayIcon;
    AdvPMVerifiDB: TAdvPopupMenu;
    MenuItem20: TMenuItem;
    VerificaTabelasPblicas1: TMenuItem;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager21: TAdvPage;
    AdvToolBarPager22: TAdvPage;
    AdvToolBarPager23: TAdvPage;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar2: TAdvQuickAccessToolBar;
    AdvToolBarButton7: TAdvToolBarButton;
    AdvToolBarButton8: TAdvToolBarButton;
    AdvToolBarButton9: TAdvToolBarButton;
    AdvToolBarButton10: TAdvToolBarButton;
    AdvToolBarButton11: TAdvToolBarButton;
    AdvToolBarButton12: TAdvToolBarButton;
    AdvToolBarButton13: TAdvToolBarButton;
    AdvToolBar14: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton55: TAdvGlowButton;
    AdvGlowButton22: TAdvGlowButton;
    AdvToolBar26: TAdvToolBar;
    AdvGlowButton24: TAdvGlowButton;
    AdvGlowButton25: TAdvGlowButton;
    AdvGlowButton26: TAdvGlowButton;
    AdvGlowButton5: TAdvGlowButton;
    AdvToolBar31: TAdvToolBar;
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton27: TAdvGlowButton;
    AdvGlowButton29: TAdvGlowButton;
    AdvGlowButton32: TAdvGlowButton;
    AdvGlowButton33: TAdvGlowButton;
    AdvGlowButton34: TAdvGlowButton;
    AdvGlowButton35: TAdvGlowButton;
    AdvGlowButton37: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AdvGlowButton10: TAdvGlowButton;
    AdvGlowButton8: TAdvGlowButton;
    AdvToolBar30: TAdvToolBar;
    AdvGlowButton13: TAdvGlowButton;
    AdvGlowButton14: TAdvGlowButton;
    AdvGlowButton15: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar18: TAdvToolBar;
    AdvGlowMenuButton1: TAdvGlowMenuButton;
    AdvGlowMenuButton2: TAdvGlowMenuButton;
    AdvGlowButton12: TAdvGlowButton;
    AdvGlowButton23: TAdvGlowButton;
    AdvToolBar7: TAdvToolBar;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton16: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar8: TAdvToolBar;
    AdvGlowButton6: TAdvGlowButton;
    AdvGlowButton28: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvGlowButton30: TAdvGlowButton;
    AdvGlowButton31: TAdvGlowButton;
    AGBFiliais: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AdvGlowMenuButton5: TAdvGlowMenuButton;
    AdvGlowMenuButton6: TAdvGlowMenuButton;
    AdvGlowButton17: TAdvGlowButton;
    AdvPage4: TAdvPage;
    AdvToolBar10: TAdvToolBar;
    AdvGlowButton7: TAdvGlowButton;
    AdvPage2: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AGBNFSe_RPSPesq: TAdvGlowButton;
    AGBNFSe_NFSePesq: TAdvGlowButton;
    AGBNFSeLRpsC: TAdvGlowButton;
    AGBNFSeFatCab: TAdvGlowButton;
    AGBNFSe_Edit: TAdvGlowButton;
    AdvToolBar15: TAdvToolBar;
    AGBNFSeMenCab: TAdvGlowButton;
    AGBNFSeSrvCad: TAdvGlowButton;
    BalloonHint1: TBalloonHint;
    AdvGlowButton21: TAdvGlowButton;
    AdvGlowButton20: TAdvGlowButton;
    AdvGlowButton36: TAdvGlowButton;
    AdvToolBar28: TAdvToolBar;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton11: TAdvGlowButton;
    AdvGlowButton62: TAdvGlowButton;
    AdvGlowButton63: TAdvGlowButton;
    APMListasDiversas: TAdvPopupMenu;
    Caractersticas1: TMenuItem;
    ransporte1: TMenuItem;
    Servio1: TMenuItem;
    AdvGlowMenuButton3: TAdvGlowMenuButton;
    AdvPage5: TAdvPage;
    AdvToolBar19: TAdvToolBar;
    AGBFrtRegFisC: TAdvGlowButton;
    AGBCFOP: TAdvGlowButton;
    AGBNCM: TAdvGlowButton;
    AdvGlowButton87: TAdvGlowButton;
    AGBUFs: TAdvGlowButton;
    AGBCNAE: TAdvGlowButton;
    GBClasFisc: TAdvGlowButton;
    AGBCSOSN: TAdvGlowButton;
    AGBNFModDocFis: TAdvGlowButton;
    AGBTribDefCad: TAdvGlowButton;
    AGBTribDefCab: TAdvGlowButton;
    AGBFrtRegFatC: TAdvGlowButton;
    iposdeMedidas1: TMenuItem;
    AdvPage6: TAdvPage;
    AdvToolBar13: TAdvToolBar;
    AdvGlowButton77: TAdvGlowButton;
    AGBPediPrzCab: TAdvGlowButton;
    AGBUnidMed: TAdvGlowButton;
    AGBFrtFatCab: TAdvGlowButton;
    Produtospredominantes1: TMenuItem;
    AGBProdutCad: TAdvGlowButton;
    AdvPage7: TAdvPage;
    AdvToolBar38: TAdvToolBar;
    AGBFatPedNFs: TAdvGlowButton;
    AGBCTeLEnC: TAdvGlowButton;
    AGBCTePesq: TAdvGlowButton;
    AGBCTeInut: TAdvGlowButton;
    AdvGlowButton126: TAdvGlowButton;
    AdvGlowButton124: TAdvGlowButton;
    AGBContingencia: TAdvGlowButton;
    AGBNFeEventos: TAdvGlowButton;
    AGBConsultaNFe: TAdvGlowButton;
    AGBNFeDest: TAdvGlowButton;
    AGBNFeLoad_Inn: TAdvGlowButton;
    AGBNFeDesDowC: TAdvGlowButton;
    AdvToolBar40: TAdvToolBar;
    AGBCTeJust: TAdvGlowButton;
    AdvGlowButton41: TAdvGlowButton;
    AdvGlowButton61: TAdvGlowButton;
    AdvToolBar39: TAdvToolBar;
    AdvGlowButton131: TAdvGlowButton;
    AdvGlowButton148: TAdvGlowButton;
    AdvGlowButton83: TAdvGlowButton;
    AdvGlowButton152: TAdvGlowButton;
    GBValidaXML: TAdvGlowButton;
    AGBNFeExportaXML_B: TAdvGlowButton;
    AGBNFeConsCad: TAdvGlowButton;
    AGBNfeNewVer: TAdvGlowButton;
    AdvGlowButton52: TAdvGlowButton;
    AGBFrtSrvCad: TAdvGlowButton;
    AdvPage8: TAdvPage;
    AdvToolBar3: TAdvToolBar;
    AdvGlowButton40: TAdvGlowButton;
    AdvGlowButton42: TAdvGlowButton;
    AGBMDFePesq: TAdvGlowButton;
    AdvGlowButton44: TAdvGlowButton;
    AGBStatServ: TAdvGlowButton;
    AdvGlowButton46: TAdvGlowButton;
    AdvGlowButton47: TAdvGlowButton;
    AdvGlowButton48: TAdvGlowButton;
    AdvGlowButton49: TAdvGlowButton;
    AdvGlowButton50: TAdvGlowButton;
    AdvGlowButton51: TAdvGlowButton;
    AdvGlowButton53: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    AGBMDFeJust: TAdvGlowButton;
    AdvGlowButton56: TAdvGlowButton;
    AdvGlowButton57: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton58: TAdvGlowButton;
    AdvGlowButton59: TAdvGlowButton;
    AdvGlowButton60: TAdvGlowButton;
    AdvGlowButton64: TAdvGlowButton;
    AdvGlowButton65: TAdvGlowButton;
    AdvGlowButton66: TAdvGlowButton;
    AGBMDFeNaoEncerrados: TAdvGlowButton;
    AdvGlowButton68: TAdvGlowButton;
    AGBLayoutMDFe: TAdvGlowButton;
    AGBFrtMnfCab: TAdvGlowButton;
    TmVersao: TTimer;
    TimerIdle: TTimer;
    AdvToolBar20: TAdvToolBar;
    AdvGlowButton18: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    AdvGlowButton167: TAdvGlowButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure frLFamiliasGetValue(const ParName: String;
      var ParValue: Variant);
    procedure frLFamiliasUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure AdvToolBar3Close(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure ResultadosMensais1Click(Sender: TObject);
    procedure ReceitaseDespesas1Click(Sender: TObject);
    procedure Prestaodecontas1Click(Sender: TObject);
    procedure Emqualquerconta1Click(Sender: TObject);
    procedure Contascontroladas1Click(Sender: TObject);
    procedure Contassazonais1Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure VerificaTabelasPblicas1Click(Sender: TObject);
    procedure AdvToolBarButton7Click(Sender: TObject);
    procedure AdvToolBarButton8Click(Sender: TObject);
    procedure AdvToolBarButton9Click(Sender: TObject);
    procedure AdvToolBarButton10Click(Sender: TObject);
    procedure AdvToolBarButton13Click(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton55Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton63Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton62Click(Sender: TObject);
    procedure AdvGlowButton36Click(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure AdvGlowButton34Click(Sender: TObject);
    procedure AdvGlowButton27Click(Sender: TObject);
    procedure AdvGlowButton32Click(Sender: TObject);
    procedure AdvGlowButton29Click(Sender: TObject);
    procedure AdvGlowButton37Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AdvGlowButton14Click(Sender: TObject);
    procedure AdvGlowButton15Click(Sender: TObject);
    procedure AdvGlowButton13Click(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure AGBNFSe_EditClick(Sender: TObject);
    procedure AGBNFSe_NFSePesqClick(Sender: TObject);
    procedure AGBNFSe_RPSPesqClick(Sender: TObject);
    procedure AGBNFSeLRpsCClick(Sender: TObject);
    procedure AGBNFSeSrvCadClick(Sender: TObject);
    procedure AGBNFSeMenCabClick(Sender: TObject);
    procedure AGBNFSeFatCabClick(Sender: TObject);
    procedure AdvGlowButton33Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure ransporte1Click(Sender: TObject);
    procedure Servio1Click(Sender: TObject);
    procedure AdvGlowButton87Click(Sender: TObject);
    procedure AGBNCMClick(Sender: TObject);
    procedure AGBUFsClick(Sender: TObject);
    procedure AGBCFOPClick(Sender: TObject);
    procedure AGBCNAEClick(Sender: TObject);
    procedure AGBNFModDocFisClick(Sender: TObject);
    procedure AGBCSOSNClick(Sender: TObject);
    procedure AGBNatOperClick(Sender: TObject);
    procedure AGBTribDefCadClick(Sender: TObject);
    procedure AGBTribDefCabClick(Sender: TObject);
    procedure AGBFrtRegFisCClick(Sender: TObject);
    procedure AGBFrtRegFatCClick(Sender: TObject);
    procedure iposdeMedidas1Click(Sender: TObject);
    procedure AGBPediPrzCabClick(Sender: TObject);
    procedure AGBUnidMedClick(Sender: TObject);
    procedure AdvGlowButton77Click(Sender: TObject);
    procedure Produtospredominantes1Click(Sender: TObject);
    procedure AGBProdutCadClick(Sender: TObject);
    procedure AdvGlowButton126Click(Sender: TObject);
    procedure AdvGlowButton131Click(Sender: TObject);
    procedure AGBCTePesqClick(Sender: TObject);
    procedure AGBFrtSrvCadClick(Sender: TObject);
    procedure AGBCTeJustClick(Sender: TObject);
    procedure AGBCTeInutClick(Sender: TObject);
    procedure AdvGlowButton83Click(Sender: TObject);
    procedure AGBStatServClick(Sender: TObject);
    procedure AGBFrtMnfCabClick(Sender: TObject);
    procedure AGBFrtFatCabClick(Sender: TObject);
    procedure AdvGlowButton52Click(Sender: TObject);
    procedure AGBLayoutMDFeClick(Sender: TObject);
    procedure AGBCTeLEnCClick(Sender: TObject);
    procedure AdvGlowButton42Click(Sender: TObject);
    procedure AGBMDFePesqClick(Sender: TObject);
    procedure AGBMDFeNaoEncerradosClick(Sender: TObject);
    procedure AGBMDFeJustClick(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
  private
    FALiberar, FAtualizouFavoritos: Boolean;
    { Private declarations }
    procedure MostraVerifiDB();
    procedure MostraLogoff();
    procedure MostraOpcoes();
    procedure MostraMatriz();
    procedure MostraAnotacoes();
    procedure CadastroBancos();
    procedure SkinMenu(Index: integer);
    procedure MostraFiliais;
    procedure MostraFormDescanso;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
  public
    { Public declarations }
    Alfabeto: TObjectList;
    //PNG, JoinedPNG: TPngObject;
    FBarraTarefa: Integer;
    FTipoNovoEnti: Integer;
    FLoadedFonts: Boolean;
    FListaArtes: TStringList;
    FArtesBMP: Integer;
    MyArtesBMP: TArtesBMP;
    FEntInt, FSeqJan, FSeqItm: Integer;
    //
    function CalculaTextFontSize(FoNam: String; FoTam: Integer;
             ResTela: Double): Integer;
    // Financeiro
    procedure RealizaPesquisas();
    procedure ResultadosMensais();
    procedure Movimento();
    procedure ImprimeExtratos();
    // Fim financeiro
    function CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure SelecionaImagemdefundo;
    procedure PagarRolarEmissao(Query: TmySQLQuery);
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure CriaMinhasEtiquetas;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; AbrirEmAba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    function RecriaTempTable(Tabela: String): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    //procedure DefineVarsCliInt(Empresa: Integer);
    procedure AcoesIniciaisDoAplicativo();
    function ColunasDeTexto(FoNome: String; FoSizeT: Integer;
             MaxWidth: Integer; Qry: TMySQLQuery;
             Campo: String; Codigo: Integer): Integer;
    procedure ReCaptionComponentesDeForm(Form: TForm);
    procedure BalanceteConfiguravel(TabLctA: String);
    procedure ExtratosFinanceirosEmpresaUnica(TabLctA: String);
    procedure CadastroDeContasNiv();
    procedure CadastroDeProtocolos(Lote: Integer);
    //
    // Logidek
    function AcaoEspecificaDeApp(Servico: String): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;


const
  CO_ARQLOG = 'Inicializacao';
  FAltLin = CO_POLEGADA / 2.16;

implementation

uses UnMyObjects, Module, Entidades, VerifiDB, MyGlyfs, Logidek_Dmk, Opcoes,
  MyListas, MyVCLSkin, CalcPercent, UnMsgInt, UnGOTOy, UCreate,
  MultiEtiq, Maladireta, EntidadesImp, GetValor, ModuleGeral, Matriz, Cartas,
  Bancos, EmiteCheque_0, Entidade2, Backup3, ChConfigCab, UnFinanceiroJan,
  UnMyPrinters, GetBiosInformation, UnitMD5, ReceDesp, PesqContaCtrl,
  Saldos, EventosCad, ModuleFin, LctEncerraMes, (*UnProtocoUnit,*)
  CashBal, Resmes, PrincipalImp, Pesquisas, CtaGruImp, Extratos, UnFinanceiro,
  Formularios, CentroCusto, CashPreCta, LinkRankSkin,
  UnDmkWeb, Anotacoes, ContasNiv,
(*
  ProtocoMot, ProtocoOco, Protocolos,
  UnBloquetos, ModuleBloGeren,
*)
  PreEmail, FavoritosG, VerifiDBTerceiros,
  ModuleLct2, ParamsEmp, UnLic_Dmk, About, TrnsCad, CfgCadLista,
  UnFrt_PF, UnPraz_PF, UnCTe_PF, UnTrns_PF,
  // Temporarios??
  FrtFatCab, UnidMed, ModuleCTe_0000, ModuleMDFe_0000, UnMDFe_PF, Descanso;
  //;

{$R *.DFM}

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MLAGeral.CriaLog(CO_ARQLOG, 'Inicio ativa��o form principal.', False);
  APP_LIBERADO := True;
  if not MyObjects.CorIniComponente() then Hide else Show;
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Trim(StatusBar.Panels[3].Text) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then
  Timer1.Enabled := True;
  MLAGeral.CriaLog(CO_ARQLOG, 'Fim ativa��o form principal.', False);
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem: String;
  Retorno: String;
begin
  FAtualizouFavoritos := False;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_KIND_DEPTO := kdNenhum;
  //
  VAR_FIN_SELFG_000_CLI := 'Membro';
  VAR_FIN_SELFG_000_FRN := '-';
  VAR_LCT := '';
  Application.OnException := MyObjects.MostraErro;
  VAR_MULTIPLAS_TAB_LCT := True;
  VAR_SERVIDOR3 := 3;
  //VAR_TYPE_LOG          := ttlCliIntUni;
  VAR_TYPE_LOG          := ttlFiliLog;
  VAR_ADMIN             := 'admin';
  FEntInt := -11;
  Alfabeto := TObjectList.Create;
  //VAR_CALCULADORA_COMPONENTCLASS := TFmCalculadora;
  //VAR_CALCULADORA_REFERENCE      := FmCalculadora;
  VAR_USA_TAG_BITBTN := True;
  //
  FTipoNovoEnti := 0;
  MLAGeral.CriaLog(CO_ARQLOG, 'Inicio cria��o form principal.', True);
  VAR_STDATALICENCA := StatusBar.Panels[07];
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_STLOGIN       := StatusBar.Panels[01];
  VAR_SD1           := sd1;
  StatusBar.Panels[3].Text := Geral.FileVerInfo(Application.ExeName, 3);
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;

  Application.OnMessage := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;

  MLAGeral.CriaLog(CO_ARQLOG, 'Vari�veis iniciais configuradas.', False);
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MLAGeral.CriaLog(CO_ARQLOG, 'Popup geral definido.', False);
  PageControl1.ActivePageIndex := 0;
  MLAGeral.CriaLog(CO_ARQLOG, 'Pagina inicial do pagecontrol definida.', False);
  //////////////////////////////////////////////////////////////////////////////
  // Local
  VAR_IMPCHEQUE       := Geral.ReadAppKeyCU('ImpCheque', 'Dermatek', ktInteger, 0);
  VAR_IMPCHEQUE_PORTA := Geral.ReadAppKeyCU('ImpCheque_Porta', 'Dermatek', ktString, 'COM2');
  // Servidor
  VAR_IMPCH_IP        := Geral.ReadAppKeyCU('ImpreCh_IP', 'Dermatek', ktString, '127.0.0.1');
  VAR_IMPCHPORTA      := Geral.ReadAppKeyCU('ImpreChPorta', 'Dermatek', ktInteger, 9520);
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Verificando impressora de cheque', False);
  MyPrinters.VerificaImpressoraDeCheque(Retorno);
  if Retorno <> '' then
    Geral.MensagemBox(Retorno, 'Aviso', MB_OK+MB_ICONWARNING);
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso, PageControl1);
end;

procedure TFmPrincipal.Servio1Click(Sender: TObject);
begin
  Frt_PF.MostraFormFrtCaracSer();
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if FListaArtes <> nil then
    FListaArtes.Free;
  if DModG <> nil then
    DModG.MostraBackup3();
  Application.Terminate;
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
  except
    Result := False
  end;
end;

procedure TFmPrincipal.Prestaodecontas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCashPreCta, FmCashPreCta, afmoNegarComAviso) then
  begin
    FmCashPreCta.ShowModal;
    FmCashPreCta.Destroy;
  end;
end;

procedure TFmPrincipal.Produtospredominantes1Click(Sender: TObject);
begin
  Frt_PF.MostraFormFrtProPred();
end;

procedure TFmPrincipal.ransporte1Click(Sender: TObject);
begin
  Frt_PF.MostraFormFrtCaracAd();
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2, True);
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  if DBCheck.CriaFm(TFmProtocoOco, FmProtocoOco, afmoNegarComAviso) then
  begin
    FmProtocoOco.ShowModal;
    FmProtocoOco.Destroy;
  end;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton126Click(Sender: TObject);
begin
  CTe_PF.MostraFormStepsCTe_StatusServico();
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton131Click(Sender: TObject);
const
  InfoTermino = True;
begin
  DmCTe_0000.AtualizaXML_No_BD_Tudo(InfoTermino);
end;

procedure TFmPrincipal.AdvGlowButton13Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton14Click(Sender: TObject);
begin
  CadastroBancos();
end;

procedure TFmPrincipal.AdvGlowButton15Click(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidadesImp, FmEntidadesImp, afmoNegarComAviso) then
  begin
    FmEntidadesImp.ShowModal;
    FmEntidadesImp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UBloquetos.MostraBloGerenInadImp(PageControl1, AdvToolBarPager1);
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UBloquetos.CadastroCNAB_Cfg;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UBloquetos.MostraBloArre(0, 0);
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton22Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UBloquetos.MostraBloGeren(-1, 0, 0, 0, PageControl1, AdvToolBarPager1);
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSaldos, FmSaldos, afmoNegarComAviso) then
  begin
    FmSaldos.ShowModal;
    FmSaldos.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  FinanceiroJan.SaldoDeContas;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
    TDmLct2(DmLctX).GerenciaEmpresa(PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton27Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AdvGlowButton29Click(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  CadastroDeProtocolos(0);
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AdvGlowButton31Click(Sender: TObject);
begin
  MostraOpcoes();
end;

procedure TFmPrincipal.AdvGlowButton32Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton33Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.AdvGlowButton34Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton35Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AdvGlowButton36Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UBloquetos.CadastroDeBloOpcoes;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton37Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  if DBCheck.CriaFm(TFmProtocoMot, FmProtocoMot, afmoNegarComAviso) then
  begin
    FmProtocoMot.ShowModal;
    FmProtocoMot.Destroy;
  end;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton42Click(Sender: TObject);
begin
  MDFe_PF.MostraFormMDFeLEnc(0);
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton52Click(Sender: TObject);
begin
  CTe_PF.MostraFormCTeLayout_0200();
end;

procedure TFmPrincipal.AdvGlowButton55Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UBloquetos.MostraBloCNAB_Ret;
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
const
  InfoTermino = True;
begin
  DmMDFe_0000.AtualizaXML_No_BD_Tudo(InfoTermino);
end;

procedure TFmPrincipal.AdvGlowButton62Click(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  ProtocoUnit.MostraProEnPr(0);
{$EndIf}
end;

procedure TFmPrincipal.AdvGlowButton63Click(Sender: TObject);
begin
  //Micell-ProtocoUnit.MostraProtoGer(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AGBFrtFatCabClick(Sender: TObject);
begin
  Frt_PF.MostraFormFrtFatCab();
end;

procedure TFmPrincipal.AGBFrtMnfCabClick(Sender: TObject);
begin
  Frt_PF.MostraFormFrtMnfCab();
end;

procedure TFmPrincipal.AGBFrtRegFatCClick(Sender: TObject);
begin
  Frt_PF.MostraFormFrtRegFatC(0);
end;

procedure TFmPrincipal.AGBFrtRegFisCClick(Sender: TObject);
begin
  Frt_PF.MostraFormFrtRegFisC(0);
end;

procedure TFmPrincipal.AGBFrtSrvCadClick(Sender: TObject);
begin
  Frt_PF.MostraFormFrtSrvCad(0);
end;

procedure TFmPrincipal.AGBLayoutMDFeClick(Sender: TObject);
begin
  MDFe_PF.MostraFormMDFeLayout_0100();
end;

procedure TFmPrincipal.AGBMDFeJustClick(Sender: TObject);
begin
  MDFe_PF.MostraFormMDFeJust();
end;

procedure TFmPrincipal.AGBMDFeNaoEncerradosClick(Sender: TObject);
begin
  MDFe_PF.MostraFormStepsMDFe_NaoEncerrados();
end;

procedure TFmPrincipal.AGBMDFePesqClick(Sender: TObject);
begin
  MDFe_PF.MostraFormMDFePesq(True, PageControl1, AdvToolBarPager1);
end;

procedure TFmPrincipal.AdvGlowButton77Click(Sender: TObject);
begin
  Frt_PF.MostraFormFrtPrcCad(0);
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvGlowButton83Click(Sender: TObject);
begin
  CTe_PF.MostraFormStepsCTe_StepGenerico();
end;

procedure TFmPrincipal.AdvGlowButton87Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  Trns_PF.MostraFormTrnsCad(0, 0);
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvToolBar3Close(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvToolBarButton10Click(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton13Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvToolBarButton7Click(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.AdvToolBarButton8Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvToolBarButton9Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso3, LaAviso2, AdvGlowButton29, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGBCFOPClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmCFOP2003, FmCFOP2003, afmoNegarComAviso) then
  begin
    FmCFOP2003.ShowModal;
    FmCFOP2003.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBCNAEClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmCNAE21, FmCNAE21, afmoNegarComAviso) then
  begin
    FmCNAE21.ShowModal;
    FmCNAE21.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBCSOSNClick(Sender: TObject);
begin
(*
  //DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  if DBCheck.CriaFm(TFmNFaEditCSOSNAlq, FmNFaEditCSOSNAlq, afmoNegarComAviso) then
  begin
    FmNFaEditCSOSNAlq.QrParamsEmp.Locate('Codigo', DModG.QrParamsEmpCodigo.Value, []);
    FmNFaEditCSOSNAlq.ShowModal;
    FmNFaEditCSOSNAlq.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBCTeInutClick(Sender: TObject);
begin
  CTe_PF.MostraFormCTeInut();
end;

procedure TFmPrincipal.AGBCTeJustClick(Sender: TObject);
begin
  CTe_PF.MostraFormCTeJust();
end;

procedure TFmPrincipal.AGBCTeLEnCClick(Sender: TObject);
begin
  CTe_PF.MostraFormCTeLEnc(0);
end;

procedure TFmPrincipal.AGBCTePesqClick(Sender: TObject);
begin
  CTe_PF.MostraFormCTePesq(True, PageControl1, AdvToolBarPager1, 0);
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraFiliais();
end;

procedure TFmPrincipal.AGBNatOperClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmNatOper, FmNatOper, afmoNegarComAviso) then
  begin
    FmNatOper.ShowModal;
    FmNatOper.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBNCMClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmNCMs, FmNCMs, afmoNegarComAviso) then
  begin
    FmNCMs.ShowModal;
    FmNCMs.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBNFModDocFisClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmNFMoDocFis, FmNFMoDocFis, afmoNegarComAviso) then
  begin
    FmNFMoDocFis.ShowModal;
    FmNFMoDocFis.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBNFSeFatCabClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFSe_PF_0000.MostraFormNFSeFatCab(True, PageControl1, AdvToolBarPager1);
{$EndIf}
end;

procedure TFmPrincipal.AGBNFSeLRpsCClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
{$EndIf}
end;

procedure TFmPrincipal.AGBNFSeMenCabClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFSe_PF_0000.MostraFormNFSeMenCab(False, nil, nil);
{$EndIf}
end;

procedure TFmPrincipal.AGBNFSeSrvCadClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
{$EndIf}
end;

procedure TFmPrincipal.AGBNFSe_EditClick(Sender: TObject);
{$IfNDef _tmp_EhLgistic}
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, NumNF: Integer;
  SerieNF: String;
{$EndIf}
begin
{$IfNDef _tmp_EhLgistic}
  Prestador := DmodG.QrFiliLogFilial.Value;
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
    Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
    Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor, SerieNF,
    NumNF, nil);
{$EndIf}
end;

procedure TFmPrincipal.AGBNFSe_NFSePesqClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
{$EndIf}
end;

procedure TFmPrincipal.AGBNFSe_RPSPesqClick(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(True, PageControl1, AdvToolBarPager1);
{$EndIf}
end;

procedure TFmPrincipal.AGBPediPrzCabClick(Sender: TObject);
begin
  Praz_PF.MostraFormPediPrzCab1(0);
end;

procedure TFmPrincipal.AGBProdutCadClick(Sender: TObject);
begin
  Frt_PF.MostraFormProdutCad(0);
end;

procedure TFmPrincipal.AGBStatServClick(Sender: TObject);
begin
  MDFe_PF.MostraFormStepsMDFe_StatusServico();
end;

procedure TFmPrincipal.AGBTribDefCabClick(Sender: TObject);
begin
(*
  Tributos_PF.MostraFormTribDefCab(0);
*)
end;

procedure TFmPrincipal.AGBTribDefCadClick(Sender: TObject);
begin
(*
  Tributos_PF.MostraFormTribDefCad(0);
*)
end;

procedure TFmPrincipal.AGBUFsClick(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmUFs, FmUFs, afmoNegarComAviso) then
  begin
    FmUFs.ShowModal;
    FmUFs.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AGBUnidMedClick(Sender: TObject);
begin
  //Result := False;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    //if Codigo <> 0 then
      //FmUnidMed.LocCod(Codigo, Codigo);
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    //Result := True;
  end;
end;

procedure TFmPrincipal.MostraFiliais;
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1, False, True, afmoLiberado);
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmLogidek_Dmk.Show;
  Enabled := False;
  FmLogidek_Dmk.Refresh;
  FmLogidek_Dmk.EdSenha.Text := FmLogidek_Dmk.EdSenha.Text+'*';
  FmLogidek_Dmk.EdSenha.Refresh;
  FmLogidek_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmLogidek_Dmk.EdSenha.Text := FmLogidek_Dmk.EdSenha.Text+'*';
  FmLogidek_Dmk.EdSenha.Refresh;
  FmLogidek_Dmk.ReloadSkin;
  FmLogidek_Dmk.EdLogin.Text := '';
  FmLogidek_Dmk.EdSenha.Text := '';
  FmLogidek_Dmk.EdSenha.Refresh;
  FmLogidek_Dmk.EdLogin.ReadOnly := False;
  FmLogidek_Dmk.EdSenha.ReadOnly := False;
  FmLogidek_Dmk.EdLogin.SetFocus;
  //FmLogidek_Dmk.ReloadSkin;
  FmLogidek_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
begin
{$IfNDef _tmp_EhLgistic}
  if DModG <> nil then
    DmodG.ExecutaPing([Dmod.MyDB, DModG.MyPID_DB, DModG.AllID_DB]);
{$EndIf}
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
Exit;
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton13, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(AdvToolBarButton8, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
  Arq: String;
begin
Exit;
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Logidek', 'Logidek',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO, CO_DMKID_APP,
    DModG.ObtemAgora(), Memo3, dtExec, Versao, Arq, False, ApenasVerifica,
    BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasPblicas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.BalanceteConfiguravel(TabLctA: String);
begin
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroBancos;
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  ShowMessage('Faturando n�o implementado');
  Result := 0;
end;

function TFmPrincipal.ColunasDeTexto(FoNome: String; FoSizeT: Integer;
MaxWidth: Integer; Qry: TMySQLQuery; Campo: String; Codigo: Integer): Integer;
var
  MeLinha1, MeLinha2: TMemo;
  R: Integer;
begin
  Result := 1;
  MeLinha1 := TMemo.Create(FmPrincipal);
  MeLinha1.Visible := False;
  MeLinha1.Update;
  MeLinha1.Parent := FmPrincipal;
  // Evitar quebra de linha antecipada!
  MeLinha1.Width := MaxWidth;
  MeLinha1.Height := Height;
  MeLinha1.Font.Name := FoNome;
  MeLinha1.Font.Size := FoSizeT;
  //
  MeLinha2 := TMemo.Create(FmPrincipal);
  MeLinha2.Visible := False;
  MeLinha2.Update;
  MeLinha2.Parent := FmPrincipal;
  MeLinha2.Width := MaxWidth;
  MeLinha2.Height := Height;
  MeLinha2.WordWrap := False;
  MeLinha2.Font.Name := FoNome;
  MeLinha2.Font.Size := FoSizeT;
  //
  MeLinha1.Lines.Clear;
  MeLinha2.Lines.Clear;
  //
  Qry.Close;
  Qry.Params[0].AsInteger := Codigo;
  Qry.Open;
  while not Qry.Eof do
  begin
    MeLinha1.Lines.Add(Qry.FieldByName(Campo).AsString);
    MeLinha2.Lines.Add(Qry.FieldByName(Campo).AsString);
    //
    Qry.Next;
  end;
  //
  if MeLinha1.Lines.Count = MeLinha2.Lines.Count then
  begin
    R := 1;
    repeat
      R := R + 1;
      MeLinha1.Width := MaxWidth div R;
      MeLinha2.Width := MeLinha1.Width;
    until (MeLinha1.Lines.Count <> MeLinha2.Lines.Count);
    Result := R - 1;
  end;
  MeLinha1.Destroy;
  MeLinha2.Destroy;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  ShowMessage('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal.Contascontroladas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesqContaCtrl, FmPesqContaCtrl, afmoNegarComAviso) then
  begin
    FmPesqContaCtrl.ShowModal;
    FmPesqContaCtrl.Destroy;
  end;
end;

procedure TFmPrincipal.Contassazonais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCtaGruImp, FmCtaGruImp, afmoNegarComAviso) then
  begin
    FmCtaGruImp.ShowModal;
    FmCtaGruImp.Destroy;
  end;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.PagarReceber1Click(Sender: TObject);
begin
  ImprimeExtratos();
end;

procedure TFmPrincipal.PagarRolarEmissao(Query: TmySQLQuery);
begin
end;

{
procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  DmodG.QrCliIntUni.Open;
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
//:::
  //DmodFin.QrCarts.Close;
  //DmodFin.QrLctos.Close;
//
end;
}

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmLogidek_Dmk.Show;
  FmLogidek_Dmk.EdLogin.Text   := '';
  FmLogidek_Dmk.EdSenha.Text   := '';
  FmLogidek_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraMatriz;
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraVerifiDB();
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.Movimento();
begin
  if DBCheck.CriaFm(TFmPrincipalImp, FmPrincipalImp, afmoNegarComAviso) then
  begin
    FmPrincipalImp.ShowModal;
    FmPrincipalImp.Destroy;
  end;
end;

procedure TFmPrincipal.Movimento1Click(Sender: TObject);
begin
  Movimento();
end;

procedure TFmPrincipal.ImprimeExtratos;
begin
  if DBCheck.CriaFm(TFmExtratos, FmExtratos, afmoNegarComAviso) then
  begin
    FmExtratos.ShowModal;
    FmExtratos.Destroy;
  end;
end;

procedure TFmPrincipal.iposdeMedidas1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'TrnsMyTpMed', 20, ncGerlSeq1,
  'Tipos de Medida', [], False, Null, [], [], False);
end;

procedure TFmPrincipal.RealizaPesquisas;
begin
  if DBCheck.CriaFm(TFmPesquisas, FmPesquisas, afmoNegarComAviso) then
  begin
    FmPesquisas.ShowModal;
    FmPesquisas.FCliente_Txt := 'Membro:';
    FmPesquisas.FFornece_Txt := '-';
    FmPesquisas.Destroy;
  end;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.CriaImpressaoDiversos(Indice: Integer);
begin
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    FmFormularios.RGRelatorio.ItemIndex := Indice;
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
end;

procedure TFmPrincipal.CriaMinhasEtiquetas();
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
end;

procedure TFmPrincipal.Emqualquerconta1Click(Sender: TObject);
begin
  RealizaPesquisas;
end;

procedure TFmPrincipal.ExtratosFinanceirosEmpresaUnica(TabLctA: String);
begin
  if VAR_LIB_EMPRESAS = '' then
    VAR_LIB_EMPRESAS := '-100000000';
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmExtratos, FmExtratos, afmoNegarComAviso) then
  begin
    FmExtratos.ShowModal;
    FmExtratos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeContasNiv;
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

function TFmPrincipal.CalculaTextFontSize(FoNam: String; FoTam: Integer;
  ResTela: Double): Integer;
var
  I, Ini, tOri, tCal: Integer;
begin
  Result := FoTam;
  if ResTela < 100 then
    Ini := 1
  else
    Ini := FoTam;
  //
  Canvas.Font.Name := FoNam;
  Canvas.Font.Size := FoTam;
  tOri := Canvas.TextWidth(TextStringRows);
  for I := Ini to 1024 do
  begin
    Canvas.Font.Size := I;
    tCal := Canvas.TextWidth(TextStringRows);
    if (tCal / tOri * 100) > ResTela then
    begin
      Result := I - 1;
      Break;
    end;
  end;
end;

procedure TFmPrincipal.frLFamiliasGetValue(const ParName: String;
  var ParValue: Variant);
begin
  //
end;

procedure TFmPrincipal.frLFamiliasUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  Val := MLAGeral.MyUserFunction(Name, p1, p2, p3, Val);
end;

procedure TFmPrincipal.ResultadosMensais();
var
  Entidade, CliInt: Integer;
begin
  if DModG.SelecionaEmpresa(sllNenhuma) then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
    if DBCheck.CriaFm(TFmResMes, FmResMes, afmoNegarComAviso) then
    begin
      DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, FmResMes.FDtEncer, FmResMes.FDtMorto,
        FmResMes.FTabLctA, FmResMes.FTabLctB, FmResMes.FTabLctD);
      FmResMes.ShowModal;
      FmResMes.Destroy;
    end;
  end;
end;

procedure TFmPrincipal.ResultadosMensais1Click(Sender: TObject);
begin
  ResultadosMensais();
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Criando Module Geral');
      DModG.MyPID_DB_Cria();
      //
      if not FAtualizouFavoritos then
      begin
        MyObjects.Informa2(LaAviso3, LaAviso2, True, 'Criando favoritos');
        DModG.CriaFavoritos(AdvToolBarPager1, LaAviso3, LaAviso2, AdvGlowButton29, FmPrincipal);
        //
        FAtualizouFavoritos := True;
      end;
      //
      //DefineVarsCliInt(FEntInt);
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      //  Descanso
      MostraFormDescanso();
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.ReceitaseDespesas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmReceDesp, FmReceDesp, afmoNegarComAviso) then
  begin
    FmReceDesp.ShowModal;
    FmReceDesp.Destroy;
  end;
end;

function TFmPrincipal.RecriaTempTable(Tabela: String): Boolean;
begin
  Result := Ucriar.RecriaTempTable(Tabela, DModG.QrUpdPID1, False) <> '';
end;

procedure TFmPrincipal.CadastroDeProtocolos(Lote: Integer);
begin
{$IfNDef _tmp_EhLgistic}
  if DBCheck.CriaFm(TFmProtocolos, FmProtocolos, afmoNegarComAviso) then
  begin
    FmProtocolos.FLocLote := Lote;
    FmProtocolos.ShowModal;
    FmProtocolos.Destroy;
    DModG.ReopenPTK();
  end;
{$EndIf}
end;

procedure TFmPrincipal.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  //Transition2.Prepare(PageControl1.Parent, PageControl1.BoundsRect);
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
end;

procedure InfoWindow(Wnd: HWnd; Codigo, Item: Integer; Texto: String; WndID: Int64);
var
  WInfo: TWindowInfo;
  //p: TPoint;
  //Atom: TAtom;
{

  DWORD cbSize, ;
  RECT  rcWindow;
  RECT  rcClient;
  DWORD dwStyle;
  DWORD dwExStyle;
  DWORD dwWindowStatus;
  UINT  cxWindowBorders;
  UINT  cyWindowBorders;
  ATOM  atomWindowType;
  WORD  wCreatorVersion;  ATOM: TAtom;
}
  cbSize,
  rcWindowT, rcWindowE, rcWindowB,
  rcWindowD, rcClientT, rcClientE,
  rcClientB, rcClientD, dwStyle,
  dwExStyle, dwWindowStatus, cxWindowBorders,
  cyWindowBorders, atomWindowType: Integer;
begin
  WInfo.cbSize := SizeOf(TWindowInfo);
  GetWindowInfo(Wnd, WInfo);
  // Obter as coordenadas de tela, as coordenadas
{  T := WInfo.rcWindow.Top;
  E := WInfo.rcWindow.Left;
  B := WInfo.rcWindow.Bottom;
  D := WInfo.rcWindow.Right;
  ATom := WInfo.atomWindowType;
{
  Showmessage('canto superior esquerdo do ret�ngulo da �rea cliente, coordenar, X: ' +
               InttoStr(P.X) + 'Y:  '+
               InttoStr(P.Y) + '�');
}
  cbSize    := WInfo.cbSize;
  rcWindowT := WInfo.rcWindow.Top;
  rcWindowE := WInfo.rcWindow.Left;
  rcWindowB := WInfo.rcWindow.Bottom;
  rcWindowD := WInfo.rcWindow.Right;
  rcClientT := WInfo.rcClient.Top;
  rcClientE := WInfo.rcClient.Left;
  rcClientB := WInfo.rcClient.Bottom;
  rcClientD := WInfo.rcClient.Right;
  //
  dwStyle         := WInfo.dwStyle;
  dwExStyle       := WInfo.dwExStyle;
  dwWindowStatus  := WInfo.dwOtherStuff;
  cxWindowBorders := WInfo.cxWindowBorders;
  cyWindowBorders := WInfo.cyWindowBorders;
  atomWindowType  := WInfo.atomWindowType;
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'janelas', False, [
  'WndID', 'Texto', 'cbSize',
  'rcWindowT', 'rcWindowE', 'rcWindowB',
  'rcWindowD', 'rcClientT', 'rcClientE',
  'rcClientB', 'rcClientD', 'dwStyle',
  'dwExStyle', 'dwWindowStatus', 'cxWindowBorders',
  'cyWindowBorders', 'atomWindowType'], [
  'Codigo', 'Item'], [
  WndID, Trim(Texto), cbSize,
  rcWindowT, rcWindowE, rcWindowB,
  rcWindowD, rcClientT, rcClientE,
  rcClientB, rcClientD, dwStyle,
  dwExStyle, dwWindowStatus, cxWindowBorders,
  cyWindowBorders, atomWindowType], [
  Codigo, Item], False);
end;

(* N�o usa
function EnumChildWindowsProc(Wnd: HWnd; Form: TFmGetJanelas): Bool; export;
  {$ifdef Win32} stdcall; {$endif}
var
  Buffer: array[0..99] of Char;
  I: Int64;
//  T, E, B, D: Integer;
begin
  GetWindowText(Wnd, Buffer, 100);
  if StrLen(Buffer) <> 0 then
  begin
    FmPrincipal.FSeqItm := FmPrincipal.FSeqItm + 1;
    InfoWindow(Wnd, FmPrincipal.FSeqJan, FmPrincipal.FSeqItm, Buffer, 0);
    //FmPrincipal.ListBox7.Items.Add(StrPas(Buffer) + '(T=' + IntToStr(T) + ', E=' + IntToStr(E) + ', B=' + IntToStr(B) + ', D=' + IntToStr(D) + ')');
    I := GetWindow(Wnd, GW_CHILD);
    if I <> 0 then
    begin
      PNode := CNode;
      EnumChildWindows(Wnd, @EnumChildWindowsProc, 0);
    end;
  end;
  Result := True;
end;
*)

(* N�o usa
function EnumWindowsProc(Wnd: HWnd; Form: TForm): Boolean; Export; StdCall;
var
  Buffer: array[0..99] of char;
//  T, E, B, D: Integer;
begin
  GetWindowText(Wnd, Buffer, 100);
  if StrLen(Buffer) <> 0 then
  begin
    FmPrincipal.FSeqItm := 0;
    FmPrincipal.FSeqJan := FmPrincipal.FSeqJan + 1;
    InfoWindow(Wnd, FmPrincipal.FSeqJan, 0, Buffer, 0);
    //FmPrincipal.ListBox5.Items.Add(StrPas(Buffer) + '(T=' + IntToStr(T) + ', E=' + IntToStr(E) + ', B=' + IntToStr(B) + ', D=' + IntToStr(D) + ')');
    EnumChildWindows(Wnd, @EnumChildWindowsProc, 0);
  end;
  Result := True;
end;
*)

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
begin
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade,
  Conta: Integer): Boolean;
begin
  Result := True;
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; AbrirEmAba: Boolean);
begin
 // Nada
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
  MostraVerifiDB();
end;

procedure TFmPrincipal.MostraAnotacoes;
begin
  if DBCheck.CriaFm(TFmAnotacoes, FmAnotacoes, afmoNegarComAviso) then
  begin
    FmAnotacoes.ShowModal;
    FmAnotacoes.Destroy;
  end;
end;

end.

