object Dmod: TDmod
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 505
  Width = 853
  PixelsPerInch = 96
  object QrFields: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 428
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrMaster: TMySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      'te.ERua, te.ENumero, te.EBairro, te.ECEP, te.ECompl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrMasterENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
      Required = True
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
  object QrRecCountX: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 392
    Top = 247
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrUser: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 12
    Top = 95
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'senhas.Login'
      Size = 30
    end
  end
  object QrSB: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 232
    Top = 7
  end
  object QrSB2: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 51
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 232
    Top = 51
  end
  object QrSB3: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, Carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 204
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 232
    Top = 95
  end
  object QrDuplicIntX: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 159
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrDuplicStrX: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrInsLogX: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalY: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 272
    Top = 7
    object QrDataBalYData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrSenha: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 12
    Top = 239
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNTEACH.senhas.Numero'
    end
    object QrSenhaSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNTEACH.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNTEACH.senhas.Perfil'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNTEACH.senhas.Lk'
    end
  end
  object QrMaster2: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 8
    Top = 195
    object QrMaster2Em: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMaster2CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
      Required = True
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
      Required = True
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrMaster2MasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
    object QrMaster2SolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
    end
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMaster2SitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
    object QrMaster2Licenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
  end
  object QrProduto: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo, Controla'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 116
    Top = 51
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
  end
  object QrVendas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtossits'
      'WHERE Produto=:P0')
    Left = 88
    Top = 51
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVendasPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtossits.Pecas'
    end
    object QrVendasValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtossits.Total'
    end
  end
  object QrEntrada: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtoscits'
      'WHERE Produto=:P0'
      '')
    Left = 64
    Top = 51
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntradaPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtoscits.Pecas'
    end
    object QrEntradaValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtoscits.Total'
    end
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 448
    Top = 47
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object QrUpdL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 95
  end
  object QrUpdY: TMySQLQuery
    Database = MyDB
    Left = 64
    Top = 239
  end
  object QrLivreY: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo FROM livres')
    Left = 60
    Top = 191
    object QrLivreYCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrCountY: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 464
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocY: TMySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 464
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrAux: TMySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object MyDB: TMySQLDatabase
    DatabaseName = 'logidek'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    ConnectionCharacterSet = 'latin1'
    ConnectionCollation = 'latin1_swedish_ci'
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=logidek'
      'UID=root'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 12
    Top = 3
  end
  object MyLocDatabase: TMySQLDatabase
    DatabaseName = 'locijob'
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=locijob'
      'UID=root'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 156
    Top = 7
  end
  object QrSB4: TMySQLQuery
    Database = MyLocDatabase
    Left = 200
    Top = 143
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 232
    Top = 143
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAuxL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 51
  end
  object QrSoma1: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(TotalC*Tipo) TOTAL, '
      'SUM(Qtde*Tipo) Qtde '
      'FROM mov'
      'WHERE Produto=:P0')
    Left = 260
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoma1TOTAL: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TOTAL'
    end
    object QrSoma1Qtde: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Qtde'
    end
  end
  object QrEstoque: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM>=:P1'
      'GROUP BY pm.Tipo')
    Left = 428
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstoqueTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrPeriodoBal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 344
    Top = 324
    object QrPeriodoBalPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrMin: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Min(Periodo) Periodo'
      'FROM balancos')
    Left = 372
    Top = 325
    object QrMinPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'DBMBWET.balancos.Periodo'
    end
  end
  object QrBalancosIts: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,'
      'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G'
      'FROM balancosits'
      'WHERE Produto=:P0'
      'AND Periodo=:P1'
      '')
    Left = 400
    Top = 324
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMBWET.balancosits.EstqQ'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMBWET.balancosits.EstqV'
    end
    object QrBalancosItsEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrBalancosItsEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
  end
  object QrEstqperiodo: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM BETWEEN :P1 AND :P2'
      'GROUP BY pm.Tipo')
    Left = 456
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqperiodoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqperiodoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstqperiodoValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrTerminal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TMySQLQuery
    Database = MyLocDatabase
    Left = 132
    Top = 212
  end
  object QrNTI: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 212
  end
  object ZZDB: TMySQLDatabase
    DatabaseName = 'mysql'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=mysql'
      'UID=root'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 40
    Top = 3
  end
  object QrUserSets: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM usersets'
      'WHERE NomeForm=:P0'
      'AND Definicao=:P1')
    Left = 344
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrUserSetsDefinicao: TWideStringField
      FieldName = 'Definicao'
      Size = 255
    end
    object QrUserSetsAbilitado: TSmallintField
      FieldName = 'Abilitado'
    end
    object QrUserSetsVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrUserSetsNomeForm: TWideStringField
      FieldName = 'NomeForm'
      Size = 255
    end
  end
  object QrPerfis: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 372
    Top = 372
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 196
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyLocDatabase
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrTerminais: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
  end
  object QrUpdZ: TMySQLQuery
    Database = MyDB
    Left = 448
    Top = 3
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    
    Left = 40
    Top = 144
  end
  object QrBoss: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 432
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrBSit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 460
    Top = 372
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSenhas: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.Funcionario, se.IP_Default'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 488
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
  end
  object QrSSit: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 516
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 20
    Top = 447
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrControle: TMySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 28
    Top = 303
    object QrControleLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrControleDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrControleDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrControleUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrControleUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrControleContasU: TIntegerField
      FieldName = 'ContasU'
    end
    object QrControleEntDefAtr: TIntegerField
      FieldName = 'EntDefAtr'
    end
    object QrControleEntAtrCad: TIntegerField
      FieldName = 'EntAtrCad'
    end
    object QrControleEntAtrIts: TIntegerField
      FieldName = 'EntAtrIts'
    end
    object QrControleBalTopoNom: TIntegerField
      FieldName = 'BalTopoNom'
    end
    object QrControleBalTopoTit: TIntegerField
      FieldName = 'BalTopoTit'
    end
    object QrControleBalTopoPer: TIntegerField
      FieldName = 'BalTopoPer'
    end
    object QrControleNCMs: TIntegerField
      FieldName = 'NCMs'
    end
    object QrControleParamsNFs: TIntegerField
      FieldName = 'ParamsNFs'
    end
    object QrControleCambioCot: TIntegerField
      FieldName = 'CambioCot'
    end
    object QrControleCambioMda: TIntegerField
      FieldName = 'CambioMda'
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
    end
    object QrControleSecuritStr: TWideStringField
      FieldName = 'SecuritStr'
      Size = 32
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Size = 255
    end
    object QrControleEquiCom: TIntegerField
      FieldName = 'EquiCom'
    end
    object QrControleContasLnk: TIntegerField
      FieldName = 'ContasLnk'
    end
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
    end
    object QrControleEquiGru: TIntegerField
      FieldName = 'EquiGru'
    end
    object QrControleCNAB_Rem: TIntegerField
      FieldName = 'CNAB_Rem'
    end
    object QrControleCNAB_Rem_I: TIntegerField
      FieldName = 'CNAB_Rem_I'
    end
    object QrControlePreEmMsgIm: TIntegerField
      FieldName = 'PreEmMsgIm'
    end
    object QrControlePreEmMsg: TIntegerField
      FieldName = 'PreEmMsg'
    end
    object QrControlePreEmail: TIntegerField
      FieldName = 'PreEmail'
    end
    object QrControleContasMes: TIntegerField
      FieldName = 'ContasMes'
    end
    object QrControleMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrControleImpObs: TIntegerField
      FieldName = 'ImpObs'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
    end
    object QrControleVerSalTabs: TIntegerField
      FieldName = 'VerSalTabs'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
    end
    object QrControleCNAB_CaD: TIntegerField
      FieldName = 'CNAB_CaD'
    end
    object QrControleCNAB_CaG: TIntegerField
      FieldName = 'CNAB_CaG'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
    end
    object QrControleContasTrf: TIntegerField
      FieldName = 'ContasTrf'
    end
    object QrControleSomaIts: TIntegerField
      FieldName = 'SomaIts'
    end
    object QrControleContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Size = 255
    end
    object QrControleConfJanela: TIntegerField
      FieldName = 'ConfJanela'
    end
    object QrControleDataPesqAuto: TSmallintField
      FieldName = 'DataPesqAuto'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
    object QrControleAtividad: TSmallintField
      FieldName = 'Atividad'
    end
    object QrControleCidades: TSmallintField
      FieldName = 'Cidades'
    end
    object QrControleChequez: TSmallintField
      FieldName = 'Chequez'
    end
    object QrControlePaises: TSmallintField
      FieldName = 'Paises'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleCambiosData: TDateField
      FieldName = 'CambiosData'
    end
    object QrControleCambiosUsuario: TIntegerField
      FieldName = 'CambiosUsuario'
    end
    object QrControlereg10: TSmallintField
      FieldName = 'reg10'
    end
    object QrControlereg11: TSmallintField
      FieldName = 'reg11'
    end
    object QrControlereg50: TSmallintField
      FieldName = 'reg50'
    end
    object QrControlereg51: TSmallintField
      FieldName = 'reg51'
    end
    object QrControlereg53: TSmallintField
      FieldName = 'reg53'
    end
    object QrControlereg54: TSmallintField
      FieldName = 'reg54'
    end
    object QrControlereg56: TSmallintField
      FieldName = 'reg56'
    end
    object QrControlereg60: TSmallintField
      FieldName = 'reg60'
    end
    object QrControlereg75: TSmallintField
      FieldName = 'reg75'
    end
    object QrControlereg88: TSmallintField
      FieldName = 'reg88'
    end
    object QrControlereg90: TSmallintField
      FieldName = 'reg90'
    end
    object QrControleNumSerieNF: TSmallintField
      FieldName = 'NumSerieNF'
    end
    object QrControleSerieNF: TIntegerField
      FieldName = 'SerieNF'
    end
    object QrControleModeloNF: TSmallintField
      FieldName = 'ModeloNF'
    end
    object QrControleMyPagTip: TSmallintField
      FieldName = 'MyPagTip'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
    end
    object QrControleControlaNeg: TSmallintField
      FieldName = 'ControlaNeg'
    end
    object QrControleFamilias: TSmallintField
      FieldName = 'Familias'
    end
    object QrControleFamiliasIts: TSmallintField
      FieldName = 'FamiliasIts'
    end
    object QrControleAskNFOrca: TSmallintField
      FieldName = 'AskNFOrca'
    end
    object QrControlePreviewNF: TSmallintField
      FieldName = 'PreviewNF'
    end
    object QrControleOrcaRapido: TSmallintField
      FieldName = 'OrcaRapido'
    end
    object QrControleDistriDescoItens: TSmallintField
      FieldName = 'DistriDescoItens'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
    end
    object QrControleBalType: TSmallintField
      FieldName = 'BalType'
    end
    object QrControleOrcaOrdem: TSmallintField
      FieldName = 'OrcaOrdem'
    end
    object QrControleOrcaLinhas: TSmallintField
      FieldName = 'OrcaLinhas'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
    end
    object QrControleOrcaModelo: TSmallintField
      FieldName = 'OrcaModelo'
    end
    object QrControleOrcaRodaPos: TSmallintField
      FieldName = 'OrcaRodaPos'
    end
    object QrControleOrcaRodape: TSmallintField
      FieldName = 'OrcaRodape'
    end
    object QrControleOrcaCabecalho: TSmallintField
      FieldName = 'OrcaCabecalho'
    end
    object QrControleCoresRel: TSmallintField
      FieldName = 'CoresRel'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleCFiscalPadr: TWideStringField
      FieldName = 'CFiscalPadr'
    end
    object QrControleSitTribPadr: TWideStringField
      FieldName = 'SitTribPadr'
    end
    object QrControleCFOPPadr: TWideStringField
      FieldName = 'CFOPPadr'
    end
    object QrControleAvisosCxaEdit: TSmallintField
      FieldName = 'AvisosCxaEdit'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleChConfCab: TIntegerField
      FieldName = 'ChConfCab'
    end
    object QrControleImpDOS: TIntegerField
      FieldName = 'ImpDOS'
    end
    object QrControleUnidadePadrao: TIntegerField
      FieldName = 'UnidadePadrao'
    end
    object QrControleProdutosV: TIntegerField
      FieldName = 'ProdutosV'
    end
    object QrControleCartDespesas: TIntegerField
      FieldName = 'CartDespesas'
    end
    object QrControleReserva: TSmallintField
      FieldName = 'Reserva'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleVersao: TIntegerField
      FieldName = 'Versao'
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleErroHora: TIntegerField
      FieldName = 'ErroHora'
    end
    object QrControleSenhas: TIntegerField
      FieldName = 'Senhas'
    end
    object QrControleSenhasIts: TIntegerField
      FieldName = 'SenhasIts'
    end
    object QrControleSalarios: TIntegerField
      FieldName = 'Salarios'
    end
    object QrControleEntidades: TIntegerField
      FieldName = 'Entidades'
    end
    object QrControleEntiCtas: TIntegerField
      FieldName = 'EntiCtas'
    end
    object QrControleUFs: TIntegerField
      FieldName = 'UFs'
    end
    object QrControleListaECivil: TIntegerField
      FieldName = 'ListaECivil'
    end
    object QrControlePerfis: TIntegerField
      FieldName = 'Perfis'
    end
    object QrControleUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrControleContas: TIntegerField
      FieldName = 'Contas'
    end
    object QrControleCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object QrControleDepartamentos: TIntegerField
      FieldName = 'Departamentos'
    end
    object QrControleDividas: TIntegerField
      FieldName = 'Dividas'
    end
    object QrControleDividasIts: TIntegerField
      FieldName = 'DividasIts'
    end
    object QrControleDividasPgs: TIntegerField
      FieldName = 'DividasPgs'
    end
    object QrControleCarteiras: TIntegerField
      FieldName = 'Carteiras'
    end
    object QrControleCarteirasU: TIntegerField
      FieldName = 'CarteirasU'
    end
    object QrControleCartaG: TIntegerField
      FieldName = 'CartaG'
    end
    object QrControleCartas: TIntegerField
      FieldName = 'Cartas'
    end
    object QrControleConsignacao: TIntegerField
      FieldName = 'Consignacao'
    end
    object QrControleGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrControleSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrControleConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrControlePlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrControleInflacao: TIntegerField
      FieldName = 'Inflacao'
    end
    object QrControlekm: TIntegerField
      FieldName = 'km'
    end
    object QrControlekmMedia: TIntegerField
      FieldName = 'kmMedia'
    end
    object QrControlekmIts: TIntegerField
      FieldName = 'kmIts'
    end
    object QrControleFatura: TIntegerField
      FieldName = 'Fatura'
    end
    object QrControleLanctos: TLargeintField
      FieldName = 'Lanctos'
    end
    object QrControleLctoEndoss: TIntegerField
      FieldName = 'LctoEndoss'
    end
    object QrControleEntiGrupos: TIntegerField
      FieldName = 'EntiGrupos'
    end
    object QrControleEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrControleEntiCargos: TIntegerField
      FieldName = 'EntiCargos'
    end
    object QrControleEntiMail: TIntegerField
      FieldName = 'EntiMail'
    end
    object QrControleEntiTel: TIntegerField
      FieldName = 'EntiTel'
    end
    object QrControleEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
    end
    object QrControleAparencias: TIntegerField
      FieldName = 'Aparencias'
    end
    object QrControlePages: TIntegerField
      FieldName = 'Pages'
    end
    object QrControleMultiEtq: TIntegerField
      FieldName = 'MultiEtq'
    end
    object QrControleEntiTransp: TIntegerField
      FieldName = 'EntiTransp'
    end
    object QrControleEntiRespon: TIntegerField
      FieldName = 'EntiRespon'
    end
    object QrControleEntiCfgRel: TIntegerField
      FieldName = 'EntiCfgRel'
    end
    object QrControleExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrControleExcelGruImp: TIntegerField
      FieldName = 'ExcelGruImp'
    end
    object QrControleMediaCH: TIntegerField
      FieldName = 'MediaCH'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControleImprime: TIntegerField
      FieldName = 'Imprime'
    end
    object QrControleImprimeBand: TIntegerField
      FieldName = 'ImprimeBand'
    end
    object QrControleImprimeView: TIntegerField
      FieldName = 'ImprimeView'
    end
    object QrControleComProdPerc: TIntegerField
      FieldName = 'ComProdPerc'
    end
    object QrControleComProdEdit: TIntegerField
      FieldName = 'ComProdEdit'
    end
    object QrControleComServPerc: TIntegerField
      FieldName = 'ComServPerc'
    end
    object QrControleComServEdit: TIntegerField
      FieldName = 'ComServEdit'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
    end
    object QrControlePadrPlacaCar: TWideStringField
      FieldName = 'PadrPlacaCar'
      Size = 100
    end
    object QrControleServSMTP: TWideStringField
      FieldName = 'ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TWideStringField
      FieldName = 'NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TWideStringField
      FieldName = 'DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TWideStringField
      FieldName = 'MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TWideMemoField
      FieldName = 'CorpoMailOC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TWideStringField
      FieldName = 'ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TWideStringField
      FieldName = 'MailCCCega'
      Size = 80
    end
    object QrControleContVen: TIntegerField
      FieldName = 'ContVen'
    end
    object QrControleContCom: TIntegerField
      FieldName = 'ContCom'
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
    end
    object QrControleMoedaVal: TFloatField
      FieldName = 'MoedaVal'
    end
    object QrControleTela1: TIntegerField
      FieldName = 'Tela1'
    end
    object QrControleChamarPgtoServ: TIntegerField
      FieldName = 'ChamarPgtoServ'
    end
    object QrControleFormUsaTam: TIntegerField
      FieldName = 'FormUsaTam'
    end
    object QrControleFormHeight: TIntegerField
      FieldName = 'FormHeight'
    end
    object QrControleFormWidth: TIntegerField
      FieldName = 'FormWidth'
    end
    object QrControleFormPixEsq: TIntegerField
      FieldName = 'FormPixEsq'
    end
    object QrControleFormPixDir: TIntegerField
      FieldName = 'FormPixDir'
    end
    object QrControleFormPixTop: TIntegerField
      FieldName = 'FormPixTop'
    end
    object QrControleFormPixBot: TIntegerField
      FieldName = 'FormPixBot'
    end
    object QrControleFormFoAlt: TIntegerField
      FieldName = 'FormFoAlt'
    end
    object QrControleFormFoPro: TFloatField
      FieldName = 'FormFoPro'
    end
    object QrControleFormUsaPro: TIntegerField
      FieldName = 'FormUsaPro'
    end
    object QrControleFormSlides: TIntegerField
      FieldName = 'FormSlides'
    end
    object QrControleFormNeg: TIntegerField
      FieldName = 'FormNeg'
    end
    object QrControleFormIta: TIntegerField
      FieldName = 'FormIta'
    end
    object QrControleFormSub: TIntegerField
      FieldName = 'FormSub'
    end
    object QrControleFormExt: TIntegerField
      FieldName = 'FormExt'
    end
    object QrControleFormFundoTipo: TIntegerField
      FieldName = 'FormFundoTipo'
    end
    object QrControleFormFundoBMP: TWideStringField
      FieldName = 'FormFundoBMP'
      Size = 255
    end
    object QrControleServInterv: TIntegerField
      FieldName = 'ServInterv'
    end
    object QrControleServAntecip: TIntegerField
      FieldName = 'ServAntecip'
    end
    object QrControleAdiLancto: TIntegerField
      FieldName = 'AdiLancto'
    end
    object QrControleContaSal: TIntegerField
      FieldName = 'ContaSal'
    end
    object QrControleContaVal: TIntegerField
      FieldName = 'ContaVal'
    end
    object QrControlePronomeE: TWideStringField
      FieldName = 'PronomeE'
    end
    object QrControlePronomeM: TWideStringField
      FieldName = 'PronomeM'
    end
    object QrControlePronomeF: TWideStringField
      FieldName = 'PronomeF'
    end
    object QrControlePronomeA: TWideStringField
      FieldName = 'PronomeA'
    end
    object QrControleSaudacaoE: TWideStringField
      FieldName = 'SaudacaoE'
      Size = 50
    end
    object QrControleSaudacaoM: TWideStringField
      FieldName = 'SaudacaoM'
      Size = 50
    end
    object QrControleSaudacaoF: TWideStringField
      FieldName = 'SaudacaoF'
      Size = 50
    end
    object QrControleSaudacaoA: TWideStringField
      FieldName = 'SaudacaoA'
      Size = 50
    end
    object QrControleNiver: TSmallintField
      FieldName = 'Niver'
    end
    object QrControleNiverddA: TSmallintField
      FieldName = 'NiverddA'
    end
    object QrControleNiverddD: TSmallintField
      FieldName = 'NiverddD'
    end
    object QrControleLastPassD: TDateTimeField
      FieldName = 'LastPassD'
    end
    object QrControleMultiPass: TIntegerField
      FieldName = 'MultiPass'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrControleCustomFR3: TIntegerField
      FieldName = 'CustomFR3'
    end
    object QrControleanotacoes: TIntegerField
      FieldName = 'anotacoes'
    end
    object QrControleContasLnkIts: TIntegerField
      FieldName = 'ContasLnkIts'
    end
    object QrControleEventosCad: TSmallintField
      FieldName = 'EventosCad'
    end
    object QrControleImprimeEmpr: TIntegerField
      FieldName = 'ImprimeEmpr'
    end
    object QrControleCartTalCH: TIntegerField
      FieldName = 'CartTalCH'
    end
    object QrControleXcelCfgIts: TIntegerField
      FieldName = 'XcelCfgIts'
    end
    object QrControleXcelCfgCab: TIntegerField
      FieldName = 'XcelCfgCab'
    end
    object QrControlePlanRelCab: TIntegerField
      FieldName = 'PlanRelCab'
    end
    object QrControlePlanRelIts: TIntegerField
      FieldName = 'PlanRelIts'
    end
    object QrControleBLQ_TopoAvisoV: TIntegerField
      FieldName = 'BLQ_TopoAvisoV'
    end
    object QrControleBLQ_MEsqAvisoV: TIntegerField
      FieldName = 'BLQ_MEsqAvisoV'
    end
    object QrControleBLQ_AltuAvisoV: TIntegerField
      FieldName = 'BLQ_AltuAvisoV'
    end
    object QrControleBLQ_LargAvisoV: TIntegerField
      FieldName = 'BLQ_LargAvisoV'
    end
    object QrControleBLQ_TopoDestin: TIntegerField
      FieldName = 'BLQ_TopoDestin'
    end
    object QrControleBLQ_MEsqDestin: TIntegerField
      FieldName = 'BLQ_MEsqDestin'
    end
    object QrControleBLQ_AltuDestin: TIntegerField
      FieldName = 'BLQ_AltuDestin'
    end
    object QrControleBLQ_LargDestin: TIntegerField
      FieldName = 'BLQ_LargDestin'
    end
  end
  object QrUpd2: TMySQLQuery
    Database = MyDB
    Left = 204
    Top = 320
  end
  object QrSomaM: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM Lanctos')
    Left = 648
    Top = 55
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
  end
end
