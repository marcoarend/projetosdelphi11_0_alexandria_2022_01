unit Teste1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  // (*TUnFMX_DmkRemoteQuery.*)
  IdHTTP, REST.Client, REST.Types,
  Data.DB, TypInfo,
  UnGrl_DmkWeb, UnDMkEnums,
  // (*TUnFMX_DmkWeb.*)
  IdURI, Net.HttpClient;



type
  TFmTeste1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    VAR_TOKEN: String;
    //
    function  (*TUnFMX_DmkRemoteQuery.*)GetToken(const CNPJ, SQLType: String; const SQL:
              array of string; const Ambiente: Integer; var Resultado: String): Integer;
    function  (*TUnFMX_DmkRemoteQuery.*)GetProduto(const Token, Codigo: String;
              var Resultado: String): Integer;
    function  (*TUnFMX_DmkRemoteQuery.*)GetPessoa(const Token, Codigo: String;
              var Resultado: String): Integer;
{
    function  (*TUnFMX_DmkWeb.*)REST_URL_Post(HeaderNam, HeaderVal, ParamsNam,
              ParamsVal: array of string; URL: String; var StatusCode: Integer;
              ContentType: String = 'XML'): String;
}
  public
    { Public declarations }
  end;

var
  FmTeste1: TFmTeste1;

implementation

uses
  // (*TUnFMX_DmkRemoteQuery.*)
  UnFMX_DmkWeb, REST.Response.Adapter, System.JSON, UnGrl_Geral, UnGrl_Vars,
  UnGrl_Consts,
  // *TUnFMX_DmkWeb.*)
{$IF DEFINED(iOS) or DEFINED(ANDROID)}
Android_FMX_NetworkState,
{$ELSE}
WiniNet; //Windows,
{$ENDIF}
//UnGrl_Geral, UnGrl_Consts;

{$R *.dfm}

procedure TFmTeste1.BitBtn1Click(Sender: TObject);
var
  CNPJ, SQL, Resultado: String;
  Ambiente: Integer;
begin
  CNPJ     := '03143014000152';
  Ambiente := 1;
  SQL := 'stIns';
  GetToken(CNPJ, SQL, ['SELECT * FROM aaa'], Ambiente, Resultado);
end;

procedure TFmTeste1.BitBtn2Click(Sender: TObject);
var
  Token: String;
  Codigo, Resultado: String;
begin
{
  Token :=
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbnZpcm9ubWVudCI6ImFndWFkZWNvY29' +
    '0cmVpbm8iLCJzaWQiOiJVbnlEdVpyd3R6ZGdpbkgvbm1WYzNuaGJ2VFhEWFpBMCtGNVIyRnJ' +
    'YWmpMVkZTdG41WXFPUUNTSlMvckxsR0lOIiwianRpIjoiNTBhMmYxZTktMGIzNi00MjA0LWF' +
    'hZmItMzA0MDMxNTM0NjI5IiwiZXhwIjoxNTkxMjI0MTAwLCJpc3MiOiJodHRwOi8vdmlydHV' +
    'hbGFnZS5jb20uYnIifQ.ECyZA_qXRBOzgEXdzNdAffhfyyoeQ-AY5dyTi4VaFlo';
}
  Token := 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJBbWJpZW50ZSI6eyJDb2RpZ28i' +
  'OiJhZ3VhZGVjb2NvdHJlaW5vICAgICIsIlRpcG9EYXRhYmFzZSI6MSwiUHJvdmlkZXJOYW1lIj' +
  'pudWxsLCJEYXRhYmFzZSI6Ik9SQVRSTiIsIlVzZXJuYW1lIjoiVUFHVUFERUNPQ09UUkVJTk8i' +
  'LCJQYXNzd29yZCI6IlVBR1VBREVDT0NPVFJFSU5PIiwiSG9zdG5hbWUiOm51bGwsIkNvZGlnb0' +
  'VtcHJlc2EiOjEsIkNvZGlnb1VzdWFyaW8iOjk5OTk5MCwiQ29kaWdvVGVybWluYWwiOjB9fQ.g' +
  'dLIm39_7hgyDJf9fW_hjBMCbVN1ZlEDy9M7C6PMBg8';

  Token := 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJBbWJpZW50ZSI6eyJDb2RpZ28iOiJhZ3VhZGVjb2NvdHJlaW5vICAgICIsIlRpcG9EYXRhYmFzZSI6MSwiUHJvdmlkZXJOYW' +
  '1lIjpudWxsLCJEYXRhYmFzZSI6Ik9SQVRSTiIsIlVzZXJuYW1lIjoiVUFHVUFERUNPQ09UUkVJTk8iLCJQYXNzd29yZCI6IlVBR1VBREVDT0NPVFJFSU5PIiwiSG9zdG5hbWUiOm51bGwsIk' +
  'NvZGlnb0VtcHJlc2EiOjEsIkNvZGlnb1VzdWFyaW8iOjk5OTk5MCwiQ29kaWdvVGVybWluYWwiOjB9fQ.gdLIm39_7hgyDJf9fW_hjBMCbVN1ZlEDy9M7C6PMBg8';




  Codigo := '1';
  GetPessoa(Token, Codigo, Resultado);
  Grl_Geral.MB_Info(Resultado);
end;

function TFmTeste1.GetPessoa(const Token, Codigo: String;
  var Resultado: String): Integer;
var
  Url: String;
  //I,
  StatusCode: Integer;
begin
  Result    := 0;
  Resultado := '';

  if Token = EmptyStr then
  begin
    Grl_Geral.MB_Aviso('O par�metro "Token" n�o foi informado!');
    Exit;
  end;

  if Length(Codigo) = 0 then
  begin
    Grl_Geral.MB_Aviso('O par�metro "Codigo" n�o foi informado!');
    Exit;
  end;

{
  SQLStr := '';

  for I := Low(SQL) to High(SQL) do
  begin
    SQLStr := SQLStr + ' ' + SQL[I];
  end;
}

  //Url := 'https://treino.bhan.com.br:9443/api/v1/autorizacao/token';
  // V1


  {
  Url := 'https://www5.bhan.com.br:5443/api/Pessoa/Consultar';
  Resultado := FMX_dmkWeb.REST_URL_Post(['token'], [token], ['cd_Pessoa'], [Codigo], Url, StatusCode, 'json');
  }

  Url := 'https://www30.bhan.com.br:9443/api/v1/produto/classificacaoproduto';
  Resultado := FMX_dmkWeb.REST_URL_Post(['token'], [token], ['cd_Produto'], [Codigo], Url, StatusCode, 'json');

  {
  //V0
  //Url := 'https://www5.bhan.com.br:5443/api/Pessoa/Consultar';
  Url := 'https://www5.bhan.com.br:5443/api/Pessoa/ListarComFiltro';
  Resultado := FMX_dmkWeb.REST_URL_Post(['Token'], [Token], ['Cd_PessoaIni', 'Cd_PessoaFin'], ['1', '100'], Url, StatusCode, 'json');
  }

  Result := StatusCode;
  //
  if Result = 200 then
  Grl_Geral.MB_Aviso(Resultado);

end;

function TFmTeste1.GetProduto(const Token, Codigo: String;  var Resultado: String): Integer;
var
  Url: String;
  //I,
  StatusCode: Integer;
begin
  Result    := 0;
  Resultado := '';

  if Token = EmptyStr then
  begin
    Grl_Geral.MB_Aviso('O par�metro "Token" n�o foi informado!');
    Exit;
  end;

  if Length(Codigo) = 0 then
  begin
    Grl_Geral.MB_Aviso('O par�metro "Codigo" n�o foi informado!');
    Exit;
  end;

{
  SQLStr := '';

  for I := Low(SQL) to High(SQL) do
  begin
    SQLStr := SQLStr + ' ' + SQL[I];
  end;
}

  //Url := 'https://treino.bhan.com.br:9443/api/v1/autorizacao/token';
  Url := 'https://treino.bhan.com.br:9443/api/v1/produto/classificacaoproduto';
  //Resultado := FMX_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN], ['cnpj', 'sql',
  Resultado := FMX_dmkWeb.REST_URL_Post(['token'], [token], ['cdProduto'], [Codigo], Url, StatusCode, 'json');
  Result := StatusCode;
  //
  if Result = 200 then
  Grl_Geral.MB_Aviso(Resultado);

end;

function TFmTeste1.GetToken(const CNPJ, SQLType: String; const SQL: array of string;
  const Ambiente: Integer; var Resultado: String): Integer;
var
  Url, SQLStr: String;
  I, StatusCode: Integer;
begin
  Result    := 0;
  Resultado := '';

  if CNPJ = EmptyStr then
  begin
    Grl_Geral.MB_Aviso('O par�metro CNPJ n�o foi informado!');
    Exit;
  end;

  if Length(SQL) = 0 then
  begin
    Grl_Geral.MB_Aviso('O par�metro SQL n�o foi informado!');
    Exit;
  end;

  SQLStr := '';

  for I := Low(SQL) to High(SQL) do
  begin
    SQLStr := SQLStr + ' ' + SQL[I];
  end;

  //Url := (*CO_URL*)VAR_URL + 'sql/executa';
  //Url	:= 'https://www5.bhan.com.br:5443/api/Ambiente/ValidarAcesso/';
  //Url := 'https://treino.bhan.com.br:9443/api/v1'; 404 not found
  //Url := 'https://treino.bhan.com.br:9443/api/v1/ValidarAcesso/';
  //Url := 'https://www30.bhan.com.br:9443/api/v1/autorizacao/token';

  // V1
  {
  Url := 'https://treino.bhan.com.br:9443/api/v1/autorizacao/token';
  Resultado := FMX_dmkWeb.REST_URL_Post(['usuario', 'senha'], ['aguadecocotrn', '123456'], [], [], Url, StatusCode, 'json');
  }
  // V0
  //Url := 'https://treino.bhan.com.br:9443/api/Ambiente/ValidarAcesso/';
  //Url := 'https://www5.bhan.com.br:5443/api/Ambiente/ValidarAcesso/';
  Url := 'https://www30.bhan.com.br:9443/api/v1/autorizacao/token';

  Resultado := FMX_dmkWeb.REST_URL_Post([], [], ['usuario', 'senha'], ['tiaguadecoco', 'Reluz@2019ac'], Url, StatusCode, 'json');

  //Resultado := FMX_dmkWeb.REST_URL_Post(['token'], [CO_TOKEN], ['cnpj', 'sql',
  Result := StatusCode;
  //
  if Result = 200 then
  begin
    Grl_Geral.MB_Aviso(Resultado);
    //VAR_TOKEN := ParseJson0(Resultado);
  end;
end;

{
function TForm1.REST_URL_Post(HeaderNam, HeaderVal, ParamsNam,
  ParamsVal: array of string; URL: String; var StatusCode: Integer;
  ContentType: String): String;
var
  Conexao, Resp, i: Integer;
  Msg: String;
  RESTRequest: TRESTRequest;
  RESTClient: TRESTClient;
  RESTResponse: TRESTResponse;
  Param: TRESTRequestParameter;
begin
  StatusCode := 0;
  Result     := '';
  //
  if Length(ParamsNam) <> Length(ParamsVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros e de valores de par�metros devem ser os mesmos!');
    Exit;
  end;
  //
  if Length(HeaderNam) <> Length(HeaderVal) then
  begin
    Grl_Geral.MB_Erro('A quantidade de par�metros do cabe�alho e de valores de par�metros do cabe�alho devem ser os mesmos!');
    Exit;
  end;
  //
  Conexao := FMX_DmkWeb.VerificaConexaoWeb(Msg);
  //
  if Conexao <= 0 then
  begin
    Grl_Geral.MB_Aviso('Sincronismo abortado!' + sLineBreak +
      'Seu dispositivo n�o est� conectado na internet!');
    Exit;
  end;
  RESTRequest  := TRESTRequest.Create(nil);
  RESTClient   := TRESTClient.Create(URL);
  RESTResponse := TRESTResponse.Create(nil);
  //
  try
    ResetRESTComponentsToDefaults(RESTRequest, RESTResponse, RESTClient);
    //
    RESTClient.BaseURL     := URL;
    RESTClient.ContentType := ContentType;
    RESTRequest.Method     := TRESTRequestMethod.rmPOST;
    RESTRequest.Client     := RESTClient;
    RESTRequest.Response   := RESTResponse;
    //
    if Length(HeaderNam) > 0 then
    begin
      for i := Low(HeaderNam) to High(HeaderNam) do
      begin
        RESTRequest.Params.AddHeader(HeaderNam[i], HeaderVal[i]);
      end;
    end;
    //
    if Length(ParamsNam) > 0 then
    begin
      for i := Low(ParamsNam) to High(ParamsNam) do
      begin
        Param       := RESTRequest.Params.AddItem;
        Param.name  := UTF8Encode(ParamsNam[i]);
        Param.Value := UTF8Encode(ParamsVal[i]);
        Param.Kind  := TRESTRequestParameterKind.pkGETorPOST;
      end;
    end;
    //
    // Marco
    try
      RESTRequest.Execute;
    except
      //on E: ENetHTTPClientException do
      on E: Exception do
      begin
        StatusCode := -1;
        Result     := E.Message;
        Grl_Geral.MB_Erro('FMX-DmkWeb-RUP' + sLineBreak +
        'N�o foi poss�vel acessar o servidor!' + sLineBreak +
        'URL: ' + URL + sLineBreak +
        'HeaderNam ' + Grl_Geral.ATS(HeaderNam) + sLineBreak +
        //'HeaderVal ' + Grl_Geral.ATS(HeaderVal) + sLineBreak +  Cuidado! pode trer dados sigilosos como senha ou token!
        'ParamsNam ' + Grl_Geral.ATS(ParamsNam) + sLineBreak +
        //'ParamsVal ' + Grl_Geral.ATS(ParamsVal) + sLineBreak +  Cuidado! pode trer dados sigilosos como senha ou token!
        'Caso esteja usando wifi sem acesso � internet, tente habilitar somente os dados m�veis!'
        + sLineBreak +
        'Resposta: ' + RESTResponse.Content);
      end;
    end;
    if StatusCode = -1 then
      Exit;
    //
    StatusCode := RESTResponse.StatusCode;
    //
    Result := RESTResponse.Content;
    //
  finally
    RESTRequest.Free;
    RESTClient.Free;
    RESTResponse.Free;
  end;
end;
}

end.
