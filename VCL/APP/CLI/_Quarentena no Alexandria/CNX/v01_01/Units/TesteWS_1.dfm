object FmTesteWS_1: TFmTesteWS_1
  Left = 0
  Top = 0
  Caption = 'Teste WS_1'
  ClientHeight = 562
  ClientWidth = 1114
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 149
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitTop = 277
    ExplicitWidth = 81
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1114
    Height = 73
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 32
      Width = 23
      Height = 13
      Caption = 'URL:'
    end
    object BitBtn1: TBitBtn
      Left = 4
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Teste 1'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object EdWS: TEdit
      Left = 8
      Top = 48
      Width = 1089
      Height = 21
      TabOrder = 1
    end
    object BitBtn2: TBitBtn
      Left = 88
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Teste 2'
      TabOrder = 2
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 172
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Teste 3'
      TabOrder = 3
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 256
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Teste 4'
      TabOrder = 4
      OnClick = BitBtn4Click
    end
    object BitBtn5: TBitBtn
      Left = 340
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Teste 5'
      TabOrder = 5
      OnClick = BitBtn5Click
    end
    object RGUseWSDL: TRadioGroup
      Left = 424
      Top = 4
      Width = 313
      Height = 41
      Caption = ' Tipo de Web Service: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Indefinido'
        'SOAP'
        'WSDL')
      TabOrder = 6
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 73
    Width = 1114
    Height = 76
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 1112
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = 'Texto de Envio'
      ExplicitWidth = 72
    end
    object RETxtEnvio: TMemo
      Left = 1
      Top = 14
      Width = 1112
      Height = 61
      Align = alClient
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 152
    Width = 1114
    Height = 410
    Align = alClient
    TabOrder = 2
    object Label2: TLabel
      Left = 1
      Top = 1
      Width = 1112
      Height = 13
      Align = alTop
      Alignment = taCenter
      Caption = 'Texto Retornado'
      ExplicitWidth = 82
    end
    object RETxtRetorno: TMemo
      Left = 1
      Top = 14
      Width = 1112
      Height = 395
      Align = alClient
      TabOrder = 0
    end
  end
end
