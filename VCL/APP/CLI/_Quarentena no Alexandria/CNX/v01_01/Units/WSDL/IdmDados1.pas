// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados
//  >Import : https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados>0
// Encoding : utf-8
// Version  : 1.0
// (03/06/2020 18:31:56 - - $Rev: 90173 $)
// ************************************************************************ //

unit IdmDados1;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient, System.Types, Soap.XSBuiltIns, SOAPMidas;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]


  // ************************************************************************ //
  // Namespace : urn:udmDados-IdmDados
  // soapAction: urn:udmDados-IdmDados#requisicao
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // use       : encoded
  // binding   : IdmDadosbinding
  // service   : IdmDadosservice
  // port      : IdmDadosPort
  // URL       : https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados
  // ************************************************************************ //
  IdmDados = interface(IAppServerSOAP)
  ['{F5792C4F-2DA1-6DAF-A524-785B5D234485}']
    function  requisicao(const XML: string): string; stdcall;
  end;

function GetRequisicao(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IdmDados;


implementation
  uses System.SysUtils;

function GetRequisicao(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IdmDados;
const
  defWSDL = 'C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\CNX\v01_01\Units\WSDL\Requisicao.wsdl';
  //defURL  = 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  defURL  = 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
  defSvc  = 'IdmDadosservice';
  defPrt  = 'IdmDadosPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IdmDados);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  { IdmDados }
  InvRegistry.RegisterInterface(TypeInfo(IdmDados), 'urn:udmDados-IdmDados', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IdmDados), 'urn:udmDados-IdmDados#requisicao');

end.