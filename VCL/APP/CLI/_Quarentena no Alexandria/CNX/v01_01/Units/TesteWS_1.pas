﻿unit TesteWS_1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  //
  Vcl.ExtCtrls, Vcl.ComCtrls, Math, mySQLDbTables, dmkImage,
  UnDmkEnums, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt, XMLDoc, WinInet, SOAPConst,
  //consCad_v200, nfe_v400, consReciNFe_v400, consStatServ_v400, inutNFe_v400, consSitNFe_v400,
  UnProjGroup_Vars, UnGrl_Vars,
  IdmDados1,
  OleCtrls, SHDocVw, XMLIntf, SOAPHTTPClient;

type
  TEnumeracao = (enumProcessoEmissao,
                 enumtpEmis,
                 enumFinNFe,
                 enumTpNF,
                 enumOrigemMercadoria,
                 enumCSTICMS);
  TTipoConsumoWS = (tcwsNFeAutorizacao,
                    tcwsNfeConsultaProtocolo,
                    tcwsNfeInutilizacao,
                    tcwsNFeRetAutorizacao,
                    tcwsNfeStatusServico,
                    tcwsRecepcaoEvento,
                    tcwsNfeConsultaCadastro);

  TFmTesteWS_1 = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    RETxtEnvio: TMemo;
    Panel3: TPanel;
    Label2: TLabel;
    RETxtRetorno: TMemo;
    EdWS: TEdit;
    Label3: TLabel;
    Splitter1: TSplitter;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    RGUseWSDL: TRadioGroup;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraRio(Rio: THTTPRIO);
    procedure OnBeforePost(const HTTPReqResp: THTTPReqResp; Data: Pointer);
    function  Teste1(): String;
    function  Teste2(): String;
    function  Teste3(): String;
    function  Teste4(): String;
    function  Teste5(): String;
  public
    { Public declarations }
  end;

var
  FmTesteWS_1: TFmTesteWS_1;
  //
  FURL(*, FDadosTxt, SerieNF, NumeroNF, CDV, FAssinTxt*): String;
  //FLaAviso1, FLaAviso2: TLabel;
  //FFatID, FFatNum, FEmpresa, FOrdem, FGravaCampo: Integer;

implementation

uses
  dmkGeral;

{$R *.dfm}

procedure TFmTesteWS_1.BitBtn1Click(Sender: TObject);
var
  I: Integer;
  Texto: String;
begin
  RETxtRetorno.Text := 'Aguardando retorno...';
  RETxtRetorno.Text := Teste1();
  Texto := '';
  for I := 1 to RETxtRetorno.Lines.count do
  begin
    Texto := Texto + Trim(RETxtRetorno.Lines[I]);
  end;
  Geral.MB_Info(Texto);
end;

procedure TFmTesteWS_1.BitBtn2Click(Sender: TObject);
begin
  RETxtRetorno.Text := 'Aguardando retorno...';
  RETxtRetorno.Text := Teste2();
end;

procedure TFmTesteWS_1.BitBtn3Click(Sender: TObject);
begin
  RETxtRetorno.Text := 'Aguardando retorno 3 ...';
  RETxtRetorno.Text := Teste3();

end;

procedure TFmTesteWS_1.BitBtn4Click(Sender: TObject);
begin
  RETxtRetorno.Text := 'Aguardando retorno 4 ...';
  RETxtRetorno.Text := Teste4();
end;

procedure TFmTesteWS_1.BitBtn5Click(Sender: TObject);
begin
  RETxtRetorno.Text := 'Aguardando retorno 4 ...';
  RETxtRetorno.Text := Teste5();
end;

procedure TFmTesteWS_1.ConfiguraRio(Rio: THTTPRIO);
begin
  {
  if FConfiguracoes.WebServices.ProxyHost <> '' then
   begin
     Rio.HTTPWebNode.Proxy        := FConfiguracoes.WebServices.ProxyHost+':'+FConfiguracoes.WebServices.ProxyPort;
     Rio.HTTPWebNode.UserName     := FConfiguracoes.WebServices.ProxyUser;
     Rio.HTTPWebNode.Password     := FConfiguracoes.WebServices.ProxyPass;
   end;
  }
  Rio.HTTPWebNode.OnBeforePost := OnBeforePost;
end;


procedure TFmTesteWS_1.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
{
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
begin
  Store := CoStore.Create;
  //Repositórios de Certifcados da Máquina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na máquia

  i := 0;
  //loop de procura ao certificado requerido pelo número serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(EdSerialNumber.Text) then
     //se o número do serial for igual ao que queremos utilizar
      begin
        //carrega informações do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MensagemBox('Falha ao selecionar o certificado.', 'Aviso', MB_OK+MB_ICONWARNING);
        end;

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
}
begin
//
end;

function  TFmTesteWS_1.Teste1(): String;
{
function TFmNFeGeraXML_0400.WS_NFeStatusServico(UFServico: String; Ambiente,
  CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
}
const
  TipoConsumo = tcwsNfeStatusServico;
(*
  Teste =
  '<?xml version="1.0" encoding="utf-8"?><soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="ht' +
  'tp://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope"><soap12:Header><nfeCabecMsg xmlns="h' +
  'ttp://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico4"><cUF>41</cUF><versaoDados>4.00</versaoDados></nfeCabecMsg></soap1' +
  '2:Header><soap12:Body><nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeStatusServico4"><consStatServ xmlns="h' +
  'ttp://www.portalfiscal.inf.br/nfe" versao="4.00"><tpAmb>1</tpAmb><cUF>41</cUF><xServ>STATUS</xServ></consStatServ></nfeDado' +
  'sMsg></soap12:Body></soap12:Envelope><env:Envelope xmlns:env=''http://www.w3.org/2003/05/soap-envelope''><env:Body xmlns:en' +
  'v=''http://www.w3.org/2003/05/soap-envelope''><nfeResultMsg xmlns=''http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServic' +
  'o4''><retConsStatServ versao=''4.00'' xmlns=''http://www.portalfiscal.inf.br/nfe''><tpAmb>1</tpAmb><verAplic>PR-v4_4_9</ver' +
  'Aplic><cStat>107</cStat><xMotivo>Servico em Operacao</xMotivo><cUF>41</cUF><dhRecbto>2019-09-16T10:15:08-03:00</dhRecbto><t' +
  'Med>1</tMed></retConsStatServ></nfeResultMsg></env:Body></env:Envelope>';
*)
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  //Stream: TBytesStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  VerServ: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  (*87654321
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  *)
  //FURL := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  FURL := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  //87654321
  //
  (*87654321
  FDadosTxt := XML_ConsStatServ(Ambiente, CodigoUF);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;

  VersaoDados := Geral.FFT_Dot(DModG.QrPrmsEmpNFeNFeVerStaSer.Value, 2, siNegativo);



  VerServ := MyObjects.SelRadioGroup('Selecione a versão ', '', ['Auto', '1', '2', '3', '4'], 2);
  case VerServ of
    0:
    begin
      if Pos('NFESTATUSSERVICO4', UPPERCASE(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeStatusServico4';
        ProcedimentoRet := 'nfeResultMsg';
      end else
      if Pos('NFESTATUSSERVICO3', UPPERCASE(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeStatusServico3';
        ProcedimentoRet := 'nfeStatusServicoNFResult';
      end else
      if Pos('NFESTATUSSERVICO2', UPPERCASE(FURL)) > 0 then
      begin
        ProcedimentoEnv := 'NfeStatusServico2';
        ProcedimentoRet := 'nfeStatusServicoNF2Result';
      end else
      begin
        ProcedimentoEnv := 'NfeStatusServico';
        ProcedimentoRet := 'nfeResultMsg';
      end;
    end;
    1:
    begin
      ProcedimentoEnv := 'NfeStatusServico';
      ProcedimentoRet := 'nfeResultMsg';
    end;
    2:
    begin
      ProcedimentoEnv := 'NfeStatusServico2';
      ProcedimentoRet := 'nfeStatusServicoNF2Result';
    end;
    3:
    begin
      ProcedimentoEnv := 'NfeStatusServico3';
      ProcedimentoRet := 'nfeStatusServicoNFResult';
    end;
    4:
    begin
      ProcedimentoEnv := 'NfeStatusServico4';
        ProcedimentoRet := 'nfeResultMsg';
    end;
  end;
*)
//87654321

////////////////////////////////////////////////////////////////////////////////
///  Atributo Tipo Tamanho Descrição                                           //
///  cd_loginws * String 20 Usuário de acesso ao Web Service.                  //
///  cd_senhaws * String 20 Senha correspondente ao usuário.                   //
////////////////////////////////////////////////////////////////////////////////
///
/// 1.3 – Ações
/// Todas os elementos enviados ao Web Service possuem um atributo chamado acao, que define
/// qual operação o Web Service realizará (inclusão, alteração ou consulta).
/// Os valores que este atributo pode receber são inc, alt e con.
/// Observações:
///  Uma requisição ao Web Service deve ter somente um Login, mas pode ter vários
/// elementos. Exemplificando, pode-se consultar um cliente e um produto na mesma
/// chamada (requisição) ao Web Service;
/// - Todas as requisições devem ter o elemento loginws com as informações de usuário e
/// senha.
/// - O Login de acesso ao Web Service deve ser solicitado ao Departamento
/// Comercial da Virtual Age, através do e-mail criacao@virtualage.com.br.
/// Exemplo de requisição:
/// 1. <?xml version="1.0" encoding="utf-8" ?>
/// 2. <requisicao in_schema="F" >
/// 3. <loginws cd_loginws="usuarioExemplo" cd_senhaws="senhaExemplo" />
/// 4. <pessoa acao="con" cd_pessoa="100" />
/// 5. </requisicao>
///
/// 1. Linha que define a codificação do XML (opcional).
/// 2. Início da requisição.
/// 3. Identificação do usuário de acesso ao Web Service.
/// 4. Elemento (TAG) que será manipulado pelo Web Service. Neste caso, a requisição retornará
/// a pessoa que está cadastrada na base de dados com o código 100.
/// 5. Fim da requisição.
/// OBS: todas as requisições enviadas ao Web Service da Virtual Age devem,
/// impreterivelmente, seguir a estrutura exemplificada (linhas 2, 3 e 5 são obrigatórias
/// para todas as requisições). Os próximos exemplos que estarão neste manual não
/// repetirão a estrutura completa da requisição.
////////////////////////////////////////////////////////////////////////////////

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
(*
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsStatServ_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';
*)
  Texto := '';

  Texto := Texto + '<?xml version="1.0" encoding="utf-8" ?>';

{

Texto := Texto + '</operation>';
Texto := Texto + '   <operation name="requisicao">';
Texto := Texto + '     <soap:operation soapAction="urn:udmDados-IdmDados#requisicao" style="rpc"/>';
Texto := Texto + '     <input message="tns:requisicao7Request">';
Texto := Texto + '       <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:udmDados-IdmDados"/>';
}

  Texto := Texto + '<requisicao in_schema="F" >';
  //Texto := Texto + '<loginws cd_loginws="usuarioExemplo" cd_senhaws="senhaExemplo" />';
  Texto := Texto + '<loginws cd_loginws="aguadecocotrn" cd_senhaws="123456" />';
  //Texto := Texto + '<loginws cd_loginws="akjsfefeuk" cd_senhaws="u32647236" />';
  //Texto := Texto + '<pessoa acao="con" cd_pessoa="100" />';
  Texto := Texto + '</requisicao>';



{
Texto := Texto + '     </input>';
(*Texto := Texto + '     <output message="tns:requisicao7Response">';
Texto := Texto + '       <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:udmDados-IdmDados"/>';
Texto := Texto + '     </output>';*)
Texto := Texto + '   </operation>';
}

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     //ConfiguraReqResp( ReqResp );  somente para certificado digital????
     ReqResp.URL := FURL;
     //ReqResp.UseUTF8InHeader := True;

(*87654321
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
*)
//87654321
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
     ReqResp.SoapAction := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados/requisicao7';// + '/requisicao'; //'urn:udmDados-IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados#requisicao';
     //FURL := '
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
////////////////////////////////////////////////////////////////////////////////
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
////////////////////////////////////////////////////////////////////////////////

{
         StrStream := TStringStream.Create('');
         ReqResp.Execute(Acao.Text, StrStream);
         //StrStream.CopyFrom(Stream, 0);
}
////////////////////////////////////////////////////////////////////////////////



         //Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         Result := StrStream.DataString;
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Teste:' + sLineBreak +
                              //'- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Acao.Free;
    Stream.Free;
  end;
end;

function  TFmTesteWS_1.Teste2(): String;
const
  TipoConsumo = tcwsNfeStatusServico;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  //Stream: TBytesStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  VerServ: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  //FURL := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  FURL := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  //

////////////////////////////////////////////////////////////////////////////////
///  Atributo Tipo Tamanho Descrição                                           //
///  cd_loginws * String 20 Usuário de acesso ao Web Service.                  //
///  cd_senhaws * String 20 Senha correspondente ao usuário.                   //
////////////////////////////////////////////////////////////////////////////////
///
/// 1.3 – Ações
/// Todas os elementos enviados ao Web Service possuem um atributo chamado acao, que define
/// qual operação o Web Service realizará (inclusão, alteração ou consulta).
/// Os valores que este atributo pode receber são inc, alt e con.
/// Observações:
///  Uma requisição ao Web Service deve ter somente um Login, mas pode ter vários
/// elementos. Exemplificando, pode-se consultar um cliente e um produto na mesma
/// chamada (requisição) ao Web Service;
/// - Todas as requisições devem ter o elemento loginws com as informações de usuário e
/// senha.
/// - O Login de acesso ao Web Service deve ser solicitado ao Departamento
/// Comercial da Virtual Age, através do e-mail criacao@virtualage.com.br.
/// Exemplo de requisição:
/// 1. <?xml version="1.0" encoding="utf-8" ?>
/// 2. <requisicao in_schema="F" >
/// 3. <loginws cd_loginws="usuarioExemplo" cd_senhaws="senhaExemplo" />
/// 4. <pessoa acao="con" cd_pessoa="100" />
/// 5. </requisicao>
///
/// 1. Linha que define a codificação do XML (opcional).
/// 2. Início da requisição.
/// 3. Identificação do usuário de acesso ao Web Service.
/// 4. Elemento (TAG) que será manipulado pelo Web Service. Neste caso, a requisição retornará
/// a pessoa que está cadastrada na base de dados com o código 100.
/// 5. Fim da requisição.
/// OBS: todas as requisições enviadas ao Web Service da Virtual Age devem,
/// impreterivelmente, seguir a estrutura exemplificada (linhas 2, 3 e 5 são obrigatórias
/// para todas as requisições). Os próximos exemplos que estarão neste manual não
/// repetirão a estrutura completa da requisição.
////////////////////////////////////////////////////////////////////////////////

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
(*
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsStatServ_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';
*)
  Texto := '';

  Texto := Texto + '<?xml version="1.0" encoding="utf-8" ?>';
  Texto := Texto + ' <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" '+
                     'xmlns:ns1="urn:udmDados-IdmDados" ' +
                     'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
                     'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
                     'xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" ' +
                     'SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';

  Texto := Texto + ' <SOAP-ENV:Body>';
  Texto := Texto + ' <ns1:requisicao> ' +
                     '<XML xsi:type="xsd:string">' +
                     '<?xml version="1.0" encoding="utf-8"?> ' +
                     '<requisicao in_schema="F"> ' +
                     '<loginws cd_loginws="aguadecocotrn" cd_senhaws="123456" /> ' +
                     //'<pessoa acao="con" nr_cpfcnpj="87698595900" /> ' +
                     '<pessoa acao="con" nr_cpfcnpj="11111111111" /> ' +
                     '</requisicao></XML> ' +
                     '</ns1:requisicao>';

  Texto := Texto + ' </SOAP-ENV:Body> </SOAP-ENV:Envelope>';

{

Texto := Texto + '</operation>';
Texto := Texto + '   <operation name="requisicao">';
Texto := Texto + '     <soap:operation soapAction="urn:udmDados-IdmDados#requisicao" style="rpc"/>';
Texto := Texto + '     <input message="tns:requisicao7Request">';
Texto := Texto + '       <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:udmDados-IdmDados"/>';
}

{
  Texto := Texto + '<requisicao in_schema="F" >';
  //Texto := Texto + '<loginws cd_loginws="usuarioExemplo" cd_senhaws="senhaExemplo" />';
  Texto := Texto + '<loginws cd_loginws="aguadecocotrn" cd_senhaws="123456" />';
  //Texto := Texto + '<loginws cd_loginws="akjsfefeuk" cd_senhaws="u32647236" />';
  //Texto := Texto + '<pessoa acao="con" cd_pessoa="100" />';
  Texto := Texto + '</requisicao>';
}



{
Texto := Texto + '     </input>';
(*Texto := Texto + '     <output message="tns:requisicao7Response">';
Texto := Texto + '       <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:udmDados-IdmDados"/>';
Texto := Texto + '     </output>';*)
Texto := Texto + '   </operation>';
}

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     //ConfiguraReqResp( ReqResp );  somente para certificado digital????
     ReqResp.URL := FURL;
     //ReqResp.UseUTF8InHeader := True;

(*87654321
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
*)
//87654321
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
     ReqResp.SoapAction := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';// + '/requisicao'; //'urn:udmDados-IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados#requisicao';
     //FURL := '
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
////////////////////////////////////////////////////////////////////////////////
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
////////////////////////////////////////////////////////////////////////////////

{
         StrStream := TStringStream.Create('');
         ReqResp.Execute(Acao.Text, StrStream);
         //StrStream.CopyFrom(Stream, 0);
}
////////////////////////////////////////////////////////////////////////////////



         //Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         Result := StrStream.DataString;
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Teste:' + sLineBreak +
                              //'- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Acao.Free;
    Stream.Free;
  end;
end;

function  TFmTesteWS_1.Teste3(): String;
const
  TipoConsumo = tcwsNfeStatusServico;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  //Stream: TBytesStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  VerServ: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  //FURL := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  FURL := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  //

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  Texto := '';


{
  Texto := Texto + '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:udmDados-IdmDados">';
  Texto := Texto +  '<soapenv:Header/>';
  Texto := Texto +  '<soapenv:Body>';
  Texto := Texto +     '<urn:requisicao soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
  Texto := Texto +        '<XML xsi:type="xsd:string">';
  Texto := Texto +        '<requisicao in_schema="F">';

  Texto := Texto + '<loginws cd_loginws="lemonbasicstrn" cd_senhaws="475869"/>';

  Texto := Texto + '<pedido acao="inc" cd_cliente="1002" cd_representant="100000003" cd_condpgto="1" dt_pedido="2018-12-14" pr_desconto="0" tp_frete="1" tp_cobranca="0" tp_situacao="1" cd_operacao="001" nr_pedidocliente="TESTE001">';

  Texto := Texto + '<pedidoItem cd_barraprd="BOL01200201" qt_solicitada="1" vl_unitario="398.00" />';

  Texto := Texto + '</pedido>';

  Texto := Texto + '</requisicao>';
  Texto := Texto +       '</XML>';
  Texto := Texto +     '</urn:requisicao>';
  Texto := Texto +  'soapenv:Body>';
  Texto := Texto + '</soapenv:Envelope>';
}


  Texto := Texto +                   '<?xml version="1.0" encoding="utf-8"?> ';
  Texto := Texto +  '<requisicao in_schema="F">';
  Texto := Texto +    '<loginws cd_loginws="lemonbasicstrn" cd_senhaws="475869"/>';
  Texto := Texto +    '<pessoa acao="con">';
  Texto := Texto +      '<representante cd_pessoa="37811"/>';
  Texto := Texto +    '</pessoa>';
  Texto := Texto +  '</requisicao>';
  //Texto := Texto + '/XML>';


  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     //ConfiguraReqResp( ReqResp );  somente para certificado digital????
     ReqResp.URL := FURL;
     //ReqResp.UseUTF8InHeader := True;

(*87654321
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
*)
//87654321
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
     //ReqResp.SoapAction := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';// + '/requisicao'; //'urn:udmDados-IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados#requisicao';
     ReqResp.SoapAction := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';// + '/requisicao'; //'urn:udmDados-IdmDados#requisicao';
     //FURL := '
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
////////////////////////////////////////////////////////////////////////////////
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
////////////////////////////////////////////////////////////////////////////////

{
         StrStream := TStringStream.Create('');
         ReqResp.Execute(Acao.Text, StrStream);
         //StrStream.CopyFrom(Stream, 0);
}
////////////////////////////////////////////////////////////////////////////////



         //Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         Result := StrStream.DataString;
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Teste:' + sLineBreak +
                              //'- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmTesteWS_1.Teste4(): String;
const
  TipoConsumo = tcwsNfeStatusServico;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  //Stream: TBytesStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  VerServ: Integer;
begin
////////////////////////////////////////////////////////////////////////////////
// https://tdn.totvs.com/display/public/VAGE/DR+Layout+WebService+Soap+-+Pessoa
////////////////////////////////////////////////////////////////////////////////
  Screen.Cursor := crHourGlass;
  //
  //FURL := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  //FURL := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  //FURL := 'https://www30.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
  //

  if EdWS.Text <> '' then
    FURL := EdWS.Text
  else
    FURL := 'https://www30.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  Texto := '';


{
  Texto := Texto + '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:udmDados-IdmDados">';
  Texto := Texto +  '<soapenv:Header/>';
  Texto := Texto +  '<soapenv:Body>';
  Texto := Texto +     '<urn:requisicao soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
  Texto := Texto +        '<XML xsi:type="xsd:string">';
  Texto := Texto +        '<requisicao in_schema="F">';

  Texto := Texto + '<loginws cd_loginws="lemonbasicstrn" cd_senhaws="475869"/>';

  Texto := Texto + '<pedido acao="inc" cd_cliente="1002" cd_representant="100000003" cd_condpgto="1" dt_pedido="2018-12-14" pr_desconto="0" tp_frete="1" tp_cobranca="0" tp_situacao="1" cd_operacao="001" nr_pedidocliente="TESTE001">';

  Texto := Texto + '<pedidoItem cd_barraprd="BOL01200201" qt_solicitada="1" vl_unitario="398.00" />';

  Texto := Texto + '</pedido>';

  Texto := Texto + '</requisicao>';
  Texto := Texto +       '</XML>';
  Texto := Texto +     '</urn:requisicao>';
  Texto := Texto +  'soapenv:Body>';
  Texto := Texto + '</soapenv:Envelope>';
}


  if RETxtEnvio.Text <> '' then
    Texto := RETxtEnvio.Text
  else
  begin
    Texto := Texto + '<?xml version="1.0" encoding="UTF-8"?> ';
    Texto := Texto + '<soapenv:Envelope ';
    Texto := Texto +     'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
    Texto := Texto +     'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ';
    Texto := Texto +     'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ';
    Texto := Texto +     'xmlns:urn="urn:udmDados-IdmDados"> ';
    Texto := Texto +    '<soapenv:Header/> ';
    Texto := Texto +    '<soapenv:Body> ';
    Texto := Texto +       '<urn:requisicao soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"> ';
    Texto := Texto +        ' ';
    Texto := Texto +          '<XML xsi:type="xsd:string"> ';
    Texto := Texto +          ' ';
    Texto := Texto +          '<requisicao in_schema="F" in_versao="F"> ';
  //Texto := Texto +          '<loginws cd_loginws="{login}" cd_senhaws="{senha}" /> ';
    Texto := Texto +          '<loginws cd_loginws="aguadecocotrn" cd_senhaws="123456" /> ';
    Texto := Texto +          '<pessoa acao="con" cd_pessoa="100" /> ';
    Texto := Texto +          '</requisicao> ';
    Texto := Texto +          ' ';
    Texto := Texto +          '</XML> ';
    Texto := Texto +          ' ';
    Texto := Texto +       '</urn:requisicao> ';
    Texto := Texto +    '</soapenv:Body> ';
    Texto := Texto + '</soapenv:Envelope>';
  end;

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);

     //ConfiguraReqResp( ReqResp );  somente para certificado digital????
     ReqResp.URL := FURL;
     //ReqResp.UseUTF8InHeader := True;

(*87654321
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/' + ProcedimentoEnv;
*)
//87654321
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
     //ReqResp.SoapAction := 'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';// + '/requisicao'; //'urn:udmDados-IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados#requisicao';
     //ReqResp.SoapAction := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados#requisicao'; //'https://treino.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';// + '/requisicao'; //'urn:udmDados-IdmDados#requisicao';
     //FURL := '
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
////////////////////////////////////////////////////////////////////////////////
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
////////////////////////////////////////////////////////////////////////////////

{
         StrStream := TStringStream.Create('');
         ReqResp.Execute(Acao.Text, StrStream);
         //StrStream.CopyFrom(Stream, 0);
}
////////////////////////////////////////////////////////////////////////////////



         //Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         Result := StrStream.DataString;
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Teste:' + sLineBreak +
                              //'- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmTesteWS_1.Teste5: String;
var
  Res: WideString;
  IRequisicao: IdmDados;
  sAviso: String;
  Rio: THTTPRIO;
  XML, Arquivo: String;
  //
  Status, Codigo: Integer;
begin
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  //FURL := 'https://www.bhan.com.br/wbsStoreage/VirtualWsServer.exe/wsdl/IdmDados';
  if EdWS.Text <> '' then
    FURL := EdWS.Text
  else
    FURL := 'https://www30.bhan.com.br/wbsStoreage/VirtualWsServer.exe/soap/IdmDados';
  case RGUseWSDL .ItemIndex of
    1: IRequisicao := GetRequisicao(False, FURL, Rio);
    2: IRequisicao := GetRequisicao(True, FURL, Rio);
    else
    begin
      Geral.MB_Aviso('Informe o tipo de Web Service');
      Exit;
    end;
  end;

  //XML := MLAGeral.LoadFileToText(FPathXML);
  if RETxtEnvio.Text <> '' then
    XML := RETxtEnvio.Text
  else
  begin
    XML := '';
    XML := XML + '<?xml version="1.0" encoding="UTF-8"?> ';
  (*
    XML := XML + '<soapenv:Envelope ';
    XML := XML +     'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
    XML := XML +     'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ';
    XML := XML +     'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ';
    XML := XML +     'xmlns:urn="urn:udmDados-IdmDados"> ';
    XML := XML +    '<soapenv:Header/> ';
    XML := XML +    '<soapenv:Body> ';
    XML := XML +       '<urn:requisicao soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"> ';
    XML := XML +        ' ';
    XML := XML +          '<XML xsi:type="xsd:string"> ';
    XML := XML +          ' ';*)
    XML := XML +          '<requisicao in_schema="F" in_versao="F"> ';
  //XML := XML +          '<loginws cd_loginws="{login}" cd_senhaws="{senha}" /> ';
    XML := XML +          '<loginws cd_loginws="aguadecocotrn" cd_senhaws="123456" /> ';
    XML := XML +          '<pessoa acao="con" cd_pessoa="100" /> ';
    XML := XML +          '</requisicao> ';
  (*
    XML := XML +          ' ';
    XML := XML +          '</XML> ';
    XML := XML +          ' ';
    XML := XML +       '</urn:requisicao> ';
    XML := XML +    '</soapenv:Body> ';
    XML := XML + '</soapenv:Envelope>';*)

    if XML = '' then
    begin
      Geral.MB_Erro('O texto do XML está vazio!');
      Exit;
    end;
    EdWS.Text := FURL;
    RETxtEnvio.Text := XML;
  end;
  Res := IRequisicao.requisicao(XML);
  //
  RETxtRetorno.Text := Res;
  Result := Res;
{
  Arquivo := DmNFSe_0000.SalvaXML(NFSE_RPS_REC_LOT,
    UnNFSe_PF_0000.FormataRPSNumero(EdId.ValueVariant), Res, nil, False);
  //
  // Alterar no BD para enviado
  Status := DmNFSe_0000.stepLoteEnvEnviado();
  Codigo := EdId.ValueVariant;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsc', False, [
  'Status'], ['Codigo'], [Status], [Codigo], True);
  //
  // Verificar retorno (Sincrono)
  EnviarLoteRpsSincronoEnvio_Retorno(Arquivo);
}
end;

end.

