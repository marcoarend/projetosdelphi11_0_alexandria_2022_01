unit UnAppPF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, SHDocVw,
  UnInternalConsts2, ComCtrls, dmkGeral,
  mySQLDBTables, UnDmkEnums;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
               Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnBloqGerl_Jan;


{ TUnAppPF }

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query, Query2: TmySQLQuery): Boolean;
begin
  if (Servico = 'ArreFut') and (Query <> nil) and (Query2 <> nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, Query2);
    //
    Result := True;
  end else
  if (Servico = 'LctGer2') and (Query <> nil) and (Query2 = nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, nil);
    //
    Result := True;
  end else
    Result := True;
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

end.
