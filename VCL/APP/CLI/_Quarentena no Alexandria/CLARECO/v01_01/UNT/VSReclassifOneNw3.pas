unit VSReclassifOneNw3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkEditCB, dmkDBLookupComboBox, UnInternalConsts, ShellApi,
  UnProjGroup_Consts, mySQLDirectQuery;

type
  TFLsItens = array of array[0..4] of String;
  TFmVSReclassifOneNw3 = class(TForm)
    PanelOC: TPanel;
    QrVSPaRclCab: TmySQLQuery;
    DsVSPaRclCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    DsVSGerArtNew: TDataSource;
    QrVSPaRclCabNO_TIPO: TWideStringField;
    QrVSPaRclCabGraGruX: TIntegerField;
    QrVSPaRclCabNome: TWideStringField;
    QrVSPaRclCabTipoArea: TSmallintField;
    QrVSPaRclCabEmpresa: TIntegerField;
    QrVSPaRclCabMovimCod: TIntegerField;
    QrVSPaRclCabCodigo: TIntegerField;
    QrVSPaRclCabVSGerRcl: TIntegerField;
    QrVSPaRclCabLstPal01: TIntegerField;
    QrVSPaRclCabLstPal02: TIntegerField;
    QrVSPaRclCabLstPal03: TIntegerField;
    QrVSPaRclCabLstPal04: TIntegerField;
    QrVSPaRclCabLstPal05: TIntegerField;
    QrVSPaRclCabLstPal06: TIntegerField;
    QrVSPaRclCabLk: TIntegerField;
    QrVSPaRclCabDataCad: TDateField;
    QrVSPaRclCabDataAlt: TDateField;
    QrVSPaRclCabUserCad: TIntegerField;
    QrVSPaRclCabUserAlt: TIntegerField;
    QrVSPaRclCabAlterWeb: TSmallintField;
    QrVSPaRclCabAtivo: TSmallintField;
    QrVSPaRclCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaRclCabNO_EMPRESA: TWideStringField;
    QrAll: TmySQLQuery;
    QrAllControle: TLargeintField;
    QrAllPecas: TFloatField;
    QrAllAreaM2: TFloatField;
    QrAllAreaP2: TFloatField;
    DsAll: TDataSource;
    QrAllBox: TIntegerField;
    QrAllVSPaRclIts: TIntegerField;
    QrAllVSPallet: TIntegerField;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    PnBox: TPanel;
    Label2: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    PnJaClass: TPanel;
    Label15: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    DBEdJaFoi_PECA: TDBEdit;
    Panel9: TPanel;
    Label9: TLabel;
    DBEdJaFoi_AREA: TDBEdit;
    PnNaoClass: TPanel;
    Label19: TLabel;
    PnIntMei: TPanel;
    Label23: TLabel;
    DBEdSdoVrtPeca: TDBEdit;
    Panel15: TPanel;
    Label25: TLabel;
    DBEdSdoVrtArM2: TDBEdit;
    QrAllVMI_Sorc: TIntegerField;
    QrAllVMI_Dest: TIntegerField;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    QrVSPaRclCabCacCod: TIntegerField;
    PnExtras: TPanel;
    DBGItensACP: TdmkDBGridZTO;
    Panel43: TPanel;
    Panel44: TPanel;
    QrVSPaRclCabVSPallet: TIntegerField;
    QrItensACP: TmySQLQuery;
    DsItensACP: TDataSource;
    QrVSPallet0: TmySQLQuery;
    QrVSPallet0Codigo: TIntegerField;
    QrVSPallet0Nome: TWideStringField;
    QrVSPallet0Lk: TIntegerField;
    QrVSPallet0DataCad: TDateField;
    QrVSPallet0DataAlt: TDateField;
    QrVSPallet0UserCad: TIntegerField;
    QrVSPallet0UserAlt: TIntegerField;
    QrVSPallet0AlterWeb: TSmallintField;
    QrVSPallet0Ativo: TSmallintField;
    QrVSPallet0Empresa: TIntegerField;
    QrVSPallet0Status: TIntegerField;
    QrVSPallet0CliStat: TIntegerField;
    QrVSPallet0GraGruX: TIntegerField;
    QrVSPallet0NO_CLISTAT: TWideStringField;
    QrVSPallet0NO_PRD_TAM_COR: TWideStringField;
    QrVSPallet0NO_STATUS: TWideStringField;
    DsVSPallet0: TDataSource;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewNO_Pallet: TWideStringField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrItensACPAreaM2: TFloatField;
    QrItensACPCodigo: TIntegerField;
    QrItensACPControle: TLargeintField;
    RGFrmaIns: TRadioGroup;
    QrItensACPAreaP2: TFloatField;
    QrItensACPCacID: TIntegerField;
    PnInfoOC: TPanel;
    Label3: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label6: TLabel;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label17: TLabel;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    Label20: TLabel;
    DBEdit12: TDBEdit;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    QrItensACPVMI_Dest: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrSorces: TmySQLQuery;
    QrVsiDest: TmySQLQuery;
    QrVsiDestMovimTwn: TIntegerField;
    QrVsiDestMovimID: TIntegerField;
    QrVsiDestCodigo: TIntegerField;
    QrVsiDestControle: TIntegerField;
    QrVsiDestGraGruX: TIntegerField;
    QrVsiDestMovimCod: TIntegerField;
    QrVsiDestEmpresa: TIntegerField;
    QrVsiDestTerceiro: TIntegerField;
    QrSorcesPecas: TFloatField;
    QrSorcesAreaM2: TFloatField;
    QrVsiSorc: TmySQLQuery;
    QrVsiSorcGraGruX: TIntegerField;
    QrSorcesVMI_Sorc: TIntegerField;
    QrVsiSorcAreaM2: TFloatField;
    QrVsiSorcValorT: TFloatField;
    QrVsiSorcCodigo: TIntegerField;
    QrVsiSorcControle: TIntegerField;
    QrVsiSorcFicha: TIntegerField;
    QrVsiSorcSerieFch: TIntegerField;
    QrVsiSorcMarca: TWideStringField;
    QrSorcesAreaP2: TFloatField;
    QrSumSorc: TmySQLQuery;
    QrSumSorcPecas: TFloatField;
    QrSumSorcAreaM2: TFloatField;
    QrSumSorcAreaP2: TFloatField;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    PnResponsaveis: TPanel;
    Label70: TLabel;
    EdRevisor: TdmkEditCB;
    CBRevisor: TdmkDBLookupComboBox;
    Label71: TLabel;
    EdDigitador: TdmkEditCB;
    CBDigitador: TdmkDBLookupComboBox;
    QrVSPaRclCabVSGerRclA: TIntegerField;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    BtClassesGeradas: TBitBtn;
    Memo1: TMemo;
    PnDesnate: TPanel;
    Panel45: TPanel;
    EdDesnate: TdmkEdit;
    SbDesnate: TSpeedButton;
    Label77: TLabel;
    CkDesnate: TCheckBox;
    DBGrid1: TDBGrid;
    QrVSCacIts: TmySQLQuery;
    QrVSCacItsGraGruX: TIntegerField;
    QrVSCacItsPecas: TFloatField;
    QrVSCacItsAreaM2: TFloatField;
    QrVSCacItsAreaP2: TFloatField;
    QrVSCacItsGraGru1: TIntegerField;
    QrVSCacItsNO_PRD_TAM_COR: TWideStringField;
    QrVSCacItsPercM2: TFloatField;
    QrVSCacItsPercPc: TFloatField;
    QrVSCacItsMediaM2PC: TFloatField;
    QrVSCacItsAgrupaTudo: TFloatField;
    QrVSCacSum: TmySQLQuery;
    QrVSCacSumPecas: TFloatField;
    QrVSCacSumAreaM2: TFloatField;
    QrVSCacSumAreaP2: TFloatField;
    DsVSCacIts: TDataSource;
    Splitter1: TSplitter;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSPaRclCabVSMovIts: TIntegerField;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    DBEdit5: TDBEdit;
    Label84: TLabel;
    EdTempo: TEdit;
    Label85: TLabel;
    PnSubClass: TPanel;
    Label86: TLabel;
    EdSubClass: TdmkEdit;
    Label87: TLabel;
    DBEdit35: TDBEdit;
    QrAllSubClass: TWideStringField;
    PnEqualize: TPanel;
    PnCfgEqz: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    DBGNotaEqz: TDBGrid;
    Splitter2: TSplitter;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    PnNota: TPanel;
    DBEdNotaEqzM2: TDBEdit;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    CkNota: TCheckBox;
    AlteraitematualSubClasse1: TMenuItem;
    N3: TMenuItem;
    QrVSPallet0FatorInt: TFloatField;
    PMIMEI: TPopupMenu;
    ImprimirfluxodemovimentodoPallet2: TMenuItem;
    ImprimirfluxodemovimentodoPallet1: TMenuItem;
    N4: TMenuItem;
    ImprimirfluxodemovimentodoIMEI1: TMenuItem;
    QrPalIMEIs: TmySQLQuery;
    QrPalIMEIsControle: TIntegerField;
    QrSumT: TmySQLQuery;
    DsSumT: TDataSource;
    QrSumTSdoVrtPeca: TFloatField;
    QrSumTSdoVrtArM2: TFloatField;
    QrSumTJaFoi_PECA: TFloatField;
    QrSumTJaFoi_AREA: TFloatField;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    QrVSPaRclCabLstPal07: TIntegerField;
    QrVSPaRclCabLstPal08: TIntegerField;
    QrVSPaRclCabLstPal09: TIntegerField;
    QrVSPaRclCabLstPal10: TIntegerField;
    QrVSPaRclCabLstPal11: TIntegerField;
    QrVSPaRclCabLstPal12: TIntegerField;
    QrVSPaRclCabLstPal13: TIntegerField;
    QrVSPaRclCabLstPal14: TIntegerField;
    QrVSPaRclCabLstPal15: TIntegerField;
    DqAll: TmySQLDirectQuery;
    DqItens: TmySQLDirectQuery;
    DqSumPall: TmySQLDirectQuery;
    PnAll: TPanel;
    SGAll: TStringGrid;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    EdAll: TdmkEdit;
    EdArrAll: TdmkEdit;
    GPBoxes: TGridPanel;
    N5: TMenuItem;
    TesteInclusao1: TMenuItem;
    PnMenu: TPanel;
    BtEncerra: TBitBtn;
    BtReabre: TBitBtn;
    BtImprime: TBitBtn;
    EdItens: TEdit;
    Palletdobox01: TMenuItem;
    Palletdobox02: TMenuItem;
    Palletdobox03: TMenuItem;
    Palletdobox04: TMenuItem;
    Palletdobox05: TMenuItem;
    Palletdobox06: TMenuItem;
    Palletdobox07: TMenuItem;
    Palletdobox08: TMenuItem;
    Palletdobox09: TMenuItem;
    Palletdobox10: TMenuItem;
    Palletdobox11: TMenuItem;
    Palletdobox12: TMenuItem;
    Palletdobox13: TMenuItem;
    Palletdobox14: TMenuItem;
    Palletdobox15: TMenuItem;
    QrVSPallet0QtdPrevPc: TIntegerField;
    Aumentarboxesdisponveis1: TMenuItem;
    N6: TMenuItem;
    Edit1: TEdit;
    Label4: TLabel;
    Excluiitensselecionadonovo1: TMenuItem;
    QrVSGerArtNewClientMO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaRclCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaRclCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaRclCabBeforeClose(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure RGFrmaInsClick(Sender: TObject);
    procedure QrItensACPBeforeClose(DataSet: TDataSet);
    procedure QrItensACPAfterScroll(DataSet: TDataSet);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure QrItensACPAfterOpen(DataSet: TDataSet);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure SbDesnateClick(Sender: TObject);
    procedure QrVSCacItsCalcFields(DataSet: TDataSet);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSubClassExit(Sender: TObject);
    procedure CBRevisorClick(Sender: TObject);
    procedure CBDigitadorClick(Sender: TObject);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure AlteraitematualSubClasse1Click(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
    procedure QrVSPallet0AfterOpen(DataSet: TDataSet);
    procedure ImprimirfluxodemovimentodoPallet2Click(Sender: TObject);
    procedure ImprimirfluxodemovimentodoPallet1Click(Sender: TObject);
    procedure QrSumTCalcFields(DataSet: TDataSet);
    procedure EdAreaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TesteInclusao1Click(Sender: TObject);
    procedure EdPecasXXChange(Sender: TObject);
    procedure Aumentarboxesdisponveis1Click(Sender: TObject);
    procedure DBEdSdoVrtPecaChange(Sender: TObject);
    procedure Excluiitensselecionadonovo1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    //FCriando: Boolean;
    FDifTime: TDateTime;
    FLsAll: array of array[0..12] of String;
    //FMartelosCod: array of Integer;
    //FMartelosNom: array of String;
    //
    //
    FMaxPecas: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of Double;
    FSGItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TStringGrid;
    FLsItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TFLsItens;
    FEdPalletPecas, FEdPalletArea, FEdPalletMedia:
    array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TdmkEdit;
    FQrVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TmySQLQuery;
    FDsVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDataSource;
    FQrVSPalRclIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TmySQLQuery;
    FDsVSPalClaIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDataSource;
    FPnTxLx, FPnBox, FPnArt1N, FPnArt2N, FPnArt3N, FPnBxTot, FPnNumBx, FPnIDBox,
    FPnSubXx, FPnIMEICtrl, FPnIMEIImei, FPnPalletID, FPnPalletPecas,
    FPnPalletArea, FPnPalletMedia,
    FPnIntMei: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TPanel;
    FLaNumBx, FLaIMEICtrl, FLaIMEIImei, FLaPalletID, FLaPalletPecas,
    FLaPalletArea, FLaPalletMedia: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TLabel;
    FCkSubClass: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TCheckBox;
    FDBEdArtNome, FDBEdArtNoSta, FDBEdArtNoCli, FDBEdIMEICtrl, FDBEdIMEIImei,
    FDBEdPalletID: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDBEdit;
    FGBIMEI, FGBPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TGroupBox;
    FPalletDoBox: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TMenuItem;
    FPMItens: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TPopupMenu;
    FClasseVisivel: Boolean;
    //
    procedure AdicionaPallet(Box: Integer);
    procedure AtualizaInfoOC();
    procedure AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, RclAPalDst: Integer);
    function  BoxDeComp(Compo: TObject): Integer;
    procedure CarregaSGAll();
    procedure CarregaSGItens(Box: Integer);
    procedure CarregaSGSumPall(Box: Integer; Acao: TAcaoMath; Pecas,
              AreaM2, AreaP2: Double);
    function  ObtemNO_MARTELO(Martelo: Integer): String;

    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure ExcluiUltimoCouro();

    procedure InsereCouroArrAll(CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri,
              VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
              Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
              Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
              FatorIntDst: Double; Controle: Int64);
    procedure LiberaDigitacao();
    procedure LiberaDigitacaoManual(Libera: Boolean);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    function  ObtemDadosBox(const Box: Integer; var VSPaRclIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer; var ExigeSubClass:
              Boolean): Boolean;
    function  ObtemDadosCouroOrigem(var ClaAPalOri, RclAPalOri, VMI_Sorc,
              VSCacItsAOri: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet: TmySQLQuery): Boolean;
    //procedure RealinhaBoxes();
    procedure ReopenAll();
    procedure ReopenVSGerArtDst();
    procedure ReopenItens(Box, VSPaRclIts, VSPallet: Integer);
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure ReopenSumPal(Box, VSPallet: Integer);
    procedure ReopenItensAClassificarPallet();
    procedure TentarFocarEdArea();
    procedure VerificaAreaAutomatica();
    procedure VerificaBoxes();
    // VSMod ???
    procedure ReopenVSPallet(Tecla: Integer; QrVSPallet, QrVSPalRclIts:
              TmySQLQuery; Pallet: Integer);
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType; CampoSubClas: Boolean);
    procedure UpdDelCouroSelecionado3(SQLTipo: TSQLType; CampoSubClas: Boolean);
    function  ZeraEstoquePalletOrigem(): Boolean;
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenDesnate(Automatico: Boolean);
    procedure ReopenEqualize(Automatico: Boolean);
    function  SubstituiOC_Antigo(): Boolean;
    function  SubstituiOC_Novo(): Boolean;
    procedure ReconfiguraPaineisIntMei(QrVSPallet: TmySQLQuery; Panel: TPanel);
    procedure ImprimeFluxoDeMovimentoDoPallet();
    procedure OcultaMostraClasses();
  public
    { Public declarations }
    FCriando: Boolean;
    FBoxMax, FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    //
    function  BoxInBoxes(Box: Integer): Boolean;
    procedure DefineBoxes(Max: Integer);
    procedure ReopenVSPaRclCab();
    procedure InsereCouroAtual();
  end;

var
  FmVSReclassifOneNw3: TFmVSReclassifOneNw3;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects,
//VSClaArtPalAd3, VSRclArtPrpNew, VSRclArtPrpNw3, VSReclassPrePal
  MyDBCheck, (*UnVS_PF,*) AppListas, UnDmkProcFunc;

{$R *.dfm}
var
  FBoxes: array [1..VAR_CLA_ART_RIB_MAX_BOX_15] of Boolean;

procedure TFmVSReclassifOneNw3.AdicionaPallet(Box: Integer);
var
  I: Integer;
begin
  {[***VerSePrecisa***]
  if DBCheck.CriaFm(TFmVSClaArtPalAd3, FmVSClaArtPalAd3, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAd3.FMovimIDGer              := emidReclasVSUni;
    FmVSClaArtPalAd3.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAd3.FClientMO                := QrVSGerArtNewClientMO.Value;
    FmVSClaArtPalAd3.FFornecedor              := Trunc(QrVSGerArtNewTerceiro.Value);
    FmVSClaArtPalAd3.FVSMulFrnCab             := QrVSGerArtNewVSMulFrnCab.Value;
    FmVSClaArtPalAd3.FMovimID                 := FMovimID;
    FmVSClaArtPalAd3.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAd3.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAd3.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAd3.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAd3.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAd3.EdCodigo.ValueVariant    := QrVSPaRclCabCodigo.Value;
    FmVSClaArtPalAd3.EdSrcPallet.ValueVariant := QrVSPaRclCabVSPallet.Value;
    FmVSClaArtPalAd3.FExigeSrcPallet          := True;
    FmVSClaArtPalAd3.EdMovimCod.ValueVariant  := QrVSPaRclCabMovimCod.Value;
    FmVSClaArtPalAd3.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAd3.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAd3.FBox := Box;
    FmVSClaArtPalAd3.PnBox.Caption := Geral.FF0(Box);
    //
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
      FmVSClaArtPalAd3.FPal[I] := QrVSPaRclCab.FieldByName('LstPal' +
      Geral.FFF(I, 2)).AsInteger;
    //
    //
    VS_PF.ReopenVSPallet(FmVSClaArtPalAd3.QrVSPallet,
      FmVSClaArtPalAd3.FEmpresa, FmVSClaArtPalAd3.FClientMO, 0, '', []);
    //
    FmVSClaArtPalAd3.ShowModal;
    FmVSClaArtPalAd3.Destroy;
    //
    ReopenVSPaRclCab();
  end;
}
end;

procedure TFmVSReclassifOneNw3.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSReclassifOneNw3.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd, False);
end;

procedure TFmVSReclassifOneNw3.AlteraitematualSubClasse1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd, True);
end;

procedure TFmVSReclassifOneNw3.AtualizaInfoOC();
var
  AreaT: Double;
begin
  {[***VerSePrecisa***]
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT Controle, VSPaRclIts, VSPallet, Pecas, ',
  'AreaM2, AreaP2, VMI_Sorc, VMI_Dest, Box, SubClass ',
  'FROM vscacitsa ',
  //'WHERE Codigo=' + Geral.FF0(FCodigo),
  'WHERE CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY Controle DESC ',
  '']);
  VS_PF.ReopenQrySaldoPallet_Visual(QrSumT, QrVSPaRclCabEmpresa.Value,
  QrVSPaRclCabVSPallet.Value, QrVSPaRclCabVSMovIts.Value);
  //
  //
  if CkDesnate.Checked then
    ReopenDesnate(True);
  if CkEqualize.Checked then
    ReopenEqualize(True);
}end;

procedure TFmVSReclassifOneNw3.AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, RclAPalDst:
  Integer);
var
  Controle: Integer;
begin
  if ClaAPalOri <> 0 then
  begin
    Controle   := ClaAPalOri;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
    'RclAPalDst'], ['Controle'], [
    RclAPalDst], [Controle], False);
  end;
  //
  if RclAPalOri <> 0 then
  begin
    Controle   := RclAPalOri;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
    'RclAPalDst'], ['Controle'], [
    RclAPalDst], [Controle], False);
  end;
end;

procedure TFmVSReclassifOneNw3.Aumentarboxesdisponveis1Click(Sender: TObject);
var
  I, Boxes, BoxIni, BoxFim: Integer;
begin
  if FBoxMax = VAR_CLA_ART_RIB_MAX_BOX_15 then
  begin
    Geral.MB_Info('A quantidade m�xima de boxes j� foi atingida!');
  end else
  begin
    Boxes := MyObjects.SelRadioGroup('Aumento de disponibilidade de Boxes',
    'Selecione a quantidade', [
    '06', '07', '08', '09', '10', '11', '12', '13', '14', '15'], 5, FBoxMax - 5);
    Boxes := Boxes + 6;
    //
    if Boxes > FBoxMax then
    begin
      BoxIni := FBoxMax + 1;
      BoxFim := Boxes;
      for I := 1 to FBoxMax do
      begin
        //FPnTxLx[I].Destroy;
        FPnTxLx[I].Free;
      end;
      DefineBoxes(BoxFim);
      ReopenVSPaRclCab();
      if not Mostrartodosboxes1.Checked then
        Mostrartodosboxes1Click(Mostrartodosboxes1);
    end;
  end;
end;

function TFmVSReclassifOneNw3.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  CompName := Copy(CompName, Length(CompName) -1);

  Result := Geral.IMV(CompName);
end;

function TFmVSReclassifOneNw3.BoxInBoxes(Box: Integer): Boolean;
begin
  if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX_15) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSReclassifOneNw3.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSReclassifOneNw3.BtImprimeClick(Sender: TObject);
begin
  {[***VerSePrecisa***]
  VS_PF.MostraFormVSMovImp(False, nil, nil, 4);
}
end;

procedure TFmVSReclassifOneNw3.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaRclCab();
end;

procedure TFmVSReclassifOneNw3.CarregaSGAll();
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsAll);
  SGAll.RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    SGAll.Cells[00, J] := IntToStr(K - J + 1);
    SGAll.Cells[01, J] := FLsAll[I, 01];   // Box
    SGAll.Cells[02, J] := FLsAll[I, 02];   // SubClass
    SGAll.Cells[03, J] := FLsAll[I, 03];   // AreaM2
    SGAll.Cells[04, J] := FLsAll[I, 04];   // NO_MARTELO
    SGAll.Cells[05, J] := FLsAll[I, 05];   // VMI_Dest
    SGAll.Cells[06, J] := FLsAll[I, 06];   // Controle
    SGAll.Cells[07, J] := FLsAll[I, 07];   // VSPaRclIts
    SGAll.Cells[08, J] := FLsAll[I, 08];   // VSPallet
    SGAll.Cells[09, J] := FLsAll[I, 09];   // Pecas
    SGAll.Cells[10, J] := FLsAll[I, 10];   // AreaP2
    SGAll.Cells[11, J] := FLsAll[I, 11];   // VMI_Sorc
    SGAll.Cells[12, J] := FLsAll[I, 12];   // VMI_Dest
  end;
end;

procedure TFmVSReclassifOneNw3.CarregaSGItens(Box: Integer);
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsItens[Box]);
  FSGItens[Box].RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    FSGItens[Box].Cells[00, J] := IntToStr(K - J + 1);
    case QrVSPaRclCabTipoArea.Value of
      0: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 03];
      1: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 04];
      else FSGItens[Box].Cells[01, J] := '?.??';
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.CarregaSGSumPall(Box: Integer; Acao: TAcaoMath;
  Pecas, AreaM2, AreaP2: Double);
var
  Area, P, A, M: Double;
begin
  if QrVSPaRclCabTipoArea.Value = 0 then
    Area := AreaM2
  else
    Area := AreaP2;
  //
  case Acao of
    amathZera:
    begin
      P := 0;
      A := 0;
    end;
    amathSubtrai:
    begin
      P := FEdPalletPecas[Box].ValueVariant - Pecas;
      A := FEdPalletArea[Box].ValueVariant - Area;
    end;
    amathSoma:
    begin
      P := FEdPalletPecas[Box].ValueVariant + Pecas;
      A := FEdPalletArea[Box].ValueVariant + Area;
    end;
    amathSubstitui:
    begin
      P := Pecas;
      A := Area;
    end;
  end;
  if P = 0 then
    M := 0
  else
    M := A / P;
  //
  FEdPalletPecas[Box].ValueVariant := P;
  FEdPalletArea[Box].ValueVariant  := A;
  FEdPalletMedia[Box].ValueVariant := M;
end;

procedure TFmVSReclassifOneNw3.CBDigitadorClick(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CBRevisorClick(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSReclassifOneNw3.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSReclassifOneNw3.DBEdSdoVrtPecaChange(Sender: TObject);
var
  CorTexto, CorFundo: TColor;
begin
  CorTexto := clWindowText;
  CorFundo := clWindow;
  if (QrSumT.State <> dsInactive) and (QrSumTSdoVrtPeca.Value = 0) then
  begin
    CorTexto := clYellow;
    CorFundo := clBlack;
  end else
  if (QrSumT.State <> dsInactive) and (QrSumTSdoVrtPeca.Value < 0) then
  begin
    CorTexto := clRed;
    CorFundo := clYellow;
  end;
  DBEdSdoVrtPeca.Color := CorFundo;
  DBEdSdoVrtPeca.Font.Color := CorTexto;
  DBEdSdoVrtArM2.Color := CorFundo;
  DBEdSdoVrtArM2.Font.Color := CorTexto;
  EdArea.Color := CorFundo;
  EdArea.Font.Color := CorTexto;
  EdBox.Color := CorFundo;
  EdBox.Font.Color := CorTexto;
end;

procedure TFmVSReclassifOneNw3.DefineBoxes(Max: Integer);
var
  I, Col, Row: Integer;
begin
  {[***VerSePrecisa***]
  GPBoxes.Visible := False;
  try
    if FBoxMax < Max then
      FBoxMax := Max;
    //
    GPBoxes.ColumnCollection.BeginUpdate;
    GPBoxes.ColumnCollection.Clear;
    GPBoxes.RowCollection.BeginUpdate;
    GPBoxes.RowCollection.Clear;
    case FBoxMax of
      00..06: begin Col := 3; Row := 2; end;
      07..09: begin Col := 3; Row := 3; end;
      10..12: begin Col := 4; Row := 3; end;
      13..15: begin Col := 5; Row := 3; end;
    end;
    for I := 0 to Col - 1 do
      GPBoxes.ColumnCollection.Add;
    for I := 0 to Row - 1 do
      GPBoxes.RowCollection.Add;
    //
    GPBoxes.ColumnCollection.EndUpdate;
    GPBoxes.RowCollection.EndUpdate;
    for I := 1 to FBoxMax do
    begin
      VS_PF.CriaBoxEmGridPanel(I, Self, GPBoxes, FPnTxLx, FPnBox, FPnArt1N,
      FPnArt2N, FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei,
      FPnIMEICtrl, FPnPalletMedia, FPnPalletArea, FPnPalletPecas, FPnPalletID,
      FPnIntMei, FLaNumBx, FLaIMEIImei, FLaIMEICtrl, FLaPalletMedia, FLaPalletArea,
      FLaPalletPecas, FLaPalletID, FCkSubClass, FDBEdArtNome, FDBEdArtNoCli,
      FDBEdArtNoSta, FDBEdIMEIImei, FDBEdIMEICtrl, FDBEdPalletID, FGBIMEI,
      FGBPallet, FEdPalletMedia, FEdPalletArea, FEdPalletPecas, FSGItens,
      FPMItens);
      FEdPalletPecas[I].OnChange := EdPecasXXChange;
      //
      FBoxes[I] := False;
      //
      FQrVSPallet[I] := TmySQLQuery.Create(Dmod);
      FDsVSPallet[I] := TDataSource.Create(Self);
      FDsVSPallet[I].DataSet := FQrVSPallet[I];
      FDBEdPalletID[I].DataSource := FDsVSPallet[I];
      FDBEdArtNome[I].DataSource  := FDsVSPallet[I];
      FDBEdArtNoCli[I].DataSource := FDsVSPallet[I];
      FDBEdArtNoSta[I].DataSource := FDsVSPallet[I];
      //
      FQrVSPalRclIts[I] := TmySQLQuery.Create(Dmod);
      FDsVSPalClaIts[I] := TDataSource.Create(Self);
      FDsVSPalClaIts[I].DataSet := FQrVSPalRclIts[I];
      FDBEdIMEICtrl[I].DataSource := FDsVSPalClaIts[I];
      FDBEdIMEIImei[I].DataSource := FDsVSPalClaIts[I];
      //
    end;
    //for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15
    FPalletDoBox[01] := PalletDoBox01;
    FPalletDoBox[02] := PalletDoBox02;
    FPalletDoBox[03] := PalletDoBox03;
    FPalletDoBox[04] := PalletDoBox04;
    FPalletDoBox[05] := PalletDoBox05;
    FPalletDoBox[06] := PalletDoBox06;
    FPalletDoBox[07] := PalletDoBox07;
    FPalletDoBox[08] := PalletDoBox08;
    FPalletDoBox[09] := PalletDoBox09;
    FPalletDoBox[10] := PalletDoBox10;
    FPalletDoBox[11] := PalletDoBox11;
    FPalletDoBox[12] := PalletDoBox12;
    FPalletDoBox[13] := PalletDoBox13;
    FPalletDoBox[14] := PalletDoBox14;
    FPalletDoBox[15] := PalletDoBox15;
  finally
    GPBoxes.Visible := True;
  end;
  //
}
end;

procedure TFmVSReclassifOneNw3.BtClassesGeradasClick(Sender: TObject);
begin
  {[***VerSePrecisa***]
  VS_PF.ImprimeReclassOC(QrVSPaRclCabCodigo.Value, QrVSPaRclCabCacCod.Value);
}
end;

procedure TFmVSReclassifOneNw3.EdAreaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {[***VerSePrecisa***]
  if Key = VK_F4 then
  begin
    VS_PF.MostraFormVSDivCouMeio(emidReclasVSUni, FBoxes, LaTipoArea.Caption);
  end;
}end;

procedure TFmVSReclassifOneNw3.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  if (FBoxMax > 9) and (Length(EdBox.Text) < 2) then
    Exit;
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
    EdSubClass.SetFocus
  else
  if (EdBox.Text = '-') then
    ExcluiUltimoCouro();
end;

procedure TFmVSReclassifOneNw3.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSReclassifOneNw3.EdPecasXXChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := Geral.IMV(Copy(TdmkEdit(Sender).Name, 8));
  if TdmkEdit(Sender).ValueVariant >= FMaxPecas[Box] then
  begin
    TdmkEdit(Sender).Font.Color := clRed;
    TdmkEdit(Sender).Color := clYellow;
  end else
  begin
    TdmkEdit(Sender).Font.Color := clBlue;
    TdmkEdit(Sender).Color := clWindow;
  end;
end;

procedure TFmVSReclassifOneNw3.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSReclassifOneNw3.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSReclassifOneNw3.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSReclassifOneNw3.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
  ExigeSubClass: Boolean;
begin
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
      //
      InsereCouroAtual();
    end;
  end;
end;

function TFmVSReclassifOneNw3.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  ExigeSubClass: Boolean;
  var
    ReabreVSPaRclCab: Boolean;
begin
  {[***VerSePrecisa***]
  Result := False;
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass) then
    Exit;
  Result := VS_PF.EncerraPalletReclassificacaoNew(QrVSPaRclCabCacCod.Value,
  QrVSPaRclCabCodigo.Value, QrVSPaRclCabVSPallet.Value, Box, VSPaClaIts,
  VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, EncerrandoTodos, Pergunta,
  ReabreVSPaRclCab);
  if ReabreVSPaRclCab then
      ReopenVSPaRclCab();
}end;

procedure TFmVSReclassifOneNw3.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaRclCab();
  end;
end;

procedure TFmVSReclassifOneNw3.EstaOCOrdemdeclassificao1Click(Sender: TObject);
  {[***VerSePrecisa***]
  var
  DtHrFimCla: String;
  //
  function DefPalRclIts(Qry: TmySQLQuery): Integer;
  begin
    if (Qry <> nil) and (Qry.State <> dsInactive) then
      Result := Qry.FieldByName('Controle').AsInteger
    else
      Result := 0;
  end;
  procedure EncerraBox(Box, PalRclIts: Integer);
  var
    VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
    ExigeSubClass: Boolean;
  begin
    if PalRclIts <> 0 then
    begin
      if ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
      ExigeSubClass) then
        VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
          QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      // Encerra IME-I
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
      'DtHrFim'], ['Controle'], [DtHrFimCla], [PalRclIts], True) then
        VS_PF.AtualizaStatPall(VSPallet);
    end;
  end;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
  LstPal07 = 0;
  LstPal08 = 0;
  LstPal09 = 0;
  LstPal10 = 0;
  LstPal11 = 0;
  LstPal12 = 0;
  LstPal13 = 0;
  LstPal14 = 0;
  LstPal15 = 0;
var
  VSPalRclIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of Integer;
  Codigo, I, Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
  Fecha: Boolean;
}
begin
  {[***VerSePrecisa***]
  if Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES then
  begin
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
    begin
      if FQrVSPalRclIts[I] <> nil then
      begin
        VSPalRclIts[I] := 0;
        if FQrVSPalRclIts[I].State <> dsInactive then
        //try
          VSPalRclIts[I] := FQrVSPalRclIts[I].FieldByName('Controle').AsInteger
        //except
          //
        //end;
      end
      else
        VSPalRclIts[I] := 0;
    end;
    //
    ZeraEstoquePalletOrigem();
     //
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    //Codigo     := QrVSGerArtNewCodigo.Value;
    Codigo := QrVSPaRclCabVSGerRclA.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerrcla', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaRclCabCodigo.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06',
      'LstPal07', 'LstPal08', 'LstPal09',
      'LstPal10', 'LstPal11', 'LstPal12',
      'LstPal13', 'LstPal14', 'LstPal15'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06,
      LstPal07, LstPal08, LstPal08,
      LstPal10, LstPal11, LstPal12,
      LstPal13, LstPal14, LstPal15
      ], [Codigo], True) then
      begin
        Fecha := not SubstituiOC_Novo();
        //
        for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
          EncerraBox(I, VSPalRclIts[I]);
        //
        if Fecha then
          Close
        else
          ReopenVSPaRclCab();
      end;
    end;
  end;
}
end;

procedure TFmVSReclassifOneNw3.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel, False);
end;

procedure TFmVSReclassifOneNw3.Excluiitensselecionadonovo1Click(
  Sender: TObject);
begin
  UpdDelCouroSelecionado3(stDel, False);
end;

procedure TFmVSReclassifOneNw3.ExcluiUltimoCouro();
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Controle, Box, All_VSPaRclIts, All_VSPallet,
  Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  QrVSPallet: TmySQLQuery;
  ExigeSubClass: Boolean;
begin
  {[***VerSePrecisa***]
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest, ExigeSubClass) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaRclIts := QrAllVSPaRclIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  if VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) =
  ID_YES then
  begin
    EdBox.ValueVariant := 0;
    EdArea.ValueVariant := 0;
    //
    if not FCriando and EdArea.CanFocus then
      EdArea.SetFocus;
    if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
      EdArea.SetFocus
    else
    if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
    EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
      EdBox.SetFocus;
    //
    ReopenItens(Box, All_VSPaRclIts, All_VSPallet);
    AtualizaInfoOC();
  end;
}
end;

procedure TFmVSReclassifOneNw3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSReclassifOneNw3.FormCreate(Sender: TObject);
  function GetFontHeight(Base: Integer): Integer;
  begin
    Result := -Trunc(-Base * (Screen.Width / 1920));
  end;
  function ObtemWidth(Base: Integer): Integer;
  begin
    Result := Trunc(Base * (Screen.Width / 1920));
  end;
  function ObtemHeigth(Base: Integer): Integer;
  var
    H1, H2: Integer;
  begin
    H1 := Trunc(Base * (Screen.Height / 1080));
      Result := H1
  end;
var
  MenuItem: TMenuItem;
  IdTXT: String;
  I: Integer;
  OK: Boolean;
begin
  //
  //
  RGFrmaIns.ItemIndex := 3;
  //
  //
  //
  //
  FCriando := True;
  FClasseVisivel := True;
  FDifTime := DModG.ObtemAgora() - Now();
  //
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  //
  SGAll.PopupMenu := PMAll;
  //
  TesteInclusao1.Enabled := VAR_USUARIO = -1;
  //
  SGAll.Cells[00, 0] := 'Seq';
  SGAll.Cells[01, 0] := 'Box';
  SGAll.Cells[02, 0] := 'SubClas';
  SGAll.Cells[03, 0] := 'AreaM2';
  SGAll.Cells[04, 0] := 'Martelo';
  SGAll.Cells[05, 0] := 'VMI_Dest';
  SGAll.Cells[06, 0] := 'Controle';
  SGAll.Cells[07, 0] := 'VSPaRclIts';
  SGAll.Cells[08, 0] := 'VSPallet';
  SGAll.Cells[09, 0] := 'Pecas';
  SGAll.Cells[10, 0] := 'AreaP2';
  SGAll.Cells[11, 0] := 'VMI_Sorc';
  SGAll.Cells[12, 0] := 'VMI_Dest';
  //
  SGAll.ColWidths[00] := 40;
  SGAll.ColWidths[01] := 40; //Box
  SGAll.ColWidths[02] := 72; //SubClass
  SGAll.ColWidths[03] := 48; //AreaM2
  SGAll.ColWidths[04] := 72; //NO_MARTELO
  SGAll.ColWidths[05] := 90; //VMI_Dest
  SGAll.ColWidths[06] := 90; //Controle
  SGAll.ColWidths[07] := 90; //VSPaRclIts
  SGAll.ColWidths[08] := 90; //VSPallet
  SGAll.ColWidths[09] := 40; //Pecas
  SGAll.ColWidths[10] := 40; //AreaP2
  SGAll.ColWidths[11] := 90; //VMI_Sorc
  SGAll.ColWidths[12] := 90; //VMI_Dest
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
  begin
    IdTXT := Geral.FFF(I, 2);
    //
    FPMItens[I] := TPopupMenu.Create(Self);
    FPMItens[I].Name := 'PMItens' + IdTXT;
    FPMItens[I].OnPopup := PMItensPopup;
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'AdicionarPallet' + IdTXT;
    MenuItem.Caption := '&Adicionar pallet';
    MenuItem.OnClick := AdicionarPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'EncerrarPallet' + IdTXT;
    MenuItem.Caption := '&Encerrar pallet';
    MenuItem.OnClick := EncerrarPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'RemoverPallet' + IdTXT;
    MenuItem.Caption := '&Remover pallet';
    MenuItem.OnClick := RemoverPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'MensagemDeDados' + IdTXT;
    MenuItem.Caption := '&Mensagem de dados';
    MenuItem.OnClick := Mensagemdedados1Click;
    FPMItens[I].Items.Add(MenuItem);
  end;
  //
  if (Screen.Width < 1920) or (Screen.Height < 1080) then
  begin
    PnAll.Width := ObtemWidth(320);
    PnAll.Font.Size := GetFontHeight(-19(*14*));
    for I := 0 to SGAll.ColCount - 1 do
      SGAll.ColWidths[I] := ObtemWidth(SGAll.ColWidths[I]);
    //
    PnExtras.Width := ObtemWidth(233);
    PnExtras.Font.Size := GetFontHeight(-19(*14*));
    DBGNotaEqz.Font.Size := GetFontHeight(-19(*14*));
    TStringGrid(DBGNotaEqz).DefaultRowHeight := ObtemHeigth(24);
    for I := 0 to TStringGrid(DBGNotaEqz).ColCount - 1 do
      TStringGrid(DBGNotaEqz).ColWidths[I] :=
      ObtemWidth(TStringGrid(DBGNotaEqz).ColWidths[I]);
    DBEdNotaEqzM2.Font.Height := GetFontHeight(-32(*24*));
    //
    PnInfoBig.Font.Height := GetFontHeight(-19(*14*));
    PnInfoBig.Height := ObtemHeigth(97);
    //PnNaoClass.Height := ObtemHeigth(97);
    PnNaoClass.Width := ObtemWidth(320);
    PnIntMei.Width := ObtemWidth(120);
    //
    DBEdSdoVrtPeca.Font.Height := GetFontHeight(-32(*24*));
    DBEdSdoVrtArM2.Font.Height := GetFontHeight(-32(*24*));
    DBEdJaFoi_PECA.Font.Height := GetFontHeight(-32(*24*));
    DBEdJaFoi_AREA.Font.Height := GetFontHeight(-32(*24*));
    //
    PnJaClass.Visible := False;
    //
    PnArea.Width := ObtemWidth(140);
    PnBox.Width := ObtemWidth(52);
    PnSubClass.Width := ObtemWidth(82);
    PnDigitacao.Width := ObtemWidth(532);
    //
    EdArea.Font.Height     := GetFontHeight(-32(*24*));
    LaTipoArea.Font.Height := GetFontHeight(-32(*24*));
    EdBox.Font.Height      := GetFontHeight(-32(*24*));
    EdSubClass.Font.Height := GetFontHeight(-32(*24*));
    //EdVSMrtCad.Font.Height := GetFontHeight(-32(*24*));
    //CBVSMrtCad.Font.Height := GetFontHeight(-32(*24*));
    //
    SGAll.Font.Size        := GetFontHeight(-19(*14*));
    SGAll.DefaultRowHeight := ObtemHeigth(28);
    //
    PanelOC.Height := ObtemHeigth(102);
    PnInfoOC.Font.Height := GetFontHeight(-19(*14*));
    PnResponsaveis.Font.Height := GetFontHeight(-19(*14*));
    PnCfgEqz.Height := ObtemHeigth(46);
    PnCfgEqz.Font.Height := GetFontHeight(-16(*12*));
    for I := 0 to Self.ComponentCount - 1 do
    begin
      OK := False;
      if Components[I] is TLabel then
      begin
        if TLabel(Components[I]).FocusControl <> nil then
          OK :=
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnInfoOC)
          or
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnResponsaveis)
          or
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnMenu)
          or
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnCfgEqz);
      end else
      if Components[I] is TControl then
        OK :=
          (TControl(Components[I]).Parent = PnInfoOC)
        or
          (TControl(Components[I]).Parent = PnResponsaveis)
        or
          (TControl(Components[I]).Parent = PnMenu)
        or
          (TControl(Components[I]).Parent = PnCfgEqz);
      if OK then
      begin
        TControl(Components[I]).Top := ObtemHeigth(TControl(Components[I]).Top);
        TControl(Components[I]).Left := ObtemHeigth(TControl(Components[I]).Left);
        TControl(Components[I]).Width := ObtemHeigth(TControl(Components[I]).Width);
        TControl(Components[I]).Height := ObtemHeigth(TControl(Components[I]).Height);
      end;
    end;
  end;
  //
end;

procedure TFmVSReclassifOneNw3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if Key = VK_MULTIPLY then
  if Key = VK_F12 then
  begin
    if PnDigitacao.Enabled then
      EdArea.SetFocus;
    OcultaMostraClasses();
  end;
end;

procedure TFmVSReclassifOneNw3.FormResize(Sender: TObject);
begin
  //RealinhaBoxes();
end;

procedure TFmVSReclassifOneNw3.ImprimeFluxoDeMovimentoDoPallet();
begin
  {[***VerSePrecisa***]
  VS_PF.MostraRelatoroFluxoPallet(QrVSPallet0Codigo.Value,
    QrVSGerArtNewEmpresa.Value, QrVSPaRclCabNO_EMPRESA.Value);
}
end;

procedure TFmVSReclassifOneNw3.ImprimirfluxodemovimentodoPallet1Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoPallet();
end;

procedure TFmVSReclassifOneNw3.ImprimirfluxodemovimentodoPallet2Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoPallet();
end;

procedure TFmVSReclassifOneNw3.InsereCouroArrAll(CacCod, CacID, Codigo,
  ClaAPalOri, RclAPalOri, VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix,
  VMI_Dest, Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
  Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
  FatorIntDst: Double; Controle: Int64);
var
  I: Integer;
  a, b, t: Cardinal;
begin
  EdAll.Text := '';
  EdArrAll.Text := '';
  EdItens.Text := '';
  a := GetTickCount();
  //
  I := Length(FLsAll);
  SetLength(FLsAll, I + 1);
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaRclIts);                   // VSPaRclIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
  //
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdArrAll.Text := FloatToStr(t / 1000) + 's';

  //////////////////////////////////////////////////////////////////////////////

  a := GetTickCount();
  //
  I := Length(FLsItens[Box]);
  SetLength(FLsItens[Box], I + 1);
  FLsItens[Box][I, 00] := IntToStr(I + 1);
  FLsItens[Box][I, 01] := IntToStr(Controle);                     // Conrole
  FLsItens[Box][I, 02] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsItens[Box][I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsItens[Box][I, 04] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  //
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
end;

procedure TFmVSReclassifOneNw3.InsereCouroAtual();
const
  VSPaClaIts = 0;
  Martelo = 0;
  Pecas = 1;
var
  Box, Codigo, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  CacCod, CacID, ClaAPalOri, RclAPalOri, Revisor, Digitador, VSCacItsAOri,
  FrmaIns: Integer;
  PecasSrc, PecasDst, AreaM2, AreaP2, FatorIntSrc, FatorIntDst: Double;
  QrVSPallet: TmySQLQuery;
  DataHora, SubClass: String;
  Controle: Int64;
  Tempo: TDateTime;
  ExigeSubClass: Boolean;
  //
  Ta, Tb, Tt: Cardinal;
  PecasSrc_TXT, PecasDst_TXT, AreaM2_TXT, AreaP2_TXT, ValorT_TXT: String;
begin
  Ta := GetTickCount();
  //
  //Geral.MB_Aviso('Fazer classificacao');
  if EdArea.ValueVariant < 0.01 then
  begin
    if PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Enabled and EdArea.CanFocus then
    begin
      EdArea.SetFocus;
      EdArea.SelectAll;
    end;
    Exit;
  end;
  Box := EdBox.ValueVariant;
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    CacCod         := FCacCod;
    CacID          := Integer(emidReclasVSUni);
    Codigo         := FCodigo;
    Controle       := 0;
    ObtemDadosCouroOrigem(ClaAPalOri, RclAPalOri, VMI_Sorc, VSCacItsAOri);
    //VSPaRclIts     := ;   ver abaixo!!!
    //VSPallet       := ;   ver abaixo!!!
    //VMI_Sorc       := ;   ver abaixo!!!
    //VMI_Dest       := ;   ver abaixo!!!
    Box            := EdBox.ValueVariant;
    Revisor        := EdRevisor.ValueVariant;
    Digitador      := EdDigitador.ValueVariant;
    DataHora       := Geral.FDT(Now() + FDifTime, 109);
    FrmaIns        := RGFrmaIns.ItemIndex;
    SubClass       := dmkPF.SoTextoLayout(EdSubClass.Text);
    //AreaM2         := ;
    //AreaP2         := ;
    //Pecas          := 1;
    if QrVSPaRclCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet) then
      Exit;
    if not ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
    ExigeSubClass) then
      Exit;
    if MyObjects.FIC(ExigeSubClass and (Trim(EdSubClass.Text) = ''), nil,
    'Informe a sub classe!') then
      Exit;
    //
    FatorIntSrc    := QrVSPallet0FatorInt.Value;
    FatorIntDst    := QrVSPallet.FieldByName('FatorInt').AsFloat;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de destino n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc < FatorIntDst, nil,
    'O artigo de destino n�o pode ter parte de material maior que a origem!')
    then
      Exit;
    //
    //PecasSrc := FatorIntSrc / FatorIntDst;
    PecasSrc := Pecas / FatorIntSrc * FatorIntDst;
    PecasDst := Pecas;
    //VMI_Dest :=
    Controle := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', CO_DATA_HORA_GRL(*, 'Sumido',
    'RclAPalDst'*), 'FrmaIns', 'SubClass',
    'FatorIntSrc', 'FatorIntDst'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix,
    VMI_Dest, Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, DataHora(*, Sumido,
    RclAPalDst*), FrmaIns, SubClass,
    FatorIntSrc, FatorIntDst], [
    Controle], False) then
    begin
      EdSubClass.Text := '';
      Tempo := Now();
        InsereCouroArrAll(CacCod, CacID, Codigo,
        ClaAPalOri, RclAPalOri, VSPaClaIts,
        VSPaRclIts, VSPallet,
        VMI_Sorc, VMI_Baix, VMI_Dest,
        Box, Pecas,
        AreaM2, AreaP2, Revisor,
        Digitador, Martelo, DataHora,
        SubClass, FatorIntSrc, FatorIntDst,
        Controle);
        //
        PecasSrc_TXT  := Geral.FFT_Dot(PecasSrc, 3, siNegativo);
        PecasDst_TXT  := Geral.FFT_Dot(PecasDst, 3, siNegativo);
        //
        AreaM2_TXT := Geral.FFT_Dot(AreaM2, 2, siNegativo);
        AreaP2_TXT := Geral.FFT_Dot(AreaP2, 2, siNegativo);
        ValorT_TXT := Geral.FFT_Dot(QrVSGerArtNewCUSTO_M2.Value * AreaM2, 2, siNegativo);
        //
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca-' + PecasSrc_TXT +
        ', SdoVrtArM2=SdoVrtArM2-' + AreaM2_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Sorc) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET Pecas=Pecas-' + PecasSrc_TXT +
        ', AreaM2=AreaM2-' + AreaM2_TXT +
        ', AreaP2=AreaP2-' + AreaP2_TXT +
        ', ValorT=ValorT-' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Baix) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca+' + PecasDst_TXT +
        ', SdoVrtArM2=SdoVrtArM2+' + AreaM2_TXT +
        ', Pecas=Pecas+' + PecasDst_TXT +
        ', AreaM2=AreaM2+' + AreaM2_TXT +
        ', AreaP2=AreaP2+' + AreaP2_TXT +
        ', ValorT=ValorT+' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Dest) +
        '');
        //
        CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
      Tempo := Now() - Tempo;
      EdTempo.Text := FormatDateTime('ss:zzz', Tempo);
      //
      AtualizaXxxAPalOri(ClaAPalOri, RclAPalOri, Controle);
      ReopenItensAClassificarPallet();
      //
      EdBox.ValueVariant := 0;
      if RGFrmaIns.ItemIndex = 3 then
        EdArea.ValueVariant := 0;
      if RGFrmaIns.ItemIndex = 3 then
      begin
        EdArea.SetFocus;
        EdArea.SelectAll;
      end else
      begin
        EdBox.SetFocus;
        EdBox.SelectAll;
      end;
      //
      ReopenItens(Box, VSPaRclIts, VSPallet);
      AtualizaInfoOC();
      //
      Tb := GetTickCount();
      Tt := Tb - Ta;
      EdTempo.Text := FloatToStr(Tt / 1000) + 's';
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSReclassifOneNw3.LiberaDigitacao;
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  //
  if not FCriando and EdArea.CanFocus then
    EdArea.SetFocus;
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
  else
  if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
  EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
    EdBox.SetFocus
end;

procedure TFmVSReclassifOneNw3.LiberaDigitacaoManual(Libera: Boolean);
begin
  PnArea.Enabled := Libera;
end;

procedure TFmVSReclassifOneNw3.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  QrSum, QrSumPal: TmySQLQuery;
  //EdPercent,
  EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
  ExigeSubClass: Boolean;
begin
(* ver o que fazer!
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass);
  case Box of
    1:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      //EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    2:
    begin
      QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      //EdPercent := EdPercent1;
      EdMedia   := EdMedia1;
    end;
    3:
    begin
      QrSum     := QrSum3;
      QrSumPal  := QrSumPal3;
      //EdPercent := EdPercent3;
      EdMedia   := EdMedia3;
    end;
    4:
    begin
      QrSum     := QrSum4;
      QrSumPal  := QrSumPal4;
      //EdPercent := EdPercent4;
      EdMedia   := EdMedia4;
    end;
    5:
    begin
      QrSum     := QrSum5;
      QrSumPal  := QrSumPal5;
      //EdPercent := EdPercent5;
      EdMedia   := EdMedia5;
    end;
    6:
    begin
      QrSum     := QrSum6;
      QrSumPal  := QrSumPal6;
      //EdPercent := EdPercent6;
      EdMedia   := EdMedia6;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('RECLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaRclCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('RECLASSIFICA��O DO PALLET  ' + Geral.FF0(QrVSPaRclCabVSPallet.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  //Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
*)
end;

procedure TFmVSReclassifOneNw3.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSReclassifOneNw3.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSReclassifOneNw3.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  Result := (FCkSubClass[Box] <> nil) and (FCkSubClass[Box].Checked);
  if Result and (Trim(EdSubClass.Text) <> '') then
    Result := False;
end;

function TFmVSReclassifOneNw3.ObtemDadosBox(const Box: Integer; var
VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer; var ExigeSubClass:
Boolean): Boolean;
begin
  VMI_Sorc      := QrVSGerArtNewControle.Value;
  VSPallet      := FQrVSPallet[Box].FieldByName('Codigo').AsInteger;
  VSPaRclIts    := FQrVSPalRclIts[Box].FieldByName('Controle').AsInteger;
  VMI_Baix      := FQrVSPalRclIts[Box].FieldByName('VMI_Baix').AsInteger;
  VMI_Dest      := FQrVSPalRclIts[Box].FieldByName('VMI_Dest').AsInteger;
  ExigeSubClass := FCkSubClass[Box].Checked;
  //
  Result        := True;
end;

function TFmVSReclassifOneNw3.ObtemDadosCouroOrigem(var ClaAPalOri, RclAPalOri,
  VMI_Sorc, VSCacItsAOri: Integer): Boolean;
begin
  Result         := False;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  VMI_Sorc       := 0;
  VSCacItsAOri   := 0;

  if RGFrmaIns.ItemIndex < 3 then
  begin
    if (QrItensACP.State = dsInactive)
    or (QrItensACP.RecordCount = 0) then
      Exit;
  end;
  //?
  //VMI_Sorc := QrItensACPVMI_Dest.Value;
  VMI_Sorc := QrVSPaRclCabVSMovIts.Value;
  case RGFrmaIns.ItemIndex of
    0,
    1,
    2:
    begin
      Result := True;
      case TEstqMovimID(QrItensACPCacID.Value) of
        (*7*)emidClassArtVSUni: ClaAPalOri := QrItensACPControle.Value;
        (*8*)emidReclasVSUni:   RclAPalOri := QrItensACPControle.Value;
        else
        begin
          Result := False;
          Geral.MB_Erro(
          '"TEstqMovimID" n�o definido em "ObtemDadosCouroOrigem()"');
        end;
      end;
    end;
    3:
    begin
      Result := True;
    end;
    else Geral.MB_Aviso(
      'Forma de reclassifica��o n�o definida em "ObtemDadosCouroOrigem()"');
  end;
end;

function TFmVSReclassifOneNw3.ObtemNO_MARTELO(Martelo: Integer): String;
begin
  Result := '';
end;

function TFmVSReclassifOneNw3.ObtemQryesBox(const Box: Integer; var QrVSPallet:
  TmySQLQuery): Boolean;
begin
  QrVSPallet := FQrVSPallet[Box];
  Result := True;
end;

procedure TFmVSReclassifOneNw3.OcultaMostraClasses();
var
  I: Integer;
begin
  FClasseVisivel := not FClasseVisivel;
  for I := 1 to FBoxMax do
  begin
    if FDBEdArtNome[I] <> nil then
      FDBEdArtNome[I].Visible  := FClasseVisivel;
  end;
end;

procedure TFmVSReclassifOneNw3.PMEncerraPopup(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to FBoxMax do
  begin
    FPalletdobox[I].Visible := True;
    FPalletdobox[I].Enabled :=
    (FQrVSPallet[I] <> nil) and
    (FQrVSPallet[I].State <> dsInactive) and
    (FQrVSPallet[I].RecordCount > 0);
  end;
  for I := FBoxMax + 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
    FPalletdobox[I].Visible := False;
end;

procedure TFmVSReclassifOneNw3.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  //AdicionarPallet1.Enabled := (QrVSPallet1.State = dsInactive) or (QrVSPallet1.RecordCount = 0);
  //MyObjects.HabilitaMenuItemCabUpd(EncerrarPallet1, QrVSPallet1);
  //MyObjects.HabilitaMenuItemCabDel(RemoverPallet1, QrVSPallet1, QrItens1);
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSReclassifOneNw3.QrItensACPAfterOpen(DataSet: TDataSet);
begin
  VerificaAreaAutomatica();
end;

procedure TFmVSReclassifOneNw3.QrItensACPAfterScroll(DataSet: TDataSet);
begin
  VerificaAreaAutomatica();
end;

procedure TFmVSReclassifOneNw3.QrItensACPBeforeClose(DataSet: TDataSet);
begin
  if RGFrmaIns.ItemIndex < 3 then
    EdArea.ValueVariant := 0;
end;

procedure TFmVSReclassifOneNw3.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaRclCabTipoArea.Value of
    0: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTSdoVrtArM2.Value;
    1: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaP2.Value - Geral.ConverteArea(QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    else QrSumTJaFoi_AREA.Value := 0;
  end;
  QrSumTJaFoi_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTSdoVrtPeca.Value;
end;

procedure TFmVSReclassifOneNw3.QrVSCacItsCalcFields(DataSet: TDataSet);
begin
  if QrVSCacSumAreaM2.Value > 0 then
    QrVSCacItsPercM2.Value := QrVSCacItsAreaM2.Value / QrVSCacSumAreaM2.Value * 100
  else
    QrVSCacItsPercM2.Value := 0;
  //
  if QrVSCacSumPecas.Value > 0 then
    QrVSCacItsPercPc.Value := QrVSCacItsPecas.Value / QrVSCacSumPecas.Value * 100
  else
    QrVSCacItsPercPc.Value := 0;
end;

procedure TFmVSReclassifOneNw3.QrVSPallet0AfterOpen(DataSet: TDataSet);
var
  I: Integer;
begin
  for I := 1 to FBoxMax do
    ReconfiguraPaineisIntMei(FQrVSPallet[I], FPnIntMei[I]);
end;

procedure TFmVSReclassifOneNw3.QrVSPaRclCabAfterScroll(DataSet: TDataSet);
var
  I, LstPal, Controle, Codigo: Integer;
begin
  {[***VerSePrecisa***]
  for I := 1 to FBoxMax do
  begin
    if QrVSPaRclCab.FieldByName(VS_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
  end;
  //
  ReopenVSGerArtDst();
  //
  VerificaBoxes();
  //
  for I := 1 to FBoxMax do
  begin
    if FQrVSPallet[I] <> nil then
    begin
      LstPal := QrVSPaRclCab.FieldByName('LstPal' + Geral.FFN(I, 2)).AsInteger;
      ReopenVSPallet(I, FQrVSPallet[I], FQrVSPalRclIts[I], LstPal);
      if (FQrVSPalRclIts[I].State <> dsInactive) and
      (FQrVSPallet[I].State <> dsInactive) then
      begin
        Controle := FQrVSPalRclIts[I].FieldByName('Controle').AsInteger;
        Codigo   := FQrVSPallet[I].FieldByName('Codigo').Value;
        ReopenItens(I, Controle, Codigo);
      end;
    end;
  end;
  //
  //if FUsaMemory then
    ReopenAll();
  AtualizaInfoOC();
  //
  //
  ReopenVSPallet(0, QrVSPallet0, nil, QrVSPaRclCabVSPallet.Value);
  ReopenItensAClassificarPallet();
}
end;

procedure TFmVSReclassifOneNw3.QrVSPaRclCabBeforeClose(DataSet: TDataSet);
var
  I: Integer;
begin
  QrVSGerArtNew.Close;
  for I := 1 to FBoxMax do
  begin
    FPnBox[I].Visible := True;
    FQrVSPallet[I].Close;
    FQrVSPalRclIts[I].Close;
  end;
end;

procedure TFmVSReclassifOneNw3.QrVSPaRclCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaRclCabTipoArea.Value of
    0: QrVSPaRclCabNO_TIPO.Value := 'm�';
    1: QrVSPaRclCabNO_TIPO.Value := 'ft�';
    else QrVSPaRclCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaRclCabNO_TIPO.Value;
end;

procedure TFmVSReclassifOneNw3.ReconfiguraPaineisIntMei(QrVsPallet: TmySQLQuery;
Panel: TPanel);
var
  Habilita: Boolean;
begin
  if QrVSPallet <> nil then
  begin
    Habilita :=
      (QrVSPallet.State <> dsInactive) and
      (QrVSGerArtNew.State <> dsInactive) and
      (QrVSPallet.FieldByName('FatorInt').AsFloat < QrVSPallet0FatorInt.Value);
    if (Panel <> nil) and (Habilita <> Panel.Visible) then
      Panel.Visible := Habilita;
  end;
end;

procedure TFmVSReclassifOneNw3.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo, DtHrFim: String;
  Codigo, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
  ExigeSubClass: Boolean;
begin
  {[***VerSePrecisa***]
  Campo := VS_PF.CampoLstPal(Box);
  Codigo := QrVSPaRclCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
  ExigeSubClass) then
    Exit;
  if VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaRclIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclcaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaRclCab();
      end;
    end;
  end;
}
end;

procedure TFmVSReclassifOneNw3.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSReclassifOneNw3.ReopenAll();
var
  I, J: Integer;
  a, b, t: Cardinal;
begin
  a := GetTickCount();
  DqAll.SQL.Text :=
  ' SELECT cia.Controle, cia.VSPaRclIts, cia.VSPallet, cia.Pecas, ' + // 0 a 3
  ' cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ' +  // 4 a 8
  ' cia.SubClass, mrt.Nome NO_MARTELO ' +                             // 9 a 10
  ' FROM vscacitsa cia ' +
  ' LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo' +
  ' WHERE cia.CacCod=' + Geral.FF0(FCacCod) +
  //' ORDER BY cia.Controle DESC ';
  ' ORDER BY cia.Controle ';
  //
  DqAll.Open();
  SetLength(FLsAll, DqAll.RecordCount);
  DqAll.First();
  while not DqAll.Eof do
  begin
    J := DqAll.RecNo;
    //
    FLsAll[J, 01] := DqAll.FieldValues[08];   // Box
    FLsAll[J, 02] := DqAll.FieldValues[09];   // SubClass
    FLsAll[J, 03] := DqAll.FieldValues[04];   // AreaM2
    FLsAll[J, 04] := DqAll.FieldValues[10];   // NO_MARTELO
    FLsAll[J, 05] := DqAll.FieldValues[07];   // VMI_Dest
    FLsAll[J, 06] := DqAll.FieldValues[00];   // Controle
    FLsAll[J, 07] := DqAll.FieldValues[01];   // VSPaRclIts
    FLsAll[J, 08] := DqAll.FieldValues[02];   // VSPallet
    FLsAll[J, 09] := DqAll.FieldValues[03];   // Pecas
    FLsAll[J, 10] := DqAll.FieldValues[05];   // AreaP2
    FLsAll[J, 11] := DqAll.FieldValues[06];   // VMI_Sorc
    FLsAll[J, 12] := DqAll.FieldValues[07];   // VMI_Dest
    //
    DqAll.Next();
  end;
  DqAll.Close();
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdAll.Text := FloatToStr(t / 1000) + 's';
end;

procedure TFmVSReclassifOneNw3.Reopendesnate(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  {[***VerSePrecisa***]
  Codigo := EdDesnate.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do desnate!');
  end else
    VS_PF.ReopenDesnate(Codigo, GraGruX, QrVSCacIts, QrVSCacSum);
}
end;

procedure TFmVSReclassifOneNw3.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  {[***VerSePrecisa***]
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);
}
end;

procedure TFmVSReclassifOneNw3.ReopenItens(Box, VSPaRclIts, VSPallet: Integer);
var
  I, J: Integer;
  a, b, t: Cardinal;
  //
begin
  a := GetTickCount();
  DqItens.SQL.Text :=
  ' SELECT Controle, Pecas, AreaM2, AreaP2 ' +
  ' FROM vscacitsa ' +
  ' WHERE CacCod=' + Geral.FF0(FCacCod) +
  ' AND VSPaRclIts=' + Geral.FF0(VSPaRclIts) +
  ' AND VSPallet=' + Geral.FF0(VSPallet) +
  //' ORDER BY Controle DESC ' +
  ' ORDER BY Controle ' +
  '';

  //
  DqItens.Open();
  SetLength(FLsItens[Box], DqItens.RecordCount);
  DqItens.First();
  while not DqItens.Eof do
  begin
    J := DqItens.RecNo;
    //
    FLsItens[Box][J, 01] := DqItens.FieldValues[00];   // Conrole
    FLsItens[Box][J, 02] := DqItens.FieldValues[01];   // Pecas
    FLsItens[Box][J, 03] := DqItens.FieldValues[02];   // AreaM2
    FLsItens[Box][J, 04] := DqItens.FieldValues[03];   // AreaP2
    //
    DqItens.Next();
  end;
  DqItens.Close();
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
  //
  ReopenSumPal(Box, VSPallet);
end;

procedure TFmVSReclassifOneNw3.ReopenItensAClassificarPallet();
var
  Forma: Integer;
  SQL_Forma: String;
begin
  LiberaDigitacaoManual(False);
  case RGFrmaIns.ItemIndex of
    0: SQL_Forma := Geral.ATS(['AND Sumido=0 ','AND RclAPalDst=0 ']);
    1: SQL_Forma := Geral.ATS(['AND Sumido=1 ','AND RclAPalDst=0 ']);
    2: SQL_Forma := Geral.ATS(['AND RclAPalDst=0 ']);
    3: LiberaDigitacaoManual(True);
    else Geral.MB_Aviso(
    'Forma de digita��o n�o definida em "ReopenItensAClassificarPallet()"');
  end;
  //
  if RGFrmaIns.ItemIndex = 3 then
  begin
    QrItensACP.Close;
    EdArea.ValueVariant := 0;
    if PnArea.Enabled and PnDigitacao.Enabled and
    EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
      EdArea.SetFocus;
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrItensACP, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(QrVSPaRclCabVSPallet.Value),
  SQL_Forma,
  'ORDER BY Controle DESC ',
  '']);
end;

procedure TFmVSReclassifOneNw3.ReopenSumPal(Box, VSPallet: Integer);
var
  J: Integer;
  Pecas, AreaM2, AreaP2: Double;
begin
  DqSumPall.SQL.Text :=
  ' SELECT ' +
  ' SUM(vmi.SdoVrtPeca) SdoVrtPeca, ' +
  ' SUM(vmi.SdoVrtArM2) SdoVrtArM2' +
  ' FROM ' + CO_SEL_TAB_VMI + ' vmi' +
  ' WHERE vmi.Pallet=' + Geral.FF0(VSPallet) +
  ' AND vmi.Pecas>0 ' +
  '';
  //
  DqSumPall.Open();
  Pecas  := Geral.DMV_Dot(DqSumPall.FieldValues[00]);   // Pecas
  AreaM2 := Geral.DMV_Dot(DqSumPall.FieldValues[01]);   // AreaM2
  AreaP2 := Geral.ConverteArea(AreaM2, TCalcType.ctM2toP2, TCalcFrac.cfQuarto);
  DqSumPall.Close();
  CarregaSGSumPall(Box, amathSubstitui, Pecas, AreaM2, AreaP2);
end;

procedure TFmVSReclassifOneNw3.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.Controle=' + Geral.FF0(QrVSPaRclCabVSMovIts.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSReclassifOneNw3.ReopenVSPaRclCab();
begin
  //
  GPBoxes.Visible := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaRclCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vga.VSPallet, vga.Codigo VSGerRClA ',
  'FROM vsparclcaba pcc',
  'LEFT JOIN vsgerrcla  vga ON vga.Codigo=pcc.vsgerrcl',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
  GPBoxes.Visible := True;
end;

procedure TFmVSReclassifOneNw3.RGFrmaInsClick(Sender: TObject);
begin
  if RGFrmaIns.ItemIndex <> 3 then
  begin
    RGFrmaIns.ItemIndex := 3;
    Exit;
  end;
  ReopenItensAClassificarPallet();
end;

procedure TFmVSReclassifOneNw3.SbDesnateClick(Sender: TObject);
begin
  ReopenDesnate(False);
end;

procedure TFmVSReclassifOneNw3.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

function TFmVSReclassifOneNw3.SubstituiOC_Antigo(): Boolean;
  {[***VerSePrecisa***]
  function MostraFormVSReclassPrePal(SQLType: TSQLType; Pallet1, Pallet2,
  Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer):
  Integer;
  begin
    Result := 0;
    if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
    begin
      FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
      //
      FmVSReclassPrePal.ShowModal;
      Result := FmVSReclassPrePal.FPallet;
      FmVSReclassPrePal.Destroy;
    end;
  end;
var
  Codigo, CacCod, Pallet, Digitador, Revisor: Integer;
  MovimID: TEstqMovimID;
}
begin
  {[***VerSePrecisa***]
  Result := False;
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  Pallet := MostraFormVSReclassPrePal(stIns,
  QrVSPaRclCabLstPal01.Value,
  QrVSPaRclCabLstPal02.Value,
  QrVSPaRclCabLstPal03.Value,
  QrVSPaRclCabLstPal04.Value,
  QrVSPaRclCabLstPal05.Value,
  QrVSPaRclCabLstPal06.Value,
  EdDigitador.ValueVariant,
  EdRevisor.ValueVariant);
  //
  if Pallet <> 0 then
  begin
    if DBCheck.CriaFm(TFmVSRclArtPrpNew, FmVSRclArtPrpNew, afmoNegarComAviso) then
    begin
      FmVSRclArtPrpNew.ImgTipo.SQLType := stIns;
      //
      FmVSRclArtPrpNew.EdPallet.ValueVariant := Pallet;
      FmVSRclArtPrpNew.CBPallet.KeyValue     := Pallet;
      //
      FmVSRclArtPrpNew.EdPallet1.ValueVariant := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNew.EdPallet2.ValueVariant := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNew.EdPallet3.ValueVariant := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNew.EdPallet4.ValueVariant := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNew.EdPallet5.ValueVariant := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNew.EdPallet6.ValueVariant := QrVSPaRclCabLstPal06.Value;
      //
      FmVSRclArtPrpNew.CBPallet1.KeyValue := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNew.CBPallet2.KeyValue := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNew.CBPallet3.KeyValue := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNew.CBPallet4.KeyValue := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNew.CBPallet5.KeyValue := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNew.CBPallet6.KeyValue := QrVSPaRclCabLstPal06.Value;
      //
      FmVSRclArtPrpNew.SbPalletClick(Self);
      FmVSRclArtPrpNew.ShowModal;
      Codigo := FmVSRclArtPrpNew.FCodigo;
      CacCod := FmVSRclArtPrpNew.FCacCod;
      MovimID := FmVSRclArtPrpNew.FMovimID;
      FmVSRclArtPrpNew.Destroy;
      //
      if Codigo <> 0 then
      begin
        FCodigo := Codigo;
        FCacCod := CacCod;
        FMovimID := MovimID;
        ReopenVSPaRclCab();
        //
        EdDigitador.ValueVariant := Digitador;
        EdRevisor.ValueVariant   := Revisor;
        Result := True;
      end else
        Result := False;
    end;
  end;
}
end;

function TFmVSReclassifOneNw3.SubstituiOC_Novo: Boolean;
  {[***VerSePrecisa***]
  function MostraFormVSReclassPrePal(SQLType: TSQLType(*; Pallet1, Pallet2,
  Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer*)):
  Integer;
  begin
    Result := 0;
    if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
    begin
      FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
      //
      FmVSReclassPrePal.ShowModal;
      Result := FmVSReclassPrePal.FPallet;
      FmVSReclassPrePal.Destroy;
    end;
  end;
var
  Codigo, CacCod, Pallet, Digitador, Revisor: Integer;
  MovimID: TEstqMovimID;
}
begin
  {[***VerSePrecisa***]
  Result := False;
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  Pallet := MostraFormVSReclassPrePal(stIns(*,
  QrVSPaRclCabLstPal01.Value,
  QrVSPaRclCabLstPal02.Value,
  QrVSPaRclCabLstPal03.Value,
  QrVSPaRclCabLstPal04.Value,
  QrVSPaRclCabLstPal05.Value,
  QrVSPaRclCabLstPal06.Value,
  EdDigitador.ValueVariant,
  EdRevisor.ValueVariant*));
  //
  if Pallet <> 0 then
  begin
    if DBCheck.CriaFm(TFmVSRclArtPrpNw3, FmVSRclArtPrpNw3, afmoNegarComAviso) then
    begin
      FmVSRclArtPrpNw3.ImgTipo.SQLType := stIns;
      //
      FmVSRclArtPrpNw3.EdPallet.ValueVariant := Pallet;
      FmVSRclArtPrpNw3.CBPallet.KeyValue     := Pallet;
      //
      FmVSRclArtPrpNw3.EdPallet01.ValueVariant := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNw3.EdPallet02.ValueVariant := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNw3.EdPallet03.ValueVariant := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNw3.EdPallet04.ValueVariant := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNw3.EdPallet05.ValueVariant := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNw3.EdPallet06.ValueVariant := QrVSPaRclCabLstPal06.Value;
      FmVSRclArtPrpNw3.EdPallet07.ValueVariant := QrVSPaRclCabLstPal07.Value;
      FmVSRclArtPrpNw3.EdPallet08.ValueVariant := QrVSPaRclCabLstPal08.Value;
      FmVSRclArtPrpNw3.EdPallet09.ValueVariant := QrVSPaRclCabLstPal09.Value;
      FmVSRclArtPrpNw3.EdPallet10.ValueVariant := QrVSPaRclCabLstPal10.Value;
      FmVSRclArtPrpNw3.EdPallet11.ValueVariant := QrVSPaRclCabLstPal11.Value;
      FmVSRclArtPrpNw3.EdPallet12.ValueVariant := QrVSPaRclCabLstPal12.Value;
      FmVSRclArtPrpNw3.EdPallet13.ValueVariant := QrVSPaRclCabLstPal13.Value;
      FmVSRclArtPrpNw3.EdPallet14.ValueVariant := QrVSPaRclCabLstPal14.Value;
      FmVSRclArtPrpNw3.EdPallet15.ValueVariant := QrVSPaRclCabLstPal15.Value;
      //
      FmVSRclArtPrpNw3.CBPallet01.KeyValue := QrVSPaRclCabLstPal01.Value;
      FmVSRclArtPrpNw3.CBPallet02.KeyValue := QrVSPaRclCabLstPal02.Value;
      FmVSRclArtPrpNw3.CBPallet03.KeyValue := QrVSPaRclCabLstPal03.Value;
      FmVSRclArtPrpNw3.CBPallet04.KeyValue := QrVSPaRclCabLstPal04.Value;
      FmVSRclArtPrpNw3.CBPallet05.KeyValue := QrVSPaRclCabLstPal05.Value;
      FmVSRclArtPrpNw3.CBPallet06.KeyValue := QrVSPaRclCabLstPal06.Value;
      FmVSRclArtPrpNw3.CBPallet07.KeyValue := QrVSPaRclCabLstPal07.Value;
      FmVSRclArtPrpNw3.CBPallet08.KeyValue := QrVSPaRclCabLstPal08.Value;
      FmVSRclArtPrpNw3.CBPallet09.KeyValue := QrVSPaRclCabLstPal09.Value;
      FmVSRclArtPrpNw3.CBPallet10.KeyValue := QrVSPaRclCabLstPal10.Value;
      FmVSRclArtPrpNw3.CBPallet11.KeyValue := QrVSPaRclCabLstPal11.Value;
      FmVSRclArtPrpNw3.CBPallet12.KeyValue := QrVSPaRclCabLstPal12.Value;
      FmVSRclArtPrpNw3.CBPallet13.KeyValue := QrVSPaRclCabLstPal13.Value;
      FmVSRclArtPrpNw3.CBPallet14.KeyValue := QrVSPaRclCabLstPal14.Value;
      FmVSRclArtPrpNw3.CBPallet15.KeyValue := QrVSPaRclCabLstPal15.Value;
      //
      FmVSRclArtPrpNw3.SbPalletClick(Self);
      FmVSRclArtPrpNw3.ShowModal;
      Codigo := FmVSRclArtPrpNw3.FCodigo;
      CacCod := FmVSRclArtPrpNw3.FCacCod;
      MovimID := FmVSRclArtPrpNw3.FMovimID;
      FmVSRclArtPrpNw3.Destroy;
      //
      if Codigo <> 0 then
      begin
        FCodigo := Codigo;
        FCacCod := CacCod;
        FMovimID := MovimID;
        ReopenVSPaRclCab();
        //
        EdDigitador.ValueVariant := Digitador;
        EdRevisor.ValueVariant   := Revisor;
        Result := True;
      end else
        Result := False;
    end;
  end;
}
end;

procedure TFmVSReclassifOneNw3.TentarFocarEdArea();
begin
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

procedure TFmVSReclassifOneNw3.TesteInclusao1Click(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator: Double;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  V_Txt := DBEdSdoVrtPeca.Text;
  //
  if InputQuery('Itens a reclassificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    for I := 1 to Vezes do
    begin
      if QrSumTSdoVrtPeca.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        Box := Random(7);
        if BoxInBoxes(Box) then
        begin
          OK := True;
          EdBox.ValueVariant := Box;
        end;
      end;
      //
      if RGFrmaIns.Itemindex = 3 then
      begin
        if QrSumTSdoVrtPeca.Value > 1 then
        begin
          Area := QrSumTSdoVrtArM2.Value / QrSumTSdoVrtPeca.Value;
          Randomize;
          Desvio := Random(3000);
          Fator := 1 + ((Desvio - 1500) / 10000);
          Area := Area * Fator;
        end else
          Area := QrSumTSdoVrtArM2.Value;
        EdArea.ValueVariant := Area * 100;
        InsereCouroAtual();
      end;
      //
    end;
  end;
end;

procedure TFmVSReclassifOneNw3.UpdDelCouroSelecionado(SQLTipo: TSQLType; CampoSubClas: Boolean);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaRclIts, All_VSPallet, Box_VSPaRclIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet: TmySQLQuery;
  Pecas, AreaM2, AreaP2: Double;
  Txt, SubClass: String;
  Continua: Boolean;
  ExigeSubClass: Boolean;
begin
  {[***VerSePrecisa***]
  Box := QrAllBox.Value;
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
  Box_VMI_Baix, Box_VMI_Dest, ExigeSubClass) then
    Exit;
  Controle       := QrAllControle.Value;
  All_VSPaRclIts := QrAllVSPaRclIts.Value;
  All_VSPallet   := QrAllVSPallet.Value;
  //
  Continua := False;
  //
  if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
  begin
    Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do!');
    Exit;
  end;
  case SQLTipo of
    stUpd:
    begin
      if CampoSubClas then
      begin
        Txt := QrAllSubClass.Value;
        if InputQuery('Altera��o de dados', 'Altera��o da sub classe', Txt) then
        begin
          Subclass := Uppercase(dmkPF.SoTextoLayout(Txt));
          //
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
          'SubClass'], ['Controle'], [SubClass], [Controle], False);
        end;
      end else
      begin
        Pecas  := 1;
        //
        if QrVSPaRclCabTipoArea.Value = 0 then
          Txt := FloatToStr(QrAllAreaM2.Value * 100)
        else
          Txt := FloatToStr(QrAllAreaP2.Value * 100);
        //
        if InputQuery('Altera��o de dados', 'Altera��o de �rea: (Apenas n�meros sem v�rgula)', Txt) then
        begin
          AreaM2 := Geral.IMV(Txt);
          //
          if QrVSPaRclCabTipoArea.Value = 0 then
          begin
            AreaM2 := AreaM2 / 100;
            AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
          end else
          begin
            AreaP2 := AreaM2 / 100;
            AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
          end;
          Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
          'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
          [Controle], False);
        end;
      end;
    end;
    stDel:
    begin
      Continua := VS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
    end;
    else
      Geral.MB_Erro('N�o implementado!');
  end;
  if Continua then
  begin
    VS_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
    Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
    //
    if PnDigitacao.Enabled then
    begin
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      //EdVSMrtCad.Text     := '';
      //CBVSMrtCad.KeyValue := Null;
      if not FCriando and EdArea.CanFocus then
        EdArea.SetFocus;
      if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
      EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
        EdArea.SetFocus
      else
      if not FCriando and PnBox.Enabled and PnDigitacao.Enabled and
      EdBox.Visible and EdBox.Enabled and EdBox.CanFocus then
        EdBox.SetFocus
    end;
    //
    ReopenItens(Box, All_VSPaRclIts, All_VSPallet);
    AtualizaInfoOC();
  end;
  ReopenAll();
}
end;

procedure TFmVSReclassifOneNw3.UpdDelCouroSelecionado3(SQLTipo: TSQLType;
  CampoSubClas: Boolean);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaRclIts, All_VSPallet, Box_VSPaRclIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet(*, QrItens, QrSum, QrSumPal*): TmySQLQuery;
  Pecas, AreaM2, AreaP2, AntM2, AntP2: Double;
  Txt: String;
  Continua: Boolean;
  //
  SelTop, SelBot, cIni, cFim, Count, I: Integer;
  //
  ExigeSubClass: Boolean;
  N: Integer;
begin
  {[***VerSePrecisa***]
  N := 0;
  SelTop := SGAll.Selection.Top;
  SelBot := SGAll.Selection.Bottom;
  Count  := Length(FLsAll);
  cIni   := Count - SelTop + 1;
  cFim   := Count - SelBot + 1;
  Geral.MB_Info(IntToStr(cIni) + ' ate ' + IntToStr(cFim));
  //
  //cIni := Count - cIni + 1;
  //cFim := Count - cFim + 1;
  //
  for I := cIni -1 downto cFim - 1 do
  begin
    if (I < Length(FLsAll)) then
    begin
      //Box := QrAllBox.Value;
      //Box := Geral.IMV(FLsAll[I, 01]); //QrAllBox.Value;
      Box := Geral.IMV(FLsAll[I, 01]); //QrAllBox.Value;
      if not ObtemQryesBox(Box, QrVSPallet) then
        Exit;
      if not ObtemDadosBox(Box, Box_VSPaRclIts, Box_VSPallet, Box_VMI_Sorc,
      Box_VMI_Baix, Box_VMI_Dest, ExigeSubclass) then
        Exit;
      //Controle       := QrAllControle.Value;
      Controle       := Geral.I64(FLsAll[I, 06]);
      //All_VSPaRclIts := QrAllVSPaRclIts.Value;
      All_VSPaRclIts := Geral.IMV(FLsAll[I, 07]);
      //All_VSPallet   := QrAllVSPallet.Value;
      All_VSPallet   := Geral.IMV(FLsAll[I, 08]);
      //
      Continua := False;
      //
      if (All_VSPaRclIts <> Box_VSPaRclIts) or (Box_VSPallet <> All_VSPallet) then
      begin
        Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do ou alterado!');
        Exit;
      end;
      case SQLTipo of
        stUpd:
        begin
          Pecas  := 1;
          //
          if QrVSPaRclCabTipoArea.Value = 0 then
            //Txt := FloatToStr(QrAllAreaM2.Value * 100)
            Txt := Geral.SoNumero_TT(FLsAll[I, 03])
          else
            //Txt := FloatToStr(QrAllAreaP2.Value * 100);
            Txt := Geral.SoNumero_TT(FLsAll[I, 10]);
          //
          if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
          begin
            AreaM2 := Geral.IMV(Txt);
            //
            if QrVSPaRclCabTipoArea.Value = 0 then
            begin
              AreaM2 := AreaM2 / 100;
              AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
            end else
            begin
              AreaP2 := AreaM2 / 100;
              AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
            end;
            AntM2  := Geral.DMV_Dot(FLsAll[I, 03]);
            AntP2  := Geral.DMV_Dot(FLsAll[I, 03]);
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                          'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                          [Controle], False);
            FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);
            FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);
            //
            CarregaSGSumPall(Box, amathSubtrai, Pecas, AntM2, AntP2);
            CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
            //
          end;
        end;
        stDel:
        begin
          if N = 0 then
          if Geral.MB_Pergunta(
          'Deseja realmente excluir os couros selecionados?') = ID_YES then
          begin
            N := N + 1;
          end;
          if N > 0 then
          begin
            Continua := VS_PF.ExcluiVSNaoVMI('', Tabela, Campo, Controle,
            Dmod.MyDB) = ID_YES;
            CarregaSGSumPall(Box, amathSubtrai, Pecas, AreaM2, AreaP2);
          end;
        end;
        else
          Geral.MB_Erro('N�o implementado!');
      end;
      if Continua then
      begin
        VS_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
        Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
        //
        if PnDigitacao.Enabled then
        begin
          EdBox.ValueVariant  := 0;
          EdArea.ValueVariant := 0;
          //EdVSMrtCad.Text     := '';
          //CBVSMrtCad.KeyValue := Null;
          EdArea.SetFocus;
          EdArea.SelectAll;
        end;
        //
        ReopenItens(Box, All_VSPaRclIts, All_VSPallet);
        AtualizaInfoOC();
      end;
    end else
      Geral.MB_Aviso('Item n�o existe!');
  end;
  ReopenAll();
}
end;

procedure TFmVSReclassifOneNw3.VerificaAreaAutomatica;
begin
  case QrVSPaRclCabTipoArea.Value of
    0: EdArea.ValueVariant := (QrItensACPAreaM2.Value * 100);
    1: EdArea.ValueVariant := (QrItensACPAreaP2.Value * 100);
    else Geral.MB_Aviso(
      'Tipo de �rea n�o definida em "QrItensACPAfterScroll()"');
  end;
  //
  if not FCriando and EdBox.CanFocus then
    EdBox.SetFocus;
end;

procedure TFmVSReclassifOneNw3.VerificaBoxes;
var
  LstPal, I: Integer;
begin
  if not Mostrartodosboxes1.Checked then
  begin
    for I := 1 to FBoxMax do
    begin
      if FPnBox[I] <> nil then
      begin
        LstPal := QrVSPaRclCab.FieldByName('LstPal' + Geral.FFF(I, 2)).AsInteger;
        FPnBox[I].Visible := LstPal <> 0;
      end;
    end;
  end else
  begin
    for I := 1 to FBoxMax do
    begin
      if FPnBox[I] <> nil then
        FPnBox[I].Visible := True;
    end;
  end;
end;

function TFmVSReclassifOneNw3.ZeraEstoquePalletOrigem(): Boolean;
var
  BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT: Double;
  DataEHora: TDateTime;
begin
  {[***VerSePrecisa***]
  Result := True;
  //
  if QrSumTSdoVrtPeca.Value > 0  then
  if Geral.MB_Pergunta(
  'Deseja zerar o estoque residual de ' + FloatToStr(QrSumTSdoVrtPeca.Value) +
  ' pe�as do pallet de origem ' + Geral.FF0(QrVSPaRclCabVSPallet.Value) + '?') =
  ID_YES then
  begin
    DataEHora := DModG.ObtemAgora();
    //
    BxaPecas    := QrSumTSdoVrtPeca.Value;
    BxaPesoKg   := 0;
    //
    BxaAreaM2 := QrSumTSdoVrtArM2.Value;
    BxaAreaP2 := Geral.ConverteArea(QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    //
    BxaValorT   := 0; // Ver o que fazer!
    Result := VS_PF.ZeraEstoquePalletOrigemReclas(QrVSPaRclCabVSPallet.Value,
      QrVSPaRclCabCodigo.Value, QrVSPaRclCabMovimCod.Value,
      QrVSPaRclCabEmpresa.Value, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
      BxaValorT, DataEHora);
  end;
}
end;

procedure TFmVSReclassifOneNw3.ReopenVSPallet(Tecla: Integer; QrVSPallet,
QrVSPalRclIts: TmySQLQuery; Pallet: Integer);
begin
  if QrVSPallet <> nil then
  begin
    if (Pallet > 0) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
      'SELECT let.*, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
      ' CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS, cn1.FatorInt ',
      'FROM vspalleta let ',
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX ',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'WHERE let.Codigo=' + Geral.FF0(Pallet),
      '']);
      ReconfiguraPaineisIntMei(QrVSPallet, FPnIntMei[Tecla]);
      //
      FMaxPecas[Tecla] := QrVSPallet.FieldByName('QtdPrevPc').AsFloat;
      if (QrVSPallet.State <> dsInactive) and (QrVSPallet.RecordCount > 0) and
      (QrVSPallet.FieldByName('FatorInt').AsFloat <= 0.01) then
      begin
        if Tecla = 0 then
          Geral.MB_Aviso(
          'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!'
          + sLineBreak + 'Para evitar erros de estoque esta janela ser� fechada!');
          Close;
          Exit;
      end;
      //
      if QrVSPalRclIts <> nil then
        UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalRclIts, Dmod.MyDB, [
        'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
        'FROM vsparclitsa ',
        'WHERE Codigo=' + Geral.FF0(FCodigo),
        'AND Tecla=' + Geral.FF0(Tecla),
        'AND VSPallet=' + Geral.FF0(Pallet),
        'ORDER BY DtHrIni DESC, Controle DESC ',
        'LIMIT 1 ',
        '']);
    end else
    begin
      QrVSPallet.Close;
    end;
  end;
end;

end.
