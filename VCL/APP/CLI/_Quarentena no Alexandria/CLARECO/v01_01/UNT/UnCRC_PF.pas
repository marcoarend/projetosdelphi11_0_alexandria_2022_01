unit UnCRC_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, Variants,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  mySQLDBTables, UnDmkEnums;

type
  TUnCRC_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AbreVSSerFch(QrVSSerFch: TmySQLQuery);
    procedure BuscaDadosTabelasDoERP();//PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; Memo1, Memo2: TMemo);
    procedure EnviaDadosParaTabelasDoERP();
    //procedure RetomaCRCUniClass(Codigo: Integer);
    //
    procedure MostraFormCRCArtigos(Codigo: Integer);
    procedure MostraFormCRCCtrl();
    procedure MostraFormVSESCCab(Codigo: Integer);
    procedure MostraFormCRCUniClassCab(const CodCabLoc: Integer; var
              PallBox01, PallBox02, PallBox03, PallBox04, PallBox05,
              PallBox06, PallBox07, PallBox08, PallBox09, PallBox10,
              PallBox11, PallBox12, PallBox13, PallBox14, PallBox15,
              CodCabSet: Integer);
    procedure MostraFormCRCUniClassPal(CodCabLoc,
              PallBox01, PallBox02, PallBox03, PallBox04, PallBox05,
              PallBox06, PallBox07, PallBox08, PallBox09, PallBox10,
              PallBox11, PallBox12, PallBox13, PallBox14, PallBox15: Integer);
    //
    procedure MostraFormVSSerFch();
  end;

var
  CRC_PF: TUnCRC_PF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, CfgCadLista,
  UMySQLDB,
  VSESCCab,
  CRCCtrl, CRCArtigos, CRCUniClassCab, CRCSyncDwnERP, CRCSyncUpERP;


{ TUnCRC_PF }

procedure TUnCRC_PF.AbreVSSerFch(QrVSSerFch: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSSerFch, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vsserfch ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TUnCRC_PF.BuscaDadosTabelasDoERP(); //PB1: TProgressBar; LaAviso1, LaAviso2: TLabel; Memo1, Memo2: TMemo);
var
  I: Integer;
const
  Tabelas:  array[0..28] of String = (
    // VS
    'CouNiv1',
    'CouNiv2',
    'GraGruXCou',
    'Operacoes',
    'vsserfch',
    'vsmrtcad',
    'vsribcla',
    'VSNatArt',
    'VSNatCad',
    'VSRibCad',
    'VSCOPCab',
    'VSCOPIts',
    'VSEntiMP',
    //'VSPMOCab',  Pre�o de M.O. !!!!!
    // Grade
    'GraCorCad',
    'GraGru1',
    'GraGru2',
    'GraGru3',
    'GraGru4',
    'GraGru5',
    'GraGruC',
    'GraGruX',
    'GraGruY',
    'GraTamCad',
    'GraTamIts',
    'PrdGrupTip',
    //Centros de estoque
    'StqCenCad',
    'StqCenLoc',
    // UnidMed
    'UnidMed',
    // Entidades
    'entidades');
begin
(*
  if DBCheck.CriaFm(TFmCRCSyncDwnERP, FmCRCSyncDwnERP, afmoNegarComAviso) then
  begin
    FmCRCSyncDwnERP.CLBTabelas.Items.Clear;
    for I := Low(Tabelas) to High(Tabelas) do
    begin
      FmCRCSyncDwnERP.CLBTabelas.Items.Add(Tabelas[I]);
      FmCRCSyncDwnERP.CLBTabelas.Checked[I] := True;
    end;
    FmCRCSyncDwnERP.ShowModal;
    FmCRCSyncDwnERP.Destroy;
  end;
*)
  Application.CreateForm(TFmCRCSyncDwnERP, FmCRCSyncDwnERP);
  FmCRCSyncDwnERP.CLBTabelas.Items.Clear;
  for I := Low(Tabelas) to High(Tabelas) do
  begin
    FmCRCSyncDwnERP.CLBTabelas.Items.Add(Tabelas[I]);
    FmCRCSyncDwnERP.CLBTabelas.Checked[I] := True;
  end;
  FmCRCSyncDwnERP.ShowModal;
  FmCRCSyncDwnERP.Destroy;
end;

procedure TUnCRC_PF.EnviaDadosParaTabelasDoERP;
begin
(*
  if DBCheck.CriaFm(TFmCRCSyncUpERP, FmCRCSyncUpERP, afmoNegarComAviso) then
  begin
    FmCRCSyncUpERP.ShowModal;
    FmCRCSyncUpERP.Destroy;
  end;
*)
  Application.CreateForm(TFmCRCSyncUpERP, FmCRCSyncUpERP);
  FmCRCSyncUpERP.ShowModal;
  FmCRCSyncUpERP.Destroy;
end;

procedure TUnCRC_PF.MostraFormCRCArtigos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCRCArtigos, FmCRCArtigos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCRCArtigos.LocCod(Codigo, Codigo);
    FmCRCArtigos.ShowModal;
    FmCRCArtigos.Destroy;
  end;
end;

procedure TUnCRC_PF.MostraFormCRCCtrl();
begin
  if DBCheck.CriaFm(TFmCRCCtrl, FmCRCCtrl, afmoNegarComAviso) then
  begin
    FmCRCCtrl.ShowModal;
    FmCRCCtrl.Destroy;
  end;
end;

procedure TUnCRC_PF.MostraFormCRCUniClassCab(const CodCabLoc: Integer; var
  PallBox01, PallBox02, PallBox03, PallBox04, PallBox05,
  PallBox06, PallBox07, PallBox08, PallBox09, PallBox10,
  PallBox11, PallBox12, PallBox13, PallBox14, PallBox15, CodCabSet: Integer);
begin
  if DBCheck.CriaFm(TFmCRCUniClassCab, FmCRCUniClassCab, afmoNegarComAviso) then
  begin
    if CodCabLoc <> 0 then
      FmCRCUniClassCab.LocCod(CodCabLoc, CodCabLoc);
    FmCRCUniClassCab.ShowModal;
    CodCabSet := FmCRCUniClassCab.FCodCabSet;
    FmCRCUniClassCab.Destroy;
    //
    if CodCabSet <> 0 then
    begin
      MostraFormCRCUniClassPal(CodCabSet,
      PallBox01, PallBox02, PallBox03, PallBox04, PallBox05,
      PallBox06, PallBox07, PallBox08, PallBox09, PallBox10,
      PallBox11, PallBox12, PallBox13, PallBox14, PallBox15);
    end;
  end;
end;

procedure TUnCRC_PF.MostraFormCRCUniClassPal(CodCabLoc, PallBox01, PallBox02,
  PallBox03, PallBox04, PallBox05, PallBox06, PallBox07, PallBox08, PallBox09,
  PallBox10, PallBox11, PallBox12, PallBox13, PallBox14, PallBox15: Integer);
begin

end;

procedure TUnCRC_PF.MostraFormVSESCCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSESCCab, FmVSESCCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSESCCab.LocCod(Codigo, Codigo);
    FmVSESCCab.ShowModal;
    FmVSESCCab.Destroy;
  end;
end;

procedure TUnCRC_PF.MostraFormVSSerFch();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VSSerFch', 60, ncGerlSeq1,
  'S�ries de Fichas RMP',
  [], False, Null, [], [], False);
end;

{
procedure TUnCRC_PF.RetomaCRCUniClass(Codigo: Integer);
begin

end;
}

end.
