object FmCRCSyncUpERP: TFmCRCSyncUpERP
  Left = 339
  Top = 185
  Caption = 'CRC-SYNCR-001 :: Envio de dados ao ERP'
  ClientHeight = 629
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 288
        Height = 32
        Caption = 'Envio de dados ao ERP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 288
        Height = 32
        Caption = 'Envio de dados ao ERP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 288
        Height = 32
        Caption = 'Envio de dados ao ERP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 452
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 452
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 452
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 322
          Top = 56
          Width = 460
          Height = 394
          Align = alClient
          TabOrder = 0
          object Memo1: TMemo
            Left = 1
            Top = 90
            Width = 458
            Height = 303
            Align = alClient
            ReadOnly = True
            TabOrder = 0
          end
          object Memo2: TMemo
            Left = 1
            Top = 1
            Width = 458
            Height = 89
            Align = alTop
            ReadOnly = True
            TabOrder = 1
          end
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 56
          Width = 320
          Height = 394
          Align = alLeft
          DataSource = DsItensPorTab
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID Psq'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Tabela'
              Width = 167
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Width = 56
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 41
          Align = alTop
          TabOrder = 2
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Origem:   '
          end
          object LaOrigem: TLabel
            Left = 60
            Top = 4
            Width = 30
            Height = 13
            Caption = '..........'
          end
          object Label2: TLabel
            Left = 4
            Top = 20
            Width = 48
            Height = 13
            Caption = 'Destino:   '
          end
          object LaDestino: TLabel
            Left = 60
            Top = 20
            Width = 30
            Height = 13
            Caption = '..........'
          end
          object CkEnviarTudo: TCheckBox
            Left = 600
            Top = 12
            Width = 97
            Height = 17
            Caption = 'Enviar tudo!'
            TabOrder = 0
            OnClick = CkEnviarTudoClick
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 500
    Width = 784
    Height = 59
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 42
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 16
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 25
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 182
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 302
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtNenhumClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrTabelas: TmySQLQuery
    Database = Dmod.MyDB
    Left = 280
    Top = 92
  end
  object QrRegistros: TmySQLQuery
    Database = Dmod.MyDB
    Left = 360
    Top = 100
  end
  object QrItensPorTab: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 176
    Top = 248
    object QrItensPorTabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItensPorTabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrItensPorTabItens: TLargeintField
      FieldName = 'Itens'
    end
    object QrItensPorTabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsItensPorTab: TDataSource
    DataSet = QrItensPorTab
    Left = 176
    Top = 296
  end
  object QrUpd: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 492
    Top = 300
  end
end
