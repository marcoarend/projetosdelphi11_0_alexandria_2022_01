unit CRCUniClassCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkEditCalc;

type
  TFmCRCUniClassCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCRCArtigos: TmySQLQuery;
    QrCRCArtigosCodigo: TIntegerField;
    QrCRCArtigosNome: TWideStringField;
    DsCRCArtigos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSSerFch: TmySQLQuery;
    QrVSSerFchCodigo: TIntegerField;
    QrVSSerFchNome: TWideStringField;
    DsVSSerFch: TDataSource;
    Panel6: TPanel;
    LaPecas: TLabel;
    EdOriPecas: TdmkEdit;
    EdOriPesoKg: TdmkEdit;
    LaPeso: TLabel;
    EdOriAreaM2: TdmkEditCalc;
    LaAreaM2: TLabel;
    EdOriAreaP2: TdmkEditCalc;
    LaAreaP2: TLabel;
    Panel7: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdSerieFch: TdmkEditCB;
    Label11: TLabel;
    CBSerieFch: TdmkDBLookupComboBox;
    EdFicha: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdPallOri: TdmkEdit;
    Panel8: TPanel;
    EdPallBox01: TdmkEdit;
    Label3: TLabel;
    EdPallBox02: TdmkEdit;
    Label6: TLabel;
    EdPallBox03: TdmkEdit;
    Label8: TLabel;
    EdPallBox04: TdmkEdit;
    Label9: TLabel;
    EdPallBox05: TdmkEdit;
    Label10: TLabel;
    CBPallBox01: TdmkDBLookupComboBox;
    CBPallBox02: TdmkDBLookupComboBox;
    CBPallBox03: TdmkDBLookupComboBox;
    CBPallBox04: TdmkDBLookupComboBox;
    CBPallBox05: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdPallBox06: TdmkEdit;
    CBPallBox06: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdPallBox07: TdmkEdit;
    CBPallBox07: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdPallBox08: TdmkEdit;
    CBPallBox08: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdPallBox09: TdmkEdit;
    CBPallBox09: TdmkDBLookupComboBox;
    Label16: TLabel;
    EdPallBox10: TdmkEdit;
    CBPallBox10: TdmkDBLookupComboBox;
    Label17: TLabel;
    EdPallBox11: TdmkEdit;
    CBPallBox11: TdmkDBLookupComboBox;
    Label18: TLabel;
    EdPallBox12: TdmkEdit;
    CBPallBox12: TdmkDBLookupComboBox;
    Label19: TLabel;
    EdPallBox13: TdmkEdit;
    CBPallBox13: TdmkDBLookupComboBox;
    Label20: TLabel;
    EdPallBox14: TdmkEdit;
    CBPallBox14: TdmkDBLookupComboBox;
    Label21: TLabel;
    EdPallBox15: TdmkEdit;
    CBPallBox15: TdmkDBLookupComboBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCRCArtigosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCRCArtigosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FCodCabSet: Integer;
    //
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCRCUniClassCab: TFmCRCUniClassCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, UnCRC_PF, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCRCUniClassCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCRCUniClassCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCRCArtigosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCRCUniClassCab.DefParams;
begin
  VAR_GOTOTABELA := 'crcuniclasscab';
  VAR_GOTOMYSQLTABLE := QrCRCArtigos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM crcuniclasscab');
  VAR_SQLx.Add('WHERE StatSinc<>' + Geral.FF0(Integer(stDel)));
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCRCUniClassCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCRCUniClassCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCRCUniClassCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmCRCUniClassCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCRCUniClassCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCRCUniClassCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCRCUniClassCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCRCUniClassCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCRCUniClassCab.BtAlteraClick(Sender: TObject);
begin
  if (QrCRCArtigos.State <> dsInactive) and (QrCRCArtigos.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCRCArtigos, [PnDados],
      [PnEdita], EdSerieFch, ImgTipo, 'crcuniclasscab');
  end;
end;

procedure TFmCRCUniClassCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCRCArtigosCodigo.Value;
  Close;
end;

procedure TFmCRCUniClassCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, SerieFch, Ficha, PallOri, ArtigoOri, PallBox01, PallBox02, PallBox03,
  PallBox04, PallBox05, PallBox06, PallBox07, PallBox08, PallBox09, PallBox10,
  PallBox11, PallBox12, PallBox13, PallBox14, PallBox15, StatSinc: Integer;
  //DstPecas, DstAreaM2, DstAreaP2, DstPesoKg
  OriPecas, OriAreaM2, OriAreaP2, OriPesoKg: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  SerieFch       := EdSerieFch.ValueVariant;
  Ficha          := EdFicha.ValueVariant;
  PallOri        := EdPallOri.ValueVariant;
  ArtigoOri      := 0;//EdArtigoOri.ValueVariant;
  OriPecas       := EdOriPecas.ValueVariant;
  OriAreaM2      := EdOriAreaM2.ValueVariant;
  OriAreaP2      := EdOriAreaP2.ValueVariant;
  OriPesoKg      := EdOriPesoKg.ValueVariant;
  //DstPecas       := EdDstPecas.ValueVariant;
  //DstAreaM2      := EdDstAreaM2.ValueVariant;
  //DstAreaP2      := EdDstAreaP2.ValueVariant;
  //DstPesoKg      := EdDstPesoKg.ValueVariant;
  PallBox01      := EdPallBox01.ValueVariant;
  PallBox02      := EdPallBox02.ValueVariant;
  PallBox03      := EdPallBox03.ValueVariant;
  PallBox04      := EdPallBox04.ValueVariant;
  PallBox05      := EdPallBox05.ValueVariant;
  PallBox06      := EdPallBox06.ValueVariant;
  PallBox07      := EdPallBox07.ValueVariant;
  PallBox08      := EdPallBox08.ValueVariant;
  PallBox09      := EdPallBox09.ValueVariant;
  PallBox10      := EdPallBox10.ValueVariant;
  PallBox11      := EdPallBox11.ValueVariant;
  PallBox12      := EdPallBox12.ValueVariant;
  PallBox13      := EdPallBox13.ValueVariant;
  PallBox14      := EdPallBox14.ValueVariant;
  PallBox15      := EdPallBox15.ValueVariant;
  StatSinc       := Integer(ImgTipo.SQLType);
  //
  Codigo := UMyMod.BPGS1I32('crcuniclasscab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'crcuniclasscab', False, [
  'SerieFch', 'Ficha', 'PallOri',
  'ArtigoOri', 'OriPecas', 'OriAreaM2',
  'OriAreaP2', 'OriPesoKg', (*'DstPecas',
  'DstAreaM2', 'DstAreaP2', 'DstPesoKg',*)
  'PallBox01', 'PallBox02', 'PallBox03',
  'PallBox04', 'PallBox05', 'PallBox06',
  'PallBox07', 'PallBox08', 'PallBox09',
  'PallBox10', 'PallBox11', 'PallBox12',
  'PallBox13', 'PallBox14', 'PallBox15',
  'StatSinc'], [
  'Codigo'], [
  SerieFch, Ficha, PallOri,
  ArtigoOri, OriPecas, OriAreaM2,
  OriAreaP2, OriPesoKg, (*DstPecas,
  DstAreaM2, DstAreaP2, DstPesoKg,*)
  PallBox01, PallBox02, PallBox03,
  PallBox04, PallBox05, PallBox06,
  PallBox07, PallBox08, PallBox09,
  PallBox10, PallBox11, PallBox12,
  PallBox13, PallBox14, PallBox15,
  StatSinc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    FCodCabSet := Codigo;
  end;
end;

procedure TFmCRCUniClassCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'crcuniclasscab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCRCUniClassCab.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCRCArtigos, [PnDados],
  [PnEdita], EdSerieFch, ImgTipo, 'crcuniclasscab');
end;

procedure TFmCRCUniClassCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  //
  FCodCabSet := 0;
  CRC_PF.AbreVSSerFch(QrVSSerFch);
end;

procedure TFmCRCUniClassCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCRCArtigosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCRCUniClassCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCRCUniClassCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCRCArtigosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCRCUniClassCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCRCUniClassCab.QrCRCArtigosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCRCUniClassCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCRCUniClassCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCRCArtigosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'crcuniclasscab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCRCUniClassCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCRCUniClassCab.QrCRCArtigosBeforeOpen(DataSet: TDataSet);
begin
  QrCRCArtigosCodigo.DisplayFormat := FFormatFloat;
end;

end.

