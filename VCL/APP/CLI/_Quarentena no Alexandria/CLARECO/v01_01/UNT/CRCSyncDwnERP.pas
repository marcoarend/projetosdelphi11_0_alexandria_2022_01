unit CRCSyncDwnERP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  Vcl.CheckLst, mySQLDbTables;

type
  TFmCRCSyncDwnERP = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CLBTabelas: TCheckListBox;
    Panel5: TPanel;
    Memo1: TMemo;
    Memo2: TMemo;
    mySQLDatabase1: TmySQLDatabase;
    PB1: TProgressBar;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaDadosTabelasDoERP(PB1: TProgressBar; LaAviso1, LaAviso2:
              TLabel; Memo1, Memo2: TMemo);
    procedure CheckItems(Checka: Boolean);
  public
    { Public declarations }
  end;

  var
  FmCRCSyncDwnERP: TFmCRCSyncDwnERP;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UMySQLDB;

{$R *.DFM}

procedure TFmCRCSyncDwnERP.BtNenhumClick(Sender: TObject);
begin
  CheckItems(False);
end;

procedure TFmCRCSyncDwnERP.BtOKClick(Sender: TObject);
begin
  BuscaDadosTabelasDoERP(PB1, LaAviso1, LaAviso2, Memo1, Memo2);
end;

procedure TFmCRCSyncDwnERP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCRCSyncDwnERP.BtTodosClick(Sender: TObject);
begin
  CheckItems(True);
end;

procedure TFmCRCSyncDwnERP.BuscaDadosTabelasDoERP(PB1: TProgressBar; LaAviso1,
  LaAviso2: TLabel; Memo1, Memo2: TMemo);
var
  I, N, Falhas: Integer;
  Tempo: TDateTime;
  //
  function BuscaDadosTabela(TbOri, SQL_WHERE_Ori: String): Boolean;
  const
    //
    Ori_User = 'root';
    Ori_Pass = CO_USERSPNOW;
    //
    Destino = 'C:\Dermatek\MySQL_DataDwn';
    Des_Host = 'localhost';
    Des_User = 'root';
    Des_Pass = CO_USERSPNOW;
  var
    Ori_Host, Ori_DB, Des_DB: String;  // = '216.117.142.81'; = 'bluederm_meza'; = 'clareco_meza';
    Ori_Port, Des_Porta: Integer; // = 3306; = 3306;
    DBOri, DBDes: TmySQLDataBase;
    ArqNome, PathMySQL, Msg, Tabela, ArqResult: String;
    Parcial: TDateTime;
  begin
    Result    := False;
    Ori_Host  := Dmod.QrCrcCtrlERP_Host.Value;
    Ori_DB    := Dmod.QrCrcCtrlERP_DBName.Value;
    Ori_Port  := Dmod.QrCrcCtrlERP_DBPorta.Value;
    Des_DB    := Lowercase(TMeuDB);
    Des_Porta := VAR_PORTA;
    DBOri     := TmySQLDataBase.Create(Dmod);
    DBDes     := TmySQLDataBase.Create(Dmod);
    try
      ArqNome   := Geral.FDT(Now, 26) + '.sql';
      PathMySQL := USQLDB.ObtemPathMySQLBin(Dmod.MyDB);
      Tabela    := ' --tables ' + Lowercase(TbOri) + ' ';
      if Trim(SQL_WHERE_Ori) <> '' then
      Tabela := Tabela +  ' --' + SQL_WHERE_Ori + ' ';
      //
      ArqResult := USQLDB.Backup_Executa(DBOri, PathMySQL, Destino, ArqNome,
                     Ori_Host, Ori_DB, Ori_User, Ori_Pass, Ori_Port, Memo2, False,
                     False, ndctDropAndCreate, Msg, nil, Tabela);
      //
      if ArqResult <> '' then
      begin
        //Executa restaura
        try
          UnDmkDAC_PF.ConectaMyDB_DAC(DBDes, Des_DB, Des_Host,
            Des_Porta, Des_User, Des_Pass, True, True, True, True);
          Result := True;
        except
          System.SysUtils.DeleteFile(ArqResult);
          //
          Geral.MB_Erro('Falha ao conectar no banco de dados destino!');
          Result := False;
          Falhas := Falhas + 1;
          Exit;
        end;
        if USQLDB.Backup_Restaura(DBDes, ArqResult, False, Msg, nil) then
        begin
          Parcial := Now() - Tempo;
          Memo1.Lines.Add(FormatDateTime('h:nn:ss', Parcial) + ' Item ' +
          Geral.FF0(I) + ' de ' + Geral.FF0(N) +
          ' > Sincronismo realizado com sucesso na tabela ' + TbOri);
        end else
        begin
          Falhas := Falhas + 1;
          Geral.MB_Erro('Falha ao restaurar backup!');
          Memo1.Lines.Add('Falha "B" ao restaurar backup na tabela ' + TbOri);
        end;
      end else
      begin
        Falhas := Falhas + 1;
        Geral.MB_Erro('Falha ao realizar backup de dados!');
        Memo1.Lines.Add('Falha "A" ao restaurar backup na tabela ' + TbOri);
      end;
    finally
      DBDes.Free;
      DBOri.Free;
    end;
  end;
var
  Tab, SQL, sTempo: String;
  MeVisible1, MeVisible2: Boolean;
begin
  Tempo := Now();
  Falhas := 0;
  MeVisible1 := Memo1.Visible;
  MeVisible2 := Memo2.Visible;
  Memo1.Visible := True;
  Memo2.Visible := True;
  Screen.Cursor := crHourGlass;
  try
    N := CLBTabelas.Items.Count;
    PB1.Position := 0;
    PB1.Max := N;
    SQL := ''; // where="codigo=1"
    //BuscaDadosTabela('unidmed', 'where="codigo=-1"');
    for I := 0 to N - 1 do
    begin
      if CLBTabelas.Checked[I] then
      begin
        Tab := CLBTabelas.Items[I];
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Atualizando tabela ' + Tab);
        //
        if not BuscaDadosTabela(Tab, SQL) then
          Exit;
      end;
    end;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, '');
    Memo1.Visible := MeVisible1;
    Memo2.Visible := MeVisible2;
    if Falhas > 0 then
      Geral.MB_Erro('Atualização finalizada com ' + Geral.FF0(Falhas) + ' falha(s)')
    else
    begin
      Tempo := Now() - Tempo;
      Geral.MB_Info('Atualização finalizada! Tempo: ' +
      FormatDateTime('h:nn:ss', Tempo));
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCRCSyncDwnERP.CheckItems(Checka: Boolean);
var
  I, N: Integer;
begin
  N := CLBTabelas.Items.Count;
  for I := 0 to N - 1 do
    CLBTabelas.Checked[I] := Checka;
end;

procedure TFmCRCSyncDwnERP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCRCSyncDwnERP.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCRCSyncDwnERP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
