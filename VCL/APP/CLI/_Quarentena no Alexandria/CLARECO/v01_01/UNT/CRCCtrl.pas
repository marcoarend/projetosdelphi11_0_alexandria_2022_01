unit CRCCtrl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkCheckBox, dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, dmkDBGrid,
  frxClass, Vcl.Menus, frxDBSet;

type
  TFmCRCCtrl = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrCRCCtrl: TmySQLQuery;
    QrCRCCtrlCodigo: TIntegerField;
    DsCRCCtrl: TDataSource;
    VUGraCorCad: TdmkValUsu;
    VUGraTamCad: TdmkValUsu;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    VUGraGru1: TdmkValUsu;
    Panel5: TPanel;
    Label1: TLabel;
    EdServerID: TdmkEdit;
    QrCRCCtrlServerID: TIntegerField;
    TabSheet2: TTabSheet;
    Panel10: TPanel;
    Label12: TLabel;
    CkInfoMulFrnImpVS: TdmkCheckBox;
    CkCanAltBalVS: TdmkCheckBox;
    RGQtdBoxClas: TdmkRadioGroup;
    EdVSImpRandStr: TdmkEdit;
    CGNObrigNFeVS: TdmkCheckGroup;
    CkVSWarSemNF: TdmkCheckBox;
    CkVSWarNoFrn: TdmkCheckBox;
    CkVSInsPalManu: TdmkCheckBox;
    Label2: TLabel;
    EdERP_Host: TdmkEdit;
    Label3: TLabel;
    EdERP_DBName: TdmkEdit;
    Label4: TLabel;
    EdERP_DBPorta: TdmkEdit;
    QrCRCCtrlCanAltBalVS: TSmallintField;
    QrCRCCtrlInfoMulFrnImpVS: TSmallintField;
    QrCRCCtrlQtdBoxClas: TSmallintField;
    QrCRCCtrlVSWarSemNF: TSmallintField;
    QrCRCCtrlVSWarNoFrn: TSmallintField;
    QrCRCCtrlVSInsPalManu: TSmallintField;
    QrCRCCtrlVSImpRandStr: TWideStringField;
    QrCRCCtrlERP_Host: TWideStringField;
    QrCRCCtrlERP_DBName: TWideStringField;
    QrCRCCtrlERP_DBPorta: TIntegerField;
    EdERP_DBRepos: TdmkEdit;
    Label5: TLabel;
    QrCRCCtrlERP_DBRepos: TWideStringField;
    QrCRCCtrlNObrigNFeVS: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrCRCCtrlAfterOpen(DataSet: TDataSet);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    //
    procedure ReopenCRCCtrl();
  public
    { Public declarations }
  end;

  var
  FmCRCCtrl: TFmCRCCtrl;

implementation

uses UnMyObjects, UMySQLModule, Module, MyDBCheck, DmkDAC_PF, MyListas,
  UnVS_CRC_PF;

{$R *.DFM}

procedure TFmCRCCtrl.BtOKClick(Sender: TObject);
var
  VSImpRandStr, ERP_Host, ERP_DBName, ERP_DBRepos: String;
  Codigo, ServerID, CanAltBalVS, InfoMulFrnImpVS, QtdBoxClas, VSWarSemNF,
  VSWarNoFrn, VSInsPalManu, AWServerID, AWStatSinc, ERP_DBPorta, NObrigNFeVS: Integer;
  SQLType: TSQLType;
begin
  SQLType           := ImgTipo.SQLType;
  Codigo            := 1;
  ServerID          := EdServerID.ValueVariant;
  CanAltBalVS       := Geral.BoolToInt(CkCanAltBalVS.Checked);
  InfoMulFrnImpVS   := Geral.BoolToInt(CkInfoMulFrnImpVS.Checked);
  VSImpRandStr      := EdVSImpRandStr.Text;
  NObrigNFeVS       := CGNObrigNFeVS.Value;
  VSWarSemNF        := Geral.BoolToInt(CkVSWarSemNF.Checked);
  VSWarNoFrn        := Geral.BoolToInt(CkVSWarNoFrn.Checked);
  VSInsPalManu      := Geral.BoolToInt(CkVSInsPalManu.Checked);
  QtdBoxClas        := RGQtdBoxClas.ItemIndex;
  ERP_Host          := EdERP_Host.Text;
  ERP_DBName        := EdERP_DBName.Text;
  ERP_DBPorta       := EdERP_DBPorta.ValueVariant;
  ERP_DBRepos       := EdERP_DBRepos.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'crcctrl', False, [
  'ServerID', 'CanAltBalVS', 'InfoMulFrnImpVS',
  'QtdBoxClas', 'VSWarSemNF', 'VSWarNoFrn',
  'VSInsPalManu', 'VSImpRandStr', 'ERP_DBRepos',
  'ERP_Host', 'ERP_DBName', 'ERP_DBPorta',
  'NObrigNFeVS'], [
  'Codigo'], [
  ServerID, CanAltBalVS, InfoMulFrnImpVS,
  QtdBoxClas, VSWarSemNF, VSWarNoFrn,
  VSInsPalManu, VSImpRandStr, ERP_DBRepos,
  ERP_Host, ERP_DBName, ERP_DBPorta,
  NObrigNFeVS], [
  Codigo], True) then
  begin
    Dmod.ReopenCRCCtrl();
    Close;
  end;
end;

procedure TFmCRCCtrl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCRCCtrl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCRCCtrl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CGNObrigNFeVS.Items := VS_CRC_PF.ObtemListaOperacoes();
  ReopenCRCCtrl();
  //
  EdServerID.ValueVariant     := QrCRCCtrlServerID.Value;
  CkCanAltBalVS.Checked       := Geral.IntToBool(QrCRCCtrlCanAltBalVS.Value);
  CkInfoMulFrnImpVS.Checked   := Geral.IntToBool(QrCRCCtrlInfoMulFrnImpVS.Value);
  EdVSImpRandStr.Text         := QrCRCCtrlVSImpRandStr.Value;
  CGNObrigNFeVS.Value         := QrCRCCtrlNObrigNFeVS.Value;
  CkVSWarSemNF.Checked        := Geral.IntToBool(QrCRCCtrlVSWarSemNF.Value);
  CkVSWarNoFrn.Checked        := Geral.IntToBool(QrCRCCtrlVSWarNoFrn.Value);
  CkVSInsPalManu.Checked      := Geral.IntToBool(QrCRCCtrlVSInsPalManu.Value);
  RGQtdBoxClas.ItemIndex      := QrCRCCtrlQtdBoxClas.Value;
  EdERP_Host.Text             := QrCRCCtrlERP_Host.Value;
  EdERP_DBName.Text           := QrCRCCtrlERP_DBName.Value;
  EdERP_DBPorta.ValueVariant  := QrCRCCtrlERP_DBPorta.Value;
  EdERP_DBRepos.Text          := QrCRCCtrlERP_DBRepos.Value;
end;

procedure TFmCRCCtrl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCRCCtrl.QrCRCCtrlAfterOpen(DataSet: TDataSet);
begin
  if Dmod.QrCRCCtrl.RecordCount > 0 then
    ImgTipo.SQLType := stUpd
  else
    ImgTipo.SQLType := stIns;
end;

procedure TFmCRCCtrl.ReopenCRCCtrl;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCRCCtrl, Dmod.MyDB, [
  'SELECT * FROM crcctrl ',
  'WHERE Codigo=1 ',
  '']);
end;

end.

