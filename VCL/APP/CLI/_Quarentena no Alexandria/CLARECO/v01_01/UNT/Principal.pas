unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, Vcl.Grids, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Menus,
  // Terceiros
  WinSkinStore, WinSkinData,
  AdvToolBar, AdvShapeButton, AdvGlowButton, AdvToolBarStylers,
  mySQLDbTables,
  // Dermatek
  dmkPageControl, dmkGeral, UnDmkEnums, UnInternalConsts, UnDmkProcFunc, ZCF2,
  MyListas, MyDBCheck, UnDmkWeb, DmkDAC_PF, AdvMenus, UnProjGroup_Vars,
  UnProjGroup_Consts, UnGrl_Vars, UnGrl_Geral;

type
  TFmPrincipal = class(TForm)
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager23: TAdvPage;
    AdvToolBar1: TAdvToolBar;
    AGBEntidade: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar7: TAdvToolBar;
    AGBMalaDireta: TAdvGlowButton;
    AGBListaEnti: TAdvGlowButton;
    AdvPage1: TAdvPage;
    AdvToolBar20: TAdvToolBar;
    AGBNovasVersoes: TAdvGlowButton;
    AGBRevertVersao: TAdvGlowButton;
    AdvToolBar8: TAdvToolBar;
    AGBVerifiDB: TAdvGlowButton;
    AGBBackup: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AGBMatriz: TAdvGlowButton;
    AGBOpcoes: TAdvGlowButton;
    AGBFiliais: TAdvGlowButton;
    AdvToolBar25: TAdvToolBar;
    AGBImagem: TAdvGlowMenuButton;
    AGBMenu: TAdvGlowMenuButton;
    AGBTema: TAdvGlowButton;
    AdvPage4: TAdvPage;
    AdvToolBar10: TAdvToolBar;
    AGBSuporte: TAdvGlowButton;
    AGBSobre: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar2: TAdvQuickAccessToolBar;
    ATBLogoff: TAdvToolBarButton;
    ATBVerificaNovaVersao: TAdvToolBarButton;
    ATBFavoritos: TAdvToolBarButton;
    ATBBackup: TAdvToolBarButton;
    ATBVerifi: TAdvToolBarButton;
    ATBMaximizaMenu: TAdvToolBarButton;
    ATBSuporte: TAdvToolBarButton;
    ATBBLastWork: TAdvToolBarButton;
    Memo3: TMemo;
    PageControl1: TdmkPageControl;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    TimerPingServer: TTimer;
    TmSuporte: TTimer;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Chamadasrecebidas1: TMenuItem;
    Chamadasatendidas1: TMenuItem;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    TySuporte: TTrayIcon;
    BalloonHint1: TBalloonHint;
    Timer1: TTimer;
    TimerAlphaBlend: TTimer;
    TimerIdle: TTimer;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    AdvPMVerificaBD: TAdvPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasterceiros1: TMenuItem;
    MenuItem2: TMenuItem;
    GerenciaCB41: TMenuItem;
    AdvPage2: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AGBCRCArtigos: TAdvGlowButton;
    AGBCRCCtrl: TAdvGlowButton;
    AGBVSSerFch: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AGBSincroDown: TAdvToolBarButton;
    TabSheet1: TTabSheet;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAvisoA1: TLabel;
    LaAvisoA2: TLabel;
    PB1: TProgressBar;
    APMVSRclArt: TAdvPopupMenu;
    Prepara1: TMenuItem;
    MenuItem13: TMenuItem;
    N4: TMenuItem;
    ReclassificaoMassiva1: TMenuItem;
    N8: TMenuItem;
    Gerenciamento1: TMenuItem;
    GerenciamentodePrReclasse1: TMenuItem;
    N9: TMenuItem;
    AGBVSRclArt: TAdvGlowMenuButton;
    APMVSClaArt: TAdvPopupMenu;
    ComearnovaOC1: TMenuItem;
    RecomearclkassificaodeOCjconfigurada1: TMenuItem;
    RecomearclkassificaodeOCjconfigurada2: TMenuItem;
    N5: TMenuItem;
    ClasificaoMassiva1: TMenuItem;
    N7: TMenuItem;
    Gerenciamento2: TMenuItem;
    N6: TMenuItem;
    AGBVSClaArt: TAdvGlowMenuButton;
    ClassificarCurtidoporpallet1: TMenuItem;
    N2: TMenuItem;
    AGBEntrada: TAdvGlowButton;
    AGBSincroUp: TAdvToolBarButton;
    AGBVSPallet: TAdvGlowButton;
    AGBVSInnCab: TAdvGlowButton;
    AGBVSGeraArtCab: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    APMVSImprime: TAdvPopupMenu;
    N26EstoqueCustoIntegrado1: TMenuItem;
    AGBVSMovImp: TAdvGlowMenuButton;
    AGBVSOutCab: TAdvGlowButton;
    AGBVSOpeCab: TAdvGlowButton;
    AGBVSPWECab: TAdvGlowButton;
    AGBVSPlCCab: TAdvGlowButton;
    AGBVSTrfLocCab: TAdvGlowButton;
    APMVSDvlRtb: TAdvPopupMenu;
    DevoluoDefinitivaAoEstoque1: TMenuItem;
    RetornoParaRetrabalhoEPosteriorReenvio1: TMenuItem;
    AGBVSDvlRtb: TAdvGlowMenuButton;
    procedure FormCreate(Sender: TObject);
    procedure AGBEntidadeClick(Sender: TObject);
    procedure TimerPingServerTimer(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure Chamadasrecebidas1Click(Sender: TObject);
    procedure Chamadasatendidas1Click(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure TimerAlphaBlendTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure AGBListaEntiClick(Sender: TObject);
    procedure AGBMalaDiretaClick(Sender: TObject);
    procedure AGBNovasVersoesClick(Sender: TObject);
    procedure AGBRevertVersaoClick(Sender: TObject);
    procedure AGBBackupClick(Sender: TObject);
    procedure AGBOpcoesClick(Sender: TObject);
    procedure AGBMatrizClick(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AGBTemaClick(Sender: TObject);
    procedure ATBMaximizaMenuClick(Sender: TObject);
    procedure ATBSuporteClick(Sender: TObject);
    procedure ATBBackupClick(Sender: TObject);
    procedure ATBFavoritosClick(Sender: TObject);
    procedure ATBLogoffClick(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure AGBSuporteClick(Sender: TObject);
    procedure AGBSobreClick(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure VerificaTabelasterceiros1Click(Sender: TObject);
    procedure AGBCRCArtigosClick(Sender: TObject);
    procedure AGBCRCCtrlClick(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AGBVSSerFchClick(Sender: TObject);
    procedure AGBSincroDownClick(Sender: TObject);
    procedure Prepara1Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
    procedure GerenciamentodePrReclasse1Click(Sender: TObject);
    procedure ComearnovaOC1Click(Sender: TObject);
    procedure RecomearclkassificaodeOCjconfigurada1Click(Sender: TObject);
    procedure RecomearclkassificaodeOCjconfigurada2Click(Sender: TObject);
    procedure ClassificarCurtidoporpallet1Click(Sender: TObject);
    procedure Gerenciamento2Click(Sender: TObject);
    procedure ClasificaoMassiva1Click(Sender: TObject);
    procedure AGBEntradaClick(Sender: TObject);
    procedure AGBSincroUpClick(Sender: TObject);
    procedure ReclassificaoMassiva1Click(Sender: TObject);
    procedure AGBVSPalletClick(Sender: TObject);
    procedure AGBVSInnCabClick(Sender: TObject);
    procedure AGBVSGeraArtCabClick(Sender: TObject);
    procedure N26EstoqueCustoIntegrado1Click(Sender: TObject);
    procedure AGBVSOutCabClick(Sender: TObject);
    procedure AGBVSPlCCabClick(Sender: TObject);
    procedure AGBVSOpeCabClick(Sender: TObject);
    procedure AGBVSPWECabClick(Sender: TObject);
    procedure AGBVSTrfLocCabClick(Sender: TObject);
  private
    { Private declarations }
    FBorda, FCursorPosX, FCursorPosY: Integer;
    FMenuMaximizado, FALiberar: Boolean;
    FAdvToolBarPager_Hei_Max: Integer;
    //
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure MostraLogoff();
    procedure ShowHint(Sender: TObject);
  public
    { Public declarations }
    FTipoNovoEnti, FEntInt: Integer;
    FLDataIni, FLDataFim: TDateTime;
    //
    FModBloq_EntCliInt, FModBloq_CliInt, FModBloq_Peri, FModBloq_FatID,
    FModBloq_Lancto: Integer;
    FModBloq_TabLctA: String;
    FModBloq_FatNum: Double;

    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade:
              Integer; Aba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    procedure AcoesIniciaisDoAplicativo();
    procedure DefineVarsCliInt(CliInt: Integer);
    //procedure MostraFormDescanso();
    procedure ReCaptionComponentesDeForm(Form: TForm);
    function  VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;
  VAR_SetorCal, VAR_SetorCur: Integer;

implementation

uses
  ModuleGeral, UnMyObjects, Module, UMySQLModule, Feriados,
{$IfNDef NAO_BINA} UnBina_PF, {$EndIf}
  // , CashTabs
  UnEntities, Descanso, LinkRankSkin, UnLic_Dmk, MalaDireta, FavoritosG, About,
  UnEmpresas_Jan, UnALL_Jan, UnCRC_PF, UnVS_CRC_PF,
  ClaReCo_Dmk, VSPalletManual;

{$R *.dfm}

{ TFmPrincipal }


procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade: Integer; Aba: Boolean);

begin
  //
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
  Codigo: Integer; Grade: TStringGrid);
begin
//
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      //DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Geral');
      DModG.MyPID_DB_Cria();
      //
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando favoritos');
      DModG.CriaFavoritos(AdvToolBarPager1, LaAviso2, LaAviso1, AGBEntidade, FmPrincipal);
      //
  {[***VerSePrecisa***]  Importa��o de dados de outro sistema - Ver B U G S T R O L
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando Module Anterior');
      DmABD_Mod.MyABD_Cria();
}
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Atualizando atrelamentos de contatos');
      DModG.AtualizaEntiConEnt();
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Setando ping ao servidor');
      TimerPingServer.Enabled := VAR_SERVIDOR = 2;
      //
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando feriados futuros');
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
  {[***VerSePrecisa***]  Ver B U G S T R O L
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Carregando paletas de cores de listas de status');
      Dmod.PoeEmMemoryCoresStatusAvul();
      Dmod.PoeEmMemoryCoresStatusOS();
      //
      // Deve ser depois da paleta de cores! > Dmod.PoeEmMemoryCoresStatusOS();
      if Dmod.QrOpcoesBugsSWTAgenda.Value = 1 then
      begin
        MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Criando configurando agenda em guia (aba)');
        MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1, False, True);
      end;
}
      //
  {[***      if DModG.QrCtrlGeralAtualizouPreEmail.AsInteger = 0 then
        DModG.AtualizaPreEmail;
      if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
      begin
        try
          GBAvisos1.Visible := True;
          Entities.AtualizaEntidadesParaEntidade2(PB1, Dmod.MyDB, DModG.AllID_DB);
        finally
          GBAvisos1.Visible := False;
        end;
      end;
}

      //

  {[***VerSePrecisa***]  Renova��es de Contratos! - Ver B U G S T R O L
      // Deixar mais para o final!!
      MyObjects.Informa2(LaAviso2, LaAviso1, True, 'Verificando a��es e renova��es');
      //DmModOS.VerificaFormulasFilhas(False);
      if DBCheck.CriaFm(TFmAllToRenew, FmAllToRenew, afmoNegarComAviso) then
      begin
        if FmAllToRenew.ItensAbertos() > 0 then
          FmAllToRenew.ShowModal;
        FmAllToRenew.Destroy;
      end;
      //
}
      DefineVarsCliInt(VAR_LIB_EMPRESA_SEL);
      //
  {[***VerSePrecisa***]  Renova��es de Contratos! - Ver B U G S T R O L
      RecriaTiposDeProdutoPadrao;
}
  {[***VerSePrecisa***]  DmodG.ConfiguraIconeAplicativo;
}
      //
{$IfDef UsaWSuport}
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      if DmkWeb.RemoteConnection() then
      begin
        if VerificaNovasVersoes(True) then
          DmkWeb.MostraBalloonHintMenuTopo(ATBVerificaNovaVersao,
          BalloonHint1, 'H� uma nova vers�o!', 'Clique aqui para atualizar!');
      end;
{$EndIf}
    end;
  finally
{$IfDef UsaWSuport}
    TmSuporte.Enabled := True;
{$EndIf}
    MyObjects.Informa2(LaAviso2, LaAviso1, False,
      Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
begin
  CRC_PF.MostraFormCRCArtigos(0);
end;

procedure TFmPrincipal.AGBListaEntiClick(Sender: TObject);
begin
  Entities.MostraFormEntidadesImp();
end;

procedure TFmPrincipal.AGBTemaClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AGBVSGeraArtCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGeraArt(0, 0);
end;

procedure TFmPrincipal.AGBVSInnCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSInnCab(0, 0, 0);
end;

procedure TFmPrincipal.AGBVSOpeCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSOpeCab(0);
end;

procedure TFmPrincipal.AGBVSOutCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSOutCab(0, 0);
end;

procedure TFmPrincipal.AGBVSPalletClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPallet(0);
{
  if DBCheck.CriaFm(TFmVSPalletManual, FmVSPalletManual, afmoNegarComAviso) then
  begin
    FmVSPalletManual.ImgTipo.SQLType := stIns;
    //FmVSPalletManual.EdPallet.ValueVariant := PalletNew;
    //FmVSPalletManual.EdPallet.ValueVariant := PalletNew;
    FmVSPalletManual.ShowModal;
(*
    if FmVSPalletManual.FPermiteIncluir then
      Result := FmVSPalletManual.FNumNewPallet;
    FmVSPalletManual.Destroy;
    if Result = 0 then
      Exit;
*)
  end;
}
end;

procedure TFmPrincipal.AGBVSPlCCabClick(Sender: TObject);
const
  Codigo   = 0;
  Controle = 0;
begin
  VS_CRC_PF.MostraFormVSPlCCab(Codigo, Controle);
end;

procedure TFmPrincipal.AGBVSPWECabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPWECab(0);
end;

procedure TFmPrincipal.AGBVSSerFchClick(Sender: TObject);
begin
   CRC_PF.MostraFormVSSerFch();
end;

procedure TFmPrincipal.AGBVSTrfLocCabClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSTrfLocCab(0);
end;

procedure TFmPrincipal.AGBNovasVersoesClick(Sender: TObject);
begin
  VerificaNovasVersoes(False);
end;

procedure TFmPrincipal.AGBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AGBCRCArtigosClick(Sender: TObject);
begin
  CRC_PF.MostraFormCRCArtigos(0);
end;

procedure TFmPrincipal.AGBCRCCtrlClick(Sender: TObject);
begin
  CRC_PF.MostraFormCRCCtrl();
end;

procedure TFmPrincipal.AGBMatrizClick(Sender: TObject);
begin
  Empresas_Jan.MostraFormMatriz();
end;

procedure TFmPrincipal.AGBOpcoesClick(Sender: TObject);
begin
//
end;

procedure TFmPrincipal.AGBRevertVersaoClick(Sender: TObject);
begin
  Lic_Dmk.ReverteVersao('ClaRecCo', Handle);
end;

procedure TFmPrincipal.AGBSincroDownClick(Sender: TObject);
begin
  CRC_PF.BuscaDadosTabelasDoERP(); //PB1, LaAviso1, LaAviso2, Memo1, Memo2);
end;

procedure TFmPrincipal.AGBSincroUpClick(Sender: TObject);
begin
  CRC_PF.EnviaDadosParaTabelasDoERP();
end;

procedure TFmPrincipal.AGBSobreClick(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AGBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AGBMalaDiretaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.ATBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.ATBMaximizaMenuClick(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.ATBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.ATBLogoffClick(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.ATBFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPager1, LaAviso1, LaAviso2, AGBNovasVersoes, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGBEntidadeClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmPrincipal.AGBEntradaClick(Sender: TObject);
begin
  CRC_PF.MostraFormVSESCCab(0);
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  Empresas_Jan.MostraFormParamsEmp();
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.Chamadasatendidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouB();
{$EndIf}
end;

procedure TFmPrincipal.Chamadasrecebidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouA();
{$EndIf}
end;

procedure TFmPrincipal.ClasificaoMassiva1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VS_PF.MostraFormVSPaMulCabA(0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmPrincipal.ClassificarCurtidoporpallet1Click(Sender: TObject);
var
  Pallets: TClass15Int;
begin
{$IfDef sAllVS}
  VS_CRC_PF.LimpaArray15Int(Pallets);
  VS_CRC_PF.MostraFormVSClaPalPrpQnz(Pallets, nil, 0, 0);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmPrincipal.ComearnovaOC1Click(Sender: TObject);
var
  Pallets: TClass15Int;
begin
  VS_CRC_PF.LimpaArray15Int(Pallets);
  VS_CRC_PF.MostraFormVSClaArtPrpQnz(Pallets, nil, 0, 0);
end;

procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := CliInt;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCliIntUni, Dmod.MyDB);
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ZZTerminate := True;
  Application.Terminate;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
begin
  //VAR_USA_MODULO_CRO := True;
  //
  //
  VAR_SALVA_REGISTROS_EXCLUIDOS        := True;
  //
  VAR_TemContratoMensalidade_FldCodigo := 'Codigo';
  VAR_TemContratoMensalidade_FldNome   := 'Nome';
  VAR_TemContratoMensalidade_TabNome    := 'contratos';
  //
  dmkPF.AcoesAntesDeIniciarApp_dmk();
  //
  FBorda := (Width - ClientWidth) div 2;
  //
  VAR_TIPO_TAB_LCT := 1;
{$IfNDef NO_FINANCEIRO}
  VAR_MULTIPLAS_TAB_LCT := True;
{$EndIf}
  //
  GERAL_MODELO_FORM_ENTIDADES := fmcadEntidade2;
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  VAR_KIND_DEPTO := kdOS1;
  VAR_LA_PRINCIPAL1   := LaAviso1;
  VAR_LA_PRINCIPAL2   := LaAviso1;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  MyObjects.SkinMenu(AdvToolBarOfficeStyler1, MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  // Deixar invis�vel
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  PageControl1.Align := alClient;
  Width := 1600;
  Height := 870;
  FAdvToolBarPager_Hei_Max := AdvToolBarPager1.Height; // 225
  //
  //  Descanso
  //MostraFormDescanso();
  //  Di�rio
  FmPrincipal.WindowState := wsMaximized;
  //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPager1);
  // d� erro!! vou abrir no AcoesIniciaisDoAplicativo();
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
  AdvToolBarPager1.Collaps;
  //
  FModBloq_EntCliInt := 0;
  FModBloq_CliInt    := 0;
  FModBloq_Peri      := 0;
  FModBloq_FatID     := 0;
  FModBloq_Lancto    := 0;
  FModBloq_TabLctA   := '';
  FModBloq_FatNum    := 0;
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
end;

procedure TFmPrincipal.Gerenciamento1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGerRclCab(0, 0);
end;

procedure TFmPrincipal.Gerenciamento2Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSGerClaCab(0);
end;

procedure TFmPrincipal.GerenciamentodePrReclasse1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPrePalCab(0);
end;

procedure TFmPrincipal.MenuItem13Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSRclArtSel();
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
end;

{
procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPager1);
end;
}

procedure TFmPrincipal.MostraLogoff();
begin
  FmPrincipal.Enabled := False;
  //
  FmClaReCo_Dmk.Show;
  FmClaReCo_Dmk.EdLogin.Text   := '';
  FmClaReCo_Dmk.EdSenha.Text   := '';
  FmClaReCo_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.N26EstoqueCustoIntegrado1Click(Sender: TObject);
const
  DataRetroativa = '';
  MostraFrx = True;
var
  Entidade, Filial, ItemAgruNoItm: Integer;
  TableSrc: String;
  DescrAgruNoItm: Boolean;
begin
(*
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Entidade);
  if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  Filial := EdEmpresa.ValueVariant;
*)
  Entidade := -11;
  Filial := 1;
(*
  DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  if not DescrAgruNoItm then
  begin
    ItemAgruNoItm := -1;
    ItemAgruNoItm := MyObjects.SelRadioGroup('Descri��o do item',
      'Selecione a forma de "Descri��o do item"',
      Geral.TSTA(RG00DescrAgruNoItm.Items), 1, -1);
    RG00DescrAgruNoItm.ItemIndex := ItemAgruNoItm;
    DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  end;
  if ItemAgruNoItm < 0 then
    Exit;
  if MyObjects.FIC(RG00DescrAgruNoItm.ItemIndex = 0, RG00DescrAgruNoItm,
  'Informe a forma de "Descri��o do item"') then
    Exit;
*)
  ItemAgruNoItm := 2;
{
  if Ck00EstoqueEm.Checked then
  begin
    VS_PF.ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro.ValueVariant,
    RG00_Ordem1.ItemIndex, RG00_Ordem2.ItemIndex, RG00_Ordem3.ItemIndex,
    RG00_Ordem4.ItemIndex, RG00_Ordem5.ItemIndex, RG00_Agrupa.ItemIndex,
    DescrAgruNoItm, Ed00StqCenCad.ValueVariant,
    RG00ZeroNegat.ItemIndex, CBEmpresa.Text, CB00StqCenCad.Text,
    CB00Terceiro.Text, TPDataRelativa.Date, Ck00DataCompra.Checked,
    Ck00EmProcessoBH.Checked, DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2,
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2, TP00EstoqueEm.Date,
    Ed00GraCusPrc.ValueVariant, Ed00NFeIni.ValueVariant,
    Ed00NFeFim.ValueVariant, Ck00Serie.Checked, Ed00Serie.ValueVariant,
    Ed00MovimCod.ValueVariant, LaAviso1, LaAviso2, MostraFrx)
  end else
  begin
}
    TableSrc := TMeuDB + '.' + CO_SEL_TAB_VMI;
    //
    VS_CRC_PF.ImprimeEstoqueReal(Entidade, Filial, 0(*Ed00Terceiro.ValueVariant*),
    (*RG00_Ordem1.ItemIndex*)9, (*RG00_Ordem2.ItemIndex*)0, (*RG00_Ordem3.ItemIndex*)2,
    (*RG00_Ordem4.ItemIndex*)4, (*RG00_Ordem5.ItemIndex*)0, (*RG00_Agrupa.ItemIndex*)3,
    DescrAgruNoItm, (*Ed00StqCenCad.ValueVariant*)0,
    (*RG00ZeroNegat.ItemIndex*)0, (*CBEmpresa.Text*)'***MEZA COUROS***', (*CB00StqCenCad.Text*)'',
    (*CB00Terceiro.Text*)'', Date(*TPDataRelativa.Date*), (*Ck00DataCompra.Checked*)False,
    (*Ck00EmProcessoBH.Checked*)False, (*DBG00GraGruY*)nil, (*DBG00GraGruX*)nil, (*DBG00CouNiv2*)nil,
    (*Qr00GraGruY*)nil, (*Qr00GraGruX*)nil, (*Qr00CouNiv2*)nil, TableSrc, DataRetroativa,
    (*Ed00GraCusPrc.ValueVariant*)-1, DmodG.ObtemAgora(), (*Ed00NFeIni.ValueVariant*)0,
    (*Ed00NFeFim.ValueVariant*)0, (*Ck00Serie.Checked*)False, (*Ed00Serie.ValueVariant*)0,
    (*Ed00MovimCod.ValueVariant*)0, MostraFrx);

(*
0  'MP / Artigo',
1  'Fornecedor',
2  'Pallet',
3  'IME-I',
4  'Ficha RMP',
5  'Marca',
6  'Centro',
7  'Local',
8  'Tipo Material',
9  'Est�gio Artigo',
0  'NFe',
1  'IME-C',
2  'Movimento',
3  'Cliente M.O.',
4  'Fornecedor M.O.'
*)
{
  end;
}
end;

procedure TFmPrincipal.Prepara1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSReclassPrePalCac(stIns, 0, 0, 0, 0, 0, 0, 0, 0);
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.ReclassificaoMassiva1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPaMulCabR(0);
end;

procedure TFmPrincipal.RecomearclkassificaodeOCjconfigurada1Click(
  Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSClaArtSel_IMEI();
end;

procedure TFmPrincipal.RecomearclkassificaodeOCjconfigurada2Click(
  Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSClaArtSel_FRMP();
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmClaReCo_Dmk.Show;
  Enabled := False;
  FmClaReCo_Dmk.Refresh;
  FmClaReCo_Dmk.EdSenha.Text := FmClaReCo_Dmk.EdSenha.Text+'*';
  FmClaReCo_Dmk.EdSenha.Refresh;
  FmClaReCo_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    (* Se precisar mudar caption dos componentes!
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPager1.Visible := True;
    *)
    // Tornar vis�vel
    TimerAlphaBlend.Enabled := True;
    FmClaReCo_Dmk.BtEntra.Enabled := True;
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados');
    Application.Terminate;
    Exit;
  end;
  {[***Desmarcar***]
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Geral.MB_(PChar('Imposs�vel criar M�dulo de vendas'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
}
  FmClaReCo_Dmk.EdSenha.Text := FmClaReCo_Dmk.EdSenha.Text+'*';
  FmClaReCo_Dmk.EdSenha.Refresh;
  FmClaReCo_Dmk.ReloadSkin;
  FmClaReCo_Dmk.EdLogin.Text := '';
  FmClaReCo_Dmk.EdLogin.PasswordChar := 'l';
  FmClaReCo_Dmk.EdSenha.Text := '';
  FmClaReCo_Dmk.EdSenha.Refresh;
  FmClaReCo_Dmk.EdLogin.ReadOnly := False;
  FmClaReCo_Dmk.EdSenha.ReadOnly := False;
  FmClaReCo_Dmk.EdLogin.SetFocus;
  //FmClaReCo_Dmk.ReloadSkin;
  FmClaReCo_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerAlphaBlendTimer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    TimerAlphaBlend.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
var
  Dia: Integer;
begin
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes(True) then
      Application.Terminate;
  end else
    Application.Terminate;
end;

procedure TFmPrincipal.TimerPingServerTimer(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  // Ping no mysql a cada 30 minutos (10800000 )
  Qry := TmySQLQuery.Create(Dmod);
  try
    Qry.Database := Dmod.MyDB;
    Qry.SQL.Clear;
    Qry.SQL.Add('SELECT 1 Um');
    UnDmkDAC_PF.AbreQuery(Qry, Dmod.MyDB);
    Qry.Close;
  finally
    Qry.Free;
  end;
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    ATBSuporte, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDB(False);
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
var
  Versao: Integer;
  Arq: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'ClaReCo',
    'ClaReCo', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, Arq, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasterceiros1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDBTerceiros(False);
end;

end.
