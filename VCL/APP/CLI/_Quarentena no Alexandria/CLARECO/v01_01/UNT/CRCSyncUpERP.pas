unit CRCSyncUpERP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  Vcl.CheckLst, mySQLDbTables, CreateGeral, UnGrl_Vars, dmkDBGridZTO;

type
  TFmCRCSyncUpERP = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Memo1: TMemo;
    Memo2: TMemo;
    PB1: TProgressBar;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrTabelas: TmySQLQuery;
    QrRegistros: TmySQLQuery;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrItensPorTab: TmySQLQuery;
    DsItensPorTab: TDataSource;
    QrItensPorTabCodigo: TIntegerField;
    QrItensPorTabNome: TWideStringField;
    QrItensPorTabItens: TLargeintField;
    QrItensPorTabAtivo: TSmallintField;
    QrUpd: TmySQLQuery;
    Panel6: TPanel;
    Label1: TLabel;
    LaOrigem: TLabel;
    Label2: TLabel;
    LaDestino: TLabel;
    CkEnviarTudo: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure CkEnviarTudoClick(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
    FLstTabs: String;
    //
    FDestino,
    FDes_Host, FDes_User, FDes_Pass, FDes_DB,
    FOri_Host, FOri_User, FOri_Pass, FOri_DB: String;
    FOri_Porta, FDes_Porta: Integer;

    procedure PesquisaNovosDadosAEnviar();
    procedure EnviaDadosParaTabelasDoERP(PB1: TProgressBar; LaAviso1, LaAviso2:
              TLabel; Memo1, Memo2: TMemo);
    procedure CheckItems(Checka: Boolean);
  public
    { Public declarations }
  end;

  var
  FmCRCSyncUpERP: TFmCRCSyncUpERP;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UMySQLModule,
  UMySQLDB, UnDmkProcFunc, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmCRCSyncUpERP.BtNenhumClick(Sender: TObject);
begin
  CheckItems(False);
end;

procedure TFmCRCSyncUpERP.BtOKClick(Sender: TObject);
begin
  if QrItensPorTab.RecordCount > 0 then
    EnviaDadosParaTabelasDoERP(PB1, LaAviso1, LaAviso2, Memo1, Memo2)
  else
    Geral.MB_Info('N�o h� dados para serem exportados ao ERP!');
end;

procedure TFmCRCSyncUpERP.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCRCSyncUpERP.BtTodosClick(Sender: TObject);
begin
  CheckItems(True);
end;

procedure TFmCRCSyncUpERP.EnviaDadosParaTabelasDoERP(PB1: TProgressBar; LaAviso1,
  LaAviso2: TLabel; Memo1, Memo2: TMemo);
var
  DBOri, DBDes: TmySQLDataBase;
  ArqNome, PathMySQL, Msg, Tabela, ArqResult, SQL_Filtro: String;
  Parcial: TDateTime;
var
  Tempo: TDateTime;
  Item, Total, Falhas: Integer;
  //
  function EnviaDadosNovosTabela(TbOri, SQL_WHERE_Ori: String): Boolean;
  begin
    Result   := False;
    ArqNome   := Geral.FDT(Now, 26) + '.sql';
    Tabela    := ' --tables ' + Lowercase(TbOri) + ' ';
    if Trim(SQL_WHERE_Ori) <> '' then
    Tabela := Tabela +  ' --' + SQL_WHERE_Ori + ' ';
    //
    ArqResult := USQLDB.Backup_Executa(DBOri, PathMySQL, FDestino, ArqNome,
      FOri_Host, FOri_DB, FOri_User, FOri_Pass, FOri_Porta, Memo2, False, False,
      ndctNoCreateAndReplaceCompleateIns, Msg, nil, Tabela);
    //
    if ArqResult <> '' then
    begin
      //Executa restaura
      try
        UnDmkDAC_PF.ConectaMyDB_DAC(DBDes, FDes_DB, FDes_Host,
          FDes_Porta, FDes_User, FDes_Pass, True, True, True, True);
        Result := True;
      except
        System.SysUtils.DeleteFile(ArqResult);
        //
        Geral.MB_Erro('Falha ao conectar no banco de dados destino!');
        Result := False;
        Falhas := Falhas + 1;
        Exit;
      end;
      if USQLDB.Backup_Restaura(DBDes, ArqResult, False, Msg, nil) then
      begin
        if CkEnviarTudo.Checked then
          SQL_Filtro := ''
        else
          SQL_Filtro := Geral.ATS([
          'WHERE AWServerID=' + VAR_TXT_AWServerID,
          'AND AWStatSinc<>' + Geral.FF0(Integer(stUpSinc)),
          '']);
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(QrUpd, DBOri, [
        'UPDATE ' + TbOri,
        'SET AWStatSinc=9 ',
        SQL_Filtro,
        '']);
        Parcial := Now() - Tempo;
        Memo1.Lines.Add(FormatDateTime('h:nn:ss', Parcial) + ' Item ' +
        Geral.FF0(Item) + ' de ' + Geral.FF0(Total) +
        ' > Sincronismo realizado com sucesso na tabela ' + TbOri);
      end else
      begin
        Falhas := Falhas + 1;
        Geral.MB_Erro('Falha ao restaurar backup!');
        Memo1.Lines.Add('Falha "B" ao restaurar backup na tabela ' + TbOri);
      end;
    end else
    begin
      Falhas := Falhas + 1;
      Geral.MB_Erro('Falha ao realizar backup de dados!');
      Memo1.Lines.Add('Falha "A" ao restaurar backup na tabela ' + TbOri);
    end;
  end;
var
  Tab, SQLUpErp, sTempo: String;
  MeVisible1, MeVisible2: Boolean;
  RootDBs: Boolean;
begin
  Tempo := Now();
  BtOK.Enabled := False;
  Item := 0;
////////////////////////////////////////////////////////////////////////////////
  PathMySQL := USQLDB.ObtemPathMySQLBin(Dmod.MyDB);
  //
  RootDBs := (Pos('clareco', FOri_DB) > 0) and
             (Pos('clareco_', FDes_DB) = 0) and
             ('clareco' <> FDes_DB) and
             (Pos('clarecocdr', FDes_DB) > 0) and
             (Pos('clarecocdr', FOri_DB) = 0) and
             (Pos('bluederm', FDes_DB) = 0) and
             (Pos('bluederm', FOri_DB) = 0);
  if MyObjects.FIC(RootDBs = False, nil, 'Databases incorretas!' + sLineBreak +
  'DB Origem: ' + FOri_DB + sLineBreak + 'DB Destino: ' + FDes_DB) then
    Exit;
  //
  DBOri := TmySQLDataBase.Create(Dmod);
  DBDes := TmySQLDataBase.Create(Dmod);
  try
////////////////////////////////////////////////////////////////////////////////
    Falhas := 0;
    MeVisible1 := Memo1.Visible;
    MeVisible2 := Memo2.Visible;
    Memo1.Visible := True;
    Memo2.Visible := True;
    Screen.Cursor := crHourGlass;
    try
      Total := QrItensPorTab.RecordCount;
      PB1.Position := 0;
      PB1.Max := Total;
      if CkEnviarTudo.Checked then
        SQLUpErp := 'where="AWServerID=' + VAR_TXT_AWServerID + '"'
      else
        SQLUpErp := 'where="AWServerID=' + VAR_TXT_AWServerID +
          ' AND AWStatSinc<>' + Geral.FF0(Integer(stUpSinc)) + '"';
      //
      // where="codigo=1"
      //BuscaDadosTabela('unidmed', 'where="codigo=-1"');
      //
      //
      QrItensPorTab.First;
      while not QrItensPorTab.Eof do
      begin
        Item := Item + 1;
        Tab := QrItensPorTabNome.Value;
        if not EnviaDadosNovosTabela(Tab, SQLUpErp) then
            Exit;
        QrItensPorTab.Next;
      end;
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, '');
      Memo1.Visible := MeVisible1;
      Memo2.Visible := MeVisible2;
      if Falhas > 0 then
      begin
        sTempo := 'Atualiza��o finalizada com ' + Geral.FF0(Falhas) + ' falha(s)';
        MyObjects.Informa2(LaAviso1, LaAviso2, False, sTempo);
        Geral.MB_Erro(sTempo);
      end else
      begin
        Tempo := Now() - Tempo;
        sTempo := 'Atualiza��o finalizada! Tempo: ' + FormatDateTime('h:nn:ss', Tempo);
        MyObjects.Informa2(LaAviso1, LaAviso2, False, sTempo);
        Geral.MB_Info(sTempo);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  finally
    DBDes.Free;
    DBOri.Free;
  end;
end;

procedure TFmCRCSyncUpERP.CheckItems(Checka: Boolean);
(*
var
  I, N: Integer;
*)
begin
(*
  N := CLBTabelas.Items.Count;
  for I := 0 to N - 1 do
    CLBTabelas.Checked[I] := Checka;
*)
end;

procedure TFmCRCSyncUpERP.CkEnviarTudoClick(Sender: TObject);
begin
  if CkEnviarTudo.Checked then
    PesquisaNovosDadosAEnviar();
end;

procedure TFmCRCSyncUpERP.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FCriando then
  begin
    FCriando := False;
    PesquisaNovosDadosAEnviar();
  end;
end;

procedure TFmCRCSyncUpERP.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCriando := True;
  FLstTabs := UnCreateGeral.RecriaTempTableNovo(ntrtt_ItensPorCod,
    DModG.QrUpdPID1, False, 1, '_Lst_Tabs_CRC_Up_');
  //
  FDestino    := 'C:\Dermatek\MySQL_DataUp';
  FDes_Host   := Dmod.QrCrcCtrlERP_Host.Value;
  FDes_User   := 'root';
  FDes_Pass   := CO_USERSPNOW;
  FDes_DB     := Dmod.QrCrcCtrlERP_DBRepos.Value; // Cuidado!!! Diferente do DownLoad!!!!
  FOri_Host   := VAR_IP;
  FOri_User   := 'root';
  FOri_Pass   := CO_USERSPNOW;
  FOri_DB     := Lowercase(TMeuDB);
  FOri_Porta  := VAR_PORTA;
  FDes_Porta  := Dmod.QrCrcCtrlERP_DBPorta.Value;
  //
  LaOrigem.Caption  := FOri_Host + ':' + Geral.FF0(FOri_Porta) + '.' + FOri_DB;
  LaDestino.Caption := FDes_Host + ':' + Geral.FF0(FDes_Porta) + '.' + FDes_DB;
end;

procedure TFmCRCSyncUpERP.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCRCSyncUpERP.PesquisaNovosDadosAEnviar();
{
var
  Nome, Msg, SQL_Filtro: String;
  Codigo, Itens, Ativo: Integer;
  SQLType: TSQLType;
}
begin
  if VS_CRC_PF.PesquisaNovosDadosClaReCo(stUpSinc, FLstTabs, Dmod.MyDB,
  QrItensPorTab, QrTabelas, QrRegistros, PB1, LaAviso1, LaAviso2,
  CkEnviarTudo.Checked, Integer(VAR_TXT_AWServerID)) then
{
    Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    SQLType := stIns;
    Codigo  := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrItensPorTab, DModG.MyPID_DB, [
    DELETE_FROM + ' ' + FLstTabs,
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrTabelas, Dmod.MyDB, [
    'SHOW TABLES LIKE "VS%" ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrTabelas.RecordCount;
    QrTabelas.First;
    while not QrTabelas.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Verificando Tabela ' + Nome);
      Ativo  := 1;
      Codigo := Codigo + 1;
      Nome   := QrTabelas.Fields[0].AsString;
      if CkEnviarTudo.Checked then
        SQL_Filtro := ''
      else
        SQL_Filtro := Geral.ATS([
        'WHERE AWServerID=' + VAR_TXT_AWServerID,
        'AND AWStatSinc<>' + Geral.FF0(Integer(stUpSinc))]);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrRegistros, Dmod.MyDB, [
      'SELECT COUNT(*) ITENS',
      'FROM ' + Nome,
      SQL_Filtro,
      '']);
      Itens  := QrRegistros.FieldByName('ITENS').AsInteger;
      //if
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLType, FLstTabs, False, [
      'Nome', 'Itens', 'Ativo'], ['Codigo'], [
      Nome, Itens, Ativo], [Codigo], False);
      //
      QrTabelas.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrItensPorTab, DModG.MyPID_DB, [
    'SELECT *',
    'FROM ' + FLstTabs,
    'WHERE Itens > 0',
    '']);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
    if QrItensPorTab.RecordCount > 0 then
}
      BtOK.Enabled := True
    else
      Geral.MB_Info('N�o h� dados para serem exportados ao ERP!');
{
    //
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

end.
