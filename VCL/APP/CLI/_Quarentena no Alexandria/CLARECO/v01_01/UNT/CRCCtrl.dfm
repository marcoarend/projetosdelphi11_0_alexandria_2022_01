object FmCRCCtrl: TFmCRCCtrl
  Left = 339
  Top = 185
  Caption = 'CRC-ONECL-003 :: Op'#231#245'es do M'#243'dulo CRC'
  ClientHeight = 619
  ClientWidth = 788
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 788
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 740
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 692
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 297
        Height = 32
        Caption = 'Op'#231#245'es do M'#243'dulo CRC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 297
        Height = 32
        Caption = 'Op'#231#245'es do M'#243'dulo CRC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 297
        Height = 32
        Caption = 'Op'#231#245'es do M'#243'dulo CRC'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 788
    Height = 457
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 788
      Height = 457
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 788
        Height = 457
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 784
          Height = 440
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          ActivePage = TabSheet2
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          object TabSheet1: TTabSheet
            Margins.Left = 2
            Margins.Top = 2
            Margins.Right = 2
            Margins.Bottom = 2
            Caption = 'ERP'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 776
              Height = 405
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 16
                Width = 125
                Height = 13
                Caption = 'ID deste Servidor no ERP:'
              end
              object Label2: TLabel
                Left = 8
                Top = 52
                Width = 122
                Height = 13
                Caption = 'Host do Servidor do ERP:'
              end
              object Label3: TLabel
                Left = 8
                Top = 112
                Width = 140
                Height = 13
                Caption = 'DB do ERP no Servidor ERP:'
              end
              object Label4: TLabel
                Left = 8
                Top = 76
                Width = 110
                Height = 13
                Caption = 'Porta do Servidor ERP:'
              end
              object Label5: TLabel
                Left = 8
                Top = 136
                Width = 148
                Height = 13
                Caption = 'DB de Coleta no Servidor ERP:'
              end
              object EdServerID: TdmkEdit
                Left = 196
                Top = 12
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfLongint
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdERP_Host: TdmkEdit
                Left = 196
                Top = 48
                Width = 400
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdERP_DBName: TdmkEdit
                Left = 196
                Top = 108
                Width = 400
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdERP_DBPorta: TdmkEdit
                Left = 196
                Top = 72
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfLongint
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdERP_DBRepos: TdmkEdit
                Left = 196
                Top = 132
                Width = 400
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Couros VS'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 776
              Height = 405
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label12: TLabel
                Left = 12
                Top = 48
                Width = 163
                Height = 13
                Caption = 'Contra senha impress'#227'o QR Code:'
              end
              object CkInfoMulFrnImpVS: TdmkCheckBox
                Left = 12
                Top = 28
                Width = 325
                Height = 17
                Caption = 'Mostrar fornecedores nas impress'#245'es das fichas de pallets.'
                TabOrder = 0
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object CkCanAltBalVS: TdmkCheckBox
                Left = 12
                Top = 8
                Width = 325
                Height = 17
                Caption = 'Permite alterar estoque de produtos ap'#243's invent'#225'rio'
                TabOrder = 1
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object RGQtdBoxClas: TdmkRadioGroup
                Left = 358
                Top = 4
                Width = 407
                Height = 45
                Caption = ' Boxes iniciais na classifica'#231#227'o e reclassifica'#231#227'o:'
                Columns = 6
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Perguntar'
                  '6 boxes'
                  '9 boxes'
                  '12 boxes'
                  '15 boxes')
                TabOrder = 2
                QryCampo = 'QtdBoxClas'
                UpdCampo = 'QtdBoxClas'
                UpdType = utYes
                OldValor = 0
              end
              object EdVSImpRandStr: TdmkEdit
                Left = 12
                Top = 64
                Width = 753
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CGNObrigNFeVS: TdmkCheckGroup
                Left = 12
                Top = 91
                Width = 753
                Height = 122
                Margins.Left = 2
                Margins.Top = 2
                Margins.Right = 2
                Margins.Bottom = 2
                Caption = ' N'#227'o obrigar preenchimento de NF-e nas seguintes opera'#231#245'es '
                Columns = 3
                Items.Strings = (
                  'VS_PF.ObtemListaOperacoes()')
                TabOrder = 4
                UpdType = utYes
                Value = 0
                OldValor = 0
              end
              object CkVSWarSemNF: TdmkCheckBox
                Left = 12
                Top = 220
                Width = 325
                Height = 17
                Caption = 'Avisa lan'#231'amentos sem defini'#231#227'o de NFe de origem/destino.'
                TabOrder = 5
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object CkVSWarNoFrn: TdmkCheckBox
                Left = 12
                Top = 240
                Width = 345
                Height = 17
                Caption = 'Avisa lan'#231'amentos sem defini'#231#227'o de fornecedor de origem.'
                TabOrder = 6
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object CkVSInsPalManu: TdmkCheckBox
                Left = 12
                Top = 260
                Width = 345
                Height = 17
                Caption = 'Inclui pallets informando numero manualmente.'
                Checked = True
                Enabled = False
                State = cbChecked
                TabOrder = 7
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 505
    Width = 788
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 784
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 549
    Width = 788
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 642
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 640
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCRCCtrl: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCRCCtrlAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM crcctrl'
      'WHERE Codigo=1')
    Left = 368
    Top = 8
    object QrCRCCtrlServerID: TIntegerField
      FieldName = 'ServerID'
    end
    object QrCRCCtrlCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCRCCtrlCanAltBalVS: TSmallintField
      FieldName = 'CanAltBalVS'
    end
    object QrCRCCtrlInfoMulFrnImpVS: TSmallintField
      FieldName = 'InfoMulFrnImpVS'
    end
    object QrCRCCtrlQtdBoxClas: TSmallintField
      FieldName = 'QtdBoxClas'
    end
    object QrCRCCtrlVSWarSemNF: TSmallintField
      FieldName = 'VSWarSemNF'
    end
    object QrCRCCtrlVSWarNoFrn: TSmallintField
      FieldName = 'VSWarNoFrn'
    end
    object QrCRCCtrlVSInsPalManu: TSmallintField
      FieldName = 'VSInsPalManu'
    end
    object QrCRCCtrlVSImpRandStr: TWideStringField
      FieldName = 'VSImpRandStr'
      Size = 255
    end
    object QrCRCCtrlERP_Host: TWideStringField
      FieldName = 'ERP_Host'
      Size = 255
    end
    object QrCRCCtrlERP_DBName: TWideStringField
      FieldName = 'ERP_DBName'
      Size = 255
    end
    object QrCRCCtrlERP_DBPorta: TIntegerField
      FieldName = 'ERP_DBPorta'
    end
    object QrCRCCtrlERP_DBRepos: TWideStringField
      FieldName = 'ERP_DBRepos'
      Size = 255
    end
    object QrCRCCtrlNObrigNFeVS: TIntegerField
      FieldName = 'NObrigNFeVS'
    end
  end
  object DsCRCCtrl: TDataSource
    DataSet = QrCRCCtrl
    Left = 368
    Top = 52
  end
  object VUGraCorCad: TdmkValUsu
    QryCampo = 'GraCorCad'
    UpdCampo = 'GraCorCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 112
    Top = 8
  end
  object VUGraTamCad: TdmkValUsu
    QryCampo = 'GraTamCad'
    UpdCampo = 'GraTamCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 140
    Top = 8
  end
  object VUGraGru1: TdmkValUsu
    QryCampo = 'Nivel1'
    UpdCampo = 'Nivel1'
    RefCampo = 'Nivel1'
    UpdType = utYes
    Left = 168
    Top = 8
  end
end
