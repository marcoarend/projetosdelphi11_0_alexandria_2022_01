object FmCRCUniClassCab: TFmCRCUniClassCab
  Left = 368
  Top = 194
  Caption = 'CRC-ONECL-002 :: Configura'#231#227'o de Classifica'#231#227'o'
  ClientHeight = 521
  ClientWidth = 984
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 984
    Height = 425
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 984
      Height = 129
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCRCArtigos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsCRCArtigos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 361
      Width = 984
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 288
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 462
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 984
    Height = 425
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 984
      Height = 353
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel6: TPanel
        Left = 2
        Top = 57
        Width = 980
        Height = 40
        Align = alTop
        TabOrder = 1
        object LaPecas: TLabel
          Left = 8
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object LaPeso: TLabel
          Left = 84
          Top = 0
          Width = 27
          Height = 13
          Caption = 'Peso:'
        end
        object LaAreaM2: TLabel
          Left = 160
          Top = 0
          Width = 39
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object LaAreaP2: TLabel
          Left = 244
          Top = 0
          Width = 37
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object EdOriPecas: TdmkEdit
          Left = 8
          Top = 16
          Width = 72
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdOriPesoKg: TdmkEdit
          Left = 84
          Top = 16
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'PesoKg'
          UpdCampo = 'PesoKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdOriAreaM2: TdmkEditCalc
          Left = 160
          Top = 16
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'AreaM2'
          UpdCampo = 'AreaM2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdOriAreaP2
          CalcType = ctM2toP2
          CalcFrac = cfQuarto
        end
        object EdOriAreaP2: TdmkEditCalc
          Left = 244
          Top = 16
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'AreaP2'
          UpdCampo = 'AreaP2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          dmkEditCalcA = EdOriAreaM2
          CalcType = ctP2toM2
          CalcFrac = cfCento
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 980
        Height = 42
        Align = alTop
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 0
          Width = 14
          Height = 13
          Caption = 'ID:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label11: TLabel
          Left = 68
          Top = 0
          Width = 83
          Height = 13
          Caption = 'S'#233'rie Ficha RMP:'
        end
        object Label4: TLabel
          Left = 280
          Top = 0
          Width = 56
          Height = 13
          Caption = 'Ficha RMP:'
        end
        object Label5: TLabel
          Left = 376
          Top = 0
          Width = 63
          Height = 13
          Caption = 'Pallet origem:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdSerieFch: TdmkEditCB
          Left = 68
          Top = 16
          Width = 40
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'SerieFch'
          UpdCampo = 'SerieFch'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBSerieFch
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnFormActivate
        end
        object CBSerieFch: TdmkDBLookupComboBox
          Left = 108
          Top = 16
          Width = 169
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsVSSerFch
          TabOrder = 2
          dmkEditCB = EdSerieFch
          QryCampo = 'SerieFch'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdFicha: TdmkEdit
          Left = 280
          Top = 16
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPallOri: TdmkEdit
          Left = 376
          Top = 16
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object Panel8: TPanel
        Left = 2
        Top = 97
        Width = 980
        Height = 254
        Align = alClient
        TabOrder = 2
        object Label3: TLabel
          Left = 8
          Top = 0
          Width = 65
          Height = 13
          Caption = 'Pallet Box 01:'
        end
        object Label6: TLabel
          Left = 8
          Top = 40
          Width = 65
          Height = 13
          Caption = 'Pallet Box 02:'
        end
        object Label8: TLabel
          Left = 8
          Top = 80
          Width = 65
          Height = 13
          Caption = 'Pallet Box 03:'
        end
        object Label9: TLabel
          Left = 8
          Top = 120
          Width = 65
          Height = 13
          Caption = 'Pallet Box 04:'
        end
        object Label10: TLabel
          Left = 8
          Top = 160
          Width = 65
          Height = 13
          Caption = 'Pallet Box 05:'
        end
        object Label12: TLabel
          Left = 332
          Top = 0
          Width = 65
          Height = 13
          Caption = 'Pallet Box 06:'
        end
        object Label13: TLabel
          Left = 332
          Top = 40
          Width = 65
          Height = 13
          Caption = 'Pallet Box 07:'
        end
        object Label14: TLabel
          Left = 332
          Top = 80
          Width = 65
          Height = 13
          Caption = 'Pallet Box 08:'
        end
        object Label15: TLabel
          Left = 332
          Top = 120
          Width = 65
          Height = 13
          Caption = 'Pallet Box 09:'
        end
        object Label16: TLabel
          Left = 332
          Top = 160
          Width = 65
          Height = 13
          Caption = 'Pallet Box 10:'
        end
        object Label17: TLabel
          Left = 652
          Top = 0
          Width = 65
          Height = 13
          Caption = 'Pallet Box 11:'
        end
        object Label18: TLabel
          Left = 652
          Top = 40
          Width = 65
          Height = 13
          Caption = 'Pallet Box 12:'
        end
        object Label19: TLabel
          Left = 652
          Top = 80
          Width = 65
          Height = 13
          Caption = 'Pallet Box 13:'
        end
        object Label20: TLabel
          Left = 652
          Top = 120
          Width = 65
          Height = 13
          Caption = 'Pallet Box 14:'
        end
        object Label21: TLabel
          Left = 652
          Top = 160
          Width = 65
          Height = 13
          Caption = 'Pallet Box 15:'
        end
        object EdPallBox01: TdmkEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPallBox02: TdmkEdit
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPallBox03: TdmkEdit
          Left = 8
          Top = 96
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPallBox04: TdmkEdit
          Left = 8
          Top = 136
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPallBox05: TdmkEdit
          Left = 8
          Top = 176
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox01: TdmkDBLookupComboBox
          Left = 64
          Top = 16
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 5
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBPallBox02: TdmkDBLookupComboBox
          Left = 64
          Top = 56
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 6
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBPallBox03: TdmkDBLookupComboBox
          Left = 64
          Top = 96
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 7
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBPallBox04: TdmkDBLookupComboBox
          Left = 64
          Top = 136
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 8
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBPallBox05: TdmkDBLookupComboBox
          Left = 64
          Top = 176
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 9
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox06: TdmkEdit
          Left = 332
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox06: TdmkDBLookupComboBox
          Left = 388
          Top = 16
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 11
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox07: TdmkEdit
          Left = 332
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox07: TdmkDBLookupComboBox
          Left = 388
          Top = 56
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 13
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox08: TdmkEdit
          Left = 332
          Top = 96
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox08: TdmkDBLookupComboBox
          Left = 388
          Top = 96
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 15
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox09: TdmkEdit
          Left = 332
          Top = 136
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 16
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox09: TdmkDBLookupComboBox
          Left = 388
          Top = 136
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 17
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox10: TdmkEdit
          Left = 332
          Top = 176
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 18
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox10: TdmkDBLookupComboBox
          Left = 388
          Top = 176
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 19
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox11: TdmkEdit
          Left = 652
          Top = 16
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 20
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox11: TdmkDBLookupComboBox
          Left = 708
          Top = 16
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 21
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox12: TdmkEdit
          Left = 652
          Top = 56
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 22
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox12: TdmkDBLookupComboBox
          Left = 708
          Top = 56
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 23
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox13: TdmkEdit
          Left = 652
          Top = 96
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 24
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox13: TdmkDBLookupComboBox
          Left = 708
          Top = 96
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 25
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox14: TdmkEdit
          Left = 652
          Top = 136
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 26
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox14: TdmkDBLookupComboBox
          Left = 708
          Top = 136
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 27
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPallBox15: TdmkEdit
          Left = 652
          Top = 176
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 28
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBPallBox15: TdmkDBLookupComboBox
          Left = 708
          Top = 176
          Width = 260
          Height = 21
          Enabled = False
          TabOrder = 29
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 362
      Width = 984
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 845
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 984
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 936
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 720
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 368
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 368
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 368
        Height = 32
        Caption = 'Configura'#231#227'o de Classifica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 984
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 980
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCRCArtigos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCRCArtigosBeforeOpen
    AfterOpen = QrCRCArtigosAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 700
    Top = 8
    object QrCRCArtigosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCRCArtigosNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCRCArtigos: TDataSource
    DataSet = QrCRCArtigos
    Left = 700
    Top = 56
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrVSSerFch: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 576
    Top = 56
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 576
    Top = 104
  end
end
