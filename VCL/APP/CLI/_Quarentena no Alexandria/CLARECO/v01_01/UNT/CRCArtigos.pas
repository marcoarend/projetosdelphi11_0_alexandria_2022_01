unit CRCArtigos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmCRCArtigos = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCRCArtigos: TmySQLQuery;
    QrCRCArtigosCodigo: TIntegerField;
    QrCRCArtigosNome: TWideStringField;
    DsCRCArtigos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCRCArtigosGraGruX: TIntegerField;
    EdGraGruX: TdmkEdit;
    LaReduzido: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCRCArtigosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCRCArtigosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCRCArtigos: TFmCRCArtigos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCRCArtigos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCRCArtigos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCRCArtigosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCRCArtigos.DefParams;
begin
  VAR_GOTOTABELA := 'crcartigos';
  VAR_GOTOMYSQLTABLE := QrCRCArtigos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM crcartigos');
  VAR_SQLx.Add('WHERE StatSinc<>' + Geral.FF0(Integer(stDel)));
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCRCArtigos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCRCArtigos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCRCArtigos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCRCArtigos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCRCArtigos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCRCArtigos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCRCArtigos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCRCArtigos.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCRCArtigos.BtAlteraClick(Sender: TObject);
begin
  if (QrCRCArtigos.State <> dsInactive) and (QrCRCArtigos.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCRCArtigos, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'crcartigos');
  end;
end;

procedure TFmCRCArtigos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCRCArtigosCodigo.Value;
  Close;
end;

procedure TFmCRCArtigos.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, GraGruX, StatSinc: Integer;
  SQLType: TSQLType;
  Qry: TmySQLQuery;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  StatSinc       := Integer(SQLType);
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o c�digo do reduzido!') then Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM crcartigos ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    'AND Codigo<>' + Geral.FF0(Codigo),
    '']);
    if MyObjects.FIC(Qry.RecordCount > 0, EdGraGruX,
    'Inclus�o abortada!' + sLineBreak +
    'Este reduzido j� est� sendo usado no artigo ' +
    Geral.FF0(Qry.FieldByName('Codigo').AsInteger)) then
      Exit;
  finally
    Qry.Free;
  end;
  Codigo := UMyMod.BPGS1I32('crcartigos', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'crcartigos', False, [
  'GraGruX', 'Nome', 'StatSinc'], [
  'Codigo'], [
  GraGruX, Nome, StatSinc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCRCArtigos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'crcartigos', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCRCArtigos.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCRCArtigos, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'crcartigos');
end;

procedure TFmCRCArtigos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmCRCArtigos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCRCArtigosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCRCArtigos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCRCArtigos.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCRCArtigosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCRCArtigos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCRCArtigos.QrCRCArtigosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCRCArtigos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCRCArtigos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCRCArtigosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'crcartigos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCRCArtigos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCRCArtigos.QrCRCArtigosBeforeOpen(DataSet: TDataSet);
begin
  QrCRCArtigosCodigo.DisplayFormat := FFormatFloat;
end;

end.

