object FmVSReclassifOneNw3: TFmVSReclassifOneNw3
  Left = 0
  Top = 0
  Caption = 'CRC-ONECL-000 :: Reclasse Couro a Couro'
  ClientHeight = 807
  ClientWidth = 1471
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PanelOC: TPanel
    Left = 0
    Top = 702
    Width = 1471
    Height = 105
    Align = alBottom
    TabOrder = 1
    object Panel43: TPanel
      Left = 1
      Top = 1
      Width = 748
      Height = 103
      Align = alLeft
      TabOrder = 0
      object PnInfoOC: TPanel
        Left = 1
        Top = 1
        Width = 746
        Height = 101
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 8
          Width = 18
          Height = 13
          Caption = 'OC:'
          FocusControl = DBEdCodigo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 128
          Top = 56
          Width = 45
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit19
        end
        object Label22: TLabel
          Left = 100
          Top = 8
          Width = 27
          Height = 13
          Caption = #193'rea:'
          FocusControl = DBEdit14
        end
        object Label14: TLabel
          Left = 172
          Top = 8
          Width = 81
          Height = 13
          Caption = 'Artigo de ribeira:'
        end
        object Label17: TLabel
          Left = 4
          Top = 32
          Width = 30
          Height = 13
          Caption = 'Pallet:'
        end
        object Label16: TLabel
          Left = 232
          Top = 32
          Width = 32
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label20: TLabel
          Left = 328
          Top = 32
          Width = 43
          Height = 13
          Caption = #193'rea m'#178':'
        end
        object Label21: TLabel
          Left = 456
          Top = 32
          Width = 43
          Height = 13
          Caption = #193'rea ft'#178':'
        end
        object Label26: TLabel
          Left = 592
          Top = 32
          Width = 53
          Height = 13
          Caption = 'Ficha RMP:'
          FocusControl = DBEdit18
        end
        object Label18: TLabel
          Left = 4
          Top = 80
          Width = 167
          Height = 13
          Caption = 'Observa'#231#227'o sobre o pallet gerado:'
        end
        object Label24: TLabel
          Left = 476
          Top = 56
          Width = 59
          Height = 13
          Caption = 'Fornecedor:'
          FocusControl = DBEdit16
        end
        object Label84: TLabel
          Left = 120
          Top = 32
          Width = 30
          Height = 13
          Caption = 'IME-I:'
        end
        object Label85: TLabel
          Left = 640
          Top = 8
          Width = 36
          Height = 13
          Caption = 'Tempo:'
        end
        object Label87: TLabel
          Left = 4
          Top = 56
          Width = 79
          Height = 13
          Caption = 'C'#243'digo reclasse:'
          FocusControl = DBEdit35
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 28
          Top = 4
          Width = 68
          Height = 21
          TabStop = False
          DataField = 'CacCod'
          DataSource = DsVSPaRclCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdit19: TDBEdit
          Left = 176
          Top = 52
          Width = 32
          Height = 21
          DataField = 'Empresa'
          DataSource = DsVSPaRclCab
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 204
          Top = 52
          Width = 269
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsVSPaRclCab
          TabOrder = 2
        end
        object DBEdit14: TDBEdit
          Left = 136
          Top = 4
          Width = 32
          Height = 21
          DataField = 'NO_TIPO'
          DataSource = DsVSPaRclCab
          TabOrder = 3
        end
        object DBEdit6: TDBEdit
          Left = 256
          Top = 4
          Width = 57
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsVSPallet0
          TabOrder = 4
        end
        object DBEdit7: TDBEdit
          Left = 312
          Top = 4
          Width = 321
          Height = 21
          DataField = 'NO_PRD_TAM_COR'
          DataSource = DsVSPallet0
          TabOrder = 5
        end
        object DBEdit10: TDBEdit
          Left = 40
          Top = 28
          Width = 76
          Height = 21
          DataField = 'VSPallet'
          DataSource = DsVSPaRclCab
          TabOrder = 6
        end
        object DBEdit9: TDBEdit
          Left = 268
          Top = 28
          Width = 56
          Height = 21
          DataField = 'Pecas'
          DataSource = DsVSGerArtNew
          TabOrder = 7
        end
        object DBEdit12: TDBEdit
          Left = 376
          Top = 28
          Width = 76
          Height = 21
          DataField = 'AreaM2'
          DataSource = DsVSGerArtNew
          TabOrder = 8
        end
        object DBEdit13: TDBEdit
          Left = 504
          Top = 28
          Width = 84
          Height = 21
          DataField = 'AreaP2'
          DataSource = DsVSGerArtNew
          TabOrder = 9
        end
        object DBEdit18: TDBEdit
          Left = 648
          Top = 28
          Width = 93
          Height = 21
          DataField = 'NO_FICHA'
          DataSource = DsVSGerArtNew
          TabOrder = 10
        end
        object DBEdit11: TDBEdit
          Left = 184
          Top = 76
          Width = 557
          Height = 21
          DataField = 'Nome'
          DataSource = DsVSPallet0
          TabOrder = 11
        end
        object DBEdit16: TDBEdit
          Left = 540
          Top = 52
          Width = 56
          Height = 21
          DataField = 'Terceiro'
          DataSource = DsVSGerArtNew
          TabOrder = 12
        end
        object DBEdit17: TDBEdit
          Left = 596
          Top = 52
          Width = 145
          Height = 21
          DataField = 'NO_FORNECE'
          DataSource = DsVSGerArtNew
          TabOrder = 13
        end
        object DBEdit5: TDBEdit
          Left = 152
          Top = 28
          Width = 76
          Height = 21
          DataField = 'VSMovIts'
          DataSource = DsVSPaRclCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object EdTempo: TEdit
          Left = 680
          Top = 4
          Width = 61
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 15
          Text = '0,000'
        end
        object DBEdit35: TDBEdit
          Left = 92
          Top = 52
          Width = 32
          Height = 21
          DataField = 'Codigo'
          DataSource = DsVSPaRclCab
          TabOrder = 16
        end
      end
    end
    object Panel44: TPanel
      Left = 749
      Top = 1
      Width = 292
      Height = 103
      Align = alClient
      TabOrder = 1
      object RGFrmaIns: TRadioGroup
        Left = 1
        Top = 1
        Width = 148
        Height = 101
        Align = alLeft
        Caption = ' Forma classifica'#231#227'o:'
        Columns = 2
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Medidos'
          'Sumidos'
          'Ambos'
          'Manual')
        TabOrder = 0
        OnClick = RGFrmaInsClick
      end
      object BtClassesGeradas: TBitBtn
        Left = 156
        Top = 12
        Width = 160
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reclasses Geradas'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtClassesGeradasClick
      end
    end
    object PnResponsaveis: TPanel
      Left = 1041
      Top = 1
      Width = 429
      Height = 103
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object Label70: TLabel
        Left = 8
        Top = 9
        Width = 99
        Height = 21
        Caption = 'Classificador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label71: TLabel
        Left = 8
        Top = 41
        Width = 74
        Height = 21
        Caption = 'Digitador:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 228
        Top = 73
        Width = 65
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Tempo:'
        FocusControl = Edit1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EdRevisor: TdmkEditCB
        Left = 124
        Top = 5
        Width = 56
        Height = 31
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRevisorRedefinido
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 182
        Top = 5
        Width = 220
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        ParentFont = False
        TabOrder = 1
        OnClick = CBRevisorClick
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDigitador: TdmkEditCB
        Left = 124
        Top = 37
        Width = 56
        Height = 31
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdDigitadorRedefinido
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 182
        Top = 37
        Width = 220
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        ParentFont = False
        TabOrder = 3
        OnClick = CBDigitadorClick
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Edit1: TEdit
        Left = 305
        Top = 69
        Width = 96
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        Text = '0,000'
      end
    end
  end
  object PnInfoBig: TPanel
    Left = 0
    Top = 0
    Width = 1471
    Height = 97
    Align = alTop
    TabOrder = 0
    object PnDigitacao: TPanel
      Left = 1107
      Top = 1
      Width = 363
      Height = 95
      Align = alRight
      Enabled = False
      TabOrder = 0
      object Label12: TLabel
        Left = 1
        Top = 1
        Width = 361
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = ' Digita'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 75
      end
      object PnBox: TPanel
        Left = 227
        Top = 22
        Width = 52
        Height = 72
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 52
          Height = 21
          Align = alTop
          Caption = 'Box:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 33
        end
        object EdBox: TdmkEdit
          Left = 0
          Top = 21
          Width = 52
          Height = 51
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdBoxChange
        end
      end
      object PnArea: TPanel
        Left = 1
        Top = 22
        Width = 226
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 226
          Height = 21
          Align = alTop
          Caption = #193'rea: [F4] para meios'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 165
        end
        object LaTipoArea: TLabel
          Left = 203
          Top = 21
          Width = 23
          Height = 51
          Align = alRight
          Caption = '?'#178
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitTop = 23
          ExplicitHeight = 29
        end
        object EdArea: TdmkEdit
          Left = 0
          Top = 21
          Width = 203
          Height = 51
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdAreaKeyDown
        end
      end
      object PnSubClass: TPanel
        Left = 279
        Top = 22
        Width = 83
        Height = 72
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object Label86: TLabel
          Left = 0
          Top = 0
          Width = 83
          Height = 21
          Align = alTop
          Caption = 'SubClas:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 64
        end
        object EdSubClass: TdmkEdit
          Left = 0
          Top = 21
          Width = 83
          Height = 51
          Align = alClient
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSubClassChange
          OnExit = EdSubClassExit
          OnKeyDown = EdSubClassKeyDown
        end
      end
    end
    object PnJaClass: TPanel
      Left = 321
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 1
      object Label15: TLabel
        Left = 1
        Top = 1
        Width = 318
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros j'#225' classificados'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 168
      end
      object Panel10: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 120
          Height = 21
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdJaFoi_PECA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 90
        end
        object DBEdJaFoi_PECA: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_PECA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Panel9: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 1
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 198
          Height = 21
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdJaFoi_AREA
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 46
        end
        object DBEdJaFoi_AREA: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_AREA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object PnNaoClass: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 95
      Align = alLeft
      TabOrder = 2
      object Label19: TLabel
        Left = 1
        Top = 1
        Width = 318
        Height = 21
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros que faltam classificar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 213
      end
      object PnIntMei: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 72
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 0
          Top = 0
          Width = 120
          Height = 21
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdSdoVrtPeca
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 90
        end
        object DBEdSdoVrtPeca: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtPeca'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnChange = DBEdSdoVrtPecaChange
          ExplicitHeight = 47
        end
      end
      object Panel15: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 72
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel9'
        TabOrder = 1
        object Label25: TLabel
          Left = 0
          Top = 0
          Width = 198
          Height = 21
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdSdoVrtArM2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 46
        end
        object DBEdSdoVrtArM2: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 51
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtArM2'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object PnMenu: TPanel
      Left = 641
      Top = 1
      Width = 466
      Height = 95
      Align = alClient
      TabOrder = 3
      object BtEncerra: TBitBtn
        Tag = 10134
        Left = 98
        Top = 4
        Width = 216
        Height = 72
        Caption = '&Menu'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = False
        OnClick = BtEncerraClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 541
        Top = 4
        Width = 72
        Height = 72
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        TabStop = False
        OnClick = BtReabreClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 320
        Top = 4
        Width = 216
        Height = 72
        Caption = '&Imprimir'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        TabStop = False
        OnClick = BtImprimeClick
      end
      object EdItens: TEdit
        Left = 16
        Top = 64
        Width = 78
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Text = 'EdItens'
      end
    end
  end
  object PnExtras: TPanel
    Left = 0
    Top = 97
    Width = 229
    Height = 605
    Align = alLeft
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 1
      Top = 152
      Width = 227
      Height = 6
      Cursor = crVSplit
      Align = alBottom
    end
    object Splitter2: TSplitter
      Left = 1
      Top = 379
      Width = 227
      Height = 4
      Cursor = crVSplit
      Align = alBottom
    end
    object DBGItensACP: TdmkDBGridZTO
      Left = 1
      Top = 1
      Width = 227
      Height = 151
      Align = alClient
      DataSource = DsItensACP
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      OptionsEx = []
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'VMI_Dest'
          Title.Caption = 'IME-I Ori.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID couro'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AreaM2'
          Title.Caption = #193'rea'
          Width = 56
          Visible = True
        end>
    end
    object Memo1: TMemo
      Left = 156
      Top = 12
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo1')
      TabOrder = 1
      Visible = False
    end
    object PnDesnate: TPanel
      Left = 1
      Top = 383
      Width = 227
      Height = 221
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object Panel45: TPanel
        Left = 0
        Top = 0
        Width = 227
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SbDesnate: TSpeedButton
          Left = 136
          Top = 4
          Width = 23
          Height = 22
          OnClick = SbDesnateClick
        end
        object Label77: TLabel
          Left = 4
          Top = 8
          Width = 44
          Height = 13
          Caption = 'Desnate:'
        end
        object EdDesnate: TdmkEdit
          Left = 52
          Top = 4
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkDesnate: TCheckBox
          Left = 172
          Top = 8
          Width = 97
          Height = 17
          Caption = 'Auto.'
          TabOrder = 1
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 29
        Width = 227
        Height = 192
        Align = alClient
        DataSource = DsVSCacIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
    end
    object PnEqualize: TPanel
      Left = 1
      Top = 158
      Width = 227
      Height = 221
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object PnCfgEqz: TPanel
        Left = 0
        Top = 0
        Width = 227
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SbEqualize: TSpeedButton
          Left = 136
          Top = 8
          Width = 23
          Height = 22
          OnClick = SbEqualizeClick
        end
        object Label88: TLabel
          Left = 8
          Top = 12
          Width = 43
          Height = 13
          Caption = 'Equ'#225'lize:'
        end
        object EdEqualize: TdmkEdit
          Left = 52
          Top = 8
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkEqualize: TCheckBox
          Left = 164
          Top = 0
          Width = 50
          Height = 17
          Caption = 'Auto.'
          TabOrder = 1
        end
        object CkNota: TCheckBox
          Left = 164
          Top = 16
          Width = 50
          Height = 17
          Caption = 'Nota.'
          TabOrder = 2
          OnClick = CkNotaClick
        end
      end
      object DBGNotaEqz: TDBGrid
        Left = 0
        Top = 74
        Width = 227
        Height = 147
        Align = alClient
        DataSource = DsNotas
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sub classe'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
      object PnNota: TPanel
        Left = 0
        Top = 33
        Width = 227
        Height = 41
        Align = alTop
        TabOrder = 2
        Visible = False
        object DBEdNotaEqzM2: TDBEdit
          Left = 1
          Top = 1
          Width = 225
          Height = 39
          Align = alClient
          DataField = 'NotaEqzM2'
          DataSource = DsNotaCrr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Font.Quality = fqAntialiased
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
  end
  object PnAll: TPanel
    Left = 1151
    Top = 97
    Width = 320
    Height = 605
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object SGAll: TStringGrid
      Left = 1
      Top = 65
      Width = 318
      Height = 539
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      ColCount = 13
      DefaultRowHeight = 28
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 318
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object BitBtn1: TBitBtn
        Left = 8
        Top = 8
        Width = 76
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'BitBtn1'
        TabOrder = 0
      end
      object EdAll: TdmkEdit
        Left = 88
        Top = 32
        Width = 110
        Height = 30
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdAll'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdAll'
        ValWarn = False
      end
      object EdArrAll: TdmkEdit
        Left = 204
        Top = 32
        Width = 110
        Height = 28
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdAll'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdAll'
        ValWarn = False
      end
    end
  end
  object GPBoxes: TGridPanel
    Left = 229
    Top = 97
    Width = 922
    Height = 605
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Caption = '...'
    ColumnCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    ControlCollection = <>
    RowCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    TabOrder = 4
  end
  object QrVSPaRclCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaRclCabBeforeClose
    AfterScroll = QrVSPaRclCabAfterScroll
    OnCalcFields = QrVSPaRclCabCalcFields
    SQL.Strings = (
      'SELECT vga.GraGruX, vga.Nome, '
      'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'vga.VSPallet'
      'FROM vspaclacab pcc'
      'LEFT JOIN vsgerart vga ON vga.Codigo=pcc.vsgerart'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vga.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa'
      'WHERE pcc.Codigo=5'
      ''
      ''
      '')
    Left = 24
    Top = 308
    object QrVSPaRclCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPaRclCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPaRclCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSPaRclCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
    end
    object QrVSPaRclCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaRclCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaRclCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaRclCabVSGerRcl: TIntegerField
      FieldName = 'VSGerRcl'
    end
    object QrVSPaRclCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
    end
    object QrVSPaRclCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
    end
    object QrVSPaRclCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
    end
    object QrVSPaRclCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
    end
    object QrVSPaRclCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
    end
    object QrVSPaRclCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
    end
    object QrVSPaRclCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
      Origin = 'vspaclacaba.LstPal07'
    end
    object QrVSPaRclCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
      Origin = 'vspaclacaba.LstPal08'
    end
    object QrVSPaRclCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
      Origin = 'vspaclacaba.LstPal09'
    end
    object QrVSPaRclCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
      Origin = 'vspaclacaba.LstPal10'
    end
    object QrVSPaRclCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
      Origin = 'vspaclacaba.LstPal11'
    end
    object QrVSPaRclCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
      Origin = 'vspaclacaba.LstPal12'
    end
    object QrVSPaRclCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
      Origin = 'vspaclacaba.LstPal13'
    end
    object QrVSPaRclCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
      Origin = 'vspaclacaba.LstPal14'
    end
    object QrVSPaRclCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
      Origin = 'vspaclacaba.LstPal15'
    end
    object QrVSPaRclCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaRclCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaRclCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaRclCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaRclCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaRclCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaRclCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaRclCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaRclCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaRclCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaRclCabVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaRclCabVSGerRclA: TIntegerField
      FieldName = 'VSGerRclA'
    end
    object QrVSPaRclCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
  end
  object DsVSPaRclCab: TDataSource
    DataSet = QrVSPaRclCab
    Left = 24
    Top = 356
  end
  object QrVSGerArtNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wbp.Empresa, wbp.GraGruX,'
      'SUM(wmi.Pecas) Pecas, SUM(wmi.PesoKg) PesoKg,  '
      'SUM(wmi.AreaM2) AreaM2, SUM(wmi.AreaP2) AreaP2,  '
      'SUM(wmi.ValorT) ValorT,  '
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,  '
      
        'IF(COUNT(DISTINCT wmi.SerieFch) <> 1, 0, wmi.SerieFch) SerieFch,' +
        ' '
      'IF(COUNT(DISTINCT wmi.Ficha) <> 1, 0, wmi.Ficha) Ficha, '
      
        'IF(COUNT(DISTINCT wmi.Terceiro) <> 1, 0, wmi.Terceiro) Terceiro,' +
        ' '
      'IF(COUNT(DISTINCT wmi.Terceiro) <> 1, "V'#225'rios", '
      '  IF(wmi.Terceiro=0, "V'#225'rios",  '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)  '
      ')) NO_FORNECE,  '
      'IF(COUNT(DISTINCT wmi.SerieFch) <> 1, "V'#225'rias",'
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL,  '
      '"?", vsf.Nome), " ", wmi.Ficha))) NO_FICHA,  '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2,  '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2  '
      'FROM vspalleta  wbp  '
      'LEFT JOIN vsmovits   wmi ON wbp.Codigo=wmi.Pallet  '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wbp.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro  '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch  '
      'WHERE wmi.Pallet>=0 '
      'AND wmi.Pecas>0  '
      'GROUP BY wmi.Pallet  ')
    Left = 164
    Top = 352
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSGerArtNewNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
      Size = 72
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSGerArtNewClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 164
    Top = 401
  end
  object QrAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'ORDER BY Controle DESC')
    Left = 24
    Top = 496
    object QrAllControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAllBox: TIntegerField
      FieldName = 'Box'
    end
    object QrAllVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrAllVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrAllVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrAllVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrAllSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
  end
  object DsAll: TDataSource
    DataSet = QrAll
    Left = 24
    Top = 544
  end
  object QrSumVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 608
    Top = 552
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 460
    Top = 488
    object EstaOCOrdemdeclassificao1: TMenuItem
      Caption = 'Encerra esta OC (Ordem de classifica'#231#227'o)'
      OnClick = EstaOCOrdemdeclassificao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Palletdobox01: TMenuItem
      Tag = 1
      Caption = 'Encerra o pallet do box 0&1'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox02: TMenuItem
      Tag = 2
      Caption = 'Encerra o pallet do box 0&2'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox03: TMenuItem
      Tag = 3
      Caption = 'Encerra o pallet do box 0&3'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox04: TMenuItem
      Tag = 4
      Caption = 'Encerra o pallet do box 0&4'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox05: TMenuItem
      Tag = 5
      Caption = 'Encerra o pallet do box 0&5'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox06: TMenuItem
      Tag = 6
      Caption = 'Encerra o pallet do box 0&6'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox07: TMenuItem
      Tag = 7
      Caption = 'Encerra o pallet do box 0&7'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox08: TMenuItem
      Tag = 8
      Caption = 'Encerra o pallet do box 0&8'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox09: TMenuItem
      Tag = 9
      Caption = 'Encerra o pallet do box 0&9'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox10: TMenuItem
      Tag = 10
      Caption = 'Encerra o pallet do box 10'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox11: TMenuItem
      Tag = 11
      Caption = 'Encerra o pallet do box 11'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox12: TMenuItem
      Tag = 12
      Caption = 'Encerra o pallet do box 12'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox13: TMenuItem
      Tag = 13
      Caption = 'Encerra o pallet do box 13'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox14: TMenuItem
      Tag = 14
      Caption = 'Encerra o pallet do box 14'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox15: TMenuItem
      Tag = 15
      Caption = 'Encerra o pallet do box 15'
      OnClick = EncerrarPalletClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostrartodosboxes1: TMenuItem
      Caption = 'Mostrar todos boxes'
      OnClick = Mostrartodosboxes1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ImprimirfluxodemovimentodoPallet1: TMenuItem
      Caption = 'Imprimir fluxo de movimento do Pallet'
      OnClick = ImprimirfluxodemovimentodoPallet1Click
    end
    object ImprimirfluxodemovimentodoIMEI1: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Aumentarboxesdisponveis1: TMenuItem
      Caption = 'Aumentar boxes dispon'#237'veis'
      OnClick = Aumentarboxesdisponveis1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object TesteInclusao1: TMenuItem
      Caption = 'Teste Inclus'#227'o'
      OnClick = TesteInclusao1Click
    end
  end
  object QrItensACP: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrItensACPAfterOpen
    BeforeClose = QrItensACPBeforeClose
    AfterScroll = QrItensACPAfterScroll
    Left = 20
    Top = 148
    object QrItensACPCacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrItensACPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItensACPControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrItensACPAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrItensACPAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrItensACPVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object DsItensACP: TDataSource
    DataSet = QrItensACP
    Left = 20
    Top = 196
  end
  object QrVSPallet0: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSPallet0AfterOpen
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspallet let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      '')
    Left = 24
    Top = 57
    object QrVSPallet0FatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSPallet0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPallet0Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPallet0Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPallet0DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPallet0DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPallet0UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPallet0UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPallet0AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPallet0Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPallet0Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPallet0Status: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPallet0CliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPallet0GraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPallet0NO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPallet0NO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPallet0NO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPallet0QtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
  end
  object DsVSPallet0: TDataSource
    DataSet = QrVSPallet0
    Left = 24
    Top = 101
  end
  object QrRevisores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 84
    Top = 452
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 84
    Top = 496
  end
  object QrDigitadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 164
    Top = 456
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 164
    Top = 500
  end
  object QrSorces: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 156
    object QrSorcesPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSorcesAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSorcesAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrSorcesVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
  end
  object QrVsiDest: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 60
    object QrVsiDestMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVsiDestMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVsiDestCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVsiDestControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVsiDestGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVsiDestMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVsiDestEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVsiDestTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
  end
  object QrVsiSorc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 108
    object QrVsiSorcGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVsiSorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVsiSorcValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVsiSorcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVsiSorcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVsiSorcFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVsiSorcSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVsiSorcMarca: TWideStringField
      FieldName = 'Marca'
    end
  end
  object QrSumSorc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 204
    object QrSumSorcPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumSorcAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrSumSorcAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object PMAll: TPopupMenu
    Left = 1764
    Top = 404
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual - '#225'rea'
      OnClick = Alteraitematual1Click
    end
    object AlteraitematualSubClasse1: TMenuItem
      Caption = 'Altera item atual - Sub &Classe'
      OnClick = AlteraitematualSubClasse1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
    object Excluiitensselecionadonovo1: TMenuItem
      Caption = 'Exclui itens selecionado (novo)'
      OnClick = Excluiitensselecionadonovo1Click
    end
  end
  object QrVSCacIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVSCacItsCalcFields
    SQL.Strings = (
      'SELECT pla.GraGruX, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'ggx.GraGru1, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR '
      'FROM vscacitsa cia '
      'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE cia.vmi_Sorc=5'
      'GROUP BY pla.GraGruX ')
    Left = 96
    Top = 624
    object QrVSCacItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSCacItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSCacItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSCacItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCacItsPercM2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercM2'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object QrVSCacItsPercPc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PercPc'
      Calculated = True
    end
    object QrVSCacItsMediaM2PC: TFloatField
      FieldName = 'MediaM2PC'
    end
    object QrVSCacItsAgrupaTudo: TFloatField
      FieldName = 'AgrupaTudo'
    end
  end
  object QrVSCacSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2'
      'FROM vscacitsa cia '
      'WHERE cia.vmi_Sorc=5')
    Left = 168
    Top = 644
    object QrVSCacSumPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacSumAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSCacSumAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
  end
  object DsVSCacIts: TDataSource
    DataSet = QrVSCacIts
    Left = 96
    Top = 668
  end
  object QrSumDest1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 728
    Top = 616
  end
  object QrSumSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 732
    Top = 664
  end
  object QrVMISorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 812
    Top = 616
  end
  object QrPalSorc1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 812
    Top = 664
  end
  object QrNotaCrr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 932
    Top = 608
    object QrNotaCrrPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaCrrAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaCrr: TDataSource
    DataSet = QrNotaCrr
    Left = 932
    Top = 652
  end
  object QrNotas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,'
      'SUM(cia.AreaM2) / 27.0034 PercM2 '
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vdc.Codigo=1'
      'GROUP BY cia.SubClass')
    Left = 992
    Top = 608
    object QrNotasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrNotasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasNotaM2: TFloatField
      FieldName = 'NotaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasPercM2: TFloatField
      FieldName = 'PercM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 992
    Top = 652
  end
  object QrNotaAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 872
    Top = 612
    object QrNotaAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaAll: TDataSource
    DataSet = QrNotaAll
    Left = 872
    Top = 656
  end
  object PMIMEI: TPopupMenu
    Left = 428
    Top = 624
    object ImprimirfluxodemovimentodoPallet2: TMenuItem
      Caption = 'Imprimir fluxo de movimento do Pallet'
      OnClick = ImprimirfluxodemovimentodoPallet2Click
    end
  end
  object QrPalIMEIs: TmySQLQuery
    Database = Dmod.MyDB
    Left = 24
    Top = 604
    object QrPalIMEIsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumTCalcFields
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 24
    Top = 404
    object QrSumTSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumTSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTJaFoi_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_PECA'
      Calculated = True
    end
    object QrSumTJaFoi_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 24
    Top = 452
  end
  object DqAll: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 252
    Top = 104
  end
  object DqItens: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 292
    Top = 104
  end
  object DqSumPall: TmySQLDirectQuery
    Database = Dmod.MyDB
    Left = 344
    Top = 104
  end
end
