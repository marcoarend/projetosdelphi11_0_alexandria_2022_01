unit Module;
// LiberaUso5
interface

uses
  Winapi.Windows, Vcl.Dialogs, (*Messages, Graphics, Controls, Forms,
  (*DBTables,*) FileCtrl, UMySQLModule, dmkEdit,
  Winsock, MySQLBatch, frxClass, frxDBSet,
  Variants, ABSMain, StdCtrls, ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts;*)
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Vcl.Forms,
  DmkGeral, ZCF2, UnInternalConsts, UnProjGroup_Vars, UnGrl_Vars;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrIdx: TmySQLQuery;
    QrNTV: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrControle: TmySQLQuery;
    QrNTI: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrUpdU: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    ZZDB: TmySQLDatabase;
    QrUpdM: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterENumero: TFloatField;
    QrMasterUsaAccMngr: TSmallintField;
    QrControleSoMaiusculas: TWideStringField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControleMoeda: TWideStringField;
    QrControleCodigo: TIntegerField;
    QrControleVersao: TIntegerField;
    QrControleDono: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleTravaCidade: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleCidade: TWideStringField;
    QrControleVerBcoTabs: TIntegerField;
    QrCRCCtrl: TmySQLQuery;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsVSMulFrnCab: TIntegerField;
    QrVSMovItsClientMO: TIntegerField;
    QrCRCCtrlServerID: TIntegerField;
    QrCRCCtrlCodigo: TIntegerField;
    QrCRCCtrlCanAltBalVS: TSmallintField;
    QrCRCCtrlInfoMulFrnImpVS: TSmallintField;
    QrCRCCtrlQtdBoxClas: TSmallintField;
    QrCRCCtrlVSWarSemNF: TSmallintField;
    QrCRCCtrlVSWarNoFrn: TSmallintField;
    QrCRCCtrlVSInsPalManu: TSmallintField;
    QrCRCCtrlVSImpRandStr: TWideStringField;
    QrCRCCtrlERP_Host: TWideStringField;
    QrCRCCtrlERP_DBName: TWideStringField;
    QrCRCCtrlERP_DBPorta: TIntegerField;
    QrCRCCtrlSrvSCenLoc: TIntegerField;
    QrCRCCtrlSrvFrnMO_BH1: TIntegerField;
    QrCRCCtrlSrvFrnMO_BH2: TIntegerField;
    QrCRCCtrlSrvFrnMO_BH3: TIntegerField;
    QrCRCCtrlSrvFrnMO_WE1: TIntegerField;
    QrCRCCtrlSrvFrnMO_WE2: TIntegerField;
    QrCRCCtrlSrvFrnMO_WE3: TIntegerField;
    QrCRCCtrlERP_DBRepos: TWideStringField;
    QrCRCCtrlNObrigNFeVS: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure QrCRCCtrlAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenControle();
    function  ReopenCRCCtrl(): Boolean;
    function  ReopenParamsEspecificos(Empresa: Integer): Boolean;
    function  TabelasQueNaoQueroCriar(): String;
  end;

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses
  DmkDAC_PF, UnGOTOy, Principal, Servidor, ModuleGeral, VerifiDB, UnLic_Dmk,
  {[***Desmarcar***]
  VerifiDBi,
  ModuleFin,
  [***NomeApp***]_Dmk,
  }
  MyListas, UnDmkWeb;

{ TDmod }

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica: Boolean;
begin
  if MyDB.Connected then
    Geral.MB_Aviso('MyDB est� connectado antes da configura��o!');
  if MyLocDataBase.Connected then
    Geral.MB_Aviso('MyLocDataBase est� connectado antes da configura��o!');
  if MyDBn.Connected then
    Geral.MB_Aviso('MyDBn est� connectado antes da configura��o!');

  MyDB.LoginPrompt := False;

  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;

  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  VAR_BDSENHA := 'wkljweryhvbirt';
  {[***Desmarcar***]
  if not GOTOy.OMySQLEstaInstalado(Fm[NomeApp]_Dmk.LaAviso1,
    Fm[NomeApp]_Dmk.LaAviso2, Fm[NomeApp]_Dmk.ProgressBar1) then
  begin
    MyObjects.Informa2(Fm[***NomeApp***]_Dmk.LaAviso1, Fm[***NomeApp***]_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    Fm[***NomeApp***]_Dmk.BtEntra.Visible := False;
    Exit;
  end;
  }
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then Geral.MB_Aviso('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MB_Aviso('M�quina cliente sem rede.');
        Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, VAR_IP,
    HKEY_LOCAL_MACHINE);
  if VAR_IP = CO_VAZIO then  // se ainda n�o estiver setado
    VAR_IP := Geral.ReadAppKeyLM('IPServer', Application.Title, ktString, CO_VAZIO);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
{
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
}
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP, VAR_PORTA,  VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  UnDmkDAC_PF.AbreQuery(QrAux, MyDB);
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados ' + TMeuDB +
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      MyDB.Disconnect;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
{$IfNDef SemDBLocal}
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  UnDmkDAC_PF.AbreQuery(QrAux, MyLocDatabase);
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
{$EndIf}
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then
    Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?');
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  Mylist.ConfiguracoesIniciais(1, Application.Name);
  //
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados Geral');
    Application.Terminate;
    Exit;
  end;
  {[***Desmarcar***]
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MB_Ero('Imposs�vel criar Modulo Financeiro');
    Application.Terminate;
    Exit;
  end;
}
  if Verifica or (DmodG.FCriouDB = False) then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    UnDmkDAC_PF.AbreQuery(QrMaster, Dmod.MyDB);
  {[***Desmarcar***]
    ReopenOpcoes[***SiglaApp***]();
}
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
      if ZZTerminate then
        Exit;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  {[***Desmarcar***]
  //
  // Conex�o na base de dados na url
  // S� pode ser ap�s abrir a tabela controle!
  // ver se est� ativado s� ent�o fazer !
  try
    if DmkWeb.ConexaoRemota(MyDBn, DmodG.QrOpcoesGerl, 1) then
    begin
      MyDBn.Connect;
      if MyDBn.Connected and Verifica then
      begin
        if not VAR_VERIFI_DB_CANCEL then
        begin
          Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
          with FmVerifiDbi do
          begin
            BtSair.Enabled := False;
            FVerifi := True;
            ShowModal;
            FVerifi := False;
            Destroy;
          end;
        end;
      end;
    end;
  except
    Geral.MB_Aviso('N�o foi poss�vel se conectar � base de dados remota!');
  end;
}
  //
////////////////////////////////////////////////////////////////////////////////
  Lic_Dmk.LiberaUso5;
  //Geral.MB_Aviso('"Uso5" desabilitado!');
////////////////////////////////////////////////////////////////////////////////
  VAR_DB := MyDB.DatabaseName;
end;

procedure TDmod.QrCRCCtrlAfterOpen(DataSet: TDataSet);
var
  Corda: String;
  //
  procedure AddS(S: String);
  begin
    if Corda <> '' then
      Corda := Corda + ',';
    Corda := Corda + S;
  end;
  procedure AddI(I: Integer);
  begin
    if I <> 0 then
      AddS(Geral.FF0(I));
  end;
begin
  VAR_TXT_AWServerID    := Geral.FF0(QrCRCCtrlServerID.Value);
  VAR_InsUpd_AWServerID := QrCRCCtrlServerID.Value <> 0;
  VAR_VSInsPalManu      := 1;//QrCRCCtrlVSInsPalManu.Value;
  VAR_InfoMulFrnImpVS   := QrCRCCtrlInfoMulFrnImpVS.Value;
  VAR_VSImpRandStr      := QrCRCCtrlVSImpRandStr.Value;
  VAR_VSWarNoFrn        := QrCRCCtrlVSWarNoFrn.Value;
  VAR_VSWarSemNF        := QrCRCCtrlVSWarSemNF.Value;
  VARF_NObrigNFeVS      := QrCRCCtrlNObrigNFeVS.Value;
  //
{ Desabilitei em 2108-03-26 porque vai precisar dos outros para sa�das de
  opera��o, proceso, tranfer�cia, etc
  //
  if QrCRCCtrlSrvSCenLoc.Value <> 0 then
    VAR_CRC_StqCen_LOC := 'AND scl.Controle=' + Geral.FF0(QrCRCCtrlSrvSCenLoc.Value);
}
  //
  VAR_CRC_FMO_BH := '';
  Corda :=  '';
  AddI(QrCRCCtrlSrvFrnMO_BH1.Value);
  AddI(QrCRCCtrlSrvFrnMO_BH2.Value);
  AddI(QrCRCCtrlSrvFrnMO_BH3.Value);
  if Corda <> '' then
    VAR_CRC_FMO_BH := 'AND fmo.Codigo IN (' + Corda + ')';
  //
  VAR_CRC_FMO_WE := '';
  Corda :=  '';
  AddI(QrCRCCtrlSrvFrnMO_WE1.Value);
  AddI(QrCRCCtrlSrvFrnMO_WE2.Value);
  AddI(QrCRCCtrlSrvFrnMO_WE3.Value);
  if Corda <> '' then
    VAR_CRC_FMO_WE := 'AND fmo.Codigo IN (' + Corda + ')';
  //
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
{
CNPJ n�o definido ou Empresa n�o definida.
Algumas ferramentas do aplicativo poder�o ficar inacess�veis.
C�digo = ?
}
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  Geral.MB_Aviso('CNPJ n�o definido ou Empresa n�o definida. ' + sLineBreak +
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title + '  ::  ' + QrMasterEm.Value +
  ' # ' + QrMasterCNPJ.Value;
  //
  ReopenControle();
  ReopenCRCCtrl();
  DModG.ReopenOpcoesGerl;
  //
  if QrControleSoMaiusculas.Value = 'V' then
    VAR_SOMAIUSCULAS := True;
{$IfNDef SemEntidade}
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_MOEDA           := QrControleMoeda.Value;
{$EndIf}
{$IfNDef NO_FINANCEIRO}
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
{$EndIf}
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
  QrMasterTE1_TXT.Value  := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value  := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.ReopenControle();
begin
  UnDmkDAC_PF.AbreQuery(QrControle, Dmod.MyDB);
end;


function TDmod.ReopenCRCCtrl(): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCRCCtrl, Dmod.MyDB, [
  'SELECT * FROM crcctrl ',
  'WHERE Codigo=1 ',
  '']);
  Result := QrCRCCtrl.RecordCount > 0;
end;

function TDmod.ReopenParamsEspecificos(Empresa: Integer): Boolean;
begin
  // Compatibilidade!!
  Result := True;
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  // Compatibilidade
end;

end.
