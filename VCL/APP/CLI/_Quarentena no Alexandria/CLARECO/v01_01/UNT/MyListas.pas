{$I dmk.inc}
unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, dmkGeral, UnDmkProcFunc,
  UnDmkEnums;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    //
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
  end;

const
  CO_VERSAO = 1803240525;
  CO_SIGLA_APP = 'CLRC';
  CO_VERSAO_BETA = False;
  //
  CO_DMKID_APP = 47;
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  //
  CO_TabLctA = 'lct'+'0001a';
  CO_EXTRA_LCT_003 = False; // True somente para Credito2
  //

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;

   _ArrClieSets: array[01..32] of String;
   _MaxClieSets: Integer;
   _ArrFornSets: array[01..32] of String;
   _MaxFornSets: Integer;

implementation

uses MyDBCheck, Module, ModuleGeral, UMySQLModule,
  UnVS_Tabs, UnPQ_Tabs,
  Geral_TbTerc, UnALL_Tabs, UnPerfJan_Tabs, Mail_Tabs,
 (*NFSe_TbTerc, CashTabs,
  NFe_Tabs, SPED_EFD_Tabs, SINTEGRA_Tabs,
  Diario_Tabs, UnPerfJan_Tabs, Bina_Tabs, Bloq_Tabs, CNAB_Tabs,
  Protocol_Tabs, Contrat_Tabs, FPMin_Tabs, FPMax_Tabs, NFSe_Tabs,
  Agenda_Tabs, UnBugMbl_Tabs, WTextos_Tabs, FTP_Tabs, WUsers_Tabs,
  CRO_Tabs,*)
  //
  UnEnti_Tabs, UnIBGE_DTB_Tabs, UnGrade_Tabs, UnUMedi_Tabs,
  UnEmpresas_Tabs;



function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
 TList<TTabelas>): Boolean;
  procedure TabelasPorCliente((*Tab: TTabsCli*));
  var
    TbCI: String;
    CliInt: Integer;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM ' + TMeuDB);
    Dmod.QrAux.SQL.Add('LIKE "entidades"');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT cliint');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE cliint <> 0');
      UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      //
      while not Dmod.QrAux.Eof do
      begin
        CliInt := Dmod.QrAux.FieldByName('cliint').AsInteger;
        { N�o permitir n�meros negativos para filial!
        if CliInt < 0 then
          TbCI := '_' + FormatFloat('000', CliInt )
        else
          TbCI := FormatFloat('0000', CliInt);
        }
        if CliInt > 0 then
        begin
          TbCI := FormatFloat('0000', CliInt);
          //
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'A'), Lowercase(LAN_CTOS));
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'B'), Lowercase(LAN_CTOS));
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'D'), Lowercase(LAN_CTOS));
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      ALL_Tabs.CarregaListaTabelas(FTabelas);
      PerfJan_Tabs.CarregaListaTabelas(FTabelas);
      Enti_Tabs.CarregaListaTabelas(FTabelas);
      VS_Tabs.CarregaListaTabelas(FTabelas);
      PQ_Tabs.CarregaListaTabelas(FTabelas);
      Grade_Tabs.CarregaListaTabelas(FTabelas);
      UMedi_Tabs.CarregaListaTabelas(Database, FTabelas);
(*
      CashTb.CarregaListaTabelasCashier(FTabelas);
      CashTb.ComplementaListaComLcts(Lista);
      NFe_Tb.CarregaListaTabelas(FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(FTabelas);
      SINTEGRA_Tb.CarregaListaTabelas(FTabelas);
      Diario_Tb.CarregaListaTabelas(FTabelas);
      UnBina_Tabs.CarregaListaTabelas(FTabelas);
      Bloq_Tb.CarregaListaTabelas(FTabelas);
      CNAB_Tb.CarregaListaTabelas(Database, FTabelas);
      Protocol_Tb.CarregaListaTabelas(Database, FTabelas);
      UnContrat_Tabs.CarregaListaTabelas(FTabelas);
      UnFPMin_Tabs.CarregaListaTabelas(Lista);
      UnNFSe_Tabs.CarregaListaTabelas(FTabelas);
      FTP_Tb.CarregaListaTabelas(Database, Lista);
      WTextos_Tb.CarregaListaTabelas(Database, Lista);
      //UnFPMax_Tabs.CarregaListaTabelas(Lista);
      UnAgenda_Tabs.CarregaListaTabelas(FTabelas);
      CRO_Tb.CarregaListaTabelas(FTabelas);
*)
      Mail_Tb.CarregaListaTabelas(Lista);
      Empresas_Tabs.CarregaListaTabelas(FTabelas);
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('controle'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CRCCtrl'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('CRCArtigos'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('CRCUniClassCab'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('CRCUniClassPal'), '');
      //
      //
    end else
    if Database = DModG.AllID_DB then
    begin
      UnGeral_TbTerc.CarregaListaTabelas(FTabelas);
      IBGE_DTB_Tabs.CarregaListaTabelas(FTabelas);
(*
      UnNFSe_TbTerc.CarregaListaTabelas(Lista);
*)
    end else
    if Database = Dmod.MyDBn then
    begin
(*
      BugMbl_Tabs.CarregaListaTabelas(FTabelas);
      FTP_Tb.CarregaListaTabelas(Database, Lista);
      WTextos_Tb.CarregaListaTabelas(Database, Lista);
      WUsers_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('arreits'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('bacen_pais'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('carteiras'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('cnab_cfg'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('dtb_munici'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('emailconta'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticliint'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entidades'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentidades'), 'entidades');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticargos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticonent'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticontat'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wenticonta'), 'enticontat');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entimail'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentimail'), 'entimail');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entitel'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentitel'), 'entitel');
      MyLinguas.AdTbLst(Lista, False, LowerCase('entitipcto'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('feriados'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('listalograd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('nfsenfscab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('preemail'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('preemmsg'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('prev'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('protocolos'), '');
      //
      TabelasPorCliente();
*)
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
(*
    CashTb.CarregaListaTabelasLocaisCashier(Lista);
    //FtabelasLocais.Add('');
*)
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('?????') then
    begin
      FListaSQL.Add('Codigo|Numero|Nome|Texto');
      FListaSQL.Add('1|999|"Teste Imagem"|"'+teste+'"');
    end else
}
    ALL_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
    IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    VS_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PQ_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Grade_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    UMedi_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
(*
    PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
    NFe_Tb.CarregaListaSQL(Tabela, FListaSQL);
    SPEDEFD_Tb.CarregaListaSQL(Tabela, FListaSQL);
    SINTEGRA_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Diario_Tb.CarregaListaSQL(Tabela, FListaSQL);
    UnBina_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Bloq_Tb.CarregaListaSQL(Tabela, FListaSQL);
    CNAB_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Protocol_Tb.CarregaListaSQL(Tabela, FListaSQL);
    UnContrat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //UnFPMax_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    FTP_Tb.CarregaListaSQL(Tabela, FListaSQL);
    WTextos_Tb.CarregaListaSQL(Tabela, FListaSQL);
    WUsers_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    UnAgenda_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
    UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
    //
    BugMbl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
    CRO_Tb.CarregaListaSQL(Tabela, FListaSQL);
*)
    Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129,"Compra de mercadorias diversas"');
      FListaSQL.Add('-130,"Frete de mercadorias"');
      FListaSQL.Add('-131,"Compra de filmes"');
      FListaSQL.Add('-132,"Frete de filmes"');
      FListaSQL.Add('-133,"Inv�lido"');
      FListaSQL.Add('-134,"Venda e/ou loca��o"');*)
    end;
    ALL_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    VS_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PQ_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
(*
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Diario_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    UnBina_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bloq_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Protocol_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    UnContrat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    FTP_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WUsers_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    //UnFPMax_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnAgenda_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    //
    BugMbl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //
    CRO_Tb.ComplementaListaSQL(Tabela, FListaSQL);
*)
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('CRCCtrl') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('CRCArtigos') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'UNIQUE1';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'GraGruX';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('CRCUniClassCab') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
    if Uppercase(TabelaBase) = Uppercase('CRCUniClassPal') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Controle';
      FLIndices.Add(FRIndices);
      //
    end else
    begin
      ALL_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      VS_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PQ_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Grade_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      UMedi_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
(*
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      NFe_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SPEDEFD_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SINTEGRA_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Diario_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnBina_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bloq_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      CNAB_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Protocol_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnContrat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMin_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //UnFPMax_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      FTP_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      WTextos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      WUsers_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnAgenda_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      BugMbl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      CRO_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
*)
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if Uppercase(AppIDtxt) = 'CLARECO' then
  begin
    VAR_CLIENTE1 := 'Cliente';
    VAR_CLIENTE2 := '';
    VAR_CLIENTE3 := '';
    VAR_CLIENTE4 := '';
    VAR_FORNECE1 := 'Fornecedor';
    VAR_FORNECE2 := 'Funcion�rio';
    VAR_FORNECE3 := '';
    VAR_FORNECE4 := '';
    VAR_FORNECE5 := '';
    VAR_FORNECE6 := '';
    VAR_FORNECE7 := '';
    VAR_FORNECE8 := '';
    VAR_QUANTI1NOME := '?????????';
    VAR_CAMPOTRANSPORTADORA := '?????????';
  end else
    Geral.MB_Aviso('Database para configura��es de "CheckBox" n�o definidos!');
end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'AtualizBloPrev';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else
    if Uppercase(Tabela) = Uppercase('CRCCtrl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ServerID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERP_Host';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERP_DBName';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERP_DBPorta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '3306';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvSCenLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '27';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvFrnMO_BH1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '38';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvFrnMO_BH2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvFrnMO_BH3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvFrnMO_WE1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '42';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvFrnMO_WE2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrvFrnMO_WE3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CanAltBalVS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfoMulFrnImpVS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdBoxClas';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSWarSemNF';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSWarNoFrn';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSInsPalManu';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSImpRandStr';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERP_DBRepos';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NObrigNFeVS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CRCArtigos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CRCUniClassCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigoOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox06';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox07';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox08';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox09';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox10';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox11';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox12';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox13';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox14';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallBox15';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CRCUniClassPal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Box';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PallDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigoOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigoDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classificador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Digitador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    begin
      ALL_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      VS_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PQ_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Grade_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UMedi_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
(*
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      NFe_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SPEDEFD_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      SINTEGRA_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Diario_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnBina_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bloq_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Protocol_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnContrat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMin_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //UnFPMax_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnAgenda_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      FTP_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WTextos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WUsers_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      //
      BugMbl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      CRO_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
*)
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    ALL_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    VS_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PQ_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Grade_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UMedi_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
(*
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    Diario_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnBina_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bloq_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    CNAB_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Protocol_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnContrat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMin_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //UnFPMax_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    NFe_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnAgenda_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    FTP_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WTextos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WUsers_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    BugMbl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    CRO_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
*)
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // CRC-SYNCR-000 :: Busca dados do ERP
  New(FRJanelas);
  FRJanelas.ID        := 'CRC-SYNCR-000';
  FRJanelas.Nome      := 'CRCSyncDwnERP';
  FRJanelas.Descricao := 'Busca dados do ERP';
  FRJanelas.Modulo    := 'CLRC';
  FLJanelas.Add(FRJanelas);
  //
  // CRC-SYNCR-001 :: Envio de dados ao ERP
  New(FRJanelas);
  FRJanelas.ID        := 'CRC-SYNCR-001';
  FRJanelas.Nome      := 'CRCSyncUpERP';
  FRJanelas.Descricao := 'Envio de dados ao ERP';
  FRJanelas.Modulo    := 'CLRC';
  FLJanelas.Add(FRJanelas);
  //
  // CRC-ONECL-000 :: Reclasse Couro a Couro
  New(FRJanelas);
  FRJanelas.ID        := 'CRC-ONECL-000';
  FRJanelas.Nome      := 'CRCReclassifOne';
  FRJanelas.Descricao := 'Reclasse Couro a Couro';
  FRJanelas.Modulo    := 'CLRC';
  FLJanelas.Add(FRJanelas);
  //
  // CRC-ONECL-001 :: Cadastro de Artigos
  New(FRJanelas);
  FRJanelas.ID        := 'CRC-ONECL-001';
  FRJanelas.Nome      := 'CRCArtigos';
  FRJanelas.Descricao := 'Cadastro de Artigos';
  FRJanelas.Modulo    := 'CLRC';
  FLJanelas.Add(FRJanelas);
  //
  // CRC-ONECL-002 :: Configura��o de Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'CRC-ONECL-002';
  FRJanelas.Nome      := 'CRCUniClassCab';
  FRJanelas.Descricao := 'Configura��o de Classifica��o';
  FRJanelas.Modulo    := 'CLRC';
  FLJanelas.Add(FRJanelas);
  //
  // CRC-ONECL-003 :: Op��es do M�dulo CRC
  New(FRJanelas);
  FRJanelas.ID        := 'CRC-ONECL-003';
  FRJanelas.Nome      := 'CRCCtrl';
  FRJanelas.Descricao := 'Op��es do M�dulo CRC';
  FRJanelas.Modulo    := 'CLRC';
  FLJanelas.Add(FRJanelas);
  //
(*
  // ATD-TELEF-001 :: Atendimento
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-001';
  FRJanelas.Nome      := 'DiarioTDI_01';
  FRJanelas.Descricao := 'Atendimento';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-002 :: Interlocu��o
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-002';
  FRJanelas.Nome      := 'DiarioTDI_01_Add';
  FRJanelas.Descricao := 'Interlocu��o';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-003 :: Entidade
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-003';
  FRJanelas.Nome      := 'DiarioTDI_01_ER0';
  FRJanelas.Descricao := 'Entidade';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-004 :: Pr�-Atendimento
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-004';
  FRJanelas.Nome      := 'DiarioTDI_01_Pre';
  FRJanelas.Descricao := 'Pr�-Atendimento';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-005 :: Terreno
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-005';
  FRJanelas.Nome      := 'DiarioTDI_01_STC';
  FRJanelas.Descricao := 'Terreno';
  FLJanelas.Add(FRJanelas);
  //
  // GSM-TELEF-001 :: Envio de SMS
  New(FRJanelas);
  FRJanelas.ID        := 'GSM-TELEF-001';
  FRJanelas.Nome      := 'FmGSM_Serial';
  FRJanelas.Descricao := 'Envio de SMS';
  FLJanelas.Add(FRJanelas);
  //
  //
*)
  ALL_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  VS_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PQ_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Grade_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UMedi_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
(*
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPEDEFD_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SINTEGRA_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Diario_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnBina_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bloq_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Protocol_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnContrat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMin_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //UnFPMax_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnAgenda_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  FTP_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WTextos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WUsers_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  BugMbl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  CRO_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
*)
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Result := True;
end;

end.
