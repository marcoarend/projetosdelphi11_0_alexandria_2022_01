program DermatekRaiseUpAgain;

uses
  Vcl.SvcMgr,
  DmkRaiseUpAgain in 'DmkRaiseUpAgain.pas' {DermatekRaiseUp: TService},
  UnMyProcesses in '..\..\..\UTL\_UNT\v01_01\UnMyProcesses.pas',
  UnApp_PF in 'v01_01\Units\UnApp_PF.pas',
  Principal in 'v01_01\Units\Principal.pas' {FmPrincipal};

{$R *.RES}

begin
  // Windows 2003 Server requires StartServiceCtrlDispatcher to be
  // called before CoRegisterClassObject, which can be called indirectly
  // by Application.Initialize. TServiceApplication.DelayInitialize allows
  // Application.Initialize to be called from TService.Main (after
  // StartServiceCtrlDispatcher has been called).
  //
  // Delayed initialization of the Application object may affect
  // events which then occur prior to initialization, such as
  // TService.OnCreate. It is only recommended if the ServiceApplication
  // registers a class object with OLE and is intended for use with
  // Windows 2003 Server.
  //
  // Application.DelayInitialize := True;
  //
  Application.Name := 'DermatekRaiseUp';
  Application.Title := 'Dermatek Raise Up Again';
  if not Application.DelayInitialize or Application.Installing then
    Application.Initialize;
  Application.CreateForm(TDermatekRaiseUp, DermatekRaiseUp);
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
