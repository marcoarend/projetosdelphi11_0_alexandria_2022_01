unit UnApp_PF;


interface

uses
  Winapi.Windows,
  Vcl.Forms,
  System.SysUtils,
  System.Classes,
  Winapi.PsAPI,
  ShellApi;

type

  TUnApp_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SalvaEmArquivoLog(Msg: String);
function RunAs(User, Password, Command: String): Integer;
  end;

var
  App_PF: TUnApp_PF;


implementation

{ TUnApp_PF }

//Bom gente... resolvi o problema... pra executar um aplicativo como outro usuario do computador, faz-se o seguinte...
  var dwSize: DWORD;
  hToken: THandle;
  lpvEnv: Pointer;
  pi: TProcessInformation;
  si: TStartupInfo;
  szPath: Array [0..MAX_PATH] of WideChar;

function CreateProcessWithLogon(lpUsername: PWideChar;
lpDomain: PWideChar;
lpPassword: PWideChar;
dwLogonFlags: DWORD;
lpApplicationName: PWideChar;
lpCommandLine: PWideChar;
dwCreationFlags: DWORD;
lpEnvironment: Pointer;
lpCurrentDirectory: PWideChar;
var lpStartupInfo: TStartupInfo;
var lpProcessInfo: TProcessInformation): BOOL; stdcall;
external 'advapi32' name 'CreateProcessWithLogonW';

function CreateEnvironmentBlock(var lpEnvironment: Pointer;
hToken: THandle;
bInherit: BOOL): BOOL; stdcall; external 'userenv';

function DestroyEnvironmentBlock(pEnvironment: Pointer): BOOL; stdcall; external 'userenv';

const
LOGON_WITH_PROFILE = $00000001;


//Emulate the RunAs function



function TUnApp_PF.RunAs(User, Password, Command: String): Integer;
begin
  ZeroMemory(@szPath, SizeOf(szPath));
  ZeroMemory(@pi, SizeOf(pi));
  ZeroMemory(@si, SizeOf(si));
  si.cb := SizeOf(TStartupInfo);
  //
  if LogonUser(PChar(User), nil, PChar(Password), LOGON32_LOGON_INTERACTIVE,
  LOGON32_PROVIDER_DEFAULT, hToken) then
  begin
    if CreateEnvironmentBlock(lpvEnv, hToken, True) then
    begin
      dwSize:=SizeOf(szPath) div SizeOf(WCHAR);
      if (GetCurrentDirectoryW(dwSize, @szPath) > 0) then
      begin
        if (CreateProcessWithLogon(PWideChar(WideString(User)), nil, PWideChar(WideString(Password)),
        LOGON_WITH_PROFILE, nil, PWideChar(WideString(Command)), CREATE_UNICODE_ENVIRONMENT,
        lpvEnv, szPath, si, pi)) then
        begin
          Result := ERROR_SUCCESS;
          CloseHandle(pi.hProcess);
          CloseHandle(pi.hThread);
          end
        else
        Result := GetLastError;
      end
      else
        Result := GetLastError;
        DestroyEnvironmentBlock(lpvEnv);
      end
    else
      Result := GetLastError;
      CloseHandle(hToken);
    end
  else
  Result := GetLastError;
end;

procedure TUnApp_PF.SalvaEmArquivoLog(Msg: String);
const
  Dir = 'C:\Dermatek\DmkRaiseUpAgain\Logs\';
  VAR_RANDM_STR_ONLY_ONE_ACESS = '**RandomAcessStrHere**';
var
  ArqLog, aamm, PreTxt: String;
  Lista: TStringList;
begin
  if not DirectoryExists(Dir) then
    ForceDirectories(Dir);
  //
  PreTxt := '[' + FormatDateTime('dd/mm/yyyy hh:nn:ss', Now()) + '][' + VAR_RANDM_STR_ONLY_ONE_ACESS + ']:';
  aamm := FormatDateTime('yyyy_mm', Now());
  ArqLog := Dir + 'Log_' + aamm + '.log';
  try
    // Cria lista
    Lista := TStringList.Create;
    try
      // se o arquivo existe, carrega ele;
      if FileExists(ArqLog) then
        Lista.LoadFromFile(ArqLog);
      // adiciona a nova string
      Lista.Add(PreTxt + Application.Name + ': ' + Msg);
    except
      on E: Exception do
        Lista.Add(PreTxt + E.Message);
    end;
  finally
    // Atualiza log
    Lista.SaveToFile(ArqLog);
    // Libera lista
    FreeAndNil(Lista); //.Free;
  end;
end;

end.
