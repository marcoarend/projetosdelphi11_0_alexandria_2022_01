unit DmkRaiseUpAgain;
(*
--------------------------------------------------------------------------------
C:\Executaveis\19.0\DermatekRaiseAgain\
--------------------------------------------------------------------------------

1. Compilar com direitos administrativos:
   -Project
     -Options
       -Application
         -Manifest File: Custom
         -Custom manifest:
         C:\_Compilers\ProjetosDelphi10_2_2_Tokyo\VCL\UTL\Microsoft\default_app_highestAvailable.manifest
--------------------------------------------------------------------------------
2. Copiar o executavel para a pasta C:\Windows
--------------------------------------------------------------------------------
3. Instalar o servi�o atrav�s da linha de comando: (cmd)
   C:\Windows>DermatekRaiseAgain.exe /install      (n�o pode ter espa�o entre "/" e "install")
   Obs.: para desistalar: /uninstall
--------------------------------------------------------------------------------
4. Observa��o:
   Pelo Painel de controle > Ferramentas Administrativas / Servi�os  (Control
     Panel > Administration > Services )
   > pode-se Iniciar, pausar e parar o servi�o
   > ao iniciar o servi�o, ele aparece na guia processos e servi�os do
     Gerenciador de Tarefas  (Task Manager > Processes / services)

5. procedure TXXXXXX.ServiceAfterInstall(Sender: TService);
   Observa��o: implementar esta procedure para que a descri��o do servi�o
   apare�a no Painel de controle > Ferramentas Administrativas / Servi�os
*)
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, Vcl.Forms, Registry,
  Shellapi;

type
  TDermatekRaiseUp = class(TService)
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
  private
    { Private declarations }
    //FLogado: Boolean;
    procedure Executar();
    procedure Exe2();
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  DermatekRaiseUp: TDermatekRaiseUp;

implementation

uses UnMyProcesses, UnApp_PF, Principal;

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  DermatekRaiseUp.Controller(CtrlCode);
end;

procedure TDermatekRaiseUp.Exe2;
var
  Pasta, Executavel, Parametros: String;
  HProcesso, Retorno: Integer;
  Handle: Hwnd;
begin
//  App_PF.RunAs('angeloni', 'eletro26', 'C:\Executaveis\19.0\Afresh\Afresh.exe');
  //Pasta        := 'C:\Executaveis\19.0\Afresh\';
  //Executavel   := 'Afresh.exe';
  Pasta        := 'C:\Dermatek\DmkRaiseUpAgain\';
  Executavel   := 'DmkRaiseUpAgain.bat';
  Parametros   := '';
  //EstadoJanela := 0;
  //Retorno      := -1;
  MyProcesses.ExecutaAppAlvo(Handle, False, Pasta, Executavel,
    Parametros, 0(*EstadoJanela*), HProcesso, Retorno);
end;

procedure TDermatekRaiseUp.Executar();
////////////////////////////////////////////////////////////////////////////////
  function  ConnectAs(const lpszUsername, lpszPassword: string): Boolean;
  var
    hToken: THandle;
  begin
    Result := LogonUser(PChar(lpszUsername), nil, PChar(lpszPassword), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hToken);
    if Result then
      Result := ImpersonateLoggedOnUser(hToken)
    else
      RaiseLastOSError;
  end;
////////////////////////////////////////////////////////////////////////////////
  procedure LogaEExecuta();
  var
    Admin, Password: String;
  begin
    //
    Admin    := 'angeloni';
    Password := 'eletro26';
  (*
    Admin    := 'Teste';
    Password := '2002';
  *)
    try
     ConnectAs(Admin, Password);
     //do something here

     Exe2();


     //terminates the impersonation
     RevertToSelf;

    except
      on E: Exception do
       //MeLog.Lines.Add(E.ClassName + ': ' + E.Message);
       App_PF.SalvaEmArquivoLog(E.ClassName + ': ' + E.Message);
    end;
  end;
begin
  LogaEExecuta();
end;

function TDermatekRaiseUp.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TDermatekRaiseUp.ServiceAfterInstall(Sender: TService);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create(KEY_READ or KEY_WRITE);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\SYSTEM\CurrentControlSet\Services\' + Application.Name, False) then
    begin
      Reg.WriteString('Description', 'Servi�o Dermatek que mant�m execut�veis Dermatek em execu��o. Restarta execut�veis finalizados.');
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TDermatekRaiseUp.ServiceExecute(Sender: TService);
const
  SecBetweenRuns = 10;
var
  Ciclo, Count: Integer;
  //
  sCmd,
  Pasta, Executavel, Parametros: String;
  Retorno: Integer;
begin
  //Pasta        := 'C:\Executaveis\19.0\Afresh\';
  //Executavel   := 'Afresh.exe';
  Pasta        := 'C:\Dermatek\DmkRaiseUpAgain\';
  Executavel   := 'DmkRaiseUpAgain.bat';
  Parametros   := '';
  //EstadoJanela := 0;
  //Retorno      := -1;
  //
  Ciclo := 0;
  Count := 0;
  while not Terminated do
  begin
    Inc(Count);
    if Count >= SecBetweenRuns then
    begin
      Ciclo := Ciclo + 1;
      Count := 0;

      { place your service code here }
      { this is where the action happens }
////////////////////////////////////////////////////////////////////////////////
    //if not MyProcesses.KillProcess(MyProcesses.FindProcessesByName(Executavel)) then
      //ShowMessage('Processo n�o finalizado.')
////////////////////////////////////////////////////////////////////////////////
{
    if MyProcesses.FindProcessesByName(Executavel) = - 1 then
      MyProcesses.ExecutaAppAlvo(
      nil, //TForm;
      True, //ExecTargetAsAdmin: Boolean; const
      Pasta,
      Executavel,
      Parametros,
      EstadoJanela,
      HProcesso);
}
  //LogMessage('Your message goes here SUCC', EVENTLOG_SUCCESS, 0, 1);
  //LogMessage('Your message goes here INFO', EVENTLOG_INFORMATION_TYPE, 0, 2);
  //LogMessage('Your message goes here WARN', EVENTLOG_WARNING_TYPE, 0, 3);
  //LogMessage('Your message goes here ERRO', EVENTLOG_ERROR_TYPE, 0, 4);

  ////////////////////////////////////////////////////////////////////////////////
  ///




{
      if MyProcesses.ExecutaAppAlvo(
      Application.Handle, //TForm;
      False, //ExecTargetAsAdmin: Boolean; const
      Pasta,
      Executavel,
      Parametros,
      EstadoJanela,
      HProcesso, Retorno) then
        App_PF.SalvaEmArquivoLog('Ciclo: ' + IntToStr(Ciclo) + ' Processo: ' + IntToStr(HProcesso))
      else
        App_PF.SalvaEmArquivoLog('Ciclo: ' + IntToStr(Ciclo) + ' Erro: ' + IntToStr(Retorno));
}




{
      //ShellExecute(Application.Handle, nil, PChar(sCmd), nil, nil, SW_HIDE);
      ShellExecute(Application.handle, 'open', 'cmd', PChar(sCmd), nil, SW_HIDE);
}
     // sCmd := 'C:\Dermatek\DmkRaiseUpAgain\DmkRaiseUpAgain.bat';
      (*
      sCmd := 'C:\Executaveis\19.0\Afresh\Afresh.exe';
      Retorno := App_PF.RunAs('angeloni', 'eletro26', sCmd);
      App_PF.SalvaEmArquivoLog('Ciclo: ' + IntToStr(Ciclo) + ' Retorno: ' + IntToStr(Retorno))
      *)
      Executar();
    end;
    Sleep(1000);
    ServiceThread.ProcessRequests(False);
  end;
end;

procedure TDermatekRaiseUp.ServiceStart(Sender: TService; var Started: Boolean);
begin
{
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  try
    // a variavel esta sendo atualizada apos o fechamento do form
    FLogado := FmPrincipal.ShowModal = mrOk;
  finally
    FmPrincipal.Free;
  end;
}
end;

end.
