unit DmkRaiseAgain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs;

type
  TDermatekRaiseAppsAgain = class(TService)
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  DermatekRaiseAppsAgain: TDermatekRaiseAppsAgain;

implementation

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  DermatekRaiseAppsAgain.Controller(CtrlCode);
end;

function TDermatekRaiseAppsAgain.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

(*
C:\Executaveis\19.0\DermatekRaiseAgain\DermatekRaiseAgain.exe / install
*)

end.
