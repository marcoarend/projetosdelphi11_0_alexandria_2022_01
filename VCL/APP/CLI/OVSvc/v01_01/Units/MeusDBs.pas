unit MeusDBs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons;

type
  TFmMeusDBs = class(TForm)
    RGDBs: TRadioGroup;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure RGDBsClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMeusDBs: TFmMeusDBs;

implementation

{$R *.DFM}

procedure TFmMeusDBs.RGDBsClick(Sender: TObject);
begin
  BtOK.Enabled := True;
  Close;
end;

procedure TFmMeusDBs.BtOKClick(Sender: TObject);
begin
  Close;
end;

end.
