unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, ComCtrls, ExtCtrls, Spin, Menus, ShellApi,
  Registry, UnInternalConsts, MySQLTools, Db, mySQLDbTables, Grids, DBGrids,
  MySQLDump, MySQLBatch, dmkGeral, UnDmkSystem, SHFolder, dmkEdit, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars;

type
  TFmPrincipal = class(TForm)
    TrayIcon1: TTrayIcon;
    PopupMenu1: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    TmLoadCSV: TTimer;
    BtSaida: TBitBtn;
    BtCon: TBitBtn;
    TmOculta: TTimer;
    procedure TrayIcon1Click(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TmLoadCSVTimer(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure TmOcultaTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
    FHides: Integer;
    FShowed: Boolean;
    FHideFirst: Boolean;
    //
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
  public
    { Public declarations }
    function  RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    procedure ReCaptionComponentesDeForm(Form: TForm);
    procedure TentaCarregar();
  end;

var
  FmPrincipal: TFmPrincipal;

const
  //CO_TimerBkExec_Interval = 10000;
  //CO_TimerInicio_Interval = 60000;
  CO_Titulo = 'Rastreador OverSeer';

implementation

{$R *.dfm}

uses  UnMyObjects, UnAppJan, MyDBCheck, UnOVS_ProjGroupVars, UnProjGroup_Jan,
  Module, ModuleGeral, ImportaCSV_ERP_01;


procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.TentaCarregar();
begin
  TmLoadCSV.Enabled := False;
  if not Visible then
  begin
(*
    EdSeg.Text := FormatDateTime('  hh:nn:ss',Now - FStartTime);
    if QrProxBk.State <> dsInactive then
      QrProxBk.Refresh;
*)
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
      //
      if not Assigned(Dmod) then
      begin
        Application.CreateForm(TDmod, Dmod);
        DMod.TentaConectar();
        try
          Application.CreateForm(TDmodG, DmodG);
        except
          Geral.MB_Erro('Imposs�vel criar Modulo de dados Geral');
          Application.Terminate;
          Exit;
        end;
      end;
    end else
      DMod.TentaConectar();
    // desmarcar quando estiver em uma vers�o bem est�vel!
    //if DMod.PodeLerDir() then
    //DMod.PodeLerDir();// then
    begin
      if DBCheck.CriaFm(TFmImportaCSV_ERP_01, FmImportaCSV_ERP_01, afmoNegarComAviso) then
      begin
        //ImportaCSV_ERP_01.ShowModal;
        FmImportaCSV_ERP_01.CarregaDados();
        FmImportaCSV_ERP_01.Destroy;
      end;
     if ProjGroup_Jan.MostraFormOVgIspPrfCfg(True(*Automatico*), nil, nil) then
       (*ReopenNovasReduzOPs()*);
    end;
  end;
end;

procedure TFmPrincipal.TmLoadCSVTimer(Sender: TObject);
begin
  TentaCarregar();
end;

procedure TFmPrincipal.TmOcultaTimer(Sender: TObject);
begin
  TmOculta.Enabled := False;
  BtSaidaClick(Self);
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.BtConClick(Sender: TObject);
begin
  AppJan.MostraVerificaConexoes();
end;

procedure TFmPrincipal.BtSaidaClick(Sender: TObject);
begin
  TrayIcon1.Visible := True;
  Application.MainFormOnTaskBar := FShowed;
  FmPrincipal.Hide;
  //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
  Application.MainFormOnTaskBar := FShowed;
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  //if FValidaContrato then
  begin
    TrayIcon1.Visible := True;
    //
    if FHides = 0 then
    begin
     FHides          := FHides + 1;
     (* Ainda n�o foi criado
     Dmod.ReopenOpcoesApp();
     TmLoadCSV.Interval := Dmod.QrOpcoesAppLoadCSVIntrv.Value * 60 * 100;
     TmLoadCSV.Enabled  := True;
     *)
    end;
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  TMeuDB          := 'overseer';
  VAR_RUN_AS_SVC  := True;
  VAR_BDSENHA     := EmptyStr;
  FHides          := 0;
  FShowed         := True;
  //
  VAR_TYPE_LOG :=  TTypeLog.ttlCliIntUni;
  Geral.DefineFormatacoes;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  TmOculta.Enabled := False;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;

end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PopupMenu1Popup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadAppKeyCU(CO_Titulo, 'Microsoft\Windows\CurrentVersion\Run', ktString, '');
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
  Mostrar1.Enabled := not FmPrincipal.Visible;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
// N�o Usa
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  //Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
  Result := True;
end;

{
function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;
}

end.
