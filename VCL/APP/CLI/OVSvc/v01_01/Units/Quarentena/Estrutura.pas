unit Estrutura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, Grids, ComCtrls, DBCtrls,
  DBGrids, Menus, DmkDAC_PF, DmkGeral;

type
  TFmEstrutura = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtLimpa: TBitBtn;
    BtImprime: TBitBtn;
    BtVerifica: TBitBtn;
    BtBackup: TBitBtn;
    MyDB: TmySQLDatabase;
    QrMyFields: TmySQLQuery;
    QrMyTables: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    lbHost: TLabel;
    EdEspelho: TEdit;
    Panel4: TPanel;
    QrMyDBs: TmySQLQuery;
    QrMyDBsDatabase: TWideStringField;
    DsMyDBs: TDataSource;
    DsMyTables: TDataSource;
    QrMyDBs2: TmySQLQuery;
    StringField1: TWideStringField;
    QrMyTables2: TmySQLQuery;
    QrMyFields2: TmySQLQuery;
    DBx: TmySQLDatabase;
    OpenDialog1: TOpenDialog;
    BtLocaliza1: TBitBtn;
    BtCopiar: TBitBtn;
    QrUpd: TmySQLQuery;
    EdSenha: TEdit;
    Label1: TLabel;
    DsUpd: TDataSource;
    Panel7: TPanel;
    Panel12: TPanel;
    Splitter2: TSplitter;
    Splitter1: TSplitter;
    Panel6: TPanel;
    PainelEstrutura: TPanel;
    Grade1: TStringGrid;
    Panel8: TPanel;
    PainelPesquisa: TPanel;
    PainelEspelho: TPanel;
    PainelComparacao: TPanel;
    DBGrid1: TDBGrid;
    Panel13: TPanel;
    PainelTabela: TPanel;
    LaTb: TLabel;
    Label5: TLabel;
    PainelDatabase: TPanel;
    Label4: TLabel;
    DBText2: TDBText;
    Panel11: TPanel;
    MemoSQL: TMemo;
    Panel14: TPanel;
    Panel9: TPanel;
    PMVerifica: TPopupMenu;
    Atabelaatual1: TMenuItem;
    TodosDatabases1: TMenuItem;
    Splitter3: TSplitter;
    PMLimpa: TPopupMenu;
    Avisos1: TMenuItem;
    SQL1: TMenuItem;
    Comparaes1: TMenuItem;
    Combinadas1: TMenuItem;
    Splitter4: TSplitter;
    Erros: TMemo;
    Panel15: TPanel;
    Avisos: TMemo;
    Erros1: TMenuItem;
    Panel16: TPanel;
    Panel10: TPanel;
    Localhost1: TMenuItem;
    LocalhostEspelho1: TMenuItem;
    ODatabaseAtual1: TMenuItem;
    LocalHost2: TMenuItem;
    Localhost3: TMenuItem;
    Tudo1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtBackupClick(Sender: TObject);
    procedure BtVerificaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure QrMyDBsAfterScroll(DataSet: TDataSet);
    procedure QrMyTablesAfterScroll(DataSet: TDataSet);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure BtCopiarClick(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure MemoSQLKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSenhaChange(Sender: TObject);
    procedure EdSenhaEnter(Sender: TObject);
    procedure EdSenhaExit(Sender: TObject);
    procedure Atabelaatual1Click(Sender: TObject);
    procedure Avisos1Click(Sender: TObject);
    procedure SQL1Click(Sender: TObject);
    procedure Combinadas1Click(Sender: TObject);
    procedure QrMyTablesAfterClose(DataSet: TDataSet);
    procedure Erros1Click(Sender: TObject);
    procedure Grade1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Localhost1Click(Sender: TObject);
    procedure LocalhostEspelho1Click(Sender: TObject);
    procedure LocalHost2Click(Sender: TObject);
    procedure Localhost3Click(Sender: TObject);
    procedure Tudo1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraTabela;
    procedure VerificaTudo(Tipo: Integer);
    procedure LimpaLinha1;
    procedure VerificaDBAtual;
  public
    { Public declarations }
  end;

var
  FmEstrutura: TFmEstrutura;
  ArqOF, ArqOF2: TStringList;
  Conta: Integer;

implementation

uses WetBlueMLA_BK, UnInternalConsts, UnMLAGeral, UnMyObjects;

{$R *.DFM}


procedure TFmEstrutura.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEstrutura.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
  PainelPesquisa.Width :=
    Grade1.ColWidths[00] + Grade1.ColWidths[01] + Grade1.ColWidths[02] +
    Grade1.ColWidths[03] + Grade1.ColWidths[04] + Grade1.ColWidths[05] + 8;
  PainelEspelho.Width :=
    Grade1.ColWidths[06] + Grade1.ColWidths[07] + Grade1.ColWidths[08] +
    Grade1.ColWidths[09] + Grade1.ColWidths[10] + Grade1.ColWidths[11] + 6;
  PainelComparacao.Width :=
    Grade1.ColWidths[12] + Grade1.ColWidths[13] + Grade1.ColWidths[14] +
    Grade1.ColWidths[15] + Grade1.ColWidths[16] + Grade1.ColWidths[17] + 6;
end;

procedure TFmEstrutura.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEstrutura.BtBackupClick(Sender: TObject);
{
var
  i: Integer;
  Text: TextFile;
  D, T, F, Name: String;
}
begin
  (*if FmWetBlueMLA_BK.LBDatabase.Items.Count < 1 then
  begin
    ShowMessage('Lista de Bases de Dados vazia.');
    Exit;
  end;
  Name := ExtractFilePath(EdEspelho.Text)+'\Estrutura.txt';
  if FileExists(Name) then DeleteFile(Name);
  AssignFile(Text, Name);
  ReWrite(Text);
  try
    Avisos.Lines.Add('');
    Avisos.Lines.Add('---------  Cria��o de Arquivo Espelho -----------');
    Avisos.Lines.Add('');
    for i := 0 to FmWetBlueMLA_BK.LBDatabase.Items.Count -1 do
    begin
      MyDB.Connected := False;
      MyDB.DatabaseName := FmWetBlueMLA_BK.LBDatabase.Items[i];
      MyDB.Connected := True;
      D := FmWetBlueMLA_BK.LBDatabase.Items[i];
      QrMyTables2.Close;
      QrMyTables2.SQL.Clear;
      QrMyTables2.SQL.Add('SHOW TABLES FROM '+fmwetbluemla_bk.lbdatabase.items[i]);
      QrMyTables2.Open;
      if QrMyTables2.RecordCount = 0 then
      begin
        WriteLn(Text, '<D>'+D);
        WriteLn(Text, '');
        WriteLn(Text, '');
        WriteLn(Text, '');
        WriteLn(Text, '');
        WriteLn(Text, '');
      end;
      while not QrMyTables2.Eof do
      begin
        QrMyFields2.Close;
        QrMyFields2.SQL.Clear;
        QrMyFields2.SQL.Add('SHOW FIELDS FROM '+qrmytables2.fields[0].asstring);
        QrMyFields2.Open;
        T := QrMyTables2.Fields[0].AsString;
        if QrMyFields2.RecordCount = 0 then
        begin
          WriteLn(Text, '<T>'+D+'.'+T);
          WriteLn(Text, '');
          WriteLn(Text, '');
          WriteLn(Text, '');
          WriteLn(Text, '');
          WriteLn(Text, '');
        end;
        while not QrMyFields2.Eof do
        begin
          F := QrMyFields2.FieldByName('Field').AsString;
          Avisos.Lines.Add(D+'.'+T+'.'+F);
          WriteLn(Text, D+'.'+T+'.'+F);
          WriteLn(Text, 'T>'+QrMyFields2.FieldByName('Type').AsString);
          WriteLn(Text, 'N>'+QrMyFields2.FieldByName('Null').AsString);
          WriteLn(Text, 'K>'+QrMyFields2.FieldByName('Key').AsString);
          WriteLn(Text, 'D>'+QrMyFields2.FieldByName('Default').AsString);
          WriteLn(Text, 'E>'+QrMyFields2.FieldByName('Extra').AsString);
          QrMyFields2.Next;
        end;
        QrMyTables2.Next;
      end;
    end;
    Application.MessageBox('T�rmino da gera��o do espelho dos Databases.',
    'Gera��o de Espelho', MB_OK+MB_ICONINFORMATION);
  finally
    CloseFile(Text);
  end;*)
end;

procedure TFmEstrutura.BtVerificaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVerifica, BtVerifica);
end;

procedure TFmEstrutura.FormCreate(Sender: TObject);
begin
  //MyDB.UserPassword := FmWetBlueMLA_BK.FSenha;
  //DBx.UserPassword  := FmWetBlueMLA_BK.FSenha;
  //
  UnDmkDAC_PF.ConectaMyDB_DAC(DBx, 'mysql', FmWetBlueMLA_BK.edHost.ValueVariant,
    FmWetBlueMLA_BK.edPorta.ValueVariant, FmWetBlueMLA_BK.EdLogin.ValueVariant,
    FmWetBlueMLA_BK.FSenha, (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
  //
  Grade1.Cells[00,0] := 'Campo';
  Grade1.Cells[01,0] := 'Tipo';
  Grade1.Cells[02,0] := 'Nulo';
  Grade1.Cells[03,0] := 'Chave';
  Grade1.Cells[04,0] := 'Default';
  Grade1.Cells[05,0] := 'Extra';
  Grade1.Cells[06,0] := 'Campo';
  Grade1.Cells[07,0] := 'Tipo';
  Grade1.Cells[08,0] := 'Nulo';
  Grade1.Cells[09,0] := 'Chave';
  Grade1.Cells[10,0] := 'Default';
  Grade1.Cells[11,0] := 'Extra';
  Grade1.Cells[12,0] := 'Campo';
  Grade1.Cells[13,0] := 'Tipo';
  Grade1.Cells[14,0] := 'Nulo';
  Grade1.Cells[15,0] := 'Chave';
  Grade1.Cells[16,0] := 'Default';
  Grade1.Cells[17,0] := 'Extra';
  Grade1.Cells[18,0] := 'DB.Tabela';
  //
  Grade1.ColWidths[00] :=  70;
  Grade1.ColWidths[01] :=  70;
  Grade1.ColWidths[02] :=  32;
  Grade1.ColWidths[03] :=  32;
  Grade1.ColWidths[04] :=  40;
  Grade1.ColWidths[05] :=  32;
  Grade1.ColWidths[06] :=  70;
  Grade1.ColWidths[07] :=  70;
  Grade1.ColWidths[08] :=  32;
  Grade1.ColWidths[09] :=  32;
  Grade1.ColWidths[10] :=  40;
  Grade1.ColWidths[11] :=  30;
  Grade1.ColWidths[12] :=  30;
  Grade1.ColWidths[13] :=  30;
  Grade1.ColWidths[14] :=  30;
  Grade1.ColWidths[15] :=  30;
  Grade1.ColWidths[16] :=  30;
  Grade1.ColWidths[17] :=  30;
  Grade1.ColWidths[18] := 150;
  //
  QrMyDBs.Open;
end;

procedure TFmEstrutura.SpeedButton1Click(Sender: TObject);
begin
  QrMyDBs.Prior;
end;

procedure TFmEstrutura.SpeedButton4Click(Sender: TObject);
begin
  QrMyDBs.Next;
end;

procedure TFmEstrutura.SpeedButton2Click(Sender: TObject);
begin
  QrMyTables.Prior;
end;

procedure TFmEstrutura.SpeedButton3Click(Sender: TObject);
begin
  QrMyTables.Next;
end;

procedure TFmEstrutura.QrMyDBsAfterScroll(DataSet: TDataSet);
begin
  (*
  MyDB.Connected := False;
  MyDB.DatabaseName := QrMyDBs.Fields[0].AsString;
  MyDB.Connected := True;
  *)
  //
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, QrMyDBs.Fields[0].AsString,
    FmWetBlueMLA_BK.edHost.ValueVariant, FmWetBlueMLA_BK.edPorta.ValueVariant,
    FmWetBlueMLA_BK.EdLogin.ValueVariant, FmWetBlueMLA_BK.FSenha,
    (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
  //
  QrMyTables.Close;
  QrMyTables.SQL.Clear;
  QrMyTables.SQL.Add('SHOW TABLES FROM '+ String(qrmydbsdatabase.value));
  QrMyTables.Open;
end;

procedure TFmEstrutura.QrMyTablesAfterScroll(DataSet: TDataSet);
begin
  LaTb.Caption := QrMyTables.Fields[0].AsString;
  QrMyFields.Close;
  QrMyFields.SQL.Clear;
  QrMyFields.SQL.Add('SHOW FIELDS FROM '+qrmytables.fields[0].asstring);
  QrMyFields.Open;
end;

procedure TFmEstrutura.BtLocaliza1Click(Sender: TObject);
begin
 if OpenDialog1.Execute then EdEspelho.Text := OpenDialog1.FileName;
end;

procedure TFmEstrutura.BtCopiarClick(Sender: TObject);
var
  i: Integer;
  Update: Boolean;
  Texto: String;
begin
  for i := 0 to Grade1.RowCount -1 do
  begin
    Update := False;
    if Grade1.Cells[12,i] = 'X' then
    begin
      if Grade1.Cells[6,i] = '' then
      Erros.Lines.Add('O Campo "'+Grade1.Cells[0,i]+
      '" n�o existe no arquivo de estrutura.');
      // Parei aqui
    end else begin
      if (Grade1.Cells[13,i] = 'X') then Update := True;
      if (Grade1.Cells[14,i] = 'X') then Update := True;
      //if (Grade1.Cells[15,i] = 'X') then Update := True; // Parei aqui
      if (Grade1.Cells[16,i] = 'X') then Update := True;
      //if (Grade1.Cells[17,i] = 'X') then Update := True; // Parei aqui
    end;
    if Update then
    begin
      QrUpd.Close;
      QrUpd.SQL.Clear;
      Texto := 'ALTER TABLE '+LaTb.Caption+' CHANGE '+Grade1.Cells[0,i]+
      ' '+Grade1.Cells[6,i]+' '+Grade1.Cells[7,i]+' ';
      if Grade1.Cells[8,i] <> 'YES' then Texto := Texto + ' NOT NULL ';
      Texto := Texto + ' DEFAULT ';
      if Grade1.Cells[10,i] <> '' then Texto := Texto + '"'+Grade1.Cells[10,i]+'"'
      else begin
        if Grade1.Cells[8,i] <> 'YES' then Texto := Texto + 'NULL '
        else Texto := Texto + '0';
      end;
      QrUpd.SQL.Add(Texto);
      MemoSQL.Lines.Add(Texto);
      QrUpd.ExecSQL;
    end;
  end;
  BtVerificaClick(Self);
end;

procedure TFmEstrutura.BtLimpaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLimpa, BtLimpa);
end;

procedure TFmEstrutura.MemoSQLKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  i: Integer;
  Texto: String;
begin
  if Key=VK_F9 then
  begin
    if EdSenha.Text = VAR_BDSENHA then
    begin
      if Application.MessageBox(PChar('Confirma a execu��o desta SQL em ['+
      String(QrMyDBsDatabase.Value) +'.'+LaTb.Caption+']?'), 'Execu��o de SQL',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrUpd.Close;
        QrUpd.SQL.Clear;
        for i := 0 to MemoSQL.Lines.Count -1 do QrUpd.SQL.Add(MemoSQL.Lines[i]);
        for i := 0 to MemoSQL.Lines.Count -1 do Avisos.Lines.Add(MemoSQL.Lines[i]);
        Texto := '';
        i := 0;
        while Length(Texto) = 0 do
        begin
          Texto := Trim(MemoSQL.Lines[i]);
          if i = MemoSQL.Lines.Count then Break;
          i := i + 1;
        end;
        if Uppercase(Copy(Texto, 1,6)) = '' then Application.MessageBox(
          'N�o h� SQL a ser executada!', 'Erro', MB_OK+MB_ICONERROR)
        else if Uppercase(Copy(Texto, 1,6)) = 'INSERT'   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,7)) = 'REPLACE'  then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,6)) = 'UPDATE '   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,6)) = 'CREATE'   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,6)) = 'DELETE'   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,5)) = 'CHECK'    then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,5)) = 'ALTER'    then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,4)) = 'DROP'     then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,5)) = 'FLUSH'    then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,5)) = 'GRANT'    then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,6)) = 'REVOKE'   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,4)) = 'LOAD'     then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,4)) = 'LOCK'     then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,6)) = 'UNLOCK'   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,3)) = 'USE'      then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,8)) = 'OPTIMIZE' then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,6)) = 'REPAIR'   then QrUpd.ExecSQL
        else if Uppercase(Copy(Texto, 1,3)) = 'SET'      then QrUpd.ExecSQL
        ///
        else if Uppercase(Copy(Texto, 1,6)) = 'SELECT'   then QrUpd.Open
        else if Uppercase(Copy(Texto, 1,4)) = 'DESC'     then QrUpd.Open
        else if Uppercase(Copy(Texto, 1,7)) = 'EXPLAIN'  then QrUpd.Open
        else if Uppercase(Copy(Texto, 1,8)) = 'DESCRIBE' then QrUpd.Open
        else if Uppercase(Copy(Texto, 1,4)) = 'SHOW'     then QrUpd.Open
        else QrUpd.Open;
      end;
    end else
      Application.MessageBox('SQL n�o executada!','Aviso de Falta de senha',
      MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmEstrutura.EdSenhaChange(Sender: TObject);
begin
  EdSenha.Font.Color := EdSenha.Color;
end;

procedure TFmEstrutura.EdSenhaEnter(Sender: TObject);
begin
  EdSenha.Font.Color := EdSenha.Color;
end;

procedure TFmEstrutura.EdSenhaExit(Sender: TObject);
begin
  EdSenha.Font.Color := EdSenha.Color;
end;

procedure TFmEstrutura.MostraTabela;
var
  i, j, k: Integer;
  F: TextFile;
  S0, Tabela, TabX, RegX: string;
  ArqOF: TStringList;
  DB, Tb: Boolean;
begin
  if Trim(LaTb.Caption) = '' then
  begin
    Erros.Lines.Add('O Database: "'+String(QrMyDBsDatabase.Value)+'" n�o possui tabelas.');
    Exit;
  end;
  BtCopiar.Enabled := False;
  Grade1.RowCount := 2;
  LimpaLinha1;
  DB := False;
  Tb := False;
  Tabela := String(QrMyDBsDatabase.Value) +'.'+LaTb.Caption+'.';
  if FileExists(EdEspelho.Text) then
  begin
    ArqOF := TStringList.Create;
    AssignFile(F, EdEspelho.Text);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S0);
      ArqOF.Add(S0);
    end;
    CloseFile(F);
    i := 0;
    while i < ArqOF.Count do
    begin
      if Uppercase(Copy(ArqOF[i], 1, Length(QrMyDBsDatabase.Value))) =
      Uppercase(String(QrMyDBsDatabase.Value)) then DB := True;
      TabX := Copy(ArqOF[i], 1, Length(Tabela));
      if Uppercase(TabX) = Uppercase(Tabela) then
      Tb := True;
      i := i + 5;
    end;
  end else begin
    Erros.Lines.Add('O arquivo espelho: "'+
    EdEspelho.Text+'" n�o foi localizado.');
    Exit;
  end;
  if not DB then
    Erros.Lines.Add('O banco de dados ['+String(QrMyDBsDatabase.Value)+
    '] n�o existe no arquivo espelho: "'+
    EdEspelho.Text+'".');

  if (LaTb.Caption <> '') then
  begin
    if not Tb then
      Erros.Lines.Add('A tabela ['+Tabela+'] n�o existe no arquivo espelho: "'+
      EdEspelho.Text+'".');
    Conta := 0;
    QrMyFields.Close;
    QrMyFields.SQL.Clear;
    QrMyFields.SQL.Add('SHOW FIELDS FROM '+qrmytables.fields[0].asstring);
    QrMyFields.Open;
    QrMyFields.First;
    while not QrMyFields.Eof do
    begin
      Conta := Conta + 1;
      Grade1.RowCount := Conta + 1;
      Grade1.Cells[0,Conta] := QrMyFields.Fields[0].AsString;
      Grade1.Cells[1,Conta] := QrMyFields.Fields[1].AsString;
      Grade1.Cells[2,Conta] := QrMyFields.Fields[2].AsString;
      Grade1.Cells[3,Conta] := QrMyFields.Fields[3].AsString;
      Grade1.Cells[4,Conta] := QrMyFields.Fields[4].AsString;
      Grade1.Cells[5,Conta] := QrMyFields.Fields[5].AsString;
      QrMyFields.Next;
    end;
  end;
  i := 0;
  Conta := Grade1.RowCount;
  while i < ArqOF.Count do
  begin
    TabX := Copy(ArqOF[i], 1, Length(Tabela));
    RegX := Copy(ArqOF[i], Length(Tabela)+1, Length(ArqOF[i]) - (Length(Tabela)));
    if Uppercase(TabX) = Uppercase(Tabela) then
    begin
      k := -1;
      for j := 0 to Grade1.RowCount -1 do
        if Uppercase(Grade1.Cells[0,j]) = Uppercase(RegX) then k := j;
      if k = -1 then
      begin
        Conta := Conta + 1;
        k := Conta
      end;
      Grade1.RowCount := Conta;
      Grade1.Cells[06,k] := RegX;
      Grade1.Cells[07,k] := Copy(ArqOF[i+1], 3, Length(ArqOF[i+1])-2);
      Grade1.Cells[08,k] := Copy(ArqOF[i+2], 3, Length(ArqOF[i+2])-2);
      Grade1.Cells[09,k] := Copy(ArqOF[i+3], 3, Length(ArqOF[i+3])-2);
      Grade1.Cells[10,k] := Copy(ArqOF[i+4], 3, Length(ArqOF[i+4])-2);
      Grade1.Cells[11,k] := Copy(ArqOF[i+5], 3, Length(ArqOF[i+5])-2);
    end;
    i := i + 6;
  end;
  for i := 1 to Grade1.RowCount -1 do
  begin
    if Uppercase(Grade1.Cells[00,i]) <> Uppercase(Grade1.Cells[06,i]) then
       Grade1.Cells[12,i] := 'X' else Grade1.Cells[12,i] := '';
    if Uppercase(Grade1.Cells[01,i]) <> Uppercase(Grade1.Cells[07,i]) then
       Grade1.Cells[13,i] := 'X' else Grade1.Cells[13,i] := '';
    if Uppercase(Grade1.Cells[02,i]) <> Uppercase(Grade1.Cells[08,i]) then
       Grade1.Cells[14,i] := 'X' else Grade1.Cells[14,i] := '';
    if Uppercase(Grade1.Cells[03,i]) <> Uppercase(Grade1.Cells[09,i]) then
       Grade1.Cells[15,i] := 'X' else Grade1.Cells[15,i] := '';
    if Uppercase(Grade1.Cells[04,i]) <> Uppercase(Grade1.Cells[10,i]) then
       Grade1.Cells[16,i] := 'X' else Grade1.Cells[16,i] := '';
    if Uppercase(Grade1.Cells[05,i]) <> Uppercase(Grade1.Cells[11,i]) then
       Grade1.Cells[17,i] := 'X' else Grade1.Cells[17,i] := '';
  end;
  ArqOF.Free;
  BtCopiar.Enabled := True;
  Avisos.Lines.Add('');
  Avisos.Lines.Add('Verifica��o da tabela "'+LaTb.Caption+'" finalizada!');
  Avisos.Lines.Add('');
  Avisos.Lines.Add('-----------------------------');
end;

procedure TFmEstrutura.VerificaTudo(Tipo: Integer);
var
  i, k, l, x, y: Integer;
  F: TextFile;
  S0, TabX, RegX, DB_X, str: string;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  BtCopiar.Enabled := False;
  Avisos.Lines.Clear;
  Erros.Lines.Clear;
  Grade1.RowCount := 2;
  LimpaLinha1;
  if FileExists(EdEspelho.Text) then
  begin
    ArqOF  := TStringList.Create;
    ArqOF2 := TStringList.Create;
    AssignFile(F, EdEspelho.Text);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S0);
      ArqOF.Add(S0);
      ArqOF2.Add(S0);
    end;
    CloseFile(F);
  end else begin
    Erros.Lines.Add('O arquivo espelho: "'+
    EdEspelho.Text+'" n�o foi localizado.');
    Exit;
  end;
  ///////////////////////////////////////////
  Conta := 0;
  if Tipo > 1 then
  begin
    QrMyDBs.First;
    while not QrMyDBs.Eof do
    begin
      VerificaDBAtual;
      QrMyDBs.Next;
    end;
  end else VerificaDBAtual;
  i := 0;
  if Tipo in [1,3] then
  begin
    while i < ArqOF2.Count do
    begin
      if ArqOF2[i][1] <> '#' then
      begin
        x := Pos('.', ArqOF2[i]);
        str := ArqOF2[i];
        str[Pos('.', ArqOF2[i])] := ':';
        //ArqOF2[i] := str;
        y    := Pos('.', Str);
        DB_X := Copy(Str, 1, x-1);
        TabX := Copy(Str, x+1, y-x-1);
        RegX := Copy(Str, y+1, Length(Str)-y);
        if ((Uppercase(DB_X) = Uppercase(String(QrMyDBsDatabase.Value))) and (Tipo=1))
        or (Tipo = 3) then
        begin
          k := Conta;
          Conta := Conta + 1;
          Grade1.RowCount := Conta;
          Grade1.Cells[06,k] := RegX;
          Grade1.Cells[07,k] := Copy(ArqOF2[i+1], 3, Length(ArqOF2[i+1])-2);
          Grade1.Cells[08,k] := Copy(ArqOF2[i+2], 3, Length(ArqOF2[i+2])-2);
          Grade1.Cells[09,k] := Copy(ArqOF2[i+3], 3, Length(ArqOF2[i+3])-2);
          Grade1.Cells[10,k] := Copy(ArqOF2[i+4], 3, Length(ArqOF2[i+4])-2);
          Grade1.Cells[11,k] := Copy(ArqOF2[i+5], 3, Length(ArqOF2[i+5])-2);
          Grade1.Cells[18,k] := DB_X+'.'+TabX;
          if Avisos.Lines[Avisos.Lines.Count-1] <> (DB_X+'.'+TabX) then
            Avisos.Lines.Add(DB_X+'.'+TabX);
        end;
      end;
      i := i + 6;
    end;
  end;
  ArqOF.Free;
  ArqOF2.Free;
  for l := 1 to Grade1.RowCount -1 do
  begin
    if Uppercase(Grade1.Cells[00,l]) <> Uppercase(Grade1.Cells[06,l]) then
       Grade1.Cells[12,l] := 'X' else Grade1.Cells[12,l] := '';
    if Uppercase(Grade1.Cells[01,l]) <> Uppercase(Grade1.Cells[07,l]) then
       Grade1.Cells[13,l] := 'X' else Grade1.Cells[13,l] := '';
    if Uppercase(Grade1.Cells[02,l]) <> Uppercase(Grade1.Cells[08,l]) then
       Grade1.Cells[14,l] := 'X' else Grade1.Cells[14,l] := '';
    if Uppercase(Grade1.Cells[03,l]) <> Uppercase(Grade1.Cells[09,l]) then
       Grade1.Cells[15,l] := 'X' else Grade1.Cells[15,l] := '';
    if Uppercase(Grade1.Cells[04,l]) <> Uppercase(Grade1.Cells[10,l]) then
       Grade1.Cells[16,l] := 'X' else Grade1.Cells[16,l] := '';
    if Uppercase(Grade1.Cells[05,l]) <> Uppercase(Grade1.Cells[11,l]) then
       Grade1.Cells[17,l] := 'X' else Grade1.Cells[17,l] := '';
  end;
  BtCopiar.Enabled := True;
  Avisos.Lines.Add('');
  case Tipo of
    0: Avisos.Lines.Add('Compara��o do database "'+String(QrMyDBsDatabase.Value)+'" finalizada!');
    1: Avisos.Lines.Add('An�lise do database "'+String(QrMyDBsDatabase.Value)+'" finalizada!');
    2: Avisos.Lines.Add('Compara��o do todos databases finalizada!');
    3: Avisos.Lines.Add('An�lise do todos databases finalizada!');
  end;
  Avisos.Lines.Add('');
  Avisos.Lines.Add('-----------------------------');
end;

procedure TFmEstrutura.Atabelaatual1Click(Sender: TObject);
begin
  MostraTabela;
end;

procedure TFmEstrutura.Avisos1Click(Sender: TObject);
begin
  Avisos.Clear;
end;

procedure TFmEstrutura.SQL1Click(Sender: TObject);
begin
  MemoSQL.Clear;
end;

procedure TFmEstrutura.Combinadas1Click(Sender: TObject);
var
  i, j, k: Integer;
  Txt: String;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  j := 1;
  for  i := 1 to Grade1.RowCount-1 do
  begin
    Txt := Grade1.Cells[12,i] + Grade1.Cells[13,i] + Grade1.Cells[14,i] +
    Grade1.Cells[15,i] + Grade1.Cells[16,i] + Grade1.Cells[17,i];
    if Txt <> '' then
    begin
      if  i <> j then
      begin
        Grade1.cells[00,j] := Grade1.cells[00,i];
        Grade1.cells[01,j] := Grade1.cells[01,i];
        Grade1.cells[02,j] := Grade1.cells[02,i];
        Grade1.cells[03,j] := Grade1.cells[03,i];
        Grade1.cells[04,j] := Grade1.cells[04,i];
        Grade1.cells[05,j] := Grade1.cells[05,i];
        Grade1.cells[06,j] := Grade1.cells[06,i];
        Grade1.cells[07,j] := Grade1.cells[07,i];
        Grade1.cells[08,j] := Grade1.cells[08,i];
        Grade1.cells[09,j] := Grade1.cells[09,i];
        Grade1.cells[10,j] := Grade1.cells[10,i];
        Grade1.cells[11,j] := Grade1.cells[11,i];
        Grade1.cells[12,j] := Grade1.cells[12,i];
        Grade1.cells[13,j] := Grade1.cells[13,i];
        Grade1.cells[14,j] := Grade1.cells[14,i];
        Grade1.cells[15,j] := Grade1.cells[15,i];
        Grade1.cells[16,j] := Grade1.cells[16,i];
        Grade1.cells[17,j] := Grade1.cells[17,i];
        Grade1.cells[18,j] := Grade1.cells[18,i];
      end;
      j := j + 1;
    end;
  end;
  if j = 1 then
  begin
    LimpaLinha1;
    j := 2;
    k := 0;
  end else k := j - 1;
  Grade1.RowCount := j;
  Avisos.Lines.Add(IntToStr(k)+' registros restantes.');
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmEstrutura.LimpaLinha1;
begin
  Grade1.Cells[00,1] := '';
  Grade1.Cells[01,1] := '';
  Grade1.Cells[02,1] := '';
  Grade1.Cells[03,1] := '';
  Grade1.Cells[04,1] := '';
  Grade1.Cells[05,1] := '';
  Grade1.Cells[06,1] := '';
  Grade1.Cells[07,1] := '';
  Grade1.Cells[08,1] := '';
  Grade1.Cells[09,1] := '';
  Grade1.Cells[10,1] := '';
  Grade1.Cells[11,1] := '';
  Grade1.Cells[12,1] := '';
  Grade1.Cells[13,1] := '';
  Grade1.Cells[14,1] := '';
  Grade1.Cells[15,1] := '';
  Grade1.Cells[16,1] := '';
  Grade1.Cells[17,1] := '';
  Grade1.Cells[18,1] := '';
end;

procedure TFmEstrutura.QrMyTablesAfterClose(DataSet: TDataSet);
begin
  LaTb.Caption := '';
end;

procedure TFmEstrutura.Erros1Click(Sender: TObject);
begin
  Erros.Clear;
end;

procedure TFmEstrutura.Grade1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  PainelPesquisa.Width :=
    Grade1.ColWidths[00] + Grade1.ColWidths[01] + Grade1.ColWidths[02] +
    Grade1.ColWidths[03] + Grade1.ColWidths[04] + Grade1.ColWidths[05] + 8;
  PainelEspelho.Width :=
    Grade1.ColWidths[06] + Grade1.ColWidths[07] + Grade1.ColWidths[08] +
    Grade1.ColWidths[09] + Grade1.ColWidths[10] + Grade1.ColWidths[11] + 6;
end;

procedure TFmEstrutura.Localhost1Click(Sender: TObject);
begin
  VerificaTudo(2);
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmEstrutura.LocalhostEspelho1Click(Sender: TObject);
begin
  VerificaTudo(3);
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmEstrutura.LocalHost2Click(Sender: TObject);
begin
  VerificaTudo(0);
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmEstrutura.Localhost3Click(Sender: TObject);
begin
  VerificaTudo(1);
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmEstrutura.VerificaDBAtual;
var
  DB, Tb: Boolean;
  i, j, k: Integer;
  Tabela, TabY, TabX, RegX: string;
begin
  DB := False;
  Tb := False;
  i := 0;
  while i < ArqOF.Count do
  begin
    if Uppercase(Copy(ArqOF[i], 1, Length(QrMyDBsDatabase.Value))) =
      Uppercase(String(QrMyDBsDatabase.Value)) then DB := True;
      i := i + 5;
  end;
  if not DB then
    Erros.Lines.Add('O banco de dados ['+String(QrMyDBsDatabase.Value)+
    '] n�o existe no arquivo espelho: "'+EdEspelho.Text+'".')
  else
  begin
    QrMyTables.First;
    while not QrMyTables.Eof do
    begin
      TabY := String(QrMyDBsDatabase.Value)+'.'+LaTb.Caption;
      Avisos.Lines.Add(TabY);
      Tabela := TabY + '.';
      i := 0;
      while i < ArqOF.Count do
      begin
        if Uppercase(Copy(ArqOF[i], 1, Length(Tabela))) =
        Uppercase(Tabela) then Tb := True;
        i := i + 5;
      end;
      if not Tb then
        Erros.Lines.Add('A tabela ['+Tabela+'] n�o existe no arquivo espelho: "'+
        EdEspelho.Text+'".');
      QrMyFields.Close;
      QrMyFields.SQL.Clear;
      QrMyFields.SQL.Add('SHOW FIELDS FROM '+qrmytables.fields[0].asstring);
      QrMyFields.Open;
      QrMyFields.First;
      while not QrMyFields.Eof do
      begin
        Conta := Conta + 1;
        Grade1.RowCount := Conta + 1;
        Grade1.Cells[00,Conta] := QrMyFields.Fields[0].AsString;
        Grade1.Cells[01,Conta] := QrMyFields.Fields[1].AsString;
        Grade1.Cells[02,Conta] := QrMyFields.Fields[2].AsString;
        Grade1.Cells[03,Conta] := QrMyFields.Fields[3].AsString;
        Grade1.Cells[04,Conta] := QrMyFields.Fields[4].AsString;
        Grade1.Cells[05,Conta] := QrMyFields.Fields[5].AsString;
        Grade1.Cells[18,Conta] := TabY;
        QrMyFields.Next;
      end;
      i := 0;
      Conta := Grade1.RowCount;
      while i < ArqOF.Count do
      begin
        TabX := Copy(ArqOF[i], 1, Length(Tabela));
        RegX := Copy(ArqOF[i], Length(Tabela)+1, Length(ArqOF[i]) - (Length(Tabela)));
        if Uppercase(TabX) = Uppercase(Tabela) then
        begin
          k := -1;
          for j := 0 to Grade1.RowCount -1 do
            if Uppercase(Grade1.Cells[0,j]) = Uppercase(RegX) then k := j;
          if k = -1 then
          begin
            k := Conta;
            Conta := Conta + 1;
          end;
          Grade1.RowCount := Conta;
          Grade1.Cells[06,k] := RegX;
          Grade1.Cells[07,k] := Copy(ArqOF[i+1], 3, Length(ArqOF[i+1])-2);
          Grade1.Cells[08,k] := Copy(ArqOF[i+2], 3, Length(ArqOF[i+2])-2);
          Grade1.Cells[09,k] := Copy(ArqOF[i+3], 3, Length(ArqOF[i+3])-2);
          Grade1.Cells[10,k] := Copy(ArqOF[i+4], 3, Length(ArqOF[i+4])-2);
          Grade1.Cells[11,k] := Copy(ArqOF[i+5], 3, Length(ArqOF[i+5])-2);
          Grade1.Cells[18,k] := TabY;
          ArqOF2[i] := '#'+ArqOF2[i];
        end;
        i := i + 6;
      end;
      QrMyTables.Next;
    end;
  end;
end;

procedure TFmEstrutura.Tudo1Click(Sender: TObject);
begin
  Grade1.RowCount := 2;
  LimpaLinha1;
end;

end.

