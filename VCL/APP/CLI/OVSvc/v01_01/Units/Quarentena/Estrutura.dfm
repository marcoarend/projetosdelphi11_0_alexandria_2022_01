object FmEstrutura: TFmEstrutura
  Left = 205
  Top = 153
  Caption = 'Verifica'#231#227'o da Estrutura de Banco de Dados'
  ClientHeight = 514
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 766
    Height = 48
    Align = alTop
    Caption = 'Verifica'#231#227'o da Estrutura de Banco de Dados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 764
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 770
      ExplicitHeight = 44
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 466
    Width = 766
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaRegistro: TLabel
      Left = 173
      Top = 1
      Width = 31
      Height = 46
      Align = alClient
      Caption = '[N]: 0'
      ExplicitWidth = 26
      ExplicitHeight = 13
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = #218'ltimo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 204
      Top = 1
      Width = 561
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 464
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtLimpa: TBitBtn
        Tag = 169
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Limpar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtLimpaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Imprimir'
        Caption = '&Imprimir'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object BtVerifica: TBitBtn
        Tag = 10001
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Verificar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtVerificaClick
      end
      object BtBackup: TBitBtn
        Tag = 269
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Exportar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtBackupClick
      end
      object BtCopiar: TBitBtn
        Tag = 10064
        Left = 279
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Atualizar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtCopiarClick
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 766
    Height = 418
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Verifica'#231#227'o'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 758
        Height = 390
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 758
          Height = 56
          Align = alTop
          TabOrder = 0
          object lbHost: TLabel
            Left = 8
            Top = 8
            Width = 41
            Height = 13
            Caption = 'Espelho:'
          end
          object Label1: TLabel
            Left = 560
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Senha:'
          end
          object EdEspelho: TEdit
            Left = 8
            Top = 25
            Width = 549
            Height = 21
            TabOrder = 0
            Text = 'C:\Dermatek\Estrutura.txt'
          end
          object BtLocaliza1: TBitBtn
            Tag = 10128
            Left = 664
            Top = 9
            Width = 90
            Height = 40
            Caption = '&Espelho'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtLocaliza1Click
          end
          object EdSenha: TEdit
            Left = 560
            Top = 24
            Width = 89
            Height = 21
            PasswordChar = '*'
            TabOrder = 2
            OnChange = EdSenhaChange
            OnEnter = EdSenhaEnter
            OnExit = EdSenhaExit
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 56
          Width = 758
          Height = 334
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 758
            Height = 334
            Align = alClient
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 1
              Top = 210
              Width = 756
              Height = 3
              Cursor = crVSplit
              Align = alBottom
              ExplicitTop = 212
              ExplicitWidth = 764
            end
            object Panel12: TPanel
              Left = 1
              Top = 213
              Width = 756
              Height = 120
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              object Splitter2: TSplitter
                Left = 0
                Top = 38
                Width = 756
                Height = 3
                Cursor = crVSplit
                Align = alTop
                ExplicitWidth = 764
              end
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 756
                Height = 38
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel11'
                TabOrder = 0
                object MemoSQL: TMemo
                  Left = 27
                  Top = 0
                  Width = 729
                  Height = 38
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Lucida Console'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  OnKeyDown = MemoSQLKeyDown
                end
                object Panel9: TPanel
                  Left = 0
                  Top = 0
                  Width = 27
                  Height = 38
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'SQL'
                  TabOrder = 1
                end
              end
              object Panel14: TPanel
                Left = 0
                Top = 41
                Width = 756
                Height = 79
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel14'
                TabOrder = 1
                object Splitter4: TSplitter
                  Left = 247
                  Top = 0
                  Height = 79
                end
                object Erros: TMemo
                  Left = 277
                  Top = 0
                  Width = 479
                  Height = 79
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Lucida Console'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                end
                object Panel15: TPanel
                  Left = 0
                  Top = 0
                  Width = 27
                  Height = 79
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'OBS'
                  TabOrder = 1
                end
                object Avisos: TMemo
                  Left = 27
                  Top = 0
                  Width = 220
                  Height = 79
                  Align = alLeft
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Lucida Console'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                end
                object Panel16: TPanel
                  Left = 250
                  Top = 0
                  Width = 27
                  Height = 79
                  Align = alLeft
                  BevelOuter = bvNone
                  Caption = 'ERR'
                  TabOrder = 3
                end
              end
            end
            object Panel6: TPanel
              Left = 1
              Top = 1
              Width = 756
              Height = 209
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object PainelEstrutura: TPanel
                Left = 0
                Top = 18
                Width = 756
                Height = 191
                Align = alClient
                TabOrder = 0
                object Splitter3: TSplitter
                  Left = 1
                  Top = 117
                  Width = 754
                  Height = 3
                  Cursor = crVSplit
                  Align = alTop
                  ExplicitWidth = 762
                end
                object Grade1: TStringGrid
                  Left = 1
                  Top = 19
                  Width = 754
                  Height = 98
                  Align = alTop
                  ColCount = 19
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing, goRowSelect]
                  TabOrder = 0
                  OnDrawCell = Grade1DrawCell
                end
                object Panel8: TPanel
                  Left = 1
                  Top = 1
                  Width = 754
                  Height = 18
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object PainelPesquisa: TPanel
                    Left = 0
                    Top = 0
                    Width = 283
                    Height = 18
                    Align = alLeft
                    Caption = 'Tabela pesquisada'
                    TabOrder = 0
                  end
                  object PainelEspelho: TPanel
                    Left = 283
                    Top = 0
                    Width = 282
                    Height = 18
                    Align = alLeft
                    Caption = 'Tabela espelho'
                    TabOrder = 1
                  end
                  object PainelComparacao: TPanel
                    Left = 565
                    Top = 0
                    Width = 123
                    Height = 18
                    Align = alLeft
                    Caption = 'Compara'#231#227'o'
                    TabOrder = 2
                  end
                  object Panel10: TPanel
                    Left = 688
                    Top = 0
                    Width = 66
                    Height = 18
                    Align = alClient
                    Caption = 'Observa'#231#245'es'
                    TabOrder = 3
                  end
                end
                object DBGrid1: TDBGrid
                  Left = 1
                  Top = 120
                  Width = 754
                  Height = 70
                  Align = alClient
                  DataSource = DsUpd
                  TabOrder = 2
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                end
              end
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 756
                Height = 18
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object PainelTabela: TPanel
                  Left = 281
                  Top = 0
                  Width = 475
                  Height = 18
                  Align = alClient
                  TabOrder = 0
                  object LaTb: TLabel
                    Left = 43
                    Top = 1
                    Width = 431
                    Height = 16
                    Align = alClient
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    ExplicitWidth = 5
                    ExplicitHeight = 13
                  end
                  object Label5: TLabel
                    Left = 1
                    Top = 1
                    Width = 42
                    Height = 16
                    Align = alLeft
                    Caption = 'Tabela:  '
                    ExplicitHeight = 13
                  end
                end
                object PainelDatabase: TPanel
                  Left = 0
                  Top = 0
                  Width = 281
                  Height = 18
                  Align = alLeft
                  TabOrder = 1
                  object Label4: TLabel
                    Left = 1
                    Top = 1
                    Width = 55
                    Height = 16
                    Align = alLeft
                    Caption = 'Database:  '
                    ExplicitHeight = 13
                  end
                  object DBText2: TDBText
                    Left = 56
                    Top = 1
                    Width = 224
                    Height = 16
                    Align = alClient
                    DataField = 'Database'
                    DataSource = DsMyDBs
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object MyDB: TmySQLDatabase
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 36
    Top = 252
  end
  object QrMyFields: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW FIELDS FROM entidades')
    Left = 120
    Top = 224
  end
  object QrMyTables: TmySQLQuery
    Database = MyDB
    AfterClose = QrMyTablesAfterClose
    AfterScroll = QrMyTablesAfterScroll
    SQL.Strings = (
      'SHOW TABLES FROM entidades')
    Left = 92
    Top = 224
  end
  object QrMyDBs: TmySQLQuery
    Database = DBx
    AfterScroll = QrMyDBsAfterScroll
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 64
    Top = 225
    object QrMyDBsDatabase: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Database'
      Required = True
      Size = 64
    end
  end
  object DsMyDBs: TDataSource
    DataSet = QrMyDBs
    Left = 64
    Top = 252
  end
  object DsMyTables: TDataSource
    DataSet = QrMyTables
    Left = 92
    Top = 252
  end
  object QrMyDBs2: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW DATABASES')
    Left = 385
    Top = 61
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Database'
      Required = True
      Size = 64
    end
  end
  object QrMyTables2: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW TABLES FROM entidades')
    Left = 412
    Top = 61
  end
  object QrMyFields2: TmySQLQuery
    Database = MyDB
    Left = 440
    Top = 60
  end
  object DBx: TmySQLDatabase
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 36
    Top = 224
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'txt'
    FileName = 'Espelho.txt'
    Left = 632
    Top = 201
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW FIELDS FROM entidades')
    Left = 120
    Top = 252
  end
  object DsUpd: TDataSource
    DataSet = QrUpd
    Left = 148
    Top = 254
  end
  object PMVerifica: TPopupMenu
    Left = 268
    Top = 441
    object Atabelaatual1: TMenuItem
      Caption = 'A &Tabela atual'
      OnClick = Atabelaatual1Click
    end
    object ODatabaseAtual1: TMenuItem
      Caption = 'Database &Atual'
      object LocalHost2: TMenuItem
        Caption = '&Comparar'
        OnClick = LocalHost2Click
      end
      object Localhost3: TMenuItem
        Caption = '&Analisar'
        OnClick = Localhost3Click
      end
    end
    object TodosDatabases1: TMenuItem
      Caption = '&Todos Databases'
      object Localhost1: TMenuItem
        Caption = '&Comparar'
        OnClick = Localhost1Click
      end
      object LocalhostEspelho1: TMenuItem
        Caption = '&Analisar'
        OnClick = LocalhostEspelho1Click
      end
    end
  end
  object PMLimpa: TPopupMenu
    Left = 392
    Top = 440
    object Avisos1: TMenuItem
      Caption = '&Avisos'
      OnClick = Avisos1Click
    end
    object Erros1: TMenuItem
      Caption = '&Erros'
      OnClick = Erros1Click
    end
    object SQL1: TMenuItem
      Caption = '&SQL'
      OnClick = SQL1Click
    end
    object Comparaes1: TMenuItem
      Caption = '&Compara'#231#245'es'
      object Combinadas1: TMenuItem
        Caption = '&Combinadas'
        OnClick = Combinadas1Click
      end
      object Tudo1: TMenuItem
        Caption = '&Tudo'
        OnClick = Tudo1Click
      end
    end
  end
end
