object FProfil: TFProfil
  Left = 263
  Top = 134
  BorderStyle = bsDialog
  Caption = 'Insere / Altera acesso'
  ClientHeight = 331
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 372
    Height = 63
    Align = alTop
    TabOrder = 0
    object Label7: TLabel
      Left = 103
      Top = 12
      Width = 43
      Height = 13
      Caption = 'Acessos:'
    end
    object CBHosts: TComboBox
      Left = 102
      Top = 27
      Width = 171
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      OnChange = CBHostsChange
    end
    object BtOK: TBitBtn
      Left = 277
      Top = 12
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus�o / altera��o'
      Caption = 'Sal&va'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtOKClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF5555555555555A655555
        55555555588FF55555555555AAA65555555555558888F55555555555AAA65555
        555555558888FF555555555AAAAA65555555555888888F55555555AAAAAA6555
        5555558888888FF5555552AA65AAA65555555888858888F555552A65555AA655
        55558885555888FF55555555555AAA65555555555558888F555555555555AA65
        555555555555888FF555555555555AA65555555555555888FF555555555555AA
        65555555555555888FF555555555555AA65555555555555888FF555555555555
        5AA6555555555555588855555555555555555555555555555555}
      NumGlyphs = 2
    end
    object BtSair: TBitBtn
      Left = 9
      Top = 11
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa�da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSairClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
        333333333333337FF3333333333333903333333333333377FF33333333333399
        03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
        99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
        99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
        03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
        33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
        33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
        3333777777333333333333333333333333333333333333333333}
      NumGlyphs = 2
    end
  end
  object TS: TTabControl
    Left = 0
    Top = 63
    Width = 372
    Height = 268
    Align = alClient
    TabOrder = 1
    Tabs.Strings = (
      '&Altera acesso'
      '&Inclui acesso')
    TabIndex = 0
    object Label1: TLabel
      Left = 52
      Top = 76
      Width = 68
      Height = 13
      Caption = 'IP hospedeiro:'
    end
    object Label2: TLabel
      Left = 52
      Top = 117
      Width = 74
      Height = 13
      Caption = 'Base de dados:'
    end
    object Label3: TLabel
      Left = 52
      Top = 155
      Width = 64
      Height = 13
      Caption = 'Login - nome:'
    end
    object Label4: TLabel
      Left = 52
      Top = 195
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label5: TLabel
      Left = 51
      Top = 33
      Width = 100
      Height = 13
      Caption = 'Desci��o do acesso:'
    end
    object EdDB: TEdit
      Left = 51
      Top = 131
      Width = 250
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object EdUser: TEdit
      Left = 51
      Top = 171
      Width = 250
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object EdPwd: TEdit
      Left = 51
      Top = 211
      Width = 250
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 2
    end
    object EdProfil: TEdit
      Left = 52
      Top = 49
      Width = 250
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      Text = '??'
    end
    object EdHost: TEdit
      Left = 52
      Top = 92
      Width = 121
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 15
      ParentFont = False
      TabOrder = 4
      Text = '111'
    end
  end
end
