object DMod: TDMod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 480
  Width = 696
  object QrPerfis: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 300
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrAux: TMySQLQuery
    Database = ZZDB
    Left = 264
    Top = 292
  end
  object QrUpd: TMySQLQuery
    Database = ZZDB
    Left = 184
    Top = 284
  end
  object MyDB: TMySQLDatabase
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 20
    Top = 12
  end
  object QrMaster: TMySQLQuery
    Database = MyDB
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha'
      'FROM entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Size = 15
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
  end
  object QrUpdZ: TMySQLQuery
    Database = MyDB
    Left = 448
    Top = 7
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyDB
    Delimiter = ';'
    Left = 8
    Top = 56
  end
  object MyLocDatabase: TMySQLDatabase
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 156
    Top = 7
  end
  object QrAuxL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 51
  end
  object QrUpdL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 95
  end
  object QrInsL: TMySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 143
  end
  object DBAgendaAtu: TMySQLDatabase
    DatabaseName = 'dermatek'
    Host = 'localhost'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=dermatek'
      'Host=localhost')
    DatasetOptions = []
    Left = 592
    Top = 148
  end
  object QrOVSSvcOpt: TMySQLQuery
    Database = DBAgendaAtu
    Left = 276
    Top = 160
    object QrOVSSvcOptDtHrLastLog: TDateTimeField
      FieldName = 'DtHrLastLog'
    end
    object QrOVSSvcOptOVS_Svc_RndmStr: TWideStringField
      FieldName = 'OVS_Svc_RndmStr'
      Size = 32
    end
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 40
    Top = 227
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object ZZDB: TMySQLDatabase
    ConnectOptions = []
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    DatasetOptions = []
    Left = 236
    Top = 16
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 68
    Top = 344
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 100
    Top = 344
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    Left = 96
    Top = 388
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 68
    Top = 388
  end
  object QrNTV: TMySQLQuery
    Database = ZZDB
    Left = 176
    Top = 344
  end
  object QrNTI: TMySQLQuery
    Database = ZZDB
    Left = 176
    Top = 392
  end
  object QrUpdU: TMySQLQuery
    Database = ZZDB
    Left = 248
    Top = 344
  end
  object QrTerminal: TMySQLQuery
    Database = ZZDB
    Left = 240
    Top = 388
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = ZZDB
    Left = 488
    Top = 364
  end
  object QrOPcoesGrl: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctrlgeral'
      'WHERE Codigo=1')
    Left = 384
    Top = 376
    object QrOPcoesGrlERPNameByCli: TWideStringField
      FieldName = 'ERPNameByCli'
      Size = 60
    end
  end
  object QrOpcoesApp: TMySQLQuery
    Database = ZZDB
    AfterOpen = QrOpcoesAppAfterOpen
    SQL.Strings = (
      'SELECT * FROM opcoesapp'
      'WHERE Codigo=1')
    Left = 384
    Top = 328
    object QrOpcoesAppLoadCSVOthIP: TWideStringField
      FieldName = 'LoadCSVOthIP'
      Size = 255
    end
    object QrOpcoesAppLoadCSVOthDir: TWideStringField
      FieldName = 'LoadCSVOthDir'
      Size = 255
    end
    object QrOpcoesAppOVpLayEsq: TIntegerField
      FieldName = 'OVpLayEsq'
    end
    object QrOpcoesAppLoadCSVIntExt: TSmallintField
      FieldName = 'LoadCSVIntExt'
      Required = True
    end
    object QrOpcoesAppLoadCSVSoLote: TSmallintField
      FieldName = 'LoadCSVSoLote'
      Required = True
    end
    object QrOpcoesAppLoadCSVTipOP: TWideStringField
      FieldName = 'LoadCSVTipOP'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVSitOP: TWideStringField
      FieldName = 'LoadCSVSitOP'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVEmpresas: TWideStringField
      FieldName = 'LoadCSVEmpresas'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVTipLoclz: TWideStringField
      FieldName = 'LoadCSVTipLoclz'
      Size = 255
    end
    object QrOpcoesAppLoadCSVTipProdOP: TWideStringField
      FieldName = 'LoadCSVTipProdOP'
      Size = 255
    end
    object QrOpcoesAppEntiTipCto_Inspecao: TIntegerField
      FieldName = 'EntiTipCto_Inspecao'
    end
    object QrOpcoesAppLoadCSVIntrv: TIntegerField
      FieldName = 'LoadCSVIntrv'
    end
    object QrOpcoesAppMailResInspResul: TWideStringField
      FieldName = 'MailResInspResul'
      Size = 255
    end
    object QrOpcoesAppForcaReabCfgInsp: TSmallintField
      FieldName = 'ForcaReabCfgInsp'
    end
    object QrOpcoesAppLoadCSVIntEx2: TSmallintField
      FieldName = 'LoadCSVIntEx2'
      Required = True
    end
    object QrOpcoesAppLoadCSVSoLot2: TSmallintField
      FieldName = 'LoadCSVSoLot2'
      Required = True
    end
    object QrOpcoesAppLoadCSVEmpresa2: TWideStringField
      FieldName = 'LoadCSVEmpresa2'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVTipO2: TWideStringField
      FieldName = 'LoadCSVTipO2'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVSitO2: TWideStringField
      FieldName = 'LoadCSVSitO2'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVTipLocl2: TWideStringField
      FieldName = 'LoadCSVTipLocl2'
      Required = True
      Size = 255
    end
    object QrOpcoesAppLoadCSVTipProdO2: TWideStringField
      FieldName = 'LoadCSVTipProdO2'
      Size = 255
    end
    object QrOpcoesAppMailResInspResu2: TWideStringField
      FieldName = 'MailResInspResu2'
      Size = 255
    end
    object QrOpcoesAppLoadCSVCodNomLocal: TIntegerField
      FieldName = 'LoadCSVCodNomLocal'
    end
    object QrOpcoesAppLoadCSVCodValr1: TWideStringField
      FieldName = 'LoadCSVCodValr1'
      Size = 255
    end
    object QrOpcoesAppLoadCSVCodValr2: TWideStringField
      FieldName = 'LoadCSVCodValr2'
      Size = 255
    end
    object QrOpcoesAppCdScConfeccao: TIntegerField
      FieldName = 'CdScConfeccao'
    end
    object QrOpcoesAppCdScTecelagem: TIntegerField
      FieldName = 'CdScTecelagem'
    end
    object QrOpcoesAppCdScTinturaria: TIntegerField
      FieldName = 'CdScTinturaria'
    end
  end
end
