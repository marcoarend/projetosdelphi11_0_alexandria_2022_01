object FmHorarios: TFmHorarios
  Left = 419
  Top = 217
  Caption = 'Hor'#225'rio de Backup'
  ClientHeight = 180
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 464
    Height = 84
    Align = alClient
    TabOrder = 0
    object lbDb: TLabel
      Left = 22
      Top = 12
      Width = 37
      Height = 13
      Caption = 'Hor'#225'rio:'
    end
    object Label1: TLabel
      Left = 106
      Top = 12
      Width = 13
      Height = 13
      Caption = 'IP:'
    end
    object EdHorario: TdmkEdit
      Left = 22
      Top = 28
      Width = 79
      Height = 21
      Alignment = taCenter
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdHorarioExit
    end
    object EdIP: TdmkEdit
      Left = 106
      Top = 28
      Width = 79
      Height = 21
      Alignment = taCenter
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 132
    Width = 464
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
    end
    object BtSaida: TBitBtn
      Left = 362
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 464
    Height = 48
    Align = alTop
    Caption = 'Hor'#225'rio de Backup'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 462
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 468
      ExplicitHeight = 44
    end
  end
end
