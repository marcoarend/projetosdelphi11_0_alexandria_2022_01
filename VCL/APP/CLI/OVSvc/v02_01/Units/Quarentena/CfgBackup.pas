unit CfgBackup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkCheckBox;

type
  TFmCfgBackup = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    LaDestino: TLabel;
    EdDestino: TdmkEdit;
    SBDestino: TSpeedButton;
    Label1: TLabel;
    EdHora: TdmkEdit;
    Label4: TLabel;
    EdHost: TdmkEdit;
    EdDb: TdmkEdit;
    Label6: TLabel;
    EdPorta: TdmkEdit;
    Label7: TLabel;
    SpeedButton2: TSpeedButton;
    LaDias: TLabel;
    EdDias: TdmkEdit;
    SBDias: TSpeedButton;
    EdUser: TdmkEdit;
    Label2: TLabel;
    Label8: TLabel;
    EdPass: TdmkEdit;
    CkAtivo: TdmkCheckBox;
    CkZipar: TdmkCheckBox;
    CBTipo: TComboBox;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBDiasClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SBDestinoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBTipoChange(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraJanela(Tipo: Integer);
  public
    { Public declarations }
    FDB: TmySQLDatabase;
    FCodigo: Integer;
  end;

  var
  FmCfgBackup: TFmCfgBackup;

implementation

uses UnMyObjects, UMySQLDB, UnGrl_Consts, DmkDAC_PF, UnGrl_DmkDB;

{$R *.DFM}

procedure TFmCfgBackup.BtOKClick(Sender: TObject);
var
  DataBase: TmySQLDataBase;
  QryUpd, QryAux: TmySQLQuery;
  Dias, Porta, Zipar, Tipo, Ativo: Integer;
  Destino, Hora, Host, Db, User, Pass, Msg: String;
begin
  Tipo    := CBTipo.ItemIndex;
  Destino := EdDestino.ValueVariant;
  Hora    := Geral.FDT(EdHora.ValueVariant, 102);
  Dias    := EdDias.ValueVariant;
  Host    := EdHost.ValueVariant;
  Porta   := EdPorta.ValueVariant;
  DB      := EdDb.ValueVariant;
  User    := EdUser.ValueVariant;
  Pass    := EdPass.ValueVariant;
  Zipar   := Geral.BoolToInt(CkZipar.Checked);
  Ativo   := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Tipo = -1, CBTipo, 'Tipo de configura��o n�o informada!') then Exit;
  //
  if Tipo = 1 then //Sincronismo
  begin
    Destino := '';
    Dias    := 0;
    Zipar   := 0;
  end else
  begin
    if MyObjects.FIC(Destino = '', EdDestino, 'Diret�rio destino n�o informado!') then Exit;
  end;
  //
  if MyObjects.FIC(Host = '', EdHost, 'Host n�o informado!') then Exit;
  if MyObjects.FIC(DB = '', EdDb, 'Banco de dados n�o informado!') then Exit;
  //
  if USQLDB.TestarConexaoMySQL(Self, Host, User, Pass, DB, Porta, False) then
  begin
    if Tipo = 1 then //Sincronismo
    begin
      DataBase := TmySQLDataBase.Create(Self);
      QryAux   := TmySQLQuery.Create(FDB.Owner);
      try
        UnDmkDAC_PF.ConectaMyDB_DAC(DataBase, DB, Host,
          Porta,  User, Pass, True, True, True);
        //
        if DataBase.Connected then
        begin
          if not USQLDB.Sincro_ValidaDBUnidirecional(DataBase, QryAux, DB, Msg) then
          begin
            Geral.MB_Aviso(Msg);
            Exit;
          end;
        end;
      finally
        DataBase.Free;
      end;
    end;
    try
      QryUpd          := TmySQLQuery.Create(FDB.Owner);
      QryUpd.Database := FDB;
      //
      if ImgTipo.SQLType = stIns then
        QryUpd.SQL.Add('INSERT INTO bkagenda SET ')
      else
        QryUpd.SQL.Add('UPDATE bkagenda SET ');
      //
      QryUpd.SQL.Add('Tipo=:P0, Destino=:P1, Hora=:P2, Dias=:P3, ');
      QryUpd.SQL.Add('Host=:P4, Port=:P5, Db=:P6, User=:P7, ');
      QryUpd.SQL.Add('Pass=AES_ENCRYPT(:P8, :P9), ');
      QryUpd.SQL.Add('Zipar=:P10, Ativo=:P11 ');
      //
      if ImgTipo.SQLType = stUpd then
        QryUpd.SQL.Add('WHERE Codigo=:P12 ');
      //
      QryUpd.Params[00].AsInteger := Tipo;
      QryUpd.Params[01].AsString  := Destino;
      QryUpd.Params[02].AsString  := Hora;
      QryUpd.Params[03].AsInteger := Dias;
      QryUpd.Params[04].AsString  := Host;
      QryUpd.Params[05].AsInteger := Porta;
      QryUpd.Params[06].AsString  := DB;
      QryUpd.Params[07].AsString  := User;
      QryUpd.Params[08].AsString  := Pass;
      QryUpd.Params[09].AsString  := CO_RandStrWeb01;
      QryUpd.Params[10].AsInteger := Zipar;
      QryUpd.Params[11].AsInteger := Ativo;
      //
      if ImgTipo.SQLType = stUpd then
        QryUpd.Params[12].AsInteger := FCodigo;
      //
      QryUpd.ExecSQL;
      //
      Close;
    finally
      QryAux.Free;
      QryUpd.Free;
    end;
  end else
    Geral.MB_Aviso('N�o foi poss�vel abrir conex�o com o servidor!' +
      sLineBreak + 'Verifique se os dados informados est�o corretos!');
end;

procedure TFmCfgBackup.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCfgBackup.CBTipoChange(Sender: TObject);
begin
  ConfiguraJanela(CBTipo.ItemIndex);
end;

procedure TFmCfgBackup.ConfiguraJanela(Tipo: Integer);
begin
  if Tipo = 1 then //Sincronismo
  begin
    LaDestino.Visible := False;
    EdDestino.Visible := False;
    SBDestino.Visible := False;
    LaDias.Visible    := False;
    EdDias.Visible    := False;
    SBDias.Visible    := False;
    CkZipar.Visible   := False;
  end else
  begin
    LaDestino.Visible := True;
    EdDestino.Visible := True;
    SBDestino.Visible := True;
    LaDias.Visible    := True;
    EdDias.Visible    := True;
    SBDias.Visible    := True;
    CkZipar.Visible   := True;
  end;
end;

procedure TFmCfgBackup.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCfgBackup.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBTipo.Items.Clear;
  CBTipo.Items.AddStrings(USQLDB.Backup_ConfiguraTipos());
end;

procedure TFmCfgBackup.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCfgBackup.FormShow(Sender: TObject);
begin
  ConfiguraJanela(CBTipo.ItemIndex);
end;

procedure TFmCfgBackup.SBDiasClick(Sender: TObject);
begin
  Geral.MB_Info('Quantidade de dias para manter os backup!' + sLineBreak +
    sLineBreak + 'EXEMPLO: ' + sLineBreak + sLineBreak + 'Dias = 10' +
    sLineBreak + 'O aplicativo ir� manter os backups dos �ltimos 10 dias!' +
    sLineBreak + 'Os demais backups ser�o exclu�dos automaticamente!' + sLineBreak +
    sLineBreak + 'AVISO:' + sLineBreak + sLineBreak +
    'Preencha com o valor "0 (zero)" para manter todos!');
end;

procedure TFmCfgBackup.SpeedButton2Click(Sender: TObject);
begin
  Geral.MB_Aviso('O backup ser� realizado todos os dias no hor�rio a ser configurado!');
end;

procedure TFmCfgBackup.SBDestinoClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDestino);
end;

end.
