u n i t   B a c k u p 2 ;  
  
 i n t e r f a c e  
  
 u s e s  
     W i n d o w s ,   M e s s a g e s ,   S y s U t i l s ,   C l a s s e s ,   G r a p h i c s ,   C o n t r o l s ,   F o r m s ,   D i a l o g s ,  
     E x t C t r l s ,   S t d C t r l s ,   B u t t o n s ,   U n   D b ,   m y S Q L D b T a b l e s ,   D B C t r l s ,   d m k G e r a l ,  
     d m k E d i t ,   U n D m k P r o c F u n c ;  
  
 t y p e  
     T F m B a c k u p 2   =   c l a s s ( T F o r m )  
         P a i n e l D a d o s :   T P a n e l ;  
         P a i n e l C o n f i r m a :   T P a n e l ;  
         B t O K :   T B i t B t n ;  
         B t S a i d a :   T B i t B t n ;  
         P a i n e l T i t u l o :   T P a n e l ;  
         I m a g e 1 :   T I m a g e ;  
         Q r D a t a b a s e s :   T m y S Q L Q u e r y ;  
         Q r D a t a b a s e s D a t a b a s e :   T S t r i n g F i e l d ;  
         C B D B :   T D B L o o k u p C o m b o B o x ;  
         L a b e l 1 :   T L a b e l ;  
         E d D e s t i n o 1 :   T d m k E d i t ;  
         L a b e l 2 :   T L a b e l ;  
         E d D e s t i n o 2 :   T d m k E d i t ;  
         L a b e l 3 :   T L a b e l ;  
         S p e e d B u t t o n 1 :   T S p e e d B u t t o n ;  
         S p e e d B u t t o n 2 :   T S p e e d B u t t o n ;  
         E d S e n h a :   T E d i t ;  
         L a b e l 4 :   T L a b e l ;  
         D s D a t a b a s e s :   T D a t a S o u r c e ;  
         E d P a t h M y S Q L :   T d m k E d i t ;  
         L a b e l 5 :   T L a b e l ;  
         S p e e d B u t t o n 3 :   T S p e e d B u t t o n ;  
         L a A l l o w :   T L a b e l ;  
         L a B u f f e r :   T L a b e l ;  
         E d A l l o w :   T E d i t ;  
         E d B u f f e r :   T E d i t ;  
         E d A r q u i v o :   T E d i t ;  
         L a b e l 6 :   T L a b e l ;  
         M e R e s u l t :   T M e m o ;  
         C k C r i a D B :   T C h e c k B o x ;  
         M e B a s e :   T M e m o ;  
         M e C m d :   T M e m o ;  
         P a n e l 1 :   T P a n e l ;  
         p r o c e d u r e   B t S a i d a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S p e e d B u t t o n 1 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S p e e d B u t t o n 2 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t O K C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S p e e d B u t t o n 3 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   E d A l l o w E x i t ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   E d B u f f e r E x i t ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   C B D B C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   C B D B E x i t ( S e n d e r :   T O b j e c t ) ;  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
         f u n c t i o n   C r i a N o m e A r q u i v o ( D i r e t o r i o :   S t r i n g ) :   S t r i n g ;  
         p r o c e d u r e   E x e c u t a C m d ( c m d :   S t r i n g ) ;  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
     e n d ;  
  
     v a r  
     F m B a c k u p 2 :   T F m B a c k u p 2 ;  
  
 i m p l e m e n t a t i o n  
  
 u s e s   W e t B l u e M L A _ B K ,   U n M y O b j e c t s ;  
  
 { $ R   * . D F M }  
  
 p r o c e d u r e   T F m B a c k u p 2 . B t S a i d a C l i c k ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     C l o s e ;  
 e n d ;  
  
 p r o c e d u r e   T F m B a c k u p 2 . F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     M y O b j e c t s . C o r I n i C o m p o n e n t e ;  
 e n d ;  
  
 p r o c e d u r e   T F m B a c k u p 2 . F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     M L A G e r a l . L o a d T e x t B i t m a p T o P a n e l ( 0 ,   0 ,   C a p t i o n ,   I m a g e 1 ,   P a i n e l T i t u l o ,   T r u e ,   0 ) ;  
 e n d ;  
  
 p r o c e d u r e   T F m B a c k u p 2 . S p e e d B u t t o n 1 C l i c k ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
 {  
     i f   D i r D i a l o g . E x e c u t e   t h e n  
     b e g i n  
         E d D e s t i n o 1 . T e x t   : =   D i r D i a l o g . P a t h + ' \ ' ;  
         M L A G e r a l . W r i t e A p p K e y ( ' B a c k U p D e s t i n o 1 ' ,   ' D e r m a t e k ' ,   E d D e s t i n o 1 . T e x t ,   k t S t r i n g ,  
         H K E Y _ L O C A L _ M A C H I N E ) ;  
     e n d ;  
 }  
     M y O b j e c t s . D e f i n e D i r e t o r i o ( S e l f ,   E d D e s t i n o 1 ) ;  
     G e r a l . W r i t e A p p K e y ( ' B a c k U p D e s t i n o 1 ' ,   ' D e r m a t e k ' ,   E d D e s t i n o 1 . T e x t ,   k t S t r i n g ,  
     H K E Y _ L O C A L _ M A C H I N E ) ;  
 e n d ;  
  
 p r o c e d u r e   T F m B a c k u p 2 . S p e e d B u t t o n 2 C l i c k ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
 {  
     i f   D i r D i a l o g . E x e c u t e   t h e n  
     b e g i n  
         E d D e s t i n o 2 . T e x t   : =   D i r D i a l o g . P a t h + ' \ ' ;  
         M L A G e r a l . W r i t e A p p K e y ( ' B a c k U p D e s t i n o 2 ' ,   ' D e r m a t e k ' ,   E d D e s t i n o 2 . T e x t ,   k t S t r i n g ,  
         H K E Y _ L O C A L _ M A C H I N E ) ;  
     e n d ;  
 }  
     M y O b j e c t s . D e f i n e D i r e t o r i o ( S e l f ,   E d D e s t i n o 2 ) ;  
     G e r a l . W r i t e A p p K e y ( ' B a c k U p D e s t i n o 2 ' ,   ' D e r m a t e k ' ,   E d D e s t i n o 2 . T e x t ,   k t S t r i n g ,  
     H K E Y _ L O C A L _ M A C H I N E ) ;  
 e n d ;  
  
 p r o c e d u r e   T F m B a c k u p 2 . F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     M e R e s u l t . L i n e s . C l e a r ;  
     / /  
     E d D e s t i n o 1 . T e x t   : =   G e r a l . R e a d A p p K e y ( ' B a c k U p D e s t i n o 1 ' ,   ' D e r m a t e k ' ,   k t S t r i n g ,   ' ' ,  
     H K E Y _ L O C A L _ M A C H I N E ) ;  
     / /  
     E d D e s t i n o 2 . T e x t   : =   G e r a l . R e a d A p p K e y ( ' B a c k U p D e s t i n o 2 ' ,   ' D e r m a t e k ' ,   k t S t r i n g ,   ' ' ,  
     H K E Y _ L O C A L _ M A C H I N E ) ;  
     / /  
     E d P a t h M y S Q L . T e x t   : =   G e r a l . R e a d A p p K e y ( ' P a t h M y S Q L ' ,   ' D e r m a t e k ' ,   k t S t r i n g ,  
     E d P a t h M y S Q L . T e x t ,   H K E Y _ L O C A L _ M A C H I N E ) ;  
     / /  
     Q r D a t a b a s e s . O p e n ;  
 e n d ;  
  
 p r o c e d u r e   T F m B a c k u p 2 . B t O K C l i c k ( S e n d e r :   T O b j e c t ) ;  
 c o n s t  
     n o C r e a t e D b   =   '   - - n o - c r e a t e - d b   ' ;  
 v a r  
     S e n h a ,   S e r v i d o r ,   U s u a r i o ,   T e x t o 1 ,   T e x t o 2 ,   C r i a D B :   S t r i n g ;  
     q :   I n t e g e r ;  
 b e g i n  
     S c r e e n . C u r s o r   : =   c r H o u r G l a s s ;  
     U p d a t e ;  
     A p p l i c a t i o n . P r o c e s s M e s s a g e s ;  
     t r y  
         S e r v i d o r   : =   T m y S Q L D a t a b a s e ( Q r D a t a b a s e s . D a t a b a s e ) . H o s t ;  
         U s u a r i o     : =   T m y S Q L D a t a b a s e ( Q r D a t a b a s e s . D a t a b a s e ) . U s e r N a m e ;  
         S e n h a         : =   T m y S Q L D a t a b a s e ( Q r D a t a b a s e s . D a t a b a s e ) . U s e r P a s s w o r d ;  
         i f   C k C r i a D B . C h e c k e d   t h e n   C r i a D B   : =   ' '   e l s e   C r i a D B   : =   n o C r e a t e D B ;  
         i f   T r i m ( E d S e n h a . T e x t )   < >   ' '   t h e n   S e n h a   : =   E d S e n h a . T e x t ;  
         q   : =   0 ;  
         i f   T r i m ( C B D B . T e x t )   =   ' '   t h e n  
         b e g i n  
             A p p l i c a t i o n . M e s s a g e B o x ( ' D e f i n a   o   b a n c o   d e   d a d o s ! ' ,   ' E r r o ' ,  
             M B _ O K + M B _ I C O N E R R O R ) ;  
             E x i t ;  
         e n d ;  
         i f   T r i m ( E d P a t h M y S Q L . T e x t )   =   ' '   t h e n  
         b e g i n  
             A p p l i c a t i o n . M e s s a g e B o x ( ' D e f i n a   o   c a m i n h o   d o   M y S Q L ! ' ,   ' E r r o ' ,  
             M B _ O K + M B _ I C O N E R R O R ) ;  
             E x i t ;  
         e n d ;  
         i f   T r i m ( E d D e s t i n o 1 . T e x t )   < >   ' '   t h e n  
         b e g i n  
             i f   n o t   D i r e c t o r y E x i s t s ( E d D e s t i n o 1 . T e x t )   t h e n  
                 F o r c e D i r e c t o r i e s ( E d D e s t i n o 1 . T e x t ) ;  
             i f   D i r e c t o r y E x i s t s ( E d D e s t i n o 1 . T e x t )   t h e n  
             b e g i n  
                 q   : =   q   +   1 ;  
                 T e x t o 1   : =   d m k P F . N o m e L o n g o P a r a C u r t o ( E d P a t h M y S Q L . T e x t ) + ' m y s q l d u m p   ' +  
                 L a A l l o w . C a p t i o n   +   G e r a l . S o N u m e r o _ T T ( E d A l l o w . T e x t ) + '   ' +  
                 L a B u f f e r . C a p t i o n   +   G e r a l . S o N u m e r o _ T T ( E d B u f f e r . T e x t ) +   c r i a D B   +  
                 '   - h   '   +   S e r v i d o r   +   '   - u   '   +   U s u a r i o   +   '   - p '   +   S e n h a   +   '     - B   ' + C B D B . T e x t   +   '   >   ' +  
                 C r i a N o m e A r q u i v o ( E d D e s t i n o 1 . T e x t ) ;  
                 M e C m d . L i n e s . A d d ( T e x t o 1 ) ;  
             e n d     e l s e   A p p l i c 