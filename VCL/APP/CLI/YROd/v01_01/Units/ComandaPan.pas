unit ComandaPan;

interface

uses
  Winapi.Windows, Winapi.Messages, Math,
  System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, mySQLDbTables, Vcl.Buttons, Vcl.StdCtrls, Data.DB,  Vcl.ComCtrls,
  dmkCheckGroup, dmkGeral, UnInternalConsts, dmkCompoStore, UnGrl_Vars,
  UnDmkEnums, dmkDBGridZTO, dmkDBGridTRB, Data.FMTBcd,
  Data.SqlExpr, Vcl.Menus;

type
  //TDBgrid=Class(DBGrids.TDBGrid)
  TDBGrid = class(Vcl.DBGrids.TDBGrid)
  private
     FColResize: TNotifyEvent;
     procedure ColWidthsChanged; override;
  protected
     property OnColResize: TNotifyEvent read FColResize Write FColResize;
  end;
  TFmComandaPan = class(TForm)
    Panel1: TPanel;
    DBGPan: TDBGrid;
    PnGerencia: TPanel;
    SbPAGO: TSpeedButton;
    Qry: TMySQLQuery;
    SbDtHrPedido: TSpeedButton;
    SbDtHrRetird: TSpeedButton;
    SbDtHrLiProd: TSpeedButton;
    SbCliente: TSpeedButton;
    SBUF: TSpeedButton;
    SbPregao: TSpeedButton;
    SbProduto: TSpeedButton;
    SbPortal: TSpeedButton;
    SbNrProcesso: TSpeedButton;
    SbNO_Responsav1: TSpeedButton;
    SbNO_Responsav2: TSpeedButton;
    SbNO_Responsav3: TSpeedButton;
    SbNO_Empresa: TSpeedButton;
    SbNO_GraFabMCd: TSpeedButton;
    SbNO_Representn: TSpeedButton;
    SbNO_Segmento: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    CSTabSheetChamou: TdmkCompoStore;
    SbEstah: TSpeedButton;
    SbColocacao: TSpeedButton;
    SbAmstrConvocDtHr: TSpeedButton;
    SbAmstrLimEntrDtHr: TSpeedButton;
    SbAmstrEntrReaDtHr: TSpeedButton;
    QrComandaCab: TMySQLQuery;
    QrComandaCabNO_Cliente: TWideStringField;
    QrComandaCabCodigo: TIntegerField;
    QrComandaCabEmpresa: TIntegerField;
    QrComandaCabDtHrPedido: TDateTimeField;
    QrComandaCabDtHrLiProd: TDateTimeField;
    QrComandaCabDtHrRetird: TDateTimeField;
    QrComandaCabCliente: TIntegerField;
    QrComandaCabQtdeTotal: TFloatField;
    QrComandaCabValrTotal: TFloatField;
    QrComandaCabValrNPag: TFloatField;
    QrComandaCabValrPago: TFloatField;
    QrComandaCabNome: TWideStringField;
    QrComandaCabDtHrLiProd_TXT: TWideStringField;
    QrComandaCabDtHrRetird_TXT: TWideStringField;
    QrComandaCabGraCusPrc: TIntegerField;
    QrComandaCabNO_GraCusPrc: TWideStringField;
    QrComandaCabBrutTotal: TFloatField;
    QrComandaCabValrAPag: TFloatField;
    QrComandaCabCondPgCombin: TIntegerField;
    QrComandaCabNO_CondPgCombin: TWideStringField;
    DsComandaCab: TDataSource;
    QrComandaCabPAGO: TWideStringField;
    Panel2: TPanel;
    Memo1: TMemo;
    dmkDBGridTRB1: TdmkDBGridTRB;
    SQLQuery1: TSQLQuery;
    procedure FormCreate(Sender: TObject);
    procedure DBGPanDblClick(Sender: TObject);
    procedure DBGPanColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure SbDtHrPedidoClick(Sender: TObject);
    procedure SbDtHrLiProdClick(Sender: TObject);
    procedure SbDtHrRetirdClick(Sender: TObject);
    procedure SbClienteClick(Sender: TObject);
    procedure SBUFClick(Sender: TObject);
    procedure DBGPanTitleClick(Column: TColumn);
    procedure SbPregaoClick(Sender: TObject);
    procedure SbProdutoClick(Sender: TObject);
    procedure DBGPanDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SbPortalClick(Sender: TObject);
    procedure SbNrProcessoClick(Sender: TObject);
    procedure SbNO_Responsav1Click(Sender: TObject);
    procedure SbNO_Responsav2Click(Sender: TObject);
    procedure SbNO_Responsav3Click(Sender: TObject);
    procedure SbNO_EmpresaClick(Sender: TObject);
    procedure SbNO_GraFabMCdClick(Sender: TObject);
    procedure SbNO_SegmentoClick(Sender: TObject);
    procedure SbNO_RepresentnClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SbColocacaoClick(Sender: TObject);
    procedure SbEstahClick(Sender: TObject);
    procedure SbAmstrConvocDtHrClick(Sender: TObject);
    procedure SbAmstrLimEntrDtHrClick(Sender: TObject);
    procedure SbAmstrEntrReaDtHrClick(Sender: TObject);
    procedure SbPAGOClick(Sender: TObject);
    procedure dmkDBGridTBB1ColResize(Sender: TObject);
    procedure SQLEnh1Click(Sender: TObject);
    procedure SQLEnh2Click(Sender: TObject);
    procedure TSQLEnh1Clik(Sender: TObject);
  private
    { Private declarations }
    FTickCount: Cardinal;

    FPesqPAGO: Integer;

    FDtHrPedidoIni, FDtHrPedidoFim, FDtHrLiProdIni, FDtHrLiProdFim,
    FDtHrRetirdIni, FDtHrRetirdFim: TDateTime;

    FReabertuHrIni, FReabertuHrFim, FAberturaHrIni, FAberturaHrFim,
    FLimCadPpHrIni, FLimCadPpHrFim, FAmstrConvocHrIni, FAmstrConvocHrFim,
    FAmstrLimEntrHrIni, FAmstrLimEntrHrFim, FAmstrEntrReaHrIni,
    FAmstrEntrReaHrFim: TTime;

    FTextoPesqCli, FTextoPesqPAGO(*, FTextoPesqUF, FTextoPesqPregao, FTextoPesqProd,
    FTextoPesqProduto, FTextoPesqPortal, FTextoPesqNrProcesso,
    FTextoPesqResponsav1, FTextoPesqResponsav2, FTextoPesqResponsav3,
    FTextoPesqEmpresa, FTextoPesqGraFabMCd, FTextoPesqSegmento,
    FTextoPesqRepresentn*): String;

    FSQL_DtHrPedido, FSQL_DtHrLiProd, FSQL_DtHrRetird, FSQL_Cliente, FSQL_PAGO:
    String;

    (*FSQL_Reabertu, FSQL_Abertura, FSQL_LimCadPp, FSQL_CLiente,
    FSQL_UF, FSQL_Pregao, FSQL_Produto, FSQL_Portal, FSQL_NrProcesso,
    FSQL_Responsav1, FSQL_Responsav2, FSQL_Responsav3, FSQL_Empresa,
    FSQL_GraFabMCd, FSQL_Segmento, FSQL_Representn, FSQL_Colocacao, FSQL_Estah,
    FSQL_AmstrConvocDtHr, FSQL_AmstrLimEntrDtHr, FSQL_AmstrEntrReaDtHr: String;
*)

    FColocacaoMin, FColocacaoMax, FEstahMin, FEstahMax: Integer;
    //
    procedure ColResize(Sender: TObject);
    //
    function  ContaDoRegistroSel(): Integer;
    procedure DefinePeriodoPesquisa(const Titulo, StrSQL: String; var _FSQL:
              String; const SpeedButton: TSpeedButton; var DtIni, DtFim:
              TDateTime);
    procedure DefineIntervaloInt(const Titulo, Campo: String; var _FSQL: String;
              const SpeedButton: TSpeedButton; var ValMin, ValMax: Integer);
    procedure ReopenComandaCab(Codigo: Integer);
    procedure PosicionaBotao(NomeCampo: String; SpeedButton: TSpeedButton);
    procedure DefineGlyph(Index: Integer; SpeedButton: TSpeedButton);
    procedure ReposicionaBotoes();
    procedure PesquisaSimplesTabela(const Tabela, FldPsq: String; var _TextoPesq,
              _FSQL: String; const SpeedButton: TSpeedButton);
    procedure PesquisaSQLInRadioGroup(const Titulo, Label_Caption: String; var
              _ItemSel: Integer; var _TextoPesq, _FSQL: String; const
              SpeedButton: TSpeedButton; const ItensCaption: array of String;
              const ItensSQLs: array of String);
    procedure PesquisaSimplesInput(const Titulo, Label_Caption, FldPsq: String;
              var _TextoPesq, _FSQL: String; const SpeedButton: TSpeedButton);
    procedure LimpaVariaveisTexto();
  public
    { Public declarations }
  end;

var
  FmComandaPan: TFmComandaPan;

implementation

uses
  Module, DmkDAC_PF, UnMyObjects, UnDmkProcFunc, UnApp_Jan, ModuleGeral, MyGlyfs,
  UnMySQLCuringa, NomeX(*, GetMinMax*);

{$R *.dfm}

{ TFmBWLiciPan }

const
  FColsPsq = 4;
  //
  FCampos: array[0..FColsPsq] of String = ('DtHrPedido', 'DtHrLiProd_TXT',
    'DtHrRetird_TXT', 'NO_Cliente', 'PAGO'(*, 'UF', 'Pregao', 'NO_Produto',
    'NO_BWPortal', 'NrProcesso', 'NO_Responsav1', 'NO_Responsav2',
    'NO_Responsav3', 'NO_Empresa', 'NO_GraFabMCd', 'NO_Segmento',
    'NO_Representn', 'Colocacao', 'Estah', 'AmstrConvocDt_TXT',
    'AmstrLimEntrDt_TXT', 'AmstrEntrReaDt_TXT'*));
var
  FSBs: array[0..FColsPsq] of TSpeedButton;
  Resizes: Integer = 0;

procedure TFmComandaPan.ColResize(Sender: TObject);
begin
  ReposicionaBotoes();
end;

function TFmComandaPan.ContaDoRegistroSel(): Integer;
begin
  if QrComandaCab.State <> dsInactive then
    Result := QrComandaCabCodigo.Value
  else
    Result := 0;
end;

procedure TFmComandaPan.DBGPanColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
  Campo: String;
  I: Integer;
begin
  Campo := Uppercase(DBGPan.Columns[ToIndex].FieldName);
  for I := Low(FCampos) to High(FCampos) do
  begin
    if Campo = Uppercase(FCampos[I]) then
      PosicionaBotao(Campo, FSBs[I]);
  end;
end;

procedure TFmComandaPan.DBGPanDblClick(Sender: TObject);
var
  TickCount, TickDiff: Cardinal;
  pt: TGridCoord;
begin
  TickCount := GetTickCount();
  TickDiff  := TickCount - FTickCount;
  if TickDiff < 300 then Exit;
(*  pt := TDBGrid(Sender).MouseCoord(Mouse., y);
   if pt.y=0 then
     TDBGrid(Sender)..Cursor:=crHandPoint
   else
     TDBGrid(Sender)..Cursor := crDefault;
*)  //
  if QrComandaCab.State <> dsInactive then
    App_Jan.MostraFormComandaCab(QrComandaCabCodigo.Value);
end;

procedure TFmComandaPan.DBGPanDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Rect.Top < 20 then
    ReposicionaBotoes();
end;

procedure TFmComandaPan.DBGPanTitleClick(Column: TColumn);
begin
  FTickCount := GetTickCount;
end;

procedure TFmComandaPan.DefineGlyph(Index: Integer; SpeedButton: TSpeedButton);
begin
  SpeedButton.Glyph := nil;
  FmMyGlyfs.ListaPesqGrid.GetBitmap(Index, SpeedButton.Glyph);
end;

procedure TFmComandaPan.DefineIntervaloInt(const Titulo, Campo: String;
  var _FSQL: String; const SpeedButton: TSpeedButton; var ValMin, ValMax: Integer);
const
  Casas     = 0;
  LeftZeros = 0;
  ValMinMin = '';
  ValMaxMin = '';
  ValMinMax = '';
  ValMaxMax = '';
  Obrigatorio = False;
  ValMinCaption = 'M�nimo';
  ValMaxCaption = 'M�ximo';
  WidthVal = 72;
var
  DefaultMin, DefaultMax, ResultadoMin, ResultadoMax: Variant;
  Conta: Integer;
begin
(*&�%$#@!"
  Conta := ContaDoRegistroSel;
  //
  DefaultMin := ValMin;
  DefaultMax := ValMax;
  if MyObjects.GetMinMaxDmk(TFmGetMinMax, FmGetMinMax, TAllFormat.dmktfInteger,
  DefaultMin, DefaultMax, Casas, LeftZeros, ValMinMin, ValMaxMin, ValMinMax, ValMaxMax,
  Obrigatorio, Titulo, ValMinCaption, ValMaxCaption, WidthVal,
  ResultadoMin, ResultadoMax) then
  begin
    ValMin := ResultadoMin;
    ValMax := ResultadoMax;
    //
    _FSQL := EmptyStr;
    if ValMax > ValMin then
    begin
      _FSQL := ' AND ' + Campo + ' BETWEEN ' + Geral.FF0(ValMin) +
        ' AND ' + Geral.FF0(ValMax);
      //
      DefineGlyph(1, SpeedButton);
    end else
    begin
      DefineGlyph(0, SpeedButton);
    end;
    ReopenComandaCab(Conta);
  end;
*)
end;

procedure TFmComandaPan.DefinePeriodoPesquisa(const Titulo, StrSQL: String; var
  _FSQL: String; const SpeedButton: TSpeedButton; var DtIni, DtFim: TDateTime);
const
  HabilitaHora = False;
  //Titulo = 'Per�odo da Reabertura';
  InfoDetalhado = '';
var
  DtDefaultIni, DtDefaultFim, SelectIni, SelectFim: TDateTime;
  HrDefaultIni, HrDefaultFim: TTime; // = 1 - (1/24/60/60);
  Conta: Integer;
begin
  Conta := ContaDoRegistroSel;
  //
  DtDefaultIni := DtIni;
  DtDefaultFim := DtFim;
  SelectIni    := 0;
  SelectFim    := 0;
  HrDefaultIni := 0;
  HrDefaultFim := 1 - (1/24/60/60);
  //
  if MyObjects.ObtemDatasPeriodo(DtDefaultIni, DtDefaultFim, SelectIni, SelectFim,
  HabilitaHora, HrDefaultIni, HrDefaultFim, Titulo, InfoDetalhado) then
  begin
    DtIni := Int(SelectIni);
    //HrIni := SelectIni - DtIni;
    //
    DtFim := Int(SelectFim);
    //HrFim := SelectFim - DtFim;
    //
    _FSQL := EmptyStr;
    if SelectFim > SelectIni then
    begin
      _FSQL := dmkPF.SQL_Periodo(StrSQL, SelectIni,
      SelectFim, True, True);
      DefineGlyph(1, SpeedButton);
    end else
    begin
      DefineGlyph(0, SpeedButton);
    end;
    ReopenComandaCab(Conta);
  end;
end;

procedure TFmComandaPan.dmkDBGridTBB1ColResize(Sender: TObject);
begin
  Resizes := Resizes;
  Memo1.Text := IntToStr(Resizes) + ': ' + Geral.FDT(Now(), 100) + sLineBreak + Memo1.Text ;
end;

procedure TFmComandaPan.FormCreate(Sender: TObject);
begin
  //
  FSBs[00] := SbDtHrPedido;
  FSBs[01] := SbDtHrLiProd;
  FSBs[02] := SbDtHrRetird;
  FSBs[03] := SbCliente;
  FSBs[04] := SbPAGO;
(*
  FSBs[05] := SbUF;
  FSBs[06] := SbPregao;
  FSBs[07] := SbProduto;
  FSBs[08] := SbPortal;
  FSBs[09] := SbNrProcesso;
  FSBs[10] := SbNO_Responsav1;
  FSBs[11] := SbNO_Responsav2;
  FSBs[12] := SbNO_Responsav3;
  FSBs[13] := SbNO_Empresa;
  FSBs[14] := SbNO_GraFabMCd;
  FSBs[15] := SbNO_Segmento;
  FSBs[16] := SbNO_Representn;
  FSBs[17] := SbColocacao;
  FSBs[18] := SbEstah;
  FSBs[19] := SbAmstrConvocDtHr;
  FSBs[20] := SbAmstrLimEntrDtHr;
  FSBs[21] := SbAmstrEntrReaDtHr;
  *)
  //
  FReabertuHrIni := 0;
  FReabertuHrFim := 1 - (1/24/60/60);
  FAberturaHrIni := 0;
  FAberturaHrFim := 1 - (1/24/60/60);
  FLimCadPpHrIni := 0;
  FLimCadPpHrFim := 1 - (1/24/60/60);
  //
  FAmstrConvocHrIni := 0;
  FAmstrConvocHrFim := 1 - (1/24/60/60);
  FAmstrLimEntrHrIni := 0;
  FAmstrLimEntrHrFim := 1 - (1/24/60/60);
  FAmstrEntrReaHrIni := 0;
  FAmstrEntrReaHrFim := 1 - (1/24/60/60);
  //
  FPesqPAGO := 0;
  //
  LimpaVariaveisTexto();
  //
  DBGPan.OnColResize := ColResize;
  //
  ReopenComandaCab(0);
  //
  ReposicionaBotoes();
  FColocacaoMin := 0;
  FColocacaoMax := 1000;
  FEstahMin     := 0;
  FEstahMax     := 1000;
end;

procedure TFmComandaPan.FormShow(Sender: TObject);
begin
  //PnGerencia.Visible := True;
end;

procedure TFmComandaPan.LimpaVariaveisTexto();
begin
  FTextoPesqCli          := '';
  FTextoPesqPAGO         := '';
(*
  FTextoPesqUF           := '';
  FTextoPesqPregao       := '';
  FTextoPesqProd         := '';
  FTextoPesqProduto      := '';
  FTextoPesqPortal       := '';
  FTextoPesqNrProcesso   := '';
  FTextoPesqResponsav1   := '';
  FTextoPesqResponsav2   := '';
  FTextoPesqResponsav3   := '';
  FTextoPesqEmpresa      := '';
  FTextoPesqGraFabMCd    := '';
  FTextoPesqSegmento     := '';
  FTextoPesqRepresentn   := '';
  FColocacaoMin          := 0;
  FColocacaoMax          := 100;
*)
  //
  FSQL_DtHrPedido        := '';
  FSQL_DtHrLiProd        := '';
  FSQL_DtHrRetird        := '';
  FSQL_Cliente           := '';
  FSQL_PAGO              := '';
(*
  FSQL_Reabertu          := '';
  FSQL_Abertura          := '';
  FSQL_LimCadPp          := '';
  FSQL_CLiente           := '';
  FSQL_UF                := '';
  FSQL_Pregao            := '';
  FSQL_Produto           := '';
  FSQL_Portal            := '';
  FSQL_NrProcesso        := '';
  FSQL_Responsav1        := '';
  FSQL_Responsav2        := '';
  FSQL_Responsav3        := '';
  FSQL_Empresa           := '';
  FSQL_GraFabMCd         := '';
  FSQL_Segmento          := '';
  FSQL_Representn        := '';
  FSQL_Colocacao         := '';
  FSQL_Estah             := '';
  FSQL_AmstrConvocDtHr   := '';
  FSQL_AmstrLimEntrDtHr  := '';
  FSQL_AmstrEntrReaDtHr  := '';
*)
  //
end;

procedure TFmComandaPan.PesquisaSimplesInput(const Titulo, Label_Caption, FldPsq:
 String; var _TextoPesq, _FSQL: String; const SpeedButton: TSpeedButton);
const
  ReturnCodUsu = False;
  LEFT_JOIN = '';
var
  Codigo: Integer;
  Conta: Integer;
  Compare: String;
begin
  Conta := ContaDoRegistroSel;
  //
  if InputQuery(Titulo, Label_Caption, _TextoPesq) and (_TextoPesq <> EmptyStr)then
  begin
   if pos('%', _TextoPesq) > 0 then
     Compare := ' LIKE '
   else
     Compare := '=';
   //
    _FSQL := ' AND ' + FldPsq + Compare + '"' + _TextoPesq + '"';
    DefineGlyph(1, SpeedButton);
  end else
  begin
    _FSQL := EmptyStr;
    DefineGlyph(0, SpeedButton);
  end;
  ReopenComandaCab(Conta);
end;

procedure TFmComandaPan.PesquisaSimplesTabela(const Tabela, FldPsq: String; var
  _TextoPesq, _FSQL: String; const SpeedButton: TSpeedButton);
const
  ReturnCodUsu = False;
  LEFT_JOIN = '';
var
  Codigo: Integer;
  Conta: Integer;
begin
  Conta := ContaDoRegistroSel;
  //
  Codigo := CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, Tabela, Dmod.MyDB,
  EmptyStr, ReturnCodUsu, LEFT_JOIN, _TextoPesq);
  //
  if VAR_EXEC_CURINGA then
  begin
    _TextoPesq := VAR_TextoPesq_CURINGA;
    if VAR_CORDA_CODIGOS <> EmptyStr then
    begin
      _FSQL := ' AND ' + FldPsq + ' IN (' + VAR_CORDA_CODIGOS + ') ';
      DefineGlyph(1, SpeedButton);
    end else
    if Codigo <> 0 then
    begin
      _FSQL := ' AND ' + FldPsq + '=' + Geral.FF0(Codigo);
      DefineGlyph(1, SpeedButton);
    end else
    begin
      _FSQL := EmptyStr;
      DefineGlyph(0, SpeedButton);
    end;
    //
    ReopenComandaCab(Conta);
  end;
end;

procedure TFmComandaPan.PesquisaSQLInRadioGroup(const Titulo, Label_Caption:
  String; var _ItemSel: Integer; var _TextoPesq, _FSQL: String; const
  SpeedButton: TSpeedButton; const ItensCaption: array of String;
  const ItensSQLs: array of String);
var
  Codigos: array of Integer;
  //Itens: array of String;
  I, N, Conta: Integer;
  P, K: Single;
begin
  Conta := ContaDoRegistroSel();
  //
  FPesqPAGO := MyObjects.SelRadioGroup(Titulo, Label_Caption, ItensCaption, 3);
  if FPesqPAGO > -1 then
  begin
    _FSQL := ItensSQLs[FPesqPAGO];
    DefineGlyph(1, SpeedButton);
  end else
  begin
    _FSQL := EmptyStr;
    DefineGlyph(0, SpeedButton);
  end;
  //
  ReopenComandaCab(Conta);
end;

procedure TFmComandaPan.PosicionaBotao(NomeCampo: String; SpeedButton: TSpeedButton);
var
  DataRect: TRect;
  I, K: Integer;
begin
  if SpeedButton = nil then Exit;
  // Place the button in the first column.
  //if (Column.Index = 0) then
  begin
    K := -1;
    for I := 0 to DBGPan.Columns.Count - 1 do
    begin
      if Uppercase(DBGPan.Columns[I].FieldName) = Uppercase(NomeCampo) then
      begin
        K := I;
        Break;
      end;
    end;
    if K = -1 then Exit;
    with TStringGrid(DBGPan) do
    begin
       DataRect := CellRect(K + 1(*Column.Index*), 0(*Row*));
    end;
    // Assign the button's parent to the grid.
    if SpeedButton.Parent <> DBGPan then
      SpeedButton.Parent := DBGPan ;
    // Set the button's coordinates.
    // In this case, right justify the button.
    if SpeedButton.Left <> (DataRect.Right - SpeedButton.Width) then
      SpeedButton.Left := (DataRect.Right - SpeedButton.Width) ;
    if (SpeedButton.Top <> DataRect.Top) then
      SpeedButton.Top := DataRect.Top ;

    // Make sure the button's height fits in row.
    if (SpeedButton.Height <> (DataRect.Bottom-DataRect.Top)) then
      SpeedButton.Height := (DataRect.Bottom-DataRect.Top);
  end;
end;

procedure TFmComandaPan.ReopenComandaCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrComandaCab, Dmod.MyDB, [
  'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ',
  'IF(cab.DtHrLiProd < 2, "", DATE_FORMAT(cab.DtHrLiProd, ',
  '"%d/%m/%Y %H:%i")) DtHrLiProd_TXT, ',
  'IF(cab.DtHrRetird < 2, "", DATE_FORMAT(cab.DtHrRetird, ',
  '"%d/%m/%Y %H:%i")) DtHrRetird_TXT, ',
  'gcp.Nome NO_GraCusPrc, ppc.Nome NO_CondPgCombin, ',
  'IF(cab.ValrAPag > 0 , "SIM", "N�O") PAGO, cab.*  ',
  'FROM comandacab cab ',
  'LEFT JOIN entidades cli ON cli.Codigo=cab.Cliente ',
  'LEFT JOIN gracusprc gcp ON gcp.Codigo=cab.GraCusPrc ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondPgCombin ',
  'WHERE cab.Codigo<>0 ',

  FSQL_DtHrPedido,
  FSQL_DtHrLiProd,
  FSQL_DtHrRetird,
  FSQL_Cliente,
  FSQL_PAGO,
(*
  FSQL_Produto,
  FSQL_Portal,
  FSQL_NrProcesso,
  FSQL_Responsav1,
  FSQL_Responsav2,
  FSQL_Responsav3,
  FSQL_Empresa,
  FSQL_GraFabMCd,
  FSQL_Segmento,
  FSQL_Representn,
  FSQL_Colocacao,
  FSQL_Estah,
  FSQL_AmstrConvocDtHr,
  FSQL_AmstrLimEntrDtHr,
  FSQL_AmstrEntrREaDtHr,
*)
  'ORDER BY DtHrLiProd DESC, DtHrPedido DESC, Cliente ',
  '']);
  //
  //Geral.MB_Teste(QrComandaCab.SQL.Text);
  //
  if Codigo <> 0 then
    QrComandaCab.Locate('Codigo', Codigo, []);
end;

procedure TFmComandaPan.ReposicionaBotoes();
var
  I: Integer;
begin
  for I := Low(FCampos) to High(FCampos) do
  begin
    //if Campo = Uppercase(FCampos[I]) then
      PosicionaBotao(FCampos[I], FSBs[I]);
  end;
(*
  PosicionaBotao('NO_BWStatus', SbBWStatus);
  PosicionaBotao('ReabertuDt_TXT', SbReabertuDtHr);
  PosicionaBotao('AberturaDt', SbAberturaDtHr);
  PosicionaBotao('LimCadPpDt', SbLimCadPpDtHr);
  PosicionaBotao('NO_Cliente', SbCliente);
  PosicionaBotao('UF', SbUF);
  PosicionaBotao('Pregao', SbPregao);
  PosicionaBotao('NO_Produto', SbProduto);
*)
end;

procedure TFmComandaPan.SbDtHrLiProdClick(Sender: TObject);
const
  Titulo = 'Per�odo limite da Produ��o';
  StrSQL = ' AND DtHrLiProd ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_DtHrLiProd, SbDtHrLiProd,
  FDtHrLiProdIni, FDtHrLiProdFim);
end;

procedure TFmComandaPan.SbAmstrConvocDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Convoca��o de Amostra';
  StrSQL = ' AND AmstrConvocDt ';
begin
(*&�%$#@!"
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_AmstrConvocDtHr, SbAmstrConvocDtHr,
  FAmstrConvocDtIni, FAmstrConvocDtFim, FAmstrConvocHrIni, FAmstrConvocHrFim);
*)
end;

procedure TFmComandaPan.SbAmstrEntrReaDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Entrega da Amostra';
  StrSQL = ' AND AmstrEntrReaDt ';
begin
(*&�%$#@!"
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_AmstrEntrReaDtHr, SbAmstrEntrReaDtHr,
  FAmstrEntrReaDtIni, FAmstrEntrReaDtFim, FAmstrEntrReaHrIni, FAmstrEntrReaHrFim);
*)
end;

procedure TFmComandaPan.SbAmstrLimEntrDtHrClick(Sender: TObject);
const
  Titulo = 'Per�odo da Data Limite de Entrega da Amostra';
  StrSQL = ' AND AmstrLimEntrDt ';
begin
(*&�%$#@!"
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_AmstrLimEntrDtHr, SbAmstrLimEntrDtHr,
  FAmstrLimEntrDtIni, FAmstrLimEntrDtFim, FAmstrLimEntrHrIni, FAmstrLimEntrHrFim);
*)
end;

procedure TFmComandaPan.SbClienteClick(Sender: TObject);
begin
  PesquisaSimplesTabela('comandacab', 'cab.Cliente', FTextoPesqCLi, FSQL_CLiente, SbCliente);
end;

procedure TFmComandaPan.SbColocacaoClick(Sender: TObject);
begin
(*&�%$#@!"
  DefineIntervaloInt('Coloca��o', 'Colocacao', FSQL_Colocacao,  SbColocacao,
    FColocacaoMin, FColocacaoMax);
*)
end;

procedure TFmComandaPan.SbEstahClick(Sender: TObject);
begin
(*&�%$#@!"
  DefineIntervaloInt('Est�', 'Estah', FSQL_Estah,  SbEstah, FEstahMin, FEstahMax);
*)
end;

procedure TFmComandaPan.SbDtHrRetirdClick(Sender: TObject);
const
  Titulo = 'Per�odo da Entrega/retirada';
  StrSQL = ' AND DtHrRetird ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_DtHrRetird, SbDtHrRetird,
  FDtHrRetirdIni, FDtHrRetirdFim);
end;

procedure TFmComandaPan.SbNO_EmpresaClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwempresas', 'lot.Empresa', FTextoPesqEmpresa,
  FSQL_Empresa, SbNO_Empresa);
*)
end;

procedure TFmComandaPan.SbNO_GraFabMCdClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('grafabmcd', 'lot.VenceMarca', FTextoPesqGraFabMCd,
  FSQL_GraFabMCd, SbNO_GraFabMCd);
*)
end;

procedure TFmComandaPan.SbNO_RepresentnClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwrepresnt', 'lot.Representn', FTextoPesqRepresentn,
  FSQL_Representn, SbNO_Representn);
*)
end;

procedure TFmComandaPan.SbNO_Responsav1Click(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwrespnsvs', 'lot.Responsav1', FTextoPesqResponsav1,
  FSQL_Responsav1, SbNO_Responsav1);
*)
end;

procedure TFmComandaPan.SbNO_Responsav2Click(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwrespnsvs', 'lot.Responsav2', FTextoPesqResponsav2,
  FSQL_Responsav2, SbNO_Responsav2);
*)
end;

procedure TFmComandaPan.SbNO_Responsav3Click(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwrespnsvs', 'lot.Responsav3', FTextoPesqResponsav3,
  FSQL_Responsav3, SbNO_Responsav3);
*)
end;

procedure TFmComandaPan.SbNO_SegmentoClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwsegmento', 'lot.Segmento', FTextoPesqSegmento,
  FSQL_Segmento, SbNO_Segmento);
*)
end;

procedure TFmComandaPan.SbNrProcessoClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesInput('N�mero do processo', 'Texto Parcial (use %%)',
  'lot.NrProcesso', FTextoPesqNrProcesso, FSQL_NrProcesso, SbNrProcesso);
*)
end;

procedure TFmComandaPan.SbPAGOClick(Sender: TObject);
begin
  PesquisaSQLInRadioGroup('Pago', 'Pago:', FPesqPAGO, FTextoPesqPAGO,
  FSQL_PAGO, SbPAGO, ['Sim', 'N�o', 'Ambos'], [
  'AND ValrAPag <= 0', 'AND ValrAPag > 0', '']);
end;

procedure TFmComandaPan.SbPortalClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwportais', 'lot.BWPortal', FTextoPesqPortal, FSQL_Portal, SbPortal);
*)
end;

procedure TFmComandaPan.SbPregaoClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesInput('Pregao', 'Texto Parcial (use %%)', 'cab.Pregao',
    FTextoPesqPregao, FSQL_Pregao, SbPregao);
*)
end;

procedure TFmComandaPan.SbProdutoClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesTabela('bwprodutos', 'its.Produto', FTextoPesqProd, FSQL_Produto, SbProduto);
*)
end;

procedure TFmComandaPan.SbDtHrPedidoClick(Sender: TObject);
const
  Titulo = 'Per�odo dos Pedidos';
  StrSQL = ' AND DtHrPedido ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_DtHrPedido, SbDtHrPedido,
  FDtHrPedidoIni, FDtHrPedidoFim);
end;

procedure TFmComandaPan.SBUFClick(Sender: TObject);
begin
(*&�%$#@!"
  PesquisaSimplesInput('UF', 'Informe a UF', 'cab.UF', FTextoPesqUF, FSQL_UF, SbUF);
*)
end;

procedure TFmComandaPan.SpeedButton1Click(Sender: TObject);
var
  I, Conta: Integer;
begin
  Conta := ContaDoRegistroSel();
  //
  LimpaVariaveisTexto();
  //
  for I := Low(FSbs) to High(FSBs) do
      DefineGlyph(0, FSBs[I]);
  ReopenComandaCab(Conta);
end;

procedure TFmComandaPan.SpeedButton2Click(Sender: TObject);
begin
  //VAR_CADASTRO := Qr.Value;
  //
  if TFmComandaPan(Self).Owner is TApplication then
    Close
  else
  try
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
    VAR_AdvToolBarPagerNovo.Visible := True;
  except
    ; // nada
  end;
end;

procedure TFmComandaPan.SQLEnh1Click(Sender: TObject);
begin
  ShowMessage('Click');
end;

procedure TFmComandaPan.SQLEnh2Click(Sender: TObject);
const
  Titulo = 'Per�odo dos Pedidos';
  StrSQL = ' AND DtHrPedido ';
begin
  DefinePeriodoPesquisa(Titulo, StrSQL, FSQL_DtHrPedido, SbDtHrPedido,
  FDtHrPedidoIni, FDtHrPedidoFim);
end;

procedure TFmComandaPan.TSQLEnh1Clik(Sender: TObject);
begin

end;

{ TDBgrid }

procedure TDBgrid.ColWidthsChanged;
begin
  inherited;
  if Assigned(FColResize) then  FColResize(self);
end;

end.
