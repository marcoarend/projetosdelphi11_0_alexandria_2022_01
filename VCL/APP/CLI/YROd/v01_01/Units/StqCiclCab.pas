unit StqCiclCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Variants, TypInfo,
  UnAppEnums, dmkLabelRotate, UnInternalConsts3, frxClass, frxDBSet;

type
  TFmStqCiclCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    QrStqCiclCab: TMySQLQuery;
    DsStqCiclCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    dmkLabel3: TdmkLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    EdComisPer: TdmkEdit;
    dmkLabel1: TdmkLabel;
    QrStqCiclCabNO_Fornece: TWideStringField;
    QrStqCiclCabCodigo: TIntegerField;
    QrStqCiclCabFornece: TIntegerField;
    QrStqCiclCabNome: TWideStringField;
    QrStqCiclCabTotQtdRem: TFloatField;
    QrStqCiclCabTotValRem: TFloatField;
    QrStqCiclCabTotQtdRet: TFloatField;
    QrStqCiclCabTotValRet: TFloatField;
    QrStqCiclCabTotQtdVen: TFloatField;
    QrStqCiclCabTotValVen: TFloatField;
    QrStqCiclCabTotQtdSdo: TFloatField;
    QrStqCiclCabTotValSdo: TFloatField;
    QrStqCiclCabComisPer: TFloatField;
    QrStqCiclCabComisVal: TFloatField;
    QrStqCiclCabComisPag: TFloatField;
    EdFilial: TdmkEditCB;
    dmkLabel2: TdmkLabel;
    CBFilial: TdmkDBLookupComboBox;
    QrStqCiclCabEmpresa: TIntegerField;
    QrStqCiclCabNO_Empresa: TWideStringField;
    dmkLabel4: TdmkLabel;
    Label2: TLabel;
    dmkLabel5: TdmkLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    QrStqCiclCabFilial: TIntegerField;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    QrMovmnts: TMySQLQuery;
    QrMovmntsCodigo: TIntegerField;
    QrMovmntsMovmnt: TLargeintField;
    QrMovmntsNO_Movmnt: TWideStringField;
    QrMovmntsNome: TWideStringField;
    QrMovmntsAbertura: TDateTimeField;
    QrMovmntsEncerrou: TDateTimeField;
    DsMovmnts: TDataSource;
    QrMovmntsEncerrou_TXT: TWideStringField;
    Panel6: TPanel;
    DBGDados: TDBGrid;
    Splitter1: TSplitter;
    DBGStqMovIts: TDBGrid;
    QrStqMovIts: TMySQLQuery;
    QrStqMovItsNivel1: TIntegerField;
    QrStqMovItsNO_PRD: TWideStringField;
    QrStqMovItsPrdGrupTip: TIntegerField;
    QrStqMovItsUnidMed: TIntegerField;
    QrStqMovItsNO_PGT: TWideStringField;
    QrStqMovItsSIGLA: TWideStringField;
    QrStqMovItsNO_TAM: TWideStringField;
    QrStqMovItsNO_COR: TWideStringField;
    QrStqMovItsGraCorCad: TIntegerField;
    QrStqMovItsGraGruC: TIntegerField;
    QrStqMovItsGraGru1: TIntegerField;
    QrStqMovItsGraTamI: TIntegerField;
    QrStqMovItsDataHora: TDateTimeField;
    QrStqMovItsIDCtrl: TIntegerField;
    QrStqMovItsTipo: TIntegerField;
    QrStqMovItsOriCodi: TIntegerField;
    QrStqMovItsOriCtrl: TIntegerField;
    QrStqMovItsOriCnta: TIntegerField;
    QrStqMovItsEmpresa: TIntegerField;
    QrStqMovItsStqCenCad: TIntegerField;
    QrStqMovItsGraGruX: TIntegerField;
    QrStqMovItsQtde: TFloatField;
    QrStqMovItsAlterWeb: TSmallintField;
    QrStqMovItsAtivo: TSmallintField;
    QrStqMovItsOriPart: TIntegerField;
    QrStqMovItsPecas: TFloatField;
    QrStqMovItsPeso: TFloatField;
    QrStqMovItsAreaM2: TFloatField;
    QrStqMovItsAreaP2: TFloatField;
    QrStqMovItsFatorClas: TFloatField;
    QrStqMovItsQuemUsou: TIntegerField;
    QrStqMovItsRetorno: TSmallintField;
    QrStqMovItsParTipo: TIntegerField;
    QrStqMovItsParCodi: TIntegerField;
    QrStqMovItsDebCtrl: TIntegerField;
    QrStqMovItsSMIMultIns: TIntegerField;
    QrStqMovItsCustoAll: TFloatField;
    QrStqMovItsValorAll: TFloatField;
    QrStqMovItsCustoBuy: TFloatField;
    QrStqMovItsCustoFrt: TFloatField;
    QrStqMovItsPackGGX: TIntegerField;
    QrStqMovItsPackUnMed: TIntegerField;
    QrStqMovItsPackQtde: TFloatField;
    QrStqMovItsTwnCtrl: TIntegerField;
    QrStqMovItsTwnBrth: TIntegerField;
    DsStqMovIts: TDataSource;
    QrSum: TMySQLQuery;
    QrSumQtde: TFloatField;
    QrSumValorAll: TFloatField;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label3: TLabel;
    DBEdit6: TDBEdit;
    Label4: TLabel;
    DBEdit7: TDBEdit;
    GroupBox2: TGroupBox;
    Panel8: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    GroupBox5: TGroupBox;
    Panel11: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    DBEdit16: TDBEdit;
    QrStqCiclCabComisOPN: TFloatField;
    QrPgtCms: TMySQLQuery;
    QrPgtCmsData: TDateField;
    QrPgtCmsVencimento: TDateField;
    QrPgtCmsBanco: TIntegerField;
    QrPgtCmsSEQ: TIntegerField;
    QrPgtCmsFatID: TIntegerField;
    QrPgtCmsContaCorrente: TWideStringField;
    QrPgtCmsDocumento: TFloatField;
    QrPgtCmsDescricao: TWideStringField;
    QrPgtCmsFatParcela: TIntegerField;
    QrPgtCmsFatNum: TFloatField;
    QrPgtCmsNOMECARTEIRA: TWideStringField;
    QrPgtCmsNOMECARTEIRA2: TWideStringField;
    QrPgtCmsBanco1: TIntegerField;
    QrPgtCmsAgencia1: TIntegerField;
    QrPgtCmsConta1: TWideStringField;
    QrPgtCmsTipoDoc: TSmallintField;
    QrPgtCmsControle: TIntegerField;
    QrPgtCmsNOMEFORNECEI: TWideStringField;
    QrPgtCmsCARTEIRATIPO: TIntegerField;
    QrPgtCmsAgencia: TIntegerField;
    QrPgtCmsNO_SIT: TWideStringField;
    QrPgtCmsCompensado_TXT: TWideStringField;
    DsPgtCms: TDataSource;
    Splitter2: TSplitter;
    PMPagto: TPopupMenu;
    IncluiCms1: TMenuItem;
    ExcluiCms1: TMenuItem;
    BtPagto: TBitBtn;
    QrSumCms: TMySQLQuery;
    QrPgtCmsDebito: TFloatField;
    QrSumCmsValor: TFloatField;
    QrPag: TMySQLQuery;
    DBEdit17: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit18: TDBEdit;
    DsPag: TDataSource;
    QrPagOPNValorAll: TFloatField;
    QrPagTotValorAll: TFloatField;
    QrPagPagValorAll: TFloatField;
    EdAnoMes: TdmkEdit;
    LaMes: TLabel;
    QrStqCiclCabAnoMes: TIntegerField;
    dmkLabel6: TdmkLabel;
    Label18: TLabel;
    DBEdit19: TDBEdit;
    QrStqCiclCabAnoMesTxt: TWideStringField;
    frxDsStqCiclCab: TfrxDBDataset;
    frxYRO_CICLO_001: TfrxReport;
    frxDsPag: TfrxDBDataset;
    Label19: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    Label20: TLabel;
    QrRec: TMySQLQuery;
    QrRecCredito: TFloatField;
    QrRecAReceber: TFloatField;
    DsRec: TDataSource;
    frxDsRec: TfrxDBDataset;
    frxDsMovmnts: TfrxDBDataset;
    QrMovmntsStqCiclCab: TIntegerField;
    frxDsStqMovIts: TfrxDBDataset;
    QrStqMovItsValorUni: TFloatField;
    Panel12: TPanel;
    dmkLabelRotate1: TdmkLabelRotate;
    DBGCms: TDBGrid;
    Splitter3: TSplitter;
    Panel13: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    DBGFat: TDBGrid;
    QrPgtVen: TMySQLQuery;
    QrPgtVenData: TDateField;
    QrPgtVenVencimento: TDateField;
    QrPgtVenBanco: TIntegerField;
    QrPgtVenSEQ: TIntegerField;
    QrPgtVenFatID: TIntegerField;
    QrPgtVenContaCorrente: TWideStringField;
    QrPgtVenDocumento: TFloatField;
    QrPgtVenDescricao: TWideStringField;
    QrPgtVenFatParcela: TIntegerField;
    QrPgtVenFatNum: TFloatField;
    QrPgtVenNOMECARTEIRA: TWideStringField;
    QrPgtVenNOMECARTEIRA2: TWideStringField;
    QrPgtVenBanco1: TIntegerField;
    QrPgtVenAgencia1: TIntegerField;
    QrPgtVenConta1: TWideStringField;
    QrPgtVenTipoDoc: TSmallintField;
    QrPgtVenControle: TIntegerField;
    QrPgtVenNOMEFORNECEI: TWideStringField;
    QrPgtVenCARTEIRATIPO: TIntegerField;
    QrPgtVenAgencia: TIntegerField;
    QrPgtVenCredito: TFloatField;
    QrPgtVenNO_SIT: TWideStringField;
    QrPgtVenCompensado_TXT: TWideStringField;
    DsPgtVen: TDataSource;
    QrGC: TMySQLQuery;
    frxDsPgtVen: TfrxDBDataset;
    frxDsPgtCms: TfrxDBDataset;
    N1: TMenuItem;
    CriaRemessa1: TMenuItem;
    CriaRetorno1: TMenuItem;
    GeraVenda1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrStqCiclCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrStqCiclCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrStqCiclCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrStqCiclCabBeforeClose(DataSet: TDataSet);
    procedure QrMovmntsAfterScroll(DataSet: TDataSet);
    procedure QrPgtCmsCalcFields(DataSet: TDataSet);
    procedure BtPagtoClick(Sender: TObject);
    procedure IncluiCms1Click(Sender: TObject);
    procedure ExcluiCms1Click(Sender: TObject);
    procedure PMPagtoPopup(Sender: TObject);
    procedure DBGDadosDblClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxYRO_CICLO_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrRecCalcFields(DataSet: TDataSet);
    procedure CriaRemessa1Click(Sender: TObject);
    procedure CriaRetorno1Click(Sender: TObject);
    procedure GeraVenda1Click(Sender: TObject);
  private
    FTabLctA: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraStqCiclSel(SQLType: TSQLType);
    //procedure ReopenStqCnsgXxCab();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FThisFatID_Cms, FThisFatID_Ven: Integer;
    //
    procedure ReopenMovmnt(Movmnt, Codigo: Integer);
    procedure AtualizaTotais(Codigo: Integer);
  end;

var
  FmStqCiclCab: TFmStqCiclCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF, UnStqPF, UnApp_Jan,
  {$IfNDef NO_FINANCEIRO}ModuleFin, UnPagtos,{$EndIf}
  StqCiclSel, UMySQLDB;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmStqCiclCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmStqCiclCab.MostraStqCiclSel(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if SQLType = stIns then
  begin
    Codigo := QrStqCiclCabCodigo.Value;
    if DBCheck.CriaFm(TFmStqCiclSel, FmStqCiclSel, afmoNegarComAviso) then
    begin
      FmStqCiclSel.ImgTipo.SQLType := SQLType;
      FmStqCiclSel.FQrCab := QrStqCiclCab;
      FmStqCiclSel.FDsCab := DsStqCiclCab;
      FmStqCiclSel.FQrIts := QrMovmnts;
      FmStqCiclSel.FCodigo := Codigo;
      FmStqCiclSel.EdCodigo.ValueVariant := QrStqCiclCabCodigo.Value;
      FmStqCiclSel.EdNome.ValueVariant := QrStqCiclCabNome.Value;
      FmStqCiclSel.FsEmpresa := Geral.FF0(QrStqCiclCabEmpresa.Value);
      FmStqCiclSel.FsFornece := Geral.FF0(QrStqCiclCabFornece.Value);
      //
      FmStqCiclSel.ReopenMovimentos();
      //
      FmStqCiclSel.ShowModal;
      FmStqCiclSel.Destroy;
      //
      LocCod(Codigo, Codigo);
    end;
  end
  else
    Geral.MB_Erro('Somente inclus�o � permitido!');
end;

procedure TFmStqCiclCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrStqCiclCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrStqCiclCab, QrMovmnts);
end;

procedure TFmStqCiclCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrStqCiclCab);
  //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrMovmnts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrMovmnts);
end;

procedure TFmStqCiclCab.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrStqCiclCab.State <> dsInactive) and (QrStqCiclCab.RecordCount > 0);
  Enab2 := (QrPgtCms.State <> dsInactive) and (QrPgtCms.RecordCount > 0);
  //
  IncluiCms1.Enabled := Enab;
  ExcluiCms1.Enabled := Enab and Enab2;
  //
end;

procedure TFmStqCiclCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrStqCiclCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmStqCiclCab.DefParams;
begin
  VAR_GOTOTABELA := 'stqciclcab';
  VAR_GOTOMYSQLTABLE := QrStqCiclCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT CONCAT(SUBSTRING(AnoMes, 5, 2), "/", ');
  VAR_SQLx.Add('SUBSTRING(AnoMes, 1, 4)) AS AnoMesTxt,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa, ');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Fornece, ');
  VAR_SQLx.Add('ent.Filial, (cab.ComisVal - cab.ComisPag) ComisOPN, cab.* ');
  VAR_SQLx.Add('FROM stqciclcab cab');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=cab.Fornece');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmStqCiclCab.DBGDadosDblClick(Sender: TObject);
const
  sProcName = 'TFmStqCiclCab.DGDadosDblClick()';
var
  Codigo, Movmnt, StqCiclCab, Fornece, Empresa: Integer;
begin
  if (QrMovmnts.State <> dsInactive) and
  (QrMovmnts.RecordCount > 0) then
  begin
    Codigo := QrMovmntsMovmnt.Value;
    Movmnt := QrMovmntsMovmnt.Value;
    StqCiclCab := QrStqCiclCabCodigo.Value;
    Fornece := QrStqCiclCabFornece.Value;
    Empresa := QrStqCiclCabEmpresa.Value;
    //
    case TMovmnts(Movmnt) of
      (*0*)movmntsIndef: ; // nada
      (*1*)movmntsCnsgEnvio: App_Jan.MostraFormStqCnsgGoCab(Codigo, StqCiclCab, Fornece, Empresa);
      (*2*)movmntsCnsgRetorno: App_Jan.MostraFormStqCnsgBkCab(Codigo, StqCiclCab, Fornece, Empresa);
      (*3*)movmntsCnsgVenda: App_Jan.MostraFormStqCnsgVeCab(Codigo, StqCiclCab, Fornece, Empresa);
      else
        Geral.MB_Erro('Movimento "' +
        GetEnumName(TypeInfo(TVarType), Integer(VarType(Movmnt))) +
        '" n�o implementado em ' + sProcName);
    end;
  end;
end;

procedure TFmStqCiclCab.ExcluiCms1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados, Codigo: Integer;
begin
  Codigo := QrStqCiclCabCodigo.Value;
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID_Cms, Codigo);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste lan�amento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPgtCms, DBGCms,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  AtualizaTotais(Codigo);
  LocCod(Codigo, Codigo);
  StqPF.ReopenPagtos(QrPgtCms, FTabLctA, Geral.FF0(Codigo), FThisFatID_Cms);
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmStqCiclCab.IncluiCms1Click(Sender: TObject);
var
  Terceiro, Codigo, Empresa, NF, Carteira: Integer;
  Valor: Double;
  Descricao: String;
begin
 {$IfNDef NO_FINANCEIRO}
  Codigo        := QrStqCiclCabCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Terceiro      := QrStqCiclCabFornece.Value;
  Descricao     := 'Comiss�o de venda de Consignado n� ' + Geral.FF0(QrStqCiclCabCodigo.Value);
  Empresa       := QrStqCiclCabEmpresa.Value;
  NF            := 0;//QrStqCiclCabnfe_nNF.Value;
  Carteira      := StqPF.DefineCarteira(Terceiro);
  Valor         := QrStqCiclCabComisOPN.Value;
  //
  UPagtos.Pagto(QrPgtCms, tpDeb, Codigo, Terceiro, FThisFatID_Cms, 0, 0, stIns,
  'Comiss�o de Venda', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0, True, True,
  Carteira, 0, 0, 0, NF, FTabLctA, Descricao);
  //
  AtualizaTotais(Codigo);
  LocCod(Codigo, Codigo);
  //
  StqPF.ReopenPagtos(QrPgtCms, FTabLctA, Geral.FF0(Codigo), FThisFatID_Cms);
 {$Else}
   Geral.MB_Info('ERP sem pagamento de Comiss�o de Venda de Consignado');
 {$EndIf}
end;

procedure TFmStqCiclCab.ItsAltera1Click(Sender: TObject);
begin
  MostraStqCiclSel(stUpd);
end;

procedure TFmStqCiclCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmStqCiclCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmStqCiclCab.CriaRemessa1Click(Sender: TObject);
var
  Codigo, Fornece, Empresa: Integer;
begin
  Codigo := QrStqCiclCabCodigo.Value;
  Fornece := QrStqCiclCabFornece.Value;
  Empresa := QrStqCiclCabEmpresa.Value;
  App_jan.MostraFormStqCnsgGoCab(0, Codigo, Fornece, Empresa);
  AtualizaTotais(Codigo);
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCiclCab.CriaRetorno1Click(Sender: TObject);
var
  Codigo, Fornece, Empresa: Integer;
begin
  Codigo := QrStqCiclCabCodigo.Value;
  Fornece := QrStqCiclCabFornece.Value;
  Empresa := QrStqCiclCabEmpresa.Value;
  App_jan.MostraFormStqCnsgBkCab(0, Codigo, Fornece, Empresa);
  AtualizaTotais(Codigo);
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCiclCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmStqCiclCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovmntAtu, CodigoAtu, MovmntNxt, CodigoNxt: Integer;
  Tabela: String;
begin
  Codigo := QrStqCiclCabCodigo.Value;
  if Geral.MB_Pergunta('Confirma a retirada do item selecionado?') = ID_YES then
  begin
    MovmntAtu := QrMovmntsMovmnt.Value;
    CodigoAtu := QrMovmntsCodigo.Value;
    //
    if QrMovmnts.RecNo = QrMovmnts.RecordCount then
      QrMovmnts.Prior
    else
      QrMovmnts.Next;
    MovmntNxt := QrMovmntsMovmnt.Value;
    CodigoNxt := QrMovmntsCodigo.Value;
    //
    Tabela := AppEnums.MovmntsToTableName(TMovmnts(MovmntAtu));
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE ' + Tabela +
      ' SET StqCiclCab=0 WHERE Codigo=' + Geral.FF0(CodigoAtu));
    AtualizaTotais(Codigo);
    LocCod(Codigo, Codigo);
    //
   QrMovmnts.Locate('Movmnt;Codigo', VarArrayOf([MovmntNxt, CodigoNxt]), []);
 end;
end;

procedure TFmStqCiclCab.ReopenMovmnt(Movmnt, Codigo: Integer);
var
  sCodigo: String;
begin
  sCodigo := Geral.FF0(QrStqCiclCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovmnts, Dmod.MyDB, [
  'SELECT Codigo, 1 Movmnt, "Envio" NO_Movmnt, ',
  'Nome, Abertura, Encerrou, StqCiclCab, ',
  'IF(Encerrou<2, "",',
  'DATE_FORMAT(Encerrou, "%d/%m/%y %H:%i")) Encerrou_TXT ',
  'FROM stqcnsggocab ',
  'WHERE StqCiclCab=' + sCodigo,
  ' ',
  'UNION ',
  ' ',
  'SELECT Codigo, 2 Movmnt, "Retorno" NO_Movmnt, ',
  'Nome, Abertura, Encerrou, StqCiclCab, ',
  'IF(Encerrou<2, "",',
  'DATE_FORMAT(Encerrou, "%d/%m/%y %H:%i")) Encerrou_TXT ',
  'FROM stqcnsgbkcab ',
  'WHERE StqCiclCab=' + sCodigo,
  ' ',
  'UNION ',
  ' ',
  'SELECT Codigo, 3 Movmnt, "Venda" NO_Movmnt, ',
  'Nome, Abertura, Encerrou, StqCiclCab, ',
  'IF(Encerrou<2, "",',
  'DATE_FORMAT(Encerrou, "%d/%m/%y %H:%i")) Encerrou_TXT ',
  'FROM stqcnsgvecab ',
  'WHERE StqCiclCab=' + sCodigo,
  '']);
  //
  QrMovmnts.Locate('Movmnt;Codigo', VarArrayOf([Movmnt, Codigo]), []);
end;


(*
procedure TFmStqCiclCab.ReopenStqCnsgXxCab();
var
  Tabela: String;
begin
  Tabela := AppEnums.MovmntsToTableName(TMovmnts(QrMovmntsMovmnt.Value));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrStqMovIts, Dmod.MyDB, [
  'SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE, ',
  'IF(tsp.Tipo=0, tsp.RazaoSocial, tsp.Nome) NO_TRANSPORTA, ',
  'tpc.Nome NO_TabePrcCab,  ',
  'pem.BalQtdItem, pem.FatSemEstq, cab.*, ei.CodCliInt ',
  'FROM ' + Tabela + ' cab ',
  'LEFT JOIN enticliint ei ON ei.CodEnti=cab.Empresa ',
  'LEFT JOIN entidades fil  ON fil.Codigo=cab.Empresa ',
  'LEFT JOIN paramsemp pem  ON pem.Codigo=cab.Empresa ',
  'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornece ',
  'LEFT JOIN entidades cli ON cli.Codigo=cab.Cliente ',
  'LEFT JOIN entidades tsp ON tsp.Codigo=cab.Transporta ',
  'LEFT JOIN tabeprccab tpc ON tpc.Codigo=cab.TabePrcCab ',
  'WHERE cab.Codigo=' + Geral.FF0(QrMovmntsCodigo.Value),
  '']);
  //
  //Geral.MB_Teste(QrStqMovIts.SQL.Text);
end;
*)

procedure TFmStqCiclCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmStqCiclCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmStqCiclCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmStqCiclCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmStqCiclCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmStqCiclCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqCiclCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrStqCiclCabCodigo.Value;
  Close;
end;

procedure TFmStqCiclCab.ItsInclui1Click(Sender: TObject);
begin
  MostraStqCiclSel(stIns);
end;

procedure TFmStqCiclCab.CabAltera1Click(Sender: TObject);
var
  Filial: Integer;
  Habilita: Boolean;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrStqCiclCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'stqciclcab');
  //
  Filial := DModG.ObtemFilialDeEntidade(QrStqCiclCabEmpresa.Value);
  EdFilial.ValueVariant := Filial;
  CBFilial.KeyValue := Filial;
  //EdAnoMes.ValueVariant := dmkPF.AnoMesToAnoEMes(QrStqCiclCabAnoMes.Value);
  //
  Habilita := QrMovmnts.RecordCount = 0;
  EdFilial.Enabled := Habilita;
  CBFilial.Enabled := Habilita;
  EdFornece.Enabled := Habilita;
  CBFornece.Enabled := Habilita;
end;

procedure TFmStqCiclCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Empresa, Fornece, AnoMes: Integer;
  (*TotQtdRem, TotValRem, TotQtdRet, TotValRet, TotQtdVen, TotValVen, TotQtdSdo,
  TotValSdo, ComisVal, ComisPag,*)
  ComisPer: Double;
  SQLType: TSQLType;
begin
  if MyObjects.FIC(EdFilial.ValueVariant = 0, EdFilial, 'Informe a empresa!') then Exit;
  //
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  Fornece        := EdFornece.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ComisPer       := EdComisPer.ValueVariant;
  (*
  TotQtdRem      := ;
  TotValRem      := ;
  TotQtdRet      := ;
  TotValRet      := ;
  TotQtdVen      := ;
  TotValVen      := ;
  TotQtdSdo      := ;
  TotValSdo      := ;
  ComisVal       := ;
  ComisPag       := ;
  *)
  AnoMes := dmkPF.MesEAnoToMes(EdAnoMes.Text);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Fornece = 0, EdNome, 'Defina o fornecedor!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('stqciclcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqciclcab', False, [
  'Empresa', 'Fornece', 'Nome',
  'ComisPer', 'AnoMes'
  (*'TotQtdRem', 'TotValRem', 'TotQtdRet',
  'TotValRet', 'TotQtdVen', 'TotValVen',
  'TotQtdSdo', 'TotValSdo', 'ComisVal',
  'ComisPag'*)], [
  'Codigo'], [
  Empresa, Fornece, Nome,
  ComisPer, AnoMes
  (*TotQtdRem, TotValRem, TotQtdRet,
  TotValRet, TotQtdVen, TotValVen,
  TotQtdSdo, TotValSdo, ComisVal,
  ComisPag*)], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    if SQLType = stUpd then
      AtualizaTotais(Codigo);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmStqCiclCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'stqciclcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'stqciclcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmStqCiclCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmStqCiclCab.BtPagtoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagto);
end;

procedure TFmStqCiclCab.AtualizaTotais(Codigo: Integer);
var
  TotQtdRem, TotValRem, TotQtdRet, TotValRet, TotQtdVen, TotValVen, TotQtdSdo,
  TotValSdo, ComisPer, ComisVal, ComisPag: Double;
  SQLType: TSQLType;
  //
  procedure ReabreSum(const FatID: Integer; var Qtde, Vall: Double);
  var
    Tabela: String;
  begin
    Tabela := AppEnums.FatIDToTableName(FatID);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [
    'SELECT SUM(smia.Qtde) Qtde, ',
    'SUM(smia.ValorAll) ValorAll ',
    'FROM stqmovitsa smia ',
    'LEFT JOIN ' + Tabela + ' cab ON cab.Codigo=smia.OriCodi ',
    'WHERE smia.Tipo=' + Geral.FF0(FatID),
    'AND cab.StqCiclCab=' + Geral.FF0(Codigo),
    '']);
    Qtde := QrSumQtde.Value;
    Vall := QrSumValorAll.Value;
  end;
  //
begin
  SQLType := stUpd;
  //
  Codigo := QrStqCiclCabCodigo.Value;
  //
  ReabreSum(VAR_FATID_4202, TotQtdRem, TotValRem);
  ReabreSum(VAR_FATID_4204, TotQtdRet, TotValRet);
  ReabreSum(VAR_FATID_4205, TotQtdVen, TotValVen);
  TotQtdSdo := TotQtdRem - (TotQtdRet + TotQtdVen);
  TotValSdo := TotValRem - (TotValRet + TotValVen);
  ComisPer := USQLDB.GetFlu(Dmod.MyDB, 'stqciclcab', 'ComisPer', 'WHERE Codigo=' + Geral.FF0(Codigo));
  ComisVal := Geral.RoundC(TotValVen * ComisPer / 100, 2);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCms, Dmod.MyDB, [
  'SELECT SUM(Debito-Credito) Valor',
  'FROM ' + FTabLctA + ' la',
  'WHERE FatNum=' + Geral.FF0(QrStqCiclCabCodigo.Value),
  'AND FatID=' + Geral.FF0(FThisFatID_Cms),
  'AND ID_Pgto=0 ', // n�o incluir quita��es!
  '']);
  //
  ComisPag := QrSumCmsValor.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqciclcab', False, [
  'TotQtdRem', 'TotValRem', 'TotQtdRet',
  'TotValRet', 'TotQtdVen', 'TotValVen',
  'TotQtdSdo', 'TotValSdo', 'ComisVal',
  'ComisPag'], [
  'Codigo'], [
  TotQtdRem, TotValRem, TotQtdRet,
  TotValRet, TotQtdVen, TotValVen,
  TotQtdSdo, TotValSdo, ComisVal,
  ComisPag], [
  Codigo], True) then
  begin
    //
  end;
end;

procedure TFmStqCiclCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmStqCiclCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FThisFatID_Cms := VAR_FATID_4217;
  FThisFatID_Ven := VAR_FATID_4205;
  //
  GBEdita.Align := alClient;
  //DGDados.Align := alClient;
  DBGStqMovIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBFilial.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdFilial, CBFilial);
end;

procedure TFmStqCiclCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrStqCiclCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCiclCab.SbImprimeClick(Sender: TObject);
begin
  QrStqCiclCab.DisableControls;
  QrPag.DisableControls;
  QrRec.DisableControls;
  QrMovmnts.DisableControls;
  QrStqMovIts.DisableControls;
  QrPgtVen.DisableControls;
  QrPgtCms.DisableControls;
  try
    //
    MyObjects.frxDefineDataSets(frxYRO_CICLO_001, [
      frxDsStqCiclCab,
      frxDsPag,
      frxDsRec,
      frxDsMovmnts,
      frxDsStqMovIts,
      frxDsPgtVen,
      frxDsPgtCms
      ]);
    //
    MyObjects.frxMostra(frxYRO_CICLO_001, 'Ciclo n� ' +
      FormatFloat('000000', QrStqCiclCabCodigo.Value));
  finally
    QrStqCiclCab.EnableControls;
    QrPag.EnableControls;
    QrRec.EnableControls;
    QrMovmnts.EnableControls;
    QrStqMovIts.EnableControls;
    QrPgtVen.EnableControls;
    QrPgtCms.EnableControls;
  end;
end;

procedure TFmStqCiclCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmStqCiclCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrStqCiclCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmStqCiclCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmStqCiclCab.QrMovmntsAfterScroll(DataSet: TDataSet);
var
  FatID: Integer;
begin
  //ReopenStqCnsgXxCab();
  FatID := AppEnums.MovmntsToFatID(TMovmnts(QrMovmntsMovmnt.Value));
  StqPF.ReopenStqMovIts(QrStqMovIts, FatID, QrMovmntsCodigo.Value, (*IDCtrl*)0);
end;

procedure TFmStqCiclCab.QrPgtCmsCalcFields(DataSet: TDataSet);
begin
  QrPgtCmsSEQ.Value := QrPgtCms.RecNo;
end;

procedure TFmStqCiclCab.QrRecCalcFields(DataSet: TDataSet);
begin
  QrRecAreceber.Value := QrStqCiclCabTotValVen.Value - QrRecCredito.Value;
end;

procedure TFmStqCiclCab.QrStqCiclCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmStqCiclCab.QrStqCiclCabAfterScroll(DataSet: TDataSet);
var
  Codigo: Integer;
  Codigos: String;
begin
  Codigo := QrStqCiclCabCodigo.Value;
  ReopenMovmnt(0, 0);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPag, Dmod.MyDB, [
  'SELECT TotValorAll, PagValorAll,',
  'TotValorAll - PagValorAll OPNValorAll',
  'FROM stqcnsgvecab',
  'WHERE StqCiclCab=' + Geral.FF0(Codigo),
  '']);
  //BtItens.Enabled := QrStqCiclCabEncerrou.Value = 0;
  if QrStqCiclCabFilial.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrStqCiclCabFilial.Value)
  else
    FTabLctA := sTabLctErr;
  //
  StqPF.ReopenPagtos(QrPgtCms, FTabLctA, Geral.FF0(Codigo), FThisFatID_Cms);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGC, Dmod.MyDB, [
  'SELECT GROUP_CONCAT(Codigo) Codigos ',
  'FROM stqcnsgvecab',
  'WHERE StqCiclCab=' + Geral.FF0(Codigo),
  '']);
  Codigos := QrGC.Fields[0].AsString;
  if Codigos = EmptyStr then
    Codigos := '-999999999';
  StqPF.ReopenPagtos(QrPgtVen, FTabLctA, Codigos, FThisFatID_Ven);
  UnDmkDAC_PF.AbreMySQLQuery0(QrRec, Dmod.MyDB, [
  'SELECT SUM(Credito) Credito ',
  'FROM ' + FtabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_4205),
  'AND FatNum IN (' + Codigos + ')',
  'AND ID_Pgto=0 ', // n�o incluir quita��es!
  'AND Compensado > 2 ',
  '']);
  BtPagto.Enabled := True;
end;

procedure TFmStqCiclCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrStqCiclCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmStqCiclCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrStqCiclCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'stqciclcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmStqCiclCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCiclCab.frxYRO_CICLO_001GetValue(const VarName: string;
  var Value: Variant);
begin
(*
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text;
*)
end;

procedure TFmStqCiclCab.GeraVenda1Click(Sender: TObject);
var
  Codigo, Fornece, Empresa: Integer;
begin
  Codigo := QrStqCiclCabCodigo.Value;
  Fornece := QrStqCiclCabFornece.Value;
  Empresa := QrStqCiclCabEmpresa.Value;
  App_jan.MostraFormStqCnsgVeCab(0, Codigo, Fornece, Empresa);
  AtualizaTotais(Codigo);
  LocCod(Codigo, Codigo);
end;

procedure TFmStqCiclCab.CabInclui1Click(Sender: TObject);
var
  Agora: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrStqCiclCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'stqciclcab');
  EdFilial.Enabled := True;
  CBFilial.Enabled := True;
  EdFornece.Enabled := True;
  CBFornece.Enabled := True;
  EdAnoMes.ValueVariant := dmkPF.DataToAnoEMes(DMOdG.ObtemAgora());
end;

procedure TFmStqCiclCab.QrStqCiclCabBeforeClose(DataSet: TDataSet);
begin
  BtPagto.Enabled := False;
  QrMovmnts.Close;
  QrPgtCms.Close;
  QrPgtVen.Close;
  QrPag.Close;
  QrRec.Close;
end;

procedure TFmStqCiclCab.QrStqCiclCabBeforeOpen(DataSet: TDataSet);
begin
  QrStqCiclCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

