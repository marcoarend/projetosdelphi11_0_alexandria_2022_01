unit ModAppGraG1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, dmkEdit,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Data.DB, mySQLDbTables, dmkGeral, MyDBCheck, UnDmkEnums, UnMyObjects,
  frxClass, frxDBSet;

type
  TDfModAppGraG1 = class(TForm)
    frxErrGrandz: TfrxReport;
    QrErrGrandz: TmySQLQuery;
    QrErrGrandzNivel1: TIntegerField;
    QrErrGrandzReduzido: TIntegerField;
    QrErrGrandzNO_GG1: TWideStringField;
    QrErrGrandzSigla: TWideStringField;
    QrErrGrandzGrandeza: TFloatField;
    QrErrGrandzxco_Grandeza: TFloatField;
    QrErrGrandzdif_Grandeza: TFloatField;
    frxDsErrGrandz: TfrxDBDataset;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AlteraProdutoTabelaPersonalizada(GraTabApp, Nivel1: Integer;
              Nome: String; QueryNivel: TMySQLQuery; PreCadastra: Boolean = False);
    function  InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem, OriCtrl:
              Integer; GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem,
              IPI_pIPI, IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP:
              Integer; SQLType: TSQLType): Boolean;
    function  InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa, nItem:
              Integer): Boolean;
    function  InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag; Codigo:
              Integer; TabLctA: String): Double;
    function  InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean; SQLType:
              TSQLType; Codigo: Integer): Boolean;
    function  PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
              EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL, EdPesoB:
              TdmkEdit): Boolean;
    function  InsAltUsoConsumoCab(Data, DataE, refNFe: String; UpdCodigo, IQ, CI,
              Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF, Serie,
              NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
              ICMS: Double; SQLType: TSQLType; FatID, FatNum,
              Empresa: Integer): Boolean;
    function  GrandesasInconsistentes(SQL: array of String; DB: TmySQLDataBase):
              Boolean;
  end;

var
  DfModAppGraG1: TDfModAppGraG1;

implementation

uses
  DmkDAC_PF, Module, ModuleGeral, GraGru1, UnUMedi_PF;

{$R *.dfm}

{ TFmModAppGraG1 }
procedure TDfModAppGraG1.AlteraProdutoTabelaPersonalizada(GraTabApp,
  Nivel1: Integer; Nome: String; QueryNivel: TMySQLQuery;
  PreCadastra: Boolean = False);
begin
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoCab(Data, DataE, refNFe: String;
  UpdCodigo, IQ, CI, Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF,
  Serie, NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
  ICMS: Double; SQLType: TSQLType; FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.GrandesasInconsistentes(SQL: array of String; DB:
  TmySQLDataBase): Boolean;
var
  SQL_PeriodoVMI: String;
  Forma: Integer;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrGrandz, DB, SQL);
  if QrErrGrandz.RecordCount > 0 then
  begin
    Geral.MB_Info('QrErrGrandz.SQL.Text:' + sLineBreak + QrErrGrandz.SQL.Text);
    Result := True;
    Forma := MyObjects.SelRadioGroup('Grandezas inesperadas', '', ['Relatório',
    'Grade editável'], 2);
    case Forma of
      0:
      begin
        MyObjects.frxDefineDataSets(frxErrGrandz, [
          DModG.frxDsDono,
          frxDsErrGrandz
        ]);
        MyObjects.frxMostra(DfModAppGraG1.frxErrGrandz, 'Grandezas inesperadas');
      end;
      1: UMedi_PF.MostraFormUnidMedMul(SQL, DB);
    end;
  end;
end;

function TDfModAppGraG1.InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag;
  Codigo: Integer; TabLctA: String): Double;
begin
  Result := 0;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean;
  SQLType: TSQLType; Codigo: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem,
  OriCtrl, GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem, IPI_pIPI,
  IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP: Integer;
  SQLType: TSQLType): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
  EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL,
  EdPesoB: TdmkEdit): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

end.
