unit StqCiclSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO,
  UnAppEnums;

type
  TFmStqCiclSel = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrMovmnts: TMySQLQuery;
    DsMovmnts: TDataSource;
    Panel5: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    QrMovmntsCodigo: TIntegerField;
    QrMovmntsMovmnt: TLargeintField;
    QrMovmntsNO_Movmnt: TWideStringField;
    QrMovmntsNome: TWideStringField;
    QrMovmntsAbertura: TDateTimeField;
    QrMovmntsEncerrou: TDateTimeField;
    DBGMovmnts: TdmkDBGridZTO;
    QrMovmntsEncerrou_TXT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
    FsEmpresa, FsFornece: String;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenMovimentos();
  end;

  var
  FmStqCiclSel: TFmStqCiclSel;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  StqCiclCab;

{$R *.DFM}

procedure TFmStqCiclSel.BtOKClick(Sender: TObject);
  procedure IncluiAtual();
  var
    Tabela: String;
    Codigo: Integer;
  begin
    Tabela := AppEnums.MovmntsToTableName(TMovmnts(QrMovmntsMovmnt.Value));
    //
    Codigo := QrMovmntsCodigo.Value;
    //
    UnDmkDAC_Pf.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'UPDATE ' + Tabela + ' SET ',
    'StqCiclCab=' + Geral.FF0(FCodigo),
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']));
  end;
var
  Corda: String;
  I, N: Integer;
begin
  if DBGMovmnts.SelectedRows.Count > 0 then
  begin
    N := 0;
    with DBGMovmnts.DataSource.DataSet do
    for I := 0 to DBGMovmnts.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBGMovmnts.SelectedRows.Items[I]);
      //
      if QrMovmntsEncerrou.Value < 2 then
        N := N + 1;
    end;
    if N = 0 then
    begin
      try
        with DBGMovmnts.DataSource.DataSet do
        for I := 0 to DBGMovmnts.SelectedRows.Count-1 do
        begin
          GotoBookmark(DBGMovmnts.SelectedRows.Items[I]);
          //
          IncluiAtual();
        end;
      finally
        FmStqCiclCab.AtualizaTotais(FCodigo);
        FQrIts.Close;
        UnDMkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
      end;
      //
      ReopenMovimentos();
    end else
      Geral.MB_Aviso('A��o abortada! Existem ' + Geral.FF0(N) + ' itens n�o encerrados!')
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
end;

procedure TFmStqCiclSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmStqCiclSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmStqCiclSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DBGMovmnts.Align := alClient;
end;

procedure TFmStqCiclSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmStqCiclSel.ReopenMovimentos();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovmnts, Dmod.MyDB, [
  'SELECT Codigo, 1 Movmnt, "Envio" NO_Movmnt, ',
  'Nome, Abertura, Encerrou,',
  'IF(Encerrou<2, "",',
  'DATE_FORMAT(Encerrou, "%d/%m/%y %H:%i")) Encerrou_TXT ',
  'FROM stqcnsggocab ',
  'WHERE Empresa=' + FsEmpresa,
  'AND Fornece=' + FsFornece,
  'AND StqCiclCab=0 ',
  ' ',
  'UNION ',
  ' ',
  'SELECT Codigo, 2 Movmnt, "Retorno" NO_Movmnt, ',
  'Nome, Abertura, Encerrou,',
  'IF(Encerrou<2, "",',
  'DATE_FORMAT(Encerrou, "%d/%m/%y %H:%i")) Encerrou_TXT ',
  'FROM stqcnsgbkcab ',
  'WHERE Empresa=' + FsEmpresa,
  'AND Fornece=' + FsFornece,
  'AND StqCiclCab=0 ',
  ' ',
  'UNION ',
  ' ',
  'SELECT Codigo, 3 Movmnt, "Venda" NO_Movmnt, ',
  'Nome, Abertura, Encerrou,',
  'IF(Encerrou<2, "",',
  'DATE_FORMAT(Encerrou, "%d/%m/%y %H:%i")) Encerrou_TXT ',
  'FROM stqcnsgvecab ',
  'WHERE Empresa=' + FsEmpresa,
  'AND Fornece=' + FsFornece,
  'AND StqCiclCab=0 ',
  '']);
end;

end.
