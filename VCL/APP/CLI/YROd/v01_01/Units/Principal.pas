﻿unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, Vcl.Grids, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Menus, Vcl.Themes,
  // Terceiros
  mySQLDbTables,
  // Dermatek
  dmkPageControl, dmkGeral, UnDmkEnums, UnInternalConsts, UnDmkProcFunc, ZCF2,
  MyListas, MyDBCheck, UnDmkWeb, DmkDAC_PF, dmkImage,
  UnGrl_Vars, Data.DB, Vcl.DBGrids, dmkDBGridZTO, UnALL_Jan,
  Camera;

type
  TFmPrincipal = class(TForm)
    AdvToolBarPagerNovo: TdmkPageControl;
    AdvToolBarPager23: TTabSheet;
    AdvPage3: TTabSheet;
    AdvToolBar7: TPanel;
    AGBMalaDireta: TBitBtn;
    AGBListaEnti: TBitBtn;
    AdvPage1: TTabSheet;
    AdvToolBar20: TPanel;
    AGBNovasVersoes: TBitBtn;
    AGBRevertVersao: TBitBtn;
    AdvToolBar8: TPanel;
    BtVerifiDB: TBitBtn;
    AGBBackup: TBitBtn;
    AdvToolBar9: TPanel;
    AGBOpcoes: TBitBtn;
    AdvToolBar25: TPanel;
    AGBImagem: TBitBtn;
    AGBTema: TBitBtn;
    AdvPage4: TTabSheet;
    AdvToolBar10: TPanel;
    AGBSuporte: TBitBtn;
    AGBSobre: TBitBtn;
    Memo3: TMemo;
    PageControl1: TdmkPageControl;
    TimerPingServer: TTimer;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAvisoA1: TLabel;
    LaAvisoA2: TLabel;
    PB1: TProgressBar;
    TmSuporte: TTimer;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Chamadasrecebidas1: TMenuItem;
    Chamadasatendidas1: TMenuItem;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    TySuporte: TTrayIcon;
    BalloonHint1: TBalloonHint;
    Timer1: TTimer;
    TimerAlphaBlend: TTimer;
    TimerIdle: TTimer;
    PMVerifiDB: TPopupMenu;
    MenuItem20: TMenuItem;
    VerificaTabelasPblicas1: TMenuItem;
    AdvPMMenuCor: TPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvPMImagem: TPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    sPanel5: TPanel;
    SbLogin: TSpeedButton;
    SbAtualizaERP: TSpeedButton;
    SbFavoritos: TSpeedButton;
    SbVerificaDB: TSpeedButton;
    SbBackup: TSpeedButton;
    SbWSuport: TSpeedButton;
    ImgLogo: TdmkImage;
    SbPopupGeral: TSpeedButton;
    SbMinimizaMenu: TSpeedButton;
    LaTopWarn1: TLabel;
    LaTopWarn2: TLabel;
    Panel1: TPanel;
    AGBMatriz: TBitBtn;
    AGBFiliais: TBitBtn;
    BtOpcoesApp: TBitBtn;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    BtStqCnsgGoCab: TBitBtn;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    BtLstInconforme: TBitBtn;
    Panel15: TPanel;
    BtOVcYnsChkCad: TBitBtn;
    Panel16: TPanel;
    Panel17: TPanel;
    BtEntidades: TBitBtn;
    TabSheet1: TTabSheet;
    Panel25: TPanel;
    Panel26: TPanel;
    PMStyles: TPopupMenu;
    BitBtn8: TBitBtn;
    BitBtn7: TBitBtn;
    BtStqCnsgVeCab: TBitBtn;
    Panel29: TPanel;
    Panel30: TPanel;
    BtOVcMobDevCad: TBitBtn;
    Panel33: TPanel;
    BtChmOcoCad: TBitBtn;
    Panel34: TPanel;
    BitBtn3: TBitBtn;
    MePingServer: TMemo;
    CkExibePings: TCheckBox;
    QrLastLog: TMySQLQuery;
    QrLastLogDataHora: TDateTimeField;
    BtOVgIspMotSta: TBitBtn;
    AdvToolBar5: TPanel;
    AdvGlowButton10: TBitBtn;
    AdvGlowButton11: TBitBtn;
    Panel20: TPanel;
    AdvToolBar4: TPanel;
    AdvGlowButton8: TBitBtn;
    AdvGlowButton9: TBitBtn;
    AdvGlowButton12: TBitBtn;
    Panel21: TPanel;
    AdvToolBar11: TPanel;
    AdvGlowButton5: TBitBtn;
    AdvGlowButton32: TBitBtn;
    AdvGlowButton93: TBitBtn;
    Panel18: TPanel;
    AdvToolBar17: TPanel;
    Panel19: TPanel;
    BtComandaCab: TBitBtn;
    TabSheet3: TTabSheet;
    AdvToolBar23: TPanel;
    BtImpressaoDoPlanoDeContas: TBitBtn;
    BtSaldoContas: TBitBtn;
    BtFinancas: TBitBtn;
    Panel22: TPanel;
    AdvToolBar1: TPanel;
    BtGrupos: TBitBtn;
    BtConjuntos: TBitBtn;
    BtPlanos: TBitBtn;
    BtSubGrupos: TBitBtn;
    BtContas: TBitBtn;
    BtNiveisPlanoContas: TBitBtn;
    AGBLinkConcBco: TBitBtn;
    Panel23: TPanel;
    AdvToolBar6: TPanel;
    AGBPediPrzCab: TBitBtn;
    BtCarteiras: TBitBtn;
    Panel24: TPanel;
    Panel27: TPanel;
    AGBFinEncerMes: TBitBtn;
    Panel28: TPanel;
    SbComanda: TSpeedButton;
    SbEntidade: TSpeedButton;
    SbFinancas: TSpeedButton;
    SbComandaPan: TSpeedButton;
    TabSheet4: TTabSheet;
    Panel61: TPanel;
    BitBtn1: TBitBtn;
    Panel62: TPanel;
    Panel31: TPanel;
    BitBtn2: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn4: TBitBtn;
    Panel60: TPanel;
    Panel32: TPanel;
    BtReceitas: TBitBtn;
    Panel35: TPanel;
    BtStqCenCad: TBitBtn;
    BtStqBalcad: TBitBtn;
    BtStqInnCad1: TBitBtn;
    BtGraImpLista: TBitBtn;
    AdvGlowButton34: TBitBtn;
    BtStqCnsgBkCab: TBitBtn;
    BtStqCiclCab: TBitBtn;
    BtGerCiclCab: TBitBtn;
    BtStqInnCad2: TBitBtn;
    BitBtn5: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure TimerPingServerTimer(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure Chamadasrecebidas1Click(Sender: TObject);
    procedure Chamadasatendidas1Click(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure TimerAlphaBlendTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure AGBListaEntiClick(Sender: TObject);
    procedure AGBMalaDiretaClick(Sender: TObject);
    procedure AGBNovasVersoesClick(Sender: TObject);
    procedure AGBRevertVersaoClick(Sender: TObject);
    procedure AGBMatrizClick(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure ATBSuporteClick(Sender: TObject);
    procedure ATBBackupClick(Sender: TObject);
    procedure ATBFavoritosClick(Sender: TObject);
    procedure ATBLogoffClick(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure VerificaTabelasPblicas1Click(Sender: TObject);
    procedure AGBSuporteClick(Sender: TObject);
    procedure AGBSobreClick(Sender: TObject);
    procedure BtVerifiDBClick(Sender: TObject);
    procedure SbVerificaDBClick(Sender: TObject);
    procedure SbBackupClick(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BtOVgIspMotStaClick(Sender: TObject);
    procedure BtOVcMobDevCadClick(Sender: TObject);
    procedure BtOpcoesAppClick(Sender: TObject);
    procedure AGBOpcoesClick(Sender: TObject);
    procedure SbLoginClick(Sender: TObject);
    procedure SbAtualizaERPClick(Sender: TObject);
    procedure SbFavoritosClick(Sender: TObject);
    procedure SbWSuportClick(Sender: TObject);
    procedure SbPopupGeralClick(Sender: TObject);
    procedure AGBBackupClick(Sender: TObject);
    procedure AGBTemaClick(Sender: TObject);
    procedure AGBImagemClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure BtChmOcoCadClick(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton8Click(Sender: TObject);
    procedure AdvGlowButton5Click(Sender: TObject);
    procedure AdvGlowButton32Click(Sender: TObject);
    procedure AdvGlowButton93Click(Sender: TObject);
    procedure BtStqCenCadClick(Sender: TObject);
    procedure BtStqBalCadClick(Sender: TObject);
    procedure BtStqInnCad1Click(Sender: TObject);
    procedure BtComandaCabClick(Sender: TObject);
    procedure BtFinancasClick(Sender: TObject);
    procedure BtSaldoContasClick(Sender: TObject);
    procedure BtImpressaoDoPlanoDeContasClick(Sender: TObject);
    procedure BtPlanosClick(Sender: TObject);
    procedure BtConjuntosClick(Sender: TObject);
    procedure BtGruposClick(Sender: TObject);
    procedure BtSubGruposClick(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure BtNiveisPlanoContasClick(Sender: TObject);
    procedure AGBLinkConcBcoClick(Sender: TObject);
    procedure BtCarteirasClick(Sender: TObject);
    procedure AGBPediPrzCabClick(Sender: TObject);
    procedure AGBFinEncerMesClick(Sender: TObject);
    procedure SbComandaClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure SbFinancasClick(Sender: TObject);
    procedure SbComandaPanClick(Sender: TObject);
    procedure SbMinimizaMenuClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtReceitasClick(Sender: TObject);
    procedure BtGraImpListaClick(Sender: TObject);
    procedure AdvGlowButton34Click(Sender: TObject);
    procedure BtStqCnsgGoCabClick(Sender: TObject);
    procedure BtStqCnsgVeCabClick(Sender: TObject);
    procedure BtStqCnsgBkCabClick(Sender: TObject);
    procedure BtStqCiclCabClick(Sender: TObject);
    procedure BtGerCiclCabClick(Sender: TObject);
  private
    { Private declarations }
    FBorda, FCursorPosX, FCursorPosY: Integer;
    FMenuMaximizado, FALiberar: Boolean;
    FAdvToolBarPager_Hei_Max: Integer;
    //
    procedure ProjectSourceProc();
    //
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure MostraLogoff();
  public
    { Public declarations }
    FLinModErr, FTipoNovoEnti, FEntInt: Integer;
    FLDataIni, FLDataFim: TDateTime;
    //
    FModBloq_EntCliInt, FModBloq_CliInt, FModBloq_Peri, FModBloq_FatID,
    FModBloq_Lancto: Integer;
    FModBloq_TabLctA: String;
    FModBloq_FatNum: Double;

    procedure MyOnHint(Sender: TObject);
    procedure AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid; Codigo:
              Integer; _: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    procedure AcoesIniciaisDoAplicativo();
    procedure DefineVarsCliInt(Empresa: Integer);
    procedure MostraFormDescanso();
    procedure ReCaptionComponentesDeForm(Form: TForm);
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure VerificaUltimoLog();
    procedure RetornoCNAB();
    procedure InfoSeqIni(Msg: String);

  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses
  ModuleGeral, UnMyObjects, Module, UMySQLModule, Feriados,
{$IfNDef NAO_BINA} UnBina_PF, {$EndIf}
  // , CashTabs
  UnEntities, Descanso, UnLic_Dmk, MalaDireta, FavoritosG, About,
  UnEmpresas_Jan,
  YRO_Dmk, UnApp_Jan, UnYRO_Jan,
  (*&¨%$#@!"
  UnChm_Jan,
  *)
  (*
  UnContabil_Jan,
  *)
  // ini temp
  TabePrcCab,
  // end temp
  CfgCadLista, UnGrade_Jan, UnFinanceiroJan, UnPraz_PF, UnRecei_Jan;

{$R *.dfm}

{ TFmPrincipal }


procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid;
  Codigo: Integer; _: Boolean);
begin
  //
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
  Codigo: Integer; Grade: TStringGrid);
begin
//
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  LaTopWarn1.Caption := '';
  LaTopWarn2.Caption := '';
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Criando Module Geral');
      DModG.MyPID_DB_Cria();
      //
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Criando favoritos');
      DModG.CriaFavoritos(AdvToolBarPagerNovo, LaAvisoA2, LaAvisoA1, BtEntidades, FmPrincipal);
      //
  {[***VerSePrecisa***]  Importação de dados de outro sistema - Ver B U G S T R O L
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Criando Module Anterior');
      DmABD_Mod.MyABD_Cria();
}
      //
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Atualizando atrelamentos de contatos');
      DModG.AtualizaEntiConEnt();
      //
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Setando ping ao servidor');
      TimerPingServer.Enabled := VAR_SERVIDOR = 2;
      //
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Verificando feriados futuros');
      UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
      //
  {[***VerSePrecisa***]  Ver B U G S T R O L
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Carregando paletas de cores de listas de status');
      Dmod.PoeEmMemoryCoresStatusAvul();
      Dmod.PoeEmMemoryCoresStatusOS();
      //
      // Deve ser depois da paleta de cores! > Dmod.PoeEmMemoryCoresStatusOS();
      if Dmod.QrOpcoesBugsSWTAgenda.Value = 1 then
      begin
        MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Criando configurando agenda em guia (aba)');
        MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPagerNovo, False, True);
      end;
}
      //
  {[***VerSePrecisa***]  Ver B U G S T R O L
      if DModG.QrCtrlGeralAtualizouPreEmail.AsInteger = 0 then
        DModG.AtualizaPreEmail;
      if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
      begin
        try
          GBAvisos1.Visible := True;
          Entities.AtualizaEntidadesParaEntidade2(PB1, Dmod.MyDB, DModG.AllID_DB);
        finally
          GBAvisos1.Visible := False;
        end;
      end;
}
      //

  {[***VerSePrecisa***]  Renovações de Contratos! - Ver B U G S T R O L
      // Deixar mais para o final!!
      MyObjects.Informa2(LaAvisoA2, LaAvisoA1, True, 'Verificando ações e renovações');
      //DmModOS.VerificaFormulasFilhas(False);
      if DBCheck.CriaFm(TFmAllToRenew, FmAllToRenew, afmoNegarComAviso) then
      begin
        if FmAllToRenew.ItensAbertos() > 0 then
          FmAllToRenew.ShowModal;
        FmAllToRenew.Destroy;
      end;
      //
}
      DefineVarsCliInt(VAR_LIB_EMPRESA_SEL);
      //
  {[***VerSePrecisa***]  Renovações de Contratos! - Ver B U G S T R O L
      RecriaTiposDeProdutoPadrao;
}


  {[***VerSePrecisa***]  Ver B U G S T R O L
      DmodG.ConfiguraIconeAplicativo;
}
      //
(* {***VerSePrecisa***]  Ver B U G S T R O L
{$IfDef UsaWSuport}
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      if DmkWeb.RemoteConnection() then
      begin
        if VerificaNovasVersoes(True) then
          DmkWeb.MostraBalloonHintMenuTopo(ATBVerificaNovaVersao,
          BalloonHint1, 'Há uma nova versão!', 'Clique aqui para atualizar!');
      end;
{$EndIf}
}*)
      VerificaUltimoLog();
    end;
  finally
{$IfDef UsaWSuport}
    TmSuporte.Enabled := True;
{$EndIf}
    MyObjects.Informa2(LaAvisoA2, LaAvisoA1, False,
      Geral.FF0(VAR_LIB_EMPRESA_SEL) + ' - ' + VAR_LIB_EMPRESA_SEL_TXT);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.BtStqInnCad1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqInnCad(0);
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  Grade_Jan.MostraFormPrdGrupTip(0);
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
var
  Nivel1: Integer;
begin
  Nivel1 := 0;
  Grade_Jan.MostraFormGraGruN(Nivel1);
(*
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
*)
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorFam(0);
end;

procedure TFmPrincipal.AdvGlowButton32Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCusPrc(0);
end;

procedure TFmPrincipal.AdvGlowButton34Click(Sender: TObject);
begin
  App_jan.MostraFormTabePrcCab();
end;

procedure TFmPrincipal.BtStqBalCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqBalCad(0);
end;

procedure TFmPrincipal.AdvGlowButton5Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraTamCad(0);
end;

procedure TFmPrincipal.BtStqCenCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCad(0);
end;

procedure TFmPrincipal.BtStqCiclCabClick(Sender: TObject);
begin
 App_jan.MostraFormStqCiclCab(0);
end;

procedure TFmPrincipal.BtStqCnsgBkCabClick(Sender: TObject);
begin
  App_jan.MostraFormStqCnsgBkCab(0, 0, 0, 0);
end;

procedure TFmPrincipal.BtStqCnsgGoCabClick(Sender: TObject);
begin
  App_jan.MostraFormStqCnsgGoCab(0, 0, 0, 0);
end;

procedure TFmPrincipal.BtStqCnsgVeCabClick(Sender: TObject);
begin
  App_jan.MostraFormStqCnsgVeCab(0, 0, 0, 0);
end;

procedure TFmPrincipal.AdvGlowButton8Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorPan(0);
end;

procedure TFmPrincipal.AdvGlowButton93Click(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCorCad(0);
end;

procedure TFmPrincipal.AGBLinkConcBcoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AGBListaEntiClick(Sender: TObject);
begin
  Entities.MostraFormEntidadesImp();
end;

procedure TFmPrincipal.AGBNovasVersoesClick(Sender: TObject);
begin
  VerificaNovasVersoes(False);
end;

procedure TFmPrincipal.AGBOpcoesClick(Sender: TObject);
begin
  App_Jan.MostraFormOpcoesGrl()
end;

procedure TFmPrincipal.AGBPediPrzCabClick(Sender: TObject);
begin
  Praz_PF.MostraFormPediPrzCab1(0);
end;

procedure TFmPrincipal.AGBMatrizClick(Sender: TObject);
begin
  Empresas_Jan.MostraFormMatriz();
end;

procedure TFmPrincipal.AGBRevertVersaoClick(Sender: TObject);
begin
  Lic_Dmk.ReverteVersao('YROd', Handle);
end;

procedure TFmPrincipal.AGBSobreClick(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AGBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AGBTemaClick(Sender: TObject);
begin
  if PMStyles.Items.Count < 2 then
    MyObjects.StylesListRefreshMenu(PMStyles);
  //
  MyObjects.MostraPopUpDeBotao(PMStyles, AGBTema);
end;

procedure TFmPrincipal.BtCarteirasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.BtChmOcoCadClick(Sender: TObject);
begin
(*&¨%$#@!"
  Chm_Jan.MostraFormChmOcoCad();
*)
end;

procedure TFmPrincipal.BtComandaCabClick(Sender: TObject);
begin
  App_Jan.MostraFormComandaCab(0);
end;

procedure TFmPrincipal.BtConjuntosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.BtContasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.BtEntidadesClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmPrincipal.BtFinancasClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := False;
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.BtGerCiclCabClick(Sender: TObject);
begin
  App_jan.MostraFormGerCiclCab(0);
end;

procedure TFmPrincipal.BtGraImpListaClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraImpLista(0,0);
end;

procedure TFmPrincipal.BtGruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.BtImpressaoDoPlanoDeContasClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.BtNiveisPlanoContasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.BtOpcoesAppClick(Sender: TObject);
begin
  App_Jan.MostraFormOpcoesApp()
end;

procedure TFmPrincipal.BtOVcMobDevCadClick(Sender: TObject);
begin
  YRO_Jan.MostraFormOVcMobDevCad();
end;

procedure TFmPrincipal.BtOVgIspMotStaClick(Sender: TObject);
begin
{
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OVgIspMotSta', 60, ncGerlSeq1,
  'Motivos de Desobrigação',
  [], False, Null, [], [], False);
}
end;

procedure TFmPrincipal.BtPlanosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.BtReceitasClick(Sender: TObject);
begin
  Recei_Jan.MostraFormReceiCab(0);
end;

procedure TFmPrincipal.BtSaldoContasClick(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.BtSubGruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.BtVerifiDBClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVerifiDB, BtVerifiDB);
end;

procedure TFmPrincipal.AGBMalaDiretaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.ATBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.ATBSuporteClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;


procedure TFmPrincipal.BitBtn6Click(Sender: TObject);
begin
  //Contabil_Jan.MostraFormCtbBalPCab();
end;

procedure TFmPrincipal.BitBtn7Click(Sender: TObject);
begin
{
  if TempDB.Connected then
    TempDB.Disconnect;
  //
  with TempDB do
  begin
    Host := 'bdhost0118.servidorwebfacil.com';
    Port := 3306;
    DatabaseName := 'dermatek_nayr';
    UserName := 'dermatek_nayr';
    UserPassword := ';+95l?-#dD6R';
    //ConnectOptions := [coCompress];
    ConnectionTimeout := 30;
    //
    Connected := True;
  end;
}
end;

procedure TFmPrincipal.BitBtn8Click(Sender: TObject);
begin
{
  if TempDB.Connected then
    TempDB.Disconnect;
  //
  with TempDB do
  begin
    Host := '52.67.64.57';
    //Port := 3307;
    Port := 3307;
    DatabaseName := '';
    UserName := 'root';
    UserPassword := 'Nayr2019#';
    //ConnectOptions := [coCompress];
    ConnectionTimeout := 30;
    //
    try
      Connected := True;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Não foi poss´vel a conexão!!!' + sLineBreak +
        E.Message)
      end;
    end;
  end;
}
end;

procedure TFmPrincipal.ATBLogoffClick(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.ATBFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    DModG.CriaFavoritos(AdvToolBarPagerNovo, LaAvisoA1, LaAvisoA2, AGBNovasVersoes, FmPrincipal);
  end;
end;

procedure TFmPrincipal.AGBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  Empresas_Jan.MostraFormParamsEmp();
end;

procedure TFmPrincipal.AGBFinEncerMesClick(Sender: TObject);
begin
  FinanceiroJan.MostraLctEncerraMes;
end;

procedure TFmPrincipal.AGBImagemClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(AdvPMImagem, AGBImagem);
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.Chamadasatendidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouB();
{$EndIf}
end;

procedure TFmPrincipal.Chamadasrecebidas1Click(Sender: TObject);
begin
{$IfNDef NAO_BINA}
  Bina_PF.MostraFormBinaLigouA();
{$EndIf}
end;

procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCliIntUni, Dmod.MyDB);
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
{
  //
  {[***VerSePrecisa***]  No B U G S T R O L não precisa!!!
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
}
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Versão difere do arquivo');
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ZZTerminate := True;
  Application.Terminate;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
begin
  ProjectSourceProc();
  //
  VAR_NAO_USA_KEY_LOCAL_MACHINE := True;
  //VAR_PushNotificatios_Memo := MePushNotifications;
  //VAR_USA_MODULO_CRO := True;
  //
  VAR_TemContratoMensalidade_FldCodigo := 'Codigo';
  VAR_TemContratoMensalidade_FldNome := 'Nome';
  VAR_TemContratoMensalidade_TabNome := 'contratos';
  //
  dmkPF.AcoesAntesDeIniciarApp_dmk();
  //
  FBorda := (Width - ClientWidth) div 2;
  //
  VAR_TIPO_TAB_LCT := 1;
{$IfNDef NO_FINANCEIRO}
  VAR_MULTIPLAS_TAB_LCT := True;
{$EndIf}
  //
  GERAL_MODELO_FORM_ENTIDADES := fmcadEntidade2;
  AdvToolBarPagerNovo.ActivePageIndex := 0;
  //
  VAR_TYPE_LOG := ttlFiliLog;
  FEntInt := -1;
  VAR_USA_TAG_BITBTN := True;
  FTipoNovoEnti := 0;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  VAR_KIND_DEPTO := kdOS1;
  VAR_LA_PRINCIPAL1   := LaAvisoA1;
  VAR_LA_PRINCIPAL2   := LaAvisoA2;
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  //
  VAR_CAD_POPUP := PMGeral;
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  //////////////////////////////////////////////////////////////////////////////
  FLDataIni := Date - Geral.ReadAppKey('Dias', Application.Title,
    ktInteger, 60, HKEY_LOCAL_MACHINE);
  FLDataFim := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  Application.OnHint      := MyOnHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
  // Deixar invisível
  AlphaBlendValue := 0;
  AlphaBlend := True;
  //
  PageControl1.Align := alClient;
  Width := 1600;
  Height := 870;
  FAdvToolBarPager_Hei_Max := AdvToolBarPagerNovo.Height; // 225
  //
  //  Descanso
  MostraFormDescanso();
  //  Diário
  FmPrincipal.WindowState := wsMaximized;
  //MyObjects.FormTDICria(TFmAgendaGer, PageControl1, AdvToolBarPagerNovo);
  // dá erro!! vou abrir no AcoesIniciaisDoAplicativo();
  //MyObjects.MaximizaAdvToolBarPager(AdvToolBarPagerNovo, deftfTrue);
  //AdvToolBarPagerNovo.Collaps;
  //
  FModBloq_EntCliInt := 0;
  FModBloq_CliInt    := 0;
  FModBloq_Peri      := 0;
  FModBloq_FatID     := 0;
  FModBloq_Lancto    := 0;
  FModBloq_TabLctA   := '';
  FModBloq_FatNum    := 0;
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  {[***VerSePrecisa***]  Importação de dados de outro sistema - Ver B U G S T R O L
  if VAR_WEB_CONECTADO = 100 then
    DmkWeb.DesconectarUsuarioWEB;
}
end;

procedure TFmPrincipal.InfoSeqIni(Msg: String);
begin
  if VAR_MSG_START then
    FmYRO_Dmk.MeAvisos.Text := Msg + sLineBreak + FmYRO_Dmk.MeAvisos.Text;
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, False);
end;

procedure TFmPrincipal.MenuItem20Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDB(False);
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
//  Erro DModG não criado
end;

procedure TFmPrincipal.MostraLogoff();
begin
  FmPrincipal.Enabled := False;
  //
  FmYRO_Dmk.Show;
  FmYRO_Dmk.BringToFront;
  FmPrincipal.SendToBack;
  FmYRO_Dmk.EdLogin.Text   := '';
  FmYRO_Dmk.EdSenha.Text   := '';
  FmYRO_Dmk.EdEmpresa.Text := '';
  FmYRO_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.ProjectSourceProc;
  function GetStyleCU(Default: String): String;
  begin
    Result := Geral.ReadAppKeyCU('StyleName', Application.Title, ktString, Default);
  end;

(*
  dmkStringField in '..\..\..\..\..\..\..\ProjetosDelphi10_2_2_Tokyo\dmkMyDB\dmkStringField.pas',
  Principal in '..\..\..\..\..\MDL\YRO\v00_00\Units\Principal.pas' {FmPrincipal},
    Project > Options > Delphi Compiler > Debug Configuration - Windows 32 bits platform > Conditional Defines:
    NAO_USA_TEXTOS;SemCashier;SemCotacoes;SemDBLocal;SemGrade;SemNFe_0000;SemPontoFunci;NAO_USA_RECIBO;NO_USE_EMAILDMK;NO_FINANCEIRO;NAO_USA_IMP_CHEQUE;sNFSe;UsaWSuport;NAO_SAC;NAO_BINA;sPraz;s9Dig;sCNTR;SBLQ;SNoti;DELPHI12_UP;

    Project > Options > Delphi Compiler > Debug Configuration - Windows 32 bits platform > Output Directory:
    C:\Executaveis\19.0\YROd\

    Project > Options > Delphi Compiler > Debug Configuration - Windows 32 bits platform > Unit Output Directory:
    C:\Projetos_DCU\YROd\Bases\

*)
begin
(*
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
*)
  TStyleManager.TrySetStyle('Lavender Classico');
  Application.Title := 'YRO Desktop';
  Application.Name  := 'YROd';
  if CO_VERMCW > CO_VERMLA then
    CO_VERSAO := CO_VERMCW
  else
    CO_VERSAO := CO_VERMLA;
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  UnDmkDAC_PF.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // Não usa ainda!
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.SbAtualizaERPClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.SbBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.SbComandaClick(Sender: TObject);
begin
  App_Jan.MostraFormComandaCab(0);
end;

procedure TFmPrincipal.SbComandaPanClick(Sender: TObject);
const
  Codigo = 0;
begin
  App_Jan.MostraFormComandaPan(True, PageControl1, AdvToolBarPagerNovo, Codigo);
end;

procedure TFmPrincipal.SbEntidadeClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmPrincipal.SbFavoritosClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFavoritosG, FmFavoritosG, afmoLiberado) then
  begin
    FmFavoritosG.ShowModal;
    FmFavoritosG.Destroy;
    //
    PageControl1.ActivePageIndex := 0;
    //
    DModG.CriaFavoritos(AdvToolBarPagerNovo, LaAvisoA2, LaAvisoA1, BtEntidades, FmPrincipal);
  end;
end;

procedure TFmPrincipal.SbFinancasClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := False;
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.SbLoginClick(Sender: TObject);
begin
  MostraLogoff();
end;

procedure TFmPrincipal.SbMinimizaMenuClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := not AdvToolBarPagerNovo.Visible;
end;

procedure TFmPrincipal.SbPopupGeralClick(Sender: TObject);
begin
  MyObjects.MostraPopupGeral();
end;

procedure TFmPrincipal.SbVerificaDBClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMVerifiDB, SBVerificaDB);
end;

procedure TFmPrincipal.SbWSuportClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.MyOnHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmYRO_Dmk.Show;
  Enabled := False;
  FmYRO_Dmk.Refresh;
  FmYRO_Dmk.EdSenha.Text := FmYRO_Dmk.EdSenha.Text+'*';
  FmYRO_Dmk.EdSenha.Refresh;
  FmYRO_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
    (* Se precisar mudar caption dos componentes!
    FDmodCriado := True;
    ReCaptionComponentesDeForm(FmPrincipal);
    AdvToolBarPagerNovo.Visible := True;
    *)
    // Tornar visível
    TimerAlphaBlend.Enabled := True;
  except
    Geral.MB_Erro('Impossível criar Modulo de dados');
    Application.Terminate;
    Exit;
  end;
  {[***Desmarcar***]
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Geral.MB_(PChar('Impossível criar Módulo de vendas'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
}
  FmYRO_Dmk.EdSenha.Text := FmYRO_Dmk.EdSenha.Text+'*';
  FmYRO_Dmk.EdSenha.Refresh;
  FmYRO_Dmk.ReloadSkin;
  FmYRO_Dmk.EdLogin.Text := '';
  FmYRO_Dmk.EdLogin.PasswordChar := 'l';
  FmYRO_Dmk.EdSenha.Text := '';
  FmYRO_Dmk.EdSenha.Refresh;
  FmYRO_Dmk.EdLogin.ReadOnly := False;
  FmYRO_Dmk.EdSenha.ReadOnly := False;
  FmYRO_Dmk.EdLogin.SetFocus;
  //FmYRO_Dmk.ReloadSkin;
  FmYRO_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerAlphaBlendTimer(Sender: TObject);
begin
  if AlphaBlendValue < 255 then
    AlphaBlendValue := AlphaBlendValue + 1
  else begin
    TimerAlphaBlend.Enabled := False;
    AlphaBlend := False;
  end;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
var
  Dia: Integer;
begin
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if not VerificaNovasVersoes(True) then
      Application.Terminate;
  end else
    Application.Terminate;
end;

procedure TFmPrincipal.TimerPingServerTimer(Sender: TObject);
var
  Res: Integer;
begin
  Res := Dmod.MyDB.Ping;
  MePingServer.Text := FormatDateTime('hh:nn:ss', Now) + ' Ping = ' + Geral.FF0(Res) + sLineBreak + MePingServer.Text;
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  (*{[***VerSePrecisa***]  Importação de dados de outro sistema - Ver B U G S T R O L
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    ATBSuporte, BalloonHint1);
  {$ENDIF}
}*)
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean): Boolean;
{
var
  Versao: Integer;
begin
}
  {[***VerSePrecisa***]  Importação de dados de outro sistema - Ver B U G S T R O L
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, '[***NomeApp***]',
    '[***NomeApp***]', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, False, ApenasVerifica,
    BalloonHint1);
}
var
  Versao: Integer;
  Arq: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'IROd',
    'YRO Desktop', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, Arq, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasPblicas1Click(Sender: TObject);
begin
  ALL_Jan.MostraFormVerifiDBTerceiros(False);
end;


procedure TFmPrincipal.VerificaUltimoLog();
var
  Agora, Dif: TDateTime;
  Txt: String;
  Dias, Horas, Minutos: Double;
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastLog, Dmod.MyDB, [
  'SELECT MAX(DataHora) DataHora ',
  'FROM ovpimplog ',
  EmptyStr]);
  Agora := DmodG.ObtemAgora();
  //
  if (QrLastLogDataHora.Value > 2) and
  ((Agora - QrLastLogDataHora.Value) > 0.1) and
  ((Agora - QrLastLogDataHora.Value) < 40) then
  begin
    Txt := '';
    Dif := Agora - QrLastLogDataHora.Value;
    if Dif >= 1 then
    begin
      Dias := Trunc(Dif);
      Dif := Dif - Dias;
      if Dias >= 2 then
        Txt := Txt + Geral.FF0(Trunc(Dias)) + ' dias '
      else
        Txt := Txt + Geral.FF0(Trunc(Dias)) + ' dia ';
    end;
    Dif := Dif * 24;
    if Dif >= 1 then
    begin
      Horas := Trunc(Dif);
      Dif := Dif - Horas;
      if Horas >= 2 then
        Txt := Txt + Geral.FF0(Trunc(Horas)) + ' horas e '
      else
        Txt := Txt + Geral.FF0(Trunc(Horas)) + ' hora  e ';
    end;
    Dif := Dif * 60;
    if Dif >= 1 then
    begin
      Minutos := Trunc(Dif);
      Dif := Dif - Minutos;
      if Minutos >= 2 then
        Txt := Txt + Geral.FF0(Trunc(Minutos)) + ' minutos '
      else
        Txt := Txt + Geral.FF0(Trunc(Minutos)) + ' minuto ';
    end;
    Geral.MB_Aviso('ATENÇÃO!' + sLineBreak +
    'A última atualização dos dados alheios foi a: '+ sLineBreak + '-> ' + Txt +
    sLineBreak + 'Em: ' + Geral.FDT(QrLastLogDataHora.Value, 0) + sLineBreak +
    sLineBreak +
    'Solicite reativação do "???YROdSvc???" ao administrador (TI);' + sLineBreak +
    EmptyStr);
  end;
}
end;

{  Tipo de arquivo!?
http://mark0.net/onlinetrid.html
}

{
CREATE USER 'teste'@'%' IDENTIFIED BY '123';
GRANT ALL PRIVILEGES ON *.* TO 'teste'@'%' IDENTIFIED BY '123';

  Connected = True
}

//  THREAD NO ANdroid
// https://stackoverflow.com/questions/58775196/android-and-application-processmessages

{
Set the SQL mode to strict
sql-mode="STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
Mudar SQL_mode
SET sql_mode = '';
SHOW VARIABLES LIKE "%sql_mode%"  >> sql_mode = STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
}


{ MySQL VARIABLE
interactive_timeout
----------------------------------------------------|
| Property               |  Value                   |
|---------------------------------------------------|
| Command-Line Format    |  --interactive-timeout=# |
| System Variable        |  interactive_timeout     |
| Scope                  |  Global, Session         |
| Dynamic                |  Yes                     |
| SET_VAR Hint Applies   |  No                      |
| Type                   |  Integer                 |
| Default Value          |  28800                   |
| Minimum Value          |  1                       |
----------------------------------------------------|
The number of seconds the server waits for activity on an interactive connection before closing it. An interactive client is defined as a client that uses the CLIENT_INTERACTIVE option to mysql_real_connect(). See also wait_timeout.


connect_timeout


Property
Value
Command-Line Format
--connect-timeout=#
System Variable
connect_timeout
Scope
Global
Dynamic
Yes
SET_VAR Hint Applies
No
Type
Integer
Default Value
10
Minimum Value
2
Maximum Value
31536000
The number of seconds that the mysqld server waits for a connect packet before responding with Bad handshake. The default value is 10 seconds.
Increasing the connect_timeout value might help if clients frequently encounter errors of the form Lost connection to MySQL server at 'XXX', system error: errno.
}

//https://sourceforge.net/projects/gr32pnglibrary/

(*
object MePushNotifications: TMemo
  Left = 128
  Top = 4
  Width = 361
  Height = 177
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Lucida Console'
  Font.Style = []
  Lines.Strings = (

      '{"registration_ids":["APA91bEThbHRMMIBYs8qc2Yr7JRR8u3y2qTy1Ej1Tg' +
      '-dGW4qOlHJ7X00Ei_JhZVnrotiRXHe6ay8mXfoDsA6CRTdfWXz37pGkGrpfXt03k' +
      'yz0nuA3FsLxrfC-yDzaKgoQ_L8C2WZvN0p"],"data":{"id":"205807968282"' +
      ',"message":"Acompanhar teste de laborat'#243'rio/","yro_acao_nome":"c' +
      'hmococad","yro_acao_id":"2"}}'

      '{"multicast_id":4062822076439002463,"success":1,"failure":0,"can' +
      'onical_ids":0,"results":[{"message_id":"0:1577818573724404%eef72' +
      '599f9fd7ecd"}]}')
  ParentFont = False
  ReadOnly = True
  TabOrder = 1
  WantReturns = False
  WordWrap = False
end
*)





{
object BtTesteJson: TBitBtn
  Left = 4
  Top = 104
  Width = 113
  Height = 41
  Caption = 'Teste Jason'
  TabOrder = 2
  OnClick = BtTesteJsonClick
end
procedure TFmPrincipal.BtTesteJsonClick(Sender: TObject);
  procedure JsonToMemo_1(JsonStr: String);
  const
    Avisa = True;
  var
    js, lista, Objeto, SubObjeto: TlkJSONobject;
    ws: TlkJSONstring;
    s: String;
    i, j, k: Integer;
    //
   json,item, Subitem:TlkJSONbase;
   Nome, Valor: String;
   //
   jParsed,
   jHead, jBody,
   jId, jMessage, jyro_acao_nome, jyro_acao_id: TlkJSONbase;
   MyArr: TMyJsonBasesArr;
  begin
    Memo2.Lines.Clear;
    js := TlkJSON.ParseText(JsonStr) as TlkJSONobject;
    jParsed:= TlkJSON.ParseText(JsonStr);
    if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, ['registration_ids', 'data'], MyArr) then
      Exit;

    jHead := MyArr[0];
    jBody := MyArr[1];
    //
    Geral.MB_Info(TlkJSON.GenerateText(jHead));
    Geral.MB_Info(TlkJSON.GenerateText(jBody));
    if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, [
    'id', 'message', 'yro_acao_nome', 'yro_acao_id'], MyArr) then
      Exit;
    jId            := MyArr[0];
    jMessage       := MyArr[1];
    jyro_acao_nome := MyArr[2];
    jyro_acao_id   := MyArr[3];
        //
    Geral.MB_Info('yro_acao_nome = ' + jyro_acao_nome.Value + sLineBreak +
    'yro_acao_id = ' + jyro_acao_id.Value);
    //
    js.Free;
  end;
  procedure JsonToMemo_2(JsonStr: String);
  const
    Avisa = True;
  var
    js, lista, Objeto, SubObjeto: TlkJSONobject;
    ws: TlkJSONstring;
    s: String;
    i, j, k: Integer;
    //
   json,item, Subitem:TlkJSONbase;
   Nome, Valor: String;
   //
   jParsed,
   //jHead, jBody,
   jmulticast_id, jsuccess, jfailure, jcanonical_ids, jresults: TlkJSONbase;
   MyArr: TMyJsonBasesArr;
  begin
    Memo2.Lines.Clear;
    js := TlkJSON.ParseText(JsonStr) as TlkJSONobject;
    jParsed:= TlkJSON.ParseText(JsonStr);
    if not MyJSON.SliceExpectedBaseItems(jParsed, Avisa, [
    'multicast_id', 'success', 'failure', 'canonical_ids', 'results'], MyArr) then
      Exit;

    jmulticast_id  := MyArr[0];
    jsuccess       := MyArr[1];
    jfailure       := MyArr[2];
    jcanonical_ids := MyArr[3];
    jresults       := MyArr[4];

    Geral.MB_Info(TlkJSON.GenerateText(jsuccess));
    Geral.MB_Info(TlkJSON.GenerateText(jfailure));
(*
    if not MyJSON.SliceExpectedBaseItems(jBody, Avisa, [
    'id', 'message', 'yro_acao_nome', 'yro_acao_id'], MyArr) then
      Exit;
    jId            := MyArr[0];
    jMessage       := MyArr[1];
    jyro_acao_nome := MyArr[2];
    jyro_acao_id   := MyArr[3];
        //
    Geral.MB_Info('yro_acao_nome = ' + jyro_acao_nome.Value + sLineBreak +
    'yro_acao_id = ' + jyro_acao_id.Value);
    //
*)
    js.Free;
  end;
  procedure Jason3(JsonStr: String);
  var
    jsonObj, jSubObj: TJSONObject;
    jv: TJSONValue;
  begin
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0)
      as TJSONObject;

    //Memo1.Lines.Clear;

    jv := jsonobj.Get('success').JsonValue;
    //jsubObj := jv as TJSONObject;

(*
    jv := jsubObj.Get('message').JsonValue;
    jsubObj := jv as TJSONObject;

    jv := jsubObj.Get('value').JsonValue;
*)

    Geral.MB_Info(jv.Value);
  end;
begin
  //JsonToMemo_2(MePushNotifications.Lines[1]);
  //Jason3(MePushNotifications.Lines[1]);
end;
}

end.
