object FmGerCiclCab: TFmGerCiclCab
  Left = 339
  Top = 185
  Caption = 'YRO-CICLO-003 :: Gerenciamento de Vendedores e Representantes'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 599
        Height = 32
        Caption = 'Gerenciamento de Vendedores e Representantes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 599
        Height = 32
        Caption = 'Gerenciamento de Vendedores e Representantes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 599
        Height = 32
        Caption = 'Gerenciamento de Vendedores e Representantes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 6
      object Label3: TLabel
        Left = 188
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Faturado:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 284
        Top = 4
        Width = 28
        Height = 13
        Caption = 'Pago:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 380
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Aberto:'
        FocusControl = DBEdit3
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 188
        Top = 20
        Width = 92
        Height = 21
        DataField = 'Faturado'
        DataSource = DsSumLct
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 284
        Top = 20
        Width = 92
        Height = 21
        DataField = 'Pago'
        DataSource = DsSumLct
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 380
        Top = 20
        Width = 92
        Height = 21
        DataField = 'Aberto'
        DataSource = DsSumLct
        TabOrder = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DBGStqCiclCab: TdmkDBGridZTO
      Left = 0
      Top = 41
      Width = 1008
      Height = 426
      Align = alClient
      DataSource = DsStqCiclCab
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBGStqCiclCabDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AnoMesTxt'
          Title.Caption = 'M'#234's/ano'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotQtdVen'
          Title.Caption = 'Qtd Ven'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotValVen'
          Title.Caption = 'Val Ven'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Faturado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Aberto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisPer'
          Title.Caption = '% Comiss'#227'o'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisVal'
          Title.Caption = 'Valor comiss.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisPag'
          Title.Caption = 'Comiss. Pago'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisOPN'
          Title.Caption = 'Comiss.Aberta'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotQtdRem'
          Title.Caption = 'Qtd Rem'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotValRem'
          Title.Caption = 'Val Rem'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotQtdRet'
          Title.Caption = 'Qtd Ret'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotValRet'
          Title.Caption = 'Val ret'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotQtdSdo'
          Title.Caption = 'Qtd Sdo'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TotValSdo'
          Title.Caption = 'Val Sdo'
          Width = 72
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object LaFornece: TdmkLabel
        Left = 304
        Top = 0
        Width = 130
        Height = 13
        Caption = 'Vendedor / Representante:'
        UpdType = utYes
        SQLType = stNil
      end
      object SbFornece: TSpeedButton
        Left = 669
        Top = 16
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbForneceClick
      end
      object dmkLabel1: TdmkLabel
        Left = 8
        Top = 0
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label1: TLabel
        Left = 692
        Top = 0
        Width = 134
        Height = 13
        Caption = 'Filtro: (use % como m'#225'scara)'
      end
      object Label18: TLabel
        Left = 847
        Top = 0
        Width = 104
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Periodo de refer'#234'ncia:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 919
        Top = 20
        Width = 6
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EdFornece: TdmkEditCB
        Left = 304
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornece'
        UpdCampo = 'Fornece'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdForneceRedefinido
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 360
        Top = 16
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsFornece
        TabOrder = 1
        dmkEditCB = EdFornece
        QryCampo = 'Fornece'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdFilial: TdmkEditCB
        Left = 8
        Top = 16
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdFilialRedefinido
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 48
        Top = 16
        Width = 253
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DsEmpresas
        TabOrder = 3
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNome: TdmkEdit
        Left = 692
        Top = 16
        Width = 153
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNomeRedefinido
      end
      object EdAnoMesIni: TdmkEdit
        Left = 847
        Top = 16
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        QryCampo = 'AnoMes'
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
        OnRedefinido = EdAnoMesIniRedefinido
      end
      object EdAnoMesFim: TdmkEdit
        Left = 931
        Top = 16
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        QryCampo = 'AnoMes'
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
        OnRedefinido = EdAnoMesFimRedefinido
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 28
    Top = 65523
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT '
      'FROM entidades'
      'WHERE'
      '      Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NO_ENT')
    Left = 136
    Top = 136
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 136
    Top = 188
  end
  object QrStqCiclCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrStqCiclCabBeforeClose
    SQL.Strings = (
      'SELECT CONCAT(SUBSTRING(AnoMes, 5, 2), '#39'/'#39', '
      '  SUBSTRING(AnoMes, 1, 4)) AS AnoMesTxt,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Fornece, '
      'ent.Filial, (cab.ComisVal - cab.ComisPag) ComisOPN, cab.* '
      'FROM stqciclcab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornece')
    Left = 56
    Top = 137
    object QrStqCiclCabNO_Fornece: TWideStringField
      FieldName = 'NO_Fornece'
      Size = 100
    end
    object QrStqCiclCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCiclCabFornece: TIntegerField
      FieldName = 'Fornece'
      Required = True
    end
    object QrStqCiclCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrStqCiclCabTotQtdRem: TFloatField
      FieldName = 'TotQtdRem'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqCiclCabTotValRem: TFloatField
      FieldName = 'TotValRem'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabTotQtdRet: TFloatField
      FieldName = 'TotQtdRet'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqCiclCabTotValRet: TFloatField
      FieldName = 'TotValRet'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabTotQtdVen: TFloatField
      FieldName = 'TotQtdVen'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqCiclCabTotValVen: TFloatField
      FieldName = 'TotValVen'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabTotQtdSdo: TFloatField
      FieldName = 'TotQtdSdo'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrStqCiclCabTotValSdo: TFloatField
      FieldName = 'TotValSdo'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabComisPer: TFloatField
      FieldName = 'ComisPer'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrStqCiclCabComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabComisPag: TFloatField
      FieldName = 'ComisPag'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabComisOPN: TFloatField
      FieldName = 'ComisOPN'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqCiclCabNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 120
    end
    object QrStqCiclCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrStqCiclCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrStqCiclCabAnoMesTxt: TWideStringField
      FieldName = 'AnoMesTxt'
      Size = 10
    end
    object QrStqCiclCabFaturado: TFloatField
      FieldKind = fkLookup
      FieldName = 'Faturado'
      LookupDataSet = QrValrs
      LookupKeyFields = 'StqCiclCab'
      LookupResultField = 'Faturado'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Lookup = True
    end
    object QrStqCiclCabPago: TFloatField
      FieldKind = fkLookup
      FieldName = 'Pago'
      LookupDataSet = QrValrs
      LookupKeyFields = 'StqCiclCab'
      LookupResultField = 'Pago'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Lookup = True
    end
    object QrStqCiclCabAberto: TFloatField
      FieldKind = fkLookup
      FieldName = 'Aberto'
      LookupDataSet = QrValrs
      LookupKeyFields = 'StqCiclCab'
      LookupResultField = 'Aberto'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Lookup = True
    end
  end
  object DsStqCiclCab: TDataSource
    DataSet = QrStqCiclCab
    Left = 56
    Top = 189
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial, ent.CliInt, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE'
      'FROM senhasits sei'
      'LEFT JOIN entidades ent ON ent.Codigo=sei.Empresa'
      'WHERE sei.Numero=:P0'
      'AND ent.Codigo < -10')
    Left = 212
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresasNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrEmpresasCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresasIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresasNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresasTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresasCodMunici: TIntegerField
      FieldName = 'CodMunici'
    end
    object QrEmpresasUF: TIntegerField
      FieldName = 'UF'
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 212
    Top = 188
  end
  object QrSCC: TMySQLQuery
    Database = Dmod.MyDB
    Left = 352
    Top = 136
  end
  object QrValrs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT svc.StqCiclCab, SUM(lct.Credito) Faturado, '
      'SUM(IF(Compensado>0 ,Credito, 0)) Pago,'
      'SUM(lct.Credito) - SUM(IF(Compensado>0 ,Credito, 0)) Aberto'
      'FROM lct0001a lct'
      'LEFT JOIN stqcnsgvecab svc ON svc.Codigo=lct.FatNum'
      'WHERE lct.FatID=4205'
      'AND lct.FatNum IN (1,2)'
      'AND ID_Pgto=0'
      'GROUP BY svc.StqCiclCab')
    Left = 56
    Top = 240
    object QrValrsStqCiclCab: TIntegerField
      FieldName = 'StqCiclCab'
    end
    object QrValrsFaturado: TFloatField
      FieldName = 'Faturado'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrValrsPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrValrsAberto: TFloatField
      FieldName = 'Aberto'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object QrSumLct: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lct.Credito) Faturado,  '
      'SUM(IF(Compensado>0 ,Credito, 0)) Pago, '
      'SUM(lct.Credito) - SUM(IF(Compensado>0 ,Credito, 0)) Aberto '
      'FROM lct0001a lct '
      'LEFT JOIN stqcnsgvecab svc ON svc.Codigo=lct.FatNum '
      'WHERE lct.FatID=4205 '
      'AND lct.FatNum IN (1,2) '
      'AND ID_Pgto=0 ')
    Left = 284
    Top = 136
    object QrSumLctFaturado: TFloatField
      FieldName = 'Faturado'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumLctPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumLctAberto: TFloatField
      FieldName = 'Aberto'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSumLct: TDataSource
    DataSet = QrSumLct
    Left = 284
    Top = 188
  end
  object QrSCV: TMySQLQuery
    Database = Dmod.MyDB
    Left = 412
    Top = 136
  end
end
