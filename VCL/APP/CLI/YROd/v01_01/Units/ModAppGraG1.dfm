object DfModAppGraG1: TDfModAppGraG1
  Left = 0
  Top = 0
  Caption = 'DfModAppGraG1'
  ClientHeight = 347
  ClientWidth = 194
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 120
  TextHeight = 17
  object frxErrGrandz: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 100
    Top = 36
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsErrGrandz
        DataSetName = 'frxDsErrGrandz'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 14.314470000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 56.692940240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          Left = 52.913420000000000000
          Top = 41.574664020000000000
          Width = 302.362136380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'GRANDEZAS INESPERADAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 355.275820000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 461.102660000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gdz GGX')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 566.929500000000000000
          Top = 41.574830000000000000
          Width = 113.385865830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 514.016080000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gdz Sel')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 408.189240000000000000
          Top = 41.574830000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gdz xco')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsErrGrandz
        DataSetName = 'frxDsErrGrandz'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NO_GG1'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrGrandz."NO_GG1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Nivel1'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Reduzido'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."Reduzido"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 461.102660000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Grandeza'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."Grandeza"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385865830000000000
          Height = 15.118110240000000000
          DataField = 'Sigla'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsErrGrandz."Sigla"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 514.016080000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'dif_Grandeza'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."dif_Grandeza"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 408.189240000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'xco_Grandeza'
          DataSet = frxDsErrGrandz
          DataSetName = 'frxDsErrGrandz'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsErrGrandz."xco_Grandeza"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrErrGrandz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ggx.GraGru1 Nivel1, ggx.Controle Reduzido,  '
      'gg1.Nome NO_GG1, med.Sigla, med.Grandeza, '
      'xco.Grandeza xco_Grandeza,  '
      'IF(xco.Grandeza > 0, xco.Grandeza,  '
      '  IF((ggx.GraGruY<2048 OR  '
      '  xco.CouNiv2<>1), 2,  '
      '  IF(xco.CouNiv2=1, 1, -1)))  dif_Grandeza '
      ' '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  '
      'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1  '
      'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2  '
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed '
      
        'WHERE vmi.DataHora BETWEEN "2016-01-01" AND "2016-01-31 23:59:59' +
        '" '
      'AND vmi.GraGruX <> 0 '
      'AND ((IF(xco.Grandeza > 0, xco.Grandeza,  '
      '  IF((ggx.GraGruY<2048 OR  '
      '  xco.CouNiv2<>1), 2,  '
      '  IF(xco.CouNiv2=1, 1, -1))) <> med.Grandeza)) ')
    Left = 100
    Top = 84
    object QrErrGrandzNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrErrGrandzReduzido: TIntegerField
      FieldName = 'Reduzido'
    end
    object QrErrGrandzNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrErrGrandzSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 6
    end
    object QrErrGrandzGrandeza: TFloatField
      FieldName = 'Grandeza'
    end
    object QrErrGrandzxco_Grandeza: TFloatField
      FieldName = 'xco_Grandeza'
    end
    object QrErrGrandzdif_Grandeza: TFloatField
      FieldName = 'dif_Grandeza'
    end
  end
  object frxDsErrGrandz: TfrxDBDataset
    UserName = 'frxDsErrGrandz'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'Reduzido=Reduzido'
      'NO_GG1=NO_GG1'
      'Sigla=Sigla'
      'dif_Grandeza=dif_Grandeza'
      'xco_Grandeza=xco_Grandeza'
      'Grandeza=Grandeza')
    DataSet = QrErrGrandz
    BCDToCurrency = False
    Left = 100
    Top = 132
  end
end
