unit UnAppEnums;

interface

uses  System.SysUtils, System.Types, dmkGeral, UnInternalConsts, TypInfo,
  Variants;

//const

type

  TUndCtrleProd = (ucprdIndefinido=0, ucprdNaoAplic=1, ucprdMinutos=2,
                   ucprdDias=3, ucprdPecas=4);
  TMovmnts = (movmntsIndef=0, movmntsCnsgEnvio=1, movmntsCnsgRetorno=2,
              movmntsCnsgVenda=3);


  TUnAppEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function MovmntsToTableName(Movimento: TMovmnts): String;
    function MovmntsToFatID(Movimento: TMovmnts): Integer;
    function FatIDToTableName(FatID: Integer): String;
  end;
var
  AppEnums: TUnAppEnums;

const
  //
  CO_TXT_ucprdIndefinido                  = 'Indefinido';
  CO_TXT_ucprdNaoAplic                    = 'N�o aplic�vel';
  CO_TXT_ucprdMinutos                     = 'Minutos';
  CO_TXT_ucprdDias                        = 'Dias';
  CO_TXT_ucprdPecas                       = 'Pe�as';
  MaxUndCtrleProd = Integer(High(TUndCtrleProd));
  sUndCtrleProd: array[0..MaxUndCtrleProd] of string = (
    CO_TXT_ucprdIndefinido     , // 0
    CO_TXT_ucprdNaoAplic       , // 1
    CO_TXT_ucprdMinutos        , // 2
    CO_TXT_ucprdDias           ,  // 3
    CO_TXT_ucprdPecas            // 4
  );

  CO_TXT_codhistMercadorias            = 'Venda de Mercadorias';
  CO_TXT_codhistServicos               = 'Venda de Servi�os';
  CO_TXT_codhistFrete                  = 'Frete';
  CO_TXT_codhistOutros                 = 'Outros';

  MaxCodHist = 3;//Integer(High(TManejoLca));
  sCodHistTextos: array[0..MaxCodHist] of string  = (
  CO_TXT_codhistMercadorias            ,
  CO_TXT_codhistServicos               ,
  CO_TXT_codhistFrete                  ,
  CO_TXT_codhistOutros                 //,
  );

  CO_SIGLA_codhistMercadorias            = 'VM';
  CO_SIGLA_codhistServicos               = 'SV';
  CO_SIGLA_codhistFrete                  = 'FR';
  CO_SIGLA_codhistOutros                 = 'OU';

  sCodHistSiglas: array[0..MaxCodHist] of string  = (
  CO_SIGLA_codhistMercadorias  ,
  CO_SIGLA_codhistServicos               ,
  CO_SIGLA_codhistFrete                  ,
  CO_SIGLA_codhistOutros                 //,
  );




implementation


{ TUnAppEnums }

function TUnAppEnums.FatIDToTableName(FatID: Integer): String;
const
  sProcName = 'TUnAppEnums.FatIDToTableName()';
begin
  case FatID of
    VAR_FATID_4202: Result := 'stqcnsggocab';
    VAR_FATID_4204: Result := 'stqcnsgbkcab';
    VAR_FATID_4205: Result := 'stqcnsgvecab';
    else
    begin
      Result := 'stqcnsg??cab';
      Geral.MB_Erro('FatID n�o definido: ' + Geral.FF0(FatID) + sLineBreak +
        sProcName);
    end;
  end;
end;

function TUnAppEnums.MovmntsToFatID(Movimento: TMovmnts): Integer;
const
  sProcName = 'TUnAppEnums.MovmntsToFatID()';
begin
  case Movimento of
    //(*0*)movmntsIndef:
    (*1*)movmntsCnsgEnvio: Result := VAR_FATID_4202;
    (*2*)movmntsCnsgRetorno: Result := VAR_FATID_4204;
    (*3*)movmntsCnsgVenda: Result := VAR_FATID_4205;
    else
    begin
      Result := -999999999;
      Geral.MB_Erro('Movimento n�o definido: ' +
      GetEnumName(TypeInfo(TVarType), Integer(VarType(Movimento))) + sLineBreak + sProcName);
    end;
  end;
end;

function TUnAppEnums.MovmntsToTableName(Movimento: TMovmnts): String;
const
  sProcName = 'TUnAppEnums.MovmntsToString()';
begin
  case Movimento of
    //(*0*)movmntsIndef:
    (*1*)movmntsCnsgEnvio: Result := 'stqcnsggocab';
    (*2*)movmntsCnsgRetorno: Result := 'stqcnsgbkcab';
    (*3*)movmntsCnsgVenda: Result := 'stqcnsgvecab';
    else
    begin
      Result := 'stqcnsg??cab';
      Geral.MB_Erro('Movimento n�o definido: ' +
      GetEnumName(TypeInfo(TVarType), Integer(VarType(Movimento))) + sLineBreak + sProcName);
    end;
  end;
end;

end.
