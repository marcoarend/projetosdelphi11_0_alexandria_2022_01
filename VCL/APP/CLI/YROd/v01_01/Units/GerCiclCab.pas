unit GerCiclCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker, Vcl.Mask;

type
  TFmGerCiclCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel2: TPanel;
    DBGStqCiclCab: TdmkDBGridZTO;
    Panel3: TPanel;
    QrFornece: TMySQLQuery;
    QrForneceCodigo: TIntegerField;
    QrForneceNO_ENT: TWideStringField;
    DsFornece: TDataSource;
    LaFornece: TdmkLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    SbFornece: TSpeedButton;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    Label1: TLabel;
    Label18: TLabel;
    EdAnoMesIni: TdmkEdit;
    EdAnoMesFim: TdmkEdit;
    Label2: TLabel;
    QrStqCiclCab: TMySQLQuery;
    QrStqCiclCabNO_Fornece: TWideStringField;
    QrStqCiclCabCodigo: TIntegerField;
    QrStqCiclCabFornece: TIntegerField;
    QrStqCiclCabNome: TWideStringField;
    QrStqCiclCabTotQtdRem: TFloatField;
    QrStqCiclCabTotValRem: TFloatField;
    QrStqCiclCabTotQtdRet: TFloatField;
    QrStqCiclCabTotValRet: TFloatField;
    QrStqCiclCabTotQtdVen: TFloatField;
    QrStqCiclCabTotValVen: TFloatField;
    QrStqCiclCabTotQtdSdo: TFloatField;
    QrStqCiclCabTotValSdo: TFloatField;
    QrStqCiclCabComisPer: TFloatField;
    QrStqCiclCabComisVal: TFloatField;
    QrStqCiclCabComisPag: TFloatField;
    QrStqCiclCabComisOPN: TFloatField;
    QrStqCiclCabEmpresa: TIntegerField;
    QrStqCiclCabNO_Empresa: TWideStringField;
    QrStqCiclCabFilial: TIntegerField;
    QrStqCiclCabAnoMes: TIntegerField;
    QrStqCiclCabAnoMesTxt: TWideStringField;
    DsStqCiclCab: TDataSource;
    QrEmpresas: TMySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasFilial: TIntegerField;
    QrEmpresasNOMEFILIAL: TWideStringField;
    QrEmpresasCNPJ_CPF: TWideStringField;
    QrEmpresasIE: TWideStringField;
    QrEmpresasNIRE: TWideStringField;
    QrEmpresasTipo: TSmallintField;
    QrEmpresasCodMunici: TIntegerField;
    QrEmpresasUF: TIntegerField;
    DsEmpresas: TDataSource;
    QrSCC: TMySQLQuery;
    QrValrs: TMySQLQuery;
    QrValrsStqCiclCab: TIntegerField;
    QrValrsFaturado: TFloatField;
    QrValrsPago: TFloatField;
    QrValrsAberto: TFloatField;
    QrStqCiclCabFaturado: TFloatField;
    QrStqCiclCabPago: TFloatField;
    QrStqCiclCabAberto: TFloatField;
    QrSumLct: TMySQLQuery;
    QrSumLctFaturado: TFloatField;
    QrSumLctPago: TFloatField;
    QrSumLctAberto: TFloatField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DsSumLct: TDataSource;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    QrSCV: TMySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbForneceClick(Sender: TObject);
    procedure EdFilialRedefinido(Sender: TObject);
    procedure EdForneceRedefinido(Sender: TObject);
    procedure EdNomeRedefinido(Sender: TObject);
    procedure EdAnoMesIniRedefinido(Sender: TObject);
    procedure EdAnoMesFimRedefinido(Sender: TObject);
    procedure DBGStqCiclCabDblClick(Sender: TObject);
    procedure QrStqCiclCabBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReabreDados();
  public
    { Public declarations }
  end;

  var
  FmGerCiclCab: TFmGerCiclCab;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, UMySQLModule, UnDmkProcFunc,
  UnApp_Jan;

{$R *.DFM}

procedure TFmGerCiclCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerCiclCab.DBGStqCiclCabDblClick(Sender: TObject);
begin
  if (QrStqCiclCab.State <> dsInactive) and
  (QrStqCiclCab.RecordCount > 0) then
    App_Jan.MostraFormStqCiclCab(QrStqCiclCabCodigo.Value);
end;

procedure TFmGerCiclCab.EdAnoMesFimRedefinido(Sender: TObject);
begin
  ReabreDados();
end;

procedure TFmGerCiclCab.EdAnoMesIniRedefinido(Sender: TObject);
begin
  ReabreDados();
end;

procedure TFmGerCiclCab.EdFilialRedefinido(Sender: TObject);
begin
  ReabreDados();
end;

procedure TFmGerCiclCab.EdNomeRedefinido(Sender: TObject);
begin
  ReabreDados();
end;

procedure TFmGerCiclCab.EdForneceRedefinido(Sender: TObject);
begin
  ReabreDados();
end;

procedure TFmGerCiclCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerCiclCab.FormCreate(Sender: TObject);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
(*
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBFilial.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdFilial, CBFilial);
*)
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial, '', False, QrEmpresas);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdFilial, CBFilial, QrEmpresas);
  //
  Data := DMOdG.ObtemAgora();
  EdAnoMesFim.ValueVariant := dmkPF.DataToAnoEMes(Data);
  DecodeDate(Data, Ano, Mes, Dia);
  Data := EncodeDate(Ano-1, Mes, 1);
  EdAnoMesIni.ValueVariant := dmkPF.DataToAnoEMes(Data);
end;

procedure TFmGerCiclCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerCiclCab.QrStqCiclCabBeforeClose(DataSet: TDataSet);
begin
  QrValrs.Close;
  QrSumLct.Close;
end;

procedure TFmGerCiclCab.ReabreDados();
var
  Filial, Empresa, Fornece: Integer;
  SQL_Empresa, SQL_Fornece, SQL_AnoMes, SQL_Nome, AMIniTxt, AMFimTxt,
  Nome, SCCs, SCVs: String;
begin
  QrStqCiclCab.Close;
  Filial  := EdFilial.ValueVariant;
  if Filial = 0 then
    Empresa := Filial
  else
    Empresa := DModG.QrEmpresasCodigo.Value;
  Fornece := EdFornece.ValueVariant;
  if (Empresa <> 0) and (Fornece <> 0) then
  begin
    SQL_Empresa := 'WHERE cab.Empresa=' + Geral.FF0(Empresa);
    SQL_Fornece := 'AND cab.Fornece=' + Geral.FF0(Fornece);
    AMIniTxt := Geral.FF0(dmkPF.MesEAnoToMes(EdAnoMesIni.Text));
    AMFimTxt := Geral.FF0(dmkPF.MesEAnoToMes(EdAnoMesFim.Text));
    SQL_AnoMes := 'AND cab.AnoMes BETWEEN ' + AMIniTxt + ' AND ' + AMFimTxt;
    SQL_Nome := '';
    Nome := Trim(EdNome.Text);
    if Nome <> EmptyStr then
    begin
      SQL_Nome := 'AND Nome LIKE "' + EdNome.Text + '"';
    end;

    UnDmkDAC_PF.AbreMySQLQuery0(QrSCC, Dmod.MyDB, [
    'SELECT GROUP_CONCAT(cab.Codigo) Codigos  ',
    'FROM stqciclcab cab ',
    SQL_Empresa,
    SQL_Fornece,
    SQL_AnoMes,
    SQL_Nome,
    '']);
    SCCs := QrSCC.Fields[0].AsString;
    if SCCs = EmptyStr then
      SCCs := '-999999999';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSCV, Dmod.MyDB, [
    'SELECT GROUP_CONCAT(Codigo) Codigos ',
    'FROM stqcnsgvecab',
    'WHERE StqCiclCab IN (' + SCCs + ')',
    '']);
    //Geral.MB_Teste(QrSCV.SQL.Text);
    SCVs := QrSCV.Fields[0].AsString;
    if SCVs = EmptyStr then
      SCVs := '-999999999';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrValrs, Dmod.MyDB, [
    'SELECT svc.StqCiclCab, SUM(lct.Credito) Faturado,  ',
    'SUM(IF(Compensado>0 ,Credito, 0)) Pago, ',
    'SUM(lct.Credito) - SUM(IF(Compensado>0 ,Credito, 0)) Aberto ',
    'FROM lct0001a lct ',
    'LEFT JOIN stqcnsgvecab svc ON svc.Codigo=lct.FatNum ',
    'WHERE lct.FatID=' + Geral.FF0(VAR_FATID_4205),
    'AND lct.FatNum IN (' + SCVs + ') ',
    'AND ID_Pgto=0 ',
    'GROUP BY svc.StqCiclCab ',
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrStqCiclCab, Dmod.MyDB, [
    'SELECT CONCAT(SUBSTRING(AnoMes, 5, 2), "/",  ',
    '  SUBSTRING(AnoMes, 1, 4)) AS AnoMesTxt, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa,  ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Fornece,  ',
    'ent.Filial, (cab.ComisVal - cab.ComisPag) ComisOPN, cab.*  ',
    'FROM stqciclcab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa ',
    'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornece ',
    'LEFT JOIN stqcnsgvecab svc ON svc.StqCiclCab=cab.Codigo ',
    SQL_Empresa,
    SQL_Fornece,
    SQL_AnoMes,
    SQL_Nome,
    'ORDER BY AnoMes DESC, Codigo DESC ',
    '']);
    //Geral.MB_Teste(QrStqCiclCab.SQL.Text);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumLct, Dmod.MyDB, [
    'SELECT SUM(lct.Credito) Faturado,  ',
    'SUM(IF(Compensado>0 ,Credito, 0)) Pago, ',
    'SUM(lct.Credito) - SUM(IF(Compensado>0 ,Credito, 0)) Aberto ',
    'FROM lct0001a lct ',
    'LEFT JOIN stqcnsgvecab svc ON svc.Codigo=lct.FatNum ',
    'WHERE lct.FatID=4205 ',
    'AND lct.FatNum IN (' + SCVs + ') ',
    'AND ID_Pgto=0 ',
    '']);
  end;
end;

procedure TFmGerCiclCab.SbForneceClick(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdFornece.ValueVariant;

  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_CADASTRO);
    EdFornece.SetFocus;
  end;
end;

end.
