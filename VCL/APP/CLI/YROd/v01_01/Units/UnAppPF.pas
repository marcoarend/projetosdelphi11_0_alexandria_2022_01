unit UnAppPF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, SHDocVw,
  UnInternalConsts2, ComCtrls, dmkGeral, AppListas, mySQLDBTables, dmkEdit,
  UnDmkEnums, UnAppEnums, dmkLabel, Mask, dmkRadioGroup, dmkEditCB,
  dmkDBLookupComboBox, Buttons, math;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  AcaoEspecificaDeApp(Tabela: String): Boolean;
    function  ObtemNomeDeIDMovIts(MovID: Integer): String;
    //
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
    //
    procedure CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
    procedure CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
    procedure CadastroServicoNFe(GraGruX: Integer; NewNome: String);
    procedure CadastroSubProduto(GraGruX: Integer; NewNome: String);
    procedure CadastroInsumo(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
    //
    function  ReabreSPEDEFD0200Exportacao(Qry: TmySQLQuery; TabName: String): Boolean;
    procedure VerificaSeExisteIMEC(EdTxt: TdmkEdit; IMEC: Integer);
    // Do V S do Bluederm
    function  ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
    function  CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
    procedure MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX: Integer; Edita:
              Boolean; Qry: TmySQLQuery(*; MultiplosGGX: array of Integer*));
    procedure ConfiguraGGXsDeGGY(GraGruY: Integer; iNiveis1: array of Integer);
    function  GGXCadGetForcaCor(GraGruY: Integer): Boolean;
    function  GGXCadGetForcaTam(GraGruY: Integer): Boolean;
    procedure CorrigeReduzidosDuplicadosDeInsumo(CorrigeGraTamIts: Boolean);
    procedure VerificaCadastroXxArtigoIncompleta();
    procedure VerificaGraGruYsNaoConfig();
    // Fim: Do V S do Bluederm
    function  TextoDeCodHist(CodHist: Integer): String;
    procedure MostraOrigemFat(FatParcRef: Integer);
    procedure MostraPopupDeCadsatroEspecificoDeGraGruXY(Form: TForm;
              Sb: TSpeedButton; PM: TPopupMenu);
    procedure AtualizaEstoqueGraGXPatr(GraGruX, Empresa: Integer; _: Boolean);
    procedure CalculaTotaisComandaIts(Codigo, Controle: Integer);
    procedure CalculaTotaisComandaCab(Codigo: Integer);
    procedure CalculaTotaisComandaFat(Codigo: Integer; Encerra: Boolean; DtHrBxa: String);
    function  TextoDeCodHist_Sigla(CodHist: Integer): String;           //*Toolrent
    // compatibilidade
    procedure MostraOrigemFatGenerico(FatID: Integer; FatNum: Double;
              FatParcela, FatParcRef: Integer);
    procedure LocalizaOrigemDoFatPedCab(FatPedCab: Integer);
    function  ImpedePorMovimentoAberto(): Boolean;

  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UMySqlModule,
  GraGruYIncompleto, StqMovOpn, ModProd;

{ TUnAppPF }

procedure TUnAppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa: Integer;
  _: Boolean);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroInsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroSubProduto(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroServicoNFe(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

function TUnAppPF.AcaoEspecificaDeApp(Tabela: String): Boolean;
begin
  //Compatibilidade
  Result := True;
end;

procedure TUnAppPF.CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

function TUnAppPF.ImpedePorMovimentoAberto(): Boolean;
  procedure ReopenMovOpn();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmProd.QrMovOpn, Dmod.MyDB, [
    'SELECT Codigo, 0.0 Mov_ID, "Balan�o" MovTXT ',
    'FROM stqbalcad ',
    'WHERE Encerrou < 2 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Codigo, 5.0 Mov_ID, "Compra" MovTXT ',
    'FROM stqinncad ',
    'WHERE Encerrou < 2 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Codigo, 4201.0 Mov_ID, "Remessa de consignado" MovTXT ',
    'FROM stqcnsggocab ',
    'WHERE Encerrou < 2 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Codigo, 4203.0 Mov_ID, "Retorno de consignado" MovTXT ',
    'FROM stqcnsgbkcab ',
    'WHERE Encerrou < 2 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Codigo, 4205.0 Mov_ID, "Venda de consignado" MovTXT ',
    'FROM stqcnsgvecab ',
    'WHERE Encerrou < 2 ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Codigo, 99.0 Mov_ID, "Movimento manual" MovTXT ',
    'FROM stqmancad ',
    'WHERE Encerrou < 2 ',
    '']);
  end;
begin
  ReopenMovOpn();
  Result := DmProd.QrMovOpn.RecordCount > 0;
  if Result then
  begin
    if DBCheck.CriaFm(TFmStqMovOpn, FmStqMovOpn, afmoNegarComAviso) then
    begin
      FmStqMovOpn.ShowModal;
      FmStqMovOpn.Destroy;
    end;
    ReopenMovOpn();
    Result := DmProd.QrMovOpn.RecordCount > 0;
  end;
end;

procedure TUnAppPF.LocalizaOrigemDoFatPedCab(FatPedCab: Integer);
var
  Contrato, CtrID: Integer;
begin
{   ver o que fazer!!!
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo, CtrId ',
  'FROM loccconits',
  'WHERE Fatpedcab=' + Geral.FF0(FatPedCab),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    if FmLocCConCab <> nil then
    begin
      if TFmLocCConCab(FmLocCConCab).ImgTipo.SQLType <> stLok then
      begin
        Geral.MB_Aviso(
        'A janela de Gerenciamento de Loca��o n�o est� em modo travado!' +
        'Finalize a Edi��o ou Inclus�o de Loca��o!');
        //
        Exit;
      end;
    end;
    //
    Contrato  := Dmod.QrAux.Fields[0].AsInteger;
    CtrId     := Dmod.QrAux.Fields[1].AsInteger;
    //
    FmPrincipal.AdvToolBarPagerNovo.Visible := False;
    GraL_Jan.MostraFormLocCCon(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, Contrato);
    if VAR_FmLocCConCab <> nil then
      TFmLocCConCab(VAR_FmLocCConCab).ReopenLocCConIts(CtrID);
  end;
}
end;

procedure TUnAppPF.MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX: Integer;
  Edita: Boolean; Qry: TmySQLQuery);
const
  sProcName = 'TUnAppPF.MostraFormXxXxxXxxGraGruY()';
begin
  //case GraGruY of
{
    CO_GraGruY_0512_TXSubPrd: TX_PF.MostraFormTXSubPrd(GraGruX, True);
    CO_GraGruY_0683_TXPSPPro: TX_PF.MostraFormTXPSPPro(GraGruX, True);
    CO_GraGruY_0853_TXPSPEnd: TX_PF.MostraFormTXPSPEnd(GraGruX, True, Qry);
}
    //CO_GraGruY_1024_TXCadNat: TX_Jan.MostraFormTXCadNat(GraGruX, True, Qry);
{
    CO_GraGruY_1195_TXNatPDA: TX_PF.MostraFormTXNatPDA(GraGruX, True);
    CO_GraGruY_1365_TXProCal: TX_PF.MostraFormTXProCal(GraGruX, True);
    CO_GraGruY_1536_TXCouCal: TX_PF.MostraFormTXCouCal(GraGruX, True);
    CO_GraGruY_1621_TXCouDTA: TX_PF.MostraFormTXCouDTA(GraGruX, True);
    CO_GraGruY_1707_TXProCur: TX_PF.MostraFormTXProCur(GraGruX, True);
    CO_GraGruY_1877_TXCouCur: TX_PF.MostraFormTXCouCur(GraGruX, True);
}
    //CO_GraGruY_2048_TXCadInd: TX_Jan.MostraFormTXCadInd(GraGruX, True, Qry);
{
    CO_GraGruY_3072_TXRibCla: TX_PF.MostraFormTXRibCla(GraGruX, True, Qry);
}
    //CO_GraGruY_4096_TXCadInt: TX_Jan.MostraFormTXCadInt(GraGruX, True, Qry);
{
    CO_GraGruY_5120_TXWetEnd: TX_PF.MostraFormTXWetEnd(GraGruX, True, Qry(*, MultiplosGGX*));
}
    //CO_GraGruY_6144_TXCadFcc: TX_Jan.MostraFormTXCadFcc(GraGruX, True, Qry(*, MultiplosGGX*));
    //else
    Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
    ' n�o est� implementado em ' + sProcName);
 // end;
end;

procedure TUnAppPF.MostraOrigemFat(FatParcRef: Integer);
begin
  // Compatibilidade
end;

procedure TUnAppPF.MostraOrigemFatGenerico(FatID: Integer; FatNum: Double;
  FatParcela, FatParcRef: Integer);
begin
    // compatibilidade
end;

procedure TUnAppPF.MostraPopupDeCadsatroEspecificoDeGraGruXY(Form: TForm;
  Sb: TSpeedButton; PM: TPopupMenu);
begin
  // Compatibilidade
end;

function TUnAppPF.ObtemNomeDeIDMovIts(MovID: Integer): String;
begin
  Result := sEstqMovimID_FRENDLY[MovID];
end;

function TUnAppPF.ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
const
  sProcName = 'TUnAppPF.ObtemNomeTabelaGraGruY()';
begin
  case GraGruY of
    -1: Result := '';
{
    CO_GraGruY_0512_TXSubPrd: Result := LowerCase('TXSubPrd');
    CO_GraGruY_0683_TXPSPPro: Result := LowerCase('TXPSPPro');
    CO_GraGruY_0853_TXPSPEnd: Result := LowerCase('TXPSPEnd');
}
    CO_GraGruY_1024_TXCadNat: Result := LowerCase('TXCadNat');
{
    CO_GraGruY_1195_TXNatPDA: Result := LowerCase('TXNatpda');
    CO_GraGruY_1365_TXProCal: Result := LowerCase('TXProCal');
    CO_GraGruY_1536_TXCouCal: Result := LowerCase('TXCouCal');
    CO_GraGruY_1621_TXCouDTA: Result := LowerCase('TXCouDTA');
    CO_GraGruY_1707_TXProCur: Result := LowerCase('TXProCur');
    CO_GraGruY_1877_TXCouCur: Result := LowerCase('TXCouCur');
}
    CO_GraGruY_2048_TXCadInd: Result := LowerCase('TXCadInd');
{
    CO_GraGruY_3072_TXRibCla: Result := LowerCase('TXRibCla');
}
    CO_GraGruY_4096_TXCadInt: Result := LowerCase('TXCadInt');
{
    CO_GraGruY_5120_TXWetEnd: Result := LowerCase('TXWetEnd');
}
    CO_GraGruY_6144_TXCadFcc: Result := LowerCase('TXCadFcc');
    else
    begin
      Result := '';
      Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
      ' n�o est� implementado em ' + sProcName);
    end;
  end;
end;

procedure TUnAppPF.CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CalculaTotaisComandaCab(Codigo: Integer);
var
  QtdeTotal, ValrTotal, BrutTotal, ValrPago, ValrNPag, ValrAPag: Double;
begin
  ///////////////  V E N D A  //////////////////////////////////////////////////
  UnDMkDAC_PF.AbreMySQLQuery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT SUM(BrutItCu) ValrBrut,',
  'SUM(ValrItCu) ValrItem, SUM(QtdeItem) QtdeItem ',
  'FROM comandaits',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  QtdeTotal      := Dmod.QrAux3.FieldByName('QtdeItem').AsFloat;
  ValrTotal      := Dmod.QrAux3.FieldByName('ValrItem').AsFloat;
  BrutTotal      := Dmod.QrAux3.FieldByName('ValrBrut').AsFloat;
  //
  ///////////////  S E R V I � O S  ////////////////////////////////////////////
  UnDMkDAC_PF.AbreMySQLQuery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT SUM(ValrBrut) ValrBrut,',
  'SUM(ValrItem) ValrItem, SUM(QtdeItem) QtdeItem ',
  'FROM comandasvc',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  QtdeTotal      := QtdeTotal + Dmod.QrAux3.FieldByName('QtdeItem').AsFloat;
  ValrTotal      := ValrTotal + Dmod.QrAux3.FieldByName('ValrItem').AsFloat;
  BrutTotal      := BrutTotal + Dmod.QrAux3.FieldByName('ValrBrut').AsFloat;
  //
  ///////////////  T O T A I S  ////////////////////////////////////////////////

  UnDMkDAC_PF.AbreMySQLQuery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT ValrPago, ValrNPag ',
  'FROM comandacab',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  ValrPago       := Dmod.QrAux3.FieldByName('ValrPago').AsFloat;
  ValrNPag       := Dmod.QrAux3.FieldByName('ValrNPag').AsFloat;
  ValrAPag       := ValrTotal - ValrPago;
  if ValrAPag < 0 then
  begin
    // descontar o valor pago a mais do n�o receb�vel
    ValrNPag := ValrAPag;
    ValrAPag := 0;
  end else
  if ValrNPag < 0 then
    ValrNPag := 0;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandacab', False, [
  'QtdeTotal', 'ValrTotal', 'BrutTotal',
  'ValrNPag', 'ValrAPag'], [
  'Codigo'], [
  QtdeTotal, ValrTotal, BrutTotal,
  ValrNPag, ValrAPag], [
  Codigo], True);
  //
end;

procedure TUnAppPF.CalculaTotaisComandaFat(Codigo: Integer; Encerra: Boolean;
  DtHrBxa: String);
var
  ValrPago: Double;
begin
  UnDmkDAC_PF.AbreMySQLQUery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT SUM(fat.ValBruto) ValBruto ',
  'FROM comandafat fat ',
  'WHERE fat.Codigo=' + Geral.FF0(Codigo),
  '']);
  if Dmod.QrAux3.RecordCount > 0 then
    ValrPago := Dmod.QrAux3.FieldByName('ValBruto').AsFloat
  else
    ValrPago := 0;
  //
{
  if Encerra then
  begin
    //DtHrBxa := DtHrFat;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandacab', False, ['ValrPago',
      'DtHrBxa'], ['Codigo'], [ValrPago, DtHrBxa], [Codigo], True);
    //
  end  else
}
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandacab', False, ['ValrPago'
      ], ['Codigo'], [ValrPago], [Codigo], True);
    //
  end;
  //
  CalculaTotaisComandaCab(Codigo);
end;

procedure TUnAppPF.CalculaTotaisComandaIts(Codigo, Controle: Integer);
var
  ValrItCu, BrutItCu, ValrCstm, BrutCstm, ValrBrut, ValrItem: Double;
begin
  UnDMkDAC_PF.AbreMySQLQuery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT SUM(ValrItem) ValrItem,',
  'SUM(ValrBrut) ValrBrut ',
  'FROM comandaits',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  ValrItem := Dmod.QrAux3.FieldByName('ValrItem').AsFloat;
  ValrBrut := Dmod.QrAux3.FieldByName('ValrBrut').AsFloat;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT SUM(ValrBrut) ValrBrut,',
  'SUM(ValrCstm) ValrCstm ',
  'FROM comandacus',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  ValrCstm := Dmod.QrAux3.FieldByName('ValrCstm').AsFloat;
  BrutCstm := Dmod.QrAux3.FieldByName('ValrBrut').AsFloat;
  ValrItCu := ValrCstm + ValrItem;
  BrutItCu := BrutCstm + ValrBrut;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'comandaits', False, [
  'ValrItCu', 'BrutItCu',
  'ValrCstm', 'BrutCstm'
  ], [
  'Controle'], [
  ValrItCu, BrutItCu,
  ValrCstm, BrutCstm
  ], [
  Controle], True) then
    CalculaTotaisComandaCab(Codigo);
end;

procedure TUnAppPF.ConfiguraGGXsDeGGY(GraGruY: Integer;
  iNiveis1: array of Integer);
const
  sProcName = 'TUnAppPF.ConfiguraGGXsDeGGY()';
var
  GraGruX, I: Integer;
  Tabela, sNiveis1: String;
  Qry: TmySQLQuery;
  Continua: Boolean;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    sNiveis1 := MyObjects.CordaDeArrayInt(iNiveis1);
    //if Nivel1 <> 0 then
    if sNiveis1 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      //'WHERE GraGru1=' + Geral.FF0(sNiveis1),
      'WHERE GraGru1 IN (' + sNiveis1 + ')',
      '']);
      Qry.First;
      GraGruX := Qry.FieldByName('Controle').AsInteger;
      Tabela := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
      if Tabela <> '' then
      begin
        //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
        Qry.First;
        while not Qry.Eof do
        begin
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, Tabela, False, [
          ], [
          'GraGruX'], ['Ativo'], [
          ], [
          Qry.FieldByName('Controle').AsInteger], [1], True);
          //
          Qry.Next;
        end;
        case GraGruY of
{
          (*
          CO_GraGruY_0512_TXSubPrd,
          CO_GraGruY_0683_TXPSPPro,
          *)
          CO_GraGruY_0853_TXPSPEnd,
}
          CO_GraGruY_1024_TXCadNat,
{
          (*
          CO_GraGruY_1195_TXNatPDA,
          CO_GraGruY_1365_TXProCal,
          CO_GraGruY_1536_TXCouCal,
          CO_GraGruY_1621_TXCouDTA,
          CO_GraGruY_1707_TXProCur,
          CO_GraGruY_1877_TXCouCur,
}
          CO_GraGruY_2048_TXCadInd,
{
          CO_GraGruY_3072_TXRibCla:
}
          CO_GraGruY_4096_TXCadInt,
          CO_GraGruY_6144_TXCadFcc:
          begin
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
            'GraGruY'], [
            'Controle'], [
            GraGruY], [
            GraGruX], True);
          end;
{
          //CO_GraGruY_5120_TXWetEnd,
          begin
            Grade_PF.CorrigeGraGruYdeGraGruX();
            Continua := True;
          end;
}
          else
          Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
          ' n�o est� implementado em ' + sProcName);
        end;
        if Continua (*and (not JaTemN1)*) then
        begin
          MostraFormXxXxxXxxGraGruY(GraGruY, GraGruX, True, Qry(*, MultiplosGGX*));
        end;
      end;
    end else
      Geral.MB_Erro('N�o foi poss�vel localizar o(s) produto(s) ' + sNiveis1 +
      ' para complementar os dados do seu cadastro!');
  finally
    Qry.Free;
  end;
end;

function TUnAppPF.CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
const
  sProcName = 'TUnAppPF.CorrigeGraGruYdeGraGruX()';
begin
  Result := False;
(*
  case GraGruY of
    CO_GraGruY_0512_TXSubPrd,
    CO_GraGruY_0683_TXPSPPro,
    CO_GraGruY_0853_TXPSPEnd,
    CO_GraGruY_1024_TXNatCad,
    CO_GraGruY_1195_TXNatPDA,
    CO_GraGruY_1365_TXProCal,
    CO_GraGruY_1536_TXCouCal,
    CO_GraGruY_1621_TXCouDTA,
    CO_GraGruY_1707_TXProCur,
    CO_GraGruY_1877_TXCouCur,
    CO_GraGruY_2048_TXRibCad,
    CO_GraGruY_3072_TXRibCla,
    CO_GraGruY_4096_TXRibOpe:
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
      'GraGruY'], [
      'Controle'], [
      GraGruY], [
      GraGruX], True);
    end;
    CO_GraGruY_5120_TXWetEnd,
    CO_GraGruY_6144_TXFinCla:
    begin
      Grade_PF.CorrigeGraGruYdeGraGruX();
      Result := True;
    end;
    else
*)
      Geral.MB_Erro('Grupo de estoque n�o implementado em ' + sProcName);
//  end;
end;

procedure TUnAppPF.CorrigeReduzidosDuplicadosDeInsumo(
  CorrigeGraTamIts: Boolean);
begin
  // Apenas compatibilidade por enquanto...
end;

function TUnAppPF.GGXCadGetForcaCor(GraGruY: Integer): Boolean;
begin
(*
  Result :=
    (GraGruY = CO_GraGruY_6144_TXFinCla)
    or
    (GraGruY = CO_GraGruY_5120_TXWetEnd);
*)
  Result := True;
end;

function TUnAppPF.GGXCadGetForcaTam(GraGruY: Integer): Boolean;
begin
  Result := GGXCadGetForcaCor(GraGruY);
end;

function TUnAppPF.ReabreSPEDEFD0200Exportacao(Qry: TmySQLQuery; TabName: String): Boolean;
begin
  Result := False;
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DmodG.MyPID_DB, [
  'SELECT ggx.Controle, ggx.GraGru1, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, gg1.cGTIN_EAN , gg1.NCM, ',
  'gg1.EX_TIPI, gg1.COD_LST, gg1.SPEDEFD_ALIQ_ICMS, ',
  //'unm.SIGLA, pgt.Tipo_Item ',
  'unm.SIGLA, gg1.prod_CEST, ',
  'IF(cou.CouNiv2 IN (2,3), 5/*subproduto*/, ',
  'IF(gg1.Nivel2 <> 0, gg2.Tipo_Item, pgt.Tipo_Item)) Tipo_Item ',
  //
  'FROM ' + TMeuDB + '.gragrux ggx ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2 ',
  'LEFT JOIN ' + TMeuDB + '.unidmed   unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN ' + TMeuDB + '.prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.couniv2 nv2 ON nv2.Codigo=cou.CouNiv2 ',
  'WHERE ggx.Controle IN ( ',
  '  SELECT DISTINCT GraGruX  ',
  '  FROM ' + TabName,
  ') ',
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
*)
end;

function TUnAppPF.TextoDeCodHist(CodHist: Integer): String;
var
  I, J: Integer;
begin
  Result := '';
  for I := 0 to MaxCodHist do
  begin
    J := Trunc(Power(2, I));
    //
    if Geral.IntInConjunto(J, CodHist) then
      Result := Result + sCodHistSiglas[I] + ' ';
  end;
end;

function TUnAppPF.TextoDeCodHist_Sigla(CodHist: Integer): String;
begin
  // Compatibilidade?????
  Result := '';
end;

procedure TUnAppPF.VerificaCadastroXxArtigoIncompleta();
const
  sProcName = 'TUnAppPF.VerificaCadastroXxArtigoIncompleta()';
{
var
  Qry: TmySQLQuery;
}
begin
  Geral.MB_Info('Ver necessidade de implementar ' + sProcName);
{
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Nome, ggc.GraGruX, ggc.CouNiv1, ',
      'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2, ',
      'ggc.MediaMinKg, ggc.MediaMaxKg  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruxcou ggc ON ggc.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1 co1 ON co1.Codigo=ggc.CouNiv1 ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggx.GraGruY<>0 ',
      'AND ggc.GraGruX IS NULL ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Existem artigos com cadastro incompleto!');
      //
      if DBCheck.CriaFm(TFmGraGruYIncompleto, FmGraGruYIncompleto, afmoNegarComAviso) then
      begin
        FmGraGruYIncompleto.ReopenIncompleto(Qry);
        FmGraGruYIncompleto.ShowModal;
        FmGraGruYIncompleto.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
}
end;

procedure TUnAppPF.VerificaGraGruYsNaoConfig();
const
  sProcName = 'TUnAppPF.VerificaGraGruYsNaoConfig()';
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero, GraTabApp: Integer;
  PerComissF, PerComissR: Double;
  //
  function GeraPrdGrupTip(): Integer;
  begin
    Result := 0;
    Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', stIns, 0);
    CodUsu := Codigo;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
    'CodUsu', 'Nome', 'MadeBy',
    'Fracio', 'Gradeado', 'TipPrd',
    'NivCad', 'FaixaIni', 'FaixaFim',
    'TitNiv1', 'TitNiv2', 'TitNiv3',
    'TitNiv4', 'TitNiv5', 'Customizav',
    'ImpedeCad', 'LstPrcFisc', 'PerComissF',
    'PerComissR', 'Tipo_Item', 'Genero',
    'GraTabApp'], [
    'Codigo'], [
    CodUsu, Nome, MadeBy,
    Fracio, Gradeado, TipPrd,
    NivCad, FaixaIni, FaixaFim,
    TitNiv1, TitNiv2, TitNiv3,
    TitNiv4, TitNiv5, Customizav,
    ImpedeCad, LstPrcFisc, PerComissF,
    PerComissR, Tipo_Item, Genero,
    GraTabApp], [
    Codigo], True) then
      Result := Codigo
  end;
var
  Qry: TmySQLQuery;
  GraGruY, PrdGrupTip: Integer;
  Inclui: Boolean;
begin
////////////////////////////////////////////////////////////////////////////////
   EXIT;  // Deprecado!!! Tentar eliminar campo PrdGrupTip da tabela GraGruY
////////////////////////////////////////////////////////////////////////////////
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM gragruy ',
    'WHERE PrdGrupTip=0 ',
    'AND Codigo <> 0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' Grupos de Estoque sem Grupo de Produto definido!' + sLineBreak +
      'Deseja que o(s) Grupo(s) de Produto sejam criado(s) e atrelados aos respectivos Grupos de Estoque?')
      = ID_YES then
      begin
        TitNiv2        := '';
        TitNiv3        := '';
        TitNiv4        := '';
        TitNiv5        := '';
        NivCad         := 1;
        FaixaIni       := 0;
        FaixaFim       := 0;
        Customizav     := 0;
        ImpedeCad      := 1;
        LstPrcFisc     := 5;
        PerComissF     := 0;
        PerComissR     := 0;
        Tipo_Item      := 0;
        Genero         := 0;
        GraTabApp      := 0;
        //
        Qry.First;
        while not Qry.Eof do
        begin
          GraGruY := Qry.FieldByName('Codigo').AsInteger;
          Inclui  := True;
          case GraGruY of
            {
            CO_GraGruY_0512_TXSubPrd:
            begin
              Nome           := CO_TXT_GraGruY_0512_TXSubPrd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 5;  // subproduto
            end;
            CO_GraGruY_0683_TXPSPPro:
            begin
              Nome           := CO_TXT_GraGruY_0683_TXPSPPro;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_0853_TXPSPEnd:
            begin
              Nome           := CO_TXT_GraGruY_0853_TXPSPEnd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;}
            CO_GraGruY_1024_TXCadNat:
            begin
              Nome           := CO_TXT_GraGruY_1024_TXCadNat;
              MadeBy         := 2;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 2;
              Tipo_Item      := 1; // materia-prima
            end;
            {
            CO_GraGruY_1195_TXNatPDA:
            begin
              Nome           := CO_TXT_GraGruY_1195_TXNatPDA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1365_TXProCal:
            begin
              Nome           := CO_TXT_GraGruY_1365_TXProCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1536_TXCouCal:
            begin
              Nome           := CO_TXT_GraGruY_1536_TXCouCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1621_TXCouDTA:
            begin
              Nome           := CO_TXT_GraGruY_1621_TXCouDTA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1707_TXProCur:
            begin
              Nome           := CO_TXT_GraGruY_1707_TXProCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1877_TXCouCur:
            begin
              Nome           := CO_TXT_GraGruY_1877_TXCouCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;}
            CO_GraGruY_2048_TXCadInd:
            begin
              Nome           := CO_TXT_GraGruY_2048_TXCadInd;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            {
            CO_GraGruY_3072_TXRibCla:
            begin
              Nome           := CO_TXT_GraGruY_3072_TXRibCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;}
            CO_GraGruY_4096_TXCadInt:
            begin
              Nome           := CO_TXT_GraGruY_4096_TXCadInt;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 6; // produto intermedi�rio
            end;
            {CO_GraGruY_5120_TXWetEnd:
            begin
              Nome           := CO_TXT_GraGruY_5120_TXWetEnd;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;}
            CO_GraGruY_6144_TXCadFcc:
            begin
              Nome           := CO_TXT_GraGruY_6144_TXCadFcc;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            else
            begin
              Geral.MB_Erro(
              'GraGruY ' + Geral.FF0(GraGruY) + ' n�o definido em "' + sProcName + '"');
              Inclui := False;
            end;
          end;
          if Inclui then
          begin
            TitNiv1 := Nome;
            PrdGrupTip := GeraPrdGrupTip();
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragruy', False, [
            'PrdGrupTip'], [
            'Codigo'], [
            PrdGrupTip], [
            GraGruY], True);
          end;
          Qry.Next;
        end;
      end;
      Qry.First;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnAppPF.VerificaSeExisteIMEC(EdTxt: TdmkEdit; IMEC: Integer);
var
  Qry: TmySQLQuery;
  Tabela, CampoDt, CampoIdx: String;
  MovimID: TEstqMovimID;
  DataAbertura: TDateTime;
  Codigo, IMEI: Integer;
begin
  EdTxt.Text := 'Falta implementar "TUnApp_Jan.VerificaSeExisteIMEC"';
{
  if IMEC = 0 then
  begin
    EdTxt.Text := '';
    Exit;
  end;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsmovcab ',
    'WHERE Codigo=' + Geral.FF0(IMEC),
    '']);
    if Qry.RecordCount = 0 then
    begin
      EdTxt.Font.Color := clRed;
      EdTxt.Text := 'IME-C n�o localizado';
      Exit;
    end;
    //
    Codigo  := Qry.FieldByName('CodigoID').AsInteger;
    MovimID := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    //
    EdTxt.Text := sEstqMovimID[Integer(MovimID)];
    if MovimID = TEstqMovimID.emidPreReclasse then
    begin
      EdTxt.Font.Color := clPurple;
      //EdTxt.Text := 'Pr� classe/reclasse';
      Exit;
    end
    else
    if MovimID in ([TEstqMovimID.emidClassArtV SUni,TEstqMovimID.emidReclasV SUni]) then
    begin
      case MovimID of
        TEstqMovimID.emidClassArtV SUni: Tabela := 'v spaclacaba';
        TEstqMovimID.emidReclasV SUni  : Tabela := 'v sparclcaba';
        else Tabela := '???';
      end;
      CampoDt := 'DataHora';
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT vsmovits ',
      'FROM ' + Tabela,
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      IMEI := Qry.FieldByName('vsmovits').AsInteger;
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + CampoDt,
      'FROM ' + CO_TAB_TMI,
      'WHERE Controle=' + Geral.FF0(IMEI),
      '']);
      DataAbertura := Qry.FieldByName(CampoDt).AsDateTime;
    end
    else
    begin
      Tabela  := ObtemNomeTabelaV SXxxCab(MovimID);
      CampoDt := ObtemNomeDtAberturaV SXxxCab(MovimID);
      if MovimID = TEstqMovimID.emidMixInsum then
        CampoIdx := 'TXMovCod'
      else
        CampoIdx := 'MovimCod';
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ' + CampoDt,
      'FROM ' + Tabela,
      'WHERE ' + CampoIdx + '=' + Geral.FF0(IMEC),
      '']);
      DataAbertura := Qry.FieldByName(CampoDt).AsDateTime;
    end;
    EdTxt.Font.Color := clBlue;
    EdTxt.Text := 'OK! ID: ' + Geral.FF0(Codigo) + ' Data abertura: ' +
    Geral.FDT(DataAbertura, 2) + ' ' + EdTxt.Text;
  finally
    Qry.Free;
  end;
}
end;

end.
