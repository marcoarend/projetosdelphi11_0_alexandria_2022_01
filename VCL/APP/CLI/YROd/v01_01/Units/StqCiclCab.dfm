object FmStqCiclCab: TFmStqCiclCab
  Left = 368
  Top = 194
  Caption = 'YRO-CICLO-001 :: Gerenciamento de Ciclos'
  ClientHeight = 510
  ClientWidth = 1264
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 52
    Width = 1264
    Height = 458
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 216
    ExplicitTop = 68
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 77
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 368
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object dmkLabel3: TdmkLabel
        Left = 560
        Top = 16
        Width = 119
        Height = 13
        Caption = 'Vendedor/representante:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel1: TdmkLabel
        Left = 856
        Top = 16
        Width = 59
        Height = 13
        Caption = '% Comiss'#227'o:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object LaMes: TLabel
        Left = 927
        Top = 16
        Width = 65
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's refer'#234'nc:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 368
        Top = 32
        Width = 188
        Height = 21
        MaxLength = 30
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFornece: TdmkEditCB
        Left = 560
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'Fornece'
        QryCampo = 'Fornece'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornece
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornece: TdmkDBLookupComboBox
        Left = 621
        Top = 32
        Width = 232
        Height = 21
        DropDownRows = 2
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsFornece
        TabOrder = 5
        dmkEditCB = EdFornece
        QryCampo = 'Fornece'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdComisPer: TdmkEdit
        Left = 856
        Top = 32
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'ComisPer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFilial: TdmkEditCB
        Left = 76
        Top = 33
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 233
        Height = 21
        DropDownRows = 2
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 2
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdAnoMes: TdmkEdit
        Left = 927
        Top = 32
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        QryCampo = 'AnoMes'
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 395
      Width = 1264
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1124
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 52
    Width = 1264
    Height = 458
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 235
      Width = 1264
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 229
      ExplicitWidth = 1008
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1264
      Height = 119
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object dmkLabel4: TdmkLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label2: TLabel
        Left = 368
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object dmkLabel5: TdmkLabel
        Left = 560
        Top = 16
        Width = 119
        Height = 13
        Caption = 'Vendedor/representante:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel6: TdmkLabel
        Left = 856
        Top = 16
        Width = 59
        Height = 13
        Caption = '% Comiss'#227'o:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label18: TLabel
        Left = 927
        Top = 16
        Width = 65
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's refer'#234'nc:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsStqCiclCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 368
        Top = 32
        Width = 189
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsStqCiclCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Filial'
        DataSource = DsStqCiclCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 233
        Height = 21
        DataField = 'NO_Empresa'
        DataSource = DsStqCiclCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 560
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Fornece'
        DataSource = DsStqCiclCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 616
        Top = 32
        Width = 237
        Height = 21
        DataField = 'NO_Fornece'
        DataSource = DsStqCiclCab
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 856
        Top = 32
        Width = 69
        Height = 21
        DataField = 'ComisPer'
        DataSource = DsStqCiclCab
        TabOrder = 6
      end
      object GroupBox1: TGroupBox
        Left = 4
        Top = 56
        Width = 125
        Height = 58
        Caption = ' Totais remessas: '
        TabOrder = 7
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 121
          Height = 41
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 0
            Top = 0
            Width = 43
            Height = 13
            Caption = 'Quantid.:'
            FocusControl = DBEdit6
          end
          object Label4: TLabel
            Left = 50
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit7
          end
          object DBEdit6: TDBEdit
            Left = 0
            Top = 16
            Width = 48
            Height = 21
            DataField = 'TotQtdRem'
            DataSource = DsStqCiclCab
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 50
            Top = 16
            Width = 68
            Height = 21
            DataField = 'TotValRem'
            DataSource = DsStqCiclCab
            TabOrder = 1
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 128
        Top = 56
        Width = 125
        Height = 58
        Caption = ' Totais retornos: '
        TabOrder = 8
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 121
          Height = 41
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 2
            Top = 0
            Width = 43
            Height = 13
            Caption = 'Quantid.:'
            FocusControl = DBEdit8
          end
          object Label6: TLabel
            Left = 52
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit9
          end
          object DBEdit8: TDBEdit
            Left = 2
            Top = 16
            Width = 48
            Height = 21
            DataField = 'TotQtdRet'
            DataSource = DsStqCiclCab
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 52
            Top = 16
            Width = 68
            Height = 21
            DataField = 'TotValRet'
            DataSource = DsStqCiclCab
            TabOrder = 1
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 252
        Top = 56
        Width = 405
        Height = 58
        Caption = ' Totais vendas / faturamento / recebimento: '
        TabOrder = 9
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 401
          Height = 41
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label8: TLabel
            Left = 2
            Top = 0
            Width = 43
            Height = 13
            Caption = 'Quantid.:'
            FocusControl = DBEdit10
          end
          object Label10: TLabel
            Left = 52
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit11
          end
          object Label16: TLabel
            Left = 122
            Top = 0
            Width = 45
            Height = 13
            Caption = 'Faturado:'
            FocusControl = DBEdit17
          end
          object Label17: TLabel
            Left = 192
            Top = 0
            Width = 43
            Height = 13
            Caption = 'A faturar:'
            FocusControl = DBEdit18
          end
          object Label19: TLabel
            Left = 262
            Top = 0
            Width = 49
            Height = 13
            Caption = 'Recebido:'
            FocusControl = DBEdit20
          end
          object Label20: TLabel
            Left = 332
            Top = 0
            Width = 54
            Height = 13
            Caption = 'A Receber:'
            FocusControl = DBEdit21
          end
          object DBEdit10: TDBEdit
            Left = 2
            Top = 16
            Width = 48
            Height = 21
            DataField = 'TotQtdVen'
            DataSource = DsStqCiclCab
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 52
            Top = 16
            Width = 68
            Height = 21
            DataField = 'TotValVen'
            DataSource = DsStqCiclCab
            TabOrder = 1
          end
          object DBEdit17: TDBEdit
            Left = 122
            Top = 16
            Width = 68
            Height = 21
            DataField = 'PagValorAll'
            DataSource = DsPag
            TabOrder = 2
          end
          object DBEdit18: TDBEdit
            Left = 192
            Top = 16
            Width = 68
            Height = 21
            DataField = 'OPNValorAll'
            DataSource = DsPag
            TabOrder = 3
          end
          object DBEdit20: TDBEdit
            Left = 262
            Top = 16
            Width = 68
            Height = 21
            DataField = 'Credito'
            DataSource = DsRec
            TabOrder = 4
          end
          object DBEdit21: TDBEdit
            Left = 332
            Top = 16
            Width = 68
            Height = 21
            DataField = 'AReceber'
            DataSource = DsRec
            TabOrder = 5
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 656
        Top = 56
        Width = 127
        Height = 58
        Caption = 'Saldos deste ciclo: '
        TabOrder = 10
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 123
          Height = 41
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label11: TLabel
            Left = 4
            Top = 0
            Width = 43
            Height = 13
            Caption = 'Quantid.:'
            FocusControl = DBEdit12
          end
          object Label12: TLabel
            Left = 54
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit13
          end
          object DBEdit12: TDBEdit
            Left = 4
            Top = 16
            Width = 48
            Height = 21
            DataField = 'TotQtdSdo'
            DataSource = DsStqCiclCab
            TabOrder = 0
          end
          object DBEdit13: TDBEdit
            Left = 54
            Top = 16
            Width = 68
            Height = 21
            DataField = 'TotValSdo'
            DataSource = DsStqCiclCab
            TabOrder = 1
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 782
        Top = 56
        Width = 223
        Height = 58
        Caption = ' Comiss'#245'es: '
        TabOrder = 11
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 219
          Height = 41
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label13: TLabel
            Left = 2
            Top = 0
            Width = 27
            Height = 13
            Caption = 'Valor:'
            FocusControl = DBEdit14
          end
          object Label14: TLabel
            Left = 76
            Top = 0
            Width = 28
            Height = 13
            Caption = 'Pago:'
            FocusControl = DBEdit15
          end
          object Label15: TLabel
            Left = 150
            Top = 0
            Width = 40
            Height = 13
            Caption = 'A pagar:'
            FocusControl = DBEdit16
          end
          object DBEdit14: TDBEdit
            Left = 2
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ComisVal'
            DataSource = DsStqCiclCab
            TabOrder = 0
          end
          object DBEdit15: TDBEdit
            Left = 76
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ComisPag'
            DataSource = DsStqCiclCab
            TabOrder = 1
          end
          object DBEdit16: TDBEdit
            Left = 150
            Top = 16
            Width = 68
            Height = 21
            DataField = 'ComisOPN'
            DataSource = DsStqCiclCab
            TabOrder = 2
          end
        end
      end
      object DBEdit19: TDBEdit
        Left = 928
        Top = 32
        Width = 72
        Height = 21
        DataField = 'AnoMesTxt'
        DataSource = DsStqCiclCab
        TabOrder = 12
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 394
      Width = 1264
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 567
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 741
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ciclo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtPagto: TBitBtn
          Tag = 187
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pagto'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPagtoClick
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 119
      Width = 1264
      Height = 116
      Align = alTop
      TabOrder = 2
      object Splitter2: TSplitter
        Left = 377
        Top = 1
        Height = 114
        ExplicitLeft = 530
      end
      object Splitter3: TSplitter
        Left = 829
        Top = 1
        Height = 114
        ExplicitLeft = 857
        ExplicitTop = 25
      end
      object DBGDados: TDBGrid
        Left = 1
        Top = 1
        Width = 376
        Height = 114
        Align = alLeft
        DataSource = DsMovmnts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGDadosDblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_Movmnt'
            Title.Caption = 'Movimento'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Abertura'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Encerrou_TXT'
            Title.Caption = 'Encerramento'
            Width = 82
            Visible = True
          end>
      end
      object Panel12: TPanel
        Left = 832
        Top = 1
        Width = 431
        Height = 114
        Align = alClient
        TabOrder = 1
        object dmkLabelRotate1: TdmkLabelRotate
          Left = 1
          Top = 1
          Width = 17
          Height = 112
          Angle = ag90
          Caption = 'Comiss'#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Align = alLeft
          ExplicitLeft = 5
          ExplicitTop = 2
        end
        object DBGCms: TDBGrid
          Left = 18
          Top = 1
          Width = 412
          Height = 112
          Align = alClient
          DataSource = DsPgtCms
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'N'#186
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compensado_TXT'
              Title.Caption = 'Compens.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 182
              Visible = True
            end>
        end
      end
      object Panel13: TPanel
        Left = 380
        Top = 1
        Width = 449
        Height = 114
        Align = alLeft
        TabOrder = 2
        object dmkLabelRotate2: TdmkLabelRotate
          Left = 1
          Top = 1
          Width = 17
          Height = 112
          Angle = ag90
          Caption = 'Faturamentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Align = alLeft
          ExplicitLeft = 5
          ExplicitTop = 2
        end
        object DBGFat: TDBGrid
          Left = 18
          Top = 1
          Width = 430
          Height = 112
          Align = alClient
          DataSource = DsPgtVen
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'N'#186
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compensado_TXT'
              Title.Caption = 'Compens.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 182
              Visible = True
            end>
        end
      end
    end
    object DBGStqMovIts: TDBGrid
      Left = 0
      Top = 295
      Width = 1264
      Height = 99
      Align = alBottom
      DataSource = DsStqMovIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'DataHora'
          Title.Caption = 'Data / Hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PGT'
          Title.Caption = 'Grupo'
          Width = 119
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD'
          Title.Caption = 'Produto'
          Width = 167
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_COR'
          Title.Caption = 'Cor'
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_TAM'
          Title.Caption = 'Tamanho'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SIGLA'
          Title.Caption = 'Sigla'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoBuy'
          Title.Caption = '$ Mercad.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoFrt'
          Title.Caption = '$ Frete'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorAll'
          Title.Caption = '$ Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IDCtrl'
          Title.Caption = 'ID Ctrl'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGru1'
          Title.Caption = 'Nivel 1'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoAll'
          Title.Caption = '$ Total'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1264
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1216
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 337
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 314
        Height = 32
        Caption = ' Gerenciamento de Ciclos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 314
        Height = 32
        Caption = ' Gerenciamento de Ciclos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 314
        Height = 32
        Caption = ' Gerenciamento de Ciclos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 553
      Top = 0
      Width = 663
      Height = 52
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 659
        Height = 35
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 176
    Top = 48
  end
  object QrStqCiclCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrStqCiclCabBeforeOpen
    AfterOpen = QrStqCiclCabAfterOpen
    BeforeClose = QrStqCiclCabBeforeClose
    AfterScroll = QrStqCiclCabAfterScroll
    SQL.Strings = (
      'SELECT CONCAT(SUBSTRING(AnoMes, 5, 2), '#39'/'#39', '
      '  SUBSTRING(AnoMes, 1, 4)) AS AnoMesTxt,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa, '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_Fornece, '
      'ent.Filial, (cab.ComisVal - cab.ComisPag) ComisOPN, cab.* '
      'FROM stqciclcab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=cab.Fornece')
    Left = 92
    Top = 193
    object QrStqCiclCabNO_Fornece: TWideStringField
      FieldName = 'NO_Fornece'
      Size = 100
    end
    object QrStqCiclCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCiclCabFornece: TIntegerField
      FieldName = 'Fornece'
      Required = True
    end
    object QrStqCiclCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrStqCiclCabTotQtdRem: TFloatField
      FieldName = 'TotQtdRem'
      Required = True
    end
    object QrStqCiclCabTotValRem: TFloatField
      FieldName = 'TotValRem'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabTotQtdRet: TFloatField
      FieldName = 'TotQtdRet'
      Required = True
    end
    object QrStqCiclCabTotValRet: TFloatField
      FieldName = 'TotValRet'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabTotQtdVen: TFloatField
      FieldName = 'TotQtdVen'
      Required = True
    end
    object QrStqCiclCabTotValVen: TFloatField
      FieldName = 'TotValVen'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabTotQtdSdo: TFloatField
      FieldName = 'TotQtdSdo'
      Required = True
    end
    object QrStqCiclCabTotValSdo: TFloatField
      FieldName = 'TotValSdo'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabComisPer: TFloatField
      FieldName = 'ComisPer'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrStqCiclCabComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabComisPag: TFloatField
      FieldName = 'ComisPag'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabComisOPN: TFloatField
      FieldName = 'ComisOPN'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrStqCiclCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrStqCiclCabNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 120
    end
    object QrStqCiclCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrStqCiclCabAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrStqCiclCabAnoMesTxt: TWideStringField
      FieldName = 'AnoMesTxt'
      Size = 10
    end
  end
  object DsStqCiclCab: TDataSource
    DataSet = QrStqCiclCab
    Left = 92
    Top = 245
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CriaRemessa1: TMenuItem
      Caption = 'Cria Re&messa'
      OnClick = CriaRemessa1Click
    end
    object CriaRetorno1: TMenuItem
      Caption = 'Cria Re&torno'
      OnClick = CriaRetorno1Click
    end
    object GeraVenda1: TMenuItem
      Caption = 'Gera &Venda'
      OnClick = GeraVenda1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENT '
      'FROM entidades'
      'WHERE'
      '      Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NO_ENT')
    Left = 180
    Top = 192
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 180
    Top = 244
  end
  object QrMovmnts: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrMovmntsAfterScroll
    SQL.Strings = (
      'SELECT Codigo, 1 Movmnt, "Envio" NO_Movmnt, Nome,'
      'Abertura, Encerrou'
      'FROM stqcnsggocab'
      'WHERE StqCiclCab=0'
      ''
      'UNION'
      ''
      'SELECT Codigo, 2 Movmnt, "Retorno" NO_Movmnt, Nome,'
      'Abertura, Encerrou'
      'FROM stqcnsgbkcab'
      'WHERE StqCiclCab=0'
      ''
      'UNION'
      ''
      'SELECT Codigo, 3 Movmnt, "Venda" NO_Movmnt, Nome,'
      'Abertura, Encerrou'
      'FROM stqcnsgvecab'
      'WHERE StqCiclCab=0')
    Left = 260
    Top = 192
    object QrMovmntsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMovmntsMovmnt: TLargeintField
      FieldName = 'Movmnt'
      Required = True
    end
    object QrMovmntsNO_Movmnt: TWideStringField
      FieldName = 'NO_Movmnt'
      Required = True
      Size = 7
    end
    object QrMovmntsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrMovmntsAbertura: TDateTimeField
      FieldName = 'Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrMovmntsEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Required = True
    end
    object QrMovmntsEncerrou_TXT: TWideStringField
      FieldName = 'Encerrou_TXT'
    end
    object QrMovmntsStqCiclCab: TIntegerField
      FieldName = 'StqCiclCab'
    end
  end
  object DsMovmnts: TDataSource
    DataSet = QrMovmnts
    Left = 260
    Top = 240
  end
  object QrStqMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_PRD, gg1.PrdGrupTip,'
      'gg1.UnidMed, pgt.Nome NO_PGT, med.Sigla SIGLA,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR, ggc.GraCorCad, '
      'ggx.GraGruC, ggx.GraGru1, ggx.GraTamI, smia.* '
      'FROM stqmovitsa smia'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=smia.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed'
      'WHERE Tipo=:P0'
      'AND OriCodi=:P1'
      'ORDER BY NO_PGT, NO_PRD, NO_TAM, NO_COR, smia.GraGruX, DataHora')
    Left = 340
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqMovItsNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrStqMovItsNO_PRD: TWideStringField
      FieldName = 'NO_PRD'
      Origin = 'gragru1.Nome'
      Size = 50
    end
    object QrStqMovItsPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'gragru1.PrdGrupTip'
    end
    object QrStqMovItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Origin = 'gragru1.UnidMed'
    end
    object QrStqMovItsNO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Origin = 'prdgruptip.Nome'
      Size = 30
    end
    object QrStqMovItsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrStqMovItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrStqMovItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrStqMovItsGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrStqMovItsGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Origin = 'gragrux.GraGruC'
    end
    object QrStqMovItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
    end
    object QrStqMovItsGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Origin = 'gragrux.GraTamI'
    end
    object QrStqMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'stqmovitsa.DataHora'
    end
    object QrStqMovItsIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovitsa.IDCtrl'
    end
    object QrStqMovItsTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'stqmovitsa.Tipo'
    end
    object QrStqMovItsOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovitsa.OriCodi'
    end
    object QrStqMovItsOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
      Origin = 'stqmovitsa.OriCtrl'
    end
    object QrStqMovItsOriCnta: TIntegerField
      FieldName = 'OriCnta'
      Origin = 'stqmovitsa.OriCnta'
    end
    object QrStqMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovitsa.Empresa'
    end
    object QrStqMovItsStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'stqmovitsa.StqCenCad'
    end
    object QrStqMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'stqmovitsa.GraGruX'
    end
    object QrStqMovItsQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'stqmovitsa.Qtde'
    end
    object QrStqMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovitsa.AlterWeb'
    end
    object QrStqMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovitsa.Ativo'
    end
    object QrStqMovItsOriPart: TIntegerField
      FieldName = 'OriPart'
      Origin = 'stqmovitsa.OriPart'
    end
    object QrStqMovItsPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'stqmovitsa.Pecas'
    end
    object QrStqMovItsPeso: TFloatField
      FieldName = 'Peso'
      Origin = 'stqmovitsa.Peso'
    end
    object QrStqMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      Origin = 'stqmovitsa.AreaM2'
    end
    object QrStqMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      Origin = 'stqmovitsa.AreaP2'
    end
    object QrStqMovItsFatorClas: TFloatField
      FieldName = 'FatorClas'
      Origin = 'stqmovitsa.FatorClas'
    end
    object QrStqMovItsQuemUsou: TIntegerField
      FieldName = 'QuemUsou'
      Origin = 'stqmovitsa.QuemUsou'
    end
    object QrStqMovItsRetorno: TSmallintField
      FieldName = 'Retorno'
      Origin = 'stqmovitsa.Retorno'
    end
    object QrStqMovItsParTipo: TIntegerField
      FieldName = 'ParTipo'
      Origin = 'stqmovitsa.ParTipo'
    end
    object QrStqMovItsParCodi: TIntegerField
      FieldName = 'ParCodi'
      Origin = 'stqmovitsa.ParCodi'
    end
    object QrStqMovItsDebCtrl: TIntegerField
      FieldName = 'DebCtrl'
      Origin = 'stqmovitsa.DebCtrl'
    end
    object QrStqMovItsSMIMultIns: TIntegerField
      FieldName = 'SMIMultIns'
      Origin = 'stqmovitsa.SMIMultIns'
    end
    object QrStqMovItsCustoAll: TFloatField
      FieldName = 'CustoAll'
      Origin = 'stqmovitsa.CustoAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsValorAll: TFloatField
      FieldName = 'ValorAll'
      Origin = 'stqmovitsa.ValorAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsCustoBuy: TFloatField
      FieldName = 'CustoBuy'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsCustoFrt: TFloatField
      FieldName = 'CustoFrt'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrStqMovItsPackGGX: TIntegerField
      FieldName = 'PackGGX'
    end
    object QrStqMovItsPackUnMed: TIntegerField
      FieldName = 'PackUnMed'
    end
    object QrStqMovItsPackQtde: TFloatField
      FieldName = 'PackQtde'
    end
    object QrStqMovItsTwnCtrl: TIntegerField
      FieldName = 'TwnCtrl'
    end
    object QrStqMovItsTwnBrth: TIntegerField
      FieldName = 'TwnBrth'
    end
    object QrStqMovItsValorUni: TFloatField
      FieldName = 'ValorUni'
    end
  end
  object DsStqMovIts: TDataSource
    DataSet = QrStqMovIts
    Left = 340
    Top = 240
  end
  object QrSum: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 412
    Top = 193
    object QrSumQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrSumValorAll: TFloatField
      FieldName = 'ValorAll'
    end
  end
  object QrPgtCms: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPgtCmsCalcFields
    Left = 573
    Top = 193
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPgtCmsData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtCmsVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtCmsBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtCmsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPgtCmsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPgtCmsContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPgtCmsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgtCmsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPgtCmsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPgtCmsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPgtCmsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPgtCmsNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPgtCmsBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPgtCmsAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPgtCmsConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPgtCmsTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPgtCmsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPgtCmsNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPgtCmsCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPgtCmsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPgtCmsNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
    end
    object QrPgtCmsCompensado_TXT: TWideStringField
      FieldName = 'Compensado_TXT'
      Size = 10
    end
    object QrPgtCmsDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsPgtCms: TDataSource
    DataSet = QrPgtCms
    Left = 573
    Top = 245
  end
  object PMPagto: TPopupMenu
    OnPopup = PMPagtoPopup
    Left = 748
    Top = 336
    object IncluiCms1: TMenuItem
      Caption = '&Inclui '
      OnClick = IncluiCms1Click
    end
    object ExcluiCms1: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiCms1Click
    end
  end
  object QrSumCms: TMySQLQuery
    Database = Dmod.MyDB
    Left = 816
    Top = 13
    object QrSumCmsValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrPag: TMySQLQuery
    Database = Dmod.MyDB
    Left = 16
    Top = 191
    object QrPagOPNValorAll: TFloatField
      FieldName = 'OPNValorAll'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPagTotValorAll: TFloatField
      FieldName = 'TotValorAll'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPagPagValorAll: TFloatField
      FieldName = 'PagValorAll'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsPag: TDataSource
    DataSet = QrPag
    Left = 16
    Top = 248
  end
  object frxDsStqCiclCab: TfrxDBDataset
    UserName = 'frxDsStqCiclCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Fornece=NO_FORNECE'
      'Codigo=Codigo'
      'Fornece=Fornece'
      'Nome=Nome'
      'TotQtdRem=TotQtdRem'
      'TotValRem=TotValRem'
      'TotQtdRet=TotQtdRet'
      'TotValRet=TotValRet'
      'TotQtdVen=TotQtdVen'
      'TotValVen=TotValVen'
      'TotQtdSdo=TotQtdSdo'
      'TotValSdo=TotValSdo'
      'ComisPer=ComisPer'
      'ComisVal=ComisVal'
      'ComisPag=ComisPag'
      'ComisOPN=ComisOPN'
      'Empresa=Empresa'
      'NO_Empresa=NO_Empresa'
      'Filial=Filial'
      'AnoMes=AnoMes'
      'AnoMesTxt=AnoMesTxt')
    DataSet = QrStqCiclCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 92
    Top = 297
  end
  object frxYRO_CICLO_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 41571.408359375000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '{'
      
        '  if <VFR_GRUPO1> = -1 then GH1.Visible := False else GH1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO1> = -1 then GF1.Visible := False else GF1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GH2.Visible := False else GH2.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GF2.Visible := False else GF2.Visibl' +
        'e := True;'
      '  //'
      '  GH1.Condition := <VFR_CODITION_A1>;'
      '  GH2.Condition := <VFR_CODITION_B1>;      '
      '}'
      'end.')
    OnGetValue = frxYRO_CICLO_001GetValue
    Left = 716
    Top = 192
    Datasets = <
      item
        DataSet = frxDsStqCiclCab
        DataSetName = 'frxDsStqCiclCab'
      end
      item
        DataSet = frxDsPag
        DataSetName = 'frxDsPag'
      end
      item
        DataSet = frxDsRec
        DataSetName = 'frxDsRec'
      end
      item
        DataSet = frxDsMovmnts
        DataSetName = 'frxDsMovmnts'
      end
      item
        DataSet = frxDsStqMovIts
        DataSetName = 'frxDsStqMovIts'
      end
      item
        DataSet = frxDsPgtVen
        DataSetName = 'frxDsPgtVen'
      end
      item
        DataSet = frxDsPgtCms
        DataSetName = 'frxDsPgtCms'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 128.504010240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795275590000000000
          Width = 491.338900000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ciclo: [frxDsStqCiclCab."Codigo"] - [frxDsStqCiclCab."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826771650000000000
          Top = 18.897650000000000000
          Width = 468.661417320000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."NO_Empresa"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472455590000000000
          Width = 491.338900000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'Vendedor: [frxDsStqCiclCab."Fornece"] - [frxDsStqCiclCab."NO_FOR' +
              'NECE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 37.795300000000000000
          Width = 188.976377950000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'M'#234's de refer'#234'ncia: [frxDsStqCiclCab."AnoMesTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 60.472480000000000000
          Width = 188.976377950000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '% de Comiss'#227'o: [FormatFloat('#39'0.0000'#39',<frxDsStqCiclCab."ComisPer"' +
              '>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929190000000000000
          Width = 79.370130000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Totais remessas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 86.929190000000000000
          Width = 79.370130000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Totais retornos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 86.929190000000000000
          Width = 294.803340000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Totais vendas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 86.929190000000000000
          Width = 79.370130000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Totais saldos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 86.929190000000000000
          Width = 147.401670000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Comiss'#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 100.157480310000000000
          Width = 26.456692913385800000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 113.385900000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotQtdRem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 113.385900000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'TotValRem'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotValRem"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 113.385826770000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'TotQtdRet'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotQtdRet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 113.385826770000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'TotValRet'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotValRet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 100.157480310000000000
          Width = 56.692913385826800000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 113.385826770000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'TotQtdVen'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotQtdVen"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 113.385826770000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'TotQtdVen'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotQtdVen"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000310000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 113.385826770000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'TotQtdSdo'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotQtdSdo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 480.000310000000000000
          Top = 113.385826770000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'TotValSdo'
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."TotValSdo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A faturar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 113.385826770000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPag."PagValorAll"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 113.385826770000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPag."OPNValorAll"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 100.157480310000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 100.157480310000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 113.385826770000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."ComisVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 113.385826770000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."ComisPag"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 100.157480310000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A pagar')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 113.385826770000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqCiclCab."ComisOPN"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 100.157480310000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 100.157480310000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 100.157480310000000000
          Width = 26.456692910000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Faturado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A receber')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 113.385826770000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRec."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 113.385826770000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRec."AReceber"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 100.157480310000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Recebido')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 661.417750000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 17.007876460000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsMovmnts."StqCiclCab"'
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315365830000000000
          Height = 17.007876460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Movimenta'#231#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 43.464586460000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        DataSet = frxDsMovmnts
        DataSetName = 'frxDsMovmnts'
        RowCount = 0
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 17.007876460000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsMovmnts
          DataSetName = 'frxDsMovmnts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMovmnts."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724490000000000000
          Top = 3.779530000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 124.724490000000000000
          Top = 17.007876460000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'NO_Movmnt'
          DataSet = frxDsMovmnts
          DataSetName = 'frxDsMovmnts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMovmnts."NO_Movmnt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 3.779530000000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 17.007876460000000000
          Width = 151.181102360000000000
          Height = 13.228346460000000000
          DataField = 'Nome'
          DataSet = frxDsMovmnts
          DataSetName = 'frxDsMovmnts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsMovmnts."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 3.779530000000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Abertura')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 17.007876460000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          DataField = 'Abertura'
          DataSet = frxDsMovmnts
          DataSetName = 'frxDsMovmnts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMovmnts."Abertura"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 3.779530000000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Encerramento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 17.007876460000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          DataField = 'Encerrou_TXT'
          DataSet = frxDsMovmnts
          DataSetName = 'frxDsMovmnts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMovmnts."Encerrou_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data/hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 30.236240000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 30.236240000000000000
          Width = 86.929175350000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Grupo de Produtos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 30.236240000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sigla')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Top = 30.236240000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 30.236240000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o unit.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Top = 30.236240000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 30.236240000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 30.236240000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 30.236240000000000000
          Width = 124.724475350000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Top = 30.236240000000000000
          Width = 26.456692913385830000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Red.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
      end
      object DT001: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        DataSet = frxDsStqMovIts
        DataSetName = 'frxDsStqMovIts'
        RowCount = 0
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'DataHora'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Width = 64.251961180000000000
          Height = 13.228346460000000000
          DataField = 'NO_PGT'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_PGT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 192.756030000000000000
          Width = 102.047236770000000000
          Height = 13.228346460000000000
          DataField = 'NO_PRD'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_PRD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'NO_COR'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 22.677165350000000000
          Height = 13.228346460000000000
          DataField = 'PrdGrupTip'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqMovIts."PrdGrupTip"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'SIGLA'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."SIGLA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 472.441250000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataField = 'Qtde'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."Qtde"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'ValorUni'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."ValorUni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 559.370440000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'CustoFrt'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."CustoFrt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ValorAll'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsStqMovIts."ValorAll"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'NO_TAM'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsStqMovIts."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 22.677165350000000000
          Height = 13.228346460000000000
          DataField = 'Nivel1'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqMovIts."Nivel1"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 294.803340000000000000
          Width = 26.456692913385830000
          Height = 13.228346460000000000
          DataField = 'GraGruX'
          DataSet = frxDsStqMovIts
          DataSetName = 'frxDsStqMovIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsStqMovIts."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 32.125996460000000000
        Top = 381.732530000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPgtCms."FatID"'
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315365830000000000
          Height = 17.007876460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Faturamentos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Top = 18.897650000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 18.897650000000000000
          Width = 56.692896300000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 170.078776770000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 325.039580000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Situa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 18.897650000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compensado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 18.897650000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lan'#231'amento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 18.897650000000000000
          Width = 181.417322834645700000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPgtVen
        DataSetName = 'frxDsPgtVen'
        RowCount = 0
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataField = 'Data'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgtVen."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Vencimento'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgtVen."Vencimento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 56.692896300000000000
          Height = 13.228346460000000000
          DataField = 'Credito'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgtVen."Credito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Width = 170.078776770000000000
          Height = 13.228346460000000000
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPgtVen."NOMECARTEIRA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 325.039580000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'NO_SIT'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPgtVen."NO_SIT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Compensado_TXT'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgtVen."Compensado_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgtVen."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 181.417322834645700000
          Height = 13.228346460000000000
          DataField = 'Descricao'
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPgtVen."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH003: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 32.125996460000000000
        Top = 502.677490000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPgtCms."FatID"'
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315365830000000000
          Height = 17.007876460000000000
          DataSet = frxDsStqCiclCab
          DataSetName = 'frxDsStqCiclCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Comiss'#245'es')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Top = 18.897650000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Top = 18.897650000000000000
          Width = 56.692896300000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 170.078776770000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 325.039580000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Situa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 18.897650000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compensado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Top = 18.897650000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lan'#231'amento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 18.897650000000000000
          Width = 181.417322834645700000
          Height = 13.228346460000000000
          DataSet = frxDsPgtVen
          DataSetName = 'frxDsPgtVen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF003: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779530000000000000
        Top = 597.165740000000000000
        Width = 680.315400000000000000
      end
      object MD003: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPgtCms
        DataSetName = 'frxDsPgtCms'
        RowCount = 0
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataField = 'Data'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgtCms."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Vencimento'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgtCms."Vencimento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 56.692896300000000000
          Height = 13.228346460000000000
          DataField = 'Debito'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgtCms."Debito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Width = 170.078776770000000000
          Height = 13.228346460000000000
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPgtCms."NOMECARTEIRA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 325.039580000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          DataField = 'NO_SIT'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPgtCms."NO_SIT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DataField = 'Compensado_TXT'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPgtCms."Compensado_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 449.764070000000000000
          Width = 49.133858267716540000
          Height = 13.228346460000000000
          DataField = 'Controle'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPgtCms."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 181.417322834645700000
          Height = 13.228346460000000000
          DataField = 'Descricao'
          DataSet = frxDsPgtCms
          DataSetName = 'frxDsPgtCms'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPgtCms."Descricao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPag: TfrxDBDataset
    UserName = 'frxDsPag'
    CloseDataSource = False
    FieldAliases.Strings = (
      'OPNValorAll=OPNValorAll'
      'TotValorAll=TotValorAll'
      'PagValorAll=PagValorAll')
    DataSet = QrPag
    BCDToCurrency = False
    DataSetOptions = []
    Left = 16
    Top = 297
  end
  object QrRec: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRecCalcFields
    SQL.Strings = (
      'SELECT SUM(Credito) Credito'
      'FROM lct0001a'
      'WHERE FatID=4205'
      'AND FatNum=1'
      'AND Compensado > 2')
    Left = 460
    Top = 192
    object QrRecCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecAReceber: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AReceber'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
  end
  object DsRec: TDataSource
    DataSet = QrRec
    Left = 460
    Top = 244
  end
  object frxDsRec: TfrxDBDataset
    UserName = 'frxDsRec'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Credito=Credito'
      'AReceber=AReceber')
    DataSet = QrRec
    BCDToCurrency = False
    DataSetOptions = []
    Left = 460
    Top = 293
  end
  object frxDsMovmnts: TfrxDBDataset
    UserName = 'frxDsMovmnts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Movmnt=Movmnt'
      'NO_Movmnt=NO_Movmnt'
      'Nome=Nome'
      'Abertura=Abertura'
      'Encerrou=Encerrou'
      'Encerrou_TXT=Encerrou_TXT'
      'StqCiclCab=StqCiclCab')
    DataSet = QrMovmnts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 260
    Top = 289
  end
  object frxDsStqMovIts: TfrxDBDataset
    UserName = 'frxDsStqMovIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nivel1=Nivel1'
      'NO_PRD=NO_PRD'
      'PrdGrupTip=PrdGrupTip'
      'UnidMed=UnidMed'
      'NO_PGT=NO_PGT'
      'SIGLA=SIGLA'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'GraCorCad=GraCorCad'
      'GraGruC=GraGruC'
      'GraGru1=GraGru1'
      'GraTamI=GraTamI'
      'DataHora=DataHora'
      'IDCtrl=IDCtrl'
      'Tipo=Tipo'
      'OriCodi=OriCodi'
      'OriCtrl=OriCtrl'
      'OriCnta=OriCnta'
      'Empresa=Empresa'
      'StqCenCad=StqCenCad'
      'GraGruX=GraGruX'
      'Qtde=Qtde'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'OriPart=OriPart'
      'Pecas=Pecas'
      'Peso=Peso'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'FatorClas=FatorClas'
      'QuemUsou=QuemUsou'
      'Retorno=Retorno'
      'ParTipo=ParTipo'
      'ParCodi=ParCodi'
      'DebCtrl=DebCtrl'
      'SMIMultIns=SMIMultIns'
      'CustoAll=CustoAll'
      'ValorAll=ValorAll'
      'CustoBuy=CustoBuy'
      'CustoFrt=CustoFrt'
      'PackGGX=PackGGX'
      'PackUnMed=PackUnMed'
      'PackQtde=PackQtde'
      'TwnCtrl=TwnCtrl'
      'TwnBrth=TwnBrth'
      'ValorUni=ValorUni')
    DataSet = QrStqMovIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 340
    Top = 289
  end
  object QrPgtVen: TMySQLQuery
    Database = Dmod.MyDB
    Left = 517
    Top = 193
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPgtVenData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtVenVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtVenBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPgtVenSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPgtVenFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPgtVenContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPgtVenDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgtVenDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPgtVenFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPgtVenFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPgtVenNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPgtVenNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPgtVenBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPgtVenAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPgtVenConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPgtVenTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPgtVenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPgtVenNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPgtVenCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPgtVenAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPgtVenCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPgtVenNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
    end
    object QrPgtVenCompensado_TXT: TWideStringField
      FieldName = 'Compensado_TXT'
      Size = 10
    end
  end
  object DsPgtVen: TDataSource
    DataSet = QrPgtVen
    Left = 517
    Top = 245
  end
  object QrGC: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 640
    Top = 192
  end
  object frxDsPgtVen: TfrxDBDataset
    UserName = 'frxDsPgtVen'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Vencimento=Vencimento'
      'Banco=Banco'
      'SEQ=SEQ'
      'FatID=FatID'
      'ContaCorrente=ContaCorrente'
      'Documento=Documento'
      'Descricao=Descricao'
      'FatParcela=FatParcela'
      'FatNum=FatNum'
      'NOMECARTEIRA=NOMECARTEIRA'
      'NOMECARTEIRA2=NOMECARTEIRA2'
      'Banco1=Banco1'
      'Agencia1=Agencia1'
      'Conta1=Conta1'
      'TipoDoc=TipoDoc'
      'Controle=Controle'
      'NOMEFORNECEI=NOMEFORNECEI'
      'CARTEIRATIPO=CARTEIRATIPO'
      'Agencia=Agencia'
      'Credito=Credito'
      'NO_SIT=NO_SIT'
      'Compensado_TXT=Compensado_TXT')
    DataSet = QrPgtVen
    BCDToCurrency = False
    DataSetOptions = []
    Left = 520
    Top = 293
  end
  object frxDsPgtCms: TfrxDBDataset
    UserName = 'frxDsPgtCms'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Vencimento=Vencimento'
      'Banco=Banco'
      'SEQ=SEQ'
      'FatID=FatID'
      'ContaCorrente=ContaCorrente'
      'Documento=Documento'
      'Descricao=Descricao'
      'FatParcela=FatParcela'
      'FatNum=FatNum'
      'NOMECARTEIRA=NOMECARTEIRA'
      'NOMECARTEIRA2=NOMECARTEIRA2'
      'Banco1=Banco1'
      'Agencia1=Agencia1'
      'Conta1=Conta1'
      'TipoDoc=TipoDoc'
      'Controle=Controle'
      'NOMEFORNECEI=NOMEFORNECEI'
      'CARTEIRATIPO=CARTEIRATIPO'
      'Agencia=Agencia'
      'NO_SIT=NO_SIT'
      'Compensado_TXT=Compensado_TXT'
      'Debito=Debito')
    DataSet = QrPgtCms
    BCDToCurrency = False
    DataSetOptions = []
    Left = 576
    Top = 293
  end
end
