object FmStqCiclSel: TFmStqCiclSel
  Left = 339
  Top = 185
  Caption = 'YRO-CICLO-002 :: Ciclo - Sele'#231#227'o de Movimentos'
  ClientHeight = 505
  ClientWidth = 768
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 360
    Width = 768
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 768
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 720
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 672
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 376
        Height = 32
        Caption = 'Ciclo - Sele'#231#227'o de Movimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 32
        Caption = 'Ciclo - Sele'#231#227'o de Movimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 376
        Height = 32
        Caption = 'Ciclo - Sele'#231#227'o de Movimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 391
    Width = 768
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 764
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 435
    Width = 768
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 622
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 768
    Height = 49
    Align = alTop
    TabOrder = 4
    object Label2: TLabel
      Left = 8
      Top = 4
      Width = 26
      Height = 13
      Caption = 'Ciclo:'
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 20
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 68
      Top = 20
      Width = 189
      Height = 21
      Enabled = False
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object DBGMovmnts: TdmkDBGridZTO
    Left = 0
    Top = 97
    Width = 768
    Height = 120
    Align = alTop
    DataSource = DsMovmnts
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <>
    Columns = <
      item
        Expanded = False
        FieldName = 'NO_Movmnt'
        Title.Caption = 'Movimento'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Abertura'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Encerrou_TXT'
        Title.Caption = 'Encerramento'
        Visible = True
      end>
  end
  object QrMovmnts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, 1 Movmnt, "Envio" NO_Movmnt, Nome,'
      'Abertura, Encerrou'
      'FROM stqcnsggocab'
      'WHERE StqCiclCab=0'
      ''
      'UNION'
      ''
      'SELECT Codigo, 2 Movmnt, "Retorno" NO_Movmnt, Nome,'
      'Abertura, Encerrou'
      'FROM stqcnsgbkcab'
      'WHERE StqCiclCab=0'
      ''
      'UNION'
      ''
      'SELECT Codigo, 3 Movmnt, "Venda" NO_Movmnt, Nome,'
      'Abertura, Encerrou'
      'FROM stqcnsgvecab'
      'WHERE StqCiclCab=0')
    Left = 216
    Top = 44
    object QrMovmntsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMovmntsMovmnt: TLargeintField
      FieldName = 'Movmnt'
      Required = True
    end
    object QrMovmntsNO_Movmnt: TWideStringField
      FieldName = 'NO_Movmnt'
      Required = True
      Size = 7
    end
    object QrMovmntsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrMovmntsAbertura: TDateTimeField
      FieldName = 'Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrMovmntsEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object QrMovmntsEncerrou_TXT: TWideStringField
      FieldName = 'Encerrou_TXT'
    end
  end
  object DsMovmnts: TDataSource
    DataSet = QrMovmnts
    Left = 216
    Top = 92
  end
end
