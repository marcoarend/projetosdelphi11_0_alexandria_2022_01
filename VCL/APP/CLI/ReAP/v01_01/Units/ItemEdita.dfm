object FmItemEdita: TFmItemEdita
  Left = 339
  Top = 185
  Caption = 'Edi'#231#227'o de item'
  ClientHeight = 594
  ClientWidth = 774
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 774
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 726
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 678
      Height = 48
      Align = alClient
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 774
    Height = 432
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 774
      Height = 432
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 774
        Height = 432
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 8
          Width = 284
          Height = 13
          Caption = 'Nome da configura'#231#227'o:  exemplo: (Windows M'#233'dia Player'#174')'
        end
        object Label3: TLabel
          Left = 12
          Top = 48
          Width = 457
          Height = 13
          Caption = 
            'Caminho (pasta) do Execut'#225'vel da tarefa alvo: exemplo: C:\Progra' +
            'm Files\Windows Media Player'
        end
        object Label2: TLabel
          Left = 12
          Top = 88
          Width = 342
          Height = 13
          Caption = 
            'Nome + extens'#227'o do execut'#225'vel da tarefa alvo: (exemplo: wmplayer' +
            '.exe)'
        end
        object Label4: TLabel
          Left = 12
          Top = 128
          Width = 405
          Height = 13
          Caption = 
            'Par'#226'metros extras na execu'#231#227'o do aplicativo da tarefa alvo: (exe' +
            'mplo: -p teste -u 123)'
        end
        object LaSegWaitOnOSReinit: TLabel
          Left = 10
          Top = 174
          Width = 314
          Height = 13
          Caption = ' Tempo em segundos de espara ao reiniciar o sistema operacional:'
          Enabled = False
        end
        object Label5: TLabel
          Left = 14
          Top = 200
          Width = 226
          Height = 13
          Caption = 'Intervalo em segundos da atividade das tarefas:'
        end
        object SbFileOpen: TSpeedButton
          Left = 740
          Top = 64
          Width = 23
          Height = 22
          Caption = '...'
          OnClick = SbFileOpenClick
        end
        object EdNome: TdmkEdit
          Left = 12
          Top = 24
          Width = 750
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdPasta: TdmkEdit
          Left = 12
          Top = 64
          Width = 725
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdExecutavel: TdmkEdit
          Left = 12
          Top = 104
          Width = 750
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdParametros: TdmkEdit
          Left = 12
          Top = 144
          Width = 750
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdSegWaitOnOSReinit: TdmkEdit
          Left = 338
          Top = 170
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '60'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '600'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 600
          ValWarn = False
        end
        object EdSegWaitScanMonit: TdmkEdit
          Left = 338
          Top = 196
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '60'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '60'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 60
          ValWarn = False
        end
        object CkExecAsAdmin: TdmkCheckBox
          Left = 16
          Top = 228
          Width = 201
          Height = 17
          Caption = 'Executa como administrador.'
          TabOrder = 6
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 480
    Width = 774
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 770
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 524
    Width = 774
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 628
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 11
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 10
        Left = 12
        Top = 8
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 48
    Top = 7
  end
end
