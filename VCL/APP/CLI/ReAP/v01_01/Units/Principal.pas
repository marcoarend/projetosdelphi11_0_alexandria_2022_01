unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids, ShellApi,
  DCPcrypt2, DCPblockciphers, DCPtwofish,
  Registry,
  UnDmkEnums;

type
  TFmPrincipal = class(TForm)
    TrayIcon1: TTrayIcon;
    PopupMenu1: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    TmOculta: TTimer;
    Panel1: TPanel;
    Label1: TLabel;
    BtSaida: TBitBtn;
    BtAtiva: TBitBtn;
    Button1: TButton;
    BtCon: TBitBtn;
    BtOpcoesApp: TBitBtn;
    EdTempoAtividade: TEdit;
    BtSobre: TBitBtn;
    DCP_twofish1: TDCP_twofish;
    TmMonitora: TTimer;
    PnEdita: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    SGApps: TStringGrid;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    TmVerificaExecucao: TTimer;
    Memo1: TMemo;
    procedure TrayIcon1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure TmOcultaTimer(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAtivaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure TmVerificaExecucaoTimer(Sender: TObject);
  private
    { Private declarations }
    FShowed, FAtivou: Boolean;
    procedure VerificaAppsEmExecucao();
    function ExecutaApp(const Nome, Pasta, Executavel, Parametros: String;
              const ExecAsAdmin: Boolean): Boolean;
  procedure ExecutaNaInicializacao(Executa: Boolean; Titulo,
            Programa: String);
  public
    { Public declarations }
    FSegWaitOnOSReinit, FSegWaitScanMonit: Integer;
    FExecAsAdmin: Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses dmkGeral, UnApp_Vars, UnApp_Consts, UnApp_PF, UnMinimumMyObjects,
  UnMinimumDmkProcFunc, UnAppMainPF, UnMyProcesses;

{$R *.dfm}

procedure TFmPrincipal.BtAlteraClick(Sender: TObject);
var
  Nome, Pasta, Executavel, Parametros, ExecAsAdmin: String;
begin
  Nome        := SGApps.Cells[1, SGApps.Row];
  Pasta       := SGApps.Cells[2, SGApps.Row];
  Executavel  := SGApps.Cells[3, SGApps.Row];
  Parametros  := SGApps.Cells[4, SGApps.Row];
  ExecAsAdmin := SGApps.Cells[5, SGApps.Row];
  App_PF.MostraFormItemEdita(stUpd, SGApps, Nome, Pasta, Executavel, Parametros,
  ExecAsAdmin);
end;

procedure TFmPrincipal.BtAtivaClick(Sender: TObject);
begin
  App_PF.MostraFormAgendaTarefa_ParaBoss(True);
end;

procedure TFmPrincipal.BtIncluiClick(Sender: TObject);
var
  Nome, Pasta, Executavel, Parametros, ExecAsAdmin: String;
begin
  Nome        := EmptyStr;
  Pasta       := EmptyStr;
  Executavel  := EmptyStr;
  Parametros  := EmptyStr;
  ExecAsAdmin := 'N�O';
  App_PF.MostraFormItemEdita(stIns, SGApps, Nome, Pasta, Executavel, Parametros,
    ExecAsAdmin);
end;

procedure TFmPrincipal.BtSaidaClick(Sender: TObject);
begin
  TrayIcon1.Visible := True;
  Application.MainFormOnTaskBar := FShowed;
  FmPrincipal.Hide;
  //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
  Application.MainFormOnTaskBar := FShowed;

end;

function TFmPrincipal.ExecutaApp(const Nome, Pasta, Executavel, Parametros: String;
  const ExecAsAdmin: Boolean): Boolean;
var
  HProcesso: Integer;
  //
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      HProcesso := sei.hProcess;
    end;
  end;
var
  FileName, Folder: string;
  WaitUntilTerminated, WaitUntilIdle(*, RunMinimized*): Boolean;
  ErrorCode: Integer;
  OK: Boolean;
  //EstadoJanela: Integer;
  HWND_CmdShow: Integer;
  //
  Keys: array of Word;
  Shift: TShiftState;
  SpecialKey: Boolean;
  Retorno: Integer;
var
  Path: string;
  ExecuteResult, MouseClickEventId: Integer;
  Continua: Boolean;
begin
  Result := False;
  //FileName  := EdPasta.Text + EdExecutavel.Text;
  FileName  := Pasta + Executavel;
  //Params    := EdParametros.Text; // '-p'; // can be empty
  Folder    := ''; //EdPasta.Text; // if empty function will extract path from FileName
  //ErrorCode := ;
  //
  WaitUntilTerminated := False;
  WaitUntilIdle       := True;//True;
  //HWND_CmdShow        := AppPF.EstadoJanelaToHWND_CmdShow(EstadoJanela);

  //if FmPrincipal.FBtAtivado = False then Exit;

  if not ExecAsAdmin then
  begin
    //Funcionando local e Server! *)
    Retorno := ShellExecute((*Application.Handle*)0, 'open', PChar(Executavel),
      PChar(Parametros), PChar(Pasta), SW_SHOWNORMAL);
    Continua := Retorno > 32;
  end else
  begin
    Continua :=  RunAsAdmin((*Application.Handle*)0, Executavel, Pasta, Parametros, HWND_CmdShow);
  end;
  //
  //
  Result := True;
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
    ExecutaNaInicializacao(True, CO_App_Name, Application.ExeName);

// UnAppPF.GetSpecialFolderPath(CSIDL_STARTUP) =
//C:\Users\angeloni\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

//Make Vista launch UAC restricted programs at startup with Task Scheduler
//https://www.techrepublic.com/blog/windows-and-office/make-vista-launch-uac-restricted-programs-at-startup-with-task-scheduler/


  //App_PF.MostraFormAgendaTarefa_ParaBoss(True);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
var
  Continua: Boolean;
begin
  MyObjects.CorIniComponente;
  Continua := False;
  //
  if FAtivou = False then
  begin
    FAtivou := True;
    if App_PF.ObtemDadosOpcoesAppDeArquivo(
      App_PF.ObtemNomeArquivoOpcoes(), DCP_twofish1) then
    begin
      if App_PF.LiberaUsoVendaApp1(DCP_twofish1(*, FApp User Pwd, FExe On Ini*)) then
      begin
        // Liberar tudo por aqui!
        if (VAR_SENHA_BOSS <> EmptyStr) and (VAR_ExigeSenhaLoginApp) then
          Continua := App_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
        else
          Continua := True;
      end else
      begin
        Geral.MB_Info('N�O Liberado!');
        Halt(0);
      end;
    end;
  end else
  begin
    Continua := True;
  end;
  //
  if Continua then
  begin
    AppMainPF.CriaEcarregaTabela(
    FmPrincipal.DCP_twofish1,
    SGApps,
(*
    MemTableTpAcao,
    MemTableCodigo,
    MemTableOrdem,
    MemTableNome,
    MemTableSegundosA,
    MemTableSegundosD,
    MemTableMousePosX,
    MemTableTexto,
    MemTableMousePosY,
    MemTableMouseEvent,
    MemTableQtdClkSmlt,
    MemTableKeyCode,
    MemTableKeyEvent,
    MemTableShiftState,
    MemTableTipoTecla,
    MemTableTeclEsp,
    MemTableLetra,
    FNome, FPasta, FExecutavel, FParametros, FMonitSohProprioProces,
    FParaEtapaOnDeactive, FExecAsAdmin, FLogInMemo, FExeOnIni,*)
    FSegWaitOnOSReinit, FSegWaitScanMonit
    (*FEstadoJanela, FMaiorCodigo*)
    );
    TmMonitora.Interval := FSegWaitScanMonit * 1000;
    //
    TmOculta.Enabled := True;
    //if ????? then
    begin
      TrayIcon1.Visible := True;
      //
    end;
    //VerificaAppsEmExecucao();
    TmVerificaExecucao.Enabled := True;
  end else
    Halt(0);
  //
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
(*&�%$#@
  FBtAtivado  := False;
  FExeOnIni   := False;
  FHProcesso  := 0;
*)

(*
  TMeuDB          := 'overseer';
  VAR_RUN_AS_SVC  := True;
  VAR_BDSENHA     := EmptyStr;
  FHides          := 0;
*)
  FShowed         := True;
  FAtivou         := False;
(*&�%$#@!
  FNome           := '';
  FPasta          := '';
  FExecutavel     := '';
  FParametros     := '';
  FEstadoJanela   := -1;
*)
  //
(*
  VAR_TYPE_LOG :=  TTypeLog.ttlCliIntUni;
*)
  Geral.DefineFormatacoes;
  dmkPF.ConfigIniApp(0);
(*&�%$#@!
  Application.OnMessage := MyObjects.FormMsg;
*)
  //Application.OnIdle    := AppIdle;
  //
  MyObjects.LimpaGrade(SGApps, (*ColIni*)1, (*RowIni*)1, (*ExcluiLinhas*)True);
  SGApps.ColWidths[1] := 200;
  SGApps.Cells[1, 0] := 'Descri��o';
  SGApps.ColWidths[2] := 400;
  SGApps.Cells[2, 0] := 'Pasta';
  SGApps.ColWidths[3] := 100;
  SGApps.Cells[3, 0] := 'Execut�vel';
  SGApps.ColWidths[4] := 300;
  SGApps.Cells[4, 0] := 'Par�metros';
  SGApps.ColWidths[5] := 300;
  SGApps.Cells[5, 0] := 'Como administrador';
  //
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;

end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, CO_App_Name, Application.ExeName);
//  App_PF.MostraFormAgendaTarefa_ParaBoss(False);
end;

procedure TFmPrincipal.PopupMenu1Popup(Sender: TObject);
var
  Valor: String;
begin
  Valor := Geral.ReadAppKeyCU(CO_App_Name, 'Microsoft\Windows\CurrentVersion\Run', ktString, '');
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
  Mostrar1.Enabled := not FmPrincipal.Visible;
end;

procedure TFmPrincipal.TmOcultaTimer(Sender: TObject);
begin
  TmOculta.Enabled := False;
  BtSaidaClick(Self);
(*&�%$#@!
  if (MemTable.State <> dsInactive)  and (FExeOnIni) then
  begin
    AtivaTarefa(True, True);
    //TmMonitora.Interval := FSegWaitScanMonit * 1000;
    TmMonitora.Enabled := True;
  end;
*)
end;

procedure TFmPrincipal.TmVerificaExecucaoTimer(Sender: TObject);
begin
  VerificaAppsEmExecucao();
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmPrincipal.Visible := True;
end;

procedure TFmPrincipal.VerificaAppsEmExecucao();
var
  I: Integer;
  Agora, Descricao, Pasta, Executavel, Parametros: String;
  ExecAsAdmin: Boolean;
begin
  Memo1.Lines.Clear;
  for I := 1 to SGApps.RowCount - 1 do
  begin
    Descricao   := SGApps.Cells[1, I];
    Pasta       := SGApps.Cells[2, I];
    Executavel  := SGApps.Cells[3, I];
    Parametros  := SGApps.Cells[4, I];
    if Uppercase(SGApps.Cells[5, I]) = 'SIM' then
      ExecAsAdmin := True
    else
      ExecAsAdmin := False;
    //
    //Executavel := Copy(Executavel, 1, Pos('.', Executavel)-1);
    // Localiza pelo nome!
    Agora := FormatDateTime('dd/mm/yy hh:nn:ss', Now());
    if MyProcesses.FindProcessesByName(Executavel) >= 0 then
      Memo1.Lines.Add(Agora + ' - ' + Executavel + ' EST� rodando...')
    else
    begin
      Memo1.Lines.Add(Agora + ' - ' + Executavel + ' N�O est� rodando!');
      ExecutaApp(Descricao, Pasta, Executavel, Parametros, ExecAsAdmin);
      if MyProcesses.FindProcessesByName(Executavel) >= 0 then
        Memo1.Lines.Add(Agora + ' - ' + Executavel + ' Foi INICIADO e EST� rodando...')
      else
        Memo1.Lines.Add(Agora + ' - ' + Executavel + ' N�O FOI POSS�VEL INICIAR!');
    end;
    //
  end;
end;

end.
