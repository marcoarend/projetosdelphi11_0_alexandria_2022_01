unit UnApp_PF;

interface


uses
  Winapi.Windows, Winapi.ShlObj, System.SysUtils, Vcl.Forms,
  Vcl.Controls, Vcl.Grids,
  //Winapi.Messages,
  Winapi.ShellApi,
  System.JSON, System.IOUtils,
  System.Classes,
  DCPtwofish, DCPsha1,
  //UnGrl_Vars*);
  UnDmkEnums, UnInternalConsts;

type
  TUnApp_PF = class(TObject)
  private
    { Private declarations }
    procedure _MostraFormAgendaTarefa();
  public
    { Public declarations }
    function  DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
    function  EnCryptJSON(JSON: string; twofish: TDCP_twofish): string;
    function  GetSpecialFolderPath(CSIDLFolder: Integer): string;
    function  LiberaAppPelaSenha(AppUserPwd: String): Boolean;
    function  LiberaUsoVendaApp1(const DCP_twofish: TDCP_twofish): Boolean;
    procedure MostraFormAgendaTarefa_ParaBoss(Agenda: Boolean);
    function  MostraFormItemEdita(const SQLType: TSQLType; const SGApps:
              TStringGrid; const Nome, Pasta, Executavel, Parametros,
              ExecAsAdmin: String): Boolean;
    procedure MostraFormOpcoes();
    function  ObtemDadosOpcoesAppDeArquivo(const Arquivo: String; const DCP_twofish:
              TDCP_twofish): Boolean;
    function  ObtemDadosLicencaDeArquivo(const Arquivo: String; const DCP_twofish:
              TDCP_twofish; var AtualizadoEm, SerialHD, CPUID, SerialKey:
              String): Boolean;
    function  ObtemNomeArquivoTarefa(Application_Name: String): String;
    function  ObtemNomeArquivoLicenca(var Arquivo, Pasta: String): Boolean;
    function  ObtemNomeArquivoOpcoes(): String;
  end;

var
  App_PF: TUnApp_PF;

implementation

uses dmkGeral, UnApp_Vars, Dmk_RestApp, UnMinimumDmkProcFunc, UnApp_Consts,
  UnLicVendaApp_Dmk, UnMyJSON,
  AgendaTarefa, OpcoesApp, LicDados, ItemEdita;

{ TUnApp_PF }

function TUnApp_PF.DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr(CO_CHAVE, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Crypt); // Descriptografa o JSON.
  twofish.Burn();
end;

function TUnApp_PF.EnCryptJSON(JSON: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr(CO_CHAVE, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.EncryptString(JSON); // Criptografa o JSON.
  twofish.Burn();
end;

function TUnApp_PF.GetSpecialFolderPath(CSIDLFolder: Integer): string;
var
  FilePath: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0, CSIDLFolder, 0, 0, FilePath);
  Result := FilePath;
end;

function TUnApp_PF.LiberaAppPelaSenha(AppUserPwd: String): Boolean;
begin
  Application.CreateForm(TFmDmk_RestApp, FmDmk_RestApp);
  FmDmk_RestApp.ShowModal;
  Result := FmDmk_RestApp.FResult;
  FmDmk_RestApp.Destroy;
end;

function TUnApp_PF.LiberaUsoVendaApp1(const DCP_twofish: TDCP_twofish): Boolean;

  function MostraFormLicDados(var CPF_CNPJ, AtualizadoEm, SerialHD, CPUID,
  SerialKey(*, App User Pwd, Exe On Ini*): String): Boolean;
  begin
    Application.CreateForm(TFmLicDados, FmLicDados);
    FmLicDados.EdCPF_CNPJ.ValueVariant  := VAR_CPF_CNPJ;
    FmLicDados.EdPosicao.ValueVariant   := VAR_Posicao;
    FmLicDados.EdCodigoPIN.ValueVariant := VAR_CodigoPIN;
    FmLicDados.ShowModal;
    CPF_CNPJ     := VAR_CPF_CNPJ;
    AtualizadoEm := FmLicDados.FAtualizadoEm;
    SerialHD     := FmLicDados.FSerialHD;
    CPUID        := FmLicDados.FCPUID;
    SerialKey    := FmLicDados.FSerialKey;
    //App User Pwd   := FmLicDados.FApp User Pwd;
    //Exe On Ini     := FmLicDados.F Exe On Ini;
    //
    Result       := FmLicDados.FLiberado;
    //
    FmLicDados.Destroy;
  end;
  //
  function GeraESalvaJSON(Dir, DirEArquivo, CPF_CNPJ, AtualizadoEm, SerialHD,
  CPUID, SerialKey(*, App User Pwd, Exe On Ini*): String): Boolean;
  var
    i: Integer;
    lJsonObj: TJSONObject;
    JSONColor: TJSONObject;
    JSONArray : TJSONArray;
    Nome, Pasta, Executavel, Parametros: String;
    EstadoJanela: Integer;
    TxtCript: String;
  begin
    Result := False;
    Screen.Cursor := crHourGlass;
    try
      lJsonObj     := TJSONObject.Create;
      //
      lJsonObj.AddPair('CPF_CNPJ', Geral.JsonText(CPF_CNPJ));
      lJsonObj.AddPair('SerialHD', Geral.JsonText(SerialHD));
      lJsonObj.AddPair('CPUID', Geral.JsonText(CPUID));
      lJsonObj.AddPair('SerialKey', Geral.JsonText(SerialKey));
      lJsonObj.AddPair('AtualizadoEm', Geral.JsonText(AtualizadoEm));
      //lJsonObj.AddPair('App User Pwd', Geral.JsonText(AppUserPwd));
      //lJsonObj.AddPair('Exe On Ini', Geral.JsonText(ExeOnIni));
      //
      TxtCript := EnCryptJSON(lJsonObj.ToString, DCP_twofish);
      ForceDirectories(Dir);
      TFile.WriteAllText(DirEArquivo, TxtCript, TEncoding.ANSI);
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

//https://forums.embarcadero.com/thread.jspa?threadID=117722
var
  Arq_AtualizadoEm, Arq_SerialHD, Arq_CPUID, Arq_SerialKey,
  Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey,
  Arquivo, Pasta: String;
  Liberado: Boolean;
  Salvar: Boolean;
  Agora, UltAtz: TDateTime;
begin
  Liberado := False;
  Salvar   := False;
  ObtemNomeArquivoLicenca(Arquivo, Pasta);
  if not FileExists(Arquivo) then
  begin
    Liberado := MostraFormLicDados(VAR_CPF_CNPJ,
      Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
    Salvar := Liberado;
  end else
  begin
    // Abrir arquivo aqui!
    ObtemDadosLicencaDeArquivo(Arquivo, DCP_twofish, Arq_AtualizadoEm,
    Arq_SerialHD, Arq_CPUID, Arq_SerialKey (*App User Pwd, Exe On Ini,*) );

    Maq_SerialKey := DmkPF.CalculaValSerialKey_VendaApp(VAR_CPF_CNPJ, Maq_SerialHD, Maq_CPUID);
    if Arq_SerialKey <> Maq_SerialKey then
    begin
      //App User Pwd := '';
      //Exe On Ini   := '0';
      Liberado := MostraFormLicDados(VAR_CPF_CNPJ,
        Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
      Salvar := Liberado;
    end else
    begin
      //UltAtz := Geral.ValidaDataBR(Arq_AtualizadoEm, (*PermiteZero*) True,
      //(*ForceNextYear*)False, (*MostraMsg*)True);
      UltAtz := Geral.ValidaDataHoraSQL(Arq_AtualizadoEm);
      //
      if LicVendaApp_Dmk.ObtemDataHoraWeb(Agora, 2000) then
      begin
        if Agora - UltAtz > 7 then // mais de uma semana
        begin
          if LicVendaApp_Dmk.LiberaUso(VAR_CPF_CNPJ, VAR_CodigoPIN, CO_DMKID_APP, CO_VERSAO,
          Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey) then
          begin
            Liberado := True;
            Salvar   := True;
          end else
           Halt(0);
          Liberado := True;
        end else
          Liberado := True;
      end else
      begin
        // N�o tem internet?
        Liberado := True;
      end;
    end;
  end;
  if Salvar then
  begin
    //if Exe On Ini = '' then
      //Exe On Ini := '0';
    GeraESalvaJSON(Pasta, Arquivo, VAR_CPF_CNPJ, Maq_AtualizadoEm, Maq_SerialHD,
      Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
  end;
  Result := Liberado;
end;

procedure TUnApp_PF.MostraFormAgendaTarefa_ParaBoss(Agenda: Boolean);
var
  Continua: Boolean;
 // ATaskName: String;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
  begin
    if Agenda then
      _MostraFormAgendaTarefa()
    else
    begin
      dmkPF.UnScheduleRunAtStartup(VAR_ATaskName);
    end;
  end;
end;

function TUnApp_PF.MostraFormItemEdita(const SQLType: TSQLType;
  const SGApps: TStringGrid; const Nome, Pasta, Executavel,
  Parametros, ExecAsAdmin: String): Boolean;
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
  begin
    Application.CreateForm(TFmItemEdita, FmItemEdita);
    FmItemEdita.ImgTipo.SQLTYpe              := SQLType;
    FmItemEdita.EdNome.ValueVariant          := Nome;
    FmItemEdita.EdPasta.ValueVariant         := Pasta;
    FmItemEdita.EdExecutavel.ValueVariant    := Executavel;
    FmItemEdita.EdParametros.ValueVariant    := Parametros;
    FmItemEdita.CkExecAsAdmin.Checked        := Uppercase(ExecAsAdmin) = 'SIM';
    //
    FmItemEdita.FSGApps                      := SGApps;
    //
    FmItemEdita.ShowModal;
    FmItemEdita.Destroy;
  end;
end;

procedure TUnApp_PF.MostraFormOpcoes;
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
  begin
    Application.CreateForm(TFmOpcoesApp, FmOpcoesApp);
    FmOpcoesApp.EdCPF_CNPJ.ValueVariant      := VAR_CPF_CNPJ;
    FmOpcoesApp.EdPosicao.ValueVariant       := VAR_Posicao;
    FmOpcoesApp.EdCodigoPIN.ValueVariant     := VAR_CodigoPIN;
    FmOpcoesApp.EdAppUserPwd.ValueVariant    := VAR_SENHA_BOSS;
    FmOpcoesApp.CkExigeSenhaLoginApp.Checked := VAR_ExigeSenhaLoginApp;
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

function TUnApp_PF.ObtemDadosLicencaDeArquivo(const Arquivo: String;
  const DCP_twofish: TDCP_twofish; var AtualizadoEm, SerialHD, CPUID,
  SerialKey: String): Boolean;
var
  Texto: TStringList;
var
  jsonObj, jSubObj: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  i: Integer;
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Txt, TxtCript: String;
  EstadoJanela: Integer;
  DtUltimaAtz: TDateTime;
  Liberado: Boolean;
begin
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(Arquivo) then Exit;
  Texto := TStringList.Create;
  try
    Txt := TFile.ReadAllText(Arquivo);
    Txt := DeCryptJSON(Txt, DCP_twofish);
    //
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
    if jsonObj = nil then
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        Exit;
    VAR_CPF_CNPJ  := MyJSON.JObjValStr(jsonObj, 'CPF_CNPJ');
    SerialHD      := MyJSON.JObjValStr(jsonObj, 'SerialHD');
    CPUID         := MyJSON.JObjValStr(jsonObj, 'CPUID');
    SerialKey     := MyJSON.JObjValStr(jsonObj, 'SerialKey');
    AtualizadoEm  := MyJSON.JObjValStr(jsonObj, 'AtualizadoEm');
    //App User Pwd    := MyJSON.JObjValStr(jsonObj, 'App User Pwd');
    //Exe On Ini      := MyJSON.JObjValStr(jsonObj, 'Exe On Ini');
    //if Exe On Ini <> '1' then
      //Exe On Ini := '0';
    DtUltimaAtz   := Geral.ValidaDataHoraSQL(AtualizadoEm);
  finally
    Texto.Free;
  end;
end;

function TUnApp_PF.ObtemDadosOpcoesAppDeArquivo(const Arquivo: String;
  const DCP_twofish: TDCP_twofish): Boolean;
var
  Texto: TStringList;
  Txt: String;
  jsonObj: TJSONObject;
begin
  Result := False;
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(Arquivo) then
  begin
    App_PF.MostraFormOpcoes();
    Halt(0);
  end else
  begin
    Texto := TStringList.Create;
    try
      Txt := TFile.ReadAllText(Arquivo);
      Txt := DeCryptJSON(Txt, DCP_twofish);
      //
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
        if jsonObj = nil then
          Exit;
      //
      VAR_CPF_CNPJ           := MyJSON.JObjValStr(jsonObj, 'CPF_CNPJ');
      VAR_SENHA_BOSS         := MyJSON.JObjValStr(jsonObj, 'SENHA_BOSS');
      VAR_Posicao            := MyJSON.JObjValInt(jsonObj, 'Posicao');
      VAR_CodigoPIN          := MyJSON.JObjValStr(jsonObj, 'CodigoPIN');
      VAR_ExigeSenhaLoginApp := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExigeSenhaLoginApp'));
      //
      Result := True;
    finally
      Texto.Free;
    end;
  end;
end;

function TUnApp_PF.ObtemNomeArquivoLicenca(var Arquivo, Pasta: String): Boolean;
begin
  Arquivo := '';
  Pasta := GetSpecialFolderPath(CSIDL_COMMON_DOCUMENTS);
  Pasta := IncludeTrailingPathDelimiter(Pasta) + 'Dermatek\RestApp\';
  Arquivo := Pasta + 'RestApp.dmklic';
  Result := True;
end;

function TUnApp_PF.ObtemNomeArquivoOpcoes: String;
var
  Pasta: String;
begin
  Result := '';
  Pasta    := GetSpecialFolderPath(CSIDL_COMMON_DOCUMENTS);
  Pasta    := IncludeTrailingPathDelimiter(Pasta) + 'Dermatek\RestApp\';
  //
  Result := Pasta + 'RestApp.dmkopt';
end;

function TUnApp_PF.ObtemNomeArquivoTarefa(Application_Name: String): String;
var
  Pasta: String;
begin
  Result := '';
  Pasta := CO_DIR_RAIZ_DMK + '\' + Application_Name + '\Data\';
  ForceDirectories(Pasta);
  Result := Pasta + 'TarefaUnica.dmkafr';
end;

procedure TUnApp_PF._MostraFormAgendaTarefa();
begin
  //ScheduleRunAtStartup();
  Application.CreateForm(TFmAgendaTarefa, FmAgendaTarefa);
  FmAgendaTarefa.ShowModal;
  FmAgendaTarefa.Destroy;
end;

end.
