unit ItemEdita;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DBCtrls, ComCtrls, System.IOUtils,
  System.JSON,
  dmkGeral, dmkLabel, dmkImage,
  UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes, dmkCheckBox;

type
  TFmItemEdita = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    EdPasta: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdExecutavel: TdmkEdit;
    EdParametros: TdmkEdit;
    LaSegWaitOnOSReinit: TLabel;
    Label5: TLabel;
    EdSegWaitOnOSReinit: TdmkEdit;
    EdSegWaitScanMonit: TdmkEdit;
    SbFileOpen: TSpeedButton;
    CkExecAsAdmin: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbFileOpenClick(Sender: TObject);
  private
    { Private declarations }
    function Salva(): Boolean;

  public
    { Public declarations }
    FSGApps: TStringGrid;
  end;

  var
  FmItemEdita: TFmItemEdita;

implementation

uses UnMinimumMyObjects, Principal, UnApp_PF;

{$R *.DFM}

procedure TFmItemEdita.BtOKClick(Sender: TObject);
var
  Linha: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Linha := FSGApps.Row;
  case SQLType of
    stIns:
    begin
      MyObjects.InsereLinhaStringGrid(FSGApps, False);
      Linha := Linha + 1;
    end;
    stUpd: ;
    else
    begin
      Geral.MB_Erro('SQLTYpe indefinido!');
      Exit;
    end;
  end;
  FSGApps.Cells[1, Linha] := EdNome.Text;
  FSGApps.Cells[2, Linha] := EdPasta.Text;
  FSGApps.Cells[3, Linha] := EdExecutavel.Text;
  FSGApps.Cells[4, Linha] := EdParametros.Text;
  if CkExecAsAdmin.Checked then
    FSGApps.Cells[5, Linha] := 'SIM'
  else
    FSGApps.Cells[5, Linha] := 'N�O';
  //
  if Salva then Close;
end;

procedure TFmItemEdita.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmItemEdita.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmItemEdita.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

function TFmItemEdita.Salva(): Boolean;
var
  i: Integer;
  lJsonObj: TJSONObject;
  JSONColor: TJSONObject;
  JSONArray : TJSONArray;
  Nome, Pasta, Executavel, Parametros, ExecAsAdmin: String;
  EstadoJanela: Integer;
  TxtCript: String;
  SegWaitOnOSReinit, SegWaitScanMonit: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    lJsonObj     := TJSONObject.Create;
    //
    //LinCom       := EdLinCom.Text;
    //Processo     := EdProcesso.Text;
    //EstadoJanela := AppPF.EstadoJanelaToHWND_CmdShow(RGEstadoJanela.ItemIndex);


    SegWaitOnOSReinit     := EdSegWaitOnOSReinit.ValueVariant;
    SegWaitScanMonit      := EdSegWaitScanMonit.ValueVariant;

    lJsonObj.AddPair('SegWaitOnOSReinit',     Geral.FF0(SegWaitOnOSReinit    ));
    lJsonObj.AddPair('SegWaitScanMonit',      Geral.FF0(SegWaitScanMonit     ));
    //
    JSONArray := TJSONArray.Create();
    //MemTable.First;
    //while not MemTable.Eof do
    for I := 1 to FSGApps.RowCount - 1 do
    begin
      Nome         := FSGApps.Cells[1, I];
      Pasta        := FSGApps.Cells[2, I];
      Executavel   := FSGApps.Cells[3, I];
      Parametros   := FSGApps.Cells[4, I];
      ExecAsAdmin  := FSGApps.Cells[5, I];

      JSONColor := TJSONObject.Create();
      //
      JSONColor.AddPair('Item',        Geral.FF0(I));
      JSONColor.AddPair('Nome',        Geral.JsonText(Nome));
      JSONColor.AddPair('Pasta',       Geral.JsonText(Pasta));
      JSONColor.AddPair('Executavel',  Geral.JsonText(Executavel));
      JSONColor.AddPair('Parametros',  Geral.JsonText(Parametros));
      JSONColor.AddPair('ExecAsAdmin', Geral.JsonText(ExecAsAdmin));

      JSONArray.Add(JSONColor);
      //
    end;
    lJsonObj.AddPair('TarefasIts', JSONArray);
    //
    TxtCript := App_PF.EnCryptJSON(lJsonObj.ToString, FmPrincipal.DCP_twofish1);
    TFile.WriteAllText(App_PF.ObtemNomeArquivoTarefa(Application.Name), TxtCript, TEncoding.ANSI);

    //btnsave.Caption := 'Load Data';
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmItemEdita.SbFileOpenClick(Sender: TObject);
var
  Arq: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Arq) then
  begin
    EdPasta.Text := ExtractFilePath(Arq);
    EdExecutavel.Text := ExtractFileName(Arq);
  end;
end;

end.
