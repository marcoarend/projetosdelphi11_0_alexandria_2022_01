program Restapp;

uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  dmkGeral in '..\..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnApp_Consts in '..\Units\UnApp_Consts.pas',
  UnApp_Vars in '..\Units\UnApp_Vars.pas',
  UnApp_PF in '..\Units\UnApp_PF.pas',
  AgendaTarefa in '..\..\..\..\..\UTL\_FRM\v01_01\AgendaTarefa.pas' {FmAgendaTarefa},
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  Dmk_RestApp in '..\Units\Dmk_RestApp.pas' {FmDmk_RestApp},
  UnMinimumMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMinimumMyObjects.pas',
  UnMinimumDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnMinimumDmkProcFunc.pas',
  OpcoesApp in '..\Units\OpcoesApp.pas' {FmOpcoesApp},
  UnMyJSON in '..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  MyListas in '..\Units\MyListas.pas',
  LicDados in '..\Units\LicDados.pas' {FmLicDados},
  UnLicVendaApp_Dmk in '..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  UnAppMainPF in '..\Units\UnAppMainPF.pas',
  ItemEdita in '..\Units\ItemEdita.pas' {FmItemEdita},
  UnMyProcesses in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyProcesses.pas';

{$R *.res}

begin
  //UnMyVclEvents in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  //
  //DELPHI12_UP;uDmkPFMin;uMyObjectsMin;NAO_USA_GLYFS;DEBUG;NO_USE_MYSQLMODULE
  //DELPHI12_UP;uDmkPFMin;uMyObjectsMin;NAO_USA_GLYFS;NO_USE_MYSQLMODULE;DEBUG
  //
  Application.Initialize;
  Application.Name := CO_App_Name;
  TStyleManager.TrySetStyle('Windows10 Dark');
  Application.Title := CO_App_Titl;
  VAR_ATaskName := Application.Name + ' as admin';
  //
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
