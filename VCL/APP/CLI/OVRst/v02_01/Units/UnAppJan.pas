unit UnAppJan;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, dmkGeral, UnInternalConsts, UnGrl_Vars;

type
  TUnAppJan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }

    procedure MostraVerificaConexoes();
  end;

var
  AppJan: TUnAppJan;

implementation

uses

  VerificaConexoes;

{ TUnAppJan }

procedure TUnAppJan.MostraVerificaConexoes();
begin
  Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
end;

end.
