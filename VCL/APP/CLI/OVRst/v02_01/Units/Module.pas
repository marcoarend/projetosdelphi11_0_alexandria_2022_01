unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, MySQLBatch, dmkGeral, UnDmkEnums, mySQLDirectQuery;

type
  TDMod = class(TDataModule)
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrAux: TmySQLQuery;
    QrUpd: TmySQLQuery;
    MyDB: TmySQLDatabase;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterENumero: TFloatField;
    QrUpdZ: TmySQLQuery;
    QlLocal: TMySQLBatchExecute;
    MyLocDatabase: TmySQLDatabase;
    QrAuxL: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrInsL: TmySQLQuery;
    DBAgendaAtu: TmySQLDatabase;
    QrOVSSvcOpt: TMySQLQuery;
    QrOVSSvcOptDtHrLastLog: TDateTimeField;
    QrAgora: TMySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrOVSSvcOptOVS_Svc_RndmStr: TWideStringField;
    ZZDB: TMySQLDatabase;
    QrIdx: TMySQLQuery;
    QrMas: TMySQLQuery;
    QrPriorNext: TMySQLQuery;
    QrSQL: TMySQLQuery;
    QrNTV: TMySQLQuery;
    QrNTI: TMySQLQuery;
    QrUpdU: TMySQLQuery;
    QrTerminal: TMySQLQuery;
    QrTerminalTerminal: TIntegerField;
    DqAux: TMySQLDirectQuery;
    QrOPcoesGrl: TMySQLQuery;
    QrOPcoesGrlERPNameByCli: TWideStringField;
    QrOpcoesApp: TMySQLQuery;
    QrOpcoesAppLoadCSVOthIP: TWideStringField;
    QrOpcoesAppLoadCSVOthDir: TWideStringField;
    QrOpcoesAppOVpLayEsq: TIntegerField;
    QrOpcoesAppLoadCSVIntExt: TSmallintField;
    QrOpcoesAppLoadCSVSoLote: TSmallintField;
    QrOpcoesAppLoadCSVTipOP: TWideStringField;
    QrOpcoesAppLoadCSVSitOP: TWideStringField;
    QrOpcoesAppLoadCSVEmpresas: TWideStringField;
    QrOpcoesAppLoadCSVTipLoclz: TWideStringField;
    QrOpcoesAppLoadCSVTipProdOP: TWideStringField;
    QrOpcoesAppEntiTipCto_Inspecao: TIntegerField;
    QrOpcoesAppLoadCSVIntrv: TIntegerField;
    QrOpcoesAppMailResInspResul: TWideStringField;
    QrOpcoesAppForcaReabCfgInsp: TSmallintField;
    QrOpcoesAppLoadCSVIntEx2: TSmallintField;
    QrOpcoesAppLoadCSVSoLot2: TSmallintField;
    QrOpcoesAppLoadCSVEmpresa2: TWideStringField;
    QrOpcoesAppLoadCSVTipO2: TWideStringField;
    QrOpcoesAppLoadCSVSitO2: TWideStringField;
    QrOpcoesAppLoadCSVTipLocl2: TWideStringField;
    QrOpcoesAppLoadCSVTipProdO2: TWideStringField;
    QrOpcoesAppMailResInspResu2: TWideStringField;
    QrOpcoesAppLoadCSVCodNomLocal: TIntegerField;
    QrOpcoesAppLoadCSVCodValr1: TWideStringField;
    QrOpcoesAppLoadCSVCodValr2: TWideStringField;
    QrOpcoesAppCdScConfeccao: TIntegerField;
    QrOpcoesAppCdScTecelagem: TIntegerField;
    QrOpcoesAppCdScTinturaria: TIntegerField;
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrOpcoesAppAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    function GetRandomStr(): String;

  public
    { Public declarations }
    procedure VerificaSenha(Tipo: Integer; Login, Senha: String);
    function Privilegios(Usuario : Integer) : Boolean;
    //
    function  TentaConectar(): Boolean;
    function  PodeLerDir(): Boolean;
    procedure ReopenOpcoesApp();
    // Do ModuleGeral
    function DModG_ObtemAgora(UTC: Boolean = False): TDateTime;
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure ReopenControle();
    function  TabelasQueNaoQueroCriar(): String;
  end;

var
  DMod: TDMod;

implementation

uses UnMLAGeral, UnInternalConsts, UnDmkProcFunc, UnAppJan, UnApp_Vars,
  DmkDAC_PF, UMySQLDB, UnProjGroup_PF, UnOVS_ProjGroupVars,
  (*ModuleGeral,*) Principal;

{$R *.DFM}

procedure TDMod.VerificaSenha(Tipo: Integer; Login, Senha: String);
begin
  // Compatibilidade
end;

procedure TDMod.DataModuleCreate(Sender: TObject);
begin
  VAR_RANDM_STR_ONLY_ONE_ACESS := GetRandomStr(); //definir j� no principal?
  QrUpd.Database := MyDB;
  QrAux.Database := MyDB;
  //ReopenOpcoesApp();
end;

function TDMod.DModG_ObtemAgora(UTC: Boolean): TDateTime;
///////////////////////////////////
// ver tamb�m > ObtemDataHora    //
///////////////////////////////////
var
  Hoje: TDateTime;
  Agora: TTime;
begin
  if UTC = True then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAgora, Dmod.MyDB, [
      'SELECT YEAR(UTC_TIMESTAMP()) ANO, MONTH(UTC_TIMESTAMP()) MES, ',
      'DAYOFMONTH(UTC_TIMESTAMP()) DIA, ',
      'HOUR(UTC_TIMESTAMP()) HORA, MINUTE(UTC_TIMESTAMP()) MINUTO, ',
      'SECOND(UTC_TIMESTAMP()) SEGUNDO, UTC_TIMESTAMP() AGORA ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAgora, Dmod.MyDB, [
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES, ',
      'DAYOFMONTH(NOW()) DIA, ',
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO, ',
      'SECOND(NOW()) SEGUNDO, NOW() AGORA ',
      '']);
  end;
  Hoje   := EncodeDate(QrAgoraAno.Value, QrAgoraMes.Value, QrAgoraDia.Value);
  Agora  := EncodeTime(QrAgoraHora.Value, QrAgoraMINUTO.Value, QrAgoraSEGUNDO.Value, 0);
  Result := Hoje + Agora;
end;

function TDMod.GetRandomStr(): String;
  function Randomico(): Integer;
  begin
    {$IfDef MSWINDOWS}
      Result := Random(62) + 1;
    {$Else}
      Result := Random(62);
    {$EndIF}
  end;
const
  StrLen = 32;
var
  Str, sAnt, sAtu: String;
begin
  Str := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  Result := '';
  sAnt := '';
  Randomize;
  repeat
    sAtu := Str[Randomico()];
    if sAtu = sAnt then
    begin
      while sAtu = sAnt do
      begin
        Randomize;
        sAtu := Str[Randomico()];
      end;
    end;
    Result := Result + sAtu;
    sAnt := sAtu;
  until (Length(Result) = StrLen);
end;

function TDMod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  QrPerfis.Close;
  if Usuario > -1000 then
  begin
    QrPerfis.SQL.Clear;
    QrPerfis.SQL.Add('SELECT pip.Libera, pit.Janela');
    QrPerfis.SQL.Add('FROM perfisits pit');
    QrPerfis.SQL.Add('LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela');
    QrPerfis.SQL.Add('AND pip.Codigo='+IntToStr(Usuario)); // Condi��o do LEFT JOIN
  end;
  QrPerfis.Open;
  if QrPerfis.RecordCount > 0 then Result := True;
end;

procedure TDMod.QrMasterCalcFields(DataSet: TDataSet);
begin
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDMod.QrOpcoesAppAfterOpen(DataSet: TDataSet);
begin
  VAR_LoadCSVIntrv := QrOpcoesAppLoadCSVIntrv.Value * 60 * 1000;
  if FmPrincipal.TmLoadCSV.Interval <> VAR_LoadCSVIntrv then
  begin
    FmPrincipal.TmLoadCSV.Enabled := False;
    FmPrincipal.TmLoadCSV.Interval := VAR_LoadCSVIntrv;
    FmPrincipal.TmLoadCSV.Enabled := True;
  end else
    if FmPrincipal.TmLoadCSV.Enabled = False then
      FmPrincipal.TmLoadCSV.Enabled := True;
end;

procedure TDMod.ReopenControle;
begin
  // Compatibilidade
end;

procedure TDMod.ReopenOpcoesApp();
begin
  UnDmkDAC_PF.AbreQuery(QrOpcoesApp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOpcoesGrl, Dmod.MyDB);
end;

procedure TDMod.ReopenParamsEspecificos(Empresa: Integer);
begin
// Compatibilidade
end;

function TDMod.TabelasQueNaoQueroCriar(): String;
begin
  // Compatibilidade
  Result := '';
end;

function TDMod.TentaConectar(): Boolean;
var
  IPServ, OthSetCon: String;
begin
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
  begin
    AppJan.MostraVerificaConexoes();
  end else
  begin
    IPServ    := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
    //VAR_IP      := 'localhost'; // S� local! (no servidor)
    VAR_IP      := IPServ; // 'localhost'; // S� local! (no servidor)
    //
    VAR_PORTA   := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
    VAR_SQLUSER := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, '');
    OthSetCon   := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
    VAR_DB      := Geral.ReadAppKeyCU('Database', Application.Title, ktString, '');
    if MyDB.Connected then
      MyDB.Disconnect;
    //
    MyDB.Host         := VAR_IP;
    MyDB.Port         := VAR_PORTA;
    MyDB.DatabaseName := VAR_DB;
    MyDB.UserName     := VAR_SQLUSER;
    MyDB.UserPassword := VAR_BDSENHA;
    //
    try
      Screen.Cursor := crHourGlass;
      //
      //MyDB.Connect;
      USQLDB.ConectaDB(MyDB, 'Module.288');
      Screen.Cursor := crDefault;
    except
      Screen.Cursor := crDefault;
    end;
    if MyDB.Connected then
    begin
      //Geral.MB_Info('Conex�o com o banco de dados estabelecida com �xito!');
      ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkRunOK,
        'Conex�o com o banco de dados estabelecida com �xito!');
    end else
    begin
      //Geral.MB_Erro('Falha ao estabelecer uma conex�o com o banco de dados!');
      ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkGetErr,
        'Falha ao estabelecer uma conex�o com o banco de dados!');
    end;
  end;
end;

function TDMod.PodeLerDir(): Boolean;
const
  Codigo = 1;
var
  LoadCSVOthDir, EntiTipCto_Inspecao, DtHrLastLog, CodStr, OVS_Svc_RndmStr: String;
  Agora, DifTime: TDateTime;
begin
  Result := False;
  CodStr := Geral.FF0(Codigo);
  if MyDB.Connected then
  begin
    //
    ReopenOpcoesApp();
    //
    Agora   := DModG_ObtemAgora();
    UnDmkDAC_PF.AbreMySQLQuery0(QrOVSSvcOpt, MyDB, [
    'LOCK TABLES ovssvcopt WRITE; ',
    'SELECT OVS_Svc_RndmStr, DtHrLastLog ',
    'FROM ovssvcopt ',
    'WHERE Codigo=' + CodStr + ' ',
    '']);
    try
      //
      DifTime := Agora - QrOVSSvcOptDtHrLastLog.Value;
      //
      if (QrOVSSvcOptOVS_Svc_RndmStr.Value = EmptyStr)
      or (QrOVSSvcOptOVS_Svc_RndmStr.Value = VAR_RANDM_STR_ONLY_ONE_ACESS)
      or (DifTime >= 0.05 (*72 minutos*)) then
      begin
        DtHrLastLog := Geral.FDT(Agora, 109);
        OVS_Svc_RndmStr := VAR_RANDM_STR_ONLY_ONE_ACESS;
        //
        if USQLDB.SQLInsUpd(QrUpd, stUpd, 'ovssvcopt', False, [
        'OVS_Svc_RndmStr', 'DtHrLastLog'], ['Codigo'], [
        OVS_Svc_RndmStr, DtHrLastLog], [Codigo], True) then
        begin
          try
            Result := True;
            //Geral.MB_Info('Antes do unlock OK!');
          finally
          end;
        end;
      end;
    finally
      MyDB.Execute('UNLOCK TABLES');
    end;
  end else
    ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkGetErr,
      'Banco de dados desconectado ao iniciar escaneamento!');
end;

{
object QrOpcoesAppLoadCSVAtbNomLocal: TWideStringField
  FieldName = 'LoadCSVAtbNomLocal'
  Size = 255
end
object QrOpcoesAppLoadCSVAtbValr1: TWideStringField
  FieldName = 'LoadCSVAtbValr1'
  Size = 255
end
object QrOpcoesAppLoadCSVAtbValr2: TWideStringField
  FieldName = 'LoadCSVAtbValr2'
  Size = 255
end
object QrOpcoesAppSecConfeccao: TWideStringField
  FieldName = 'SecConfeccao'
  Size = 60
end
object QrOpcoesAppSecTecelagem: TWideStringField
  FieldName = 'SecTecelagem'
  Size = 60
end
object QrOpcoesAppSecTinturaria: TWideStringField
  FieldName = 'SecTinturaria'
  Size = 60
end
}
end.

