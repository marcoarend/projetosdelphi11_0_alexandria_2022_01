u n i t   B k A g e n d a ;  
 i n t e r f a c e  
  
 u s e s  
     W i n d o w s ,   M e s s a g e s ,   S y s U t i l s ,   C l a s s e s ,   G r a p h i c s ,   C o n t r o l s ,   F o r m s ,   D i a l o g s ,  
     E x t C t r l s ,   B u t t o n s ,   S t d C t r l s ,   D B C t r l s ,   D b ,   D B T a b l e s ,   E x t D l g s ,   Z C F 2 ,  
     R e s I n t S t r i n g s ,   U n   U n I n t e r n a l C o n s t s ,   U n M s g I n t ,  
     U n I n t e r n a l C o n s t s 2 ,   m y S Q L D b T a b l e s ,   d m k G e r a l ,  
     d m k P e r m i s s o e s ,   d m k E d i t ,   d m k L a b e l ,   d m k D B E d i t ,   M a s k ;  
  
 t y p e  
     T F m B k A g e n d a   =   c l a s s ( T F o r m )  
         P a i n e l D a d o s :   T P a n e l ;  
         D s B k A g e n d a :   T D a t a S o u r c e ;  
         Q r B k A g e n d a :   T m y S Q L Q u e r y ;  
         P a i n e l T i t u l o :   T P a n e l ;  
         L a T i p o :   T d m k L a b e l ;  
         I m a g e 1 :   T I m a g e ;  
         P n P e s q :   T P a n e l ;  
         S b I m p r i m e :   T B i t B t n ;  
         S b N o v o :   T B i t B t n ;  
         S b N u m e r o :   T B i t B t n ;  
         S b N o m e :   T B i t B t n ;  
         S b Q u e r y :   T B i t B t n ;  
         P a i n e l E d i t a :   T P a n e l ;  
         P a i n e l C o n f i r m a :   T P a n e l ;  
         B t C o n f i r m a :   T B i t B t n ;  
         P a i n e l C o n t r o l e :   T P a n e l ;  
         L a R e g i s t r o :   T S t a t i c T e x t ;  
         P a n e l 5 :   T P a n e l ;  
         S p e e d B u t t o n 4 :   T B i t B t n ;  
         S p e e d B u t t o n 3 :   T B i t B t n ;  
         S p e e d B u t t o n 2 :   T B i t B t n ;  
         S p e e d B u t t o n 1 :   T B i t B t n ;  
         P a n e l 3 :   T P a n e l ;  
         B t E x c l u i :   T B i t B t n ;  
         B t A l t e r a :   T B i t B t n ;  
         B t I n c l u i :   T B i t B t n ;  
         P a i n e l E d i t :   T P a n e l ;  
         E d C o d i g o :   T d m k E d i t ;  
         P a i n e l D a t a :   T P a n e l ;  
         L a b e l 1 :   T L a b e l ;  
         D B E d C o d i g o :   T d m k D B E d i t ;  
         D B E d N o m e :   T d m k D B E d i t ;  
         L a b e l 2 :   T L a b e l ;  
         P a n e l 1 :   T P a n e l ;  
         B t D e s i s t e :   T B i t B t n ;  
         P a n e l 2 :   T P a n e l ;  
         B t S a i d a :   T B i t B t n ;  
         E d N o m e :   T d m k E d i t ;  
         d m k P e r m i s s o e s 1 :   T d m k P e r m i s s o e s ;  
         L a b e l 3 :   T L a b e l ;  
         D B E d i t 1 :   T D B E d i t ;  
         L a b e l 7 :   T L a b e l ;  
         L a b e l 8 :   T L a b e l ;  
         L a b e l 9 :   T L a b e l ;  
         E d C o d U s u :   T d m k E d i t ;  
         Q r B k A g e n d a C o d i g o :   T I n t e g e r F i e l d ;  
         Q r B k A g e n d a C o d U s u :   T I n t e g e r F i e l d ;  
         Q r B k A g e n d a N o m e :   T S t r i n g F i e l d ;  
         E d H o s t :   T d m k E d i t ;  
         L a b e l 4 :   T L a b e l ;  
         L a b e l 5 :   T L a b e l ;  
         E d D b :   T d m k E d i t ;  
         L a b e l 6 :   T L a b e l ;  
         E d D e s t i n o :   T d m k E d i t ;  
         L a b e l 1 0 :   T L a b e l ;  
         E d H o r a :   T d m k E d i t ;  
         p r o c e d u r e   S p e e d B u t t o n 1 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S p e e d B u t t o n 2 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S p e e d B u t t o n 3 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S p e e d B u t t o n 4 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t A l t e r a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t S a i d a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t C o n f i r m a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t D e s i s t e C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S b N u m e r o C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S b N o m e C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m C l o s e ( S e n d e r :   T O b j e c t ;   v a r   A c t i o n :   T C l o s e A c t i o n ) ;  
         p r o c e d u r e   Q r B k A g e n d a A f t e r O p e n ( D a t a S e t :   T D a t a S e t ) ;  
         p r o c e d u r e   F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   S b Q u e r y C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   Q r B k A g e n d a B e f o r e O p e n ( D a t a S e t :   T D a t a S e t ) ;  
         p r o c e d u r e   B t I n c l u i C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   E d C o d U s u K e y D o w n ( S e n d e r :   T O b j e c t ;   v a r   K e y :   W o r d ;  
             S h i f t :   T S h i f t S t a t e ) ;  
         p r o c e d u r e   S b N o v o C l i c k ( S e n d e r :   T O b j e c t ) ;  
     p r i v a t e  
         p r o c e d u r e   C r i a O F o r m ;  
         p r o c e d u r e   D e f i n e O N o m e D o F o r m ;  
         p r o c e d u r e   Q u e r y P r i n c i p a l A f t e r O p e n ;  
         / / p r o c e d u r e   I n c l u i R e g i s t r o ;  
         / / p r o c e d u r e   A l t e r a R e g i s t r o ;  
       / / / / P r o c e d u r e s   d o   f o r m  
         p r o c e d u r e   M o s t r a E d i c a o ( M o s t r a :   I n t e g e r ;   S Q L T y p e :   T S Q L T y p e ;   C o d i g o :   I n t e g e r ) ;  
         p r o c e d u r e   D e f P a r a m s ;  
         p r o c e d u r e   L o c C o d ( A t u a l ,   C o d i g o :   I n t e g e r ) ;  
         p r o c e d u r e   V a ( P a r a :   T V a i P a r a ) ;  
     p u b l i c  
         {   P u b l i c   d e c l a r a t i o n s   }  
     e n d ;  
  
 v a r  
     F m B k A g e n d a :   T F m B k A g e n d a ;  
 c o n s t  
     F F o r m a t F l o a t   =   ' 0 0 0 0 0 ' ;  
  
 i m p l e m e n t a t i o n  
  
 u s e s   M o d u l e ;  
  
 { $ R   * . D F M }  
  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
 p r o c e d u r e   T F m B k A g e n d a . L o c C o d ( A t u a l ,   C o d i g o :   I n t e g e r ) ;  
 b e g i n  
     {  
     D e f P a r a m s ;  
     G O T O y . L C ( A t u a l ,   C o d i g o ) ;  
     }  
 e n d ;  
  
 p r o c e d u r e   T F m B k A g e n d a . V a ( P a r a :   T V a i P a r a ) ;  
 b e g i n  
     {  
     D e f P a r a m s ;  
     L a R e g i s t r o . C a p t i o n   : =   G O T O y . G o ( P a r a ,   Q r B k A g e n d a C o d i g o . V a l u e ,   L a R e g i s t r o . C a p t i o n [ 2 ] ) ;  
     }  
 e n d ;  
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /  
  
 p r o c e d u r e   T F m B k A g e n d a . D e f P a r a m s ;  
 b e g i n  
     {  
     V A R _ G O T O T A B E L A   : =   ' b k a g e n d a ' ;  
     V A R _ G O T O M Y S Q L T A B L E   : =   Q r B k A g e n d a ;  
     V A R _ G O T O N E G   : =   g o t o P o s ;  
     V A R _ G O T O C A M P O   : =   C O _ C O D I G O ;  
     V A R _ G O T O N O M E   : =   C O _ N O M E ;  
     V A R _ G O T O M y S Q L D B N A M E   : =   D m o d . M y D B ;  
     V A R _ G O T O V A R   : =   0 ;  
  
     G O T O y . L i m p a V A R _ S Q L ;  
  
     V A R _ S Q L x . A d d ( ' S E L E C T   C o d i g o ,   C o d U s u ,   N o m e ' ) ;  
     V A R _ S Q L x . A d d ( ' F R O M   b k a g e n d a ' ) ;  
     V A R _ S Q L x . A d d ( ' W H E R E   C o d i g o   >   0 ' ) ;  
     / /  
     V A R _ S Q L 1 . A d d ( ' A N D   C o d i g o = : P 0 ' ) ;  
     / /  
     V A R _ S Q L 2 . A d d ( ' A N D   C o d U s u = : P 0 ' ) ;  
     / /  
     V A R _ S Q L a . A d d ( ' A N D   N o m e   L i k e   : P 0 ' ) ;  
     / /  
     }  
 e n d ;  
  
 p r o c e d u r e   T F m B k A g e n d a . E d C o d U s u K e y D o w n ( S e n d e r :   T O b j e c t ;   v a r   K e y :   W o r d ;  
     S h i f t :   T S h i f t S t a t e ) ;  
 b e g i n  
 {  
     i f   ( K e y   =   V K _ F 4 )   a n d   ( L a T i p o . S Q L T y p e   =   s t I n s )   t h e n  
         E d C o d U s u . V a l u e V a r i a n t   : =  
         U M y M o d . B u s c a N o v o C o d i g o _ I n t ( D m o d . Q r A u x ,   ' b k a g e n d a ' ,   ' C o d U s u ' ,   [ ] ,   [ ] ) ;  
 }  
 e n d ;  
  
 p r o c e d u r e   T F m B k A g e n d a . M o s t r a E d i c a o ( M o s t r a :   I n t e g e r ;   S Q L T y p e :   T S Q L T y p e ;   C o d i g o :   I n t e g e r ) ;  
 b e g i n  
