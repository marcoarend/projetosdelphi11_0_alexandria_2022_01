unit Profil;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, Buttons, Mask,IniFiles;

type
  TFProfil = class(TForm)
    Panel1: TPanel;
    Label7: TLabel;
    TS: TTabControl;
    EdDB: TEdit;
    EdUser: TEdit;
    EdPwd: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CBHosts: TComboBox;
    EdProfil: TEdit;
    Label5: TLabel;
    EdHost: TEdit;
    BtOK: TBitBtn;
    BtSair: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CBHostsChange(Sender: TObject);
    Function LadeProfil(Nr:integer):boolean;
    Function SaveProfil(Nr:integer):boolean;
    procedure LadeAlleProfile;
    procedure BtSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
  private
    { Private-Deklarationen }
    sHostProfil:string;
  public
    { Public-Deklarationen }
  end;

var
  FProfil: TFProfil;

implementation

uses WetBlueMLA_BK, UnMLAGeral;

{$R *.DFM}

const Kodieren=1;
      DeKodieren=2;

Function CryptPwd(Mode,ChKey:integer; Value:string):string;
var s : string[255];
    c : array[0..255] of byte absolute s;
    i,xkey : Integer;
begin
  try
      xkey:=Ord(ChKey);
      s := Value;
      if trim(s) <> '' then begin
        if Mode = Kodieren then begin
            for i := 1 To ord(s[0]) do begin
             c[i] := xkey Xor c[i];
             Result :=Result+FormatFloat('000', ord(c[i]));
            end;
        end else  begin
            s:='';
            i:=1;
            while i < Length(Value) do begin
                s:=s+Char(StrToInt(copy(Value,i,3)));
                i:=i+3;
            end;
            for i := 1 To Length(s) do s[i] := chr(xkey Xor ord(s[i]));
            Result := s;
        end;
      end;
  except
    Result :='';
  end;
end;


procedure TFProfil.FormShow(Sender: TObject);
begin
  CBHosts.Clear;
  CBHosts.Items.Assign(FmWetBlueMLA_BK.CBHosts.Items);
  CBHosts.ItemIndex:=FmWetBlueMLA_BK.CBHosts.ItemIndex;
  CBHostsChange(Self);
end;

procedure TFProfil.BtOKClick(Sender: TObject);
var ActIndex:integer;
begin
  ActIndex:=CBHosts.ItemIndex;
  if TS.TabIndex=0 then begin
     SaveProfil(CBHosts.ItemIndex+1);
  end else begin
    if Application.MessageBox('Inclui novo acesso?', 'Acesso', MB_OKCANCEL)
    = ID_OK then begin
      SaveProfil(CBHosts.Items.count+1);
      ActIndex:=CBHosts.Items.count;
    end;
  end;
 LadeAlleProfile;
 CBHosts.ItemIndex:=ActIndex;
 CBHostsChange(Self);
end;

procedure TFProfil.CBHostsChange(Sender: TObject);
begin
  LadeProfil(CBHosts.ItemIndex+1);
  EdProfil.Text:=sHostProfil;
  edHost.Text:=SMyHost;
  edDb.Text:=SMyDB;
  edUser.Text:=SUser;
  edPwd.Text:=SPwd;
end;


Function TFProfil.LadeProfil(Nr:integer):boolean;
 var
  IniFile: TIniFile;
  sHost: string;
  xkey:integer;
begin
  Result:=false;
  xkey:=33;
  if not FileExists(ExtractFilePath(Application.ExeName)+'\Hosts.ini') then
  begin
    IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\Hosts.ini');
    try
      IniFile.WriteString('Host1', 'PrName', 'Novo Host');
      IniFile.WriteString('Host1', 'IP_Nr', 'localhost');
      IniFile.WriteString('Host1', 'DB', 'mysql');
      IniFile.WriteString('Host1', 'User', 'root');
      IniFile.WriteString('Host1', 'Pw', '');
    finally
     IniFile.Free;
    end;
    sHost:='Host'+InttoStr(Nr);
    IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\Hosts.ini');
    try
      sHostProfil:=IniFile.ReadString(sHost, 'PrName', '???');
      SMyHost:=IniFile.ReadString(sHost, 'IP_Nr', '');
      SMyDB:=IniFile.ReadString(sHost, 'DB', '');
      SUser:=IniFile.ReadString(sHost, 'User', '');
      if length(SUser) > 1 then xkey:= ord(SUser[1]);
      SPwd:=CryptPwd(DeKodieren,xkey, IniFile.ReadString(sHost, 'Pw', ''));
    finally
      IniFile.Free;
    end;
    Result:= trim(SMyHost) <> '';
  end;
end;

Function TFProfil.SaveProfil(Nr:integer):boolean;
 var
  IniFile: TIniFile;
  sHost: string;
  xkey:integer;

begin
  Result:=false;
  xkey:=33;
  sHost:='Host'+InttoStr(Nr);
  IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\Hosts.ini');
  try
    IniFile.WriteString(sHost, 'PrName', EdProfil.Text);
    IniFile.WriteString(sHost, 'IP_Nr', EdHost.Text);
    IniFile.WriteString(sHost, 'DB', EdDB.Text );
    IniFile.WriteString(sHost, 'User', EdUser.Text);
    if length(SUser) > 1 then xkey:= ord(EdUser.Text[1]);
    IniFile.WriteString(sHost, 'Pw',
          CryptPwd(Kodieren,xkey, trim(EdPwd.Text)));
    Result:=true;
  except
    IniFile.Free;
  end;
end;


procedure TFProfil.LadeAlleProfile;
var i:integer;
begin
  i:=1;
  CBHosts.Clear;
  while true do begin
    if LadeProfil(i) then begin
      CBHosts.Items.add(sHostProfil);
      inc(i);
    end else break;
  end;
  FmWetBlueMLA_BK.CBHosts.Clear;
  FmWetBlueMLA_BK.CBHosts.Items.Assign(CBHosts.Items);
  LadeProfil(1);
  FmWetBlueMLA_BK.CBHosts.ItemIndex:=0;
end;


procedure TFProfil.BtSairClick(Sender: TObject);
begin
  close;
end;

procedure TFProfil.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    FmWetBlueMLA_BK.CBHosts.ItemIndex:=CBHosts.ItemIndex;
  except
  end;
end;

procedure TFProfil.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

end.
