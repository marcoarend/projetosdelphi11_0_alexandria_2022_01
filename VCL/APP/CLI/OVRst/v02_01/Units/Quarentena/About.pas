unit About;

interface

uses Windows, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, Sysutils,ExtCtrls;

type
  TAboutBox = class(TForm)
    OKButton: TButton;
    Panel1: TPanel;
    Version: TLabel;
    Copyright: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Memo1: TMemo;
    LbDumpV: TLabel;
    Panel2: TPanel;
    ProgramIcon: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

{$R *.DFM}

uses WetBlueMLA_BK;

procedure TAboutBox.FormShow(Sender: TObject);
begin
  try
    if FileExists('Info.txt')
       then Memo1.Lines.LoadFromFile('Info.txt');
    LbDumpV.Caption:='DumpVersion: '+FmWetBlueMLA_BK.MySQLBackUp1.DumpVersion;
  except
  end;
end;

procedure TAboutBox.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Action:=caFree;
end;

end.


