unit AppListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, UnMyLinguas, UnInternalConsts, dmkGeral, DB,
  mySQLDbTables, UnDmkProcFunc, UnDmkEnums;

const
  //
  CO_TXT_pfcIndefinido       = 'Indefinido';
  CO_TXT_pfcValorTotal       = 'Valor total';
  CO_TXT_pfcValorUnitario    = 'Valor unit�rio';
  MaxPrmFatorValor = Integer(High(TPrmFatorValor));
  sPrmFatorValor: array[0..MaxPrmFatorValor] of string = (
  CO_TXT_pfcIndefinido       ,
  CO_TXT_pfcValorTotal       ,
  CO_TXT_pfcValorUnitario
  );
  //
  CO_TXT_pfrIndefinido       = 'Indefinido';
  CO_TXT_pfrValorTotal       = 'Valor total';
  CO_TXT_pfrValorPorPessoa   = 'Valor por pessoa';
  MaxPrmFormaRateio = Integer(High(TPrmFormaRateio));
  sPrmFormaRateio: array[0..MaxPrmFormaRateio] of string = (
  CO_TXT_pfrIndefinido       ,
  CO_TXT_pfrValorTotal       ,
  CO_TXT_pfrValorPorPessoa
  );
  //
  CO_TXT_slcdUnknownToInf    = 'A informar';
  CO_TXT_slcdNoGerCtaAPag    = 'N�o gerar contas a pagar';
  CO_TXT_slcdDescoFaturam    = 'Descontar do faturamento';
  CO_TXT_slcdGeraCtaAPagr    = 'Gerar contas a pagar';
  MaxSrvLCtbDsp = Integer(High(TSrvLCtbDsp));
  sSrvLCtbDsp: array[0..MaxSrvLCtbDsp] of string = (
  CO_TXT_slcdUnknownToInf   ,
  CO_TXT_slcdNoGerCtaAPag   ,
  CO_TXT_slcdDescoFaturam   ,
  CO_TXT_slcdGeraCtaAPagr
  );

    // Tabelas GraGruY
  CO_GraGruY_1024_GXPatPri = 1024; // Patrim�nio Primario
  CO_GraGruY_2048_GXPatSec = 2048; // Patrim�nio Secund�rio
  CO_GraGruY_3072_GXPatAce = 3072; // Acess�rio
  CO_GraGruY_4096_GXPatApo = 4096; // Apoio
  CO_GraGruY_5120_GXPatUso = 5120; // Uso
  CO_GraGruY_6144_GXPrdCns = 6144; // Consumo
  CO_GraGruY_7168_GXPatSrv = 7168; // Servi�o
  CO_GraGruY_8192_GXPrdVen = 8192; // Produto de Venda
  //
  CO_TXT_GraGruY_1024_TXCadNat = 'Mat�ria-prima';
  (*CO_TXT_GraGruY_1195_TXNatPDA = 'Mat�ria-prima PDA';
  CO_TXT_GraGruY_1365_TXProCal = 'Couro em Caleiro';
  CO_TXT_GraGruY_1536_TXCouCal = 'Couro Caleirado';
  CO_TXT_GraGruY_1621_TXCouDTA = 'Couro Caleirado DTA';
  CO_TXT_GraGruY_1707_TXProCur = 'Couro em Curtimento';
  CO_TXT_GraGruY_1877_TXCouCur = 'Couro Curtido';*)
  CO_TXT_GraGruY_2048_TXCadOpe = 'Artigo em confec��o';
  (*CO_TXT_GraGruY_3072_TXRibCla = 'Artigo de Ribeira Classificado';
  CO_TXT_GraGruY_4096_TXRibOpe = 'Artigo em Opera��o';
  CO_TXT_GraGruY_5120_TXWetEnd = 'Artigo Semi em Processo';
*)
  CO_TXT_GraGruY_6144_TXCadFcc = 'Artigo Acabado';
(*
  CO_TXT_GraGruY_0512_TXSubPrd = 'Subproduto In Natura';
  CO_TXT_GraGruY_0683_TXPSPPro = 'Subproduto em Processo';
  CO_TXT_GraGruY_0853_TXPSPEnd = 'Subproduto Processado';*)

{
  CO_TXT_            = '';
  CO_TXT_            = '';
  CO_TXT_            = '';

  Max??? = Integer(High(T???));
  s???: array[0..Max???] of string = (
  CO_TXT_,
  CO_TXT_,
  CO_TXT_
  );
}
  CO_CONTROLE_ESTOQUE_GRAGRUY = '1024, 2048, 3072';
  CO_TXT_emidAjuste        = '[ND]';
  CO_TXT_emidCompra        = 'Entrada';
  CO_TXT_emidVenda         = 'Sa�da';
  CO_TXT_emidReclas        = 'Reclasse';
  CO_TXT_emidBaixa         = 'Baixa';
  CO_TXT_emidIndstrlzc     = 'Industrializa��o';

  MaxEstqMovimID = Integer(High(TEstqMovimID));
  //sEstqMovimID: array[0..MaxEstqMovimID] of string = (
  sEstqMovimID: array[0..5] of string = (
    CO_TXT_emidAjuste        , // 0
    CO_TXT_emidCompra        , // 1
    CO_TXT_emidVenda         , // 2
    CO_TXT_emidReclas        , // 3
    CO_TXT_emidBaixa         , // 4
    CO_TXT_emidIndstrlzc      // 5
{
    CO_TXT_emidIndsVS        , // 6
    CO_TXT_emidClassArtVSUni , // 7
    CO_TXT_emidReclasVSUni   , // 8
    CO_TXT_emidForcado       , // 9
    CO_TXT_emidSemOrigem     , // 10
    CO_TXT_emidEmOperacao    , // 11
    CO_TXT_emidResiduoReclas , // 12
    CO_TXT_emidInventario    , // 13
    CO_TXT_emidClassArtVSMul , // 14
    CO_TXT_emidPreReclasse   , // 15
    CO_TXT_emidEntradaPlC    , // 16
    CO_TXT_emidExtraBxa      , // 17
    CO_TXT_emidSaldoAnterior , // 18
    CO_TXT_emidEmProcWE      , // 19
    CO_TXT_emidFinished      , // 20
    CO_TXT_emidDevolucao     , // 21
    CO_TXT_emidRetrabalho    , // 22
    CO_TXT_emidGeraSubProd   , // 23
    CO_TXT_emidReclasVSMul   , // 24
    CO_TXT_emidTransfLoc     , // 25
    CO_TXT_emidEmProcCal     , // 26
    CO_TXT_emidEmProcCur     , // 27
    CO_TXT_emidDesclasse     , // 28
    CO_TXT_emidCaleado       , // 29
    CO_TXT_emidRibPDA        , // 30
    CO_TXT_emidRibDTA        , // 31
    CO_TXT_emidEmProcSP      , // 32
    CO_TXT_emidEmReprRM      , // 33
    CO_TXT_emidCurtido       , // 34
    CO_TXT_emidMixInsum      , // 35
    CO_TXT_emidInnSemCob     , // 36
    CO_TXT_emidOutSemCob     // 37
}
  );
  CO_TXT_eminSemNiv          = 'Sem n�vel';
  CO_TXT_eminSorcClass       = 'Origem classifica��o';
  CO_TXT_eminDestClass       = 'Destino classifica��o';
  CO_TXT_eminSorcInds        = ''; //'Origem gera��o de artigo';
  CO_TXT_eminDestInds        = ''; //'Destino gera��o de artigo';
  CO_TXT_eminSorcReclass     = ''; //'Origem reclassifica��o';
  CO_TXT_eminDestReclass     = ''; //'Destino reclassifica��o';
  CO_TXT_eminSorcOper        = ''; //'Origem opera��o (Divis�o,...)';
  CO_TXT_eminEmOperInn       = ''; //'Em opera��o (Divis�o,...)';
  CO_TXT_eminDestOper        = ''; //'Destino opera��o (Divis�o,...)';
  CO_TXT_eminEmOperBxa       = ''; //'Baixa de opera��o (Divis�o,...)';
  CO_TXT_eminSorcPreReclas   = ''; //'Baixa em pr� reclasse';
  CO_TXT_eminDestPreReclas   = ''; //'Entrada para reclasse';
  CO_TXT_eminDestCurtiVS     = ''; //'Totalizador de Artigo Gerado';
  CO_TXT_eminSorcCurtiVS     = ''; //'Item de gera��o de Artigo Gerado';
  CO_TXT_eminBaixCurtiVS     = ''; //'Item de baixa de Artigo In Natura';
  CO_TXT_eminSdoArtInNat     = ''; //'Artigo In Natura';
  CO_TXT_eminSdoArtGerado    = ''; //'Artigo Gerado';
  CO_TXT_eminSdoArtClassif   = ''; //'Artigo Classificado';
  CO_TXT_eminSdoArtEmOper    = ''; //'Artigo em Opera��o';
  CO_TXT_eminSorcWEnd        = ''; //'Origem semi acabado em processo';
  CO_TXT_eminEmWEndInn       = ''; //'Semi acabado em processo';
  CO_TXT_eminDestWEnd        = ''; //''; //'Destino semi acabado em processo';
  CO_TXT_eminEmWEndBxa       = ''; //'Baixa de semi acabado em processo';
  CO_TXT_eminSdoArtEmWEnd    = ''; //'Artigo Semi Acabado';
  CO_TXT_eminFinishSdo       = ''; //'Artigo Acabado';
  CO_TXT_eminFinishInn       = ''; //'Artigo Acabado Calssificado';
  CO_TXT_emineminSdoSubPrd   = ''; //'Sub produto';
  CO_TXT_eminSorcLocal       = 'Origem de transf. de local';
  CO_TXT_eminDestLocal       = 'Destino de transf. de local';

  CO_TXT_eminSorcCal         = ''; //'Origem caleiro em processo';
  CO_TXT_eminEmCalInn        = ''; //'Caleiro em processo';
  CO_TXT_eminDestCal         = ''; //'Destino caleiro em processo';
  CO_TXT_eminEmCalBxa        = ''; //'Baixa de caleiro em processo';
  CO_TXT_eminSdoArtEmCal     = ''; //'Artigo de caleiro';

  CO_TXT_eminSorcCur         = ''; //'Origem curtimento em processo';
  CO_TXT_eminEmCurInn        = ''; //'Curtimento em processo';
  CO_TXT_eminDestCur         = ''; //'Destino curtimento em processo';
  CO_TXT_eminEmCurBxa        = ''; //'Baixa de curtimento em processo';
  CO_TXT_eminSdoArtEmCur     = ''; //'Artigo de curtimento';

  CO_TXT_eminSorcPDA         = ''; //'Origem PDA em opera��o';
  CO_TXT_eminEmPDAInn        = ''; //'PDA em opera��o';
  CO_TXT_eminDestPDA         = ''; //'Destino PDA em opera��o';
  CO_TXT_eminEmPDABxa        = ''; //'Baixa de PDA em opera��o';
  CO_TXT_eminSdoArtEmPDA     = ''; //'Artigo de PDA';

  CO_TXT_eminSorcDTA         = ''; //'Origem DTA em opera��o';
  CO_TXT_eminEmDTAInn        = ''; //'DTA opera��o';
  CO_TXT_eminDestDTA         = ''; //'Destino DTA em opera��o';
  CO_TXT_eminEmDTABxa        = ''; //'Baixa de DTA em opera��o';
  CO_TXT_eminSdoArtEmDTA     = ''; //'Artigo de DTA';

  CO_TXT_eminSorcPSP         = ''; //'Origem PSP em processo';
  CO_TXT_eminEmPSPInn        = ''; //'PSP processo';
  CO_TXT_eminDestPSP         = ''; //'Destino PSP em processo';
  CO_TXT_eminEmPSPBxa        = ''; //'Baixa de PSP em processo';
  CO_TXT_eminSdoArtEmPSP     = ''; //'Artigo de PSP';

  CO_TXT_eminSorcRRM         = ''; //'Origem RRM em reprocesso';
  CO_TXT_eminEmRRMInn        = ''; //'RRM reprocesso';
  CO_TXT_eminDestRRM         = ''; //'Destino RRM em reprocesso';
  CO_TXT_eminEmRRMBxa        = ''; //'Baixa de RRM em reprocesso';
  CO_TXT_eminSdoArtEmRRM     = ''; //'Artigo de Reprocesso / reparo';

  CO_TXT_eminSorcMixInsum    = ''; //'Baixa de insumo em mistura';
  CO_TXT_eminDestMixInsum    = ''; //'Gera��o de insumo em mistura';

  CO_TXT_eminSorcIndzc       = 'Origem industrializa��o';
  CO_TXT_eminEmIndzcInn      = 'Em industrializa��o';
  CO_TXT_eminDestIndzc       = 'Destino industrializa��o';
  CO_TXT_eminEmIndzcBxa      = 'Baixa de industrializa��o';

  CO_TXT_eminSorcCon         = 'Origem in natura em conserva��o';
  CO_TXT_eminEmConInn        = 'In natura em conserva��o';
  CO_TXT_eminDestCon         = 'Destino in natura em conserva��o';
  CO_TXT_eminEmConBxa        = 'Baixa de in natura em conserva��o';
  CO_TXT_eminSdoArtEmCon     = 'Couro conservado';

  MaxEstqMovimNiv = Integer(High(TEstqMovimNiv));
  sEstqMovimNiv: array[0..MaxEstqMovimNiv] of string = (
    CO_TXT_eminSemNiv          , // 00
    CO_TXT_eminSorcClass       , // 01
    CO_TXT_eminDestClass       , // 02
    CO_TXT_eminSorcInds        , // 03
    CO_TXT_eminDestInds        , // 04
    CO_TXT_eminSorcReclass     , // 05
    CO_TXT_eminDestReclass     , // 06
    CO_TXT_eminSorcOper        , // 07
    CO_TXT_eminEmOperInn       , // 08
    CO_TXT_eminDestOper        , // 09
    CO_TXT_eminEmOperBxa       , // 10
    CO_TXT_eminSorcPreReclas   , // 11
    CO_TXT_eminDestPreReclas   , // 12
    CO_TXT_eminDestCurtiVS     , // 13
    CO_TXT_eminSorcCurtiVS     , // 14
    CO_TXT_eminBaixCurtiVS     , // 15
    CO_TXT_eminSdoArtInNat     , // 16
    CO_TXT_eminSdoArtGerado    , // 17
    CO_TXT_eminSdoArtClassif   , // 18
    CO_TXT_eminSdoArtEmOper    , // 19
    CO_TXT_eminSorcWEnd        , // 20
    CO_TXT_eminEmWEndInn       , // 21
    CO_TXT_eminDestWEnd        , // 22
    CO_TXT_eminEmWEndBxa       , // 23
    CO_TXT_eminSdoArtEmWEnd    , // 24
    CO_TXT_eminFinishSdo       , // 25
    CO_TXT_emineminSdoSubPrd   , // 26
    CO_TXT_eminSorcLocal       , // 27
    CO_TXT_eminDestLocal       , // 28

    CO_TXT_eminSorcCal         , // 29
    CO_TXT_eminEmCalInn        , // 30
    CO_TXT_eminDestCal         , // 31
    CO_TXT_eminEmCalBxa        , // 32
    CO_TXT_eminSdoArtEmCal     , // 33

    CO_TXT_eminSorcCur         , // 34
    CO_TXT_eminEmCurInn        , // 35
    CO_TXT_eminDestCur         , // 36
    CO_TXT_eminEmCurBxa        , // 37
    CO_TXT_eminSdoArtEmCur     , // 38

    CO_TXT_eminSorcPDA         , // 39
    CO_TXT_eminEmPDAInn        , // 40
    CO_TXT_eminDestPDA         , // 41
    CO_TXT_eminEmPDABxa        , // 42
    CO_TXT_eminSdoArtEmPDA     , // 43

    CO_TXT_eminSorcDTA         , // 44
    CO_TXT_eminEmDTAInn        , // 45
    CO_TXT_eminDestDTA         , // 46
    CO_TXT_eminEmDTABxa        , // 47
    CO_TXT_eminSdoArtEmDTA     , // 48

    CO_TXT_eminSorcPSP         , // 49
    CO_TXT_eminEmPSPInn        , // 50
    CO_TXT_eminDestPSP         , // 51
    CO_TXT_eminEmPSPBxa        , // 52
    CO_TXT_eminSdoArtEmPSP     , // 53

    CO_TXT_eminSorcRRM         , // 54
    CO_TXT_eminEmRRMInn        , // 55
    CO_TXT_eminDestRRM         , // 56
    CO_TXT_eminEmRRMBxa        , // 57
    CO_TXT_eminSdoArtEmRRM     , // 58

    CO_TXT_eminSorcMixInsum    , // 59
    CO_TXT_eminDestMixInsum    , // 60

    CO_TXT_eminSorcIndzc       , // 61
    CO_TXT_eminEmIndzcInn      , // 62
    CO_TXT_eminDestIndzc       , // 63
    CO_TXT_eminEmIndzcBxa      , // 64

    CO_TXT_eminSorcCon         , // 65
    CO_TXT_eminEmConInn        , // 66
    CO_TXT_eminDestCon         , // 67
    CO_TXT_eminEmConBxa        , // 68
    CO_TXT_eminSdoArtEmCon      // 69
  );

{
  CO_ALL_CODS_PRI_IDX_TUPLE  = '1,8,9,15';
  //  V S_PF.AdicionarNovosV S_emid();
  CO_ALL_CODS_CLASS_TX       = '0,7,8,11,13,14,16,20,21,22,24,28,36';
  CO_ALL_CODS_BLEND_TX       = '1,7,8,10,13,14,16,24,28';
  CO_ALL_CODS_INN_POSIT_TX   = '1,9,13,16';
  CO_ALL_CODS_INN_ENTRA_TX   = '1,13,16';
}
  CO_ALL_CODS_BXA_POSIT_TX   = '9,17,28';
  CO_ALL_CODS_BXA_EXTRA_TX   = '9,13,17,28';
{
  CO_ALL_CODS_INN_NF_SPED_TX = '1,16,21,22'; // Entradas no estoque com documento fiscal de compra, remessa, devolucao, etc
  CO_ALL_CODS_NO_INN_SPED_TX = '6,11,19,20,23,26,27,32,33'; // Entradas no estoque pela producao (sem doc fiscal de compra, remessa, devolucao, etc)
  CO_ALL_CODS_ESTQ_SEM_PC    = '23,32'; N�o deveria usar, pois s� tem qtde!!
  CO_ONLY_COD_IDS_CLAS_TX    = '7,14';
  CO_ONLY_COD_IDS_RECL_TX    = '8,24,28';
}
  CO_CODS_OPER_PROC_ID_GET   = '11,19,26,27,29,32,33,38';
{
  CO_CODS_OPER_PROC_ID_SET   = '11,19,38';
}
  CO_CODS_OPER_PROC_NIV_GET  = '7,20,61';
  CO_CODS_OPER_PROC_NIV_FRO  = '7,10,20,23,30,32,34,35,37,50,52,55,57,61';
{
  CO_CODS_NIV_NEED_MO_FORNEC = '8,13,21,30,35';
  CO_CODS_NIV_NEED_MO_CLIENT = '1,2,8,9,11,12,13,14,15,20,21,22,23,27,28,29,30,34,35';
  CO_CODS_NIV_NO_BXA_AJS     = '8,21,30,35,40,45,50,55';
  //
  //CO_COUNIV2_SUBPRD          = '2,3'; // 1=Couro, 2=Raspa, 3=Subproduto
  CO_COUNIV2_SUBPRD          = '3'; // 1=Couro, 2=Raspa, 3=Subproduto
}
  // ATT_StatPall
  CO_TXT_txspIndefinido      = 'Indefinido';
  CO_TXT_txspMontando        = 'Montando';
  CO_TXT_txspDesmontando     = 'Desmontando';
  CO_TXT_txspMontEDesmo      = 'Mont. e desmo.';
  CO_TXT_txspEncerrado       = 'Encerrado';
  CO_TXT_txspEncerMont       = 'Encer. mas Mont.';
  CO_TXT_txspEncerDesmo      = 'Encer. mas Desmo.';
  CO_TXT_txspEncerMontEDesmo = 'Encer mas mnt e desm.';
  CO_TXT_txspRemovido        = 'Removido';
  MaxTXStatPall = Integer(High(TXXStatPall));
  sTXStatPall: array[0..MaxTXStatPall] of string = (
  CO_TXT_txspIndefinido      ,
  CO_TXT_txspMontando        ,
  CO_TXT_txspDesmontando     ,
  CO_TXT_txspMontEDesmo      ,
  CO_TXT_txspEncerrado       ,
  CO_TXT_txspEncerMont       ,
  CO_TXT_txspEncerDesmo      ,
  CO_TXT_txspEncerMontEDesmo ,
  CO_TXT_txspRemovido
  );
  Max_TX_IMP_ESTQ_ORD = 14;


  Max_TXGrandeza = 3;
  CO_TXT_TXGrandezaIndf = 'ND';
  CO_TXT_TXGrandezaPeca = 'p�';
  CO_TXT_TXGrandezaArea = 'm�';
  CO_TXT_TXGrandezaPeso = 'kg';
  CO_TXT_TXGrandezaLivr = 'ND';
s_TXGrandeza: array[-1..Max_TXGrandeza] of String = (
  CO_TXT_TXGrandezaIndf,
  CO_TXT_TXGrandezaPeca,
  CO_TXT_TXGrandezaArea,
  CO_TXT_TXGrandezaPeso,
  CO_TXT_TXGrandezaLivr
);

  CO_TXT_LONG_emidAjuste        = '[N�o Definido]';
  CO_TXT_LONG_emidCompra        = 'Compra';
  CO_TXT_LONG_emidVenda         = 'Venda';
  CO_TXT_LONG_emidReclas        = 'Reclassifica��o';
  CO_TXT_LONG_emidBaixa         = 'Baixa do estoque';
  CO_TXT_LONG_emidIndsWE        = 'Acabado';
  CO_TXT_LONG_emidIndsVS        = 'Artigo de Ribeira';
  CO_TXT_LONG_emidClassArtVSUni = 'Artigo de Ribeira Classificado Unit.';
  CO_TXT_LONG_emidReclasVSUni   = 'Artigo de Ribeira Reclassificado Unit.';
  CO_TXT_LONG_emidForcado       = 'Baixa For�ada';
  CO_TXT_LONG_emidSemOrigem     = '[Sem Origem!]';
  CO_TXT_LONG_emidEmOperacao    = 'Ordem de Opera��o';
  CO_TXT_LONG_emidResiduoReclas = 'Res�duo de reclassifica��o';
  CO_TXT_LONG_emidInventario    = 'Ajuste de Invent�rio';
  CO_TXT_LONG_emidClassArtVSMul = 'Artigo de Ribeira Classificado Mult.';
  CO_TXT_LONG_emidPreReclasse   = 'Prepara��o para reclassifica��o';
  CO_TXT_LONG_emidEntradaPlC    = 'Compra de classificado';
  CO_TXT_LONG_emidExtraBxa      = 'Baixa extra(vio)';
  CO_TXT_LONG_emidSaldoAnterior = 'Saldo anterior';
  CO_TXT_LONG_emidEmProcWE      = 'Em processo wet end';
  CO_TXT_LONG_emidFinished      = 'Acabado e classificado';
  CO_TXT_LONG_emidDevolucao     = 'Devolu��o de couro vendido';
  CO_TXT_LONG_emidRetrabalho    = 'Retrabalho de couro vendido';
  CO_TXT_LONG_emidGeraSubProd   = 'Gera��o de Sub Produto';
  CO_TXT_LONG_emidReclasVSMul   = 'Artigo de Ribeira Reclassificado Mult.';
  CO_TXT_LONG_emidTransfLoc     = 'Transfer�ncia de Local';
  CO_TXT_LONG_emidEmProcCal     = 'Em processo de caleiro';
  CO_TXT_LONG_emidEmProcCur     = 'Em processo de curtimento';
  CO_TXT_LONG_emidDesclasse     = 'Desclassifica��o de wet blue';
  CO_TXT_LONG_emidCaleado       = 'Couro caleado';
  CO_TXT_LONG_emidRibPDA        = 'Couro pr�-descarnado e aparado';
  CO_TXT_LONG_emidRibDTA        = 'Couro tripa (dividido ou laminado ou integral)';
  CO_TXT_LONG_emidEmProcSP      = 'Processamento de subproduto';
  CO_TXT_LONG_emidEmReprRM      = 'Reprocesso / reparo de material';
  CO_TXT_LONG_emidCurtido       = 'Couro curtido';
  CO_TXT_LONG_emidMixInsum      = 'Dilui��o / mistura de insumos';
  CO_TXT_LONG_emidInnSemCob     = 'Entrada sem Cobertura';
  CO_TXT_LONG_emidOutSemCob     = 'Sa�da sem Cobertura';
  CO_TXT_LONG_emidIndstrlzc     = 'Industrializa��o';
  CO_TXT_LONG_emidEmProcCon     = 'Em Conserva��o';
  CO_TXT_LONG_emidConservado    = 'Conservado';
  CO_TXT_LONG_emidEntraExced    = 'Entrada de excedente';

  MaxEstqMovimIDLong = Integer(High(TEstqMovimID));
  sEstqMovimIDLong: array[0..MaxEstqMovimID] of string = (
    CO_TXT_LONG_emidAjuste        , // 0
    CO_TXT_LONG_emidCompra        , // 1
    CO_TXT_LONG_emidVenda         , // 2
    CO_TXT_LONG_emidReclas        , // 3
    CO_TXT_LONG_emidBaixa         , // 4
    CO_TXT_LONG_emidIndsWE        , // 5
    CO_TXT_LONG_emidIndsVS        , // 6
    CO_TXT_LONG_emidClassArtVSUni , // 7
    CO_TXT_LONG_emidReclasVSUni   , // 8
    CO_TXT_LONG_emidForcado       , // 9
    CO_TXT_LONG_emidSemOrigem     , // 10
    CO_TXT_LONG_emidEmOperacao    , // 11
    CO_TXT_LONG_emidResiduoReclas , // 12
    CO_TXT_LONG_emidInventario    , // 13
    CO_TXT_LONG_emidClassArtVSMul , // 14
    CO_TXT_LONG_emidPreReclasse   , // 15
    CO_TXT_LONG_emidEntradaPlC    , // 16
    CO_TXT_LONG_emidExtraBxa      , // 17
    CO_TXT_LONG_emidSaldoAnterior , // 18
    CO_TXT_LONG_emidEmProcWE      , // 19
    CO_TXT_LONG_emidFinished      , // 20
    CO_TXT_LONG_emidDevolucao     , // 21
    CO_TXT_LONG_emidRetrabalho    , // 22
    CO_TXT_LONG_emidGeraSubProd   , // 23
    CO_TXT_LONG_emidReclasVSMul   , // 24
    CO_TXT_LONG_emidTransfLoc     , // 25
    CO_TXT_LONG_emidEmProcCal     , // 26
    CO_TXT_LONG_emidEmProcCur     , // 27
    CO_TXT_LONG_emidDesclasse     , // 28
    CO_TXT_LONG_emidCaleado       , // 29
    CO_TXT_LONG_emidRibPDA        , // 30
    CO_TXT_LONG_emidRibDTA        , // 31
    CO_TXT_LONG_emidEmProcSP      , // 32
    CO_TXT_LONG_emidEmReprRM      , // 33
    CO_TXT_LONG_emidCurtido       , // 34
    CO_TXT_LONG_emidMixInsum      , // 35
    CO_TXT_LONG_emidInnSemCob     , // 36
    CO_TXT_LONG_emidOutSemCob     , // 37
    CO_TXT_LONG_emidIndstrlzc     , // 38
    CO_TXT_LONG_emidEmProcCon     , // 39
    CO_TXT_LONG_emidConservado    , // 40
    CO_TXT_LONG_emidEntraExced      // 41
  );

type
  TUnAppListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UnAppListas: TUnAppListas;

implementation

uses Module, DmkDAC_PF, ModuleGeral, UnMyObjects;

{ TUnAppListas }

end.
