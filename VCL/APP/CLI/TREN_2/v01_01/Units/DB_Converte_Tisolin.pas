unit DB_Converte_Tisolin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnMLAGeral,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts, (*DBSDDadoss,*)
  Math, UnDmkProcFunc, UnDmkEnums,
  // ini Ver Delphi Alexandria
  //Data.DBXFirebird,
  // fim Ver Delphi Alexandria
  Datasnap.DBClient,
  SimpleDS, Data.SqlExpr, MySQLBatch, dmkDBGridZTO, AppListas, UnAppEnums,
  Variants;

type
  TFmDB_Converte_Tisolin = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImportaDados: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    DsDBF: TDataSource;
    DsCadCli: TDataSource;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    DsPsq: TDataSource;
    Panel7: TPanel;
    Panel9: TPanel;
    DBGrid2: TDBGrid;
    MeSQL1: TMemo;
    Panel8: TPanel;
    BtExecuta: TBitBtn;
    Splitter1: TSplitter;
    GroupBox2: TGroupBox;
    Panel10: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdCliCodIni: TdmkEdit;
    EdForCodIni: TdmkEdit;
    EdEntCodIni: TdmkEdit;
    QrPsqEnt: TmySQLQuery;
    QrPsqEntCodigo: TIntegerField;
    CGDBF: TdmkCheckGroup;
    TabSheet4: TTabSheet;
    Memo1: TMemo;
    DsCadFor: TDataSource;
    DsCadGru: TDataSource;
    DataSource1: TDataSource;
    QrExiste: TmySQLQuery;
    QrGraGXPatr: TmySQLQuery;
    QrGraGXPatrAGRPAT: TWideStringField;
    DataSource2: TDataSource;
    DsCadVde: TDataSource;
    TabSheet5: TTabSheet;
    Memo2: TMemo;
    QrGraFabMar: TmySQLQuery;
    QrGraFabMarControle: TIntegerField;
    QrGraGXPatrGraGruX: TIntegerField;
    Button1: TButton;
    QrSituacao: TmySQLQuery;
    QrSituacaoSituacao: TWordField;
    QrSituacaoSIGLA: TWideStringField;
    QrSituacaoITENS: TLargeintField;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    DBFB: TSQLConnection;
    SDTabelas: TSimpleDataSet;
    SDDados: TSimpleDataSet;
    SDCliente: TSimpleDataSet;
    SDCidade: TSimpleDataSet;
    TabSheet6: TTabSheet;
    MeErrosGerais: TMemo;
    SDCidadC: TSimpleDataSet;
    SDClienteCLIENTE: TFMTBCDField;
    SDClienteRAZAOSOCIAL: TStringField;
    SDClienteFANTASIA: TStringField;
    SDClienteTIPOPESSOA: TStringField;
    SDClienteCPFCGC: TStringField;
    SDClienteRGINSCRICAOESTADUAL: TStringField;
    SDClienteNATURALIDADE: TStringField;
    SDClienteENDERECO: TStringField;
    SDClienteENDERECONUMERO: TFMTBCDField;
    SDClienteBAIRRO: TStringField;
    SDClienteCOMPLEMENTO: TStringField;
    SDClienteCIDADE: TFMTBCDField;
    SDClienteCEP: TStringField;
    SDClienteFONE1: TStringField;
    SDClienteFONE2: TStringField;
    SDClienteCELULAR: TStringField;
    SDClienteANIVERSARIOABERTURA: TDateField;
    SDClienteTIPOCADASTRO: TFMTBCDField;
    SDClienteCIDADECOBRANCA: TFMTBCDField;
    SDClienteENDERECOCOBRANCA: TStringField;
    SDClienteBAIRROENDERECOCOBRANCA: TStringField;
    SDClienteCEPENDERECOCOBRANCA: TStringField;
    SDClienteCOMPLEMENTOCOBRANCA: TStringField;
    SDClienteLOCALTRABALHO: TStringField;
    SDClienteENDERECOTRABALHO: TStringField;
    SDClienteFONETRABALHO: TStringField;
    SDClienteTEMPOTRABALHO: TStringField;
    SDClienteFUNCAOTRABALHO: TStringField;
    SDClienteRENDATRABALHO: TFMTBCDField;
    SDClienteNOMECONJUGE: TStringField;
    SDClienteLOCALTRABALHOCONJUGE: TStringField;
    SDClienteENDERECOTRABALHOCONJUGE: TStringField;
    SDClienteFONETRABALHOCONJUGE: TStringField;
    SDClienteTEMPOTRABALHOCONJUGE: TStringField;
    SDClienteFUNCAOTRABALHOCONJUGE: TStringField;
    SDClienteRENDACONJUGE: TFMTBCDField;
    SDClientePAI: TStringField;
    SDClienteMAE: TStringField;
    SDClienteREFERENCIA1: TStringField;
    SDClienteREFERENCIAFONE_1: TStringField;
    SDClienteREFERENCIA2: TStringField;
    SDClienteREFERENCIAFONE_2: TStringField;
    SDClienteREFERENCIA3: TStringField;
    SDClienteREFERENCIAFONE_3: TStringField;
    SDClienteOBSERVACAO: TStringField;
    SDClienteINFORMACOES: TStringField;
    SDClienteEMAILPRIMARIO: TStringField;
    SDClienteCREDITO: TFMTBCDField;
    SDClienteISENTO: TStringField;
    SDClienteCONDICAO: TFMTBCDField;
    SDClientePARCELAS: TFMTBCDField;
    SDClienteINTERVALO: TFMTBCDField;
    SDClienteDATACADASTRO: TDateField;
    SDClienteDATAATUALIZACAO: TDateField;
    SDClienteULTIMACOMPRA: TDateField;
    SDClienteUSUARIOCADASTRO: TStringField;
    SDClienteUSUARIOATUALIZACAO: TStringField;
    SDClienteENDERECOENTREGA: TStringField;
    SDClienteOBSERVACAOCLIENTE: TStringField;
    SDClienteLIBERADODOCUMENTOVENCIDO: TStringField;
    SDClienteOBSERVACAOVENDA: TStringField;
    SDClienteDESCONTO: TFMTBCDField;
    SDClienteVENDEDOR: TFMTBCDField;
    SDClienteOBSERVACAOTRAVAMENTO: TStringField;
    SDClienteTRAVAMENTO: TStringField;
    SDClienteLIBERADOUMAFATURA: TStringField;
    SDClienteATIVO: TStringField;
    SDClienteORGAOPUBLICO: TStringField;
    SDClienteNCM: TStringField;
    SDClientePASTAXML: TStringField;
    SDClienteDIACOBRANCA: TFMTBCDField;
    SDClienteOBSERVACAODIACOBRANCA: TStringField;
    SDClienteEMAILFINANCEIRO: TStringField;
    SDClienteNAOCONTRIBUINTE: TStringField;
    SDClienteCONTRIBUINTEISENTO: TStringField;
    SDClienteCONTATO: TStringField;
    SDClienteFATURAR: TStringField;
    SDClienteCOBRADOR: TFMTBCDField;
    SDClienteCODCTC: TFMTBCDField;
    SDClienteIMPRIMEPRECOLIQUIDO: TStringField;
    SDClienteNUMEROPEDIDOITEM: TStringField;
    SDClienteINSCRICAOMUNICIPAL: TStringField;
    SDClienteSIMPLESNACIONAL: TStringField;
    SDClienteALIQUOTAISS: TFMTBCDField;
    SDClienteRETENCAOISS: TStringField;
    SDClienteLISTASTATUS: TStringField;
    MePronto: TMemo;
    SDClienteCIDADE_DESCRICAO: TStringField;
    SDClienteCIDADE_IBGE: TStringField;
    SDClienteCIDADE_PAIS: TStringField;
    SDClienteCIDADE_UF: TStringField;
    SDClienteCIDADC_IBGE: TStringField;
    SDClienteCIDADC_DESCRICAO: TStringField;
    SDLocacao: TSimpleDataSet;
    SDLocacaoLOCACAO: TFMTBCDField;
    SDLocacaoCLIENTE: TFMTBCDField;
    SDLocacaoEMISSAODATA: TDateField;
    SDLocacaoEMISSAOHORA: TTimeField;
    SDLocacaoDEVOLUCAODATA: TDateField;
    SDLocacaoDEVOLUCAOHORA: TTimeField;
    SDLocacaoSAIDADATA: TDateField;
    SDLocacaoSAIDAHORA: TTimeField;
    SDLocacaoFINALIZADODATA: TDateField;
    SDLocacaoFINALIZADOHORA: TTimeField;
    SDLocacaoVALOREQUIPAMENTO: TFMTBCDField;
    SDLocacaoVALORLOCACAO: TFMTBCDField;
    SDLocacaoVALORADICIONAL: TFMTBCDField;
    SDLocacaoVALORADIANTAMENTO: TFMTBCDField;
    SDLocacaoTIPOALUGUEL: TStringField;
    SDLocacaoOBSERVACAO: TStringField;
    SDLocacaoFECHAMENTODATA: TDateField;
    SDLocacaoFECHAMENTOHORA: TTimeField;
    SDLocacaoUSUARIO: TStringField;
    SDLocacaoSTATUS: TFMTBCDField;
    SDLocacaoVALORDESCONTO: TFMTBCDField;
    SDLocacaoOBRA: TStringField;
    SDLocacaoCANCELADA: TStringField;
    SDLocacaoULTIMACOBRANCAMENSAL: TDateField;
    SDAux: TSimpleDataSet;
    Panel11: TPanel;
    CkLimit: TCheckBox;
    Label7: TLabel;
    EdSkip: TdmkEdit;
    Label8: TLabel;
    EdFirst: TdmkEdit;
    MySQLBatchExecute1: TMySQLBatchExecute;
    SDLocacaoItens: TSimpleDataSet;
    SDStatusCliente: TSimpleDataSet;
    SDStatusClienteSTATUS: TFMTBCDField;
    SDStatusClienteDESCRICAO: TStringField;
    SDLocacaoItensLOCACAO: TFMTBCDField;
    SDLocacaoItensSEQUENCIA: TFMTBCDField;
    SDLocacaoItensPRODUTO: TFMTBCDField;
    SDLocacaoItensQUANTIDADEPRODUTO: TFMTBCDField;
    SDLocacaoItensVALORPRODUTO: TFMTBCDField;
    SDLocacaoItensQUANTIDADELOCACAO: TFMTBCDField;
    SDLocacaoItensVALORLOCACAO: TFMTBCDField;
    SDLocacaoItensQUANTIDADEDEVOLUCAO: TFMTBCDField;
    SDLocacaoItensTROCA: TStringField;
    SDLocacaoItensCATEGORIA: TStringField;
    SDLocacaoItensCOBRANCALOCACAO: TStringField;
    SDLocacaoItensCOBRANCACONSUMO: TFMTBCDField;
    SDLocacaoItensCOBRANCAREALIZADAVENDA: TStringField;
    SDLocacaoItensDATAINICIALCOBRANCA: TDateField;
    SDLocacaoItensHORAINICIALCOBRANCA: TTimeField;
    SDLocacaoItensDATASAIDA: TDateField;
    SDLocacaoItensHORASAIDA: TTimeField;
    SDClienteStatus: TSimpleDataSet;
    SDClienteStatusCLIENTE: TFMTBCDField;
    SDClienteStatusSTATUS: TFMTBCDField;
    SDProduto: TSimpleDataSet;
    SDProdutoPRODUTO: TFMTBCDField;
    SDProdutoDESCRICAO: TStringField;
    SDProdutoMAQ_BASE: TStringField;
    SDProdutoMARCA: TStringField;
    SDProdutoIMPORTADO: TStringField;
    SDProdutoGRUPO: TFMTBCDField;
    SDProdutoMARGEM: TFMTBCDField;
    SDProdutoUNIDADE: TStringField;
    SDProdutoCUSTO: TFMTBCDField;
    SDProdutoESTOQUE: TFMTBCDField;
    SDProdutoPRECOVENDA: TFMTBCDField;
    SDProdutoATIVO: TStringField;
    SDProdutoMARGEMSUBSTITUICAO: TFMTBCDField;
    SDProdutoPRODUTODECIMAL: TStringField;
    SDProdutoDUPLICA: TStringField;
    SDProdutoPRECOPROMOCAO: TFMTBCDField;
    SDProdutoDATAPROMOCAO: TDateField;
    SDProdutoCLASSIFICACAOFISCAL: TFMTBCDField;
    SDProdutoULTIMOFORNECEDOR: TFMTBCDField;
    SDProdutoULTIMANOTA: TStringField;
    SDProdutoULTIMAQUANTIDADE: TFMTBCDField;
    SDProdutoULTIMADATA: TDateField;
    SDProdutoPULTIMOFORNECEDOR: TFMTBCDField;
    SDProdutoPULTIMANOTA: TStringField;
    SDProdutoPULTIMAQUANTIDADE: TFMTBCDField;
    SDProdutoPULTIMADATA: TDateField;
    SDProdutoPULTIMOCUSTO: TFMTBCDField;
    SDProdutoPULTIMOPRECOVENDA: TFMTBCDField;
    SDProdutoDATAMVTOESTOQUE: TSQLTimeStampField;
    SDProdutoMENSAGEM: TStringField;
    SDProdutoNCM: TStringField;
    SDProdutoULTIMAATUALIZACAO: TSQLTimeStampField;
    SDProdutoULTIMOVALOR: TFMTBCDField;
    SDProdutoPULTIMOVALOR: TFMTBCDField;
    SDProdutoLINHA: TStringField;
    SDProdutoESTOQUEEXTERNO: TFMTBCDField;
    SDProdutoDESCONTO: TFMTBCDField;
    SDProdutoBOCACAIXA: TFMTBCDField;
    SDProdutoSOMACUSTO: TFMTBCDField;
    SDProdutoTIPOITEM: TFMTBCDField;
    SDProdutoGENERO: TFMTBCDField;
    SDProdutoMODELO: TFMTBCDField;
    SDProdutoSERIE: TStringField;
    SDProdutoREFERENCIA: TStringField;
    SDProdutoPESOMAQUINA: TFMTBCDField;
    SDProdutoCODIGOBARRAS: TStringField;
    SDProdutoCODIGOFABRICA: TStringField;
    SDProdutoUSUARIOCADASTRO: TStringField;
    SDProdutoUSUARIOALTERACAO: TStringField;
    SDProdutoDATAHORACADASTRO: TSQLTimeStampField;
    SDProdutoCLA_TRI: TStringField;
    SDProdutoPERCIPI: TFMTBCDField;
    SDProdutoPERCFRETEPROPRIO: TFMTBCDField;
    SDProdutoPERCFRETETERCEIRO: TFMTBCDField;
    SDProdutoPERCDESPESAS: TFMTBCDField;
    SDProdutoPERCFINANCEIRO: TFMTBCDField;
    SDProdutoPERCPISCOFINSCREDITO: TFMTBCDField;
    SDProdutoPERCICMORIGEM: TFMTBCDField;
    SDProdutoPERCICMDESTINO: TFMTBCDField;
    SDProdutoTABELAFORNECEDOR: TFMTBCDField;
    SDProdutoCUSTOREPOSICAO: TFMTBCDField;
    SDProdutoPULTIMOPERCFRETEPROPRIO: TFMTBCDField;
    SDProdutoPULTIMOPERCFRETETERCEIRO: TFMTBCDField;
    SDProdutoPULTIMOPERCDESPESAS: TFMTBCDField;
    SDProdutoPULTIMOPERCFINANCEIRO: TFMTBCDField;
    SDProdutoPULTIMOPERCPISCOFINSCREDITO: TFMTBCDField;
    SDProdutoPULTIMOPERCICMORIGEM: TFMTBCDField;
    SDProdutoPULTIMOPERCICMDESTINO: TFMTBCDField;
    SDProdutoPULTIMOCUSTOREPOSICAO: TFMTBCDField;
    SDProdutoPULTIMOPERCIPI: TFMTBCDField;
    SDProdutoPULTIMAMARGEMSUBSTITUICAO: TFMTBCDField;
    SDProdutoPULTIMATABELAFORNECEDOR: TFMTBCDField;
    SDProdutoPULTIMAMARGEM: TFMTBCDField;
    SDProdutoQUANTIDADEEMBALAGEMCOMPRA: TFMTBCDField;
    SDProdutoSERVICO: TStringField;
    SDProdutoLOCALIZACAO: TStringField;
    SDProdutoMULTIPLOVENDA: TFMTBCDField;
    SDProdutoDESCRICAOETIQUETA: TStringField;
    SDProdutoFICHATECNICA: TStringField;
    SDProdutoIMAGEM: TMemoField;
    SDProdutoTEMFICHATECNICA: TStringField;
    SDProdutoTEMIMAGEM: TStringField;
    SDProdutoULTIMAATUALIZACAOIMAGEM: TDateField;
    SDProdutoPRODUTONOVO: TFMTBCDField;
    SDProdutoPATRIMONIO: TStringField;
    SDProdutoCATEGORIA: TStringField;
    SDProdutoVALORDIARIA: TFMTBCDField;
    SDProdutoVALORSEMANAL: TFMTBCDField;
    SDProdutoVALORQUINZENAL: TFMTBCDField;
    SDProdutoVALORMENSAL: TFMTBCDField;
    SDProdutoPRECOFDS: TFMTBCDField;
    SDProdutoDIASCARENCIA: TFMTBCDField;
    SDProdutoREDUTORDIARIA: TFMTBCDField;
    SDProdutoCOBRANCACONSUMO: TFMTBCDField;
    SDGrupo: TSimpleDataSet;
    SDGrupoGRUPO: TFMTBCDField;
    SDGrupoDESCRICAO: TStringField;
    SDGrupoATUALIZA: TStringField;
    SDGrupoULTIMAATUALIZACAO: TSQLTimeStampField;
    SDGrupoMENSAGEM: TStringField;
    TabSheet7: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    SDLinha: TSimpleDataSet;
    SDLinhaDESCRICAO: TStringField;
    SDLinhaGRUPO: TFMTBCDField;
    SDLinhaLINHA: TStringField;
    SDLinhaITENS: TIntegerField;
    SDMarca: TSimpleDataSet;
    SDMarcaMARCA: TStringField;
    SDMarcaULTIMAATUALIZACAO: TSQLTimeStampField;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    QrGraGX: TMySQLQuery;
    SDGraGX: TSimpleDataSet;
    SDGraGXPRODUTO: TFMTBCDField;
    SDGraGXACESSORIO: TFMTBCDField;
    SDProdutoAcessorio: TSimpleDataSet;
    SDProdutoAcessorioPRODUTO: TFMTBCDField;
    SDProdutoAcessorioACESSORIO: TFMTBCDField;
    SDProdutoAcessorioQUANTIDADE: TFMTBCDField;
    QrItem: TMySQLQuery;
    QrItemGraGruX: TIntegerField;
    QrItemTABELA: TWideStringField;
    QrItemAplicacao: TIntegerField;
    QrGraGruY: TMySQLQuery;
    FbDB: TMySQLDatabase;
    QrGraGruYTipoProd: TWideStringField;
    QrGraGruYGGY_KIND: TWideStringField;
    QrGraGruYAtivo: TWideStringField;
    QrGraGruYDESCRICAO: TWideStringField;
    QrGraGruYPRODUTO: TLargeintField;
    QrGraGruYLinha: TWideStringField;
    QrGraGruYCategoria: TWideStringField;
    QrGraGruYReferencia: TWideStringField;
    QrGraGruYPatrimonio: TWideStringField;
    QrLocados: TMySQLQuery;
    QrLocadosPRODUTO: TLargeintField;
    BitBtn2: TBitBtn;
    QrSeqIL: TMySQLQuery;
    QrSeqILGraGruY: TIntegerField;
    QrSeqILLOCACAO: TLargeintField;
    QrSeqILSEQUENCIA: TLargeintField;
    QrSeqILPRODUTO: TLargeintField;
    QrSeqILQUANTIDADEPRODUTO: TLargeintField;
    QrSeqILVALORPRODUTO: TFloatField;
    QrSeqILQUANTIDADELOCACAO: TLargeintField;
    QrSeqILVALORLOCACAO: TFloatField;
    QrSeqILQUANTIDADEDEVOLUCAO: TLargeintField;
    QrSeqILTROCA: TWideStringField;
    QrSeqILCATEGORIA: TWideStringField;
    QrSeqILCOBRANCALOCACAO: TWideStringField;
    QrSeqILCOBRANCACONSUMO: TFloatField;
    QrSeqILCOBRANCAREALIZADAVENDA: TWideStringField;
    QrSeqILDATAINICIALCOBRANCA: TDateField;
    QrSeqILHORAINICIALCOBRANCA: TTimeField;
    QrSeqILDATASAIDA: TDateField;
    QrSeqILHORASAIDA: TTimeField;
    TabSheet8: TTabSheet;
    Panel12: TPanel;
    Label9: TLabel;
    EdLoadCSVOthDir: TEdit;
    SbLoadCSVOthDir: TSpeedButton;
    QrLocadosQtdLocado: TFloatField;
    SDGerencia: TSimpleDataSet;
    SDGerenciaSEQUENCIA: TFMTBCDField;
    SDGerenciaRAZAOSOCIAL: TStringField;
    SDGerenciaVERSAO: TStringField;
    SDGerenciaLOJA: TStringField;
    SDGerenciaFONE: TStringField;
    SDGerenciaNOTAFISCAL: TFMTBCDField;
    SDGerenciaINSCRICAOESTADUAL: TStringField;
    SDGerenciaCNPJ: TStringField;
    SDGerenciaCIDADE: TFMTBCDField;
    SDGerenciaENDERECO: TStringField;
    SDGerenciaNUMERO: TFMTBCDField;
    SDGerenciaCOMPLEMENTO: TStringField;
    SDGerenciaBAIRRO: TStringField;
    SDGerenciaCEP: TStringField;
    SDGerenciaRESPONSAVEL: TStringField;
    SDGerenciaFONERESPONSAVEL: TStringField;
    SDGerenciaIMPRESSORANOTAFISCAL: TStringField;
    SDGerenciaDATABACKUP: TDateField;
    SDGerenciaLOGOMENU: TBlobField;
    SDGerenciaLOGO: TBlobField;
    SDGerenciaITENSORCAMENTO: TFMTBCDField;
    SDGerenciaPASTABACKUP: TStringField;
    SDGerenciaSERVERNAME: TStringField;
    SDGerenciaDATABASENAME: TStringField;
    SDGerenciaFANTASIA: TStringField;
    SDGerenciaHOST: TStringField;
    SDGerenciaUSERNAME: TStringField;
    SDGerenciaSENHA: TStringField;
    SDGerenciaSITE: TStringField;
    SDGerenciaHISTORICOJUROS: TFMTBCDField;
    SDGerenciaPROXY: TStringField;
    SDGerenciaUSUARIOPROXY: TStringField;
    SDGerenciaSENHAPROXY: TStringField;
    SDGerenciaEMAILNFE: TStringField;
    SDGerenciaSENHAEMAILNFE: TStringField;
    SDGerenciaHISTORICOVISTA: TFMTBCDField;
    SDGerenciaHISTORICOPRAZO: TFMTBCDField;
    SDGerenciaCOBRADORVISTA: TFMTBCDField;
    SDGerenciaCOBRADORPRAZO: TFMTBCDField;
    SDGerenciaHISTORICOCAIXADUPLICATA: TFMTBCDField;
    SDGerenciaHISTORICOCAIXACARTEIRA: TFMTBCDField;
    SDGerenciaHISTORICOJUROCAIXA: TFMTBCDField;
    SDGerenciaHISTORICODESPESACARTORIO: TFMTBCDField;
    SDGerenciaVENDEDORLOJA: TFMTBCDField;
    SDGerenciaHISTORICOCHEQUEPRE: TFMTBCDField;
    SDGerenciaCOBRADORCHEQUEPRE: TFMTBCDField;
    SDGerenciaIMPRESSORARESERVA: TStringField;
    SDGerenciaDDD: TFMTBCDField;
    SDGerenciaVALIDADEORCAMENTO: TStringField;
    SDGerenciaLIMITEDEVOLUCAODIAS: TFMTBCDField;
    SDGerenciaHISTORICODEVOLUCAO: TFMTBCDField;
    SDGerenciaCOBRADORDEVOLUCAO: TFMTBCDField;
    SDGerenciaPOSICAOLOJA: TDateField;
    SDGerenciaCOBRADORTROCO: TFMTBCDField;
    SDGerenciaHISTORICOTROCOCREDITO: TFMTBCDField;
    SDGerenciaHISTORICOTROCODEBITO: TFMTBCDField;
    SDGerenciaDIASLIMITEORCAMENTO: TFMTBCDField;
    SDGerenciaMULTILOJA: TStringField;
    SDGerenciaCOBRADORCOMPRAS: TFMTBCDField;
    SDGerenciaHISTORICOJUROFORNECEDOR: TFMTBCDField;
    SDGerenciaHISTORICODESCONTOFORNECEDOR: TFMTBCDField;
    SDGerenciaHORAINICIAL: TTimeField;
    SDGerenciaHORAENTRADAMANHA: TTimeField;
    SDGerenciaHORASAIDAMANHA: TTimeField;
    SDGerenciaHORAENTRADATARDE: TTimeField;
    SDGerenciaHORASAIDATARDE: TTimeField;
    SDGerenciaALTERACAOPRODUTO: TMemoField;
    SDGerenciaTEMPOSINCRONIA: TFMTBCDField;
    SDGerenciaPASTABACKUPWINDOWS: TStringField;
    SDGerenciaIMPRESSORANFCE: TStringField;
    SDGerenciaBLOQUEIOINATIVO: TFMTBCDField;
    SDGerenciaCONSUMIDOR: TFMTBCDField;
    SDGerenciaULTIMASEQUENCIA: TFMTBCDField;
    SDGerenciaIP: TStringField;
    SDGerenciaPASTABACKUPFTP: TStringField;
    SDGerenciaINDICADORSITESPECIAL: TStringField;
    SDGerenciaINDICADORPESSOAJURIDICA: TFMTBCDField;
    SDGerenciaINDICADORTIPOATIVIDADE: TFMTBCDField;
    SDGerenciaINDICADORINCIDENCIATRIBUTARIA: TFMTBCDField;
    SDGerenciaINDICADORMETODOAPROPRIACAO: TFMTBCDField;
    SDGerenciaINDICADORTIPOCONTRIBUICAO: TFMTBCDField;
    SDGerenciaMODELOECF: TStringField;
    SDGerenciaSERIEECF: TStringField;
    SDGerenciaINDICADORATIVIDADE: TFMTBCDField;
    SDGerenciaCRCCONTADOR: TStringField;
    SDGerenciaPERFIL: TStringField;
    SDGerenciaCONTADOR: TStringField;
    SDGerenciaCPFCONTADOR: TStringField;
    SDGerenciaCNPJESCRITORIOCONT: TStringField;
    SDGerenciaCEPESCRITORIOCONT: TStringField;
    SDGerenciaENDERECOESCRITORIOCONT: TStringField;
    SDGerenciaNUMEROENDESCRITORIOCONT: TFMTBCDField;
    SDGerenciaCOMPLENDESCRITORIOCONT: TStringField;
    SDGerenciaBAIRROESCRITORIOCONT: TStringField;
    SDGerenciaFONEESCRITORIOCONT: TStringField;
    SDGerenciaFAXESCRITORIOCONT: TStringField;
    SDGerenciaEMAILESCRITORIOCONT: TStringField;
    SDGerenciaCIDADEESCRITORIOCONT: TFMTBCDField;
    SDGerenciaINDUSTRIA: TStringField;
    SDGerenciaEMAILFTPLOJA: TStringField;
    SDGerenciaIDLOJASITEF: TStringField;
    SDGerenciaIPSITEF: TStringField;
    SDGerenciaECF: TStringField;
    SDGerenciaEMAILLOJA: TStringField;
    SDGerenciaPORTASMTP: TFMTBCDField;
    SDGerenciaMENSAGEMDEVOLUCAO: TStringField;
    SDGerenciaULTIMAATUALIZACAOSELFCOLOR: TSQLTimeStampField;
    SDGerenciaLAYOUTSPEDFISCAL: TFMTBCDField;
    SDGerenciaLAYOUTSPEDPISCOFINS: TFMTBCDField;
    SDGerenciaDATABASENAMECEP: TStringField;
    SDGerenciaASSUNTOEMAIL: TStringField;
    SDGerenciaCORPOEMAIL: TStringField;
    SDGerenciaTIPOBOLETO: TFMTBCDField;
    SDGerenciaPASTALAZZUMIX: TStringField;
    SDGerenciaMENSAGEMETIQUETA: TStringField;
    SDGerenciaPISCOFINSDEBITO: TFMTBCDField;
    SDGerenciaOPERACIONAL: TFMTBCDField;
    SDGerenciaCOMISSAO: TFMTBCDField;
    SDGerenciaCUSTOENTREGA: TFMTBCDField;
    SDGerenciaPROLABORE: TFMTBCDField;
    SDGerenciaPISCOFINS: TFMTBCDField;
    SDGerenciaIRCSLL: TFMTBCDField;
    SDGerenciaPORTAIMPRESSORANFCE: TStringField;
    SDGerenciaLOGONFCE: TMemoField;
    SDGerenciaSITECONSULTANFCE: TStringField;
    SDGerenciaTEMPOIMPRESSAOBEMATECH: TFMTBCDField;
    SDGerenciaHISTORICODESCONTOCLIENTE: TFMTBCDField;
    SDGerenciaCAMINHOIMPRESSORADIVERSOS: TStringField;
    SDGerenciaHISTORICOCOMPRA: TFMTBCDField;
    SDGerenciaCLIENTECARTAO: TFMTBCDField;
    SDGerenciaHISTORICOCARTAO: TFMTBCDField;
    SDGerenciaHISTORICOSAIDATROCACHEQUE: TFMTBCDField;
    SDGerenciaHISTORICOENTRADATROCACHEQUE: TFMTBCDField;
    SDGerenciaHISTORICOSERVICO: TFMTBCDField;
    SDGerenciaHISTORICORETENCAO: TFMTBCDField;
    SDGerenciaCOBRADORSERVICO: TFMTBCDField;
    SDGerenciaNOMEIMPRESSORAETIQUETA: TStringField;
    SDGerenciaIMPRESSORAENTREGAREQUISICAO: TStringField;
    SDGerenciaEMAILCONTATO: TStringField;
    SDGerenciaCAMINHOANEXOSCLIENTE: TStringField;
    SDGerenciaCAMINHOANEXOSLOCACAO: TStringField;
    SDGerenciaDATABACKUPANEXO: TSQLTimeStampField;
    SDGerenciaPASTABACKUPANEXO: TStringField;
    SDGerenciaHORARIOABERTURA: TTimeField;
    SDGerenciaTRABALHASABADO: TStringField;
    SDGerenciaHORASCORTESIA: TTimeField;
    SDGerenciaCAMINHOANEXOSOS: TStringField;
    SDGerenciaCAMINHOANEXOSVISTAEXPLODIDA: TStringField;
    SDGerenciaHISTORICOLOCACAO: TFMTBCDField;
    SDGerenciaVENDEDORLOCACAO: TFMTBCDField;
    SDGerenciaVENDEDOROS: TFMTBCDField;
    SDGerenciaTRABALHADOMINGO: TStringField;
    SDGerenciaTRABALHAFERIADO: TStringField;
    SDLocacaoMovimento: TSimpleDataSet;
    SDLocacaoMovimentoLOCACAO: TFMTBCDField;
    SDLocacaoMovimentoSEQUENCIA: TFMTBCDField;
    SDLocacaoMovimentoDATA: TDateField;
    SDLocacaoMovimentoHORA: TTimeField;
    SDLocacaoMovimentoUSUARIO: TStringField;
    SDLocacaoMovimentoPRODUTO: TFMTBCDField;
    SDLocacaoMovimentoTIPO: TStringField;
    SDLocacaoMovimentoQUANTIDADE: TFMTBCDField;
    SDLocacaoMovimentoDESCRICAO: TStringField;
    SDLocacaoMovimentoCOBRANCALOCACAO: TStringField;
    SDLocacaoMovimentoVALORLOCACAO: TFMTBCDField;
    SDLocacaoMovimentoLIMPO: TStringField;
    SDLocacaoMovimentoSUJO: TStringField;
    SDLocacaoMovimentoQUEBRADO: TStringField;
    SDLocacaoMovimentoTESTADODEVOLUCAO: TStringField;
    SDLocacaoMovimentoPRODUTOENTRADA: TFMTBCDField;
    SDLocacaoMovimentoMOTIVOTROCA: TFMTBCDField;
    SDLocacaoMovimentoOBSERVACAOTROCA: TStringField;
    SDLocacaoMovimentoQUANTIDADELOCADA: TFMTBCDField;
    SDLocacaoMovimentoQUANTIDADEJADEVOLVIDA: TFMTBCDField;
    SDLocacaoTrocaMotivo: TSimpleDataSet;
    SDLocacaoTrocaMotivoTROCAMOTIVO: TFMTBCDField;
    SDLocacaoTrocaMotivoDESCRICAO: TStringField;
    SDLocacaoHistorico: TSimpleDataSet;
    SDLocacaoHistoricoLOCACAO: TFMTBCDField;
    SDLocacaoHistoricoDATAHORA: TSQLTimeStampField;
    SDLocacaoHistoricoDESCRICAO: TStringField;
    SDLocacaoHistoricoUSUARIO: TStringField;
    SDAnexoDocumento: TSimpleDataSet;
    SDAnexoDocumentoTIPO: TStringField;
    SDAnexoDocumentoCHAVE: TFMTBCDField;
    SDAnexoDocumentoSEQUENCIA: TFMTBCDField;
    SDAnexoDocumentoDESCRICAO: TStringField;
    SDAnexoDocumentoNOME: TStringField;
    SDAnexoDocumentoTIPOARQUIVO: TStringField;
    SDAnexoDocumentoDATAATUALIZACAO: TSQLTimeStampField;
    Label11: TLabel;
    EdPathGBak: TEdit;
    SbGBak: TSpeedButton;
    Label12: TLabel;
    EdArqFB_Bkp: TEdit;
    Label13: TLabel;
    SbArqFB_Bkp: TSpeedButton;
    EdIB_DB: TEdit;
    SpeedButton2: TSpeedButton;
    GroupBox3: TGroupBox;
    Panel13: TPanel;
    Panel4: TPanel;
    LaAvisoG1: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoG2: TLabel;
    LaAvisoB2: TLabel;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    Panel14: TPanel;
    Panel15: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel16: TPanel;
    Label10: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    SDFields: TSimpleDataSet;
    SDFieldsTABELA: TStringField;
    SDFieldsCAMPO: TStringField;
    SDFieldsTIPO_CAMPO: TStringField;
    SDFieldsTAMANHO: TSmallintField;
    SDFieldsESCALA: TLargeintField;
    SDFieldsPRIMARY_KEY: TStringField;
    SDFieldsNOT_NULL: TStringField;
    SDFieldsFOREIGN_KEY: TStringField;
    SDFieldsINDICE_CHAVE: TStringField;
    SDFieldsTABELA_CHAVE: TStringField;
    SDFieldsCAMPO_CHAVE: TStringField;
    SDFieldsREGRA_UPDATE: TStringField;
    SDFieldsREGRA_DELETE: TStringField;
    DsFields: TDataSource;
    BtCriaEstrutura: TBitBtn;
    BtEspelha: TBitBtn;
    EdDBNome: TdmkEdit;
    Label16: TLabel;
    EdIPServer: TdmkEdit;
    Label17: TLabel;
    Label18: TLabel;
    EdPorta: TdmkEdit;
    DBAnt: TMySQLDatabase;
    QrUpd: TMySQLQuery;
    QrAux: TMySQLQuery;
    QrTabelas: TMySQLQuery;
    QrTabelasTabela: TWideStringField;
    QrCampos: TMySQLQuery;
    QrCamposTabela: TWideStringField;
    QrCamposCampo: TWideStringField;
    QrCamposDataType: TWideStringField;
    QrCamposTamanho: TIntegerField;
    QrCamposEscala: TIntegerField;
    QrCamposPrimaryKey: TWideStringField;
    QrCamposNotNull: TWideStringField;
    QrCamposForeignKey: TWideStringField;
    QrCamposIndiceChave: TWideStringField;
    QrCamposTabelaChave: TWideStringField;
    QrCamposCampoChave: TWideStringField;
    QrCamposRegraUpdate: TWideStringField;
    QrCamposRegraDelete: TWideStringField;
    MySQLBatchExecute2: TMySQLBatchExecute;
    Panel17: TPanel;
    DBGTabelas: TdmkDBGridZTO;
    Panel18: TPanel;
    BtTodosDBAnt: TBitBtn;
    BtNenhumDBAnt: TBitBtn;
    QrPRODUTO: TMySQLQuery;
    QrPRODUTODESCRICAO: TWideStringField;
    DsTabelas: TDataSource;
    BitBtn3: TBitBtn;
    BtIB_DB: TBitBtn;
    DBGrid3: TDBGrid;
    Panel19: TPanel;
    MeCmd: TMemo;
    MeResult_: TMemo;
    QrPRODUTOPRODUTO: TLargeintField;
    SDFornecedorProduto: TSimpleDataSet;
    SDFornecedorProdutoFORNECEDOR: TFMTBCDField;
    SDFornecedorProdutoPRODUTO: TFMTBCDField;
    SDFornecedorProdutoCODIGOFORNECEDOR: TStringField;
    SDLoja: TSimpleDataSet;
    SDLojaLOJA: TStringField;
    SDLojaFILIAL: TStringField;
    SDLojaDESCRICAO: TStringField;
    SDLojaIP: TStringField;
    SDLojaMORADIARIA: TFMTBCDField;
    SDLojaTRAVACLIENTE: TFMTBCDField;
    SDLojaDESCONTONAVENDA: TFMTBCDField;
    SDLojaELOJA: TStringField;
    SDLojaULTIMAATUALIZACAOESCRITORIO: TSQLTimeStampField;
    SDLojaULTIMAATUALIZACAOLOJAS: TSQLTimeStampField;
    SDLojaPERIODOINATIVO: TFMTBCDField;
    SDLojaSPED: TStringField;
    SDLojaACRESCIMOVENDA: TFMTBCDField;
    SDLojaSOMADESCONTO: TFMTBCDField;
    SDLojaMULTA: TFMTBCDField;
    SDLojaCERTIFICADODIGITAL: TStringField;
    SDLojaRAZAOSOCIAL: TStringField;
    SDLojaDDD: TFMTBCDField;
    SDLojaFONE: TStringField;
    SDLojaINSCRICAOESTADUAL: TStringField;
    SDLojaCNPJ: TStringField;
    SDLojaCIDADE: TFMTBCDField;
    SDLojaENDERECO: TStringField;
    SDLojaNUMERO: TFMTBCDField;
    SDLojaCOMPLEMENTO: TStringField;
    SDLojaBAIRRO: TStringField;
    SDLojaCEP: TStringField;
    SDLojaFANTASIA: TStringField;
    SDLojaEMPRESATRIBUTADA: TStringField;
    SDLojaCHAVENFE: TStringField;
    SDLojaSERIECONTINGENCIA: TStringField;
    SDLojaSERIENFE: TStringField;
    SDLojaMODELONFE: TFMTBCDField;
    SDLojaTIPOEMISSAONFE: TStringField;
    SDLojaHOMOLOGACAOPRODUCAO: TStringField;
    SDLojaIMPRESSAONF: TStringField;
    SDLojaREGIMETRIBUTARIO: TFMTBCDField;
    SDLojaSIMPLESNACIONAL: TStringField;
    SDLojaALIQUOTAAPROVEITAMENTO: TFMTBCDField;
    SDLojaMODELONFCE: TFMTBCDField;
    SDLojaIMPRESSAONFCE: TStringField;
    SDLojaTIPOEMISSAONFCE: TStringField;
    SDLojaSERIENFCE: TStringField;
    SDLojaIDTOKEN: TStringField;
    SDLojaCSC: TStringField;
    SDLojaULTIMONSUCOMPRAS: TStringField;
    SDLojaALIQUOTAIMPOSTOAPROXIMADO: TFMTBCDField;
    SDLojaSERIENFSE: TStringField;
    SDLojaCHAVENFSE: TStringField;
    SDLojaCNAE: TFMTBCDField;
    SDLojaCODIGOTRIBUTACAOMUNICIPIO: TStringField;
    SDLojaINSCRICAOMUNICIPAL: TStringField;
    SDLojaREGIMEESPECIALTRIBUTACAO: TFMTBCDField;
    SDLojaINCENTIVOFISCAL: TFMTBCDField;
    SDLojaOPTANTESIMPLESNACIONAL: TFMTBCDField;
    SDLojaIMPRESSAONFSE: TStringField;
    SDLojaVALORMINIMORETENCOES: TFMTBCDField;
    SDLojaALIQUOTARETENCAOPIS: TFMTBCDField;
    SDLojaALIQUOTARETENCAOCOFINS: TFMTBCDField;
    SDLojaALIQUOTARETENCAOCSLL: TFMTBCDField;
    SDLojaVALORMINIMORETENCAOIR: TFMTBCDField;
    SDLojaALIQUOTARETENCAOIR: TFMTBCDField;
    SDLojaCODIGOSERVICO: TStringField;
    SDLojaALIQUOTAISS: TFMTBCDField;
    SDLojaIMPOSTOPIS: TFMTBCDField;
    SDLojaIMPOSTOCOFINS: TFMTBCDField;
    SDLojaIMPOSTOCSLL: TFMTBCDField;
    SDLojaIMPOSTOIRPJ: TFMTBCDField;
    SDLojaHOMOLOCACAOPRODUCAONFSE: TFMTBCDField;
    SDLojaCLIENTEEXTERNO: TFMTBCDField;
    SDLojaCLIENTE: TFMTBCDField;
    SDLojaTEMSINCRONIA: TStringField;
    QrCarteiras: TMySQLQuery;
    QrCarteirasNome: TWideStringField;
    QrCarteirasCodigo: TIntegerField;
    SDLancamento: TSimpleDataSet;
    SDLancamentoAUTORIZACAO: TFMTBCDField;
    SDLancamentoQUITADO: TStringField;
    SDLancamentoDOCUMENTO: TStringField;
    SDLancamentoEMISSAO: TDateField;
    SDLancamentoCLIENTE: TFMTBCDField;
    SDLancamentoMODO: TStringField;
    SDLancamentoVENCIMENTO: TDateField;
    SDLancamentoVALOR: TFMTBCDField;
    SDLancamentoDESCRICAO: TStringField;
    SDLancamentoVENDEDOR: TFMTBCDField;
    SDLancamentoORCAMENTO: TFMTBCDField;
    SDLancamentoHISTORICO: TFMTBCDField;
    SDLancamentoPAGAMENTO: TDateField;
    SDLancamentoVENCIMENTOORIGINAL: TDateField;
    SDLancamentoCOBRADOR: TFMTBCDField;
    SDLancamentoUSUARIO: TStringField;
    SDLancamentoLOJA: TStringField;
    SDLancamentoRECIBO: TFMTBCDField;
    SDLancamentoTERCEIRO: TStringField;
    SDLancamentoANO: TFMTBCDField;
    SDLancamentoDUPLICATA: TFMTBCDField;
    SDLancamentoDATA: TDateField;
    SDLancamentoHORA: TTimeField;
    SDLancamentoUSUARIOCANCQUITACAO: TStringField;
    SDLancamentoTROCA: TStringField;
    SDLancamentoNOTACOMPRA: TFMTBCDField;
    SDLancamentoMODELOCOMPRA: TFMTBCDField;
    SDLancamentoSERIECOMPRA: TStringField;
    SDLancamentoNOMECHEQUE: TStringField;
    SDLancamentoMOTIVOEXCLUSAO: TStringField;
    SDLancamentoNOTAFISCALVENDA: TFMTBCDField;
    SDLancamentoSERIEVENDA: TStringField;
    SDLancamentoCOBRADORANTERIOR: TFMTBCDField;
    SDLancamentoNOSSONUMERO: TStringField;
    SDLancamentoAUTORIZACAOCHEQUECAIXA: TFMTBCDField;
    SDLancamentoACORDO: TFMTBCDField;
    SDLancamentoRECEBIMENTOTITULO: TStringField;
    SDLancamentoSTATUSCOBRANCA: TFMTBCDField;
    SDLancamentoOS: TFMTBCDField;
    SDLancamentoLOCACAO: TFMTBCDField;
    QrFbAux: TMySQLQuery;
    BtCorrige: TBitBtn;
    TabSheet9: TTabSheet;
    Panel20: TPanel;
    SGContas: TStringGrid;
    Button2: TButton;
    BitBtn1: TBitBtn;
    SDTabelaXML: TSimpleDataSet;
    Panel21: TPanel;
    MeQry: TMemo;
    SDQry: TSimpleDataSet;
    DsQry: TDataSource;
    QrClienteFone: TMySQLQuery;
    QrClienteEmail: TMySQLQuery;
    QrClienteFoneCLIENTE: TLargeintField;
    QrClienteFoneTELEFONE: TWideStringField;
    QrClienteFoneCONTATO: TWideStringField;
    QrClienteFonePRINCIPAL: TWideStringField;
    QrClienteEmailCLIENTE: TLargeintField;
    QrClienteEmailEMAIL: TWideStringField;
    QrClienteEmailCONTATO: TWideStringField;
    QrClienteEmailNFE: TWideStringField;
    QrClienteEmailCOBRANCA: TWideStringField;
    QrAux1: TMySQLQuery;
    BitBtn4: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImportaDadosClick(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure MeSQL1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CGDBFClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SbLoadCSVOthDirClick(Sender: TObject);
    procedure EdLoadCSVOthDirChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure SbGBakClick(Sender: TObject);
    procedure SbArqFB_BkpClick(Sender: TObject);
    procedure BtIB_DBClick(Sender: TObject);
    procedure BtEspelhaClick(Sender: TObject);
    procedure BtCriaEstruturaClick(Sender: TObject);
    procedure BtNenhumDBAntClick(Sender: TObject);
    procedure BtTodosDBAntClick(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MeQryKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn4Click(Sender: TObject);
  private
    { Private declarations }
    FTabTabs: String;
    FTabLctA, FTabela, FTabStr: String;
    FCargoSocio, FPrincipalCli, FConComprador, FPrincipalFor,
    FFoneFix, FFoneCel, FFoneFax, FEMail, FETP_SERASA, FETP_SCPC, FETP_ACIM: Integer;
    FArquivoGraGruY, FArquivoTributacaoST: String;
    FGraGruY, FTributacaoST: array of Integer;
    F_LOCACAOITENS_PRODUTO_GGX, F_LOCACAOITENS_PRODUTO_CTRID,
    F_LOCACAOITENS_PRODUTO_ITEM, F_LOCACAOITENS_PRODUTO_GTRTAB: array of array of Integer;
    //
    procedure Avisa(Str: String; Aguarde: Boolean = True);
    procedure ReabrePsqEnt(Antigo: String);
    procedure ReabreTabela(Table: TSimpleDataSet);
    //
    function PV_N(Texto: String): String;
    function PV_A(Texto: String): String;
    //
    function  VerificaCadastro(Tabela, Codigo, CodUsu, Nome,
              Texto: String; NovoCodigo: TNovoCodigo): Integer;
{
    procedure IncluiSocio(Codigo, CargoSocio: Integer;
              Nome, _RG, _CPF: String; Natal: TDateTime);
    procedure IncluiContato(Codigo, Cargo: Integer;
              Nome, Tel, Cel, Fax, Mail: String);
    procedure IncluiEntiTel(Codigo, Controle, EntiTipCto: Integer;
              Tel: String);
    procedure IncluiEntiMail(Codigo, Controle, EntiTipCto: Integer;
              EMail: String);
    procedure IncluiBanco(Codigo, Ordem: Integer; Nome, Bco, Age: String);
    procedure IncluiConsulta(Codigo, Orgao: Integer;
              Numero: String; _Data: TDateTime; _Hora, _Resultado: String);
}
    //
    procedure CadastraPrdGrupTip();
(*
    procedure CadastraGraGru1SemGrade(PrdGrupTip, Nivel5, Nivel4, Nivel3,
              Nivel2, Nivel1, CodUsu: Integer; Nome, Referencia: String);
*)
    function  TabelaEstahSelecionada(Tabela: String): Boolean;
    //
    function  ObtemGraGruXDeCODMATX(CODMATX: String): Integer;
    //
    procedure CadastraSituacoes();

    //
{
    procedure ImportaTabela_CadCli();
    procedure ImportaTabela_CadFor();
    procedure ImportaTabela_CadGru();
    procedure ImportaTabela_CadPat();
    procedure ImportaTabela_CadCtr();
    procedure ImportaTabela_CadVde();
    procedure ImportaTabela_CadBco();
    procedure ImportaTabela_CadNat();
    procedure ImportaTabela_PagDup();
    procedure ImportaTabela_RecDup();
}

////////////////////////////////////////////////////////////////////////////////
    // deprecado
    procedure ImportaTabelaFB_Cliente2();
    //
    function  GetNivel2(Grupo, Linha: Integer): Integer;
    procedure ImportaTabelaFB_Gerencia();
    procedure ImportaTabelaFB_Loja();
    procedure ImportaTabelaFB_StatusEnti();
    procedure ImportaTabelaFB_Cliente();
    procedure ImportaTabelaFB_ClienteStatus();
    procedure ImportaTabelaFB_AnexoDocumento();
    procedure ImportaTabelaFB_Marca();
    procedure ImportaTabelaFB_Grupo();
    procedure ImportaTabelaFB_Linha();
    procedure ImportaTabelaFB_Produto();
    procedure ImportaTabelaFB_ProdutoAcessorio();
    procedure ImportaTabelaFB_FornecedorProduto();
    procedure ImportaTabelaFB_Locacao();
    procedure ImportaTabelaFB_LocacaoItens();
    procedure ImportaTabelaFB_LocacaoHistorico();
    procedure ImportaTabelaFB_LocacaoTrocaMotivo();
    procedure ImportaTabelaFB_LocacaoMovimento();
    procedure ImportaTabelaFB_Lancamento();
    procedure ImportaTabelaFB_ClienteFone();
    procedure ImportaTabelaFB_ClienteEmail();

    procedure AbreTabela(Tabela: String);
    function AdicionaEntiContat(Codigo, Controle: Integer; Nome: String): Integer;
{
    procedure ImportaTabela_PagDup;
    procedure ImportaTabela_RecDup;
}
    procedure AtualizaSaldosLocados();
    //
    function  CarregaGraGruY(): Boolean;
    function  CarregaTributacaoST(): Boolean;
    procedure CriaEstrutura();
    procedure EspelhaDados();
    procedure EstruturaBDs();
    procedure AbreTabelaTabelas();
    function  Conectou(DBNome: String; Criar: Boolean): Boolean;
    procedure EstruturaTabela(NomeTab: String; SD: TSimpleDataset);
    function  ExcluiTabela(QryAux: TMySQLQuery; DB, Tabela: String): Boolean;
    function  TabelaExiste(QryAux: TMySQLQuery; DB, Tabela: String): Boolean;
    procedure ConectaID_DB();
    //
  public
    { Public declarations }
  end;

  var
  FmDB_Converte_Tisolin: TFmDB_Converte_Tisolin;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModProd, ModuleGeral,
  UnFinanceiro, UnGrade_Jan, GraGruX, GraGruY, MyMySQL, MyGlyfs, ParamsEmp,
  Matriz, MyDBCheck, UnAppPF;

{$R *.DFM}

var
  LocDatas: array of String;
  LocHoras: array of String;
  DevDatas: array of String;
  DevHoras: array of String;
  //Acessorio: array of Boolean;
  TipoLoc: array of String[1];

procedure TFmDB_Converte_Tisolin.AbreTabela(Tabela: String);
begin
  Exit;
  //
  SDDados.Active := False;
  SDDados.DataSet.CommandType := ctQuery;
  SDDados.DataSet.CommandText :=
  //'select * from ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
  'select * from ' + Tabela;
  SDDados.Active := True;
end;

procedure TFmDB_Converte_Tisolin.AbreTabelaTabelas();
begin
  if not DBAnt.Connected then
  begin
    if not Conectou(EdDBNome.Text, True) then
    begin
      Geral.MB_Erro('N�o foi poss�vel conectar no BD ' + EdDBNome.Text);
      Exit;
    end;
  end;
  try
    UnMyMySQL.AbreMySQLQuery1(QrTabelas, [
    'SELECT DISTINCT Tabela ',
    'FROM ' + FTabTabs,
    'ORDER BY Tabela ',
    '']);
    BtEspelha.Enabled      := SDFields.Active;
    BtImportaDados.Enabled := QrTabelas.RecordCount > 0;
  except;
    BtEspelha.Enabled := False;
    BtImportaDados.Enabled := False;
(*
    if Geral.MB_Pergunta(
    'Tabela das tabelas ainda n�o criada ou com erro ao abrir!' + sLineBreak +
    'Deseja executar "&1 Cria estrutura" agora para tentar abrir novamente?') = ID_YES then
    begin
      CriaEstrutura();
    end;
*)
  end;
  SDAux.Close;
  SDAux.DataSet.CommandText := 'SELECT * FROM historicocaixa';
  SDAux.Open;
  //
  SGContas.RowCount := SDAux.RecordCount + 1;
  SDAux.First;
  while SDAux.Eof do
  begin
    SGContas.Cells[2, SDAux.RecNo] := SDAux.FieldByName('HISTORICOCAIXA').AsString;
    SGContas.Cells[3, SDAux.RecNo] := SDAux.FieldByName('DESCRICAO').AsString;
    //
    SDAux.Next;
  end;
end;

function TFmDB_Converte_Tisolin.AdicionaEntiContat(Codigo, Controle: Integer;
  Nome: String): Integer;
var
  //Nome, DtaNatal, CNPJ, CPF: String;
  //Codigo, Controle, Cargo, DiarioAdd, Sexo, Tipo, Aplicacao, Entidade: Integer;
  SQLType: TSQLType;
begin
  Result         := 0;
  SQLType        := stIns;
{
  Codigo         := ;
  Controle       := 0;
  Nome           := ;
  Cargo          := ;
  DiarioAdd      := ;
  DtaNatal       := ;
  Sexo           := ;
  Tipo           := ;
  CNPJ           := ;
  CPF            := ;
  Aplicacao      := ;
  Entidade       := ;
}
  //
  Controle       := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', SQLType, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'enticontat', False, [
  'Codigo', 'Nome'(*, 'Cargo',
  'DiarioAdd', 'DtaNatal', 'Sexo',
  'Tipo', 'CNPJ', 'CPF',
  'Aplicacao', 'Entidade'*)], [
  'Controle'], [
  Codigo, Nome(*, Cargo,
  DiarioAdd, DtaNatal, Sexo,
  Tipo, CNPJ, CPF,
  Aplicacao, Entidade*)], [
  Controle], True) then
  begin
    Result := Controle;
    DModG.AtualizaEntiConEnt();
  end;
end;

procedure TFmDB_Converte_Tisolin.AtualizaSaldosLocados();
var
  GraGruX: Integer;
  //EstqAnt, EstqEnt, EstqSdo,
  EstqSai: Double;
  //SQLType: TSQLType;
  sEstqLoc: String;
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Atualiza��o de estoque de produtos de loca��o');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocados, FbDB, [
(*
    'DROP TABLE IF EXISTS _LOCACAOES_ABERTAS_; ',
    'CREATE TABLE _LOCACAOES_ABERTAS_ ',
    '      SELECT DISTINCT LOCACAO ',
    '      FROM LOCACAO ',
    '      WHERE FECHAMENTODATA < "1900-01-01" ',
    '      AND STATUS=2; ',
    'SELECT PRODUTO,  ',
    '(QUANTIDADEPRODUTO-QUANTIDADEDEVOLUCAO) QtdLocado ',
    'FROM LOCACAOITENS its ',
    'WHERE CATEGORIA="L" ',
    '/*AND PRODUTO  = 37*/ ',
    'AND QUANTIDADEDEVOLUCAO<QUANTIDADEPRODUTO ',
    'AND LOCACAO IN ( ',
    '  SELECT LOCACAO FROM _LOCACAOES_ABERTAS_ ',
    ') ',
    'GROUP BY PRODUTO ',
    'ORDER BY PRODUTO; ',
*)
    'SELECT PRODUTO, SUM((its.QUANTIDADELOCACAO * its.QUANTIDADEPRODUTO)-its.QUANTIDADEDEVOLUCAO) + 0.000 QtdLocado ',
    'FROM LOCACAOITENS its ',
    'LEFT JOIN LOCACAO loc ON loc.LOCACAO=its.Locacao ',
    'WHERE its.CATEGORIA="L" ',
    'AND its.QUANTIDADEDEVOLUCAO<(its.QUANTIDADELOCACAO * its.QUANTIDADEPRODUTO)',
    'AND (its.QUANTIDADELOCACAO * its.QUANTIDADEPRODUTO) > 0',
    'AND loc.FECHAMENTODATA < "1900-01-01" ',
    'AND loc.STATUS=2 ',
    'GROUP BY its.PRODUTO ',
    'ORDER BY its.PRODUTO ',
    '']);
    QrLocados.First;
    while not QrLocados.Eof do
    begin
      GraGruX        := QrLocadosPRODUTO.Value;
      EstqSai        := QrLocadosQtdLocado.Value;
      SEstqLoc       := Geral.FFT_Dot(EstqSai, 3, siPositivo);
      //
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
        'UPDATE gragruepat SET EstqLoc = ' + sEstqLoc +
        ', EstqAnt = EstqAnt + ' + sEstqLoc +
        ' WHERE GraGruX=' + Geral.FF0(GraGruX));
      //
      QrLocados.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDB_Converte_Tisolin.Avisa(Str: String; Aguarde: Boolean);
begin
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, Aguarde, FTabStr + Str);
end;

procedure TFmDB_Converte_Tisolin.BitBtn1Click(Sender: TObject);
var
  NOTAFISCAL, MODELO: Integer;
  LOJA, SERIE, XMLNota, XMLCancelamento: String;
begin
  SDTabelaXML.Close;
  SDTabelaXML.Connection := DBFB;
  SDTabelaXML.DataSet.CommandType := ctQuery;
  SDTabelaXML.DataSet.CommandText := 'SELECT * FROM tabelaxml';
  SDTabelaXML.Open;
  Screen.Cursor := crHourGlass;
  try
    PB1.Position := 0;
    PB1.Max := SDTabelaXML.RecordCount;
    SDTabelaXML.First;
    while not SDTabelaXML.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAvisoG1, LaAvisoG2);
      //if SDTabelaXML.FieldByName('XMLNOTA').AsString <> EmptyStr then
      begin
        LOJA        := SDTabelaXML.FieldByName('LOJA').AsString;
        NOTAFISCAL  := SDTabelaXML.FieldByName('NOTAFISCAL').AsInteger;
        MODELO      := SDTabelaXML.FieldByName('MODELO').AsInteger;
        SERIE       := SDTabelaXML.FieldByName('SERIE').AsString;
        //
        XMLNota := Geral.WideStringToSQLString(SDTabelaXML.FieldByName('XMLNOTA').AsString);
        XMLCancelamento := Geral.WideStringToSQLString(SDTabelaXML.FieldByName('XMLCANCELAMENTO').AsString);
        //
        UMyMod.SQLInsUpd(QrFBAux, stUpd, 'tabelaxml', False, [
        'XMLNOTA', 'XMLCancelamento'], [
        'LOJA', 'NOTAFISCAL', 'MODELO', 'SERIE'], [
        XMLNota, XMLCancelamento], [
        LOJA, NOTAFISCAL, MODELO, SERIE], False);
      end;
      SDTabelaXML.Next;
    end;

  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDB_Converte_Tisolin.BitBtn2Click(Sender: TObject);
var
  //LocacaoAnt,
  LocacaoAtu, CtrID, ItensNaLocacao: Integer;
  CtrTemPri, CtrTemOut: Boolean;
  LocacaoesComProblema: String;
  LocacaoesItemUnico: String;
  ContaLCP, ContaLIU: Integer;
begin
  if not CarregaGraGruY() then
    Exit;
  //
  if not CarregaTributacaoST() then
    Exit;
  //
  LocacaoesComProblema := EmptyStr;
  LocacaoesItemUnico   := EmptyStr;
  ContaLCP := 0;
  ContaLIU := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSeqIL, Dmod.MyDB, [
  'SELECT ggx.GraGruY, Its.*  ',
  'FROM zzz_fb_db_ant.LOCACAOITENS its ',
  'LEFT JOIN ' + TMeuDB + '.GraGruX ggx  ',
  '  ON ggx.Controle=its.PRODUTO ',
  //'/*WHERE its.CATEGORIA = "L"*/ ',
  'WHERE its.CATEGORIA = "L"',
  'ORDER BY its.LOCACAO, its.SEQUENCIA ',
  '']);
  //LocacaoAnt := 0;
  LocacaoAtu := 0;
  CtrID := 0;
  CtrTemPri  := True;
  CtrTemOut  := True;
  //
  QrSeqIL.First;
  while not QrSeqIL.Eof do
  begin
    if QrSeqILLocacao.Value <> LocacaoAtu then
    begin

      if (CtrTemOut = True) and (CtrTemPri = False) then
      begin
        if ItensNaLocacao = 1 then
        begin
          LocacaoesItemUnico   := LocacaoesComProblema + ',' + Geral.FF0(LocacaoAtu);
          ContaLIU := ContaLIU + 1;
        end else
        begin
          LocacaoesComProblema := LocacaoesComProblema + ',' + Geral.FF0(LocacaoAtu);
          ContaLCP := ContaLCP + 1;
          //
          Geral.MB_Info(Geral.FF0(QrSeqILLocacao.Value));
        end;
      end;
      //
      LocacaoAtu := QrSeqILLocacao.Value;
      ItensNaLocacao := 1;
      CtrID      := CtrID + 1;
      CtrTemPri  := False;
      CtrTemOut  := False;
    end else
      ItensNaLocacao := ItensNaLocacao + 1;
    if QrSeqILGraGruY.Value = 1024 then
    begin
      if CtrTemPri = True then
        CtrID := CtrID + 1
      else
        CtrTemPri := True;
      //
      // Insere aqui PRI
      //
    end else
    begin
      if CtrTemPri = False then
      begin
        CtrTemOut := True;
      //
      // Insere aqui PRI
      //
      end;
    end;
    //
    QrSeqIL.Next;
  end;
  Geral.MB_Info('Loca��es com problema: ' + Geral.FF0(ContaLCP) + sLineBreak +
  LocacaoesComProblema);
  Geral.MB_Info('Loca��es com item �nico sem principal: ' + Geral.FF0(ContaLIU) + sLineBreak +
  LocacaoesItemUnico);
end;

procedure TFmDB_Converte_Tisolin.BitBtn3Click(Sender: TObject);
var
  PathGBak, ArqFB_Bkp, IB_DB: String;
  Texto1: WideString;
begin
  Screen.Cursor := crHourGlass;
  try
    PathGBak := EdPathGBak.Text;
    IB_DB    := EdIB_DB.Text;
    ArqFB_Bkp := EdArqFB_Bkp.Text;

    Texto1 := String(dmkPF.NomeLongoParaCurto(AnsiString(PathGBak))) +
    'gbak -create -FIX_FSS_METADATA ISO8859_1 -v ' +
    String(dmkPF.NomeLongoParaCurto(AnsiString(ArqFB_Bkp))) +
    ' ' +
    String(dmkPF.NomeLongoParaCurto(AnsiString(IB_DB))) +
    ' -user SYSDBA -pass masterkey';
    MeCmd.Text := Texto1;
    //
    //CD C:\Program Files (x86)\Firebird\Firebird_2_5\bin\
    //CD C:\Program Files (x86)\Firebird\Firebird_2_5\bin\gbak -create -FIX_FSS_METADATA ISO8859_1 -v C:\Teste\Teste.bkp C:\teste\teste5.fdb -user SYSDBA -pass masterkey;
    MyObjects.ExecutaCmd(Texto1, MeResult_);
    //
    FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDB_Converte_Tisolin.BitBtn4Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a importa��o das tabelas de telefone e email?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> IDYES then
    Exit;
  //
  //if TabelaEstahSelecionada('ClienteFone') then
  ImportaTabelaFB_ClienteFone();
  //if TabelaEstahSelecionada('ClienteEmail') then
  ImportaTabelaFB_ClienteEmail();
{
  anexoDocumento
                /
  produtos:
  GRUPO
}
  //.......
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Importa��o finalizada!');
  //
  FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
end;

procedure TFmDB_Converte_Tisolin.BtCorrigeClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Corrigindo "Controle.stqmovusu"!');
  Dmod.MyDB.Execute(
  'UPDATE Controle SET stqmovusu=(SELECT Max(CodUsu) CodUsu FROM fatpedcab )');
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Corrigindo "Controle.Lanctos"!');
  Dmod.MyDB.Execute(
  'UPDATE Controle SET Lanctos =(SELECT Max(Controle) FROM lct0001a)');

  //

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Verificar filial"!');
  if Geral.MB_Pergunta('Deseja verificar filial?') = ID_YES then
  begin
    if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
    begin
      FmParamsEmp.ShowModal;
      FmParamsEmp.Destroy;
    end;
  end;

    //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Verificar matriz"!');
  if Geral.MB_Pergunta('Deseja verificar a matriz?') = ID_YES then
  begin
    if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
    begin
      FmMatriz.ShowModal;
      FmMatriz.Destroy;
    end;
  end;

  FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
  //

    //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Verificar estoque de loca��o"!');
  if Geral.MB_Pergunta('Deseja verificar os estoques de loca��o?') = ID_YES then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * FROM gragxpatr ',
    '']);
    PB1.Max := Dmod.QrAux.RecordCount;
    PB1.Position := 0;
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      GraGruX := Dmod.QrAux.FieldByName('GraGruX').AsInteger;
      MyObjects.Informa2EUpdPB(PB1, LaAvisoG1, LaAvisoG2, True,
        'Equipamento c�digo: ' + Geral.FF0(GraGruX));
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, -11, True);
      //
      Dmod.QrAux.Next;
    end;
  end;

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Corre��es realizadas!');
end;

procedure TFmDB_Converte_Tisolin.BtCriaEstruturaClick(Sender: TObject);
begin
  CriaEstrutura();
  //
  FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
  //
end;

procedure TFmDB_Converte_Tisolin.BtExecutaClick(Sender: TObject);
begin
{20200920
  Screen.Cursor := crHourGlass;
  try
    QrPsq.Close;
    QrPsq.SQL.Text := MeSQL1.Text;
    QrPsq.Open;
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmDB_Converte_Tisolin.BtIB_DBClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DBFB.Connected := False;
    DBFB.ConnectionName := 'FBConnection';
    //DBFB.Driver := 'FireBird';
    DBFB.Params.Text := Geral.ATS([
      'DriverName=Firebird',
      'Database=' + EdIB_DB.Text,
      'RoleName=RoleName',
      'User_Name=sysdba',
      'Password=masterkey',
      'ServerCharSet=',
      'SQLDialect=3',
      'ErrorResourceFile=',
      'LocaleCode=0000',
      'BlobSize=-1',
      'CommitRetain=False',
      'WaitOnLocks=True',
      'IsolationLevel=ReadCommitted',
      'Trim Char=False',
        'ConnectionString=DriverName=Firebird,Database='
        + EdIB_DB.Text +
        ',RoleName=RoleName,User_Name=sysdba,Password=masterkey,ServerCh' +
        'arSet=,SQLDialect=3,ErrorResourceFile=,LocaleCode=0000,BlobSize=' +
        '-1,CommitRetain=False,WaitOnLocks=True,IsolationLevel=ReadCommit' +
        'ted,Trim Char=False']);
    DBFB.Connected := True;
    //
    //Geral.MB_Teste(DBFB.Params.Text);
    SDFields.Active := False;
    SDFields.Active := True;
    //
    //BtErrado.Enabled := SDFields.Active;
    //
    BtCriaEstrutura.Enabled := SDFields.Active;
    //
    FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
    //
  finally
    Screen.Cursor := crDefault;
  end;
  AbretabelaTabelas();
end;

procedure TFmDB_Converte_Tisolin.BtEspelhaClick(Sender: TObject);
begin
  EspelhaDados();
  //
  FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
  //
end;

procedure TFmDB_Converte_Tisolin.BtNenhumClick(Sender: TObject);
begin
  CGDBF.Value := 0;
end;

procedure TFmDB_Converte_Tisolin.BtNenhumDBAntClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGTabelas), False);
end;

procedure TFmDB_Converte_Tisolin.BtImportaDadosClick(Sender: TObject);
var
  I, p: Integer;
  sNoGGY: String;
begin
  if PageControl2.ActivePageIndex <> 1 then
  begin
    PageControl2.ActivePageIndex := 1;
    Exit;
  end;
  MeErrosGerais.Lines.Clear;
  MePronto.Lines.Clear;
  DBFB.Connected := False;
  DBFB.Params.Text := Geral.ATS([
    'DriverName=Firebird',
    'Database=' + EdIB_DB.Text,
    'RoleName=RoleName',
    'User_Name=sysdba',
    'Password=masterkey',
    'ServerCharSet=',
    'SQLDialect=3',
    'ErrorResourceFile=',
    'LocaleCode=0000',
    'BlobSize=-1',
    'CommitRetain=False',
    'WaitOnLocks=True',
    'IsolationLevel=ReadCommitted',
    'Trim Char=False',
      'ConnectionString=DriverName=Firebird,Database='
      + EdIB_DB.Text +
      ',RoleName=RoleName,User_Name=sysdba,Password=masterkey,ServerCh' +
      'arSet=,SQLDialect=3,ErrorResourceFile=,LocaleCode=0000,BlobSize=' +
      '-1,CommitRetain=False,WaitOnLocks=True,IsolationLevel=ReadCommit' +
      'ted,Trim Char=False']);
  DBFB.Connected := True;
  if not FbDB.Connected then Exit;
  //

  //
  if Application.MessageBox('Confirma a importa��o das tabelas selecionadas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> IDYES then
    Exit;
  //
  SetLength(F_LOCACAOITENS_PRODUTO_GGX, 35000);
  SetLength(F_LOCACAOITENS_PRODUTO_ITEM, 35000);
  SetLength(F_LOCACAOITENS_PRODUTO_CTRID, 35000);
  SetLength(F_LOCACAOITENS_PRODUTO_GTRTAB, 35000);
(*
  for I := 0 to 99999 do
  begin
    //SetLength(F_LOCACAOITENS_PRODUTO_CTRID[I], 10000);
    //SetLength(F_LOCACAOITENS_PRODUTO_ITEM[I], 10000);
  end;
  //
*)

  if (TabelaEstahSelecionada('LocacaoItens'))
  or (TabelaEstahSelecionada('Produto')) then
  begin
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Verificando lista de GrGruX pr�-definida em arquivo CSV!');
    if Length(FGraGruY) < 1 then
    begin
      if not CarregaGraGruY() then
      begin
        Exit;
      end;
      UnDMkDAC_PF.AbreMySQLQuery0(QrPRODUTO, DBAnt, [
      'SELECT PRODUTO, DESCRICAO ',
      'FROM PRODUTO ',
      'WHERE CATEGORIA = "L" ',
      'OR PATRIMONIO <> "" ',
      'ORDER BY PRODUTO ',
      '']);
      QrPRODUTO.First;
      sNoGGY := EmptyStr;
      while not QrPRODUTO.Eof do
      begin
        if FGraGruY[QrPRODUTOPRODUTO.Value] < 1 then
        begin
          sNoGGY := sNoGGY + Geral.FF0(QrPRODUTOPRODUTO.Value) + ' - ' +
            QrPRODUTODESCRICAO.Value + sLineBreak;
        end;
        //
        QrPRODUTO.Next;
      end;
      if sNoGGY <> EmptyStr then
      begin
        Geral.MB_Erro(
        'Produtos sem GraGruY no arquivo ' + EdLoadCSVOthDir.Text + '\GraGruY.csv' +
        sLineBreak + sNoGGY);
        Exit;
      end;
    end;

    /////////////////////////////////////////////////////////////////////////////

    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Verificando lista de S.T. pr�-definida em arquivo CSV!');
    if Length(FTributacaoST) < 1 then
    begin
      if not CarregaTributacaoST() then
      begin
        Exit;
      end;
(*
      UnDMkDAC_PF.AbreMySQLQuery0(QrPRODUTO, DBAnt, [
      'SELECT PRODUTO, DESCRICAO ',
      'FROM PRODUTO ',
      'WHERE CATEGORIA = "L" ',
      'OR PATRIMONIO <> "" ',
      'ORDER BY PRODUTO ',
      '']);
*)
      QrPRODUTO.First;
      sNoGGY := EmptyStr;
      while not QrPRODUTO.Eof do
      begin
        if FTributacaoST[QrPRODUTOPRODUTO.Value] < 0 then
        begin
          sNoGGY := sNoGGY + Geral.FF0(QrPRODUTOPRODUTO.Value) + ' - ' +
            QrPRODUTODESCRICAO.Value + sLineBreak;
        end;
        //
        QrPRODUTO.Next;
      end;
      if sNoGGY <> EmptyStr then
      begin
        Geral.MB_Erro(
        'Produtos sem defini��o de ICMS ST no arquivo ' + EdLoadCSVOthDir.Text + '\TributacaoST.csv' +
        sLineBreak + sNoGGY);
        Exit;
      end;
    end;

  end;
  //
  //  Cadastros
  if TabelaEstahSelecionada('Gerencia') then ImportaTabelaFB_Gerencia();
  if TabelaEstahSelecionada('Loja') then ImportaTabelaFB_Loja();
  if TabelaEstahSelecionada('StatusCliente') then ImportaTabelaFB_StatusEnti();
  if TabelaEstahSelecionada('Cliente') then ImportaTabelaFB_Cliente();
  if TabelaEstahSelecionada('ClienteStatus') then ImportaTabelaFB_ClienteStatus();
  if TabelaEstahSelecionada('AnexoDocumento') then ImportaTabelaFB_AnexoDocumento();
  if TabelaEstahSelecionada('Marca') then ImportaTabelaFB_Marca();
  if TabelaEstahSelecionada('Grupo') then ImportaTabelaFB_Grupo();
  if TabelaEstahSelecionada('Linha') then ImportaTabelaFB_Linha();

  if TabelaEstahSelecionada('Produto') then ImportaTabelaFB_Produto();
  if TabelaEstahSelecionada('ProdutoAcessorio') then ImportaTabelaFB_ProdutoAcessorio();
  if TabelaEstahSelecionada('FornecedorProduto') then ImportaTabelaFB_FornecedorProduto();
  if TabelaEstahSelecionada('Locacao') then ImportaTabelaFB_Locacao();
  if TabelaEstahSelecionada('LocacaoItens') then ImportaTabelaFB_LocacaoItens();
  if TabelaEstahSelecionada('LocacaoTrocaMotivo') then ImportaTabelaFB_LocacaoTrocaMotivo();
  if TabelaEstahSelecionada('LocacaoMovimento') then ImportaTabelaFB_LocacaoMovimento();
  if TabelaEstahSelecionada('LocacaoHistorico') then ImportaTabelaFB_LocacaoHistorico();

  if TabelaEstahSelecionada('Lancamento') then ImportaTabelaFB_Lancamento();
  if TabelaEstahSelecionada('ClienteFone') then ImportaTabelaFB_ClienteFone();
  if TabelaEstahSelecionada('ClienteEmail') then ImportaTabelaFB_ClienteEmail();
{
  anexoDocumento

  produtos:
  GRUPO
}
  //.......
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, 'Importa��o finalizada!');
  //
  FmMyGlyfs.ObtemBitmapDeGlyph(0194, TBitBtn(Sender));
  //

end;

procedure TFmDB_Converte_Tisolin.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDB_Converte_Tisolin.BtTodosDBAntClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(DBGTabelas), True);
end;

procedure TFmDB_Converte_Tisolin.BtTudoClick(Sender: TObject);
begin
  CGDBF.SetMaxValue;
end;

procedure TFmDB_Converte_Tisolin.Button1Click(Sender: TObject);
begin
  CadastraSituacoes();
end;

procedure TFmDB_Converte_Tisolin.Button2Click(Sender: TObject);
begin
  CarregaTributacaoST();
end;

{
procedure TFmDB_Converte_Tisolin.CadastraGraGru1SemGrade(
  PrdGrupTip, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu: Integer;
  Nome, Referencia, Patrimonio: String);
var
  Controle, GraCorCad: Integer;
  GraTamCad, GraGruC, GraGru1, GraTamI: Integer;
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1', False, [
  'Nivel5', 'Nivel4', 'Nivel3', 'Nivel2', 'CodUsu',
  'Nome', 'PrdGrupTip', 'Referencia', 'Patrimonio'], [
  'Nivel1'], [
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu,
  Nome, PrdGrupTip, Referencia, Patrimonio], [
  Nivel1], True) then
  begin
    // Definir Grade de tamanhos
    GraTamCad := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
    'GraTamCad'], ['Nivel1'], [
    GraTamCad], [Nivel1], True);

    // Registrar a cor
    //Nivel1         := Nivel1;
    Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
    GraCorCad      := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
    'Nivel1', 'GraCorCad'], [
    'Controle'], [
    Nivel1, GraCorCad], [
    Controle], True) then
    begin
      // Cadastrar o item �nico da grade
      GraGruC        := Controle;
      GraGru1        := Nivel1;
      GraTamI        := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
      Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
      'GraGruC', 'GraGru1', 'GraTamI'], [
      'Controle'], [
      GraGruC, GraGru1, GraTamI], [
      Controle], True) then
      begin
        VAR_CADASTRO2 := Controle;
      end;
    end;
  end;
end;
}

procedure TFmDB_Converte_Tisolin.CadastraPrdGrupTip();
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero: Integer;
  PerComissF, PerComissR: Double;
begin
  Codigo         := 1;
  CodUsu         := 1;
  Nome           := 'Patrim�nio';
  MadeBy         := 2;
  Fracio         := 0;
  Gradeado       := 0;
  TipPrd         := 3;
  NivCad         := 5;
  FaixaIni       := 0;
  FaixaFim       := 0;
  TitNiv1        := 'Nivel 1';
  TitNiv2        := 'Nivel 2';
  TitNiv3        := 'Nivel 3';
  TitNiv4        := 'Nivel 4';
  TitNiv5        := 'Nivel 5';
  Customizav     := 0;
  ImpedeCad      := 1;
  LstPrcFisc     := 5;
  PerComissF     := 0;
  PerComissR     := 0;
  Tipo_Item      := 0;
  Genero         := 0;

  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
  'CodUsu', 'Nome', 'MadeBy',
  'Fracio', 'Gradeado', 'TipPrd',
  'NivCad', 'FaixaIni', 'FaixaFim',
  'TitNiv1', 'TitNiv2', 'TitNiv3',
  'Customizav', 'ImpedeCad', 'LstPrcFisc',
  'PerComissF', 'PerComissR', 'Tipo_Item',
  'Genero', 'TitNiv4', 'TitNiv5'], [
  'Codigo'], [
  CodUsu, Nome, MadeBy,
  Fracio, Gradeado, TipPrd,
  NivCad, FaixaIni, FaixaFim,
  TitNiv1, TitNiv2, TitNiv3,
  Customizav, ImpedeCad, LstPrcFisc,
  PerComissF, PerComissR, Tipo_Item,
  Genero, TitNiv4, TitNiv5], [
  Codigo], True);
end;

procedure TFmDB_Converte_Tisolin.CadastraSituacoes();
var
  Nome, Sigla: String;
  Codigo, Aplicacao: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSituacao, Dmod.MyDB, [
  'SELECT DISTINCT Situacao, ',
  'CHAR(Situacao) SIGLA, ',
  'COUNT(Situacao) ITENS ',
  'FROM gragxpatr ',
  'GROUP BY Situacao ',
  'ORDER BY ITENS DESC ',
  '']);
  QrSituacao.First;
  while not QrSituacao.Eof do
  begin
    Codigo         := QrSituacaoSituacao.Value;
    Nome           := QrSituacaoSIGLA.Value;
    Sigla          := QrSituacaoSIGLA.Value;
    Aplicacao      := 0;
    if QrSituacaoSIGLA.Value = 'D' then
      Aplicacao := 1;
    //
    UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'graglsitu', False, [
    'Nome', 'Sigla', 'Aplicacao'], ['Codigo'], ['Ativo'], [
    Nome, Sigla, Aplicacao], [Codigo], [1], True);
    //
    QrSituacao.Next;
  end;
end;

function TFmDB_Converte_Tisolin.CarregaGraGruY(): Boolean;
var
  I, Codigo, GraGruY: Integer;
  Linha, Letra: String;
  LstArq, LstLin1(*, LstLin2, LstLin3*): TStringList;
  //Continua: Boolean;
  MaxCod: Integer;
begin
  Result := False;
  MaxCod := 100;
(*&
  AtualizaLog(_STATUS_100, '', EmptyStr, 0);
  //FItens := 0;
*)
  if FileExists(FArquivoGraGruY) then
  begin
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Abrindo arquivo: ' + FArquivoGraGruY + '.');
    LstArq := TStringList.Create;
    try
      LstLin1 := TStringList.Create;
      try
        LstArq.LoadFromFile(FArquivoGraGruY);
        if lstArq.Count > 0 then
        begin
          Linha := ';' + LstArq[0] + ';';
          //
          LstLin1 := Geral.Explode3(Linha, ';');
          if LstLin1.Count > 0 then
          begin
            if (Uppercase(LstLin1[0]) <> Uppercase('PRODUTO'))
            or (Uppercase(LstLin1[1]) <> Uppercase('TIPO')) then
            begin
              Geral.MB_Aviso('Cabe�alho invalido! Deve ser ColA = "Produto" e ColB = "Tipo"');
              Exit;
            end;
          end;
        end;
        //
        SetLength(FGraGruY, MaxCod + 1);
        for I := 0 to MaxCod do
        begin
          FGraGruY[I] := -1;
        end;

        for I := 1 to LstArq.Count - 1 do
        begin
          Linha := ';' + LstArq[I] + ';';
          //
          LstLin1 := Geral.Explode3(Linha, ';');
          if LstLin1.Count > 0 then
          begin
            Codigo := Geral.IMV(LstLin1[0]);
            while Codigo >= MaxCod do
            begin
              MaxCod := MaxCod + 1;
              SetLength(FGraGruY, MaxCod + 1);
              FGraGruY[MaxCod] := -1;
            end;
            //
            Letra  := LstLin1[1];
            if Letra = 'P' then
              GraGruY := CO_GraGruY_1024_GXPatPri
            else
            if Letra = 'S' then
              GraGruY := CO_GraGruY_2048_GXPatSec
            else
            if Letra = 'A' then
              GraGruY := CO_GraGruY_3072_GXPatAce
            else
            if Letra = 'R' then
              GraGruY := CO_GraGruY_4096_GXPatApo // Reposi
            else
            if Letra = 'U' then
              GraGruY := CO_GraGruY_5120_GXPatUso
            else
            if Letra = 'C' then
              GraGruY := CO_GraGruY_6144_GXPrdCns
            else
            if (Letra = 'F')
            or (Letra = '!') then
              GraGruY := CO_GraGruY_7168_GXPatSrv
            else
            if Letra = 'V' then
              GraGruY := CO_GraGruY_8192_GXPrdVen
            else
            begin
              Geral.MB_Erro('Tipo de Produto n�o esperado: ' + Letra);
              GraGruY := 999999999;
            end;
            MePronto.Lines.Add('Item : ' + Geral.FF0(I) + ' >> ' + (LstLin1[0] + ' - ' + LstLin1[1]));
          end;
         // ver aqui Uso >> Produto Reduzido 226 - CONSUMO DISCO !!!
          FGraGruY[Codigo] := GraGruY;
        end;
        Result := True;
      finally
        if LstLin1 <> nil then
          LstLin1.Free;
      end;
    finally
      if LstArq <> nil then
        LstArq.Free;
    end;
  end else
    Geral.MB_Erro('Arquivo n�o localizado: ' + FArquivoGraGruY)
end;

function TFmDB_Converte_Tisolin.CarregaTributacaoST: Boolean;
var
  I, Codigo, UsaSubsTrib: Integer;
  Linha, Situacao: String;
  LstArq, LstLin1(*, LstLin2, LstLin3*): TStringList;
  //Continua: Boolean;
  MaxCod: Integer;
begin
  Result := False;
  MaxCod := 100;
(*&
  AtualizaLog(_STATUS_100, '', EmptyStr, 0);
  //FItens := 0;
*)
  if FileExists(FArquivoTributacaoST) then
  begin
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Abrindo arquivo: ' + FArquivoTributacaoST + '.');
    LstArq := TStringList.Create;
    try
      LstLin1 := TStringList.Create;
      try
        LstArq.LoadFromFile(FArquivoTributacaoST);
        if lstArq.Count > 0 then
        begin
          Linha := ';' + LstArq[0] + ';';
          //
          LstLin1 := Geral.Explode3(Linha, ';');
          if LstLin1.Count > 0 then
          begin
            if (Uppercase(LstLin1[0]) <> Uppercase('CODIGO'))
            or (Uppercase(LstLin1[1]) <> Uppercase('NCM')) then
            if (Uppercase(LstLin1[2]) <> Uppercase('DESCRICAO'))
            or (Uppercase(LstLin1[3]) <> Uppercase('TRIBUTACAO')) then
            begin
              Geral.MB_Aviso('Cabe�alho invalido! Deve ser ColA = "CODIGO", ColB="NCM", ColC="DESCRICAO" e ColD = "TRIBUTACAO"');
              Exit;
            end;
          end;
        end;
        //
        SetLength(FTributacaoST, MaxCod + 1);
        for I := 0 to MaxCod do
        begin
          FTributacaoST[I] := 2;
        end;

        for I := 1 to LstArq.Count - 1 do
        begin
          Linha := ';' + LstArq[I] + ';';
          //
          LstLin1 := Geral.Explode3(Linha, ';');
          if LstLin1.Count > 0 then
          begin
            Codigo := Geral.IMV(LstLin1[0]);
            while Codigo >= MaxCod do
            begin
              MaxCod := MaxCod + 1;
              SetLength(FTributacaoST, MaxCod + 1);
              FTributacaoST[MaxCod] := 2;
            end;
            //
            Situacao  := LstLin1[3];
            if Situacao = 'TRIBUTADO' then
              UsaSubsTrib := 0
            else
            if (Situacao = 'S.T.')
            or (Situacao = 'S.T')
            or (Situacao = 'ST') then
              UsaSubsTrib := 1
            else
            begin
              Geral.MB_Erro('Situa��o de Produto n�o esperado: ' + Situacao);
              UsaSubsTrib := -1;
            end;
            MePronto.Lines.Add('ST Item: ' + Geral.FF0(I) + ' >> ' + (LstLin1[0] + ' - ' + LstLin1[1]));
          end;
         // ver aqui Uso >> Produto Reduzido 226 - CONSUMO DISCO !!!
          FTributacaoST[Codigo] := UsaSubsTrib;
        end;
        Result := True;
      finally
        if LstLin1 <> nil then
          LstLin1.Free;
      end;
    finally
      if LstArq <> nil then
        LstArq.Free;
    end;
  end else
    Geral.MB_Erro('Arquivo n�o localizado: ' + FArquivoTributacaoST)
end;

procedure TFmDB_Converte_Tisolin.CGDBFClick(Sender: TObject);
(*
var
  p: Integer;
  x: String;
*)
begin
(*
  x := Trim(CGDBF.Items[CGDBF.ItemIndex]);
  p := Pos(' ', x);
  if p = 0 then p := 1000;
  EdTabela.Text := Trim(Copy(x, 1, p));
*)
end;

procedure TFmDB_Converte_Tisolin.ConectaID_DB;
begin
  Screen.Cursor := crHourGlass;
  try
    DBFB.Connected := False;
    DBFB.Params.Text := Geral.ATS([
      'DriverName=Firebird',
      'Database=' + EdIB_DB.Text,
      'RoleName=RoleName',
      'User_Name=sysdba',
      'Password=masterkey',
      'ServerCharSet=',
      'SQLDialect=3',
      'ErrorResourceFile=',
      'LocaleCode=0000',
      'BlobSize=-1',
      'CommitRetain=False',
      'WaitOnLocks=True',
      'IsolationLevel=ReadCommitted',
      'Trim Char=False',
        'ConnectionString=DriverName=Firebird,Database='
        + EdIB_DB.Text +
        ',RoleName=RoleName,User_Name=sysdba,Password=masterkey,ServerCh' +
        'arSet=,SQLDialect=3,ErrorResourceFile=,LocaleCode=0000,BlobSize=' +
        '-1,CommitRetain=False,WaitOnLocks=True,IsolationLevel=ReadCommit' +
        'ted,Trim Char=False']);
    DBFB.Connected := True;
    //
    SDFields.Active := False;
    SDFields.Active := True;
    //
    //BtErrado.Enabled := SDFields.Active;
    //
    BtCriaEstrutura.Enabled := SDFields.Active;
  finally
    Screen.Cursor := crDefault;
  end;
  //
  AbretabelaTabelas();
end;

function TFmDB_Converte_Tisolin.Conectou(DBNome: String; Criar: Boolean): Boolean;
begin
  DBAnt.Disconnect;
  DBAnt.DatabaseName := DBNome;
  DBAnt.Host := EdIPServer.Text;
  DBAnt.Port := EdPorta.ValueVariant;
  DBAnt.UserName := 'root';
  DBAnt.UserPassword := 'wkljwery' + 'hvbirt';
  try
    DBAnt.Connect;
  except
    on E: Exception do
    begin
      Geral.MB_Info(E.Message);
      if DBNome <> 'mysql' then
      begin
        if Geral.MB_Pergunta('Deseja criar a base de dados "' + DBNome + '"?') =
        ID_YES then
        begin
          Dmod.MyDB.Execute('CREATE DATABASE ' + DBNome);
        end;
      end;
    end;
  end;
  Result := DBAnt.Connected;
end;

procedure TFmDB_Converte_Tisolin.CriaEstrutura();
begin
  EstruturaBDs();
  AbreTabelaTabelas();
end;

procedure TFmDB_Converte_Tisolin.EdLoadCSVOthDirChange(Sender: TObject);
begin
  FArquivoGraGruY := Geral.Substitui(EdLoadCSVOthDir.Text + '\' + 'GraGruY.csv', '\\', '\');
  if not FileExists(FArquivoGraGruY) then
    Geral.MB_Aviso('Arquivo n�o encontrado: ' + FArquivoGraGruY);
  //
  FArquivoTributacaoST := Geral.Substitui(EdLoadCSVOthDir.Text + '\' + 'TributacaoST.csv', '\\', '\');
  if not FileExists(FArquivoTributacaoST) then
    Geral.MB_Aviso('Arquivo n�o encontrado: ' + FArquivoTributacaoST);
  //
end;

procedure TFmDB_Converte_Tisolin.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MeResult_.Lines.Clear;
end;

procedure TFmDB_Converte_Tisolin.FormCreate(Sender: TObject);
var
  Arquivo: String;
begin
  FTabLctA := 'lct0001a';
  FTabTabs := '_tab_tabs_';
  //
  ImgTipo.SQLType := stLok;
  CGDBF.Value := Trunc(//1 +  // n�o importar carteiras!
    Power(2,  2) + //       4
    Power(2,  4) + //      16
    Power(2,  5) + //      32
    Power(2,  6) + //      64
    Power(2,  9) + //     512
    Power(2, 12) + //    4096
    power(2, 21) + // 2097152
    power(2, 22) + // 4194304
    0);
  try
    UnDmkDAC_PF.ConectaMyDB_DAC(FbDB, 'zzz_fb_db_ant', VAR_IP, VAR_PORTA, VAR_SQLUSER,
      VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
    FbDB.Connected := True;
  except
    Geral.MB_Erro('N�o foi poss�vel conectar no BD "zzz_fb_db_ant"');
  end;
  //
  EdLoadCSVOthDirChange(Self);
end;

procedure TFmDB_Converte_Tisolin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAvisoR1, LaAvisoR2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmDB_Converte_Tisolin.GetNivel2(Grupo, Linha: Integer): Integer;
begin
  Result := (Integer(Grupo) * 1000) + Integer(Linha);
end;

procedure TFmDB_Converte_Tisolin.ReabrePsqEnt(Antigo: String);
begin
  QrPsqEnt.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqEnt, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM entidades ',
  'WHERE Antigo="' + Antigo + '"',
  'AND Codigo <> 0 ',
  'AND TRIM(Antigo) <> "" ',
  '']);
end;

procedure TFmDB_Converte_Tisolin.ReabreTabela(Table: TSimpleDataSet);
begin
  Avisa('Abrindo');
  Table.Close;
  Table.Open;
  Avisa('Aberta');
  PB1.Position := 0;
  PB1.Max := Table.RecordCount;
end;

procedure TFmDB_Converte_Tisolin.SbArqFB_BkpClick(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdArqFB_Bkp.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Arquivo', '',
  [], SelPath) then
    EdArqFB_Bkp.Text := SelPath;
end;

procedure TFmDB_Converte_Tisolin.SbGBakClick(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdPathGBak.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
    EdPathGBak.Text := ExtractFileDir(SelPath);
end;

procedure TFmDB_Converte_Tisolin.SbLoadCSVOthDirClick(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdLoadCSVOthDir.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
    EdLoadCSVOthDir.Text := ExtractFileDir(SelPath);
end;

procedure TFmDB_Converte_Tisolin.SpeedButton2Click(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdIB_DB.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Arquivo', '',
  [], SelPath) then
    EdIB_DB.Text := SelPath;
end;

function TFmDB_Converte_Tisolin.TabelaEstahSelecionada(Tabela: String): Boolean;
var
  I, p: Integer;
  sTabSel: String;
begin
  FTabela := Tabela;
  Result := False;
  //
  for I := 0 to CGDBF.Items.Count - 1 do
  begin
    if CGDBF.Checked[I] then
    begin
      p := Pos(' ', Trim(CGDBF.Items[I]));
      sTabSel := Uppercase(Trim(Copy(CGDBF.Items[I], 1, p)));
      if sTabSel = Uppercase(FTabela) then
      begin
        (*
        TbDBF.Close;
        TbDBF.TableName := FTabela;
        TbDBF.Open;*)
        AbreTabela(FTabela);
        FTabStr := 'Tabela: [' + FTabela + '] ';
        Avisa('');
        //
        Result := True;
        Exit;
      end;
    end;
  end;
end;

function TFmDB_Converte_Tisolin.TabelaExiste(QryAux: TMySQLQuery; DB,
  Tabela: String): Boolean;
begin
  UnMyMySQL.AbreMySQLQuery1(QryAux, [
  'SHOW TABLES FROM ' + DB + ' LIKE "' + Tabela + '" ',
  '']);
  //
  Result := QryAux.RecordCount > 0;
end;

function TFmDB_Converte_Tisolin.VerificaCadastro(Tabela, Codigo, CodUsu, Nome,
  Texto: String; NovoCodigo: TNovoCodigo): Integer;
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT ' + Codigo + ' FROM ' + Tabela,
  'WHERE ' + Nome + '="' + Texto + '"',
  '']);
  //
  if Dmod.QrAux.RecordCount > 0 then
    Result := Dmod.QrAux.FieldByName(Codigo).AsInteger
  else begin
    Idx := 0;
    case NovoCodigo of
      ncControle: Idx := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, Tabela, Codigo, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: Idx := UMyMod.BPGS1I32(
        Tabela, Codigo, '', '', tsDef, stIns, 0);
      else
      begin
        Geral.MensagemBox('Tipo de obten��o de novo c�digo indefinido! [2]',
        'ERRO', MB_OK+MB_ICONERROR);
        Halt(0);
        //Exit;
      end;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ' + Tabela + ' SET ');
    Dmod.QrUpd.SQL.Add(Codigo + '=' + Geral.FF0(Idx) + ',');
    if CodUsu <> '' then
      Dmod.QrUpd.SQL.Add(CodUsu + '=' + Geral.FF0(Idx) + ',');
    Dmod.QrUpd.SQL.Add(Nome + '="' + Texto + '"');
    Dmod.QrUpd.ExecSQL;
    //
    Result := Idx;
  end;
end;

//////////////////////// T A B E L A S  ////////////////////////////////////////

{
procedure TFmDB_Converte_Tisolin.ImportaTabela_CadBco();
var
  Nome, Nome2: String;
  Codigo, Tipo, ForneceI: Integer;
  SQLType: TSQLType;
begin
  ReabreTabela(TbCadBco);
  TbCadBco.First;
  while not TbCadBco.Eof do
  begin
    Codigo         := Trunc(TbCadBcoCODBCO.Value);
    Tipo           := 2;
    Nome           := TbCadBcoNOMBCO.Value;
    Nome2          := Nome;
    ForneceI       := -11;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Codigo ID',
    'FROM carteiras ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrExiste.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //  
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Nome);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'carteiras', False, [
    'Nome', 'Nome2', 'ForneceI'], [
    'Codigo', 'Tipo'], [
    Nome, Nome2, ForneceI], [
    Codigo, Tipo], True);
    //
    TbCadBco.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadCli();
const
  Mae = ''; NIRE = ''; FormaSociet = ''; Simples = ''; IEST = ''; Atividade = '';
  CPF_Pai = ''; CPF_Conjuge = ''; SSP = ''; DataRG = ''; CidadeNatal = '';
  EstCivil = ''; UFNatal = ''; Nacionalid = ''; EPais = ''; Ete3 = ''; ECel = '';
  PPais = ''; Pte3 = ''; PCel = '';
  //
  Cliente1 = 'V';
var
  ENDCLI: String;
  EAtividad, EUF, ECEP,
  PAtividad, PUF, PCEP,
  Codigo,    CUF, CCEP,
  Ordem, CodUsu, Tipo, Orgao: Integer;
  //
  CNPJ, IE, RazaoSocial, Fantasia, ELograd, ERua, ENumero, ECompl, EBairro, ECidade, ENatal, ETe1, ETe2, EFax, EContato, EEmail,
  CPF,  RG, Nome,        Apelido,  PLograd, PRua, PNumero, PCompl, PBairro, PCidade, PNatal, PTe1, PTe2, PFax, PContato, PEMail,
  Antigo, xBairro, xLogr, ECOCLI,  CLograd, CRua, CNumero, CCompl, CBairro, CCidade,
  //Numero, Compl, Bairro,
  Pai, Nivel, CreditosU, Observacoes, Comprador, Te1,
  Te2, Fax, Email, Respons1, Respons2, Txt: String;
  LimiCred: Double;
begin
(*
DELETE FROM entidades WHERE Codigo > 10000;
DELETE FROM enticontat;
DELETE FROM entimail;
DELETE FROM entirespon;
DELETE FROM entitel;
DELETE FROM entictas;
DELETE FROM entisrvpro;
*)
  ReabreTabela(TbCadCli);
  TbCadCli.First;
  while not TbCadCli.Eof do
  begin
    // N�o usa: TbCadCliFATCLI
    Antigo := 'C' + TbCadCliCODCLI.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Antigo);
    ReabrePsqEnt(Antigo);
    if QrPsqEnt.RecordCount = 0 then
    begin
      Codigo := Geral.IMV(TbCadCliCODCLI.Value) + EdCliCodIni.ValueVariant - 1;
      CodUsu := Codigo;
      ENDCLI := TbCadCliENDCLI.Value;
      CNPJ := Geral.SoNumero_TT(TbCadCliCGCCLI.Value);
      IE   := TbCadCliINSCLI.Value;
      if Geral.SoNumero_TT(IE) = '' then
        IE := '';
      CPF  := Geral.SoNumero_TT(TbCadCliCPFCLI.Value);
      RG   := TbCadCliRGECLI.Value;
      if Geral.SoNumero_TT(RG) = '' then
        RG := '';
      RazaoSocial := '';
      Fantasia := '';
      Nome := '';
      Apelido := '';
      EAtividad := 0; PAtividad := 0;
      ELograd := ''; ERua := ''; ENumero := ''; ECompl := '';
      PLograd := ''; PRua := ''; PNumero := ''; PCompl := '';
      EBairro := ''; PBairro := '';
      ECidade := ''; PCidade := '';
      EUF := 0; PUF := 0;
      ECEP := 0; PCEP := 0;
      ENatal := '0000-00-00';
      ETe1 := ''; ETe2 := ''; EFax := '';
      PTe1 := ''; PTe2 := ''; PFax := '';
      PNatal := '0000-00-00';
      PContato := ''; EContato := '';
      EEmail := ''; PEmail := '';
      //
      Comprador := Trim(TbCadCliCOMCLI.Value);
      Te1 := Geral.SoNumero_TT(TbCadCliTE1CLI.Value);
      Te2 := Geral.SoNumero_TT(TbCadCliTE2CLI.Value);
      Fax := Geral.SoNumero_TT(TbCadCliFAXCLI.Value);
      Email := Trim(Lowercase(TbCadCliEMACLI.Value));
      //
      PNumero := '';
      PCompl := '';
      xBairro := '';
      xLogr := '';
      PLograd := '';
      PRua := '';
      //
      if Length(CNPJ) >= 14 then
      begin
        Tipo := 0;
        Geral.SeparaEndereco(
          0, ENDCLI, ENumero, ECompl, xBairro, xLogr, ELograd, ERua,
          False, Memo1);
        //
        RazaoSocial := TbCadCliRAZCLI.Value;
        Fantasia := TbCadCliFANCLI.Value;
        EAtividad := Geral.IMV(TbCadCliRAMCLI.Value);
        EBairro := TbCadCliBAICLI.Value;
        ECidade := TbCadCliCIDCLI.Value;
        EUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESTCLI.Value);
        ECEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEPCLI.Value));
        ENatal := Geral.FDT(TbCadCliDTNCLI.Value, 1);
        //
        ETe1 := Te1;
        ETe2 := Te2;
        EFax := Fax;
        EContato := Comprador;
        EEmail := Email;
      end else
      begin
        Tipo := 1;
        Geral.SeparaEndereco(
        0, ENDCLI, PNumero, PCompl, xBairro, xLogr, PLograd, PRua,
        False, Memo1);
        //
        Nome := TbCadCliRAZCLI.Value;
        Apelido := TbCadCliFANCLI.Value;
        PAtividad := Geral.IMV(TbCadCliRAMCLI.Value);
        PBairro := TbCadCliBAICLI.Value;
        PCidade := TbCadCliCIDCLI.Value;
        PUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESTCLI.Value);
        PCEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEPCLI.Value));
        PNatal := Geral.FDT(TbCadCliDTNCLI.Value, 1);
        //
        PTe1 := Te1;
        PTe2 := Te2;
        PFax := Fax;
        PContato := Comprador;
        PEmail := Email;
      end;
      ECOCLI := TbCadCliECOCLI.Value;
      Geral.SeparaEndereco(
        0, ENDCLI, CNumero, CCompl, xBairro, xLogr, CLograd, CRua,
        False, Memo1);
      CBairro := TbCadCliBCOCLI.Value;
      CCidade := TbCadCliCIOCLI.Value;
      CUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESOCLI.Value);
      CCEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEOCLI.Value));
      //
      Pai := TbCadCliFILCLI.Value;
      Nivel := TbCadCliLIBCLI.Value;
      CreditosU :=  Geral.FDT(TbCadCliDTUCLI.Value, 1);
      LimiCred := TbCadCliLIMCLI.Value;
      try
        Observacoes := TbCadCliOBSCLI.Value;
      except
        Observacoes := '';
      end;
      //
      Respons1 := TbCadCliSO1CLI.Value;
      Respons2 := TbCadCliSO2CLI.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP',*) 'Cliente1',
      (*'Cliente2', 'Cliente3', 'Cliente4',
      'Fornece1', 'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', 'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      (*'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao',*) 'Nivel',
      (*'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD',*) 'CreditosU',
      (*'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4', 
      'QuantN1', 'QuantN2', 'QuantN3', 
      'Agenda', 'SenhaQuer', 'Senha1',*) 
      'LimiCred', (*'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo' (*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP,*) Cliente1,
      (*Cliente2, Cliente3, Cliente4,
      Fornece1, Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      (*CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao,*) Nivel,
      (*Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD,*) CreditosU,
      (*CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,*)
      LimiCred, (*Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo (*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        //
        if (Comprador <> '') or (Te1 <> '') or (Te2 <> '') or (Fax <> '') or
        (Email <> '') then
          IncluiContato(Codigo, FConComprador, Comprador, Te1, Te2, Fax, Email);
        //
        //
        // S � C I O S
        //
          // S�cio 1
        if (TbCadCliSO1CLI.Value <> '') (*Nome*)
        or (TbCadCliRG1CLI.Value <> '') (*RG*)
        or (TbCadCliCP1CLI.Value <> '') (*CPF*) then
        //or (TbCadCliDN1CLI.Value <> 0) (*Nasc*) then
          IncluiSocio(Codigo, FCargoSocio,
          TbCadCliSO1CLI.Value, TbCadCliRG1CLI.Value, TbCadCliCP1CLI.Value,
          TbCadCliDN1CLI.Value);
          // S�cio 2
        if (TbCadCliSO2CLI.Value <> '') (*Nome*)
        or (TbCadCliRG2CLI.Value <> '') (*RG*)
        or (TbCadCliCP2CLI.Value <> '') (*CPF*)  then
        //or (TbCadCliDN2CLI.Value <> 0) (*Nasc*) then
          IncluiSocio(Codigo, FCargoSocio,
          TbCadCliSO2CLI.Value, TbCadCliRG2CLI.Value, TbCadCliCP2CLI.Value,
          TbCadCliDN2CLI.Value);

        // P r i n c i p a i s   c l i e n t e s
        if (TbCadCliCL1CLI.Value <> '') (*Nome*)
        or (TbCadCliCF1CLI.Value <> '') (*tel*) then
          // Cliente 1
          IncluiContato(Codigo, FPrincipalCli, TbCadCliCL1CLI.Value,
            TbCadCliCF1CLI.Value, '', '', '');
        if (TbCadCliCL2CLI.Value <> '') (*Nome*)
        or (TbCadCliCF2CLI.Value <> '') (*tel*) then
          // Cliente 2
          IncluiContato(Codigo, FPrincipalCli, TbCadCliCL2CLI.Value,
            TbCadCliCF2CLI.Value, '', '', '');

        // P r i n c i p a i s   f o r n e c e d o r e s
        if (TbCadCliFO1CLI.Value <> '') (*Nome*)
        or (TbCadCliFF1CLI.Value <> '') (*tel*) then
          // Fornecedor 1
          IncluiContato(Codigo, FPrincipalFor, TbCadCliFO1CLI.Value,
            TbCadCliFF1CLI.Value, '', '', '');
        if (TbCadCliFO2CLI.Value <> '') (*Nome*)
        or (TbCadCliFF2CLI.Value <> '') (*tel*) then
          // Fornecedor 2
          IncluiContato(Codigo, FPrincipalFor, TbCadCliFO2CLI.Value,
            TbCadCliFF2CLI.Value, '', '', '');

        // R e f e r e n c i a s   b a n c � r i a s
        if (TbCadCliBC1CLI.Value <> '') (*Txt*)
        or (TbCadCliNB1CLI.Value <> '') (*Bco*)
        or (TbCadCliNA1CLI.Value <> '') (*Ag*) then
        begin
          Ordem := 1;
          IncluiBanco(Codigo, Ordem,
            TbCadCliBC1CLI.Value, TbCadCliNB1CLI.Value, TbCadCliNA1CLI.Value);
        end;
        if (TbCadCliBC2CLI.Value <> '') (*Txt*)
        or (TbCadCliNB2CLI.Value <> '') (*Bco*)
        or (TbCadCliNA2CLI.Value <> '') (*Ag*) then
        begin
          Ordem := 2;
          IncluiBanco(Codigo, Ordem,
            TbCadCliBC2CLI.Value, TbCadCliNB2CLI.Value, TbCadCliNA2CLI.Value);
        end;

        // EntiSrvPro > SPC
        if (TbCadCliSPCCLI.Value <> '') (*Txt*)
        or (TbCadCliNRCCLI.Value <> '') (*Num*)
        or (TbCadCliHORCLI.Value <> '') (*Hora*) then
        begin
          Txt := TbCadCliSPCCLI.Value + TbCadCliNRCCLI.Value;
          if Pos('SERASA', Txt) > 0 then
            Orgao := FETP_SERASA
          else
          if Pos('ACIM', Txt) > 0 then
            Orgao := FETP_SERASA
          else
            Orgao := FETP_SCPC;
          //
          IncluiConsulta(Codigo, Orgao, TbCadCliNRCCLI.Value, TbCadCliDSPCLI.Value,
            TbCadCliHORCLI.Value, TbCadCliSPCCLI.Value);
        end;
      end;
    end;
    TbCadCli.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadCtr();
  procedure IncluiLocFCab();
  var
    DtHrFat: String;
    Codigo, Controle, NumNF: Integer;
    ValLocad, ValConsu, ValUsado, ValFrete, ValDesco, ValTotal: Double;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Controle       := Trunc(TbCadCtrNRCTR.Value);
    DtHrFat        := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    ValLocad       := TbCadCtrVALUNI1.Value + TbCadCtrVALUNI2.Value;
    ValConsu       := 0;
    ValUsado       := 0;
    ValFrete       := TbCadCtrFRETE.Value;
    ValDesco       := 0;
    ValTotal       := TbCadCtrTOTPEDG.Value;
    NumNF          := Trunc(TbCadCtrNRNF.Value);
    //
    //UMyMod.BPGS1I32('locfcab', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stins, 'locfcab', False, [
    'Codigo', 'DtHrFat', 'ValLocad',
    'ValConsu', 'ValUsado', 'ValFrete',
    'ValDesco', 'ValTotal', 'NumNF'], [
    'Controle'], [
    Codigo, DtHrFat, ValLocad,
    ValConsu, ValUsado, ValFrete,
    ValDesco, ValTotal, NumNF], [
    Controle], True);
  end;

  procedure IncluiLctFatRef(Valor: Double);
  var
    Codigo, Controle, Conta: Integer;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Controle       := Trunc(TbCadCtrNRCTR.Value);
    //Conta          := ;
    //Valor          := ;
    //
    Conta := UMyMod.BPGS1I32('lctfatref', 'Conta', '', '', tsPos, stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctfatref', False, [
    'Codigo', 'Controle', 'Valor'], [
    'Conta'], [
    Codigo, Controle, Valor], [
    Conta], True);
  end;

  //

  procedure IncluiLocCCon();
  const
    Empresa = -11;
  var
    DtHrEmi, DtHrBxa, LocalObra, Obs0, Obs1, Obs2: String;
    Codigo, Contrato, Cliente, Vendedor: Integer;
    ValorTot: Double;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Contrato       := -999;
    Cliente        := DModG.ObtemCodigoDeEntidadePeloAntigo('C' + FormatFloat('00000', TbCadCtrCODCLIP.Value));
    DtHrEmi        := Geral.FDT(TbCadCtrDTEMI.Value, 1);
    DtHrBxa        := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    Vendedor       := TbCadCtrVEND.Value;
    ValorTot       := TbCadCtrTOTPEDG.Value;
    LocalObra      := TbCadCtrENDOBRA.Value;
    Obs0           := TbCadCtrOBS0.Value;
    Obs1           := TbCadCtrOBS1.Value;
    Obs2           := TbCadCtrOBS2.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'locccon', False, [
    'Empresa',
    'Contrato', 'Cliente', 'DtHrEmi',
    'DtHrBxa', 'Vendedor', 'ValorTot',
    'LocalObra', 'Obs0', 'Obs1',
    'Obs2'], [
    'Codigo'], [
    Empresa,
    Contrato, Cliente, DtHrEmi,
    DtHrBxa, Vendedor, ValorTot,
    LocalObra, Obs0, Obs1,
    Obs2], [
    Codigo], True);
  end;

  //

  procedure IncluiLocCPatPri(const GraGruX: Integer; var CtrID: Integer);
    function LiberadoPor(Data: TDateTime): Integer;
    begin
      if Data > 2 then
        Result := -1
      else
        Result := 0;
    end;
  var
    DtHrLocado, DtHrRetorn, LibDtHr, RELIB, HOLIB: String;
    Codigo, LibFunci: Integer;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    //CtrID          := ;
    //GraGruX        := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT1.Value);
    DtHrLocado     := Geral.FDT(TbCadCtrDTEMI.Value, 1);
    DtHrRetorn     := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    LibFunci       := LiberadoPor(TbCadCtrDTBAIXA.Value);
    LibDtHr        := Geral.FDT(TbCadCtrDTLIB.Value, 1);
    RELIB          := TbCadCtrRELIB.Value;
    HOLIB          := TbCadCtrHOLIB.Value;
    //
    CtrID := UMyMod.BPGS1I32('loccpatpri', 'CtrID', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatpri', False, [
    'Codigo', 'GraGruX', 'DtHrLocado',
    'DtHrRetorn', 'LibFunci', 'LibDtHr',
    'RELIB', 'HOLIB'], [
    'CtrID'], [
    Codigo, GraGruX, DtHrLocado,
    DtHrRetorn, LibFunci, LibDtHr,
    RELIB, HOLIB], [
    CtrID], True);
  end;

  //

var
  GraGruX, CtrID: Integer;
  CodX: String;
begin
  ReabreTabela(TbCadCtr);
  TbCadCtr.First;
  while not TbCadCtr.Eof do
  begin
    try
      CodX := Geral.FF0(Trunc(TbCadCtrNRCTR.Value));
      MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + CodX);
      //
      if TbCadCtrNRCTR.Value <> 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
        'SELECT Codigo ID',
        'FROM locccon ',
        'WHERE Codigo=' + CodX,
        '']);
        if QrExiste.RecordCount = 0 then
        begin
          IncluiLocCCon();
          //

          GraGruX := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT1.Value);
          if GraGruX <> 0 then
          begin
            IncluiLocCPatPri(GraGrux, CtrID);
            if (TbCadCtrDTBAIXA.Value > 1) and (TbCadCtrVALUNI1.Value >= 0.01) then
              IncluiLctFatRef(TbCadCtrVALUNI1.Value);
          end;
          //
          GraGruX := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT2.Value);
          if GraGruX <> 0 then
          begin
            IncluiLocCPatPri(GraGrux, CtrID);
            if (TbCadCtrDTBAIXA.Value > 1) and (TbCadCtrVALUNI2.Value >= 0.01) then
              IncluiLctFatRef(TbCadCtrVALUNI2.Value);
          end;
          //
          if (TbCadCtrDTBAIXA.Value > 1) then
            IncluiLocFCab();
        end;
      end;
    except
      Memo2.Lines.Add('Erro no c�digo: ' + Geral.FFT(TbCadCtrNRCTR.Value, 0, siPositivo));
    end;
    TbCadCtr.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadFor();
const
  Mae = ''; NIRE = ''; FormaSociet = ''; Simples = ''; IEST = ''; Atividade = '';
  CPF_Pai = ''; CPF_Conjuge = ''; SSP = ''; DataRG = ''; CidadeNatal = '';
  EstCivil = ''; UFNatal = ''; Nacionalid = ''; EPais = ''; Ete3 = ''; ECel = '';
  PPais = ''; Pte3 = ''; PCel = '';
  Respons1 = ''; Respons2 = ''; Pai = '';
  //
  Fornece1 = 'V';
var
  ENDFOR: String;
  EAtividad, EUF, ECEP,
  PAtividad, PUF, PCEP,
  Codigo,    //CUF, CCEP,
  //Ordem, Orgao,
  CodUsu, Tipo: Integer;
  //
  CNPJ, IE, RazaoSocial, Fantasia, ELograd, ERua, ENumero, ECompl, EBairro, ECidade, ENatal, ETe1, ETe2, EFax, EContato, EEmail,
  CPF,  RG, Nome,        Apelido,  PLograd, PRua, PNumero, PCompl, PBairro, PCidade, PNatal, PTe1, PTe2, PFax, PContato, PEMail,
  Antigo, xBairro, xLogr,
  //Numero, Compl, Bairro, Txt,
  Observacoes, Te1,
  Te2, Fax: String;
  //
  //LimiCred: Double;
  //ECOFOR, CLograd, CRua, CNumero, CCompl, CBairro, CCidade, Comprador, Email,
  //Respons1, Respons2, Pai, Nivel, CreditosU: String;
begin
(*
DELETE FROM entidades WHERE Codigo > 20000;
DELETE FROM enticontat;
DELETE FROM entimail;
DELETE FROM entirespon;
DELETE FROM entitel;
DELETE FROM entictas;
DELETE FROM entisrvpro;
*)
  ReabreTabela(TbCadFor);
  TbCadFor.First;
  while not TbCadFor.Eof do
  begin
    // N�o usa: TbCadForFATFOR
    Antigo := 'F' + TbCadForCODFOR.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Antigo);
    ReabrePsqEnt(Antigo);
    if QrPsqEnt.RecordCount = 0 then
    begin
      Codigo := Geral.IMV(TbCadForCODFOR.Value) + EdForCodIni.ValueVariant - 1;
      CodUsu := Codigo;
      ENDFOR := TbCadForENDFOR.Value;
      CNPJ := Geral.SoNumero_TT(TbCadForCGCFOR.Value);
      IE   := TbCadForINSFOR.Value;
      if Geral.SoNumero_TT(IE) = '' then
        IE := '';
      CPF  := Geral.SoNumero_TT(TbCadForCPFFOR.Value);
      RG   := TbCadForRGEFOR.Value;
      if Geral.SoNumero_TT(RG) = '' then
        RG := '';
      RazaoSocial := '';
      Fantasia := '';
      Nome := '';
      Apelido := '';
      EAtividad := 0; PAtividad := 0;
      ELograd := ''; ERua := ''; ENumero := ''; ECompl := '';
      PLograd := ''; PRua := ''; PNumero := ''; PCompl := '';
      EBairro := ''; PBairro := '';
      ECidade := ''; PCidade := '';
      EUF := 0; PUF := 0;
      ECEP := 0; PCEP := 0;
      ENatal := '0000-00-00';
      ETe1 := ''; ETe2 := ''; EFax := '';
      PTe1 := ''; PTe2 := ''; PFax := '';
      PNatal := '0000-00-00';
      PContato := ''; EContato := '';
      EEmail := ''; PEmail := '';
      //
      //Comprador := Trim(TbCadForCOMFOR.Value);
      Te1 := Geral.SoNumero_TT(TbCadForTE1FOR.Value);
      Te2 := Geral.SoNumero_TT(TbCadForTE2FOR.Value);
      Fax := Geral.SoNumero_TT(TbCadForFAXFOR.Value);
      //Email := Trim(Lowercase(TbCadForEMAFOR.Value));
      //
      PNumero := '';
      PCompl := '';
      xBairro := '';
      xLogr := '';
      PLograd := '';
      PRua := '';
      //
      if Length(CNPJ) >= 14 then
      begin
        Tipo := 0;
        Geral.SeparaEndereco(
          0, ENDFOR, ENumero, ECompl, xBairro, xLogr, ELograd, ERua,
          False, Memo1);
        //
        RazaoSocial := TbCadForRAZFOR.Value;
        Fantasia := TbCadForFANFOR.Value;
        EAtividad := Geral.IMV(TbCadForRAMFOR.Value);
        EBairro := TbCadForBAIFOR.Value;
        ECidade := TbCadForCIDFOR.Value;
        EUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESTFOR.Value);
        ECEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEPFOR.Value));
        //ENatal := Geral.FDT(TbCadForDTNFOR.Value, 1);
        //
        ETe1 := Te1;
        ETe2 := Te2;
        EFax := Fax;
        //EContato := Comprador;
        //EEmail := Email;
      end else
      begin
        Tipo := 1;
        Geral.SeparaEndereco(
        0, ENDFOR, PNumero, PCompl, xBairro, xLogr, PLograd, PRua,
        False, Memo1);
        //
        Nome := TbCadForRAZFOR.Value;
        Apelido := TbCadForFANFOR.Value;
        PAtividad := Geral.IMV(TbCadForRAMFOR.Value);
        PBairro := TbCadForBAIFOR.Value;
        PCidade := TbCadForCIDFOR.Value;
        PUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESTFOR.Value);
        PCEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEPFOR.Value));
        //PNatal := Geral.FDT(TbCadForDTNFOR.Value, 1);
        //
        PTe1 := Te1;
        PTe2 := Te2;
        PFax := Fax;
        //PContato := Comprador;
        //PEmail := Email;
      end;
      (*
      ECOFOR := TbCadForECOFOR.Value;
      Geral.SeparaEndereco(
        0, ENDFOR, CNumero, CCompl, xBairro, xLogr, CLograd, CRua,
        False, Memo1);
      CBairro := TbCadForBCOFOR.Value;
      CCidade := TbCadForCIOFOR.Value;
      CUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESOFOR.Value);
      CCEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEOFOR.Value));
      //
      Pai := TbCadForFILFOR.Value;
      Nivel := TbCadForLIBFOR.Value;
      CreditosU :=  Geral.FDT(TbCadForDTUFOR.Value, 1);
      LimiCred := TbCadForLIMFOR.Value;
      //
      Respons1 := TbCadForSO1FOR.Value;
      Respons2 := TbCadForSO2FOR.Value;
      *)
      try
        Observacoes := TbCadForOBSFOR.Value;
      except
        Observacoes := '';
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
      'Cliente2', 'Cliente3', 'Cliente4',*)
      'Fornece1', (*'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', (*'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao', 'Nivel',
      'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD', 'CreditosU',
      'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4',
      'QuantN1', 'QuantN2', 'QuantN3',
      'Agenda', 'SenhaQuer', 'Senha1',
      'LimiCred', 'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo' (*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP, Cliente1,
      Cliente2, Cliente3, Cliente4,*)
      Fornece1, (*Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, (*CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao, Nivel,
      Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD, CreditosU,
      CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,
      LimiCred, Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo (*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
2470        // N�o h� outras inclus�es!
      end;
    end;
    TbCadFor.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadGru;
var
  //Teste,
  CodsX, Cod, CodUsu, Nivel2, Nivel3, Nivel4, Nivel5, Nivel6: String;
  //I,
  Itens: Integer;
  ArrNiveis: array of String;
  Nome: String;
begin
  ReabreTabela(TbCadGru);
  TbCadGru.First;
  while not TbCadGru.Eof do
  begin
    Itens := 0;
    CodsX := TbCadGruCODGRU.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + CodsX);
    while CodsX <> '' do
    begin
      Itens := Itens + 1;
      SetLength(ArrNiveis, Itens);
      if Geral.SeparaPrimeiraOcorrenciaDeTexto('.', CodsX, Cod, CodsX) then
        ArrNiveis[Itens-1] := Cod
      else
      begin
        Geral.MensagemBox('Erro fatal na importa��o de grupos de grade!' +
        #13#10 + 'O aplicativo ser� finalizado!', 'ERRO', MB_OK+MB_ICONERROR);
        //
        Halt(0);
      end;
    end;
    Nivel2 := '0';
    Nivel3 := '0';
    Nivel4 := '0';
    Nivel5 := '0';
    //
    Nome := TbCadGruDESGRU.Value;
    case Itens of
      1:
      begin
        Nivel6 := '0';
        Nivel5 := ArrNiveis[0];
        CodUsu := Nivel5;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru5', False, [
        'CodUsu', 'Nome', 'Nivel6'], ['Nivel5'], ['Ativo'], [
        CodUsu, Nome, Nivel6], [Nivel5], [1], True);
      end;
      2:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        CodUsu         := Nivel4;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru4', False, [
        'CodUsu', 'Nome', 'Nivel5'], ['Nivel4'], ['Ativo'], [
        CodUsu, Nome, Nivel5], [Nivel4], [1], True);
      end;
      3:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        Nivel3         := ArrNiveis[2];
        CodUsu         := Nivel3;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru3', False, [
        'CodUsu', 'Nome', 'Nivel5', 'Nivel4'], ['Nivel3'], ['Ativo'], [
        CodUsu, Nome, Nivel5, Nivel4], [Nivel3], [1], True);
      end;
      4:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        Nivel3         := ArrNiveis[2];
        Nivel2         := ArrNiveis[3];
        CodUsu         := Nivel2;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru2', False, [
        'CodUsu', 'Nome', 'Nivel5', 'Nivel4', 'Nivel3'], [
        'Nivel2'], ['Ativo'], [
        CodUsu, Nome, Nivel5, Nivel4, Nivel3], [
        Nivel2], [1], True);
      end;
      else Geral.MensagemBox('Item de grupo de patrim�nio n�o importado: ' +
      #13#10 + TbCadGruCODGRU.Value + #13#10 + Nome,
      'Aviso', MB_OK+MB_ICONERROR);
    end;
    //
    TbCadGru.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadNat();
var
  Nome, Nome2, Debito: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  ReabreTabela(TbCadNat);
  TbCadNat.First;
  while not TbCadNat.Eof do
  begin
    Codigo         := TbCadNatCODNAT.Value;
    Nome           := TbCadNatDESNAT.Value;
    Nome2          := Nome;
    Debito         := 'V';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Codigo ID',
    'FROM contas ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrExiste.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Nome);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'contas', False, [
    'Nome', 'Nome2', 'Debito'], [
    'Codigo'], [
    Nome, Nome2, Debito], [
    Codigo], True);
    //
    TbCadNat.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadPat();
  function MarcaCadastrada(Texto: String): Integer;
  const
    Codigo = 0;
  var
    Nome: String;
    Controle: Integer;
  begin
    if Trim(Texto) <> '' then
    begin
      Nome := Texto;
      if Nome = 'DINAPAC' then
        Nome := 'DYNAPAC'
      else
      if Nome = 'ERBELE' then
        Nome := 'EBERLE'
      else
      if Nome = 'JOWA/WACKE' then
        Nome := 'JOWA'
      else
      if Nome = 'KOLBACK' then
        Nome := 'KOLBACH'
      else
      if Nome = 'ROBIN' then
        Nome := 'ROBBIN'
      else
      if Nome = 'VIBROMAK' then
        Nome := 'VIBROMACK'
      else
      if Nome = 'WEGG' then
        Nome := 'WEG'
      ;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM grafabmar ',
      'WHERE Nome="' + Nome + '"',
      '']);
      //
      Result := QrGraFabMarControle.Value;
      if Result = 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY_Def('grafabmar', 'Controle', stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'grafabmar', False, [
        'Codigo', 'Nome'], [
        'Controle'], [
        Codigo, Nome], [
        Controle], True) then
          Result := Controle;
      end;
    end else Result := 0;
  end;
var
  Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia,
  Capacid, VendaData, VendaDocu, Observa, AGRPAT, CLVPAT: String;
  GraGruX, Nivel1, Situacao, Agrupador, VendaEnti: Integer;
  AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr: Double;
  Marca: Integer;
  //
  //GraGru1
  Cod, CodsX, Referencia, MARPAT: String;
  ArrNiveis: array of String;
  Nivel2, Nivel3, Nivel4, Nivel5,
  I, Itens, PrdGrupTip, CodUsu: Integer;
begin
  ReabreTabela(TbCadPat);
  SetLength(ArrNiveis, 4);
  TbCadPat.First;
  while not TbCadPat.Eof do
  begin
    Referencia := TbCadPatCODPAT.Value;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Referencia);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Nivel1 ID',
    'FROM gragru1 ',
    'WHERE Referencia=''' + Referencia + '''',
    '']);
    // Caso n�o tenha sido inserido ainda!
    if QrExiste.RecordCount = 0 then
    begin
      for I := 0 to 3 do
        ArrNiveis[I] := '';
      Itens := 0;
      CodsX := TbCadPatGRUPAT.Value;
      while CodsX <> '' do
      begin
        Itens := Itens + 1;
        if Geral.SeparaPrimeiraOcorrenciaDeTexto('.', CodsX, Cod, CodsX) then
          ArrNiveis[Itens-1] := Cod
        else
        begin
          Geral.MensagemBox('Erro fatal na importa��o de grupos de grade!' +
          #13#10 + 'O aplicativo ser� finalizado!', 'ERRO', MB_OK+MB_ICONERROR);
          //
          Halt(0);
        end;
      end;
      Nivel5 := Geral.IMV(ArrNiveis[0]);
      Nivel4 := Geral.IMV(ArrNiveis[1]);
      Nivel3 := Geral.IMV(ArrNiveis[2]);
      Nivel2 := Geral.IMV(ArrNiveis[3]);
      //
      Nivel1 := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', stIns, 0);
      CodUsu := Nivel1;
      PrdGrupTip := 1;
      GraGruX := Nivel1;
      //
      CadastraGraGru1SemGrade(PrdGrupTip, Nivel5, Nivel4, Nivel3,
      Nivel2, Nivel1, CodUsu, TbCadPatNOMPAT.Value, TbCadPatCODPAT.Value);
      //

      MARPAT         := TbCadPatMARPAT.Value;

      Complem        := TbCadPatCOMPAT.Value;
      AquisData      := Geral.FDT(TbCadPatDATPAT.Value, 1);
      AquisDocu      := TbCadPatDOCPAT.Value;
      AquisValr      := TbCadPatVLRPAT.Value;
      Situacao       := Dmod.CodigoDeSitPat(TbCadPatSITPAT.Value);
      AtualValr      := TbCadPatVLEPAT.Value;
      ValorMes       := TbCadPatVLMPAT.Value;
      ValorQui       := TbCadPatVLQPAT.Value;
      ValorSem       := TbCadPatVLSPAT.Value;
      ValorDia       := TbCadPatVLDPAT.Value;
      Agrupador      := 0; // Fazer depois!
      Marca          := MarcaCadastrada(MARPAT);
      Modelo         := TbCadPatMODPAT.Value;
      Serie          := TbCadPatSERPAT.Value;
      Voltagem       := TbCadPatENEPAT.Value;
      Potencia       := TbCadPatPOTPAT.Value;
      Capacid        := ''; // N�o tem
      VendaData      := Geral.FDT(TbCadPatDTVPAT.Value, 1);
      VendaDocu      := TbCadPatNFVPAT.Value;
      VendaValr      := TbCadPatVLVPAT.Value;
      VendaEnti      := DModG.ObtemCodigoDeEntidadePeloAntigo('C' + TbCadPatCLVPAT.Value);  // Cliente!
      Observa        := TbCadPatMOMPAT.Value;
      AGRPAT         := TbCadPatAGRPAT.Value;
      CLVPAT         := TbCadPatCLVPAT.Value;

      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragxpatr', False, [
      'GraGruX', 'Complem', 'AquisData',
      'AquisDocu', 'AquisValr', 'Situacao',
      'AtualValr', 'ValorMes', 'ValorQui',
      'ValorSem', 'ValorDia', 'Agrupador',
      'Marca', 'Modelo', 'Serie',
      'Voltagem', 'Potencia', 'Capacid',
      'VendaData', 'VendaDocu', 'VendaValr',
      'VendaEnti', 'Observa',
      'AGRPAT', 'CLVPAT', 'MARPAT'], [
      ], [
      GraGruX, Complem, AquisData,
      AquisDocu, AquisValr, Situacao,
      AtualValr, ValorMes, ValorQui,
      ValorSem, ValorDia, Agrupador,
      Marca, Modelo, Serie,
      Voltagem, Potencia, Capacid,
      VendaData, VendaDocu, VendaValr,
      VendaEnti, Observa,
      AGRPAT, CLVPAT, MARPAT], [
      ], True);
    end;
    //
    TbCadPat.Next;
  end;
  TbCadPat.First;
  PB1.Position := 0;
  QrGraGXPatr.Close;
  UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
  while not QrGraGXPatr.Eof do
  begin
    GraGruX := QrGraGXPatrGraGRuX.Value;
    Referencia := QrGraGXPatrAGRPAT.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Geral.FF0(GraGruX));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Nivel1 ID',
    'FROM gragru1 ',
    'WHERE Referencia=''' + Referencia + '''',
    '']);
    // N�o testei Ainda!
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(QrExiste.FieldByName('ID').AsInteger),
    '']);
    Agrupador := QrExiste.FieldByName('ID').AsInteger;
    //
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragxpatr', False, [
    'Agrupador'], ['GraGruX'], [Agrupador], [GraGruX], True);
    //
    QrGraGXPatr.Next;
  end;

end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_CadVde();
var
  //Senha, DataSenha, SenhaDia, IP_Default,
  Login: String;
  //Perfil, Funcionario, SitSenha,
  Numero: Integer;
begin
  ReabreTabela(TbCadVde);
  TbCadVde.First;
  while not TbCadVde.Eof do
  begin
    Login := TbCadVdeNOMVDE.Value;
    Numero := Trunc(TbCadVdeCODVDE.Value);
    //
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Login);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'senhas', False, [
    'Numero'], ['Login'], [Numero], [Login], True);
    //
    TbCadVde.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_PagDup();
var
  Fornecedor: Integer;
begin
  ReabreTabela(TbPagDup);
  TbPagDup.First;
  while not TbPagDup.Eof do
  begin
    Fornecedor := DModG.ObtemCodigoDeEntidadePeloAntigo('F' +
      FormatFloat('00000', Geral.IMV(TbPagDupFORPAG.Value)));
    //
    //
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbPagDupDTEPAG.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbPagDupDTVPAG.Value, 1);
    FLAN_DataCad       := Geral.FDT(date, 1);
    FLAN_Mez           := 0;
    FLAN_Descricao     := TbPagDupOBSPAG.Value;
    FLAN_Compensado    := Geral.FDT(TbPagDupDTPPAG.Value, 1);
    FLAN_Duplicata     := TbPagDupTITPAG.Value;
    FLAN_Doc2          := TbPagDupNOCPAG.Value;
    FLAN_SerieCH       := Geral.SoLetra_TT(TbPagDupNCHPAG.Value);

    FLAN_Documento     := Geral.IMV('0' + Geral.SoNumero_TT(TbPagDupNCHPAG.Value));
    FLAN_Tipo          := 2;
    FLAN_Credito       := 0;
    FLAN_Debito        := TbPagDupVLRPAG.Value;
    FLAN_Genero        := TbPagDupNATPAG.Value;
    FLAN_SerieNF       := Geral.SoLetra_TT(TbPagDupNNFPAG.Value);
    FLAN_Antigo        := TbPagDupCODPAG.Value;
    FLAN_NotaFiscal    := Geral.IMV('0' + Geral.SoNumero_TT(TbPagDupNNFPAG.Value));
    if TbPagDupDTPPAG.Value > 2 then
    begin
      FLAN_Sit           := 3;
      FLAN_Carteira      := 2; // Banco
    end else
    begin
      FLAN_Sit           := 0;
      FLAN_Carteira      := 3; // Duplicatas
    end;
    FLAN_Controle      := 0;
    FLAN_Sub           := 0;
    FLAN_ID_Pgto       := 0;
    FLAN_Cartao        := 0;
    FLAN_Linha         := 0;
    FLAN_Fornecedor    := Fornecedor;
    FLAN_Cliente       := 0;
    FLAN_MoraDia       := 0;
    FLAN_Multa         := 0;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(Date, 1);
    FLAN_Vendedor      := 0;
    FLAN_Account       := 0;
    FLAN_ICMS_P        := 0;
    FLAN_ICMS_V        := 0;
    FLAN_CliInt        := -11;
    FLAN_Depto         := 0;
    FLAN_DescoPor      := 0;
    FLAN_ForneceI      := 0;
    FLAN_DescoVal      := 0;
    FLAN_NFVal         := 0;
    FLAN_Unidade       := 0;
    FLAN_Qtde          := 0;
    FLAN_FatID         := 0;
    FLAN_FatID_Sub     := 0;
    FLAN_FatNum        := 0;
    FLAN_FatParcela    := 0;
    FLAN_FatParcRef    := 0;
    FLAN_FatGrupo      := 0;
    //
    FLAN_MultaVal      := 0;
    FLAN_TaxasVal      := 0;
    FLAN_MoraVal       := TbPagDupJURPAG.Value;
    FLAN_CtrlIni       := 0;
    FLAN_Nivel         := 0;
    FLAN_CNAB_Sit      := 0;
    FLAN_TipoCH        := 0;
    FLAN_Atrelado      := 0;
    FLAN_SubPgto1      := 0;
    FLAN_MultiPgto     := 0;
    FLAN_EventosCad    := 0;
    FLAN_IndiPag       := 0;
    FLAN_FisicoSrc     := 0;
    FLAN_FisicoCod     := 0;
    //
    FLAN_Emitente      := '';
    FLAN_CNPJCPF       := '';
    FLAN_Banco         := 0;
    FLAN_Agencia       := 0;
    FLAN_ContaCorrente := '';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Geral.FF0(TbPagDup.RecNo));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM ' + FTabLctA,
    'WHERE Data=''' + FLAN_Data + '''',
    'AND Vencimento=''' + FLAN_Vencimento + '''',
    'AND Duplicata=''' + Geral.Substitui(dmkPF.DuplicaBarras(FLAN_Duplicata), '''', ' ') + '''',
    'AND Fornecedor=' + Geral.FF0(FLAN_Fornecedor),
    'AND Debito=' + Geral.FFT_Dot(FLAN_Debito, 2, siNegativo),
    '']);
    if QrExiste.RecordCount = 0 then
    begin
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
    //
    TbPagDup.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabela_RecDup();
var
  Cliente: Integer;
begin
  ReabreTabela(TbRecDup);
  TbRecDup.First;
  while not TbRecDup.Eof do
  begin
    Cliente := DModG.ObtemCodigoDeEntidadePeloAntigo('C' +
      FormatFloat('00000', Geral.IMV(TbRecDupCLIREC.Value)));
    //
    //
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbRecDupDTEREC.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbRecDupDTVREC.Value, 1);
    FLAN_DataCad       := Geral.FDT(date, 1);
    FLAN_Mez           := 0;
    FLAN_Descricao     := TbRecDupOBSREC.Value;
    FLAN_Compensado    := Geral.FDT(TbRecDupDTPREC.Value, 1);
    FLAN_Duplicata     := TbRecDupTITREC.Value;
    FLAN_Doc2          := TbRecDupNRPREC.Value;
    FLAN_SerieCH       := '';

    FLAN_Documento     := 0;//Geral.IMV('0' + Geral.SoNumero_TT(TbRecDupNCHREC.Value));
    FLAN_Tipo          := 2;
    //FLAN_Carteira      := TbRecDupBCOREC.Value;
    FLAN_Credito       := TbRecDupVLRREC.Value;
    FLAN_Debito        := 0;
    FLAN_Genero        := 100;
    FLAN_SerieNF       := Geral.SoLetra_TT(TbRecDupNNFREC.Value);
    FLAN_Antigo        := TbRecDupCODREC.Value;
    FLAN_NotaFiscal    := Geral.IMV('0' + Geral.SoNumero_TT(TbRecDupNNFREC.Value));
    if TbRecDupDTPREC.Value > 2 then
    begin
      FLAN_Sit           := 3;
      FLAN_Carteira      := 2; // Banco
    end else
    begin
      FLAN_Sit           := 0;
      FLAN_Carteira      := 4; // Faturas
    end;
    FLAN_Controle      := 0;
    FLAN_Sub           := 0;
    FLAN_ID_Pgto       := 0;
    FLAN_Cartao        := 0;
    FLAN_Linha         := 0;
    FLAN_Fornecedor    := 0;
    FLAN_Cliente       := Cliente;
    FLAN_MoraDia       := 0;
    FLAN_Multa         := 0;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(Date, 1);
    FLAN_Vendedor      := 0;
    FLAN_Account       := 0;
    FLAN_ICMS_P        := 0;
    FLAN_ICMS_V        := 0;
    FLAN_CliInt        := -11;
    FLAN_Depto         := 0;
    FLAN_DescoPor      := 0;
    FLAN_ForneceI      := 0;
    FLAN_DescoVal      := 0;
    FLAN_NFVal         := 0;
    FLAN_Unidade       := 0;
    FLAN_Qtde          := 0;
    FLAN_FatID         := 0;
    FLAN_FatID_Sub     := 0;
    FLAN_FatNum        := 0;
    FLAN_FatParcela    := 0;
    FLAN_FatParcRef    := 0;
    FLAN_FatGrupo      := 0;
    //
    FLAN_MultaVal      := 0;
    FLAN_TaxasVal      := 0;
    FLAN_MoraVal       := TbRecDupJURREC.Value;
    FLAN_CtrlIni       := 0;
    FLAN_Nivel         := 0;
    FLAN_CNAB_Sit      := 0;
    FLAN_TipoCH        := 0;
    FLAN_Atrelado      := 0;
    FLAN_SubPgto1      := 0;
    FLAN_MultiPgto     := 0;
    FLAN_EventosCad    := 0;
    FLAN_IndiPag       := 0;
    FLAN_FisicoSrc     := 0;
    FLAN_FisicoCod     := 0;
    //
    FLAN_Emitente      := '';
    FLAN_CNPJCPF       := '';
    FLAN_Banco         := 0;
    FLAN_Agencia       := 0;
    FLAN_ContaCorrente := '';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True, FTabStr + Geral.FF0(TbRecDup.RecNo));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM ' + FTabLctA,
    'WHERE Data=''' + FLAN_Data + '''',
    'AND Vencimento=''' + FLAN_Vencimento + '''',
    'AND Duplicata=''' + Geral.Substitui(dmkPF.DuplicaBarras(FLAN_Duplicata), '''', ' ') + '''',
    'AND Cliente=' + Geral.FF0(FLAN_Cliente),
    'AND Credito=' + Geral.FFT_Dot(FLAN_Credito, 2, siNegativo),
    '']);
    if QrExiste.RecordCount = 0 then
    begin
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
    //
    TbRecDup.Next;
  end;
end;
}
procedure TFmDB_Converte_Tisolin.EspelhaDados();
var
  //Txt,
  SQLa, SQLb, SQLc, Tb, Campo, Extra, Temp: String;
  //K,
  I, N, T, Tam, J: Integer;
  Valor: WideString;
  //
  Campos: array of String;
  Mostra: Boolean;
  //
  Comeca, Continua: Boolean;
  //
  SQL: String;
  Skip, ItsOK, Step, Total: Integer;
  //
  CamposTabDst, SQL_FIELDS, SQL_Rec, SQL_VALUES: String;
begin
  if DBGTabelas.SelectedRows.Count < 1 then
  begin
    Geral.MB_Aviso('Nenhuma tabela foi selecionada!');
    Exit;
  end;
  MySQLBatchExecute1.Database := DBAnt;
  LaAvisoR1.Font.Color := clSilver;
  LaAvisoR2.Font.Color := clRed;
  //
(*
  UnMyMySQL.AbreMySQLQuery1(QrTabelas, [
  'SELECT DISTINCT Tabela ',
  'FROM ' + FTabTabs,
  'ORDER BY Tabela ',
  '']);
*)
  PB1.Max := QrTabelas.RecordCount;
  PB1.Position := 0;
  QrTabelas.First;
  //QtdCampos := 0;

  //  while not QrTabelas.Eof do
  with DBGTabelas.DataSource.DataSet do
  for T := 0 to DBGTabelas.SelectedRows.Count-1 do
  begin
    GotoBookmark(DBGTabelas.SelectedRows.Items[T]);
    SQLc := '';
   //
    Tb := Trim(QrTabelasTabela.Value);
    //

////////////////////////////////////////////////////////////////////////////////
    //EXIT;
////////////////////////////////////////////////////////////////////////////////


    PB1.Position := PB1.Position + 1;
    MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, '...');
    MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
    //
    Tam := 0;
    SetLength(Campos, Tam);
    //if (LowerCase(Tb) = 'modelos') or (LowerCase(Tb) = 'orcamentosib') then
    //if (LowerCase(Tb) = 'pragas') then
    if pos('$', Tb) = 0 then
      Comeca := True
    else
      Comeca := False;
    if Comeca then
    begin
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Tabela "' + Tb + '". ');
      //


        SDDados.Close;
        SDDados.DataSet.CommandText := 'SELECT FIRST 1 * FROM ' + Tb;
        SDDados.Open;

          MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando tabela no MySQL.');
          EstruturaTabela(Tb, SDDados);
          QrUpd.SQL.Text := Memo1.Text;
          UnMyMySQL.ExecutaQuery(QrUpd);


      CamposTabDst := UnMyMySQL.ObtemCamposDeTabelaIdentica(DBAnt, Tb, '');
      SQL_FIELDS := 'INSERT INTO ' + tb + ' (' + CamposTabDst + ') VALUES ';
      //Geral.MB_Info(SQL_FIELDS);
      UnMyMySQL.AbreMySQLQuery1(QrCampos, [
      'SELECT * ',
      'FROM ' + FTabTabs,
      'WHERE Tabela="' + Tb + '" ',
      '']);
      //SQLa := 'INSERT INTO ' + Tb + ' VALUES ';
      SQLa := 'INSERT INTO ' + Tb + ' SET ';
      //
      Continua := True;
      ItsOK := 0;
      //





(*
        SDDados.Close;
        SDDados.DataSet.CommandText := 'SELECT FIRST 1 * FROM ' + Tb;
        SDDados.Open;

          MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando tabela no MySQL.');
          EstruturaTabela(Tb, SDDados);
          QrUpd.SQL.Text := Memo1.Text;
          UnMyMySQL.ExecutaQuery(QrUpd);
*)




  SDAux.Close;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + Tb;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
      UnMyMySQL.ExecutaMySQLQuery1(QrUpd, [
      'DELETE FROM ' + tb,
      '']);
      //
      QrUpd.SQL.Text := '';
      while Total > ItsOK do
      begin
        MyObjects.UpdPB(PB2, nil, nil);
        SQL := 'SELECT ';
        if CkLimit.Checked then
        begin
          if EdFirst.ValueVariant > 0 then
          begin
            SQL := SQL + ' FIRST ' + Geral.FF0(Step);
            if ItsOK > 0 then
              SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
          end;
        end;
        //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
        SQL := SQL + ' * FROM ' + QrTabelasTabela.Value;
        SDDados.Close;
        SDDados.DataSet.CommandText := SQL;
        SDDados.Open;
        Continua := SDDados.RecordCount > 0;
        if Continua then
        begin
          MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
            Geral.FF0(SDDados.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
          SQL_VALUES := '';
          SDDados.First;
          while not SDDados.Eof do
          begin
            try
              SQLb       := '';
              N          := 0;
              Mostra     := False;
              if SDDados.RecNo = 1 then
                SQL_Rec    := '('
              else
                SQL_Rec    := ', (';
              for I := 0 to SDDados.FieldCount - 1 do
              begin
                if I > 0 then
                  SQL_Rec := SQL_Rec + ', ';
                if SDDados.Fields[I].Value = Null then
                  SQL_Rec := SQL_Rec + 'null '
                else
                //if SDDados.Fields[I].Value <> Null then
                begin
                  (*
                  if N > 0 then
                    SQLb := SQLb + ', ';
                  //
                  Extra := '';
                  Campo := SDDados.Fields[I].FieldName;
                  *)
                  if SDDados.Fields[I].DataType = ftFMTBcd then
                    Valor := Geral.VariavelToString(TFMTBCDField(SDDados.Fields[I]).AsVariant)
                  else
                    if (SDDados.Fields[I].DataType in ([ftBlob])) then
                    begin
                      if pos('{\rtf', SDDados.Fields[I].Value) = 1 then
                      begin
                        Temp := SDDados.Fields[I].AsString;
                        Valor := '';
                        for J := 1 to Length(Temp) do
                        begin
                          if Ord(Temp[J]) <> 0 then
                            Valor := Valor + Temp[J];
                        end;
                        Valor := '"' + Valor + '"';
                      end else
                      if pos('BM', SDDados.Fields[I].Value) = 1 then
                      begin
                        // n�o faz nada!parei aqui
                      end else
                        //Valor := Geral.VariavelToString(SDDados.Fields[I].Value);
                        Valor := '""';
                    end
                    else
                    if (SDDados.Fields[I].DataType in ([ftMemo])) then
                    begin
                      if pos('{\rtf', SDDados.Fields[I].Value) = 1 then
                      begin
                        Temp := SDDados.Fields[I].AsString;
                        Valor := '';
                        for J := 1 to Length(Temp) do
                        begin
                          if Ord(Temp[J]) <> 0 then
                            Valor := Valor + Temp[J];
                        end;
                        Valor := '"' + Valor + '"';
                      end else
                    end
                    else
                    //if (Integer(SDDados.Fields[I].DataType) = 8209) then
                    if (SDDados.Fields[I].DataType) in ([ftBlob]) then
                    begin
                      //Tipo de vari�vel: 8209 => Array Byte
                      Valor := '""';
                    end
                    else
                      Valor := Geral.VariavelToString(SDDados.Fields[I].Value);
                  if Valor = 'Null' then
                    Valor := '';
                  SQLb := SQLb + Campo + '='(*+ ''''*) + Extra + Valor + Extra(*+ ''''*);
                  N := N + 1;
                  //
                  SQL_Rec := SQL_Rec + Valor;
                end;
              end;
              //
(*
              SQLb := SQLa + SQLb;
              SQLc := SQLc + SQLb + ';';
              if Mostra then
                Geral.MensagemBox(SQLb, 'SQL a ser Executada', MB_OK+MB_ICONINFORMATION);
              //
              //DBAnt.Execute(SQLb);
*)
              SQL_Rec := SQL_Rec + ') ';
              //
              SQL_VALUES := SQL_VALUES + sLineBreak + SQL_Rec;
            except
              ;
            end;
            SDDados.Next;
          end;
        end;
(*
        if SQLc <> EMptyStr then
        begin
          //DBAnt.Execute(SQLc);
          MySQLBatchExecute1.SQL.Text := SQLc;
          MySQLBatchExecute1.ExecSQL;
        end;
        SQLc := EmptyStr;
*)
        if SQL_VALUES <> EmptyStr then
        begin
          //DBAnt.Execute(SQLc);
          MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
          try
            MySQLBatchExecute1.ExecSQL;
          except
            on E: Exception do
            begin
              Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
              E.Message + sLineBreak +
              SQL_FIELDS + SQL_VALUES);
              //
              EXIT;
            end;
          end;
        end;
        SQLc := EmptyStr;

        //
        ItsOK := ItsOK + Step;
      end;
      //
    end;
    //QrTabelas.Next;
  end;
  //
  Geral.MensagemBox('Cria��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, False, '...');
  MyObjects.Informa2(LaAvisoB1, LaAvisoB2, False, '...');
  PB1.Position := 0;
  //
end;

procedure TFmDB_Converte_Tisolin.EstruturaBDs();
var
  DB: String;
  FLCampos   : TList;
  FLIndices  : TList;
var
  Tabela, Campo, DataType, PrimaryKey, NotNull, ForeignKey, IndiceChave, TabelaChave, CampoChave, RegraUpdate, RegraDelete: String;
  Tamanho, Escala: Integer;
begin
  MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Verificando e conectando ao BD ' + DB);
  DB := LowerCase(EdDBNome.Text);
  if Conectou('mysql', True) then
  begin
    try
      FLCampos  := TList.Create;
      FLIndices := TList.Create;
      //
      UnMyMySQL.AbreMySQLQuery1(QrAux, [
      'SHOW DATABASES LIKE "' + DB + '" ',
      '']);
      if QrAux.RecordCount = 0 then
      begin
        UnMyMySQL.ExecutaMySQLQuery1(QrUpd, [
        'CREATE DATABASE ' + DB,
        '']);
      end;
      if not Conectou(DB, True) then Exit;
      //
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True, 'Verificando a tabela de tabelas');
      //if not TabelaExiste(QrAux, DB, FTabTabs) then
      if ExcluiTabela(QrAux, DB, FTabTabs) then
      begin
        FLCampos.Clear;
        FLIndices.Clear;
        UnMyMySQL.AdicionaCampo(FLCampos, 'Tabela'       , 'varchar(60)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Campo'        , 'varchar(60)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'DataType'     , 'varchar(30)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Tamanho'      , 'int(11)'    , '0');
        UnMyMySQL.AdicionaCampo(FLCampos, 'Escala'       , 'int(11)'    , '0');
        UnMyMySQL.AdicionaCampo(FLCampos, 'PrimaryKey'   , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'NotNull'      , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'ForeignKey'   , 'char(1)'    , 'o');
        UnMyMySQL.AdicionaCampo(FLCampos, 'IndiceChave'  , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'TabelaChave'  , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'CampoChave'   , 'varchar(60)', '' );
        UnMyMySQL.AdicionaCampo(FLCampos, 'RegraUpdate'  , 'varchar(30)', '?');
        UnMyMySQL.AdicionaCampo(FLCampos, 'RegraDelete'  , 'varchar(30)', '?');
        //
        UnMyMySQL.CriaTabela(QrUpd, FTabTabs, FTabTabs, Memo1, actCreate,
        LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2, FLCampos, FLIndices);
      end;
      //
      PB1.Max := SDFields.RecordCount;
      PB1.Position := SDFields.RecordCount;
      SDFields.First;
      while not SDFields.Eof do
      begin
        PB1.Position := PB1.Position - 1;
        MyObjects.Informa2(LaAvisoR1, LaAvisoR2, True,
          'Inserindo informa��es das tabelas na tabela das tabelas ' +
        Geral.FF0(SDFields.RecNo) + ' de ' + Geral.FF0(SDFields.RecordCount));
        Tabela         := SDFieldsTABELA.Value;
        Campo          := SDFieldsCAMPO.Value;
        DataType       := SDFieldsTIPO_CAMPO.Value;
        Tamanho        := SDFieldsTAMANHO.Value;
        Escala         := SDFieldsESCALA.AsInteger;
        PrimaryKey     := SDFieldsPRIMARY_KEY.Value;
        NotNull        := SDFieldsNOT_NULL.Value;
        ForeignKey     := SDFieldsFOREIGN_KEY.Value;
        IndiceChave    := SDFieldsINDICE_CHAVE.Value;
        TabelaChave    := SDFieldsTABELA_CHAVE.Value;
        CampoChave     := SDFieldsCAMPO_CHAVE.Value;
        RegraUpdate    := SDFieldsREGRA_UPDATE.Value;
        RegraDelete    := SDFieldsREGRA_DELETE.Value;
        //
        //if
        //if (Tabela= 'BOLETO_PAGAR') and (Campo = '' then

        UnMyMySQL.SQLInsUpd(QrUpd, stIns, '_tab_tabs_', False, [
        'Tabela', 'Campo', 'DataType',
        'Tamanho', 'Escala', 'PrimaryKey',
        'NotNull', 'ForeignKey', 'IndiceChave',
        'TabelaChave', 'CampoChave', 'RegraUpdate',
        'RegraDelete'], [
        ], [
        Tabela, Campo, DataType,
        Tamanho, Escala, PrimaryKey,
        NotNull, ForeignKey, IndiceChave,
        TabelaChave, CampoChave, RegraUpdate,
        RegraDelete], [
        ], False);
        //
        SDFields.Next;
      end;
      //
{
      VerificaEstruturaTabelas();
      //
      ImportarRegistrosTabelas();
      //...
      Geral.MensagemBox('Verifica��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
}
      BtImportaDados.Enabled := True;
      //
      MyObjects.Informa2(LaAvisoR1, LaAvisoR2, False, '...');
    finally
{
      if FLCampos <> nil then
        FLCampos := nil;
      if FLIndices <> nil then
        FLIndices := nil;
}
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.EstruturaTabela(NomeTab: String;
  SD: TSimpleDataset);
  function DC(Texto: String; Tamanho: Integer): String;
  begin
    Result := Texto;
    while length(Result) < Tamanho do
      Result := Result + ' ';
  end;
(*
const
  NomeTab = 'NomeTabela';
*)
var
  Memo: TMemo;
  FIndice,
  Linha, Virgula, V1, V2, V3, V5: String;
  //
  //J,
  I, T, P: Integer;
begin
  FIndice := '';
  Memo := Memo1;
  Memo.Lines.Clear;

{
  if Nome = Lowercase('CNAB_Rem') then begin
    Qry.SQL.Add('CREATE TABLE CNAB_Rem (');
    Qry.SQL.Add('  Item      int(11)    AUTO_INCREMENT, ');
    Qry.SQL.Add('  Linha     int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  Codigo    int(11)    NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  TipoDado  tinyint(3) NOT NULL DEFAULT 0, ');
    Qry.SQL.Add('  DadoI     int(11)             DEFAULT NULL, ');
    Qry.SQL.Add('  DadoF     double              DEFAULT NULL, ');
    Qry.SQL.Add('  DadoD     date                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoH     time                DEFAULT NULL, ');
    Qry.SQL.Add('  DadoT     varchar(255)        DEFAULT "", ');
    Qry.SQL.Add('  PRIMARY KEY (Item)');
    Qry.SQL.Add(') TYPE=MyISAM');
    Qry.ExecSQL;
  end else
}
  //Memo.Lines.Add('  if Nome = Lowercase('''+ NomeTab +''') then begin');
  Memo.Lines.Add('DROP TABLE IF EXISTS ' + NomeTab +';');
  Memo.Lines.Add('CREATE TABLE ' + NomeTab +' (');
  for I := 0 to SD.Fields.Count - 1 do
  begin
    V1 := SD.Fields[I].FullName;
    case SD.Fields[I].DataType of
      ftUnknown:        V2 := 'Unknown';
      ftString:         V2 := 'Varchar'; // diferente
      ftSmallint:       V2 := 'Smallint';
      ftInteger:        V2 := 'Integer';
      ftWord:           V2 := 'Word';
      ftBoolean:        V2 := 'tinyint(1)'; // 'Boolean'; Diferente? n�o testado
      ftFloat:          V2 := 'Float';
      ftCurrency:       V2 := 'Currency';
      ftBCD:            V2 := 'BCD';
      ftDate:           V2 := 'Date';
      ftTime:           V2 := 'Time';
      ftDateTime:       V2 := 'DateTime';
      ftBytes:          V2 := 'Bytes';
      ftVarBytes:       V2 := 'VarBytes';
      ftAutoInc:        V2 := 'AutoInc';
      ftMemo:           V2 := 'Text';  // diferente
      ftGraphic:        V2 := 'Graphic';
      ftFmtMemo:        V2 := 'FmtMemo';
      ftParadoxOle:     V2 := 'ParadoxOle';
      ftDBaseOle:       V2 := 'DBaseOle';
      ftTypedBinary:    V2 := 'TypedBinary';
      ftCursor:         V2 := 'Cursor';
      ftFixedChar:      V2 := 'FixedChar';
      ftWideString:     V2 := 'WideString';
      ftLargeint:       V2 := 'Largeint';
      ftADT:            V2 := 'ADT';
      ftArray:          V2 := 'Array';
      ftReference:      V2 := 'Reference';
      ftDataSet:        V2 := 'DataSet';
      ftOraBlob:        V2 := 'OraBlob';
      ftOraClob:        V2 := 'OraClob';
      ftVariant:        V2 := 'Variant';
      ftInterface:      V2 := 'Interface';
      ftIDispatch:      V2 := 'IDispatch';
      ftGuid:           V2 := 'Guid';
      ftTimeStamp:      V2 := 'TimeStamp';
      ftFMTBcd:         //V2 := 'BigInt';// 'Double'; //'FMTBcd';
      begin
        if TFMTBcdField(SD.Fields[I]).Size > 0 then
          V2 := 'Double' //'FMTBcd';
        else
          V2 := 'BigInt';//'FMTBcd';
      end;
      ftFixedWideChar:  V2 := 'FixedWideChar';
      ftWideMemo:       V2 := 'WideMemo';
      ftOraTimeStamp:   V2 := 'OraTimeStamp';
      ftOraInterval:    V2 := 'OraInterval';
      ftBlob:
      begin
        V2 := '';
        SD.First;
        while not SD.Eof do
        begin
          if pos('BM[', SD.Fields[I].Value) = 1 then
          begin
            V2 := 'blob';
            SD.Last;
          end;
          SD.Next;
        end;
        SD.First;
        if V2 = '' then
          V2 := 'Text';
      end;
    end;
    if not (SD.Fields[I].DataType in ([ftBlob, ftMemo])) then
    begin
      if SD.Fields[I].DataType = ftFMTBcd then
        T := TFMTBcdField(SD.Fields[I]).Precision
      else
        T := SD.Fields[I].Size;
      if T > 0 then
      begin
        V2 := V2 + '(' + FormatFloat('0', T);
        case SD.Fields[I].DataType of
          ftFloat:   P := TFloatField(SD.Fields[I]).Precision;
          ftBCD:     P := TBCDField(SD.Fields[I]).Precision;
          ftFMTBcd:  P := SD.Fields[I].Size;
          else P := 0;
        end;
        if P > 0 then
          V2 := V2 + ',' + FormatFloat('0', P);
        V2 := V2 + ')';
      end;
    end;
    //
    if SD.Fields[I].Required then
      V3 := 'NOT NULL'
    else
      V3 := '';
    if SD.Fields[I].IsIndexField then
    begin
      FIndice := FIndice + V1 + ',';
      //V4 := 'PRI';
    end;
    V5 := ''; // N�o tem como saber o valor default?
    //Linha := '    Qry.SQL.Add(''  '+
    Linha := '    ' +
      DC(V1,20)+' '+
      DC(V2,12)+' '+
      DC(V3,08)+' '+
      //DC(V4,00)+' '+
      DC(V5,20);
    if (I + 1 < SD.Fields.Count) or (FIndice <> '') then
      Virgula := ','
    else Virgula := '';
    Linha := Linha + Virgula;
    //Linha := Linha + ''');';
    //
    Memo.Lines.Add(Linha);
  end;
  if FIndice <> '' then
  begin
    FIndice := Copy(FIndice, 1, Length(FIndice)-2);
    //Memo.Lines.Add('    Qry.SQL.Add(''  PRIMARY KEY (' + FIndice + ')'');');
    Memo.Lines.Add('    PRIMARY KEY (' + FIndice + ')');
  end;
  Memo.Lines.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci;');
  //Memo.Lines.Add('    Qry.SQL.Add('') TYPE=MyISAM'');');
  //Memo.Lines.Add('    Qry.ExecSQL;');
  //Memo.Lines.Add('  end else');
end;

function TFmDB_Converte_Tisolin.ExcluiTabela(QryAux: TMySQLQuery; DB,
  Tabela: String): Boolean;
begin
  UnMyMySQL.ExecutaMySQLQuery1(QryAux, [
  'DROP TABLE IF EXISTS ' + Tabela,
  '']);
  Result := not TabelaExiste(QryAux, DB, Tabela);
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_AnexoDocumento();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Nome, TIPO, DESCRICAO, TIPOARQUIVO, DATAATUALIZACAO, DtHr, DataHora: String;
  N, Codigo, CodEnti, TipImg, CHAVE, SEQUENCIA: Integer;
  SQLType: TSQLType;
const
  OriTab = 'anexodocumento';
  DstTab = 'entiimgs';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Nome, CodEnti, TipImg, TIPO, CHAVE, SEQUENCIA, ',
    'DESCRICAO, TIPOARQUIVO, DATAATUALIZACAO) VALUES ']);
  //

  ItsOK := 0;
  N := 0;
  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDAnexoDocumento.Close;
    SDAnexoDocumento.DataSet.CommandType := ctQuery;
    SDAnexoDocumento.DataSet.CommandText := SQL;
    SDAnexoDocumento.Open;
    Continua := SDAnexoDocumento.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDAnexoDocumento.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDAnexoDocumento.First;
      SQLRec    := '';
      while not SDAnexoDocumento.Eof do
      begin
        N := N + 1;
        Codigo := N;
        DtHr     := SDANEXODOCUMENTO.FieldByName('DATAATUALIZACAO').AsWideString;
        DataHora := Geral.FDT(Geral.ValidaDataBR(Copy(DtHr, 1, 10), True, False), 1);
        DataHora := DataHora + ' ' + Copy(DtHr, 11);
        //
        if SDAnexoDocumento.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Nome,
        + PV_N(Geral.VariavelToString(SDAnexoDocumentoNOME.Value))
        //CodEnti        := ;
        + PV_N('0') //Geral.FF0(Integer(SDAnexoDocumentoCHAVE.Value)))
        //TipImg         := ;
        + PV_N('-1')
        //TIPO           := ;
        + PV_N(Geral.VariavelToString(SDAnexoDocumentoTIPO.Value))
        //CHAVE          := ;
        + PV_N(Geral.FF0(Integer(SDAnexoDocumentoCHAVE.Value)))
        //SEQUENCIA      := ;
        + PV_N(Geral.FF0(Integer(SDAnexoDocumentoSEQUENCIA.Value)))
        //DESCRICAO      := ;
        + PV_N(Geral.VariavelToString(SDAnexoDocumentoDESCRICAO.Value))
        //TIPOARQUIVO    := ;
        + PV_N(Geral.VariavelToString(SDAnexoDocumentoTIPOARQUIVO.Value))
        //DATAATUALIZACAO:= ;
        + PV_A(DataHora)
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDAnexoDocumento.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDAnexoDocumento.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Cliente;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;

const
  OriTab = 'Cliente';
  DstTab = 'entidades';

var
  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,
  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,
  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,
  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,
  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,
  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
  Fornece8, Terceiro, Cadastro, Informacoes, (*Logo,*) Mensal, Observacoes, CRua,
  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,
  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel, (*Logo2,*)
  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,
  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,
  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,
  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,
  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus:
  String;

  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,
  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,
  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,
  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,
  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,
  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,
  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,
  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,
  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais:
  Integer;

  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,
  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,
  JuroSacado, CPMF: Double;

begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    '  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,',
    '  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,',
    '  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,',
    '  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,',
    '  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,',
    '  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,',
    '  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,',
    '  Fornece8, Terceiro, Cadastro, Informacoes, Mensal, Observacoes, CRua,',
    '  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,',
    '  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel,',
    '  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,',
    '  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,',
    '  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,',
    '  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,',
    '  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus,',
    '  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,',
    '  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,',
    '  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,',
    '  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,',
    '  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,',
    '  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,',
    '  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,',
    '  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,',
    '  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais, ',
    '  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,',
    '  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,',
    '  JuroSacado, CPMF',
{
  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,
  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,
  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,
  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,
  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,
  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
  Fornece8, Terceiro, Cadastro, Informacoes, Mensal, Observacoes, CRua,
  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,
  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel,
  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,
  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,
  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,
  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,
  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus,
  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,
  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,
  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,
  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,
  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,
  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,
  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,
  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,
  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais
  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,
  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,
  JuroSacado, CPMF
}
  ' ) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab +
  ' WHERE Codigo > -11');
  //

  SDCidade.Close;
  SDCidade.Open;
  //
  SDCidadC.Close;
  SDCidadC.Open;
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDCliente.Close;
    SDCliente.DataSet.CommandType := ctQuery;
    SDCliente.DataSet.CommandText := SQL;
    SDCliente.Open;
    Continua := SDCliente.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDCliente.First;
      SQLRec    := '';
      while not SDCliente.Eof do
      begin
        // Antes
        if Uppercase(SDClienteTIPOPESSOA.Value) = 'J' then
          Tipo           := 0   // Juridica
        else
          Tipo           := 1;  // Fisica
        // Sequencia no BD
        Codigo         := Integer(SDClienteCLIENTE.Value);
        CodUsu         := Codigo; // Acima
        Respons1       := ''; // SDCliente.Value;
        Respons2       := ''; // SDCliente.Value;
        Pai            := ''; // SDCliente.Value;
        Mae            := ''; // SDCliente.Value;
        if Tipo = 0 then
        begin
          RazaoSocial    := SDClienteRAZAOSOCIAL.Value;
          Fantasia       := SDClienteFANTASIA.Value;
          Nome           := '';
          Apelido        := '';
          CNPJ           := SDClienteCPFCGC.Value;
          IE             := SDClienteRGINSCRICAOESTADUAL.Value;
          CPF            := '';
          RG             := '';
          ELograd        := 0; // SDCliente.Value;
          ERua           := SDClienteENDERECO.Value;
          ENumero        := Integer(SDClienteENDERECONUMERO.Value);
          ECompl         := SDClienteCOMPLEMENTO.Value;
          EBairro        := SDClienteBAIRRO.Value;
          ECidade        := SDClienteCIDADE_DESCRICAO.Value;
          EUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
          if SDClienteCEP.Value = EMptyStr then
            ECEP           := 0
          else
            ECEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
          EPais          := 'Brasil';// SDCliente.Value;
          ETe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
          Ete2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
          Ete3           := ''; // SDCliente.Value;
          ECel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
          EFax           := ''; // SDCliente.Value;
          EEmail         := SDClienteEMAILPRIMARIO.Value;
          EContato       := SDClienteCONTATO.Value;
          ENatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

          PLograd        := 0;
          PRua           := '';
          PNumero        := 0;
          PCompl         := '';
          PBairro        := '';
          PCidade        := '';
          PUF            := 0;
          PCEP           := 0;
          PPais          := '';
          PTe1           := '';
          Pte2           := '';
          Pte3           := '';
          PCel           := '';
          PFax           := '';
          PEmail         := '';
          PContato       := '';
          PNatal         := '0000-00-00';

          PCidadeCod     := 0;
          ECidadeCod     := 0;
          PPaisCod       := 0;
          EPaisCod       := 0;

          PCodMunici     := 0;
          PCodiPais      := 0;
          if SDClienteCIDADE_IBGE.Value = EmptyStr then
            ECodMunici := 0
          else
            ECodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
          //
          if SDClienteCIDADE_PAIS.Value = EmptyStr then
            ECodiPais       := 0
          else
            ECodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

        end else
        begin
          RazaoSocial    := '';
          Fantasia       := '';
          Nome           := SDClienteRAZAOSOCIAL.Value;
          Apelido        := SDClienteFANTASIA.Value;
          CNPJ           := '';
          IE             := '';
          CPF            := SDClienteCPFCGC.Value;
          RG             := SDClienteRGINSCRICAOESTADUAL.Value;

          ELograd        := 0;
          ERua           := '';
          ENumero        := 0;
          ECompl         := '';
          EBairro        := '';
          ECidade        := '';
          EUF            := 0;
          ECEP           := 0;
          EPais          := '';
          ETe1           := '';
          Ete2           := '';
          Ete3           := '';
          ECel           := '';
          EFax           := '';
          EEmail         := '';
          EContato       := '';
          ENatal         := '0000-00-00';

          PLograd        := 0; // SDCliente.Value;
          PRua           := SDClienteENDERECO.Value;
          PNumero        := Integer(SDClienteENDERECONUMERO.Value);
          PCompl         := SDClienteCOMPLEMENTO.Value;
          PBairro        := SDClienteBAIRRO.Value;
          PCidade        := SDClienteCIDADE_DESCRICAO.Value;
          PUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
          if SDClienteCEP.Value = EmptyStr then
            PCEP           := 0
          else
            PCEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
          PPais          := 'Brasil'; // SDCliente.Value;
          PTe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
          Pte2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
          Pte3           := ''; // SDCliente.Value;
          PCel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
          PFax           := ''; // SDCliente.Value;
          PEmail         := SDClienteEMAILPRIMARIO.Value;
          PContato       := SDClienteCONTATO.Value;
          PNatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

          PCidadeCod     := 0;
          ECidadeCod     := 0;
          PPaisCod       := 0;
          EPaisCod       := 0;

          ECodMunici     := 0;
          ECodiPais      := 0;
          if SDClienteCIDADE_IBGE.Value = EmptyStr then
            PCodMunici := 0
          else
            PCodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
          //
          if SDClienteCIDADE_PAIS.Value = EmptyStr then
            PCodiPais       := 0
          else
            PCodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

        end;
        NIRE           := ''; // SDCliente.Value;
        FormaSociet    := ''; // SDCliente.Value;
        Simples        := 0; // SDCliente.Value;
        IEST           := ''; // SDCliente.Value;
        Atividade      := ''; // SDCliente.Value;
        //CPF            := Acima!!!
        CPF_Pai        := ''; // SDCliente.Value;
        CPF_Conjuge    := ''; // SDCliente.Value;
        //RG             := Acima
        SSP            := ''; // SDCliente.Value;
        DataRG         := '0000-00-00'; // SDCliente.Value;
        CidadeNatal    := ''; // SDClienteNATURALIDADE.Value;
        EstCivil       := 0; // SDCliente.Value;
        UFNatal        := 0; // SDCliente.Value;
        Nacionalid     := ''; // SDCliente.Value;
        Sexo           := ''; // SDCliente.Value;
        Responsavel    := ''; // SDCliente.Value;
        Profissao      := ''; // SDCliente.Value;
        Cargo          := ''; // SDCliente.Value;
        Recibo         := 0; // SDCliente.Value;
        DiaRecibo      := 0; // SDCliente.Value;
        AjudaEmpV      := 0; // SDCliente.Value;
        AjudaEmpP      := 0; // SDCliente.Value;
        //
        Cliente1       := 'F';
        Cliente2       := 'F';
        Cliente3       := 'F';
        Cliente4       := 'F';
        Fornece1       := 'F';
        Fornece2       := 'F';
        Fornece3       := 'F';
        Fornece4       := 'F';
        Fornece5       := 'F';
        Fornece6       := 'F';
        Fornece7       := 'F';
        Fornece8       := 'F';
        Terceiro       := 'F';
        case Integer(SDClienteTIPOCADASTRO.Value) of
          1: Cliente1       := 'V';
          2: Fornece1       := 'V';
          else MeErrosGerais.Lines.Add('Cliente ' + Geral.FF0(Codigo) + ' ' + Geral.FF0(Integer(SDClienteTIPOCADASTRO.Value)) + ' n�o implementado!!!');
        end;
        Cadastro       := Geral.FDT(SDClienteDATACADASTRO.Value, 1);
        Informacoes    := SDClienteINFORMACOES.Value;
        //Logo           := ''; // SDCliente.Value;
        Veiculo        := 0; // SDCliente.Value;
        Mensal         := ''; // SDCliente.Value;
        Observacoes    := SDClienteOBSERVACAO.Value;
        //Tipo           := Acima!!
        CLograd        := 0; //SDCliente.Value;
        CRua           := ''; //SDClienteENDERECOCOBRANCA.Value;
        CNumero        := 0; // SDCliente.Value;
        CCompl         := ''; //SDClienteCOMPLEMENTOCOBRANCA.Value;
        CBairro        := ''; //SDClienteBAIRROENDERECOCOBRANCA.Value;
        CCidade        := ''; //SDClienteCIDADC_DESCRICAO.Value;
        CUF            := 0; // SDCliente.Value;
        CCEP           := 0; //Geral.IMV(Geral.SoNumero_TT(SDClienteCEPENDERECOCOBRANCA.Value));
        CPais          := ''; // SDCliente.Value;
        if SDClienteCIDADC_IBGE.Value = EmptyStr then
          CCodMunici := 0
        else
          CCodMunici     := 0; //Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADC_IBGE.Value));
        //
        CTel           := ''; // SDCliente.Value;
        CCel           := ''; // SDCliente.Value;
        CFax           := ''; // SDCliente.Value;
        CContato       := ''; // SDCliente.Value;
        LLograd        := 0; // SDCliente.Value;
        LRua           := ''; // SDCliente.Value;
        LNumero        := 0; // SDCliente.Value;
        LCompl         := ''; // SDCliente.Value;
        LBairro        := ''; // SDCliente.Value;
        LCidade        := ''; // SDCliente.Value;
        LUF            := 0; // SDCliente.Value;
        LCEP           := 0; // SDCliente.Value;
        LPais          := ''; // SDCliente.Value;
        LTel           := ''; // SDCliente.Value;
        LCel           := ''; // SDCliente.Value;
        LFax           := ''; // SDCliente.Value;
        LContato       := ''; // SDCliente.Value;
        Comissao       := 0.00; // SDCliente.Value;
        Situacao       := 0; // SDCliente.Value;
        Nivel          := ''; // SDCliente.Value;
        Grupo          := 0; // SDCliente.Value;
        Account        := 0; // SDCliente.Value;
        //Logo2          := ''; // SDCliente.Value;
        ConjugeNome    := ''; // SDCliente.Value;
        ConjugeNatal   := '0000-00-00'; // SDCliente.Value;
        Nome1          := ''; // SDCliente.Value;
        Natal1         := '0000-00-00'; // SDCliente.Value;
        Nome2          := ''; // SDCliente.Value;
        Natal2         := '0000-00-00'; // SDCliente.Value;
        Nome3          := ''; // SDCliente.Value;
        Natal3         := '0000-00-00'; // SDCliente.Value;
        Nome4          := ''; // SDCliente.Value;
        Natal4         := '0000-00-00'; // SDCliente.Value;
        CreditosI      := 0; // SDCliente.Value;
        CreditosL      := 0; // SDCliente.Value;
        CreditosF2     := 0;
        CreditosD      := '0000-00-00'; // SDCliente.Value;
        CreditosU      := Geral.FDT(SDClienteULTIMACOMPRA.Value, 1);
        CreditosV      := '0000-00-00'; // SDCliente.Value;
        Motivo         := 0; // SDCliente.Value;
        QuantI1        := 0; // SDCliente.Value;
        QuantI2        := 0; // SDCliente.Value;
        QuantI3        := 0; // SDCliente.Value;
        QuantI4        := 0; // SDCliente.Value;
        QuantN1        := 0.00; // SDCliente.Value;
        QuantN2        := 0.00; // SDCliente.Value;
        QuantN3        := 0.00; // SDCliente.Value;
        Agenda         := ''; // SDCliente.Value;
        SenhaQuer      := ''; // SDCliente.Value;
        Senha1         := ''; // SDCliente.Value;
        LimiCred       := Double(SDClienteCREDITO.Value);
        if LimiCred = 0 then
          LimiCred := 500;
        Desco          := 0; // SDCliente.Value;
        CasasApliDesco := 0; // SDCliente.Value;
        TempA          := 0.00; // SDCliente.Value;
        TempD          := 0.00; // SDCliente.Value;
        Banco          := 0; // SDCliente.Value;
        Agencia        := ''; // SDCliente.Value;
        ContaCorrente  := ''; // SDCliente.Value;
        FatorCompra    := 0.00; // SDCliente.Value;
        AdValorem      := 0.00; // SDCliente.Value;
        DMaisC         := 0; // SDCliente.Value;
        DMaisD         := 0; // SDCliente.Value;
        Empresa        := 0; // SDCliente.Value;
        CBE            := 0; // SDCliente.Value;
        SCB            := 0; // SDCliente.Value;
        PAtividad      := 0; // SDCliente.Value;
        EAtividad      := 0; // SDCliente.Value;
        Antigo         := ''; // SDCliente.Value;
        CUF2           := ''; // SDCliente.Value;
        Contab         := ''; // SDCliente.Value;
        MSN1           := ''; // SDCliente.Value;
        PastaTxtFTP    := ''; // SDCliente.Value;
        PastaPwdFTP    := ''; // SDCliente.Value;
        Protestar      := 0; // SDCliente.Value;
        MultaCodi      := 0; // SDCliente.Value;
        MultaDias      := 0; // SDCliente.Value;
        MultaValr      := 0.00; // SDCliente.Value;
        MultaPerc      := 0.00; // SDCliente.Value;
        MultaTiVe      := 0; // SDCliente.Value;
        JuroSacado     := 0.00; // SDCliente.Value;
        CPMF           := 0.00; // SDCliente.Value;
        Corrido        := 0; // SDCliente.Value;
        CPF_Resp1      := ''; // SDCliente.Value;
        CliInt         := 0; // SDCliente.Value;
        AltDtPlaCt     := '0000-00-00'; // SDCliente.Value;
        CartPref       := 0; // SDCliente.Value;
        RolComis       := 0; // SDCliente.Value;
        Filial         := 0; // SDCliente.Value;
        EEndeRef       := ''; // SDCliente.Value;
        PEndeRef       := ''; // SDCliente.Value;
        CEndeRef       := ''; // SDCliente.Value;
        LEndeRef       := ''; // SDCliente.Value;
        CNAE           := ''; // SDCliente.Value;
        SUFRAMA        := ''; // SDCliente.Value;
        L_CNPJ         := ''; // SDCliente.Value;
        L_Ativo        := 0; // SDCliente.Value;
        URL            := ''; // SDCliente.Value;
        CRT            := 0; // SDCliente.Value;
        COD_PART       := ''; // SDCliente.Value;
        ETe1Tip        := 0; // SDCliente.Value;
        ECelTip        := 0; // SDCliente.Value;
        PTe1Tip        := 0; // SDCliente.Value;
        PCelTip        := 0; // SDCliente.Value;
        ESite          := ''; // SDCliente.Value;
        PSite          := ''; // SDCliente.Value;
        EstrangDef     := 0; // SDCliente.Value;
        EstrangTip     := 0; // SDCliente.Value;
        EstrangNum     := ''; // SDCliente.Value;
        indIEDest      := 0; // SDCliente.Value;
        L_CPF          := ''; // SDCliente.Value;
        L_Nome         := ''; // SDCliente.Value;
        LCodiPais      := 0; // SDCliente.Value;
        LEmail         := SDClienteEMAILFINANCEIRO.Value;
        L_IE           := ''; // SDCliente.Value;
        // Campos Novos!!
        ListaStatus    := SDClienteLISTASTATUS.Value;


        //
        if SDCliente.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec

        +     (Geral.VariavelToString(RazaoSocial))
        + PV_N(Geral.VariavelToString(Fantasia))
        + PV_N(Geral.VariavelToString(Respons1))
        + PV_N(Geral.VariavelToString(Respons2))
        + PV_N(Geral.VariavelToString(Pai))
        + PV_N(Geral.VariavelToString(Mae))
        + PV_N(Geral.VariavelToString(CNPJ))
        + PV_N(Geral.VariavelToString(IE))
        + PV_N(Geral.VariavelToString(NIRE))
        + PV_N(Geral.VariavelToString(FormaSociet))
        + PV_N(Geral.VariavelToString(IEST))
        + PV_N(Geral.VariavelToString(Atividade))
        + PV_N(Geral.VariavelToString(Nome))
        + PV_N(Geral.VariavelToString(Apelido))
        + PV_N(Geral.VariavelToString(CPF))
        + PV_N(Geral.VariavelToString(CPF_Pai))
        + PV_N(Geral.VariavelToString(CPF_Conjuge))
        + PV_N(Geral.VariavelToString(RG))
        + PV_N(Geral.VariavelToString(SSP))
        + PV_N(Geral.VariavelToString(DataRG))
        + PV_N(Geral.VariavelToString(CidadeNatal))
        + PV_N(Geral.VariavelToString(Nacionalid))
        + PV_N(Geral.VariavelToString(ERua))
        + PV_N(Geral.VariavelToString(ECompl))
        + PV_N(Geral.VariavelToString(EBairro))
        + PV_N(Geral.VariavelToString(ECidade))
        + PV_N(Geral.VariavelToString(EPais))
        + PV_N(Geral.VariavelToString(ETe1))
        + PV_N(Geral.VariavelToString(Ete2))
        + PV_N(Geral.VariavelToString(Ete3))
        + PV_N(Geral.VariavelToString(ECel))
        + PV_N(Geral.VariavelToString(EFax))
        + PV_N(Geral.VariavelToString(EEmail))
        + PV_N(Geral.VariavelToString(EContato))
        + PV_N(Geral.VariavelToString(ENatal))
        + PV_N(Geral.VariavelToString(PRua))
        + PV_N(Geral.VariavelToString(PCompl))
        + PV_N(Geral.VariavelToString(PBairro))
        + PV_N(Geral.VariavelToString(PCidade))
        + PV_N(Geral.VariavelToString(PPais))
        + PV_N(Geral.VariavelToString(PTe1))
        + PV_N(Geral.VariavelToString(Pte2))
        + PV_N(Geral.VariavelToString(Pte3))
        + PV_N(Geral.VariavelToString(PCel))
        + PV_N(Geral.VariavelToString(PFax))
        + PV_N(Geral.VariavelToString(PEmail))
        + PV_N(Geral.VariavelToString(PContato))
        + PV_N(Geral.VariavelToString(PNatal))
        + PV_N(Geral.VariavelToString(Sexo))
        + PV_N(Geral.VariavelToString(Responsavel))
        + PV_N(Geral.VariavelToString(Profissao))
        + PV_N(Geral.VariavelToString(Cargo))
        + PV_N(Geral.VariavelToString(Cliente1))
        + PV_N(Geral.VariavelToString(Cliente2))
        + PV_N(Geral.VariavelToString(Cliente3))
        + PV_N(Geral.VariavelToString(Cliente4))
        + PV_N(Geral.VariavelToString(Fornece1))
        + PV_N(Geral.VariavelToString(Fornece2))
        + PV_N(Geral.VariavelToString(Fornece3))
        + PV_N(Geral.VariavelToString(Fornece4))
        + PV_N(Geral.VariavelToString(Fornece5))
        + PV_N(Geral.VariavelToString(Fornece6))
        + PV_N(Geral.VariavelToString(Fornece7))
        + PV_N(Geral.VariavelToString(Fornece8))
        + PV_N(Geral.VariavelToString(Terceiro))
        + PV_N(Geral.VariavelToString(Cadastro))
        + PV_N(Geral.VariavelToString(Informacoes))
        + PV_N(Geral.VariavelToString(Mensal))
        + PV_N(Geral.VariavelToString(Observacoes))
        + PV_N(Geral.VariavelToString(CRua))
        + PV_N(Geral.VariavelToString(CCompl))
        + PV_N(Geral.VariavelToString(CBairro))
        + PV_N(Geral.VariavelToString(CCidade))
        + PV_N(Geral.VariavelToString(CPais))
        + PV_N(Geral.VariavelToString(CTel))
        + PV_N(Geral.VariavelToString(CCel))
        + PV_N(Geral.VariavelToString(CFax))
        + PV_N(Geral.VariavelToString(CContato))
        + PV_N(Geral.VariavelToString(LRua))
        + PV_N(Geral.VariavelToString(LCompl))
        + PV_N(Geral.VariavelToString(LBairro))
        + PV_N(Geral.VariavelToString(LCidade))
        + PV_N(Geral.VariavelToString(LPais))
        + PV_N(Geral.VariavelToString(LTel))
        + PV_N(Geral.VariavelToString(LCel))
        + PV_N(Geral.VariavelToString(LFax))
        + PV_N(Geral.VariavelToString(LContato))
        + PV_N(Geral.VariavelToString(Nivel))
        + PV_N(Geral.VariavelToString(ConjugeNome))
        + PV_N(Geral.VariavelToString(ConjugeNatal))
        + PV_N(Geral.VariavelToString(Nome1))
        + PV_N(Geral.VariavelToString(Natal1))
        + PV_N(Geral.VariavelToString(Nome2))
        + PV_N(Geral.VariavelToString(Natal2))
        + PV_N(Geral.VariavelToString(Nome3))
        + PV_N(Geral.VariavelToString(Natal3))
        + PV_N(Geral.VariavelToString(Nome4))
        + PV_N(Geral.VariavelToString(Natal4))
        + PV_N(Geral.VariavelToString(CreditosD))
        + PV_N(Geral.VariavelToString(CreditosU))
        + PV_N(Geral.VariavelToString(CreditosV))
        + PV_N(Geral.VariavelToString(Agenda))
        + PV_N(Geral.VariavelToString(SenhaQuer))
        + PV_N(Geral.VariavelToString(Senha1))
        + PV_N(Geral.VariavelToString(Agencia))
        + PV_N(Geral.VariavelToString(ContaCorrente))
        + PV_N(Geral.VariavelToString(Antigo))
        + PV_N(Geral.VariavelToString(CUF2))
        + PV_N(Geral.VariavelToString(Contab))
        + PV_N(Geral.VariavelToString(MSN1))
        + PV_N(Geral.VariavelToString(PastaTxtFTP))
        + PV_N(Geral.VariavelToString(PastaPwdFTP))
        + PV_N(Geral.VariavelToString(CPF_Resp1))
        + PV_N(Geral.VariavelToString(AltDtPlaCt))
        + PV_N(Geral.VariavelToString(EEndeRef))
        + PV_N(Geral.VariavelToString(PEndeRef))
        + PV_N(Geral.VariavelToString(CEndeRef))
        + PV_N(Geral.VariavelToString(LEndeRef))
        + PV_N(Geral.VariavelToString(CNAE))
        + PV_N(Geral.VariavelToString(SUFRAMA))
        + PV_N(Geral.VariavelToString(L_CNPJ))
        + PV_N(Geral.VariavelToString(URL))
        + PV_N(Geral.VariavelToString(COD_PART))
        + PV_N(Geral.VariavelToString(ESite))
        + PV_N(Geral.VariavelToString(PSite))
        + PV_N(Geral.VariavelToString(EstrangNum))
        + PV_N(Geral.VariavelToString(L_CPF))
        + PV_N(Geral.VariavelToString(L_Nome))
        + PV_N(Geral.VariavelToString(LEmail))
        + PV_N(Geral.VariavelToString(L_IE))
        + PV_N(Geral.VariavelToString(ListaStatus))
        + PV_N(Geral.VariavelToString(Codigo))
        + PV_N(Geral.VariavelToString(CodUsu))
        + PV_N(Geral.VariavelToString(Simples))
        + PV_N(Geral.VariavelToString(EstCivil))
        + PV_N(Geral.VariavelToString(UFNatal))
        + PV_N(Geral.VariavelToString(ELograd))
        + PV_N(Geral.VariavelToString(ENumero))
        + PV_N(Geral.VariavelToString(EUF))
        + PV_N(Geral.VariavelToString(ECEP))
        + PV_N(Geral.VariavelToString(PLograd))
        + PV_N(Geral.VariavelToString(PNumero))
        + PV_N(Geral.VariavelToString(PUF))
        + PV_N(Geral.VariavelToString(PCEP))
        + PV_N(Geral.VariavelToString(Recibo))
        + PV_N(Geral.VariavelToString(DiaRecibo))
        + PV_N(Geral.VariavelToString(Veiculo))
        + PV_N(Geral.VariavelToString(Tipo))
        + PV_N(Geral.VariavelToString(CLograd))
        + PV_N(Geral.VariavelToString(CNumero))
        + PV_N(Geral.VariavelToString(CUF))
        + PV_N(Geral.VariavelToString(CCEP))
        + PV_N(Geral.VariavelToString(LLograd))
        + PV_N(Geral.VariavelToString(LNumero))
        + PV_N(Geral.VariavelToString(LUF))
        + PV_N(Geral.VariavelToString(LCEP))
        + PV_N(Geral.VariavelToString(Situacao))
        + PV_N(Geral.VariavelToString(Grupo))
        + PV_N(Geral.VariavelToString(Account))
        + PV_N(Geral.VariavelToString(CreditosI))
        + PV_N(Geral.VariavelToString(CreditosL))
        + PV_N(Geral.VariavelToString(Motivo))
        + PV_N(Geral.VariavelToString(QuantI1))
        + PV_N(Geral.VariavelToString(QuantI2))
        + PV_N(Geral.VariavelToString(QuantI3))
        + PV_N(Geral.VariavelToString(QuantI4))
        + PV_N(Geral.VariavelToString(CasasApliDesco))
        + PV_N(Geral.VariavelToString(Banco))
        + PV_N(Geral.VariavelToString(DMaisC))
        + PV_N(Geral.VariavelToString(DMaisD))
        + PV_N(Geral.VariavelToString(Empresa))
        + PV_N(Geral.VariavelToString(CBE))
        + PV_N(Geral.VariavelToString(SCB))
        + PV_N(Geral.VariavelToString(PAtividad))
        + PV_N(Geral.VariavelToString(EAtividad))
        + PV_N(Geral.VariavelToString(PCidadeCod))
        + PV_N(Geral.VariavelToString(ECidadeCod))
        + PV_N(Geral.VariavelToString(PPaisCod))
        + PV_N(Geral.VariavelToString(EPaisCod))
        + PV_N(Geral.VariavelToString(Protestar))
        + PV_N(Geral.VariavelToString(MultaCodi))
        + PV_N(Geral.VariavelToString(MultaDias))
        + PV_N(Geral.VariavelToString(MultaTiVe))
        + PV_N(Geral.VariavelToString(Corrido))
        + PV_N(Geral.VariavelToString(CliInt))
        + PV_N(Geral.VariavelToString(CartPref))
        + PV_N(Geral.VariavelToString(RolComis))
        + PV_N(Geral.VariavelToString(Filial))
        + PV_N(Geral.VariavelToString(ECodMunici))
        + PV_N(Geral.VariavelToString(PCodMunici))
        + PV_N(Geral.VariavelToString(CCodMunici))
        + PV_N(Geral.VariavelToString(LCodMunici))
        + PV_N(Geral.VariavelToString(ECodiPais))
        + PV_N(Geral.VariavelToString(PCodiPais))
        + PV_N(Geral.VariavelToString(L_Ativo))
        + PV_N(Geral.VariavelToString(CRT))
        + PV_N(Geral.VariavelToString(ETe1Tip))
        + PV_N(Geral.VariavelToString(ECelTip))
        + PV_N(Geral.VariavelToString(PTe1Tip))
        + PV_N(Geral.VariavelToString(PCelTip))
        + PV_N(Geral.VariavelToString(EstrangDef))
        + PV_N(Geral.VariavelToString(EstrangTip))
        + PV_N(Geral.VariavelToString(indIEDest))
        + PV_N(Geral.VariavelToString(LCodiPais))
        + PV_N(Geral.VariavelToString(AjudaEmpV))
        + PV_N(Geral.VariavelToString(AjudaEmpP))
        + PV_N(Geral.VariavelToString(Comissao))
        + PV_N(Geral.VariavelToString(CreditosF2))
        + PV_N(Geral.VariavelToString(QuantN1))
        + PV_N(Geral.VariavelToString(QuantN2))
        + PV_N(Geral.VariavelToString(QuantN3))
        + PV_N(Geral.VariavelToString(LimiCred))
        + PV_N(Geral.VariavelToString(Desco))
        + PV_N(Geral.VariavelToString(TempA))
        + PV_N(Geral.VariavelToString(TempD))
        + PV_N(Geral.VariavelToString(FatorCompra))
        + PV_N(Geral.VariavelToString(AdValorem))
        + PV_N(Geral.VariavelToString(MultaValr))
        + PV_N(Geral.VariavelToString(MultaPerc))
        + PV_N(Geral.VariavelToString(JuroSacado))
        + PV_N(Geral.VariavelToString(CPMF))
        + ')';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDCliente.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Cliente2;
var
  RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ, IE, NIRE,
  FormaSociet, IEST, Atividade, Nome, Apelido, CPF, CPF_Pai, CPF_Conjuge, RG,
  SSP, DataRG, CidadeNatal, Nacionalid, ERua, ECompl, EBairro, ECidade, EPais,
  ETe1, Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PRua, PCompl, PBairro,
  PCidade, PPais, PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal, Sexo,
  Responsavel, Profissao, Cargo, Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7,
  Fornece8, Terceiro, Cadastro, Informacoes, (*Logo,*) Mensal, Observacoes, CRua,
  CCompl, CBairro, CCidade, CPais, CTel, CCel, CFax, CContato, LRua, LCompl,
  LBairro, LCidade, LPais, LTel, LCel, LFax, LContato, Nivel, (*Logo2,*)
  ConjugeNome, ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3, Natal3, Nome4,
  Natal4, CreditosD, CreditosU, CreditosV, Agenda, SenhaQuer, Senha1, Agencia,
  ContaCorrente, Antigo, CUF2, Contab, MSN1, PastaTxtFTP, PastaPwdFTP, CPF_Resp1,
  AltDtPlaCt, EEndeRef, PEndeRef, CEndeRef, LEndeRef, CNAE, SUFRAMA, L_CNPJ, URL,
  COD_PART, ESite, PSite, EstrangNum, L_CPF, L_Nome, LEmail, L_IE, ListaStatus:
  String;

  Codigo, CodUsu, Simples, EstCivil, UFNatal, ELograd, ENumero, EUF, ECEP,
  PLograd, PNumero, PUF, PCEP, Recibo, DiaRecibo, Veiculo, Tipo, CLograd,
  CNumero, CUF, CCEP, LLograd, LNumero, LUF, LCEP, Situacao, Grupo, Account,
  CreditosI, CreditosL, Motivo, QuantI1, QuantI2, QuantI3, QuantI4,
  CasasApliDesco, Banco, DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad,
  EAtividad, PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Protestar, MultaCodi,
  MultaDias, MultaTiVe, Corrido, CliInt, CartPref, RolComis, Filial, ECodMunici,
  PCodMunici, CCodMunici, LCodMunici, ECodiPais, PCodiPais, L_Ativo, CRT, ETe1Tip,
  ECelTip, PTe1Tip, PCelTip, EstrangDef, EstrangTip, indIEDest, LCodiPais:
  Integer;

  AjudaEmpV, AjudaEmpP, Comissao, CreditosF2, QuantN1, QuantN2, QuantN3,
  LimiCred, Desco, TempA, TempD, FatorCompra, AdValorem, MultaValr, MultaPerc,
  JuroSacado, CPMF: Double;

  SQLType: TSQLType;

  ItemAtual, NextItens: Integer;
const
  Step = 20;
  Tabela = 'Cliente';
begin


  SQLType        := stIns;


  SDCidade.Close;
  SDCidade.Open;
  //
  SDCidadC.Close;
  SDCidadC.Open;
  //
  SDCliente.Close;
  SDCliente.Connection := DBFB;
  SDCliente.Open;
  SDCliente.First;
  ItemAtual := 0;
  NextItens := Step;
  PB1.Position := 0;
  PB1.Max := (SDCliente.RecordCount div Step) + 1;
  while not SDCliente.EOF do
  begin
    ItemAtual := ItemAtual + 1;
    if ItemAtual > NextItens then
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAvisoR1, LaAvisoR2, True,
      'Tabela ' + Tabela + '. Registros verificados: ' + Geral.FF0(NextItens));
      NextItens := NextItens + Step;
    end;

    // Antes
    if Uppercase(SDClienteTIPOPESSOA.Value) = 'J' then
      Tipo           := 0   // Juridica
    else
      Tipo           := 1;  // Fisica
    // Sequencia no BD
    Codigo         := Integer(SDClienteCLIENTE.Value);
    CodUsu         := Codigo; // Acima
    Respons1       := ''; // SDCliente.Value;
    Respons2       := ''; // SDCliente.Value;
    Pai            := ''; // SDCliente.Value;
    Mae            := ''; // SDCliente.Value;
    if Tipo = 0 then
    begin
      RazaoSocial    := SDClienteRAZAOSOCIAL.Value;
      Fantasia       := SDClienteFANTASIA.Value;
      Nome           := '';
      Apelido        := '';
      CNPJ           := SDClienteCPFCGC.Value;
      IE             := SDClienteRGINSCRICAOESTADUAL.Value;
      CPF            := '';
      RG             := '';
      ELograd        := 0; // SDCliente.Value;
      ERua           := SDClienteENDERECO.Value;
      ENumero        := Integer(SDClienteENDERECONUMERO.Value);
      ECompl         := SDClienteCOMPLEMENTO.Value;
      EBairro        := SDClienteBAIRRO.Value;
      ECidade        := SDClienteCIDADE_DESCRICAO.Value;
      EUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
      if SDClienteCEP.Value = EMptyStr then
        ECEP           := 0
      else
        ECEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
      EPais          := 'Brasil';// SDCliente.Value;
      ETe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
      Ete2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
      Ete3           := ''; // SDCliente.Value;
      ECel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
      EFax           := ''; // SDCliente.Value;
      EEmail         := SDClienteEMAILPRIMARIO.Value;
      EContato       := SDClienteCONTATO.Value;
      ENatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

      PLograd        := 0;
      PRua           := '';
      PNumero        := 0;
      PCompl         := '';
      PBairro        := '';
      PCidade        := '';
      PUF            := 0;
      PCEP           := 0;
      PPais          := '';
      PTe1           := '';
      Pte2           := '';
      Pte3           := '';
      PCel           := '';
      PFax           := '';
      PEmail         := '';
      PContato       := '';
      PNatal         := '';

      PCidadeCod     := 0;
      ECidadeCod     := 0;
      PPaisCod       := 0;
      EPaisCod       := 0;

      PCodMunici     := 0;
      PCodiPais      := 0;
      if SDClienteCIDADE_IBGE.Value = EmptyStr then
        ECodMunici := 0
      else
        ECodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
      //
      if SDClienteCIDADE_PAIS.Value = EmptyStr then
        ECodiPais       := 0
      else
        ECodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

    end else
    begin
      RazaoSocial    := '';
      Fantasia       := '';
      Nome           := SDClienteRAZAOSOCIAL.Value;
      Apelido        := SDClienteFANTASIA.Value;
      CNPJ           := '';
      IE             := '';
      CPF            := SDClienteCPFCGC.Value;
      RG             := SDClienteRGINSCRICAOESTADUAL.Value;

      ELograd        := 0;
      ERua           := '';
      ENumero        := 0;
      ECompl         := '';
      EBairro        := '';
      ECidade        := '';
      EUF            := 0;
      ECEP           := 0;
      EPais          := '';
      ETe1           := '';
      Ete2           := '';
      Ete3           := '';
      ECel           := '';
      EFax           := '';
      EEmail         := '';
      EContato       := '';
      ENatal         := '';

      PLograd        := 0; // SDCliente.Value;
      PRua           := SDClienteENDERECO.Value;
      PNumero        := Integer(SDClienteENDERECONUMERO.Value);
      PCompl         := SDClienteCOMPLEMENTO.Value;
      PBairro        := SDClienteBAIRRO.Value;
      PCidade        := SDClienteCIDADE_DESCRICAO.Value;
      PUF            := Geral.GetCodigoUF_da_SiglaUF(SDClienteCIDADE_UF.Value);
      if SDClienteCEP.Value = EmptyStr then
        PCEP           := 0
      else
        PCEP           := Geral.IMV(Geral.SoNumero_TT(SDClienteCEP.Value));
      PPais          := 'Brasil'; // SDCliente.Value;
      PTe1           := Geral.SoNumero_TT(SDClienteFONE1.Value);
      Pte2           := Geral.SoNumero_TT(SDClienteFONE2.Value);
      Pte3           := ''; // SDCliente.Value;
      PCel           := Geral.SoNumero_TT(SDClienteCELULAR.Value);
      PFax           := ''; // SDCliente.Value;
      PEmail         := SDClienteEMAILPRIMARIO.Value;
      PContato       := SDClienteCONTATO.Value;
      PNatal         := Geral.FDT(SDClienteANIVERSARIOABERTURA.Value, 1);

      PCidadeCod     := 0;
      ECidadeCod     := 0;
      PPaisCod       := 0;
      EPaisCod       := 0;

      ECodMunici     := 0;
      ECodiPais      := 0;
      if SDClienteCIDADE_IBGE.Value = EmptyStr then
        PCodMunici := 0
      else
        PCodMunici     := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_IBGE.Value));
      //
      if SDClienteCIDADE_PAIS.Value = EmptyStr then
        PCodiPais       := 0
      else
        PCodiPais       := Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADE_PAIS.Value));

    end;
    NIRE           := ''; // SDCliente.Value;
    FormaSociet    := ''; // SDCliente.Value;
    Simples        := 0; // SDCliente.Value;
    IEST           := ''; // SDCliente.Value;
    Atividade      := ''; // SDCliente.Value;
    //CPF            := Acima!!!
    CPF_Pai        := ''; // SDCliente.Value;
    CPF_Conjuge    := ''; // SDCliente.Value;
    //RG             := Acima
    SSP            := ''; // SDCliente.Value;
    DataRG         := ''; // SDCliente.Value;
    CidadeNatal    := ''; // SDClienteNATURALIDADE.Value;
    EstCivil       := 0; // SDCliente.Value;
    UFNatal        := 0; // SDCliente.Value;
    Nacionalid     := ''; // SDCliente.Value;
    Sexo           := ''; // SDCliente.Value;
    Responsavel    := ''; // SDCliente.Value;
    Profissao      := ''; // SDCliente.Value;
    Cargo          := ''; // SDCliente.Value;
    Recibo         := 0; // SDCliente.Value;
    DiaRecibo      := 0; // SDCliente.Value;
    AjudaEmpV      := 0; // SDCliente.Value;
    AjudaEmpP      := 0; // SDCliente.Value;
    //
    Cliente1       := 'F';
    Cliente2       := 'F';
    Cliente3       := 'F';
    Cliente4       := 'F';
    Fornece1       := 'F';
    Fornece2       := 'F';
    Fornece3       := 'F';
    Fornece4       := 'F';
    Fornece5       := 'F';
    Fornece6       := 'F';
    Fornece7       := 'F';
    Fornece8       := 'F';
    Terceiro       := 'F';
    case Integer(SDClienteTIPOCADASTRO.Value) of
      1: Cliente1       := 'V';
      2: Fornece1       := 'V';
      else MeErrosGerais.Lines.Add('Cliente ' + Geral.FF0(Codigo) + ' ' + Geral.FF0(Integer(SDClienteTIPOCADASTRO.Value)) + ' n�o implementado!!!');
    end;
    Cadastro       := Geral.FDT(SDClienteDATACADASTRO.Value, 1);
    Informacoes    := SDClienteINFORMACOES.Value;
    //Logo           := ''; // SDCliente.Value;
    Veiculo        := 0; // SDCliente.Value;
    Mensal         := ''; // SDCliente.Value;
    Observacoes    := SDClienteOBSERVACAO.Value;
    //Tipo           := Acima!!
    CLograd        := 0; //SDCliente.Value;
    CRua           := ''; //SDClienteENDERECOCOBRANCA.Value;
    CNumero        := 0; // SDCliente.Value;
    CCompl         := ''; //SDClienteCOMPLEMENTOCOBRANCA.Value;
    CBairro        := ''; //SDClienteBAIRROENDERECOCOBRANCA.Value;
    CCidade        := ''; //SDClienteCIDADC_DESCRICAO.Value;
    CUF            := 0; // SDCliente.Value;
    CCEP           := 0; //Geral.IMV(Geral.SoNumero_TT(SDClienteCEPENDERECOCOBRANCA.Value));
    CPais          := ''; // SDCliente.Value;
    if SDClienteCIDADC_IBGE.Value = EmptyStr then
      CCodMunici := 0
    else
      CCodMunici     := 0; //Geral.IMV(Geral.SoNumero_TT(SDClienteCIDADC_IBGE.Value));
    //
    CTel           := ''; // SDCliente.Value;
    CCel           := ''; // SDCliente.Value;
    CFax           := ''; // SDCliente.Value;
    CContato       := ''; // SDCliente.Value;
    LLograd        := 0; // SDCliente.Value;
    LRua           := ''; // SDCliente.Value;
    LNumero        := 0; // SDCliente.Value;
    LCompl         := ''; // SDCliente.Value;
    LBairro        := ''; // SDCliente.Value;
    LCidade        := ''; // SDCliente.Value;
    LUF            := 0; // SDCliente.Value;
    LCEP           := 0; // SDCliente.Value;
    LPais          := ''; // SDCliente.Value;
    LTel           := ''; // SDCliente.Value;
    LCel           := ''; // SDCliente.Value;
    LFax           := ''; // SDCliente.Value;
    LContato       := ''; // SDCliente.Value;
    Comissao       := 0.00; // SDCliente.Value;
    Situacao       := 0; // SDCliente.Value;
    Nivel          := ''; // SDCliente.Value;
    Grupo          := 0; // SDCliente.Value;
    Account        := 0; // SDCliente.Value;
    //Logo2          := ''; // SDCliente.Value;
    ConjugeNome    := ''; // SDCliente.Value;
    ConjugeNatal   := ''; // SDCliente.Value;
    Nome1          := ''; // SDCliente.Value;
    Natal1         := ''; // SDCliente.Value;
    Nome2          := ''; // SDCliente.Value;
    Natal2         := ''; // SDCliente.Value;
    Nome3          := ''; // SDCliente.Value;
    Natal3         := ''; // SDCliente.Value;
    Nome4          := ''; // SDCliente.Value;
    Natal4         := ''; // SDCliente.Value;
    CreditosI      := 0; // SDCliente.Value;
    CreditosL      := 0; // SDCliente.Value;
    CreditosF2     := 0;
    CreditosD      := ''; // SDCliente.Value;
    CreditosU      := Geral.FDT(SDClienteULTIMACOMPRA.Value, 1);
    CreditosV      := ''; // SDCliente.Value;
    Motivo         := 0; // SDCliente.Value;
    QuantI1        := 0; // SDCliente.Value;
    QuantI2        := 0; // SDCliente.Value;
    QuantI3        := 0; // SDCliente.Value;
    QuantI4        := 0; // SDCliente.Value;
    QuantN1        := 0.00; // SDCliente.Value;
    QuantN2        := 0.00; // SDCliente.Value;
    QuantN3        := 0.00; // SDCliente.Value;
    Agenda         := ''; // SDCliente.Value;
    SenhaQuer      := ''; // SDCliente.Value;
    Senha1         := ''; // SDCliente.Value;
    LimiCred       := Double(SDClienteCREDITO.Value);
    Desco          := 0; // SDCliente.Value;
    CasasApliDesco := 0; // SDCliente.Value;
    TempA          := 0.00; // SDCliente.Value;
    TempD          := 0.00; // SDCliente.Value;
    Banco          := 0; // SDCliente.Value;
    Agencia        := ''; // SDCliente.Value;
    ContaCorrente  := ''; // SDCliente.Value;
    FatorCompra    := 0.00; // SDCliente.Value;
    AdValorem      := 0.00; // SDCliente.Value;
    DMaisC         := 0; // SDCliente.Value;
    DMaisD         := 0; // SDCliente.Value;
    Empresa        := 0; // SDCliente.Value;
    CBE            := 0; // SDCliente.Value;
    SCB            := 0; // SDCliente.Value;
    PAtividad      := 0; // SDCliente.Value;
    EAtividad      := 0; // SDCliente.Value;
    Antigo         := ''; // SDCliente.Value;
    CUF2           := ''; // SDCliente.Value;
    Contab         := ''; // SDCliente.Value;
    MSN1           := ''; // SDCliente.Value;
    PastaTxtFTP    := ''; // SDCliente.Value;
    PastaPwdFTP    := ''; // SDCliente.Value;
    Protestar      := 0; // SDCliente.Value;
    MultaCodi      := 0; // SDCliente.Value;
    MultaDias      := 0; // SDCliente.Value;
    MultaValr      := 0.00; // SDCliente.Value;
    MultaPerc      := 0.00; // SDCliente.Value;
    MultaTiVe      := 0; // SDCliente.Value;
    JuroSacado     := 0.00; // SDCliente.Value;
    CPMF           := 0.00; // SDCliente.Value;
    Corrido        := 0; // SDCliente.Value;
    CPF_Resp1      := ''; // SDCliente.Value;
    CliInt         := 0; // SDCliente.Value;
    AltDtPlaCt     := ''; // SDCliente.Value;
    CartPref       := 0; // SDCliente.Value;
    RolComis       := 0; // SDCliente.Value;
    Filial         := 0; // SDCliente.Value;
    EEndeRef       := ''; // SDCliente.Value;
    PEndeRef       := ''; // SDCliente.Value;
    CEndeRef       := ''; // SDCliente.Value;
    LEndeRef       := ''; // SDCliente.Value;
    CNAE           := ''; // SDCliente.Value;
    SUFRAMA        := ''; // SDCliente.Value;
    L_CNPJ         := ''; // SDCliente.Value;
    L_Ativo        := 0; // SDCliente.Value;
    URL            := ''; // SDCliente.Value;
    CRT            := 0; // SDCliente.Value;
    COD_PART       := ''; // SDCliente.Value;
    ETe1Tip        := 0; // SDCliente.Value;
    ECelTip        := 0; // SDCliente.Value;
    PTe1Tip        := 0; // SDCliente.Value;
    PCelTip        := 0; // SDCliente.Value;
    ESite          := ''; // SDCliente.Value;
    PSite          := ''; // SDCliente.Value;
    EstrangDef     := 0; // SDCliente.Value;
    EstrangTip     := 0; // SDCliente.Value;
    EstrangNum     := ''; // SDCliente.Value;
    indIEDest      := 0; // SDCliente.Value;
    L_CPF          := ''; // SDCliente.Value;
    L_Nome         := ''; // SDCliente.Value;
    LCodiPais      := 0; // SDCliente.Value;
    LEmail         := SDClienteEMAILFINANCEIRO.Value;
    L_IE           := ''; // SDCliente.Value;
    // Campos Novos!!
    ListaStatus    := SDClienteLISTASTATUS.Value;
(* N�o possuem dados !!!!
    SDClienteLOCALTRABALHO: TStringField
    SDClienteENDERECOTRABALHO: TStringField
    SDClienteFONETRABALHO: TStringField
    SDClienteTEMPOTRABALHO: TStringField
    SDClienteFUNCAOTRABALHO: TStringField
    SDClienteRENDATRABALHO: TFMTBCDField
    SDClienteNOMECONJUGE: TStringField
    SDClienteLOCALTRABALHOCONJUGE: TStringField
    SDClienteENDERECOTRABALHOCONJUGE: TStringField
    SDClienteFONETRABALHOCONJUGE: TStringField
    SDClienteTEMPOTRABALHOCONJUGE: TStringField
    SDClienteFUNCAOTRABALHOCONJUGE: TStringField
    SDClienteRENDACONJUGE: TFMTBCDField
    SDClientePAI: TStringField
    SDClienteMAE: TStringField
    SDClienteREFERENCIA1: TStringField
    SDClienteREFERENCIAFONE_1: TStringField
    SDClienteREFERENCIA2: TStringField
    SDClienteREFERENCIAFONE_2: TStringField
    SDClienteREFERENCIA3: TStringField
    SDClienteREFERENCIAFONE_3: TStringField
    //
    SDClienteINFORMACOES: TStringField
    //
    SDClienteISENTO: TStringField
    SDClienteCONDICAO: TFMTBCDField
    SDClientePARCELAS: TFMTBCDField
    SDClienteINTERVALO: TFMTBCDField
    //
    SDClienteDATACADASTRO: TDateField
    SDClienteDATAATUALIZACAO: TDateField
    //
    SDClienteUSUARIOCADASTRO: TStringField
    SDClienteUSUARIOATUALIZACAO: TStringField
    SDClienteENDERECOENTREGA: TStringField
    SDClienteOBSERVACAOCLIENTE: TStringField
    SDClienteLIBERADODOCUMENTOVENCIDO: TStringField
    SDClienteOBSERVACAOVENDA: TStringField
    SDClienteDESCONTO: TFMTBCDField
    SDClienteVENDEDOR: TFMTBCDField
    SDClienteOBSERVACAOTRAVAMENTO: TStringField
    SDClienteTRAVAMENTO: TStringField
    SDClienteLIBERADOUMAFATURA: TStringField
    SDClienteATIVO: TStringField
    SDClienteORGAOPUBLICO: TStringField
    SDClienteNCM: TStringField
    SDClientePASTAXML: TStringField
    SDClienteDIACOBRANCA: TFMTBCDField
    SDClienteOBSERVACAODIACOBRANCA: TStringField
    //
    SDClienteNAOCONTRIBUINTE: TStringField
    SDClienteCONTRIBUINTEISENTO: TStringField
    //
    SDClienteFATURAR: TStringField
    SDClienteCOBRADOR: TFMTBCDField
    SDClienteCODCTC: TFMTBCDField
    SDClienteIMPRIMEPRECOLIQUIDO: TStringField
    SDClienteNUMEROPEDIDOITEM: TStringField
    SDClienteINSCRICAOMUNICIPAL: TStringField
    SDClienteSIMPLESNACIONAL: TStringField
    SDClienteALIQUOTAISS: TFMTBCDField
    SDClienteRETENCAOISS: TStringField

*)
    //
    if UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, SQLType, 'entidades', False, [
    'CodUsu', 'RazaoSocial', 'Fantasia',
    'Respons1', 'Respons2', 'Pai',
    'Mae', 'CNPJ', 'IE',
    'NIRE', 'FormaSociet', 'Simples',
    'IEST', 'Atividade', 'Nome',
    'Apelido', 'CPF', 'CPF_Pai',
    'CPF_Conjuge', 'RG', 'SSP',
    'DataRG', 'CidadeNatal', 'EstCivil',
    'UFNatal', 'Nacionalid', 'ELograd',
    'ERua', 'ENumero', 'ECompl',
    'EBairro', 'ECidade', 'EUF',
    'ECEP', 'EPais', 'ETe1',
    'Ete2', 'Ete3', 'ECel',
    'EFax', 'EEmail', 'EContato',
    'ENatal', 'PLograd', 'PRua',
    'PNumero', 'PCompl', 'PBairro',
    'PCidade', 'PUF', 'PCEP',
    'PPais', 'PTe1', 'Pte2',
    'Pte3', 'PCel', 'PFax',
    'PEmail', 'PContato', 'PNatal',
    'Sexo', 'Responsavel', 'Profissao',
    'Cargo', 'Recibo', 'DiaRecibo',
    'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
    'Cliente2', 'Cliente3', 'Cliente4',
    'Fornece1', 'Fornece2', 'Fornece3',
    'Fornece4', 'Fornece5', 'Fornece6',
    'Fornece7', 'Fornece8', 'Terceiro',
    'Cadastro', 'Informacoes', (*'Logo',*)
    'Veiculo', 'Mensal', 'Observacoes',
    'Tipo', 'CLograd', 'CRua',
    'CNumero', 'CCompl', 'CBairro',
    'CCidade', 'CUF', 'CCEP',
    'CPais', 'CTel', 'CCel',
    'CFax', 'CContato', 'LLograd',
    'LRua', 'LNumero', 'LCompl',
    'LBairro', 'LCidade', 'LUF',
    'LCEP', 'LPais', 'LTel',
    'LCel', 'LFax', 'LContato',
    'Comissao', 'Situacao', 'Nivel',
    'Grupo', 'Account', (*'Logo2',*)
    'ConjugeNome', 'ConjugeNatal', 'Nome1',
    'Natal1', 'Nome2', 'Natal2',
    'Nome3', 'Natal3', 'Nome4',
    'Natal4', 'CreditosI', 'CreditosL',
    'CreditosF2', 'CreditosD', 'CreditosU',
    'CreditosV', 'Motivo', 'QuantI1',
    'QuantI2', 'QuantI3', 'QuantI4',
    'QuantN1', 'QuantN2', 'QuantN3',
    'Agenda', 'SenhaQuer', 'Senha1',
    'LimiCred', 'Desco', 'CasasApliDesco',
    'TempA', 'TempD', 'Banco',
    'Agencia', 'ContaCorrente', 'FatorCompra',
    'AdValorem', 'DMaisC', 'DMaisD',
    'Empresa', 'CBE', 'SCB',
    'PAtividad', 'EAtividad', 'PCidadeCod',
    'ECidadeCod', 'PPaisCod', 'EPaisCod',
    'Antigo', 'CUF2', 'Contab',
    'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
    'Protestar', 'MultaCodi', 'MultaDias',
    'MultaValr', 'MultaPerc', 'MultaTiVe',
    'JuroSacado', 'CPMF', 'Corrido',
    'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
    'CartPref', 'RolComis', 'Filial',
    'EEndeRef', 'PEndeRef', 'CEndeRef',
    'LEndeRef', 'ECodMunici', 'PCodMunici',
    'CCodMunici', 'LCodMunici', 'CNAE',
    'SUFRAMA', 'ECodiPais', 'PCodiPais',
    'L_CNPJ', 'L_Ativo', 'URL',
    'CRT', 'COD_PART', 'ETe1Tip',
    'ECelTip', 'PTe1Tip', 'PCelTip',
    'ESite', 'PSite', 'EstrangDef',
    'EstrangTip', 'EstrangNum', 'indIEDest',
    'L_CPF', 'L_Nome', 'LCodiPais',
    'LEmail', 'L_IE', 'ListaStatus'], [
    'Codigo'], [
    CodUsu, RazaoSocial, Fantasia,
    Respons1, Respons2, Pai,
    Mae, CNPJ, IE,
    NIRE, FormaSociet, Simples,
    IEST, Atividade, Nome,
    Apelido, CPF, CPF_Pai,
    CPF_Conjuge, RG, SSP,
    DataRG, CidadeNatal, EstCivil,
    UFNatal, Nacionalid, ELograd,
    ERua, ENumero, ECompl,
    EBairro, ECidade, EUF,
    ECEP, EPais, ETe1,
    Ete2, Ete3, ECel,
    EFax, EEmail, EContato,
    ENatal, PLograd, PRua,
    PNumero, PCompl, PBairro,
    PCidade, PUF, PCEP,
    PPais, PTe1, Pte2,
    Pte3, PCel, PFax,
    PEmail, PContato, PNatal,
    Sexo, Responsavel, Profissao,
    Cargo, Recibo, DiaRecibo,
    AjudaEmpV, AjudaEmpP, Cliente1,
    Cliente2, Cliente3, Cliente4,
    Fornece1, Fornece2, Fornece3,
    Fornece4, Fornece5, Fornece6,
    Fornece7, Fornece8, Terceiro,
    Cadastro, Informacoes, (*Logo,*)
    Veiculo, Mensal, Observacoes,
    Tipo, CLograd, CRua,
    CNumero, CCompl, CBairro,
    CCidade, CUF, CCEP,
    CPais, CTel, CCel,
    CFax, CContato, LLograd,
    LRua, LNumero, LCompl,
    LBairro, LCidade, LUF,
    LCEP, LPais, LTel,
    LCel, LFax, LContato,
    Comissao, Situacao, Nivel,
    Grupo, Account, (*Logo2,*)
    ConjugeNome, ConjugeNatal, Nome1,
    Natal1, Nome2, Natal2,
    Nome3, Natal3, Nome4,
    Natal4, CreditosI, CreditosL,
    CreditosF2, CreditosD, CreditosU,
    CreditosV, Motivo, QuantI1,
    QuantI2, QuantI3, QuantI4,
    QuantN1, QuantN2, QuantN3,
    Agenda, SenhaQuer, Senha1,
    LimiCred, Desco, CasasApliDesco,
    TempA, TempD, Banco,
    Agencia, ContaCorrente, FatorCompra,
    AdValorem, DMaisC, DMaisD,
    Empresa, CBE, SCB,
    PAtividad, EAtividad, PCidadeCod,
    ECidadeCod, PPaisCod, EPaisCod,
    Antigo, CUF2, Contab,
    MSN1, PastaTxtFTP, PastaPwdFTP,
    Protestar, MultaCodi, MultaDias,
    MultaValr, MultaPerc, MultaTiVe,
    JuroSacado, CPMF, Corrido,
    CPF_Resp1, CliInt, AltDtPlaCt,
    CartPref, RolComis, Filial,
    EEndeRef, PEndeRef, CEndeRef,
    LEndeRef, ECodMunici, PCodMunici,
    CCodMunici, LCodMunici, CNAE,
    SUFRAMA, ECodiPais, PCodiPais,
    L_CNPJ, L_Ativo, URL,
    CRT, COD_PART, ETe1Tip,
    ECelTip, PTe1Tip, PCelTip,
    ESite, PSite, EstrangDef,
    EstrangTip, EstrangNum, indIEDest,
    L_CPF, L_Nome, LCodiPais,
    LEmail, L_IE, ListaStatus], [
    Codigo], True) then
    begin
    // Parei aqui! Fazer!
    end;
    //
    SDCliente.Next;
  end;
  MePronto.Text := 'Tabela ' + Tabela + ': ' + IntToStr(ItemAtual) +
    ' registros verificados!' + sLineBreak + MePronto.Text;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_ClienteEmail();
var
  EMail: String;
  Codigo, Controle, Conta, EntiTipCto, Ordem, DiarioAdd: Integer;
  SQLType: TSQLType;
  //
  Nome: String;
  Total: Integer;
const
  OriTab = 'ClienteEmail';
  DstTab = 'EntiMail';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, FbDB, [
  'SELECT COUNT(*) FROM ' + OriTab
  ]);
  //
  Total := QrAux.Fields[0].AsInteger;
  PB2.Position := 0;
  PB2.Max := Total;
  //
  UnDmkDAC_PF.AbreQuery(QrClienteEmail, FbDB);
  QrClienteEmail.First;
  Conta          := 0;
  SQLType        := stIns;
    //
  while not QrClienteEmail.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB2, LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(QrClienteEmail.RecNo) + ' de ' + Geral.FF0(Total));
    //
    Codigo         := QrClienteEmailCLIENTE.Value;
    Controle       := 0;
    Nome           := QrClienteEmailCONTATO.Value;
    Email          := QrClienteEmailEMAIL.Value;
    EntiTipCto     := 3;
    Ordem          := 0;
    DiarioAdd      := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux1, Dmod.MyDB, [
    'SELECT Email ',
    'FROM entimail ',
    'WHERE Email="' + Email + '"',
    'AND Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrAux1.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM enticontat ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Nome="' + Nome + '" ',
      'ORDER BY Controle DESC',
      '']);
      //Geral.MB_Teste(QrAux.SQL.Text);
      //
      if QrAux.RecordCount > 0 then
        Controle := QrAux.FieldByName('Controle').AsInteger;
      if Controle = 0 then
        Controle := AdicionaEntiContat(Codigo, Controle, Nome);

      Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', SQLType, 0);

      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entimail', False, [
      'Codigo', 'Controle', 'EMail',
      'EntiTipCto', 'Ordem', 'DiarioAdd'], [
      'Conta'], [
      Codigo, Controle, EMail,
      EntiTipCto, Ordem, DiarioAdd], [
      Conta], True) then
      begin
         //
      end;
      //
    end;
    QrClienteEmail.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_ClienteFone();
var
  Telefone(*, Ramal*): String;
  Codigo, Controle, Conta(*, EntiTipCto, DiarioAdd*): Integer;
  SQLType: TSQLType;
  //
  Nome: String;
  Total: Integer;
const
  OriTab = 'ClienteFone';
  DstTab = 'EntiTel';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, FbDB, [
  'SELECT COUNT(*) FROM ' + OriTab
  ]);
  //
  Total := QrAux.Fields[0].AsInteger;
  PB2.Position := 0;
  PB2.Max := Total;
  //
  UnDmkDAC_PF.AbreQuery(QrClienteFone, FbDB);
  QrClienteFone.First;
  Conta          := 0;
(*
  EntiTipCto     := 0;
  Ramal          := '';
  DiarioAdd      := 0;
*)
  SQLType        := stIns;
  while not QrClienteFone.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB2, LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(QrClienteFone.RecNo) + ' de ' + Geral.FF0(Total));
    //
    Codigo         := QrClienteFoneCLIENTE.Value;
    Controle       := 0;
    Nome           := QrClienteFoneCONTATO.Value;
    Telefone       := Geral.SoNumero_TT(QrClienteFoneTELEFONE.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAux1, Dmod.MyDB, [
    'SELECT Conta ',
    'FROM entitel ',
    'WHERE Telefone="' + Telefone + '"',
    'AND Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrAux1.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM enticontat ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Nome="' + Nome + '" ',
      'ORDER BY Controle DESC',
      '']);
      //Geral.MB_Teste(QrAux.SQL.Text);
      //
      if QrAux.RecordCount > 0 then
        Controle := QrAux.FieldByName('Controle').AsInteger;
      if Controle = 0 then
        Controle := AdicionaEntiContat(Codigo, Controle, Nome);

      Conta := UMyMod.BuscaEmLivreY_Def('entitel', 'Conta', SQLType, 0);

      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'entitel', False, [
      'Codigo', 'Controle', 'Telefone'(*,
      'EntiTipCto', 'Ramal', 'DiarioAdd'*)], [
      'Conta'], [
      Codigo, Controle, Telefone(*,
      EntiTipCto, Ramal, DiarioAdd*)], [
      Conta], True) then
      begin
         //
      end;
    end;
    //
    QrClienteFone.Next;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_ClienteStatus;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, Status: Integer;
const
  OriTab = 'ClienteStatus';
  DstTab = 'EntiStatus';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Status) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDClienteStatus.Close;
    SDClienteStatus.DataSet.CommandType := ctQuery;
    SDClienteStatus.DataSet.CommandText := SQL;
    SDClienteStatus.Open;
    Continua := SDClienteStatus.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDClienteStatus.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDClienteStatus.First;
      SQLRec    := '';
      while not SDClienteStatus.Eof do
      begin
        Codigo := Integer(SDClienteStatusCLIENTE.Value);
        Status := Integer(SDClienteStatusSTATUS.Value);
        //
        if SDClienteStatus.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Empresa,
        + PV_N(Geral.FF0(Status))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDClienteStatus.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_FornecedorProduto;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec, Observacao: String;
  ItsOK, Total, Step, Embalagem, CFOP_Inn: Integer;
  Continua: Boolean;
  //
  Nivel1, GraGruX, Fornece, CFOP: Integer;
  cProd, xProd, NCM, uCom: String;
const
  OriTab = 'FornecedorProduto';
  DstTab = 'gragrueits';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'cProd, xProd, Fornece, NCM, CFOP, uCom, Nivel1, GraGruX, ',
    'Embalagem, Observacao, CFOP_Inn) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDFornecedorProduto.Close;
    SDFornecedorProduto.DataSet.CommandType := ctQuery;
    SDFornecedorProduto.DataSet.CommandText := SQL;
    SDFornecedorProduto.Open;
    Continua := SDFornecedorProduto.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDFornecedorProduto.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDFornecedorProduto.First;
      SQLRec    := '';
      while not SDFornecedorProduto.Eof do
      begin
        cProd      := SDFornecedorProdutoCODIGOFORNECEDOR.Value;
        xProd      := '';
        Fornece    := Integer(SDFornecedorProdutoFORNECEDOR.Value);
        NCM        := '';
        CFOP       := 0;
        uCom       := '';
        Nivel1     := Integer(SDFornecedorProdutoPRODUTO.Value);
        GraGruX    := Integer(SDFornecedorProdutoPRODUTO.Value);
        Embalagem  := 0;
        Observacao := '';
        CFOP_Inn   := 0;
        //
        //'Embalagem, Observacao, CFOP_Inn) VALUES ']);
        //
        if SDFornecedorProduto.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //cProd
        + Geral.VariavelToString(cProd)
        //xProd,
        + PV_N(Geral.VariavelToString(xProd))
        //Fornece,
        + PV_N(Geral.FF0(Fornece))
        //NCM,
        + PV_N(Geral.VariavelToString(NCM))
        //CFOP,
        + PV_N(Geral.FF0(CFOP))
        //uCom,
        + PV_N(Geral.VariavelToString(uCom))
        //Nivel1,
        + PV_N(Geral.FF0(Nivel1))
        //GraGruX, ',
        + PV_N(Geral.FF0(GraGruX))
        //'Embalagem,
        + PV_N(Geral.FF0(Embalagem))
        //Observacao,
        + PV_N(Geral.VariavelToString(Observacao))
        //CFOP_Inn
        + PV_N(Geral.FF0(CFOP_Inn))
        //+ ...
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDFornecedorProduto.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Gerencia();
var
  Continua: Boolean;
begin
  SDGerencia.Close;
  SDGerencia.DataSet.CommandType := ctQuery;
  SDGerencia.DataSet.CommandText := 'SELECT * FROM gerencia';
  SDGerencia.Open;
  Continua := SDGerencia.RecordCount > 0;
  if Continua then
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM entidades WHERE Codigo=-11');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'INSERT INTO entidades SET ' +
    ' Codigo=-11, CodUsu=-11, Tipo=0, Filial=1 ' +
    ', RazaoSocial="' + SDGerenciaRAZAOSOCIAL.Value + '" ' +
    ', Fantasia="' + SDGerenciaFANTASIA.Value + '" ' +
    ', CNPJ="' + Geral.SoNumero_TT(SDGerenciaCNPJ.Value) + '"' +
    ', IE="' + Geral.SoNumero_TT(SDGerenciaINSCRICAOESTADUAL.Value) + '"' +
    ', ERua=' + Geral.VariavelToString(SDGerenciaENDERECO.Value) +
    ', ECompl=' + Geral.VariavelToString(SDGerenciaCOMPLEMENTO.Value) +
    ', ENumero=' + Geral.FF0(Integer(SDGerenciaNUMERO.Value)) +
    ', EBairro=' + Geral.VariavelToString(SDGerenciaBAIRRO.Value) +
    ', ECEP=' + Geral.SoNumero_TT(SDGerenciaCEP.Value) +
    ', EUF=-18' +
    ', ETe1="' + Geral.SoNumero_TT(SDGerenciaFONE.Value) + '"' +
    ', EEmail="' + SDGerenciaEMAILFTPLOJA.Value + '"' +
    ', ESite="' + SDGerenciaSITE.Value + '"' +
    ', Cliente1="V"' +
    ', Fornece1="V"' +
    ', Fornece2="V"' +
    ', Fornece3="V"' +
    ', Terceiro="V"' +
    ', ECodMunici=4115200' +
    ', ECodiPais=1058' +
    '');
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM master ');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'INSERT INTO master SET ' +
    '  Em="' + SDGerenciaRAZAOSOCIAL.Value + '" ' +
    ', CNPJ="' + Geral.SoNumero_TT(SDGerenciaCNPJ.Value) + '"' +
    ', Monitorar=1' +
    ', DataF="' + Geral.FDT(Now() + 365, 1) + '" ' +
    '');
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM controle ');
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'INSERT INTO Controle SET ' +
    '  Codigo=1' +
    ', Dono=-11' +
    ', CNPJ="' + Geral.SoNumero_TT(SDGerenciaCNPJ.Value) + '"' +
    '');
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Grupo;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, CodUsu, NivCad: Integer;
  Nome: String;
const
  OriTab = 'Grupo';
  DstTab = 'prdgruptip';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, COdUsu, Nome, NivCad) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDGrupo.Close;
    SDGrupo.DataSet.CommandType := ctQuery;
    SDGrupo.DataSet.CommandText := SQL;
    SDGrupo.Open;
    Continua := SDGrupo.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDGrupo.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDGrupo.First;
      SQLRec    := '';
      while not SDGrupo.Eof do
      begin
        Codigo := Integer(SDGrupoGRUPO.Value);
        CodUsu := Codigo;
        Nome   := SDGrupoDESCRICAO.Value;
        NivCad := 2;
        //
        if SDGrupo.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //CodUsu,
        + PV_N(Geral.FF0(CodUsu))
        //Nome,
        + PV_N(Geral.VariavelToString(Nome))
        //NivCad
        + PV_N(Geral.FF0(NivCad))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDGrupo.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDGrupo.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Lancamento();
const
  sProcName = 'TFmDB_Converte_Tisolin.ImportaTabelaFB_Lancamento()';
  //
  procedure VerificaNomeCarteira(Codigo: Integer; Nome: String; Tipo, Banco:
    Integer);
  var
    SQLType: TSQLType;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome FROM carteiras WHERE Codigo=' + Geral.FF0(Codigo)]);
    if (QrCarteiras.RecordCount > 0) and (QrCarteirasCodigo.Value = Codigo) then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'carteiras', False, [
    'Tipo', 'Nome', 'Nome2',
    'ForneceI', 'Banco'], [
    'Codigo'], [
    Tipo, Nome, Nome,
    -11, Banco], [
    Codigo], True);
  end;
  procedure VerificaNomeConta(Codigo: Integer; Nome: String; Subgrupo: Integer;
    Credito, Debito: String);
  var
    SQLType: TSQLType;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCarteiras, Dmod.MyDB, [
    'SELECT Codigo, Nome FROM contas WHERE Codigo=' + Geral.FF0(Codigo)]);
    if (QrCarteiras.RecordCount > 0) and (QrCarteirasCodigo.Value = Codigo) then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'contas', False, [
    'Nome', 'Nome2', 'Nome3',
    'Subgrupo', 'Credito', 'Debito'], [
    'Codigo'], [
    Nome, Nome, Nome,
    Subgrupo, Credito, Debito], [
    Codigo], True);
  end;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Controle, Sit, Carteira, Tipo, ErrCtrl, IncCtrl, ID_Pgto, Controle2, Genero,
  CartBco: Integer;
  //Nome: String;
  Credito, Debito: Double;
const
  OriTab = 'lancamento';
  DstTab = 'lct0001a';
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFbAux, FbDB, [
  'SELECT MAX(Autorizacao) Autorizacao ',
  'FROM lancamento ',
  '']);
  IncCtrl := QrFbAux.FieldByName('Autorizacao').AsInteger;
  //
(*
////////////////////////////////////////////////////////////////////////////////
Desmarcado ap�s a Karolina da Tisolin j� ter criado as contas e carteiras
////////////////////////////////////////////////////////////////////////////////
  VerificaNomeCarteira(-101, 'Venda � vista', 0, 0);
  VerificaNomeCarteira(-102, 'Venda � Prazo', 2, 4);
  VerificaNomeCarteira(-103, 'Cheque Pr�', 2, 4);
  VerificaNomeCarteira(-104, 'Banco Provis�rio', 1, 0);
  VerificaNomeCarteira(-105, 'Adiantamento Cliente', 2, 4);
  VerificaNomeCarteira(-106, 'Compras', 2, 4);
  VerificaNomeCarteira(-107, 'Servi�os', 2, 4);
  VerificaNomeCarteira(-108, 'Boletos', 2, 4);
  VerificaNomeCarteira(-109, 'Carteira', 2, 4);
  VerificaNomeCarteira(-110, 'Dep�sito', 2, 4);
  VerificaNomeCarteira(-111, 'Sicoob', 2, 4);
  //
  VerificaNomeConta(-101, 'VENDA A VISTA', 1, 'V', 'F');
  VerificaNomeConta(-102, 'VENDA A PRAZO', 1, 'V', 'F');
  VerificaNomeConta(-103, 'DESCONTO DE CLIENTE', 1, 'V', 'F');
  VerificaNomeConta(-104, 'JURO NO CAIXA', 1, 'V', 'F');
  VerificaNomeConta(-105, 'DESPESA DE CART�RIO', 2, 'F', 'V');
  VerificaNomeConta(-106, 'BAIXA COMO DUPLICATA NO CAIXA', 1, 'V', 'F');
  VerificaNomeConta(-107, 'BAIXA COM CARTEIRA NO CAIXA', 1, 'V', 'F');
  VerificaNomeConta(-108, 'CHEQUE PR�', 1, 'V', 'F');
  VerificaNomeConta(-109, 'DEVOLU��O', 2, 'F', 'V');
  VerificaNomeConta(-110, 'TROCO D�BITO', 2, 'F', 'V');
  VerificaNomeConta(-111, 'TROCO CR�DITO', 1, 'V', 'F');
  VerificaNomeConta(-112, 'CART�O', 1, 'V', 'F');
  VerificaNomeConta(-113, 'COMPRAS', 2, 'F', 'V');
  VerificaNomeConta(-114, 'JUROS DE FORNECEDOR', 2, 'F', 'V');
  VerificaNomeConta(-115, 'DESCONTO DE FORNECEDOR', 1, 'V', 'F');
  VerificaNomeConta(-116, 'SERVI�O', 1, 'V', 'F');
  VerificaNomeConta(-117, 'RETEN��O', 1, 'V', 'F');
  VerificaNomeConta(-118, 'PROPOSTA COMERCIAL', 1, 'V', 'F');
  VerificaNomeConta(-119, 'LOCA��O A VISTA', 1, 'V', 'F');
  VerificaNomeConta(-120, 'LOCA��O A PRAZO', 1, 'V', 'F');
  VerificaNomeConta(-165, 'CORRE��O DE VALOR LAN�ADO INDEVIDO', 1, 'V', 'F');
*)
  //
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    //AUTORIZACAO,  QUITADO DOCUMENTO        MODO
    'Controle, Sub, Sit, Duplicata, Data, DataDoc, Cliente, Fatura, Vencimento, ' +
    //                             ORCAMENTO HISTORICO PAGAMENTO
    'Credito, Debito, Descricao, Vendedor, Antigo, Genero, Compensado, ' +
    //                                      RECIBO       DUPLICATA
    'VctoOriginal, Tipo, Carteira, UserCad, Autorizacao, Documento, ' +
    // DATA   HORA     NOTACOMPRA  MODELOCOMPRA SERIECOMPRA  LOCACAO
    'DataCad, HoraCad, NotaFiscal, ModeloNF,    SerieNF,     FatParcRef, ' +
    'CliInt, ErrCtrl, ID_Pgto) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDLancamento.Close;
    SDLancamento.DataSet.CommandType := ctQuery;
    SDLancamento.DataSet.CommandText := SQL;
    SDLancamento.Open;
    Continua := SDLancamento.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLancamento.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLancamento.First;
      SQLRec    := '';
      while not SDLancamento.Eof do
      begin
        Controle := Integer(SDLancamentoAUTORIZACAO.Value);
        ErrCtrl  := 0;
        if SDLancamentoQUITADO.Value = 'N' then
          Sit := 0
        else
        if SDLancamentoQUITADO.Value = 'S' then
        begin
          Sit := 3;
          ErrCtrl := -99999999; // para dizer que n�o tem ID_Pgto!
        end else
        begin
          Sit := -1;
          Geral.MB_Erro(
          'N�o foi poss�vel definir a situa��o do lan�amento financeiro ' +
          Geral.FF0(Controle));
        end;
        Carteira := Integer(SDLancamentoCOBRADOR.Value);
        case Carteira of
          1: Tipo := 0; //, 'Venda � vista', 0, 0);
          2: Tipo := 2; //, 'Venda � Prazo', 2, 4;
          3: Tipo := 2; //, 'Cheque Pr�', 2, 4;
          4: Tipo := 1;
          5: Tipo := 2; //, 'Adiantamento Cliente', 2, 4;
          6: Tipo := 2; //, 'Compras', 2, 4;
          7: Tipo := 2; //, 'Servi�os', 2, 4;
          8: Tipo := 2; //, 'Boletos', 2, 4;
          9: Tipo := 2; //, 'Carteira', 2, 4;
          10: Tipo := 2; //, 'Dep�sito', 2, 4;
          11: Tipo := 2; //, 'Sicoob', 1, 0);
          else Tipo := 0;
        end;
        //
        Carteira := -(Carteira + 100);
        CartBco := -104;  // Banco provis�rio
        Genero := -(Integer(SDLancamentoHISTORICO.Value) + 100);
        Credito := 0;
        Debito  := 0;
        if Uppercase(SDLancamentoMODO.Value) = 'R' then // Receber?
          Credito := SDLancamento.FieldByName('VALOR').AsExtended
        else
        if Uppercase(SDLancamentoMODO.Value) = 'P' then // Pagar?
          Debito  := SDLancamento.FieldByName('VALOR').AsExtended
        else
        begin
          Debito  := SDLancamento.FieldByName('VALOR').AsExtended;
          Geral.MB_Aviso('MODO = ' + SDLancamentoMODO.Value +
          ' n�o implementado em ' + sProcName);
        end;
        //
        if (Tipo=2) and (SDLancamentoPAGAMENTO.Value > 2) then
          ID_Pgto := Controle + IncCtrl
        else
          ID_Pgto := 0;
        //
        if SDLancamento.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec

//AUTORIZACAO,  QUITADO DOCUMENTO        MODO
//'Controle, Sub, Sit, Duplicata, Cliente, Fatura, Vencimento, ' +
        //Controle,
        +     (Geral.FF0(Controle))
        //Sub
        + PV_N('0')
        //Sit
        + PV_N(Geral.FF0(Sit))
        //Duplicata
        + PV_N(Geral.VariavelToString(SDLancamentoDOCUMENTO.Value))
        //Data
        + PV_A(Geral.FDT(SDLancamentoEMISSAO.Value, 1))
        //DataDoc
        + PV_A(Geral.FDT(SDLancamentoEMISSAO.Value, 1))
        //Cliente
        + PV_N(Geral.FF0(Integer(SDLancamentoCLIENTE.Value)))
        //Fatura
        + PV_A(SDLancamentoMODO.Value)
        //Vencimento
        + PV_A(Geral.FDT(SDLancamentoVENCIMENTO.Value, 1))
//                             ORCAMENTO HISTORICO PAGAMENTO
//'Credito, Descricao, Vendedor, Antigo, Genero, Compensado, ' +
        //Credito
        + PV_N(Geral.FFT_Dot(Credito, 2, siNegativo))
        //Debito
        + PV_N(Geral.FFT_Dot(Debito, 2, siNegativo))
        //Descricao
        + PV_N(Geral.VariavelToString(Copy(SDLancamentoDESCRICAO.Value, 1, 100)))
        //Vendedor
        + PV_N(Geral.FF0(Integer(SDLancamentoVENDEDOR.Value)))
        //Antigo
        + PV_A(Geral.FF0(Integer(SDLancamentoORCAMENTO.Value)))
        //Genero
        + PV_N(Geral.FF0(Genero))
        //Compensado
        + PV_A(Geral.FDT(SDLancamentoPAGAMENTO.Value, 1))
//'VctoOriginal, Tipo, Carteira, UserCad, Autorizacao, Documento ' +
// DATA   HORA     NOTACOMPRA  MODELOCOMPRA SERIECOMPRA  LOCACAO
        //VctoOriginal,
        + PV_A(Geral.FDT(SDLancamentoVENCIMENTOORIGINAL.Value, 1))
        //Tipo,
        + PV_N(Geral.FF0(Tipo))
        //Carteira,
        + PV_N(Geral.FF0(Carteira))
        //UserCad,
        + PV_N(Geral.FF0(Integer(SDLancamentoVENDEDOR.Value)))
        //Autorizacao,
        + PV_N(Geral.FF0(Integer(SDLancamentoRECIBO.Value)))
        //Documento
        + PV_N(Geral.FF0(Integer(SDLancamentoDUPLICATA.Value)))
//'DataCad, HoraCad, NotaFiscal, ModeloNF,    SerieNF,     LctFatRef ' +
        //DataCad
        + PV_A(Geral.FDT(SDLancamentoDATA.Value, 1))
        //HoraCad,
        + PV_A(Geral.FDT(SDLancamentoHORA.Value, 100))
        //NotaFiscal
        + PV_N(Geral.FF0(Integer(SDLancamentoNOTACOMPRA.Value)))
        //ModeloNF,
        + PV_N(Geral.FF0(Integer(SDLancamentoMODELOCOMPRA.Value)))
        //SerieNF,
        + PV_N(Geral.VariavelToString(Copy(SDLancamentoSERIECOMPRA.Value, 1, 3)))
        //FatParcRef
        + PV_N(Geral.FF0(Integer(SDLancamentoLOCACAO.Value)))
//'CliInt) VALUES ']);
        + PV_N(Geral.FF0(-11))
        // ErrCtrl
        + PV_N(Geral.FF0(ErrCtrl))
        // ID_Pgto
        + PV_N(Geral.FF0(ID_Pgto))
        //

        + ') ';
        //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
/////////////////////////////// ini 2021-01-16 /////////////////////////////////
        if (Tipo=2) and (SDLancamentoPAGAMENTO.Value > 2) then
        begin
          Controle2 := ID_Pgto;
          ID_Pgto := Controle;
          SQLRec    := ', (';
          //
          SQLRec := SQLRec

  //AUTORIZACAO,  QUITADO DOCUMENTO        MODO
  //'Controle, Sub, Sit, Duplicata, Cliente, Fatura, Vencimento, ' +
          //Controle,
          +     (Geral.FF0(Controle2))
          //Sub
          + PV_N('0')
          //Sit
          + PV_N(Geral.FF0(3))
          //Duplicata
          + PV_N(Geral.VariavelToString(SDLancamentoDOCUMENTO.Value))
          //Data
          + PV_A(Geral.FDT(SDLancamentoPAGAMENTO.Value, 1))
          //DataDoc
          + PV_A(Geral.FDT(SDLancamentoEMISSAO.Value, 1))
          //Cliente
          + PV_N(Geral.FF0(Integer(SDLancamentoCLIENTE.Value)))
          //Fatura
          + PV_A(SDLancamentoMODO.Value)
          //Vencimento
          + PV_A(Geral.FDT(SDLancamentoVENCIMENTO.Value, 1))
  //                             ORCAMENTO HISTORICO PAGAMENTO
  //'Credito, Descricao, Vendedor, Antigo, Genero, Compensado, ' +
          //Credito
          + PV_N(Geral.FFT_Dot(Credito, 2, siNegativo))
          //Debito
          + PV_N(Geral.FFT_Dot(Debito, 2, siNegativo))
          //Descricao
          + PV_N(Geral.VariavelToString(Copy(SDLancamentoDESCRICAO.Value, 1, 100)))
          //Vendedor
          + PV_N(Geral.FF0(Integer(SDLancamentoVENDEDOR.Value)))
          //Antigo
          + PV_A(Geral.FF0(Integer(SDLancamentoORCAMENTO.Value)))
          //Genero
          + PV_N(Geral.FF0(Integer(SDLancamentoHISTORICO.Value)))
          //Compensado
          + PV_A(Geral.FDT(SDLancamentoPAGAMENTO.Value, 1))
  //'VctoOriginal, Tipo, Carteira, UserCad, Autorizacao, Documento ' +
  // DATA   HORA     NOTACOMPRA  MODELOCOMPRA SERIECOMPRA  LOCACAO
          //VctoOriginal,
          + PV_A(Geral.FDT(SDLancamentoVENCIMENTOORIGINAL.Value, 1))
          //Tipo,
          + PV_N(Geral.FF0(1))
          //Carteira,
          + PV_N(Geral.FF0(CartBco))
          //UserCad,
          + PV_N(Geral.FF0(Integer(SDLancamentoVENDEDOR.Value)))
          //Autorizacao,
          + PV_N(Geral.FF0(Integer(SDLancamentoRECIBO.Value)))
          //Documento
          + PV_N(Geral.FF0(Integer(SDLancamentoDUPLICATA.Value)))
  //'DataCad, HoraCad, NotaFiscal, ModeloNF,    SerieNF,     LctFatRef ' +
          //DataCad
          + PV_A(Geral.FDT(SDLancamentoDATA.Value, 1))
          //HoraCad,
          + PV_A(Geral.FDT(SDLancamentoHORA.Value, 100))
          //NotaFiscal
          + PV_N(Geral.FF0(Integer(SDLancamentoNOTACOMPRA.Value)))
          //ModeloNF,
          + PV_N(Geral.FF0(Integer(SDLancamentoMODELOCOMPRA.Value)))
          //SerieNF,
          + PV_N(Geral.VariavelToString(Copy(SDLancamentoSERIECOMPRA.Value, 1, 3)))
          //FatParcRef
          + PV_N(Geral.FF0(Integer(SDLancamentoLOCACAO.Value)))
  //'CliInt) VALUES ']);
          + PV_N(Geral.FF0(-11))
          // ErrCtrl
          + PV_N(Geral.FF0(ErrCtrl))
          // ID_Pgto
          + PV_N(Geral.FF0(ID_Pgto))
          //
          + ') ';
          //
          SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        end;
/////////////////////////////// fim 2021-01-16 /////////////////////////////////
        SDLancamento.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLancamento.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));

  Dmod.MyDB.Execute('UPDATE lct0001a SET Carteira=5, Tipo=2 WHERE Carteira IN (-105,-101,-108,-111); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da carteira 5 definidos!');

  Dmod.MyDB.Execute('UPDATE lct0001a SET Carteira=21, Tipo=2 WHERE Carteira IN (-103,-109); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da carteira 21 definidos!');

  Dmod.MyDB.Execute('UPDATE lct0001a SET Carteira=7, Tipo=2 WHERE Carteira IN (-110); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da carteira 7 definidos!');

  Dmod.MyDB.Execute('UPDATE lct0001a SET Carteira=23, Tipo=1 WHERE Carteira IN (-104); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da carteira 23 definidos!');

  Dmod.MyDB.Execute('UPDATE lct0001a SET Carteira=24, Tipo=2 WHERE Carteira IN (-102,-107); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da carteira 24 definidos!');

  Dmod.MyDB.Execute('DELETE FROM lct0001a WHERE Carteira=-106; ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da carteira 106 exclu�dos!');

  //

  Dmod.MyDB.Execute('UPDATE lct0001a SET Genero=25 WHERE Genero IN (-120,-119,-116,-109,-111); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da da conta 25 definidos!');

  Dmod.MyDB.Execute('UPDATE lct0001a SET Genero=51 WHERE Genero IN (-101,-102); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos da da conta 51 definidos!');

  Dmod.MyDB.Execute('DELETE FROM lct0001a WHERE Genero IN (-103,-104,-113,-165); ');
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Lan�amentos das contas 3, 4, 13 e 65 exclu�dos!');

  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Financeiro convertido!');
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Linha();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu, //ContaCTB,
  PrdGrupTip, Tipo_Item: Integer;
  Nome: String;
const
  OriTab = 'Linha';
  DstTab = 'gragru2';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Nivel5, Nivel4, Nivel3, Nivel2, CodUsu, Nome, ', //ContaCTB,
    'PrdGrupTip, Tipo_Item) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + Geral.ATS([' lin.Descricao, prd.Grupo, ',
    'prd.Linha, COUNT(prd.Produto) ITENS ',
    'FROM produto prd ',
    'LEFT JOIN linha lin ON (lin.Linha=prd.Linha) ',
    'GROUP BY prd.Linha, prd.Grupo, lin.Descricao ',
    'ORDER BY prd.Linha, prd.Grupo ',
    '']);
    //SQL := SQL + ' FROM ' + OriTab;
    SDLinha.Close;
    SDLinha.DataSet.CommandType := ctQuery;
    SDLinha.DataSet.CommandText := SQL;
    SDLinha.Open;
    Continua := SDLinha.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLinha.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLinha.First;
      SQLRec    := '';
      while not SDLinha.Eof do
      begin
        Nivel5      := 0;
        Nivel4      := 0;
        Nivel3      := 0;
        Nivel2      := GetNivel2(Integer(SDLinhaGRUPO.Value), Geral.IMV(SDLinhaLinha.Value));
        CodUsu      := Nivel2;
        Nome        := Copy(SDLinhaDESCRICAO.Value, 1, 30);
        //ContaCTB,
        PrdGrupTip  := Integer(SDLinhaGRUPO.Value);
        Tipo_Item   := 1;
        //
        if SDLinha.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Nivel5,
        + Geral.FF0(Nivel5)
        //Nivel4,
        + PV_N(Geral.FF0(Nivel4))
        //Nivel3,
        + PV_N(Geral.FF0(Nivel3))
        //Nivel2,
        + PV_N(Geral.FF0(Nivel2))
        //CodUsu,
        + PV_N(Geral.FF0(CodUsu))
        //Nome,
        + PV_N(Geral.VariavelToString(Nome))
        //PrdGrupTip,
        + PV_N(Geral.FF0(PrdGrupTip))
        //Tipo_Item
        + PV_N(Geral.FF0(Tipo_Item))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLinha.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLinha.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Locacao();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step, Codigo, JaLocou, Status: Integer;
  Continua: Boolean;

const
  OriTab = 'Locacao';
  DstTab = 'loccconcab';
  FormaCobrLoca = 2;  // Na Loca��o!
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Empresa, Contrato, Cliente, DtHrEmi, DtHrBxa, ',
    'Vendedor, ECComprou, ECRetirou, NumOC, ValorTot, EndCobra, LocalObra, ',
    'LocalCntat, LocalFone, Obs0, Obs1, Obs2, ',
    //Historico,
    'HistImprime, HistNaoImpr, ',
    'TipoAluguel, DtHrSai, ',
    'ValorEquipamentos, ValorAdicional, ValorAdiantamento, ValorDesconto, ',
    'Status, JaLocou, Importado, ',
    //
    'DtHrDevolver, ValorLocacao, DtUltCobMens, FormaCobrLoca ',
    ') VALUES ']);
  //


  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT MAX(Locacao) FROM ' + OriTab;
  SDAux.Open;
  //
  SetLength(LocDatas, SDAux.Fields[0].AsInteger + 1);
  SetLength(LocHoras, SDAux.Fields[0].AsInteger + 1);
  SetLength(DevDatas, SDAux.Fields[0].AsInteger + 1);
  SetLength(DevHoras, SDAux.Fields[0].AsInteger + 1);
  SetLength(TipoLoc, SDAux.Fields[0].AsInteger + 1);

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDLocacao.Close;
    SDLocacao.DataSet.CommandType := ctQuery;
    SDLocacao.DataSet.CommandText := SQL;
    SDLocacao.Open;
    Continua := SDLocacao.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacao.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLocacao.First;
      SQLRec    := '';
      while not SDLocacao.Eof do
      begin
        Codigo := Integer(SDLocacaoLOCACAO.Value);
        LocDatas[Codigo] := Geral.FDT(SDLocacaoSAIDADATA.Value, 1);
        LocHoras[Codigo] := Geral.FDT(SDLocacaoSAIDAHORA.Value, 100);
        DevDatas[Codigo] := Geral.FDT(SDLocacaoDEVOLUCAODATA.Value, 1);
        DevHoras[Codigo] := Geral.FDT(SDLocacaoDEVOLUCAOHORA.Value, 100);
        TipoLoc[Codigo] := SDLocacaoTIPOALUGUEL.Value;
        //
        if SDLocacaoStatus.Value > 1 then
          JaLocou := 1
        else
          JaLocou := 0;
        ///

        if SDLocacao.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        // 2020-12-13
        Status := Integer(SDLocacaoSTATUS.Value);
        (* ===================================================================
           Re-desmarquei em 2021-01-11 porque a Karoline analisou os itens que
           se encaixam em (FECHAMENTODATA > 2) e (Status < 3)
           arquivo: C:\_Sincro\_MLArend\Clientes\Tisolin\2021\Teste4.csv
        if (SDLocacaoFECHAMENTODATA.Value > 2) and (Status < 3) then
          Status := 3;
        =====================================================================*)
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Empresa,
        + PV_N('-11')
        //Contrato,
        + PV_N('1')
        //Cliente,
        + PV_N(Geral.FF0(Integer(SDLocacaoCLIENTE.Value)))
        //DtHrEmi,
        + PV_A(Geral.FDT(SDLocacaoEMISSAODATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoEMISSAOHORA.Value, 100))
        //DtHrBxa,
        + PV_A(Geral.FDT(SDLocacaoFECHAMENTODATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoFECHAMENTOHORA.Value, 100))
        //Vendedor,
        + PV_N('-1')
        //ECComprou,
        + PV_N('0')
        //ECRetirou,
        + PV_N('0')
        //NumOC,
        + PV_A('')




        //ValorTot, Valor faturado! o que fazer?
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('VALORLOCACAO').AsExtended, 2, siNegativo))
        //EndCobra,
        + PV_A('')
        //LocalObra,
        + PV_N(Geral.VariavelToString(Copy(SDLocacaoObra.Value, 1, 100)))
        //LocalCntat,
        + PV_A('')
        //LocalFone,
        + PV_A('')
        //Obs0,
        + PV_A('')
        //Obs1,
        + PV_A('')
        //Obs2
        + PV_N(Geral.VariavelToString(SDLocacaoUSUARIO.Value))
{
        //Historico
        + PV_N(Geral.VariavelToString(SDLocacao.FieldByName('OBSERVACAO').AsVariant) + sLineBreak + Geral.VariavelToString(SDLocacaoObra.Value))
}
        //HistImprime
        + PV_N(Geral.VariavelToString(SDLocacao.FieldByName('OBRA').AsString))
        //HistNaoImpr
        + PV_N(Geral.VariavelToString(SDLocacao.FieldByName('OBSERVACAO').AsString))
        //TipoAluguel
        + PV_A(SDLocacaoTIPOALUGUEL.Value)
        //DtHrSai,
        + PV_A(Geral.FDT(SDLocacaoSAIDADATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoSAIDAHORA.Value, 100))
        //ValorEquipamentos
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorEquipamento').AsExtended, 2, siNegativo))
        //ValorAdicional
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorAdicional').AsExtended, 2, siNegativo))
        //ValorAdiantamento
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorAdiantamento').AsExtended, 2, siNegativo))
        //ValorDesconto
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorDesconto').AsExtended, 2, siNegativo))
        //Status
        + PV_N(Geral.FF0(Status)) //Integer(SDLocacaoStatus.Value)))
        //JaLocou
        + PV_N(Geral.FF0(Integer(JaLocou)))
        // Importado
        + PV_N('1') // foi importado Tisolin
        //DtHrDevolver,
        + PV_A(Geral.FDT(SDLocacaoDEVOLUCAODATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoDEVOLUCAOHORA.Value, 100))
        //ValorLocacao,
        + PV_N(Geral.FFT_Dot(SDLocacao.FieldByName('ValorLocacao').AsExtended, 2, siNegativo))
        //DtUltCobMens
        + PV_A(Geral.FDT(SDLocacaoULTIMACOBRANCAMENSAL.Value, 1))
        //FormaCobrLoca
        + PV_N(Geral.FF0(FormaCobrLoca))
        //
        + ') ';
  //object SDLocacaoLOCACAO: TFMTBCDField                      >>> Codigo
  //object SDLocacaoCLIENTE: TFMTBCDField                      >>> Cliente
  //object SDLocacaoEMISSAODATA: TDateField                    >>> DtHrEmi
  //object SDLocacaoEMISSAOHORA: TTimeField                    >>> DtHrEmi
  //object SDLocacaoDEVOLUCAODATA: TDateField                  >>> array para itens
  //object SDLocacaoDEVOLUCAOHORA: TTimeField                  >>> array para itens
  //object SDLocacaoSAIDADATA: TDateField                      >>> DtHrSai
  //object SDLocacaoSAIDAHORA: TTimeField                      >>> DtHrSai
  //object SDLocacaoFINALIZADODATA: TDateField                 N�o usa!
  //object SDLocacaoFINALIZADOHORA: TTimeField                 N�o usa!
  //object SDLocacaoVALOREQUIPAMENTO: TFMTBCDField             >>> ValorEquipamento
  //object SDLocacaoVALORLOCACAO: TFMTBCDField                 >>> ValorTot
  //object SDLocacaoVALORADICIONAL: TFMTBCDField               >>> ValorAdicional
  //object SDLocacaoVALORADIANTAMENTO: TFMTBCDField            >>> ValorAdiantamento
  //object SDLocacaoTIPOALUGUEL: TStringField                  >>> TipoAluguel
  //object SDLocacaoOBSERVACAO: TStringField                   >>> Historico
  //object SDLocacaoFECHAMENTODATA: TDateField                 >>> DtHrBxa
  //object SDLocacaoFECHAMENTOHORA: TTimeField                 >>> DtHrBxa
  //object SDLocacaoUSUARIO: TStringField                      >>> Obs2
  //object SDLocacaoSTATUS: TFMTBCDField                       >>> Status
  //object SDLocacaoVALORDESCONTO: TFMTBCDField                >>> ValorDesconto
  //object SDLocacaoOBRA: TStringField                         >>> LocalObra
  //object SDLocacaoCANCELADA: TStringField                    >>> Usa????
  //object SDLocacaoULTIMACOBRANCAMENSAL: TDateField           >>> DtUltCobMens
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLocacao.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoItens();
const
  sProcName = 'TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoItens();';
var
  SQL,
  //SQL_FIELDS_LCA, SQL_FIELDS_RUC, SQL_FIELDS_SVC, SQL_FIELDS_USO,
  //SQL_FIELDS_CNS,
  SQL_FIELDS_LOC_ANT, SQL_FIELDS_VEN_ANT,
  SQL_FIELDS_LCA, SQL_FIELDS_RUC, SQL_FIELDS_SVC, SQL_FIELDS_VEN, SQL_FIELDS_ITS,
  SQL_VALUES_LCA, SQL_VALUES_RUC, SQL_VALUES_SVC, SQL_VALUES_VEN, SQL_VALUES_ITS,
  SQLRec_ITS, SQLRec_LCA, SQLRec_RUC, SQLRec_SVC, SQLRec_VEN,
  SQL_VALUES_LOC_ANT, SQL_VALUES_VEN_ANT,
  SQLRec_LOC_ANT, SQLRec_VEN_ANT: String;
  ItsOK, Total, Step, Codigo(*, CtrIDAtu*): Integer;
  Continua: Boolean;
  //
  CATEGORIA: String;
  //
  //CtrID,
  GraGruX,
  CodAnt, CodAtu: Integer;
  ErrosTxt: String;
  ErrosCount: Integer;
  TROCA, COBRANCALOCACAO, COBRANCAREALIZADAVENDA, DATAINICIALCOBRANCA,
  HORAINICIALCOBRANCA, DATASAIDA, HORASAIDA: String;
  LOCACAO, SEQUENCIA, PRODUTO, QUANTIDADEPRODUTO, QUANTIDADELOCACAO,
  QUANTIDADEDEVOLUCAO: Integer;
  VALORPRODUTO, VALORLOCACAO, COBRANCACONSUMO: Double;
  SQLType: TSQLType;
  I, Acessorios_Count(*, GGXPrincipal*): Integer;
  EhAcessorio: Boolean;
  GTRTab: TGraToolRent;
  ValorDia, ValorSem, ValorQui, ValorMes: Double;
  RetFunci, RetExUsr, LibExUsr, LibFunci, QtdeLocacao, QtdeDevolucao, Item,

  CtrID_LOC_ANT, CtrID_VEN_ANT, CtrID_LOC, CtrID_SVC, CtrID_VEN,
  Item_LCA, Item_RUC, Item_SVC, Item_VEN,

  Trocado: Integer;
  DtHrLocado, DtHrRetorn, LibDtHr, RELIB, HOLIB, CobranIniDtHr, SaiDtHr,
  TipoAluguel: String;
  TemDatasLocacao: Boolean;

  GraGruY, ContaCtrID, ManejoLca, N, UsaSubsTrib: Integer;
  ValorLocAAdiantar: Double;
const
  OriTab = 'LocacaoItens';
  DstTab_ITS = 'loccconits';
  DstTab_LCA = 'loccitslca';
  DstTab_RUC = 'loccitsruc';
  DstTab_Svc = 'loccitssvc';
  DstTab_Ven = 'loccitsven';
  DstTab_LOC_ANT = 'locclocant';
  DstTab_VEN_ANT = 'loccvenant';
begin
  ErrosTxt := '';
  ErrosCount := 0;
  TemDatasLocacao := Length(LocDatas) > 0;
  if TemDatasLocacao = False then
  if Geral.MB_Pergunta('ATEN��O!!!!!!!!!!!!' + sLineBreak +
  'As datas de loca��o de itens s�o definidas ao importar o cabe�alho da loca��o antes!'
  + sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then Exit;

{

////////////////////////////////////////////////////////////////////////////////
  Precisa cadastrar os produtos pimeiro!
////////////////////////////////////////////////////////////////////////////////
}


{
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Verificando produtos que s�o acess�rios');
  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT DISTINCT ACESSORIO FROM PRODUTOACESSORIO ORDER BY ACESSORIO';
  SDAux.Open;
  //
  SDAux.Last;
  Acessorios_Count := SDAux.FieldByName('ACESSORIO').AsInteger + 1;
  SetLength(Acessorio, Acessorios_Count);
  for I := 0 to Acessorios_Count -1 do
    Acessorio[I] := False;
  //
  SDAux.First;
  while not SDAux.Eof do
  begin
    Acessorio[Integer(SDAux.FieldByName('ACESSORIO').AsInteger)] := True;
    //
    SDAux.Next;
  end;
}
  //

















// COME�AR AQUI!


  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS_ITS := Geral.ATS([
    'INSERT INTO ' + DstTab_ITS + ' ( ',
    'CtrID, Codigo, IDTab, Status ',
    ') VALUES ']);
    //
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS_LCA := Geral.ATS([
    'INSERT INTO ' + DstTab_LCA + ' ( ',
    'Item, CtrID, Codigo, ManejoLca, GraGruX, ',
    'ValorDia, ValorSem, ValorQui, ',
    'ValorMes, RetFunci, RetExUsr, ',
    'DtHrLocado, DtHrRetorn, LibExUsr, ',
    'LibFunci, LibDtHr, RELIB, ',
    'HOLIB, QtdeProduto, ValorProduto, ',
    'QtdeLocacao, ValorLocacao, QtdeDevolucao, ',
    'Troca, Categoria, COBRANCALOCACAO, ',
    'CobrancaConsumo, CobrancaRealizadaVenda, CobranIniDtHr, ',
    'SaiDtHr, ValorLocAAdiantar ',
    ') VALUES ']);
    //
  SQL_FIELDS_RUC := Geral.ATS([
    'INSERT INTO ' + DstTab_RUC + ' ( ',
    'Item, CtrID, Codigo, ManejoLca, GraGruX, ',
    'Unidade, QtdIni, QtdFim, ',
    'QtdUso, PrcUni, ValUso, ',
    'QtdeProduto, ValorProduto, QtdeLocacao, ',
    'ValorLocacao, QtdeDevolucao, Troca, ',
    'Categoria, COBRANCALOCACAO, CobrancaConsumo, ',
    'CobrancaRealizadaVenda, CobranIniDtHr, SaiDtHr ',
    ') VALUES ']);
  //
  SQL_FIELDS_SVC := Geral.ATS([
    'INSERT INTO ' + DstTab_SVC + ' ( ',
    'Item, CtrID, Codigo, ManejoLca, GraGruX, ',
    'Quantidade, PrcUni, ValorTotal, ',
    'QtdeProduto, ValorProduto, ',
    'QtdeLocacao, ValorLocacao, QtdeDevolucao, ',
    'Troca, Categoria, COBRANCALOCACAO, ',
    'CobrancaConsumo, CobrancaRealizadaVenda, CobranIniDtHr, ',
    'SaiDtHr ',
    ') VALUES ']);
  //
  SQL_FIELDS_VEN := Geral.ATS([
    'INSERT INTO ' + DstTab_VEN + ' ( ',
    'Item, CtrID, Codigo, ManejoLca, GraGruX, ',
    'Unidade, Quantidade, PrcUni, ',
    'ValorTotal, QtdeProduto, ValorProduto, ',
    'QtdeLocacao, ValorLocacao, QtdeDevolucao, ',
    'Troca, Categoria, COBRANCALOCACAO, ',
    'CobrancaConsumo, CobrancaRealizadaVenda, CobranIniDtHr, ',
    'SaiDtHr ',
    ') VALUES ']);


  //


  SQL_FIELDS_LOC_ANT := Geral.ATS([
    'INSERT INTO ' + DstTab_LOC_ANT + ' ( ',
    'Item, Codigo, CtrID, GraGruX, LOCACAO, ',
    'SEQUENCIA, PRODUTO, QUANTIDADEPRODUTO, ',
    'VALORPRODUTO, QUANTIDADELOCACAO, VALORLOCACAO, ',
    'QUANTIDADEDEVOLUCAO, TROCA, CATEGORIA, ',
    'COBRANCALOCACAO, COBRANCACONSUMO, COBRANCAREALIZADAVENDA, ',
    'DATAINICIALCOBRANCA, HORAINICIALCOBRANCA, DATASAIDA, ',
    'HORASAIDA',
    ') VALUES ']);
  //
  SQL_FIELDS_VEN_ANT := Geral.ATS([
    'INSERT INTO ' + DstTab_VEN_ANT + ' ( ',
    'Item, Codigo, CtrID, GraGruX, LOCACAO, ',
    'SEQUENCIA, PRODUTO, QUANTIDADEPRODUTO, ',
    'VALORPRODUTO, QUANTIDADELOCACAO, VALORLOCACAO, ',
    'QUANTIDADEDEVOLUCAO, TROCA, CATEGORIA, ',
    'COBRANCALOCACAO, COBRANCACONSUMO, COBRANCAREALIZADAVENDA, ',
    'DATAINICIALCOBRANCA, HORAINICIALCOBRANCA, DATASAIDA, ',
    'HORASAIDA',
    ') VALUES ']);
  //

  ItsOK := 0;
  CtrID_LOC_ANT  := 0;
  CtrID_VEN_ANT  := 0;
  CtrID_LOC     := 0;
  CtrID_SVC     := 0;
  CtrID_VEN     := 0;
  ContaCtrID    := 0;
  Item_LCA  := 0;
  Item_RUC  := 0;
  Item_SVC  := 0;
  Item_VEN  := 0;

  //

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Limpando tabelas de itens de loca��o');
  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados ITS. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_ITS);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados LCA. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_LCA);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados RUC. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_RUC);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados SVC. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_SVC);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados VEN. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_VEN);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados ANT LOC. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_LOC_ANT);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados ANT VEN. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_VEN_ANT);
  //
  //QrUpd.SQL.Text := '';
  //
  CodAnt := 0;
  CodAtu := 0;
  //CtrIDAtu := 0;
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'iniciando importa��o');
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    ///////////////////////////////////////////////////////////////////////////
    SQL := SQL + sLineBreak + 'ORDER BY LOCACAO, SEQUENCIA';
    ///////////////////////////////////////////////////////////////////////////
    ///
    SDLocacaoItens.Close;
    SDLocacaoItens.DataSet.CommandType := ctQuery;
    SDLocacaoItens.DataSet.CommandText := SQL;
    SDLocacaoItens.Open;
    Continua := SDLocacaoItens.RecordCount > 0;

    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacaoItens.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES_LOC_ANT := '';
      SQL_VALUES_VEN_ANT := '';
      SDLocacaoItens.First;
      SQLRec_LOC_ANT     := '';
      SQLRec_VEN_ANT     := '';
      SQLRec_ITS         := '';
      SQLRec_LCA         := '';
      SQLRec_RUC         := '';
      SQLRec_SVC         := '';
      SQLRec_VEN         := '';
      while not SDLocacaoItens.Eof do
      begin
        Codigo    := Integer(SDLocacaoItensLOCACAO.Value);
        GraGruX   := Integer(SDLocacaoItensPRODUTO.Value);
        CATEGORIA := SDLocacaoItensCATEGORIA.Value;
        //
{
        if Codigo <> CodAtu then
        begin
          CodAnt := CodAtu;
          CodAtu := Codigo;
          CtrIDAtu := 0;
          QuantidadeLocacao := SDLocacaoItens.FieldByName('QUANTIDADELOCACAO').AsExtended;
          ValorLocacao := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
          if (QuantidadeLocacao > 0.001) and (ValorLocacao > 0.001) then
          begin
            CtrID := CtrID + 1;
            CtrIDAtu := CtrID;
          end else
          begin
            ErrosCount := ErrosCount + 1;
            ErrosTxt := ErrosTxt + Geral.FF0(Codigo) + ',' + sLineBreak;
          end;
        end;
}

        //Buscar aqui todos valores de campos antes de definir SQLs!

        LOCACAO                := Integer(SDLOCACAOITENSLOCACAO.Value);
        SEQUENCIA              := Integer(SDLOCACAOITENSSEQUENCIA.Value);
        PRODUTO                := Integer(SDLOCACAOITENSPRODUTO.Value); // GraGruX
        QUANTIDADEPRODUTO      := Integer(SDLOCACAOITENSQUANTIDADEPRODUTO.Value);
        VALORPRODUTO           := SDLocacaoItens.FieldByName('VALORPRODUTO').AsExtended;
        QUANTIDADELOCACAO      := Integer(SDLOCACAOITENSQUANTIDADELOCACAO.Value);
        VALORLOCACAO           := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
        QUANTIDADEDEVOLUCAO    := Integer(SDLOCACAOITENSQUANTIDADEDEVOLUCAO.Value);
        TROCA                  := SDLOCACAOITENSTROCA.Value;
        if TROCA = 'S' then
          Trocado := 1
        else
          Trocado := 0;
        CATEGORIA              := UPPERCASE(SDLOCACAOITENSCATEGORIA.Value);
        COBRANCALOCACAO        := SDLOCACAOITENSCOBRANCALOCACAO.Value;
        COBRANCACONSUMO        := SDLocacaoItens.FieldByName('COBRANCACONSUMO').AsExtended;
        COBRANCAREALIZADAVENDA := SDLOCACAOITENSCOBRANCAREALIZADAVENDA.Value;
        DATAINICIALCOBRANCA    := Geral.FDT(SDLOCACAOITENSDATAINICIALCOBRANCA.Value, 1);
        HORAINICIALCOBRANCA    := Geral.FDT(SDLOCACAOITENSHORAINICIALCOBRANCA.Value, 100);
        DATASAIDA              := Geral.FDT(SDLOCACAOITENSDATASAIDA.Value, 1);
        HORASAIDA              := Geral.FDT(SDLOCACAOITENSHORASAIDA.Value, 100);
        //
        if HORAINICIALCOBRANCA = '' then
          HORAINICIALCOBRANCA := '00:00:00';
        if HORASAIDA = '' then
          HORASAIDA := '00:00:00';




        if Codigo <> CodAtu then
        begin
          CodAnt := CodAtu;
          CodAtu := Codigo;
          //CtrIDAtu := 0;
          CtrID_LOC := 0;
          CtrID_SVC := 0;
          CtrID_VEN := 0;
          //GGXPrincipal := 0;
        end;



        if (Uppercase(CATEGORIA) = 'L') then // Loca��o?
        //or (Uppercase(CATEGORIA) = 'M') then // Manuten��o?
        begin
          GraGruY := FGraGruY[GraGruX];
          if GraGruY < 1 then
            GraGruY := 8192; // Venda!
          case GraGruY of
            1024,
            2048,
            3072: GTRTab := TGraToolRent.gbsLocar;
            4096,
            5120,
            6144: GTRTab := TGraToolRent.gbsOutrs;
            7168: GTRTab := TGraToolRent.gbsServs;
            8192: GTRTab := TGraToolRent.gbsVends;
            else
            begin
              GTRTab := TGraToolRent.gbsIndef;
              Geral.MB_Erro('Tipo de loca��o indefinido! ' + sLineBreak +
              'Loca��o: ' + Geral.FF0(LOCACAO) + sLineBreak +
              'Produto: ' + Geral.FF0(PRODUTO));
            end;
          end;
          case GraGruY of
            1024: ManejoLca := Integer(TManejoLca.manelcaPrincipal);
            2048: ManejoLca := Integer(TManejoLca.manelcaSecundario);
            3072: ManejoLca := Integer(TManejoLca.manelcaAcessorio);
            4096: ManejoLca := Integer(TManejoLca.manelcaApoio);
            5120: ManejoLca := Integer(TManejoLca.manelcaUso);
            6144: ManejoLca := Integer(TManejoLca.manelcaConsumo);
            7168: ManejoLca := Integer(TManejoLca.manelcaServico);
            8192: ManejoLca := Integer(TManejoLca.manelcaVenda);
            else
            begin
              GTRTab := TGraToolRent.gbsIndef;
              Geral.MB_Erro('Tipo de loca��o indefinido! ' + sLineBreak +
              'Loca��o: ' + Geral.FF0(LOCACAO) + sLineBreak +
              'Produto: ' + Geral.FF0(PRODUTO));
            end;
          end;
          case GTRTab of
            TGraToolRent.gbsLocar: // LCA
            begin
              Item_LCA := Item_LCA + 1;
              //GGXPrincipal := GraGruX;
              if CtrID_LOC = 0 then
              begin
                ContaCtrID :=ContaCtrID + 1;
                CtrID_LOC := ContaCtrID;
                //
                if SQLRec_ITS = EmptyStr then
                  SQLRec_ITS    := ' ('
                else
                  SQLRec_ITS    := ', (';
                //
                SQLRec_ITS := SQLRec_ITS
                //CtrID
                + Geral.FF0(CtrID_LOC)
                //Codigo
                + PV_N(Geral.FF0(Codigo))
                //IDTab
                + PV_N(Geral.FF0(Integer(TTabConLocIts.tcliLocacao))) // Locacao
                //Status
                + PV_N(Geral.FF0(Integer(TStatusMovimento.statmovPedido)))
                //
                + ') ';
                //
                SQL_VALUES_ITS := SQL_VALUES_ITS + sLineBreak + SQLRec_ITS;

              end;
              // usar� em locacaomovimento >> LocCMovAll
              N := Length(F_LOCACAOITENS_PRODUTO_GGX[Codigo]);
              SetLength(F_LOCACAOITENS_PRODUTO_GGX[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_CTRID[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_ITEM[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo], N + 1);

              F_LOCACAOITENS_PRODUTO_GGX[Codigo][N] := GraGruX;
              F_LOCACAOITENS_PRODUTO_CTRID[Codigo][N] := CtrID_LOC;
              F_LOCACAOITENS_PRODUTO_ITEM[Codigo][N] := Item_LCA;
              F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo][N] := Integer(GTRTab);
              //

              //

              //loccpatpri
              //CtrID
              //Codigo
              //GraGruX
              ValorDia := 0.00;
              ValorSem := 0.00;
              ValorQui := 0.00;
              ValorMes := 0.00;
              RetFunci               := 0;
              RetExUsr               := 0;
              if TemDatasLocacao then
              begin
                DtHrLocado           := LocDatas[Codigo] + ' ' + LocHoras[Codigo];
                DtHrRetorn           := DevDatas[Codigo] + ' ' + DevHoras[Codigo];
                TipoAluguel          := TipoLoc[Codigo];
              end else
              begin
                DtHrLocado           := '0000-00-00 00:00:00';
                DtHrRetorn           := '0000-00-00 00:00:00';
                TipoAluguel          := '?';
              end;
              if TipoAluguel = 'D' then
                ValorDia := VALORLOCACAO
              else
              if TipoAluguel = 'S' then
                ValorSem := VALORLOCACAO
              else
              if TipoAluguel = 'Q' then
                ValorQui := VALORLOCACAO
              else
              if TipoAluguel = 'M' then
                ValorMes := VALORLOCACAO
              else
                ValorDia := VALORLOCACAO;
              //
              LibExUsr               := 0;
              LibFunci               := 0;
              LibDtHr                := '0000-00-00 00:00:00';
              RELIB                  := '';
              HOLIB                  := '';
              //ValorProduto           :=
              QtdeLocacao            := QUANTIDADELOCACAO;
              ValorLocacao           := VALORLOCACAO;
              QtdeDevolucao          := QUANTIDADEDEVOLUCAO;
              //Troca
              //Categoria
              //COBRANCALOCACAO
              //CobrancaConsumo
              //CobrancaRealizadaVenda
              CobranIniDtHr           := DATAINICIALCOBRANCA + ' ' + HORAINICIALCOBRANCA;
              SaiDtHr                 := DATASAIDA + ' ' + HORASAIDA;
              ValorLocAAdiantar       := QtdeLocacao * QUANTIDADEPRODUTO * ValorLocacao;
              //

              if SQLRec_LCA = EmptyStr then
                SQLRec_LCA    := ' ('
              else
                SQLRec_LCA    := ', (';
              //
              SQLRec_LCA := SQLRec_LCA
              // Item
              + Geral.FF0(Item_LCA)
              //CtrID
              + PV_N(Geral.FF0(CtrID_LOC))
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //ManejoLca
              + PV_N(Geral.FF0(ManejoLca))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
              //ValorDia
              + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
              //ValorSem
              + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
              //ValorQui
              + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
              //ValorMes
              + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
              //RetFunci
              + PV_N(Geral.FF0(RetFunci))
              //RetExUsr
              + PV_N(Geral.FF0(RetExUsr))

              //DtHrLocado
              + PV_A(DtHrLocado)
              //DtHrRetorn
              + PV_A(DtHrRetorn)
              //LibExUsr
              + PV_N(Geral.FF0(LibExUsr))
              //LibFunci
              + PV_N(Geral.FF0(LibFunci))
              //LibDtHr
              + PV_A(LibDtHr)
              // RELIB
              + PV_A(RELIB)
              //HOLIB
              + PV_A(HOLIB)
              //QtdeProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //ValorLocAAdiantar
              + PV_N(Geral.FFT_Dot(ValorLocAAdiantar, 2, siNegativo))
              //
              + ') ';
              //
              SQL_VALUES_LCA := SQL_VALUES_LCA + sLineBreak + SQLRec_LCA;

            end;
          end;
            //
          case GTRTab of
            TGraToolRent.gbsOutrs: // RUC
            begin
              Item_RUC := Item_RUC + 1;
              //GGXPrincipal := GraGruX;
              if CtrID_LOC = 0 then
              begin
                ContaCtrID :=ContaCtrID + 1;
                CtrID_LOC := ContaCtrID;
                //
                if SQLRec_ITS = EmptyStr then
                  SQLRec_ITS    := ' ('
                else
                  SQLRec_ITS    := ', (';
                //
                SQLRec_ITS := SQLRec_ITS
                //CtrID
                + Geral.FF0(CtrID_LOC)
                //Codigo
                + PV_N(Geral.FF0(Codigo))
                //IDTab
                + PV_N(Geral.FF0(Integer(TTabConLocIts.tcliLocacao))) // Locacao!
                //Status
                + PV_N(Geral.FF0(Integer(TStatusMovimento.statmovPedido)))
                //
                + ') ';
                //
                SQL_VALUES_ITS := SQL_VALUES_ITS + sLineBreak + SQLRec_ITS;

              end;
              //

              // usar� em locacaomovimento >> LocCMovAll
              N := Length(F_LOCACAOITENS_PRODUTO_GGX[Codigo]);
              SetLength(F_LOCACAOITENS_PRODUTO_GGX[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_CTRID[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_ITEM[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo], N + 1);

              F_LOCACAOITENS_PRODUTO_GGX[Codigo][N] := GraGruX;
              F_LOCACAOITENS_PRODUTO_CTRID[Codigo][N] := CtrID_LOC;
              F_LOCACAOITENS_PRODUTO_ITEM[Codigo][N] := Item_RUC;
              F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo][N] := Integer(GTRTab);
              //
              //loccpatpri
              //CtrID
              //Codigo
              //GraGruX
              ValorDia := 0.00;
              ValorSem := 0.00;
              ValorQui := 0.00;
              ValorMes := 0.00;
              RetFunci               := 0;
              RetExUsr               := 0;
              if TemDatasLocacao then
              begin
                DtHrLocado           := LocDatas[Codigo] + ' ' + LocHoras[Codigo];
                DtHrRetorn           := DevDatas[Codigo] + ' ' + DevHoras[Codigo];
                TipoAluguel          := TipoLoc[Codigo];
              end else
              begin
                DtHrLocado           := '0000-00-00 00:00:00';
                DtHrRetorn           := '0000-00-00 00:00:00';
                TipoAluguel          := '?';
              end;
              if TipoAluguel = 'D' then
                ValorDia := VALORLOCACAO
              else
              if TipoAluguel = 'S' then
                ValorSem := VALORLOCACAO
              else
              if TipoAluguel = 'Q' then
                ValorQui := VALORLOCACAO
              else
              if TipoAluguel = 'M' then
                ValorMes := VALORLOCACAO
              else
                ValorDia := VALORLOCACAO;
              //
              LibExUsr               := 0;
              LibFunci               := 0;
              LibDtHr                := '0000-00-00 00:00:00';
              RELIB                  := '';
              HOLIB                  := '';
              //ValorProduto           :=
              QtdeLocacao            := QUANTIDADELOCACAO;
              ValorLocacao           := VALORLOCACAO;
              QtdeDevolucao          := QUANTIDADEDEVOLUCAO;
              //Troca
              //Categoria
              //COBRANCALOCACAO
              //CobrancaConsumo
              //CobrancaRealizadaVenda
              CobranIniDtHr           := DATAINICIALCOBRANCA + ' ' + HORAINICIALCOBRANCA;
              SaiDtHr                 := DATASAIDA + ' ' + HORASAIDA;

              //

              if SQLRec_RUC = EmptyStr then
                SQLRec_RUC    := ' ('
              else
                SQLRec_RUC    := ', (';
              //
              SQLRec_RUC := SQLRec_RUC
              // Item
              + Geral.FF0(Item_RUC)
              //CtrID
              + PV_N(Geral.FF0(CtrID_LOC))
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //ManejoLca
              + PV_N(Geral.FF0(ManejoLca))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
              //Unidade
              + PV_N(Geral.FF0(1))
              //QtdIni
              + PV_N(Geral.FF0(QUANTIDADELOCACAO * QUANTIDADEPRODUTO))
              //QtdFim
              + PV_N('0')
              //QtdUso
              + PV_N(Geral.FF0(QUANTIDADELOCACAO * QUANTIDADEPRODUTO))
              //PrcUni
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //ValUso
              + PV_N(Geral.FFT_DOT(QUANTIDADELOCACAO * QUANTIDADEPRODUTO * VALORLOCACAO, 2, siNegativo))
              //QtdProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //
              + ') ';
              //
              SQL_VALUES_RUC := SQL_VALUES_RUC + sLineBreak + SQLRec_RUC;

            end;

            TGraToolRent.gbsServs:
            begin
              Item_SVC := ITEM_SVC + 1;
              //
              if CtrID_SVC = 0 then
              begin
                ContaCtrID := ContaCtrID + 1;
                CtrID_SVC  := ContaCtrID;
                //
                if SQLRec_ITS = EmptyStr then
                  SQLRec_ITS    := ' ('
                else
                  SQLRec_ITS    := ', (';
                //
                SQLRec_ITS := SQLRec_ITS
                //CtrID
                + Geral.FF0(CtrID_SVC)
                //Codigo
                + PV_N(Geral.FF0(Codigo))
                //IDTab
                + PV_N(Geral.FF0(Integer(TTabConLocIts.tcliOutroServico)))
                //Status
                + PV_N(Geral.FF0(Integer(TStatusMovimento.statmovPedido)))
                //
                + ') ';
                //
                SQL_VALUES_ITS := SQL_VALUES_ITS + sLineBreak + SQLRec_ITS;

              end;
              //

              // usar� em locacaomovimento >> LocCMovAll
              N := Length(F_LOCACAOITENS_PRODUTO_GGX[Codigo]);
              SetLength(F_LOCACAOITENS_PRODUTO_GGX[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_CTRID[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_ITEM[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo], N + 1);

              F_LOCACAOITENS_PRODUTO_GGX[Codigo][N] := GraGruX;
              F_LOCACAOITENS_PRODUTO_CTRID[Codigo][N] := CtrID_SVC;
              F_LOCACAOITENS_PRODUTO_ITEM[Codigo][N] := Item_SVC;
              F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo][N] := Integer(GTRTab);
              //
              //

              if SQLRec_SVC = EmptyStr then
                SQLRec_SVC    := ' ('
              else
                SQLRec_SVC    := ', (';
              //
              SQLRec_SVC := SQLRec_SVC
              //Item
              + Geral.FF0(Item_SVC)
              //CtrID
              + PV_N(Geral.FF0(CtrID_SVC))
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //ManejoLca
              + PV_N(Geral.FF0(ManejoLca))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
              //Quantidade
              + PV_N(Geral.FFT_Dot(QUANTIDADELOCACAO * QUANTIDADEPRODUTO, 3, siNegativo))
              //PrcUni
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //ValorTotal
              + PV_N(Geral.FFT_Dot(QUANTIDADELOCACAO * QUANTIDADEPRODUTO * VALORLOCACAO, 2, siNegativo))
              //QtdeProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //
              + ') ';
              //
              SQL_VALUES_SVC := SQL_VALUES_SVC + sLineBreak + SQLRec_SVC;

            end;
            //
            TGraToolRent.gbsVends:
            begin
              Item_Ven := ITEM_Ven + 1;
              //
              if CtrID_VEN = 0 then
              begin
                ContaCtrID := ContaCtrID + 1;
                CtrID_VEN  := ContaCtrID;
                //
                if SQLRec_ITS = EmptyStr then
                  SQLRec_ITS    := ' ('
                else
                  SQLRec_ITS    := ', (';
                //
                SQLRec_ITS := SQLRec_ITS
                //CtrID
                + Geral.FF0(CtrID_VEN)
                //Codigo
                + PV_N(Geral.FF0(Codigo))
                //IDTab
                + PV_N(Geral.FF0(Integer(TTabConLocIts.tcliVenda)))
                //Status
                + PV_N(Geral.FF0(Integer(TStatusMovimento.statmovPedido)))
                //
                + ') ';
                //
                SQL_VALUES_ITS := SQL_VALUES_ITS + sLineBreak + SQLRec_ITS;

              end;

              //

              // usar� em locacaomovimento >> LocCMovAll
              N := Length(F_LOCACAOITENS_PRODUTO_GGX[Codigo]);
              SetLength(F_LOCACAOITENS_PRODUTO_GGX[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_CTRID[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_ITEM[Codigo], N + 1);
              SetLength(F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo], N + 1);

              F_LOCACAOITENS_PRODUTO_GGX[Codigo][N] := GraGruX;
              F_LOCACAOITENS_PRODUTO_CTRID[Codigo][N] := CtrID_VEN;
              F_LOCACAOITENS_PRODUTO_ITEM[Codigo][N] := Item_VEN;
              F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo][N] := Integer(GTRTab);
              //
              if SQLRec_VEN = EmptyStr then
                SQLRec_VEN    := ' ('
              else
                SQLRec_VEN    := ', (';
              //
              SQLRec_VEN := SQLRec_VEN
              //Item
              + Geral.FF0(Item_VEN)
              //CtrID
              + PV_N(Geral.FF0(CtrID_VEN))
              //Codigo
              + PV_N(Geral.FF0(Codigo))
              //ManejoLca
              + PV_N(Geral.FF0(ManejoLca))
              //GraGruX
              + PV_N(Geral.FF0(GraGruX))
              //Unidade
              + PV_N('1')
              //Quantidade
              + PV_N(Geral.FFT_Dot(QUANTIDADELOCACAO * QUANTIDADEPRODUTO, 6, siNegativo))
              //PrcUni
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //ValorTotal
              + PV_N(Geral.FFT_Dot(QUANTIDADELOCACAO * QUANTIDADEPRODUTO * VALORLOCACAO, 2, siNegativo))
              //QtdeProduto
              + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
              //VALORPRODUTO
              + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
              //QtdeLocacao
              + PV_N(Geral.FF0(QUANTIDADELOCACAO))
              //ValorLocacao
              + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
              //QtdeDevolucao
              + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
              //TROCA
              + PV_N(Geral.FF0(TROCADO))
              //CATEGORIA
              + PV_A(CATEGORIA)
              //COBRANCALOCACAO
              + PV_A(COBRANCALOCACAO)
              //COBRANCACONSUMO
              + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
              //COBRANCAREALIZADAVENDA
              + PV_A(COBRANCAREALIZADAVENDA)
              //CobranIniDtHr
              + PV_A(CobranIniDtHr)
              //SaiDtHr
              + PV_A(SaiDtHr)
              //
              + ') ';
              //
              SQL_VALUES_VEN := SQL_VALUES_VEN + sLineBreak + SQLRec_VEN;

            end;
          end;

          CtrID_LOC_ANT := CtrID_LOC_ANT + 1;

          if SQLRec_LOC_ANT = EmptyStr then
            SQLRec_LOC_ANT    := ' ('
          else
            SQLRec_LOC_ANT    := ', (';
          //
          SQLRec_LOC_ANT := SQLRec_LOC_ANT
          //Item
          + Geral.FF0(CtrID_LOC_ANT)
          //Codigo
          + PV_N(Geral.FF0(Codigo))
          //CtrID
          + PV_N(Geral.FF0(CtrID_LOC_ANT))
          //GraGruX
          + PV_N(Geral.FF0(GraGruX))
          //Locacao
          + PV_N(Geral.FF0(Locacao))
          //Sequencia
          + PV_N(Geral.FF0(Sequencia))
          //Produto
          + PV_N(Geral.FF0(Produto))
          //QUANTIDADEPRODUTO
          + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
          //VALORPRODUTO
          + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
          //QUANTIDADELOCACAO
          + PV_N(Geral.FF0(QUANTIDADELOCACAO))
          //VALORLOCACAO
          + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
          //QUANTIDADEDEVOLUCAO
          + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
          //TROCA
          + PV_N(Geral.VariavelToString(TROCA))
          //CATEGORIA
          + PV_N(Geral.VariavelToString(CATEGORIA))
          //COBRANCALOCACAO
          + PV_N(Geral.VariavelToString(COBRANCALOCACAO))
          //COBRANCACONSUMO
          + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
          //COBRANCAREALIZADAVENDA
          + PV_N(Geral.VariavelToString(COBRANCAREALIZADAVENDA))
          //DATAINICIALCOBRANCA
          + PV_A(DATAINICIALCOBRANCA)
          //HORAINICIALCOBRANCA
          + PV_A(HORAINICIALCOBRANCA)
          //DATASAIDA
          + PV_A(DATASAIDA)
          //HORASAIDA
          + PV_A(HORASAIDA)
          //
          + ') ';
          //
          SQL_VALUES_LOC_ANT := SQL_VALUES_LOC_ANT + sLineBreak + SQLRec_LOC_ANT;
        end else
        //Venda
        if (Uppercase(CATEGORIA) = 'V')
        or (Uppercase(CATEGORIA) = 'M') then // Manuten��o?
        begin
          Item_Ven := ITEM_Ven + 1;
          ManejoLca := Integer(TManejoLca.manelcaVenda);
          //
          if CtrID_VEN = 0 then
          begin
            ContaCtrID := ContaCtrID + 1;
            CtrID_VEN  := ContaCtrID;
            //
            if SQLRec_ITS = EmptyStr then
              SQLRec_ITS    := ' ('
            else
              SQLRec_ITS    := ', (';
            //
            SQLRec_ITS := SQLRec_ITS
            //CtrID
            + Geral.FF0(CtrID_VEN)
            //Codigo
            + PV_N(Geral.FF0(Codigo))
            //IDTab
            + PV_N(Geral.FF0(Integer(TTabConLocIts.tcliVenda)))
            //Status
            + PV_N(Geral.FF0(Integer(TStatusMovimento.statmovPedido)))
            //
            + ') ';
            //
            SQL_VALUES_ITS := SQL_VALUES_ITS + sLineBreak + SQLRec_ITS;

          end;

          //

          // usar� em locacaomovimento >> LocCMovAll
          N := Length(F_LOCACAOITENS_PRODUTO_GGX[Codigo]);
          SetLength(F_LOCACAOITENS_PRODUTO_GGX[Codigo], N + 1);
          SetLength(F_LOCACAOITENS_PRODUTO_CTRID[Codigo], N + 1);
          SetLength(F_LOCACAOITENS_PRODUTO_ITEM[Codigo], N + 1);
          SetLength(F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo], N + 1);

          F_LOCACAOITENS_PRODUTO_GGX[Codigo][N] := GraGruX;
          F_LOCACAOITENS_PRODUTO_CTRID[Codigo][N] := CtrID_VEN;
          F_LOCACAOITENS_PRODUTO_ITEM[Codigo][N] := Item_VEN;
          F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo][N] := Integer(GTRTab);
          //
          if SQLRec_VEN = EmptyStr then
            SQLRec_VEN    := ' ('
          else
            SQLRec_VEN    := ', (';
          //
          SQLRec_VEN := SQLRec_VEN
          //Item
          + Geral.FF0(Item_VEN)
          //CtrID
          + PV_N(Geral.FF0(CtrID_VEN))
          //Codigo
          + PV_N(Geral.FF0(Codigo))
          //ManejoLca
          + PV_N(Geral.FF0(ManejoLca))
          //GraGruX
          + PV_N(Geral.FF0(GraGruX))
          //Unidade
          + PV_N('1')
          //Quantidade
          + PV_N(Geral.FFT_Dot(QUANTIDADELOCACAO * QUANTIDADEPRODUTO, 6, siNegativo))
          //PrcUni
          + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
          //ValorTotal
          + PV_N(Geral.FFT_Dot(QUANTIDADELOCACAO * QUANTIDADEPRODUTO * VALORLOCACAO, 2, siNegativo))
          //QtdeProduto
          + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
          //VALORPRODUTO
          + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
          //QtdeLocacao
          + PV_N(Geral.FF0(QUANTIDADELOCACAO))
          //ValorLocacao
          + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
          //QtdeDevolucao
          + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
          //TROCA
          + PV_N(Geral.FF0(TROCADO))
          //CATEGORIA
          + PV_A(CATEGORIA)
          //COBRANCALOCACAO
          + PV_A(COBRANCALOCACAO)
          //COBRANCACONSUMO
          + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
          //COBRANCAREALIZADAVENDA
          + PV_A(COBRANCAREALIZADAVENDA)
          //CobranIniDtHr
          + PV_A(CobranIniDtHr)
          //SaiDtHr
          + PV_A(SaiDtHr)
          //
          + ') ';
          //
          SQL_VALUES_VEN := SQL_VALUES_VEN + sLineBreak + SQLRec_VEN;













          CtrID_VEN_ANT      := CtrID_VEN_ANT + 1;

(*
          LOCACAO                := Codigo;
          SEQUENCIA              := Integer(SDLocacaoItensSEQUENCIA.Value);
          PRODUTO                := GraGruX;
          QUANTIDADEPRODUTO      := Integer(SDLocacaoItensQUANTIDADEPRODUTO.Value);
          VALORPRODUTO           := SDLocacaoItens.FieldByName('VALORPRODUTO').AsExtended;
          QUANTIDADELOCACAO      := Integer(SDLocacaoItensQUANTIDADELOCACAO.Value);
          VALORLOCACAO           := SDLocacaoItens.FieldByName('VALORLOCACAO').AsExtended;
          QUANTIDADEDEVOLUCAO    := Integer(SDLocacaoItensQUANTIDADEDEVOLUCAO.Value);
          TROCA                  := SDLocacaoItensTROCA.Value;
          CATEGORIA              := SDLocacaoItensCATEGORIA.Value;
          COBRANCALOCACAO        := SDLocacaoItensCOBRANCALOCACAO.Value;
          COBRANCACONSUMO        := SDLocacaoItens.FieldByName('COBRANCACONSUMO').AsExtended;
          COBRANCAREALIZADAVENDA := SDLocacaoItensCOBRANCAREALIZADAVENDA.Value;
          DATAINICIALCOBRANCA    := Geral.FDT(SDLocacaoItensDATAINICIALCOBRANCA.Value, 1);
          HORAINICIALCOBRANCA    := Geral.FDT(SDLocacaoItensHORAINICIALCOBRANCA.Value, 100);
          if HORAINICIALCOBRANCA = '' then
            HORAINICIALCOBRANCA := '00:00:00';
          DATASAIDA              := Geral.FDT(SDLocacaoItensDATASAIDA.Value, 1);
          HORASAIDA              := Geral.FDT(SDLocacaoItensHORASAIDA.Value, 100);
          if HORASAIDA = '' then
            HORASAIDA := '00:00:00';
          //
*)
          if SQLRec_VEN_ANT = EmptyStr then
            SQLRec_VEN_ANT    := ' ('
          else
            SQLRec_VEN_ANT    := ', (';
          //
          SQLRec_VEN_ANT := SQLRec_VEN_ANT
          //Item
          + Geral.FF0(CtrID_VEN_ANT)
          //Codigo
          + PV_N(Geral.FF0(Codigo))
          //CtrID
          + PV_N(Geral.FF0(CtrID_VEN_ANT))
          //GraGruX
          + PV_N(Geral.FF0(GraGruX))
          //Locacao
          + PV_N(Geral.FF0(Locacao))
          //Sequencia
          + PV_N(Geral.FF0(Sequencia))
          //Produto
          + PV_N(Geral.FF0(Produto))
          //QUANTIDADEPRODUTO
          + PV_N(Geral.FF0(QUANTIDADEPRODUTO))
          //VALORPRODUTO
          + PV_N(Geral.FFT_Dot(VALORPRODUTO, 2, siNegativo))
          //QUANTIDADELOCACAO
          + PV_N(Geral.FF0(QUANTIDADELOCACAO))
          //VALORLOCACAO
          + PV_N(Geral.FFT_Dot(VALORLOCACAO, 2, siNegativo))
          //QUANTIDADEDEVOLUCAO
          + PV_N(Geral.FF0(QUANTIDADEDEVOLUCAO))
          //TROCA
          + PV_N(Geral.VariavelToString(TROCA))
          //CATEGORIA
          + PV_N(Geral.VariavelToString(CATEGORIA))
          //COBRANCALOCACAO
          + PV_N(Geral.VariavelToString(COBRANCALOCACAO))
          //COBRANCACONSUMO
          + PV_N(Geral.FFT_Dot(COBRANCACONSUMO, 2, siNegativo))
          //COBRANCAREALIZADAVENDA
          + PV_N(Geral.VariavelToString(COBRANCAREALIZADAVENDA))
          //DATAINICIALCOBRANCA
          + PV_A(DATAINICIALCOBRANCA)
          //HORAINICIALCOBRANCA
          + PV_A(HORAINICIALCOBRANCA)
          //DATASAIDA
          + PV_A(DATASAIDA)
          //HORASAIDA
          + PV_A(HORASAIDA)
          //
          + ') ';
          //
          SQL_VALUES_VEN_ANT := SQL_VALUES_VEN_ANT + sLineBreak + SQLRec_VEN_ANT;
        end;
        SDLocacaoItens.Next;
      end;

      //



















      if SQL_VALUES_ITS <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_ITS + SQL_VALUES_ITS;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_ITS + SQL_VALUES_ITS);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_ITS := EmptyStr;


      if SQL_VALUES_LCA <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_LCA + SQL_VALUES_LCA;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_LCA + SQL_VALUES_LCA);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_LCA := EmptyStr;


      //


      if SQL_VALUES_RUC <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_RUC + SQL_VALUES_RUC;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_RUC + SQL_VALUES_RUC);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_RUC := EmptyStr;


      //


      if SQL_VALUES_SVC <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_SVC + SQL_VALUES_SVC;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_SVC + SQL_VALUES_SVC);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_SVC := EmptyStr;


      //


      if SQL_VALUES_VEN <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_VEN + SQL_VALUES_VEN;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_VEN + SQL_VALUES_VEN);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_VEN := EmptyStr;


      //


      if SQL_VALUES_LOC_ANT <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_LOC_ANT + SQL_VALUES_LOC_ANT;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_LOC_ANT + SQL_VALUES_LOC_ANT);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_LOC_ANT := EmptyStr;


      //


      if SQL_VALUES_VEN_ANT <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_VEN_ANT + SQL_VALUES_VEN_ANT;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_VEN_ANT + SQL_VALUES_VEN_ANT);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_VEN_ANT := EmptyStr;


      //


      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLocacaoItens.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
  //
  AtualizaSaldosLocados();
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoMovimento();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step, Codigo, ManejoLca, CtrID, Item, Controle, GraGruX, I: Integer;
  Continua: Boolean;
  TipoMotiv: String;
const
  OriTab = 'LocacaoMovimento';
  DstTab = 'loccmovall';

begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Controle, Codigo, GTRTab, CtrID, Item, SEQUENCIA, DataHora, USUARIO, ',
    'GraGruX, TipoES, Quantidade, TipoMotiv, COBRANCALOCACAO, VALORLOCACAO, ',
    'LIMPO, SUJO, QUEBRADO, TESTADODEVOLUCAO, GGXEntrada, MotivoTroca, ',
    'ObservacaoTroca, QuantidadeLocada, QuantidadeJaDevolvida, Empresa ',
    ') VALUES ']);
  //


  ItsOK := 0;
  Controle := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDLocacaoMovimento.Close;
    SDLocacaoMovimento.DataSet.CommandType := ctQuery;
    SDLocacaoMovimento.DataSet.CommandText := SQL;
    SDLocacaoMovimento.Open;
    Continua := SDLocacaoMovimento.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacaoMovimento.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLocacaoMovimento.First;
      SQLRec    := '';
      while not SDLocacaoMovimento.Eof do
      begin
        Codigo   := Integer(SDLocacaoMovimentoLOCACAO.Value);
        GraGruX  := Integer(SDLocacaoMovimentoPRODUTO.Value);
        CtrID    := 0;
        Item     := 0;
        ManejoLca    := 0;
        for I := 0 to Length(F_LOCACAOITENS_PRODUTO_GGX[Codigo]) - 1 do
        begin
          if F_LOCACAOITENS_PRODUTO_GGX[Codigo][I] = GraGruX then
          begin
            CtrID      := F_LOCACAOITENS_PRODUTO_CTRID[Codigo][I];
            Item       := F_LOCACAOITENS_PRODUTO_ITEM[Codigo][I];
            ManejoLca  := F_LOCACAOITENS_PRODUTO_GTRTAB[Codigo][I];
            Break;
          end;
        end;
        Controle := Controle + 1;
        ///
        if SDLocacaoMovimentoDESCRICAO.Value = 'Devolu��o de Loca��o' then
          TipoMotiv := '4'
        else
        if SDLocacaoMovimentoDESCRICAO.Value = 'DL' then
          TipoMotiv := '4'
        else
        if SDLocacaoMovimentoDESCRICAO.Value = 'Retirado para Loca��o' then
          TipoMotiv := '1'
        else
        if SDLocacaoMovimentoDESCRICAO.Value = 'Retorno para Loca��o [troca]' then
          TipoMotiv := '2'
        else
        if SDLocacaoMovimentoDESCRICAO.Value = 'Retirado para Loca��o [troca]' then
          TipoMotiv := '3'
        else
          TipoMotiv := '0'
        ;

        if SDLocacaoMovimento.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Controle,
        +     (Geral.FF0(Controle))
        //Codigo,
        + PV_N(Geral.FF0(Codigo))
        //ManejoLca,
        + PV_N(Geral.FF0(ManejoLca))
        //CtrID,
        + PV_N(Geral.FF0(CtrID))
        //Item,
        + PV_N(Geral.FF0(Item))
        //SEQUENCIA,
        + PV_N(Geral.FF0(Integer(SDLocacaoMovimentoSEQUENCIA.Value)))
        //DataHora,
        + PV_A(Geral.FDT(SDLocacaoMovimentoDATA.Value, 1) + ' ' + Geral.FDT(SDLocacaoMovimentoHORA.Value, 100))
        //USUARIO
        + PV_A(SDLocacaoMovimentoUSUARIO.Value)
        //GraGruX,
        + PV_N(Geral.FF0(GraGruX))
        //TipoES,
        + PV_A(SDLocacaoMovimentoTIPO.Value)
        //Quantidade,
        + PV_N(Geral.FFT_Dot(SDLocacaoMovimento.FieldByName('QUANTIDADE').AsExtended, 3, siNegativo))
        //TipoMotiv,
        + PV_N(TipoMotiv)
        //COBRANCALOCACAO,
        + PV_A(SDLocacaoMovimentoCOBRANCALOCACAO.Value)
        //VALORLOCACAO,
        + PV_N(Geral.FFT_Dot(SDLocacaoMovimento.FieldByName('VALORLOCACAO').AsExtended, 2, siNegativo))
        //LIMPO,
        + PV_A(SDLocacaoMovimentoLIMPO.Value)
        //SUJO,
        + PV_A(SDLocacaoMovimentoSUJO.Value)
        //QUEBRADO,
        + PV_A(SDLocacaoMovimentoQUEBRADO.Value)
        //TESTADODEVOLUCAO,
        + PV_A(SDLocacaoMovimentoTESTADODEVOLUCAO.Value)
        //GGXEntrada,
        + PV_N(Geral.FF0(Integer(SDLocacaoMovimentoPRODUTOENTRADA.Value)))
        //MotivoTroca, ',
        + PV_N(Geral.FF0(Integer(SDLocacaoMovimentoMotivoTroca.Value)))
        //ObservacaoTroca,
        + PV_N(Geral.VariavelToString(SDLocacaoMovimentoObservacaoTroca.Value))
        //QuantidadeLocada,
        + PV_N(Geral.FF0(Integer(SDLocacaoMovimentoQuantidadeLocada.Value)))
        //QuantidadeJaDevolvida ',
        + PV_N(Geral.FF0(Integer(SDLocacaoMovimentoQuantidadeJADevolvida.Value)))
        //Empresa ',
        + PV_N('-11')
        //
        + ') ';
               //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLocacaoMovimento.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoHistorico();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo: Integer;
  Nome, DataHora, Usuario, DtHr: String;
const
  OriTab = 'locacaohistorico';
  DstTab = 'locchisall';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, DataHora, Nome, Usuario) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDLocacaoHistorico.Close;
    SDLocacaoHistorico.DataSet.CommandType := ctQuery;
    SDLocacaoHistorico.DataSet.CommandText := SQL;
    SDLocacaoHistorico.Open;
    Continua := SDLocacaoHistorico.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacaoHistorico.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLocacaoHistorico.First;
      SQLRec    := '';
      while not SDLocacaoHistorico.Eof do
      begin
        Codigo   := Integer(SDLocacaoHistoricoLOCACAO.Value);
        DtHr     := SDLocacaoHistorico.FieldByName('DATAHORA').AsWideString;
        DataHora := Geral.FDT(Geral.ValidaDataBR(Copy(DtHr, 1, 10), True, False), 1);
        DataHora := DataHora + ' ' + Copy(DtHr, 11);
        Nome     := SDLocacaoHistoricoDESCRICAO.Value;
        Usuario  := SDLocacaoHistoricoUSUARIO.Value;
        //
        if SDLocacaoHistorico.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //DataHora
        + PV_A(DataHora)
        //Nome
        + PV_N(Geral.VariavelToString(Nome))
        //Nome
        + PV_N(Geral.VariavelToString(Usuario))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLocacaoHistorico.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLocacaoHistorico.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_LocacaoTrocaMotivo;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, Controle: Integer;
  Nome: String;
const
  OriTab = 'locacaotrocamotivo';
  DstTab = 'loctromotcad';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Nome) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDLocacaoTrocaMotivo.Close;
    SDLocacaoTrocaMotivo.DataSet.CommandType := ctQuery;
    SDLocacaoTrocaMotivo.DataSet.CommandText := SQL;
    SDLocacaoTrocaMotivo.Open;
    Continua := SDLocacaoTrocaMotivo.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDLocacaoTrocaMotivo.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDLocacaoTrocaMotivo.First;
      SQLRec    := '';
      while not SDLocacaoTrocaMotivo.Eof do
      begin
        Codigo   := Integer(SDLocacaoTrocaMotivoTROCAMOTIVO.Value);
        Nome     := SDLocacaoTrocaMotivoDESCRICAO.Value;
        //
        if SDLocacaoTrocaMotivo.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Nome
        + PV_N(Geral.VariavelToString(Nome))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDLocacaoTrocaMotivo.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDLocacaoTrocaMotivo.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Loja;
var
  CSC, CSCpos: String;
begin
  SDLoja.Close;
  SDLoja.DataSet.CommandType := ctQuery;
  SDLoja.DataSet.CommandText := 'SELECT * FROM loja';
  SDLoja.Open;
  CSC    := SDLojaCSC.Value;
  CSCpos := SDLojaIDToken.Value;
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE paramsemp SET  ' +
  ' CSC="' + CSC + '" ' +
  ', CSCPos="' + CSCPos + '" ');
  //
(*
CREATE TABLE `paramsemp` (
  `Codigo` int(11) NOT NULL DEFAULT '0',
  `Moeda` int(11) NOT NULL DEFAULT '0',
  `Situacao` int(11) NOT NULL DEFAULT '0',
  `RegrFiscal` int(11) NOT NULL DEFAULT '0',
  `FretePor` tinyint(1) NOT NULL DEFAULT '0',
  `TipMediaDD` tinyint(1) NOT NULL DEFAULT '0',
  `TipCalcJuro` tinyint(1) NOT NULL DEFAULT '2',
  `FatSemPrcL` tinyint(1) NOT NULL DEFAULT '0',
  `BalQtdItem` double(15,3) NOT NULL DEFAULT '0.000',
  `Associada` int(11) NOT NULL DEFAULT '0',
  `AssocModNF` int(11) NOT NULL DEFAULT '0',
  `FatSemEstq` tinyint(1) NOT NULL DEFAULT '2',
  `CtaProdVen` int(11) NOT NULL DEFAULT '0',
  `CtaProdCom` int(11) NOT NULL DEFAULT '0',
  `TxtProdVen` varchar(100) NOT NULL DEFAULT 'VP',
  `TxtProdCom` varchar(100) NOT NULL DEFAULT 'CP',
  `FaturaNum` tinyint(1) NOT NULL DEFAULT '1',
  `FaturaSep` char(1) NOT NULL DEFAULT '/',
  `FaturaSeq` tinyint(1) NOT NULL DEFAULT '0',
  `FaturaDta` tinyint(1) NOT NULL DEFAULT '0',
  `Logo3x1` varchar(255) DEFAULT NULL,
  `PediVdaNElertas` tinyint(1) NOT NULL DEFAULT '0',
  `SimplesFed` tinyint(1) NOT NULL DEFAULT '0',
  `CRT` tinyint(1) NOT NULL DEFAULT '0',
  `CSOSN` int(11) NOT NULL DEFAULT '0',
  `SimplesEst` tinyint(1) NOT NULL DEFAULT '0',
  `DirNFeGer` varchar(255) DEFAULT NULL,
  `DirNFeAss` varchar(255) DEFAULT NULL,
  `DirEnvLot` varchar(255) DEFAULT NULL,
  `DirRec` varchar(255) DEFAULT NULL,
  `DirPedRec` varchar(255) DEFAULT NULL,
  `DirProRec` varchar(255) DEFAULT NULL,
  `DirDen` varchar(255) DEFAULT NULL,
  `DirPedCan` varchar(255) DEFAULT NULL,
  `DirCan` varchar(255) DEFAULT NULL,
  `DirPedInu` varchar(255) DEFAULT NULL,
  `DirInu` varchar(255) DEFAULT NULL,
  `DirPedSit` varchar(255) DEFAULT NULL,
  `DirSit` varchar(255) DEFAULT NULL,
  `DirPedSta` varchar(255) DEFAULT NULL,
  `DirSta` varchar(255) DEFAULT NULL,
  `UF_WebServ` char(2) NOT NULL DEFAULT '??',
  `UF_Servico` varchar(10) NOT NULL DEFAULT '??',
  `NFeUF_EPEC` varchar(10) NOT NULL DEFAULT 'SVC-RS',
  `SiglaCustm` varchar(15) NOT NULL DEFAULT 'CUSTOMIZADO',
  `InfoPerCuz` tinyint(1) NOT NULL DEFAULT '1',
  `PathLogoNF` varchar(255) DEFAULT NULL,
  `CtaServico` int(11) NOT NULL DEFAULT '0',
  `CtaServicoPg` int(11) NOT NULL DEFAULT '0',
  `TxtServico` varchar(100) NOT NULL DEFAULT 'MO',
  `TxtServicoPg` varchar(100) NOT NULL DEFAULT 'PS',
  `DupServico` char(3) NOT NULL DEFAULT 'MO-',
  `DupServicoPg` char(3) NOT NULL DEFAULT 'PS-',
  `DupProdVen` char(3) NOT NULL DEFAULT 'VP-',
  `DupProdCom` char(3) NOT NULL DEFAULT 'CP-',
  `PedVdaMudLista` tinyint(1) NOT NULL DEFAULT '2',
  `PedVdaMudPrazo` tinyint(1) NOT NULL DEFAULT '2',
  `NFeSerNum` varchar(255) DEFAULT NULL,
  `NFeSerVal` date NOT NULL DEFAULT '0000-00-00',
  `NFeSerAvi` tinyint(3) NOT NULL DEFAULT '30',
  `DirDANFEs` varchar(255) DEFAULT NULL,
  `DirNFeProt` varchar(255) DEFAULT NULL,
  `PreMailAut` int(11) NOT NULL DEFAULT '-1',
  `PreMailCan` int(11) NOT NULL DEFAULT '-2',
  `PreMailEveCCe` int(11) NOT NULL DEFAULT '-3',
  `versao` double(4,2) NOT NULL DEFAULT '2.00',
  `NT2018_05v120` tinyint(1) NOT NULL DEFAULT '1',
  `NFeVerStaSer` double(4,2) NOT NULL DEFAULT '2.00',
  `NFeVerEnvLot` double(4,2) NOT NULL DEFAULT '2.00',
  `NFeVerConLot` double(4,2) NOT NULL DEFAULT '2.00',
  `NFeVerCanNFe` double(4,2) NOT NULL DEFAULT '2.00',
  `NFeVerInuNum` double(4,2) NOT NULL DEFAULT '2.00',
  `NFeVerConNFe` double(4,2) NOT NULL DEFAULT '2.01',
  `NFeVerLotEve` double(4,2) NOT NULL DEFAULT '2.00',
  `ide_mod` tinyint(2) NOT NULL DEFAULT '55',
  `ide_tpImp` tinyint(1) NOT NULL DEFAULT '1',
  `ide_tpAmb` tinyint(1) NOT NULL DEFAULT '2',
  `AppCode` tinyint(1) NOT NULL DEFAULT '0',
  `AssDigMode` tinyint(1) NOT NULL DEFAULT '0',
  `EntiTipCto` int(11) NOT NULL DEFAULT '0',
  `EntiTipCt1` int(11) NOT NULL DEFAULT '0',
  `MyEmailNFe` varchar(255) DEFAULT NULL,
  `DirSchema` varchar(255) DEFAULT NULL,
  `pCredSNAlq` double(15,2) NOT NULL DEFAULT '0.00',
  `pCredSNMez` int(11) NOT NULL DEFAULT '0',
  `DirNFeRWeb` varchar(255) DEFAULT NULL,
  `DirEveEnvLot` varchar(255) DEFAULT NULL,
  `DirEveRetLot` varchar(255) DEFAULT NULL,
  `DirEvePedCCe` varchar(255) DEFAULT NULL,
  `DirEveRetCCe` varchar(255) DEFAULT NULL,
  `DirEveProcCCe` varchar(255) DEFAULT NULL,
  `DirEvePedCan` varchar(255) DEFAULT NULL,
  `DirEveRetCan` varchar(255) DEFAULT NULL,
  `DirRetNfeDes` varchar(255) DEFAULT NULL,
  `NFeVerConDes` double(4,2) NOT NULL DEFAULT '1.01',
  `DirEvePedMDe` varchar(255) DEFAULT NULL,
  `DirEveRetMDe` varchar(255) DEFAULT NULL,
  `UF_MDeMDe` varchar(10) NOT NULL DEFAULT 'AN',
  `UF_MDeDes` varchar(10) NOT NULL DEFAULT 'AN',
  `UF_MDeNFe` varchar(10) NOT NULL DEFAULT 'AN',
  `DirDowNFeDes` varchar(255) DEFAULT NULL,
  `DirDowNFeNFe` varchar(255) DEFAULT NULL,
  `NFeNT2013_003LTT` tinyint(1) NOT NULL DEFAULT '0',
  `NFeInfCpl` int(11) NOT NULL DEFAULT '0',
  `NFeVerConsCad` double(4,2) NOT NULL DEFAULT '2.00',
  `NFeVerDistDFeInt` double(4,2) NOT NULL DEFAULT '1.00',
  `UF_DistDFeInt` varchar(10) NOT NULL DEFAULT 'AN',
  `DirDistDFeInt` varchar(255) DEFAULT NULL,
  `DirRetDistDFeInt` varchar(255) DEFAULT NULL,
  `NFeVerDowNFe` double(4,2) NOT NULL DEFAULT '2.00',
  `DirDowNFeCnf` varchar(255) DEFAULT NULL,
  `NoDANFEMail` tinyint(1) NOT NULL DEFAULT '0',
  `infRespTec_Usa` tinyint(1) NOT NULL DEFAULT '0',
  `infRespTec_CNPJ` varchar(14) NOT NULL DEFAULT '03143014000152',
  `infRespTec_xContato` varchar(60) NOT NULL DEFAULT 'Cleide Merli Arend',
  `infRespTec_email` varchar(60) NOT NULL DEFAULT 'cleide@dermatek.com.br',
  `infRespTec_fone` varchar(14) NOT NULL DEFAULT '04430315027',
  `infRespTec_idCSRT` char(2) NOT NULL DEFAULT '00',
  `infRespTec_CSRT` varchar(120) DEFAULT NULL,
  `NFSeMetodo` int(11) NOT NULL DEFAULT '0',
  `NFSeVersao` double(4,2) NOT NULL DEFAULT '2.01',
  `NFSeTipoRps` tinyint(1) NOT NULL DEFAULT '0',
  `NFSeSerieRps` varchar(5) DEFAULT NULL,
  `DpsNumero` int(11) NOT NULL DEFAULT '0',
  `HomNumLote` int(11) NOT NULL DEFAULT '0',
  `RPSNumLote` int(11) NOT NULL DEFAULT '0',
  `NFSeWSProducao` varchar(255) DEFAULT NULL,
  `NFSeWSHomologa` varchar(255) DEFAULT NULL,
  `NFSeAmbiente` tinyint(1) NOT NULL DEFAULT '0',
  `NFSeMetodEnvRPS` tinyint(1) NOT NULL DEFAULT '0',
  `NFSeTipCtoMail` int(11) NOT NULL DEFAULT '0',
  `DirNFSeDPSGer` varchar(255) DEFAULT NULL,
  `DirNFSeDPSAss` varchar(255) DEFAULT NULL,
  `DirNFSeRPSEnvLot` varchar(255) DEFAULT NULL,
  `DirNFSeRPSRecLot` varchar(255) DEFAULT NULL,
  `NFSeCertDigital` varchar(255) DEFAULT NULL,
  `NFSeCertValidad` date NOT NULL DEFAULT '0000-00-00',
  `NFSeCertAviExpi` tinyint(255) NOT NULL DEFAULT '30',
  `DirNFSeLogs` varchar(255) DEFAULT NULL,
  `DirNFSeSchema` varchar(255) DEFAULT NULL,
  `NFSeLogoPref` varchar(255) DEFAULT NULL,
  `NFSeLogoFili` varchar(255) DEFAULT NULL,
  `NFSeUserWeb` varchar(60) DEFAULT NULL,
  `NFSeSenhaWeb` varchar(60) DEFAULT NULL,
  `NFSeCodMunici` int(11) NOT NULL DEFAULT '0',
  `NFSePrefeitura1` varchar(100) DEFAULT NULL,
  `NFSePrefeitura2` varchar(100) DEFAULT NULL,
  `NFSePrefeitura3` varchar(100) DEFAULT NULL,
  `NFSeMsgVisu` tinyint(1) NOT NULL DEFAULT '0',
  `NFSePreMailAut` int(11) NOT NULL DEFAULT '-6',
  `NFSePreMailCan` int(11) NOT NULL DEFAULT '-7',
  `MyEmailNFSe` varchar(255) DEFAULT NULL,
  `Nfse_WhatsApp_Msg` text,
  `Nfse_WhatsApp_EntiTipCto` int(11) NOT NULL DEFAULT '0',
  `FisFax` varchar(30) DEFAULT NULL,
  `FisContNom` varchar(28) DEFAULT NULL,
  `FisContTel` varchar(30) DEFAULT NULL,
  `Reg10` tinyint(1) NOT NULL DEFAULT '1',
  `Reg11` tinyint(1) NOT NULL DEFAULT '1',
  `Reg50` tinyint(1) NOT NULL DEFAULT '0',
  `Reg51` tinyint(1) NOT NULL DEFAULT '0',
  `Reg53` tinyint(1) NOT NULL DEFAULT '0',
  `Reg54` tinyint(1) NOT NULL DEFAULT '0',
  `Reg55` tinyint(1) NOT NULL DEFAULT '0',
  `Reg56` tinyint(1) NOT NULL DEFAULT '0',
  `Reg57` tinyint(1) NOT NULL DEFAULT '0',
  `Reg60M` tinyint(1) NOT NULL DEFAULT '0',
  `Reg60A` tinyint(1) NOT NULL DEFAULT '0',
  `Reg60D` tinyint(1) NOT NULL DEFAULT '0',
  `Reg60I` tinyint(1) NOT NULL DEFAULT '0',
  `Reg60R` tinyint(1) NOT NULL DEFAULT '0',
  `Reg61` tinyint(1) NOT NULL DEFAULT '0',
  `Reg61R` tinyint(1) NOT NULL DEFAULT '0',
  `Reg70` tinyint(1) NOT NULL DEFAULT '0',
  `Reg71` tinyint(1) NOT NULL DEFAULT '0',
  `Reg74` tinyint(1) NOT NULL DEFAULT '0',
  `Reg75` tinyint(1) NOT NULL DEFAULT '0',
  `Reg76` tinyint(1) NOT NULL DEFAULT '0',
  `Reg77` tinyint(1) NOT NULL DEFAULT '0',
  `Reg85` tinyint(1) NOT NULL DEFAULT '0',
  `Reg86` tinyint(1) NOT NULL DEFAULT '0',
  `Reg90` tinyint(1) NOT NULL DEFAULT '1',
  `SINTEGRA_Path` varchar(255) DEFAULT NULL,
  `UsaReferen` tinyint(1) NOT NULL DEFAULT '0',
  `Estq0UsoCons` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_IND_PERFIL` char(1) NOT NULL DEFAULT '?',
  `SPED_EFD_IND_ATIV` tinyint(1) NOT NULL DEFAULT '9',
  `SPED_EFD_CadContador` int(11) NOT NULL DEFAULT '0',
  `SPED_EFD_CRCContador` varchar(15) DEFAULT NULL,
  `SPED_EFD_EscriContab` int(11) NOT NULL DEFAULT '0',
  `SPED_EFD_EnderContab` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_Path` varchar(255) DEFAULT NULL,
  `SPED_EFD_DtFiscal` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_ID_0150` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_ID_0200` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_Peri_E100` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_Peri_E500` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_Peri_K100` tinyint(1) NOT NULL DEFAULT '0',
  `SPED_EFD_Producao` tinyint(1) NOT NULL DEFAULT '1',
  `SPED_EFD_ICMS_IPI_VersaoGuia` varchar(15) DEFAULT NULL,
  `SPED_EFD_MovSubPrd` tinyint(1) NOT NULL DEFAULT '0',
  `NFeItsLin` tinyint(1) NOT NULL DEFAULT '3',
  `NFeFTRazao` tinyint(2) NOT NULL DEFAULT '12',
  `NFeFTEnder` tinyint(2) NOT NULL DEFAULT '8',
  `NFeFTFones` tinyint(2) NOT NULL DEFAULT '8',
  `NFeMaiusc` tinyint(1) NOT NULL DEFAULT '0',
  `NFeShowURL` varchar(255) DEFAULT NULL,
  `NFetpEmis` tinyint(1) NOT NULL DEFAULT '1',
  `SCAN_Ser` mediumint(3) NOT NULL DEFAULT '900',
  `SCAN_nNF` int(11) NOT NULL DEFAULT '0',
  `TZD_UTC_Auto_TermoAceite` int(11) NOT NULL DEFAULT '0',
  `TZD_UTC_Auto_DataHoraAceite` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TZD_UTC_Auto_UsuarioAceite` int(11) NOT NULL DEFAULT '0',
  `TZD_UTC_Auto` tinyint(1) NOT NULL DEFAULT '0',
  `TZD_UTC` double(15,10) NOT NULL DEFAULT '0.0011820331',
  `hVeraoAsk` date NOT NULL DEFAULT '0000-00-00',
  `hVeraoIni` date NOT NULL DEFAULT '0000-00-00',
  `hVeraoFim` date NOT NULL DEFAULT '0000-00-00',
  `NFe_indFinalCpl` tinyint(1) NOT NULL DEFAULT '1',
  `NFSe_indFinalCpl` tinyint(1) NOT NULL DEFAULT '1',
  `CartEmisHonFun` int(11) NOT NULL DEFAULT '0',
  `DirCTeGer` varchar(255) DEFAULT NULL,
  `DirCTeAss` varchar(255) DEFAULT NULL,
  `DirCTeEnvLot` varchar(255) DEFAULT NULL,
  `DirCTeRec` varchar(255) DEFAULT NULL,
  `DirCTePedRec` varchar(255) DEFAULT NULL,
  `DirCTeProRec` varchar(255) DEFAULT NULL,
  `DirCTeDen` varchar(255) DEFAULT NULL,
  `DirCTePedCan` varchar(255) DEFAULT NULL,
  `DirCTeCan` varchar(255) DEFAULT NULL,
  `DirCTePedInu` varchar(255) DEFAULT NULL,
  `DirCTeInu` varchar(255) DEFAULT NULL,
  `DirCTePedSit` varchar(255) DEFAULT NULL,
  `DirCTeSit` varchar(255) DEFAULT NULL,
  `DirCTePedSta` varchar(255) DEFAULT NULL,
  `DirCTeSta` varchar(255) DEFAULT NULL,
  `CTeUF_WebServ` char(2) NOT NULL DEFAULT '??',
  `CTeUF_Servico` varchar(10) NOT NULL DEFAULT '??',
  `PathLogoCTe` varchar(255) DEFAULT NULL,
  `CtaFretPrest` int(11) NOT NULL DEFAULT '0',
  `TxtFretPrest` varchar(100) NOT NULL DEFAULT 'FP',
  `DupFretPrest` char(3) NOT NULL DEFAULT 'FP-',
  `CTeSerNum` varchar(255) DEFAULT NULL,
  `CTeSerVal` date NOT NULL DEFAULT '0000-00-00',
  `CTeSerAvi` tinyint(3) NOT NULL DEFAULT '30',
  `DirDACTes` varchar(255) DEFAULT NULL,
  `DirCTeProt` varchar(255) DEFAULT NULL,
  `CTePreMailAut` int(11) NOT NULL DEFAULT '-1',
  `CTePreMailCan` int(11) NOT NULL DEFAULT '-2',
  `CTePreMailEveCCe` int(11) NOT NULL DEFAULT '-3',
  `CTeversao` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerStaSer` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerEnvLot` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerConLot` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerCanCTe` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerInuNum` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerConCTe` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerEPEC` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerLotEve` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeide_mod` tinyint(2) NOT NULL DEFAULT '57',
  `CTeide_tpImp` tinyint(1) NOT NULL DEFAULT '1',
  `CTeide_tpAmb` tinyint(1) NOT NULL DEFAULT '2',
  `CTeAppCode` tinyint(1) NOT NULL DEFAULT '0',
  `CTeAssDigMode` tinyint(1) NOT NULL DEFAULT '0',
  `CTeEntiTipCto` int(11) NOT NULL DEFAULT '0',
  `CTeEntiTipCt1` int(11) NOT NULL DEFAULT '0',
  `MyEmailCTe` varchar(255) DEFAULT NULL,
  `DirCTeSchema` varchar(255) DEFAULT NULL,
  `DirCTeRWeb` varchar(255) DEFAULT NULL,
  `DirCTeEveEnvLot` varchar(255) DEFAULT NULL,
  `DirCTeEveRetLot` varchar(255) DEFAULT NULL,
  `DirCTeEvePedCCe` varchar(255) DEFAULT NULL,
  `DirCTeEveRetCCe` varchar(255) DEFAULT NULL,
  `DirCTeEveProcCCe` varchar(255) DEFAULT NULL,
  `DirCTeEvePedCan` varchar(255) DEFAULT NULL,
  `DirCTeEveRetCan` varchar(255) DEFAULT NULL,
  `DirCTeRetCTeDes` varchar(255) DEFAULT NULL,
  `CTeCTeVerConDes` double(4,2) NOT NULL DEFAULT '0.00',
  `DirCTeEvePedMDe` varchar(255) DEFAULT NULL,
  `DirCTeEveRetMDe` varchar(255) DEFAULT NULL,
  `DirCTeEvePedEPEC` varchar(255) DEFAULT NULL,
  `DirCTeEveRetEPEC` varchar(255) DEFAULT NULL,
  `DirDowCTeDes` varchar(255) DEFAULT NULL,
  `DirDowCTeCTe` varchar(255) DEFAULT NULL,
  `CTeInfCpl` int(11) NOT NULL DEFAULT '0',
  `CTeVerConsCad` double(4,2) NOT NULL DEFAULT '2.00',
  `CTeVerDistDFeInt` double(4,2) NOT NULL DEFAULT '0.00',
  `CTeVerDowCTe` double(4,2) NOT NULL DEFAULT '2.00',
  `DirDowCTeCnf` varchar(255) DEFAULT NULL,
  `CTeItsLin` tinyint(1) NOT NULL DEFAULT '3',
  `CTeFTRazao` tinyint(2) NOT NULL DEFAULT '12',
  `CTeFTEnder` tinyint(2) NOT NULL DEFAULT '8',
  `CTeFTFones` tinyint(2) NOT NULL DEFAULT '8',
  `CTeMaiusc` tinyint(1) NOT NULL DEFAULT '0',
  `CTeShowURL` varchar(255) DEFAULT NULL,
  `CTetpEmis` tinyint(1) NOT NULL DEFAULT '1',
  `CTeSCAN_Ser` mediumint(3) NOT NULL DEFAULT '900',
  `CTeSCAN_nCT` int(11) NOT NULL DEFAULT '0',
  `CTe_indFinalCpl` tinyint(1) NOT NULL DEFAULT '1',
  `ParamsCTe` int(11) NOT NULL DEFAULT '0',
  `CTeUF_EPEC` varchar(10) NOT NULL DEFAULT 'SVSP',
  `CTeUF_Conting` char(2) NOT NULL DEFAULT 'SP',
  `DirMDFeGer` varchar(255) DEFAULT NULL,
  `DirMDFeAss` varchar(255) DEFAULT NULL,
  `DirMDFeEnvLot` varchar(255) DEFAULT NULL,
  `DirMDFeRec` varchar(255) DEFAULT NULL,
  `DirMDFePedRec` varchar(255) DEFAULT NULL,
  `DirMDFeProRec` varchar(255) DEFAULT NULL,
  `DirMDFeDen` varchar(255) DEFAULT NULL,
  `DirMDFePedCan` varchar(255) DEFAULT NULL,
  `DirMDFeCan` varchar(255) DEFAULT NULL,
  `DirMDFePedInu` varchar(255) DEFAULT NULL,
  `DirMDFeInu` varchar(255) DEFAULT NULL,
  `DirMDFePedSit` varchar(255) DEFAULT NULL,
  `DirMDFeSit` varchar(255) DEFAULT NULL,
  `DirMDFePedSta` varchar(255) DEFAULT NULL,
  `DirMDFeSta` varchar(255) DEFAULT NULL,
  `MDFeUF_WebServ` char(2) NOT NULL DEFAULT '??',
  `MDFeUF_Servico` varchar(10) NOT NULL DEFAULT '??',
  `PathLogoMDFe` varchar(255) DEFAULT NULL,
  `MDFeSerNum` varchar(255) DEFAULT NULL,
  `MDFeSerVal` date NOT NULL DEFAULT '0000-00-00',
  `MDFeSerAvi` tinyint(3) NOT NULL DEFAULT '30',
  `DirDAMDFes` varchar(255) DEFAULT NULL,
  `DirMDFeProt` varchar(255) DEFAULT NULL,
  `MDFePreMailAut` int(11) NOT NULL DEFAULT '-1',
  `MDFePreMailCan` int(11) NOT NULL DEFAULT '-2',
  `MDFePreMailEveCCe` int(11) NOT NULL DEFAULT '-3',
  `MDFeversao` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerStaSer` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerEnvLot` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerConLot` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerCanMDFe` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerInuNum` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerConMDFe` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerNaoEnc` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerLotEve` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeide_mod` tinyint(2) NOT NULL DEFAULT '58',
  `MDFeide_tpImp` tinyint(1) NOT NULL DEFAULT '1',
  `MDFeide_tpAmb` tinyint(1) NOT NULL DEFAULT '2',
  `MDFeAppCode` tinyint(1) NOT NULL DEFAULT '0',
  `MDFeAssDigMode` tinyint(1) NOT NULL DEFAULT '0',
  `MDFeEntiTipCto` int(11) NOT NULL DEFAULT '0',
  `MDFeEntiTipCt1` int(11) NOT NULL DEFAULT '0',
  `MyEmailMDFe` varchar(255) DEFAULT NULL,
  `DirMDFeSchema` varchar(255) DEFAULT NULL,
  `DirMDFeRWeb` varchar(255) DEFAULT NULL,
  `DirMDFeEveEnvLot` varchar(255) DEFAULT NULL,
  `DirMDFeEveRetLot` varchar(255) DEFAULT NULL,
  `DirMDFeEvePedEnc` varchar(255) DEFAULT NULL,
  `DirMDFeEveRetEnc` varchar(255) DEFAULT NULL,
  `DirMDFeEveProcEnc` varchar(255) DEFAULT NULL,
  `DirMDFeEvePedCan` varchar(255) DEFAULT NULL,
  `DirMDFeEveRetCan` varchar(255) DEFAULT NULL,
  `DirMDFeRetMDFeDes` varchar(255) DEFAULT NULL,
  `DirMDFeEvePedIdC` varchar(255) DEFAULT NULL,
  `DirMDFeEveRetIdC` varchar(255) DEFAULT NULL,
  `MDFeMDFeVerConDes` double(4,2) NOT NULL DEFAULT '0.00',
  `DirMDFeEvePedMDe` varchar(255) DEFAULT NULL,
  `DirMDFeEveRetMDe` varchar(255) DEFAULT NULL,
  `DirDowMDFeDes` varchar(255) DEFAULT NULL,
  `DirDowMDFeMDFe` varchar(255) DEFAULT NULL,
  `MDFeInfCpl` int(11) NOT NULL DEFAULT '0',
  `MDFeVerConsCad` double(4,2) NOT NULL DEFAULT '1.00',
  `MDFeVerDistDFeInt` double(4,2) NOT NULL DEFAULT '0.00',
  `MDFeVerDowMDFe` double(4,2) NOT NULL DEFAULT '1.00',
  `DirDowMDFeCnf` varchar(255) DEFAULT NULL,
  `MDFeItsLin` tinyint(1) NOT NULL DEFAULT '3',
  `MDFeFTRazao` tinyint(2) NOT NULL DEFAULT '12',
  `MDFeFTEnder` tinyint(2) NOT NULL DEFAULT '8',
  `MDFeFTFones` tinyint(2) NOT NULL DEFAULT '8',
  `MDFeMaiusc` tinyint(1) NOT NULL DEFAULT '0',
  `MDFeShowURL` varchar(255) DEFAULT NULL,
  `MDFetpEmis` tinyint(1) NOT NULL DEFAULT '1',
  `MDFeSCAN_Ser` mediumint(3) NOT NULL DEFAULT '900',
  `MDFeSCAN_nMDF` int(11) NOT NULL DEFAULT '0',
  `MDFe_indFinalCpl` tinyint(1) NOT NULL DEFAULT '1',
  `ParamsMDFe` int(11) NOT NULL DEFAULT '0',
  `RetImpost` tinyint(1) NOT NULL DEFAULT '1',
  `FreteRpICMS` double(15,4) NOT NULL DEFAULT '0.0000',
  `FreteRpPIS` double(15,4) NOT NULL DEFAULT '0.0000',
  `FreteRpCOFINS` double(15,4) NOT NULL DEFAULT '0.0000',
  `PlanoPadrao` int(11) NOT NULL DEFAULT '0',
  `Lk` int(11) NOT NULL DEFAULT '0',
  `DataCad` date DEFAULT NULL,
  `DataAlt` date DEFAULT NULL,
  `UserCad` int(11) NOT NULL DEFAULT '0',
  `UserAlt` int(11) NOT NULL DEFAULT '0',
  `AlterWeb` tinyint(1) NOT NULL DEFAULT '1',
  `AWServerID` int(11) NOT NULL DEFAULT '0',
  `AWStatSinc` tinyint(1) NOT NULL DEFAULT '0',
  `Ativo` tinyint(1) NOT NULL DEFAULT '1',
  `CSC` varchar(36) DEFAULT NULL,
  `CSC_pos` int(11) NOT NULL DEFAULT '0',
  `SCAN_nNFC` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
*)


(*
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'INSERT INTO `paramsemp` VALUES (' +
  '-11,1,0,0,0,1,2,0,0.000,0,0,2,1,2,"VP","CP",1,"/",0,0,"",0,0,3,0,0,' +
  '"C:\\Dermatek\\NFe\\Tisolin","C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\NfeAss",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EnvLot",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\Rec",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\PedRec",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\ProRec",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\Den",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\PedCan",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\Can",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\PedInu",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\Inu"' +
  ',"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\PedSit",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\Sit",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\PedSta",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\Sta",' +
  '"PR","PR","SVC-RS","CUSTOMIZADO",1,"",3,4,"MO","PS","MO-","PS-","VP-","CP-",2,2,' +
  '"6D8C200528674E74","2021-06-01",30,"",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\NfeProt",' +
  '1,1,0,4.00,1,4.00,4.00,4.00,0.00,4.00,4.00,4.00,55,1,2,0,0,0,0,"","",0.00,0,' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\NfeWeb",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EveEnvLot",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EveRetLot",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EvePedCCe",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EveRetCCe",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EveProcCCe",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EvePedCan",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EveRetCan",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\RetNfeDes",1.01,' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EvePedMDe",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\EveRetMDe",' +
  '"AN","AN","AN","C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\DowNFeDes",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\DowNFeNFe",' +
  '0,0,4.00,0.00,"AN","C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\DistDFeInt",' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\RetDistDFeInt",0.00,' +
  '"C:\\Dermatek\\NFe\\Tisolin\\CNPJ_14608688000100\\DowNFeCnf",' +
  '0,0,"03143014000152","Cleide Merli Arend","cleide@dermatek.com.br",' +
  '"04430315027","00","",0,2.01,0,"",0,0,0,"","",0,0,0,"","","","","","1899-12-30",' +
  '30,"","","","","","",0,"","","",0,0,0,"",NULL,0,NULL,NULL,NULL,' +
  '1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,NULL,0,0,"?",9,0,"",0,0,"",' +
  '0,0,0,5,5,5,1,"",0,3,12,8,8,0,"",1,900,0,0,"0000-00-00 00:00:00",0,0,' +
  '-0.1250000000,"2021-09-30","1899-12-30","1899-12-30",1,1,0,' +
  '"","","","","","","","","","","","","","","","??","??","",5,"FP","FP-","",' +
  '"1899-12-30",30,"","",0,0,0,2.00,2.00,2.00,2.00,2.00,2.00,2.00,2.00,2.00,57,' +
  '1,2,0,0,0,0,"","","","","","","","","","",NULL,0.00,NULL,NULL,"","",' +
  'NULL,NULL,0,2.00,0.00,2.00,NULL,3,12,8,8,0,"",1,900,0,1,0,"SVSP","SP",' +
  '"","","","","","","","","","","","","","","","??","??","","","1899-12-30",30,' +
  '"","",0,0,0,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,58,1,2,0,0,0,0,' +
  '"","","","","","","",NULL,"","",NULL,"","",0.00,NULL,NULL,NULL,NULL,0,1.00,' +
  '0.00,1.00,NULL,3,12,8,8,0,"",1,900,0,1,0,1,0.0000,0.0000,0.0000,0,0,NULL,' +
  '"2020-10-23",0,-1,1,0,1,1,"' + CSC + '", ' + CSCpos + ',0);');
*)
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Marca;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo, Controle: Integer;
  Nome: String;
const
  OriTab = 'Marca';
  DstTab = 'grafabmar';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Controle, Nome) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDMarca.Close;
    SDMarca.DataSet.CommandType := ctQuery;
    SDMarca.DataSet.CommandText := SQL;
    SDMarca.Open;
    Continua := SDMarca.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDMarca.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDMarca.First;
      SQLRec    := '';
      while not SDMarca.Eof do
      begin
        Codigo   := 0;
        Controle := SDMarca.RecNo;
        Nome     := SDMarcaMARCA.Value;
        //
        if SDMarca.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Controle,
        + PV_N(Geral.FF0(Controle))
        //Nome
        + PV_N(Geral.VariavelToString(Nome))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDMarca.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDMarca.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_Produto();
var

  SQL_FIELDS_UNIDMED,  SQL_FIELDS_XXX,
  SQL_FIELDS_GraGruX, SQL_VALUES_GraGruX, SQLRec_GraGruX,
  SQL_FIELDS_GraGru1, SQL_VALUES_GraGru1, SQLRec_GraGru1,
  SQL_FIELDS_GraGruC, SQL_VALUES_GraGruC, SQLRec_GraGruC,
  SQL_FIELDS_GraGXPatr, SQL_VALUES_GraGXPatr, SQLRec_GraGXPatr,
  SQL_FIELDS_GraGXOutr, SQL_VALUES_GraGXOutr, SQLRec_GraGXOutr,
  SQL_FIELDS_GraGXServ, SQL_VALUES_GraGXServ, SQLRec_GraGXServ,
  SQL_FIELDS_GraGXVend, SQL_VALUES_GraGXVend, SQLRec_GraGXVend,
  SQL_FIELDS_GraGruEPat, SQL_VALUES_GraGruEPat, SQLRec_GraGruEPat,
  SQL_FIELDS_GraGruEVen, SQL_VALUES_GraGruEVen, SQLRec_GraGruEVen,
  SQL_FIELDS_StqBalIts, SQL_Values_StqBalIts, SQLRec_StqBalIts,
  SQL_FIELDS_StqBalCad, SQL_Values_StqBalCad, SQLRec_StqBalCad,
  SQL_FIELDS_StqMovItsA, SQL_Values_StqMovItsA, SQLRec_StqMovItsA,
  //SQL_FIELDS_StqMovCusA, SQL_Values_StqMovCusA, SQLRec_StqMovCusA,
  SQL_FIELDS_GraGruVal, SQL_Values_GraGruVal, SQLRec_GraGruVal,
  SQL: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Controle, GraGruC, GraGru1: Integer;


  //

////////////////////////////////////////////////////////////////////////////////
///  GraGru1
  Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu: Integer;
  Nome: String;
  PrdGrupTip, GraTamCad, UnidMed: Integer;
(*
  CST_A, CST_B,
*)
  NCM: String;
(*
Peso, IPI_Alq, IPI_CST,
IPI_cEnq, IPI_vUnid, IPI_TpTrib,
TipDimens, PerCuztMin, PerCuztMax,
MedOrdem, PartePrinc, InfAdProd,
SiglaCustm, HowBxaEstq, GerBxaEstq,
PIS_CST, PIS_AlqP, PIS_AlqV,
PISST_AlqP, PISST_AlqV, COFINS_CST,
COFINS_AlqP, COFINS_AlqV, COFINSST_AlqP,
COFINSST_AlqV, ICMS_modBC, ICMS_modBCST,
ICMS_pRedBC, ICMS_pRedBCST, ICMS_pMVAST,
ICMS_pICMSST, ICMS_Pauta, ICMS_MaxTab,
*)
  IPI_pIPI: Double;

  cGTIN_EAN: String;
(*
  EX_TIPI,
PIS_pRedBC, PISST_pRedBCST, COFINS_pRedBC,
COFINSST_pRedBCST, ICMSRec_pRedBC, IPIRec_pRedBC,
PISRec_pRedBC, COFINSRec_pRedBC, ICMSRec_pAliq,
IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq,
ICMSRec_tCalc, IPIRec_tCalc, PISRec_tCalc,
COFINSRec_tCalc, ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,
FatorClas, prod_indTot,
*)
  Referencia: String;
(*
Fabricante, PerComissF, PerComissR,
PerComissZ, NomeTemp, COD_LST,
SPEDEFD_ALIQ_ICMS, DadosFisc, GraTabApp,
ICMS_pDif, ICMS_pICMSDeson, ICMS_motDesICMS,
prod_CEST, prod_indEscala
*)

////////////////////////////////////////////////////////////////////////////////
///  GraGXPatr
var
(*  Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
  VendaData, VendaDocu,*) Observa, AGRPAT, (*CLVPAT,*) MARPAT,(*, DescrTerc*)
  PATRIMONIO: String;
  (*GraGruX, Situacao, Agrupador,*) Marca, (*VendaEnti,*) Aplicacao: Integer;
  AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr: Double;
  //
  _ATIVO, CATEGORIA: String;
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
///  GraGXPatr
  GraGruX, ItemUnid: Integer;
  ItemValr: Double;
////////////////////////////////////////////////////////////////////////////////
  UsaSubsTrib: Integer;
const
  OriTab   = 'Produto';
  GraTamI  = 1;
  COD_ITEM = '';
var
  DstTab, DstTab_GGX, DstTab_GG1, DstTab_GGC, DstTab_Patr, DstTab_Outr,
  DstTab_Serv, DstTab_Vend, DstTab_GGEPat, DstTab_GGEVen,
  DstTab_BalC, DstTab_BalI, DstTab_MovA, DstTab_CusA, DstTab_GGV: String; //= 'gragrux';
  GraGruY, I: Integer;
  //
  EquipamentosErr: String;
  //GraGruYs: array of String[1];
  //
  TipoProd: String[1];
  Estoque, CustoAll, CustoUni: Double;
  Ativo: Integer;
  ValorFDS: Double;
  DMenos: Integer;
  //StqBalCad
  Codigo, GrupoBal: Integer;
  Abertura: String;
  QtdLei: Double;
  //
  SBI, SMA, GGV: Integer;
  EAN13: String;
begin
  SBI := 0;
  SMA := 0;
  GGV := 0;
  Abertura := Geral.FDT(Trunc(DModG.ObtemAgora()), 109);
  //
{ Fazer assim?????     2020-10-01
SELECT ggx.GraGruY, Its.*
FROM zzz_fb_db_ant.LOCACAOITENS its
LEFT JOIN toolrent_tisolin.GraGruX ggx
  ON ggx.Controle=its.PRODUTO
/*WHERE its.CATEGORIA = "L"*/
ORDER BY ggx.GraGruY, its.LOCACAO, its.SEQUENCIA




tem esse erro atualmente!

DROP TABLE IF EXISTS _REDUZIDOS_PRI_;
CREATE TABLE _REDUZIDOS_PRI_
SELECT DISTINCT GraGruX
FROM loccpatpri pri
;

DROP TABLE IF EXISTS _REDUZIDOS_SEC_;
CREATE TABLE _REDUZIDOS_SEC_
SELECT DISTINCT GraGruX
FROM loccpatsec pri
;

SELECT pri.GraGruX GgxPri, sec.GraGruX GgxSec
FROM _REDUZIDOS_PRI_ pri
LEFT JOIN _REDUZIDOS_SEC_ sec ON sec.GraGruX=pri.GraGruX
;



Erros doe erros!
SELECT ggx.GraGruY, Its.*
FROM zzz_fb_db_ant.LOCACAOITENS its
LEFT JOIN toolrent_tisolin.GraGruX ggx
  ON ggx.Controle=its.PRODUTO
WHERE its.CATEGORIA = "L"
AND its.LOCACAO IN(
12,15,23,26,28,29,30,32,33,36,42,43,49,57,65,67,69,73,82,86,89,96,99,104,110,116,
126,138,146,147,148,153,156,162,164,168,171,178,179,180,190,192,193,194,201,206,208,214,222,224,225,236,238,239,241,243,246,260,271,272,273,278,284,
296,303,309,316,320,321,324,329,332,336,343,347,349,359,361,362,368,372,397,407,408,412,414,417,420,425,432,435,437,438,440,441,447,448,451,452,462,
464,477,483,484,487,488,492,493,494,495,496,497,499,511,516,517,518,525,527,529,534,535,540,541,544,549,550,561,566,567,568,574,582,583,591,593,594,
600,605,606,610,612,613,617,620,630,636,640,645,664,666,680,683,684,686,693,696,697,698,700,707,710,723,724,725,733,737,738,739,740,741,745,746,748,
753,763,764,765,772,773,774,778,779,780,785,787,792,796,799,802,821,824,828,839,840,842,853,854,858,862,869,880,886,891,900,902,903,904,905,914,917,
920,923,924,925,933,938,945,946,948,949,952,954,957,962,967,968,986,993,994,998,999,1000,1015,1021,1024,1026,1030,1033,1039,1042,1044,1045,1049,1057,
1061,1064,1069,1072,1075,1094,1097,1101,1104,111





}
  EquipamentosErr :=  '';
////////////////////////////////////////////////////////////////////////////////
///  Lista de GraGruY  /////////////////////////////////////////////////////////
     //Provis�rio! Ver lista excel!!!
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando lista de GraGruY');


(*
Patrim�nios secund�rios e Uso e consumo!
543 registros
==========================================
SELECT DISTINCT "" PRI, "" SEC, "" ACE,
"" ANC, "" USO, "" CNS,
IF(ace.ACESSORIO IS NULL, "NAO", "SIM")
 EhAcessorio, prd.PRODUTO, prd.DESCRICAO,
prd.Ativo, prd.Grupo,
gru.DESCRICAO GRUPO_DESCRICAO, prd.Estoque,
prd.PrecoVenda, prd.REFERENCIA,
prd.PATRIMONIO, prd.CATEGORIA, prd.VALORDIARIA,
prd.VALORSEMANAL, prd.VALORQUINZENAL,
prd.VALORMENSAL, prd.PRECOFDS, prd.REDUTORDIARIA
FROM PRODUTO prd
LEFT JOIN PRODUTOACESSORIO ace
  ON ace.ACESSORIO=prd.PRODUTO
LEFT JOIN GRUPO gru
  ON gru.GRUPO=prd.GRUPO
WHERE prd.CATEGORIA = "L"
AND (
  prd.ValorDiaria=0
  AND
  prd.PATRIMONIO = ""
)
AND prd.GRUPO>=0

ORDER BY prd.DESCRICAO
*)

(*
Patrim�nios Principal!
420 registros
==========================================
{SELECT ace.*, prd.*
FROM PRODUTO prd
LEFT JOIN PRODUTOACESSORIO ace
  ON ace.ACESSORIO=prd.PRODUTO
WHERE prd.CATEGORIA = "L"
AND (
  prd.ValorDiaria<>0
  OR
  prd.PATRIMONIO <> ""
)
AND GRUPO>=0
AND ace.Produto IS NULL
ORDER BY prd.DESCRICAO
*)

{
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruY, FbDB, [
  'DROP TABLE IF EXISTS _PRODUTO_ALL_; ',
  'CREATE TABLE _PRODUTO_ALL_ ',
  ' ',
  'SELECT "S" GGY_KIND, Ativo, DESCRICAO,  ',
  'PRODUTO, Linha, Categoria,  ',
  'Referencia, Patrimonio ',
  'FROM PRODUTO ',
  'WHERE Categoria = "L" ',
  'AND Patrimonio = "" ',
  ' ',
  'UNION ',
  ' ',
  'SELECT "P" GGY_KIND, Ativo, DESCRICAO,  ',
  'PRODUTO, Linha, Categoria,  ',
  'Referencia, Patrimonio ',
  'FROM PRODUTO ',
  'WHERE Categoria = "L" ',
  'AND Patrimonio <> "" ',
  ' ',
  'UNION',
  '',
  'SELECT "V" GGY_KIND, Ativo, DESCRICAO,  ',
  'PRODUTO, Linha, Categoria,  ',
  'Referencia, Patrimonio ',
  'FROM PRODUTO ',
  'WHERE Categoria = "V" ',
  ' ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS _PRODUTO_ACE_; ',
  'CREATE TABLE _PRODUTO_ACE_ ',
  ' ',
  'SELECT DISTINCT ACESSORIO ',
  'FROM PRODUTOACESSORIO ',
  '; ',
  ' ',
  'DROP TABLE IF EXISTS PRODUTOS_GGY; ',
  'CREATE TABLE PRODUTOS_GGY ',
  'SELECT IF(ace.ACESSORIO > 0, "A", pal.GGY_KIND) TipoProd,  ',
  'pal.*  ',
  'FROM _PRODUTO_ALL_ pal ',
  'LEFT JOIN PRODUTOACESSORIO ace ON ace.ACESSORIO=pal.PRODUTO ',
  ' ',
  '; ',
  'SELECT * FROM PRODUTOS_GGY',
  //'ORDER BY GGY_KIND DESC, DESCRICAO ',
  'ORDER BY PRODUTO DESC ',
  ';',
  '']);
  //
  QrGraGruY.First;
  SetLength(GraGruYs, QrGraGruYPRODUTO.Value + 1);
  for I := 0 to QrGraGruYPRODUTO.Value do
    GraGruYs[I] := '?';
  while not QrGraGruY.Eof do
  begin
    GraGruYs[QrGraGruYPRODUTO.Value] := QrGraGruYTipoProd.Value;
    //
    QrGraGruY.Next;
  end;
}
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamCad
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'unidmed';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_UNIDMED := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, CodUsu, Sigla, Nome, Sequencia, Grandeza) VALUES ',
    '(1,     1,      "UN","UNIDADE",   0,     0)']);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_UNIDMED);
  // Codigo  CodUsu, Sigla, Nome, Sequencia, Grandeza

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela gracorcad
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'gracorcad';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_XXX := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, CodUsu, PrintCor, Nome) VALUES (0,0,0,"�NICO"), (1,1,0,"�NICO")']);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_XXX);
  //
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamCad
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'gratamcad';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_XXX := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, CodUsu, PrintTam, Nome) VALUES (0,0,0,"�NICO"), (1,1,0,"�NICO")']);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_XXX);
  //
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamIts
////////////////////////////////////////////////////////////////////////////////
  DstTab := 'gratamits';
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ' + DstTab);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  SQL_FIELDS_XXX := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Controle, PrintTam, Nome) VALUES ',
    '(0,0,0,"�NICO"), ',
    '(1,1,0,"�NICO") '
    ]);
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, SQL_FIELDS_XXX);
  //
////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraTamIts
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
///  Recria��o tabela GraGuX e GraGru1
////////////////////////////////////////////////////////////////////////////////
  DstTab_GGC := 'gragruc';
  DstTab_GGX := 'gragrux';
  DstTab_GG1 := 'gragru1';
  DstTab_Patr := 'gragxpatr';
  DstTab_Outr := 'gragxoutr';
  DstTab_Serv := 'gragxserv';
  DstTab_Vend := 'gragxvend';
  DstTab_GGEPat := 'gragruepat';
  DstTab_GGEVen := 'gragrueven';
  DstTab_BalC   := 'stqbalcad';
  DstTab_BalI   := 'stqbalits';
  DstTab_MovA   := 'stqmovitsa';
  DstTab_CusA   := 'stqmovcusa';
  DstTab_GGV    := 'gragruval';

  MySQLBatchExecute1.Database := Dmod.MyDB;
  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruC := Geral.ATS([
    'INSERT INTO ' + DstTab_GGC + ' ( ',
    'Controle, Nivel1, GraCorCad, GraCmpCad) VALUES ']);
  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruX := Geral.ATS([
    'INSERT INTO ' + DstTab_GGX + ' ( ',
    'Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM, Ativo) VALUES ']);
  //////////////////////////////////////////////////////////
  SQL_FIELDS_GraGru1 := Geral.ATS([
    'INSERT INTO ' + DstTab_GG1 + ' ( ',

  'Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu, ', //: Integer
  'Nome, ', //: String;
  'PrdGrupTip, GraTamCad, UnidMed, ', //: Integer;
(*
  CST_A, CST_B,
*)
  'NCM, ', //: String;
(*
Peso, IPI_Alq, IPI_CST,
IPI_cEnq, IPI_vUnid, IPI_TpTrib,
TipDimens, PerCuztMin, PerCuztMax,
MedOrdem, PartePrinc, InfAdProd,
SiglaCustm, HowBxaEstq, GerBxaEstq,
PIS_CST, PIS_AlqP, PIS_AlqV,
PISST_AlqP, PISST_AlqV, COFINS_CST,
COFINS_AlqP, COFINS_AlqV, COFINSST_AlqP,
COFINSST_AlqV, ICMS_modBC, ICMS_modBCST,
ICMS_pRedBC, ICMS_pRedBCST, ICMS_pMVAST,
ICMS_pICMSST, ICMS_Pauta, ICMS_MaxTab,
*)
  'IPI_pIPI, ', //: Double;

  'cGTIN_EAN, ', //: String;
(*
  EX_TIPI,
PIS_pRedBC, PISST_pRedBCST, COFINS_pRedBC,
COFINSST_pRedBCST, ICMSRec_pRedBC, IPIRec_pRedBC,
PISRec_pRedBC, COFINSRec_pRedBC, ICMSRec_pAliq,
IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq,
ICMSRec_tCalc, IPIRec_tCalc, PISRec_tCalc,
COFINSRec_tCalc, ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,
FatorClas, prod_indTot,
*)
  'Referencia, Patrimonio, ', //: String;
(*
Fabricante, PerComissF, PerComissR,
PerComissZ, NomeTemp, COD_LST,
SPEDEFD_ALIQ_ICMS, DadosFisc, GraTabApp,
ICMS_pDif, ICMS_pICMSDeson, ICMS_motDesICMS,
prod_CEST, prod_indEscala
*)
  'UsaSubsTrib', // 2021-03-25

    ') VALUES ']);
  //

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGXPatr := Geral.ATS([
    'INSERT INTO ' + DstTab_Patr + ' ( ',
    'GraGruX, ',
  (*Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
  VendaData, VendaDocu,*)
  'Observa, ',
  'AGRPAT, ',
  //CLVPAT,*)
  'MARPAT, ',
  (*, DescrTerc, Situacao, Agrupador,*)
  'Marca, ',(*VendaEnti,*)
  'Aplicacao, ',
  'AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr, ',
  'ValorFDS, DMenos',
  ') VALUES ']);

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGXOutr := Geral.ATS([
    'INSERT INTO ' + DstTab_Outr + ' ( ',
    'GraGruX, Aplicacao, ItemValr, ItemUnid) VALUES ']);

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruEPat := Geral.ATS([
    'INSERT INTO ' + DstTab_GGEPat + ' ( ',
    'Empresa, GraGruX, EstqAnt, EstqEnt, EstqSai, EstqLoc, EstqSdo) VALUES ']);
  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGruEVen := Geral.ATS([
    'INSERT INTO ' + DstTab_GGEVen + ' ( ',
    'GraGruX, EstqAnt, EstqEnt, EstqSai, EstqLoc, EstqSdo) VALUES ']);
  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGXServ := Geral.ATS([
    'INSERT INTO ' + DstTab_Serv + ' ( ',
    'GraGruX, Aplicacao, ItemValr, ItemUnid) VALUES ']);

  ///////////////////////////////////////////////////////
  SQL_FIELDS_GraGXVend := Geral.ATS([
    'INSERT INTO ' + DstTab_Vend + ' ( ',
    'GraGruX, Aplicacao, ItemValr, ItemUnid, Marca, MARPAT) VALUES ']);

  ///////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////
  SQL_FIELDS_StqBalCad := Geral.ATS([
    'INSERT INTO ' + DstTab_BalC + ' ( ',
(*      Mais simples fazer direto!
    Codigo'
    'CodUsu', 'Nome', 'Empresa',
    'PrdGrupTip', 'StqCenCad', 'CasasProd',
    'Abertura',
    'Encerrou',
    'Status',
    'GrupoBal', 'Forcada'
*)
    ') VALUES ']);


  ///////////////////////////////////////////////////////
  SQL_FIELDS_StqBalIts := Geral.ATS([
    'INSERT INTO ' + DstTab_BalI + ' ( ',
    'Controle, Codigo, Tipo, DataHora, StqCenLoc, GraGruX, QtdLei, QtdAnt) VALUES ']);

  ///////////////////////////////////////////////////////
  SQL_FIELDS_StqMovItsA := Geral.ATS([
    'INSERT INTO ' + DstTab_MovA + ' ( ',
    'IDCtrl, DataHora, Tipo, OriCodi, OriCtrl, OriCnta, Empresa, ' +
    'StqCenCad, StqCenLoc, GraGruX, Qtde, CustoAll, ValorAll, '+
    'GrupoBal, Baixa, AntQtde, UnidMed, ValiStq) VALUES ']);

  ///////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////


(*
  SQL_FIELDS_StqMovCusA := Geral.ATS([
    'INSERT INTO ' + DstTab_CusA + ' ( ',
    'Empresa, StqCenCad, GraGruX, Qtde, ValorTotal, UltPreco, '+
    'CustoPadrao, CustoMedio, RegBaseCont, RegBaseQtde) VALUES ']);
*)
  SQL_FIELDS_GraGruVal := Geral.ATS([
    'INSERT INTO ' + DstTab_GGV + ' ( ',
    'Controle, GraGruX, GraGru1, GraCusPrc, ',
    'CustoPreco, Entidade, DataIni) VALUES ']);
  ///////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////



  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Cores (GGX x GG1)');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGC);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Reduzidos');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGX);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. N�vel 1');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GG1);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Patrim�nio');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_Patr);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Outros');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_Outr);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Servi�os');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_Serv);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Produtos de venda');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_Vend);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Estoque Patrim�nio');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGEPat);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Estoque Venda');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGEVen);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Balanco Cabe�alho');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_BalC);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Balanco Mercadorias');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_BalI);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Estoque Mercadorias');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_MovA);
  //
  //MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Custo Mercadorias');
  //UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_CusA);
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. Custo Mercadorias');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab_GGV);
  //
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDProduto.Close;
    SDProduto.DataSet.CommandType := ctQuery;
    SDProduto.DataSet.CommandText := SQL;
    SDProduto.Open;
    Continua := SDProduto.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDProduto.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES_GraGruX    := '';
      SQL_VALUES_GraGru1    := '';
      SQL_VALUES_GraGruC    := '';
      SQL_VALUES_GraGXPatr  := '';
      SQL_VALUES_GraGXOutr  := '';
      SQL_VALUES_GraGXServ  := '';
      SQL_VALUES_GraGXVend  := '';
      SQL_VALUES_GraGruEPat := '';
      SQL_VALUES_GraGruEVen := '';
      SQL_Values_StqBalCad  := '';
      SQL_Values_StqBalIts  := '';
      SQL_Values_StqMovItsA := '';
      //SQL_Values_StqMovCusA := '';
      SQL_Values_GraGruVal  := '';

      SDProduto.First;

      SQLRec_GraGruX     := '';
      SQLRec_GraGru1     := '';
      SQLRec_GraGruC     := '';
      SQLRec_GraGXPatr   := '';
      SQLRec_GraGXOutr   := '';
      SQLRec_GraGXServ   := '';
      SQLRec_GraGXVend   := '';
      SQLRec_GraGruEPat  := '';
      SQLRec_GraGruEVen  := '';
      SQLRec_StqBalCad   := '';
      SQLRec_StqBalIts   := '';
      SQLRec_StqMovItsA  := '';
      //SQLRec_StqMovCusA  := '';
      SQLRec_GRaGruVal   := '';


      while not SDProduto.Eof do
      begin
        Controle := Integer(SDProdutoProduto.Value);
        GraGruX  := Controle;
        GraGruC  := Controle;
        GraGru1  := Controle;
        EAN13    := '';
        Nome     := SDProdutoDESCRICAO.Value;
        if pos('#', Nome) = 14 then
        begin
          EAN13 := Copy(Nome, 1, 13);
          if EAN13 = Geral.SoNumero_TT(EAN13) then
          begin
            Nome := Trim(Copy(Nome, 15))
          end else
            EAN13 := '';
        end;
        //
        //TipoProd := GraGruYs[Controle];
        //
        ///  Ver tabela e tipo de equipamento!
        _ATIVO     := SDProdutoATIVO.Value;
        CATEGORIA := SDProdutoCATEGORIA.Value;  // "V" e "L"    Venda e Loca��o???
        //
{
        if TipoProd = 'P' then // Produto Principal
          GraGruY  := CO_GraGruY_1024_GXPatPri
        else
        if TipoProd = 'S' then // Produto Secund�rio
          GraGruY  := CO_GraGruY_2048_GXPatSec
        else
        if TipoProd = 'A' then // Acessorio
          GraGruY  := CO_GraGruY_3072_GXPatAce
        else
        if TipoProd = 'U' then // Uso
          GraGruY  := CO_GraGruY_4096_GXPatUso
        else
        if TipoProd = 'C' then // Consumo
          GraGruY  := CO_GraGruY_5120_GXPatCns
        else
          GraGruY  := CO_GraGruY_6144_GXPrdVen
        ;
}
        if Uppercase(CATEGORIA) =  'V' then
          GraGruY := 8192
        else
        begin
         // ver aqui Uso >> Produto Reduzido 226 - CONSUMO DISCO !!!
          GraGruY  := FGraGruY[Controle];
        end;
        if GraGruY = -1 then
        begin
          Geral.MB_Erro('Produto sem GraGruY: ' + Geral.FF0(GraGruX) +
          ' - ' + SDProdutoDESCRICAO.Value);
          // Fazer algo aqui?
        end;
        //
        if _ATIVO = 'S' then
          Ativo := 1
        else
          Ativo := 0;
        //
        if SDProduto.RecNo = 1 then
          SQLRec_GraGruX    := ' ('
        else
          SQLRec_GraGruX    := ', (';
        //
        SQLRec_GraGruX := SQLRec_GraGruX
        //Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM
        +      Geral.FF0(Controle)
        + PV_N(Geral.FF0(GraGruC))
        + PV_N(Geral.FF0(GraGru1))
        + PV_N(Geral.FF0(GraTamI))
        + PV_A(EAN13)
        + PV_N(Geral.FF0(GraGruY))
        + PV_A(COD_ITEM)
        + PV_N(Geral.FF0(Ativo))
        //
        + ') ';
              //
        SQL_VALUES_GraGruX := SQL_VALUES_GraGruX + sLineBreak + SQLRec_GraGruX;

        //////////////////////////////////////////////////
        ///        GraGru1
        //////////////////////////////////////////////////
        Nivel5      := 0;
        Nivel4      := 0;
        Nivel3      := 0;
        Nivel2      := GetNivel2(Integer(SDProdutoGRUPO.Value), Geral.IMV(SDProdutoLinha.Value));
        Nivel1      := GraGru1;
        CodUsu      := Nivel1;
        //Nome        := SDProdutoDESCRICAO.Value;     definido acima no EAN13 !!
        PrdGrupTip  := Integer(SDProdutoGRUPO.Value);
        GraTamCad   := 1; // Fixo! para este cliente! Tisolin
        UnidMed     := 1; // Fixo! para este cliente! Tisolin
(*
CST_A, CST_B,
*)
        NCM         := SDProdutoNCM.Value;
(*
Peso, IPI_Alq, IPI_CST,
IPI_cEnq, IPI_vUnid, IPI_TpTrib,
TipDimens, PerCuztMin, PerCuztMax,
MedOrdem, PartePrinc, InfAdProd,
SiglaCustm, HowBxaEstq, GerBxaEstq,
PIS_CST, PIS_AlqP, PIS_AlqV,
PISST_AlqP, PISST_AlqV, COFINS_CST,
COFINS_AlqP, COFINS_AlqV, COFINSST_AlqP,
COFINSST_AlqV, ICMS_modBC, ICMS_modBCST,
ICMS_pRedBC, ICMS_pRedBCST, ICMS_pMVAST,
ICMS_pICMSST, ICMS_Pauta, ICMS_MaxTab,
*)

        IPI_pIPI    := SDProduto.FieldByName('PERCIPI').AsExtended;
        cGTIN_EAN   := SDProdutoCODIGOBARRAS.Value;

(*
  EX_TIPI,
PIS_pRedBC, PISST_pRedBCST, COFINS_pRedBC,
COFINSST_pRedBCST, ICMSRec_pRedBC, IPIRec_pRedBC,
PISRec_pRedBC, COFINSRec_pRedBC, ICMSRec_pAliq,
IPIRec_pAliq, PISRec_pAliq, COFINSRec_pAliq,
ICMSRec_tCalc, IPIRec_tCalc, PISRec_tCalc,
COFINSRec_tCalc, ICMSAliqSINTEGRA, ICMSST_BaseSINTEGRA,
FatorClas, prod_indTot,
*)

(*
DROP TABLE IF EXISTS _REFERENCIAS_DUPLICADAS_;

CREATE TABLE _REFERENCIAS_DUPLICADAS_
SELECT REFERENCIA, COUNT(REFERENCIA) ITENS
FROM PRODUTO
GROUP BY Referencia;

SELECT PRODUTO, REFERENCIA, DESCRICAO
FROM PRODUTO
WHERE REFERENCIA IN (
  SELECT REFERENCIA
  FROM _REFERENCIAS_DUPLICADAS_
  WHERE ITENS > 1
)
ORDER BY REFERENCIA, PRODUTO;
*)
        Referencia := SDProdutoREFERENCIA.Value;
        if Referencia = EmptyStr then
          Referencia := 'Produto ' + Geral.FF0(Nivel1);
        Patrimonio := SDProdutoPATRIMONIO.Value;
(*
Fabricante, PerComissF, PerComissR,
PerComissZ, NomeTemp, COD_LST,
SPEDEFD_ALIQ_ICMS, DadosFisc, GraTabApp,
ICMS_pDif, ICMS_pICMSDeson, ICMS_motDesICMS,
prod_CEST, prod_indEscala
*)
        UsaSubsTrib := FTributacaoST[GraGruX];
        if not (UsaSubsTrib in ([0, 1, 2])) then
          UsaSubsTrib := 2;

        //
        if SDProduto.RecNo = 1 then
          SQLRec_GraGru1    := ' ('
        else
          SQLRec_GraGru1    := ', (';
        //
        SQLRec_GraGru1 := SQLRec_GraGru1
        //Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM
        +      Geral.FF0(Nivel5)
        + PV_N(Geral.FF0(Nivel4))
        + PV_N(Geral.FF0(Nivel3))
        + PV_N(Geral.FF0(Nivel2))
        + PV_N(Geral.FF0(Nivel1))
        + PV_N(Geral.FF0(CodUsu))
        + PV_N(Geral.VariavelToString(Nome))
        + PV_N(Geral.FF0(PrdGrupTip))
        + PV_N(Geral.FF0(GraTamCad))
        + PV_N(Geral.FF0(UnidMed))
        + PV_A(Geral.FormataNCM(NCM))
        + PV_N(Geral.FFT_Dot(IPI_pIPI, 2, siPositivo))
        + PV_A(cGTIN_EAN)
        + PV_N(Geral.VariavelToString(Referencia))
        + PV_N(Geral.VariavelToString(Patrimonio))
        + PV_N(Geral.VariavelToString(UsaSubsTrib))
        //
        + ') ';
        //
        SQL_VALUES_GraGru1 := SQL_VALUES_GraGru1 + sLineBreak + SQLRec_GraGru1;




        if SDProduto.RecNo = 1 then
          SQLRec_GraGruC    := ' ('
        else
          SQLRec_GraGruC    := ', (';
        //
        SQLRec_GraGruC := SQLRec_GraGruC
        //Controle, GraGruC, GraGru1, GraTamI, EAN13, GraGruY, COD_ITEM
        +      Geral.FF0(Controle)
        + PV_N(Geral.FF0(Nivel1))
        + PV_N(Geral.FF0((*GraCorCad*)1))
        + PV_N(Geral.FF0((*GraCmpCad*)0))
        //
        + ') ';
        //
        SQL_VALUES_GraGruC := SQL_VALUES_GraGruC + sLineBreak + SQLRec_GraGruC;



(*
Complem, AquisData, AquisDocu,
*)
         AquisValr := SDProduto.FieldByName('CUSTO').AsExtended;
(*
Situacao,
*)
         AtualValr  := SDProduto.FieldByName('PRECOVENDA').AsExtended;
         ValorMes   := SDProduto.FieldByName('VALORMENSAL').AsExtended;
         ValorQui   := SDProduto.FieldByName('VALORQUINZENAL').AsExtended;
         ValorSem   := SDProduto.FieldByName('VALORSEMANAL').AsExtended;
         ValorDia   := SDProduto.FieldByName('VALORDIARIA').AsExtended;
         ValorFDS   := SDProduto.FieldByName('PRECOFDS').AsExtended;
         DMenos     := SDProduto.FieldByName('DIASCARENCIA').AsInteger;
(*
Agrupador,
*)
        UnDmkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM grafabmar ',
        'WHERE Nome="' + SDProdutoMARCA.Value + '"',
        '']);
        //
        Marca := QrGraFabMarControle.Value;
(*
Modelo, Serie, Voltagem,
Potencia, Capacid, VendaData,
VendaDocu, VendaValr, VendaEnti,
*)
        Observa := SDProdutoFICHATECNICA.Value;
        AGRPAT  := Geral.FF0(Integer(SDProdutoGRUPO.Value));
        Patrimonio := SDProdutoPATRIMONIO.Value;
(*
CLVPAT,
*)

        MARPAT := SDProdutoMARCA.Value;
(*
, Aplicacao, DescrTerc], [
GraGruX]
*)
        /////














{
        if Uppercase(CATEGORIA) = 'L' then
        //Loca��o
        begin
          if Uppercase(ATIVO) = 'S' then
          begin
            SDGraGX.Close;
            SDGraGX.DataSet.CommandType := ctQuery;
            SDGraGX.DataSet.CommandText := Geral.ATS([
            'SELECT PRODUTO, ACESSORIO  ',
            'FROM produtoacessorio ',
            'WHERE PRODUTO=' + Geral.FF0(Controle),
            'OR ACESSORIO=' + Geral.FF0(Controle)
            ]);
            SDGraGX.Open;
            if SDGraGX.RecordCount > 0 then
            begin
              if Integer(SDGraGXPRODUTO.Value) = Controle then
                Aplicacao := 1
              else
                Aplicacao := 2
            end else
            begin
              //Geral.MB_ERRO('Reduzido de loca��o sem atrelamento de produto X acess�rio: ' + Geral.FF0(Controle))
              EquipamentosErr :=  EquipamentosErr + ', ' + Geral.FF0(Controle);
              Aplicacao := 1;
            end;
          end else
            Aplicacao := 0;

          //
          GraGruX   := Controle;
          //
          if SQL_VALUES_GraGXPatr = EmptyStr then
            SQLRec_GraGXPatr    := ' ('
          else
            SQLRec_GraGXPatr    := ', (';
          //
          SQLRec_GraGXPatr := SQLRec_GraGXPatr
          +      Geral.FF0(GraGruX)
          (*Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
          VendaData, VendaDocu,*)
          + PV_N(Geral.VariavelToString(Observa))
          + PV_N(Geral.VariavelToString(AGRPAT))
          (*CLVPAT,*)
          + PV_N(Geral.VariavelToString(MARPAT))
          (*, DescrTerc, Situacao, Agrupador,*)
          + PV_N(Geral.FF0(Marca))
          (*VendaEnti,*)
          + PV_N(Geral.FF0(Aplicacao))
          + PV_N(Geral.FFT_Dot(AquisValr, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(AtualValr, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
          + PV_N(Geral.FFT_Dot(VendaValr, 2, siNegativo))
          //
          + ') ';
          //
          SQL_VALUES_GraGXPatr := SQL_VALUES_GraGXPatr + sLineBreak + SQLRec_GraGXPatr;

        end else // "V"
        // Venda!
        begin
          GraGruX   := Controle;
          ItemValr  := AtualValr;
          ItemUnid  := 1;
          Aplicacao := 1;
          //
          //
          if SQL_VALUES_GraGXOutr = EmptyStr then
            SQLRec_GraGXOutr    := ' ('
          else
            SQLRec_GraGXOutr    := ', (';
          //
          SQLRec_GraGXOutr := SQLRec_GraGXOutr
          +      Geral.FF0(GraGruX)
          + PV_N(Geral.FF0(Aplicacao))
          + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
          + PV_N(Geral.FF0(ItemUnid))
          //
          + ') ';
          //
          SQL_VALUES_GraGXOutr := SQL_VALUES_GraGXOutr + sLineBreak + SQLRec_GraGXOutr;
        end;
}


























        //if Controle = 862 then
          //Geral.MB_Info(Geral.FF0(Controle));

        //if pos('FRET', Nome) > 0 then
          //Geral.MB_Info(Nome);
        if Uppercase(CATEGORIA) = 'L' then
        //Loca��o
        begin
{
          SDGraGX.Close;
          SDGraGX.DataSet.CommandType := ctQuery;
          SDGraGX.DataSet.CommandText := Geral.ATS([
          'SELECT PRODUTO, ACESSORIO  ',
          'FROM produtoacessorio ',
          'WHERE PRODUTO=' + Geral.FF0(Controle),
          'OR ACESSORIO=' + Geral.FF0(Controle)
          ]);
          SDGraGX.Open;
          if (SDGraGX.RecordCount = 0) or (Integer(SDGraGXPRODUTO.Value) = Controle) then
          begin
}
            case GraGruY of
              1024: Aplicacao := 1;
              2048: Aplicacao := 2;
              3072: Aplicacao := 3;
              4096: Aplicacao := 1;
              5120: Aplicacao := 2;
              6144: Aplicacao := 3;
              7168: Aplicacao := 1;
              8192: Aplicacao := 1;
              else
              begin
                Aplicacao := 0;
                Geral.MB_Erro('Produto sem GraGruY! ' + Geral.FF0(Controle));
              end;
            end;
(*
            if Uppercase(_ATIVO) = 'S' then
            begin
              if Integer(SDProdutoGRUPO.Value) = 3 then //M�quina
                Aplicacao := 1
              else
                Aplicacao := 2;
            end else
              Aplicacao := 0;
            GraGruX   := Controle;
            //
*)
            case GraGruY of
              // Patrimonios COM controle de estoque
              1024,
              2048,
              3072:
              begin
                if SQL_VALUES_GraGXPatr = EmptyStr then
                  SQLRec_GraGXPatr    := ' ('
                else
                  SQLRec_GraGXPatr    := ', (';
                //
                SQLRec_GraGXPatr := SQLRec_GraGXPatr
                +      Geral.FF0(GraGruX)
                (*Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia, Capacid,
                VendaData, VendaDocu,*)
                + PV_N(Geral.VariavelToString(Observa))
                + PV_N(Geral.VariavelToString(AGRPAT))
                (*CLVPAT,*)
                + PV_N(Geral.VariavelToString(MARPAT))
                (*, DescrTerc, Situacao, Agrupador,*)
                + PV_N(Geral.FF0(Marca))
                (*VendaEnti,*)
                + PV_N(Geral.FF0(Aplicacao))
                + PV_N(Geral.FFT_Dot(AquisValr, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(AtualValr, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(ValorMes, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(ValorQui, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(ValorSem, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(ValorDia, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(VendaValr, 2, siNegativo))
                + PV_N(Geral.FFT_Dot(ValorFDS, 2, siNegativo))
                + PV_N(Geral.FF0(DMenos))
                //+ PV_N(Geral.VariavelToString(Patrimonio))
                //
                + ') ';
                //
                SQL_VALUES_GraGXPatr := SQL_VALUES_GraGXPatr + sLineBreak + SQLRec_GraGXPatr;
              end;
              // Patrimonios SEM controle de estoque
              4096,
              5120,
              6144:
              begin
                //
                GraGruX   := Controle;
                ItemValr  := AtualValr;
                ItemUnid  := 1;
                //
                //
                if SQL_VALUES_GraGXOutr = EmptyStr then
                  SQLRec_GraGXOutr    := ' ('
                else
                  SQLRec_GraGXOutr    := ', (';
                //
                SQLRec_GraGXOutr := SQLRec_GraGXOutr
                +      Geral.FF0(GraGruX)
                + PV_N(Geral.FF0(Aplicacao))
                + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
                + PV_N(Geral.FF0(ItemUnid))
                //
                + ') ';
                //
                SQL_VALUES_GraGXOutr := SQL_VALUES_GraGXOutr + sLineBreak + SQLRec_GraGXOutr;
              end;
              // Servi�os
              7168:
              begin
                //
                GraGruX   := Controle;
                ItemValr  := AtualValr;
                ItemUnid  := 1;
                //
                //
                if SQL_VALUES_GraGXServ = EmptyStr then
                  SQLRec_GraGXServ    := ' ('
                else
                  SQLRec_GraGXServ    := ', (';
                //
                SQLRec_GraGXServ := SQLRec_GraGXServ
                +      Geral.FF0(GraGruX)
                + PV_N(Geral.FF0(Aplicacao))
                + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
                + PV_N(Geral.FF0(ItemUnid))
                //
                + ') ';
                //
                SQL_VALUES_GraGXServ := SQL_VALUES_GraGXServ + sLineBreak + SQLRec_GraGXServ;
              end;
              // Produtos de Venda
              8192:
              begin
                //
                GraGruX   := Controle;
                ItemValr  := AtualValr;
                ItemUnid  := 1;
                //
                //
                if SQL_VALUES_GraGXVend = EmptyStr then
                  SQLRec_GraGXVend    := ' ('
                else
                  SQLRec_GraGXVend    := ', (';
                //
                SQLRec_GraGXVend := SQLRec_GraGXVend
                +      Geral.FF0(GraGruX)
                + PV_N(Geral.FF0(Aplicacao))
                + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
                + PV_N(Geral.FF0(ItemUnid))
                + PV_N(Geral.FF0(Marca))
                + PV_N(Geral.VariavelToString(MARPAT))
                //
                + ') ';
                //
                SQL_VALUES_GraGXVend := SQL_VALUES_GraGXVend + sLineBreak + SQLRec_GraGXVend;
              end;
          end;
        end else // "V"
        // Venda!
        begin
          Aplicacao := 1;
          GraGruY   := 8192;
          //
          GraGruX   := Controle;
          ItemValr  := AtualValr;
          ItemUnid  := 1;
          //
          //
          if SQL_VALUES_GraGXVend = EmptyStr then
            SQLRec_GraGXVend    := ' ('
          else
            SQLRec_GraGXVend    := ', (';
          //
          SQLRec_GraGXVend := SQLRec_GraGXVend
          +      Geral.FF0(GraGruX)
          + PV_N(Geral.FF0(Aplicacao))
          + PV_N(Geral.FFT_Dot(ItemValr, 2, siPositivo))
          + PV_N(Geral.FF0(ItemUnid))
          + PV_N(Geral.FF0(Marca))
          + PV_N(Geral.VariavelToString(MARPAT))
          //
          + ') ';
          //
          SQL_VALUES_GraGXVend := SQL_VALUES_GraGXVend + sLineBreak + SQLRec_GraGXVend;
        end;


















(*
DROP TABLE IF EXISTS _produtoacessorio_PRODUTOS;
CREATE TABLE _produtoacessorio_PRODUTOS
SELECT PRODUTO
FROM produtoacessorio;

DROP TABLE IF EXISTS _produtoacessorio_ACESSORIO;
CREATE TABLE _produtoacessorio_ACESSORIO
SELECT ACESSORIO
FROM produtoacessorio;

SELECT prd.PRODUTO, ace.ACESSORIO
FROM _produtoacessorio_PRODUTOS prd
LEFT JOIN _produtoacessorio_ACESSORIO ace
  ON prd.PRODUTO=ace.ACESSORIO
WHERE NOT(ace.ACESSORIO IS NULL)
;
*)


        Estoque := SDPRODUTO.FieldByName('ESTOQUE').AsExtended;
        if Estoque < 0 then
          Estoque := 0;
        CustoUni := SDPRODUTO.FieldByName('CUSTO').AsExtended;
        CustoAll := CustoUni * Estoque;
        //
        case GraGruY of
          1024,
          2048,
          3072:
          begin
            if SQLRec_GraGruEPat = EmptyStr then
              SQLRec_GraGruEPat    := ' ('
            else
              SQLRec_GraGruEPat    := ', (';
            //
            SQLRec_GraGruEPat := SQLRec_GraGruEPat
            //GraGruX, EstqAnt, EstqEnt, EstqSai, EstqLoc, EstqSdo
            +      Geral.FF0(-11) // Empresa
            + PV_N(Geral.FF0(GraGruX))
            + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
            + PV_N('0.00')
            + PV_N('0.00')
            + PV_N('0.00')
            + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
            //
            + ') ';
            //
            SQL_VALUES_GraGruEPat := SQL_VALUES_GraGruEPat + sLineBreak + SQLRec_GraGruEPat;
          end;
          4096,
          5120,
          6144: ; // nada, loca��es que n�o controla estoque
          7168: ; // nada, servi�os, n�o controla estoque
          8192:
          begin
            if SQLRec_GraGruEVen = EmptyStr then
              SQLRec_GraGruEVen    := ' ('
            else
              SQLRec_GraGruEVen    := ', (';
            //
            SQLRec_GraGruEVen := SQLRec_GraGruEVen
            //GraGruX, EstqAnt, EstqEnt, EstqSai, EstqLoc, EstqSdo
            +      Geral.FF0(GraGruX)
            + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
            + PV_N('0.00')
            + PV_N('0.00')
            + PV_N('0.00')
            + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
            //
            + ') ';
            //
            SQL_VALUES_GraGruEVen := SQL_VALUES_GraGruEVen + sLineBreak + SQLRec_GraGruEVen;






            if SQLRec_StqBalIts = EmptyStr then
              SQLRec_StqBalIts    := ' ('
            else
              SQLRec_StqBalIts    := ', (';
            //
            SBI := SBI + 1;
            //
            SQLRec_StqBalIts := SQLRec_StqBalIts
            //Controle, Codigo, Tipo, DataHora, StqCenLoc, GraGruX, QtdLei, QtdAnt
            +      Geral.FF0((*Controle*)SBI)
            + PV_N(Geral.FF0(PrdGrupTip))
            + PV_N('0'(*Tipo*)) // Balan�o
            + PV_A(Abertura(*DataHora*))
            + PV_N('1'(*StqCenLoc*))
            + PV_N(Geral.FF0(GraGruX))
            + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
            + PV_N('0.00'(*QtdAnt*))
            //
            + ') ';
            //
            SQL_VALUES_StqBalIts := SQL_VALUES_StqBalIts + sLineBreak + SQLRec_StqBalIts;




            if SQLRec_StqMovItsA = EmptyStr then
              SQLRec_StqMovItsA    := ' ('
            else
              SQLRec_StqMovItsA    := ', (';
            //
            SMA := SMA + 1;
            //
            SQLRec_StqMovItsA := SQLRec_StqMovItsA
            //    'IDCtrl, DataHora, Tipo, OriCodi, OriCtrl, OriCnta, Empresa, ' +
            //    'StqCenCad, StqCenLoc, GraGruX, Qtde, CustoAll, ValorAll, '+
            //    'GrupoBal, Baixa, AntQtde, UnidMed, ValiStq) VALUES ']);
            +      Geral.FF0((*IDCtrl,*)SMA)
            + PV_A(Abertura(*DataHora*))
            + PV_N('0'(*Tipo*)) // Balan�o
            + PV_N(Geral.FF0(PrdGrupTip(*OriCodi*)))
            + PV_N(Geral.FF0((*OriCtrl,*)SMA))
            + PV_N('0'(*OriCnta*))
            + PV_N('-11'(*Empresa*))
            + PV_N('1'(*StqCenCad*))
            + PV_N('0'(*StqCenLoc*))
            + PV_N(Geral.FF0(GraGruX))
            + PV_N(Geral.FFT_Dot(Estoque, 6, siNegativo))
            + PV_N(Geral.FFT_Dot(CustoAll, 2, siNegativo))
            + PV_N('0.00'(*ValorAll*))
            + PV_N('0'(*GrupoBal*))
            + PV_N('1'(*Baixa*))
            + PV_N('0.00'(*AntQtde*))
            + PV_N('1'(*UnidMed*))
            + PV_N('100'(*ValiStq*))
            //
            + ') ';
            //
            SQL_VALUES_StqMovItsA := SQL_VALUES_StqMovItsA + sLineBreak + SQLRec_StqMovItsA;





{
            if SQLRec_StqMovCusA = EmptyStr then
              SQLRec_StqMovCusA    := ' ('
            else
              SQLRec_StqMovCusA    := ', (';
            //
            SMA := SMA + 1;
            //
            SQLRec_StqMovCusA := SQLRec_StqMovCusA
    //'Empresa, StqCenCad, GraGruX, Qtde, ValorTotal, UltPreco, '+
    //'CustoPadrao, CustoMedio, RegBaseCont, RegBaseQtde) VALUES ']);
            +     ('-11'(*Empresa*))
            + PV_N('1'(*StqCenCad*))
            + PV_N(Geral.FF0(GraGruX))
            + PV_N(Geral.FFT_Dot(Estoque, 3, siNegativo))  (*Qtde*)
            + PV_N(Geral.FFT_Dot(CustoAll, 2, siNegativo)) (*ValorTotal*)
            + PV_N(Geral.FFT_Dot(CustoUni, 4, siNegativo)) (*UltPreco*)
            + PV_N('0.0000') (*CustoPadrao*)
            + PV_N(Geral.FFT_Dot(CustoUni, 4, siNegativo)) (*CustoMedio*)
            + PV_N('1'(*RegBaseCont*))
            + PV_N(Geral.FFT_Dot(Estoque, 3, siNegativo))  (*RegBaseQtde*)
            //
            + ') ';
            //
            SQL_VALUES_StqMovCusA := SQL_VALUES_StqMovCusA + sLineBreak + SQLRec_StqMovCusA;
}


            if SQLRec_GraGruVal = EmptyStr then
              SQLRec_GraGruVal    := ' ('
            else
              SQLRec_GraGruVal    := ', (';
            //
            GGV := GGV + 1;
            //
            SQLRec_GraGruVal := SQLRec_GraGruVal
    //'Controle, GraGruX, GraGru1, GraCusPrc, ',
    //'CustoPreco, Entidade, DataIni) VALUES ']);
            +     (Geral.FF0(GGV))
            + PV_N(Geral.FF0(GraGruX))
            + PV_N(Geral.FF0(GraGru1)) //GraGru1
            + PV_N('5'(*GraCusPrc*))
            + PV_N(Geral.FFT_Dot(CustoUni, 4, siNegativo)) (*Preco*)
            + PV_N('-11'(*Empresa*))
            + PV_N('"0000-00-00"') (*DataIni*)
            //
            + ') ';
            //
            SQL_VALUES_GraGruVal := SQL_VALUES_GraGruVal + sLineBreak + SQLRec_GraGruVal;

          end
          else
          begin
            Geral.MB_Erro('Produto sem GraGruY! (estoque)' + Geral.FF0(Controle));
          end;
        end;



        SDProduto.Next;
      end;

      if SQL_VALUES_GraGruX <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruX + SQL_VALUES_GraGruX;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruX + SQL_VALUES_GraGruX);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruX := EmptyStr;

      //
      if SQL_VALUES_GraGru1 <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGru1 + SQL_VALUES_GraGru1;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGru1 + SQL_VALUES_GraGru1);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGru1 := EmptyStr;

      //
      if SQL_VALUES_GraGruC <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruC + SQL_VALUES_GraGruC;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruC + SQL_VALUES_GraGruC);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruC := EmptyStr;

      //
      if SQL_VALUES_GraGXPatr <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGXPatr + SQL_VALUES_GraGXPatr;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGXPatr + SQL_VALUES_GraGXPatr);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGXPatr := EmptyStr;

      //
      if SQL_VALUES_GraGXOutr <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGXOutr + SQL_VALUES_GraGXOutr;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGXOutr + SQL_VALUES_GraGXOutr);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGXOutr := EmptyStr;

      //
      if SQL_VALUES_GraGXServ <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGXServ + SQL_VALUES_GraGXServ;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGXServ + SQL_VALUES_GraGXServ);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGXServ := EmptyStr;

      //
      if SQL_VALUES_GraGXVend <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGXVend + SQL_VALUES_GraGXVend;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGXVend + SQL_VALUES_GraGXVend);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGXVend := EmptyStr;

      //
      if SQL_VALUES_GraGruEPat <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruEPat + SQL_VALUES_GraGruEPat;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruEPat + SQL_VALUES_GraGruEPat);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruEPat := EmptyStr;

      //
      if SQL_VALUES_GraGruEVen <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruEVen + SQL_VALUES_GraGruEVen;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruEVen + SQL_VALUES_GraGruEVen);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruEVen := EmptyStr;

      //
      if SQL_VALUES_StqBalIts <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_StqBalIts + SQL_VALUES_StqBalIts;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_StqBalIts + SQL_VALUES_StqBalIts);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_StqBalIts := EmptyStr;

      //
      if SQL_VALUES_StqMovItsA <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_StqMovItsA + SQL_VALUES_StqMovItsA;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_StqMovItsA + SQL_VALUES_StqMovItsA);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_StqMovItsA := EmptyStr;

      //
      if SQL_VALUES_GraGruVal <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS_GraGruVal + SQL_VALUES_GraGruVal;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS_GraGruVal + SQL_VALUES_GraGruVal);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES_GraGruVal := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDProduto.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
  //
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando cabe�alhos de Balan�o de Mercadorias');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT DISTINCT gg1.PrdGrupTip',
  'FROM gragrueven ggv',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'WHERE ggv.EstqSdo>0',
  'ORDER BY gg1.PrdGrupTip',
  '']);
  Dmod.QrAux.First;
  while not Dmod.QrAux.Eof do
  begin

    Codigo := Dmod.QrAux.FieldByName('PrdGrupTip').AsInteger;//UMyMod.BuscaEmLivreY_Def('StqBalCad', 'Codigo', stIns, 0);
    //DModG.BuscaProximoCodigoInt_Novo('Controle', 'GrupoBal', '', 0, '', GrupoBal);
    GrupoBal := Codigo;
    CodUsu := Codigo;
    Nome   := 'Importa��o do App anterior (Grupo ' + Geral.FF0(Codigo) + ')';
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalcad', False, [
    'CodUsu', 'Nome', 'Empresa',
    'PrdGrupTip', 'StqCenCad', 'CasasProd',
    'Abertura',
    'Encerrou',
    'Status',
    'GrupoBal', 'Forcada'], [
    'Codigo'], [
    CodUsu, Nome, (*Empresa*)-11,
    PrdGrupTip, (*StqCenCad*)1, (*CasasProd*)3,
    Abertura,
    (*Encerrou*)Abertura,
    9(*Status*), // Encerrado
    GrupoBal, (*Forcada*)0], [
    Codigo], True) then;
    (*
    'INSERT INTO stqbalcad SET AlterWeb = 1, UserCad=-1, DataCad="2020-10-12"  ' +
    ', ' +
    'GrupoBal=1, ' +
    'Nome="Importa��o App anterior", ' +
    'CodUsu=1, ' +
    'CasasProd=3, ' +
    'Abertura="2020-10-12 15:22:21", ' +
    'Codigo=1, ' +
    'Forcada=0, ' +
    'Empresa=-11, ' +
    'PrdGrupTip=5, ' +
    'StqCenCad=1 ' +*)
    //
    Dmod.QrAux.Next;
  end;
  //
{
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Criando itens de Balan�o de Mercadorias');
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT gg1.PrdGrupTip, ggv.*',
  'FROM gragrueven ggv',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ggv.GraGruX',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip',
  'WHERE ggv.EstqSdo>0',
  'ORDER BY gg1.PrdGrupTip, ggx.Controle',
  '']);
  PB2.Position := 0;
  PB2.Max := Dmod.QrAux.RecordCount;
  Dmod.QrAux.First;
  while not Dmod.QrAux.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB2, LaAvisoG1, LaAvisoG2, True,
    'Criando iten de Balan�o de Mercadorias: ' + Geral.FF0(Dmod.QrAux.RecNo));
    //
    Codigo   := Dmod.QrAux.FieldByName('PrdGrupTip').AsInteger;
    GraGruX  := Dmod.QrAux.FieldByName('GraGruX').AsInteger;
    QtdLei   := Dmod.QrAux.FieldByName('EstqAnt').AsExtended;
    //
    Controle := DModG.BuscaProximoInteiro('stqbalits', 'Controle', '', 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqbalits', False,
      ['Tipo', 'DataHora', 'StqCenLoc', 'GraGruX', 'QtdLei', 'QtdAnt'],
      ['Codigo', 'Controle'],
      [(*Tipo*)0, (*DataHora*)Abertura, (*StqCenLoc*)1, GraGruX, QtdLei, (*QtdAnt*)0],
      [Codigo, Controle], False) then ;
    //
    Dmod.QrAux.Next;
  end;
}

  if EquipamentosErr <> EmptyStr then
    Geral.MB_Erro('Equipamentos sem atrelamento: ' + sLineBreak +
    EquipamentosErr);
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_ProdutoAcessorio;
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Conta, GraGXPatr, GraGXOutr, Quantidade: Integer;
const
  OriTab = 'ProdutoAcessorio';
  DstTab = 'gragxpits';
begin
  Conta := 0;
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Conta, GraGXPatr, GraGXOutr, Quantidade) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDProdutoAcessorio.Close;
    SDProdutoAcessorio.DataSet.CommandType := ctQuery;
    SDProdutoAcessorio.DataSet.CommandText := SQL;
    SDProdutoAcessorio.Open;
    Continua := SDProdutoAcessorio.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDProdutoAcessorio.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDProdutoAcessorio.First;
      SQLRec    := '';
      while not SDProdutoAcessorio.Eof do
      begin
        Conta      := Conta + 1;
        GraGXPatr  := Integer(SDProdutoAcessorioPRODUTO.Value);
        GraGXOutr  := Integer(SDProdutoAcessorioACESSORIO.Value);
        Quantidade := Integer(SDProdutoAcessorioQUANTIDADE.Value);
        //
        if SDProdutoAcessorio.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Conta
        + Geral.FF0(Conta)
        //GraGXPatr
        + PV_N(Geral.FF0(GraGXPatr))
        //GraGXOutr
        + PV_N(Geral.FF0(GraGXOutr))
        //Quantidade
        + PV_N(Geral.FF0(Quantidade))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDProdutoAcessorio.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDProdutoAcessorio.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

procedure TFmDB_Converte_Tisolin.ImportaTabelaFB_StatusEnti();
var
  SQL, SQL_FIELDS, SQL_VALUES, SQLRec: String;
  ItsOK, Total, Step: Integer;
  Continua: Boolean;
  //
  Codigo: Integer;
  Nome: String;
const
  OriTab = 'StatusCliente';
  DstTab = 'StatusEnti';
begin
  MySQLBatchExecute1.Database := Dmod.MyDB;
  SQL_FIELDS := Geral.ATS([
    'INSERT INTO ' + DstTab + ' ( ',
    'Codigo, Nome) VALUES ']);
  //

  ItsOK := 0;

  //

  SDAux.Close;
  SDAux.DataSet.CommandType := ctQuery;
  SDAux.DataSet.CommandText := 'SELECT COUNT(*) FROM ' + OriTab;
  SDAux.Open;
  //
  Total := SDAux.Fields[0].AsInteger;
  Step :=  EdFirst.ValueVariant;
  PB2.Position := 0;
  PB2.Max := (Total div Step) + 1;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
  Geral.FF0(ItsOK) + ' de ' + Geral.FF0(Total));

  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Exclu�ndo dados. ');
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM ' + DstTab);
  //
  //QrUpd.SQL.Text := '';
  while Total > ItsOK do
  begin
    MyObjects.UpdPB(PB2, nil, nil);
    SQL := 'SELECT ';
    if CkLimit.Checked then
    begin
      if EdFirst.ValueVariant > 0 then
      begin
        SQL := SQL + ' FIRST ' + Geral.FF0(Step);
        if ItsOK > 0 then
          SQL := SQL + ' SKIP ' + Geral.FF0(ItsOK);
      end;
    end;
    //SQL := SQL + ' * FROM ' + SDTabelas.FieldByName('RDB$RELATION_NAME').AsString;
    SQL := SQL + ' * FROM ' + OriTab;
    SDStatusCliente.Close;
    SDStatusCliente.DataSet.CommandType := ctQuery;
    SDStatusCliente.DataSet.CommandText := SQL;
    SDStatusCliente.Open;
    Continua := SDStatusCliente.RecordCount > 0;
    if Continua then
    begin
      MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
        Geral.FF0(SDStatusCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
      SQL_VALUES := '';
      SDStatusCliente.First;
      SQLRec    := '';
      while not SDStatusCliente.Eof do
      begin
        Codigo := Integer(SDStatusClienteSTATUS.Value);
        Nome   := SDStatusClienteDESCRICAO.Value;
        //
        if SDStatusCliente.RecNo = 1 then
          SQLRec    := ' ('
        else
          SQLRec    := ', (';
        //
        SQLRec := SQLRec
        //Codigo,
        + Geral.FF0(Codigo)
        //Empresa,
        + PV_N(Geral.VariavelToString(Nome))
        //
        + ') ';
              //
        SQL_VALUES := SQL_VALUES + sLineBreak + SQLRec;
        SDStatusCliente.Next;
      end;

      if SQL_VALUES <> EmptyStr then
      begin
        //DBAnt.Execute(SQLc);
        MySQLBatchExecute1.SQL.Text := SQL_FIELDS + SQL_VALUES;
        try
          MySQLBatchExecute1.ExecSQL;
        except
          on E: Exception do
          begin
            Geral.MB_INFO('ERRO de execu��o de SQL!' + sLineBreak +
            E.Message + sLineBreak +
            SQL_FIELDS + SQL_VALUES);
            //
            EXIT;
          end;
        end;
      end;
      SQL_VALUES := EmptyStr;

      //
      ItsOK := ItsOK + Step;
    end;
  end;
  MyObjects.Informa2(LaAvisoG1, LaAvisoG2, True, 'Registro ' +
    Geral.FF0(SDStatusCliente.RecNo + ItsOK) + ' de ' + Geral.FF0(Total));
end;

{
procedure TFmDB_Converte_Tisolin.IncluiBanco(Codigo, Ordem: Integer;
  Nome, Bco, Age: String);
var
  ContaCor, ContaTip, DAC_A, DAC_C, DAC_AC, Observ: String;
  Controle, Banco, Agencia: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entictas', 'Controle', stIns, 0);
  Banco          := 0;
  Agencia        := 0;
  ContaCor       := '';
  ContaTip       := '';
  DAC_A          := '';
  DAC_C          := '';
  DAC_AC         := '';
  Observ         := Nome + ' ' + Bco + ' ' + Age;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entictas', False, [
  'Codigo', 'Ordem', 'Banco',
  'Agencia', 'ContaCor', 'ContaTip',
  'DAC_A', 'DAC_C', 'DAC_AC',
  'Observ'], [
  'Controle'], [
  Codigo, Ordem, Banco,
  Agencia, ContaCor, ContaTip,
  DAC_A, DAC_C, DAC_AC,
  Observ], [
  Controle], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiConsulta(Codigo, Orgao: Integer;
  Numero: String; _Data: TDateTime; _Hora, _Resultado: String);
var
  Data, Hora, Resultado: String;
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entisrvpro', 'Controle', stIns, 0);
  Data           := Geral.FDT(_Data, 1);
  Hora           := Geral.SoHora_TT(_Hora, pt2pt60);
  Resultado      := _Resultado + ' ' + _Hora;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entisrvpro', False, [
  'Codigo', 'Orgao', 'Data',
  'Hora', 'Numero', 'Resultado'], [
  'Controle'], [
  Codigo, Orgao, Data,
  Hora, Numero, Resultado], [
  Controle], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiContato(Codigo, Cargo: Integer;
  Nome, Tel, Cel, Fax, Mail: String);
const
  DiarioAdd = 0;
var
  Controle: Integer;
  Telefone, EMail: String;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticontat', False, [
  'Codigo', 'Nome', 'Cargo',
  'DiarioAdd'], [
  'Controle'], [
  Codigo, Nome, Cargo,
  DiarioAdd], [
  Controle], True) then
  begin
    Telefone := Geral.SoNumero_TT(Tel);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneFix, Telefone);
    //
    Telefone := Geral.SoNumero_TT(Cel);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneCel, Telefone);
    //
    Telefone := Geral.SoNumero_TT(Fax);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneFax, Telefone);
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    EMail := Lowercase(Trim(Mail));
    if EMail <> '' then
      IncluiEntiMail(Codigo, Controle, FEMail, EMail);
    //
  end;
end;

procedure TFmDB_Converte_Tisolin.IncluiEntiMail(Codigo, Controle,
  EntiTipCto: Integer; EMail: String);
const
  Ordem = 0;
  DiarioAdd = 0;
var
  Conta: Integer;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entimail', False, [
  'Codigo', 'Controle', 'EMail',
  'EntiTipCto', 'Ordem', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, EMail,
  EntiTipCto, Ordem, DiarioAdd], [
  Conta], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiEntiTel(Codigo, Controle, EntiTipCto: Integer;
  Tel: String);
const
  Ramal = '';
  DiarioAdd = 0;
var
  Conta: Integer;
  Telefone: String;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('entitel', 'Conta', stIns, 0);
  Telefone := Tel;
  if Copy(Telefone, 1, 2) = '00' then
    Telefone := Copy(Telefone, 2);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entitel', False, [
  'Codigo', 'Controle', 'Telefone',
  'EntiTipCto', 'Ramal', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, Telefone,
  EntiTipCto, Ramal, DiarioAdd], [
  Conta], True);
end;

procedure TFmDB_Converte_Tisolin.IncluiSocio(Codigo, CargoSocio: Integer;
  Nome, _RG, _CPF: String; Natal: TDateTime);
var
  Observ, MandatoIni, MandatoFim, CPF, RG, SSP, DataRG, DtaNatal: String;
  Controle, Cargo, (*Assina,*) OrdemLista: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entirespon', 'Controle', stIns, 0);
  //Nome           := ;
  Cargo          := CargoSocio;
  //Assina         := ;
  OrdemLista     := Controle;
  Observ         := Nome + _RG + _CPF;
  MandatoIni     := '0000-00-00';
  MandatoFim     := '0000-00-00';
  CPF            := Geral.SoNumero_TT(_CPF);
  RG             := Geral.SoNumero_TT(_RG);
  SSP            := Geral.SoLetra_TT(_RG);
  DataRG         := '';
  DtaNatal       := Geral.FDT(Natal, 1);

  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entirespon', False, [
  'Codigo', 'Nome', 'Cargo',
  (*'Assina',*) 'OrdemLista', 'Observ',
  'MandatoIni', 'MandatoFim', 'CPF',
  'RG', 'SSP', 'DataRG',
  'DtaNatal'], [
  'Controle'], [
  Codigo, Nome, Cargo,
  (*Assina,*) OrdemLista, Observ,
  MandatoIni, MandatoFim, CPF,
  RG, SSP, DataRG,
  DtaNatal], [
  Controle], True);
end;
}

procedure TFmDB_Converte_Tisolin.MeQryKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
  begin
    Screen.Cursor := crHourGlass;
    try
      SDQry.Close;
      SDQry.Connection := DBFB;
      SDQry.DataSet.CommandType := ctQuery;
      SDQry.DataSet.CommandText := MeQry.Text;
      SDQry.Open;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmDB_Converte_Tisolin.MeSQL1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BtExecutaClick(Sender);
end;

function TFmDB_Converte_Tisolin.ObtemGraGruXDeCODMATX(CODMATX: String): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
  'SELECT ggx.Controle ID',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1 ',
  'WHERE gg1.Referencia=''' + CODMATX + '''',
  '']);
  //
  Result := QrExiste.FieldByName('ID').AsInteger;
end;

function TFmDB_Converte_Tisolin.PV_A(Texto: String): String;
begin
  Result := ', "' + Texto + '"';
end;

function TFmDB_Converte_Tisolin.PV_N(Texto: String): String;
begin
  Result := ', ' + Texto;
end;

procedure TFmDB_Converte_Tisolin.QrPsqBeforeClose(DataSet: TDataSet);
begin
end;

(*
DROP TABLE IF EXISTS _REFERENCIAS_DUPLICADAS_;

CREATE TABLE _REFERENCIAS_DUPLICADAS_
SELECT REFERENCIA, COUNT(REFERENCIA) ITENS
FROM PRODUTO
GROUP BY Referencia;

SELECT PRODUTO, REFERENCIA, DESCRICAO
FROM PRODUTO
WHERE REFERENCIA IN (
  SELECT REFERENCIA
  FROM _REFERENCIAS_DUPLICADAS_
  WHERE ITENS > 1
)
ORDER BY REFERENCIA, PRODUTO;
*)


{ Defini��o de produtos:
DROP TABLE IF EXISTS _PRODUTO_ALL_;
CREATE TABLE _PRODUTO_ALL_

SELECT "S" GGY_KIND, Ativo, DESCRICAO,
PRODUTO, Linha, Categoria,
Referencia, Patrimonio
FROM PRODUTO
WHERE Categoria = "L"
AND Patrimonio = ""

UNION

SELECT "P" GGY_KIND, Ativo, DESCRICAO,
PRODUTO, Linha, Categoria,
Referencia, Patrimonio
FROM PRODUTO
WHERE Categoria = "L"
AND Patrimonio <> ""

;

DROP TABLE IF EXISTS _PRODUTO_ACE_;
CREATE TABLE _PRODUTO_ACE_

SELECT DISTINCT ACESSORIO
FROM PRODUTOACESSORIO
;

DROP TABLE IF EXISTS PRODUTOS_GGY;
CREATE TABLE PRODUTOS_GGY
SELECT IF(ace.ACESSORIO > 0, "A", pal.GGY_KIND) TipoProd,
pal.*
FROM _PRODUTO_ALL_ pal
LEFT JOIN PRODUTOACESSORIO ace ON ace.ACESSORIO=pal.PRODUTO
ORDER BY pal.GGY_KIND DESC, pal.DESCRICAO

;
}

{
 SQL para saber quais carteiras tem lanctos sem quitar

SELECT Cobrador,
SUM(IF(PAGAMENTO>"1900-01-01", 1, 0)) Pagos,
COUNT(Cobrador) ITENS,
COUNT(Cobrador) -
 SUM(IF(PAGAMENTO>"1900-01-01", 1, 0)) Abertos,
SUM(IF(Quitado = "S" AND PAGAMENTO<"1900-01-01", 1, 0)) ErrQuita,
SUM(IF(Quitado = "N" AND PAGAMENTO>"1900-01-01", 1, 0)) ErrNQuit


FROM lancamento
GROUP BY Cobrador
}




end.
