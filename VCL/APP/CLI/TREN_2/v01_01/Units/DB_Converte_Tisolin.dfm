object FmDB_Converte_Tisolin: TFmDB_Converte_Tisolin
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Convers'#227'o de Banco de Dados'
  ClientHeight = 814
  ClientWidth = 1324
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1324
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 379
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 379
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 379
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 55
    Width = 1324
    Height = 687
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1324
      Height = 687
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1241
      ExplicitHeight = 644
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1324
        Height = 687
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1241
        ExplicitHeight = 644
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 1320
          Height = 670
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 1237
          ExplicitHeight = 627
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tabelas DBF'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 1312
              Height = 642
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 1229
              ExplicitHeight = 599
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 1312
                Height = 642
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 0
                ExplicitWidth = 1229
                ExplicitHeight = 599
                object PageControl2: TPageControl
                  Left = 1
                  Top = 1
                  Width = 1310
                  Height = 539
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ActivePage = TabSheet2
                  Align = alClient
                  TabOrder = 0
                  ExplicitWidth = 1227
                  ExplicitHeight = 496
                  object TabSheet8: TTabSheet
                    Caption = ' Restaura backup DB FB'
                    object Panel12: TPanel
                      Left = 0
                      Top = 0
                      Width = 1302
                      Height = 209
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      ExplicitWidth = 1219
                      object Label9: TLabel
                        Left = 12
                        Top = 124
                        Width = 308
                        Height = 13
                        Caption = 'Diret'#243'rio de rastreio para carregamento dos dados csv (GraGruY):'
                      end
                      object SbLoadCSVOthDir: TSpeedButton
                        Left = 780
                        Top = 140
                        Width = 23
                        Height = 22
                        Caption = '...'
                        OnClick = SbLoadCSVOthDirClick
                      end
                      object Label11: TLabel
                        Left = 12
                        Top = 4
                        Width = 232
                        Height = 13
                        Caption = 'Caminho completo do restaurador Fb > gbak.exe:'
                      end
                      object SbGBak: TSpeedButton
                        Left = 780
                        Top = 20
                        Width = 23
                        Height = 22
                        Caption = '...'
                        OnClick = SbGBakClick
                      end
                      object Label12: TLabel
                        Left = 12
                        Top = 44
                        Width = 192
                        Height = 13
                        Caption = 'Arquivo de backup FB para restaura'#231#227'o:'
                      end
                      object Label13: TLabel
                        Left = 12
                        Top = 84
                        Width = 535
                        Height = 13
                        Caption = 
                          'Arquivo base de dados a ser criado na restaura'#231#227'o do backup  e q' +
                          'ue servir'#225' de base para convers'#227'o dos dados:'
                      end
                      object SbArqFB_Bkp: TSpeedButton
                        Left = 780
                        Top = 60
                        Width = 23
                        Height = 22
                        Caption = '...'
                        OnClick = SbArqFB_BkpClick
                      end
                      object SpeedButton2: TSpeedButton
                        Left = 780
                        Top = 100
                        Width = 23
                        Height = 22
                        Caption = '...'
                        OnClick = SpeedButton2Click
                      end
                      object Label16: TLabel
                        Left = 12
                        Top = 164
                        Width = 255
                        Height = 13
                        Caption = 'Nome do BD de importa'#231#227'o simples e direta (espelho):'
                        Enabled = False
                      end
                      object Label17: TLabel
                        Left = 276
                        Top = 164
                        Width = 70
                        Height = 13
                        Caption = 'IP do Servidor:'
                      end
                      object Label18: TLabel
                        Left = 372
                        Top = 164
                        Width = 28
                        Height = 13
                        Caption = 'Porta:'
                      end
                      object EdLoadCSVOthDir: TEdit
                        Left = 12
                        Top = 140
                        Width = 765
                        Height = 21
                        TabOrder = 0
                        Text = 'C:\Dermatek\Conversao\Tisolin\'
                        OnChange = EdLoadCSVOthDirChange
                      end
                      object EdPathGBak: TEdit
                        Left = 12
                        Top = 20
                        Width = 765
                        Height = 21
                        TabOrder = 1
                        Text = 'C:\Program Files (x86)\Firebird\Firebird_2_5\bin\'
                        OnChange = EdLoadCSVOthDirChange
                      end
                      object EdArqFB_Bkp: TEdit
                        Left = 12
                        Top = 60
                        Width = 765
                        Height = 21
                        TabOrder = 2
                        Text = 'C:\Dermatek\Conversao\Tisolin\LOC-15102020-190021.BKP'
                      end
                      object EdIB_DB: TEdit
                        Left = 12
                        Top = 100
                        Width = 765
                        Height = 21
                        TabOrder = 3
                        Text = 'C:\Dermatek\Conversao\Tisolin\Tisolin.FDB'
                      end
                      object EdDBNome: TdmkEdit
                        Left = 12
                        Top = 180
                        Width = 261
                        Height = 21
                        Enabled = False
                        TabOrder = 4
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'ZZZ_FB_DB_ANT'
                        QryCampo = 'DirPedCan'
                        UpdCampo = 'DirPedCan'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'ZZZ_FB_DB_ANT'
                        ValWarn = False
                      end
                      object EdIPServer: TdmkEdit
                        Left = 276
                        Top = 180
                        Width = 93
                        Height = 21
                        TabOrder = 5
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '127.0.0.1'
                        QryCampo = 'DirPedCan'
                        UpdCampo = 'DirPedCan'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = '127.0.0.1'
                        ValWarn = False
                      end
                      object EdPorta: TdmkEdit
                        Left = 372
                        Top = 180
                        Width = 37
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 6
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '3306'
                        QryCampo = 'DirPedCan'
                        UpdCampo = 'DirPedCan'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 3306
                        ValWarn = False
                      end
                      object Panel19: TPanel
                        Left = 899
                        Top = 0
                        Width = 403
                        Height = 209
                        Align = alRight
                        Caption = 'Panel19'
                        TabOrder = 7
                        ExplicitLeft = 816
                        object MeCmd: TMemo
                          Left = 1
                          Top = 1
                          Width = 401
                          Height = 56
                          Align = alTop
                          TabOrder = 0
                        end
                        object MeResult_: TMemo
                          Left = 1
                          Top = 57
                          Width = 401
                          Height = 151
                          Align = alClient
                          Color = clBlack
                          Font.Charset = ANSI_CHARSET
                          Font.Color = clSilver
                          Font.Height = -15
                          Font.Name = 'Courier'
                          Font.Style = [fsBold]
                          Lines.Strings = (
                            'Teste 1'
                            'C:\Meus '
                            'Documentos')
                          ParentFont = False
                          ReadOnly = True
                          TabOrder = 1
                        end
                      end
                    end
                    object Panel17: TPanel
                      Left = 0
                      Top = 209
                      Width = 261
                      Height = 302
                      Align = alLeft
                      Caption = 'Panel9'
                      TabOrder = 1
                      ExplicitHeight = 259
                      object DBGTabelas: TdmkDBGridZTO
                        Left = 1
                        Top = 57
                        Width = 259
                        Height = 201
                        Align = alLeft
                        DataSource = DsTabelas
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                        TabOrder = 0
                        TitleFont.Charset = ANSI_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Tabela'
                            Width = 201
                            Visible = True
                          end>
                      end
                      object Panel18: TPanel
                        Left = 1
                        Top = 1
                        Width = 259
                        Height = 56
                        Align = alTop
                        BevelOuter = bvNone
                        TabOrder = 1
                        object BtTodosDBAnt: TBitBtn
                          Tag = 127
                          Left = 6
                          Top = 8
                          Width = 120
                          Height = 40
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = '&Todos'
                          NumGlyphs = 2
                          TabOrder = 0
                          OnClick = BtTodosDBAntClick
                        end
                        object BtNenhumDBAnt: TBitBtn
                          Tag = 128
                          Left = 128
                          Top = 8
                          Width = 120
                          Height = 40
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = '&Nenhum'
                          NumGlyphs = 2
                          TabOrder = 1
                          OnClick = BtNenhumDBAntClick
                        end
                      end
                    end
                    object DBGrid3: TDBGrid
                      Left = 261
                      Top = 209
                      Width = 1041
                      Height = 302
                      Align = alClient
                      DataSource = DsFields
                      TabOrder = 2
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                  end
                  object TabSheet2: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Registros'
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 360
                      Width = 1219
                      Height = 65
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataSource = DsDBF
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                    object Panel7: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 276
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      TabOrder = 1
                      object GroupBox2: TGroupBox
                        Left = 1
                        Top = 1
                        Width = 98
                        Height = 274
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alLeft
                        Caption = ' C'#243'digos ini: '
                        TabOrder = 0
                        object Panel10: TPanel
                          Left = 2
                          Top = 15
                          Width = 94
                          Height = 257
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label3: TLabel
                            Left = 2
                            Top = 0
                            Width = 35
                            Height = 13
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cliente:'
                          end
                          object Label4: TLabel
                            Left = 2
                            Top = 42
                            Width = 57
                            Height = 13
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Fornecedor:'
                          end
                          object Label5: TLabel
                            Left = 2
                            Top = 83
                            Width = 45
                            Height = 13
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Entidade:'
                          end
                          object EdCliCodIni: TdmkEdit
                            Left = 1
                            Top = 17
                            Width = 74
                            Height = 21
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 0
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '10001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 10001
                            ValWarn = False
                          end
                          object EdForCodIni: TdmkEdit
                            Left = 1
                            Top = 58
                            Width = 74
                            Height = 21
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 1
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '20001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 20001
                            ValWarn = False
                          end
                          object EdEntCodIni: TdmkEdit
                            Left = 1
                            Top = 99
                            Width = 74
                            Height = 21
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 2
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '30001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 30001
                            ValWarn = False
                          end
                          object BtTudo: TBitBtn
                            Tag = 127
                            Left = 1
                            Top = 124
                            Width = 90
                            Height = 40
                            Caption = '&Todos'
                            NumGlyphs = 2
                            TabOrder = 3
                            OnClick = BtTudoClick
                          end
                          object BtNenhum: TBitBtn
                            Tag = 128
                            Left = 1
                            Top = 164
                            Width = 90
                            Height = 40
                            Caption = '&Nenhum'
                            NumGlyphs = 2
                            TabOrder = 4
                            OnClick = BtNenhumClick
                          end
                          object BitBtn4: TBitBtn
                            Tag = 163
                            Left = 2
                            Top = 204
                            Width = 90
                            Height = 40
                            Caption = 'Tel + Mail'
                            TabOrder = 5
                            OnClick = BitBtn4Click
                          end
                        end
                      end
                      object CGDBF: TdmkCheckGroup
                        Left = 99
                        Top = 1
                        Width = 1119
                        Height = 274
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Caption = ' Tabelas: '
                        Columns = 2
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Items.Strings = (
                          'Gerencia           [SIM]   (Cadastro da Empresa)'
                          'Loja               [SIM]   (Cadastro da loja)'
                          'StatusCliente      [SIM]   (Cadastro de Status de entidades)'
                          'Cliente            [SIM]   (Cadastro de entidades)'
                          'ClienteStatus      [SIM]   (Status das entidades)'
                          'AnexoDocumento     [SIM]   (documentos anexados)'
                          'Grupo              [SIM]   (Grupos de Grade)'
                          'Marca              [SIM]   (Marcas de equipamentos)'
                          'Linha              [SIM]   (Nivel 2 Grade)'
                          
                            'Produto            [SIM]   (Nivel 1 GradeProddutos loca'#231#227'o e ven' +
                            'da)'
                          'ProdutoAcessorio   [SIM]   (Acess'#243'rios de equipamentos)'
                          
                            'FornecedorProduto  [SIM]   (cadastros de c'#243'digos de produtos de ' +
                            'XML de Fornecedires)'
                          'Locacao            [SIM]   (Cabel'#231'alho das loca'#231#245'es)'
                          'LocacaoItens       [SIM]   (Itens de loca'#231#245'es)'
                          
                            'LocacaoTrocaMotivo [SIM]   (Cadastro de motivos de trocas de equ' +
                            'ip.)'
                          'LocacaoMovimento   [SIM]   (Movimenta'#231#245'es de Itens de loca'#231#245'es)'
                          'LocacaoHistorico   [SIM]   (Hist'#243'rico de loca'#231#245'es)'
                          'Lancamento         [SIM]   (Lan'#231'amentos de contas a receber?)'
                          'ClienteFone        [SIM]   (Telefones de entidades)'
                          'ClienteEmail       [SIM]   (Emails de entidades)'
                          '')
                        ParentFont = False
                        TabOrder = 1
                        OnClick = CGDBFClick
                        UpdType = utYes
                        Value = 0
                        OldValor = 0
                      end
                    end
                    object MePronto: TMemo
                      Left = 0
                      Top = 424
                      Width = 1219
                      Height = 44
                      Align = alBottom
                      TabOrder = 2
                    end
                    object Panel11: TPanel
                      Left = 0
                      Top = 276
                      Width = 1219
                      Height = 29
                      Align = alTop
                      Caption = 'Panel11'
                      TabOrder = 3
                      object Label7: TLabel
                        Left = 104
                        Top = 8
                        Width = 27
                        Height = 13
                        Caption = 'Pular:'
                        Enabled = False
                      end
                      object Label8: TLabel
                        Left = 244
                        Top = 8
                        Width = 47
                        Height = 13
                        Caption = 'Registros:'
                      end
                      object CkLimit: TCheckBox
                        Left = 4
                        Top = 8
                        Width = 97
                        Height = 17
                        Caption = 'Limitar dados:'
                        Checked = True
                        Enabled = False
                        State = cbChecked
                        TabOrder = 0
                      end
                      object EdSkip: TdmkEdit
                        Left = 140
                        Top = 4
                        Width = 92
                        Height = 21
                        Alignment = taRightJustify
                        Enabled = False
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                      end
                      object EdFirst: TdmkEdit
                        Left = 300
                        Top = 4
                        Width = 92
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 2
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '500'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 500
                        ValWarn = False
                      end
                    end
                  end
                  object TabSheet3: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'SQL'
                    ImageIndex = 1
                    object Splitter1: TSplitter
                      Left = 0
                      Top = 105
                      Width = 1302
                      Height = 6
                      Cursor = crVSplit
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      ExplicitWidth = 1214
                    end
                    object Panel9: TPanel
                      Left = 0
                      Top = 0
                      Width = 1302
                      Height = 105
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      TabOrder = 0
                      ExplicitWidth = 1219
                      object MeSQL1: TMemo
                        Left = 1
                        Top = 1
                        Width = 1135
                        Height = 103
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Lines.Strings = (
                          'SELECT * FROM CADCLI'
                          '')
                        ParentFont = False
                        TabOrder = 0
                        OnKeyDown = MeSQL1KeyDown
                        ExplicitWidth = 1052
                      end
                      object Panel8: TPanel
                        Left = 1136
                        Top = 1
                        Width = 165
                        Height = 103
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alRight
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        ExplicitLeft = 1053
                        object BtExecuta: TBitBtn
                          Tag = 14
                          Left = 6
                          Top = 5
                          Width = 120
                          Height = 40
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = '&Executa'
                          NumGlyphs = 2
                          TabOrder = 0
                          OnClick = BtExecutaClick
                        end
                      end
                    end
                    object DBGrid2: TDBGrid
                      Left = 0
                      Top = 111
                      Width = 1302
                      Height = 400
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataSource = DsPsq
                      TabOrder = 1
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                  end
                  object TabSheet4: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Erros Endere'#231'os'
                    ImageIndex = 2
                    object Memo1: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 468
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet5: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Erros Loca'#231#245'es'
                    ImageIndex = 3
                    object Memo2: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 468
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet6: TTabSheet
                    Caption = ' Erros Gerais '
                    ImageIndex = 4
                    object MeErrosGerais: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 468
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet7: TTabSheet
                    Caption = 'Teste'
                    ImageIndex = 5
                    object dmkDBGridZTO1: TdmkDBGridZTO
                      Left = 0
                      Top = 177
                      Width = 1219
                      Height = 291
                      Align = alClient
                      DataSource = DsQry
                      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      RowColors = <>
                    end
                    object Panel21: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 177
                      Align = alTop
                      TabOrder = 1
                      object MeQry: TMemo
                        Left = 1
                        Top = 1
                        Width = 1217
                        Height = 175
                        Align = alClient
                        Color = clBlack
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWhite
                        Font.Height = -13
                        Font.Name = 'Lucida Console'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 0
                        OnKeyDown = MeQryKeyDown
                      end
                    end
                  end
                  object TabSheet9: TTabSheet
                    Caption = 'Contas'
                    ImageIndex = 7
                    object Panel20: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 468
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object SGContas: TStringGrid
                        Left = 0
                        Top = 0
                        Width = 1219
                        Height = 468
                        Align = alClient
                        ColCount = 4
                        DefaultColWidth = 24
                        DefaultRowHeight = 21
                        TabOrder = 0
                        ColWidths = (
                          24
                          51
                          91
                          590)
                      end
                    end
                  end
                end
                object GroupBox3: TGroupBox
                  Left = 1
                  Top = 540
                  Width = 1310
                  Height = 101
                  Align = alBottom
                  Caption = ' Avisos: '
                  TabOrder = 1
                  ExplicitTop = 497
                  ExplicitWidth = 1227
                  object Panel13: TPanel
                    Left = 2
                    Top = 15
                    Width = 1223
                    Height = 84
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Panel4: TPanel
                      Left = 0
                      Top = 0
                      Width = 1097
                      Height = 84
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object LaAvisoG1: TLabel
                        Left = 13
                        Top = 34
                        Width = 120
                        Height = 16
                        Caption = '..............................'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clSilver
                        Font.Height = -13
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        Transparent = True
                      end
                      object LaAvisoB1: TLabel
                        Left = 13
                        Top = 18
                        Width = 120
                        Height = 16
                        Caption = '..............................'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clSilver
                        Font.Height = -13
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        Transparent = True
                      end
                      object LaAvisoG2: TLabel
                        Left = 12
                        Top = 33
                        Width = 120
                        Height = 16
                        Caption = '..............................'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clGreen
                        Font.Height = -13
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        Transparent = True
                      end
                      object LaAvisoB2: TLabel
                        Left = 12
                        Top = 17
                        Width = 120
                        Height = 16
                        Caption = '..............................'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlue
                        Font.Height = -13
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        Transparent = True
                      end
                      object LaAvisoR1: TLabel
                        Left = 13
                        Top = 2
                        Width = 120
                        Height = 16
                        Caption = '..............................'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clSilver
                        Font.Height = -13
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        Transparent = True
                      end
                      object LaAvisoR2: TLabel
                        Left = 12
                        Top = 1
                        Width = 120
                        Height = 16
                        Caption = '..............................'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clRed
                        Font.Height = -13
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        Transparent = True
                      end
                      object PB1: TProgressBar
                        Left = 0
                        Top = 67
                        Width = 1097
                        Height = 17
                        Align = alBottom
                        TabOrder = 0
                      end
                      object PB2: TProgressBar
                        Left = 0
                        Top = 50
                        Width = 1097
                        Height = 17
                        Align = alBottom
                        TabOrder = 1
                      end
                    end
                    object Panel14: TPanel
                      Left = 1209
                      Top = 0
                      Width = 14
                      Height = 84
                      Align = alRight
                      BevelOuter = bvNone
                      TabOrder = 1
                    end
                    object Panel15: TPanel
                      Left = 1141
                      Top = 0
                      Width = 68
                      Height = 84
                      Align = alRight
                      BevelOuter = bvNone
                      TabOrder = 2
                      object LaTempoI: TLabel
                        Left = 0
                        Top = 0
                        Width = 68
                        Height = 14
                        Align = alTop
                        Alignment = taRightJustify
                        AutoSize = False
                        Caption = '...'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clRed
                        Font.Height = -11
                        Font.Name = 'Arial'
                        Font.Style = [fsBold]
                        ParentFont = False
                        ExplicitLeft = 1
                        ExplicitTop = 1
                        ExplicitWidth = 9
                      end
                      object LaTempoF: TLabel
                        Left = 0
                        Top = 14
                        Width = 68
                        Height = 14
                        Align = alTop
                        Alignment = taRightJustify
                        AutoSize = False
                        Caption = '...'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clGreen
                        Font.Height = -11
                        Font.Name = 'Arial'
                        Font.Style = [fsBold]
                        ParentFont = False
                        ExplicitLeft = 1
                        ExplicitTop = 15
                        ExplicitWidth = 9
                      end
                      object LaTempoT: TLabel
                        Left = 0
                        Top = 28
                        Width = 68
                        Height = 14
                        Align = alTop
                        Alignment = taRightJustify
                        AutoSize = False
                        Caption = '...'
                        Color = clBtnFace
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clBlue
                        Font.Height = -11
                        Font.Name = 'Arial'
                        Font.Style = [fsBold]
                        ParentColor = False
                        ParentFont = False
                        Transparent = True
                        ExplicitLeft = -19
                        ExplicitTop = 63
                        ExplicitWidth = 339
                      end
                    end
                    object Panel16: TPanel
                      Left = 1097
                      Top = 0
                      Width = 44
                      Height = 84
                      Align = alRight
                      BevelOuter = bvNone
                      TabOrder = 3
                      object Label10: TLabel
                        Left = 0
                        Top = 0
                        Width = 44
                        Height = 14
                        Align = alTop
                        AutoSize = False
                        Caption = 'In'#237'cio:'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        ExplicitLeft = 1
                        ExplicitTop = 1
                        ExplicitWidth = 9
                      end
                      object Label14: TLabel
                        Left = 0
                        Top = 14
                        Width = 44
                        Height = 14
                        Align = alTop
                        AutoSize = False
                        Caption = 'T'#233'rmino:'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentFont = False
                        ExplicitLeft = 1
                        ExplicitTop = 15
                        ExplicitWidth = 9
                      end
                      object Label15: TLabel
                        Left = 0
                        Top = 28
                        Width = 44
                        Height = 14
                        Align = alTop
                        AutoSize = False
                        Caption = 'Tempo:'
                        Color = clBtnFace
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -11
                        Font.Name = 'Arial'
                        Font.Style = []
                        ParentColor = False
                        ParentFont = False
                        Transparent = True
                        ExplicitLeft = -19
                        ExplicitTop = 63
                        ExplicitWidth = 339
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 742
    Width = 1324
    Height = 72
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1096
      Top = 15
      Width = 143
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 15
        Top = 4
        Width = 120
        Height = 44
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1094
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImportaDados: TBitBtn
        Tag = 163
        Left = 499
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&5.  Importa dados'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtImportaDadosClick
      end
      object Button1: TButton
        Left = 1029
        Top = -9
        Width = 60
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Situa'#231#227'o'
        TabOrder = 5
        OnClick = Button1Click
      end
      object BitBtn2: TBitBtn
        Left = 968
        Top = 24
        Width = 121
        Height = 25
        Caption = 'Seq. its Loca'#231#245'es'
        TabOrder = 6
        OnClick = BitBtn2Click
      end
      object BtCriaEstrutura: TBitBtn
        Tag = 163
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Caption = '&3. Cria estrutura'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCriaEstruturaClick
      end
      object BtEspelha: TBitBtn
        Tag = 163
        Left = 376
        Top = 4
        Width = 120
        Height = 40
        Caption = '&4. Espelha'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtEspelhaClick
      end
      object BitBtn3: TBitBtn
        Tag = 163
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = '&1. Restaura FB'
        TabOrder = 0
        OnClick = BitBtn3Click
      end
      object BtIB_DB: TBitBtn
        Tag = 163
        Left = 128
        Top = 4
        Width = 120
        Height = 41
        Caption = '&2. Conecta'
        TabOrder = 1
        OnClick = BtIB_DBClick
      end
      object BtCorrige: TBitBtn
        Tag = 163
        Left = 623
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&6.  Corrige ERP'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtCorrigeClick
      end
      object Button2: TButton
        Left = 969
        Top = -9
        Width = 60
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'S.T.'
        TabOrder = 8
        OnClick = Button2Click
      end
      object BitBtn1: TBitBtn
        Tag = 163
        Left = 747
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&7. XML'
        NumGlyphs = 2
        TabOrder = 9
        OnClick = BitBtn1Click
      end
    end
  end
  object DsDBF: TDataSource
    Left = 496
    Top = 12
  end
  object DsCadCli: TDataSource
    Left = 556
    Top = 12
  end
  object DsPsq: TDataSource
    Left = 468
    Top = 40
  end
  object QrPsqEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE Antigo=:P0')
    Left = 496
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCadFor: TDataSource
    Left = 556
    Top = 40
  end
  object DsCadGru: TDataSource
    Left = 612
    Top = 12
  end
  object DataSource1: TDataSource
    Left = 612
    Top = 40
  end
  object QrExiste: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1 ID'
      'FROM gragru1'
      'WHERE Referencia=:P0')
    Left = 320
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX, AGRPAT'
      'FROM gragxpatr ')
    Left = 364
    Top = 380
    object QrGraGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Required = True
      Size = 25
    end
    object QrGraGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DataSource2: TDataSource
    Left = 668
    Top = 40
  end
  object DsCadVde: TDataSource
    Left = 668
    Top = 12
  end
  object QrGraFabMar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM grafabmar'
      'WHERE Nome=:P0')
    Left = 408
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraFabMarControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSituacao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Situacao, '
      'CHAR(Situacao) SIGLA,'
      'COUNT(Situacao) ITENS'
      'FROM gragxpatr'
      'GROUP BY Situacao'
      'ORDER BY ITENS DESC')
    Left = 444
    Top = 380
    object QrSituacaoSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrSituacaoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Required = True
      Size = 4
    end
    object QrSituacaoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DataSource3: TDataSource
    Left = 724
    Top = 12
  end
  object DataSource4: TDataSource
    Left = 724
    Top = 40
  end
  object DataSource5: TDataSource
    Left = 780
    Top = 12
  end
  object DataSource6: TDataSource
    Left = 780
    Top = 40
  end
  object DBFB: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'Database=C:\Dermatek\Conversao\Tisolin\Tisolin.FDB'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet='
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False'
      
        'ConnectionString=DriverName=Firebird,Database=C:\Dermatek\Conver' +
        'sao\Tisolin\Tisolin.FDB,RoleName=RoleName,User_Name=sysdba,Passw' +
        'ord=masterkey,ServerCharSet=,SQLDialect=3,ErrorResourceFile=,Loc' +
        'aleCode=0000,BlobSize=-1,CommitRetain=False,WaitOnLocks=True,Iso' +
        'lationLevel=ReadCommitted,Trim Char=False'
      '')
    Left = 36
    Top = 432
  end
  object SDTabelas: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 36
    Top = 480
  end
  object SDDados: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 36
    Top = 528
  end
  object SDCliente: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    FieldDefs = <
      item
        Name = 'CLIENTE'
        Attributes = [faRequired]
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'RAZAOSOCIAL'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'FANTASIA'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TIPOPESSOA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CPFCGC'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'RGINSCRICAOESTADUAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NATURALIDADE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECONUMERO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'BAIRRO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'COMPLEMENTO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CIDADE'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'FONE1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FONE2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CELULAR'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ANIVERSARIOABERTURA'
        DataType = ftDate
      end
      item
        Name = 'TIPOCADASTRO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CIDADECOBRANCA'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'ENDERECOCOBRANCA'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'BAIRROENDERECOCOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CEPENDERECOCOBRANCA'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'COMPLEMENTOCOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'LOCALTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FONETRABALHO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TEMPOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FUNCAOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'RENDATRABALHO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'NOMECONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'LOCALTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FONETRABALHOCONJUGE'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TEMPOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FUNCAOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'RENDACONJUGE'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'PAI'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MAE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIA1'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_1'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'REFERENCIA2'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_2'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'REFERENCIA3'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_3'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'OBSERVACAO'
        DataType = ftString
        Size = 10000
      end
      item
        Name = 'INFORMACOES'
        DataType = ftString
        Size = 10000
      end
      item
        Name = 'EMAILPRIMARIO'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'CREDITO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'ISENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONDICAO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'PARCELAS'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'INTERVALO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'DATACADASTRO'
        DataType = ftDate
      end
      item
        Name = 'DATAATUALIZACAO'
        DataType = ftDate
      end
      item
        Name = 'ULTIMACOMPRA'
        DataType = ftDate
      end
      item
        Name = 'USUARIOCADASTRO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'USUARIOATUALIZACAO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ENDERECOENTREGA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'OBSERVACAOCLIENTE'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'LIBERADODOCUMENTOVENCIDO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OBSERVACAOVENDA'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DESCONTO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'VENDEDOR'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'OBSERVACAOTRAVAMENTO'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TRAVAMENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LIBERADOUMAFATURA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ATIVO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ORGAOPUBLICO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NCM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PASTAXML'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DIACOBRANCA'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'OBSERVACAODIACOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'EMAILFINANCEIRO'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'NAOCONTRIBUINTE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONTRIBUINTEISENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONTATO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FATURAR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'COBRADOR'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CODCTC'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'IMPRIMEPRECOLIQUIDO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMEROPEDIDOITEM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INSCRICAOMUNICIPAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIMPLESNACIONAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ALIQUOTAISS'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'RETENCAOISS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LISTASTATUS'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 384
    Top = 484
    object SDClienteCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDClienteRAZAOSOCIAL: TStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 200
    end
    object SDClienteFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 100
    end
    object SDClienteTIPOPESSOA: TStringField
      FieldName = 'TIPOPESSOA'
      Size = 1
    end
    object SDClienteCPFCGC: TStringField
      FieldName = 'CPFCGC'
    end
    object SDClienteRGINSCRICAOESTADUAL: TStringField
      FieldName = 'RGINSCRICAOESTADUAL'
    end
    object SDClienteNATURALIDADE: TStringField
      FieldName = 'NATURALIDADE'
      Size = 50
    end
    object SDClienteENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object SDClienteENDERECONUMERO: TFMTBCDField
      FieldName = 'ENDERECONUMERO'
      Precision = 18
      Size = 0
    end
    object SDClienteBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 50
    end
    object SDClienteCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 50
    end
    object SDClienteCIDADE: TFMTBCDField
      FieldName = 'CIDADE'
      Precision = 18
      Size = 0
    end
    object SDClienteCEP: TStringField
      FieldName = 'CEP'
      Size = 9
    end
    object SDClienteFONE1: TStringField
      FieldName = 'FONE1'
      Size = 30
    end
    object SDClienteFONE2: TStringField
      FieldName = 'FONE2'
      Size = 30
    end
    object SDClienteCELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 30
    end
    object SDClienteANIVERSARIOABERTURA: TDateField
      FieldName = 'ANIVERSARIOABERTURA'
    end
    object SDClienteTIPOCADASTRO: TFMTBCDField
      FieldName = 'TIPOCADASTRO'
      Precision = 18
      Size = 0
    end
    object SDClienteCIDADECOBRANCA: TFMTBCDField
      FieldName = 'CIDADECOBRANCA'
      Precision = 18
      Size = 0
    end
    object SDClienteENDERECOCOBRANCA: TStringField
      FieldName = 'ENDERECOCOBRANCA'
      Size = 80
    end
    object SDClienteBAIRROENDERECOCOBRANCA: TStringField
      FieldName = 'BAIRROENDERECOCOBRANCA'
      Size = 50
    end
    object SDClienteCEPENDERECOCOBRANCA: TStringField
      FieldName = 'CEPENDERECOCOBRANCA'
      Size = 9
    end
    object SDClienteCOMPLEMENTOCOBRANCA: TStringField
      FieldName = 'COMPLEMENTOCOBRANCA'
      Size = 50
    end
    object SDClienteLOCALTRABALHO: TStringField
      FieldName = 'LOCALTRABALHO'
      Size = 50
    end
    object SDClienteENDERECOTRABALHO: TStringField
      FieldName = 'ENDERECOTRABALHO'
      Size = 50
    end
    object SDClienteFONETRABALHO: TStringField
      FieldName = 'FONETRABALHO'
      Size = 15
    end
    object SDClienteTEMPOTRABALHO: TStringField
      FieldName = 'TEMPOTRABALHO'
      Size = 50
    end
    object SDClienteFUNCAOTRABALHO: TStringField
      FieldName = 'FUNCAOTRABALHO'
      Size = 50
    end
    object SDClienteRENDATRABALHO: TFMTBCDField
      FieldName = 'RENDATRABALHO'
      Precision = 18
      Size = 2
    end
    object SDClienteNOMECONJUGE: TStringField
      FieldName = 'NOMECONJUGE'
      Size = 50
    end
    object SDClienteLOCALTRABALHOCONJUGE: TStringField
      FieldName = 'LOCALTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteENDERECOTRABALHOCONJUGE: TStringField
      FieldName = 'ENDERECOTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteFONETRABALHOCONJUGE: TStringField
      FieldName = 'FONETRABALHOCONJUGE'
      Size = 15
    end
    object SDClienteTEMPOTRABALHOCONJUGE: TStringField
      FieldName = 'TEMPOTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteFUNCAOTRABALHOCONJUGE: TStringField
      FieldName = 'FUNCAOTRABALHOCONJUGE'
      Size = 50
    end
    object SDClienteRENDACONJUGE: TFMTBCDField
      FieldName = 'RENDACONJUGE'
      Precision = 18
      Size = 2
    end
    object SDClientePAI: TStringField
      FieldName = 'PAI'
      Size = 50
    end
    object SDClienteMAE: TStringField
      FieldName = 'MAE'
      Size = 50
    end
    object SDClienteREFERENCIA1: TStringField
      FieldName = 'REFERENCIA1'
      Size = 50
    end
    object SDClienteREFERENCIAFONE_1: TStringField
      FieldName = 'REFERENCIAFONE_1'
      Size = 15
    end
    object SDClienteREFERENCIA2: TStringField
      FieldName = 'REFERENCIA2'
      Size = 50
    end
    object SDClienteREFERENCIAFONE_2: TStringField
      FieldName = 'REFERENCIAFONE_2'
      Size = 15
    end
    object SDClienteREFERENCIA3: TStringField
      FieldName = 'REFERENCIA3'
      Size = 50
    end
    object SDClienteREFERENCIAFONE_3: TStringField
      FieldName = 'REFERENCIAFONE_3'
      Size = 15
    end
    object SDClienteOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 10000
    end
    object SDClienteINFORMACOES: TStringField
      FieldName = 'INFORMACOES'
      Size = 10000
    end
    object SDClienteEMAILPRIMARIO: TStringField
      FieldName = 'EMAILPRIMARIO'
      Size = 1024
    end
    object SDClienteCREDITO: TFMTBCDField
      FieldName = 'CREDITO'
      Precision = 18
      Size = 2
    end
    object SDClienteISENTO: TStringField
      FieldName = 'ISENTO'
      Size = 1
    end
    object SDClienteCONDICAO: TFMTBCDField
      FieldName = 'CONDICAO'
      Precision = 18
      Size = 0
    end
    object SDClientePARCELAS: TFMTBCDField
      FieldName = 'PARCELAS'
      Precision = 18
      Size = 0
    end
    object SDClienteINTERVALO: TFMTBCDField
      FieldName = 'INTERVALO'
      Precision = 18
      Size = 0
    end
    object SDClienteDATACADASTRO: TDateField
      FieldName = 'DATACADASTRO'
    end
    object SDClienteDATAATUALIZACAO: TDateField
      FieldName = 'DATAATUALIZACAO'
    end
    object SDClienteULTIMACOMPRA: TDateField
      FieldName = 'ULTIMACOMPRA'
    end
    object SDClienteUSUARIOCADASTRO: TStringField
      FieldName = 'USUARIOCADASTRO'
      Size = 10
    end
    object SDClienteUSUARIOATUALIZACAO: TStringField
      FieldName = 'USUARIOATUALIZACAO'
      Size = 10
    end
    object SDClienteENDERECOENTREGA: TStringField
      FieldName = 'ENDERECOENTREGA'
      Size = 50
    end
    object SDClienteOBSERVACAOCLIENTE: TStringField
      FieldName = 'OBSERVACAOCLIENTE'
      Size = 1024
    end
    object SDClienteLIBERADODOCUMENTOVENCIDO: TStringField
      FieldName = 'LIBERADODOCUMENTOVENCIDO'
      Size = 1
    end
    object SDClienteOBSERVACAOVENDA: TStringField
      FieldName = 'OBSERVACAOVENDA'
      Size = 100
    end
    object SDClienteDESCONTO: TFMTBCDField
      FieldName = 'DESCONTO'
      Precision = 18
      Size = 2
    end
    object SDClienteVENDEDOR: TFMTBCDField
      FieldName = 'VENDEDOR'
      Precision = 18
      Size = 0
    end
    object SDClienteOBSERVACAOTRAVAMENTO: TStringField
      FieldName = 'OBSERVACAOTRAVAMENTO'
      Size = 100
    end
    object SDClienteTRAVAMENTO: TStringField
      FieldName = 'TRAVAMENTO'
      Size = 1
    end
    object SDClienteLIBERADOUMAFATURA: TStringField
      FieldName = 'LIBERADOUMAFATURA'
      Size = 1
    end
    object SDClienteATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
    object SDClienteORGAOPUBLICO: TStringField
      FieldName = 'ORGAOPUBLICO'
      Size = 1
    end
    object SDClienteNCM: TStringField
      FieldName = 'NCM'
      Size = 1
    end
    object SDClientePASTAXML: TStringField
      FieldName = 'PASTAXML'
      Size = 100
    end
    object SDClienteDIACOBRANCA: TFMTBCDField
      FieldName = 'DIACOBRANCA'
      Precision = 18
      Size = 0
    end
    object SDClienteOBSERVACAODIACOBRANCA: TStringField
      FieldName = 'OBSERVACAODIACOBRANCA'
      Size = 50
    end
    object SDClienteEMAILFINANCEIRO: TStringField
      FieldName = 'EMAILFINANCEIRO'
      Size = 1024
    end
    object SDClienteNAOCONTRIBUINTE: TStringField
      FieldName = 'NAOCONTRIBUINTE'
      Size = 1
    end
    object SDClienteCONTRIBUINTEISENTO: TStringField
      FieldName = 'CONTRIBUINTEISENTO'
      Size = 1
    end
    object SDClienteCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 50
    end
    object SDClienteFATURAR: TStringField
      FieldName = 'FATURAR'
      Size = 1
    end
    object SDClienteCOBRADOR: TFMTBCDField
      FieldName = 'COBRADOR'
      Precision = 18
      Size = 0
    end
    object SDClienteCODCTC: TFMTBCDField
      FieldName = 'CODCTC'
      Precision = 18
      Size = 0
    end
    object SDClienteIMPRIMEPRECOLIQUIDO: TStringField
      FieldName = 'IMPRIMEPRECOLIQUIDO'
      Size = 1
    end
    object SDClienteNUMEROPEDIDOITEM: TStringField
      FieldName = 'NUMEROPEDIDOITEM'
      Size = 1
    end
    object SDClienteINSCRICAOMUNICIPAL: TStringField
      FieldName = 'INSCRICAOMUNICIPAL'
    end
    object SDClienteSIMPLESNACIONAL: TStringField
      FieldName = 'SIMPLESNACIONAL'
      Size = 1
    end
    object SDClienteALIQUOTAISS: TFMTBCDField
      FieldName = 'ALIQUOTAISS'
      Precision = 18
      Size = 2
    end
    object SDClienteRETENCAOISS: TStringField
      FieldName = 'RETENCAOISS'
      Size = 1
    end
    object SDClienteLISTASTATUS: TStringField
      FieldName = 'LISTASTATUS'
      Size = 50
    end
    object SDClienteCIDADE_DESCRICAO: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_DESCRICAO'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'DESCRICAO'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADE_IBGE: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_IBGE'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'IBGE'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADE_PAIS: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_PAIS'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'PAIS'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADE_UF: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADE_UF'
      LookupDataSet = SDCidade
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'UF'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADC_IBGE: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADC_IBGE'
      LookupDataSet = SDCidadC
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'IBGE'
      KeyFields = 'CIDADE'
      Size = 255
      Lookup = True
    end
    object SDClienteCIDADC_DESCRICAO: TStringField
      FieldKind = fkLookup
      FieldName = 'CIDADC_DESCRICAO'
      LookupDataSet = SDCidadC
      LookupKeyFields = 'CIDADE'
      LookupResultField = 'DESCRICAO'
      KeyFields = 'CIDADECOBRANCA'
      Size = 255
      Lookup = True
    end
  end
  object SDCidade: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'CIDADE'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 251
    Top = 540
  end
  object SDCidadC: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'CIDADE'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 247
    Top = 588
  end
  object SDLocacao: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 383
    Top = 536
    object SDLocacaoLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Precision = 18
      Size = 0
    end
    object SDLocacaoEMISSAODATA: TDateField
      FieldName = 'EMISSAODATA'
    end
    object SDLocacaoEMISSAOHORA: TTimeField
      FieldName = 'EMISSAOHORA'
    end
    object SDLocacaoDEVOLUCAODATA: TDateField
      FieldName = 'DEVOLUCAODATA'
    end
    object SDLocacaoDEVOLUCAOHORA: TTimeField
      FieldName = 'DEVOLUCAOHORA'
    end
    object SDLocacaoSAIDADATA: TDateField
      FieldName = 'SAIDADATA'
    end
    object SDLocacaoSAIDAHORA: TTimeField
      FieldName = 'SAIDAHORA'
    end
    object SDLocacaoFINALIZADODATA: TDateField
      FieldName = 'FINALIZADODATA'
    end
    object SDLocacaoFINALIZADOHORA: TTimeField
      FieldName = 'FINALIZADOHORA'
    end
    object SDLocacaoVALOREQUIPAMENTO: TFMTBCDField
      FieldName = 'VALOREQUIPAMENTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoVALORLOCACAO: TFMTBCDField
      FieldName = 'VALORLOCACAO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoVALORADICIONAL: TFMTBCDField
      FieldName = 'VALORADICIONAL'
      Precision = 18
      Size = 2
    end
    object SDLocacaoVALORADIANTAMENTO: TFMTBCDField
      FieldName = 'VALORADIANTAMENTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoTIPOALUGUEL: TStringField
      FieldName = 'TIPOALUGUEL'
      Size = 1
    end
    object SDLocacaoOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 10000
    end
    object SDLocacaoFECHAMENTODATA: TDateField
      FieldName = 'FECHAMENTODATA'
    end
    object SDLocacaoFECHAMENTOHORA: TTimeField
      FieldName = 'FECHAMENTOHORA'
    end
    object SDLocacaoUSUARIO: TStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object SDLocacaoSTATUS: TFMTBCDField
      FieldName = 'STATUS'
      Precision = 18
      Size = 0
    end
    object SDLocacaoVALORDESCONTO: TFMTBCDField
      FieldName = 'VALORDESCONTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoOBRA: TStringField
      FieldName = 'OBRA'
      Size = 10000
    end
    object SDLocacaoCANCELADA: TStringField
      FieldName = 'CANCELADA'
      Size = 1
    end
    object SDLocacaoULTIMACOBRANCAMENSAL: TDateField
      FieldName = 'ULTIMACOBRANCAMENSAL'
    end
  end
  object SDAux: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 604
    Top = 376
  end
  object MySQLBatchExecute1: TMySQLBatchExecute
    Database = Dmod.MyDB
    Delimiter = ';'
    Left = 504
    Top = 292
  end
  object SDLocacaoItens: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 387
    Top = 588
    object SDLocacaoItensLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensSEQUENCIA: TFMTBCDField
      FieldName = 'SEQUENCIA'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensQUANTIDADEPRODUTO: TFMTBCDField
      FieldName = 'QUANTIDADEPRODUTO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensVALORPRODUTO: TFMTBCDField
      FieldName = 'VALORPRODUTO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoItensQUANTIDADELOCACAO: TFMTBCDField
      FieldName = 'QUANTIDADELOCACAO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensVALORLOCACAO: TFMTBCDField
      FieldName = 'VALORLOCACAO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoItensQUANTIDADEDEVOLUCAO: TFMTBCDField
      FieldName = 'QUANTIDADEDEVOLUCAO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoItensTROCA: TStringField
      FieldName = 'TROCA'
      Size = 1
    end
    object SDLocacaoItensCATEGORIA: TStringField
      FieldName = 'CATEGORIA'
      Size = 1
    end
    object SDLocacaoItensCOBRANCALOCACAO: TStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object SDLocacaoItensCOBRANCACONSUMO: TFMTBCDField
      FieldName = 'COBRANCACONSUMO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoItensCOBRANCAREALIZADAVENDA: TStringField
      FieldName = 'COBRANCAREALIZADAVENDA'
      Size = 1
    end
    object SDLocacaoItensDATAINICIALCOBRANCA: TDateField
      FieldName = 'DATAINICIALCOBRANCA'
    end
    object SDLocacaoItensHORAINICIALCOBRANCA: TTimeField
      FieldName = 'HORAINICIALCOBRANCA'
    end
    object SDLocacaoItensDATASAIDA: TDateField
      FieldName = 'DATASAIDA'
    end
    object SDLocacaoItensHORASAIDA: TTimeField
      FieldName = 'HORASAIDA'
    end
  end
  object SDStatusCliente: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 463
    Top = 484
    object SDStatusClienteSTATUS: TFMTBCDField
      FieldName = 'STATUS'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDStatusClienteDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
  end
  object SDClienteStatus: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 467
    Top = 532
    object SDClienteStatusCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDClienteStatusSTATUS: TFMTBCDField
      FieldName = 'STATUS'
      Required = True
      Precision = 18
      Size = 0
    end
  end
  object SDProduto: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = 1
    DataSet.Params = <>
    Params = <>
    Left = 555
    Top = 484
    object SDProdutoPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDProdutoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object SDProdutoMAQ_BASE: TStringField
      FieldName = 'MAQ_BASE'
    end
    object SDProdutoMARCA: TStringField
      FieldName = 'MARCA'
      Size = 15
    end
    object SDProdutoIMPORTADO: TStringField
      FieldName = 'IMPORTADO'
      Size = 1
    end
    object SDProdutoGRUPO: TFMTBCDField
      FieldName = 'GRUPO'
      Precision = 18
      Size = 0
    end
    object SDProdutoMARGEM: TFMTBCDField
      FieldName = 'MARGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoUNIDADE: TStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object SDProdutoCUSTO: TFMTBCDField
      FieldName = 'CUSTO'
      Precision = 18
      Size = 4
    end
    object SDProdutoESTOQUE: TFMTBCDField
      FieldName = 'ESTOQUE'
      Precision = 18
      Size = 6
    end
    object SDProdutoPRECOVENDA: TFMTBCDField
      FieldName = 'PRECOVENDA'
      Precision = 18
      Size = 2
    end
    object SDProdutoATIVO: TStringField
      FieldName = 'ATIVO'
      Size = 1
    end
    object SDProdutoMARGEMSUBSTITUICAO: TFMTBCDField
      FieldName = 'MARGEMSUBSTITUICAO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPRODUTODECIMAL: TStringField
      FieldName = 'PRODUTODECIMAL'
      Size = 1
    end
    object SDProdutoDUPLICA: TStringField
      FieldName = 'DUPLICA'
      Size = 1
    end
    object SDProdutoPRECOPROMOCAO: TFMTBCDField
      FieldName = 'PRECOPROMOCAO'
      Precision = 18
      Size = 2
    end
    object SDProdutoDATAPROMOCAO: TDateField
      FieldName = 'DATAPROMOCAO'
    end
    object SDProdutoCLASSIFICACAOFISCAL: TFMTBCDField
      FieldName = 'CLASSIFICACAOFISCAL'
      Precision = 18
      Size = 0
    end
    object SDProdutoULTIMOFORNECEDOR: TFMTBCDField
      FieldName = 'ULTIMOFORNECEDOR'
      Precision = 18
      Size = 0
    end
    object SDProdutoULTIMANOTA: TStringField
      FieldName = 'ULTIMANOTA'
      Size = 10
    end
    object SDProdutoULTIMAQUANTIDADE: TFMTBCDField
      FieldName = 'ULTIMAQUANTIDADE'
      Precision = 18
      Size = 6
    end
    object SDProdutoULTIMADATA: TDateField
      FieldName = 'ULTIMADATA'
    end
    object SDProdutoPULTIMOFORNECEDOR: TFMTBCDField
      FieldName = 'PULTIMOFORNECEDOR'
      Precision = 18
      Size = 0
    end
    object SDProdutoPULTIMANOTA: TStringField
      FieldName = 'PULTIMANOTA'
      Size = 10
    end
    object SDProdutoPULTIMAQUANTIDADE: TFMTBCDField
      FieldName = 'PULTIMAQUANTIDADE'
      Precision = 18
      Size = 6
    end
    object SDProdutoPULTIMADATA: TDateField
      FieldName = 'PULTIMADATA'
    end
    object SDProdutoPULTIMOCUSTO: TFMTBCDField
      FieldName = 'PULTIMOCUSTO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMOPRECOVENDA: TFMTBCDField
      FieldName = 'PULTIMOPRECOVENDA'
      Precision = 18
      Size = 2
    end
    object SDProdutoDATAMVTOESTOQUE: TSQLTimeStampField
      FieldName = 'DATAMVTOESTOQUE'
    end
    object SDProdutoMENSAGEM: TStringField
      FieldName = 'MENSAGEM'
      Size = 60
    end
    object SDProdutoNCM: TStringField
      FieldName = 'NCM'
      Size = 8
    end
    object SDProdutoULTIMAATUALIZACAO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAO'
    end
    object SDProdutoULTIMOVALOR: TFMTBCDField
      FieldName = 'ULTIMOVALOR'
      Precision = 18
      Size = 6
    end
    object SDProdutoPULTIMOVALOR: TFMTBCDField
      FieldName = 'PULTIMOVALOR'
      Precision = 18
      Size = 6
    end
    object SDProdutoLINHA: TStringField
      FieldName = 'LINHA'
      Size = 3
    end
    object SDProdutoESTOQUEEXTERNO: TFMTBCDField
      FieldName = 'ESTOQUEEXTERNO'
      Precision = 18
      Size = 3
    end
    object SDProdutoDESCONTO: TFMTBCDField
      FieldName = 'DESCONTO'
      Precision = 18
      Size = 0
    end
    object SDProdutoBOCACAIXA: TFMTBCDField
      FieldName = 'BOCACAIXA'
      Precision = 18
      Size = 2
    end
    object SDProdutoSOMACUSTO: TFMTBCDField
      FieldName = 'SOMACUSTO'
      Precision = 18
      Size = 2
    end
    object SDProdutoTIPOITEM: TFMTBCDField
      FieldName = 'TIPOITEM'
      Precision = 18
      Size = 0
    end
    object SDProdutoGENERO: TFMTBCDField
      FieldName = 'GENERO'
      Precision = 18
      Size = 0
    end
    object SDProdutoMODELO: TFMTBCDField
      FieldName = 'MODELO'
      Precision = 18
      Size = 0
    end
    object SDProdutoSERIE: TStringField
      FieldName = 'SERIE'
      Size = 3
    end
    object SDProdutoREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
    end
    object SDProdutoPESOMAQUINA: TFMTBCDField
      FieldName = 'PESOMAQUINA'
      Precision = 18
      Size = 3
    end
    object SDProdutoCODIGOBARRAS: TStringField
      FieldName = 'CODIGOBARRAS'
    end
    object SDProdutoCODIGOFABRICA: TStringField
      FieldName = 'CODIGOFABRICA'
      Size = 50
    end
    object SDProdutoUSUARIOCADASTRO: TStringField
      FieldName = 'USUARIOCADASTRO'
      Size = 10
    end
    object SDProdutoUSUARIOALTERACAO: TStringField
      FieldName = 'USUARIOALTERACAO'
      Size = 10
    end
    object SDProdutoDATAHORACADASTRO: TSQLTimeStampField
      FieldName = 'DATAHORACADASTRO'
    end
    object SDProdutoCLA_TRI: TStringField
      FieldName = 'CLA_TRI'
      Size = 3
    end
    object SDProdutoPERCIPI: TFMTBCDField
      FieldName = 'PERCIPI'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCFRETEPROPRIO: TFMTBCDField
      FieldName = 'PERCFRETEPROPRIO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCFRETETERCEIRO: TFMTBCDField
      FieldName = 'PERCFRETETERCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCDESPESAS: TFMTBCDField
      FieldName = 'PERCDESPESAS'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCFINANCEIRO: TFMTBCDField
      FieldName = 'PERCFINANCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCPISCOFINSCREDITO: TFMTBCDField
      FieldName = 'PERCPISCOFINSCREDITO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCICMORIGEM: TFMTBCDField
      FieldName = 'PERCICMORIGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoPERCICMDESTINO: TFMTBCDField
      FieldName = 'PERCICMDESTINO'
      Precision = 18
      Size = 2
    end
    object SDProdutoTABELAFORNECEDOR: TFMTBCDField
      FieldName = 'TABELAFORNECEDOR'
      Precision = 18
      Size = 4
    end
    object SDProdutoCUSTOREPOSICAO: TFMTBCDField
      FieldName = 'CUSTOREPOSICAO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMOPERCFRETEPROPRIO: TFMTBCDField
      FieldName = 'PULTIMOPERCFRETEPROPRIO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCFRETETERCEIRO: TFMTBCDField
      FieldName = 'PULTIMOPERCFRETETERCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCDESPESAS: TFMTBCDField
      FieldName = 'PULTIMOPERCDESPESAS'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCFINANCEIRO: TFMTBCDField
      FieldName = 'PULTIMOPERCFINANCEIRO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCPISCOFINSCREDITO: TFMTBCDField
      FieldName = 'PULTIMOPERCPISCOFINSCREDITO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCICMORIGEM: TFMTBCDField
      FieldName = 'PULTIMOPERCICMORIGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOPERCICMDESTINO: TFMTBCDField
      FieldName = 'PULTIMOPERCICMDESTINO'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMOCUSTOREPOSICAO: TFMTBCDField
      FieldName = 'PULTIMOCUSTOREPOSICAO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMOPERCIPI: TFMTBCDField
      FieldName = 'PULTIMOPERCIPI'
      Precision = 18
      Size = 2
    end
    object SDProdutoPULTIMAMARGEMSUBSTITUICAO: TFMTBCDField
      FieldName = 'PULTIMAMARGEMSUBSTITUICAO'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMATABELAFORNECEDOR: TFMTBCDField
      FieldName = 'PULTIMATABELAFORNECEDOR'
      Precision = 18
      Size = 4
    end
    object SDProdutoPULTIMAMARGEM: TFMTBCDField
      FieldName = 'PULTIMAMARGEM'
      Precision = 18
      Size = 2
    end
    object SDProdutoQUANTIDADEEMBALAGEMCOMPRA: TFMTBCDField
      FieldName = 'QUANTIDADEEMBALAGEMCOMPRA'
      Precision = 18
      Size = 0
    end
    object SDProdutoSERVICO: TStringField
      FieldName = 'SERVICO'
      Size = 1
    end
    object SDProdutoLOCALIZACAO: TStringField
      FieldName = 'LOCALIZACAO'
    end
    object SDProdutoMULTIPLOVENDA: TFMTBCDField
      FieldName = 'MULTIPLOVENDA'
      Precision = 18
      Size = 0
    end
    object SDProdutoDESCRICAOETIQUETA: TStringField
      FieldName = 'DESCRICAOETIQUETA'
      Size = 100
    end
    object SDProdutoFICHATECNICA: TStringField
      FieldName = 'FICHATECNICA'
      Size = 10000
    end
    object SDProdutoIMAGEM: TMemoField
      FieldName = 'IMAGEM'
      BlobType = ftMemo
      Size = 1
    end
    object SDProdutoTEMFICHATECNICA: TStringField
      FieldName = 'TEMFICHATECNICA'
      Size = 1
    end
    object SDProdutoTEMIMAGEM: TStringField
      FieldName = 'TEMIMAGEM'
      Size = 1
    end
    object SDProdutoULTIMAATUALIZACAOIMAGEM: TDateField
      FieldName = 'ULTIMAATUALIZACAOIMAGEM'
    end
    object SDProdutoPRODUTONOVO: TFMTBCDField
      FieldName = 'PRODUTONOVO'
      Precision = 18
      Size = 0
    end
    object SDProdutoPATRIMONIO: TStringField
      FieldName = 'PATRIMONIO'
      Size = 50
    end
    object SDProdutoCATEGORIA: TStringField
      FieldName = 'CATEGORIA'
      Size = 10
    end
    object SDProdutoVALORDIARIA: TFMTBCDField
      FieldName = 'VALORDIARIA'
      Precision = 18
      Size = 2
    end
    object SDProdutoVALORSEMANAL: TFMTBCDField
      FieldName = 'VALORSEMANAL'
      Precision = 18
      Size = 2
    end
    object SDProdutoVALORQUINZENAL: TFMTBCDField
      FieldName = 'VALORQUINZENAL'
      Precision = 18
      Size = 2
    end
    object SDProdutoVALORMENSAL: TFMTBCDField
      FieldName = 'VALORMENSAL'
      Precision = 18
      Size = 2
    end
    object SDProdutoPRECOFDS: TFMTBCDField
      FieldName = 'PRECOFDS'
      Precision = 18
      Size = 2
    end
    object SDProdutoDIASCARENCIA: TFMTBCDField
      FieldName = 'DIASCARENCIA'
      Precision = 18
      Size = 0
    end
    object SDProdutoREDUTORDIARIA: TFMTBCDField
      FieldName = 'REDUTORDIARIA'
      Precision = 18
      Size = 2
    end
    object SDProdutoCOBRANCACONSUMO: TFMTBCDField
      FieldName = 'COBRANCACONSUMO'
      Precision = 18
      Size = 2
    end
  end
  object SDGrupo: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 552
    Top = 536
    object SDGrupoGRUPO: TFMTBCDField
      FieldName = 'GRUPO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDGrupoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
    object SDGrupoATUALIZA: TStringField
      FieldName = 'ATUALIZA'
      Size = 1
    end
    object SDGrupoULTIMAATUALIZACAO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAO'
    end
    object SDGrupoMENSAGEM: TStringField
      FieldName = 'MENSAGEM'
      Size = 1
    end
  end
  object SDLinha: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT FIRST 500  lin.Descricao, prd.Grupo, '#13#10' prd.Linha, COUNT(' +
      'prd.Produto) ITENS'#13#10' FROM produto prd'#13#10' LEFT JOIN linha lin ON (' +
      'lin.Linha=prd.Linha)'#13#10' GROUP BY prd.Linha, prd.Grupo, lin.Descri' +
      'cao'#13#10' ORDER BY prd.Linha, prd.Grupo'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 467
    Top = 584
    object SDLinhaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object SDLinhaGRUPO: TFMTBCDField
      FieldName = 'GRUPO'
      Precision = 18
      Size = 0
    end
    object SDLinhaLINHA: TStringField
      FieldName = 'LINHA'
      Size = 3
    end
    object SDLinhaITENS: TIntegerField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object SDMarca: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 551
    Top = 588
    object SDMarcaMARCA: TStringField
      FieldName = 'MARCA'
      Required = True
      Size = 15
    end
    object SDMarcaULTIMAATUALIZACAO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAO'
    end
  end
  object QrGraGX: TMySQLQuery
    Database = Dmod.MyDB
    Left = 531
    Top = 388
  end
  object SDGraGX: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT PRODUTO, ACESSORIO  '#13#10'FROM produtoacessorio '#13#10'WHERE PRODU' +
      'TO=2877 '#13#10'OR ACESSORIO=2877'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 643
    Top = 532
    object SDGraGXPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDGraGXACESSORIO: TFMTBCDField
      FieldName = 'ACESSORIO'
      Required = True
      Precision = 18
      Size = 0
    end
  end
  object SDProdutoAcessorio: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'PRODUTOACESSORIO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 643
    Top = 480
    object SDProdutoAcessorioPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDProdutoAcessorioACESSORIO: TFMTBCDField
      FieldName = 'ACESSORIO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDProdutoAcessorioQUANTIDADE: TFMTBCDField
      FieldName = 'QUANTIDADE'
      Precision = 18
      Size = 0
    end
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX, "gragxpatr" TABELA, '
      'Aplicacao '
      'FROM gragxpatr '
      'WHERE GraGruX=10  '
      ' '
      'UNION '
      ' '
      'SELECT GraGruX, "gragxoutr" TABELA, '
      'Aplicacao '
      'FROM gragxoutr '
      'WHERE GraGruX=10 ')
    Left = 643
    Top = 584
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItemTABELA: TWideStringField
      FieldName = 'TABELA'
      Required = True
      Size = 9
    end
    object QrItemAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Required = True
    end
  end
  object QrGraGruY: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _PRODUTO_ALL_; '
      'CREATE TABLE _PRODUTO_ALL_ '
      ' '
      'SELECT "S" GGY_KIND, Ativo, DESCRICAO,  '
      'PRODUTO, Linha, Categoria,  '
      'Referencia, Patrimonio '
      'FROM PRODUTO '
      'WHERE Categoria = "L" '
      'AND Patrimonio = "" '
      ' '
      'UNION '
      ' '
      'SELECT "P" GGY_KIND, Ativo, DESCRICAO,  '
      'PRODUTO, Linha, Categoria,  '
      'Referencia, Patrimonio '
      'FROM PRODUTO '
      'WHERE Categoria = "L" '
      'AND Patrimonio <> "" '
      ' '
      '; '
      ' '
      'DROP TABLE IF EXISTS _PRODUTO_ACE_; '
      'CREATE TABLE _PRODUTO_ACE_ '
      ' '
      'SELECT DISTINCT ACESSORIO '
      'FROM PRODUTOACESSORIO '
      '; '
      ' '
      'DROP TABLE IF EXISTS PRODUTOS_GGY; '
      'CREATE TABLE PRODUTOS_GGY '
      'SELECT IF(ace.ACESSORIO > 0, "A", pal.GGY_KIND) TipoProd,  '
      'pal.*  '
      'FROM _PRODUTO_ALL_ pal '
      'LEFT JOIN PRODUTOACESSORIO ace ON ace.ACESSORIO=pal.PRODUTO '
      ' '
      '; '
      'SELECT * FROM PRODUTOS_GGY'
      'ORDER BY GGY_KIND DESC, DESCRICAO '
      ';')
    Left = 823
    Top = 416
    object QrGraGruYTipoProd: TWideStringField
      FieldName = 'TipoProd'
      Required = True
      Size = 1
    end
    object QrGraGruYGGY_KIND: TWideStringField
      FieldName = 'GGY_KIND'
      Required = True
      Size = 1
    end
    object QrGraGruYAtivo: TWideStringField
      FieldName = 'Ativo'
      Size = 1
    end
    object QrGraGruYDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object QrGraGruYPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrGraGruYLinha: TWideStringField
      FieldName = 'Linha'
      Size = 3
    end
    object QrGraGruYCategoria: TWideStringField
      FieldName = 'Categoria'
      Size = 10
    end
    object QrGraGruYReferencia: TWideStringField
      FieldName = 'Referencia'
    end
    object QrGraGruYPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 50
    end
  end
  object FbDB: TMySQLDatabase
    DatabaseName = 'zzz_fb_db_ant'
    DesignOptions = []
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'Host=127.0.0.1'
      'DatabaseName=zzz_fb_db_ant')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 820
    Top = 480
  end
  object QrLocados: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _LOCACAOES_ABERTAS_; '
      'CREATE TABLE _LOCACAOES_ABERTAS_ '
      '      SELECT DISTINCT LOCACAO '
      '      FROM LOCACAO '
      '      WHERE FECHAMENTODATA < "1900-01-01" '
      '      AND STATUS<3; '
      'SELECT PRODUTO,  '
      '(QUANTIDADEPRODUTO-QUANTIDADEDEVOLUCAO) QtdLocado '
      'FROM LOCACAOITENS its '
      'WHERE CATEGORIA="L" '
      '/*AND PRODUTO  = 37*/ '
      'AND QUANTIDADEDEVOLUCAO<QUANTIDADEPRODUTO '
      'AND LOCACAO IN ( '
      '  SELECT LOCACAO FROM _LOCACAOES_ABERTAS_ '
      ') '
      'GROUP BY PRODUTO '
      'ORDER BY PRODUTO; ')
    Left = 823
    Top = 552
    object QrLocadosPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrLocadosQtdLocado: TFloatField
      FieldName = 'QtdLocado'
    end
  end
  object QrSeqIL: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, Its.*  '
      'FROM zzz_fb_db_ant.LOCACAOITENS its '
      'LEFT JOIN toolrent_tisolin.GraGruX ggx  '
      '  ON ggx.Controle=its.PRODUTO '
      '/*WHERE its.CATEGORIA = "L"*/ '
      'ORDER BY ggx.GraGruY, its.LOCACAO, its.SEQUENCIA ')
    Left = 935
    Top = 676
    object QrSeqILGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrSeqILLOCACAO: TLargeintField
      FieldName = 'LOCACAO'
      Required = True
    end
    object QrSeqILSEQUENCIA: TLargeintField
      FieldName = 'SEQUENCIA'
      Required = True
    end
    object QrSeqILPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
      Required = True
    end
    object QrSeqILQUANTIDADEPRODUTO: TLargeintField
      FieldName = 'QUANTIDADEPRODUTO'
    end
    object QrSeqILVALORPRODUTO: TFloatField
      FieldName = 'VALORPRODUTO'
    end
    object QrSeqILQUANTIDADELOCACAO: TLargeintField
      FieldName = 'QUANTIDADELOCACAO'
    end
    object QrSeqILVALORLOCACAO: TFloatField
      FieldName = 'VALORLOCACAO'
    end
    object QrSeqILQUANTIDADEDEVOLUCAO: TLargeintField
      FieldName = 'QUANTIDADEDEVOLUCAO'
    end
    object QrSeqILTROCA: TWideStringField
      FieldName = 'TROCA'
      Size = 1
    end
    object QrSeqILCATEGORIA: TWideStringField
      FieldName = 'CATEGORIA'
      Size = 1
    end
    object QrSeqILCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object QrSeqILCOBRANCACONSUMO: TFloatField
      FieldName = 'COBRANCACONSUMO'
    end
    object QrSeqILCOBRANCAREALIZADAVENDA: TWideStringField
      FieldName = 'COBRANCAREALIZADAVENDA'
      Size = 1
    end
    object QrSeqILDATAINICIALCOBRANCA: TDateField
      FieldName = 'DATAINICIALCOBRANCA'
    end
    object QrSeqILHORAINICIALCOBRANCA: TTimeField
      FieldName = 'HORAINICIALCOBRANCA'
    end
    object QrSeqILDATASAIDA: TDateField
      FieldName = 'DATASAIDA'
    end
    object QrSeqILHORASAIDA: TTimeField
      FieldName = 'HORASAIDA'
    end
  end
  object SDGerencia: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = 1
    DataSet.Params = <>
    FieldDefs = <
      item
        Name = 'CLIENTE'
        Attributes = [faRequired]
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'RAZAOSOCIAL'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'FANTASIA'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TIPOPESSOA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CPFCGC'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'RGINSCRICAOESTADUAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NATURALIDADE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECONUMERO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'BAIRRO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'COMPLEMENTO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CIDADE'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CEP'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'FONE1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FONE2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CELULAR'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'ANIVERSARIOABERTURA'
        DataType = ftDate
      end
      item
        Name = 'TIPOCADASTRO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CIDADECOBRANCA'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'ENDERECOCOBRANCA'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'BAIRROENDERECOCOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CEPENDERECOCOBRANCA'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'COMPLEMENTOCOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'LOCALTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FONETRABALHO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TEMPOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FUNCAOTRABALHO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'RENDATRABALHO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'NOMECONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'LOCALTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ENDERECOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FONETRABALHOCONJUGE'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TEMPOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FUNCAOTRABALHOCONJUGE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'RENDACONJUGE'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'PAI'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MAE'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIA1'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_1'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'REFERENCIA2'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_2'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'REFERENCIA3'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'REFERENCIAFONE_3'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'OBSERVACAO'
        DataType = ftString
        Size = 10000
      end
      item
        Name = 'INFORMACOES'
        DataType = ftString
        Size = 10000
      end
      item
        Name = 'EMAILPRIMARIO'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'CREDITO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'ISENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONDICAO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'PARCELAS'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'INTERVALO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'DATACADASTRO'
        DataType = ftDate
      end
      item
        Name = 'DATAATUALIZACAO'
        DataType = ftDate
      end
      item
        Name = 'ULTIMACOMPRA'
        DataType = ftDate
      end
      item
        Name = 'USUARIOCADASTRO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'USUARIOATUALIZACAO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ENDERECOENTREGA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'OBSERVACAOCLIENTE'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'LIBERADODOCUMENTOVENCIDO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OBSERVACAOVENDA'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DESCONTO'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'VENDEDOR'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'OBSERVACAOTRAVAMENTO'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TRAVAMENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LIBERADOUMAFATURA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ATIVO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ORGAOPUBLICO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NCM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PASTAXML'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DIACOBRANCA'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'OBSERVACAODIACOBRANCA'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'EMAILFINANCEIRO'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'NAOCONTRIBUINTE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONTRIBUINTEISENTO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONTATO'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FATURAR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'COBRADOR'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'CODCTC'
        DataType = ftFMTBcd
        Precision = 18
        Size = 4
      end
      item
        Name = 'IMPRIMEPRECOLIQUIDO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMEROPEDIDOITEM'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INSCRICAOMUNICIPAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIMPLESNACIONAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ALIQUOTAISS'
        DataType = ftFMTBcd
        Precision = 18
        Size = 2
      end
      item
        Name = 'RETENCAOISS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LISTASTATUS'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 120
    Top = 440
    object SDGerenciaSEQUENCIA: TFMTBCDField
      FieldName = 'SEQUENCIA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaRAZAOSOCIAL: TStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 50
    end
    object SDGerenciaVERSAO: TStringField
      FieldName = 'VERSAO'
      Size = 10
    end
    object SDGerenciaLOJA: TStringField
      FieldName = 'LOJA'
      Size = 3
    end
    object SDGerenciaFONE: TStringField
      FieldName = 'FONE'
      Size = 15
    end
    object SDGerenciaNOTAFISCAL: TFMTBCDField
      FieldName = 'NOTAFISCAL'
      Precision = 18
      Size = 0
    end
    object SDGerenciaINSCRICAOESTADUAL: TStringField
      FieldName = 'INSCRICAOESTADUAL'
    end
    object SDGerenciaCNPJ: TStringField
      FieldName = 'CNPJ'
    end
    object SDGerenciaCIDADE: TFMTBCDField
      FieldName = 'CIDADE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object SDGerenciaNUMERO: TFMTBCDField
      FieldName = 'NUMERO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 50
    end
    object SDGerenciaBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 50
    end
    object SDGerenciaCEP: TStringField
      FieldName = 'CEP'
      Size = 8
    end
    object SDGerenciaRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Size = 50
    end
    object SDGerenciaFONERESPONSAVEL: TStringField
      FieldName = 'FONERESPONSAVEL'
      Size = 12
    end
    object SDGerenciaIMPRESSORANOTAFISCAL: TStringField
      FieldName = 'IMPRESSORANOTAFISCAL'
      Size = 50
    end
    object SDGerenciaDATABACKUP: TDateField
      FieldName = 'DATABACKUP'
    end
    object SDGerenciaLOGOMENU: TBlobField
      FieldName = 'LOGOMENU'
      Size = 1
    end
    object SDGerenciaLOGO: TBlobField
      FieldName = 'LOGO'
      Size = 1
    end
    object SDGerenciaITENSORCAMENTO: TFMTBCDField
      FieldName = 'ITENSORCAMENTO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaPASTABACKUP: TStringField
      FieldName = 'PASTABACKUP'
      Size = 50
    end
    object SDGerenciaSERVERNAME: TStringField
      FieldName = 'SERVERNAME'
      Size = 50
    end
    object SDGerenciaDATABASENAME: TStringField
      FieldName = 'DATABASENAME'
      Size = 80
    end
    object SDGerenciaFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 50
    end
    object SDGerenciaHOST: TStringField
      FieldName = 'HOST'
      Size = 50
    end
    object SDGerenciaUSERNAME: TStringField
      FieldName = 'USERNAME'
      Size = 50
    end
    object SDGerenciaSENHA: TStringField
      FieldName = 'SENHA'
      Size = 50
    end
    object SDGerenciaSITE: TStringField
      FieldName = 'SITE'
      Size = 50
    end
    object SDGerenciaHISTORICOJUROS: TFMTBCDField
      FieldName = 'HISTORICOJUROS'
      Precision = 18
      Size = 0
    end
    object SDGerenciaPROXY: TStringField
      FieldName = 'PROXY'
      Size = 50
    end
    object SDGerenciaUSUARIOPROXY: TStringField
      FieldName = 'USUARIOPROXY'
      Size = 50
    end
    object SDGerenciaSENHAPROXY: TStringField
      FieldName = 'SENHAPROXY'
      Size = 50
    end
    object SDGerenciaEMAILNFE: TStringField
      FieldName = 'EMAILNFE'
      Size = 50
    end
    object SDGerenciaSENHAEMAILNFE: TStringField
      FieldName = 'SENHAEMAILNFE'
      Size = 50
    end
    object SDGerenciaHISTORICOVISTA: TFMTBCDField
      FieldName = 'HISTORICOVISTA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOPRAZO: TFMTBCDField
      FieldName = 'HISTORICOPRAZO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOBRADORVISTA: TFMTBCDField
      FieldName = 'COBRADORVISTA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOBRADORPRAZO: TFMTBCDField
      FieldName = 'COBRADORPRAZO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOCAIXADUPLICATA: TFMTBCDField
      FieldName = 'HISTORICOCAIXADUPLICATA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOCAIXACARTEIRA: TFMTBCDField
      FieldName = 'HISTORICOCAIXACARTEIRA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOJUROCAIXA: TFMTBCDField
      FieldName = 'HISTORICOJUROCAIXA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICODESPESACARTORIO: TFMTBCDField
      FieldName = 'HISTORICODESPESACARTORIO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaVENDEDORLOJA: TFMTBCDField
      FieldName = 'VENDEDORLOJA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOCHEQUEPRE: TFMTBCDField
      FieldName = 'HISTORICOCHEQUEPRE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOBRADORCHEQUEPRE: TFMTBCDField
      FieldName = 'COBRADORCHEQUEPRE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaIMPRESSORARESERVA: TStringField
      FieldName = 'IMPRESSORARESERVA'
      Size = 50
    end
    object SDGerenciaDDD: TFMTBCDField
      FieldName = 'DDD'
      Precision = 18
      Size = 0
    end
    object SDGerenciaVALIDADEORCAMENTO: TStringField
      FieldName = 'VALIDADEORCAMENTO'
      Size = 50
    end
    object SDGerenciaLIMITEDEVOLUCAODIAS: TFMTBCDField
      FieldName = 'LIMITEDEVOLUCAODIAS'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICODEVOLUCAO: TFMTBCDField
      FieldName = 'HISTORICODEVOLUCAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOBRADORDEVOLUCAO: TFMTBCDField
      FieldName = 'COBRADORDEVOLUCAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaPOSICAOLOJA: TDateField
      FieldName = 'POSICAOLOJA'
    end
    object SDGerenciaCOBRADORTROCO: TFMTBCDField
      FieldName = 'COBRADORTROCO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOTROCOCREDITO: TFMTBCDField
      FieldName = 'HISTORICOTROCOCREDITO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOTROCODEBITO: TFMTBCDField
      FieldName = 'HISTORICOTROCODEBITO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaDIASLIMITEORCAMENTO: TFMTBCDField
      FieldName = 'DIASLIMITEORCAMENTO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaMULTILOJA: TStringField
      FieldName = 'MULTILOJA'
      Size = 1
    end
    object SDGerenciaCOBRADORCOMPRAS: TFMTBCDField
      FieldName = 'COBRADORCOMPRAS'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOJUROFORNECEDOR: TFMTBCDField
      FieldName = 'HISTORICOJUROFORNECEDOR'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICODESCONTOFORNECEDOR: TFMTBCDField
      FieldName = 'HISTORICODESCONTOFORNECEDOR'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHORAINICIAL: TTimeField
      FieldName = 'HORAINICIAL'
    end
    object SDGerenciaHORAENTRADAMANHA: TTimeField
      FieldName = 'HORAENTRADAMANHA'
    end
    object SDGerenciaHORASAIDAMANHA: TTimeField
      FieldName = 'HORASAIDAMANHA'
    end
    object SDGerenciaHORAENTRADATARDE: TTimeField
      FieldName = 'HORAENTRADATARDE'
    end
    object SDGerenciaHORASAIDATARDE: TTimeField
      FieldName = 'HORASAIDATARDE'
    end
    object SDGerenciaALTERACAOPRODUTO: TMemoField
      FieldName = 'ALTERACAOPRODUTO'
      BlobType = ftMemo
      Size = 1
    end
    object SDGerenciaTEMPOSINCRONIA: TFMTBCDField
      FieldName = 'TEMPOSINCRONIA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaPASTABACKUPWINDOWS: TStringField
      FieldName = 'PASTABACKUPWINDOWS'
      Size = 60
    end
    object SDGerenciaIMPRESSORANFCE: TStringField
      FieldName = 'IMPRESSORANFCE'
      Size = 50
    end
    object SDGerenciaBLOQUEIOINATIVO: TFMTBCDField
      FieldName = 'BLOQUEIOINATIVO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCONSUMIDOR: TFMTBCDField
      FieldName = 'CONSUMIDOR'
      Precision = 18
      Size = 0
    end
    object SDGerenciaULTIMASEQUENCIA: TFMTBCDField
      FieldName = 'ULTIMASEQUENCIA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaIP: TStringField
      FieldName = 'IP'
      Size = 100
    end
    object SDGerenciaPASTABACKUPFTP: TStringField
      FieldName = 'PASTABACKUPFTP'
      Size = 80
    end
    object SDGerenciaINDICADORSITESPECIAL: TStringField
      FieldName = 'INDICADORSITESPECIAL'
      Size = 1
    end
    object SDGerenciaINDICADORPESSOAJURIDICA: TFMTBCDField
      FieldName = 'INDICADORPESSOAJURIDICA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaINDICADORTIPOATIVIDADE: TFMTBCDField
      FieldName = 'INDICADORTIPOATIVIDADE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaINDICADORINCIDENCIATRIBUTARIA: TFMTBCDField
      FieldName = 'INDICADORINCIDENCIATRIBUTARIA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaINDICADORMETODOAPROPRIACAO: TFMTBCDField
      FieldName = 'INDICADORMETODOAPROPRIACAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaINDICADORTIPOCONTRIBUICAO: TFMTBCDField
      FieldName = 'INDICADORTIPOCONTRIBUICAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaMODELOECF: TStringField
      FieldName = 'MODELOECF'
    end
    object SDGerenciaSERIEECF: TStringField
      FieldName = 'SERIEECF'
    end
    object SDGerenciaINDICADORATIVIDADE: TFMTBCDField
      FieldName = 'INDICADORATIVIDADE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCRCCONTADOR: TStringField
      FieldName = 'CRCCONTADOR'
    end
    object SDGerenciaPERFIL: TStringField
      FieldName = 'PERFIL'
      Size = 1
    end
    object SDGerenciaCONTADOR: TStringField
      FieldName = 'CONTADOR'
      Size = 50
    end
    object SDGerenciaCPFCONTADOR: TStringField
      FieldName = 'CPFCONTADOR'
    end
    object SDGerenciaCNPJESCRITORIOCONT: TStringField
      FieldName = 'CNPJESCRITORIOCONT'
    end
    object SDGerenciaCEPESCRITORIOCONT: TStringField
      FieldName = 'CEPESCRITORIOCONT'
    end
    object SDGerenciaENDERECOESCRITORIOCONT: TStringField
      FieldName = 'ENDERECOESCRITORIOCONT'
      Size = 50
    end
    object SDGerenciaNUMEROENDESCRITORIOCONT: TFMTBCDField
      FieldName = 'NUMEROENDESCRITORIOCONT'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOMPLENDESCRITORIOCONT: TStringField
      FieldName = 'COMPLENDESCRITORIOCONT'
      Size = 50
    end
    object SDGerenciaBAIRROESCRITORIOCONT: TStringField
      FieldName = 'BAIRROESCRITORIOCONT'
      Size = 50
    end
    object SDGerenciaFONEESCRITORIOCONT: TStringField
      FieldName = 'FONEESCRITORIOCONT'
    end
    object SDGerenciaFAXESCRITORIOCONT: TStringField
      FieldName = 'FAXESCRITORIOCONT'
    end
    object SDGerenciaEMAILESCRITORIOCONT: TStringField
      FieldName = 'EMAILESCRITORIOCONT'
      Size = 50
    end
    object SDGerenciaCIDADEESCRITORIOCONT: TFMTBCDField
      FieldName = 'CIDADEESCRITORIOCONT'
      Precision = 18
      Size = 0
    end
    object SDGerenciaINDUSTRIA: TStringField
      FieldName = 'INDUSTRIA'
      Size = 1
    end
    object SDGerenciaEMAILFTPLOJA: TStringField
      FieldName = 'EMAILFTPLOJA'
      Size = 100
    end
    object SDGerenciaIDLOJASITEF: TStringField
      FieldName = 'IDLOJASITEF'
      Size = 8
    end
    object SDGerenciaIPSITEF: TStringField
      FieldName = 'IPSITEF'
    end
    object SDGerenciaECF: TStringField
      FieldName = 'ECF'
      Size = 50
    end
    object SDGerenciaEMAILLOJA: TStringField
      FieldName = 'EMAILLOJA'
      Size = 100
    end
    object SDGerenciaPORTASMTP: TFMTBCDField
      FieldName = 'PORTASMTP'
      Precision = 18
      Size = 0
    end
    object SDGerenciaMENSAGEMDEVOLUCAO: TStringField
      FieldName = 'MENSAGEMDEVOLUCAO'
      Size = 100
    end
    object SDGerenciaULTIMAATUALIZACAOSELFCOLOR: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAOSELFCOLOR'
    end
    object SDGerenciaLAYOUTSPEDFISCAL: TFMTBCDField
      FieldName = 'LAYOUTSPEDFISCAL'
      Precision = 18
      Size = 0
    end
    object SDGerenciaLAYOUTSPEDPISCOFINS: TFMTBCDField
      FieldName = 'LAYOUTSPEDPISCOFINS'
      Precision = 18
      Size = 0
    end
    object SDGerenciaDATABASENAMECEP: TStringField
      FieldName = 'DATABASENAMECEP'
      Size = 100
    end
    object SDGerenciaASSUNTOEMAIL: TStringField
      FieldName = 'ASSUNTOEMAIL'
      Size = 100
    end
    object SDGerenciaCORPOEMAIL: TStringField
      FieldName = 'CORPOEMAIL'
      Size = 10000
    end
    object SDGerenciaTIPOBOLETO: TFMTBCDField
      FieldName = 'TIPOBOLETO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaPASTALAZZUMIX: TStringField
      FieldName = 'PASTALAZZUMIX'
      Size = 100
    end
    object SDGerenciaMENSAGEMETIQUETA: TStringField
      FieldName = 'MENSAGEMETIQUETA'
      Size = 50
    end
    object SDGerenciaPISCOFINSDEBITO: TFMTBCDField
      FieldName = 'PISCOFINSDEBITO'
      Precision = 18
      Size = 2
    end
    object SDGerenciaOPERACIONAL: TFMTBCDField
      FieldName = 'OPERACIONAL'
      Precision = 18
      Size = 2
    end
    object SDGerenciaCOMISSAO: TFMTBCDField
      FieldName = 'COMISSAO'
      Precision = 18
      Size = 2
    end
    object SDGerenciaCUSTOENTREGA: TFMTBCDField
      FieldName = 'CUSTOENTREGA'
      Precision = 18
      Size = 2
    end
    object SDGerenciaPROLABORE: TFMTBCDField
      FieldName = 'PROLABORE'
      Precision = 18
      Size = 2
    end
    object SDGerenciaPISCOFINS: TFMTBCDField
      FieldName = 'PISCOFINS'
      Precision = 18
      Size = 2
    end
    object SDGerenciaIRCSLL: TFMTBCDField
      FieldName = 'IRCSLL'
      Precision = 18
      Size = 2
    end
    object SDGerenciaPORTAIMPRESSORANFCE: TStringField
      FieldName = 'PORTAIMPRESSORANFCE'
      Size = 100
    end
    object SDGerenciaLOGONFCE: TMemoField
      FieldName = 'LOGONFCE'
      BlobType = ftMemo
      Size = 1
    end
    object SDGerenciaSITECONSULTANFCE: TStringField
      FieldName = 'SITECONSULTANFCE'
      Size = 100
    end
    object SDGerenciaTEMPOIMPRESSAOBEMATECH: TFMTBCDField
      FieldName = 'TEMPOIMPRESSAOBEMATECH'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICODESCONTOCLIENTE: TFMTBCDField
      FieldName = 'HISTORICODESCONTOCLIENTE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCAMINHOIMPRESSORADIVERSOS: TStringField
      FieldName = 'CAMINHOIMPRESSORADIVERSOS'
      Size = 50
    end
    object SDGerenciaHISTORICOCOMPRA: TFMTBCDField
      FieldName = 'HISTORICOCOMPRA'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCLIENTECARTAO: TFMTBCDField
      FieldName = 'CLIENTECARTAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOCARTAO: TFMTBCDField
      FieldName = 'HISTORICOCARTAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOSAIDATROCACHEQUE: TFMTBCDField
      FieldName = 'HISTORICOSAIDATROCACHEQUE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOENTRADATROCACHEQUE: TFMTBCDField
      FieldName = 'HISTORICOENTRADATROCACHEQUE'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICOSERVICO: TFMTBCDField
      FieldName = 'HISTORICOSERVICO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaHISTORICORETENCAO: TFMTBCDField
      FieldName = 'HISTORICORETENCAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaCOBRADORSERVICO: TFMTBCDField
      FieldName = 'COBRADORSERVICO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaNOMEIMPRESSORAETIQUETA: TStringField
      FieldName = 'NOMEIMPRESSORAETIQUETA'
      Size = 100
    end
    object SDGerenciaIMPRESSORAENTREGAREQUISICAO: TStringField
      FieldName = 'IMPRESSORAENTREGAREQUISICAO'
      Size = 100
    end
    object SDGerenciaEMAILCONTATO: TStringField
      FieldName = 'EMAILCONTATO'
      Size = 100
    end
    object SDGerenciaCAMINHOANEXOSCLIENTE: TStringField
      FieldName = 'CAMINHOANEXOSCLIENTE'
      Size = 1024
    end
    object SDGerenciaCAMINHOANEXOSLOCACAO: TStringField
      FieldName = 'CAMINHOANEXOSLOCACAO'
      Size = 1024
    end
    object SDGerenciaDATABACKUPANEXO: TSQLTimeStampField
      FieldName = 'DATABACKUPANEXO'
    end
    object SDGerenciaPASTABACKUPANEXO: TStringField
      FieldName = 'PASTABACKUPANEXO'
      Size = 100
    end
    object SDGerenciaHORARIOABERTURA: TTimeField
      FieldName = 'HORARIOABERTURA'
    end
    object SDGerenciaTRABALHASABADO: TStringField
      FieldName = 'TRABALHASABADO'
      Size = 1
    end
    object SDGerenciaHORASCORTESIA: TTimeField
      FieldName = 'HORASCORTESIA'
    end
    object SDGerenciaCAMINHOANEXOSOS: TStringField
      FieldName = 'CAMINHOANEXOSOS'
      Size = 1024
    end
    object SDGerenciaCAMINHOANEXOSVISTAEXPLODIDA: TStringField
      FieldName = 'CAMINHOANEXOSVISTAEXPLODIDA'
      Size = 1024
    end
    object SDGerenciaHISTORICOLOCACAO: TFMTBCDField
      FieldName = 'HISTORICOLOCACAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaVENDEDORLOCACAO: TFMTBCDField
      FieldName = 'VENDEDORLOCACAO'
      Precision = 18
      Size = 0
    end
    object SDGerenciaVENDEDOROS: TFMTBCDField
      FieldName = 'VENDEDOROS'
      Precision = 18
      Size = 0
    end
    object SDGerenciaTRABALHADOMINGO: TStringField
      FieldName = 'TRABALHADOMINGO'
      Size = 1
    end
    object SDGerenciaTRABALHAFERIADO: TStringField
      FieldName = 'TRABALHAFERIADO'
      Size = 1
    end
  end
  object SDLocacaoMovimento: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 943
    Top = 436
    object SDLocacaoMovimentoLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoSEQUENCIA: TFMTBCDField
      FieldName = 'SEQUENCIA'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoDATA: TDateField
      FieldName = 'DATA'
    end
    object SDLocacaoMovimentoHORA: TTimeField
      FieldName = 'HORA'
    end
    object SDLocacaoMovimentoUSUARIO: TStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object SDLocacaoMovimentoPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoTIPO: TStringField
      FieldName = 'TIPO'
      Size = 1
    end
    object SDLocacaoMovimentoQUANTIDADE: TFMTBCDField
      FieldName = 'QUANTIDADE'
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object SDLocacaoMovimentoCOBRANCALOCACAO: TStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 50
    end
    object SDLocacaoMovimentoVALORLOCACAO: TFMTBCDField
      FieldName = 'VALORLOCACAO'
      Precision = 18
      Size = 2
    end
    object SDLocacaoMovimentoLIMPO: TStringField
      FieldName = 'LIMPO'
      Size = 1
    end
    object SDLocacaoMovimentoSUJO: TStringField
      FieldName = 'SUJO'
      Size = 1
    end
    object SDLocacaoMovimentoQUEBRADO: TStringField
      FieldName = 'QUEBRADO'
      Size = 1
    end
    object SDLocacaoMovimentoTESTADODEVOLUCAO: TStringField
      FieldName = 'TESTADODEVOLUCAO'
      Size = 1
    end
    object SDLocacaoMovimentoPRODUTOENTRADA: TFMTBCDField
      FieldName = 'PRODUTOENTRADA'
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoMOTIVOTROCA: TFMTBCDField
      FieldName = 'MOTIVOTROCA'
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoOBSERVACAOTROCA: TStringField
      FieldName = 'OBSERVACAOTROCA'
      Size = 10000
    end
    object SDLocacaoMovimentoQUANTIDADELOCADA: TFMTBCDField
      FieldName = 'QUANTIDADELOCADA'
      Precision = 18
      Size = 0
    end
    object SDLocacaoMovimentoQUANTIDADEJADEVOLVIDA: TFMTBCDField
      FieldName = 'QUANTIDADEJADEVOLVIDA'
      Precision = 18
      Size = 0
    end
  end
  object SDLocacaoTrocaMotivo: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'LOCACAOTROCAMOTIVO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 943
    Top = 484
    object SDLocacaoTrocaMotivoTROCAMOTIVO: TFMTBCDField
      FieldName = 'TROCAMOTIVO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLocacaoTrocaMotivoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
  end
  object SDLocacaoHistorico: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'LOCACAOHISTORICO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 947
    Top = 540
    object SDLocacaoHistoricoLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Precision = 18
      Size = 0
    end
    object SDLocacaoHistoricoDATAHORA: TSQLTimeStampField
      FieldName = 'DATAHORA'
    end
    object SDLocacaoHistoricoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 1024
    end
    object SDLocacaoHistoricoUSUARIO: TStringField
      FieldName = 'USUARIO'
    end
  end
  object SDAnexoDocumento: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'ANEXODOCUMENTO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 947
    Top = 596
    object SDAnexoDocumentoTIPO: TStringField
      FieldName = 'TIPO'
      Required = True
      Size = 1
    end
    object SDAnexoDocumentoCHAVE: TFMTBCDField
      FieldName = 'CHAVE'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDAnexoDocumentoSEQUENCIA: TFMTBCDField
      FieldName = 'SEQUENCIA'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDAnexoDocumentoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object SDAnexoDocumentoNOME: TStringField
      FieldName = 'NOME'
      Size = 1024
    end
    object SDAnexoDocumentoTIPOARQUIVO: TStringField
      FieldName = 'TIPOARQUIVO'
      Size = 10
    end
    object SDAnexoDocumentoDATAATUALIZACAO: TSQLTimeStampField
      FieldName = 'DATAATUALIZACAO'
    end
  end
  object SDFields: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 
      'SELECT      RRF.RDB$RELATION_NAME AS TABELA,  '#13#10'            RRF.' +
      'RDB$FIELD_NAME AS CAMPO,  '#13#10'            CASE  '#13#10'              RT' +
      'P.RDB$TYPE_NAME  '#13#10'                WHEN '#39'VARYING'#39'  THEN  '#39'VARCHA' +
      'R'#39'  '#13#10'                WHEN '#39'LONG'#39'     THEN  '#39'INTEGER'#39'  '#13#10'       ' +
      '         WHEN '#39'SHORT'#39'    THEN  '#39'SMALLINT'#39'  '#13#10'                WHE' +
      'N '#39'DOUBLE'#39'   THEN  '#39'DOUBLE'#39'  '#13#10'                WHEN '#39'FLOAT'#39'    T' +
      'HEN  '#39'FLOAT'#39'  '#13#10'                WHEN '#39'INT64'#39'    THEN  '#39'NUMERIC'#39' ' +
      ' '#13#10'                WHEN '#39'TEXT'#39'     THEN  '#39'CHAR'#39'  '#13#10'             ' +
      '   ELSE RTP.RDB$TYPE_NAME  '#13#10'            END TIPO_CAMPO,  '#13#10'    ' +
      '        CASE  '#13#10'              RTP.RDB$TYPE_NAME  '#13#10'             ' +
      '   WHEN  '#39'VARYING'#39' THEN RFL.RDB$FIELD_LENGTH  '#13#10'                ' +
      'ELSE  RFL.RDB$FIELD_PRECISION  '#13#10'            END AS TAMANHO,  '#13#10 +
      '            (RFL.RDB$FIELD_SCALE * -1) AS ESCALA,  '#13#10'           ' +
      ' IIF(  EXISTS(   SELECT      FIRST 1 1  '#13#10'                      ' +
      '      FROM        RDB$RELATION_CONSTRAINTS  RCN  '#13#10'             ' +
      '               INNER JOIN  RDB$INDEX_SEGMENTS        ISG     ON ' +
      '   RCN.RDB$INDEX_NAME = ISG.RDB$INDEX_NAME AND  '#13#10'              ' +
      '                                                                ' +
      '  ISG.RDB$FIELD_NAME = RRF.RDB$FIELD_NAME  '#13#10'                   ' +
      '         WHERE       RCN.RDB$CONSTRAINT_TYPE = '#39'PRIMARY KEY'#39' AND' +
      '  '#13#10'                                        RCN.RDB$RELATION_NAM' +
      'E =  RRF.RDB$RELATION_NAME),  '#13#10'                  '#39'X'#39',  '#13#10'      ' +
      '            '#39'O'#39')  AS  PRIMARY_KEY,  '#13#10'            IIF(  EXISTS( ' +
      '  SELECT      FIRST 1 1  '#13#10'                            FROM     ' +
      '   RDB$RELATION_CONSTRAINTS  RCN  '#13#10'                            ' +
      'INNER JOIN  RDB$CHECK_CONSTRAINTS     CCN     ON    RCN.RDB$CONS' +
      'TRAINT_NAME = CCN.RDB$CONSTRAINT_NAME AND  '#13#10'                   ' +
      '                                                             CCN' +
      '.RDB$TRIGGER_NAME = RRF.RDB$FIELD_NAME  '#13#10'                      ' +
      '       WHERE      RCN.RDB$RELATION_NAME =  RRF.RDB$RELATION_NAME' +
      ' ),  '#13#10'                  '#39'X'#39',  '#13#10'                  '#39'O'#39')  AS  NOT' +
      '_NULL,  '#13#10'            IIF(  RRC.RDB$RELATION_NAME IS NOT NULL,  ' +
      ' '#13#10'                  '#39'X'#39',  '#13#10'                  '#39'O'#39')  AS  FOREIGN' +
      '_KEY,  '#13#10'            RFC.RDB$CONST_NAME_UQ AS  INDICE_CHAVE,  '#13#10 +
      '            RRC.RDB$RELATION_NAME AS  TABELA_CHAVE,  '#13#10'         ' +
      '   RIS2.RDB$FIELD_NAME   AS  CAMPO_CHAVE,  '#13#10'            RFC.RDB' +
      '$UPDATE_RULE   AS  REGRA_UPDATE,  '#13#10'            RFC.RDB$DELETE_R' +
      'ULE   AS  REGRA_DELETE  '#13#10'FROM        RDB$RELATION_FIELDS   RRF ' +
      ' '#13#10'INNER JOIN  RDB$FIELDS            RFL     ON    RFL.RDB$FIELD' +
      '_NAME = RRF.RDB$FIELD_SOURCE  '#13#10'INNER JOIN  RDB$TYPES           ' +
      '  RTP     ON    RTP.RDB$TYPE = RFL.RDB$FIELD_TYPE AND  '#13#10'       ' +
      '                                         RTP.RDB$FIELD_NAME = '#39'R' +
      'DB$FIELD_TYPE'#39'  '#13#10'LEFT JOIN   RDB$INDEX_SEGMENTS    RIS     ON  ' +
      '  RIS.RDB$FIELD_NAME = RRF.RDB$FIELD_NAME AND  '#13#10'               ' +
      '                                 EXISTS (  SELECT      FIRST 1 1' +
      '  '#13#10'                                                          FR' +
      'OM        RDB$INDICES   IND  '#13#10'                                 ' +
      '                         INNER JOIN  RDB$REF_CONSTRAINTS   RFC  ' +
      ' ON    RFC.RDB$CONSTRAINT_NAME = IND.RDB$INDEX_NAME  '#13#10'         ' +
      '                                                 WHERE       IND' +
      '.RDB$INDEX_NAME = RIS.RDB$INDEX_NAME AND  '#13#10'                    ' +
      '                                                  IND.RDB$RELATI' +
      'ON_NAME = RRF.RDB$RELATION_NAME)  '#13#10'LEFT JOIN   RDB$REF_CONSTRAI' +
      'NTS   RFC     ON    RFC.RDB$CONSTRAINT_NAME = RIS.RDB$INDEX_NAME' +
      '  '#13#10'LEFT JOIN   RDB$INDEX_SEGMENTS    RIS2    ON    RIS2.RDB$IND' +
      'EX_NAME = RFC.RDB$CONST_NAME_UQ AND  '#13#10'                         ' +
      '                       RIS2.RDB$FIELD_POSITION = RIS.RDB$FIELD_P' +
      'OSITION  '#13#10'LEFT  JOIN  RDB$RELATION_CONSTRAINTS RRC  ON    RFC.R' +
      'DB$CONST_NAME_UQ = RRC.RDB$CONSTRAINT_NAME AND  '#13#10'              ' +
      '                                  RRC.RDB$CONSTRAINT_TYPE = '#39'PRI' +
      'MARY KEY'#39'  '#13#10'WHERE       RRF.RDB$RELATION_NAME NOT STARTING WITH' +
      ' '#39'RDB$'#39'  '#13#10'ORDER BY    RRF.RDB$RELATION_NAME'
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 852
    Top = 136
    object SDFieldsTABELA: TStringField
      FieldName = 'TABELA'
      FixedChar = True
      Size = 93
    end
    object SDFieldsCAMPO: TStringField
      FieldName = 'CAMPO'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTIPO_CAMPO: TStringField
      FieldName = 'TIPO_CAMPO'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTAMANHO: TSmallintField
      FieldName = 'TAMANHO'
    end
    object SDFieldsESCALA: TLargeintField
      FieldName = 'ESCALA'
    end
    object SDFieldsPRIMARY_KEY: TStringField
      FieldName = 'PRIMARY_KEY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsNOT_NULL: TStringField
      FieldName = 'NOT_NULL'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsFOREIGN_KEY: TStringField
      FieldName = 'FOREIGN_KEY'
      Required = True
      FixedChar = True
      Size = 1
    end
    object SDFieldsINDICE_CHAVE: TStringField
      FieldName = 'INDICE_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsTABELA_CHAVE: TStringField
      FieldName = 'TABELA_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsCAMPO_CHAVE: TStringField
      FieldName = 'CAMPO_CHAVE'
      FixedChar = True
      Size = 93
    end
    object SDFieldsREGRA_UPDATE: TStringField
      FieldName = 'REGRA_UPDATE'
      FixedChar = True
      Size = 11
    end
    object SDFieldsREGRA_DELETE: TStringField
      FieldName = 'REGRA_DELETE'
      FixedChar = True
      Size = 11
    end
  end
  object DsFields: TDataSource
    DataSet = SDFields
    Left = 852
    Top = 184
  end
  object DBAnt: TMySQLDatabase
    DatabaseName = 'zzz_fb_db_ant'
    UserName = 'root'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=zzz_fb_db_ant'
      'UID=root')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 916
    Top = 124
  end
  object QrUpd: TMySQLQuery
    Database = DBAnt
    Left = 916
    Top = 172
  end
  object QrAux: TMySQLQuery
    Database = DBAnt
    Left = 916
    Top = 220
  end
  object QrTabelas: TMySQLQuery
    Database = DBAnt
    SQL.Strings = (
      'SELECT DISTINCT Tabela '
      'FROM _tab_tabs_'
      'ORDER BY Tabela'
      '')
    Left = 916
    Top = 268
    object QrTabelasTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
  end
  object QrCampos: TMySQLQuery
    Database = DBAnt
    SQL.Strings = (
      'SELECT * '
      'FROM _tab_tabs_'
      'WHERE Tabela="ABASTECIB"'
      ''
      '')
    Left = 920
    Top = 364
    object QrCamposTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrCamposCampo: TWideStringField
      FieldName = 'Campo'
      Size = 60
    end
    object QrCamposDataType: TWideStringField
      FieldName = 'DataType'
      Size = 30
    end
    object QrCamposTamanho: TIntegerField
      FieldName = 'Tamanho'
    end
    object QrCamposEscala: TIntegerField
      FieldName = 'Escala'
    end
    object QrCamposPrimaryKey: TWideStringField
      FieldName = 'PrimaryKey'
      Size = 1
    end
    object QrCamposNotNull: TWideStringField
      FieldName = 'NotNull'
      Size = 1
    end
    object QrCamposForeignKey: TWideStringField
      FieldName = 'ForeignKey'
      Size = 1
    end
    object QrCamposIndiceChave: TWideStringField
      FieldName = 'IndiceChave'
      Size = 60
    end
    object QrCamposTabelaChave: TWideStringField
      FieldName = 'TabelaChave'
      Size = 60
    end
    object QrCamposCampoChave: TWideStringField
      FieldName = 'CampoChave'
      Size = 60
    end
    object QrCamposRegraUpdate: TWideStringField
      FieldName = 'RegraUpdate'
      Size = 30
    end
    object QrCamposRegraDelete: TWideStringField
      FieldName = 'RegraDelete'
      Size = 30
    end
  end
  object MySQLBatchExecute2: TMySQLBatchExecute
    Database = DBAnt
    Delimiter = ';'
    Left = 1048
    Top = 228
  end
  object QrPRODUTO: TMySQLQuery
    Database = DBAnt
    Left = 167
    Top = 537
    object QrPRODUTODESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 512
    end
    object QrPRODUTOPRODUTO: TLargeintField
      FieldName = 'PRODUTO'
    end
  end
  object DsTabelas: TDataSource
    DataSet = QrTabelas
    Left = 920
    Top = 316
  end
  object SDFornecedorProduto: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'FORNECEDORPRODUTO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 699
    Top = 324
    object SDFornecedorProdutoFORNECEDOR: TFMTBCDField
      FieldName = 'FORNECEDOR'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDFornecedorProdutoPRODUTO: TFMTBCDField
      FieldName = 'PRODUTO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDFornecedorProdutoCODIGOFORNECEDOR: TStringField
      FieldName = 'CODIGOFORNECEDOR'
      Size = 30
    end
  end
  object SDLoja: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'LOJA'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 735
    Top = 488
    object SDLojaLOJA: TStringField
      FieldName = 'LOJA'
      Required = True
      Size = 3
    end
    object SDLojaFILIAL: TStringField
      FieldName = 'FILIAL'
      Size = 1
    end
    object SDLojaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 50
    end
    object SDLojaIP: TStringField
      FieldName = 'IP'
      Size = 80
    end
    object SDLojaMORADIARIA: TFMTBCDField
      FieldName = 'MORADIARIA'
      Precision = 18
      Size = 2
    end
    object SDLojaTRAVACLIENTE: TFMTBCDField
      FieldName = 'TRAVACLIENTE'
      Precision = 18
      Size = 0
    end
    object SDLojaDESCONTONAVENDA: TFMTBCDField
      FieldName = 'DESCONTONAVENDA'
      Precision = 18
      Size = 2
    end
    object SDLojaELOJA: TStringField
      FieldName = 'ELOJA'
      Size = 1
    end
    object SDLojaULTIMAATUALIZACAOESCRITORIO: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAOESCRITORIO'
    end
    object SDLojaULTIMAATUALIZACAOLOJAS: TSQLTimeStampField
      FieldName = 'ULTIMAATUALIZACAOLOJAS'
    end
    object SDLojaPERIODOINATIVO: TFMTBCDField
      FieldName = 'PERIODOINATIVO'
      Precision = 18
      Size = 0
    end
    object SDLojaSPED: TStringField
      FieldName = 'SPED'
      Size = 1
    end
    object SDLojaACRESCIMOVENDA: TFMTBCDField
      FieldName = 'ACRESCIMOVENDA'
      Precision = 18
      Size = 2
    end
    object SDLojaSOMADESCONTO: TFMTBCDField
      FieldName = 'SOMADESCONTO'
      Precision = 18
      Size = 2
    end
    object SDLojaMULTA: TFMTBCDField
      FieldName = 'MULTA'
      Precision = 18
      Size = 2
    end
    object SDLojaCERTIFICADODIGITAL: TStringField
      FieldName = 'CERTIFICADODIGITAL'
      Size = 1024
    end
    object SDLojaRAZAOSOCIAL: TStringField
      FieldName = 'RAZAOSOCIAL'
      Size = 50
    end
    object SDLojaDDD: TFMTBCDField
      FieldName = 'DDD'
      Precision = 18
      Size = 0
    end
    object SDLojaFONE: TStringField
      FieldName = 'FONE'
      Size = 15
    end
    object SDLojaINSCRICAOESTADUAL: TStringField
      FieldName = 'INSCRICAOESTADUAL'
    end
    object SDLojaCNPJ: TStringField
      FieldName = 'CNPJ'
    end
    object SDLojaCIDADE: TFMTBCDField
      FieldName = 'CIDADE'
      Precision = 18
      Size = 0
    end
    object SDLojaENDERECO: TStringField
      FieldName = 'ENDERECO'
      Size = 50
    end
    object SDLojaNUMERO: TFMTBCDField
      FieldName = 'NUMERO'
      Precision = 18
      Size = 0
    end
    object SDLojaCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Size = 50
    end
    object SDLojaBAIRRO: TStringField
      FieldName = 'BAIRRO'
      Size = 50
    end
    object SDLojaCEP: TStringField
      FieldName = 'CEP'
      Size = 8
    end
    object SDLojaFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 50
    end
    object SDLojaEMPRESATRIBUTADA: TStringField
      FieldName = 'EMPRESATRIBUTADA'
      Size = 1
    end
    object SDLojaCHAVENFE: TStringField
      FieldName = 'CHAVENFE'
      Size = 128
    end
    object SDLojaSERIECONTINGENCIA: TStringField
      FieldName = 'SERIECONTINGENCIA'
      Size = 3
    end
    object SDLojaSERIENFE: TStringField
      FieldName = 'SERIENFE'
      Size = 3
    end
    object SDLojaMODELONFE: TFMTBCDField
      FieldName = 'MODELONFE'
      Precision = 18
      Size = 0
    end
    object SDLojaTIPOEMISSAONFE: TStringField
      FieldName = 'TIPOEMISSAONFE'
      Size = 1
    end
    object SDLojaHOMOLOGACAOPRODUCAO: TStringField
      FieldName = 'HOMOLOGACAOPRODUCAO'
      Size = 1
    end
    object SDLojaIMPRESSAONF: TStringField
      FieldName = 'IMPRESSAONF'
      Size = 1
    end
    object SDLojaREGIMETRIBUTARIO: TFMTBCDField
      FieldName = 'REGIMETRIBUTARIO'
      Precision = 18
      Size = 0
    end
    object SDLojaSIMPLESNACIONAL: TStringField
      FieldName = 'SIMPLESNACIONAL'
      Size = 1
    end
    object SDLojaALIQUOTAAPROVEITAMENTO: TFMTBCDField
      FieldName = 'ALIQUOTAAPROVEITAMENTO'
      Precision = 18
      Size = 2
    end
    object SDLojaMODELONFCE: TFMTBCDField
      FieldName = 'MODELONFCE'
      Precision = 18
      Size = 0
    end
    object SDLojaIMPRESSAONFCE: TStringField
      FieldName = 'IMPRESSAONFCE'
      Size = 1
    end
    object SDLojaTIPOEMISSAONFCE: TStringField
      FieldName = 'TIPOEMISSAONFCE'
      Size = 1
    end
    object SDLojaSERIENFCE: TStringField
      FieldName = 'SERIENFCE'
      Size = 3
    end
    object SDLojaIDTOKEN: TStringField
      FieldName = 'IDTOKEN'
      Size = 10
    end
    object SDLojaCSC: TStringField
      FieldName = 'CSC'
      Size = 50
    end
    object SDLojaULTIMONSUCOMPRAS: TStringField
      FieldName = 'ULTIMONSUCOMPRAS'
      Size = 50
    end
    object SDLojaALIQUOTAIMPOSTOAPROXIMADO: TFMTBCDField
      FieldName = 'ALIQUOTAIMPOSTOAPROXIMADO'
      Precision = 18
      Size = 2
    end
    object SDLojaSERIENFSE: TStringField
      FieldName = 'SERIENFSE'
      Size = 10
    end
    object SDLojaCHAVENFSE: TStringField
      FieldName = 'CHAVENFSE'
      Size = 128
    end
    object SDLojaCNAE: TFMTBCDField
      FieldName = 'CNAE'
      Precision = 18
      Size = 0
    end
    object SDLojaCODIGOTRIBUTACAOMUNICIPIO: TStringField
      FieldName = 'CODIGOTRIBUTACAOMUNICIPIO'
    end
    object SDLojaINSCRICAOMUNICIPAL: TStringField
      FieldName = 'INSCRICAOMUNICIPAL'
      Size = 15
    end
    object SDLojaREGIMEESPECIALTRIBUTACAO: TFMTBCDField
      FieldName = 'REGIMEESPECIALTRIBUTACAO'
      Precision = 18
      Size = 0
    end
    object SDLojaINCENTIVOFISCAL: TFMTBCDField
      FieldName = 'INCENTIVOFISCAL'
      Precision = 18
      Size = 0
    end
    object SDLojaOPTANTESIMPLESNACIONAL: TFMTBCDField
      FieldName = 'OPTANTESIMPLESNACIONAL'
      Precision = 18
      Size = 0
    end
    object SDLojaIMPRESSAONFSE: TStringField
      FieldName = 'IMPRESSAONFSE'
      Size = 1
    end
    object SDLojaVALORMINIMORETENCOES: TFMTBCDField
      FieldName = 'VALORMINIMORETENCOES'
      Precision = 18
      Size = 2
    end
    object SDLojaALIQUOTARETENCAOPIS: TFMTBCDField
      FieldName = 'ALIQUOTARETENCAOPIS'
      Precision = 18
      Size = 2
    end
    object SDLojaALIQUOTARETENCAOCOFINS: TFMTBCDField
      FieldName = 'ALIQUOTARETENCAOCOFINS'
      Precision = 18
      Size = 2
    end
    object SDLojaALIQUOTARETENCAOCSLL: TFMTBCDField
      FieldName = 'ALIQUOTARETENCAOCSLL'
      Precision = 18
      Size = 2
    end
    object SDLojaVALORMINIMORETENCAOIR: TFMTBCDField
      FieldName = 'VALORMINIMORETENCAOIR'
      Precision = 18
      Size = 2
    end
    object SDLojaALIQUOTARETENCAOIR: TFMTBCDField
      FieldName = 'ALIQUOTARETENCAOIR'
      Precision = 18
      Size = 2
    end
    object SDLojaCODIGOSERVICO: TStringField
      FieldName = 'CODIGOSERVICO'
      Size = 10
    end
    object SDLojaALIQUOTAISS: TFMTBCDField
      FieldName = 'ALIQUOTAISS'
      Precision = 18
      Size = 2
    end
    object SDLojaIMPOSTOPIS: TFMTBCDField
      FieldName = 'IMPOSTOPIS'
      Precision = 18
      Size = 2
    end
    object SDLojaIMPOSTOCOFINS: TFMTBCDField
      FieldName = 'IMPOSTOCOFINS'
      Precision = 18
      Size = 2
    end
    object SDLojaIMPOSTOCSLL: TFMTBCDField
      FieldName = 'IMPOSTOCSLL'
      Precision = 18
      Size = 2
    end
    object SDLojaIMPOSTOIRPJ: TFMTBCDField
      FieldName = 'IMPOSTOIRPJ'
      Precision = 18
      Size = 2
    end
    object SDLojaHOMOLOCACAOPRODUCAONFSE: TFMTBCDField
      FieldName = 'HOMOLOCACAOPRODUCAONFSE'
      Precision = 18
      Size = 0
    end
    object SDLojaCLIENTEEXTERNO: TFMTBCDField
      FieldName = 'CLIENTEEXTERNO'
      Precision = 18
      Size = 0
    end
    object SDLojaCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Precision = 18
      Size = 0
    end
    object SDLojaTEMSINCRONIA: TStringField
      FieldName = 'TEMSINCRONIA'
      Size = 1
    end
  end
  object QrCarteiras: TMySQLQuery
    Database = DBAnt
    Left = 243
    Top = 480
    object QrCarteirasNome: TWideStringField
      DisplayWidth = 255
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object SDLancamento: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.CommandText = 'LANCAMENTO'
    DataSet.CommandType = ctTable
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 303
    Top = 452
    object SDLancamentoAUTORIZACAO: TFMTBCDField
      FieldName = 'AUTORIZACAO'
      Required = True
      Precision = 18
      Size = 0
    end
    object SDLancamentoQUITADO: TStringField
      FieldName = 'QUITADO'
      Size = 1
    end
    object SDLancamentoDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      Size = 15
    end
    object SDLancamentoEMISSAO: TDateField
      FieldName = 'EMISSAO'
    end
    object SDLancamentoCLIENTE: TFMTBCDField
      FieldName = 'CLIENTE'
      Precision = 18
      Size = 0
    end
    object SDLancamentoMODO: TStringField
      FieldName = 'MODO'
      Size = 1
    end
    object SDLancamentoVENCIMENTO: TDateField
      FieldName = 'VENCIMENTO'
    end
    object SDLancamentoVALOR: TFMTBCDField
      FieldName = 'VALOR'
      Precision = 18
      Size = 2
    end
    object SDLancamentoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 1024
    end
    object SDLancamentoVENDEDOR: TFMTBCDField
      FieldName = 'VENDEDOR'
      Precision = 18
      Size = 0
    end
    object SDLancamentoORCAMENTO: TFMTBCDField
      FieldName = 'ORCAMENTO'
      Precision = 18
      Size = 0
    end
    object SDLancamentoHISTORICO: TFMTBCDField
      FieldName = 'HISTORICO'
      Precision = 18
      Size = 0
    end
    object SDLancamentoPAGAMENTO: TDateField
      FieldName = 'PAGAMENTO'
    end
    object SDLancamentoVENCIMENTOORIGINAL: TDateField
      FieldName = 'VENCIMENTOORIGINAL'
    end
    object SDLancamentoCOBRADOR: TFMTBCDField
      FieldName = 'COBRADOR'
      Precision = 18
      Size = 0
    end
    object SDLancamentoUSUARIO: TStringField
      FieldName = 'USUARIO'
      Size = 10
    end
    object SDLancamentoLOJA: TStringField
      FieldName = 'LOJA'
      Size = 3
    end
    object SDLancamentoRECIBO: TFMTBCDField
      FieldName = 'RECIBO'
      Precision = 18
      Size = 0
    end
    object SDLancamentoTERCEIRO: TStringField
      FieldName = 'TERCEIRO'
      Size = 1024
    end
    object SDLancamentoANO: TFMTBCDField
      FieldName = 'ANO'
      Precision = 18
      Size = 0
    end
    object SDLancamentoDUPLICATA: TFMTBCDField
      FieldName = 'DUPLICATA'
      Precision = 18
      Size = 0
    end
    object SDLancamentoDATA: TDateField
      FieldName = 'DATA'
    end
    object SDLancamentoHORA: TTimeField
      FieldName = 'HORA'
    end
    object SDLancamentoUSUARIOCANCQUITACAO: TStringField
      FieldName = 'USUARIOCANCQUITACAO'
      Size = 10
    end
    object SDLancamentoTROCA: TStringField
      FieldName = 'TROCA'
      Size = 1
    end
    object SDLancamentoNOTACOMPRA: TFMTBCDField
      FieldName = 'NOTACOMPRA'
      Precision = 18
      Size = 0
    end
    object SDLancamentoMODELOCOMPRA: TFMTBCDField
      FieldName = 'MODELOCOMPRA'
      Precision = 18
      Size = 0
    end
    object SDLancamentoSERIECOMPRA: TStringField
      FieldName = 'SERIECOMPRA'
      Size = 3
    end
    object SDLancamentoNOMECHEQUE: TStringField
      FieldName = 'NOMECHEQUE'
      Size = 50
    end
    object SDLancamentoMOTIVOEXCLUSAO: TStringField
      FieldName = 'MOTIVOEXCLUSAO'
      Size = 50
    end
    object SDLancamentoNOTAFISCALVENDA: TFMTBCDField
      FieldName = 'NOTAFISCALVENDA'
      Precision = 18
      Size = 0
    end
    object SDLancamentoSERIEVENDA: TStringField
      FieldName = 'SERIEVENDA'
      Size = 3
    end
    object SDLancamentoCOBRADORANTERIOR: TFMTBCDField
      FieldName = 'COBRADORANTERIOR'
      Precision = 18
      Size = 0
    end
    object SDLancamentoNOSSONUMERO: TStringField
      FieldName = 'NOSSONUMERO'
      Size = 50
    end
    object SDLancamentoAUTORIZACAOCHEQUECAIXA: TFMTBCDField
      FieldName = 'AUTORIZACAOCHEQUECAIXA'
      Precision = 18
      Size = 0
    end
    object SDLancamentoACORDO: TFMTBCDField
      FieldName = 'ACORDO'
      Precision = 18
      Size = 0
    end
    object SDLancamentoRECEBIMENTOTITULO: TStringField
      FieldName = 'RECEBIMENTOTITULO'
      Size = 1
    end
    object SDLancamentoSTATUSCOBRANCA: TFMTBCDField
      FieldName = 'STATUSCOBRANCA'
      Precision = 18
      Size = 0
    end
    object SDLancamentoOS: TFMTBCDField
      FieldName = 'OS'
      Precision = 18
      Size = 0
    end
    object SDLancamentoLOCACAO: TFMTBCDField
      FieldName = 'LOCACAO'
      Precision = 18
      Size = 0
    end
  end
  object QrFbAux: TMySQLQuery
    Database = DBAnt
    Left = 595
    Top = 276
  end
  object SDTabelaXML: TSimpleDataSet
    Aggregates = <>
    Connection = DBFB
    DataSet.MaxBlobSize = 1
    DataSet.Params = <>
    Params = <>
    Left = 691
    Top = 432
  end
  object SDQry: TSimpleDataSet
    Aggregates = <>
    DataSet.MaxBlobSize = -1
    DataSet.Params = <>
    Params = <>
    Left = 219
    Top = 184
  end
  object DsQry: TDataSource
    DataSet = SDQry
    Left = 219
    Top = 244
  end
  object QrClienteFone: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'SELECT * FROM clientefone')
    Left = 821
    Top = 603
    object QrClienteFoneCLIENTE: TLargeintField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrClienteFoneTELEFONE: TWideStringField
      FieldName = 'TELEFONE'
    end
    object QrClienteFoneCONTATO: TWideStringField
      FieldName = 'CONTATO'
      Size = 50
    end
    object QrClienteFonePRINCIPAL: TWideStringField
      FieldName = 'PRINCIPAL'
      Size = 1
    end
  end
  object QrClienteEmail: TMySQLQuery
    Database = FbDB
    SQL.Strings = (
      'SELECT * FROM clienteemail')
    Left = 821
    Top = 655
    object QrClienteEmailCLIENTE: TLargeintField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrClienteEmailEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 1024
    end
    object QrClienteEmailCONTATO: TWideStringField
      FieldName = 'CONTATO'
      Size = 100
    end
    object QrClienteEmailNFE: TWideStringField
      FieldName = 'NFE'
      Size = 1
    end
    object QrClienteEmailCOBRANCA: TWideStringField
      FieldName = 'COBRANCA'
      Size = 1
    end
  end
  object QrAux1: TMySQLQuery
    Database = DBAnt
    Left = 1036
    Top = 332
  end
end
