unit UnAppEnums;

interface

uses  System.SysUtils, System.Types {$IfDef MSWINDOWS}, Winapi.Windows {$EndIf};

//const

type

  TGraToolRent = (gbsIndef=0, gbsLocar=1, gbsOutrs=2, gbsServs=3, gbsVends=4);
  TTabConLocIts = (tcliIndefinido=0, tcliLocacao=1, tcliOutroServico=2, tcliVenda=3); // IDTab
  //
  TManejoLca = (manelcaIndefinido=0, manelcaPrincipal=1, manelcaSecundario=2,
                       manelcaAcessorio=3, manelcaApoio=4, manelcaUso=5,
                       manelcaConsumo=6, manelcaServico=7, manelcaVenda=8);
  //
  TStatusLocacao = (statlocIndefinido=0, statlocAberto=1, statlocLocado=2,
                    statlocFinalizado=3, statlocCancelado=4, statlocSuspenso=5);
  //
  TStatusMovimento = (statmovIndefinido=0, statmovOrcamento=1, statmovPedido=2,
                      statmovFaturado=3, statmovCancelado=4);
  TTipoMotivLocMov = (tmlmIndefinido=0, tmlmLocarSaida=1, tmlmTrocaEntra=2,
                      tmlmTrocaSaida=3, tmlmLocarVolta=4);
  TServicoAtrelado = (svcatrlIndefinido=0, svcatrlNenhum=1, svcatrlLocacao=2,
                      svcatrlVenda=3);
  TGraGXToolRnt = (ggxoIndef,
                   ggxo1gAcessorio, ggxo1gUso, ggxo1gConsumo,
                   ggxo2gPrincipal, ggxo2gSecundario, ggxo2gAcessorio,
                   ggxo2gApoio, ggxo2gUso, ggxo2gConsumo);

  //
{
  TEstqMovimID = (emidAjuste=0, emidCompra=1, emidVenda=2,
                 emidReclasWE=3, emidBaixa=4, (*emidIndsWE=5*)emidIndstrlzc=5
(*                , emidIndsVS=6,
                 emidClassArtVSUni=7, emidReclasVSUni=8, emidForcado=9,
                 emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
                 emidInventario=13, emidClassArtVSMul=14, emidPreReclasse=15,
                 emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,
                 emidEmProcWE=19, emidFinished=20, emidDevolucao=21,
                 emidRetrabalho=22, emidGeraSubProd=23, emidReclasVSMul=24,
                 emidTransfLoc=25, emidEmProcCal=26, emidEmProcCur=27,
                 emidDesclasse=28, emidCaleado=29, emidEmRibPDA=30,
                 emidEmRibDTA=31, emidEmProcSP=32, emidEmReprRM=33,
                 emidCurtido=34, emidMixInsum=35, emidInnSemCob=36,
                 emidOutSemCob=37*)
                 );

  TEstqMovimNiv = (eminSemNiv=0, eminSorcClass=1, eminDestClass=2(*,
                   eminSorcIndsWB=3, eminDestIndsWB=4, eminSorcReclass=5,
                   eminDestReclass=6,

                   eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10,

                   eminSorcPreReclas=11, eminDestPreReclas=12,

                   eminDestCurtiVS=13, eminSorcCurtiVS=14, eminBaixCurtiVS=15,

                   eminSdoArtInNat=16, eminSdoArtGerado=17,
                   eminSdoArtClassif=18, eminSdoArtEmOper=19,

                   eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22,
                   eminEmWEndBxa=23, eminSdoArtEmWEnd=24,

                   eminSdoFinish=25,
                   eminSdoSubPrd=26,

                   eminSorcLocal=27, eminDestLocal=28,

                   eminSorcCal=29, eminEmCalInn=30, eminDestCal=31,
                   eminEmCalBxa=32, eminSdoArtEmCal=33,

                   eminSorcCur=34, eminEmCurInn=35, eminDestCur=36,
                   eminEmCurBxa=37, eminSdoArtEmCur=38,

                   eminSorcPDA=39, eminEmPDAInn=40, eminDestPDA=41,
                   eminEmPDABxa=42, eminSdoArtEmPDA=43,

                   eminSorcDTA=44, eminEmDTAInn=45, eminDestDTA=46,
                   eminEmDTABxa=47, eminSdoArtEmDTA=48,

                   eminSorcPSP=49, eminEmPSPInn=50, eminDestPSP=51,
                   eminEmPSPBxa=52, eminSdoArtEmPSP=53,

                   eminSorcRRM=54, eminEmRRMInn=55, eminDestRRM=56,
                   eminEmRRMBxa=57, eminSdoArtEmRRM=58,

                   eminSorcMixInsum=59, eminDestMixInsum=60
*)

                   ); }
  TUnAppEnums = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  AppEnums: TUnAppEnums;

const
  (*0*)CO_TXT_tmlmIndefinido    = 'Indefinido!';
  (*1*)CO_TXT_tmlmLocarSaida    = 'Retirado por Loca��o';
  (*2*)CO_TXT_tmlmTrocaEntra    = 'Devolvido e substitu�do';
  (*3*)CO_TXT_tmlmTrocaSaida    = 'Substituiu devolvido';
  (*4*)CO_TXT_tmlmLocarVolta    = 'Devolu��o de Loca��o';
  MaxTipoMotivLocMov = Integer(High(TTipoMotivLocMov));
  sTipoMotivLocMov: array[0..MaxTipoMotivLocMov] of string = (
    CO_TXT_tmlmIndefinido     , // 0
    CO_TXT_tmlmLocarSaida     , // 1
    CO_TXT_tmlmTrocaEntra     , // 2
    CO_TXT_tmlmTrocaSaida     , // 3
    CO_TXT_tmlmLocarVolta       // 4
  );

  (*0*)CO_TXT_manelcaIndefinido   = 'Indefinido!';
  (*1*)CO_TXT_manelcaPrincipal    = 'Patrim�nio Principal';
  (*2*)CO_TXT_manelcaSecundario   = 'Patrim�nio Secund�rio';
  (*3*)CO_TXT_manelcaAcessorio    = 'Acess�rio';
  (*4*)CO_TXT_manelcaApoio        = 'Material de Apoio';
  (*5*)CO_TXT_manelcaUso          = 'Material de Uso';
  (*6*)CO_TXT_manelcaConsumo      = 'Material de Consumo';
  (*7*)CO_TXT_manelcaServico      = 'Servi�o';
  (*8*)CO_TXT_manelcaVenda        = 'Mercadoria (venda)';


  MaxManejoLca = Integer(High(TManejoLca));
  sManejoLca: array[0..MaxManejoLca] of string  = (
    CO_TXT_manelcaIndefinido   ,
    CO_TXT_manelcaPrincipal    ,
    CO_TXT_manelcaSecundario   ,
    CO_TXT_manelcaAcessorio    ,
    CO_TXT_manelcaApoio        ,
    CO_TXT_manelcaUso          ,
    CO_TXT_manelcaConsumo      ,
    CO_TXT_manelcaServico      ,
    CO_TXT_manelcaVenda        //,
  );


(*
  CO_codhistAdiantamentoDeLocacao  =  1;   //0; //1;
  CO_codhistValorParcialDeLocacao  =  2;   //1; //2;
  CO_codhistRenovacaoDeLocacao     =  4;   //2; //4;
  CO_codhistQuitacaoDeLocacao      =  8;   //3; //8;
  CO_codhistFrete                  =  16;  //4; //16;
  CO_codhistVendaDeMercadoria      =  32;  //5; //32;
  CO_codhistServicos               =  64;  //6; //64;
  CO_codhistOutros                 =  128; //7; //128;
*)

  CO_TXT_codhistAdiantamentoDeLocacao  = 'Adiantamento de loca��o';
  CO_TXT_codhistValorParcialDeLocacao  = 'Valor Parcial de loca��o';
  CO_TXT_codhistRenovacaoDeLocacao     = 'Renova��o de loca��o';
  CO_TXT_codhistQuitacaoDeLocacao      = 'Quita��o de loca��o';
  CO_TXT_codhistFrete                  = 'Frete';
  CO_TXT_codhistVendadeMercadorias      = 'Venda de Mercadorias';
  CO_TXT_codhistServicos               = 'Servi�os';
  CO_TXT_codhistOutros                 = 'Outros';

  MaxCodHist = 7;//Integer(High(TManejoLca));
  sCodHistTextos: array[0..MaxCodHist] of string  = (
  CO_TXT_codhistAdiantamentoDeLocacao  ,
  CO_TXT_codhistValorParcialDeLocacao  ,
  CO_TXT_codhistRenovacaoDeLocacao     ,
  CO_TXT_codhistQuitacaoDeLocacao      ,
  CO_TXT_codhistFrete                  ,
  CO_TXT_codhistVendadeMercadorias     ,
  CO_TXT_codhistServicos               ,
  CO_TXT_codhistOutros                 //,
  );

  CO_SIGLA_codhistAdiantamentoDeLocacao  = 'AL';
  CO_SIGLA_codhistValorParcialDeLocacao  = 'PL';
  CO_SIGLA_codhistRenovacaoDeLocacao     = 'RL';
  CO_SIGLA_codhistQuitacaoDeLocacao      = 'QL';
  CO_SIGLA_codhistFrete                  = 'FR';
  CO_SIGLA_codhistVendadeMercadorias     = 'VM';
  CO_SIGLA_codhistServicos               = 'SV';
  CO_SIGLA_codhistOutros                 = 'OU';

  CO_DESCR_codhistAdiantamentoDeLocacao  = 'Adiantamento de loca��o';
  CO_DESCR_codhistValorParcialDeLocacao  = 'Valor parcial de loca��o';
  CO_DESCR_codhistRenovacaoDeLocacao     = 'Renova��o de loca��o';
  CO_DESCR_codhistQuitacaoDeLocacao      = 'Qquita��o de loca��o';
  CO_DESCR_codhistFrete                  = 'Frete';
  CO_DESCR_codhistVendadeMercadorias      = 'Venda de Mercadorias';
  CO_DESCR_codhistServicos               = 'Servi�os';
  CO_DESCR_codhistOutros                 = 'Outros';

  sCodHistSiglas: array[0..MaxCodHist] of string  = (
  CO_SIGLA_codhistAdiantamentoDeLocacao  ,
  CO_SIGLA_codhistValorParcialDeLocacao  ,
  CO_SIGLA_codhistRenovacaoDeLocacao     ,
  CO_SIGLA_codhistQuitacaoDeLocacao      ,
  CO_SIGLA_codhistFrete                  ,
  CO_SIGLA_codhistVendadeMercadorias     ,
  CO_SIGLA_codhistServicos               ,
  CO_SIGLA_codhistOutros                //,
  );

  sCodHistDescris: array[0..MaxCodHist] of string  = (
  CO_DESCR_codhistAdiantamentoDeLocacao  ,
  CO_DESCR_codhistValorParcialDeLocacao  ,
  CO_DESCR_codhistRenovacaoDeLocacao     ,
  CO_DESCR_codhistQuitacaoDeLocacao      ,
  CO_DESCR_codhistFrete                  ,
  CO_DESCR_codhistVendadeMercadorias     ,
  CO_DESCR_codhistServicos               ,
  CO_DESCR_codhistOutros                 //,
  );




implementation

end.
