unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants, UnAppEnums;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    // Compatibilidade
    procedure MostraFormUsoEConsumo();
    procedure MostraFormMateriaPrima();
    procedure MostraFormCorrigeSmia();

  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, CfgCadLista, UnGrade_Jan, CorrigeSmia;

{ TUnApp_Jan }


procedure TUnApp_Jan.MostraFormCorrigeSmia();
begin
  Application.CreateForm(TFmCorrigeSmia, FmCorrigeSmia);
  FmCorrigeSmia.ShowModal;
  FmCorrigeSmia.Destroy;
end;

procedure TUnApp_Jan.MostraFormMateriaPrima;
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TUnApp_Jan.MostraFormUsoEConsumo;
begin

end;

end.
