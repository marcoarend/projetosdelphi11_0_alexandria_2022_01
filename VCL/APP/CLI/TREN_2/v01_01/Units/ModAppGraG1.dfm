object DfModAppGraG1: TDfModAppGraG1
  Left = 0
  Top = 0
  Caption = 'DfModAppGraG1'
  ClientHeight = 437
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 120
  TextHeight = 17
  object QrGraGXComEstq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome, ggx.GraGruY, ptr.*'
      'FROM gragxpatr ptr '
      'LEFT JOIN gragrux ggx ON ggx.Controle=ptr.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE ptr.GraGruX =204'
      'AND ggx.Ativo=1 ')
    Left = 46
    Top = 6
    object QrGraGXComEstqNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 120
    end
    object QrGraGXComEstqGraGruY: TIntegerField
      FieldName = 'GraGruY'
      Required = True
    end
    object QrGraGXComEstqGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrGraGXComEstqComplem: TWideStringField
      FieldName = 'Complem'
      Size = 60
    end
    object QrGraGXComEstqAquisData: TDateField
      FieldName = 'AquisData'
      Required = True
    end
    object QrGraGXComEstqAquisDocu: TWideStringField
      FieldName = 'AquisDocu'
      Size = 60
    end
    object QrGraGXComEstqAquisValr: TFloatField
      FieldName = 'AquisValr'
      Required = True
    end
    object QrGraGXComEstqSituacao: TWordField
      FieldName = 'Situacao'
      Required = True
    end
    object QrGraGXComEstqAtualValr: TFloatField
      FieldName = 'AtualValr'
      Required = True
    end
    object QrGraGXComEstqValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrGraGXComEstqValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrGraGXComEstqValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrGraGXComEstqValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrGraGXComEstqAgrupador: TIntegerField
      FieldName = 'Agrupador'
      Required = True
    end
    object QrGraGXComEstqMarca: TIntegerField
      FieldName = 'Marca'
      Required = True
    end
    object QrGraGXComEstqModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXComEstqSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXComEstqVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXComEstqPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXComEstqCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXComEstqVendaData: TDateField
      FieldName = 'VendaData'
      Required = True
    end
    object QrGraGXComEstqVendaDocu: TWideStringField
      FieldName = 'VendaDocu'
      Size = 60
    end
    object QrGraGXComEstqVendaValr: TFloatField
      FieldName = 'VendaValr'
      Required = True
    end
    object QrGraGXComEstqVendaEnti: TIntegerField
      FieldName = 'VendaEnti'
      Required = True
    end
    object QrGraGXComEstqObserva: TWideStringField
      FieldName = 'Observa'
      Size = 510
    end
    object QrGraGXComEstqAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Size = 25
    end
    object QrGraGXComEstqCLVPAT: TWideStringField
      FieldName = 'CLVPAT'
      Size = 25
    end
    object QrGraGXComEstqMARPAT: TWideStringField
      FieldName = 'MARPAT'
      Size = 60
    end
    object QrGraGXComEstqAplicacao: TIntegerField
      FieldName = 'Aplicacao'
      Required = True
    end
    object QrGraGXComEstqDescrTerc: TWideStringField
      FieldName = 'DescrTerc'
      Size = 60
    end
    object QrGraGXComEstqValorFDS: TFloatField
      FieldName = 'ValorFDS'
      Required = True
    end
    object QrGraGXComEstqDMenos: TSmallintField
      FieldName = 'DMenos'
      Required = True
    end
    object QrGraGXComEstqItemUnid: TIntegerField
      FieldName = 'ItemUnid'
      Required = True
    end
    object QrGraGXComEstqLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraGXComEstqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGXComEstqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGXComEstqUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraGXComEstqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraGXComEstqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraGXComEstqAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraGXComEstqAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraGXComEstqAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrEstq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 48
    Top = 53
    object QrEstqEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,'
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,'
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,'
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,'
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,'
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,'
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid'
      ''
      'FROM gragrux ggx'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle'
      'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle'
      'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo'
      'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao'
      'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle'
      'WHERE  gg1.PrdGrupTip=1 AND (gg1.Nivel1=ggx.GraGru1)'
      'AND cpl.Aplicacao <> 0'
      ''
      'ORDER BY NO_GG1'
      '')
    Left = 48
    Top = 104
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrTXT_MES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MES'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_QUI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QUI'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_SEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SEM'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_DIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DIA'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
    end
    object QrGraGXPatrDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrGraGXPatrPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 60
    end
    object QrGraGXPatrNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
  end
  object QrPesqGPR1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.Controle'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  gg1.Referencia=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 48
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqGPR1Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesqGPR1Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
    object QrPesqGPR1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPesqGPR2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Referencia'
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle'
      'WHERE  cpl.GraGruX=:P0'
      'AND NOT (cpl.GraGruX IS NULL)')
    Left = 49
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqGPR2Referencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 25
    end
    object QrPesqGPR2Patrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 255
    end
  end
end
