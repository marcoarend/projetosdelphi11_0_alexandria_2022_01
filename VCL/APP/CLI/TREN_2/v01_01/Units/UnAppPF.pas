unit UnAppPF;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, SHDocVw,
  Variants, UnInternalConsts2, ComCtrls, dmkGeral, mySQLDBTables, UnDmkEnums,
  UnAppEnums, UnDmkProcFunc, Math, UnProjGroup_Vars,

  Buttons, dmkLabel, Mask, dmkEdit, dmkRadioGroup, dmkEditCB, Principal,
  dmkDBLookupComboBox;
type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
    procedure EquipamentoComEstoque(Sender: TObject);
    procedure EquipamentoSemEstoque(Sender: TObject);
    procedure Servico(Sender: TObject);
    procedure MercadoriaDeVenda(Sender: TObject);
  public
    { Public declarations }
    function AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
               Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    //
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
    //
    procedure CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
    procedure CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
    procedure CadastroServicoNFe(GraGruX: Integer; NewNome: String);
    procedure CadastroSubProduto(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoEConsumo(GraGruX: Integer; NewNome: String);
    procedure CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
    procedure CadastroInsumo(GraGruX: Integer; NewNome: String);
    //
    function  ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
    function  CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
    procedure MostraFormXXXxxXxxGraGruY(GraGruY, GraGruX: Integer; Edita:
              Boolean; Qry: TmySQLQuery(*; MultiplosGGX: array of Integer*));
    function  GGXCadGetForcaCor(GraGruY: Integer): Boolean;
    function  GGXCadGetForcaTam(GraGruY: Integer): Boolean;
    procedure CorrigeReduzidosDuplicadosDeInsumo(CorrigeGraTamIts: Boolean);
    procedure VerificaCadastroVSArtigoIncompleta();
    procedure VerificaGraGruYsNaoConfig();
    procedure VerificaCadastroXxArtigoIncompleta();
    procedure ConfiguraGGXsDeGGY(GraGruY: Integer; iNiveis1: array of Integer);

    // T i s o l i n
    function  AjustaLarguraR(Tam: Integer; Texto: String): String;
    function  AjustaLarguraL(Tam: Integer; Texto: String): String;
    function  AtualizaEstoqueGraGXPatr(GraGruX, Empresa: Integer; AvisaNegativo:
              Boolean): Boolean;
    procedure AtualizaTotaisLocCConCab_Totais(sCodigo: String);
    procedure AtualizaTotaisLocCConCab_Locacao(Codigo: Integer);
    procedure AtualizaTotaisLocCConCab_Servicos(Codigo: Integer);
    procedure AtualizaTotaisLocCConCab_Vendas(Codigo: Integer);
    procedure AtualizaTotaisLocCConCab_Faturados(Codigo: Integer; Encerra:
              Boolean; DtHrBxa: String);

    procedure AtualizaValDevolParci(Codigo, GTRTab, CtrID, Item: Integer);
    function  ObtemTabelaDeGraGruY_Cadastro(const GraGruY: Integer; var
              Tabela: String): Boolean;
    function  ObtemTabelaDeGraGruY_Movimento(const GraGruY: Integer; var
              Tabela: String): Boolean;
    function  ObtemValorProdutoDeGraGruXEGraGruY(
              const GraGruX, GraGruY: Integer; var ValorProduto: Double): Boolean;
    function  ObtemManejoLocaDeGraGruY(GraGruY: Integer): Integer;

    procedure CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS(const GeraLog:
              Boolean; const Saida, Volta, HrTolerancia, OriSubstSaiDH: TDateTime; const DMenos: Integer;
              const ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, QtdeLocacao,
              QtdeProduto, QtdeDevolvido, ValDevolParci: Double; const Troca:
              Byte; DtHrDevolver: TDateTime; var Log:
              String; var IniCobranca: TDateTime; var DiasFDS, DiasUteis:
              Integer; var TempoLocacao_TXT: String; var ValorUni, ValorLocacao:
              Double; var Calculou: Boolean);
    function  AtualizaPendenciasLocacao(QrItsLca: TmySQLQuery; Codigo, StatusLocacao: Integer;
              DtHrEmi, DtFimCobra, DtHrDevolver: TDateTime; GeraLog: Boolean): Boolean;
    function  InserePedidoVda(const Empresa, Cliente: Integer; const _DtaInclu,
              _DtaPrevi: TDateTime; const RegrFiscal, CondicaoPG, CartEmis,
              TabelaPrc, indFinal, indPres, finNFe, idDest, FretePor, EntregaEnti,
              RetiradaEnti: Integer; const DOCENT, PedidoCli, Observa: String;
              var _PediVda, _FatPedCab: Integer): Boolean;


    function  RecalculaItens_Datas(QrItsLca: TmySQLQuery;Codigo, StatusLocacao:
              Integer; QualID: Variant; LocCConCabDtHrEmi, LocCConCabDtHrSai,
              LocCConCabDtHrDevolver, DtFimCobra: TDateTime; GeraLog: Boolean;
              Redefine: Boolean = False): Boolean;
    procedure ReopenItsLca(QrItsLca: TmySQLQuery; Codigo: Integer; SQL_CtrID:
              String);
    procedure MostraPopupDeCadsatroEspecificoDeGraGruXY(Form: TForm; Sb:
              TSpeedButton; PM: TPopupMenu);
    function  TextoDeCodHist_Descricao(CodHist: Integer): String;
    function  TextoDeCodHist_Sigla(CodHist: Integer): String;
    function  InsAltLocCItsVen(const SQLTYpe: TSQLType; const Codigo, CtrID,
              FatID, Empresa, FatPedCab, FatPedVolCnta: Integer; const ManejoLca:
              TManejoLca; const GraGru1, Unidade, GraGruX, StqCencad, NF_Stat: Integer;
              const Quantidade, PrcUni, PercDesco, ValorBruto, ValorTotal,
              prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double; const
              OldStatusMovimento, NewStatusMovimento: TStatusMovimento;
              // ini 2023-12-16
              const AltItem: Integer; var SMIA_IDCtrl: Integer;
              // ini 2023-12-16
              var FAlterou: Boolean; var FNewCtrID, FNewItem: Integer;
              var Tmp_FatPedFis: String): Boolean;
    function  InsereItem2(const SQLType: TSQLType; const FatID, StqCenCad,
              FatPedVolCnta, GraGru1: Integer; const PrecoR, QuantP, DescoP,
              prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double; const
              Tmp_FatPedFis: String; var OriCtrl, IDCtrl: Integer): Boolean;
    function  GeraFatPedCabEPediVdaDeLocCItsVen(const SQLType: TSQLType; const
              DtHrEmi: TDateTime; const Empresa, Cliente, Codigo: Integer;
              var Confirmado: Boolean;
              var PediVda, FatPedCab, FatPedVolCnta: Integer): Boolean;
    function  DevolveTodosEquipamentosLocacao(Codigo, Empresa: Integer;
              QrItsLca: TmySQLQuery; Finaliza: Boolean; BxaDtHr, DtHrDevolver:
              TDateTime; ValorPndt: Double): Boolean;
    function  MudaStatus_Finalizado(Codigo: Integer; QrItsLca: TmySQLQuery;
              DataBaixa: TDateTime; ValorPndt: Double): Boolean;
    procedure VerificaEstoqueVenda(Empresa, FatPedCab, NF_Stat, CabStatus,
              ItsStatus: Integer);
    procedure MostraOrigemFat(FatParcRef: Integer);
    function  ObtemTipoDeTGraGXOutr(Tipo: TGraGXToolRnt): Integer;
////////////////////////////////////////////////////////////////////////////////
///
    function  LocCItsMat_IncluiPri(const SQLType: TSQLType; const Codigo,
              CtrID, GraGruX, Empresa: Integer; const QtdeLocacao, QtdeProduto,
              ValorLocacao, QtdeLocacaoAntes: Double; const DtHrLocado, Categoria:
              String; const OriSubstItem: Integer; const OriSubstSaiDH: String;
              var Item: Integer): Boolean;
    function  LocCItsMat_IncluiSec(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, Empresa: Integer; const QtdeLocacao, QtdeProduto: Double;
              const TipoAluguel, DtHrLOcado: String; const QrLocIPatSec:
              TmySQLQuery; const OriSubstItem: Integer; const OriSubstSaiDH:
              String; var Item: Integer): Boolean;
    function  LocCItsMat_IncluiAce(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, Empresa, ManejoLca: Integer; const QtdeLocacao, QtdeProduto,
              ValorProduto: Double; const DtHrLocado, Categoria: String; const
              QrLocIPatAce: TmySQLQuery; const OriSubstItem: Integer;
              const OriSubstSaiDH: String; var Item: Integer): Boolean;
    function  LocCItsMat_IncluiApo(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, ManejoLca: Integer; const PrcUni, QtdeLocacao, QtdeProduto,
              ValorProduto: Double; const DtHrLocado, Categoria: String;
              const QrLocIPatApo: TmySQLQuery; var Item: Integer): Boolean;
    function  LocCItsMat_IncluiUso(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, ManejoLca, Unidade: Integer; const PrcUni, QtdeLocacao,
              QtdeProduto, ValorProduto: Double; const DtHrLocado, Categoria:
              String; const QrLocIPatUso: TmySQLQuery; var Item: Integer): Boolean;
    function  LocCItsMat_IncluiCns(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, ManejoLca, Unidade: Integer; const PrcUni, QtdeLocacao,
              QtdeProduto, ValorProduto: Double; const DtHrLocado, Categoria:
              String; const QrLocIPatCns: TmySQLQuery; var Item: Integer): Boolean;
    procedure LocCItsRet_ReopenItsLca(QrItsLca: TmySQLQuery; BtPula: TBitBtn;
              Item, Codigo, CtrID: Integer);
    ///

    function  LocCItsRet_InsereLocCMovAll(const SQLType: TSQLType; const
              DataHora, USUARIO, TipoES, COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO,
              TESTADODEVOLUCAO, ObservacaoTroca: String; const Codigo,
              ManejoLca, CtrID, Item, SEQUENCIA, GraGruX, Quantidade, TipoMotiv,
              MotivoTroca, QuantidadeLocada, QuantidadeJaDevolvida: Integer;
              const VALORLOCACAO, GGXEntrada: Double; const SubstitutoLca:
              Integer; var Controle, GTRTab: Integer): Boolean;
    procedure LocCItsRet_ReopnGraGXPatr(QrGraGXPatr: TmySQLQuery; Empresa,
              GraGruX: Integer);
    function  LocCItsRet_DadosDevolucaoOK(EdQuantidade: TdmkEdit; QuantidadeAlt,
              QuantidadeLocada, QuantidadeJaDevolvida: Double): Boolean;
    procedure LocCItsRet_PesquisaPorGraGruX(EdGraGruX: TdmkEditCB;
              CBGraGruX: TdmkDBLookupComboBox; EdPatrimonio, EdReferencia:
              TdmkEdit; Tipo: TGraGXToolRnt; Limpa: Boolean);
    procedure LocCItsRet_PesquisaPorPatrimonio(EdGraGruX: TdmkEditCB;
              CBGraGruX: TdmkDBLookupComboBox; EdPatrimonio, EdReferencia:
              TdmkEdit; Tipo: TGraGXToolRnt; Limpa: Boolean);
    procedure LocCItsRet_PesquisaPorReferencia(EdGraGruX: TdmkEditCB;
              CBGraGruX: TdmkDBLookupComboBox; EdPatrimonio, EdReferencia:
              TdmkEdit; Tipo: TGraGXToolRnt; Limpa: Boolean);
    function  ObtemTabelaGraGXTollRnt(Tipo: TGraGXToolRnt): String;
    procedure ReopenGraGXToll(GraGruY: Integer; Tipo: TGraGXToolRnt;
              QrGraGXToll: TmySQLQuery);

///
////////////////////////////////////////////////////////////////////////////////
    function  CalculaDiasFDS(const CobranIni, Devol, HrTolerancia, DtHrDevolver: TDateTime;
              var DdFDS, DdUteis: Integer; var DdExtra: Integer; var TempoX:
              TDateTime): Boolean;
    procedure CalculaMelhorValorDaLocacaoParaOCliente(
              const DiasFDS, DiasUteis: Integer; const GeraLog: Boolean;
              const ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS: Double;
              const Troca: Integer; QtdeDevolvido, QtdeProduto: Double;
              const Saida, Volta, IniCobranca: TDateTime;
              const DMenos, DdExtra: Integer;
              var TempoLocacao_TXT, sLog: String; var ValorUni: Double);

    procedure LocalizaOrigemDoFatPedCab(FatPedCab: Integer);
    procedure AtualizaValorAluguel_LocCItsLca(Codigo: Integer; TipoAluguel: String);
    function  PesquisaGraGXVend(var GraGruX: Integer): Boolean;
    // compatibilidade
    procedure MostraOrigemFatGenerico(FatID: Integer; FatNum: Double;
              FatParcela, FatParcRef: Integer);
    function  ImpedePorMovimentoAberto(): Boolean;
    function  NFeJaLancada(ChaveNFe: String): Boolean;
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, UnBloqGerl_Jan,
  AppListas, UMySQLDB,
  //UnTX_Jan,
  UMySQLModule, UnGraL_Jan, ModProd, ModuleFatura, UnGrade_PF, LocCConCab,
  ModAppGraG1, GraGXVePsq;


{ TUnAppPF }

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query, Query2: TmySQLQuery): Boolean;
begin
  if (Servico = 'ArreFut') and (Query <> nil) and (Query2 <> nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, Query2);
    //
    Result := True;
  end else
  if (Servico = 'LctGer2') and (Query <> nil) and (Query2 = nil) then
  begin
    UBloqGerl_Jan.MostraFmCondGerArreFut(stIns, CliInt, EntCliInt, nil, Query, nil);
    //
    Result := True;
  end else
    Result := True;
end;

function TUnAppPF.AjustaLarguraR(Tam: Integer; Texto: String): String;
begin
  Result := Texto;
  while Length(Result) < Tam do
    Result := Result + ' ';
end;

function TUnAppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa: Integer; AvisaNegativo:
  Boolean): Boolean;
var
  EstqLoc, EstqSdo: Double;
  sEstqLoc, sGraGruX, sEmpresa: String;
begin
{
  sGraGruX := Geral.FF0(GraGruX);
  Dmod.DqAux.Close;
  //Dmod.DqAux.SQL.Text := Geral.ATS([
  DModG.MyPID_DB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _LOCACAOES_ABERTAS_;  ',
  'CREATE TABLE _LOCACAOES_ABERTAS_  ',
  '      SELECT DISTINCT Codigo  ',
  '      FROM ' + TMeuDB + '.locccon  ',
  '      WHERE DtHrBxa < "1900-01-01"  ',
  '      AND STATUS<3;  ',
  '  ',
  'DROP TABLE IF EXISTS _LOCACOES_ITS_ABERTAS_;  ',
  'CREATE TABLE _LOCACOES_ITS_ABERTAS_  ',
  'SELECT "PRI" TABELA, SUM((QtdeLocacao-QtdeDevolucao)) QtdLocado  ',
  'FROM ' + TMeuDB + '.loccpatpri pri  ',
  'WHERE GraGruX=' + sGraGruX,
  'AND QtdeDevolucao<QtdeLocacao  ',
  'AND Codigo IN (  ',
  '  SELECT Codigo FROM _LOCACAOES_ABERTAS_  ',
  ')  ',
  '  ',
  'UNION  ',
  '  ',
  'SELECT "SEC" TABELA, SUM((QtdeLocacao-QtdeDevolucao))QtdLocado  ',
  'FROM ' + TMeuDB + '.loccpatsec sec  ',
  'WHERE GraGruX=' + sGraGruX,
  'AND QtdeDevolucao<QtdeLocacao  ',
  'AND Codigo IN (  ',
  '  SELECT Codigo FROM _LOCACAOES_ABERTAS_  ',
  ')  ',
  ';  ',
  '']));
  Dmod.DqAux.SQL.Text :=
  'SELECT SUM(QtdLocado) Itens  ' +
  'FROM _LOCACOES_ITS_ABERTAS_  ';
  Dmod.DqAux.Open;
  //
}
  sGraGruX := Geral.FF0(GraGruX);
  sEmpresa := Geral.FF0(Empresa);
  //
  Dmod.QrAux3.SQL.Text :=
  ' SELECT SUM((pri.QtdeLocacao * pri.QtdeProduto)-pri.QtdeDevolucao) QtdLocado' + sLineBreak +
  ' FROM loccitslca pri   ' + sLineBreak +
  ' LEFT JOIN loccconcab cab ON cab.Codigo=pri.Codigo  ' + sLineBreak +
  ' WHERE pri.GraGrux='  +  sGraGruX + sLineBreak +
  ' AND pri.QtdeDevolucao < (pri.QtdeLocacao * pri.QtdeProduto) ' + sLineBreak +
  ' AND cab.Empresa=' + sEmpresa + sLineBreak +
  ' AND cab.DtHrBxa < "1900-01-01"   ' + sLineBreak +
  ' AND cab.Status IN (' +
  Geral.FF0(Integer(TStatusLocacao.statlocAberto))
  + ',' +
  Geral.FF0(Integer(TStatusLocacao.statlocLocado))
  + ') ' + sLineBreak +
  '';
  //Geral.MB_Teste(Dmod.QrAux3.SQL.Text);
  Dmod.QrAux3.Open;
  //
  EstqLoc := Dmod.QrAux3.FieldByName('QtdLocado').AsFloat;
  //EstqSdo := Dmod.QrAux3.FieldByName('EstqSdo').AsFloat;
  sEstqLoc := Geral.FFT_Dot(EstqLoc, 3, siNegativo);
  //
  UnDMkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE gragruepat SET EstqLoc=' + sEstqLoc +
    ', EstqSdo = EstqAnt + EstqEnt - EstqSai - ' + sEstqLoc +
    ' WHERE GraGruX=' + sGraGruX +
    ' AND Empresa=' + sEmpresa + '; ');
  //
  Dmod.QrAux3.SQL.Text :=
  'SELECT EstqSdo FROM gragruepat ' +
  ' WHERE GraGruX=' + sGraGruX +
  ' AND Empresa=' + sEmpresa;
  Dmod.QrAux3.Open;
  //
  Result := Dmod.QrAux3.FieldByName('EstqSdo').AsFloat >= 0;
  //
  if (Result = False) and AvisaNegativo then
  begin
    Geral.MB_Aviso('ATEN��O: O reduzido ' + Geral.FF0(GraGruX) +
    ' ficou com o saldo negativo! ' + sLineBreak +
    'Saldo = ' + Geral.FFT(Dmod.QrAux3.FieldByName('EstqSdo').AsFloat, 3, siNegativo));
  end;
  //
end;

function TUnAppPF.AtualizaPendenciasLocacao(QrItsLca: TmySQLQuery; Codigo, StatusLocacao:
  Integer; DtHrEmi, DtFimCobra, DtHrDevolver: TDateTime; GeraLog: Boolean): Boolean;
const
  sProcName = 'TUnAppPF.AtualizaPendenciasLocacao()';
var
  SaiDtHr, IniCobra, FimCobra, OriSubstSaiDH: TDateTime; //: Integer; //
  Item, DMenos: Integer;
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS: Double;
  Log: String;
  IniCobranca: TDateTime;
  DiasFDS, DiasUteis: Integer;
  COBRANCALOCACAO: String;
  ValorLocAntigo, ValorUnitario, ValorLocAtualizado, ValorBruto, ValDevolParci,
  SOMA_ValLocAtz, QtdeLocacao, QtdeProduto, QtdeDevolucao: Double;
  SQLType: TSQLType;
  sCodigo, Logs: String;
  Continua, Calculou: Boolean;
  Troca: Byte;
  HrTolerancia: TDateTime;
begin
  Result  := False;
  case TStatusLocacao(StatusLocacao) of
    TStatusLocacao.statlocIndefinido,
    TStatusLocacao.statlocFinalizado,
    TStatusLocacao.statlocCancelado: Continua := False;
    TStatusLocacao.statlocLocado,
    TStatusLocacao.statlocAberto: Continua := True;
  end;
  if not Continua then
    Exit;
  SQLType := stUpd;
  Logs := '';
  //
  Screen.Cursor     := crHourGlass;
  try
    SOMA_ValLocAtz := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(QrItsLca, Dmod.MyDB, [
    'SELECT *  ',
    'FROM loccitslca ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    if QrItsLca.RecordCount > 0 then
    begin
      // ini 2021-04-06
      //FimCobra := Trunc(DModG.ObtemAgora());
      FimCobra := DtFimCobra;
      if FimCobra < 2 then
        FimCobra := DModG.ObtemAgora();
      // fim 2021-04-06
      QrItsLca.First;
      while not QrItsLca.Eof do
      begin
        //manelcaPrincipal=1,
        //manelcaSecundario=2,
        //manelcaAcessorio=3
        if QrItsLca.FieldByName('ManejoLca').AsInteger in ([1,2,3]) then
        begin
{ ini 2021-06-26
          SaiDtHr        := Trunc(QrItsLca.FieldByName('SaiDtHr').AsDateTime);
          IniCobra       := Trunc(QrItsLca.FieldByName('CobranIniDtHr').AsDateTime);
          IniCobranca    := IniCobra; // Data de cobran�a j� definida!
          OriSubstSaiDH  := Trunc(QrItsLca.FieldByName('OriSubstSaiDH').AsDateTime);
}
          //
          SaiDtHr        := QrItsLca.FieldByName('SaiDtHr').AsDateTime;
          IniCobra       := QrItsLca.FieldByName('CobranIniDtHr').AsDateTime;
          IniCobranca    := IniCobra; // Data de cobran�a j� definida!
          OriSubstSaiDH  := QrItsLca.FieldByName('OriSubstSaiDH').AsDateTime;
// fim 2021-06-26
          //
          DMenos         := QrItsLca.FieldByName('DMenos').AsInteger;
          ValorDia       := QrItsLca.FieldByName('ValorDia').AsExtended;
          ValorSem       := QrItsLca.FieldByName('ValorSem').AsExtended;
          ValorQui       := QrItsLca.FieldByName('ValorQui').AsExtended;
          ValorMes       := QrItsLca.FieldByName('ValorMes').AsExtended;
          ValorFDS       := QrItsLca.FieldByName('ValorFDS').AsExtended;
          QtdeLocacao    := QrItsLca.FieldByName('QtdeLocacao').AsExtended;
          QtdeProduto    := QrItsLca.FieldByName('QtdeProduto').AsExtended;
          QtdeDevolucao  := QrItsLca.FieldByName('QtdeDevolucao').AsExtended;
          ValorLocAntigo := QrItsLca.FieldByName('ValorLocAtualizado').AsExtended;
          ValDevolParci  := QrItsLca.FieldByName('ValDevolParci').AsExtended;
          Troca          := QrItsLca.FieldByName('Troca').AsInteger;
          HrTolerancia   := QrItsLca.FieldByName('HrTolerancia').AsDateTime;
          //
          Item := QrItsLca.FieldByName('Item').AsInteger;
          //
          DiasFDS            := 0;
          DiasUteis          := 0;
          COBRANCALOCACAO    := '';
          ValorLocAtualizado := 0.00;
          //
          if IniCobra = 0 then
          begin
            (*  estar� indefinido no primeiro c�lculo
            Geral.MB_Erro(
            'Data inicial de cobran�a n�o definida! Ser� usada a data de sa�da!'
            + sLineBreak + sProcName);
            //
            *)
            if SaiDtHr < 2 then
            begin
              (* estar� indefinido no primeiro c�lculo
              Geral.MB_Erro(
              'Data inicial de cobran�a e sa�da n�o definidas! Ser� usada a data de emiss�o!'
              + sLineBreak + sProcName);
              //
              *)
              IniCobra := Trunc(DtHrEmi);
            end else
              IniCobra := Trunc(SaiDtHr);
          end;
          //
          CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS(GeraLog,
          IniCobra, FimCobra, HrTolerancia, OriSubstSaiDH, DMenos, ValorDia, ValorSem, ValorQui, ValorMes,
          ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao, ValDevolParci,
          Troca, DtHrDevolver, Log, IniCobranca,
          // Dados de retorno:
          DiasFDS, DiasUteis, COBRANCALOCACAO, ValorUnitario, ValorLocAtualizado,
          Calculou);
          //
          if Calculou then
          begin
            Logs := Logs + Log;
            ValorBruto := ValorLocAtualizado;
            if
            UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
            'DMenos', 'ValorLocAtualizado', 'ValorBruto',
            'COBRANCALOCACAO', 'DiasFDS', 'DiasUteis'], [
            'Item'], [
            DMenos, ValorLocAtualizado, ValorBruto,
            COBRANCALOCACAO, DiasFDS, DiasUteis], [
            Item], True) then
              SOMA_ValLocAtz := SOMA_ValLocAtz + ValorLocAtualizado;
          end else
            SOMA_ValLocAtz := SOMA_ValLocAtz + ValorLocAntigo;
        end;
        //
        QrItsLca.Next;
      end;
    end;
    //
    sCodigo := Geral.FF0(Codigo);
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE loccconcab SET ValorLocAtualizado=' +
    Geral.FFt_Dot(SOMA_ValLocAtz, 2, siNegativo) +
    ', DtUltAtzPend="'+ Geral.FDT(FimCobra, 1) + '" ' +
    ' WHERE Codigo=' + sCodigo + '; ');
    //
    AtualizaTotaisLocCConCab_Locacao(Codigo);
    //
    Result := True;
    //
    if GeraLog then
      Geral.MB_Info(Logs);
  finally
    Screen.Cursor := crDefault;
  end;

end;

procedure TUnAppPF.AtualizaTotaisLocCConCab_Servicos(Codigo: Integer);
var
  sCodigo, sValorBrto, sValorTotal: String;
  ValorBrto, ValorTotal: Double;
//var
  sValorOrca, sValorPedi, sValorFatu, sValorServicos, sCtrID: String;
  ValorOrca, ValorPedi, ValorFatu, ValorServicos: Double;
begin
  sCodigo := Geral.FF0(Codigo);
  // Garantir que vai zerar CtrID's que n�o tiverem itens!
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconits SET ' +
  '  ValorBrto=0.00 ' +
  ', ValorOrca=0.00 ' +
  ', ValorPedi=0.00 ' +
  ', ValorFatu=0.00 ' +
  ' WHERE Codigo=' + sCodigo+
  ' AND IDTab=' + Geral.FF0(Integer(TTabConLocIts.tcliOutroServico)) + '; '); // 2
  //
////////////////////////////////////////////////////////////////////////////////
  //
  ValorServicos := 0;
  Dmod.QrAux3.SQL.Text :=
  ' SELECT svc.CtrID, ' +
  ' SUM(svc.ValorBruto) ValorBrto,' +
  ' SUM(IF(its.Status>0, svc.ValorTotal, 0)) ValorOrca,' +
  ' SUM(IF(its.Status>1, svc.ValorTotal, 0)) ValorPedi, ' +
  ' SUM(IF(its.Status>2, svc.ValorTotal, 0)) ValorFatu' +
  ' FROM loccitssvc svc' +
  ' LEFT JOIN loccconits its ON its.CtrID=svc.CtrID' +
  ' WHERE svc.Codigo=' + sCodigo +
  ' GROUP BY svc.CtrID' +
  '';
  Dmod.QrAux3.Open;
  while not Dmod.QrAux3.Eof do
  begin
    sCtrID := Geral.FF0(Dmod.QrAux3.FieldByName('CtrID').AsInteger);
    //
    ValorBrto := Dmod.QrAux3.FieldByName('ValorBrto').AsFloat;
    sValorBrto := Geral.FFT_Dot(ValorBrto, 2, siNegativo);
    //
    ValorOrca := Dmod.QrAux3.FieldByName('ValorOrca').AsFloat;
    sValorOrca := Geral.FFT_Dot(ValorOrca, 2, siNegativo);
    //
    ValorPedi := Dmod.QrAux3.FieldByName('ValorPedi').AsFloat;
    sValorPedi := Geral.FFT_Dot(ValorPedi, 2, siNegativo);
    //
    ValorFatu := Dmod.QrAux3.FieldByName('ValorFatu').AsFloat;
    sValorFatu := Geral.FFT_Dot(ValorFatu, 2, siNegativo);
    //
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE loccconits SET ' +
    '  ValorBrto=' + sValorBrto +
    ', ValorOrca=' + sValorOrca +
    ', ValorPedi=' + sValorPedi +
    ', ValorFatu=' + sValorFatu +
    ' WHERE CtrID=' + sCtrID + '; ');
    //
    ValorServicos := ValorServicos + ValorPedi;
    //
    Dmod.QrAux3.Next;
  end;
  sValorServicos := Geral.FFT_Dot(ValorServicos, 2, siNegativo);

  //
////////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconcab SET ValorServicos=' + sValorServicos +
  ' WHERE Codigo=' + sCodigo + '; ');
  //
  AtualizaTotaisLocCConCab_Totais(sCodigo);
  //
end;

function TUnAppPF.AjustaLarguraL(Tam: Integer; Texto: String): String;
begin
  Result := Texto;
  while Length(Result) < Tam do
    Result := ' ' + Result;
end;

procedure TUnAppPF.CadastroInsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroMateriaPrima(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroProdutoAcabado(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroServicoNFe(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroSubProduto(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoEConsumo(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

procedure TUnAppPF.CadastroUsoPermanente(GraGruX: Integer; NewNome: String);
begin
  //Compatibilidade
end;

{
procedure TUnAppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS(
  const GeraLog: Boolean; const Saida, Volta, OriSubstSaiDH: TdateTime; const DMenos: Integer;
  const ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto,
  QtdeDevolvido, ValDevolParci: Double; const Troca: Byte; var Log: String;
  var IniCobranca: TDateTime; var DiasFDS, DiasUteis: Integer;
  var TempoLocacao_TXT: String; var ValorUni, ValorLocacao: Double; var
  Calculou: Boolean);

  function CalculaData_CobranIniDtHr(Saida: TDateTime; DMenos: Integer): Integer;
  const
    sProcName = 'CalculaData_CobranIniDtHr()';
  var
    CobranIni: Integer;
    Partes: Integer;
    DiaInutil, DiasUteis, DiasPula: Integer;
    UsarHorarioPreDefinido: Boolean;
    Hora, Parte, Min15: TDateTime;
  begin
    CobranIni := Trunc(Saida);
    // DMenos s�o dias �teis que n�o ser�o cobrados!
    if DMenos > 0 then
    begin
      DiasUteis := 0;
      DiasPula  := 1;
      while DMenos > DiasUteis do
      begin
        CobranIni := CobranIni + DiasPula;
        DiaInutil := UMyMod.DiaInutil(CobranIni);
        case DiaInutil of
            0: DiasUteis := DiasUteis + 1;
          201: (*Domingo*) DiasPula := 1;
          207: (*S�bado *) DiasPula := 2;
          208: (*Feriado*) DiasPula := 1;
        end;
      end;
    end;
    Result := CobranIni;
  end;
  //
  function CalculaDiasFDS(const CobranIni, Devol: TDateTime; var DdFDS, DdUteis: Integer): Boolean;
  type
    TTipoDia = (tipdiaIndef=0, tipdiaInutil=1, tipdiaUtil=2);
  var
    Ini, Fim, I, DiaInutil: Integer;
    UltimoTipoDia: TTipoDia;
  begin
    Ini := Trunc(CobranIni);
    Fim := Trunc(Devol);
    //
    DdFDS   := 0;
    DdUteis := 0;
    //
    if UMyMod.DiaInutil(Ini) = 0 then
      UltimoTipoDia := TTipoDia.tipdiaUtil
    else
      UltimoTipoDia := TTipoDia.tipdiaInutil;
    //
    for I := Ini + 1 to Fim do
    begin
      DiaInutil := UMyMod.DiaInutil(I);
      case DiaInutil of
        0:
        begin
          if UltimoTipoDia = TTipoDia.tipdiaInutil then
            DiasFDS   := DiasFDS + 1
          else
            DiasUteis := DiasUteis + 1;
          //
          UltimoTipoDia := TTipoDia.tipdiaUtil;
        end;
        201,
        207,
        208:
        begin
          UltimoTipoDia := TTipoDia.tipdiaInutil;
        end;
      end;
    end;
  end;
var
 sLog: String;
var
  QDias, Resto: Integer;
  QSems, QQuis, QMess: Double;
  //
  Valores: array of Double;
  sPeriod: array of String;
  I, N: Integer;
  QtdeCobrar: Double;
  FatDesSbst: Double; // Fator de desconto por ter substituido outro equipamento!
  QPDS: Integer; // dias para calcular o FatDesSbst
begin
  Calculou := False;

  // INI se ainda n�o foi calculado a data de in�cio de cobran�a...
  if IniCobranca < 2 then
  begin
    IniCobranca := Trunc(Saida);
    if DMenos > 0 then
      IniCobranca := CalculaData_CobranIniDtHr(IniCobranca, DMenos);
  end;
  // FIm ...se ainda n�o foi calculado a data de in�cio de cobran�a.
  //
  Log := '';
  DiasFDS := 0;
  DiasUteis := 0;
  if ValorFDS >= 0.01 then
    CalculaDiasFDS(IniCobranca, Volta, DiasFDS, DiasUteis);
  //
  QDias := 0;
  QSems := 0;
  QQuis := 0;
  QMess := 0;
  //
  TempoLocacao_TXT := EmptyStr;
  ValorUni         := 0.00;
  ValorLocacao     := 0.00;
  //
  if Troca = 1 then // Trocado
    Exit;
  if QtdeDevolvido >= QtdeProduto then
  begin
    //Exit;
  end;
  if (ValorDia = 0) and (ValorSem = 0) and (ValorQui = 0) and (ValorMes = 0) then
    Exit;
  //
////////////////////////////////////////////////////////////////////////////////
  //
  //sNaoVQ := '';
  QDias := Trunc(Volta) - Trunc(IniCobranca);
  QSems := QDias / 7;
  QQuis := QDias / 15;
  QMess := dmkPF.Dmk_MonthsBetween(Volta, Saida);
  if QDias < 1 then
  begin
////////////////////////////////////////////////////////////////////////////////
    if GeraLog then
      sLog := sLineBreak + sLog + 'Dias menor que 1 (dias = ' +
      Geral.FF0(QDias) + ') ent�o => Dias = 1';
////////////////////////////////////////////////////////////////////////////////
    QDias := 1;
  end;



////////////////////////////////////////////////////////////////////////////////
///
  FatDesSbst := 1;
  //if OriSubstItem <> 0 then
  if OriSubstSaiDH > 2 then
  begin
    QPDS := Trunc(OriSubstSaiDH) - Trunc(IniCobranca);
    if QDias > 0 then
      FatDesSbst := 1 - (QPDS / QDias);
  end;
///
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
///
///
///
///
///
///     M E N S A L
///
///
///
///
///
///
///
  if (QMess >= 1) and (ValorMes > 0)  then
  begin
    TempoLocacao_TXT  := Geral.FFT(QMess, 4, siNegativo) + ' M';
    ValorUni  := QMess * ValorMes;
////////////////////////////////////////////////////////////////////////////////
    if GeraLog then
      sLog := sLog + sLineBreak + (*AjustaLarguraR*)('Meses >= 1: ') +
      (*AjustaLarguraR*)(TempoLocacao_TXT ) + (*AjustaLarguraL*)(' $' +
      FloatToStr(QMess) + ' x ' + Geral.FFT(ValorMes, 2, siNegativo) + ' = ' +
      Geral.FFT(ValorUni, 2, siNegativo)) + sLineBreak;
////////////////////////////////////////////////////////////////////////////////
  end else
  begin
    I := -1;
    case QDias of
////////////////////////////////////////////////////////////////////////////////
      1..7:
      begin
        N := 4;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        Valores[I] := ValorSem;
        sPeriod[I] := '1 S';
        //
        I := I + 1;
        Valores[I] := ValorQui;
        sPeriod[I] := '1 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
      8..15:
      begin
        N := 5;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorSem > 0) then
          Valores[I] := ValorSem + ((QDias -7) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '1 S - ' + Geral.FF0(QDias - 7) + ' D';
        //
        if QDias < 15 then
        begin
          I := I + 1;
          Valores[I] := 2 * ValorSem;
          sPeriod[I] := '2 S';
        end else
        begin
          I := I + 1;
          if (ValorDia > 0) and (ValorSem > 0) then
            Valores[I] := (2 * ValorSem) + ValorDia
          else
            Valores[I] := 0;
          sPeriod[I] := '2 S - 1 D';
        end;

        //
        I := I + 1;
        Valores[I] := ValorQui;
        sPeriod[I] := '1 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
      16..21:
      begin
        N := 7;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorSem > 0) then
          Valores[I] := (2 * ValorSem) + ((QDias - 14) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '2 S - ' + Geral.FF0(QDias - 14) + ' D';
        //
        I := I + 1;
        Valores[I] := (3 * ValorSem);
        sPeriod[I] := '3 S';
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorMes > 0) then
          Valores[I] := ValorQui + ((QDias  - 15) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - ' + Geral.FF0(QDias - 15) + ' D';
        //
        I := I + 1;
        if (ValorSem > 0) and (ValorMes > 0) then
          Valores[I] := ValorQui + ValorSem
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - 1 S';
        //
        I := I + 1;
        Valores[I] := (2 * ValorQui);
        sPeriod[I] := '2 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
      22..29:
      begin
        N := 8;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorSem > 0) then
          Valores[I] := (3 * ValorSem) + ((QDias - 21) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '3 S - ' + Geral.FF0(QDias - 21) + ' D';
        //
        if QDias < 28 then
        begin
          I := I + 1;
          Valores[I] := (4 * ValorSem);
          sPeriod[I] := '4 S';
        end else
        begin
          I := I + 1;
          Valores[I] := (5 * ValorSem);
          sPeriod[I] := '5 S';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorQui > 0) then
          Valores[I] := ValorQui + (QDias  - 15) * ValorDia
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - ' + Geral.FF0(QDias - 15) + ' D';
        //
        if QDias = 22 then
        begin
          I := I + 1;
          if (ValorSem > 0) and (ValorQui > 0) then
            Valores[I] := ValorQui + ValorSem
          else
            Valores[I] := 0;
          sPeriod[I] := '1 Q - 1 S';
        end else
        begin
          I := I + 1;
          if (ValorDia > 0) and (ValorSem > 0) and (ValorQui > 0) then
            Valores[I] := ValorQui + ValorSem + (QDias  - 22) * ValorDia
          else
            Valores[I] := 0;
          sPeriod[I] := '1 Q - 1 S - ' + Geral.FF0(QDias - 22) + ' D';
        end;
        //
        I := I + 1;
        if (ValorSem > 0) and (ValorQui > 0) then
          Valores[I] := ValorQui + (ValorSem * 2)
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - 2 S';
        //
        I := I + 1;
        Valores[I] := (2 * ValorQui);
        sPeriod[I] := '2 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
      30..31:
      begin
        N := 7;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        Valores[I] := (5 * ValorSem);
        sPeriod[I] := '5 S';
        //
        if QDias >= 31 then
        begin
          I := I + 1;
          Valores[I] := (2 * ValorQui) + (QDias  - 30) * ValorDia;
          sPeriod[I] := '2 Q - ' + Geral.FF0(QDias - 30) + ' D';
        end else
        begin
          I := I + 1;
          Valores[I] := (2 * ValorQui);
          sPeriod[I] := '2 Q';
        end;
        //
        I := I + 1;
        Valores[I] := (ValorQui * 2) + ValorSem;
        sPeriod[I] := '2 Q - 1 S';
        //
        I := I + 1;
        Valores[I] := (ValorQui * 2) + (ValorSem * 3);
        sPeriod[I] := '1 Q - 3 S';
        //
        I := I + 1;
        Valores[I] := (3 * ValorQui);
        sPeriod[I] := '3 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
      //32..>>  Coeficiente de caga�o!
      else
      begin
        N := 4;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        I := I + 1;
        Valores[I] := QDias * ValorDia;
        sPeriod[I] := Geral.FF0(QDias) + ' D';
        //
        I := I + 1;
        Valores[I] := ((QDias + 6) div 7) * ValorSem;
        sPeriod[I] := Geral.FF0(((QDias + 6) div 7)) + ' S';
        //
        I := I + 1;
        Valores[I] := ((QDias + 14) div 15) * ValorQui;
        sPeriod[I] := Geral.FF0(((QDias + 14) div 15)) + ' Q';
        //
        I := I + 1;
        Valores[I] := ((QDias + 29) div 30) * ValorMes;
        sPeriod[I] := Geral.FF0(((QDias + 29) div 30)) + ' M';
        //
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
    ValorLocacao  := 0;
    TempoLocacao_TXT  := EmptyStr;
    for I := 0 to N - 1 do
    begin
      if ValorUni = 0 then
      begin
        ValorUni := Valores[I];
        TempoLocacao_TXT  := sPeriod[I];
      end else
      begin
        if (Valores[I] > 0) and (Valores[I] <= ValorUni) then
        begin
          ValorUni := Valores[I];
          TempoLocacao_TXT  := sPeriod[I];
        end;
      end;
    end;
    if GeraLog then
    begin
      for I := 0 to N - 1 do
      begin
        sLog := sLog + '| ' + AjustaLarguraR(34, sPeriod[I]) + ' >> ' +
               AjustaLarguraL(20, Geral.FFT(Valores[I], 2, siNegativo)) +
               ' |' + sLineBreak;
      end;
    end;
  end;
  QtdeCobrar := QtdeProduto - QtdeDevolvido;
  if QtdeCobrar < 1 then
    QtdeCobrar := 0;
  ValorLocacao := ValDevolParci + (QtdeLocacao * QtdeCobrar * ValorUni) * FatDesSbst;
  //
  if GeraLog then
  begin
    sLog := '============================================================' +
    sLineBreak + AjustaLarguraR(59, '|  Data inicial de cobran�a: ' + Geral.FDT(IniCobranca, 11)) + '|' +
    sLineBreak + AjustaLarguraR(59, '|  Data final   de cobran�a: ' + Geral.FDT(Volta, 11)) + '|' +
    sLineBreak + AjustaLarguraR(59, '|  Dias F: ' + Geral.FF0(DiasFDS))  + '|' +
    sLineBreak + AjustaLarguraR(59, '|  Dias U: ' + Geral.FF0(DiasUteis))  + '|' +
    sLineBreak + '============================================================' +
    sLineBreak + sLog;
    //
    //
    sLog := sLog + '============================================================' +
    sLineBreak +sLineBreak +
    AjustaLarguraR(32, '| Fator de substitui��o') + ' >> ' +
    AjustaLarguraL(20, Geral.FFT(FatDesSbst, 10, siNegativo) ) + ' |' + sLineBreak;
    //
    sLog := sLog + '============================================================' +
    sLineBreak +sLineBreak +
    AjustaLarguraR(20, '| Selecionado:') + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Periodo') + ' >> ' +
    AjustaLarguraL(20, TempoLocacao_TXT ) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Valor Per�odo') +' >> ' +
    AjustaLarguraL(20, Geral.FFT(ValorUni, 2, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Qtde Locacao') +' >> ' +
    AjustaLarguraL(20, Geral.FFT(QtdeLocacao, 3, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Qtde Produto') +' >> ' +
    AjustaLarguraL(20, Geral.FFT(QtdeProduto, 3, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Qtde Devolvido') +' >> ' +
    AjustaLarguraL(20, Geral.FFT(QtdeDevolvido, 3, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     $ devol. parc.') +' >> ' +
    AjustaLarguraL(20, Geral.FFT(ValDevolParci, 2, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Valor Total') +' >> ' +
    AjustaLarguraL(20, Geral.FFT(ValorLocacao, 2, siNegativo)) + ' |' + sLineBreak +
    '============================================================' +
    sLineBreak;
  end;
  Log := sLog;
  Calculou := True;
end;
}

procedure TUnAppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS(
  const GeraLog: Boolean; const Saida, Volta, HrTolerancia, OriSubstSaiDH: TDateTime; const DMenos: Integer;
  const ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto,
  QtdeDevolvido, ValDevolParci: Double; const Troca: Byte;
  DtHrDevolver: TDateTime; var Log: String;
  var IniCobranca: TDateTime; var DiasFDS, DiasUteis: Integer;
  var TempoLocacao_TXT: String; var ValorUni, ValorLocacao: Double; var
  Calculou: Boolean);

  function CalculaData_CobranIniDtHr(Saida: TDateTime; DMenos: Integer): TDateTime;
  const
    sProcName = 'CalculaData_CobranIniDtHr()';
  var
    CobranIni: Integer;
    Partes: Integer;
    DiaInutil, DiasUteis, DiasPula: Integer;
    UsarHorarioPreDefinido: Boolean;
    Hora, Parte, Min15: TDateTime;
  begin
    Result := Saida;
    {
    CobranIni := Trunc(Saida);
    // DMenos s�o dias �teis que n�o ser�o cobrados!
    if DMenos > 0 then
    begin
      DiasUteis := 0;
      DiasPula  := 1;
      while DMenos > DiasUteis do
      begin
        CobranIni := CobranIni + DiasPula;
        DiaInutil := UMyMod.DiaInutil(CobranIni);
        case DiaInutil of
            0: DiasUteis := DiasUteis + 1;
          201: (*Domingo*) DiasPula := 1;
          207: (*S�bado *) DiasPula := 2;
          208: (*Feriado*) DiasPula := 1;
        end;
      end;
    end;
    Result := CobranIni;
    }
  end;
  //
var
 sLog: String;
var
{
  QDias(*, Resto*): Integer;
  QSems, QQuis, QMess: Double;
}
  //
{
  Valores: array of Double;
  sPeriod: array of String;
  I, N: Integer;
}
  QtdeCobrar: Double;
{
  FatDesSbst: Double; // Fator de desconto por ter substituido outro equipamento!
}
  QPDS: Integer; // dias para calcular o FatDesSbst
  PDS_TempoLocacao_TXT, PDS_sLog: String;
  PDS_ValorUni: Double;
  PDS_ValorLocacao: Double;
  PDS_DiasFDS, PDS_DiasUteis, DdExtra: Integer;
  TempoX: TDateTime;
  sTmp: String;
begin
  Calculou := False;

  // INI se ainda n�o foi calculado a data de in�cio de cobran�a...
  if IniCobranca < 2 then
  begin
    IniCobranca := Saida;
    if DMenos > 0 then
      IniCobranca := CalculaData_CobranIniDtHr(IniCobranca, DMenos);
  end;
  // FIm ...se ainda n�o foi calculado a data de in�cio de cobran�a.
  //
  Log := '';
  DiasFDS := 0;
  DiasUteis := 0;
  DdExtra := 0;

  // ini 2021-04-27
  //if ValorFDS >= 0.01 then
    CalculaDiasFDS(IniCobranca, Volta, HrTolerancia, DtHrDevolver, DiasFDS, DiasUteis,
    DdExtra, TempoX);
  // nao 2021-04-27
{
  QDias := 0;
  QSems := 0;
  QQuis := 0;
  QMess := 0;
  //
}
  TempoLocacao_TXT := EmptyStr;
  ValorUni         := 0.00;
  ValorLocacao     := 0.00;
  //
  if Troca = 1 then // Trocado
    Exit;
  if QtdeDevolvido >= QtdeProduto then
  begin
    //Exit;
  end;
  if (ValorDia = 0) and (ValorSem = 0) and (ValorQui = 0) and (ValorMes = 0) then
    Exit;
  //
////////////////////////////////////////////////////////////////////////////////
  //
  //sNaoVQ := '';
{
  QDias := Trunc(Volta) - Trunc(IniCobranca);
  QSems := QDias / 7;
  QQuis := QDias / 15;
  QMess := dmkPF.Dmk_MonthsBetween(Volta, Saida);
  if QDias < 1 then
  begin
////////////////////////////////////////////////////////////////////////////////
    if GeraLog then
      sLog := sLineBreak + sLog + 'Dias menor que 1 (dias = ' +
      Geral.FF0(QDias) + ') ent�o => Dias = 1';
////////////////////////////////////////////////////////////////////////////////
    QDias := 1;
  end;
}

////////////////////////////////////////////////////////////////////////////////
///
  //FatDesSbst := 1;
  PDS_TempoLocacao_TXT := EmptyStr;
  PDS_sLog             := EmptyStr;
  PDS_ValorUni         := 0.00;
  //if OriSubstItem <> 0 then
  if OriSubstSaiDH > 2 then
  begin
    QPDS := Trunc(OriSubstSaiDH) - Trunc(IniCobranca);
    if QPDS > 0 then
    begin
      //FatDesSbst := 1 - (QPDS / QDias);

      //if ValorFDS >= 0.01 then
        CalculaDiasFDS(OriSubstSaiDH, Volta, HrTolerancia, DtHrDevolver, PDS_DiasFDS,
        PDS_DiasUteis, DdExtra, TempoX);
      //
      CalculaMelhorValorDaLocacaoParaOCliente(
      PDS_DiasFDS, PDS_DiasUteis, GeraLog,
      ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS,
      Troca, QtdeDevolvido, QtdeProduto,
      Saida, (*Volta*)OriSubstSaiDH, IniCobranca,
      DMenos, DdExtra,
      PDS_TempoLocacao_TXT, PDS_sLog, PDS_ValorUni);
    end;
  end;
///
////////////////////////////////////////////////////////////////////////////////

  CalculaMelhorValorDaLocacaoParaOCliente(
  DiasFDS, DiasUteis, GeraLog,
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS,
  Troca, QtdeDevolvido, QtdeProduto,
  Saida, Volta, IniCobranca,
  DMenos, DdExtra,
  TempoLocacao_TXT, sLog, ValorUni);

  QtdeCobrar := QtdeProduto - QtdeDevolvido;
  if QtdeCobrar < 1 then
    QtdeCobrar := 0;
  //
  PDS_ValorLocacao := QtdeLocacao * QtdeCobrar * PDS_ValorUni;
  ValorLocacao := ValDevolParci + (QtdeLocacao * QtdeCobrar * ValorUni) - PDS_ValorLocacao; // * FatDesSbst;
  //
  if GeraLog then
  begin

    if TempoX >= 1/1440 then
      sTmp := sLineBreak + AjustaLarguraR(61, '|  Tempo extra: ' + Geral.FDT(TempoX, 102))  + '|'
    else
      sTmp := EMptyStr;

    stmp := sTmp +
    sLineBreak + '==============================================================' +
    sLineBreak;

    sLog := '==============================================================' +
    sLineBreak + AjustaLarguraR(61, '|  Data inicial de cobran�a: ' + Geral.FDT(IniCobranca, 11)) + '|' +
    sLineBreak + AjustaLarguraR(61, '|  Data final   de cobran�a: ' + Geral.FDT(Volta, 11)) + '|' +
    sLineBreak + AjustaLarguraR(61, '|  Dias F: ' + Geral.FF0(DiasFDS) + ' (Fim-de-semana e feriado)')  + '| ' +
    sLineBreak + AjustaLarguraR(61, '|  Dias U: ' + Geral.FF0(DiasUteis) + ' (Dias �teis)')  + '|' +
    sLineBreak + AjustaLarguraR(61, '|  Dias T: ' + Geral.FF0(DMenos) + ' (Dias de transporte)')  + '|' +
    sLineBreak + '|============================================================|' +
    sLineBreak + AjustaLarguraR(61, '|  Dia  X: ' + Geral.FF0(DdExtra) + ' (Dia extra pelo hor�rio incluso nos dias �teis)')  + '|'
    + sTmp + sLineBreak + sLog;

    //
    //
    if PDS_ValorLocacao <> 0 then
    begin
      sLog := sLog + '==============================================================' +
      sLineBreak +sLineBreak +
      AjustaLarguraR(32, '| Valor de substitui��o') + ' >> ' +
      AjustaLarguraL(20, Geral.FFT(-PDS_ValorLocacao, 10, siNegativo) ) + ' |' + sLineBreak;
    end;
    //
    sLog := sLog + '==============================================================' +
    sLineBreak +
    AjustaLarguraR(60, '| ') + ' |' + sLineBreak +
    AjustaLarguraR(60, '| Selecionado:') + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Periodo') + ' >> ' +
    AjustaLarguraL(24, TempoLocacao_TXT ) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Valor Per�odo') +' >> ' +
    AjustaLarguraL(24, Geral.FFT(ValorUni, 2, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Qtde Locacao') +' >> ' +
    AjustaLarguraL(24, Geral.FFT(QtdeLocacao, 3, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Qtde Produto') +' >> ' +
    AjustaLarguraL(24, Geral.FFT(QtdeProduto, 3, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Qtde Devolvido') +' >> ' +
    AjustaLarguraL(24, Geral.FFT(QtdeDevolvido, 3, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     $ devol. parc.') +' >> ' +
    AjustaLarguraL(24, Geral.FFT(ValDevolParci, 2, siNegativo)) + ' |' + sLineBreak +
    AjustaLarguraR(32, '|     Valor Total') +' >> ' +
    AjustaLarguraL(24, Geral.FFT(ValorLocacao, 2, siNegativo)) + ' |' + sLineBreak +
    '============================================================' +
    sLineBreak;
  end;
  Log := sLog;
  Calculou := True;
  if GeraLog then
    Geral.MB_Info(Log)

end;

function TUnAppPF.CalculaDiasFDS(const CobranIni, Devol, HrTolerancia, DtHrDevolver:
  TDateTime; var DdFDS, DdUteis: Integer; var DdExtra: Integer; var TempoX:
  TDateTime): Boolean;
type
  TTipoDia = (tipdiaIndef=0, tipdiaInutil=1, tipdiaUtil=2);
var
  Ini, Fim, I, DiaInutil: Integer;
  UltimoTipoDia: TTipoDia;
  HrVoltou, HrToDev: TDateTime;
begin
  Ini := Trunc(CobranIni);
  Fim := Trunc(Devol);
  //
  DdFDS   := 0;
  DdUteis := 0;
  //
  if (Fim - Ini) > 10 then Exit;
  //
  HrVoltou := Devol - Trunc(Devol);
  HrToDev  := DtHrDevolver - Trunc(DtHrDevolver);
  TempoX := HrVoltou - (HrToDev + HrTolerancia);
  // caso devolveu depois das 10:00 ap�s o dia seguinte:
  if (TempoX >= (1/1440)) and (Fim > Ini) then
    // Cobra mais um dia �til!
    DdExtra := 1
  else
    DdExtra := 0;
  DdUteis := DdUteis + DdExtra;
  //
  if UMyMod.DiaInutil(Ini) = 0 then
    UltimoTipoDia := TTipoDia.tipdiaUtil
  else
    UltimoTipoDia := TTipoDia.tipdiaInutil;
  //
  for I := Ini + 1 to Fim do
  begin
    DiaInutil := UMyMod.DiaInutil(I);
    case DiaInutil of
      0:
      begin
        if UltimoTipoDia = TTipoDia.tipdiaInutil then
          DdFDS   := DdFDS + 1
        else
          DdUteis := DdUteis + 1;
        //
        UltimoTipoDia := TTipoDia.tipdiaUtil;
      end;
      201,
      207,
      208:
      begin
        UltimoTipoDia := TTipoDia.tipdiaInutil;
      end;
    end;
{
    if (DdFDS + DdUteis) > 4 then
    begin
      DdFDS   := 0;
      DdUteis := 0;
      //
      Exit;
    end;
}
  end;
end;

procedure TUnAppPF.CalculaMelhorValorDaLocacaoParaOCliente(
  const DiasFDS, DiasUteis: Integer; const GeraLog: Boolean;
  const ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS: Double;
  const Troca: Integer; QtdeDevolvido, QtdeProduto: Double;
  const Saida, Volta, IniCobranca: TDateTime;
  const DMenos, DdExtra: Integer;
  var TempoLocacao_TXT, sLog: String; var ValorUni: Double);
var
  //DD,
  QDias(*, Resto*): Integer;
  QSems, QQuis, QMess: Double;
  DUteisEFDSMenosDMenos: Integer;
  //
  Valores: array of Double;
  sPeriod: array of String;
  I, N: Integer;
  QtdeCobrar: Double;
  FatDesSbst: Double; // Fator de desconto por ter substituido outro equipamento!
  QPDS: Integer; // dias para calcular o FatDesSbst
  DiaInutil, DtAnt, StepD, SumDdInut, VolInt, IniCobInt: Integer;
  HrRet: TDateTime;
  TmpU, TmpF, Tmpm: Integer;
begin

  QDias := 0;
  QSems := 0;
  QQuis := 0;
  QMess := 0;
  //
  TempoLocacao_TXT := EmptyStr;
  ValorUni         := 0.00;
  //ValorLocacao     := 0.00;
  //
  if Troca = 1 then // Trocado
    Exit;
  if QtdeDevolvido >= QtdeProduto then
  begin
    //Exit;
  end;
  if (ValorDia = 0) and (ValorSem = 0) and (ValorQui = 0) and (ValorMes = 0) then
    Exit;
  //
////////////////////////////////////////////////////////////////////////////////
  //
  //sNaoVQ := '';
  VolInt := Trunc(Volta);
  IniCobInt := Trunc(IniCobranca);

  //QDias := Trunc((Volta - IniCobranca) + (1439/1440));
  QDias := Trunc(Volta) - Trunc(IniCobranca) + DdExtra;
//
  QSems := QDias / 7;
  QQuis := QDias / 15;
  QMess := dmkPF.Dmk_MonthsBetween(Volta, Saida);
(*
// ini 2021-06-04
  DD    := DiasFDS + DiasUteis;
  if DD < 1 then
    DD := 1;
// fim 2021-06-04
*)
  if QDias < 1 then
  begin
////////////////////////////////////////////////////////////////////////////////
    if GeraLog then
      sLog := sLineBreak + sLog + 'Dias menor que 1 (dias = ' +
      Geral.FF0(QDias) + ') ent�o => Dias = 1';
////////////////////////////////////////////////////////////////////////////////
    QDias := 1;
  end;



////////////////////////////////////////////////////////////////////////////////
///
///
///
///
///
///     M E N S A L
///
///
///
///
///
///
///
  if (QMess >= 1) and (ValorMes > 0)  then
  begin
    TempoLocacao_TXT  := Geral.FFT(QMess, 4, siNegativo) + ' M';
    ValorUni  := QMess * ValorMes;
////////////////////////////////////////////////////////////////////////////////
    if GeraLog then
      sLog := sLog + sLineBreak + (*AjustaLarguraR*)('Meses >= 1: ') +
      (*AjustaLarguraR*)(TempoLocacao_TXT ) + (*AjustaLarguraL*)(' $' +
      FloatToStr(QMess) + ' x ' + Geral.FFT(ValorMes, 2, siNegativo) + ' = ' +
      Geral.FFT(ValorUni, 2, siNegativo)) + sLineBreak;
////////////////////////////////////////////////////////////////////////////////
  end else
  begin
    I := -1;
    case QDias of
////////////////////////////////////////////////////////////////////////////////
      1..7:
      begin
        N := 4;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        DUteisEFDSMenosDMenos := (DiasUteis  + DiasFDS) - DMenos;
        if DUteisEFDSMenosDMenos < 2 then
        begin
          I := I + 1;
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' +
                        Geral.FF0(DiasFDS) + ' F' +
                        ' - ' + Geral.FF0(DMenos) + ' T = 1 ';
          if (DiasFDS > 0) and (ValorFDS >= 0.01) then
          begin
            Valores[I] := ValorFDS;
            sPeriod[I] := sPeriod[I] + 'F';
          end else
          begin
            Valores[I] := ValorDia;
            sPeriod[I] := sPeriod[I] + 'D';
          end;
          //
        end else
        //if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        //if ((DiasUteis  + DiasFDS) > 0) then
        begin
          TmpU := DiasUteis;
          TmpF := DiasFDS;
          Tmpm := DMenos;
          while Tmpm > 0 do
          begin
            if TmpU > 0 then
              TmpU := TmpU - 1
            else
              TmpF := TmpF - 1;
            //
            Tmpm := Tmpm - 1;
          end;
          I := I + 1;
          if (ValorFDS >= 0.01) then
          begin
            //Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS)
            Valores[I] := (TmpU * ValorDia) + (TmpF * ValorFDS);
          end else
          begin
            Valores[I] := ((TmpU + TmpF) * ValorDia);
          end;
          //
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' +
                        Geral.FF0(DiasFDS) + ' F' +
                        ' - ' + Geral.FF0(DMenos) + ' D = ' +
                        Geral.FF0(TmpU) + ' U + ' +
                        Geral.FF0(TmpF) + ' F';
        end;
        //
        I := I + 1;
        Valores[I] := ValorSem;
        sPeriod[I] := '1 S';
        //
        I := I + 1;
        Valores[I] := ValorQui;
        sPeriod[I] := '1 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
      8..15:
      begin
        N := 5;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorSem > 0) then
          Valores[I] := ValorSem + ((QDias -7) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '1 S - ' + Geral.FF0(QDias - 7) + ' D';
        //
        if QDias < 15 then
        begin
          I := I + 1;
          Valores[I] := 2 * ValorSem;
          sPeriod[I] := '2 S';
        end else
        begin
          I := I + 1;
          if (ValorDia > 0) and (ValorSem > 0) then
            Valores[I] := (2 * ValorSem) + ValorDia
          else
            Valores[I] := 0;
          sPeriod[I] := '2 S - 1 D';
        end;

        //
        I := I + 1;
        Valores[I] := ValorQui;
        sPeriod[I] := '1 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
      16..21:
      begin
        N := 7;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorSem > 0) then
          Valores[I] := (2 * ValorSem) + ((QDias - 14) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '2 S - ' + Geral.FF0(QDias - 14) + ' D';
        //
        I := I + 1;
        Valores[I] := (3 * ValorSem);
        sPeriod[I] := '3 S';
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorMes > 0) then
          Valores[I] := ValorQui + ((QDias  - 15) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - ' + Geral.FF0(QDias - 15) + ' D';
        //
        I := I + 1;
        if (ValorSem > 0) and (ValorMes > 0) then
          Valores[I] := ValorQui + ValorSem
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - 1 S';
        //
        I := I + 1;
        Valores[I] := (2 * ValorQui);
        sPeriod[I] := '2 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
      22..29:
      begin
        N := 8;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorSem > 0) then
          Valores[I] := (3 * ValorSem) + ((QDias - 21) * ValorDia)
        else
          Valores[I] := 0;
        sPeriod[I] := '3 S - ' + Geral.FF0(QDias - 21) + ' D';
        //
        if QDias < 28 then
        begin
          I := I + 1;
          Valores[I] := (4 * ValorSem);
          sPeriod[I] := '4 S';
        end else
        begin
          I := I + 1;
          Valores[I] := (5 * ValorSem);
          sPeriod[I] := '5 S';
        end;
        //
        I := I + 1;
        if (ValorDia > 0) and (ValorQui > 0) then
          Valores[I] := ValorQui + (QDias  - 15) * ValorDia
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - ' + Geral.FF0(QDias - 15) + ' D';
        //
        if QDias = 22 then
        begin
          I := I + 1;
          if (ValorSem > 0) and (ValorQui > 0) then
            Valores[I] := ValorQui + ValorSem
          else
            Valores[I] := 0;
          sPeriod[I] := '1 Q - 1 S';
        end else
        begin
          I := I + 1;
          if (ValorDia > 0) and (ValorSem > 0) and (ValorQui > 0) then
            Valores[I] := ValorQui + ValorSem + (QDias  - 22) * ValorDia
          else
            Valores[I] := 0;
          sPeriod[I] := '1 Q - 1 S - ' + Geral.FF0(QDias - 22) + ' D';
        end;
        //
        I := I + 1;
        if (ValorSem > 0) and (ValorQui > 0) then
          Valores[I] := ValorQui + (ValorSem * 2)
        else
          Valores[I] := 0;
        sPeriod[I] := '1 Q - 2 S';
        //
        I := I + 1;
        Valores[I] := (2 * ValorQui);
        sPeriod[I] := '2 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
      30..31:
      begin
        N := 7;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        if (ValorFDS >= 0.01) and (DiasFDS > 0) then
        begin
          I := I + 1;
          Valores[I] := (DiasUteis * ValorDia) + (DiasFDS * ValorFDS);
          sPeriod[I] := Geral.FF0(DiasUteis) + ' U + ' + Geral.FF0(DiasFDS) + ' F';
        end else
        begin
          I := I + 1;
          Valores[I] := QDias * ValorDia;
          sPeriod[I] := Geral.FF0(QDias) + ' D';
        end;
        //
        I := I + 1;
        Valores[I] := (5 * ValorSem);
        sPeriod[I] := '5 S';
        //
        if QDias >= 31 then
        begin
          I := I + 1;
          Valores[I] := (2 * ValorQui) + (QDias  - 30) * ValorDia;
          sPeriod[I] := '2 Q - ' + Geral.FF0(QDias - 30) + ' D';
        end else
        begin
          I := I + 1;
          Valores[I] := (2 * ValorQui);
          sPeriod[I] := '2 Q';
        end;
        //
        I := I + 1;
        Valores[I] := (ValorQui * 2) + ValorSem;
        sPeriod[I] := '2 Q - 1 S';
        //
        I := I + 1;
        Valores[I] := (ValorQui * 2) + (ValorSem * 3);
        sPeriod[I] := '1 Q - 3 S';
        //
        I := I + 1;
        Valores[I] := (3 * ValorQui);
        sPeriod[I] := '3 Q';
        //
        I := I + 1;
        Valores[I] := ValorMes;
        sPeriod[I] := '1 M';
        //
      end;
      //32..>>  Coeficiente de caga�o!
      else
      begin
        N := 4;
        SetLength(Valores, N);
        SetLength(sPeriod, N);
        //
        I := I + 1;
        Valores[I] := QDias * ValorDia;
        sPeriod[I] := Geral.FF0(QDias) + ' D';
        //
        I := I + 1;
        Valores[I] := ((QDias + 6) div 7) * ValorSem;
        sPeriod[I] := Geral.FF0(((QDias + 6) div 7)) + ' S';
        //
        I := I + 1;
        Valores[I] := ((QDias + 14) div 15) * ValorQui;
        sPeriod[I] := Geral.FF0(((QDias + 14) div 15)) + ' Q';
        //
        I := I + 1;
        Valores[I] := ((QDias + 29) div 30) * ValorMes;
        sPeriod[I] := Geral.FF0(((QDias + 29) div 30)) + ' M';
        //
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
    //ValorLocacao  := 0;
    TempoLocacao_TXT  := EmptyStr;
    for I := 0 to N - 1 do
    begin
      if ValorUni = 0 then
      begin
        ValorUni := Valores[I];
        TempoLocacao_TXT  := sPeriod[I];
      end else
      begin
        if (Valores[I] > 0) and (Valores[I] <= ValorUni) then
        begin
          ValorUni := Valores[I];
          TempoLocacao_TXT  := sPeriod[I];
        end;
      end;
    end;
    if GeraLog then
    begin
      for I := 0 to N - 1 do
      begin
        sLog := sLog + '| ' + AjustaLarguraR(34, sPeriod[I]) + ' >> ' +
               AjustaLarguraL(20, Geral.FFT(Valores[I], 2, siNegativo)) +
               ' |' + sLineBreak;
      end;
    end;
  end;
end;

procedure TUnAppPF.AtualizaTotaisLocCConCab_Faturados(Codigo: Integer; Encerra:
  Boolean; DtHrBxa: String);
var
  ValorPago: Double;
begin
  //FmLocFCab2.BtOKClick...
  UnDmkDAC_PF.AbreMySQLQUery0(Dmod.QrAux3, Dmod.MyDB, [
  'SELECT SUM(lfc.ValBruto) ValBruto ',
  'FROM locfcab lfc ',
  'WHERE lfc.Codigo=' + Geral.FF0(Codigo),
  '']);
  if Dmod.QrAux3.RecordCount > 0 then
    ValorPago := Dmod.QrAux3.FieldByName('ValBruto').AsFloat
  else
    ValorPago := 0;
  //
  if Encerra then
  begin
    //DtHrBxa := DtHrFat;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, ['ValorPago',
      'DtHrBxa'], ['Codigo'], [ValorPago, DtHrBxa], [Codigo], True);
    //
  end  else
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconcab', False, ['ValorPago'
      ], ['Codigo'], [ValorPago], [Codigo], True);
    //
  end;
  //
  AppPF.AtualizaTotaisLocCConCab_Totais(Geral.FF0(Codigo));
end;

procedure TUnAppPF.AtualizaTotaisLocCConCab_Locacao(Codigo: Integer);
var
  sCodigo, sValorProduto, sValorLocAAdiantar, sValorBrto, sValorLocAtualizado,
  sValorUsos, sValorConsumos: String;
//var
  sValorOrca, sValorPedi, sValorFatu, sValorLocacoes, sCtrID: String;
  ValorOrca, ValorPedi, ValorFatu, ValorLocacoes, ValorBrto, ValorLocAtualizado,
  ValorUsos, ValorConsumos, ValorProduto: Double;
  Cursor: TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  //
  sCodigo := Geral.FF0(Codigo);
  // Garantir que vai zerar CtrID's que n�o tiverem itens!
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconits SET ' +
  '  ValorBrto=0.00 ' +
  ', ValorOrca=0.00 ' +
  ', ValorPedi=0.00 ' +
  ', ValorFatu=0.00 ' +
  ' WHERE Codigo=' + sCodigo+
  ' AND IDTab=' + Geral.FF0(Integer(TTabConLocIts.tcliLocacao)) + '; '); // 1
  //
  Dmod.QrAux3.SQL.Text :=
  // ini 2021-03-03
  //' SELECT SUM(IF(QtdeLocacao>0, QtdeProduto*ValorProduto, 0)) ValorProduto ' +
  ' SELECT SUM(IF(QtdeLocacao>0, QtdeProduto*ValorProduto, 0) + ValorRucAAdiantar) ValorProduto ' +
  // fim 2021-03-03
  ' FROM loccitsruc ' +
  ' WHERE Codigo=' + sCodigo +
  '';
  Dmod.QrAux3.Open;
  ValorProduto := Dmod.QrAux3.FieldByName('ValorProduto').AsFloat;
  //
  Dmod.QrAux3.SQL.Text :=
  ' SELECT SUM(IF(QtdeLocacao>0, QtdeProduto*ValorProduto, 0)) ValorProduto, ' +
  ' SUM(ValorLocAAdiantar) ValorLocAAdiantar,' +
  ' SUM(ValorLocacao*QtdeProduto) ValorLocacoes ' +
  ' FROM loccitslca ' +
  ' WHERE Codigo=' + sCodigo +
  '';
  Dmod.QrAux3.Open;
  ValorProduto := ValorProduto + Dmod.QrAux3.FieldByName('ValorProduto').AsFloat;
  //
  sValorProduto        := Geral.FFT_Dot(ValorProduto, 2, siNegativo);
  sValorLocAAdiantar   := Geral.FFT_Dot(Dmod.QrAux3.FieldByName('ValorLocAAdiantar').AsFloat, 2, siNegativo);
  sValorLocacoes       := Geral.FFT_Dot(Dmod.QrAux3.FieldByName('ValorLocacoes').AsFloat, 2, siNegativo);
  //
////////////////////////////////////////////////////////////////////////////////
  //
  ValorLocAtualizado := 0;
  Dmod.QrAux3.SQL.Text :=
  ' SELECT lca.CtrID, ' +
  ' SUM(lca.ValorBruto) ValorBrto,' +
  ' SUM(IF(its.Status>0, lca.ValorLocAtualizado, 0)) ValorOrca,' +
  ' SUM(IF(its.Status>1, lca.ValorLocAtualizado, 0)) ValorPedi, ' +
  ' SUM(IF(its.Status>2, lca.ValorLocAtualizado, 0)) ValorFatu' +
  ' FROM loccitslca lca' +
  ' LEFT JOIN loccconits its ON its.CtrID=lca.CtrID' +
  ' WHERE lca.Codigo=' + sCodigo +
  ' GROUP BY lca.CtrID' +
  '';
  Dmod.QrAux3.Open;
  while not Dmod.QrAux3.Eof do
  begin
    sCtrID := Geral.FF0(Dmod.QrAux3.FieldByName('CtrID').AsInteger);
    //
    ValorBrto := Dmod.QrAux3.FieldByName('ValorBrto').AsFloat;
    sValorBrto := Geral.FFT_Dot(ValorBrto, 2, siNegativo);
    //
    ValorOrca := Dmod.QrAux3.FieldByName('ValorOrca').AsFloat;
    sValorOrca := Geral.FFT_Dot(ValorOrca, 2, siNegativo);
    //
    ValorPedi := Dmod.QrAux3.FieldByName('ValorPedi').AsFloat;
    sValorPedi := Geral.FFT_Dot(ValorPedi, 2, siNegativo);
    //
    ValorFatu := Dmod.QrAux3.FieldByName('ValorFatu').AsFloat;
    sValorFatu := Geral.FFT_Dot(ValorFatu, 2, siNegativo);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE loccconits SET ' +
    '  ValorBrto=' + sValorBrto +
    ', ValorOrca=' + sValorOrca +
    ', ValorPedi=' + sValorPedi +
    ', ValorFatu=' + sValorFatu +
    ' WHERE CtrID=' + sCtrID + '; ');
    //
    ValorLocAtualizado := ValorLocAtualizado + ValorPedi;
    //
    Dmod.QrAux3.Next;
  end;
  sValorLocAtualizado := Geral.FFT_Dot(ValorLocAtualizado, 2, siNegativo);
 //
////////////////////////////////////////////////////////////////////////////////
  //
(* Repetido!
  Dmod.QrAux3.SQL.Text :=
  ' SELECT lca.CtrID, ' +
  ' SUM(IF(its.Status>0, lca.ValorLocAtualizado, 0)) ValorOrca,' +
  ' SUM(IF(its.Status>1, lca.ValorLocAtualizado, 0)) ValorPedi, ' +
  ' SUM(IF(its.Status>2, lca.ValorLocAtualizado, 0)) ValorFatu' +
  ' FROM loccitslca lca' +
  ' LEFT JOIN loccconits its ON its.CtrID=lca.CtrID' +
  ' WHERE lca.Codigo=' + sCodigo +
  ' GROUP BY lca.CtrID' +
  '';
  Dmod.QrAux3.Open;
  while not Dmod.QrAux3.Eof do  /
  begin
    sCtrID := Geral.FF0(Dmod.QrAux3.FieldByName('CtrID').AsInteger);
    //
    ValorOrca := Dmod.QrAux3.FieldByName('ValorOrca').AsFloat;
    sValorOrca := Geral.FFT_Dot(ValorOrca, 2, siNegativo);
    //
    ValorPedi := Dmod.QrAux3.FieldByName('ValorPedi').AsFloat;
    sValorPedi := Geral.FFT_Dot(ValorPedi, 2, siNegativo);
    //
    ValorFatu := Dmod.QrAux3.FieldByName('ValorFatu').AsFloat;
    sValorFatu := Geral.FFT_Dot(ValorFatu, 2, siNegativo);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE loccconits SET ValorOrca=' + sValorOrca +
    ', ValorPedi=' + sValorPedi +
    ', ValorFatu=' + sValorFatu +
    ' WHERE CtrID=' + sCtrID + '; ');
    //
    ValorLocAtualizado := ValorLocAtualizado + ValorPedi;
    //
    Dmod.QrAux3.Next;
  end;
  sValorLocAtualizado := Geral.FFT_Dot(ValorLocAtualizado, 2, siNegativo);
 //
////////////////////////////////////////////////////////////////////////////////
*)
  //
  Dmod.QrAux3.SQL.Text :=
  ' SELECT ruc.CtrID, ' +
  ' SUM(ruc.ValorBruto) ValorBrto,' +
  ' SUM(IF(its.Status>0, ruc.ValUso, 0)) ValorOrca,' +
  ' SUM(IF(its.Status>1, ruc.ValUso, 0)) ValorPedi, ' +
  ' SUM(IF(its.Status>2, ruc.ValUso, 0)) ValorFatu' +
  ' FROM loccitsruc ruc ' +
  ' LEFT JOIN loccconits its ON its.CtrID=ruc.CtrID' +
  ' WHERE ruc.Codigo=' + sCodigo +
  ' GROUP BY ruc.CtrID' +
  '';
  Dmod.QrAux3.Open;
  while not Dmod.QrAux3.Eof do
  begin
    sCtrID := Geral.FF0(Dmod.QrAux3.FieldByName('CtrID').AsInteger);
    //
    ValorBrto := Dmod.QrAux3.FieldByName('ValorBrto').AsFloat;
    sValorBrto := Geral.FFT_Dot(ValorBrto, 2, siNegativo);
    //
    ValorOrca := Dmod.QrAux3.FieldByName('ValorOrca').AsFloat;
    sValorOrca := Geral.FFT_Dot(ValorOrca, 2, siNegativo);
    //
    ValorPedi := Dmod.QrAux3.FieldByName('ValorPedi').AsFloat;
    sValorPedi := Geral.FFT_Dot(ValorPedi, 2, siNegativo);
    //
    ValorFatu := Dmod.QrAux3.FieldByName('ValorFatu').AsFloat;
    sValorFatu := Geral.FFT_Dot(ValorFatu, 2, siNegativo);
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE loccconits SET ' +
    '  ValorBrto=ValorBrto + ' + sValorBrto +
    ', ValorOrca=ValorOrca + ' + sValorOrca +
    ', ValorPedi=ValorPedi + ' + sValorPedi +
    ', ValorFatu=ValorFatu + ' + sValorFatu +
    ' WHERE CtrID=' + sCtrID + '; ');
    //
    //ValorLocAtualizado := ValorLocAtualizado + ValorPedi;
    //
    Dmod.QrAux3.Next;
  end;
  //sValorLocAtualizado := Geral.FFT_Dot(ValorLocAtualizado, 2, siNegativo);
 //
////////////////////////////////////////////////////////////////////////////////
  //
  Dmod.QrAux3.SQL.Text :=
  ' SELECT SUM(IF(ruc.ManejoLca=5, ruc.ValUso + ValorRucAAdiantar, 0)) ValorUsos, ' + // Uso
  ' SUM(IF(ruc.ManejoLca=6, ruc.ValUso + ValorRucAAdiantar, 0)) ValorConsumos ' +         // Consumo
  ' FROM loccitsruc ruc' +
  ' LEFT JOIN loccconits its ON its.CtrID=ruc.CtrID' +
  ' WHERE ruc.Codigo=' + sCodigo +
  ' AND its.Status>0 ' +
  '';
  Dmod.QrAux3.Open;
  ValorUsos := Dmod.QrAux3.FieldByName('ValorUsos').AsFloat;
  sValorUsos := Geral.FFT_Dot(ValorUsos, 2, siNegativo);
  //
  ValorConsumos := Dmod.QrAux3.FieldByName('ValorConsumos').AsFloat;
  sValorConsumos := Geral.FFT_Dot(ValorConsumos, 2, siNegativo);
 //
////////////////////////////////////////////////////////////////////////////////
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE loccconcab SET ' +
    '  ValorProdutos=' + sValorProduto +
    ', ValorEquipamentos=' + sValorProduto +
    ', ValorLocAAdiantar=' + sValorLocAAdiantar +
    ', Valorlocacao=' + sValorLocacoes +
    ', ValorUsos=' + sValorUsos +
    ', ValorConsumos=' + sValorConsumos +
    ', ValorLocAtualizado=' + sValorLocAtualizado +
    ' WHERE Codigo=' + sCodigo);
   //
  AtualizaTotaisLocCConCab_Totais(sCodigo);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TUnAppPF.AtualizaTotaisLocCConCab_Totais(sCodigo: String);
var
  sValorBrto, sValorOrca, sValorPedi, sValorFatu(*, sValorVendas*): String;
  ValorBrto, ValorOrca, ValorPedi, ValorFatu(*, ValorVendas*): Double;
  Status: Integer;
begin
(*
  Dmod.QrAux3.SQL.Text :=
  ' SELECT Status ' +
  ' FROM loccconcab ' +
  ' WHERE Codigo=' + sCodigo +
  '';
  Dmod.QrAux3.Open;
  //
  Status := Dmod.QrAux3.FieldByName('Status').AsInteger;
*)
(*
ValorTot?
ValorLocacao?
ValorAdicional
ValorAdiantamento
ValorDesconto
ValorProdutos
ValorLocAtualizado
ValorServicos
ValorVenda
ValorPago
*)

  Dmod.QrAux3.SQL.Text :=
  ' SELECT ' +
  ' SUM(ValorBrto) ValorBrto, ' +
  ' SUM(ValorOrca) ValorOrca, ' +
  ' SUM(ValorPedi) ValorPedi, ' +
  ' SUM(ValorFatu) ValorFatu ' +
  ' FROM loccconits' +
  ' WHERE Codigo=' + sCodigo +
  '';
  Dmod.QrAux3.Open;
  //
  ValorBrto := Dmod.QrAux3.FieldByName('ValorBrto').AsFloat;
  sValorBrto := Geral.FFT_Dot(ValorBrto, 2, siNegativo);
  //
  ValorOrca := Dmod.QrAux3.FieldByName('ValorOrca').AsFloat;
  sValorOrca := Geral.FFT_Dot(ValorOrca, 2, siNegativo);
  //
  ValorPedi := Dmod.QrAux3.FieldByName('ValorPedi').AsFloat;
  sValorPedi := Geral.FFT_Dot(ValorPedi, 2, siNegativo);
  //
  ValorFatu := Dmod.QrAux3.FieldByName('ValorFatu').AsFloat;
  sValorFatu := Geral.FFT_Dot(ValorFatu, 2, siNegativo);
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconcab SET ' +
  '  ValorBrto=' + sValorBrto +
  ', ValorOrca=' + sValorOrca +
  ', ValorPedi=' + sValorPedi +
  ', ValorFatu=' + sValorFatu +
  ' WHERE Codigo=' + sCodigo + '; ');
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconcab SET ' +
  // ini 2021-03-30
  //'  ValorTot = ValorLocAtualizado + ValorUsos + ValorConsumos + ValorServicos + ValorVendas ' +
  //', APgarLocacoes = ValorLocAtualizado - PagoLocacoes ' +
  '  ValorTot = IF(ValorLocAtualizado > ValorLocAAdiantar, ValorLocAtualizado, ValorLocAAdiantar) + ValorUsos + ValorConsumos + ValorServicos + ValorVendas ' +
  ', APgarLocacoes = IF(ValorLocAtualizado > ValorLocAAdiantar, ValorLocAtualizado, ValorLocAAdiantar) - PagoLocacoes ' +
  // fim 2021-03-030
  ', APgarUsos = ValorUsos - PagoUsos ' +
  ', APgarConsumos = ValorConsumos - PagoConsumos ' +
  ', APgarServicos = ValorServicos - PagoServicos ' +
  ', APgarVendas = ValorVendas - PagoVendas ' +
  ', PagoPago = PagoLocacoes + PagoUsos + PagoConsumos + PagoServicos + PagoVendas ' +
  ', PagoTotal = PagoLocacoes + PagoUsos + PagoConsumos + PagoServicos + PagoVendas + PagoDesconto ' +
  ', APgarTotal = IF(Status=4, 0, APgarLocacoes + APgarUsos + APgarConsumos + APgarServicos + APgarVendas + PagoDesconto) ' +
  ' WHERE Codigo=' + sCodigo + '; ');
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconcab SET ' +
  ' ValorPndt = IF(Status=4, 0, ValorTot - ValorPago) ' +  // Status 4: Cancelado
  ' WHERE Codigo=' + sCodigo + '; ');
  //
end;

procedure TUnAppPF.AtualizaTotaisLocCConCab_Vendas(Codigo: Integer);
var
  sCodigo, sValorBrto, sValorOrca, sValorPedi, sValorFatu, sValorVendas, sCtrID: String;
  ValorBrto, ValorOrca, ValorPedi, ValorFatu, ValorVendas: Double;
begin
  sCodigo := Geral.FF0(Codigo);
  // Garantir que vai zerar CtrID's que n�o tiverem itens!
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconits SET ' +
  '  ValorBrto=0.00 ' +
  ', ValorOrca=0.00 ' +
  ', ValorPedi=0.00 ' +
  ', ValorFatu=0.00 ' +
  ', ValorBrto=0.00 ' +
  ' WHERE Codigo=' + sCodigo+
  ' AND IDTab=' + Geral.FF0(Integer(TTabConLocIts.tcliVenda)) + '; '); // 3
  //
  ValorVendas := 0;
  Dmod.QrAux3.SQL.Text :=
  ' SELECT ven.CtrID, SUM(ven.ValorBruto) ValorBrto,' +
  ' SUM(IF(its.Status>0, ven.ValorTotal, 0)) ValorOrca,' +
  ' SUM(IF(its.Status>1, ven.ValorTotal, 0)) ValorPedi, ' +
  ' SUM(IF(its.Status>2, ven.ValorTotal, 0)) ValorFatu' +
  ' FROM loccitsven ven' +
  ' LEFT JOIN loccconits its ON its.CtrID=ven.CtrID' +
  ' WHERE ven.Codigo=' + sCodigo +
  ' GROUP BY ven.CtrID' +
  '';
  Dmod.QrAux3.Open;
  while not Dmod.QrAux3.Eof do
  begin
    sCtrID := Geral.FF0(Dmod.QrAux3.FieldByName('CtrID').AsInteger);
    //
    ValorBrto := Dmod.QrAux3.FieldByName('ValorBrto').AsFloat;
    sValorBrto := Geral.FFT_Dot(ValorBrto, 2, siNegativo);
    //
    ValorOrca := Dmod.QrAux3.FieldByName('ValorOrca').AsFloat;
    sValorOrca := Geral.FFT_Dot(ValorOrca, 2, siNegativo);
    //
    ValorPedi := Dmod.QrAux3.FieldByName('ValorPedi').AsFloat;
    sValorPedi := Geral.FFT_Dot(ValorPedi, 2, siNegativo);
    //
    ValorFatu := Dmod.QrAux3.FieldByName('ValorFatu').AsFloat;
    sValorFatu := Geral.FFT_Dot(ValorFatu, 2, siNegativo);
    //
    //
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'UPDATE loccconits SET ' +
    '  ValorBrto=' + sValorBrto +
    ', ValorOrca=' + sValorOrca +
    ', ValorPedi=' + sValorPedi +
    ', ValorFatu=' + sValorFatu +
    ' WHERE CtrID=' + sCtrID + '; ');
    //
    ValorVendas := ValorVendas + ValorPedi;
    //
    Dmod.QrAux3.Next;
  end;
  sValorVendas := Geral.FFT_Dot(ValorVendas, 2, siNegativo);
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  'UPDATE loccconcab SET ValorVendas=' + sValorVendas +
  ' WHERE Codigo=' + sCodigo + '; ');
  //
  AtualizaTotaisLocCConCab_Totais(sCodigo);
  //
end;

procedure TUnAppPF.AtualizaValDevolParci(Codigo, GTRTab, CtrID, Item: Integer);
var
  sValDevolParci, sQtdeDevolucao: String;
begin
  Dmod.QrAux3.SQL.Text :=
  ' SELECT SUM(ValorLocacao) ValorLocacao, ' +
  ' SUM(Quantidade) Quantidade ' +
  ' FROM loccmovall ' +
  ' WHERE Codigo=' + Geral.FF0(Codigo) +
  ' AND GTRTab=' + Geral.FF0(GTRTab) +
  ' AND CtrID=' + Geral.FF0(CtrID) +
  ' AND Item=' + Geral.FF0(Item) +
  // ini 2021-07-07
  ' AND TipoES="E" ' +
  // fim 2021-07-07
  ' ';
  Dmod.QrAux3.Open;
  //
  sValDevolParci := Geral.FFT_Dot(Dmod.QrAux3.FieldByName('ValorLocacao').AsFloat, 2, sinegativo);
  sQtdeDevolucao := Geral.FF0(Dmod.QrAux3.FieldByName('Quantidade').AsInteger);
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  ' UPDATE loccitslca SET ' +
  ' ValDevolParci=' + sValDevolParci +
  ', QtdeDevolucao=' + sQtdeDevolucao +
  ' WHERE Codigo=' + Geral.FF0(Codigo) +
  ' AND CtrID=' + Geral.FF0(CtrID) +
  ' AND Item=' + Geral.FF0(Item) +
  ' ');
  //
end;

procedure TUnAppPF.AtualizaValorAluguel_LocCItsLca(Codigo: Integer; TipoAluguel: String);
var
  ValorLocacao, ValorBruto, QtdeProduto, QtdeLocacao, ValorLocAtualizado: Double;
  Item: Integer;
begin
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  ' UPDATE loccconcab SET ' +
  ' ValorLocAAdiantar=0.00 ' +
  ' WHERE Codigo=' + Geral.FF0(Codigo));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrLca, Dmod.MyDB, [
  'SELECT lpp.Item, lpp.ValorDia, lpp.ValorSem, ',
  'lpp.ValorQui, lpp.ValorMes, lpp.ValorLocacao, ',
  'ValorBruto, QtdeProduto, QtdeLocacao ',
  'FROM loccitslca lpp ',
  'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaPrincipal)),
  '']);
  //
  Dmod.QrLca.First;
  while not Dmod.QrLca.Eof do
  begin
    Item := Dmod.QrLcaItem.Value;
    if TipoAluguel = 'D' then ValorLocacao := Dmod.QrLcaValorDia.Value else
    if TipoAluguel = 'S' then ValorLocacao := Dmod.QrLcaValorSem.Value else
    if TipoAluguel = 'Q' then ValorLocacao := Dmod.QrLcaValorQui.Value else
    if TipoAluguel = 'M' then ValorLocacao := Dmod.QrLcaValorMes.Value else
                              ValorLocacao := 0.00;
    //
    QtdeProduto := Dmod.QrLcaQtdeProduto.Value;
    QtdeLocacao := Dmod.QrLcaQtdeLocacao.Value;
    ValorBruto := QtdeProduto * QtdeLocacao * ValorLocacao;
    ValorLocAtualizado := QtdeProduto * QtdeLocacao * ValorLocacao;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccitslca', False, [
    'ValorLocacao', 'ValorBruto', 'ValorLocAtualizado'], [
    'Item'], [
    ValorLocacao, ValorBruto, ValorLocAtualizado], [
    Item], True);
    //
    Dmod.QrLca.Next;
  end;
end;

procedure TUnAppPF.ConfiguraGGXsDeGGY(GraGruY: Integer;
  iNiveis1: array of Integer);
const
  sProcName = 'TUnAppPF.ConfiguraGGXsDeGGY()';
var
  GraGruX, I: Integer;
  Tabela, sNiveis1: String;
  Qry: TmySQLQuery;
  Continua: Boolean;
begin
  Geral.MB_Info('Ver com a Dermatek: ' + sProcName);
{
  Qry := TmySQLQuery.Create(Dmod);
  try
    sNiveis1 := MyObjects.CordaDeArrayInt(iNiveis1);
    //if Nivel1 <> 0 then
    if sNiveis1 <> '' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM gragrux ',
      //'WHERE GraGru1=' + Geral.FF0(sNiveis1),
      'WHERE GraGru1 IN (' + sNiveis1 + ')',
      '']);
      Qry.First;
      GraGruX := Qry.FieldByName('Controle').AsInteger;
      Tabela := AppPF.ObtemNomeTabelaGraGruY(GraGruY);
      if Tabela <> '' then
      begin
        //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
        Qry.First;
        while not Qry.Eof do
        begin
          UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, Tabela, False, [
          ], [
          'GraGruX'], ['Ativo'], [
          ], [
          Qry.FieldByName('Controle').AsInteger], [1], True);
          //
          Qry.Next;
        end;
        case GraGruY of
          CO_GraGruY_1024_....
          CO_GraGruY_2048_....
          ...
          CO_GraGruY_6144_...:
          begin
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
            'GraGruY'], [
            'Controle'], [
            GraGruY], [
            GraGruX], True);
          end;
          //CO_GraGruY_5120_TXWetEnd,
          begin
            Grade_PF.CorrigeGraGruYdeGraGruX();
            Continua := True;
          end;
          else
          Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
          ' n�o est� implementado em ' + sProcName);
        end;
        if Continua (*and (not JaTemN1)*) then
        begin
          MostraFormXXXxxXxxGraGruY(GraGruY, GraGruX, True, Qry(*, MultiplosGGX*));
        end;
      end;
    end else
      Geral.MB_Erro('N�o foi poss�vel localizar o(s) produto(s) ' + sNiveis1 +
      ' para complementar os dados do seu cadastro!');
  finally
    Qry.Free;
  end;
}
end;

function TUnAppPF.CorrigeGraGruYdeGraGruX(GraGruY, GraGruX: Integer): Boolean;
const
  sProcName = 'TUnAppPF.CorrigeGraGruYdeGraGruX()';
begin
  Result := False;
(*
  case GraGruY of
    CO_GraGruY_0512_TXSubPrd,
    CO_GraGruY_0683_TXPSPPro,
    CO_GraGruY_0853_TXPSPEnd,
    CO_GraGruY_1024_TXNatCad,
    CO_GraGruY_1195_TXNatPDA,
    CO_GraGruY_1365_TXProCal,
    CO_GraGruY_1536_TXCouCal,
    CO_GraGruY_1621_TXCouDTA,
    CO_GraGruY_1707_TXProCur,
    CO_GraGruY_1877_TXCouCur,
    CO_GraGruY_2048_TXRibCad,
    CO_GraGruY_3072_TXRibCla,
    CO_GraGruY_4096_TXRibOpe:
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragrux', False, [
      'GraGruY'], [
      'Controle'], [
      GraGruY], [
      GraGruX], True);
    end;
    CO_GraGruY_5120_TXWetEnd,
    CO_GraGruY_6144_TXFinCla:
    begin
      Grade_PF.CorrigeGraGruYdeGraGruX();
      Result := True;
    end;
    else
*)
      Geral.MB_Erro('Grupo de estoque n�o implementado em ' + sProcName);
//  end;
end;

procedure TUnAppPF.CorrigeReduzidosDuplicadosDeInsumo(
  CorrigeGraTamIts: Boolean);
begin
  // Apenas compatibilidade por enquanto...
end;

function TUnAppPF.DevolveTodosEquipamentosLocacao(Codigo, Empresa: Integer;
  QrItsLca: TmySQLQuery; Finaliza: Boolean; BxaDtHr, DtHrDevolver: TDateTime; ValorPndt:
  Double): Boolean;
var
  Agora: TdateTime;
  sAgora: String;
  //
  procedure CalculaDadosAtual(const Volta: TDateTime; var ValorLocAtualizado:
  Double; var CalcAdiLOCACAO: String);
  const
    GeraLog = True;
  var
    ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto,
    QtdeDevolucao, ValorLocAntigo, ValDevolParci, ValorUnitario: Double;
    DMenos, Troca, DiasFDS, DiasUteis: Integer;
    (*CalcAdiLOCACAO,*) Log: String;
    Calculou: Boolean;
    Saida, (*Volta,*) IniCobranca, OriSubstSaiDH, HrTolerancia: TDateTime;
  begin
    Screen.Cursor := crHourGlass;
    try
      //Volta    := QrLocCConCabDtHrDevolver;
      //
      DMenos         := QrItsLca.FieldByName('DMenos').AsInteger;
      //
(*
      if QrGraGXPatr.State <> dsInactive then
      begin
        ValorDia       := QrGraGXPatrValorDia').AsFloat;
        ValorSem       := QrGraGXPatrValorSem').AsFloat;
        ValorQui       := QrGraGXPatrValorQui').AsFloat;
        ValorMes       := QrGraGXPatrValorMes').AsFloat;
        ValorFDS       := QrGraGXPatrValorFDS').AsFloat;
      end else
      begin
*)
        ValorDia       := QrItsLca.FieldByName('ValorDia').AsFloat;
        ValorSem       := QrItsLca.FieldByName('ValorSem').AsFloat;
        ValorQui       := QrItsLca.FieldByName('ValorQui').AsFloat;
        ValorMes       := QrItsLca.FieldByName('ValorMes').AsFloat;
        ValorFDS       := QrItsLca.FieldByName('ValorFDS').AsFloat;
(*
      end;
*)
      //
      QtdeLocacao    := QrItsLca.FieldByName('QtdeLocacao').AsInteger;
      QtdeProduto    := QrItsLca.FieldByName('QtdeProduto').AsInteger;
      QtdeDevolucao  := QrItsLca.FieldByName('QtdeDevolucao').AsInteger;
      ValorLocAntigo := QrItsLca.FieldByName('ValorLocAtualizado').AsFloat;
      ValDevolParci  := QrItsLca.FieldByName('ValDevolParci').AsFloat;
      Troca          := QrItsLca.FieldByName('Troca').AsInteger;
      IniCobranca    := QrItsLca.FieldByName('CobranIniDtHr').AsDateTime;
      OriSubstSaiDH  := QrItsLca.FieldByName('OriSubstSaiDH').AsDateTime;
      HrTolerancia   := QrItsLca.FieldByName('HrTolerancia').AsDateTime;
      //
      DiasFDS            := 0;
      DiasUteis          := 0;
      CalcAdiLOCACAO     := '';
      ValorUnitario      := 0;
      ValorLocAtualizado := 0.00;
      //
      Calculou           := False;
      Saida              := QrItsLca.FieldByName('SaiDtHr').AsDateTime;
      //Volta              := Trunc(TPVolta.Date);
      //
      AppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
      (*const*) Saida, Volta, HrTolerancia, OriSubstSaiDH, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
      ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao,
      ValDevolParci, Troca, DtHrDevolver, (*var*) Log, (*var*)IniCobranca, (*var*)DiasFDS,
      DiasUteis, (*var*)CalcAdiLOCACAO,
      ValorUnitario, ValorLocAtualizado, Calculou);
      //
      //EdValorLocAtualizado.ValueVariant := ValorLocAtualizado;
      //EdCOBRANCALOCACAO.Text := CalcAdiLOCACAO;
      //
      //Memo1.Text := Log;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

  procedure DevolveItemAtual();
  var
    DataHora, USUARIO, TipoES, COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO,
    TESTADODEVOLUCAO, ObservacaoTroca: String;
    GTRTab, CtrID, Item, Controle, SEQUENCIA, GraGruX, Quantidade,
    TipoMotiv, MotivoTroca, QuantidadeLocada, QuantidadeJaDevolvida: Integer;
    VALORLOCACAO, GGXEntrada: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    //Codigo         := FCodigo;
    case TManejoLca(QrItsLca.FieldByName('ManejoLca').AsInteger) of
      TManejoLca.manelcaPrincipal,
      TManejoLca.manelcaSecundario,
      TManejoLca.manelcaAcessorio: GTRTab := Integer(TGraToolRent.gbsLocar);
      TManejoLca.manelcaApoio,
      TManejoLca.manelcaUso,
      TManejoLca.manelcaConsumo: GTRTab := Integer(TGraToolRent.gbsOutrs);
    end;
    CtrID                 := QrItsLca.FieldByName('CtrID').AsInteger;
    Item                  := QrItsLca.FieldByName('Item').AsInteger;
    Controle              := 0;
    SEQUENCIA             := -1;
    DataHora              := sAgora;
    USUARIO               := '';
    GraGruX               := QrItsLca.FieldByName('GraGruX').AsInteger;
    TipoES                := 'E'; // Entrada
    Quantidade            :=  (QrItsLca.FieldByName('QtdeLocacao').AsInteger *
                              QrItsLca.FieldByName('QtdeProduto').AsInteger) -
                              QrItsLca.FieldByName('QtdeDevolucao').AsInteger;
    TipoMotiv             := Integer(TTipoMotivLocMov.tmlmLocarVolta);
    COBRANCALOCACAO       := ''; //EdCOBRANCALOCACAO.Text;
    VALORLOCACAO          := 0.00; //EdValorLocAtualizado.ValueVariant;
    CalculaDadosAtual(Agora, VALORLOCACAO, COBRANCALOCACAO);
    //
    LIMPO                 := ''; //dmkPF.EscolhaDe2Str(CkLimpo.Checked, 'S', 'N');
    SUJO                  := ''; //dmkPF.EscolhaDe2Str(CkSujo.Checked, 'S', 'N');
    QUEBRADO              := ''; //dmkPF.EscolhaDe2Str(CkQuebrado.Checked, 'S', 'N');
    TESTADODEVOLUCAO      := ''; //dmkPF.EscolhaDe2Str(CkTestadoDevolucao.Checked, 'S', 'N');
    GGXEntrada            := 0;
    MotivoTroca           := 0;
    ObservacaoTroca       := '';
    QuantidadeLocada      := QrItsLca.FieldByName('QtdeProduto').AsInteger;
    QuantidadeJaDevolvida := QrItsLca.FieldByName('QtdeDevolucao').AsInteger;
    //
    (*if Quantidade > (FQuantidadeAlt + QuantidadeLocada - QuantidadeJaDevolvida) then
    begin
      if Geral.MB_Pergunta(
      'Quantidade de devolu��o maior que o poss�vel! ' + sLineBreak +
      'Ser� necess�rio Senha BOSS!' + sLineBreak +
      'Deseja continuar assim mesmo?') = ID_YES then
      begin
        if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
          Exit;
      end else
        Exit;
    end;
    *)
    if Quantidade <= 0 then Exit;
    //
    Controle := UMyMod.BPGS1I32('loccmovall', 'Controle', '', '', tsPos, SQLType, Controle);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccmovall', False, [
    'Codigo', 'GTRTab', 'CtrID',
    'Item', 'SEQUENCIA', 'DataHora',
    'USUARIO', 'GraGruX', 'TipoES',
    'Quantidade', 'TipoMotiv', 'COBRANCALOCACAO',
    'VALORLOCACAO', 'LIMPO', 'SUJO',
    'QUEBRADO', 'TESTADODEVOLUCAO', 'GGXEntrada',
    'MotivoTroca', 'ObservacaoTroca', 'QuantidadeLocada',
    'QuantidadeJaDevolvida'], [
    'Controle'], [
    Codigo, GTRTab, CtrID,
    Item, SEQUENCIA, DataHora,
    USUARIO, GraGruX, TipoES,
    Quantidade, TipoMotiv, COBRANCALOCACAO,
    VALORLOCACAO, LIMPO, SUJO,
    QUEBRADO, TESTADODEVOLUCAO, GGXEntrada,
    MotivoTroca, ObservacaoTroca, QuantidadeLocada,
    QuantidadeJaDevolvida], [
    Controle], True) then
    begin
      AppPF.AtualizaValDevolParci(Codigo, GTRTab, CtrID, Item);
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa, True);
    end;
  end;
begin
  Result := False;
  Agora  := DModG.ObtemAgora();
  sAgora := Geral.FDT(Agora, 109);
  //
  ReopenItsLca(QrItsLca, Codigo, 'AND QtdeDevolucao < (QtdeLocacao * QtdeProduto)');
  QrItsLca.First;
  while not QrItsLca.Eof do
  begin
    DevolveItemAtual();
    //
    QrItsLca.Next;
  end;
  Result := True;
  if Finaliza then
  begin
    MudaStatus_Finalizado(Codigo, QrItsLca, BxaDtHr, ValorPndt);
  end;
end;

procedure TUnAppPF.EquipamentoComEstoque(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXPatr(0);
end;

procedure TUnAppPF.EquipamentoSemEstoque(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXOutr(0);
end;

function TUnAppPF.GeraFatPedCabEPediVdaDeLocCItsVen(const SQLType: TSQLType;
  const DtHrEmi: TDateTime; const Empresa, Cliente, Codigo: Integer;
  var Confirmado: Boolean; var PediVda, FatPedCab, FatPedVolCnta:
  Integer): Boolean;
var
  DtaInclu, DtaPrevi: TDateTime;
  RegrFiscal, FretePor, indFinal, indPres, UnidMed, Cnta: Integer;
begin
  Result        := True;
  PediVda       := 0;
  FatPedCab     := 0;
  FatPedVolCnta := 0;
  //






  DtaInclu   := DtHrEmi;
  DtaPrevi   := DtHrEmi;
  RegrFiscal := Dmod.QrOpcoesTRenLocRegrFisNFe.Value;
  //
  PediVda    := 0;
  FatPedCab  := 0;
  //
  if MyObjects.FIC(RegrFiscal = 0, nil,
  'Regra fiscal n�o definida nas op��es espec�ficas!') then
  begin
    Confirmado := False;
    Exit;
  end;
  //
  FretePor       := 9; //Sem Frete! // Presencial (indPres<>4) n�o pode ter frete (modFrete<>9)
  indFinal       := 1; // Consumidor final
  indPres        := 1; // Presencial
  //
  if AppPF.InserePedidoVda(Empresa, Cliente, DtaInclu, DtaPrevi, RegrFiscal,
  (*CondicaoPG*)0, (*CartEmis*)0, (*TabelaPrc*)0, indFinal,
  indPres, (*finNFe**)1, (*idDest*)1, FretePor, (*EntregaEnti*)0,
  (*RetiradaEnti*)0, (*DOCENT*)'', (*PedidoCli*)'', (*Observa*)'',
  PediVda, FatPedCab) then
  begin
    UnidMed := 0;
    Cnta := UMyMod.BuscaEmLivreY_Def('fatpedvol', 'Cnta', SQLType, Cnta);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fatpedvol', False, [
    'Codigo', 'UnidMed'], ['Cnta'], [Codigo, UnidMed], [Cnta], True) then
    begin
      FatPedVolCnta := Cnta;
      //
      Result := True;

      (*
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
      'PediVda', 'FatPedCab', 'FatPedVolCnta'], ['CtrID'], [
      PediVda, FatPedCab, FatPedVolCnta], [CtrID], True)
      then ;
      *)

    end;
  end;
end;

function TUnAppPF.GGXCadGetForcaCor(GraGruY: Integer): Boolean;
begin
(*
  Result :=
    (GraGruY = CO_GraGruY_6144_TXFinCla)
    or
    (GraGruY = CO_GraGruY_5120_TXWetEnd);
*)
  Result := True;
end;

function TUnAppPF.GGXCadGetForcaTam(GraGruY: Integer): Boolean;
begin
  Result := GGXCadGetForcaCor(GraGruY);
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.LocalizaOrigemDoFatPedCab(FatPedCab: Integer);
var
  Contrato, CtrID: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo, CtrId ',
  'FROM loccconits',
  'WHERE Fatpedcab=' + Geral.FF0(FatPedCab),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    if FmLocCConCab <> nil then
    begin
      if TFmLocCConCab(FmLocCConCab).ImgTipo.SQLType <> stLok then
      begin
        Geral.MB_Aviso(
        'A janela de Gerenciamento de Loca��o n�o est� em modo travado!' +
        'Finalize a Edi��o ou Inclus�o de Loca��o!');
        //
        Exit;
      end;
    end;
    //
    Contrato  := Dmod.QrAux.Fields[0].AsInteger;
    CtrId     := Dmod.QrAux.Fields[1].AsInteger;
    //
    FmPrincipal.AdvToolBarPagerNovo.Visible := False;
    GraL_Jan.MostraFormLocCCon(True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, Contrato);
    if VAR_FmLocCConCab <> nil then
      TFmLocCConCab(VAR_FmLocCConCab).ReopenLocCConIts(CtrID);
  end;
end;

function TUnAppPF.LocCItsMat_IncluiAce(const SQLType: TSQLType; const Codigo,
  CtrID, GraGruX, Empresa, ManejoLca: Integer; const QtdeLocacao, QtdeProduto,
  ValorProduto: Double; const DtHrLocado, Categoria: String; const QrLocIPatAce:
  TmySQLQuery; const OriSubstItem: Integer; const OriSubstSaiDH: String; var
  Item: Integer): Boolean;
//var
  //Item: Integer;
  //ValBem
  //PrcUni, QtdIni
  //QtdeProduto, QtdeLocacao, ValorProduto: Double;
begin
  Result := False;
  Item := 0;
  //ValBem  := QrGraGXTollItemValr.Value;
(*&�%$
  ValorProduto  := QrGraGXTollItemValr.Value;
  QtdeProduto   := EdQtdeProduto.ValueVariant;
  QtdeLocacao   := EdQtdeLocacao.ValueVariant;
*)
  //
  //
  Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, stIns, 0);
  //UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatace', False, [
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
  'Codigo', 'GraGruX', 'ValorDia',
  'ValorSem', 'ValorQui', 'ValorMes',
  'DtHrLocado',
  'Categoria', 'QtdeProduto',
  'ValorProduto', 'QtdeLocacao', 'ManejoLca',
  'CtrID', 'ValorFDS', 'DMenos',
  'OriSubstItem', 'OriSubstSaiDH'], [
  'Item'], [
  Codigo, GraGruX, (*ValorDia*)0,
  (*ValorSem*)0, (*ValorQui*)0, (*ValorMes*)0,
  DtHrLocado,
  Categoria, QtdeProduto,
  ValorProduto, QtdeLocacao, ManejoLca,
  CtrID, (*ValorFDS*)0, (*DMenos*)0,
  OriSubstItem, OriSubstSaiDH], [
  Item], True) then
  begin
    Result := True;
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa, True);
    AppPF.AtualizaTotaisLocCConCab_Locacao(Codigo);
    if QrLocIPatAce <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(QrLocIPatAce, Dmod.MyDB);
      QrLocIPatAce.Locate('Item', Item, []);
    end;
    //FmLocCConCab.ReopenLocIPatAce(QrLocIPatAce, Codigo, CtrID, Item);
  end;
end;

function TUnAppPF.LocCItsMat_IncluiApo(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, ManejoLca: Integer; const PrcUni, QtdeLocacao, QtdeProduto,
              ValorProduto: Double; const DtHrLocado, Categoria: String;
              const QrLocIPatApo: TmySQLQuery; var Item: Integer): Boolean;
var
  //Item: Integer;
  QtdIni: Double;
begin
  Result := False;
  Item := 0;
  //PrcUni        := QrGraGXTollItemValr.Value;
  QtdIni        := 0;
  //ValorProduto  := QrGraGXTollItemValr.Value;
  //QtdeLocacao   := EdQtdeLocacao.ValueVariant;
  //QtdeProduto   := EdQtdeProduto.ValueVariant;
  //
  //
  Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitsruc', False, [
    'Codigo', 'CtrID', 'GraGruX',
    //'ValBem'

    'PrcUni', 'QtdIni', 'QtdeLocacao',

    //'PrcUni', 'QtdIni'
    'Categoria', 'QtdeProduto', 'ManejoLca',
    'ValorProduto'
     ], ['Item'], [
    Codigo, CtrID, GraGruX,
    //ValBem

    PrcUni, QtdIni, QtdeLocacao,

    //'PrcUni', 'QtdIni'
    Categoria, QtdeProduto, ManejoLca,
    ValorProduto
    ], [Item], True)
  then
  begin
    Result := True;
    if QrLocIPatApo <> nil then
      FmLocCConCab.ReopenLocIPatApo(QrLocIPatApo, Codigo, CtrID, Item);
  end;
end;

function TUnAppPF.LocCItsMat_IncluiCns(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, ManejoLca, Unidade: Integer; const PrcUni, QtdeLocacao,
              QtdeProduto, ValorProduto: Double; const DtHrLocado, Categoria:
              String; const QrLocIPatCns: TmySQLQuery; var Item: Integer): Boolean;
var
  //Item: Integer;
  QtdIni, QtdFim, ValUso: Double;
begin
  Result := False;
  Item := 0;
  //Unidade        := QrGraGXTollItemUnid.Value;
  QtdIni         := 0;
  QtdFim         := 0;
  //PrcUni         := QrGraGXTollItemValr.Value;;
  ValUso         := 0;
  //QtdeProduto    := EdQtdeProduto.ValueVariant;
  //QtdeLocacao    := EdQtdeLocacao.ValueVariant;
  //ValorProduto   := QrGraGXTollItemValr.Value;
  //
  Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
    'Codigo', 'CtrID', 'GraGruX',

    'Categoria', 'QtdeProduto', 'ManejoLca',

    'Unidade', 'QtdIni', 'QtdFim',
    'PrcUni', 'ValUso', 'QtdeLocacao',
    'ValorProduto'], [
    'Item'], [
    Codigo, CtrID, GraGruX,

    Categoria, QtdeProduto, ManejoLca,

    Unidade, QtdIni, QtdFim,
    PrcUni, ValUso, QtdeLocacao,
    ValorProduto], [
    Item], True)
  then
  begin
    Result := True;
    if QrLocIPatCns <> nil then
      FmLocCConCab.ReopenLocIPatCns(QrLocIPatCns, Codigo, CtrID, Item);
  end;
end;


function TUnAppPF.ImpedePorMovimentoAberto: Boolean;
begin
   // Compatibilidade: Estoque stqmovitsa com abertura e fechamento
  Result := False;
end;

function TUnAppPF.InsAltLocCItsVen(const SQLTYpe: TSQLType; const Codigo, CtrID,
  FatID, Empresa, FatPedCab, FatPedVolCnta: Integer; const ManejoLca: TManejoLca;
  const GraGru1, Unidade, GraGruX, StqCencad, NF_Stat: Integer; const Quantidade, PrcUni,
  PercDesco, ValorBruto, ValorTotal, prod_vFrete, prod_vSeg, prod_vDesc,
  prod_vOutro: Double; const OldStatusMovimento, NewStatusMovimento: TStatusMovimento;
  // ini 2023-12-16
  const AltItem: Integer; var SMIA_IDCtrl: Integer;
  // ini 2023-12-16
  var FAlterou: Boolean; var FNewCtrID, FNewItem: Integer; var
  Tmp_FatPedFis: String): Boolean;
const
  Categoria = 'S';
var
  Continua: Boolean;
  Item, OriCtrl, IDCtrl, SMIA_OriCtrl, (*SMIA_IDCtrl,*) Status: Integer;
  PrecoR, QuantP, DescoP, Estoque, Saldo: Double;
  sIDCtrl: String;
  EhPedido: Boolean;
  EraOrcamento: Boolean;
  //
  //prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
begin
  Result := False;
  //
  Dmod.ReopenEstoque(Empresa, GraGruX, StqCenCad);
  //
          Status := Integer(NewStatusMovimento);
          //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'loccconits', False, [
  'Status'], ['CtrID'], [Status], [CtrID], True);
  //
  EhPedido       := TStatusMovimento(NewStatusMovimento) = TStatusMovimento.statmovPedido;
  EraOrcamento   := TStatusMovimento(OldStatusMovimento) = TStatusMovimento.statmovOrcamento;
  if EhPedido then
  begin
    Estoque := Dmod.QrEstoqueQtde.Value;
    Saldo   := Estoque - Quantidade;
    if Saldo < 0 then
    begin
      if Geral.MB_Pergunta('Estoque insuficiente!' + sLineBreak +
      'Deseja continuar assim mesmo?') <> ID_YES then
      begin
        if EraOrcamento then
        begin
          // ini 2023-12-17
          //if IDCtrl <> 0 then
          if SMIA_IDCtrl <> 0 then
          begin
            sIDCtrl := Geral.FF0(SMIA_IDCtrl);
            //
(*
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE IDCtrl=' + sIDCtrl);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovvala WHERE IDCtrl=' + sIDCtrl);
*)
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE stqmovitsa SET Baixa=0 WHERE IDCtrl=' + sIDCtrl);
            UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE stqmovvala SET Ativo=0 WHERE IDCtrl=' + sIDCtrl); // Serve para algo?
            // fim 2023-12-17
            //
          end;
        end;
        //
        Exit;
      end;
    end;
  end;
  //
  if not Dmod.ReopenItem(GraGruX) then Exit;
  //
  //if FEhPedido then
  if EhPedido then
  begin
    if (Dmod.QrFatPedCab.State = dsInactive) or (Tmp_FatPedFis = EmptyStr) then
    begin
      Dmod.ReopenFatPedCab(FatPedCab, Tmp_FatPedFis);
    end;
    PrecoR := PrcUni;
    QuantP := Quantidade;
    DescoP := 0;
    // ini 2020-12-11
    (*
    Continua := InsereItem2(SQLType, FatID, StqCenCad, FatPedVolCnta, GraGru1, PrecoR, QuantP,
    DescoP, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro, Tmp_FatPedFis, OriCtrl, IDCtrl);
    *)
    //
    // ini 2023-12-17
    // Se SMIA_IDCtrl > 0 ...
    // ... excluir lan�amento no estoque e nfe para evitar erro ou duplicidade
    // j� que pode ser que tente lan�ar novamente o estoque.
    if SMIA_IDCtrl <> 0 then
    begin
      sIDCtrl := Geral.FF0(SMIA_IDCtrl);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE IDCtrl=' + sIDCtrl);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovvala WHERE IDCtrl=' + sIDCtrl);
    end;
    // fim 2023-12-17
    //
    Continua := InsereItem2(stIns, FatID, StqCenCad, FatPedVolCnta, GraGru1, PrecoR, QuantP,
    DescoP, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro, Tmp_FatPedFis, OriCtrl, IDCtrl);
    // fim 2020-12-11
  end else
  begin
    Continua := True;
    OriCtrl  := 0;
    IDCtrl   := 0;
  end;
  if Continua then
  begin
    SMIA_OriCtrl := OriCtrl;
    SMIA_IDCtrl  := IDCtrl;
    //
    // Ini 2023-12-16
    if SQLType = stUpd then
      Item := AltItem;
    // Fim 2023-12-16
    Item := UMyMod.BPGS1I32('loccitsven', 'Item', '', '', tsPos, SQLType, Item);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitsven', False, [
    'Codigo', 'CtrID', 'ManejoLca',
    'GraGruX', 'Quantidade', 'PrcUni',
    'ValorTotal', 'Categoria',
    'SMIA_OriCtrl', 'SMIA_IDCtrl',
    'prod_vFrete', 'prod_vSeg',
    'prod_vDesc', 'prod_vOutro',
    'StqCenCad', 'NF_Stat',
    'PercDesco', 'ValorBruto', 'Unidade'], [
    'Item'], [
    Codigo, CtrID, ManejoLca,
    GraGruX, Quantidade, PrcUni,
    ValorTotal, Categoria,
    SMIA_OriCtrl, SMIA_IDCtrl,
    prod_vFrete, prod_vSeg,
    prod_vDesc, prod_vOutro,
    StqCenCad, NF_Stat,
    PercDesco, ValorBruto, Unidade], [
    Item], True) then
    begin
      FAlterou := True;
      AppPF.AtualizaTotaisLocCConCab_Vendas(Codigo);
      FNewCtrID := CtrID;
      FNewItem  := Item;
      Result    := True;
    end else
    begin
      if EhPedido then
        if SMIA_IDCtrl <> 0 then
          Geral.MB_Teste('Exclus�o cancelada. Avise a Dermatek!');
      // ini 2023-12-17
      // Anular c�digo abaixo pois pode dar erro no estoque ao voltar a baixa
      // para ativa e n�o haveria mais a baixa para ativar!!!!!
      (*
      // Se ocorreu erro...
      // ... excluir lan�amento no estoque e nfe
      //if FEhPedido then
      if EhPedido then
      begin
        if SMIA_IDCtrl <> 0 then
        begin
          sIDCtrl := Geral.FF0(SMIA_IDCtrl);
          UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovitsa WHERE IDCtrl=' + sIDCtrl);
          UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'DELETE FROM stqmovvala WHERE IDCtrl=' + sIDCtrl);
        end;
      end;
      *)
    end;
  end;
end;

function TUnAppPF.InsereItem2(const SQLType: TSQLType; const FatID, StqCenCad,
  FatPedVolCnta, GraGru1: Integer;
  const PrecoR, QuantP, DescoP, prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro:
  Double; const Tmp_FatPedFis: String; var OriCtrl, IDCtrl: Integer): Boolean;
// copiado de: procedure TFmFatDivGru2.InsereItem2(Col, Row: Integer);
const
  Ativa = True;
var
  Cliente, RegrFiscal, GraGruX,
  //StqCenCad,
  FatSemEstq, AFP_Sit: Integer;
  AFP_Per: Double;
  Cli_Tipo: Integer;
  Cli_IE: String;
  Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer;
  Item_IPI_ALq: Double;
  Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE: Double;
  Preco_MedOrdem: Integer;
  //SQLType: TSQLType;
  PediVda: Integer;
  TabelaPrc, OriCodi, Empresa, OriCnta, Associada, OriPart, InfAdCuztm: Integer;
  NO_tablaPrc: String;
  //
  Falta, Pecas, AreaM2, AreaP2, Peso: Double;
  Msg: String;
//var
  ErrPreco, AvisoPrc, QtdRed, Casas, Nivel1, Codigo, Controle: Integer;
  //PrecoR, QuantP, DescoP,
  Tot, Qtd, (*PrecoO,*) PrecoF, ValBru, ValLiq, DescoV, DescoI: Double;
  //
// var
  Qtde: Double;
  TipoCalc, prod_indTot(*, GraGru1*): Integer;
begin
  Result := False;
  //
  InfAdCuztm := 0;
(*
  //StqCenCad        := Geral.IMV(EdStqCenCad.Text);
  if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
    'Informe o Centro de estoque!', 'Codigo', 'CodUsu') then Exit;
*)
  //
  //NFe_FatID        := FatID; //VAR_FATID_0001;
  Cliente          := (*FmFatDivGer2.*)Dmod.QrFatPedCabCliente.Value; //QrCliCodigo.Value;
  RegrFiscal       := (*FmFatDivGer2.*)Dmod.QrFatPedCabRegrFiscal.Value; //QrPediVdaRegrFiscal.Value;
  GraGruX          := Dmod.QrItemGraGruX.Value;
  prod_indTot      := Dmod.QrItemprod_indTot.Value;
  FatSemEstq       := (*FmFatDivGer2.*)Dmod.QrFatPedCabFatSemEstq.Value;
  AFP_Sit          := (*FmFatDivGer2.*)Dmod.QrFatPedCabAFP_Sit.Value;
  AFP_Per          := (*FmFatDivGer2.*)Dmod.QrFatPedCabAFP_Per.Value;
  Cli_Tipo         := (*FmFatDivGer2.*)Dmod.QrCliTipo.Value;
  Cli_IE           := (*FmFatDivGer2.*)Dmod.QrCliIE.Value;
  Cli_UF           := Trunc((*FmFatDivGer2.*)Dmod.QrCliUF.Value);
  EMP_UF           := (*FmFatDivGer2.*)Dmod.QrFatPedCabEMP_UF.Value;
  EMP_FILIAL       := (*FmFatDivGer2.*)Dmod.QrFatPedCabEMP_FILIAL.Value;
  ASS_CO_UF        := (*FmFatDivGer2.*)Dmod.QrFatPedCabASS_CO_UF.Value;
  ASS_FILIAL       := (*FmFatDivGer2.*)Dmod.QrFatPedCabASS_FILIAL.Value;
  Item_MadeBy      := Dmod.QrItemMadeBy.Value;
  ITEM_IPI_Alq     := Dmod.QrItemIPI_Alq.Value;
  Empresa          := (*FmFatDivGer2.*)Dmod.QrFatPedCabEmpresa.Value;
  //
  //TipoCalc := Geral.IMV(Grade0.Cells[Col, Row]);
  TipoCalc := DmProd.DefineTipoCalc((*FmFatDivGer2.*)Dmod.QrFatPedCabRegrFiscal.Value,
                StqCenCad, Empresa);
  //
  if TipoCalc = -1 then
    Exit;
  //
  Casas    := Dmod.QrControleCasasProd.Value;
  //PrecoO   := Geral.DMV(GradeL.Cells[Col, Row]);
  //PrecoR   := Geral.DMV(GradeF.Cells[Col, Row]);
  //QuantP   := Geral.DMV(GradeQ.Cells[Col, Row]);
  //DescoP   := Geral.DMV(GradeD.Cells[Col, Row]);
  DescoI   := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
  PrecoF   := PrecoR - DescoI;
  DescoV   := DescoI * QuantP;
  ValBru   := PrecoR * QuantP;
  ValLiq   := ValBru - DescoV;
//
  Preco_PrecoF     := (*QrPreco*)PrecoF(*.Value*);
  Preco_PercCustom := 0;//(*QrPreco*)PercCustom(*.Value*);
  Preco_MedidaC    := 0;//(*QrPreco*)MedidaC(*.Value*);
  Preco_MedidaL    := 0;//(*QrPreco*)MedidaL(*.Value*);
  Preco_MedidaA    := 0;//(*QrPreco*)MedidaA(*.Value*);
  Preco_MedidaE    := 0;//(*QrPreco*)MedidaE(*.Value*);
  Preco_MedOrdem   := 0;//(*QrPreco*)MedOrdem(*.Value*);
//
  //
  //SQLType          := ImgTipo.SQLType;
  PediVda          := (*FmFatDivGer2.*)Dmod.QrFatPedCabPedido.Value;
  TabelaPrc        := (*FmFatDivGer2.*)Dmod.QrFatPedCabTabelaPrc.Value;
  NO_tablaPrc      := (*FmFatDivGer2.*)Dmod.QrFatPedCabNO_TabelaPrc.Value;
  OriCodi          := (*FmFatDivGer2.*)Dmod.QrFatPedCabCodigo.Value;
  OriCnta          := FatPedVolCnta; //FFatPedVolCnta; //(*FmFatDivGer2.*)QrFatPedVolCnta.Value;
  Associada        := (*FmFatDivGer2.*)Dmod.QrFatPedCabAssociada.Value;
  //
  if (Dmod.QrItem.State = dsInactive) or (Dmod.QrItem.RecordCount = 0) then
  begin
    Geral.MB_Aviso('Reduzido n�o definido!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Qtde := QuantP;
(*
  if FMultiGrandeza then
  begin
    Pecas  := - Geral.DMV(Grade1.Cells[Col, Row]);
    Peso   := - Geral.DMV(Grade2.Cells[Col, Row]);
    AreaM2 := - Geral.DMV(Grade3.Cells[Col, Row]);
    AreaP2 := - Geral.DMV(Grade4.Cells[Col, Row]);
    //
    if not Grade_PF.ValidaGrandeza(TipoCalc, Dmod.QrItemHowBxaEstq.Value, Pecas,
      AreaM2, AreaP2, Peso, Msg) then
    begin
      Geral.MB_Aviso(Msg);
      Exit;
    end;
  end else*)
  begin
    Pecas  := 0;
    AreaM2 := 0;
    AreaP2 := 0;
    Peso   := 0;
  end;
  IDCtrl := 0;
  OriPart := 0;
  //
  if DmFatura.InsereItemStqMov(FatID, OriCodi, OriCnta, Empresa, Cliente,
    Associada, RegrFiscal, GraGruX, NO_tablaPrc, Tmp_FatPedFis, StqCenCad,
    FatSemEstq, AFP_Sit, AFP_Per, Qtde, Cli_Tipo, Cli_IE, Cli_UF, EMP_UF,
    EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq, Preco_PrecoF,
    Preco_PercCustom, Preco_MedidaC, Preco_MedidaL, Preco_MedidaA, Preco_MedidaE,
    Preco_MedOrdem, SQLType, PediVda, OriPart, InfAdCuztm, (*TipoNF*)0,
    (*modNF*)0, (*Serie*)0, (*nNF*)0, (*SitDevolu*)0, (*Servico*)0, (*refNFe*)'',
    0, OriCtrl, Pecas, AreaM2, AreaP2, Peso, TipoCalc, prod_indTot,
    prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
    IDCtrl, Ativa) then
  begin
    //GraGru1 := QrGraGXVendGraGru1.Value;
    //
    Grade_PF.InsereFatPedFis(Tmp_FatPedFis, GraGru1, IDCtrl, FatID,
      OriCodi, OriCtrl, OriCnta, OriPart, RegrFiscal);
    //
    Result := True;
  end;
end;

function TUnAppPF.InserePedidoVda(const Empresa, Cliente: Integer; const
  _DtaInclu, _DtaPrevi: TDateTime; const RegrFiscal, CondicaoPG, CartEmis,
  TabelaPrc, indFinal, indPres, finNFe, idDest, FretePor, EntregaEnti,
  RetiradaEnti: Integer; const DOCENT,
  PedidoCli, Observa: String; var _PediVda, _FatPedCab: Integer): Boolean;
  ////////////////////////////////////////////////////////////////////////////////
var // PediVda
  (*CondPag, CentroCust, ItemCust,*)
  DtaEmiss, DtaEntra, DtaInclu, DtaPrevi: String;
  Codigo, CodUsu, Represen, Prioridade, Moeda, Situacao, MotivoSit, LoteProd,
  Transporta, Redespacho, AFP_Sit, (*Tipo,*) EntSai, UsaReferen, IndSinc,
  RetiradaUsa, EntregaUsa: Integer;
  DesoAces_V, DesoAces_P, Frete_V, Frete_P, Seguro_V, Seguro_P,
  (*TotalQtd, Total_Vlr, Total_Des, Total_Tot,*) AFP_Per, ComisFat, ComisRec
  (*, QuantP, ValLiq*): Double;
  SQLType: TSQLType;
////////////////////////////////////////////////////////////////////////////////
var // fatpedcab
  Pedido(*, Codigo, CodUsu*): Integer;
  Abertura: String;
////////////////////////////////////////////////////////////////////////////////
begin
  Result         := False;
  _PediVda       := 0;
  _FatPedCab     := 0;
  SQLType        := stIns;
  Codigo         := 0;
  CodUsu         := 0;
  //Empresa        := ;
  //Cliente        := ;
  DtaEmiss       := '0000-00-00 00:00:00';
  DtaEntra       := '0000-00-00 00:00:00';
  DtaInclu       := Geral.FDT(_DtaInclu, 109);
  DtaPrevi       := Geral.FDT(_DtaInclu, 109);
  Represen       := 0;
  Prioridade     := 0;
  //CondicaoPG     := ;
  Moeda          := 1;
  Situacao       := 0;
  //TabelaPrc      := ;
  MotivoSit      := 0;
  LoteProd       := 0;
  //PedidoCli      := '';
  //FretePor       := 9; //Sem Frete! // Presencial (indPres<>4) n�o pode ter frete (modFrete<>9)
  Transporta     := 0;
  Redespacho     := 0;
  //RegrFiscal     := ;
  DesoAces_V     := 0.00;
  DesoAces_P     := 0.00;
  Frete_V        := 0.00;
  Frete_P        := 0.00;
  Seguro_V       := 0.00;
  Seguro_P       := 0.00;
  (*
  TotalQtd       := ;
  Total_Vlr      := ;
  Total_Des      := ;
  Total_Tot      := ;
  *)
  //Observa        := ;
  AFP_Sit        := 0;
  AFP_Per        := 0.00;
  ComisFat       := 0.00;
  ComisRec       := 0.00;
  //CartEmis       := ;
  (*
  QuantP         := ;
  ValLiq         := ;
  Tipo           := 0;
  *)
  EntSai         := 1;
  (*CondPag        := ;
  CentroCust     := ;
  ItemCust       := ;
  *)
  UsaReferen     := 0;
  //idDest         := ;
  //indFinal       := 1; // Consumidor final
  //indPres        := 1; // Presencial
  IndSinc        := 1; // Sincrono
  //finNFe         := ;
  RetiradaUsa    := Geral.BoolToInt(RetiradaEnti <> 0);
  //RetiradaEnti   := ;
  EntregaUsa     := Geral.BoolToInt(EntregaEnti <> 0);;
  //EntregaEnti    := ;
  //
(*
    // NFe 3.10
    if not Entities.ValidaIndicadorDeIEEntidade(DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesindIEDest.Value, DmPediVda.QrClientesTipo.Value, False,
      Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
*)
  Codigo := UMyMod.BuscaEmLivreY_Def('pedivda', 'Codigo', SQLType, (*Codigo*)0);
  //EdCodigo_Ped.ValueVariant := Codigo;
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'pedivda', 'CodUsu', [], [],
    SQLType, (*EdCodUsu_Ped.ValueVariant*)0, siNegativo, (*EdCodUsu_Ped*)nil);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivda', False, [
  'CodUsu', 'Empresa', 'Cliente',
  'DtaEmiss', 'DtaEntra', 'DtaInclu',
  'DtaPrevi', 'Represen', 'Prioridade',
  'CondicaoPG', 'Moeda', 'Situacao',
  'TabelaPrc', 'MotivoSit', 'LoteProd',
  'PedidoCli', 'FretePor', 'Transporta',
  'Redespacho', 'RegrFiscal', 'DesoAces_V',
  'DesoAces_P', 'Frete_V', 'Frete_P',
  'Seguro_V', 'Seguro_P', (*'TotalQtd',
  'Total_Vlr', 'Total_Des', 'Total_Tot',*)
  'Observa', 'AFP_Sit', 'AFP_Per',
  'ComisFat', 'ComisRec', 'CartEmis',
  (*'QuantP', 'ValLiq', 'Tipo',*)
  'EntSai', (*'CondPag', 'CentroCust',
  'ItemCust',*) 'UsaReferen', 'idDest',
  'indFinal', 'indPres', 'IndSinc',
  'finNFe', 'RetiradaUsa', 'RetiradaEnti',
  'EntregaUsa', 'EntregaEnti'], [
  'Codigo'], [
  CodUsu, Empresa, Cliente,
  DtaEmiss, DtaEntra, DtaInclu,
  DtaPrevi, Represen, Prioridade,
  CondicaoPG, Moeda, Situacao,
  TabelaPrc, MotivoSit, LoteProd,
  PedidoCli, FretePor, Transporta,
  Redespacho, RegrFiscal, DesoAces_V,
  DesoAces_P, Frete_V, Frete_P,
  Seguro_V, Seguro_P, (*TotalQtd,
  Total_Vlr, Total_Des, Total_Tot,*)
  Observa, AFP_Sit, AFP_Per,
  ComisFat, ComisRec, CartEmis,
  (*QuantP, ValLiq, Tipo,*)
  EntSai, (*CondPag, CentroCust,
  ItemCust,*) UsaReferen, idDest,
  indFinal, indPres, IndSinc,
  finNFe, RetiradaUsa, RetiradaEnti,
  EntregaUsa, EntregaEnti], [
  Codigo], True) then
  begin
(*

INSERT INTO pedivda SET AlterWeb = 1, UserCad=-1, DataCad="2020-10-11"
,
Cliente=13774,
Situacao=0,
DtaInclu="2020-10-11 16:32:07",
DtaEmiss="1899-12-30 16:32:07",
DtaEntra="1899-12-30 16:32:07",
Prioridade=0,
DtaPrevi="2020-10-11 16:32:07",
PedidoCli="",
CartEmis=6,
CodUsu=-1126,
Codigo=1127,
UsaReferen=0,
FretePor=0,
Transporta=0,
Redespacho=0,
DesoAces_V=0.000000000000000,
Frete_V=0.000000000000000,
Seguro_P=0.000000000000000,
DesoAces_P=0.000000000000000,
Frete_P=0.000000000000000,
Seguro_V=0.000000000000000,
Represen=0,
ComisFat=0.000000000000000,
ComisRec=0.000000000000000,
idDest=1,
indFinal=0,
indPres="9",
IndSinc=1,
finNFe=1,
EntregaUsa=0,
EntregaEnti=0,
RetiradaUsa=0,
RetiradaEnti=0,
AFP_Sit=0,
AFP_Per=0.000000000000000,
Empresa=-11,
CondicaoPG=1,
Moeda=1,
TabelaPrc=1,
MotivoSit=0,
RegrFiscal=1
}
*)


    Pedido := Codigo;
    Abertura   := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    Codigo := UMyMod.BuscaEmLivreY_Def('fatpedcab', 'Codigo', SQLType,
      (*FmFatDivGer2.QrFatPedCabCodigo.Value*) 0);
    CodUsu := DModG.BuscaProximoCodigoInt('controle', 'StqMovUsu', '',
      (*EdCodUsu_Fat.ValueVariant*)0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'fatpedcab', False, [
    'CodUsu', 'Pedido', 'Abertura'
    {
    'Serie', 'NF', 'PIS#Per', 'PIS#Val',
    'COFINS#Per', 'COFINS#Val', 'IR#Per', 'IR#Val', 'CS#Per', 'CS#Val',
    'ISS#Per', 'ISS#Val'
    }
    ], ['Codigo'], [
    CodUsu, Pedido, Abertura
    {Serie, NF, PIS#Per, PIS#Val,
    COFINS#Per, COFINS#Val, IR#Per, IR#Val, CS#Per, CS#Val,
    ISS#Per, ISS#Val
    }
    ], [Codigo], True) then
    begin
      (*
      FmFatDivGer2.LocCod(Codigo, Codigo);
      if (SQLType = stIns) and (FmFatDivGer2.QrFatPedCabCodigo.Value = Codigo) then
        FmFatDivGer2.IncluiNovoVolume();
      *)
      Result     := True;
      _PediVda   := Pedido;
      _FatPedCab := Codigo;
    end;
  end;
end;

function TUnAppPF.LocCItsMat_IncluiPri(const SQLType: TSQLType; const Codigo,
              CtrID, GraGruX, Empresa: Integer; const QtdeLocacao, QtdeProduto,
              ValorLocacao, QtdeLocacaoAntes: Double; const DtHrLocado, Categoria:
              String; const OriSubstItem: Integer; const OriSubstSaiDH: String;
              var Item: Integer): Boolean;
const
  sProcName = 'TUnAppPF.LocCItsMat_IncluiPri()';
var
  DMenos: Integer;
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, ValorBruto: Double;
  // Fazer no Cab?  DtHrLocado: String;
  (*
var
  DtHrRetorn, LibDtHr, RELIB, HOLIB,*)
  //CobrancaRealizadaVenda,
  //COBRANCALOCACAO,
  //Categoria: String;
  // Fazer no Cab? SaiDtHr,
  //CobranIniDtHr: String;
  //Codigo, CtrID, GraGruX,
  //RetFunci, RetExUsr, LibExUsr, LibFunci,}
  //QtdeProduto,
  (*, QtdeDevolucao, Troca,*)
  ManejoLca, GraGruY: Integer;
  ValorProduto(*, CobrancaConsumo*),
  ValorLocAAdiantar, ValorLocAtualizado: Double;
  SdoFuturo, SaldoMudado: Double;
begin
  if SQLType = stIns then
    Item := 0;
  Result := False;
(*&�%$#
  FAlterou     := True;
  FCategoria   := 'L';
  SQLType := ImgTipo.SQLType;
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
  begin
    //if MyObjects.FIC(QrGraGXPatrNO_SITAPL.Value = 0, nil,
    if MyObjects.FIC(NO_SITAPL = 0, nil,
      'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
    then
      Exit;
  end;
*)
  //
(*
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  CtrID          := EdCtrID.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
*)
  UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrGraGXPatr, Dmod.MyDB, [
  'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
  'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,  ',
  'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
  'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
  'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
  'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
  'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
  'cpl.ValorFDS, cpl.DMenos, gg1.Patrimonio, gg1.Nivel2 ',
  '    ',
  'FROM gragrux ggx   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
  'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
  'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
  'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
  'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(Empresa),
  'LEFT JOIN gragruY    ggy ON ggy.Codigo=ggx.GraGruY ',
  'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
  '']);
  //
  if DfModAppGraG1.QrGraGXPatr.RecordCount > 0 then
  begin
    ValorDia       := DfModAppGraG1.QrGraGXPatrValorDia.Value;
    ValorSem       := DfModAppGraG1.QrGraGXPatrValorSem.Value;
    ValorQui       := DfModAppGraG1.QrGraGXPatrValorQui.Value;
    ValorMes       := DfModAppGraG1.QrGraGXPatrValorMes.Value;
    ValorFDS       := DfModAppGraG1.QrGraGXPatrValorFDS.Value;
    DMenos         := DfModAppGraG1.QrGraGXPatrDMenos.Value;
    // Fazer no Cab?
    //DtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  {  Dados somente ap�s inclus�o (Versao AAPA)
    RetFunci       := ;
    RetExUsr       := ;
    DtHrRetorn     := ;
    LibExUsr       := ;
    LibFunci       := ;
    LibDtHr        := ;
    RELIB          := ;
    HOLIB          := ;
  }
    ValorProduto   := DfModAppGraG1.QrGraGXPatrAtualValr.Value;    // Pre�o de venda para indeniza��o
    //QtdeProduto    := EdQtdeProduto.ValueVariant;
    //QtdeLocacao    := Geral.BoolToInt(CkQtdeLocacao.Checked);
    //ValorLocacao   := EdValorLocacao.ValueVariant;
    {
    CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
    (*const*) Saida, Volta, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
    ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao,
    ValDevolParci, Troca, (*var*) Log, (*var*)IniCobranca, (*var*)DiasFDS,
    DiasUteis, (*var*)CalcAdiLOCACAO,
    ValorUnitario, ValorLocAAdiantar, Calculou);
    }
    //
    if OriSubstItem = 0 then
      ValorLocAAdiantar  := QtdeProduto * QtdeLocacao * ValorLocacao
    else
      ValorLocAAdiantar  := 0;
    //
    ValorLocAtualizado := ValorLocAAdiantar;
    ValorBruto         := ValorLocAAdiantar;
    //QtdeDevolucao  := ;
    //Troca          := ;
    //Categoria      := 'L'; // Loca��o
    //CobrancaConsumo:= ; ??? valor quando definido igual ao ValorLocacao
  (*
   SELECT its.CATEGORIA,
    CASE
    WHEN VALORPRODUTO = 0 THEN "VP=0"
    WHEN VALORPRODUTO=COBRANCACONSUMO THEN "VP=CC"
    ELSE "???" END VPxCC,
  COUNT(its.PRODUTO) ITENS
  FROM locacaoitens its
  WHERE its.COBRANCACONSUMO <> 0
  GROUP BY CATEGORIA, VPxCC
  *)
    //CobrancaRealizadaVenda:= ; "S" quando cobrou venda!
    // Fazer no Cab? SaiDtHr        := Geral.FDT(Trunc(TPSaiDtHr.Date), 1) + ' ' + EdSaiDtHr.Text; // := Geral.FDT(FDtHrSai, 109);
    //COBRANCALOCACAO:= '2 M - 1 Q - 1 S - 1 D';
    // Fazer no Cab? CobranIniDtHr  := Geral.FDT(Trunc(TPCobranIniDtHr.Date), 1) + ' ' + EdCobranIniDtHr.Text; Fazer calculo de DMenos e Sabado + feriados!
    //
  (*
    if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
      Exit;
    if MyObjects.FIC(QtdeProduto <= 0, EdQtdeProduto, 'Informe a quantidade!') then
      Exit;
  *)
    if Dmod.QrOpcoesTRenPermLocSemEstq.Value = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrEstq, Dmod.MyDB, [
      'SELECT EstqSdo  ',
      'FROM GraGruEPat ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      SdoFuturo := DfModAppGraG1.QrEstqEstqSdo.Value + QtdeLocacaoAntes - (QtdeLocacao * QtdeProduto);
      //
      if SdoFuturo < 0 then
      begin
        Geral.MB_Info('Saldo insuficiente para loca��o!');
        SaldoMudado := DfModAppGraG1.QrGraGXPatrEstqSdo.Value - DfModAppGraG1.QrEstqEstqSdo.Value;
        if SaldoMudado <> 0 then
        begin
          Geral.MB_Info(
          'ATEN��O: o saldo deste patrim�nio mudou durante esta inclus�o!' +
          'Outro usu�rio pode ter liberado ou locada o item!' +
          sLineBreak + 'Diferen�a: ' + Geral.FFT(SaldoMudado, 2, siNegativo));
        end;
        Exit;
      end;
    end;
    //
    GraGruY   := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
    //
    //CtrID := FCtrID; //UMyMod.BPGS1I32('loccitslca', 'CtrID', '', '', tsPos, SQLType, CtrID);
    Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, SQLType, Item);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
      'Codigo', 'GraGruX', 'ValorDia',
      'ValorSem', 'ValorQui', 'ValorMes',
      // Fazer no Cab?
      'DtHrLocado',
      'ValorProduto', 'QtdeLocacao', 'ValorLocacao',
      // Fazer no Cab? 'SaiDtHr',
      //'CobranIniDtHr',
      'Categoria', 'QtdeProduto', 'ManejoLca',
      'CtrID', 'ValorFDS', 'DMenos',
      'ValorLocAAdiantar', 'ValorLocAtualizado', 'ValorBruto',
      'OriSubstItem', 'OriSubstSaiDH'
    ], [
      'Item'
    ], [
      Codigo, GraGruX, ValorDia,
      ValorSem, ValorQui, ValorMes,
      // Fazer no Cab?
      DtHrLocado,
      ValorProduto, QtdeLocacao , ValorLocacao,
      // Fazer no Cab? SaiDtHr,
      //CobranIniDtHr,
      Categoria, QtdeProduto, ManejoLca,
      CtrID, ValorFDS, DMenos,
      ValorLocAAdiantar, ValorLocAtualizado, ValorBruto,
      OriSubstItem, OriSubstSaiDH
    ], [
      Item], True);
    AppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa, True);
  end else
    Geral.MB_Erro('N�o foi poss�vel localizar o reduzido ' +
    Geral.FF0(GraGruX) +  '!' + sLineBreak + sProcName);
end;

function TUnAppPF.LocCItsMat_IncluiSec(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, Empresa: Integer; const QtdeLocacao, QtdeProduto: Double;
              const TipoAluguel, DtHrLOcado: String; const QrLocIPatSec:
              TmySQLQuery; const OriSubstItem: Integer; const OriSubstSaiDH:
              String; var Item: Integer): Boolean;
var
  ManejoLca, GraGruY, DMenos: Integer;
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS: Double;
  Categoria: String;
  ValorProduto, ValorLocacao: Double;
  SdoFuturo, SaldoMudado: Double;
begin
  Result := False;
  if SQLType = stIns then
    Item := 0;
(*&�%$
  if Dmod.QrOpcoesTRenBloPrdSPer.Value = 1 then
  begin
    if MyObjects.FIC(QrGraGXTollNO_SITAPL.Value = 0, nil,
      'A situa��o atual deste patrim�nio n�o permite sua loca��o!')
    then
      Exit;
  end;
*)
  //
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrGraGXComEstq, Dmod.MyDB, [
  'SELECT gg1.Nome, ggx.GraGruY, ptr.*',
  'FROM gragxpatr ptr ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ptr.GraGruX',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE ptr.GraGruX=' + Geral.FF0(GraGruX),
  'AND ggx.Ativo=1 ',
  '']);
  //
  if DfModAppGraG1.QrGraGXComEstq.RecordCount > 0 then
  begin
    //GraGruX        := QrGraGXComEstqGraGruX.Value;
    ValorDia       := DfModAppGraG1.QrGraGXComEstqValorDia.Value;
    ValorSem       := DfModAppGraG1.QrGraGXComEstqValorSem.Value;
    ValorQui       := DfModAppGraG1.QrGraGXComEstqValorQui.Value;
    ValorMes       := DfModAppGraG1.QrGraGXComEstqValorMes.Value;
    ValorFDS       := DfModAppGraG1.QrGraGXComEstqValorFDS.Value;
    DMenos         := DfModAppGraG1.QrGraGXComEstqDMenos.Value;
    // Fazer no Cab? DtHrLocado     := Geral.FDT(Trunc(TPDataLoc.Date), 1) + ' ' + EdHoraLoc.Text;
  {  Dados somente ap�s inclus�o (Versao AAPA)
    RetFunci       := ;
    RetExUsr       := ;
    DtHrRetorn     := ;
    LibExUsr       := ;
    LibFunci       := ;
    LibDtHr        := ;
    RELIB          := ;
    HOLIB          := ;
  }
    ValorProduto   := DfModAppGraG1.QrGraGXComEstqAtualValr.Value;    // Pre�o de venda para indeniza��o
    if TipoAluguel = 'D' then
      ValorLocacao := ValorDia
    else
    if TipoAluguel = 'S' then
      ValorLocacao := ValorSem
    else
    if TipoAluguel = 'Q' then
      ValorLocacao := ValorQui
    else
    if TipoAluguel = 'M' then
      ValorLocacao := ValorMes
    else
    begin
      Geral.MB_Erro('"Tipo de Aluguel" n�o definido! (2048)');
      ValorLocacao := 0;
    end;

    //QtdeDevolucao  := ;
    //Troca          := ;
    Categoria      := 'L'; // Loca��o
    //CobrancaConsumo:= ; ??? valor quando definido igual ao ValorLocacao
  (*
   SELECT its.CATEGORIA,
    CASE
    WHEN VALORPRODUTO = 0 THEN "VP=0"
    WHEN VALORPRODUTO=COBRANCACONSUMO THEN "VP=CC"
    ELSE "???" END VPxCC,
  COUNT(its.PRODUTO) ITENS
  FROM locacaoitens its
  WHERE its.COBRANCACONSUMO <> 0
  GROUP BY CATEGORIA, VPxCC
  *)
    //CobrancaRealizadaVenda:= ; "S" quando cobrou venda!
    // Fazer no Cab? SaiDtHr        := Geral.FDT(Trunc(TPSaiDtHr.Date), 1) + ' ' + EdSaiDtHr.Text; // := Geral.FDT(FDtHrSai, 109);
    //COBRANCALOCACAO:= '2 M - 1 Q - 1 S - 1 D';
    // Fazer no Cab? CobranIniDtHr  := Geral.FDT(Trunc(TPCobranIniDtHr.Date), 1) + ' ' + EdCobranIniDtHr.Text; ???
    //
(*
    if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe um patrim�nio principal!') then
      Exit;
*)
    if Dmod.QrOpcoesTRenPermLocSemEstq.Value = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrEstq, Dmod.MyDB, [
      'SELECT EstqSdo  ',
      'FROM GraGruEPat ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      SdoFuturo := DfModAppGraG1.QrEstqEstqSdo.Value - (QtdeLocacao * QtdeProduto);
      //
      if SdoFuturo < 0 then
      begin
        Geral.MB_Info('Saldo insuficiente para loca��o! Produto ' + Geral.FF0(GraGruX));
        //Exit;  impedir na confirma��o?
      end;
    end;
    //
    GraGruY        := DmProd.ObtemGraGruYDeGraGruX(GraGruX);
    ManejoLca      := AppPF.ObtemManejoLocaDeGraGruY(GraGruY);
    //
    //CtrID := UMyMod.BPGS1I32('loccitslca', 'CtrID', '', '', tsPos, SQLType, CtrID);
    Item := UMyMod.BPGS1I32('loccitslca', 'Item', '', '', tsPos, SQLType, CtrID);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
      'Codigo', 'GraGruX', 'ValorDia',
      'ValorSem', 'ValorQui', 'ValorMes',
      // Fazer no Cab?
      'DtHrLocado',
      'ValorProduto', 'QtdeLocacao', 'ValorLocacao',
      // Fazer no Cab? 'SaiDtHr',
      //'CobranIniDtHr',
      'Categoria', 'QtdeProduto', 'ManejoLca',
      'CtrID', 'ValorFDS', 'DMenos',
      'OriSubstItem', 'OriSubstSaiDH'
    ], [
    'Item'], [
      Codigo, GraGruX, ValorDia,
      ValorSem, ValorQui, ValorMes,
      // Fazer no Cab?
      DtHrLocado,
      ValorProduto, QtdeLocacao , ValorLocacao,
      // Fazer no Cab? SaiDtHr,
      //CobranIniDtHr,
      Categoria, QtdeProduto, ManejoLca,
      CtrID, ValorFDS, DMenos,
      OriSubstItem, OriSubstSaiDH
    ], [
    Item], True) then
    begin
      Result := True;
      AppPF.AtualizaEstoqueGraGXPatr(GraGruX, Empresa, True);
      if QrLocIPatSec <> nil then
      begin
        UnDmkDAC_PF.AbreQuery(QrLocIPatSec, Dmod.MyDB);
        QrLocIPatSec.Locate('Item', Item, []);
      end;
    end;
  end else
    Geral.MB_Erro('Patrim�nio secund�rio n�o localizado!');
end;

function TUnAppPF.LocCItsMat_IncluiUso(const SQLType: TSQLType; const Codigo, CtrID,
              GraGruX, ManejoLca, Unidade: Integer; const PrcUni, QtdeLocacao,
              QtdeProduto, ValorProduto: Double; const DtHrLocado, Categoria:
              String; const QrLocIPatUso: TmySQLQuery; var Item: Integer): Boolean;
var
  AvalIni: Integer;
  AvalFim, ValUso: Double;
begin
  Result := False;
  Item := 0;
  //Unidade        := QrGraGXTollItemUnid.Value;
  AvalIni        := 0;
  AvalFim        := 0;
  //PrcUni         := QrGraGXTollItemValr.Value;
  ValUso         := 0;
  //QtdeProduto    := EdQtdeProduto.ValueVariant;
  //QtdeLocacao    := EdQtdeLocacao.ValueVariant;
  //ValorProduto   := QrGraGXTollItemValr.Value;
  //
  Item := UMyMod.BPGS1I32('loccitsruc', 'Item', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccitsruc', False, [
    'Codigo', 'CtrID', 'GraGruX',

    'Categoria', 'QtdeProduto', 'ManejoLca',

    'Unidade', 'AvalIni', 'AvalFim',
    'PrcUni', 'ValUso', 'QtdeLocacao',
    'ValorProduto'], [
    'Item'], [
    Codigo, CtrID, GraGruX,

    Categoria, QtdeProduto, ManejoLca,

    Unidade, AvalIni, AvalFim,
    PrcUni, ValUso, QtdeLocacao,
    ValorProduto], [
    Item], True)
  then
  begin
    Result := True;
    if QrLocIPatUso <> nil then
      FmLocCConCab.ReopenLocIPatUso(QrLocIPatUso, Codigo, CtrID, Item);
  end;
end;


function TUnAppPF.LocCItsRet_DadosDevolucaoOK(EdQuantidade: TdmkEdit;
  QuantidadeAlt, QuantidadeLocada, QuantidadeJaDevolvida: Double): Boolean;
begin
  Result := False;
  if MyObjects.FIC(EdQuantidade.ValueVariant = 0, EdQuantidade,
    'Informe a quantidade de devolu��o') then Exit;
  //
  if EdQuantidade.ValueVariant > (QuantidadeAlt + QuantidadeLocada - QuantidadeJaDevolvida) then
  begin
    if Geral.MB_Pergunta(
    'Quantidade de devolu��o maior que o poss�vel! ' + sLineBreak +
    'Ser� necess�rio Senha BOSS!' + sLineBreak +
    'Deseja continuar assim mesmo?') = ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaPwdLibFunc() then
        Exit;
    end else
      Exit;
  end;
  Result := True;
end;

function TUnAppPF.LocCItsRet_InsereLocCMovAll(const SQLType: TSQLType; const
  DataHora, USUARIO, TipoES, COBRANCALOCACAO, LIMPO, SUJO, QUEBRADO,
  TESTADODEVOLUCAO, ObservacaoTroca: String; const Codigo, ManejoLca, CtrID,
  Item, SEQUENCIA, GraGruX, Quantidade, TipoMotiv, MotivoTroca,
  QuantidadeLocada, QuantidadeJaDevolvida: Integer; const VALORLOCACAO,
  GGXEntrada: Double; const SubstitutoLca: Integer; var Controle, GTRTab: Integer): Boolean;
begin
  case TManejoLca(ManejoLca) of
    TManejoLca.manelcaPrincipal,
    TManejoLca.manelcaSecundario,
    TManejoLca.manelcaAcessorio: GTRTab := Integer(TGraToolRent.gbsLocar);
    TManejoLca.manelcaApoio,
    TManejoLca.manelcaUso,
    TManejoLca.manelcaConsumo: GTRTab := Integer(TGraToolRent.gbsOutrs);
  end;
  Controle := UMyMod.BPGS1I32('loccmovall', 'Controle', '', '', tsPos, SQLType, Controle);
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccmovall', False, [
  'Codigo', 'GTRTab', 'CtrID',
  'Item', 'SEQUENCIA', 'DataHora',
  'USUARIO', 'GraGruX', 'TipoES',
  'Quantidade', 'TipoMotiv', 'COBRANCALOCACAO',
  'VALORLOCACAO', 'LIMPO', 'SUJO',
  'QUEBRADO', 'TESTADODEVOLUCAO', 'GGXEntrada',
  'MotivoTroca', 'ObservacaoTroca', 'QuantidadeLocada',
  'QuantidadeJaDevolvida', 'SubstitutoLca'], [
  'Controle'], [
  Codigo, GTRTab, CtrID,
  Item, SEQUENCIA, DataHora,
  USUARIO, GraGruX, TipoES,
  Quantidade, TipoMotiv, COBRANCALOCACAO,
  VALORLOCACAO, LIMPO, SUJO,
  QUEBRADO, TESTADODEVOLUCAO, GGXEntrada,
  MotivoTroca, ObservacaoTroca, QuantidadeLocada,
  QuantidadeJaDevolvida, SubstitutoLca], [
  Controle], True);
end;

procedure TUnAppPF.LocCItsRet_PesquisaPorGraGruX(EdGraGruX: TdmkEditCB;
  CBGraGruX: TdmkDBLookupComboBox; EdPatrimonio, EdReferencia: TdmkEdit;
  Tipo: TGraGXToolRnt; Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrPesqGPR2, Dmod.MyDB, [
  'SELECT gg1.Referencia, gg1.Patrimonio  ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + ObtemTabelaGraGXTollRnt(Tipo) + ' ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  ggo.GraGruX=' + Geral.FF0(EdGraGruX.ValueVariant),
  'AND NOT (ggo.GraGruX IS NULL) ',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  if DfModAppGraG1.QrPesqGPR2.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> DfModAppGraG1.QrPesqGPR2Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := DfModAppGraG1.QrPesqGPR2Patrimonio.Value;
    end;
    if EdReferencia.ValueVariant <> DfModAppGraG1.QrPesqGPR2Referencia.Value then
    begin
      EdReferencia.ValueVariant := DfModAppGraG1.QrPesqGPR2Referencia.Value;
    end;
  end else
  if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
end;

procedure TUnAppPF.LocCItsRet_PesquisaPorPatrimonio(EdGraGruX: TdmkEditCB;
  CBGraGruX: TdmkDBLookupComboBox; EdPatrimonio, EdReferencia: TdmkEdit;
  Tipo: TGraGXToolRnt; Limpa: Boolean);
var
  Reabre: Boolean;
begin
  Reabre := False;
  UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrPesqGPR1, Dmod.MyDB, [
  'SELECT ggx.Controle, gg1.Referencia, gg1.Patrimonio ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragxpatr cpl ON cpl.GraGruX=ggx.Controle ',
  'WHERE  gg1.Patrimonio="' + EdPatrimonio.Text + '"' ,
  'AND NOT (cpl.GraGruX IS NULL)',
  '']);
  DfModAppGraG1.QrPesqGPR1.Open;
  if DfModAppGraG1.QrPesqGPR1.RecordCount > 0 then
  begin
    if EdGraGruX.ValueVariant <> DfModAppGraG1.QrPesqGPR1Controle.Value then
    begin
      EdGraGruX.ValueVariant := DfModAppGraG1.QrPesqGPR1Controle.Value;
      Reabre := True;
    end;
    if EdReferencia.ValueVariant <> DfModAppGraG1.QrPesqGPR1Referencia.Value then
    begin
      EdReferencia.ValueVariant := DfModAppGraG1.QrPesqGPR1Referencia.Value;
      Reabre := True;
    end;
    //  Precisa ?
    if EdGraGruX.ValueVariant <> DfModAppGraG1.QrPesqGPR1Controle.Value then
      EdGraGruX.ValueVariant := DfModAppGraG1.QrPesqGPR1Controle.Value;
    if CBGraGruX.KeyValue <> DfModAppGraG1.QrPesqGPR1Controle.Value then
      CBGraGruX.KeyValue := DfModAppGraG1.QrPesqGPR1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
end;

procedure TUnAppPF.LocCItsRet_PesquisaPorReferencia(EdGraGruX: TdmkEditCB;
  CBGraGruX: TdmkDBLookupComboBox; EdPatrimonio, EdReferencia: TdmkEdit;
  Tipo: TGraGXToolRnt; Limpa: Boolean);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DfModAppGraG1.QrPesqGPR1, Dmod.MyDB, [
  'SELECT ggx.Controle ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + ObtemTabelaGraGXTollRnt(Tipo) + ' ggo ON ggo.GraGruX=ggx.Controle ',
  'WHERE  gg1.Referencia="' + EdReferencia.Text + '"' ,
  'AND NOT (ggo.GraGruX IS NULL)',
  'AND ggo.Aplicacao <> 0 ',
  //
  'AND ggx.Ativo=1 ',
  '']);
  DfModAppGraG1.QrPesqGPR1.Open;
  if DfModAppGraG1.QrPesqGPR1.RecordCount > 0 then
  begin
    if EdPatrimonio.ValueVariant <> DfModAppGraG1.QrPesqGPR1Patrimonio.Value then
    begin
      EdPatrimonio.ValueVariant := DfModAppGraG1.QrPesqGPR1Patrimonio.Value;
(*&�%$
      Reabre := True;
*)
    end;
    if EdGraGruX.ValueVariant <> DfModAppGraG1.QrPesqGPR1Controle.Value then
    begin
      EdGraGruX.ValueVariant := DfModAppGraG1.QrPesqGPR1Controle.Value;
    end;
    if CBGraGruX.KeyValue <> DfModAppGraG1.QrPesqGPR1Controle.Value then
      CBGraGruX.KeyValue := DfModAppGraG1.QrPesqGPR1Controle.Value;
  end else if Limpa then
  begin
    EdPatrimonio.ValueVariant := '';
    EdReferencia.ValueVariant := '';
  end;
end;

procedure TUnAppPF.LocCItsRet_ReopenItsLca(QrItsLca: TmySQLQuery; BtPula:
  TBitBtn; Item, Codigo, CtrID: Integer);
begin
  if Item = 0 then
  begin
    if BtPula <> nil then
      BtPula.Visible := True;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrItsLca, Dmod.MyDB, [
    'SELECT gg1.Patrimonio, lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
    'FROM loccitslca lpp ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
    'LEFT JOIN gragxpatr pat ON pat.GraGruX=ggx.Controle ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
    'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
    'AND lpp.CtrID=' + Geral.FF0(CtrID),
    'AND QtdeLocacao>0 ',
    'AND QtdeProduto>0',
    'AND QtdeDevolucao<(QtdeProduto*QtdeLocacao)',
    'AND Troca=0',
    'ORDER BY lpp.ManejoLca, lpp.Item',
    '']);
  end else
  begin
    if BtPula <> nil then
      BtPula.Visible := False;
    UnDmkDAC_PF.AbreMySQLQuery0(QrItsLca, Dmod.MyDB, [
    'SELECT gg1.Patrimonio, lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
    'FROM loccitslca lpp ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
    'LEFT JOIN gragxpatr pat ON pat.GraGruX=ggx.Controle ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
    'WHERE lpp.Codigo=' + Geral.FF0(Codigo),
    'AND lpp.CtrID=' + Geral.FF0(CtrID),
    'AND Item=' + Geral.FF0(Item),
    '']);
  end;
  //
  QrItsLca.First;
end;

procedure TUnAppPF.LocCItsRet_ReopnGraGXPatr(QrGraGXPatr: TmySQLQuery; Empresa,
  GraGruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
  'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, ',
  'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1, ',
  'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,  ',
  'cpl.Situacao, cpl.AtualValr, cpl.ValorFDS, cpl.ValorMes, ',
  'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia, cpl.ValorFDS, ',
  'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie, ',
  'cpl.Voltagem, cpl.Potencia, cpl.Capacid, gg1.Nivel2 ',
  '   ',
  'FROM gragrux ggx  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle  ',
  'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle  ',
  'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo  ',
  'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao',
  'LEFT JOIN gragruepat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(Empresa),
  'WHERE cpl.GraGruX=' + Geral.FF0(GraGruX),
  '']);
  //
end;

procedure TUnAppPF.MercadoriaDeVenda(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXVend(0);
end;

procedure TUnAppPF.MostraFormXXXxxXxxGraGruY(GraGruY, GraGruX: Integer;
  Edita: Boolean; Qry: TmySQLQuery);
const
  sProcName = 'TUnAppPF.MostraFormXXXxxXxxGraGruY()';
begin
{
  case GraGruY of
(*
    CO_GraGruY_0512_TXSubPrd: VS_PF.MostraFormVSSubPrd(GraGruX, True);
    CO_GraGruY_0683_TXPSPPro: VS_PF.MostraFormVSPSPPro(GraGruX, True);
    CO_GraGruY_0853_TXPSPEnd: VS_PF.MostraFormVSPSPEnd(GraGruX, True, Qry);
*)
    CO_GraGruY_1024_TXCadNat: TX_Jan.MostraFormTXCadNat(GraGruX, True, Qry);
(*
    CO_GraGruY_1195_TXNatPDA: VS_PF.MostraFormVSNatPDA(GraGruX, True);
    CO_GraGruY_1365_TXProCal: VS_PF.MostraFormVSProCal(GraGruX, True);
    CO_GraGruY_1536_TXCouCal: VS_PF.MostraFormVSCouCal(GraGruX, True);
    CO_GraGruY_1621_TXCouDTA: VS_PF.MostraFormVSCouDTA(GraGruX, True);
    CO_GraGruY_1707_TXProCur: VS_PF.MostraFormVSProCur(GraGruX, True);
    CO_GraGruY_1877_TXCouCur: VS_PF.MostraFormVSCouCur(GraGruX, True);
*)
    CO_GraGruY_2048_TXCadOpe: TX_Jan.MostraFormTXCadOpe(GraGruX, True, Qry);
(*
    CO_GraGruY_3072_TXRibCla: VS_PF.MostraFormVSRibCla(GraGruX, True, Qry);
    CO_GraGruY_4096_TXRibOpe: VS_PF.MostraFormVSRibOpe(GraGruX, True);
    CO_GraGruY_5120_TXWetEnd: VS_PF.MostraFormVSWetEnd(GraGruX, True, Qry(*, MultiplosGGX*));
*)
    CO_GraGruY_6144_TXCadFcc: TX_Jan.MostraFormTXCadFcc(GraGruX, True, Qry(*, MultiplosGGX*));
    else
}
    Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
    ' n�o est� implementado em ' + sProcName);
  //end;
end;

procedure TUnAppPF.MostraOrigemFat(FatParcRef: Integer);
begin
  if FmLocCConCab <> nil then
  begin
    if TFmLocCConCab(FmLocCConCab).ImgTipo.SQLType <> stLok then
    begin
      Geral.MB_Aviso(
      'A janela de Gerenciamento de Loca��o n�o est� em modo travado!' +
      'Finalize a Edi��o ou Inclus�o de Loca��o!');
      //
      Exit;
    end;
  end;
  //
  FmPrincipal.AdvToolBarPagerNovo.Visible := False;
  GraL_Jan.MostraFormLocCCon(True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo, FatParcRef);
end;

procedure TUnAppPF.MostraOrigemFatGenerico(FatID: Integer; FatNum: Double;
  FatParcela, FatParcRef: Integer);
begin
    // compatibilidade
end;

procedure TUnAppPF.MostraPopupDeCadsatroEspecificoDeGraGruXY(Form: TForm; Sb:
  TSpeedButton; PM: TPopupMenu);
var
  //PM: TPopupMenu;
  Item: TMenuItem;
begin
  //if _PM <> nil then PM := _PM else
  begin
    //PM := TPopupMenu(Form);
    PM.Items.Clear;
    //
    Item := TmenuItem.Create(PM);
    Item.Caption := '&Equipamento Principal/Secund�rio/Acess�rio';
    Item.OnClick := EquipamentoComEstoque;
    PM.Items.add(Item);
    //
    Item := TmenuItem.Create(PM);
    Item.Caption := '&Equipamento de Apoio/de Uso/ de Consumo';
    Item.OnClick := EquipamentoSemEstoque;
    PM.Items.add(Item);
    //
    Item := TmenuItem.Create(Form);
    Item.Caption := '&Servi�o';
    //Item.Enabled := False;
    Item.OnClick := Servico;
    PM.Items.add(Item);
    //
    Item := TmenuItem.Create(Form);
    Item.Caption := '&Mercadoria de venda';
    Item.OnClick := MercadoriaDeVenda;
    PM.Items.add(Item);
  end;
  //
  MyObjects.MostraPopUpDeBotao(PM, Sb);
end;

function TUnAppPF.MudaStatus_Finalizado(Codigo: Integer; QrItsLca: TmySQLQuery;
  DataBaixa: TDateTime; ValorPndt: Double): Boolean;
const
  AvisaEstqNeg = True;
  GeraLog = False;
  SQL_CtrID = '';
  StatusLocacao = TStatusLocacao.statlocFinalizado;
var
  Status, GraGruX: Integer;
  SQLType: TSQLType;
  DtHrBxa: String;
begin
  Result := False;
  if ValorPndt > 0 then
  begin
    Geral.MB_Aviso(
    'Antes de mudar o status para "Finalizado" � necess�rio faturar $ ' +
    Geral.FFT(ValorPndt, 2, siNegativo));
    //
    Exit;
  end else
  begin
    SQLType           := stUpd;
    Status            := Integer(StatusLocacao);
    DtHrBxa           := Geral.FDT(DataBaixa, 109);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccconcab', False, [
    'Status', 'DtHrBxa'], ['Codigo'], [
    Status, DtHrBxa], [Codigo], True) then
    begin
      (*
      AppPF.RecalculaItens_Datas(QrItsLca, Codigo, Status, Null,
      QrLocCConCabDtHrEmi.Value, QrLocCConCabDtHrSai.Value,
      QrLocCConCabDtHrDevolver.Value, GeraLog);
      *)
      AppPF.ReopenItsLca(QrItsLca, Codigo, SQL_CtrID);
      //RecalculaEstoques(); J� racalculou ao devolver!
      Result := True;
    end;
  end;
end;

function TUnAppPF.NFeJaLancada(ChaveNFe: String): Boolean;
var
  Qry: TmySQLQuery;
  Entrada: Integer;
begin
  Result := False;
  if ChaveNFe = EmptyStr then Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM stqinncad',
    'WHERE nfe_Id="' + Geral.SoNumero_TT(ChaveNFe) + '"',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Entrada := Qry.Fields[0].AsInteger;
      Result := Entrada <> 0;
      if Result then
        Geral.MB_Aviso('Esta NFe j� foi lan�ada na entrada por compra n� '
        + Geral.FF0(Entrada));
    end;
  finally
    Qry.Free;
  end;
end;

function TUnAppPF.ObtemManejoLocaDeGraGruY(GraGruY: Integer): Integer;
const
  sProcName = 'TUnAppPF.ObtemManejoLocaDeGraGruY(GraGruY)';
begin
  case GraGruY of
    CO_GraGruY_1024_GXPatPri: Result := Integer(TManejoLca.manelcaPrincipal);
    CO_GraGruY_2048_GXPatSec: Result := Integer(TManejoLca.manelcaSecundario);
    CO_GraGruY_3072_GXPatAce: Result := Integer(TManejoLca.manelcaAcessorio);
    CO_GraGruY_4096_GXPatApo: Result := Integer(TManejoLca.manelcaApoio);
    CO_GraGruY_5120_GXPatUso: Result := Integer(TManejoLca.manelcaUso);
    CO_GraGruY_6144_GXPrdCns: Result := Integer(TManejoLca.manelcaConsumo);
    CO_GraGruY_7168_GXPatSrv: Result := Integer(TManejoLca.manelcaServico);
    CO_GraGruY_8192_GXPrdVen: Result := Integer(TManejoLca.manelcaVenda);
    else
    begin
      Geral.MB_Erro('GraGruY n�o implementado em ' + sProcName);
      Result := 0;
    end;
  end;
end;

function TUnAppPF.ObtemNomeTabelaGraGruY(GraGruY: Integer): String;
const
  sProcName = 'TUnAppPF.ObtemNomeTabelaGraGruY()';
begin
  Geral.MB_Info('Ver com a Dermatek: ' + sProcName);
{
(*
  case GraGruY of
    -1: Result := '';
(*
    CO_GraGruY_0512_TXSubPrd: Result := LowerCase('TXSubPrd');
    CO_GraGruY_0683_TXPSPPro: Result := LowerCase('TXPSPPro');
    CO_GraGruY_0853_TXPSPEnd: Result := LowerCase('TXPSPEnd');
*)
    CO_GraGruY_1024_TXCadNat: Result := LowerCase('TXCadNat');
(*
    CO_GraGruY_1195_TXNatPDA: Result := LowerCase('TXNatpda');
    CO_GraGruY_1365_TXProCal: Result := LowerCase('TXProCal');
    CO_GraGruY_1536_TXCouCal: Result := LowerCase('TXCouCal');
    CO_GraGruY_1621_TXCouDTA: Result := LowerCase('TXCouDTA');
    CO_GraGruY_1707_TXProCur: Result := LowerCase('TXProCur');
    CO_GraGruY_1877_TXCouCur: Result := LowerCase('TXCouCur');
*)
    CO_GraGruY_2048_TXCadOpe: Result := LowerCase('TXCadOpe');
(*
    CO_GraGruY_3072_TXRibCla: Result := LowerCase('TXRibCla');
    CO_GraGruY_4096_TXRibOpe: Result := LowerCase('TXRibOpe');
    CO_GraGruY_5120_TXWetEnd: Result := LowerCase('TXWetEnd');
*)
    CO_GraGruY_6144_TXCadFcc: Result := LowerCase('TXCadFcc');
    else
    begin
      Result := '';
      Geral.MB_Erro('O grupo de estoque n�mero ' + Geral.FF0(GraGruY) +
      ' n�o est� implementado em ' + sProcName);
    end;
  end;
}
end;

function TUnAppPF.ObtemTabelaDeGraGruY_Cadastro(const GraGruY: Integer;
  var Tabela: String): Boolean;
const
  sProcName = 'TUnAppPF.ObtemTabelaDeGraGruY_Cadastro()';
begin
  Result := True;
  case GraGruY of
    CO_GraGruY_1024_GXPatPri: Tabela := 'gragxpatr'; // Patrim�nio Principal
    CO_GraGruY_2048_GXPatSec: Tabela := 'gragxpatr'; // Patrim�nio Secund�rio
    CO_GraGruY_3072_GXPatAce: Tabela := 'gragxpatr'; // Acess�rio
    CO_GraGruY_4096_GXPatApo: Tabela := 'gragxoutr'; // Apoio
    CO_GraGruY_5120_GXPatUso: Tabela := 'gragxoutr'; // Uso
    CO_GraGruY_6144_GXPrdCns: Tabela := 'gragxoutr'; // Consumo
    CO_GraGruY_7168_GXPatSrv: Tabela := 'gragxserv'; // Servi�o
    CO_GraGruY_8192_GXPrdVen: Tabela := 'gragxvend'; // Produto de Venda
    else
    begin
      Result := False;
      Geral.MB_Erro('GraGruY n�o implementado em ' + sProcName);
      Tabela := 'gragx????';
    end;
  end;
end;

function TUnAppPF.ObtemTabelaDeGraGruY_Movimento(const GraGruY: Integer; var
  Tabela: String): Boolean;
const
  sProcName = 'TUnAppPF.ObtemTabelaDeGraGruY_Movimento()';
begin
  Result := True;
  case GraGruY of
    CO_GraGruY_1024_GXPatPri: Tabela := 'loccitslca'; // Patrim�nio Principal
    CO_GraGruY_2048_GXPatSec: Tabela := 'loccitslca'; // Patrim�nio Secund�rio
    CO_GraGruY_3072_GXPatAce: Tabela := 'loccitslca'; // Acess�rio
    CO_GraGruY_4096_GXPatApo: Tabela := 'loccitsruc'; // Apoio
    CO_GraGruY_5120_GXPatUso: Tabela := 'loccitsruc'; // Uso
    CO_GraGruY_6144_GXPrdCns: Tabela := 'loccitsruc'; // Consumo
    CO_GraGruY_7168_GXPatSrv: Tabela := 'loccitssvc'; // Servi�o
    CO_GraGruY_8192_GXPrdVen: Tabela := 'loccitsven'; // Produto de Venda
    else
    begin
      Result := False;
      Geral.MB_Erro('GraGruY n�o implementado em ' + sProcName);
      Tabela := 'loccits???';
    end;
  end;
end;

function TUnAppPF.ObtemTabelaGraGXTollRnt(Tipo: TGraGXToolRnt): String;
begin
  case Tipo of
    TGraGXToolRnt.ggxo2gPrincipal,
    TGraGXToolRnt.ggxo2gSecundario,
    TGraGXToolRnt.ggxo2gAcessorio:
      Result := 'gragxpatr';
    TGraGXToolRnt.ggxo2gApoio,
    TGraGXToolRnt.ggxo2gUso,
    TGraGXToolRnt.ggxo2gConsumo:
      Result := 'gragxoutr';
    else Result := '?????'
  end;
end;

function TUnAppPF.ObtemTipoDeTGraGXOutr(Tipo: TGraGXToolRnt): Integer;
begin
  case Tipo of
    TGraGXToolRnt.ggxo1gAcessorio:  Result := 1;
    TGraGXToolRnt.ggxo1gUso:        Result := 2;
    TGraGXToolRnt.ggxo1gConsumo:    Result := 3;
    TGraGXToolRnt.ggxo2gPrincipal:  Result := 1;
    TGraGXToolRnt.ggxo2gSecundario: Result := 2;
    TGraGXToolRnt.ggxo2gAcessorio:  Result := 3;
    TGraGXToolRnt.ggxo2gApoio:      Result := 1;
    TGraGXToolRnt.ggxo2gUso:        Result := 2;
    TGraGXToolRnt.ggxo2gConsumo:    Result := 3;
    else Result := -99999999
  end;
end;

function TUnAppPF.ObtemValorProdutoDeGraGruXEGraGruY(
  const GraGruX, GraGruY: Integer; var ValorProduto: Double): Boolean;
const
  sProcName = 'TUnAppPF.ObtemValorProdutoDeGraGruXEGraGruY()';
var
  Tabela: String;
begin
  ValorProduto := 0;
  Result := True;
  if AppPF.ObtemTabelaDeGraGruY_Cadastro(GraGruY, Tabela) then
  begin
    case GraGruY of
      CO_GraGruY_1024_GXPatPri,
      CO_GraGruY_2048_GXPatSec,
      CO_GraGruY_3072_GXPatAce:
      begin
        UnDmkDAC_PF.AbreMySQLDirectQuery0(Dmod.DqAux, Dmod.MyDB, [
        'SELECT AtualValr',
        'FROM ' + Tabela,
        'WHERE GraGruX=' + Geral.FF0(GraGruX),
        '']);
        //
        ValorProduto := USQLDB.Flu(Dmod.DqAux, 'AtualValr');
      end;
      CO_GraGruY_4096_GXPatApo,
      CO_GraGruY_5120_GXPatUso,
      CO_GraGruY_6144_GXPrdCns,
      CO_GraGruY_7168_GXPatSrv,
      CO_GraGruY_8192_GXPrdVen:
      begin
        UnDmkDAC_PF.AbreMySQLDirectQuery0(Dmod.DqAux, Dmod.MyDB, [
        'SELECT ItemValr ' +
        'FROM ' + Tabela,
        'WHERE GraGruX=' + Geral.FF0(GraGruX),
        '']);
        //
        ValorProduto := USQLDB.Flu(Dmod.DqAux, 'ItemValr');
      end;
      else
      begin
        Result := False;
        Geral.MB_Erro('GraGruY n�o implementado em ' + sProcName);
        Tabela := 'loccits???';
      end;
    end;
  end;
end;

function TUnAppPF.PesquisaGraGXVend(var GraGruX: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmGraGXVePsq, FmGraGXVePsq, afmoNegarComAviso) then
  begin
    FmGraGXVePsq.FGraGruX := GraGruX;
    FmGraGXVePsq.ShowModal;
    GraGruX := FmGraGXVePsq.FGraGruX;
    Result := GraGruX <> 0;
    FmGraGXVePsq.Destroy;
  end;
end;

function TUnAppPF.RecalculaItens_Datas(QrItsLca: TmySQLQuery; Codigo,
  StatusLocacao: Integer; QualID: Variant; LocCConCabDtHrEmi, LocCConCabDtHrSai,
  LocCConCabDtHrDevolver, DtFimCobra: TDateTime; GeraLog: Boolean;
  Redefine: Boolean): Boolean;
var
  //
  Item: Integer;
  DMenos, DiasFDS, DiasUteis: Integer;
  SaiDtHr, CobranIniDtHr, CalcAdiLOCACAO, sCodigo, sValorAtz: String;
  Saida, Volta: TDateTime;
  IniCobranca: TDateTime;
  Log: String;
  ValorFDS, ValorLocAntigo, ValorUnitario, ValDevolParci, ValorLocAAdiantar,
  SOMA_ValLocAtz: Double;
  SQLType: TSQLType;
  SQL_CtrID: String;
  CtrID: Integer;
  Calculou: Boolean;
  Troca: Byte;
  //
  procedure CalculaDadosAtual();
  //const
    //GeraLog = True;
  var
    ValorDia, ValorSem, ValorQui, ValorMes, QtdeLocacao, QtdeProduto,
    QtdeDevolucao: Double;
    OriSubstSaiDH, HrTolerancia: TDateTime;
  begin
    Screen.Cursor := crHourGlass;
    try
      SQLType  := stUpd;
      //Volta    := QrLocCConCabDtHrDevolver;
      //
      DMenos         := QrItsLca.FieldByName('DMenos').AsInteger;
      //
      ValorDia       := QrItsLca.FieldByName('ValorDia').AsFloat;
      ValorSem       := QrItsLca.FieldByName('ValorSem').AsFloat;
      ValorQui       := QrItsLca.FieldByName('ValorQui').AsFloat;
      ValorMes       := QrItsLca.FieldByName('ValorMes').AsFloat;
      ValorFDS       := QrItsLca.FieldByName('ValorFDS').AsFloat;
      QtdeLocacao    := QrItsLca.FieldByName('QtdeLocacao').AsFloat;
      QtdeProduto    := QrItsLca.FieldByName('QtdeProduto').AsFloat;
      QtdeDevolucao  := QrItsLca.FieldByName('QtdeDevolucao').AsFloat;
      ValorLocAntigo := QrItsLca.FieldByName('ValorLocAtualizado').AsFloat;
      ValDevolParci  := QrItsLca.FieldByName('ValDevolParci').AsFloat;
      Troca          := QrItsLca.FieldByName('Troca').AsInteger;
      OriSubstSaiDH  := QrItsLca.FieldByName('OriSubstSaiDH').AsDateTime;
      HrTolerancia   := QrItsLca.FieldByName('HrTolerancia').AsDateTime;

      //
      //ValorFDS := 0;
      // For�ar defini��o da data inicial de cobran�a, pois ainda n�o foi definida!
      IniCobranca := 0;
      //
      DiasFDS            := 0;
      DiasUteis          := 0;
      CalcAdiLOCACAO     := '';
      ValorLocAAdiantar  := 0.00;
          //
      CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
      (*const*) Saida, Volta, HrTolerancia, OriSubstSaiDH, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
      ValorQui, ValorMes, ValorFDS, QtdeLocacao, QtdeProduto, QtdeDevolucao,
      ValDevolParci, Troca, LocCConCabDtHrDevolver, (*var*) Log, (*var*)IniCobranca, (*var*)DiasFDS,
      DiasUteis, (*var*)CalcAdiLOCACAO,
      ValorUnitario, ValorLocAAdiantar, Calculou);

      //
      //Geral.MB_Info(Log);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
var
  Continua: Boolean;
begin
  Result := False;
  //
  if Redefine then
  begin
    case TStatusLocacao(StatusLocacao) of
      TStatusLocacao.statlocIndefinido,
      TStatusLocacao.statlocCancelado,
      TStatusLocacao.statlocSuspenso: Continua := False;
      //
      TStatusLocacao.statlocFinalizado,
      TStatusLocacao.statlocLocado,
      TStatusLocacao.statlocAberto: Continua := True;
    end;
    if not Continua then
      Geral.MB_Info('O status do atendimento n�o permite redefini��o!');
  end else
  begin
    case TStatusLocacao(StatusLocacao) of
      TStatusLocacao.statlocIndefinido,
      TStatusLocacao.statlocFinalizado,
      TStatusLocacao.statlocCancelado,
      TStatusLocacao.statlocSuspenso: Continua := False;
      //
      TStatusLocacao.statlocLocado,
      TStatusLocacao.statlocAberto: Continua := True;
    end;
  end;
  if not Continua then
    Exit;
  SQLType           := stUpd;
  Screen.Cursor     := crHourGlass;
  try
    SOMA_ValLocAtz  := 0;
    //
    if QualID <> Null then
    begin
      CtrID     := Integer(QualID);
      if CtrID <> 0 then
        SQL_CtrID := 'AND CtrID=' + Geral.FF0(CtrID)
      else
        SQL_CtrID := '';
    end else
      SQL_CtrID := '';
    //
    ReopenItsLca(QrItsLca, Codigo, SQL_CtrID);
    if QrItsLca.RecordCount > 0 then
    begin
      Saida   := LocCConCabDtHrSai;
      SaiDtHr := Geral.FDT(Saida, 109);
      Volta   := LocCConCabDtHrDevolver;
      //
      QrItsLca.First;
      while not QrItsLca.Eof do
      begin
        //manelcaPrincipal=1,
        //manelcaSecundario=2,
        //manelcaAcessorio=3
        if QrItsLca.FieldByName('ManejoLca').AsInteger in ([1,2,3]) then
        begin
          Item := QrItsLca.FieldByName('Item').AsInteger;
          //
          CalculaDadosAtual();
          //
          if Calculou then
          begin
            CobranIniDtHr := Geral.FDT(IniCobranca, 109);

            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
            'CobranIniDtHr', 'SaiDtHr',
            'ValorFDS', 'DMenos', 'ValorLocAAdiantar',
            'CalcAdiLOCACAO', 'DiasFDS', 'DiasUteis'], [
            'Item'], [
            CobranIniDtHr, SaiDtHr,
            ValorFDS, DMenos, ValorLocAAdiantar,
            CalcAdiLOCACAO, DiasFDS, DiasUteis], [
            Item], True) then
              SOMA_ValLocAtz := SOMA_ValLocAtz + ValorLocAAdiantar;
          end else
          begin
            SOMA_ValLocAtz := SOMA_ValLocAtz + ValorLocAntigo;
            //
            if QrItsLca.FieldByName('SaiDtHr').AsDateTime <> Saida then
              UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'loccitslca', False, [
              'SaiDtHr'], ['Item'], [SaiDtHr], [Item], True);
          end;
        end;
        //
        QrItsLca.Next;
      end;
    end;
    //
    //
    if Trunc(DModG.ObtemAgora()) <= Trunc(LocCConCabDtHrSai) then
    begin
      sCodigo   := Geral.FF0(Codigo);
      sValorAtz := Geral.FFt_Dot(SOMA_ValLocAtz, 2, siNegativo);
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
      'UPDATE loccconcab SET ValorLocAAdiantar=' + sValorAtz +
      ', ValorLocAtualizado=' + sValorAtz +
      //', DtUltAtzPend="'+ Geral.FDT(FimCobra, 1) + '" ' + precisa ???
      ' WHERE Codigo=' + sCodigo + '; ');
    end else
    begin
      AtualizaPendenciasLocacao(QrItsLca, Codigo, StatusLocacao, LocCConCabDtHrEmi,
        DtFimCobra, LocCConCabDtHrDevolver, GeraLog);
    end;
  //
    Result := True;
    //
    // N�o fechar QrItsLca! Poder� usado para recalcular estoque
    //
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnAppPF.ReopenGraGXToll(GraGruY: Integer; Tipo: TGraGXToolRnt;
  QrGraGXToll: TmySQLQuery);
begin
  case GraGruY of
    CO_GraGruY_1024_GXPatPri,
    CO_GraGruY_2048_GXPatSec,
    CO_GraGruY_3072_GXPatAce:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXToll, Dmod.MyDB, [
      'SELECT ' +
      'gep.EstqSdo, ' +  // 2021-04-06
      'ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1,  ',
      'ggo.GraGruX, ggo.AtualValr ItemValr, ggo.ItemUnid ',
      'FROM gragxpatr ggo  ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
 'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle AND gep.Empresa=' + Geral.FF0(-11),   // 2021-04-06
      'WHERE ggo.Aplicacao=' + Geral.FF0(AppPF.ObtemTipoDeTGraGXOutr(Tipo)),
      'ORDER BY NO_GG1',
      '']);
    end;
    CO_GraGruY_4096_GXPatApo,
    CO_GraGruY_5120_GXPatUso,
    CO_GraGruY_6144_GXPrdCns:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXToll, Dmod.MyDB, [
      'SELECT 0.000 EstqSdo, ggx.Controle, gg1.Referencia, gg1.Nome NO_GG1,  ',
      'ggo.GraGruX, ggo.ItemValr, ggo.ItemUnid ',
      'FROM gragxoutr ggo  ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=ggo.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggo.Aplicacao=' + Geral.FF0(AppPF.ObtemTipoDeTGraGXOutr(Tipo)),
      'ORDER BY NO_GG1',
      '']);
    end;
  end;
  //Geral.MB_SQL(Self, QrGraGXToll);
end;

procedure TUnAppPF.ReopenItsLca(QrItsLca: TmySQLQuery; Codigo: Integer; SQL_CtrID: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsLca, Dmod.MyDB, [
  'SELECT *  ',
  'FROM loccitslca ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  SQL_CtrID,
  'ORDER BY CtrID, Item ',
  '']);
end;

procedure TUnAppPF.Servico(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXServ(0);
end;

function TUnAppPF.TextoDeCodHist_Descricao(CodHist: Integer): String;
var
  I, J: Integer;
begin
  Result := '';
  for I := 0 to MaxCodHist do
  begin
    J := Trunc(Power(2, I));
    //
    if Geral.IntInConjunto(J, CodHist) then
    begin
      if Result <> EmptyStr then
        Result := Result + ' - ';
      Result := Result + sCodHistDescris[I];
    end;
  end;
end;

function TUnAppPF.TextoDeCodHist_Sigla(CodHist: Integer): String;
var
  I, J: Integer;
begin
  Result := '';
  for I := 0 to MaxCodHist do
  begin
    J := Trunc(Power(2, I));
    //
    if Geral.IntInConjunto(J, CodHist) then
      Result := Result + sCodHistSiglas[I] + ' ';
  end;
end;

procedure TUnAppPF.VerificaCadastroVSArtigoIncompleta;
const
  sProcName = 'TUnAppPF.VerificaCadastroVSArtigoIncompleta()';
{
var
  Qry: TmySQLQuery;
}
begin
  Geral.MB_Info('Ver necessidade de implementar ' + sProcName);
{
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Nome, ggc.GraGruX, ggc.CouNiv1, ',
      'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2, ',
      'ggc.MediaMinKg, ggc.MediaMaxKg  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruxcou ggc ON ggc.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1 co1 ON co1.Codigo=ggc.CouNiv1 ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggx.GraGruY<>0 ',
      'AND ggc.GraGruX IS NULL ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Existem artigos com cadastro incompleto!');
      //
      if DBCheck.CriaFm(TFmGraGruYIncompleto, FmGraGruYIncompleto, afmoNegarComAviso) then
      begin
        FmGraGruYIncompleto.ReopenIncompleto(Qry);
        FmGraGruYIncompleto.ShowModal;
        FmGraGruYIncompleto.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
}
end;

procedure TUnAppPF.VerificaCadastroXxArtigoIncompleta;
const
  sProcName = 'TUnAppPF.VerificaCadastroXxArtigoIncompleta()';
{
var
  Qry: TmySQLQuery;
}
begin
  Geral.MB_Info('Ver necessidade de implementar ' + sProcName);
{
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ggx.Controle, gg1.Nome, ggc.GraGruX, ggc.CouNiv1, ',
      'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2, ',
      'ggc.MediaMinKg, ggc.MediaMaxKg  ',
      'FROM gragrux ggx  ',
      'LEFT JOIN gragruxcou ggc ON ggc.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1 co1 ON co1.Codigo=ggc.CouNiv1 ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE ggx.GraGruY<>0 ',
      'AND ggc.GraGruX IS NULL ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Existem artigos com cadastro incompleto!');
      //
      if DBCheck.CriaFm(TFmGraGruYIncompleto, FmGraGruYIncompleto, afmoNegarComAviso) then
      begin
        FmGraGruYIncompleto.ReopenIncompleto(Qry);
        FmGraGruYIncompleto.ShowModal;
        FmGraGruYIncompleto.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
}
end;

procedure TUnAppPF.VerificaEstoqueVenda(Empresa, FatPedCab, NF_Stat, CabStatus,
  ItsStatus: Integer);
var
  Baixa, Tipo, OriCodi, ValiStq: Integer;
begin
  // atualizar estoqe em funcao do NF_Stat, its.Status e cab.Status!
  //if (TStatusLocacao(CabStatus) = TStatusLocacao.statlocLocado)
  //if (CabStatus >= Integer(TStatusLocacao.statlocAberto))
  if (TStatusLocacao(CabStatus) in ([
    TStatusLocacao.statlocAberto,
    TStatusLocacao.statlocLocado,
    TStatusLocacao.statlocFinalizado,
    // ini 2023-12-01
    //TStatusLocacao.statlocCancelado,  // desfeito em 15/12/2023
    // fim 2023-12-01
    TStatusLocacao.statlocSuspenso]))
  //and (TStatusMovimento(ItsStatus) in ([TStatusMovimento.statmovPedido(*2*), TStatusMovimento.statmovFaturado(*3*)])) then
  and (
        (ItsStatus >= Integer(TStatusMovimento.statmovPedido))
        and
        (ItsStatus < Integer(TStatusMovimento.statmovCancelado))
      ) then
    Baixa := -1
  else
    Baixa := 0;
  begin
    Tipo    := VAR_FATID_0002; //FatID;
    OriCodi := FatPedCab;
(*
    if NF_Stat = 101 then
      ValiStq := 101
    else
      ValiStq := 100;
*)
    //
    ValiStq := NF_Stat;
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovitsa', False, [
    'Baixa', 'ValiStq'], [
    'Empresa', 'Tipo', 'OriCodi'], [
    Baixa, ValiStq], [
    Empresa, Tipo, OriCodi], False);
  end;
end;

procedure TUnAppPF.VerificaGraGruYsNaoConfig;
const
  sProcName = 'TUnAppPF.VerificaGraGruYsNaoConfig()';
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero, GraTabApp: Integer;
  PerComissF, PerComissR: Double;
  //
  function GeraPrdGrupTip(): Integer;
  begin
    Result := 0;
    Codigo := UMyMod.BuscaEmLivreY_Def('prdgruptip', 'Codigo', stIns, 0);
    CodUsu := Codigo;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
    'CodUsu', 'Nome', 'MadeBy',
    'Fracio', 'Gradeado', 'TipPrd',
    'NivCad', 'FaixaIni', 'FaixaFim',
    'TitNiv1', 'TitNiv2', 'TitNiv3',
    'TitNiv4', 'TitNiv5', 'Customizav',
    'ImpedeCad', 'LstPrcFisc', 'PerComissF',
    'PerComissR', 'Tipo_Item', 'Genero',
    'GraTabApp'], [
    'Codigo'], [
    CodUsu, Nome, MadeBy,
    Fracio, Gradeado, TipPrd,
    NivCad, FaixaIni, FaixaFim,
    TitNiv1, TitNiv2, TitNiv3,
    TitNiv4, TitNiv5, Customizav,
    ImpedeCad, LstPrcFisc, PerComissF,
    PerComissR, Tipo_Item, Genero,
    GraTabApp], [
    Codigo], True) then
      Result := Codigo
  end;
var
  Qry: TmySQLQuery;
  GraGruY, PrdGrupTip: Integer;
  Inclui: Boolean;
begin
////////////////////////////////////////////////////////////////////////////////
   EXIT;  // Deprecado!!! Tentar eliminar campo PrdGrupTip da tabela GraGruY
////////////////////////////////////////////////////////////////////////////////
{
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM gragruy ',
    'WHERE PrdGrupTip=0 ',
    'AND Codigo <> 0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(Qry.RecordCount) +
      ' Grupos de Estoque sem Grupo de Produto definido!' + sLineBreak +
      'Deseja que o(s) Grupo(s) de Produto sejam criado(s) e atrelados aos respectivos Grupos de Estoque?')
      = ID_YES then
      begin
        TitNiv2        := '';
        TitNiv3        := '';
        TitNiv4        := '';
        TitNiv5        := '';
        NivCad         := 1;
        FaixaIni       := 0;
        FaixaFim       := 0;
        Customizav     := 0;
        ImpedeCad      := 1;
        LstPrcFisc     := 5;
        PerComissF     := 0;
        PerComissR     := 0;
        Tipo_Item      := 0;
        Genero         := 0;
        GraTabApp      := 0;
        //
        Qry.First;
        while not Qry.Eof do
        begin
          GraGruY := Qry.FieldByName('Codigo').AsInteger;
          Inclui  := True;
          case GraGruY of
            (*
            CO_GraGruY_0512_TXSubPrd:
            begin
              Nome           := CO_TXT_GraGruY_0512_TXSubPrd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 5;  // subproduto
            end;
            CO_GraGruY_0683_TXPSPPro:
            begin
              Nome           := CO_TXT_GraGruY_0683_TXPSPPro;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_0853_TXPSPEnd:
            begin
              Nome           := CO_TXT_GraGruY_0853_TXPSPEnd;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;*)
            CO_GraGruY_1024_TXCadNat:
            begin
              Nome           := CO_TXT_GraGruY_1024_TXCadNat;
              MadeBy         := 2;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 2;
              Tipo_Item      := 1; // materia-prima
            end;
            (*
            CO_GraGruY_1195_TXNatPDA:
            begin
              Nome           := CO_TXT_GraGruY_1195_TXNatPDA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1365_TXProCal:
            begin
              Nome           := CO_TXT_GraGruY_1365_TXProCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1536_TXCouCal:
            begin
              Nome           := CO_TXT_GraGruY_1536_TXCouCal;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1621_TXCouDTA:
            begin
              Nome           := CO_TXT_GraGruY_1621_TXCouDTA;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 6; // intermediario
            end;
            CO_GraGruY_1707_TXProCur:
            begin
              Nome           := CO_TXT_GraGruY_1707_TXProCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_1877_TXCouCur:
            begin
              Nome           := CO_TXT_GraGruY_1877_TXCouCur;
              MadeBy         := 1;
              Fracio         := 3;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;*)
            CO_GraGruY_2048_TXCadOpe:
            begin
              Nome           := CO_TXT_GraGruY_2048_TXCadOpe;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            (*
            CO_GraGruY_3072_TXRibCla:
            begin
              Nome           := CO_TXT_GraGruY_3072_TXRibCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;
            CO_GraGruY_4096_TXRibOpe:
            begin
              Nome           := CO_TXT_GraGruY_4096_TXRibOpe;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 0;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_5120_TXWetEnd:
            begin
              Nome           := CO_TXT_GraGruY_5120_TXWetEnd;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 3; // produto em processo
            end;
            CO_GraGruY_6144_TXFinCla:
            begin
              Nome           := CO_TXT_GraGruY_6144_TXFinCla;
              MadeBy         := 1;
              Fracio         := 2;
              Gradeado       := 1;
              TipPrd         := 1;
              Tipo_Item      := 4; // produto pronto
            end;*)
            else
            begin
              Geral.MB_Erro(
              'GraGruY ' + Geral.FF0(GraGruY) + ' n�o definido em "' + sProcName + '"');
              Inclui := False;
            end;
          end;
          if Inclui then
          begin
            TitNiv1 := Nome;
            PrdGrupTip := GeraPrdGrupTip();
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragruy', False, [
            'PrdGrupTip'], [
            'Codigo'], [
            PrdGrupTip], [
            GraGruY], True);
          end;
          Qry.Next;
        end;
      end;
      Qry.First;
    end;
  finally
    Qry.Free;
  end;
}
end;

end.
