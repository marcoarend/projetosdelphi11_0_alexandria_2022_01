unit ContratApp1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxRich, dmkGeral, DB, mySQLDbTables, frxDBSet, UnDmkEnums;

type
  TFmContratApp1 = class(TForm)
    frxContrato: TfrxReport;
    frxRichObject1: TfrxRichObject;
    QrEndContratada: TmySQLQuery;
    QrEndContratadaTipo: TSmallintField;
    QrEndContratadaCONTRATADA: TWideStringField;
    QrEndContratadaNOMELOGRAD: TWideStringField;
    QrEndContratadaNOMERUA: TWideStringField;
    QrEndContratadaCOMPL: TWideStringField;
    QrEndContratadaBAIRRO: TWideStringField;
    QrEndContratadaCIDADE: TWideStringField;
    QrEndContratadaNOMEUF: TWideStringField;
    QrEndContratadaCNPJ_CPF: TWideStringField;
    QrEndContratadaIE_RG: TWideStringField;
    QrEndContratadaCodigo: TIntegerField;
    QrEndContratadaTEL: TWideStringField;
    QrEndContratadaFAX: TWideStringField;
    QrEndContratadaTEL_TXT: TWideStringField;
    QrEndContratadaFAX_TXT: TWideStringField;
    QrEndContratadaCNPJ_TXT: TWideStringField;
    QrEndContratante: TmySQLQuery;
    QrEndContratanteCodigo: TIntegerField;
    QrEndContratanteTipo: TSmallintField;
    QrEndContratanteNOMELOGRAD: TWideStringField;
    QrEndContratanteNOMERUA: TWideStringField;
    QrEndContratanteCOMPL: TWideStringField;
    QrEndContratanteBAIRRO: TWideStringField;
    QrEndContratanteCIDADE: TWideStringField;
    QrEndContratanteNOMEUF: TWideStringField;
    QrEndContratanteCNPJ_CPF: TWideStringField;
    QrEndContratanteIE_RG: TWideStringField;
    QrEndContratanteTEL: TWideStringField;
    QrEndContratanteFAX: TWideStringField;
    QrEndContratanteTEL_TXT: TWideStringField;
    QrEndContratanteCONTRATANTE: TWideStringField;
    QrEndContratanteFAX_TXT: TWideStringField;
    QrEndContratanteCNPJ_TXT: TWideStringField;
    QrContrato: TmySQLQuery;
    QrContratoCodigo: TIntegerField;
    QrContratoContratada: TIntegerField;
    QrContratoContratante: TIntegerField;
    QrContratoDContrato: TDateField;
    QrContratoDInstal: TDateField;
    QrContratoDVencimento: TDateField;
    QrContratoStatus: TSmallintField;
    QrContratoAtivo: TSmallintField;
    QrContratoValorMes: TFloatField;
    QrContratoNCONTRATADA: TWideStringField;
    QrContratoNCONTRATANTE: TWideStringField;
    QrContratoWinDocArq: TWideStringField;
    QrContratoFormaUso: TSmallintField;
    QrContratoNome: TWideStringField;
    QrContratoDtaPropIni: TDateField;
    QrContratoDtaPropFim: TDateField;
    QrContratoDtaCntrFim: TDateField;
    QrContratoDtaImprime: TDateTimeField;
    QrContratoDtaAssina: TDateField;
    QrContratoTratamento: TWideStringField;
    QrContratoArtigoDef: TSmallintField;
    QrContratoPerDefImpl: TSmallintField;
    QrContratoPerQtdImpl: TSmallintField;
    QrContratoPerDefTrei: TSmallintField;
    QrContratoPerQtdTrei: TSmallintField;
    QrContratoPerDefMoni: TSmallintField;
    QrContratoPerQtdMoni: TSmallintField;
    QrContratoPerDefIniI: TSmallintField;
    QrContratoPerQtdIniI: TSmallintField;
    QrContratoRepreLegal: TIntegerField;
    QrContratoTestemun01: TIntegerField;
    QrContratoTestemun02: TIntegerField;
    QrContratoPercMulta: TFloatField;
    QrContratoPercJuroM: TFloatField;
    QrContratoTpPagDocu: TWideStringField;
    QrContratoddMesVcto: TSmallintField;
    QrContratoTexto: TWideMemoField;
    QrContratoNO_REP_LEGAL: TWideStringField;
    QrContratoNO_TESTEM_01: TWideStringField;
    QrContratoNO_TESTEM_02: TWideStringField;
    QrContratoLugarNome: TWideStringField;
    QrContratoLugArtDef: TSmallintField;
    QrContratoVersao: TFloatField;
    frxDsContratos: TfrxDBDataset;
    frxDsEndContratada: TfrxDBDataset;
    frxDsEndContratante: TfrxDBDataset;
    frxDsLocCCon: TfrxDBDataset;
    QrLocCCon: TmySQLQuery;
    QrLocCConEmpresa: TIntegerField;
    QrLocCConNO_EMP: TWideStringField;
    QrLocCConNO_COMPRADOR: TWideStringField;
    QrLocCConNO_RECEBEU: TWideStringField;
    QrLocCConCodigo: TIntegerField;
    QrLocCConContrato: TIntegerField;
    QrLocCConCliente: TIntegerField;
    QrLocCConDtHrEmi: TDateTimeField;
    QrLocCConDtHrBxa: TDateTimeField;
    QrLocCConVendedor: TIntegerField;
    QrLocCConECComprou: TIntegerField;
    QrLocCConECRetirou: TIntegerField;
    QrLocCConNumOC: TWideStringField;
    QrLocCConValorTot: TFloatField;
    QrLocCConLocalObra: TWideStringField;
    QrLocCConObs0: TWideStringField;
    QrLocCConObs1: TWideStringField;
    QrLocCConObs2: TWideStringField;
    QrLocCConNO_LOGIN: TWideStringField;
    QrLocCConNO_CLIENTE: TWideStringField;
    QrLocCConDataEmi: TDateField;
    QrLocCConHoraEmi: TTimeField;
    QrLocCConDtHrBxa_TXT: TWideStringField;
    QrLocCConEndCobra: TWideStringField;
    QrLocCConLocalCntat: TWideStringField;
    QrLocCConLocalFone: TWideStringField;
    QrLocCConLOCALFONE_TXT: TWideStringField;
    QrEndContratadaCEP_TXT: TWideStringField;
    QrEndContratanteCEP_TXT: TWideStringField;
    QrEndContratadaCEP: TFloatField;
    QrEndContratanteCEP: TFloatField;
    QrLocCPatPri: TmySQLQuery;
    QrLocCPatPriCodigo: TIntegerField;
    QrLocCPatPriCtrID: TIntegerField;
    QrLocCPatPriGraGruX: TIntegerField;
    QrLocCPatPriValorDia: TFloatField;
    QrLocCPatPriValorSem: TFloatField;
    QrLocCPatPriValorQui: TFloatField;
    QrLocCPatPriValorMes: TFloatField;
    QrLocCPatPriDtHrRetorn: TDateTimeField;
    QrLocCPatPriLibFunci: TIntegerField;
    QrLocCPatPriLibDtHr: TDateTimeField;
    QrLocCPatPriRELIB: TWideStringField;
    QrLocCPatPriHOLIB: TWideStringField;
    QrLocCPatPriNO_GGX: TWideStringField;
    QrLocCPatPriLOGIN: TWideStringField;
    QrLocCPatPriREFERENCIA: TWideStringField;
    QrLocCPatPriTXT_DTA_DEVOL: TWideStringField;
    QrLocCPatPriTXT_DTA_LIBER: TWideStringField;
    QrLocCPatPriTXT_QEM_LIBER: TWideStringField;
    QrLocCPatPriDtHrLocado: TDateTimeField;
    QrLocCPatSec: TmySQLQuery;
    QrLocCPatSecCodigo: TIntegerField;
    QrLocCPatSecCtrID: TIntegerField;
    QrLocCPatSecItem: TIntegerField;
    QrLocCPatSecGraGruX: TIntegerField;
    QrLocCPatSecNO_GGX: TWideStringField;
    QrLocCPatSecREFERENCIA: TWideStringField;
    QrLocCPatAce: TmySQLQuery;
    QrLocCPatAceCodigo: TIntegerField;
    QrLocCPatAceCtrID: TIntegerField;
    QrLocCPatAceItem: TIntegerField;
    QrLocCPatAceGraGruX: TIntegerField;
    QrLocCPatAceNO_GGX: TWideStringField;
    QrLocCPatAceREFERENCIA: TWideStringField;
    QrLocCPatCns: TmySQLQuery;
    QrLocCPatCnsCodigo: TIntegerField;
    QrLocCPatCnsCtrID: TIntegerField;
    QrLocCPatCnsItem: TIntegerField;
    QrLocCPatCnsGraGruX: TIntegerField;
    QrLocCPatCnsNO_GGX: TWideStringField;
    QrLocCPatCnsREFERENCIA: TWideStringField;
    QrLocCPatCnsUnidade: TIntegerField;
    QrLocCPatCnsQtdIni: TFloatField;
    QrLocCPatCnsQtdFim: TFloatField;
    QrLocCPatCnsQtdUso: TFloatField;
    QrLocCPatCnsPrcUni: TFloatField;
    QrLocCPatCnsValUso: TFloatField;
    QrLocCPatCnsSIGLA: TWideStringField;
    QrLocCPatUso: TmySQLQuery;
    QrLocCPatUsoCodigo: TIntegerField;
    QrLocCPatUsoCtrID: TIntegerField;
    QrLocCPatUsoItem: TIntegerField;
    QrLocCPatUsoGraGruX: TIntegerField;
    QrLocCPatUsoNO_GGX: TWideStringField;
    QrLocCPatUsoREFERENCIA: TWideStringField;
    QrLocCPatUsoAvalIni: TIntegerField;
    QrLocCPatUsoAvalFim: TIntegerField;
    QrLocCPatUsoPrcUni: TFloatField;
    QrLocCPatUsoValTot: TFloatField;
    QrLocCPatUsoUnidade: TIntegerField;
    QrLocCPatUsoSIGLA: TWideStringField;
    QrLocCPatUsoNO_AVALINI: TWideStringField;
    QrLocCPatUsoNO_AVALFIM: TWideStringField;
    frxDsLocCPatPri: TfrxDBDataset;
    frxDsLocCPatSec: TfrxDBDataset;
    frxDsLocCPatAce: TfrxDBDataset;
    frxDsLocCPatCns: TfrxDBDataset;
    frxDsLocCPatUso: TfrxDBDataset;
    QrLocCPatPriAtualValr: TFloatField;
    QrLocCPatPriRetFunci: TIntegerField;
    QrLocCPatPriRetExUsr: TIntegerField;
    QrLocCPatPriLibExUsr: TIntegerField;
    QrLocCPatPriVAL_SEM_CALC: TFloatField;
    QrLocCPatPriVAL_QUI_CALC: TFloatField;
    QrLocCPatPriVAL_MES_CALC: TFloatField;
    QrLocCPatSecAtualValr: TFloatField;
    QrLocCPatAceValBem: TFloatField;
    QrEndContratadaNUMERO: TFloatField;
    QrEndContratanteNUMERO: TFloatField;
    frxLOC_PATRI_001_001: TfrxReport;
    procedure frxContratoGetValue(const VarName: string; var Value: Variant);
    procedure QrEndContratadaCalcFields(DataSet: TDataSet);
    procedure QrEndContratanteCalcFields(DataSet: TDataSet);
    procedure QrLocCConCalcFields(DataSet: TDataSet);
    procedure QrLocCPatPriAfterScroll(DataSet: TDataSet);
    procedure QrLocCPatPriBeforeClose(DataSet: TDataSet);
    procedure QrLocCPatPriCalcFields(DataSet: TDataSet);
    procedure QrLocCConAfterScroll(DataSet: TDataSet);
    procedure QrLocCConBeforeClose(DataSet: TDataSet);
    procedure QrLocCPatCnsCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxLOC_PATRI_001_001GetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
    FLocCPatPriCtrID: String;
    procedure ReopenLocCPatPri();
    procedure ReopenLocCPatSec();
    procedure ReopenLocCPatAce();
    procedure ReopenLocCPatUso();
    procedure ReopenLocCPatCns();
  public
    { Public declarations }
    FLocCCon: Integer;
    //
    procedure Imprime();
    procedure ImprimeDev(LocCPatPriCtrID: String);
  end;

var
  FmContratApp1: TFmContratApp1;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, LocCCon, DmkDAC_PF,
  ModuleGeral;

{$R *.dfm}

{ TFmContratApp }

procedure TFmContratApp1.FormCreate(Sender: TObject);
begin
  FLocCPatPriCtrID := '';
  //
  DModG.ReopenOpcoesGerl;
end;

procedure TFmContratApp1.frxContratoGetValue(const VarName: string;
  var Value: Variant);
var
  CATipo, CETipo: Integer;
  //CCMes, CCAno: Integer;
  //CCData: String;
begin
  CATipo := QrEndContratadaTipo.Value;
  CETipo := QrEndContratanteTipo.Value;
  //
  if VarName = 'DAT_ASIN' then
    Value := Geral.FDT(QrLocCConDataEmi.Value, 6)
  else
  if VarName = 'HOR_ASIN' then
    Value := Geral.FDT(QrLocCConDtHrEmi.Value, 102)
  else
  if VarName = 'VARF_CAENDERECO' then
    Value := QrEndContratadaNOMELOGRAD.Value + ' ' +
     QrEndContratadaNOMERUA.Value + ', N� ' +
     Geral.FFT(QrEndContratadaNUMERO.Value, 0, siPositivo) + ' ' +
     QrEndContratadaCOMPL.Value
  else
  if VarName = 'VARF_CAIERG' then
  begin
    case CATipo of
    0:
      if QrEndContratadaIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := QrEndContratadaIE_RG.Value;
    1:
      if QrEndContratadaIE_RG.Value = '' then
        Value := ''
      else
        Value := QrEndContratadaIE_RG.Value;
    end;
  end
  else
  if VarName = 'VARF_CALACNPJCFP' then
  begin
    if CATipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end
  else
  if VarName = 'VARF_CALAIERG' then
  begin
    if CATipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end
  else
  //
  if VarName = 'VARF_CEENDERECO' then
    Value := QrEndContratanteNOMELOGRAD.Value + ' ' +
     QrEndContratanteNOMERUA.Value + ', N� ' +
     FormatFloat('0', QrEndContratanteNUMERO.Value) + ' ' +
     QrEndContratanteCOMPL.Value
  else
  if VarName = 'VARF_CEIERG' then
  begin
    case CETipo of
    0:
      if QrEndContratanteIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := QrEndContratanteIE_RG.Value;
    1:
      if QrEndContratanteIE_RG.Value = '' then
        Value := ''
      else
        Value := QrEndContratanteIE_RG.Value;
    end;
  end else
  if VarName = 'VARF_CELACNPJCFP' then
  begin
    if CETipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end else
  if VarName = 'VARF_CELAIERG' then
  begin
    if CETipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end else
  if VarName = 'VAR_SLOGANFOOT' then
    Value := DmodG.QrOpcoesGerl.FieldByName('SloganFoot').AsString
  else
  if VarName = 'VAR_ValorDia' then
  begin
    if QrLocCPatPriValorDia.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocCPatPriValorDia.Value, 2, siNegativo);
  end;
  if VarName = 'VAR_VAL_SEM_CALC' then
  begin
    if QrLocCPatPriValorSem.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocCPatPriVAL_SEM_CALC.Value, 2, siNegativo);
  end else
  if VarName = 'VAR_VAL_QUI_CALC' then
  begin
    if QrLocCPatPriValorQui.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocCPatPriVAL_QUI_CALC.Value, 2, siNegativo);
  end else
  if VarName = 'VAR_VAL_MES_CALC' then
  begin
    if QrLocCPatPriValorMes.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocCPatPriVAL_MES_CALC.Value, 2, siNegativo);
  end;
  if VarName = 'VAR_OBSERVACOES' then
  begin
    if (QrLocCConObs0.Value <> '') or (QrLocCConObs1.Value <> '') or (QrLocCConObs2.Value <> '') then
      Value := QrLocCConObs0.Value + ' ' + QrLocCConObs1.Value + ' ' + QrLocCConObs2.Value
    else
      Value := '';
  end;
  //
{
  if VarName = 'CIDADE_O' then Value := Dmod.QrControleCidade.Value;
  //if VarName = 'UF_O    ' then Value := Dmod.QrControleNOMEUFPADRAO.Value;
  if VarName = 'DATAC_O ' then Value := QrContratoDContrato.Value;
  if VarName = 'DATACE_O' then Value := FormatDateTime('dd " de " mmmm " de " yyyy', QrContratoDContrato.Value);
  if VarName = 'HOJE_O  ' then Value := FormatDateTime('dd"/"mm"/"yyyy', Now);
  if VarName = 'HOJEEX_O' then Value := FormatDateTime('dd " de " mmmm " de " yyyy', Now);
  if VarName = 'DATAI_O ' then Value := QrContratoDInstal.Value;
  if VarName = 'DATAIE_O' then Value := FormatDateTime('dd " de " mmmm " de " yyyy', QrContratoDInstal.Value);
  if VarName = 'DATAV_O ' then Value := QrContratoDVencimento.Value;
  if VarName = 'DATAVE_O' then Value := FormatDateTime('dd " de " mmmm " de " yyyy', QrContratoDVencimento.Value);
  if VarName = 'DATACC_O' then
  begin
    CCAno  := Geral.IMV(FormatDateTime('yyyy', QrContratoDVencimento.Value));
    CCMes  := Geral.IMV(FormatDateTime('mm', QrContratoDVencimento.Value));
    CCData := DateToStr(EncodeDate(CCAno, CCMes, 1)-1);
    Value := CCData + ' (' + FormatDateTime('dd " de " mmmm " de " yyyy', StrToDate(CCData)) + ')';
  end;
}
end;

procedure TFmContratApp1.frxLOC_PATRI_001_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_SLOGANFOOT' then
    Value := DmodG.QrOpcoesGerl.FieldByName('SloganFoot').AsString
  else if VarName = 'VAR_DATAHORAENCER' then
  begin
    if QrLocCConDtHrBxa.Value < 2 then
      Value := '___ / ___ / ______ �s ___ : ___ horas'
    else
      Value := Geral.FDT(QrLocCConDtHrBxa.Value, 8);
  end else
  if VarName = 'VAR_QtdUso' then
  begin
    if (QrLocCPatCnsQtdIni.Value <> 0) and (QrLocCPatCnsQtdFim.Value <> 0) then
      Value := Geral.FFT(QrLocCPatCnsQtdUso.Value, 3, siPositivo)
    else
      Value := '';
  end;
end;

procedure TFmContratApp1.Imprime();
begin
  QrLocCCon.Close;
  QrLocCCon.Params[0].AsInteger := FLocCCon;
  UMyMod.AbreQuery(QrLocCCon, Dmod.MyDB);
  //
  QrEndContratante.Close;
  QrEndContratante.Params[0].AsInteger := QrLocCConCliente.Value;
  UMyMod.AbreQuery(QrEndContratante, Dmod.MyDB);
  //
  QrEndContratada.Close;
  QrEndContratada.Params[0].AsInteger := QrLocCConEmpresa.Value;
  UMyMod.AbreQuery(QrEndContratada, Dmod.MyDB);
  //
  QrContrato.Close;
  QrContrato.Params[0].AsInteger := QrLocCConContrato.Value;
  UMyMod.AbreQuery(QrContrato, Dmod.MyDB);
  //
  MyObjects.frxDefineDatasets(frxContrato, [
  //DModG.frxDsDono,
  frxDsLocCCon,
  frxDsContratos,
  frxDsEndContratada,
  frxDsEndContratante,
  frxDsLocCPatAce,
  frxDsLocCPatCns,
  frxDsLocCPatPri,
  frxDsLocCPatSec,
  frxDsLocCPatUso
  ]);
  //
  MyObjects.frxMostra(frxContrato, 'Contrato de Loca��o');
end;

procedure TFmContratApp1.ImprimeDev(LocCPatPriCtrID: String);
begin
  FLocCPatPriCtrID := LocCPatPriCtrID;
  //
  QrLocCCon.Close;
  QrLocCCon.Params[0].AsInteger := FLocCCon;
  UMyMod.AbreQuery(QrLocCCon, Dmod.MyDB);
  //
  QrEndContratante.Close;
  QrEndContratante.Params[0].AsInteger := QrLocCConCliente.Value;
  UMyMod.AbreQuery(QrEndContratante, Dmod.MyDB);
  //
  QrEndContratada.Close;
  QrEndContratada.Params[0].AsInteger := QrLocCConEmpresa.Value;
  UMyMod.AbreQuery(QrEndContratada, Dmod.MyDB);
  //
  QrContrato.Close;
  QrContrato.Params[0].AsInteger := QrLocCConContrato.Value;
  UMyMod.AbreQuery(QrContrato, Dmod.MyDB);
  //
  MyObjects.frxDefineDatasets(frxContrato, [
  //DModG.frxDsDono,
  frxDsLocCCon,
  frxDsContratos,
  frxDsEndContratada,
  frxDsEndContratante,
  frxDsLocCPatAce,
  frxDsLocCPatCns,
  frxDsLocCPatPri,
  frxDsLocCPatSec,
  frxDsLocCPatUso
  ]);
  //
  MyObjects.frxMostra(frxLOC_PATRI_001_001, 'Devolu��o de loca��o');
end;

procedure TFmContratApp1.QrEndContratadaCalcFields(DataSet: TDataSet);
begin
  QrEndContratadaTEL_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaTEL.Value);
  QrEndContratadaFAX_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaFAX.Value);
  QrEndContratadaCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratadaCNPJ_CPF.Value);
  QrEndContratadaCEP_TXT.Value := Geral.FormataCEP_NT(QrEndContratadaCEP.Value);
end;

procedure TFmContratApp1.QrEndContratanteCalcFields(DataSet: TDataSet);
begin
  QrEndContratanteTEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteTEL.Value);
  QrEndContratanteFAX_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteFAX.Value);
  QrEndContratanteCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratanteCNPJ_CPF.Value);
  QrEndContratanteCEP_TXT.Value := Geral.FormataCEP_NT(QrEndContratanteCEP.Value);
end;

procedure TFmContratApp1.QrLocCConAfterScroll(DataSet: TDataSet);
begin
  ReopenLocCPatPri();
end;

procedure TFmContratApp1.QrLocCConBeforeClose(DataSet: TDataSet);
begin
  QrLocCPatPri.Close;
end;

procedure TFmContratApp1.QrLocCConCalcFields(DataSet: TDataSet);
begin
  QrLocCConDataEmi.Value := Trunc(QrLocCConDtHrEmi.Value);
  QrLocCConHoraEmi.Value := QrLocCConDtHrEmi.Value - QrLocCConDataEmi.Value;
  QrLocCConDtHrBxa_TXT.Value :=  Geral.FDT(QrLocCConDtHrBxa.Value, 107);
  QrLocCConLOCALFONE_TXT.Value :=  Geral.FormataTelefone_TT(
    Geral.SoNumero_TT(QrLocCConLocalFone.Value));
end;

procedure TFmContratApp1.QrLocCPatCnsCalcFields(DataSet: TDataSet);
begin
  QrLocCPatCnsQTDUSO.Value :=
    QrLocCPatCnsQtdFim.Value - QrLocCPatCnsQtdIni.Value;
end;

procedure TFmContratApp1.QrLocCPatPriAfterScroll(DataSet: TDataSet);
begin
  ReopenLocCPatAce();
  ReopenLocCPatCns();
  ReopenLocCPatSec();
  ReopenLocCPatUso();
end;

procedure TFmContratApp1.QrLocCPatPriBeforeClose(DataSet: TDataSet);
begin
  QrLocCPatAce.Close;
  QrLocCPatCns.Close;
  QrLocCPatSec.Close;
  QrLocCPatUso.Close;
end;

procedure TFmContratApp1.QrLocCPatPriCalcFields(DataSet: TDataSet);
begin
  QrLocCPatPriTXT_DTA_DEVOL.Value := Geral.FDT(QrLocCPatPriDtHrRetorn.Value, 107);
  QrLocCPatPriTXT_DTA_LIBER.Value := Geral.FDT(QrLocCPatPriLibDtHr.Value, 107);
  QrLocCPatPriTXT_QEM_LIBER.Value := Trim(QrLocCPatPriLOGIN.Value + ' ' +
    QrLocCPatPriRELIB.Value + ' ' +QrLocCPatPriHOLIB.Value);

  //
  if QrLocCPatPriValorSem.Value >= 0.01 then
    QrLocCPatPriVAL_SEM_CALC.Value := QrLocCPatPriValorSem.Value
  else
    QrLocCPatPriVAL_SEM_CALC.Value := QrLocCPatPriValorDia.Value * 7;
  //
  if QrLocCPatPriValorQui.Value >= 0.01 then
    QrLocCPatPriVAL_QUI_CALC.Value := QrLocCPatPriValorQui.Value
  else
    QrLocCPatPriVAL_QUI_CALC.Value := QrLocCPatPriValorSem.Value / 7 * 15;
  //
  if QrLocCPatPriValorMes.Value >= 0.01 then
    QrLocCPatPriVAL_MES_CALC.Value := QrLocCPatPriValorMes.Value
  else
    QrLocCPatPriVAL_MES_CALC.Value := QrLocCPatPriValorQui.Value * 2;
end;

procedure TFmContratApp1.ReopenLocCPatAce();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatAce, Dmod.MyDB, [
  'SELECT lpa.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
  'FROM loccpatace lpa ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpa.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lpa.CtrID=' + Geral.FF0(QrLocCPatPriCtrID.Value),
  '']);
end;

procedure TFmContratApp1.ReopenLocCPatCns();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatCns, Dmod.MyDB, [
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'med.SIGLA ',
  'FROM loccpatcns lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.CtrID=' + Geral.FF0(QrLocCPatPriCtrID.Value),
  '']);
end;

procedure TFmContratApp1.ReopenLocCPatPri();
var
  SQL: String;
begin
  if FLocCPatPriCtrID <> '' then
    SQL := 'AND lpp.CtrID IN (' + FLocCPatPriCtrID + ')'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatPri, Dmod.MyDB, [
  'SELECT ggp.AtualValr, lpp.*, ',
  'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccpatpri lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragxpatr ggp ON ggx.Controle=ggp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(QrLocCConCodigo.Value),
  SQL,
  '']);
end;

procedure TFmContratApp1.ReopenLocCPatSec();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatSec, Dmod.MyDB, [
  'SELECT ggp.AtualValr, lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
  'FROM loccpatsec lps ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX ',
  'LEFT JOIN gragxpatr ggp ON ggx.Controle=ggp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lps.CtrID=' + Geral.FF0(QrLocCPatPriCtrID.Value),
  'AND lps.GraGruX <> 0 ',
  '']);
end;

procedure TFmContratApp1.ReopenLocCPatUso();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatUso, Dmod.MyDB, [
  'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA, ',
  'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM  ',
  'FROM loccpatuso lpu  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade ',
  'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni ',
  'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim ',
  'WHERE lpu.CtrID=' + Geral.FF0(QrLocCPatPriCtrID.Value),
  '']);
end;

end.
