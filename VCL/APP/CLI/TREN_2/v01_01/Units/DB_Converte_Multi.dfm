object FmDB_Converte_Multi: TFmDB_Converte_Multi
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: Convers'#227'o de Banco de Dados'
  ClientHeight = 763
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 443
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Convers'#227'o de Banco de Dados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 547
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 547
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1241
        Height = 547
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 18
          Width = 1237
          Height = 527
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 0
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tabelas DBF'
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 1229
              Height = 496
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 1229
                Height = 496
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                TabOrder = 0
                object PageControl2: TPageControl
                  Left = 1
                  Top = 1
                  Width = 1227
                  Height = 494
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ActivePage = TabSheet2
                  Align = alClient
                  TabOrder = 0
                  object TabSheet2: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Registros'
                    object DBGrid1: TDBGrid
                      Left = 0
                      Top = 276
                      Width = 1219
                      Height = 187
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataSource = DsDBF
                      TabOrder = 0
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -14
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                    object Panel7: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 276
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      TabOrder = 1
                      object GroupBox2: TGroupBox
                        Left = 1
                        Top = 1
                        Width = 85
                        Height = 274
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alLeft
                        Caption = ' C'#243'digos ini: '
                        TabOrder = 0
                        object Panel10: TPanel
                          Left = 2
                          Top = 18
                          Width = 81
                          Height = 254
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label3: TLabel
                            Left = 2
                            Top = 5
                            Width = 44
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cliente:'
                          end
                          object Label4: TLabel
                            Left = 2
                            Top = 54
                            Width = 73
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Fornecedor:'
                          end
                          object Label5: TLabel
                            Left = 2
                            Top = 103
                            Width = 57
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Entidade:'
                          end
                          object EdCliCodIni: TdmkEdit
                            Left = 1
                            Top = 25
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 0
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '10001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 10001
                            ValWarn = False
                          end
                          object EdForCodIni: TdmkEdit
                            Left = 1
                            Top = 74
                            Width = 74
                            Height = 26
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 1
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '20001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 20001
                            ValWarn = False
                          end
                          object EdEntCodIni: TdmkEdit
                            Left = 1
                            Top = 123
                            Width = 74
                            Height = 26
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Alignment = taRightJustify
                            TabOrder = 2
                            FormatType = dmktfInteger
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 0
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            Texto = '30001'
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = 30001
                            ValWarn = False
                          end
                        end
                      end
                      object CGDBF: TdmkCheckGroup
                        Left = 86
                        Top = 1
                        Width = 1132
                        Height = 274
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Caption = ' Tabelas: '
                        Columns = 2
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Items.Strings = (
                          'CADBCO        20 [SIM]             (Cadastro de Carteiras)'
                          'CADCOM       275 [NAO] 2002 / 2005 (???)'
                          'CADCLI      4852 [SIM]             (Cadastro de Clientes)'
                          'CADCSI      2779 [NAO] 2002 / 2007 (??/)'
                          'CADCTR     11381 [   ] 2002 / .... (Contratos?)'
                          'CADFOR      1313 [SIM]             (Cadastro Fornecedores)'
                          'CADGRU        85 [   ]             (Grup. equip. - 4 n'#237'v.)'
                          'CADIMP         5 [   ]             (% de Impostos)'
                          'CADMAT     30758 [   ]             (Materiais [????])'
                          'CADNAT        92 [   ]             (Plano de contas???)'
                          'CADNOP        15 [NAO]             (Natureza opera'#231#227'o)'
                          'CADORC      2764 [NAO] 2002 / 2007 (Orcamento)'
                          'CADPAT       398 [   ] 1999 / .... (PATRIMONIO)'
                          'CADPED      1367 [NAO] 2002 / 2007 (Pedidos???)'
                          'CADPGO       106 [   ]             (Cond. pagto)'
                          'CADRAM        26 [   ]             (Ramos de atividade)'
                          'CADREP         0                                      '
                          'CADREQ         0                                     '
                          'CADSIT        15 [   ]             (Situa'#231#227'o???)'
                          'CADVDE         7 [   ]             (Vendedores)'
                          'CTRAUX     15731 [   ] 2002 / .... (tab aux contrato [NFe?])'
                          'PAGDUP      8013 [   ] 2002 / .... (Contas a pagar???)'
                          'RECDUP     16388 [   ] 2002 / .... (Contas a receber ???)')
                        ParentFont = False
                        TabOrder = 1
                        OnClick = CGDBFClick
                        OnDblClick = CGDBFDblClick
                        UpdType = utYes
                        Value = 0
                        OldValor = 0
                      end
                    end
                  end
                  object TabSheet3: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'SQL'
                    ImageIndex = 1
                    object Splitter1: TSplitter
                      Left = 0
                      Top = 105
                      Width = 1219
                      Height = 6
                      Cursor = crVSplit
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      ExplicitWidth = 1214
                    end
                    object Panel9: TPanel
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 105
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      TabOrder = 0
                      object MeSQL1: TMemo
                        Left = 1
                        Top = 1
                        Width = 1052
                        Height = 103
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -17
                        Font.Name = 'Courier New'
                        Font.Style = []
                        Lines.Strings = (
                          'SELECT * FROM CADCLI'
                          '')
                        ParentFont = False
                        TabOrder = 0
                        OnKeyDown = MeSQL1KeyDown
                      end
                      object Panel8: TPanel
                        Left = 1053
                        Top = 1
                        Width = 165
                        Height = 103
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alRight
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 1
                        object BtExecuta: TBitBtn
                          Tag = 14
                          Left = 10
                          Top = 5
                          Width = 148
                          Height = 49
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = '&Executa'
                          NumGlyphs = 2
                          TabOrder = 0
                          OnClick = BtExecutaClick
                        end
                      end
                    end
                    object DBGrid2: TDBGrid
                      Left = 0
                      Top = 111
                      Width = 1219
                      Height = 352
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      DataSource = DsPsq
                      TabOrder = 1
                      TitleFont.Charset = ANSI_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -14
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                    end
                  end
                  object TabSheet4: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Erros Endere'#231'os'
                    ImageIndex = 2
                    object Memo1: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 463
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                  object TabSheet5: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Erros Loca'#231#245'es'
                    ImageIndex = 3
                    object Memo2: TMemo
                      Left = 0
                      Top = 0
                      Width = 1219
                      Height = 463
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      TabOrder = 0
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 606
    Width = 1241
    Height = 71
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 31
        Width = 1237
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 677
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 330
        Top = 5
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Registros Tb:'
      end
      object Label2: TLabel
        Left = 442
        Top = 5
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Registros:'
      end
      object Label6: TLabel
        Left = 192
        Top = 5
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tabela:'
      end
      object SpeedButton1: TSpeedButton
        Left = 290
        Top = 26
        Width = 29
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '>'
        OnClick = SpeedButton1Click
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdRegistrosTb: TdmkEdit
        Left = 330
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdRegistrosQr: TdmkEdit
        Left = 443
        Top = 25
        Width = 99
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTabela: TdmkEdit
        Left = 192
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'CADCLI.DBF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'CADCLI.DBF'
        ValWarn = False
        OnChange = EdTabelaChange
      end
      object Button1: TButton
        Left = 689
        Top = 15
        Width = 93
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Situa'#231#227'o'
        TabOrder = 4
        OnClick = Button1Click
      end
    end
  end
  object DsDBF: TDataSource
    Left = 496
    Top = 12
  end
  object DsCadCli: TDataSource
    Left = 556
    Top = 12
  end
  object DsPsq: TDataSource
    Left = 468
    Top = 40
  end
  object QrPsqEnt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE Antigo=:P0')
    Left = 496
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPsqEntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCadFor: TDataSource
    Left = 556
    Top = 40
  end
  object DsCadGru: TDataSource
    Left = 612
    Top = 12
  end
  object DataSource1: TDataSource
    Left = 612
    Top = 40
  end
  object QrExiste: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1 ID'
      'FROM gragru1'
      'WHERE Referencia=:P0')
    Left = 320
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraGruX, AGRPAT'
      'FROM gragxpatr ')
    Left = 364
    Top = 380
    object QrGraGXPatrAGRPAT: TWideStringField
      FieldName = 'AGRPAT'
      Required = True
      Size = 25
    end
    object QrGraGXPatrGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DataSource2: TDataSource
    Left = 668
    Top = 40
  end
  object DsCadVde: TDataSource
    Left = 668
    Top = 12
  end
  object QrGraFabMar: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM grafabmar'
      'WHERE Nome=:P0')
    Left = 408
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraFabMarControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSituacao: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Situacao, '
      'CHAR(Situacao) SIGLA,'
      'COUNT(Situacao) ITENS'
      'FROM gragxpatr'
      'GROUP BY Situacao'
      'ORDER BY ITENS DESC')
    Left = 444
    Top = 380
    object QrSituacaoSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrSituacaoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Required = True
      Size = 4
    end
    object QrSituacaoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DataSource3: TDataSource
    Left = 724
    Top = 12
  end
  object DataSource4: TDataSource
    Left = 724
    Top = 40
  end
  object DataSource5: TDataSource
    Left = 780
    Top = 12
  end
  object DataSource6: TDataSource
    Left = 780
    Top = 40
  end
end
