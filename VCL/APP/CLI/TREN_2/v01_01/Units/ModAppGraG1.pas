unit ModAppGraG1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, dmkEdit,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables,
  dmkGeral, MyDBCheck, UnDmkEnums, UnMyObjects;

type
  TDfModAppGraG1 = class(TForm)
    QrGraGXComEstq: TMySQLQuery;
    QrGraGXComEstqNome: TWideStringField;
    QrGraGXComEstqGraGruY: TIntegerField;
    QrGraGXComEstqGraGruX: TIntegerField;
    QrGraGXComEstqComplem: TWideStringField;
    QrGraGXComEstqAquisData: TDateField;
    QrGraGXComEstqAquisDocu: TWideStringField;
    QrGraGXComEstqAquisValr: TFloatField;
    QrGraGXComEstqSituacao: TWordField;
    QrGraGXComEstqAtualValr: TFloatField;
    QrGraGXComEstqValorMes: TFloatField;
    QrGraGXComEstqValorQui: TFloatField;
    QrGraGXComEstqValorSem: TFloatField;
    QrGraGXComEstqValorDia: TFloatField;
    QrGraGXComEstqAgrupador: TIntegerField;
    QrGraGXComEstqMarca: TIntegerField;
    QrGraGXComEstqModelo: TWideStringField;
    QrGraGXComEstqSerie: TWideStringField;
    QrGraGXComEstqVoltagem: TWideStringField;
    QrGraGXComEstqPotencia: TWideStringField;
    QrGraGXComEstqCapacid: TWideStringField;
    QrGraGXComEstqVendaData: TDateField;
    QrGraGXComEstqVendaDocu: TWideStringField;
    QrGraGXComEstqVendaValr: TFloatField;
    QrGraGXComEstqVendaEnti: TIntegerField;
    QrGraGXComEstqObserva: TWideStringField;
    QrGraGXComEstqAGRPAT: TWideStringField;
    QrGraGXComEstqCLVPAT: TWideStringField;
    QrGraGXComEstqMARPAT: TWideStringField;
    QrGraGXComEstqAplicacao: TIntegerField;
    QrGraGXComEstqDescrTerc: TWideStringField;
    QrGraGXComEstqValorFDS: TFloatField;
    QrGraGXComEstqDMenos: TSmallintField;
    QrGraGXComEstqItemUnid: TIntegerField;
    QrGraGXComEstqLk: TIntegerField;
    QrGraGXComEstqDataCad: TDateField;
    QrGraGXComEstqDataAlt: TDateField;
    QrGraGXComEstqUserCad: TIntegerField;
    QrGraGXComEstqUserAlt: TIntegerField;
    QrGraGXComEstqAlterWeb: TSmallintField;
    QrGraGXComEstqAWServerID: TIntegerField;
    QrGraGXComEstqAWStatSinc: TSmallintField;
    QrGraGXComEstqAtivo: TSmallintField;
    QrEstq: TMySQLQuery;
    QrEstqEstqSdo: TFloatField;
    QrGraGXPatr: TMySQLQuery;
    QrGraGXPatrEstqSdo: TFloatField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrTXT_MES: TWideStringField;
    QrGraGXPatrTXT_QUI: TWideStringField;
    QrGraGXPatrTXT_SEM: TWideStringField;
    QrGraGXPatrTXT_DIA: TWideStringField;
    QrGraGXPatrNO_SIT: TWideStringField;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    QrGraGXPatrValorFDS: TFloatField;
    QrGraGXPatrDMenos: TIntegerField;
    QrGraGXPatrPatrimonio: TWideStringField;
    QrGraGXPatrNivel2: TIntegerField;
    QrPesqGPR1: TMySQLQuery;
    QrPesqGPR1Referencia: TWideStringField;
    QrPesqGPR1Patrimonio: TWideStringField;
    QrPesqGPR1Controle: TIntegerField;
    QrPesqGPR2: TMySQLQuery;
    QrPesqGPR2Referencia: TWideStringField;
    QrPesqGPR2Patrimonio: TWideStringField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AlteraProdutoTabelaPersonalizada(GraTabApp, Nivel1: Integer;
              Nome: String; QueryNivel: TMySQLQuery; PreCadastra: Boolean = False);
    function  GrandesasInconsistentes(SQL: array of String; DB: TmySQLDataBase):
              Boolean;
    function  InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem, OriCtrl:
              Integer; GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem,
              IPI_pIPI, IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP:
              Integer; SQLType: TSQLType): Boolean;
    function  InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa, nItem:
              Integer): Boolean;
    function  InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag; Codigo:
              Integer; TabLctA: String): Double;
    function  InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean; SQLType:
              TSQLType; Codigo: Integer): Boolean;
    function  PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
              EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL, EdPesoB:
              TdmkEdit): Boolean;
    function  InsAltUsoConsumoCab(Data, DataE, refNFe: String; UpdCodigo, IQ, CI,
              Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF, Serie,
              NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
              ICMS: Double; SQLType: TSQLType; FatID, FatNum,
              Empresa: Integer): Boolean;
  end;

var
  DfModAppGraG1: TDfModAppGraG1;

implementation

uses
  DmkDAC_PF, Module,
  GraGru1;
{$R *.dfm}

{ TFmModAppGraG1 }
procedure TDfModAppGraG1.AlteraProdutoTabelaPersonalizada(GraTabApp,
  Nivel1: Integer; Nome: String; QueryNivel: TMySQLQuery;
  PreCadastra: Boolean = False);
begin
  //Compatibilidade
  //imprimir diferencas de baixa extra
end;

function TDfModAppGraG1.GrandesasInconsistentes(SQL: array of String;
  DB: TmySQLDataBase): Boolean;
var
  SQL_PeriodoVMI: String;
  Forma: Integer;
begin
{20200920 Fazer???!!!
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrErrGrandz, DB, SQL);
  //Geral.MB_SQL(nil, QrErrGrandz);
  if QrErrGrandz.RecordCount > 0 then
  begin
    Geral.MB_Info('QrErrGrandz.SQL.Text:' + sLineBreak + QrErrGrandz.SQL.Text);
    Result := True;
    Forma := MyObjects.SelRadioGroup('Grandezas inesperadas', '', ['Relatório',
    'Grade editável'], 2);
    case Forma of
      0:
      begin
        MyObjects.frxDefineDataSets(frxErrGrandz, [
          DModG.frxDsDono,
          frxDsErrGrandz
        ]);
        MyObjects.frxMostra(DfModAppGraG1.frxErrGrandz, 'Grandezas inesperadas');
      end;
      1: UMedi_PF.MostraFormUnidMedMul(SQL, DB);
    end;
  end;
}
end;

function TDfModAppGraG1.InsAltUsoConsumoCab(Data, DataE, refNFe: String;
  UpdCodigo, IQ, CI, Transportadora, NF, Conhecimento, Pedido, TipoNF, modNF,
  Serie, NF_RP, NF_CC, HowLoad: Integer; Frete, PesoB, PesoL, ValorNF,
  ICMS: Double; SQLType: TSQLType; FatID, FatNum, Empresa: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoCalculaDiferencas(tPag: TAquemPag;
  Codigo: Integer; TabLctA: String): Double;
begin
  Result := 0;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoExcluiItem(FatID, FatNum, Empresa,
  nItem: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoFinalizaEntrada(Pergunta: Boolean;
  SQLType: TSQLType; Codigo: Integer): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.InsAltUsoConsumoIts(FatID, FatNum, Empresa, nItem,
  OriCtrl, GraGruX, Volumes: Integer; PesoVB, PesoVL, ValorItem, IPI_pIPI,
  IPI_vIPI, TotalCusto, TotalPeso: Double; prod_CFOP: Integer;
  SQLType: TSQLType): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

function TDfModAppGraG1.PreparaUpdUsoConsumoCab(FatID, FatNUm, Empresa: Integer;
  EdNF_RP, EdNF_CC, EdConhecimento, EdFrete, EdPesoL,
  EdPesoB: TdmkEdit): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

end.
