unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, Mask, DBCtrls, ImgList,
  contnrs, frxClass, frxDBSet, dmkGeral, Variants, MyDBCheck, dmkDBGrid,
  dmkEdit, OleCtrls, SHDocVw, (*dmkPopOutFntCBox,*) ValEdit, DmkDAC_PF,
  xmldom, XMLIntf, msxmldom, XMLDoc,
  //DMKpnfsConversao,
  UnDmkEnums, Vcl.Imaging.pngimage, UnDmkProcFunc, UnLic_Dmk, UnitWin,
  dmkPageControl, dmkImage, UnGrl_Vars, ShellApi, UnProjGroup_Consts;

const
  TMaxArtes = 127;

type
  (*
  TcpCalc = (cpJurosMes, cpMulta);
  TTipoMaterialNF = (tnfMateriaPrima, tnfUsoEConsumo);
  *)
  //
  TFmPrincipal = class(TForm)
    Timer1: TTimer;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    Memo3: TMemo;
    AdvPMImagem: TPopupMenu;
    MenuItem1: TMenuItem;
    Limpar1: TMenuItem;
    AdvPMMenuCor: TPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    APMExtratos: TPopupMenu;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    ResultadosMensais1: TMenuItem;
    ReceitaseDespesas1: TMenuItem;
    Prestaodecontas1: TMenuItem;
    APMPesquisas: TPopupMenu;
    Emqualquerconta1: TMenuItem;
    Contascontroladas1: TMenuItem;
    Contassazonais1: TMenuItem;
    APMListas: TPopupMenu;
    Avaliaes1: TMenuItem;
    Situaesdemateriais1: TMenuItem;
    MeusServiosNFSe1: TMenuItem;
    PMVerificaBD: TPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasPublicas1: TMenuItem;
    XMLDocument1: TXMLDocument;
    AdvPreviewMenu1: TPopupMenu;
    AdvToolBarPagerNovo: TdmkPageControl;
    AdvToolBarPager11: TTabSheet;
    AdvToolBarPager12: TTabSheet;
    AdvToolBarPager13: TTabSheet;
    AdvPage1: TTabSheet;
    AdvPage2: TTabSheet;
    AdvPage3: TTabSheet;
    AdvPage4: TTabSheet;
    AdvPage5: TTabSheet;
    AdvPage6: TTabSheet;
    AdvPage7: TTabSheet;
    AdvToolBar24: TPanel;
    AGBGraGXOutr: TBitBtn;
    AGBGraGXPatr: TBitBtn;
    BtLocCCon: TBitBtn;
    AdvToolBar17: TPanel;
    AGBFixGereCab: TBitBtn;
    AGBFixGXPatr: TBitBtn;
    AGBFixPecaCad: TBitBtn;
    AGBFixServCad: TBitBtn;
    AdvToolBar23: TPanel;
    BtImpressaoDoPlanoDeContas: TBitBtn;
    BtSaldoContas: TBitBtn;
    BtFinancas: TBitBtn;
    AdvToolBar1: TPanel;
    BtGrupos: TBitBtn;
    BtConjuntos: TBitBtn;
    BtPlanos: TBitBtn;
    BtSubGrupos: TBitBtn;
    BtContas: TBitBtn;
    BtNiveisPlanoContas: TBitBtn;
    AGBLinkConcBco: TBitBtn;
    AdvToolBar6: TPanel;
    AGBPediPrzCab: TBitBtn;
    BtCarteiras: TBitBtn;
    AdvToolBar4: TPanel;
    AGBFinEncerMes: TBitBtn;
    AdvToolBar5: TPanel;
    AdvGlowButton22: TBitBtn;
    AdvGlowButton20: TBitBtn;
    AdvGlowButton36: TBitBtn;
    AdvGlowButton21: TBitBtn;
    AdvToolBar14: TPanel;
    AdvGlowButton3: TBitBtn;
    AdvGlowButton2: TBitBtn;
    AGVContratos: TBitBtn;
    AdvGlowButton23: TBitBtn;
    AdvToolBar3: TPanel;
    AdvGlowButton28: TBitBtn;
    AdvGlowButton30: TBitBtn;
    AdvGlowButton31: TBitBtn;
    AdvToolBar15: TPanel;
    AdvGlowButton34: TBitBtn;
    AdvGlowButton118: TBitBtn;
    AdvGlowButton117: TBitBtn;
    AdvGlowButton105: TBitBtn;
    AdvGlowButton113: TBitBtn;
    GBReduzido: TBitBtn;
    BtListas: TBitBtn;
    AGBMedidas: TBitBtn;
    AGBGraFabMar: TBitBtn;
    AdvToolBar16: TPanel;
    AGBNfe_Pesq: TBitBtn;
    AGBNFeEventos: TBitBtn;
    AGBNFeLEnc: TBitBtn;
    AGBNFeSteps_0: TBitBtn;
    AGBNFeInut: TBitBtn;
    AGBConsultaNFe: TBitBtn;
    AdvToolBar26: TPanel;
    AGBNFeJust: TBitBtn;
    AGBNFeInfCpl: TBitBtn;
    AdvToolBar27: TPanel;
    AGBExportaXML: TBitBtn;
    AGBXML_No_BD: TBitBtn;
    AGBValidaXML: TBitBtn;
    AdvToolBar28: TPanel;
    AGBFisRegCad: TBitBtn;
    AGBModelosImp: TBitBtn;
    AGBCFOP2003: TBitBtn;
    AGBNCMs: TBitBtn;
    AdvToolBar19: TPanel;
    AGBNFSe_RPSPesq: TBitBtn;
    AGBNFSe_NFSePesq: TBitBtn;
    AGBNFSeLRpsC: TBitBtn;
    AGBNFSeFatCab: TBitBtn;
    AGBNFSe_Edit: TBitBtn;
    AdvToolBar20: TPanel;
    AGBNFSeMenCab: TBitBtn;
    AGBNFSeSrvCad: TBitBtn;
    AdvToolBar18: TPanel;
    AGB_DTB_BACEN: TBitBtn;
    AGBErrosAlertas: TBitBtn;
    AdvGlowButton24: TBitBtn;
    AGBListServ: TBitBtn;
    AdvToolBar22: TPanel;
    AGBPesquisas: TBitBtn;
    AGBExtratos: TBitBtn;
    AdvGlowButton9: TBitBtn;
    AdvGlowButton16: TBitBtn;
    AdvToolBar7: TPanel;
    AdvGlowButton18: TBitBtn;
    AdvGlowButton92: TBitBtn;
    AGBImpGXPatr: TBitBtn;
    AdvToolBar8: TPanel;
    AGMBBackup: TBitBtn;
    AGMBVerifiBD: TBitBtn;
    AdvToolBar9: TPanel;
    AGBOpcoes: TBitBtn;
    AGBFiliais: TBitBtn;
    AGBOpcoesTRen: TBitBtn;
    AGBMatriz: TBitBtn;
    AdvToolBar10: TPanel;
    BtTema: TBitBtn;
    AdvGlowMenuButton4: TBitBtn;
    AdvGlowMenuButton3: TBitBtn;
    AdvToolBar11: TPanel;
    AdvGlowButton17: TBitBtn;
    AdvToolBar2: TPanel;
    TySuporte: TTrayIcon;
    TmSuporte: TTimer;
    PageControl1: TdmkPageControl;
    BalloonHint1: TBalloonHint;
    AdvGlowButton19: TBitBtn;
    AdvToolBar12: TPanel;
    AdvGlowButton6: TBitBtn;
    AGBCntngnc: TBitBtn;
    AGBNfeCabA: TBitBtn;
    AGBFatPedNFs: TBitBtn;
    AdvToolBar13: TPanel;
    AGBFatDivGer: TBitBtn;
    AGBNFeDesDowC: TBitBtn;
    AGBNFeLoad_Inn: TBitBtn;
    AGBNFeDest: TBitBtn;
    AdvGlowButton48: TBitBtn;
    APServicos: TTabSheet;
    AdvToolBar25: TPanel;
    AGBSrvLCad: TBitBtn;
    AGBFolhaFunci: TBitBtn;
    AGBAgeEqiCab: TBitBtn;
    AGBSrvLPre: TBitBtn;
    AGBSrvLPrmCad: TBitBtn;
    AdvToolBar21: TPanel;
    AGBSrvLOSCab: TBitBtn;
    AGBSrvLFatCab: TBitBtn;
    AGBSrvLHonCab: TBitBtn;
    AGBAgeEntCad: TBitBtn;
    AGBAgeEqiCfg: TBitBtn;
    AdvGlowButton26: TBitBtn;
    AdvGlowButton27: TBitBtn;
    AGBSrvlImp: TBitBtn;
    AdvToolBar29: TPanel;
    AdvGlowButton35: TBitBtn;
    TmVersao: TTimer;
    TimerIdle: TTimer;
    AdvGlowButton25: TBitBtn;
    AdvGlowButton167: TBitBtn;
    sPanel5: TPanel;
    SbLogin: TSpeedButton;
    SbAtualizaERP: TSpeedButton;
    SbVerificaDB: TSpeedButton;
    SbBackup: TSpeedButton;
    SbWSuport: TSpeedButton;
    ImgLogo: TdmkImage;
    SbPopupGeral: TSpeedButton;
    SbMinimizaMenu: TSpeedButton;
    LaTopWarn1: TLabel;
    LaTopWarn2: TLabel;
    Panel2: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    AGBNewFinMigra: TBitBtn;
    Panel32: TPanel;
    Panel33: TPanel;
    BtEntidade: TBitBtn;
    BtEntiStatus: TBitBtn;
    BtEntiTipDoc: TBitBtn;
    BtGraGruEPat: TBitBtn;
    BtFeraidos: TBitBtn;
    BtGraGXServ: TBitBtn;
    BtGraGXVend: TBitBtn;
    Panel34: TPanel;
    Panel35: TPanel;
    AdvGlowButton119: TBitBtn;
    TabSheet1: TTabSheet;
    Panel36: TPanel;
    Panel37: TPanel;
    BtStqCenCad: TBitBtn;
    BtStqBalcad: TBitBtn;
    BtStqInnCad: TBitBtn;
    Pedidos: TTabSheet;
    Panel38: TPanel;
    AdvGlowButton72: TBitBtn;
    AdvGlowButton74: TBitBtn;
    Panel39: TPanel;
    Panel40: TPanel;
    AdvGlowButton56: TBitBtn;
    AdvGlowButton75: TBitBtn;
    AdvGlowButton76: TBitBtn;
    AdvGlowButton77: TBitBtn;
    AdvGlowButton149: TBitBtn;
    Panel41: TPanel;
    BtWS: TBitBtn;
    BitBtn2: TBitBtn;
    SbEntidade: TSpeedButton;
    SbFinancas: TSpeedButton;
    SbLocCCon: TSpeedButton;
    PMStyles: TPopupMenu;
    BtLocados: TBitBtn;
    SbGraGXVend: TSpeedButton;
    BtCEST: TBitBtn;
    Motivosdetrocadeequipamentos1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    SbGraGXPatr: TSpeedButton;
    Panel42: TPanel;
    Panel43: TPanel;
    BtOldSisOrcamentos: TBitBtn;
    BtOldSisNFe: TBitBtn;
    BitBtn1: TBitBtn;
    Button1: TButton;
    BitBtn3: TBitBtn;
    Panel44: TPanel;
    Panel45: TPanel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BtMMC_Snap_In: TBitBtn;
    BitBtn8: TBitBtn;
    BtOpcoesGrad: TBitBtn;
    SbPsqGGXVen: TSpeedButton;
    N4: TMenuItem;
    Canaisdeatendimento1: TMenuItem;
    Panel46: TPanel;
    BtGraImpLista7: TBitBtn;
    Panel47: TPanel;
    BtLocImpVend: TBitBtn;
    BtRefazGGY: TBitBtn;
    BtGraImpLista1: TBitBtn;
    Panel48: TPanel;
    AGBLaySPEDEFD: TBitBtn;
    AGBTabsSPEDEFD: TBitBtn;
    Panel53: TPanel;
    BitBtn9: TBitBtn;
    PMWebServices: TPopupMenu;
    LoadWebServices1: TMenuItem;
    GerenciarWebServices1: TMenuItem;
    BtCorrecaoSmia: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure frLFamiliasGetValue(const ParName: String;
      var ParValue: Variant);
    procedure frLFamiliasUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure AGMBBackupClick(Sender: TObject);
    procedure BtFinancasClick(Sender: TObject);
    procedure BtConjuntosClick(Sender: TObject);
    procedure BtGruposClick(Sender: TObject);
    procedure BtSubGruposClick(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure BtNiveisPlanoContasClick(Sender: TObject);
    procedure AdvToolBarButton1Click(Sender: TObject);
    procedure AGMBVerifiBDClick(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure AGBOpcoesClick(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure BtTemaClick(Sender: TObject);
    procedure AdvToolBar3Close(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AGBLinkConcBcoClick(Sender: TObject);
    procedure BtCarteirasClick(Sender: TObject);
    procedure AGBNewFinMigraClick(Sender: TObject);
    procedure AGBFinEncerMesClick(Sender: TObject);
    procedure BtSaldoContasClick(Sender: TObject);
    procedure BtImpressaoDoPlanoDeContasClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure ResultadosMensais1Click(Sender: TObject);
    procedure ReceitaseDespesas1Click(Sender: TObject);
    procedure Prestaodecontas1Click(Sender: TObject);
    procedure Emqualquerconta1Click(Sender: TObject);
    procedure Contascontroladas1Click(Sender: TObject);
    procedure Contassazonais1Click(Sender: TObject);
    procedure AdvGlowButton16Click(Sender: TObject);
    procedure AdvGlowButton18Click(Sender: TObject);
    procedure AdvGlowButton92Click(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AdvGlowButton28Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AdvGlowButton31Click(Sender: TObject);
    procedure AdvGlowButton23Click(Sender: TObject);
    procedure AdvGlowButton21Click(Sender: TObject);
    procedure AdvGlowButton20Click(Sender: TObject);
    procedure AdvGlowButton22Click(Sender: TObject);
    procedure AdvGlowButton36Click(Sender: TObject);
    procedure AdvGlowButton117Click(Sender: TObject);
    procedure AdvGlowButton118Click(Sender: TObject);
    procedure AdvGlowButton34Click(Sender: TObject);
    procedure AdvGlowButton105Click(Sender: TObject);
    procedure AdvGlowButton113Click(Sender: TObject);
    procedure GBReduzidoClick(Sender: TObject);
    procedure BtLocCConClick(Sender: TObject);
    procedure AGBOpcoesTRenClick(Sender: TObject);
    procedure AGBGraGXPatrClick(Sender: TObject);
    procedure AGBGraFabMarClick(Sender: TObject);
    procedure AGBGraGXOutrClick(Sender: TObject);
    procedure AGBMedidasClick(Sender: TObject);
    procedure Avaliaes1Click(Sender: TObject);
    procedure Situaesdemateriais1Click(Sender: TObject);
    procedure AGBImpGXPatrClick(Sender: TObject);
    procedure AdvGlowButton17Click(Sender: TObject);
    procedure AGBMatrizClick(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AGBFatDivGerClick(Sender: TObject);
    procedure AGBCntngncClick(Sender: TObject);
    procedure AGBNfeCabAClick(Sender: TObject);
    procedure AGBFatPedNFsClick(Sender: TObject);
    procedure AGBNFeLEncClick(Sender: TObject);
    procedure AGBNfe_PesqClick(Sender: TObject);
    procedure AGBNFeSteps_0Click(Sender: TObject);
    procedure AGBNFeEventosClick(Sender: TObject);
    procedure AGBNFeInutClick(Sender: TObject);
    procedure AGBConsultaNFeClick(Sender: TObject);
    procedure AGBNFeJustClick(Sender: TObject);
    procedure AGBNFeInfCplClick(Sender: TObject);
    procedure AGBXML_No_BDClick(Sender: TObject);
    procedure AGBExportaXMLClick(Sender: TObject);
    procedure AGBValidaXMLClick(Sender: TObject);
    procedure AGBFisRegCadClick(Sender: TObject);
    procedure AGBCFOP2003Click(Sender: TObject);
    procedure AGBNCMsClick(Sender: TObject);
    procedure AGBModelosImpClick(Sender: TObject);
    procedure AGBMoDocFisClick(Sender: TObject);
    procedure AGBNatOperClick(Sender: TObject);
    procedure AGBPediPrzCabClick(Sender: TObject);
    procedure AGVContratosClick(Sender: TObject);
    procedure AGBListServClick(Sender: TObject);
    procedure MeusServiosNFSe1Click(Sender: TObject);
    procedure AGBNFSe_EditClick(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure VerificaTabelasPublicas1Click(Sender: TObject);
    procedure AGBErrosAlertasClick(Sender: TObject);
    procedure AGB_DTB_BACENClick(Sender: TObject);
    procedure AGBNFSeLRpsCClick(Sender: TObject);
    procedure AGBNFSe_RPSPesqClick(Sender: TObject);
    procedure AGBNFSe_NFSePesqClick(Sender: TObject);
    procedure AGBNFSeFatCabClick(Sender: TObject);
    procedure AGBNFSeSrvCadClick(Sender: TObject);
    procedure AGBNFSeMenCabClick(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure AGBFixGereCabClick(Sender: TObject);
    procedure AGBFixGXPatrClick(Sender: TObject);
    procedure AGBFixPecaCadClick(Sender: TObject);
    procedure AGBFixServCadClick(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure BtPlanosClick(Sender: TObject);
    procedure AGBNFeDestClick(Sender: TObject);
    procedure AGBNFeLoad_InnClick(Sender: TObject);
    procedure AGBNFeDesDowCClick(Sender: TObject);
    procedure AdvGlowButton48Click(Sender: TObject);
    procedure AGBSrvLCadClick(Sender: TObject);
    procedure AGBFolhaFunciClick(Sender: TObject);
    procedure AGBAgeEqiCabClick(Sender: TObject);
    procedure AGBSrvLOSCabClick(Sender: TObject);
    procedure AGBSrvLPreClick(Sender: TObject);
    procedure AGBSrvLPrmCadClick(Sender: TObject);
    procedure AGBSrvLFatCabClick(Sender: TObject);
    procedure AGBSrvLHonCabClick(Sender: TObject);
    procedure AGBAgeEntCadClick(Sender: TObject);
    procedure AGBAgeEqiCfgClick(Sender: TObject);
    procedure AdvGlowButton26Click(Sender: TObject);
    procedure AGBSrvlImpClick(Sender: TObject);
    procedure AdvGlowButton35Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure sd1FormSkin(Sender: TObject; aName: string; var DoSkin: Boolean);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AdvGlowButton167Click(Sender: TObject);
    procedure SbLoginClick(Sender: TObject);
    procedure SbAtualizaERPClick(Sender: TObject);
    procedure SbBackupClick(Sender: TObject);
    procedure SbWSuportClick(Sender: TObject);
    procedure SbMinimizaMenuClick(Sender: TObject);
    procedure SbVerificaDBClick(Sender: TObject);
    procedure BtEntiStatusClick(Sender: TObject);
    procedure BtEntiTipDocClick(Sender: TObject);
    procedure BtGraGruEPatClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtFeraidosClick(Sender: TObject);
    procedure BtListasClick(Sender: TObject);
    procedure BtGraGXServClick(Sender: TObject);
    procedure BtGraGXVendClick(Sender: TObject);
    procedure AdvGlowButton119Click(Sender: TObject);
    procedure BtStqBalcadClick(Sender: TObject);
    procedure BtStqInnCadClick(Sender: TObject);
    procedure AdvGlowButton74Click(Sender: TObject);
    procedure AdvGlowButton72Click(Sender: TObject);
    procedure AdvGlowButton77Click(Sender: TObject);
    procedure AdvGlowButton149Click(Sender: TObject);
    procedure BtWSClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure SbFinancasClick(Sender: TObject);
    procedure SbLocCConClick(Sender: TObject);
    procedure BtLocadosClick(Sender: TObject);
    procedure SbGraGXVendClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtCESTClick(Sender: TObject);
    procedure Motivosdetrocadeequipamentos1Click(Sender: TObject);
    procedure SbGraGXPatrClick(Sender: TObject);
    procedure BtOldSisOrcamentosClick(Sender: TObject);
    procedure BtOldSisNFeClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtMMC_Snap_InClick(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BtOpcoesGradClick(Sender: TObject);
    procedure SbPsqGGXVenClick(Sender: TObject);
    procedure Canaisdeatendimento1Click(Sender: TObject);
    procedure BtGraImpLista7Click(Sender: TObject);
    procedure BtLocImpVendClick(Sender: TObject);
    procedure BtRefazGGYClick(Sender: TObject);
    procedure BtGraImpLista1Click(Sender: TObject);
    procedure AGBLaySPEDEFDClick(Sender: TObject);
    procedure AGBTabsSPEDEFDClick(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure LoadWebServices1Click(Sender: TObject);
    procedure GerenciarWebServices1Click(Sender: TObject);
    procedure BtCorrecaoSmiaClick(Sender: TObject);
  private
    { Private declarations }
    FALiberar, FAtualizouFavoritos: Boolean;
    //
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    procedure MostraVerifiDB();
    procedure MostraLogoff();
    procedure MostraMatriz();
    procedure MostraFiliais();
    procedure MostraAnotacoes();
    procedure CadastroBancos();
    procedure SkinMenu(Index: integer);
    procedure MostraProtocoOco;
    procedure MostraFormDescanso;
  public
    { Public declarations }
    FTemModuloWEB: Boolean;
    FTipoEntradTitu: String;
    FTipoEntradaDig, FTipoEntradaNFe, FTipoEntradaEFD, FCliIntUnico,
    FLinModErr: Integer;
    //
    Alfabeto: TObjectList;
    //PNG, JoinedPNG: TPngObject;
    FBarraTarefa: Integer;
    FTipoNovoEnti: Integer;
    FEntInt, FSeqJan, FSeqItm: Integer;
    //
    function CalculaTextFontSize(FoNam: String; FoTam: Integer;
             ResTela: Double): Integer;
    function CadastroDeContasSdoSimples(Entidade, Conta: Integer): Boolean;
    procedure ShowHint(Sender: TObject);
    procedure RetornoCNAB;
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure SelecionaImagemdefundo;
    procedure PagarRolarEmissao(Query: TmySQLQuery);
    procedure CriaImpressaoDiversos(Indice: Integer);
    procedure CriaMinhasEtiquetas;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid; Entidade:
              Integer; Aba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    function RecriaTempTable(Tabela: String): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure DefineVarsCliInt(Empresa: Integer);
    procedure AcoesIniciaisDoAplicativo();
    function ColunasDeTexto(FoNome: String; FoSizeT: Integer;
             MaxWidth: Integer; Qry: TMySQLQuery;
             Campo: String; Codigo: Integer): Integer;
    procedure ReCaptionComponentesDeForm(Form: TForm);
    procedure CadastroDeContasNiv();
    procedure CadastroDeProtocolos(Lote: Integer);
    procedure MostraFormContratos(Codigo: Integer);

    // ESTOQUE
    // Grade
    procedure MostraTabePrcCab();
    procedure MostraFormGraGruN();
    procedure MostraFormGraFabCad();
    function  MostraUnidMed(Codigo: Integer): Boolean;
    procedure MostraFormFixGereCab(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; Codigo: Integer);
    procedure MostraFormFixGXPatr(Codigo: Integer);
    procedure MostraFormFixPecaCad(Codigo: Integer);
    procedure MostraFormFixServCad(Codigo: Integer);
    // NFe
    procedure MostraFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda: Integer; ForcaCriarXML: Boolean);
    function CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
             ShowForm: Boolean; IDCtrl: Integer): Boolean;
    // Movimenta��o
{
    procedure MostraTabePrcCab();
}
    procedure MostraFormGraGLSitu(Codigo: Integer);
    // Fim ESTOQUE
    function  AcaoEspecificaDeApp(Servico: String): Boolean;
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
    procedure MyOnHint(Sender: TObject);
    procedure InfoSeqIni(Msg: String);
  end;

var
  FmPrincipal: TFmPrincipal;


const
  CO_ARQLOG = 'Inicializacao';
  FAltLin = CO_POLEGADA / 2.16;

implementation

uses
  UnMyObjects, Module, Entidades, VerifiDB, MyGlyfs, ToolRent_Dmk, UnEntities,
  MyListas, MyVCLSkin, CalcPercent, UnMsgInt, UnGOTOy, UCreate, MultiEtiq,
  Maladireta, EntidadesImp, GetValor, ModuleGeral, Matriz, Cartas, Bancos,
  EmiteCheque_0, Entidade2, Backup3, ChConfigCab, UnMyPrinters,
  GetBiosInformation, UnitMD5, EventosCad, ModuleFin, ModuleLct2, UnFinanceiro,
  Formularios, CentroCusto, LinkRankSkin, NovaVersao, Anotacoes, OpcoesTRen,
  ContasNiv, PreEmail, UnBloquetos, (*DB_Converte_Multi,*) UnBloqGerl_Jan,
  {$IfNDef semEntradaNFe}EntradaCab, {$EndIf}
  UnDmkWeb, CartaG, GraGXPatr, GraFabMar, GraGXOutr, GraGXVend,
  CfgCadLista, GraGLSitu, ImpGXPatr, ParamsEmp, PediPrzCab1, FatDivGer,
  ModuleNFe_0000, ModPediVda, Contratos, ListServ, NFSe_PF_0000,
  VerifiDBTerceiros, NFeLoadTabs, NFSe_0000_Module, NFSe_PF_0201,
  NFSeABRASF_0201, Restaura2, FixGereCab, FixGXPatr, FixPecaCad, FixServCad,
  About, UnFinanceiroJan, NFe_PF, UnGrade_Jan, NFMoDocFis, NatOper, UnSrvL_PF,
  FpFunci, UnEmpg_PF, UnGFat_Jan, UnProtocoUnit, UnPraz_PF, Descanso,
  UnFixBugs, UnGraL_Jan, UnFiscal_PF,
  DB_Converte_Tisolin,
  // Testes
  TesteDadosLoc, NFCeLEnU_0400, NFaEdit_0400_Quiet, NFeXMLGerencia,
  //
  OldSisOrcamento, OldSisNFe, OldSisCompras, UnXXe_PF, UnAppPF,
  SPED_EFD_DownTabs, UnEfdIcmsIpi_PF, Efd_RegObrig, UnApp_Jan;

{$R *.DFM}

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Crias as fun��es de corre��o aqui
  try
    if FixBug = 0 then
      AtualizarOSGruposEOpcoes()
    else if FixBug = 1 then
      AtualizarOSMulServico()
    else if FixBug = 2 then
      AtualizarOSCabAgeEqiCab()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  MLAGeral.CriaLog(CO_ARQLOG, 'Inicio ativa��o form principal.', False);
  APP_LIBERADO := True;
  if not MyObjects.CorIniComponente() then Hide else Show;
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Trim(StatusBar.Panels[3].Text) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    ShowMessage('Vers�o difere do arquivo');
  if not FALiberar then
  Timer1.Enabled := True;
  MLAGeral.CriaLog(CO_ARQLOG, 'Fim ativa��o form principal.', False);
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
{
const
  NomeForm = 'FmToolRent_MLA';
var
  Form : PChar;
begin
  if not dmkPF.AcoesAntesDoFormPrincipal('BlueDer2', 'Blue Derm', 'TFmBlueDerm_dmk', 'Lavender Classico') then Halt(0);

  Form := PChar(NomeForm);
  CreateMutex(nil, False, Form);
  Application.Initialize;
  // ivMsgVersaoDifere
  if CO_VERMCW > CO_VERMLA then
    CO_VERSAO := CO_VERMCW
  else
    CO_VERSAO := CO_VERMLA;
  if CO_VERM28 > CO_VERSAO then
    CO_VERSAO := CO_VERM28;
  //
  TStyleManager.TrySetStyle('Luna');
  Application.Title := 'ToolRent';
  try
    Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.CreateForm(TFmLinkRankSkin, FmLinkRankSkin);
  Application.CreateForm(TDmNFe_0000, DmNFe_0000);
  Application.CreateForm(TDmNFSe_0000, DmNFSe_0000);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TDmBco, DmBco);
  Application.CreateForm(TDfModAppGraG1, DfModAppGraG1);
  Application.CreateForm(TDmLct2, DmLct2);
  Application.CreateForm(TDBloGeren, DBloGeren);
  Application.CreateForm(TDBloqGerl, DBloqGerl);
  Application.CreateForm(TDmProd, DmProd);
  Application.CreateForm(TDmFatura, DmFatura);
  //Application.CreateForm(TDmModAgenda, DmModAgenda);
  Application.CreateForm(TFmToolrent_Dmk, FmToolrent_Dmk);
  except
    Application.Terminate;
    Exit;
  end;
  Application.Run;
end.
}
var
  MenuStyle: Integer;
  Retorno, Imagem: String;
begin
  VAR_USA_FRX_IMPR_Recibo := True;
  VAR_USA_PED_SIT := True;
  VAR_APP_EMPARELHA_CODIGO_DO_GG1_COM_O_CONTROLE_DO_GGX := True;
  VAR_APP_HABILITADO_IMPRESSAO_MARCA_OU_REFERENCIA := True;
  VAR_INFO_DATA_HOJE := True;
  FAtualizouFavoritos := False;
  //
  //VAR_AdvToolBarPagerPrincipal := AdvToolBarPager1;
  AdvToolBarPagerNovo.ActivePageIndex := 0;
  //
  VAR_TemContratoMensalidade_FldCodigo := 'Codigo';
  VAR_TemContratoMensalidade_FldNome := 'Nome';
  VAR_TemContratoMensalidade_TabNome := 'contratos';
  //
  // Ini 2020-09-24
  VAR_USA_IDX_REF_NO_GG1 := True;
  VAR_USA_IDX_REF_NO_GG1 := False;
  // Fim 2020-09-24
  VAR_FIN_SELFG_000_CLI := 'Cliente';
  VAR_FIN_SELFG_000_FRN := 'Fornecedor';
  VAR_LCT := '';
  Application.OnException := MyObjects.MostraErro;
  VAR_MULTIPLAS_TAB_LCT := True;
  //VAR_TYPE_LOG          := ttlCliIntUni;
  VAR_TYPE_LOG          := ttlFiliLog;
  VAR_ADMIN             := 'admin';
  FEntInt := -11;
  Alfabeto := TObjectList.Create;
  //VAR_CALCULADORA_COMPONENTCLASS := TFmCalculadora;
  //VAR_CALCULADORA_REFERENCE      := FmCalculadora;
  VAR_USA_TAG_BITBTN := True;
  //
  FTipoNovoEnti := 0;
  MLAGeral.CriaLog(CO_ARQLOG, 'Inicio cria��o form principal.', True);
  VAR_STDATALICENCA := StatusBar.Panels[07];
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_STLOGIN       := StatusBar.Panels[01];
  //VAR_SD1           := sd1;
  StatusBar.Panels[3].Text := Geral.FileVerInfo(Application.ExeName, 3);
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;

  Application.OnMessage := MyObjects.FormMsg;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;

  MLAGeral.CriaLog(CO_ARQLOG, 'Vari�veis iniciais configuradas.', False);
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  MLAGeral.CriaLog(CO_ARQLOG, 'Popup geral definido.', False);
  PageControl1.ActivePageIndex := 0;
  MLAGeral.CriaLog(CO_ARQLOG, 'Pagina inicial do pagecontrol definida.', False);
  //////////////////////////////////////////////////////////////////////////////
  // Local
  VAR_IMPCHEQUE       := Geral.ReadAppKeyCU('ImpCheque', 'Dermatek', ktInteger, 0);
  VAR_IMPCHEQUE_PORTA := Geral.ReadAppKeyCU('ImpCheque_Porta', 'Dermatek', ktString, 'COM2');
  // Servidor
  VAR_IMPCH_IP        := Geral.ReadAppKeyCU('ImpreCh_IP', 'Dermatek', ktString, '127.0.0.1');
  VAR_IMPCHPORTA      := Geral.ReadAppKeyCU('ImpreChPorta', 'Dermatek', ktInteger, 9520);
  //
  MLAGeral.CriaLog(CO_ARQLOG, 'Verificando impressora de cheque', False);
  MyPrinters.VerificaImpressoraDeCheque(Retorno);
  if Retorno <> '' then
    Geral.MensagemBox(Retorno, 'Aviso', MB_OK+MB_ICONWARNING);
  //
  MyObjects.CopiaItensDeMenu(PMGeral, FmPrincipal);
  AdvToolBarPagerNovo.Visible := True;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso, PageControl1);
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DModG <> nil then
    DModG.MostraBackup3(False);
  Application.Terminate;
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
  except
    Result := False
  end;
end;

procedure TFmPrincipal.Prestaodecontas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashPreCta(0);
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AdvGlowButton105Click(Sender: TObject);
begin
  Grade_Jan.MostraFormPrdGrupTip(0);
end;

procedure TFmPrincipal.BtNiveisPlanoContasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.BtOldSisNFeClick(Sender: TObject);
begin
  if Dmod.ConectaDBAnt() then
  begin
    Application.CreateForm(TFmOldSisNFe, FmOldSisNFe);
    FmOldSisNFe.ShowModal;
    FmOldSisNFe.Destroy;
  end;
end;

procedure TFmPrincipal.BtOldSisOrcamentosClick(Sender: TObject);
begin
  if Dmod.ConectaDBAnt() then
  begin
    Application.CreateForm(TFmOldSisOrcamento, FmOldSisOrcamento);
    FmOldSisOrcamento.ShowModal;
    FmOldSisOrcamento.Destroy;
  end;
end;

procedure TFmPrincipal.BtOpcoesGradClick(Sender: TObject);
begin
  Grade_Jan.MostraFormOpcoesGrad();
end;

procedure TFmPrincipal.AdvGlowButton113Click(Sender: TObject);
begin
  MostraFormGraGruN();
end;

procedure TFmPrincipal.AdvGlowButton117Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraTamCad(0);
end;

procedure TFmPrincipal.AdvGlowButton118Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraCusPrc(0);
end;

procedure TFmPrincipal.AdvGlowButton119Click(Sender: TObject);
begin
  Grade_Jan.MostraFormStqCenCad(0);
end;

procedure TFmPrincipal.BtContasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.BtCorrecaoSmiaClick(Sender: TObject);
begin
  App_Jan.MostraFormCorrigeSmia();
end;

procedure TFmPrincipal.BtStqBalcadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqBalCad(0);
end;

procedure TFmPrincipal.BtSubGruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.BtGruposClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AGBErrosAlertasClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormTsErrosAlertasCab();
end;

procedure TFmPrincipal.AGBExportaXMLClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeExportaXML();
end;

procedure TFmPrincipal.BtStqInnCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormStqInnCad(0);
end;

procedure TFmPrincipal.AdvGlowButton149Click(Sender: TObject);
begin
  GFat_Jan.MostraFormPediPrzCab1(0);
end;

procedure TFmPrincipal.BtConjuntosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.BtPlanosClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.BtRefazGGYClick(Sender: TObject);
var
  Qry: TmySQLQuery;
  Corda, Msg: String;
  Itens: Integer;
begin
  Msg := '';
  Qry := TmySQLQuery.Create(Dmod);
  try
    UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.Controle, ggx.GraGruY, pat.Aplicacao ',
    'FROM gragxpatr pat ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=pat.GraGruX ',
    'WHERE ( ',
    '  NOT Aplicacao IN (1,2,3) ',
    '  ) OR ( ',
    '  GraGruY / Aplicacao <> 1024 ',
    ') ',
    '']);
    if Itens > 0 then
    begin
      // Parei Aqui! Fazer !!!???
      //
      Msg := Msg + Geral.FF0(Itens) + ' cadastros de patrimonio N�O tiveram seu GGY refeito!' + sLineBreak;
    end;
    //
    //
    UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGruY, otr.Aplicacao, COUNT(ggx.GraGruY) ITENS ',
    'FROM gragxoutr otr ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=otr.GraGruX ',
    'WHERE ( ',
    '  NOT Aplicacao IN (1,2,3) ',
    '  ) OR ( ',
    '  (GraGruY - 3072) / Aplicacao <> 1024 ',
    ') ',
    '']);
    if Itens > 0 then
    begin
      // Parei Aqui! Fazer !!!???
      //
      Msg := Msg + Geral.FF0(Itens) + ' cadastros de materias N�O tiveram seu GGY refeito!' + sLineBreak;
    end;
    //
    UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT 8192 GraGruY, ggx.Controle, gg1.DataCad, gg1.Nome ',
    'FROM gragxvend ven ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=ven.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE GraGruY <> 8192 ',
    'ORDER BY ggx.Controle ',
    ' ']);
    Itens := Qry.RecordCount;
    if Itens > 0 then
    begin
      UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GROUP_CONCAT(ggx.Controle) Controles',
      'FROM gragxvend ven ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=ven.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE GraGruY <> 8192 ',
      'ORDER BY ggx.Controle ',
      ' ']);
      Corda := Qry.Fields[0].AsString;
      //
      Dmod.MyDB.Execute(
        'UPDATE gragrux SET GraGruY=8192 WHERE Controle IN (' + Corda + ')');
      Msg := Msg + Geral.FF0(Itens) + ' cadastros de venda tiveram seu GGY refeito!' + sLineBreak;
    end;
    //
    //
    UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT 7168 GraGruY, ggx.Controle, gg1.DataCad, gg1.Nome ',
    'FROM gragxserv srv ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=srv.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE GraGruY <> 7168 ',
    'ORDER BY ggx.Controle ',
    ' ']);
    Itens := Qry.RecordCount;
    if Itens > 0 then
    begin
      UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GROUP_CONCAT(ggx.Controle) Controles',
      'FROM gragxserv srv ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=srv.GraGruX ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'WHERE GraGruY <> 7168 ',
      'ORDER BY ggx.Controle ',
      ' ']);
      Corda := Qry.Fields[0].AsString;
      //
      Dmod.MyDB.Execute(
        'UPDATE gragrux SET GraGruY=7168 WHERE Controle IN (' + Corda + ')');
      Msg := Msg + Geral.FF0(Itens) + ' cadastros de servi�o tiveram seu GGY refeito!' + sLineBreak;
    end;
    //
    if Msg = '' then
      Msg := 'Nenhum item precisa ter seu GGY refeito!';
    //
    Geral.MB_Info(Msg);
  finally
     Qry.Free;
  end;
end;

procedure TFmPrincipal.AdvGlowButton167Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton16Click(Sender: TObject);
begin
  FinanceiroJan.MostraSaldos(0);
end;

procedure TFmPrincipal.AdvGlowButton17Click(Sender: TObject);
begin
  Dmod.VerificaSituacaoPatrimonio(1);
end;

procedure TFmPrincipal.AGBOpcoesClick(Sender: TObject);
begin
  Entities.MostraFormOpcoes();
end;

procedure TFmPrincipal.AGBOpcoesTRenClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmOpcoesTRen, FmOpcoesTRen, afmoNegarComAviso) then
  begin
    FmOpcoesTRen.ShowModal;
    FmOpcoesTRen.Destroy;
  end;
end;

procedure TFmPrincipal.AGBPediPrzCabClick(Sender: TObject);
begin
  Praz_PF.MostraFormPediPrzCab1(0);
end;

procedure TFmPrincipal.AGBSrvLOSCabClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLOSCab(0);
end;

procedure TFmPrincipal.AGBSrvLCadClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLCad(0);
end;

procedure TFmPrincipal.AGBSrvLFatCabClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLFatCad(0);
end;

procedure TFmPrincipal.AGBSrvLHonCabClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLHonCad(0);
end;

procedure TFmPrincipal.AGBSrvlImpClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLImp(0);
end;

procedure TFmPrincipal.AGBSrvLPreClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLPOCab(0);
end;

procedure TFmPrincipal.AGBSrvLPrmCadClick(Sender: TObject);
begin
  SrvL_PF.MostraFormSrvLPrmCad(0);
end;

procedure TFmPrincipal.AGBTabsSPEDEFDClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPED_EFD_DownTabs, FmSPED_EFD_DownTabs, afmoNegarComAviso) then
  begin
    FmSPED_EFD_DownTabs.ShowModal;
    FmSPED_EFD_DownTabs.Destroy;
  end;
end;

procedure TFmPrincipal.AGBValidaXMLClick(Sender: TObject);
begin
  UnNFe_PF.ValidaXML_NFe('');
end;

procedure TFmPrincipal.AGBXML_No_BDClick(Sender: TObject);
begin
  DmNFe_0000.AtualizaXML_No_BD_Tudo(True);
end;

procedure TFmPrincipal.AGB_DTB_BACENClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFeLoadTabs, FmNFeLoadTabs, afmoNegarComAviso) then
  begin
   FmNFeLoadTabs.ShowModal;
   FmNFeLoadTabs.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton18Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidadesImp, FmEntidadesImp, afmoNegarComAviso) then
  begin
    FmEntidadesImp.ShowModal;
    FmEntidadesImp.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.BtEntidadeClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := False;
  MyObjects.FormTDICria(TFmEntidade2, PageControl1, AdvToolBarPagerNovo, True, True);
end;

procedure TFmPrincipal.AdvGlowButton20Click(Sender: TObject);
begin
  UBloqGerl_Jan.CadastroCNAB_Cfg;
end;

procedure TFmPrincipal.AdvGlowButton21Click(Sender: TObject);
begin
  UBloquetos.MostraBloArre(0, 0);
end;

procedure TFmPrincipal.AdvGlowButton22Click(Sender: TObject);
begin
  UBloquetos.MostraBloGeren(-1, 0, 0, 0, PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AdvGlowButton23Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton26Click(Sender: TObject);
begin
  SrvL_PF.MostraFormStatusOSs();
end;

procedure TFmPrincipal.AGBCntngncClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCntngnc();
end;

procedure TFmPrincipal.AGBConsultaNFeClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeConsulta();
end;

procedure TFmPrincipal.AdvGlowButton28Click(Sender: TObject);
begin
  CadastroDeProtocolos(0);
end;

procedure TFmPrincipal.BtFinancasClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := False;
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  CadastroBancos();
end;                              

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocoMot;
end;

procedure TFmPrincipal.AdvGlowButton31Click(Sender: TObject);
begin
  MostraProtocoOco;
end;

procedure TFmPrincipal.AdvGlowButton34Click(Sender: TObject);
begin
  MostraTabePrcCab();
end;

procedure TFmPrincipal.AdvGlowButton35Click(Sender: TObject);
begin
  FinanceiroJan.MostraFinancas(PageControl1, AdvToolBarPagerNovo, False);
end;

procedure TFmPrincipal.AdvGlowButton36Click(Sender: TObject);
begin
  UBloquetos.CadastroDeBloOpcoes;
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvGlowButton48Click(Sender: TObject);
begin
  //UnNFe_PF.MostraFormLayoutNFe();
  UnNFe_PF.MostraFormNFeNewVer();
end;

procedure TFmPrincipal.BtImpressaoDoPlanoDeContasClick(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.BtSaldoContasClick(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AdvGlowButton72Click(Sender: TObject);
begin
  GFat_Jan.MostraFormPediVdaImp();
end;

procedure TFmPrincipal.AdvGlowButton74Click(Sender: TObject);
begin
  GFat_Jan.MostraFormPediVda();
end;

procedure TFmPrincipal.AdvGlowButton77Click(Sender: TObject);
begin
  GFat_Jan.MostraFormTabePrcCab(0);
end;

procedure TFmPrincipal.BtTemaClick(Sender: TObject);
begin
//  FmLinkRankSkin.Show;
  if PMStyles.Items.Count < 2 then
    MyObjects.StylesListRefreshMenu(PMStyles);
  //
  MyObjects.MostraPopUpDeBotao(PMStyles, BtTema);
end;

procedure TFmPrincipal.BtCarteirasClick(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.BtCESTClick(Sender: TObject);
begin
  Fiscal_PF.MostraFormCEST(0);
end;

procedure TFmPrincipal.AdvGlowButton92Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMalaDireta, FmMalaDireta, afmoNegarComAviso) then
  begin
    FmMalaDireta.ShowModal;
    FmMalaDireta.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  FinanceiroJan.MostraCashBal(0);
end;

procedure TFmPrincipal.AdvToolBar3Close(Sender: TObject);
begin
  MostraAnotacoes();
end;

procedure TFmPrincipal.AdvToolBarButton1Click(Sender: TObject);
begin
  MostraVerifiDB();
end;

procedure TFmPrincipal.AGBAgeEntCadClick(Sender: TObject);
begin
  SrvL_PF.MostraFormAgeEntCad(0);
end;

procedure TFmPrincipal.AGBAgeEqiCabClick(Sender: TObject);
begin
  Empg_PF.MostraFormAgeEqiCab(0);
end;

procedure TFmPrincipal.AGBAgeEqiCfgClick(Sender: TObject);
begin
  Empg_PF.MostraFormAgeEqiCfg(0);
end;

procedure TFmPrincipal.AGBCFOP2003Click(Sender: TObject);
begin
  Grade_Jan.MostraFormCFOP2003();
end;

procedure TFmPrincipal.AGBFatDivGerClick(Sender: TObject);
begin
  GFat_Jan.MostraFormFatDivGer();
end;

procedure TFmPrincipal.AGBFatPedNFsClick(Sender: TObject);
const
  Cliente       = 0;
  CU_PediVda    = 0;
  ForcaCriarXML = False;
var
  EMP_FILIAL: Integer;
begin
  EMP_FILIAL := DmodG.QrFiliLogFilial.Value;
  //
  UnNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda, ForcaCriarXML);
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  MostraFiliais();
end;

procedure TFmPrincipal.AGBFinEncerMesClick(Sender: TObject);
begin
  FinanceiroJan.MostraLctEncerraMes;
end;

procedure TFmPrincipal.AGBFisRegCadClick(Sender: TObject);
begin
  Grade_Jan.MostraFormFisRegCad(0);
end;

procedure TFmPrincipal.AGBFixGereCabClick(Sender: TObject);
begin
  MostraFormFixGereCab(True, PageControl1, AdvToolBarPagerNovo, 0);
end;

procedure TFmPrincipal.AGBFixGXPatrClick(Sender: TObject);
begin
  MostraFormFixGXPatr(0);
end;

procedure TFmPrincipal.AGBFixPecaCadClick(Sender: TObject);
begin
  MostraFormFixPecaCad(0);
end;

procedure TFmPrincipal.AGBFixServCadClick(Sender: TObject);
begin
  MostraFormFixServCad(0);
end;

procedure TFmPrincipal.AGBFolhaFunciClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFpFunci, FmFpFunci, afmoNegarComAviso) then
  begin
    FmFpFunci.ShowModal;
    FmFpFunci.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGraFabMarClick(Sender: TObject);
begin
//  MostraFormGraFabCad();
  GraL_Jan.MostraFormMarcasSimples;
end;

procedure TFmPrincipal.AGBGraGXOutrClick(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXOutr(0);
end;

procedure TFmPrincipal.AGBGraGXPatrClick(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXPatr(0);
end;

procedure TFmPrincipal.AGBImpGXPatrClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmImpGXPatr, FmImpGXPatr, afmoNegarComAviso) then
  begin
    FmImpGXPatr.ShowModal;
    FmImpGXPatr.Destroy;
  end;
end;

procedure TFmPrincipal.AGBLaySPEDEFDClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Ser�o baixadas e atualizadas as tabelas do layout do SPED EFD.' +
  'Tem certeza que deseja continuar?') =
  ID_YES then
  begin
    Memo3.Visible := True;
    Memo3.Lines.Clear;
    EfdIcmsIpi_PF.BaixaLayoutSPED(Memo3);
    Memo3.Visible := False;
  end;
end;

procedure TFmPrincipal.AGBLinkConcBcoClick(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AGBListServClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormListServ(0);
end;

procedure TFmPrincipal.BtLocadosClick(Sender: TObject);
begin
  GraL_Jan.MostraFormLocados();
end;

procedure TFmPrincipal.BtLocCConClick(Sender: TObject);
const
  Codigo = 0;
begin
  AdvToolBarPagerNovo.Visible := False;
  GraL_Jan.MostraFormLocCCon(True, PageControl1, AdvToolBarPagerNovo, Codigo);
end;

procedure TFmPrincipal.BtLocImpVendClick(Sender: TObject);
begin
  GraL_Jan.MostraFormLocImpVend();
end;

procedure TFmPrincipal.BtMMC_Snap_InClick(Sender: TObject);
begin
  UnWin.MostraMMC_SnapIn();
end;

procedure TFmPrincipal.AGBMatrizClick(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AGBMedidasClick(Sender: TObject);
begin
  MostraUnidMed(0);
end;

procedure TFmPrincipal.AGBModelosImpClick(Sender: TObject);
begin
  Grade_Jan.MostraFormModelosImp();
end;

procedure TFmPrincipal.AGBMoDocFisClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFMoDocFis, FmNFMoDocFis, afmoNegarComAviso) then
  begin
    FmNFMoDocFis.ShowModal;
    FmNFMoDocFis.Destroy;
  end;
end;

procedure TFmPrincipal.AGBNCMsClick(Sender: TObject);
begin
  Grade_Jan.MostraFormNCMs();
end;

procedure TFmPrincipal.AGBNewFinMigraClick(Sender: TObject);
begin
{
  DmLct0.MigraLctsParaTabLct();
}


  // DESMARCAR AQUI !!!!!!!!!!!!!
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //




{  Migra��o da AAPA...
  if DBCheck.CriaFm(TFmDB_Converte_Multi, FmDB_Converte_Multi, afmoNegarComAviso) then
  begin
    FmDB_Converte_Multi.ShowModal;
    FmDB_Converte_Multi.Destroy;
  end;

  // ... ap�s a migra��o da AAPA
  if Trim(Dmod.QrMasterCNPJ.Value) = CO_VAZIO then
    GOTOy.AvisoIndef(5);
  //

  // Migra��o do Tisolin. Inicio desenvolvimento 2020-09-20
}
  Application.CreateForm(TFmDB_Converte_Tisolin, FmDB_Converte_Tisolin);
  FmDB_Converte_Tisolin.ShowModal;
  FmDB_Converte_Tisolin.Destroy;
end;

procedure TFmPrincipal.AGBNfeCabAClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCabA();
end;

procedure TFmPrincipal.AGBNFeDesDowCClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeCnfDowC_0100();
end;

procedure TFmPrincipal.AGBNFeDestClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeDistDFeInt();
end;

procedure TFmPrincipal.AGBNFeEventosClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeEveRLoE(0);
end;

procedure TFmPrincipal.AGBNFeInfCplClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInfCpl();
end;

procedure TFmPrincipal.AGBNFeInutClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeInut();
end;

procedure TFmPrincipal.AGBNFeJustClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeJust();
end;

procedure TFmPrincipal.AGBNFeLEncClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLEnc(0);
end;

procedure TFmPrincipal.AGBNFeLoad_InnClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeLoad_Inn();
end;

procedure TFmPrincipal.AGBNFeSteps_0Click(Sender: TObject);
const
  MostraForm = True;
var
  CodStatus: Integer;
  TxtStatus: String;
begin
  UnNFe_PF.NFe_StatusServicoCodMul(Self, CodStatus, TxtStatus);
  Geral.MB_Info(TxtStatus + ' (C�digo ' + Geral.FF0(CodStatus) + ')');
end;

procedure TFmPrincipal.AGBNfe_PesqClick(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFePesq(True, PageControl1, AdvToolBarPagerNovo, 0);
end;

procedure TFmPrincipal.AGBNFSeFatCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(True, PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AGBNFSeLRpsCClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
end;

procedure TFmPrincipal.AGBNFSeMenCabClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(False, nil, nil);
end;

procedure TFmPrincipal.AGBNFSeSrvCadClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.AGBNFSe_EditClick(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, NumNF: Integer;
  SerieNF: String;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  UnNFSe_PF_0201.MostraFormNFSe(SQLType, Prestador, Tomador, Intermediario,
    MeuServico, ItemListSrv, Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico,
    nil, Valor, SerieNF, NumNF, nil);
end;

procedure TFmPrincipal.AGBNFSe_NFSePesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(True, PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AGBNFSe_RPSPesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(True, PageControl1, AdvToolBarPagerNovo);
end;

procedure TFmPrincipal.AGMBBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.AGMBVerifiBDClick(Sender: TObject);
begin
  MostraVerifiDB();
end;

procedure TFmPrincipal.AGVContratosClick(Sender: TObject);
begin
  MostraFormContratos(0);
end;

procedure TFmPrincipal.Avaliaes1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'GraGLAval', 60, ncGerlSeq1,
    'Avalia��es de Materiais', [], False, Null, [], [], False);
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  Application.CreateForm(TFmTesteDadosLoc, FmTesteDadosLoc);
  FmTesteDadosLoc.ShowModal;
  FmTesteDadosLoc.Destroy;
end;

procedure TFmPrincipal.BitBtn2Click(Sender: TObject);
//function TNFCe_PF.MostraFormNFCeLEnU(): Boolean;
{
var
  VersaoNFCe: Integer;
}
begin
{
  VersaoNFCe := VersaoNFCeEmUso();
  //
  //FNaoCriouXML := False;
  //
  Result := False;
  case VersaoNFCe of
    400:
}
    if DBCheck.CriaFm(TFmNFCeLEnU_0400, FmNFCeLEnU_0400, afmoNegarComAviso) then
    begin
      (*
      FmNFCeLEnU_0400.Show;
      Result := FmNFCeLEnU_0400.EnviarNFe();
      if Result then
        FmNFCeLEnU_0400.Destroy;
      *)
      FmNFCeLEnU_0400.EdFatID.ValueVariant   := 2;
      FmNFCeLEnU_0400.EdFatNum.ValueVariant  := 14;
      FmNFCeLEnU_0400.EdEmpresa.ValueVariant := -11;
      FmNFCeLEnU_0400.EdIDCtrl.ValueVariant := 2699;
      FmNFCeLEnU_0400.ShowModal;
      FmNFCeLEnU_0400.Destroy;
    end;
(*
    else AvisoNaoImplemVerNFCe(VersaoNFCe);
  end;
*)
end;


procedure TFmPrincipal.BitBtn3Click(Sender: TObject);
begin
  if Dmod.ConectaDBAnt() then
  begin
    Application.CreateForm(TFmOldSisCompras, FmOldSisCompras);
    FmOldSisCompras.ShowModal;
    FmOldSisCompras.Destroy;
  end;
(*
SELECT cli.RAZAOSOCIAL, cab.FORNECEDOR,
cab.NOTAFISCAL, cab.DATANF, cab.ENTRADA,
its.PRODUTO, its.QUANTIDADE, its.PRECO,
its.CUSTO
FROM compras cab
LEFT JOIN comprasitens its
  ON  cab.FORNECEDOR=its.FORNECEDOR
  AND cab.NOTAFISCAL=its.NOTAFISCAL
  AND cab.MODELO=its.Modelo
  AND cab.SERIE=its.Serie
LEFT JOIN cliente cli ON cli.CLIENTE=cab.Fornecedor
WHERE its.Produto=3155
ORDER BY cab.ENTRADA, cab.FORNECEDOR,
cab.NOTAFISCAL, its.SEQUENCIA
*)
end;

procedure TFmPrincipal.BitBtn5Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruEPat(0);
end;

procedure TFmPrincipal.BitBtn6Click(Sender: TObject);
begin
  GraL_Jan.MostraFormStqAlocGer(0, -1);
end;

procedure TFmPrincipal.BitBtn8Click(Sender: TObject);
begin
  NFeXMLGeren.AutoridadeCertificadoraRaizBrasileiraV5();
end;

procedure TFmPrincipal.BitBtn9Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmEfd_RegObrig, FmEfd_RegObrig, afmoNegarComAviso) then
  begin
    FmEfd_RegObrig.ShowModal;
    FmEfd_RegObrig.Destroy;
  end;
end;

procedure TFmPrincipal.BtEntiStatusClick(Sender: TObject);
begin
{
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'entistatus', 255, ncControle,
  'Status e Alertas de Entidades',
  [], False, Null, [], [], False);
}
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'statusenti', 255, ncControle,
  'Status e Alertas de Entidades',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.BtEntiTipDocClick(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'entitipdoc', 60, ncControle,
  'Tipos de Documentos',
  [], False, Null, [], [], False);
end;

procedure TFmPrincipal.BtFeraidosClick(Sender: TObject);
begin
  GraL_Jan.MostraFormFeriados();
end;

procedure TFmPrincipal.BtGraGruEPatClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruEPat(0);
end;

procedure TFmPrincipal.BtGraGXServClick(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXServ(0);
end;

procedure TFmPrincipal.BtGraGXVendClick(Sender: TObject);
begin
  GraL_Jan.MostraFormGraGXVend(0);
end;

procedure TFmPrincipal.BtGraImpLista1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraImpLista(0, 0, 1);
end;

procedure TFmPrincipal.BtGraImpLista7Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraImpLista(0, 0, 7);
end;

procedure TFmPrincipal.BtListasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(APMListas, BtListas);
end;

procedure TFmPrincipal.BtWSClick(Sender: TObject);
begin
  //UnNFe_PF.MostraFormNFeWebservices();
  MyObjects.MostraPopupdeBotao(PMWebservices, BtWS);
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  FmMyGlyfs.ExportaGlyfs(FmMyGlyfs.Lista_32x64A, nil, LaTopWarn1, LaTopWarn2);
  FmMyGlyfs.ExportaGlyfs(FmMyGlyfs.Lista_32x64B, nil, LaTopWarn1, LaTopWarn2);
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmToolRent_Dmk.Show;
  Enabled := False;
  FmToolRent_Dmk.Refresh;
  FmToolRent_Dmk.EdSenha.Text := FmToolRent_Dmk.EdSenha.Text+'*';
  FmToolRent_Dmk.EdSenha.Refresh;
  FmToolRent_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  try
    Application.CreateForm(TDmPediVda, DmPediVda);
  except
    Application.MessageBox(PChar('Imposs�vel criar M�dulo de vendas'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmToolRent_Dmk.EdSenha.Text := FmToolRent_Dmk.EdSenha.Text+'*';
  FmToolRent_Dmk.EdSenha.Refresh;
  FmToolRent_Dmk.ReloadSkin;
  FmToolRent_Dmk.EdLogin.Text := '';
  FmToolRent_Dmk.EdSenha.Text := '';
  FmToolRent_Dmk.EdSenha.Refresh;
  FmToolRent_Dmk.EdLogin.ReadOnly := False;
  FmToolRent_Dmk.EdSenha.ReadOnly := False;
  FmToolRent_Dmk.EdLogin.SetFocus;
  //FmToolRent_Dmk.ReloadSkin;
  FmToolRent_Dmk.Refresh;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
begin
  if DModG <> nil then
    DmodG.ExecutaPing(FmToolrent_Dmk, [Dmod.MyDB, DModG.MyPID_DB, DModG.AllID_DB]);
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    (*AdvToolBarButton13*)SbWSuport, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo((*AdvToolBarButton8*)SbAtualizaERP, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  MostraVerifiDB();
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Int64;
  ArqNome: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'ToolRent',
    'ToolRent', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, ArqNome, False,
    ApenasVerifica, BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasPublicas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroBancos;
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  ShowMessage('Faturando n�o implementado');
  Result := 0;
end;

function TFmPrincipal.ColunasDeTexto(FoNome: String; FoSizeT: Integer;
MaxWidth: Integer; Qry: TMySQLQuery; Campo: String; Codigo: Integer): Integer;
var
  MeLinha1, MeLinha2: TMemo;
  R: Integer;
begin
  Result := 1;
  MeLinha1 := TMemo.Create(FmPrincipal);
  MeLinha1.Visible := False;
  MeLinha1.Update;
  MeLinha1.Parent := FmPrincipal;
  // Evitar quebra de linha antecipada!
  MeLinha1.Width := MaxWidth;
  MeLinha1.Height := Height;
  MeLinha1.Font.Name := FoNome;
  MeLinha1.Font.Size := FoSizeT;
  //
  MeLinha2 := TMemo.Create(FmPrincipal);
  MeLinha2.Visible := False;
  MeLinha2.Update;
  MeLinha2.Parent := FmPrincipal;
  MeLinha2.Width := MaxWidth;
  MeLinha2.Height := Height;
  MeLinha2.WordWrap := False;
  MeLinha2.Font.Name := FoNome;
  MeLinha2.Font.Size := FoSizeT;
  //
  MeLinha1.Lines.Clear;
  MeLinha2.Lines.Clear;
  //
  Qry.Close;
  Qry.Params[0].AsInteger := Codigo;
  Qry.Open;
  while not Qry.Eof do
  begin
    MeLinha1.Lines.Add(Qry.FieldByName(Campo).AsString);
    MeLinha2.Lines.Add(Qry.FieldByName(Campo).AsString);
    //
    Qry.Next;
  end;
  //
  if MeLinha1.Lines.Count = MeLinha2.Lines.Count then
  begin
    R := 1;
    repeat
      R := R + 1;
      MeLinha1.Width := MaxWidth div R;
      MeLinha2.Width := MeLinha1.Width;
    until (MeLinha1.Lines.Count <> MeLinha2.Lines.Count);
    Result := R - 1;
  end;
  MeLinha1.Destroy;
  MeLinha2.Destroy;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  ShowMessage('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal.Contascontroladas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaContasControladas(0);
end;

procedure TFmPrincipal.Contassazonais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraContasSazonais;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

function TFmPrincipal.CriaFormEntradaCab(MaterialNF: TTipoMaterialNF;
  ShowForm: Boolean; IDCtrl: Integer): Boolean;
begin
  Result := False;
  {$IFNDef semEntradaNFe}
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  case MaterialNF of
    tnfMateriaPrima:
    begin
      FTipoEntradaDig := VAR_FATID_0113;
      FTipoEntradaNFE := VAR_FATID_0013;
      FTipoEntradaEFD := VAR_FATID_0213;
      FTipoEntradTitu := 'Mat�ria-prima';
    end;
    tnfUsoEConsumo:
    begin
      FTipoEntradaDig := VAR_FATID_0151;
      FTipoEntradaNFe := VAR_FATID_0051;
      FTipoEntradaEFD := VAR_FATID_0251;
      FTipoEntradTitu := 'Uso e Consumo';
    end;
  end;
  Result := DBCheck.CriaFm(TFmEntradaCab, FmEntradaCab, afmoNegarComAviso);
  if Result and ShowForm then
  begin
    if IDCtrl <> 0 then
    begin
      FmEntradaCab.LocCod(IDCtrl, IDCtrl);
      if FmEntradaCab.QrNFeCabAIDCtrl.Value <> IDCtrl then
        Geral.MensagemBox('N�o foi poss�vel localizar o lan�amento solicitado!',
        'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
    FmEntradaCab.ShowModal;
    FmEntradaCab.Destroy;
  end;
 {$EndIf}
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.PagarReceber1Click(Sender: TObject);
begin
  FinanceiroJan.ExtratosFinanceirosEmpresaUnica();
end;

procedure TFmPrincipal.PagarRolarEmissao(Query: TmySQLQuery);
begin
end;

procedure TFmPrincipal.DefineVarsCliInt(Empresa: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := Empresa;
  DmodG.QrCliIntUni.Open;
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
{:::
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
}
end;

procedure TFmPrincipal.MostraLogoff;
begin
  FmPrincipal.Enabled := False;
  //
  FmToolRent_Dmk.Show;
  FmToolRent_Dmk.EdLogin.Text   := '';
  FmToolRent_Dmk.EdSenha.Text   := '';
  FmToolRent_Dmk.EdLogin.SetFocus;
end;

procedure TFmPrincipal.MostraMatriz;
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.MostraProtocoOco;
begin
  ProtocoUnit.MostraFormProtocoOco;
end;

procedure TFmPrincipal.MostraTabePrcCab();
begin
  GFat_Jan.MostraFormTabePrcCab(0);
end;

function TFmPrincipal.MostraUnidMed(Codigo: Integer): Boolean;
begin
  Grade_Jan.MostraFormUnidMed(0);
end;

procedure TFmPrincipal.MostraVerifiDB();
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.Motivosdetrocadeequipamentos1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'LocTroMotCad', 60, ncGerlSeq1,
    'Motivos de Troca de Equipamentos', [], False, Null, [], [], False);
end;

procedure TFmPrincipal.Movimento1Click(Sender: TObject);
begin
  FinanceiroJan.MostraMovimento(0);
end;

procedure TFmPrincipal.MyOnHint(Sender: TObject);
begin
  {
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
  }
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.CriaImpressaoDiversos(Indice: Integer);
begin
  if DBCheck.CriaFm(TFmFormularios, FmFormularios, afmoNegarComAviso) then
  begin
    FmFormularios.RGRelatorio.ItemIndex := Indice;
    FmFormularios.ShowModal;
    FmFormularios.Destroy;
  end;
end;

procedure TFmPrincipal.CriaMinhasEtiquetas();
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
end;

procedure TFmPrincipal.Emqualquerconta1Click(Sender: TObject);
begin
  FinanceiroJan.MostraPesquisaPorNveldoPLanodeContas(0);
end;

procedure TFmPrincipal.CadastroDeContasNiv;
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

function TFmPrincipal.CalculaTextFontSize(FoNam: String; FoTam: Integer;
  ResTela: Double): Integer;
var
  I, Ini, tOri, tCal: Integer;
begin
  Result := FoTam;
  if ResTela < 100 then
    Ini := 1
  else
    Ini := FoTam;
  //
  Canvas.Font.Name := FoNam;
  Canvas.Font.Size := FoTam;
  tOri := Canvas.TextWidth(TextStringRows);
  for I := Ini to 1024 do
  begin
    Canvas.Font.Size := I;
    tCal := Canvas.TextWidth(TextStringRows);
    if (tCal / tOri * 100) > ResTela then
    begin
      Result := I - 1;
      Break;
    end;
  end;
end;

procedure TFmPrincipal.Canaisdeatendimento1Click(Sender: TObject);
begin
  GraL_Jan.MostraFormAtndCanal();
end;

procedure TFmPrincipal.frLFamiliasGetValue(const ParName: String;
  var ParValue: Variant);
begin
  //
end;

procedure TFmPrincipal.frLFamiliasUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  Val := MLAGeral.MyUserFunction(Name, p1, p2, p3, Val);
end;

procedure TFmPrincipal.AGBNatOperClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNatOper, FmNatOper, afmoNegarComAviso) then
  begin
    FmNatOper.ShowModal;
    FmNatOper.Destroy;
  end;
end;

procedure TFmPrincipal.GBReduzidoClick(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruReduzido;
end;

procedure TFmPrincipal.GerenciarWebServices1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeWebservicesGer();
end;

procedure TFmPrincipal.InfoSeqIni(Msg: String);
begin
  if VAR_MSG_START then
    FmToolrent_Dmk.MeAvisos.Text := Msg + sLineBreak + FmToolrent_Dmk.MeAvisos.Text;
end;

procedure TFmPrincipal.ResultadosMensais1Click(Sender: TObject);
begin
  FinanceiroJan.MostraResultadosMensais;
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade: TStringGrid);
begin
   // Compatibilidade
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Criando Module Geral');
    if DModG <> nil then
    begin
      if not FAtualizouFavoritos then
      begin
        DModG.CriaFavoritos(AdvToolBarPagerNovo, LaTopWarn1, LaTopWarn2, AdvGlowButton25, FmPrincipal);
        //
        FAtualizouFavoritos := True;
      end;
      DModG.MyPID_DB_Cria();
      DefineVarsCliInt(FEntInt);
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      //
      if Pos('WEB', DModG.QrMasterHabilModulos.Value) > 0 then
        FTemModuloWEB := True
      else
        FTemModuloWEB := False;
      //
      //  Descanso
      MostraFormDescanso();
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmodG.VerificaHorVerao();
      //
      MyObjects.Informa2(LaTopWarn1, LaTopWarn2, True, 'Definindo Aba inicial');
      if DModG.QrCtrlGeralAbaIniApp.Value > 0 then
        AdvToolBarPagerNovo.ActivePageIndex := DModG.QrCtrlGeralAbaIniApp.Value;
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
      UFixBugs.MostraFixBugs([]);
      //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      if VAR_USUARIO = -1 then
        Geral.MB_Info('Testar emiss�o de NFCe com ACBr ');
      //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    end;
  finally
    MyObjects.Informa2(LaTopWarn1, LaTopWarn2, False, '');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.ReceitaseDespesas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraReceDesp(0);
end;

function TFmPrincipal.RecriaTempTable(Tabela: String): Boolean;
begin
  Result := Ucriar.RecriaTempTable(Tabela, DModG.QrUpdPID1, False) <> '';
end;

procedure TFmPrincipal.CadastroDeProtocolos(Lote: Integer);
begin
  ProtocoUnit.MostraFormProtocolos(Lote, 0);
end;

{
procedure TFmPrincipal.LeDadosFonte(Cam, Arte, Fonte: String; Strings: TStrings);
var
  F: TextFile;
  S: String;
  Name: String;
begin
  Name := Cam+'\'+Arte+'\'+Fonte+'\Dados.Txt';
  if FileExists(Name) then
  begin
    AssignFile(F, Name);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Strings.Add(S);
    end;
  end;
end;
}

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  MyObjects.SelecionaLimpaImagemdefundoFormDescanso(FmPrincipal, TFmDescanso,
    PageControl1, True);
end;

procedure TFmPrincipal.LoadWebServices1Click(Sender: TObject);
begin
  UnNFe_PF.MostraFormNFeWebservices();
end;

procedure InfoWindow(Wnd: HWnd; Codigo, Item: Integer; Texto: String; WndID: Int64);
var
  WInfo: TWindowInfo;
  //p: TPoint;
  //Atom: TAtom;
{

  DWORD cbSize, ;
  RECT  rcWindow;
  RECT  rcClient;
  DWORD dwStyle;
  DWORD dwExStyle;
  DWORD dwWindowStatus;
  UINT  cxWindowBorders;
  UINT  cyWindowBorders;
  ATOM  atomWindowType;
  WORD  wCreatorVersion;  ATOM: TAtom;
}
  cbSize,
  rcWindowT, rcWindowE, rcWindowB,
  rcWindowD, rcClientT, rcClientE,
  rcClientB, rcClientD, dwStyle,
  dwExStyle, dwWindowStatus, cxWindowBorders,
  cyWindowBorders, atomWindowType: Integer;
begin
  WInfo.cbSize := SizeOf(TWindowInfo);
  GetWindowInfo(Wnd, WInfo);
  // Obter as coordenadas de tela, as coordenadas
{  T := WInfo.rcWindow.Top;
  E := WInfo.rcWindow.Left;
  B := WInfo.rcWindow.Bottom;
  D := WInfo.rcWindow.Right;
  ATom := WInfo.atomWindowType;
{
  Showmessage('canto superior esquerdo do ret�ngulo da �rea cliente, coordenar, X: ' +
               InttoStr(P.X) + 'Y:  '+
               InttoStr(P.Y) + '�');
}
  cbSize    := WInfo.cbSize;
  rcWindowT := WInfo.rcWindow.Top;
  rcWindowE := WInfo.rcWindow.Left;
  rcWindowB := WInfo.rcWindow.Bottom;
  rcWindowD := WInfo.rcWindow.Right;
  rcClientT := WInfo.rcClient.Top;
  rcClientE := WInfo.rcClient.Left;
  rcClientB := WInfo.rcClient.Bottom;
  rcClientD := WInfo.rcClient.Right;
  //
  dwStyle         := WInfo.dwStyle;
  dwExStyle       := WInfo.dwExStyle;
  dwWindowStatus  := WInfo.dwOtherStuff;
  cxWindowBorders := WInfo.cxWindowBorders;
  cyWindowBorders := WInfo.cyWindowBorders;
  atomWindowType  := WInfo.atomWindowType;
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'janelas', False, [
  'WndID', 'Texto', 'cbSize',
  'rcWindowT', 'rcWindowE', 'rcWindowB',
  'rcWindowD', 'rcClientT', 'rcClientE',
  'rcClientB', 'rcClientD', 'dwStyle',
  'dwExStyle', 'dwWindowStatus', 'cxWindowBorders',
  'cyWindowBorders', 'atomWindowType'], [
  'Codigo', 'Item'], [
  WndID, Trim(Texto), cbSize,
  rcWindowT, rcWindowE, rcWindowB,
  rcWindowD, rcClientT, rcClientE,
  rcClientB, rcClientD, dwStyle,
  dwExStyle, dwWindowStatus, cxWindowBorders,
  cyWindowBorders, atomWindowType], [
  Codigo, Item], False);
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

procedure TFmPrincipal.SbAtualizaERPClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.SbBackupClick(Sender: TObject);
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.SbEntidadeClick(Sender: TObject);
begin
  BtEntidadeClick(Sender);
end;

procedure TFmPrincipal.SbFinancasClick(Sender: TObject);
begin
  BtFinancasClick(Sender);
end;

procedure TFmPrincipal.SbGraGXPatrClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := False;
  MyObjects.FormTDICria(TFmGraGXPatr, PageControl1, AdvToolBarPagerNovo, True, True);
end;

procedure TFmPrincipal.SbGraGXVendClick(Sender: TObject);
begin
//  GraL_Jan.MostraFormGraGXVend(-1);
  AdvToolBarPagerNovo.Visible := False;
  MyObjects.FormTDICria(TFmGraGXVend, PageControl1, AdvToolBarPagerNovo, True, True);
end;

procedure TFmPrincipal.SbLocCConClick(Sender: TObject);
begin
  BtLocCConClick(Sender);
end;

procedure TFmPrincipal.SbLoginClick(Sender: TObject);
begin
  MostraLogoff;
end;

procedure TFmPrincipal.SbMinimizaMenuClick(Sender: TObject);
begin
  AdvToolBarPagerNovo.Visible := not AdvToolBarPagerNovo.Visible;
end;

procedure TFmPrincipal.SbPsqGGXVenClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  AppPF.PesquisaGraGXVend(GraGruX);
end;

procedure TFmPrincipal.SbVerificaDBClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMVerificaBD, SbVerificaDB);
end;

procedure TFmPrincipal.SbWSuportClick(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.sd1FormSkin(Sender: TObject; aName: string;
  var DoSkin: Boolean);
begin
  if aName = VAR_FORMTDI_NAME then
    DoSkin := False;
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
end;

procedure TFmPrincipal.Situaesdemateriais1Click(Sender: TObject);
begin
  MostraFormGraGLSitu(0);
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
begin
{20200920
  case Index of
    0:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Blue;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Blue;
    end;
    1:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Classic;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Classic;
    end;
    2:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Olive;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Olive;
    end;
    3:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2003Silver;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2003Silver;
    end;
    4:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Luna;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Luna;
    end;
    5:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Obsidian;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Obsidian;
    end;
    6:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOffice2007Silver;
      AdvPreviewMenuOfficeStyler1.Style := psOffice2007Silver;
    end;
    7:
    begin
      AdvToolBarOfficeStyler1.Style     := bsOfficeXP;
      AdvPreviewMenuOfficeStyler1.Style := psOfficeXP;
    end;
    8:
    begin
      AdvToolBarOfficeStyler1.Style     := bsWhidbeyStyle;
      AdvPreviewMenuOfficeStyler1.Style := psWhidbeyStyle;
    end;
    9:
    begin
      AdvToolBarOfficeStyler1.Style     := bsWindowsXP;
      AdvPreviewMenuOfficeStyler1.Style := psWindowsXP;
    end;
  end;
}
end;

function TFmPrincipal.CadastroDeContasSdoSimples(Entidade,
  Conta: Integer): Boolean;
begin
  Result := True;
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
begin
  Result := True;
  //Compatibilidade
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
Entidade: Integer; Aba: Boolean);
begin
  //Compatibilidade
  //
  // Nao eh desse jeito!
{
var
  Qry: TmySQLQuery;
  Edita: Boolean;
  Codigo(*, CondicaoPG, Carteira, GeneroHon, DiaPagaHon*): Integer;
begin
  Edita := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE ' + VAR_FP_FUNCION,
    'AND NOT (Codigo IN  ',
    '  ( ',
    '    SELECT Codigo FROM ageentcad ',
    '  ) ',
    ') ',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Codigo         := Qry.FieldByName('Codigo').AsInteger;
      (*
      CondicaoPG     := ;
      Carteira       := ;
      GeneroHon      := ;
      DiaPagaHon     := ;
      *)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ageentcad', False, [
      (*'CondicaoPG', 'Carteira', 'GeneroHon',
      'DiaPagaHon'*)], [
      'Codigo'], [
      (*CondicaoPG, Carteira, GeneroHon,
      DiaPagaHon*)], [
      Codigo], True) then ;
      //
      Qry.Next;
    end;
    Qry.First;
    case Qry.RecordCount of
      0: ;// Nada!
      1:
      begin
        Edita := Geral.MB_Pergunta(
        'Foi adicionado uma nova entidade na lista de agentes!' +
        sLineBreak + 'Deseja configur�-lo?') = ID_YES;
      end;
      else
      begin
        Edita := Geral.MB_Pergunta(
        'Foram adicionadas ' + Geral.FF0(Qry.RecordCount) +
        ' novas entidades na lista de agentes!' +
        sLineBreak + 'Deseja configur�-los?') = ID_YES;
      end;
    end;
    if Edita then
      SrvL_PF.MostraFormAgeEntCad(Qry.FieldByName('Codigo').AsInteger);
  finally
    Qry.Free;
  end;
}
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MeusServiosNFSe1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.MostraAnotacoes;
begin
  if DBCheck.CriaFm(TFmAnotacoes, FmAnotacoes, afmoNegarComAviso) then
  begin
    FmAnotacoes.ShowModal;
    FmAnotacoes.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFatPedNFs(EMP_FILIAL, Cliente,
  CU_PediVda: Integer; ForcaCriarXML: Boolean);
begin
  UnNFe_PF.MostraFormFatPedNFs(EMP_FILIAL, Cliente, CU_PediVda, ForcaCriarXML);
end;

procedure TFmPrincipal.MostraFiliais();
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormContratos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmContratos.LocCod(Codigo, Codigo);
    FmContratos.ShowModal;
    FmContratos.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormDescanso;
begin
  MyObjects.FormTDICria(TFmDescanso, PageControl1, AdvToolBarPagerNovo, False, True, afmoLiberado);
end;

procedure TFmPrincipal.MostraFormFixGereCab(AbrirEmAba: Boolean;
  InOwner: TWincontrol; AdvToolBarPager: TdmkPageControl; Codigo: Integer);
var
  Form: TForm;
begin
  if AbrirEmAba then
  begin
    if FmFixGereCab = nil then
    begin
      Form := MyObjects.FormTDICria(TFmFixGereCab, InOwner, AdvToolBarPager, True, True);
      //
      if Codigo <> 0 then
        TFmFixGereCab(Form).LocCod(Codigo, Codigo);
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmFixGereCab, FmFixGereCab, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmFixGereCab.LocCod(Codigo, Codigo);
      FmFixGereCab.ShowModal;
      FmFixGereCab.Destroy;
      //
      FmFixGereCab := nil;
    end;
  end;
end;

procedure TFmPrincipal.MostraFormFixGXPatr(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFixGXPatr, FmFixGXPatr, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmFixGXPatr.LocCod(Codigo, Codigo);
    FmFixGXPatr.ShowModal;
    FmFixGXPatr.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormFixPecaCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFixPecaCad, FmFixPecaCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmFixPecaCad.LocCod(Codigo, Codigo);
    FmFixPecaCad.ShowModal;
    FmFixPecaCad.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormFixServCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFixServCad, FmFixServCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmFixServCad.LocCod(Codigo, Codigo);
    FmFixServCad.ShowModal;
    FmFixServCad.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormGraFabCad();
begin
  Grade_Jan.MostraFormGraFabCad;
end;

procedure TFmPrincipal.MostraFormGraGLSitu(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraGLSitu, FmGraGLSitu, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraGLSitu.LocCod(Codigo, Codigo);
    FmGraGLSitu.ShowModal;
    FmGraGLSitu.Destroy;
  end;
end;

procedure TFmPrincipal.MostraFormGraGruN;
begin
  Grade_Jan.MostraFormGraGruN(0);
end;


{  Img Toolrent?
object AdvShapeButton1: TSpeedButton
  AlignWithMargins = True
  Left = 5
  Top = 6
  Width = 45
  Height = 45
  Margins.Left = 4
  Margins.Top = 4
  Margins.Right = 4
  Margins.Bottom = 4
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
end
}

(*

  case Dmod.QrOpcoesTRenFormaCobrLoca.Value of
    0,1: // Modo simplificado - AAPA
    begin
      ...
    end;
    2: // Modo completo - Tisolin
    begin
      ...
    end;
  end;

*)


(*
function TNFCe_PF.MostraFormNFCeLEnU(): Boolean;
var
  VersaoNFCe: Integer;
begin
  VersaoNFCe := VersaoNFCeEmUso();
  //
  //FNaoCriouXML := False;
  //
  Result := False;
  case VersaoNFCe of
    400:
    if DBCheck.CriaFm(TFmNFCeLEnU_0400, FmNFCeLEnU_0400, afmoNegarComAviso) then
    begin
      FmNFCeLEnU_0400.Show;
      Result := FmNFCeLEnU_0400.EnviarNFe();
      if Result then
        FmNFCeLEnU_0400.Destroy;
    end;
    else AvisoNaoImplemVerNFCe(VersaoNFCe);
  end;
end;
*)

end.

