object FmContratApp: TFmContratApp
  Left = 0
  Top = 0
  Caption = 'Impress'#227'o de contrato'
  ClientHeight = 362
  ClientWidth = 549
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object frxContrato: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PreviewOptions.ZoomMode = zmPageWidth
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.852886284700000000
    ReportOptions.LastChange = 41824.418973252310000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VAR_OBSERVACOES> <> '#39#39' then'
      '    MasterData4.Visible := True'
      '  else'
      
        '    MasterData4.Visible := False;                               ' +
        '                        '
      'end.')
    OnGetValue = frxContratoGetValue
    Left = 64
    Top = 36
    Datasets = <
      item
        DataSet = frxDsContratos
        DataSetName = 'frxDsContratos'
      end
      item
        DataSet = frxDsEndContratada
        DataSetName = 'frxDsEndContratada'
      end
      item
        DataSet = frxDsEndContratante
        DataSetName = 'frxDsEndContratante'
      end
      item
        DataSet = frxDsLocCCon
        DataSetName = 'frxDsLocCCon'
      end
      item
        DataSet = frxDsLocCPatAce
        DataSetName = 'frxDsLocCPatAce'
      end
      item
        DataSet = frxDsLocCPatCns
        DataSetName = 'frxDsLocCPatCns'
      end
      item
        DataSet = frxDsLocCPatPri
        DataSetName = 'frxDsLocCPatPri'
      end
      item
        DataSet = frxDsLocCPatSec
        DataSetName = 'frxDsLocCPatSec'
      end
      item
        DataSet = frxDsLocCPatUso
        DataSetName = 'frxDsLocCPatUso'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 98.267780000000000000
        Width = 793.701300000000000000
        RowCount = 1
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 124.724490000000000000
        Width = 793.701300000000000000
        RowCount = 1
        Stretched = True
        object frxDsContratosTitulo: TfrxMemoView
          Left = 0.377953000000000000
          Width = 793.700787400000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsContratos."Nome"] - N'#186' [frxDsLocCCon."Codigo"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
      object DetailData3: TfrxDetailData
        FillType = ftBrush
        Height = 185.196970000000000000
        Top = 166.299320000000000000
        Width = 793.701300000000000000
        RowCount = 1
        object Shape2: TfrxShapeView
          Left = 75.590600000000000000
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 52.913420000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Shape1: TfrxShapeView
          Left = 75.590600000000000000
          Top = 11.338590000000010000
          Width = 680.315400000000000000
          Height = 52.913420000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo3: TfrxMemoView
          Left = 79.370105590000000000
          Top = 18.897635350000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 117.165373860000000000
          Top = 18.897635350000000000
          Width = 302.362209610000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 419.527805590000000000
          Top = 18.897623149999990000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 461.102603860000000000
          Top = 18.897650000000000000
          Width = 291.023619610000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAENDERECO]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 79.370105590000000000
          Top = 34.015730940000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 117.165373860000000000
          Top = 34.015730940000000000
          Width = 215.811163000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."BAIRRO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 336.378040630000000000
          Top = 34.015730940000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 366.614261100000000000
          Top = 34.015730940000000000
          Width = 219.212666770000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CIDADE"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 589.606452990000000000
          Top = 34.236240000000010000
          Width = 18.897637800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 608.504090790000000000
          Top = 34.236240000000010000
          Width = 22.677167800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."NOMEUF"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 79.370105590000000000
          Top = 49.133838739999990000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALACNPJCFP]:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 117.165373860000000000
          Top = 49.133838739999990000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 241.889783310000000000
          Top = 49.133838739999990000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALAIERG]:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 287.244114020000000000
          Top = 49.133838739999990000
          Width = 132.283469450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAIERG]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 419.527805590000000000
          Top = 49.133826540000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 457.323073860000000000
          Top = 49.133826540000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."TEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 582.047483310000000000
          Top = 49.133826540000000000
          Width = 22.677150710000000000
          Height = 15.118110240000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 604.724634020000000000
          Top = 49.133826540000000000
          Width = 147.401589450000000000
          Height = 15.118110240000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."FAX_TXT"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 79.369868820000000000
          Top = 83.149645349999990000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 117.165137090000000000
          Top = 83.149645349999990000
          Width = 302.362204720000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 419.527568820000000000
          Top = 83.149633150000000000
          Width = 41.574798270000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 461.102367090000000000
          Top = 83.149660000000010000
          Width = 291.023614720000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEENDERECO]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 79.369868820000000000
          Top = 98.267740939999990000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 117.165137090000000000
          Top = 98.267740939999990000
          Width = 215.811163000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."BAIRRO"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 336.377952760000000000
          Top = 98.267740939999990000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 366.614173230000000000
          Top = 98.267740939999990000
          Width = 219.212666770000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CIDADE"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 589.606431020000000000
          Top = 98.267740939999990000
          Width = 18.897637800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 608.504068820000000000
          Top = 98.267740939999990000
          Width = 22.677167800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."NOMEUF"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 79.369868820000000000
          Top = 113.385848740000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELACNPJCFP]:')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 117.165137090000000000
          Top = 113.385848740000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 241.889546540000000000
          Top = 113.385848740000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELAIERG]:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 287.243877240000000000
          Top = 113.385848740000000000
          Width = 132.283464570000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEIERG]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 419.527568820000000000
          Top = 113.385836540000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 457.322837090000000000
          Top = 113.385836540000000000
          Width = 124.724409450000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."TEL_TXT"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 582.047246540000000000
          Top = 113.385836540000000000
          Width = 22.677150710000000000
          Height = 15.118110240000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 604.724397240000000000
          Top = 113.385836540000000000
          Width = 147.401584570000000000
          Height = 15.118110240000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."FAX_TXT"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 86.929133860000000000
          Top = 3.000000000000000000
          Width = 68.031540000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Contratada:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 86.929133860000000000
          Top = 67.252010000000010000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Contratante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 634.961040000000000000
          Top = 34.015770000000010000
          Width = 22.677167800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 657.638207800000000000
          Top = 34.015770000000010000
          Width = 94.488237800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CEP_TXT"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 634.961040000000000000
          Top = 98.267779999999990000
          Width = 22.677167800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 657.638207800000000000
          Top = 98.267779999999990000
          Width = 94.488237800000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CEP_TXT"]')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Left = 75.590600000000000000
          Top = 139.842610000000000000
          Width = 680.315400000000000000
          Height = 41.574830000000000000
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo122: TfrxMemoView
          Left = 86.929190000000000000
          Top = 132.283550000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            'Outras informa'#231#245'es:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 79.370130000000000000
          Top = 147.401670000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Local da obra ou evento:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 173.858380000000000000
          Top = 147.401670000000000000
          Width = 245.669450000000000000
          Height = 15.118110240000000000
          DataField = 'LocalObra'
          DataSet = frxDsLocCCon
          DataSetName = 'frxDsLocCCon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCCon."LocalObra"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 423.307360000000000000
          Top = 147.401670000000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Contato:')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 457.323130000000000000
          Top = 147.401670000000000000
          Width = 102.047310000000000000
          Height = 15.118110240000000000
          DataField = 'LocalCntat'
          DataSet = frxDsLocCCon
          DataSetName = 'frxDsLocCCon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCCon."LocalCntat"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 559.370440000000000000
          Top = 147.401670000000000000
          Width = 56.692950000000010000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 616.063390000000000000
          Top = 147.401670000000000000
          Width = 136.062999450000000000
          Height = 15.118110240000000000
          DataField = 'LocalFone'
          DataSet = frxDsLocCCon
          DataSetName = 'frxDsLocCCon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCCon."LocalFone"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 79.370130000000000000
          Top = 162.519790000000000000
          Width = 83.149660000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'Endere'#231'o de cobran'#231'a:')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 162.519790000000000000
          Top = 162.519790000000000000
          Width = 589.606680000000000000
          Height = 15.118110240000000000
          DataField = 'EndCobra'
          DataSet = frxDsLocCCon
          DataSetName = 'frxDsLocCCon'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCCon."EndCobra"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 559.370440000000000000
          Top = 162.519790000000000000
          Width = 56.692950000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            'N'#186' Requisi'#231#227'o:')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 616.063390000000000000
          Top = 162.519790000000000000
          Width = 136.062999450000000000
          Height = 15.118110240000000000
          DataSet = frxDsLocCCon
          DataSetName = 'frxDsLocCCon'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCCon."NumOC"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 52.913420000000000000
        Top = 1409.764690000000000000
        Width = 793.701300000000000000
        object Memo41: TfrxMemoView
          Left = 415.748031500000000000
          Top = 18.897650000000110000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000110000
          Width = 340.157700000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_SLOGANFOOT]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 68.031540000000010000
        Top = 434.645950000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLocCPatPri
        DataSetName = 'frxDsLocCPatPri'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo112: TfrxMemoView
          Left = 75.590600000000000000
          Top = 60.472479999999850000
          Width = 680.315400000000000000
          Height = 7.559050240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 75.590600000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 181.417440000000000000
          Top = 15.118119999999980000
          Width = 257.008040000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 438.425480000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Indenizat'#243'rio')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 514.016080000000000000
          Top = 15.118119999999980000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ dia')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 574.488560000000000000
          Top = 15.118119999999980000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ semana')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 634.961040000000000000
          Top = 15.118119999999980000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ quinzena')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 695.433520000000000000
          Top = 15.118119999999980000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$  m'#234's')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 75.590600000000000000
          Top = 30.236240000000010000
          Width = 105.826840000000000000
          Height = 30.236220470000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatPri."REFERENCIA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 181.417440000000000000
          Top = 30.236240000000010000
          Width = 257.008040000000000000
          Height = 30.236220470000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatPri."NO_GGX"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 438.425480000000000000
          Top = 30.236240000000010000
          Width = 75.590560940000000000
          Height = 30.236220470000000000
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatPri."AtualValr"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 514.016080000000000000
          Top = 30.236240000000010000
          Width = 60.472440940000000000
          Height = 30.236220470000000000
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_ValorDia]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 574.488560000000000000
          Top = 30.236240000000010000
          Width = 60.472440940000000000
          Height = 30.236220470000000000
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_VAL_SEM_CALC]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 634.961040000000000000
          Top = 30.236240000000010000
          Width = 60.472440940000000000
          Height = 30.236220470000000000
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_VAL_QUI_CALC]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 695.433520000000000000
          Top = 30.236240000000010000
          Width = 60.472440940000000000
          Height = 30.236220470000000000
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_VAL_MES_CALC]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Patrim'#244'nio Principal')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 241.889920000000000000
        Top = 1145.197590000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        object Memo150: TfrxMemoView
          Left = 75.590600000000000000
          Top = 41.574830000000020000
          Width = 332.598640000000000000
          Height = 45.354330710000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]'
            'Contratada')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 423.307360000000000000
          Top = 41.574830000000020000
          Width = 332.598640000000000000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]'
            'Contratante')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 75.590600000000000000
          Top = 124.724490000000000000
          Width = 332.598640000000000000
          Height = 45.354330710000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Testemunha 1:')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 423.307360000000000000
          Top = 124.724490000000000000
          Width = 332.598640000000000000
          Height = 45.354360000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Testemunha 2:')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 75.590600000000000000
          Top = 200.315090000000100000
          Width = 332.598449610000000000
          Height = 30.236220470000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            
              'Equipamento devolvido ao locador em: ___ / ___ / ______ '#224's ___ :' +
              ' ___ horas')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 423.307360000000000000
          Top = 200.315090000000100000
          Width = 332.598640000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]'
            'Contratada')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 75.590600000000000000
          Top = 170.078850000000100000
          Width = 332.598640000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Encerramento do contrato')
          ParentFont = False
        end
      end
      object DetailData4: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 616.063390000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLocCPatSec
        DataSetName = 'frxDsLocCPatSec'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo117: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 94.488250000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatSec
          DataSetName = 'frxDsLocCPatSec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatSec."REFERENCIA"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 200.315090000000000000
          Width = 461.102660000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatSec
          DataSetName = 'frxDsLocCPatSec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatSec."NO_GGX"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 661.417750000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'AtualValr'
          DataSet = frxDsLocCPatSec
          DataSetName = 'frxDsLocCPatSec'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatSec."AtualValr"]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 653.858690000000000000
        Width = 793.701300000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 563.149970000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLocCPatSec."Codigo"'
        object Memo113: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 94.488250000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 200.315090000000000000
          Top = 15.118119999999980000
          Width = 461.102660000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 94.488250000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Patrim'#244'nios Secund'#225'rios')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 661.417750000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Indenizat'#243'rio')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 676.535870000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLocCPatAce."Codigo"'
        object Memo114: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 94.488250000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 200.315090000000000000
          Top = 15.118119999999980000
          Width = 461.102660000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 94.488250000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Acess'#243'rios')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 661.417750000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Indenizat'#243'rio')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object DetailData5: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 729.449290000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLocCPatAce
        DataSetName = 'frxDsLocCPatAce'
        RowCount = 0
        object Memo118: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 94.488250000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatAce
          DataSetName = 'frxDsLocCPatAce'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatAce."REFERENCIA"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 200.315090000000000000
          Width = 461.102660000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatAce
          DataSetName = 'frxDsLocCPatAce'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatAce."NO_GGX"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 661.417750000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'ValBem'
          DataSet = frxDsLocCPatAce
          DataSetName = 'frxDsLocCPatAce'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatAce."ValBem"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Top = 767.244590000000000000
        Width = 793.701300000000000000
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 789.921770000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLocCPatUso."Codigo"'
        object Memo115: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 94.488250000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 200.315090000000000000
          Top = 15.118119999999980000
          Width = 385.512060000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 94.488250000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Material de Uso')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Left = 661.417750000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Indenizat'#243'rio')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 585.827150000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Estado na retirada')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object DetailData6: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 842.835190000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLocCPatUso
        DataSetName = 'frxDsLocCPatUso'
        RowCount = 0
        object Memo119: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 94.488250000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatUso
          DataSetName = 'frxDsLocCPatUso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatUso."REFERENCIA"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 200.315090000000000000
          Width = 385.512060000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatUso
          DataSetName = 'frxDsLocCPatUso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatUso."NO_GGX"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 661.417750000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'PrcUni'
          DataSet = frxDsLocCPatUso
          DataSetName = 'frxDsLocCPatUso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatUso."PrcUni"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 585.827150000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'NO_AVALINI'
          DataSet = frxDsLocCPatUso
          DataSetName = 'frxDsLocCPatUso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatUso."NO_AVALINI"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Top = 880.630490000000000000
        Width = 793.701300000000000000
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 903.307670000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLocCPatCns."Codigo"'
        object Memo116: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 94.488250000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 200.315090000000000000
          Top = 15.118119999999980000
          Width = 351.496290000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 94.488250000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Material de Consumo')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Left = 661.417750000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o R$ unit.')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 585.827150000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantid. na retirada')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 551.811380000000000000
          Top = 15.118119999999980000
          Width = 34.015730940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object DetailData7: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 956.221090000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLocCPatCns
        DataSetName = 'frxDsLocCPatCns'
        RowCount = 0
        object Memo111: TfrxMemoView
          Left = 75.590600000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 737.008350000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 94.488250000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatCns."REFERENCIA"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 200.315090000000000000
          Width = 351.496290000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCPatCns."NO_GGX"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 585.827150000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'QtdIni'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatCns."QtdIni"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 661.417750000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'PrcUni'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatCns."PrcUni"]')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 551.811380000000000000
          Width = 34.015730940000000000
          Height = 15.118110240000000000
          DataField = 'SIGLA'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatCns."SIGLA"]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Top = 994.016390000000000000
        Width = 793.701300000000000000
      end
      object GroupHeader5: TfrxGroupHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 1016.693570000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsContratos."Codigo"'
      end
      object GroupFooter5: TfrxGroupFooter
        FillType = ftBrush
        Top = 1084.725110000000000000
        Width = 793.701300000000000000
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 1043.150280000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        RowCount = 1
        Stretched = True
        object Rich1: TfrxRichView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897637800000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsContratos
          DataSetName = 'frxDsContratos'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C2A5C67656E
            657261746F722052696368656432302031302E302E31393034317D5C76696577
            6B696E64345C756331200D0A5C706172645C66305C667331365C7061720D0A7D
            0D0A00}
        end
      end
      object GroupHeader6: TfrxGroupHeader
        FillType = ftBrush
        Top = 411.968770000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLocCPatPri."CtrID"'
      end
      object GroupFooter6: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 525.354670000000000000
        Width = 793.701300000000000000
        object Memo103: TfrxMemoView
          Left = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 7.559060000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 374.173470000000000000
        Width = 793.701300000000000000
        RowCount = 1
        Stretched = True
        object Memo129: TfrxMemoView
          Left = 132.283550000000000000
          Width = 623.622450000000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[VAR_OBSERVACOES]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 75.590600000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
      end
    end
  end
  object frxRichObject1: TfrxRichObject
    Left = 92
    Top = 36
  end
  object QrEndContratada: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEndContratadaCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATADA,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,'
      'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      'WHERE ent.Codigo=:P0'
      'ORDER BY CONTRATADA')
    Left = 64
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEndContratadaTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEndContratadaCONTRATADA: TWideStringField
      FieldName = 'CONTRATADA'
      Size = 100
    end
    object QrEndContratadaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratadaNOMERUA: TWideStringField
      FieldName = 'NOMERUA'
      Size = 30
    end
    object QrEndContratadaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratadaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratadaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEndContratadaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratadaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratadaIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratadaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEndContratadaTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratadaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratadaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEndContratadaTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratadaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratadaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratadaCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEndContratadaNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrEndContratante: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEndContratanteCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,'
      'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      'WHERE ent.Codigo=:P0'
      'ORDER BY CONTRATANTE')
    Left = 64
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEndContratanteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEndContratanteTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEndContratanteNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratanteNOMERUA: TWideStringField
      FieldName = 'NOMERUA'
      Size = 30
    end
    object QrEndContratanteCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratanteBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratanteCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEndContratanteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratanteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratanteIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratanteTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratanteFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratanteCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEndContratanteTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratanteCONTRATANTE: TWideStringField
      FieldName = 'CONTRATANTE'
      Size = 100
    end
    object QrEndContratanteFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratanteCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratanteCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEndContratanteNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrContrato: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONTRATADA,'
      'IF(enb.Tipo=0, enb.RazaoSocial, enb.Nome) NCONTRATANTE,'
      'IF(rep.Tipo=0, rep.RazaoSocial, rep.Nome) NO_REP_LEGAL,'
      'IF(ts1.Tipo=0, ts1.RazaoSocial, ts1.Nome) NO_TESTEM_01,'
      'IF(ts2.Tipo=0, ts2.RazaoSocial, ts2.Nome) NO_TESTEM_02'
      'FROM contratos con'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Contratada'
      'LEFT JOIN entidades enb ON enb.Codigo = con.Contratante'
      'LEFT JOIN entidades rep ON rep.Codigo = con.RepreLegal'
      'LEFT JOIN entidades ts1 ON ts1.Codigo = con.Testemun01'
      'LEFT JOIN entidades ts2 ON ts2.Codigo = con.Testemun02'
      'WHERE con.Codigo=:P0'
      ''
      '')
    Left = 64
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContratoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contratos.Codigo'
    end
    object QrContratoContratada: TIntegerField
      FieldName = 'Contratada'
      Origin = 'contratos.Contratada'
    end
    object QrContratoContratante: TIntegerField
      FieldName = 'Contratante'
      Origin = 'contratos.Contratante'
    end
    object QrContratoDContrato: TDateField
      FieldName = 'DContrato'
      Origin = 'contratos.DContrato'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoDInstal: TDateField
      FieldName = 'DInstal'
      Origin = 'contratos.DInstal'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoDVencimento: TDateField
      FieldName = 'DVencimento'
      Origin = 'contratos.DVencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'contratos.Status'
    end
    object QrContratoAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'contratos.Ativo'
    end
    object QrContratoValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'contratos.ValorMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrContratoNCONTRATADA: TWideStringField
      FieldName = 'NCONTRATADA'
      Size = 100
    end
    object QrContratoNCONTRATANTE: TWideStringField
      FieldName = 'NCONTRATANTE'
      Size = 100
    end
    object QrContratoWinDocArq: TWideStringField
      FieldName = 'WinDocArq'
      Origin = 'contratos.WinDocArq'
      Size = 255
    end
    object QrContratoFormaUso: TSmallintField
      FieldName = 'FormaUso'
      Origin = 'contratos.FormaUso'
    end
    object QrContratoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contratos.Nome'
      Size = 255
    end
    object QrContratoDtaPropIni: TDateField
      FieldName = 'DtaPropIni'
      Origin = 'contratos.DtaPropIni'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoDtaPropFim: TDateField
      FieldName = 'DtaPropFim'
      Origin = 'contratos.DtaPropFim'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
      Origin = 'contratos.DtaCntrFim'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoDtaImprime: TDateTimeField
      FieldName = 'DtaImprime'
      Origin = 'contratos.DtaImprime'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoDtaAssina: TDateField
      FieldName = 'DtaAssina'
      Origin = 'contratos.DtaAssina'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratoTratamento: TWideStringField
      FieldName = 'Tratamento'
      Origin = 'contratos.Tratamento'
      Size = 30
    end
    object QrContratoArtigoDef: TSmallintField
      FieldName = 'ArtigoDef'
      Origin = 'contratos.ArtigoDef'
    end
    object QrContratoPerDefImpl: TSmallintField
      FieldName = 'PerDefImpl'
      Origin = 'contratos.PerDefImpl'
    end
    object QrContratoPerQtdImpl: TSmallintField
      FieldName = 'PerQtdImpl'
      Origin = 'contratos.PerQtdImpl'
    end
    object QrContratoPerDefTrei: TSmallintField
      FieldName = 'PerDefTrei'
      Origin = 'contratos.PerDefTrei'
    end
    object QrContratoPerQtdTrei: TSmallintField
      FieldName = 'PerQtdTrei'
      Origin = 'contratos.PerQtdTrei'
    end
    object QrContratoPerDefMoni: TSmallintField
      FieldName = 'PerDefMoni'
      Origin = 'contratos.PerDefMoni'
    end
    object QrContratoPerQtdMoni: TSmallintField
      FieldName = 'PerQtdMoni'
      Origin = 'contratos.PerQtdMoni'
    end
    object QrContratoPerDefIniI: TSmallintField
      FieldName = 'PerDefIniI'
      Origin = 'contratos.PerDefIniI'
    end
    object QrContratoPerQtdIniI: TSmallintField
      FieldName = 'PerQtdIniI'
      Origin = 'contratos.PerQtdIniI'
    end
    object QrContratoRepreLegal: TIntegerField
      FieldName = 'RepreLegal'
      Origin = 'contratos.RepreLegal'
    end
    object QrContratoTestemun01: TIntegerField
      FieldName = 'Testemun01'
      Origin = 'contratos.Testemun01'
    end
    object QrContratoTestemun02: TIntegerField
      FieldName = 'Testemun02'
      Origin = 'contratos.Testemun02'
    end
    object QrContratoPercMulta: TFloatField
      FieldName = 'PercMulta'
      Origin = 'contratos.PercMulta'
      DisplayFormat = '0.00'
    end
    object QrContratoPercJuroM: TFloatField
      FieldName = 'PercJuroM'
      Origin = 'contratos.PercJuroM'
      DisplayFormat = '0.00'
    end
    object QrContratoTpPagDocu: TWideStringField
      FieldName = 'TpPagDocu'
      Origin = 'contratos.TpPagDocu'
      Size = 50
    end
    object QrContratoddMesVcto: TSmallintField
      FieldName = 'ddMesVcto'
      Origin = 'contratos.ddMesVcto'
    end
    object QrContratoTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'contratos.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrContratoNO_REP_LEGAL: TWideStringField
      FieldName = 'NO_REP_LEGAL'
      Size = 100
    end
    object QrContratoNO_TESTEM_01: TWideStringField
      FieldName = 'NO_TESTEM_01'
      Size = 100
    end
    object QrContratoNO_TESTEM_02: TWideStringField
      FieldName = 'NO_TESTEM_02'
      Size = 100
    end
    object QrContratoLugarNome: TWideStringField
      FieldName = 'LugarNome'
      Size = 100
    end
    object QrContratoLugArtDef: TSmallintField
      FieldName = 'LugArtDef'
    end
    object QrContratoVersao: TFloatField
      FieldName = 'Versao'
    end
  end
  object frxDsContratos: TfrxDBDataset
    UserName = 'frxDsContratos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Contratada=Contratada'
      'Contratante=Contratante'
      'DContrato=DContrato'
      'DInstal=DInstal'
      'DVencimento=DVencimento'
      'Status=Status'
      'Ativo=Ativo'
      'ValorMes=ValorMes'
      'NCONTRATADA=NCONTRATADA'
      'NCONTRATANTE=NCONTRATANTE'
      'WinDocArq=WinDocArq'
      'FormaUso=FormaUso'
      'Nome=Nome'
      'DtaPropIni=DtaPropIni'
      'DtaPropFim=DtaPropFim'
      'DtaCntrFim=DtaCntrFim'
      'DtaImprime=DtaImprime'
      'DtaAssina=DtaAssina'
      'Tratamento=Tratamento'
      'ArtigoDef=ArtigoDef'
      'PerDefImpl=PerDefImpl'
      'PerQtdImpl=PerQtdImpl'
      'PerDefTrei=PerDefTrei'
      'PerQtdTrei=PerQtdTrei'
      'PerDefMoni=PerDefMoni'
      'PerQtdMoni=PerQtdMoni'
      'PerDefIniI=PerDefIniI'
      'PerQtdIniI=PerQtdIniI'
      'RepreLegal=RepreLegal'
      'Testemun01=Testemun01'
      'Testemun02=Testemun02'
      'PercMulta=PercMulta'
      'PercJuroM=PercJuroM'
      'TpPagDocu=TpPagDocu'
      'ddMesVcto=ddMesVcto'
      'Texto=Texto'
      'NO_REP_LEGAL=NO_REP_LEGAL'
      'NO_TESTEM_01=NO_TESTEM_01'
      'NO_TESTEM_02=NO_TESTEM_02'
      'LugarNome=LugarNome'
      'LugArtDef=LugArtDef'
      'Versao=Versao')
    DataSet = QrContrato
    BCDToCurrency = False
    Left = 92
    Top = 64
  end
  object frxDsEndContratada: TfrxDBDataset
    UserName = 'frxDsEndContratada'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tipo=Tipo'
      'CONTRATADA=CONTRATADA'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMERUA=NOMERUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMEUF=NOMEUF'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'Codigo=Codigo'
      'TEL=TEL'
      'FAX=FAX'
      'CEP=CEP'
      'TEL_TXT=TEL_TXT'
      'FAX_TXT=FAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'NUMERO=NUMERO'
      'CEP_TXT=CEP_TXT')
    DataSet = QrEndContratada
    BCDToCurrency = False
    Left = 92
    Top = 92
  end
  object frxDsEndContratante: TfrxDBDataset
    UserName = 'frxDsEndContratante'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Tipo=Tipo'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMERUA=NOMERUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMEUF=NOMEUF'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'TEL=TEL'
      'FAX=FAX'
      'CEP=CEP'
      'TEL_TXT=TEL_TXT'
      'CONTRATANTE=CONTRATANTE'
      'FAX_TXT=FAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'NUMERO=NUMERO'
      'CEP_TXT=CEP_TXT')
    DataSet = QrEndContratante
    BCDToCurrency = False
    Left = 92
    Top = 120
  end
  object frxDsLocCCon: TfrxDBDataset
    UserName = 'frxDsLocCCon'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'NO_EMP=NO_EMP'
      'NO_COMPRADOR=NO_COMPRADOR'
      'NO_RECEBEU=NO_RECEBEU'
      'Codigo=Codigo'
      'Contrato=Contrato'
      'Cliente=Cliente'
      'DtHrEmi=DtHrEmi'
      'DtHrBxa=DtHrBxa'
      'Vendedor=Vendedor'
      'ECComprou=ECComprou'
      'ECRetirou=ECRetirou'
      'NumOC=NumOC'
      'ValorTot=ValorTot'
      'LocalObra=LocalObra'
      'Obs0=Obs0'
      'Obs1=Obs1'
      'Obs2=Obs2'
      'NO_LOGIN=NO_LOGIN'
      'NO_CLIENTE=NO_CLIENTE'
      'DataEmi=DataEmi'
      'HoraEmi=HoraEmi'
      'DtHrBxa_TXT=DtHrBxa_TXT'
      'EndCobra=EndCobra'
      'LocalCntat=LocalCntat'
      'LocalFone=LocalFone'
      'LOCALFONE_TXT=LOCALFONE_TXT')
    DataSet = QrLocCCon
    BCDToCurrency = False
    Left = 92
    Top = 148
  end
  object QrLocCCon: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLocCConBeforeClose
    AfterScroll = QrLocCConAfterScroll
    OnCalcFields = QrLocCConCalcFields
    SQL.Strings = (
      'SELECT com.Nome NO_COMPRADOR, com.Nome NO_RECEBEU,'
      'lcc.*, sen.Login NO_LOGIN,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTE'
      'FROM locccon lcc'
      'LEFT JOIN entidades emp ON emp.Codigo=lcc.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=lcc.Cliente'
      'LEFT JOIN senhas sen ON sen.Numero=lcc.Vendedor'
      'LEFT JOIN enticontat com ON com.Controle=lcc.ECComprou'
      'LEFT JOIN enticontat ret ON ret.Controle=lcc.ECRetirou'
      'WHERE lcc.Codigo=:P0'
      '')
    Left = 64
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCConEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrLocCConNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrLocCConNO_COMPRADOR: TWideStringField
      FieldName = 'NO_COMPRADOR'
      Size = 30
    end
    object QrLocCConNO_RECEBEU: TWideStringField
      FieldName = 'NO_RECEBEU'
      Size = 30
    end
    object QrLocCConCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCConContrato: TIntegerField
      FieldName = 'Contrato'
    end
    object QrLocCConCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocCConDtHrEmi: TDateTimeField
      FieldName = 'DtHrEmi'
    end
    object QrLocCConDtHrBxa: TDateTimeField
      FieldName = 'DtHrBxa'
    end
    object QrLocCConVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocCConECComprou: TIntegerField
      FieldName = 'ECComprou'
    end
    object QrLocCConECRetirou: TIntegerField
      FieldName = 'ECRetirou'
    end
    object QrLocCConNumOC: TWideStringField
      FieldName = 'NumOC'
      Size = 60
    end
    object QrLocCConValorTot: TFloatField
      FieldName = 'ValorTot'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCConLocalObra: TWideStringField
      DisplayWidth = 100
      FieldName = 'LocalObra'
      Size = 50
    end
    object QrLocCConObs0: TWideStringField
      FieldName = 'Obs0'
      Size = 50
    end
    object QrLocCConObs1: TWideStringField
      FieldName = 'Obs1'
      Size = 50
    end
    object QrLocCConObs2: TWideStringField
      FieldName = 'Obs2'
      Size = 50
    end
    object QrLocCConNO_LOGIN: TWideStringField
      FieldName = 'NO_LOGIN'
      Required = True
      Size = 30
    end
    object QrLocCConNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrLocCConDataEmi: TDateField
      FieldKind = fkCalculated
      FieldName = 'DataEmi'
      Calculated = True
    end
    object QrLocCConHoraEmi: TTimeField
      FieldKind = fkCalculated
      FieldName = 'HoraEmi'
      Calculated = True
    end
    object QrLocCConDtHrBxa_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrBxa_TXT'
      Calculated = True
    end
    object QrLocCConEndCobra: TWideStringField
      FieldName = 'EndCobra'
      Size = 100
    end
    object QrLocCConLocalCntat: TWideStringField
      FieldName = 'LocalCntat'
      Size = 50
    end
    object QrLocCConLocalFone: TWideStringField
      FieldName = 'LocalFone'
    end
    object QrLocCConLOCALFONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LOCALFONE_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrLocCPatPri: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLocCPatPriBeforeClose
    AfterScroll = QrLocCPatPriAfterScroll
    OnCalcFields = QrLocCPatPriCalcFields
    SQL.Strings = (
      'SELECT ggp.AtualValr, lpp.*, '
      'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN '
      'FROM loccpatpri lpp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX'
      'LEFT JOIN gragxpatr ggp ON ggx.Controle=ggp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci'
      'WHERE lpp.Codigo=:P0'
      '')
    Left = 64
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCPatPriCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'loccpatpri.Codigo'
    end
    object QrLocCPatPriCtrID: TIntegerField
      FieldName = 'CtrID'
      Origin = 'loccpatpri.CtrID'
    end
    object QrLocCPatPriGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'loccpatpri.GraGruX'
    end
    object QrLocCPatPriValorDia: TFloatField
      FieldName = 'ValorDia'
      Origin = 'loccpatpri.ValorDia'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriValorSem: TFloatField
      FieldName = 'ValorSem'
      Origin = 'loccpatpri.ValorSem'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriValorQui: TFloatField
      FieldName = 'ValorQui'
      Origin = 'loccpatpri.ValorQui'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'loccpatpri.ValorMes'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCPatPriDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Origin = 'loccpatpri.DtHrRetorn'
    end
    object QrLocCPatPriLibFunci: TIntegerField
      FieldName = 'LibFunci'
      Origin = 'loccpatpri.LibFunci'
    end
    object QrLocCPatPriLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
      Origin = 'loccpatpri.LibDtHr'
    end
    object QrLocCPatPriRELIB: TWideStringField
      FieldName = 'RELIB'
      Origin = 'loccpatpri.RELIB'
    end
    object QrLocCPatPriHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Origin = 'loccpatpri.HOLIB'
      Size = 5
    end
    object QrLocCPatPriNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrLocCPatPriLOGIN: TWideStringField
      FieldName = 'LOGIN'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrLocCPatPriREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object QrLocCPatPriTXT_DTA_DEVOL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DTA_DEVOL'
      Calculated = True
    end
    object QrLocCPatPriTXT_DTA_LIBER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DTA_LIBER'
      Size = 30
      Calculated = True
    end
    object QrLocCPatPriTXT_QEM_LIBER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QEM_LIBER'
      Size = 100
      Calculated = True
    end
    object QrLocCPatPriDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Origin = 'loccpatpri.DtHrLocado'
    end
    object QrLocCPatPriAtualValr: TFloatField
      FieldName = 'AtualValr'
      Origin = 'gragxpatr.AtualValr'
    end
    object QrLocCPatPriRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Origin = 'loccpatpri.RetFunci'
    end
    object QrLocCPatPriRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Origin = 'loccpatpri.RetExUsr'
    end
    object QrLocCPatPriLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
      Origin = 'loccpatpri.LibExUsr'
    end
    object QrLocCPatPriVAL_SEM_CALC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_SEM_CALC'
      Calculated = True
    end
    object QrLocCPatPriVAL_QUI_CALC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_QUI_CALC'
      Calculated = True
    end
    object QrLocCPatPriVAL_MES_CALC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_MES_CALC'
      Calculated = True
    end
  end
  object QrLocCPatSec: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggp.AtualValr, lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA'
      'FROM loccpatsec lps'
      'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX'
      'LEFT JOIN gragxpatr ggp ON ggx.Controle=ggp.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE lps.CtrID>0'
      '')
    Left = 64
    Top = 212
    object QrLocCPatSecCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatSecCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatSecItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCPatSecGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatSecNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatSecREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocCPatSecAtualValr: TFloatField
      FieldName = 'AtualValr'
    end
  end
  object QrLocCPatAce: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA'
      'FROM loccpatace lps'
      'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE lps.CtrID>0'
      ''
      '')
    Left = 64
    Top = 240
    object QrLocCPatAceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatAceCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatAceItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCPatAceGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatAceNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatAceREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocCPatAceValBem: TFloatField
      FieldName = 'ValBem'
    end
  end
  object QrLocCPatCns: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLocCPatCnsCalcFields
    SQL.Strings = (
      'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA '
      'FROM loccpatcns lpc '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade'
      'WHERE lpc.CtrID>0')
    Left = 64
    Top = 268
    object QrLocCPatCnsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCPatCnsCtrID: TIntegerField
      FieldName = 'CtrID'
    end
    object QrLocCPatCnsItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocCPatCnsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrLocCPatCnsNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Size = 120
    end
    object QrLocCPatCnsREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Required = True
      Size = 25
    end
    object QrLocCPatCnsUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrLocCPatCnsQtdIni: TFloatField
      FieldName = 'QtdIni'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocCPatCnsQtdFim: TFloatField
      FieldName = 'QtdFim'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocCPatCnsQtdUso: TFloatField
      FieldName = 'QtdUso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrLocCPatCnsPrcUni: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrLocCPatCnsValUso: TFloatField
      FieldName = 'ValUso'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLocCPatCnsSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
  end
  object QrLocCPatUso: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA,'
      'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM '
      'FROM loccpatuso lpu '
      'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade'
      'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni'
      'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim'
      'WHERE lpu.CtrID>0'
      '')
    Left = 64
    Top = 296
    object QrLocCPatUsoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'loccpatuso.Codigo'
    end
    object QrLocCPatUsoCtrID: TIntegerField
      FieldName = 'CtrID'
      Origin = 'loccpatuso.CtrID'
    end
    object QrLocCPatUsoItem: TIntegerField
      FieldName = 'Item'
      Origin = 'loccpatuso.Item'
    end
    object QrLocCPatUsoGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'loccpatuso.GraGruX'
    end
    object QrLocCPatUsoNO_GGX: TWideStringField
      FieldName = 'NO_GGX'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrLocCPatUsoREFERENCIA: TWideStringField
      FieldName = 'REFERENCIA'
      Origin = 'gragru1.Referencia'
      Required = True
      Size = 25
    end
    object QrLocCPatUsoAvalIni: TIntegerField
      FieldName = 'AvalIni'
      Origin = 'loccpatuso.AvalIni'
    end
    object QrLocCPatUsoAvalFim: TIntegerField
      FieldName = 'AvalFim'
      Origin = 'loccpatuso.AvalFim'
    end
    object QrLocCPatUsoPrcUni: TFloatField
      FieldName = 'PrcUni'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCPatUsoValTot: TFloatField
      FieldName = 'ValTot'
      Origin = 'loccpatuso.ValTot'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLocCPatUsoUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'loccpatuso.Unidade'
    end
    object QrLocCPatUsoSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Origin = 'unidmed.Sigla'
      Size = 6
    end
    object QrLocCPatUsoNO_AVALINI: TWideStringField
      FieldName = 'NO_AVALINI'
      Origin = 'graglaval.Nome'
      Size = 60
    end
    object QrLocCPatUsoNO_AVALFIM: TWideStringField
      FieldName = 'NO_AVALFIM'
      Origin = 'graglaval.Nome'
      Size = 60
    end
  end
  object frxDsLocCPatPri: TfrxDBDataset
    UserName = 'frxDsLocCPatPri'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CtrID=CtrID'
      'GraGruX=GraGruX'
      'ValorDia=ValorDia'
      'ValorSem=ValorSem'
      'ValorQui=ValorQui'
      'ValorMes=ValorMes'
      'DtHrRetorn=DtHrRetorn'
      'LibFunci=LibFunci'
      'LibDtHr=LibDtHr'
      'RELIB=RELIB'
      'HOLIB=HOLIB'
      'NO_GGX=NO_GGX'
      'LOGIN=LOGIN'
      'REFERENCIA=REFERENCIA'
      'TXT_DTA_DEVOL=TXT_DTA_DEVOL'
      'TXT_DTA_LIBER=TXT_DTA_LIBER'
      'TXT_QEM_LIBER=TXT_QEM_LIBER'
      'DtHrLocado=DtHrLocado'
      'AtualValr=AtualValr'
      'RetFunci=RetFunci'
      'RetExUsr=RetExUsr'
      'LibExUsr=LibExUsr'
      'VAL_SEM_CALC=VAL_SEM_CALC'
      'VAL_QUI_CALC=VAL_QUI_CALC'
      'VAL_MES_CALC=VAL_MES_CALC')
    DataSet = QrLocCPatPri
    BCDToCurrency = False
    Left = 92
    Top = 184
  end
  object frxDsLocCPatSec: TfrxDBDataset
    UserName = 'frxDsLocCPatSec'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CtrID=CtrID'
      'Item=Item'
      'GraGruX=GraGruX'
      'NO_GGX=NO_GGX'
      'REFERENCIA=REFERENCIA'
      'AtualValr=AtualValr')
    DataSet = QrLocCPatSec
    BCDToCurrency = False
    Left = 92
    Top = 212
  end
  object frxDsLocCPatAce: TfrxDBDataset
    UserName = 'frxDsLocCPatAce'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CtrID=CtrID'
      'Item=Item'
      'GraGruX=GraGruX'
      'NO_GGX=NO_GGX'
      'REFERENCIA=REFERENCIA'
      'ValBem=ValBem')
    DataSet = QrLocCPatAce
    BCDToCurrency = False
    Left = 92
    Top = 240
  end
  object frxDsLocCPatCns: TfrxDBDataset
    UserName = 'frxDsLocCPatCns'
    CloseDataSource = False
    DataSet = QrLocCPatCns
    BCDToCurrency = False
    Left = 92
    Top = 268
  end
  object frxDsLocCPatUso: TfrxDBDataset
    UserName = 'frxDsLocCPatUso'
    CloseDataSource = False
    DataSet = QrLocCPatUso
    BCDToCurrency = False
    Left = 92
    Top = 296
  end
  object frxLOC_PATRI_001_001: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41802.645199189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxLOC_PATRI_001_001GetValue
    Left = 320
    Top = 136
    Datasets = <
      item
        DataSet = frxDsContratos
        DataSetName = 'frxDsContratos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEndContratada
        DataSetName = 'frxDsEndContratada'
      end
      item
        DataSet = frxDsEndContratante
        DataSetName = 'frxDsEndContratante'
      end
      item
        DataSet = frxDsLocCCon
        DataSetName = 'frxDsLocCCon'
      end
      item
        DataSet = frxDsLocCPatAce
        DataSetName = 'frxDsLocCPatAce'
      end
      item
        DataSet = frxDsLocCPatCns
        DataSetName = 'frxDsLocCPatCns'
      end
      item
        DataSet = frxDsLocCPatPri
        DataSetName = 'frxDsLocCPatPri'
      end
      item
        DataSet = frxDsLocCPatSec
        DataSetName = 'frxDsLocCPatSec'
      end
      item
        DataSet = frxDsLocCPatUso
        DataSetName = 'frxDsLocCPatUso'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 529.133858270000000000
        Top = 16.000000000000000000
        Width = 680.315400000000000000
        RowCount = 2
        object Subreport1: TfrxSubreport
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Page = frxLOC_PATRI_001_001.Page2
        end
      end
    end
    object Page2: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 210.000000000000000000
      PaperHeight = 148.000000000000000000
      PaperSize = 11
      LeftMargin = 5.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 20.000000000000000000
      EndlessHeight = True
      LargeDesignHeight = True
      object DetailData4: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 336.378170000000000000
        Width = 737.008350000000000000
        DataSet = frxDsLocCPatSec
        DataSetName = 'frxDsLocCPatSec'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo117: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 18.897650000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatSec
          DataSetName = 'frxDsLocCPatSec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatSec."REFERENCIA"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 124.724490000000000000
          Width = 536.693260000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatSec
          DataSetName = 'frxDsLocCPatSec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatSec."NO_GGX"]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Top = 374.173470000000000000
        Width = 737.008350000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 283.464750000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsLocCPatSec."Codigo"'
        object Memo113: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 18.897650000000000000
          Top = 15.118119999999920000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 124.724490000000000000
          Top = 15.118119999999920000
          Width = 536.693260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Patrim'#244'nios Secund'#225'rios')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 396.850650000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsLocCPatAce."Codigo"'
        object Memo114: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 18.897650000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 124.724490000000000000
          Top = 15.118119999999980000
          Width = 536.693260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Acess'#243'rios')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object DetailData5: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 449.764070000000000000
        Width = 737.008350000000000000
        DataSet = frxDsLocCPatAce
        DataSetName = 'frxDsLocCPatAce'
        RowCount = 0
        object Memo118: TfrxMemoView
          Left = 661.417322834646000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 18.897650000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatAce
          DataSetName = 'frxDsLocCPatAce'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatAce."REFERENCIA"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Left = 124.724490000000000000
          Width = 536.693260000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatAce
          DataSetName = 'frxDsLocCPatAce'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatAce."NO_GGX"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = -0.000000000000004441
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Top = 487.559370000000000000
        Width = 737.008350000000000000
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 510.236550000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsLocCPatUso."Codigo"'
        object Memo115: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 18.897650000000000000
          Top = 15.118119999999920000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 124.724490000000000000
          Top = 15.118119999999920000
          Width = 536.693260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Material de Uso')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object DetailData6: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 563.149970000000000000
        Width = 737.008350000000000000
        DataSet = frxDsLocCPatUso
        DataSetName = 'frxDsLocCPatUso'
        RowCount = 0
        object Memo119: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 18.897650000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatUso
          DataSetName = 'frxDsLocCPatUso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatUso."REFERENCIA"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 124.724490000000000000
          Width = 536.693260000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatUso
          DataSetName = 'frxDsLocCPatUso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatUso."NO_GGX"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 600.945270000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsLocCPatCns."Codigo"'
        object Memo116: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 18.897650000000000000
          Top = 15.118119999999980000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 124.724490000000000000
          Top = 15.118119999999980000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Left = 18.897650000000000000
          Width = 642.520100000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Material de Consumo')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 585.827150000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade usado')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 510.236550000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade final')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 434.645950000000000000
          Top = 15.118119999999980000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade inicial')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 377.953000000000000000
          Top = 15.118119999999980000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'SIGLA')
          ParentFont = False
        end
      end
      object DetailData7: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 653.858690000000000000
        Width = 737.008350000000000000
        DataSet = frxDsLocCPatCns
        DataSetName = 'frxDsLocCPatCns'
        RowCount = 0
        object Memo111: TfrxMemoView
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 18.897650000000000000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatCns."REFERENCIA"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Left = 124.724490000000000000
          Width = 253.228510000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsLocCPatCns."NO_GGX"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 585.827150000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_QtdUso]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 510.236550000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'QtdFim'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatCns."QtdFim"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 434.645950000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataField = 'QtdIni'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocCPatCns."QtdIni"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 377.953000000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'SIGLA'
          DataSet = frxDsLocCPatCns
          DataSetName = 'frxDsLocCPatCns'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatCns."SIGLA"]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        FillType = ftBrush
        Top = 691.653990000000000000
        Width = 737.008350000000000000
      end
      object GroupHeader6: TfrxGroupHeader
        FillType = ftBrush
        Top = 143.622140000000000000
        Width = 737.008350000000000000
        Condition = 'frxDsLocCPatPri."CtrID"'
      end
      object GroupFooter6: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897637795275590000
        Top = 241.889920000000000000
        Width = 737.008350000000000000
        object Memo103: TfrxMemoView
          Width = 680.315400000000000000
          Height = 7.559060000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 53.692940240000000000
        Top = 166.299320000000000000
        Width = 737.008350000000000000
        DataSet = frxDsLocCPatPri
        DataSetName = 'frxDsLocCPatPri'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo112: TfrxMemoView
          Top = 46.133889999999950000
          Width = 680.315400000000000000
          Height = 7.559050240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 14671839
          HAlign = haCenter
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Top = 15.118120000000090000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 105.826840000000000000
          Top = 15.118120000000090000
          Width = 574.488560000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Top = 30.236240000000060000
          Width = 105.826840000000000000
          Height = 15.118110240000000000
          DataField = 'REFERENCIA'
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocCPatPri."REFERENCIA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 30.236240000000060000
          Width = 574.488560000000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGX'
          DataSet = frxDsLocCPatPri
          DataSetName = 'frxDsLocCPatPri'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCPatPri."NO_GGX"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Patrim'#244'nio Principal')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 132.283550000000000000
        Top = 714.331170000000000000
        Width = 737.008350000000000000
        RowCount = 1
        object Memo151: TfrxMemoView
          Top = 75.370130000000010000
          Width = 332.598640000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]'
            'Contratante')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Width = 332.598449610000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Equipamento devolvido ao locador em: [VAR_DATAHORAENCER]')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 347.716760000000000000
          Top = 75.370130000000010000
          Width = 332.598640000000000000
          Height = 30.236240000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]'
            'Contratada')
          ParentFont = False
        end
        object Line41: TfrxLineView
          Top = 132.283550000000000000
          Width = 789.921770000000000000
          Color = clBlack
          ArrowLength = 100
          ArrowWidth = 20
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 102.047300240000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        RowCount = 1
        object Memo1: TfrxMemoView
          Top = 56.692949999999990000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 79.370130000000000000
          Top = 56.692913390000000000
          Width = 600.945270000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCCon."NO_CLIENTE"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 71.811070000000000000
          Width = 79.370078740000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de retirada:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 86.929190000000000000
          Width = 79.370078740000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#245'es:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 79.370130000000000000
          Top = 86.929190000000000000
          Width = 600.945270000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              '[frxDsLocCCon."Obs0"] [frxDsLocCCon."Obs1"] [frxDsLocCCon."Obs2"' +
              ']')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'DEVOLU'#199#195'O DA LOCA'#199#195'O [frxDsLocCCon."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 79.370130000000000000
          Top = 71.811070000000000000
          Width = 600.945270000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLocCCon."LocalObra"]')
          ParentFont = False
        end
      end
    end
  end
end
