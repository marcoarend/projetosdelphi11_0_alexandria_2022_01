unit ContratApp2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxRich, dmkGeral, DB, mySQLDbTables, frxDBSet,
  UnDmkEnums, UnAppEnums, UnInternalConsts, Vcl.StdCtrls;

type
  TFmContratApp2 = class(TForm)
    frxLOC_PATRI_101_001_07: TfrxReport;
    frxRichObject1: TfrxRichObject;
    QrEndContratada: TmySQLQuery;
    QrEndContratadaTipo: TSmallintField;
    QrEndContratadaCONTRATADA: TWideStringField;
    QrEndContratadaNOMELOGRAD: TWideStringField;
    QrEndContratadaNOMERUA: TWideStringField;
    QrEndContratadaCOMPL: TWideStringField;
    QrEndContratadaBAIRRO: TWideStringField;
    QrEndContratadaNOMEUF: TWideStringField;
    QrEndContratadaCNPJ_CPF: TWideStringField;
    QrEndContratadaIE_RG: TWideStringField;
    QrEndContratadaCodigo: TIntegerField;
    QrEndContratadaTEL: TWideStringField;
    QrEndContratadaFAX: TWideStringField;
    QrEndContratadaTEL_TXT: TWideStringField;
    QrEndContratadaFAX_TXT: TWideStringField;
    QrEndContratadaCNPJ_TXT: TWideStringField;
    QrEndContratante: TmySQLQuery;
    QrEndContratanteCodigo: TIntegerField;
    QrEndContratanteTipo: TSmallintField;
    QrEndContratanteNOMELOGRAD: TWideStringField;
    QrEndContratanteNOMERUA: TWideStringField;
    QrEndContratanteCOMPL: TWideStringField;
    QrEndContratanteBAIRRO: TWideStringField;
    QrEndContratanteNOMEUF: TWideStringField;
    QrEndContratanteCNPJ_CPF: TWideStringField;
    QrEndContratanteIE_RG: TWideStringField;
    QrEndContratanteTEL: TWideStringField;
    QrEndContratanteFAX: TWideStringField;
    QrEndContratanteTEL_TXT: TWideStringField;
    QrEndContratanteCONTRATANTE: TWideStringField;
    QrEndContratanteFAX_TXT: TWideStringField;
    QrEndContratanteCNPJ_TXT: TWideStringField;
    QrContrato: TmySQLQuery;
    QrContratoCodigo: TIntegerField;
    QrContratoContratada: TIntegerField;
    QrContratoContratante: TIntegerField;
    QrContratoDContrato: TDateField;
    QrContratoDInstal: TDateField;
    QrContratoDVencimento: TDateField;
    QrContratoStatus: TSmallintField;
    QrContratoAtivo: TSmallintField;
    QrContratoValorMes: TFloatField;
    QrContratoNCONTRATADA: TWideStringField;
    QrContratoNCONTRATANTE: TWideStringField;
    QrContratoWinDocArq: TWideStringField;
    QrContratoFormaUso: TSmallintField;
    QrContratoNome: TWideStringField;
    QrContratoDtaPropIni: TDateField;
    QrContratoDtaPropFim: TDateField;
    QrContratoDtaCntrFim: TDateField;
    QrContratoDtaImprime: TDateTimeField;
    QrContratoDtaAssina: TDateField;
    QrContratoTratamento: TWideStringField;
    QrContratoArtigoDef: TSmallintField;
    QrContratoPerDefImpl: TSmallintField;
    QrContratoPerQtdImpl: TSmallintField;
    QrContratoPerDefTrei: TSmallintField;
    QrContratoPerQtdTrei: TSmallintField;
    QrContratoPerDefMoni: TSmallintField;
    QrContratoPerQtdMoni: TSmallintField;
    QrContratoPerDefIniI: TSmallintField;
    QrContratoPerQtdIniI: TSmallintField;
    QrContratoRepreLegal: TIntegerField;
    QrContratoTestemun01: TIntegerField;
    QrContratoTestemun02: TIntegerField;
    QrContratoPercMulta: TFloatField;
    QrContratoPercJuroM: TFloatField;
    QrContratoTpPagDocu: TWideStringField;
    QrContratoddMesVcto: TSmallintField;
    QrContratoTexto: TWideMemoField;
    QrContratoNO_REP_LEGAL: TWideStringField;
    QrContratoNO_TESTEM_01: TWideStringField;
    QrContratoNO_TESTEM_02: TWideStringField;
    QrContratoLugarNome: TWideStringField;
    QrContratoLugArtDef: TSmallintField;
    QrContratoVersao: TFloatField;
    frxDsContratos: TfrxDBDataset;
    frxDsEndContratada: TfrxDBDataset;
    frxDsEndContratante: TfrxDBDataset;
    frxDsLocCConCab: TfrxDBDataset;
    QrLocCConCab: TMySQLQuery;
    QrLocCConCabEmpresa: TIntegerField;
    QrLocCConCabNO_EMP: TWideStringField;
    QrLocCConCabNO_COMPRADOR: TWideStringField;
    QrLocCConCabNO_RECEBEU: TWideStringField;
    QrLocCConCabCodigo: TIntegerField;
    QrLocCConCabContrato: TIntegerField;
    QrLocCConCabCliente: TIntegerField;
    QrLocCConCabDtHrEmi: TDateTimeField;
    QrLocCConCabDtHrBxa: TDateTimeField;
    QrLocCConCabVendedor: TIntegerField;
    QrLocCConCabECComprou: TIntegerField;
    QrLocCConCabECRetirou: TIntegerField;
    QrLocCConCabNumOC: TWideStringField;
    QrLocCConCabValorTot: TFloatField;
    QrLocCConCabLocalObra: TWideStringField;
    QrLocCConCabObs0: TWideStringField;
    QrLocCConCabObs1: TWideStringField;
    QrLocCConCabObs2: TWideStringField;
    QrLocCConCabNO_LOGIN: TWideStringField;
    QrLocCConCabNO_CLIENTE: TWideStringField;
    QrLocCConCabDataEmi: TDateField;
    QrLocCConCabHoraEmi: TTimeField;
    QrLocCConCabDtHrBxa_TXT: TWideStringField;
    QrLocCConCabEndCobra: TWideStringField;
    QrLocCConCabLocalCntat: TWideStringField;
    QrLocCConCabLocalFone: TWideStringField;
    QrLocCConCabLOCALFONE_TXT: TWideStringField;
    QrEndContratadaCEP_TXT: TWideStringField;
    QrEndContratanteCEP_TXT: TWideStringField;
    QrEndContratadaCEP: TFloatField;
    QrEndContratanteCEP: TFloatField;
    QrLocIPatSec: TMySQLQuery;
    frxDsLocIPatPri: TfrxDBDataset;
    frxDsLocIPatSec: TfrxDBDataset;
    frxDsLocIPatAce: TfrxDBDataset;
    frxDsLocIPatCns: TfrxDBDataset;
    frxDsLocIPatUso: TfrxDBDataset;
    QrEndContratadaNUMERO: TFloatField;
    QrEndContratanteNUMERO: TFloatField;
    frxLOC_PATRI_101_002_10: TfrxReport;
    QrLocIPatPri: TMySQLQuery;
    QrLocIPatPriCodigo: TIntegerField;
    QrLocIPatPriCtrID: TIntegerField;
    QrLocIPatPriGraGruX: TIntegerField;
    QrLocIPatPriValorDia: TFloatField;
    QrLocIPatPriValorSem: TFloatField;
    QrLocIPatPriValorQui: TFloatField;
    QrLocIPatPriValorMes: TFloatField;
    QrLocIPatPriDtHrRetorn: TDateTimeField;
    QrLocIPatPriLibFunci: TIntegerField;
    QrLocIPatPriLibDtHr: TDateTimeField;
    QrLocIPatPriRELIB: TWideStringField;
    QrLocIPatPriHOLIB: TWideStringField;
    QrLocIPatPriNO_GGX: TWideStringField;
    QrLocIPatPriLOGIN: TWideStringField;
    QrLocIPatPriREFERENCIA: TWideStringField;
    QrLocIPatPriTXT_DTA_DEVOL: TWideStringField;
    QrLocIPatPriTXT_DTA_LIBER: TWideStringField;
    QrLocIPatPriTXT_QEM_LIBER: TWideStringField;
    QrLocIPatPriDtHrLocado: TDateTimeField;
    QrLocIPatPriLibExUsr: TIntegerField;
    QrLocIPatPriRetFunci: TIntegerField;
    QrLocIPatPriRetExUsr: TIntegerField;
    QrLocIPatPriValorProduto: TFloatField;
    QrLocIPatPriQtdeLocacao: TIntegerField;
    QrLocIPatPriValorLocacao: TFloatField;
    QrLocIPatPriQtdeDevolucao: TIntegerField;
    QrLocIPatPriTroca: TSmallintField;
    QrLocIPatPriCategoria: TWideStringField;
    QrLocIPatPriCobrancaConsumo: TFloatField;
    QrLocIPatPriCobrancaRealizadaVenda: TWideStringField;
    QrLocIPatPriSaiDtHr: TDateTimeField;
    QrLocIPatPriQtdeProduto: TIntegerField;
    QrLocIPatPriCOBRANCALOCACAO: TWideStringField;
    QrLocIPatPriCobranIniDtHr: TDateTimeField;
    QrLocIPatPriItem: TIntegerField;
    QrLocIPatPriVAL_SEM_CALC: TFloatField;
    QrLocIPatPriVAL_QUI_CALC: TFloatField;
    QrLocIPatPriVAL_MES_CALC: TFloatField;
    QrLocCConIts: TMySQLQuery;
    QrLocCConItsNO_IDTab: TWideStringField;
    QrLocCConItsCodigo: TIntegerField;
    QrLocCConItsCtrID: TIntegerField;
    QrLocCConItsIDTab: TIntegerField;
    QrLocCConItsStatus: TSmallintField;
    QrLocCConItsNO_STATUS: TWideStringField;
    QrLocCConItsFatID: TIntegerField;
    QrLocCConItsFatNum: TIntegerField;
    QrLocCConItsValorOrca: TFloatField;
    QrLocCConItsValorPedi: TFloatField;
    QrLocCConItsValorFatu: TFloatField;
    QrLocCConItsValorStat: TFloatField;
    QrLocCConItsPediVda: TIntegerField;
    QrLocCConItsFatPedCab: TIntegerField;
    QrLocCConItsFatPedVolCnta: TIntegerField;
    frxDsLocCConIts: TfrxDBDataset;
    QrLocIPatSecCodigo: TIntegerField;
    QrLocIPatSecCtrID: TIntegerField;
    QrLocIPatSecItem: TIntegerField;
    QrLocIPatSecManejoLca: TSmallintField;
    QrLocIPatSecGraGruX: TIntegerField;
    QrLocIPatSecValorDia: TFloatField;
    QrLocIPatSecValorSem: TFloatField;
    QrLocIPatSecValorQui: TFloatField;
    QrLocIPatSecValorMes: TFloatField;
    QrLocIPatSecRetFunci: TIntegerField;
    QrLocIPatSecRetExUsr: TIntegerField;
    QrLocIPatSecDtHrLocado: TDateTimeField;
    QrLocIPatSecDtHrRetorn: TDateTimeField;
    QrLocIPatSecLibExUsr: TIntegerField;
    QrLocIPatSecLibFunci: TIntegerField;
    QrLocIPatSecLibDtHr: TDateTimeField;
    QrLocIPatSecRELIB: TWideStringField;
    QrLocIPatSecHOLIB: TWideStringField;
    QrLocIPatSecQtdeProduto: TIntegerField;
    QrLocIPatSecValorProduto: TFloatField;
    QrLocIPatSecQtdeLocacao: TIntegerField;
    QrLocIPatSecValorLocacao: TFloatField;
    QrLocIPatSecQtdeDevolucao: TIntegerField;
    QrLocIPatSecTroca: TSmallintField;
    QrLocIPatSecCategoria: TWideStringField;
    QrLocIPatSecCOBRANCALOCACAO: TWideStringField;
    QrLocIPatSecCobrancaConsumo: TFloatField;
    QrLocIPatSecCobrancaRealizadaVenda: TWideStringField;
    QrLocIPatSecCobranIniDtHr: TDateTimeField;
    QrLocIPatSecSaiDtHr: TDateTimeField;
    QrLocIPatSecValorFDS: TFloatField;
    QrLocIPatSecDMenos: TSmallintField;
    QrLocIPatSecValorLocAAdiantar: TFloatField;
    QrLocIPatSecValorLocAtualizado: TFloatField;
    QrLocIPatSecCalcAdiLOCACAO: TWideStringField;
    QrLocIPatSecDiasUteis: TIntegerField;
    QrLocIPatSecDiasFDS: TIntegerField;
    QrLocIPatSecNO_GGX: TWideStringField;
    QrLocIPatSecREFERENCIA: TWideStringField;
    QrLocIPatSecLOGIN: TWideStringField;
    QrLocIPatAce: TMySQLQuery;
    QrLocIPatAceCodigo: TIntegerField;
    QrLocIPatAceCtrID: TIntegerField;
    QrLocIPatAceItem: TIntegerField;
    QrLocIPatAceGraGruX: TIntegerField;
    QrLocIPatAceNO_GGX: TWideStringField;
    QrLocIPatAceREFERENCIA: TWideStringField;
    QrLocIPatAceQtdeProduto: TIntegerField;
    QrLocIPatAceValorProduto: TFloatField;
    QrLocIPatAceQtdeLocacao: TIntegerField;
    QrLocIPatAceValorLocacao: TFloatField;
    QrLocIPatAceQtdeDevolucao: TIntegerField;
    QrLocIPatAceDtHrLocado: TDateTimeField;
    QrLocIPatAceDtHrRetorn: TDateTimeField;
    QrLocIPatCns: TMySQLQuery;
    QrLocIPatCnsCodigo: TIntegerField;
    QrLocIPatCnsCtrID: TIntegerField;
    QrLocIPatCnsItem: TIntegerField;
    QrLocIPatCnsGraGruX: TIntegerField;
    QrLocIPatCnsNO_GGX: TWideStringField;
    QrLocIPatCnsREFERENCIA: TWideStringField;
    QrLocIPatCnsUnidade: TIntegerField;
    QrLocIPatCnsQtdIni: TFloatField;
    QrLocIPatCnsQtdFim: TFloatField;
    QrLocIPatCnsQtdUso: TFloatField;
    QrLocIPatCnsPrcUni: TFloatField;
    QrLocIPatCnsValUso: TFloatField;
    QrLocIPatCnsSIGLA: TWideStringField;
    QrLocIPatUso: TMySQLQuery;
    QrLocIPatUsoCodigo: TIntegerField;
    QrLocIPatUsoCtrID: TIntegerField;
    QrLocIPatUsoItem: TIntegerField;
    QrLocIPatUsoGraGruX: TIntegerField;
    QrLocIPatUsoNO_GGX: TWideStringField;
    QrLocIPatUsoREFERENCIA: TWideStringField;
    QrLocIPatUsoAvalIni: TIntegerField;
    QrLocIPatUsoAvalFim: TIntegerField;
    QrLocIPatUsoPrcUni: TFloatField;
    QrLocIPatUsoValUso: TFloatField;
    QrLocIPatUsoUnidade: TIntegerField;
    QrLocIPatUsoSIGLA: TWideStringField;
    QrLocIPatUsoNO_AVALINI: TWideStringField;
    QrLocIPatUsoNO_AVALFIM: TWideStringField;
    QrLocIPatApo: TMySQLQuery;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    IntegerField8: TIntegerField;
    WideStringField3: TWideStringField;
    QrLocCItsSvc: TMySQLQuery;
    QrLocCItsSvcCodigo: TIntegerField;
    QrLocCItsSvcCtrID: TIntegerField;
    QrLocCItsSvcItem: TIntegerField;
    QrLocCItsSvcManejoLca: TSmallintField;
    QrLocCItsSvcGraGruX: TIntegerField;
    QrLocCItsSvcQuantidade: TFloatField;
    QrLocCItsSvcPrcUni: TFloatField;
    QrLocCItsSvcValorTotal: TFloatField;
    QrLocCItsSvcQtdeProduto: TIntegerField;
    QrLocCItsSvcValorProduto: TFloatField;
    QrLocCItsSvcQtdeLocacao: TIntegerField;
    QrLocCItsSvcValorLocacao: TFloatField;
    QrLocCItsSvcQtdeDevolucao: TIntegerField;
    QrLocCItsSvcTroca: TSmallintField;
    QrLocCItsSvcCategoria: TWideStringField;
    QrLocCItsSvcCOBRANCALOCACAO: TWideStringField;
    QrLocCItsSvcCobrancaConsumo: TFloatField;
    QrLocCItsSvcCobrancaRealizadaVenda: TWideStringField;
    QrLocCItsSvcCobranIniDtHr: TDateTimeField;
    QrLocCItsSvcSaiDtHr: TDateTimeField;
    QrLocCItsSvcLk: TIntegerField;
    QrLocCItsSvcDataCad: TDateField;
    QrLocCItsSvcDataAlt: TDateField;
    QrLocCItsSvcUserCad: TIntegerField;
    QrLocCItsSvcUserAlt: TIntegerField;
    QrLocCItsSvcAlterWeb: TSmallintField;
    QrLocCItsSvcAWServerID: TIntegerField;
    QrLocCItsSvcAWStatSinc: TSmallintField;
    QrLocCItsSvcAtivo: TSmallintField;
    QrLocCItsSvcNO_GGX: TWideStringField;
    QrLocCItsSvcREFERENCIA: TWideStringField;
    QrLocCItsVen: TMySQLQuery;
    QrLocCItsVenCodigo: TIntegerField;
    QrLocCItsVenCtrID: TIntegerField;
    QrLocCItsVenItem: TIntegerField;
    QrLocCItsVenManejoLca: TSmallintField;
    QrLocCItsVenGraGruX: TIntegerField;
    QrLocCItsVenUnidade: TIntegerField;
    QrLocCItsVenQuantidade: TFloatField;
    QrLocCItsVenPrcUni: TFloatField;
    QrLocCItsVenValorTotal: TFloatField;
    QrLocCItsVenQtdeProduto: TIntegerField;
    QrLocCItsVenValorProduto: TFloatField;
    QrLocCItsVenQtdeLocacao: TIntegerField;
    QrLocCItsVenValorLocacao: TFloatField;
    QrLocCItsVenQtdeDevolucao: TIntegerField;
    QrLocCItsVenTroca: TSmallintField;
    QrLocCItsVenCategoria: TWideStringField;
    QrLocCItsVenCOBRANCALOCACAO: TWideStringField;
    QrLocCItsVenCobrancaConsumo: TFloatField;
    QrLocCItsVenCobrancaRealizadaVenda: TWideStringField;
    QrLocCItsVenCobranIniDtHr: TDateTimeField;
    QrLocCItsVenSaiDtHr: TDateTimeField;
    QrLocCItsVenLk: TIntegerField;
    QrLocCItsVenDataCad: TDateField;
    QrLocCItsVenDataAlt: TDateField;
    QrLocCItsVenUserCad: TIntegerField;
    QrLocCItsVenUserAlt: TIntegerField;
    QrLocCItsVenAlterWeb: TSmallintField;
    QrLocCItsVenAWServerID: TIntegerField;
    QrLocCItsVenAWStatSinc: TSmallintField;
    QrLocCItsVenAtivo: TSmallintField;
    QrLocCItsVenNO_GGX: TWideStringField;
    QrLocCItsVenREFERENCIA: TWideStringField;
    QrLocCItsVenSIGLA: TWideStringField;
    QrLocCItsVenSMIA_OriCtrl: TIntegerField;
    QrLocCItsVenSMIA_IDCtrl: TIntegerField;
    frxDsLocIPatApo: TfrxDBDataset;
    QrLocCConCabImportado: TSmallintField;
    QrLocCConCabDtHrDevolver: TDateTimeField;
    QrLocCConCabDtHrSai: TDateTimeField;
    QrLocCConCabValorEquipamentos: TFloatField;
    QrLocCConCabValorLocacao: TFloatField;
    QrLocCConCabValorAdicional: TFloatField;
    QrLocCConCabValorASerAdiantado: TFloatField;
    QrLocCConCabValorAdiantamento: TFloatField;
    QrLocCConCabValorDesconto: TFloatField;
    QrLocCConCabTipoAluguel: TWideStringField;
    QrLocCConCabStatus: TIntegerField;
    QrLocCConCabDtUltCobMens: TDateField;
    QrLocCConCabFormaCobrLoca: TSmallintField;
    QrLocCConCabDtUltAtzPend: TDateField;
    QrLocCConCabValorProdutos: TFloatField;
    QrLocCConCabValorLocAAdiantar: TFloatField;
    QrLocCConCabValorLocAtualizado: TFloatField;
    QrLocCConCabValorServicos: TFloatField;
    QrLocCConCabValorVendas: TFloatField;
    QrLocCConCabValorOrca: TFloatField;
    QrLocCConCabValorPedi: TFloatField;
    QrLocCConCabValorFatu: TFloatField;
    QrLocCConCabValorPago: TFloatField;
    QrEndContratadaNO_CIDADE: TWideStringField;
    QrEndContratadaNO_PAIS: TWideStringField;
    QrEndContratanteNO_CIDADE: TWideStringField;
    QrEndContratanteNO_PAIS: TWideStringField;
    QrLocIPatCnsValorRucAAdiantar: TFloatField;
    QrLocIPatUsoValorRucAAdiantar: TFloatField;
    QrLocIPatPriValorLocAAdiantar: TFloatField;
    QrLocIPatPriPatrimonio: TWideStringField;
    frxDsLocCItsSvc: TfrxDBDataset;
    frxDsLocCItsVen: TfrxDBDataset;
    QrLocCConCabNO_TipoAluguel: TWideStringField;
    QrLocCConCabHistImprime: TWideMemoField;
    QrLocCConCabHistNaoImpr: TWideMemoField;
    QrLocCItsVenValDevolParci: TFloatField;
    frxLOC_PATRI_101_004: TfrxReport;
    QrSumLoc: TMySQLQuery;
    frxDsSumLoc: TfrxDBDataset;
    QrSumLocValorIndeniz: TFloatField;
    QrSumLocValorLocacao: TFloatField;
    QrSumSvc: TMySQLQuery;
    QrSumSvcValorTotal: TFloatField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    QrEndContratanteCEL: TWideStringField;
    QrEndContratanteCEL_TXT: TWideStringField;
    QrEndContratadaEmail: TWideStringField;
    QrEndContratanteEmail: TWideStringField;
    QrEndContratadaCEL_TXT: TWideStringField;
    QrEndContratadaCEL: TWideStringField;
    QrLocFCab: TMySQLQuery;
    QrLocFCabCodigo: TIntegerField;
    QrLocFCabControle: TIntegerField;
    QrLocFCabDtHrFat: TDateTimeField;
    QrLocFCabValLocad: TFloatField;
    QrLocFCabValConsu: TFloatField;
    QrLocFCabValUsado: TFloatField;
    QrLocFCabValDesco: TFloatField;
    QrLocFCabValTotal: TFloatField;
    QrLocFCabSerNF: TWideStringField;
    QrLocFCabNumNF: TIntegerField;
    QrLocFCabCondicaoPG: TIntegerField;
    QrLocFCabCartEmis: TIntegerField;
    QrLocFCabValVenda: TFloatField;
    QrLocFCabValFrete: TFloatField;
    QrLocFCabValServi: TFloatField;
    QrLocFCabCodHist: TIntegerField;
    QrLocFCabPercDesco: TFloatField;
    QrLctFatRef: TMySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    QrLocFCabPercCalc: TFloatField;
    QrLctFatRefValCalc: TFloatField;
    frxDsLocFCab: TfrxDBDataset;
    frxDsLctFatRef: TfrxDBDataset;
    QrLocFCabValCalc: TFloatField;
    QrLocFCabNO_CondicaoPG: TWideStringField;
    frxLOC_PATRI_101_005: TfrxReport;
    QrLocCConItsNF_Stat: TIntegerField;
    QrLocCConItside_mod: TIntegerField;
    QrLocCConItside_nNF: TIntegerField;
    QrLocCItsVenGraGru1: TIntegerField;
    QrLocCItsVenprod_vFrete: TFloatField;
    QrLocCItsVenprod_vSeg: TFloatField;
    QrLocCItsVenprod_vDesc: TFloatField;
    QrLocCItsVenprod_vOutro: TFloatField;
    QrLocCItsVenStqCenCad: TIntegerField;
    QrLocCItsVenPercDesco: TFloatField;
    QrLocCItsVenValorBruto: TFloatField;
    frxLOC_PATRI_101_003: TfrxReport;
    frxLOC_PATRI_101_007: TfrxReport;
    QrLocCConItsCondiOrca: TIntegerField;
    QrLocCConItsNO_CondiOrca: TWideStringField;
    Label6: TLabel;
    Label7: TLabel;
    frxLOC_PATRI_101_006: TfrxReport;
    Label8: TLabel;
    frxLOC_PATRI_101_008: TfrxReport;
    QrSumRuc: TMySQLQuery;
    QrSumRucValorIndeniz: TFloatField;
    QrSumRucValorLocacao: TFloatField;
    frxLOC_PATRI_101_001_10: TfrxReport;
    frxLOC_PATRI_101_002_07: TfrxReport;
    _frxLOC_PATRI_101_002_: TfrxReport;
    QrLocFCabValVeMaq: TFloatField;
    QrLocIPatAceQTDE: TFloatField;
    QrLocIPatSecQTDE: TFloatField;
    procedure frxLOC_PATRI_101_001_07GetValue(const VarName: string; var Value: Variant);
    procedure QrEndContratadaCalcFields(DataSet: TDataSet);
    procedure QrEndContratanteCalcFields(DataSet: TDataSet);
    procedure QrLocCConCabCalcFields(DataSet: TDataSet);
    procedure QrLocIPatPriCalcFields(DataSet: TDataSet);
    procedure QrLocCConCabBeforeClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure frxLOC_PATRI_101_002_10GetValue(const VarName: string;
      var Value: Variant);
    procedure QrLocCConItsAfterScroll(DataSet: TDataSet);
    procedure QrLocCConItsBeforeClose(DataSet: TDataSet);
    procedure QrLocIPatCnsCalcFields(DataSet: TDataSet);
    procedure QrLocCConCabAfterOpen(DataSet: TDataSet);
    procedure QrLocFCabAfterScroll(DataSet: TDataSet);
    procedure QrLocFCabBeforeClose(DataSet: TDataSet);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCtrID: Integer;
    //FLocIPatPriCtrID: String;
    procedure ReopenLocCConIts(CtrID: Integer);
              //StatusMovimento: TStatusMovimento);
    procedure ReopenLocIPatPri();
    procedure ReopenLocIPatSec();
    procedure ReopenLocIPatAce();
    procedure ReopenLocIPatUso();
    procedure ReopenLocIPatCns();
    procedure ReopenLocIPatApo();
    procedure ReopenLocCItsSvc();
    procedure ReopenLocCItsVen();
    //
    procedure ReopenContrato();
    procedure ReopenEndContratante();
    procedure ReopenEndContratada();
    procedure ReopenLocCConCab();
    procedure ReopenSumLoc();
    //
    procedure ReopenLocFCab(IDTab: TTabConLocIts);
    procedure ReopenLctFatRef();

  public
    { Public declarations }
    FLocCConCab: Integer;
    FTodos: Boolean;
    //FStatusMovimento: TStatusMovimento;
    FSQLStatusMovimentos, FSQLAtrelamento, FSQLMovimentoIDTab, FPatrimonioAtual_TXT: String;
    //
    procedure ImprimeContrato(Fonte: Integer);
    procedure ImprimeDevolucao((*LocIPatPriCtrID: String*)Todos: Boolean; Fonte: Integer);
    procedure ImprimeFichaDeTroca();
    procedure ImprimeOrcamentoLocacao();
(*
    procedure ImprimeOrcaPediSelecionado(CtrID: Integer);
    procedure ImprimeOrcaPediTodos(StatusMovimento: TStatusMovimento; Vias:
              Integer);
*)
    procedure ImprimeOrcaPedi(CtrID: Integer; StatusMovimento: TStatusMovimento;
              Vias: Integer; ImpDesco: Boolean);
  end;

var
  FmContratApp2: TFmContratApp2;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule, LocCCon, DmkDAC_PF,
  ModuleGeral, UnDmkProcFunc, UnGOTOy, UnXXe_PF;

{$R *.dfm}

{ TFmContratApp }

procedure TFmContratApp2.FormCreate(Sender: TObject);
begin
  FCtrID := 0;
  //FLocIPatPriCtrID := '';
  FTodos := True;
  //
  DModG.ReopenOpcoesGerl;
end;

procedure TFmContratApp2.frxLOC_PATRI_101_001_07GetValue(const VarName: string;
  var Value: Variant);
var
  CATipo, CETipo: Integer;
  //CCMes, CCAno: Integer;
  //CCData: String;
begin
  CATipo := QrEndContratadaTipo.Value;
  CETipo := QrEndContratanteTipo.Value;
  //
  if VarName = 'DAT_ASIN' then
    Value := Geral.FDT(QrLocCConCabDataEmi.Value, 6)
  else
  if VarName = 'HOR_ASIN' then
    Value := Geral.FDT(QrLocCConCabDtHrEmi.Value, 102)
  else
  if VarName = 'VARF_CAENDERECO' then
    Value := QrEndContratadaNOMELOGRAD.Value + ' ' +
     QrEndContratadaNOMERUA.Value + ', N� ' +
     Geral.FFT(QrEndContratadaNUMERO.Value, 0, siPositivo) + ' ' +
     QrEndContratadaCOMPL.Value
  else
  if VarName = 'VARF_CAIERG' then
  begin
    case CATipo of
    0:
      if QrEndContratadaIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := QrEndContratadaIE_RG.Value;
    1:
      if QrEndContratadaIE_RG.Value = '' then
        Value := ''
      else
        Value := QrEndContratadaIE_RG.Value;
    end;
  end
  else
  if VarName = 'VARF_CALACNPJCFP' then
  begin
    if CATipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end
  else
  if VarName = 'VARF_CALAIERG' then
  begin
    if CATipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end
  else
  //
  if VarName = 'VARF_CEENDERECO' then
    Value := QrEndContratanteNOMELOGRAD.Value + ' ' +
     QrEndContratanteNOMERUA.Value + ', N� ' +
     FormatFloat('0', QrEndContratanteNUMERO.Value) + ' ' +
     QrEndContratanteCOMPL.Value
  else
  if VarName = 'VARF_CEIERG' then
  begin
    case CETipo of
    0:
      if QrEndContratanteIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := QrEndContratanteIE_RG.Value;
    1:
      if QrEndContratanteIE_RG.Value = '' then
        Value := ''
      else
        Value := QrEndContratanteIE_RG.Value;
    end;
  end else
  if VarName = 'VARF_CELACNPJCFP' then
  begin
    if CETipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end else
  if VarName = 'VARF_CELAIERG' then
  begin
    if CETipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end else
  if VarName = 'VAR_SLOGANFOOT' then
    Value := DmodG.QrOpcoesGerl.FieldByName('SloganFoot').AsString
  else
  if VarName = 'VAR_ValorDia' then
  begin
    if QrLocIPatPriValorDia.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocIPatPriValorDia.Value, 2, siNegativo);
  end;
  if VarName = 'VAR_VAL_SEM_CALC' then
  begin
    if QrLocIPatPriValorSem.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocIPatPriVAL_SEM_CALC.Value, 2, siNegativo);
  end else
  if VarName = 'VAR_VAL_QUI_CALC' then
  begin
    if QrLocIPatPriValorQui.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocIPatPriVAL_QUI_CALC.Value, 2, siNegativo);
  end else
  if VarName = 'VAR_VAL_MES_CALC' then
  begin
    if QrLocIPatPriValorMes.Value = 0 then
      Value := ''
    else
      Value := Geral.FFT(QrLocIPatPriVAL_MES_CALC.Value, 2, siNegativo);
  end;
  if VarName = 'VAR_OBSERVACOES' then
  begin
    Value := '';
    if QrLocCConCabObs0.Value <> '' then
      Value := Value + QrLocCConCabObs0.Value + ' ';
    if QrLocCConCabObs1.Value <> '' then
      Value := Value + QrLocCConCabObs1.Value + ' ';
    if QrLocCConCabObs2.Value <> '' then
      Value := Value + QrLocCConCabObs2.Value + ' ';
    if QrLocCConCabHistImprime.Value <> '' then
      Value := Value + QrLocCConCabHistImprime.Value;
  end else
  if VarName = 'VARF_HOJE' then Value := FormatDateTime(VAR_FORMATDATE2, Date)
  //else if VarName = 'VARF_CHQS' then Value := QrLotIts.RecordCount
  else if VarName = 'VARF_MOEDA' then Value := Dmod.QrControleMoeda.Value
  else if VarName = 'VARF_TOTAL_EXTENSO' then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(QrLocCConCabValorEquipamentos.Value, 2, siPositivo))
  else if VarName = 'VARF_CIDADE_E_DATA' then
    Value := QrEndContratadaNO_CIDADE.Value
    + ', '+FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLocCConCabDtHrEmi.Value)
  else if VarName = 'VARF_ENDERECO_CLIENTE' then Value :=
    GOTOy.EnderecoDeEntidade(QrLocCConCabCliente.Value, 0)
  else if VarName = 'DATA_CON' then Value :=
    FormatDateTime(VAR_FORMATDATE2, QrLocCConCabDtHrEmi.Value)
  else if VarName = 'NOME_CLI' then Value := QrLocCConCabNO_CLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := DModG.QrDonoCIDADE.Value + ', ' +
    FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLocCConCabDtHrEmi.Value)
  else
  if VarName = 'VAR_PERIODO_EXTENSO' then
      Value := Dmod.QrOpcoesTRenLocPeriodoExtenso.Value
  else
  if VarName = 'Logo2_4_x_1_0Existe' then
  begin
    Value := FileExists(Dmod.QrOpcoesTRenLogo2_4_x_1_0.Value);
    //
    if VAR_Usuario = -1 then
      Geral.MB_Info('Caminho Logo2_4_x_1_0: ' + sLineBreak +
      Dmod.QrOpcoesTRenLogo2_4_x_1_0.Value);
  end else
  if VarName = 'Logo2_4_x_1_0Path' then
    Value := Dmod.QrOpcoesTRenLogo2_4_x_1_0.Value
  else
  if VarName = 'Logo3_4_x_3_4Existe' then
  begin
    Value := FileExists(Dmod.QrOpcoesTRenLogo3_4_x_3_4.Value);
    //
    if VAR_Usuario = -1 then
      Geral.MB_Info('Caminho Logo2_4_x_1_0: ' + sLineBreak +
      Dmod.QrOpcoesTRenLogo2_4_x_1_0.Value);
  end else
  if VarName = 'Logo3_4_x_3_4Path' then
    Value := Dmod.QrOpcoesTRenLogo3_4_x_3_4.Value
  else
  if VarName = 'VAR_SLOGANFOOT' then
    Value := DmodG.QrOpcoesGerl.FieldByName('SloganFoot').AsString
  else
  if VarName = 'VARF_SUM_LOC_E_SRV' then
    Value := QrSumLocValorLocacao.Value + QrSumRucValorLocacao.Value + QrSumSvcValorTotal.Value
  else
  if VarName = 'VARF_VALIDADE_ORCAMENTO_VENDA' then
  begin
    if  QrLocCConItsStatus.Value = Integer(TStatusMovimento.statmovOrcamento) then
    begin
      Value := 'Validade do or�amento: ' +
        Geral.FF0(Dmod.QrOpcoesTRenDdValidOrcaVen.Value) + ' dias.';
    end
    else if QrLocCConItside_nNF.Value > 0  then
    begin
      Value := XXe_PF.TipoDoc_ide_Mod(QrLocCConItside_mod.Value) + ' ' +
        Geral.FF0(QrLocCConItside_nNF.Value) + ' ' +
        XXe_PF.XXeEstahCancelada_TXT(QrLocCConItsNF_Stat.Value);
    end else
      Value := '';
  end else
  if VarName = 'VARF_NUMS_NFs' then
  begin
    // se precisar, ver o que fazer
    Value := '';
  end
  else
  if VarName = 'VARF_X_CTRIDS' then
  begin
    if (QrLocCConIts.State <> dsInactive) and (QrLocCConIts.RecordCount > 1) then
      Value := 'S'
    else
      Value := '';
  end;
  //
  //  ...
  //FmPrincipal.TextosCartasGetValue(VarName, Value)
end;

procedure TFmContratApp2.frxLOC_PATRI_101_002_10GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_SLOGANFOOT' then
    Value := DmodG.QrOpcoesGerl.FieldByName('SloganFoot').AsString
  else if VarName = 'VAR_DATAHORAENCER' then
  begin
    if QrLocCConCabDtHrBxa.Value < 2 then
      Value := '___ / ___ / ______ �s ___ : ___ horas'
    else
      Value := Geral.FDT(QrLocCConCabDtHrBxa.Value, 8);
  end else
  if VarName = 'VAR_QtdUso' then
  begin
    if (QrLocIPatCnsQtdIni.Value <> 0) and (QrLocIPatCnsQtdFim.Value <> 0) then
      Value := Geral.FFT(QrLocIPatCnsQtdUso.Value, 3, siPositivo)
    else
      Value := '';
  end else
  if VarName = 'VARF_TEXTO_DEVOL_EQUIP_ESTADO_LIMPEZA' then
    Value := Dmod.QrOpcoesTRenTxtDevolLimpeza.Value
  else
  if VarName = 'VARF_TEXTO_DEVOL_EQUIP_ESTADO_CONSERVA' then
    Value := Dmod.QrOpcoesTRenTxtDevolConserva.Value
  else
  if VarName = 'Logo2_4_x_1_0Existe' then
    Value := FileExists(Dmod.QrOpcoesTRenLogo2_4_x_1_0.Value)
  else
  if VarName = 'Logo2_4_x_1_0Path' then
    Value := Dmod.QrOpcoesTRenLogo2_4_x_1_0.Value
  else
  if VarName = 'Logo3_4_x_3_4Existe' then
    Value := FileExists(Dmod.QrOpcoesTRenLogo3_4_x_3_4.Value)
  else
  if VarName = 'Logo3_4_x_3_4Path' then
    Value := Dmod.QrOpcoesTRenLogo3_4_x_3_4.Value
  else
  if VarName = 'VARF_PATRIMONIO_ATUAL' then
    Value := FPatrimonioAtual_TXT
  else
  //...
end;

procedure TFmContratApp2.ImprimeContrato(Fonte: Integer);
var
  Relatorio: TfrxReport;
begin
  case Fonte of
    07: Relatorio := frxLOC_PATRI_101_001_07;
    10: Relatorio := frxLOC_PATRI_101_001_10;
    else
    begin
      Relatorio := frxLOC_PATRI_101_001_07;
      Geral.MB_Erro('Fonte n�o definida para o contrato! Ser� usado a fonte 7');
    end;
  end;
  //FStatusMovimento := TStatusMovimento.statmovIndefinido;
  FSQLStatusMovimentos := 'AND Status>' + Geral.FF0(Integer(TStatusMovimento.statmovOrcamento));
  FSQLAtrelamento      := 'AND lpp.Atrelamento=' + Geral.FF0(Integer(TServicoAtrelado.svcatrlLocacao));
  FSQLMovimentoIDTab   := 'AND IDTab < ' + Geral.FF0(Integer(TTabConLocIts.tcliVenda));
  //
  ReopenLocCConCab();
  ReopenEndContratante();
  ReopenEndContratada();
  ReopenContrato();
  ReopenSumLoc();
  //
  MyObjects.frxDefineDatasets(Relatorio, [
  //DModG.frxDsDono,
  frxDsLocCConCab,
  frxDsContratos,
  frxDsEndContratada,
  frxDsEndContratante,
  frxDsLocCConIts,
  frxDsLocIPatPri,
  frxDsLocIPatSec,
  frxDsLocIPatAce,
  frxDsLocIPatApo,
  frxDsLocIPatCns,
  frxDsLocIPatUso,
  frxDsLocCItsSvc,
  frxDsLocCItsVen,
  frxDsSumLoc
  ]);
  //
  MyObjects.frxMostra(Relatorio, 'Contrato de Loca��o');
end;

procedure TFmContratApp2.ImprimeDevolucao((*LocIPatPriCtrID: String*)Todos: Boolean; Fonte: Integer);
var
  Report: TfrxReport;
begin
(*
  //FStatusMovimento := TStatusMovimento.statmovIndefinido;
  FSQLStatusMovimentos := '';
  FSQLAtrelamento := ''; // N�o usa?
  FSQLMovimentoIDTab := '';
*)
  FSQLStatusMovimentos := 'AND Status>' + Geral.FF0(Integer(TStatusMovimento.statmovOrcamento));
  FSQLAtrelamento      := 'AND lpp.Atrelamento=' + Geral.FF0(Integer(TServicoAtrelado.svcatrlLocacao));
  FSQLMovimentoIDTab   := 'AND IDTab < ' + Geral.FF0(Integer(TTabConLocIts.tcliVenda));
  //
  //

  //QtdeDevolucao - QtdeLocacao ???

  //FLocIPatPriCtrID := LocIPatPriCtrID;
  FTodos := Todos;
  //
  //
  ReopenLocCConCab();
  ReopenEndContratante();
  ReopenEndContratada();
  ReopenContrato();
  //
  if Fonte = 10 then
    Report := frxLOC_PATRI_101_002_10
  else
    Report := frxLOC_PATRI_101_002_07;
  //
  MyObjects.frxDefineDatasets(Report, [
  //DModG.frxDsDono,
  frxDsLocCConCab,
  frxDsContratos,
  frxDsEndContratada,
  frxDsEndContratante,
  frxDsLocCConIts,
  frxDsLocIPatPri,
  frxDsLocIPatSec,
  frxDsLocIPatAce,
  frxDsLocIPatApo,
  frxDsLocIPatCns,
  frxDsLocIPatUso
  ]);
  //
  MyObjects.frxMostra(Report, 'Devolu��o de loca��o');
end;

procedure TFmContratApp2.ImprimeFichaDeTroca();
begin
  ReopenLocCConCab();
  ReopenEndContratada();
  //
  MyObjects.frxDefineDatasets(frxLOC_PATRI_101_008, [
  //DModG.frxDsDono,
  frxDsLocCConCab,
  frxDsEndContratada
  ]);
  //
  MyObjects.frxMostra(frxLOC_PATRI_101_008, 'Ficha de Troca');
end;

{
procedure TFmContratApp2.ImprimeOrcaPediTodos(StatusMovimento: TStatusMovimento;
  Vias: Integer);
var
  MDVias: TfrxMasterData;
begin
  //FStatusMovimento := StatusMovimento;
  if Integer(StatusMovimento) > 0 then
  begin
    FSQLStatusMovimentos := 'AND Status = ' + Geral.FF0(Integer(StatusMovimento));
    FSQLAtrelamento      := 'AND lpp.Atrelamento=' + Geral.FF0(Integer(TServicoAtrelado.svcatrlVenda));
    FSQLMovimentoIDTab   := 'AND IDTab = ' + Geral.FF0(Integer(TTabConLocIts.tcliVenda));
  end;
  //
  ReopenLocCConCab();
  ReopenEndContratante();
  ReopenEndContratada();
  ReopenLocFCab(TTabConLocIts.tcliVenda);
  //
  MDVias := frxLOC_PATRI_101_004.FindObject('MDVias') as TfrxMasterData;
  MDVias.RowCount := Vias;
  MyObjects.frxDefineDatasets(frxLOC_PATRI_101_004, [
  //DModG.frxDsDono,
  frxDsLocCConCab,
  frxDsEndContratada,
  frxDsEndContratante,
  frxDsLocCConIts,
  frxDsLocCItsVen,
  frxDsLocFCab,
  frxDsLctFatRef
  ]);
  //
  MyObjects.frxMostra(frxLOC_PATRI_101_004, QrLocCConItsNO_STATUS.Value);
end;
}

procedure TFmContratApp2.ImprimeOrcamentoLocacao();
begin
  //FStatusMovimento := TStatusMovimento.statmovIndefinido;
  FSQLStatusMovimentos := 'AND Status=' + Geral.FF0(Integer(TStatusMovimento.statmovOrcamento));
  FSQLAtrelamento      := 'AND lpp.Atrelamento=' + Geral.FF0(Integer(TServicoAtrelado.svcatrlLocacao));
  FSQLMovimentoIDTab   := 'AND IDTab < ' + Geral.FF0(Integer(TTabConLocIts.tcliVenda));
  //
  ReopenLocCConCab();
  ReopenEndContratante();
  ReopenEndContratada();
  ReopenContrato();
  ReopenSumLoc();
  //
  MyObjects.frxDefineDatasets(frxLOC_PATRI_101_005, [
  //DModG.frxDsDono,
  frxDsLocCConCab,
  frxDsContratos,
  frxDsEndContratada,
  frxDsEndContratante,
  frxDsLocCConIts,
  frxDsLocIPatPri,
  frxDsLocIPatSec,
  frxDsLocIPatAce,
  frxDsLocIPatApo,
  frxDsLocIPatCns,
  frxDsLocIPatUso,
  frxDsLocCItsSvc,
  frxDsLocCItsVen,
  frxDsSumLoc

  ]);
  //
  MyObjects.frxMostra(frxLOC_PATRI_101_005, 'Or�amento de Loca��o');
end;

procedure TFmContratApp2.ImprimeOrcaPedi(CtrID: Integer;
  StatusMovimento: TStatusMovimento; Vias: Integer; ImpDesco: Boolean);
var
  MDVias: TfrxMasterData;
  frxReport: TfrxReport;
begin
  FCtrID := CtrID;
  //FStatusMovimento := StatusMovimento;
  if Integer(StatusMovimento) > 0 then
  begin
    FSQLStatusMovimentos := 'AND Status = ' + Geral.FF0(Integer(StatusMovimento));
    FSQLAtrelamento      := 'AND lpp.Atrelamento=' + Geral.FF0(Integer(TServicoAtrelado.svcatrlVenda));
    FSQLMovimentoIDTab   := 'AND IDTab = ' + Geral.FF0(Integer(TTabConLocIts.tcliVenda));
  end;
  //
  ReopenLocCConCab();
  ReopenEndContratante();
  ReopenEndContratada();
  if Integer(StatusMovimento) > Integer(TStatusMovimento.statmovOrcamento) then
    ReopenLocFCab(TTabConLocIts.tcliVenda);
  //
  if CtrID <> 0 then
    ReopenLocCConIts(CtrID);
  //
  case StatusMovimento of
    TStatusMovimento.statmovOrcamento:
    begin
      if ImpDesco then
        frxReport := frxLOC_PATRI_101_006
      else
        frxReport := frxLOC_PATRI_101_007;
      //
      MDVias := frxReport.FindObject('MDVias') as TfrxMasterData;
      MDVias.RowCount := Vias;
      MyObjects.frxDefineDatasets(frxReport, [
      //DModG.frxDsDono,
      frxDsLocCConCab,
      frxDsEndContratada,
      frxDsEndContratante,
      frxDsLocCConIts,
      frxDsLocCItsVen
      //frxDsLocFCab,
      //frxDsLctFatRef
      ]);
    end
    else
    begin
      if ImpDesco then
        frxReport := frxLOC_PATRI_101_003
      else
        frxReport := frxLOC_PATRI_101_004;
    end;
      //
      MDVias := frxReport.FindObject('MDVias') as TfrxMasterData;
      MDVias.RowCount := Vias;
      MyObjects.frxDefineDatasets(frxReport, [
      //DModG.frxDsDono,
      frxDsLocCConCab,
      frxDsEndContratada,
      frxDsEndContratante,
      frxDsLocCConIts,
      frxDsLocCItsVen,
      frxDsLocFCab,
      frxDsLctFatRef
      ]);
  end;
  //
  //
  MyObjects.frxMostra(frxReport, QrLocCConItsNO_STATUS.Value);
end;

procedure TFmContratApp2.QrEndContratadaCalcFields(DataSet: TDataSet);
begin
  QrEndContratadaTEL_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaTEL.Value);
  QrEndContratadaCEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratadaCEL.Value);
  QrEndContratadaFAX_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaFAX.Value);
  QrEndContratadaCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratadaCNPJ_CPF.Value);
  QrEndContratadaCEP_TXT.Value := Geral.FormataCEP_NT(QrEndContratadaCEP.Value);
end;

procedure TFmContratApp2.QrEndContratanteCalcFields(DataSet: TDataSet);
begin
  QrEndContratanteTEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteTEL.Value);
  QrEndContratanteCEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteCEL.Value);
  QrEndContratanteFAX_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteFAX.Value);
  QrEndContratanteCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratanteCNPJ_CPF.Value);
  QrEndContratanteCEP_TXT.Value := Geral.FormataCEP_NT(QrEndContratanteCEP.Value);
end;

procedure TFmContratApp2.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.value := QrLctFatRef.RecNo;
end;

procedure TFmContratApp2.QrLocCConCabAfterOpen(DataSet: TDataSet);
begin
  // Fazer aqui em vez de fazer no AfterScroll para diminuir o tempo de
  // aberturas de tabelas e de impress�o do contrato!
  if FCtrID <> 0 then
    Exit;
  ReopenLocCConIts(0); //, FStatusMovimento);
end;

procedure TFmContratApp2.QrLocCConCabBeforeClose(DataSet: TDataSet);
begin
  QrLocCConIts.Close;
end;

procedure TFmContratApp2.QrLocCConCabCalcFields(DataSet: TDataSet);
begin
  QrLocCConCabDataEmi.Value := Trunc(QrLocCConCabDtHrEmi.Value);
  QrLocCConCabHoraEmi.Value := QrLocCConCabDtHrEmi.Value - QrLocCConCabDataEmi.Value;
  QrLocCConCabDtHrBxa_TXT.Value :=  Geral.FDT(QrLocCConCabDtHrBxa.Value, 107);
  QrLocCConCabLOCALFONE_TXT.Value :=  Geral.FormataTelefone_TT(
    Geral.SoNumero_TT(QrLocCConCabLocalFone.Value));
end;

procedure TFmContratApp2.QrLocCConItsAfterScroll(DataSet: TDataSet);
const
  sProcName = 'TFmContratApp2.QrLocCConItsAfterScroll()';
begin
{
  QrLocIPatPri.Close;
  QrLocIPatSec.Close;
  QrLocIPatAce.Close;
  QrLocIPatUso.Close;
  QrLocIPatCns.Close;
  QrLocIPatApo.Close;
  QrLocCItsSvc.Close;
  QrLocCItsVen.Close;
  case TTabConLocIts(QrLocCConItsIDTab.Value) of
    tcliLocacao:
    begin
      ReopenLocIPatPri();
      ReopenLocIPatSec();
      ReopenLocIPatAce();
      ReopenLocIPatCns();
      ReopenLocIPatUso();
      ReopenLocIPatApo();
    end;
    tcliOutroServico:
    begin
(*&�%
      ReopenLocCItsSvc(QrLocCItsSvc, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
*)
    end;
    tcliVenda:
    begin
(*&�%
      ReopenLocCItsVen(QrLocCItsVen, QrLocCConCabCodigo.Value, QrLocCConItsCtrID.Value, 0);
*)
    end
    else
      Geral.MB_Erro('"IDTab" n�o implementado em ' + sProcName);
  end;
}
  ReopenLocIPatPri();
  ReopenLocIPatSec();
  ReopenLocIPatAce();
  ReopenLocIPatCns();
  ReopenLocIPatUso();
  ReopenLocIPatApo();
  ReopenLocCItsSvc();
  ReopenLocCItsVen();
end;

procedure TFmContratApp2.QrLocCConItsBeforeClose(DataSet: TDataSet);
begin
  QrLocIPatPri.Close;
  QrLocIPatSec.Close;
  QrLocIPatAce.Close;
  QrLocIPatUso.Close;
  QrLocIPatCns.Close;
  QrLocIPatApo.Close;
  QrLocCItsSvc.Close;
  QrLocCItsVen.Close;
end;

procedure TFmContratApp2.QrLocFCabAfterScroll(DataSet: TDataSet);
begin
  ReopenLctFatRef();
end;

procedure TFmContratApp2.QrLocFCabBeforeClose(DataSet: TDataSet);
begin
  QrLctFatRef.Close;
end;

procedure TFmContratApp2.QrLocIPatCnsCalcFields(DataSet: TDataSet);
begin
  QrLocIPatCnsQTDUSO.Value :=
    QrLocIPatCnsQtdFim.Value - QrLocIPatCnsQtdIni.Value;
end;

procedure TFmContratApp2.QrLocIPatPriCalcFields(DataSet: TDataSet);
begin
  QrLocIPatPriTXT_DTA_DEVOL.Value := Geral.FDT(QrLocIPatPriDtHrRetorn.Value, 107);
  QrLocIPatPriTXT_DTA_LIBER.Value := Geral.FDT(QrLocIPatPriLibDtHr.Value, 107);
  QrLocIPatPriTXT_QEM_LIBER.Value := Trim(QrLocIPatPriLOGIN.Value + ' ' +
    QrLocIPatPriRELIB.Value + ' ' +QrLocIPatPriHOLIB.Value);

  //
  if QrLocIPatPriValorSem.Value >= 0.01 then
    QrLocIPatPriVAL_SEM_CALC.Value := QrLocIPatPriValorSem.Value
  else
    QrLocIPatPriVAL_SEM_CALC.Value := QrLocIPatPriValorDia.Value * 7;
  //
  if QrLocIPatPriValorQui.Value >= 0.01 then
    QrLocIPatPriVAL_QUI_CALC.Value := QrLocIPatPriValorQui.Value
  else
    QrLocIPatPriVAL_QUI_CALC.Value := QrLocIPatPriValorSem.Value / 7 * 15;
  //
  if QrLocIPatPriValorMes.Value >= 0.01 then
    QrLocIPatPriVAL_MES_CALC.Value := QrLocIPatPriValorMes.Value
  else
    QrLocIPatPriVAL_MES_CALC.Value := QrLocIPatPriValorQui.Value * 2;
end;

procedure TFmContratApp2.ReopenContrato();
begin
  QrContrato.Close;
  QrContrato.Params[0].AsInteger := QrLocCConCabContrato.Value;
  UMyMod.AbreQuery(QrContrato, Dmod.MyDB);
end;

procedure TFmContratApp2.ReopenEndContratada();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratada, Dmod.MyDB, [
  'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tipo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATADA,',
  'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,',
  'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,',
  '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/',
  'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,',
  'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,',
  'IF(ent.Tipo=0, ent.ECel, ent.Pcel) CEL, ',
  'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,',
  'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP, ',
  'IF(ent.Tipo=0, ent.EEmail, ent.PEmail) Email ',
  'FROM entidades ent',
  'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF',
  'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF',
  'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd',
  'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
  'WHERE ent.Codigo=' + Geral.FF0(QrLocCConCabEmpresa.Value),
  'ORDER BY CONTRATADA',
  '']);
end;

procedure TFmContratApp2.ReopenEndContratante();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratante, Dmod.MyDB, [
  'SELECT mun.Nome NO_CIDADE, pai.Nome NO_PAIS, ent.Codigo, ent.Tipo,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,',
  'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,',
  'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,',
  'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,',
  '/*IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,*/',
  'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,',
  'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,',
  'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,',
  'IF(ent.Tipo=0, ent.ECel, ent.Pcel) CEL, ',
  'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX,',
  'IF(ent.Tipo=0, ent.ECep, ent.PCEP) + 0.000 CEP, ',
  'IF(ent.Tipo=0, ent.EEmail, ent.PEmail) Email ',
  'FROM entidades ent',
  'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF',
  'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF',
  'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd',
  'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
  'WHERE ent.Codigo=' + Geral.FF0(QrLocCConCabCliente.Value),
  'ORDER BY CONTRATANTE',
  '']);
end;

procedure TFmContratApp2.ReopenLctFatRef();
var
  FatorValr(*, FatorDesco*): Double;
  sFatorValr(*, sFatorDesco*): String;
begin
  sFatorValr  := '1.000000000000';
  //sFatorDesco := '1.000000000000';
  if QrLocFCabValTotal.Value <> 0 then
  begin
    //FatorValr := QrLocFCabValCalc.Value / QrLocFCabValTotal.Value;
    sFatorValr  := Geral.FFT_Dot(QrLocFCabPercCalc.Value, 12, siNegativo);
  end;
  //
(*
  if QrLocFCabPercDesco.Value < 1 then
  begin
    //sFatorDesco := QrLocFCabValCalc.Value / QrLocFCabValTotal.Value;
    sFatorDesco := Geral.FFT_Dot(QrLocFCabPercDesco.Value, 12, siNegativo);
  end;
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT (lfr.Valor * ' + sFatorValr + ') ValCalc, lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(QrLocFCabControle.Value),
  'AND FatID IN(0, '+ Geral.FF0(VAR_FATID_3001) + ') ',
  '']);
end;

procedure TFmContratApp2.ReopenLocCConCab();
begin
  QrLocCConCab.Close;
  QrLocCConCab.Params[0].AsInteger := FLocCConCab;
  UMyMod.AbreQuery(QrLocCConCab, Dmod.MyDB);
end;

procedure TFmContratApp2.ReopenLocCConIts(CtrID: Integer); //; StatusMovimento: TStatusMovimento);
var
  //SQL_StatusMovimento,
  SQL_CtrID: String;
begin
  if FCtrID <> 0 then
    SQL_CtrID := 'AND its.CtrID=' + Geral.FF0(CtrID)
  else
    SQL_CtrID := '';
  //
(*
  if StatusMovimento <> TStatusMovimento.statmovIndefinido then
    SQL_StatusMovimento := 'AND Status=' + Geral.FF0(Integer(StatusMovimento))
  else
    SQL_StatusMovimento := '';
*)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCConIts, Dmod.MyDB, [
  'SELECT ELT(its.IDTab+1, "Indefinido",  ',
  '"Loca��o", "Servi�o", "Venda", "? ? ?") NO_IDTab,  ',
  'ELT(its.Status+1,  "Indefinido", ',
  '"Or�amento", "Pedido", "Fatura",   ',
  '"? ? ?") NO_Status,  ',
  'CASE its.Status  ',
  '  WHEN 1 THEN its.ValorOrca  ',
  '  WHEN 2 THEN its.ValorPedi  ',
  '  WHEN 3 THEN its.ValorFatu ',
  'ELSE 0 END ValorStat, ',
  'ppc.Nome NO_CondiOrca, its.*   ',
  'FROM loccconits its  ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=its.CondiOrca',
  'WHERE its.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  //SQL_StatusMovimento,
  FSQLStatusMovimentos,
  FSQLMovimentoIDTab,
  SQL_CtrID,
  ' ']);

  //Geral.MB_Teste(QrLocCConIts.SQL.Text);
end;

procedure TFmContratApp2.ReopenLocCItsSvc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCItsSvc, Dmod.MyDB, [
  'SELECT lpp.*, gg1.Nome NO_GGX, gg1.REFERENCIA',
  'FROM loccitssvc lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lpp.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpp.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaServico)), // Servi�o
  FSQLAtrelamento,
  '']);
end;

procedure TFmContratApp2.ReopenLocCItsVen;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCItsVen, Dmod.MyDB, [
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'ggx.GraGru1, med.SIGLA ',
  'FROM loccitsven lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpc.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpc.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaVenda)), // Venda
  '']);
end;

procedure TFmContratApp2.ReopenLocFCab(IDTab: TTabConLocIts);
// = (tcliIndefinido=0, tcliLocacao=1, tcliOutroServico=2, tcliVenda=3);
var
  SQL_AND, SQL_VAL: String;
begin
  SQL_AND := '';
  case IDTab of
    tcliIndefinido:   SQL_AND := '';
    tcliLocacao:      SQL_AND := 'AND (ValLocad + ValConsu + ValUsado) > 0';
    tcliOutroServico: SQL_AND := 'AND ValFrete > 0';
    tcliVenda:        SQL_AND := 'AND (ValVenda + ValVeMaq)> 0';
  end;
  SQL_VAL := '';
  case IDTab of
    tcliIndefinido:   SQL_VAL := ' ValTotal ';
    tcliLocacao:      SQL_VAL := ' (ValLocad + ValConsu + ValUsado) ';
    tcliOutroServico: SQL_VAL := ' ValFrete ';
    tcliVenda:        SQL_VAL := ' (ValVenda + ValVeMaq) ';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocFCab, Dmod.MyDB, [
  'SELECT ppc.Nome NO_CondicaoPG, ' + SQL_VAL + ' ValCalc, ',
  'IF(ValTotal + ValDesco = 0, 0, ValDesco / (ValTotal + ValDesco)) PercDesco, ',
  'IF(ValTotal + ValDesco = 0, 0, ' + SQL_VAL + ' / (ValTotal +  + ValDesco)) PercCalc, ',
  'lfc.* ',
  'FROM locfcab lfc ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=lfc.CondicaoPG ',
  'WHERE lfc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  SQL_AND,
  '']);
  //Geral.MB_Teste(QrLocFCab.SQL.Text);
end;

procedure TFmContratApp2.ReopenLocIPatAce();
var
  SQL: String;
begin
  if FTodos = False then
    SQL := 'AND lpp.QtdeDevolucao < (lpp.QtdeLocacao * lpp.QtdeProduto)'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatAce, Dmod.MyDB, [
  'SELECT lpp.*, (QtdeProduto * QtdeLocacao) + 0.000 QTDE, ',
  'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccitslca lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpp.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaAcessorio)), // Acessorio
  SQL,
  '']);
(*
  'SELECT lpa.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
  'FROM loccpatace lpa ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpa.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lpa.CtrID=' + Geral.FF0(QrLocIPatPriCtrID.Value),
  '']);
*)
end;

procedure TFmContratApp2.ReopenLocIPatApo();
var
  SQL: String;
begin
  if FTodos = False then
    // Nada
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatApo, Dmod.MyDB, [
    'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
    'med.SIGLA ',
    'FROM loccitsruc lpc ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
    'WHERE lpc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
    'AND lpc.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
    'AND lpc.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaApoio)), // Apoio
    '']);
  end;
end;

procedure TFmContratApp2.ReopenLocIPatCns();
var
  SQL: String;
begin
  if FTodos = False then
    SQL := 'AND ((lpc.QtdIni > lpc.QtdFim) OR (lpc.AvalIni > lpc.AvalFim))'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatCns, Dmod.MyDB, [
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'med.SIGLA ',
  'FROM loccitsruc lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpc.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpc.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaConsumo)), // Consumo
  SQL,
  '']);
(*
  'SELECT lpc.*, gg1.Nome NO_GGX, gg1.REFERENCIA,',
  'med.SIGLA ',
  'FROM loccpatcns lpc ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpc.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed med ON med.Codigo=lpc.Unidade',
  'WHERE lpc.CtrID=' + Geral.FF0(QrLocIPatPriCtrID.Value),
  '']);
*)
end;

procedure TFmContratApp2.ReopenLocIPatPri();
(*
var
  SQL: String;
begin
  if FLocIPatPriCtrID <> '' then
    SQL := 'AND lpp.CtrID IN (' + FLocIPatPriCtrID + ')'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatPri, Dmod.MyDB, [
  'SELECT ggp.AtualValr, lpp.*, ',
  'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM LocIPatPri lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragxpatr ggp ON ggx.Controle=ggp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  SQL,
  '']);
*)
var
  SQL: String;
begin
  if FTodos = False then
    SQL := 'AND lpp.QtdeDevolucao < (lpp.QtdeLocacao * lpp.QtdeProduto)'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatPri, Dmod.MyDB, [
  'SELECT gg1.Patrimonio, lpp.*, gg1.Nome NO_GGX, ',
  'gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccitslca lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragxpatr pat ON ggx.Controle=pat.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpp.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaPrincipal)), // Principal
  SQL,
  '']);
end;

procedure TFmContratApp2.ReopenLocIPatSec();
var
  SQL: String;
begin
  if FTodos = False then
    SQL := 'AND lpp.QtdeDevolucao < (lpp.QtdeLocacao * lpp.QtdeProduto)'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatSec, Dmod.MyDB, [
  'SELECT lpp.*, (QtdeProduto * QtdeLocacao) + 0.000 QTDE, ',
  'gg1.Nome NO_GGX, gg1.REFERENCIA, sen.LOGIN ',
  'FROM loccitslca lpp ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN senhas sen ON sen.Numero=lpp.LibFunci ',
  'WHERE lpp.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpp.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpp.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaSecundario)), // Secundario
  SQL,
  '']);
end;

(*
procedure TFmContratApp2.ReopenLocCPatSec();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocCPatSec, Dmod.MyDB, [
  'SELECT ggp.AtualValr, lps.*, gg1.Nome NO_GGX, gg1.REFERENCIA ',
  'FROM loccpatsec lps ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lps.GraGruX ',
  'LEFT JOIN gragxpatr ggp ON ggx.Controle=ggp.GraGruX ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE lps.CtrID=' + Geral.FF0(QrLocIPatPriCtrID.Value),
  'AND lps.GraGruX <> 0 ',
  '']);
end;
*)

procedure TFmContratApp2.ReopenLocIPatUso();
var
  SQL: String;
begin
  if FTodos = False then
    SQL := 'AND ((lpu.QtdIni > lpu.QtdFim) OR (lpu.AvalIni > lpu.AvalFim))'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocIPatUso, Dmod.MyDB, [
  'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA, ',
  'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM  ',
  'FROM loccitsruc lpu  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade ',
  'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni ',
  'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim ',
  'WHERE lpu.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lpu.CtrID=' + Geral.FF0(QrLocCConItsCtrID.Value),
  'AND lpu.ManejoLca=' + Geral.FF0(Integer(TManejoLca.manelcaUso)), // Uso
  SQL,
  '']);
(*
  'SELECT lpu.*, gg1.Nome NO_GGX, gg1.REFERENCIA, ',
  'med.SIGLA, avi.Nome NO_AVALINI, avf.Nome NO_AVALFIM  ',
  'FROM loccpatuso lpu  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=lpu.GraGruX  ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed med ON med.Codigo=lpu.Unidade ',
  'LEFT JOIN graglaval avi ON avi.Codigo=lpu.AvalIni ',
  'LEFT JOIN graglaval avf ON avf.Codigo=lpu.AvalFim ',
  'WHERE lpu.CtrID=' + Geral.FF0(QrLocIPatPriCtrID.Value),
  '']);
*)
end;

procedure TFmContratApp2.ReopenSumLoc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumLoc, Dmod.MyDB, [
  'SELECT SUM(lca.ValorProduto * lca.QtdeProduto) ValorIndeniz, ',
  'SUM(lca.ValorLocAAdiantar) ValorLocacao ',
  'FROM loccitslca lca ',
  'LEFT JOIN loccconits its ON its.CtrID=lca.CtrID ',
  'WHERE lca.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  FSQLStatusMovimentos,
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumRuc, Dmod.MyDB, [
  'SELECT SUM(lca.ValorProduto * lca.QtdeProduto) ValorIndeniz, ',
  'SUM(lca.ValorRucAAdiantar) ValorLocacao ',
  'FROM loccitsruc lca ',
  'LEFT JOIN loccconits its ON its.CtrID=lca.CtrID ',
  'WHERE lca.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  FSQLStatusMovimentos,
  '']);
  //Geral.MB_Teste(QrSumRuc.SQL.Text);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumSvc, Dmod.MyDB, [
  'SELECT SUM(lca.ValorTotal) ValorTotal',
  'FROM loccitssvc lca ',
  'LEFT JOIN loccconits its ON its.CtrID=lca.CtrID ',
  'WHERE lca.Codigo=' + Geral.FF0(QrLocCConCabCodigo.Value),
  'AND lca.Atrelamento=' + Geral.FF0(Integer(TServicoAtrelado.svcatrlLocacao)),  // 2
  FSQLStatusMovimentos,
  '']);
end;

{Valor Loca��o
R$ [frxDsSumLoc."ValorLocacao"]
}

end.
