object Dmod: TDmod
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 614
  Width = 814
  PixelsPerInch = 96
  object QrMaster: TMySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM Entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrAux: TMySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrControle: TMySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM Controle')
    Left = 12
    Top = 287
    object QrControleContasU: TIntegerField
      FieldName = 'ContasU'
      Origin = 'controle.ContasU'
    end
    object QrControleEntDefAtr: TIntegerField
      FieldName = 'EntDefAtr'
      Origin = 'controle.EntDefAtr'
    end
    object QrControleEntAtrCad: TIntegerField
      FieldName = 'EntAtrCad'
      Origin = 'controle.EntAtrCad'
    end
    object QrControleEntAtrIts: TIntegerField
      FieldName = 'EntAtrIts'
      Origin = 'controle.EntAtrIts'
    end
    object QrControleBalTopoNom: TIntegerField
      FieldName = 'BalTopoNom'
      Origin = 'controle.BalTopoNom'
    end
    object QrControleBalTopoTit: TIntegerField
      FieldName = 'BalTopoTit'
      Origin = 'controle.BalTopoTit'
    end
    object QrControleBalTopoPer: TIntegerField
      FieldName = 'BalTopoPer'
      Origin = 'controle.BalTopoPer'
    end
    object QrControleCasasProd: TSmallintField
      FieldName = 'CasasProd'
      Origin = 'controle.CasasProd'
    end
    object QrControleNCMs: TIntegerField
      FieldName = 'NCMs'
      Origin = 'controle.NCMs'
    end
    object QrControleParamsNFs: TIntegerField
      FieldName = 'ParamsNFs'
      Origin = 'controle.ParamsNFs'
    end
    object QrControleCambioCot: TIntegerField
      FieldName = 'CambioCot'
      Origin = 'controle.CambioCot'
    end
    object QrControleCambioMda: TIntegerField
      FieldName = 'CambioMda'
      Origin = 'controle.CambioMda'
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
      Origin = 'controle.MoedaBr'
    end
    object QrControleSecuritStr: TWideStringField
      FieldName = 'SecuritStr'
      Origin = 'controle.SecuritStr'
      Size = 32
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Origin = 'controle.LogoBig1'
      Size = 255
    end
    object QrControleEquiCom: TIntegerField
      FieldName = 'EquiCom'
      Origin = 'controle.EquiCom'
    end
    object QrControleContasLnk: TIntegerField
      FieldName = 'ContasLnk'
      Origin = 'controle.ContasLnk'
    end
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
      Origin = 'controle.LastBco'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
      Origin = 'controle.MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
      Origin = 'controle.MyPerMulta'
    end
    object QrControleEquiGru: TIntegerField
      FieldName = 'EquiGru'
      Origin = 'controle.EquiGru'
    end
    object QrControleCNAB_Rem: TIntegerField
      FieldName = 'CNAB_Rem'
      Origin = 'controle.CNAB_Rem'
    end
    object QrControleCNAB_Rem_I: TIntegerField
      FieldName = 'CNAB_Rem_I'
      Origin = 'controle.CNAB_Rem_I'
    end
    object QrControlePreEmMsgIm: TIntegerField
      FieldName = 'PreEmMsgIm'
      Origin = 'controle.PreEmMsgIm'
    end
    object QrControlePreEmMsg: TIntegerField
      FieldName = 'PreEmMsg'
      Origin = 'controle.PreEmMsg'
    end
    object QrControlePreEmail: TIntegerField
      FieldName = 'PreEmail'
      Origin = 'controle.PreEmail'
    end
    object QrControleContasMes: TIntegerField
      FieldName = 'ContasMes'
      Origin = 'controle.ContasMes'
    end
    object QrControleMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
      Origin = 'controle.MultiPgto'
    end
    object QrControleImpObs: TIntegerField
      FieldName = 'ImpObs'
      Origin = 'controle.ImpObs'
    end
    object QrControleProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
      Origin = 'controle.ProLaINSSr'
    end
    object QrControleProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
      Origin = 'controle.ProLaINSSp'
    end
    object QrControleVerSalTabs: TIntegerField
      FieldName = 'VerSalTabs'
      Origin = 'controle.VerSalTabs'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
      Origin = 'controle.VerBcoTabs'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
      Origin = 'controle.CNABCtaTar'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
      Origin = 'controle.CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
      Origin = 'controle.CNABCtaMul'
    end
    object QrControleCNAB_CaD: TIntegerField
      FieldName = 'CNAB_CaD'
      Origin = 'controle.CNAB_CaD'
    end
    object QrControleCNAB_CaG: TIntegerField
      FieldName = 'CNAB_CaG'
      Origin = 'controle.CNAB_CaG'
    end
    object QrControleAtzCritic: TIntegerField
      FieldName = 'AtzCritic'
      Origin = 'controle.AtzCritic'
    end
    object QrControleContasTrf: TIntegerField
      FieldName = 'ContasTrf'
      Origin = 'controle.ContasTrf'
    end
    object QrControleSomaIts: TIntegerField
      FieldName = 'SomaIts'
      Origin = 'controle.SomaIts'
    end
    object QrControleContasAgr: TIntegerField
      FieldName = 'ContasAgr'
      Origin = 'controle.ContasAgr'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
      Origin = 'controle.VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
      Origin = 'controle.VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
      Origin = 'controle.VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
      Origin = 'controle.VendaDiasPg'
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Origin = 'controle.LogoNF'
      Size = 255
    end
    object QrControleConfJanela: TIntegerField
      FieldName = 'ConfJanela'
      Origin = 'controle.ConfJanela'
    end
    object QrControleDataPesqAuto: TSmallintField
      FieldName = 'DataPesqAuto'
      Origin = 'controle.DataPesqAuto'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Origin = 'controle.MeuLogoPath'
      Size = 255
    end
    object QrControleAtividad: TSmallintField
      FieldName = 'Atividad'
      Origin = 'controle.Atividad'
    end
    object QrControleCidades: TSmallintField
      FieldName = 'Cidades'
      Origin = 'controle.Cidades'
    end
    object QrControleChequez: TSmallintField
      FieldName = 'Chequez'
      Origin = 'controle.Chequez'
    end
    object QrControlePaises: TSmallintField
      FieldName = 'Paises'
      Origin = 'controle.Paises'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
      Origin = 'controle.MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
      Origin = 'controle.MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
      Origin = 'controle.MyPgPeri'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
      Origin = 'controle.MyPgDias'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
      Origin = 'controle.CorRecibo'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
      Origin = 'controle.IdleMinutos'
    end
    object QrControleCambiosData: TDateField
      FieldName = 'CambiosData'
      Origin = 'controle.CambiosData'
    end
    object QrControleCambiosUsuario: TIntegerField
      FieldName = 'CambiosUsuario'
      Origin = 'controle.CambiosUsuario'
    end
    object QrControlereg10: TSmallintField
      FieldName = 'reg10'
      Origin = 'controle.reg10'
    end
    object QrControlereg11: TSmallintField
      FieldName = 'reg11'
      Origin = 'controle.reg11'
    end
    object QrControlereg50: TSmallintField
      FieldName = 'reg50'
      Origin = 'controle.reg50'
    end
    object QrControlereg51: TSmallintField
      FieldName = 'reg51'
      Origin = 'controle.reg51'
    end
    object QrControlereg53: TSmallintField
      FieldName = 'reg53'
      Origin = 'controle.reg53'
    end
    object QrControlereg54: TSmallintField
      FieldName = 'reg54'
      Origin = 'controle.reg54'
    end
    object QrControlereg56: TSmallintField
      FieldName = 'reg56'
      Origin = 'controle.reg56'
    end
    object QrControlereg60: TSmallintField
      FieldName = 'reg60'
      Origin = 'controle.reg60'
    end
    object QrControlereg75: TSmallintField
      FieldName = 'reg75'
      Origin = 'controle.reg75'
    end
    object QrControlereg88: TSmallintField
      FieldName = 'reg88'
      Origin = 'controle.reg88'
    end
    object QrControlereg90: TSmallintField
      FieldName = 'reg90'
      Origin = 'controle.reg90'
    end
    object QrControleNumSerieNF: TSmallintField
      FieldName = 'NumSerieNF'
      Origin = 'controle.NumSerieNF'
    end
    object QrControleSerieNF: TIntegerField
      FieldName = 'SerieNF'
      Origin = 'controle.SerieNF'
    end
    object QrControleModeloNF: TSmallintField
      FieldName = 'ModeloNF'
      Origin = 'controle.ModeloNF'
    end
    object QrControleMyPagTip: TSmallintField
      FieldName = 'MyPagTip'
      Origin = 'controle.MyPagTip'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
      Origin = 'controle.MyPagCar'
    end
    object QrControleControlaNeg: TSmallintField
      FieldName = 'ControlaNeg'
      Origin = 'controle.ControlaNeg'
    end
    object QrControleFamilias: TSmallintField
      FieldName = 'Familias'
      Origin = 'controle.Familias'
    end
    object QrControleFamiliasIts: TSmallintField
      FieldName = 'FamiliasIts'
      Origin = 'controle.FamiliasIts'
    end
    object QrControleAskNFOrca: TSmallintField
      FieldName = 'AskNFOrca'
      Origin = 'controle.AskNFOrca'
    end
    object QrControlePreviewNF: TSmallintField
      FieldName = 'PreviewNF'
      Origin = 'controle.PreviewNF'
    end
    object QrControleOrcaRapido: TSmallintField
      FieldName = 'OrcaRapido'
      Origin = 'controle.OrcaRapido'
    end
    object QrControleDistriDescoItens: TSmallintField
      FieldName = 'DistriDescoItens'
      Origin = 'controle.DistriDescoItens'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
      Origin = 'controle.EntraSemValor'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
      Origin = 'controle.MensalSempre'
    end
    object QrControleBalType: TSmallintField
      FieldName = 'BalType'
      Origin = 'controle.BalType'
    end
    object QrControleOrcaOrdem: TSmallintField
      FieldName = 'OrcaOrdem'
      Origin = 'controle.OrcaOrdem'
    end
    object QrControleOrcaLinhas: TSmallintField
      FieldName = 'OrcaLinhas'
      Origin = 'controle.OrcaLinhas'
    end
    object QrControleOrcaLFeed: TIntegerField
      FieldName = 'OrcaLFeed'
      Origin = 'controle.OrcaLFeed'
    end
    object QrControleOrcaModelo: TSmallintField
      FieldName = 'OrcaModelo'
      Origin = 'controle.OrcaModelo'
    end
    object QrControleOrcaRodaPos: TSmallintField
      FieldName = 'OrcaRodaPos'
      Origin = 'controle.OrcaRodaPos'
    end
    object QrControleOrcaRodape: TSmallintField
      FieldName = 'OrcaRodape'
      Origin = 'controle.OrcaRodape'
    end
    object QrControleOrcaCabecalho: TSmallintField
      FieldName = 'OrcaCabecalho'
      Origin = 'controle.OrcaCabecalho'
    end
    object QrControleCoresRel: TSmallintField
      FieldName = 'CoresRel'
      Origin = 'controle.CoresRel'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
      Origin = 'controle.MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'controle.Multa'
    end
    object QrControleCFiscalPadr: TWideStringField
      FieldName = 'CFiscalPadr'
      Origin = 'controle.CFiscalPadr'
    end
    object QrControleSitTribPadr: TWideStringField
      FieldName = 'SitTribPadr'
      Origin = 'controle.SitTribPadr'
    end
    object QrControleCFOPPadr: TWideStringField
      FieldName = 'CFOPPadr'
      Origin = 'controle.CFOPPadr'
    end
    object QrControleAvisosCxaEdit: TSmallintField
      FieldName = 'AvisosCxaEdit'
      Origin = 'controle.AvisosCxaEdit'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
      Origin = 'controle.TravaCidade'
    end
    object QrControleChConfCab: TIntegerField
      FieldName = 'ChConfCab'
      Origin = 'controle.ChConfCab'
    end
    object QrControleImpDOS: TIntegerField
      FieldName = 'ImpDOS'
      Origin = 'controle.ImpDOS'
    end
    object QrControleUnidadePadrao: TIntegerField
      FieldName = 'UnidadePadrao'
      Origin = 'controle.UnidadePadrao'
    end
    object QrControleProdutosV: TIntegerField
      FieldName = 'ProdutosV'
      Origin = 'controle.ProdutosV'
    end
    object QrControleCartDespesas: TIntegerField
      FieldName = 'CartDespesas'
      Origin = 'controle.CartDespesas'
    end
    object QrControleReserva: TSmallintField
      FieldName = 'Reserva'
      Origin = 'controle.Reserva'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Size = 18
    end
    object QrControleVerWeb: TIntegerField
      FieldName = 'VerWeb'
      Origin = 'controle.VerWeb'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
      Origin = 'controle.UFPadrao'
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'controle.Cidade'
      Size = 100
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
    end
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Origin = 'controle.Moeda'
      Size = 4
    end
    object QrControleErroHora: TIntegerField
      FieldName = 'ErroHora'
      Origin = 'controle.ErroHora'
    end
    object QrControleSenhas: TIntegerField
      FieldName = 'Senhas'
      Origin = 'controle.Senhas'
    end
    object QrControleSenhasIts: TIntegerField
      FieldName = 'SenhasIts'
      Origin = 'controle.SenhasIts'
    end
    object QrControleSalarios: TIntegerField
      FieldName = 'Salarios'
      Origin = 'controle.Salarios'
    end
    object QrControleEntidades: TIntegerField
      FieldName = 'Entidades'
      Origin = 'controle.Entidades'
    end
    object QrControleEntiCtas: TIntegerField
      FieldName = 'EntiCtas'
      Origin = 'controle.EntiCtas'
    end
    object QrControleUFs: TIntegerField
      FieldName = 'UFs'
      Origin = 'controle.UFs'
    end
    object QrControleListaECivil: TIntegerField
      FieldName = 'ListaECivil'
      Origin = 'controle.ListaECivil'
    end
    object QrControlePerfis: TIntegerField
      FieldName = 'Perfis'
      Origin = 'controle.Perfis'
    end
    object QrControleUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'controle.Usuario'
    end
    object QrControleContas: TIntegerField
      FieldName = 'Contas'
      Origin = 'controle.Contas'
    end
    object QrControleCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Origin = 'controle.CentroCusto'
    end
    object QrControleDepartamentos: TIntegerField
      FieldName = 'Departamentos'
      Origin = 'controle.Departamentos'
    end
    object QrControleDividas: TIntegerField
      FieldName = 'Dividas'
      Origin = 'controle.Dividas'
    end
    object QrControleDividasIts: TIntegerField
      FieldName = 'DividasIts'
      Origin = 'controle.DividasIts'
    end
    object QrControleDividasPgs: TIntegerField
      FieldName = 'DividasPgs'
      Origin = 'controle.DividasPgs'
    end
    object QrControleCarteiras: TIntegerField
      FieldName = 'Carteiras'
      Origin = 'controle.Carteiras'
    end
    object QrControleCarteirasU: TIntegerField
      FieldName = 'CarteirasU'
      Origin = 'controle.CarteirasU'
    end
    object QrControleCartaG: TIntegerField
      FieldName = 'CartaG'
      Origin = 'controle.CartaG'
    end
    object QrControleCartas: TIntegerField
      FieldName = 'Cartas'
      Origin = 'controle.Cartas'
    end
    object QrControleConsignacao: TIntegerField
      FieldName = 'Consignacao'
      Origin = 'controle.Consignacao'
    end
    object QrControleGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'controle.Grupo'
    end
    object QrControleSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
      Origin = 'controle.SubGrupo'
    end
    object QrControleConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'controle.Conjunto'
    end
    object QrControlePlano: TIntegerField
      FieldName = 'Plano'
      Origin = 'controle.Plano'
    end
    object QrControleInflacao: TIntegerField
      FieldName = 'Inflacao'
      Origin = 'controle.Inflacao'
    end
    object QrControlekm: TIntegerField
      FieldName = 'km'
      Origin = 'controle.km'
    end
    object QrControlekmMedia: TIntegerField
      FieldName = 'kmMedia'
      Origin = 'controle.kmMedia'
    end
    object QrControlekmIts: TIntegerField
      FieldName = 'kmIts'
      Origin = 'controle.kmIts'
    end
    object QrControleFatura: TIntegerField
      FieldName = 'Fatura'
      Origin = 'controle.Fatura'
    end
    object QrControleLanctos: TLargeintField
      FieldName = 'Lanctos'
      Origin = 'controle.lanctos'
    end
    object QrControleLctoEndoss: TIntegerField
      FieldName = 'LctoEndoss'
      Origin = 'controle.LctoEndoss'
    end
    object QrControleEntiGrupos: TIntegerField
      FieldName = 'EntiGrupos'
      Origin = 'controle.EntiGrupos'
    end
    object QrControleEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'controle.EntiContat'
    end
    object QrControleEntiCargos: TIntegerField
      FieldName = 'EntiCargos'
      Origin = 'controle.EntiCargos'
    end
    object QrControleEntiMail: TIntegerField
      FieldName = 'EntiMail'
      Origin = 'controle.EntiMail'
    end
    object QrControleEntiTel: TIntegerField
      FieldName = 'EntiTel'
      Origin = 'controle.EntiTel'
    end
    object QrControleEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'controle.EntiTipCto'
    end
    object QrControleAparencias: TIntegerField
      FieldName = 'Aparencias'
      Origin = 'controle.Aparencias'
    end
    object QrControlePages: TIntegerField
      FieldName = 'Pages'
      Origin = 'controle.Pages'
    end
    object QrControleMultiEtq: TIntegerField
      FieldName = 'MultiEtq'
      Origin = 'controle.MultiEtq'
    end
    object QrControleEntiTransp: TIntegerField
      FieldName = 'EntiTransp'
      Origin = 'controle.EntiTransp'
    end
    object QrControleEntiRespon: TIntegerField
      FieldName = 'EntiRespon'
      Origin = 'controle.EntiRespon'
    end
    object QrControleEntiCfgRel: TIntegerField
      FieldName = 'EntiCfgRel'
      Origin = 'controle.EntiCfgRel'
    end
    object QrControleExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'controle.ExcelGru'
    end
    object QrControleExcelGruImp: TIntegerField
      FieldName = 'ExcelGruImp'
      Origin = 'controle.ExcelGruImp'
    end
    object QrControleMediaCH: TIntegerField
      FieldName = 'MediaCH'
      Origin = 'controle.MediaCH'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Origin = 'controle.ContraSenha'
      Size = 50
    end
    object QrControleImprime: TIntegerField
      FieldName = 'Imprime'
      Origin = 'controle.Imprime'
    end
    object QrControleImprimeBand: TIntegerField
      FieldName = 'ImprimeBand'
      Origin = 'controle.ImprimeBand'
    end
    object QrControleImprimeView: TIntegerField
      FieldName = 'ImprimeView'
      Origin = 'controle.ImprimeView'
    end
    object QrControleComProdPerc: TIntegerField
      FieldName = 'ComProdPerc'
      Origin = 'controle.ComProdPerc'
    end
    object QrControleComProdEdit: TIntegerField
      FieldName = 'ComProdEdit'
      Origin = 'controle.ComProdEdit'
    end
    object QrControleComServPerc: TIntegerField
      FieldName = 'ComServPerc'
      Origin = 'controle.ComServPerc'
    end
    object QrControleComServEdit: TIntegerField
      FieldName = 'ComServEdit'
      Origin = 'controle.ComServEdit'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControlePadrPlacaCar: TWideStringField
      FieldName = 'PadrPlacaCar'
      Origin = 'controle.PadrPlacaCar'
      Size = 100
    end
    object QrControleServSMTP: TWideStringField
      FieldName = 'ServSMTP'
      Origin = 'controle.ServSMTP'
      Size = 50
    end
    object QrControleNomeMailOC: TWideStringField
      FieldName = 'NomeMailOC'
      Origin = 'controle.NomeMailOC'
      Size = 50
    end
    object QrControleDonoMailOC: TWideStringField
      FieldName = 'DonoMailOC'
      Origin = 'controle.DonoMailOC'
      Size = 50
    end
    object QrControleMailOC: TWideStringField
      FieldName = 'MailOC'
      Origin = 'controle.MailOC'
      Size = 80
    end
    object QrControleCorpoMailOC: TWideMemoField
      FieldName = 'CorpoMailOC'
      Origin = 'controle.CorpoMailOC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleConexaoDialUp: TWideStringField
      FieldName = 'ConexaoDialUp'
      Origin = 'controle.ConexaoDialUp'
      Size = 50
    end
    object QrControleMailCCCega: TWideStringField
      FieldName = 'MailCCCega'
      Origin = 'controle.MailCCCega'
      Size = 80
    end
    object QrControleContVen: TIntegerField
      FieldName = 'ContVen'
      Origin = 'controle.ContVen'
    end
    object QrControleContCom: TIntegerField
      FieldName = 'ContCom'
      Origin = 'controle.ContCom'
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
      Origin = 'controle.CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
      Origin = 'controle.CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
      Origin = 'controle.CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
      Origin = 'controle.CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
      Origin = 'controle.CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
      Origin = 'controle.CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
      Origin = 'controle.CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
      Origin = 'controle.CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
      Origin = 'controle.CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
      Origin = 'controle.CartEmA'
    end
    object QrControleMoedaVal: TFloatField
      FieldName = 'MoedaVal'
      Origin = 'controle.MoedaVal'
    end
    object QrControleTela1: TIntegerField
      FieldName = 'Tela1'
      Origin = 'controle.Tela1'
    end
    object QrControleChamarPgtoServ: TIntegerField
      FieldName = 'ChamarPgtoServ'
      Origin = 'controle.ChamarPgtoServ'
    end
    object QrControleFormUsaTam: TIntegerField
      FieldName = 'FormUsaTam'
      Origin = 'controle.FormUsaTam'
    end
    object QrControleFormHeight: TIntegerField
      FieldName = 'FormHeight'
      Origin = 'controle.FormHeight'
    end
    object QrControleFormWidth: TIntegerField
      FieldName = 'FormWidth'
      Origin = 'controle.FormWidth'
    end
    object QrControleFormPixEsq: TIntegerField
      FieldName = 'FormPixEsq'
      Origin = 'controle.FormPixEsq'
    end
    object QrControleFormPixDir: TIntegerField
      FieldName = 'FormPixDir'
      Origin = 'controle.FormPixDir'
    end
    object QrControleFormPixTop: TIntegerField
      FieldName = 'FormPixTop'
      Origin = 'controle.FormPixTop'
    end
    object QrControleFormPixBot: TIntegerField
      FieldName = 'FormPixBot'
      Origin = 'controle.FormPixBot'
    end
    object QrControleFormFoAlt: TIntegerField
      FieldName = 'FormFoAlt'
      Origin = 'controle.FormFoAlt'
    end
    object QrControleFormFoPro: TFloatField
      FieldName = 'FormFoPro'
      Origin = 'controle.FormFoPro'
    end
    object QrControleFormUsaPro: TIntegerField
      FieldName = 'FormUsaPro'
      Origin = 'controle.FormUsaPro'
    end
    object QrControleFormSlides: TIntegerField
      FieldName = 'FormSlides'
      Origin = 'controle.FormSlides'
    end
    object QrControleFormNeg: TIntegerField
      FieldName = 'FormNeg'
      Origin = 'controle.FormNeg'
    end
    object QrControleFormIta: TIntegerField
      FieldName = 'FormIta'
      Origin = 'controle.FormIta'
    end
    object QrControleFormSub: TIntegerField
      FieldName = 'FormSub'
      Origin = 'controle.FormSub'
    end
    object QrControleFormExt: TIntegerField
      FieldName = 'FormExt'
      Origin = 'controle.FormExt'
    end
    object QrControleFormFundoTipo: TIntegerField
      FieldName = 'FormFundoTipo'
      Origin = 'controle.FormFundoTipo'
    end
    object QrControleFormFundoBMP: TWideStringField
      FieldName = 'FormFundoBMP'
      Origin = 'controle.FormFundoBMP'
      Size = 255
    end
    object QrControleServInterv: TIntegerField
      FieldName = 'ServInterv'
      Origin = 'controle.ServInterv'
    end
    object QrControleServAntecip: TIntegerField
      FieldName = 'ServAntecip'
      Origin = 'controle.ServAntecip'
    end
    object QrControleAdiLancto: TIntegerField
      FieldName = 'AdiLancto'
      Origin = 'controle.AdiLancto'
    end
    object QrControleContaSal: TIntegerField
      FieldName = 'ContaSal'
      Origin = 'controle.ContaSal'
    end
    object QrControleContaVal: TIntegerField
      FieldName = 'ContaVal'
      Origin = 'controle.ContaVal'
    end
    object QrControlePronomeE: TWideStringField
      FieldName = 'PronomeE'
      Origin = 'controle.PronomeE'
    end
    object QrControlePronomeM: TWideStringField
      FieldName = 'PronomeM'
      Origin = 'controle.PronomeM'
    end
    object QrControlePronomeF: TWideStringField
      FieldName = 'PronomeF'
      Origin = 'controle.PronomeF'
    end
    object QrControlePronomeA: TWideStringField
      FieldName = 'PronomeA'
      Origin = 'controle.PronomeA'
    end
    object QrControleSaudacaoE: TWideStringField
      FieldName = 'SaudacaoE'
      Origin = 'controle.SaudacaoE'
      Size = 50
    end
    object QrControleSaudacaoM: TWideStringField
      FieldName = 'SaudacaoM'
      Origin = 'controle.SaudacaoM'
      Size = 50
    end
    object QrControleSaudacaoF: TWideStringField
      FieldName = 'SaudacaoF'
      Origin = 'controle.SaudacaoF'
      Size = 50
    end
    object QrControleSaudacaoA: TWideStringField
      FieldName = 'SaudacaoA'
      Origin = 'controle.SaudacaoA'
      Size = 50
    end
    object QrControleNiver: TSmallintField
      FieldName = 'Niver'
      Origin = 'controle.Niver'
    end
    object QrControleNiverddA: TSmallintField
      FieldName = 'NiverddA'
      Origin = 'controle.NiverddA'
    end
    object QrControleNiverddD: TSmallintField
      FieldName = 'NiverddD'
      Origin = 'controle.NiverddD'
    end
    object QrControleLastPassD: TDateTimeField
      FieldName = 'LastPassD'
      Origin = 'controle.LastPassD'
    end
    object QrControleMultiPass: TIntegerField
      FieldName = 'MultiPass'
      Origin = 'controle.MultiPass'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'controle.Codigo'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'controle.AlterWeb'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'controle.Ativo'
    end
    object QrControleBLQ_TopoAvisoV: TIntegerField
      FieldName = 'BLQ_TopoAvisoV'
      Origin = 'controle.BLQ_TopoAvisoV'
    end
    object QrControleBLQ_MEsqAvisoV: TIntegerField
      FieldName = 'BLQ_MEsqAvisoV'
      Origin = 'controle.BLQ_MEsqAvisoV'
    end
    object QrControleBLQ_AltuAvisoV: TIntegerField
      FieldName = 'BLQ_AltuAvisoV'
      Origin = 'controle.BLQ_AltuAvisoV'
    end
    object QrControleBLQ_LargAvisoV: TIntegerField
      FieldName = 'BLQ_LargAvisoV'
      Origin = 'controle.BLQ_LargAvisoV'
    end
    object QrControleBLQ_TopoDestin: TIntegerField
      FieldName = 'BLQ_TopoDestin'
      Origin = 'controle.BLQ_TopoDestin'
    end
    object QrControleBLQ_MEsqDestin: TIntegerField
      FieldName = 'BLQ_MEsqDestin'
      Origin = 'controle.BLQ_MEsqDestin'
    end
    object QrControleBLQ_AltuDestin: TIntegerField
      FieldName = 'BLQ_AltuDestin'
      Origin = 'controle.BLQ_AltuDestin'
    end
    object QrControleBLQ_LargDestin: TIntegerField
      FieldName = 'BLQ_LargDestin'
      Origin = 'controle.BLQ_LargDestin'
    end
  end
  object QrTerminal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TMySQLQuery
    Database = MyDB
    Left = 60
    Top = 4
  end
  object QrNTI: TMySQLQuery
    Database = MyDB
    Left = 60
    Top = 48
  end
  object ZZDB: TMySQLDatabase
    DesignOptions = []
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    LoginPrompt = True
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 96
    Top = 3
  end
  object QrTerminais: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrTransf: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM Lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 72
    Top = 144
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransfData: TDateField
      FieldName = 'Data'
    end
    object QrTransfTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTransfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTransfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfDocumento: TFloatField
      FieldName = 'Documento'
    end
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    DataSetOptions = []
    Left = 44
    Top = 144
  end
  object QrNew: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM entidades'
      'WHERE Cliente1="V"'
      'AND NOT (Codigo IN ('
      '     SELECT Codigo FROM cunscad'
      '     )'
      ')')
    Left = 648
    Top = 104
    object QrNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = ZZDB
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 648
    Top = 11
  end
  object QrSomaM: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM Lanctos')
    Left = 648
    Top = 55
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 60
    Top = 95
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrDuplicStrX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM PQ'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrDuplicIntX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM PQPSE'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 159
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrFields: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SHOW FIELDS FROM :P0')
    Left = 428
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'DELETE FROM Logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrInsLogX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'INSERT INTO Logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrRecCountX: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 392
    Top = 247
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrLocY: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM Carteiras'
      '')
    Left = 464
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrCountY: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 464
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrPerfis: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM PerfisIts pit'
      
        'LEFT JOIN PerfisItsPerf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 300
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrSel: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'UNLOCK TABLES;'
      'UPDATE Controle SET Movix=Movix+1;'
      'LOCK TABLES Controle READ;'
      'SELECT Movix FROM Controle;'
      'UNLOCK TABLES;')
    Left = 236
    Top = 292
    object QrSelMovix: TIntegerField
      FieldName = 'Movix'
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM Entidades en'
      'LEFT JOIN UFs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN UFs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 196
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
  end
  object QrAuxL: TMySQLQuery
    Database = ZZDB
    Left = 160
    Top = 51
  end
  object QrUpdL: TMySQLQuery
    Database = ZZDB
    Left = 160
    Top = 95
  end
  object QrBinaLigouA: TMySQLQuery
    Database = MyDB
    OnCalcFields = QrBinaLigouACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM binaligoua '
      'ORDER BY Codigo DESC')
    Left = 728
    Top = 332
    object QrBinaLigouACodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrBinaLigouANome: TWideStringField
      FieldName = 'Nome'
    end
    object QrBinaLigouAxData: TWideStringField
      FieldName = 'xData'
      Size = 10
    end
    object QrBinaLigouAxHora: TWideStringField
      FieldName = 'xHora'
      Size = 10
    end
    object QrBinaLigouAdData: TDateField
      FieldName = 'dData'
    end
    object QrBinaLigouAdHora: TTimeField
      FieldName = 'dHora'
    end
    object QrBinaLigouAUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrBinaLigouATerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrBinaLigouAIP: TWideStringField
      FieldName = 'IP'
      Size = 60
    end
    object QrBinaLigouAForcado: TSmallintField
      FieldName = 'Forcado'
    end
    object QrBinaLigouALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBinaLigouADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBinaLigouADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBinaLigouAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBinaLigouAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBinaLigouAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrBinaLigouAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBinaLigouATELEFONE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELEFONE'
      Size = 30
      Calculated = True
    end
  end
  object DsBinaLigouA: TDataSource
    DataSet = QrBinaLigouA
    Left = 728
    Top = 380
  end
  object QrBinaAB: TMySQLQuery
    Database = MyDB
    Left = 724
    Top = 284
  end
  object QrOpcoesTRen: TMySQLQuery
    Database = MyDB
    AfterOpen = QrOpcoesTRenAfterOpen
    Left = 728
    Top = 428
    object QrOpcoesTRenCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOpcoesTRenDefContrat: TIntegerField
      FieldName = 'DefContrat'
    end
    object QrOpcoesTRenGraNivPatr: TIntegerField
      FieldName = 'GraNivPatr'
    end
    object QrOpcoesTRenGraCodPatr: TIntegerField
      FieldName = 'GraCodPatr'
    end
    object QrOpcoesTRenGraNivOutr: TIntegerField
      FieldName = 'GraNivOutr'
    end
    object QrOpcoesTRenGraCodOutr: TIntegerField
      FieldName = 'GraCodOutr'
    end
    object QrOpcoesTRenBloPrdSPer: TSmallintField
      FieldName = 'BloPrdSPer'
    end
    object QrOpcoesTRenTipCodPatr: TIntegerField
      FieldName = 'TipCodPatr'
    end
    object QrOpcoesTRenTipCodOutr: TIntegerField
      FieldName = 'TipCodOutr'
    end
    object QrOpcoesTRenFormaCobrLoca: TSmallintField
      FieldName = 'FormaCobrLoca'
    end
    object QrOpcoesTRenHrLimPosDiaNaoUtil: TTimeField
      FieldName = 'HrLimPosDiaNaoUtil'
    end
    object QrOpcoesTRenLocArredMinut: TIntegerField
      FieldName = 'LocArredMinut'
    end
    object QrOpcoesTRenLocArredHrIniIni: TTimeField
      FieldName = 'LocArredHrIniIni'
    end
    object QrOpcoesTRenLocArredHrIniFim: TTimeField
      FieldName = 'LocArredHrIniFim'
    end
    object QrOpcoesTRenPermLocSemEstq: TSmallintField
      FieldName = 'PermLocSemEstq'
    end
    object QrOpcoesTRenTipCodServ: TIntegerField
      FieldName = 'TipCodServ'
    end
    object QrOpcoesTRenTipCodVend: TIntegerField
      FieldName = 'TipCodVend'
    end
    object QrOpcoesTRenStatIniPadrLoc: TSmallintField
      FieldName = 'StatIniPadrLoc'
    end
    object QrOpcoesTRenStatIniPadrSvc: TSmallintField
      FieldName = 'StatIniPadrSvc'
    end
    object QrOpcoesTRenStatIniPadrVen: TSmallintField
      FieldName = 'StatIniPadrVen'
    end
    object QrOpcoesTRenLocRegrFisNFe: TIntegerField
      FieldName = 'LocRegrFisNFe'
    end
    object QrOpcoesTRenNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Size = 50
    end
    object QrOpcoesTRenLocPeriodoExtenso: TWideStringField
      FieldName = 'LocPeriodoExtenso'
      Size = 60
    end
    object QrOpcoesTRenDdValidOrcaLoc: TIntegerField
      FieldName = 'DdValidOrcaLoc'
    end
    object QrOpcoesTRenDdValidOrcaVen: TIntegerField
      FieldName = 'DdValidOrcaVen'
    end
    object QrOpcoesTRenTxtDevolLimpeza: TWideStringField
      FieldName = 'TxtDevolLimpeza'
      Size = 255
    end
    object QrOpcoesTRenTxtDevolConserva: TWideStringField
      FieldName = 'TxtDevolConserva'
      Size = 255
    end
    object QrOpcoesTRenLocRegrFisNFCe: TIntegerField
      FieldName = 'LocRegrFisNFCe'
    end
    object QrOpcoesTRenLogo2_4_x_1_0: TWideStringField
      FieldName = 'Logo2_4_x_1_0'
      Size = 255
    end
    object QrOpcoesTRenLogo3_4_x_3_4: TWideStringField
      FieldName = 'Logo3_4_x_3_4'
      Size = 255
    end
    object QrOpcoesTRenMaxDescFatAtnd: TFloatField
      FieldName = 'MaxDescFatAtnd'
    end
  end
  object QrLCPP: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT lpp.DtHrLocado, lpp.DtHrRetorn, lpp.LibDtHr'
      'FROM loccpatpri lpp'
      'WHERE lpp.GraGruX=:P0'
      'ORDER BY lpp.DtHrLocado DESC'
      'LIMIT 1')
    Left = 412
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLCPPDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
    end
    object QrLCPPDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
    end
    object QrLCPPLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
    end
    object QrLCPPGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object QrGGXP: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Situacao'
      'FROM gragxpatr'
      'WHERE GraGruX>0')
    Left = 468
    Top = 388
    object QrGGXPSituacao: TWordField
      FieldName = 'Situacao'
    end
  end
  object QrUpd2: TMySQLQuery
    Database = MyDB
    Left = 204
    Top = 320
  end
  object MyDB: TMySQLDatabase
    DesignOptions = []
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 12
    Top = 8
  end
  object DqAux: TMySQLDirectQuery
    Database = MyDB
    Left = 588
    Top = 284
  end
  object QrAux3: TMySQLQuery
    Database = MyDB
    Left = 588
    Top = 336
  end
  object QrGGXEPatNeg: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT gg1.Nome, gg1.Patrimonio, gg1.Referencia, '
      'gep.*  '
      'FROM gragruepat gep '
      'LEFT JOIN gragrux ggx ON ggx.Controle=gep.GraGrux '
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE GraGruX IN ( '
      '0,1,2 '
      ') '
      'AND EstqSdo >-1 ')
    Left = 504
    Top = 24
    object QrGGXEPatNegNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrGGXEPatNegGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrGGXEPatNegEstqAnt: TFloatField
      FieldName = 'EstqAnt'
      Required = True
    end
    object QrGGXEPatNegEstqEnt: TFloatField
      FieldName = 'EstqEnt'
      Required = True
    end
    object QrGGXEPatNegEstqSai: TFloatField
      FieldName = 'EstqSai'
      Required = True
    end
    object QrGGXEPatNegEstqLoc: TFloatField
      FieldName = 'EstqLoc'
      Required = True
    end
    object QrGGXEPatNegEstqSdo: TFloatField
      FieldName = 'EstqSdo'
      Required = True
    end
    object QrGGXEPatNegLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGGXEPatNegDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGGXEPatNegDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGGXEPatNegUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGGXEPatNegUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGGXEPatNegAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGGXEPatNegAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGGXEPatNegAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGGXEPatNegAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGGXEPatNegPatrimonio: TWideStringField
      FieldName = 'Patrimonio'
      Size = 25
    end
    object QrGGXEPatNegReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
  end
  object QrItem: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, '
      'pgt.MadeBy, pgt.Fracio, '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 48
    Top = 504
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrItemHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrItemGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrItemprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object QrFatPedCab: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa, '
      'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal,'
      'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente,'
      'pvd.Codigo ID_Pedido, pvd.PedidoCli,'
      'ppc.JurosMes,'
      'frc.Nome NOMEFISREGCAD,'
      'ppc.MedDDReal, ppc.MedDDSimpl,'
      'par.TipMediaDD, par.Associada, par.FatSemEstq,'
      ''
      'par.CtaProdVen EMP_CtaProdVen,'
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtProdVen EMP_TxtProdVen,'
      'emp.Filial EMP_FILIAL,'
      'ufe.Codigo EMP_UF,'
      ''
      'ass.CtaProdVen ASS_CtaProdVen,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtProdVen ASS_TxtProdVen,'
      'ufa.Nome ASS_NO_UF,'
      'ufa.Codigo ASS_CO_UF,'
      'ase.Filial ASS_FILIAL,'
      ''
      'tpc.Nome NO_TabelaPrc,'
      '/*frm.TipoCalc,*/ fpc.*'
      'FROM fatpedcab fpc'
      'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa'
      'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo'
      
        'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, e' +
        'mp.PUF)'
      
        'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, a' +
        'se.PUF)'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      '/* N'#227'o tem como'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=pvd.RegrFiscal'
      '  AND frm.StqCenCad=1'
      '  AND frm.Empresa=-11'
      '*/'
      'WHERE fpc.Codigo > -1000')
    Left = 48
    Top = 456
    object QrFatPedCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'pedivda.Empresa'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatpedcab.Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatpedcab.CodUsu'
      Required = True
    end
    object QrFatPedCabPedido: TIntegerField
      FieldName = 'Pedido'
      Origin = 'fatpedcab.Pedido'
      Required = True
    end
    object QrFatPedCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'pedivda.CondicaoPG'
    end
    object QrFatPedCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'pedivda.TabelaPrc'
    end
    object QrFatPedCabMedDDReal: TFloatField
      FieldName = 'MedDDReal'
      Origin = 'pediprzcab.MedDDReal'
    end
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
      Origin = 'pediprzcab.MedDDSimpl'
    end
    object QrFatPedCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Origin = 'pedivda.RegrFiscal'
    end
    object QrFatPedCabNO_TabelaPrc: TWideStringField
      FieldName = 'NO_TabelaPrc'
      Origin = 'tabeprccab.Nome'
      Required = True
      Size = 50
    end
    object QrFatPedCabTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Origin = 'paramsemp.TipMediaDD'
      Required = True
    end
    object QrFatPedCabAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Origin = 'pedivda.AFP_Sit'
      Required = True
    end
    object QrFatPedCabAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Origin = 'pedivda.AFP_Per'
      Required = True
    end
    object QrFatPedCabAssociada: TIntegerField
      FieldName = 'Associada'
      Origin = 'paramsemp.Associada'
      Required = True
    end
    object QrFatPedCabCU_PediVda: TIntegerField
      FieldName = 'CU_PediVda'
      Origin = 'pedivda.CodUsu'
      Required = True
    end
    object QrFatPedCabAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatpedcab.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatpedcab.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrFatPedCabFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
      Required = True
    end
    object QrFatPedCabENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 30
      Calculated = True
    end
    object QrFatPedCabEMP_CtaProdVen: TIntegerField
      FieldName = 'EMP_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabEMP_TxtProdVen: TWideStringField
      FieldName = 'EMP_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabASS_CtaProdVen: TIntegerField
      FieldName = 'ASS_CtaProdVen'
      Origin = 'paramsemp.CtaProdVen'
    end
    object QrFatPedCabASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrFatPedCabASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrFatPedCabASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
      Origin = 'paramsemp.FaturaDta'
    end
    object QrFatPedCabASS_TxtProdVen: TWideStringField
      FieldName = 'ASS_TxtProdVen'
      Origin = 'paramsemp.TxtProdVen'
      Size = 100
    end
    object QrFatPedCabASS_NO_UF: TWideStringField
      FieldName = 'ASS_NO_UF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrFatPedCabASS_CO_UF: TIntegerField
      FieldName = 'ASS_CO_UF'
      Origin = 'ufs.Codigo'
      Required = True
    end
    object QrFatPedCabASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
      Origin = 'entidades.Filial'
    end
    object QrFatPedCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'pedivda.Cliente'
    end
    object QrFatPedCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFatPedCabNFDesfeita: TIntegerField
      FieldName = 'NFDesfeita'
    end
    object QrFatPedCabPEDIDO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PEDIDO_TXT'
      Size = 21
      Calculated = True
    end
    object QrFatPedCabID_Pedido: TIntegerField
      FieldName = 'ID_Pedido'
      Required = True
    end
    object QrFatPedCabJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrFatPedCabNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrFatPedCabEMP_UF: TIntegerField
      FieldName = 'EMP_UF'
      Required = True
    end
    object QrFatPedCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
  end
  object QrCli: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, indIEDest,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END + 0' +
        '.000 NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL,'
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE,'
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END + 0.000 UF,'
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD,'
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF,'
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's,'
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END + 0' +
        '.000 Lograd,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END + 0' +
        '.000 CEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF,'
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ','
      'L_Ativo '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 128
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliindIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
    object QrCliL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
  end
  object QrEstoque: TMySQLQuery
    Database = MyDB
    Left = 128
    Top = 460
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,###,##0.000'
    end
  end
  object DBAnt: TMySQLDatabase
    UserName = 'root'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root')
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 264
    Top = 8
  end
  object QrLca: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT lpp.Item, lpp.ValorDia, lpp.ValorSem, '
      'lpp.ValorQui, lpp.ValorMes, lpp.ValorLocacao'
      'FROM loccitslca lpp '
      'WHERE lpp.Codigo=30494'
      'AND lpp.ManejoLca=1')
    Left = 340
    Top = 412
    object QrLcaItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrLcaValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrLcaValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrLcaValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrLcaValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrLcaValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrLcaQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrLcaValorBruto: TFloatField
      FieldName = 'ValorBruto'
    end
    object QrLcaQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
    end
  end
  object QrOX1: TMySQLQuery
    Database = DBAnt
    SQL.Strings = (
      'SELECT Controle'
      'FROM gragrux'
      'WHERE GraGru1=:P0'
      'ORDER BY Controle')
    Left = 192
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOX1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
