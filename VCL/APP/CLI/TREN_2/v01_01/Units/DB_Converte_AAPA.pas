unit DB_Converte_AAPA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnMLAGeral,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts, (*DBTables,*)
  Math, UnDmkProcFunc, UnDmkEnums;

type
  TFmDB_Converte_AAPA = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    DsDBF: TDataSource;
    EdRegistrosTb: TdmkEdit;
    Label1: TLabel;
    DsCadCli: TDataSource;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    DsPsq: TDataSource;
    Panel7: TPanel;
    Panel9: TPanel;
    DBGrid2: TDBGrid;
    MeSQL1: TMemo;
    Panel8: TPanel;
    BtExecuta: TBitBtn;
    Splitter1: TSplitter;
    Label2: TLabel;
    EdRegistrosQr: TdmkEdit;
    GroupBox2: TGroupBox;
    Panel10: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdCliCodIni: TdmkEdit;
    EdForCodIni: TdmkEdit;
    EdEntCodIni: TdmkEdit;
    QrPsqEnt: TmySQLQuery;
    QrPsqEntCodigo: TIntegerField;
    CGDBF: TdmkCheckGroup;
    TabSheet4: TTabSheet;
    Memo1: TMemo;
    PB1: TProgressBar;
    Label6: TLabel;
    EdTabela: TdmkEdit;
    SpeedButton1: TSpeedButton;
    DsCadFor: TDataSource;
    DsCadGru: TDataSource;
    DataSource1: TDataSource;
    QrExiste: TmySQLQuery;
    QrGraGXPatr: TmySQLQuery;
    QrGraGXPatrAGRPAT: TWideStringField;
    DataSource2: TDataSource;
    DsCadVde: TDataSource;
    TabSheet5: TTabSheet;
    Memo2: TMemo;
    QrGraFabMar: TmySQLQuery;
    QrGraFabMarControle: TIntegerField;
    QrGraGXPatrGraGruX: TIntegerField;
    Button1: TButton;
    QrSituacao: TmySQLQuery;
    QrSituacaoSituacao: TWordField;
    QrSituacaoSIGLA: TWideStringField;
    QrSituacaoITENS: TLargeintField;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbDBFBeforeClose(DataSet: TDataSet);
    procedure TbDBFAfterOpen(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure QrPsqAfterOpen(DataSet: TDataSet);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure MeSQL1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdTabelaChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CGDBFDblClick(Sender: TObject);
    procedure CGDBFClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FTabLctA, FTabela, FTabStr: String;
    FCargoSocio, FPrincipalCli, FConComprador, FPrincipalFor,
    FFoneFix, FFoneCel, FFoneFax, FEMail, FETP_SERASA, FETP_SCPC, FETP_ACIM: Integer;
    //
    procedure Avisa(Str: String; Aguarde: Boolean = True);
    procedure ReabrePsqEnt(Antigo: String);
    procedure ReabreTabela(Table: TTable);
    //
    function  VerificaCadastro(Tabela, Codigo, CodUsu, Nome,
              Texto: String; NovoCodigo: TNovoCodigo): Integer;
    procedure IncluiSocio(Codigo, CargoSocio: Integer;
              Nome, _RG, _CPF: String; Natal: TDateTime);
    procedure IncluiContato(Codigo, Cargo: Integer;
              Nome, Tel, Cel, Fax, Mail: String);
    procedure IncluiEntiTel(Codigo, Controle, EntiTipCto: Integer;
              Tel: String);
    procedure IncluiEntiMail(Codigo, Controle, EntiTipCto: Integer;
              EMail: String);
    procedure IncluiBanco(Codigo, Ordem: Integer; Nome, Bco, Age: String);
    procedure IncluiConsulta(Codigo, Orgao: Integer;
              Numero: String; _Data: TDateTime; _Hora, _Resultado: String);
    //
    procedure CadastraPrdGrupTip();
    procedure CadastraGraGru1SemGrade(PrdGrupTip, Nivel5, Nivel4, Nivel3,
              Nivel2, Nivel1, CodUsu: Integer; Nome, Referencia: String);
    function  TabelaEstahSelecionada(Tabela: String): Boolean;
    //
    function  ObtemGraGruXDeCODMATX(CODMATX: String): Integer;
    //
    procedure CadastraSituacoes();

    //
    procedure ImportaTabela_CadCli();
    procedure ImportaTabela_CadFor();
    procedure ImportaTabela_CadGru();
    procedure ImportaTabela_CadPat();
    procedure ImportaTabela_CadCtr();
    procedure ImportaTabela_CadVde();
    procedure ImportaTabela_CadBco();
    procedure ImportaTabela_CadNat();
    procedure ImportaTabela_PagDup();
    procedure ImportaTabela_RecDup();
  public
    { Public declarations }
  end;

  var
  FmDB_Converte_AAPA: TFmDB_Converte_AAPA;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModProd, ModuleGeral,
  UnFinanceiro, UnGrade_Jan;

{$R *.DFM}

procedure TFmDB_Converte_AAPA.Avisa(Str: String; Aguarde: Boolean);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, Aguarde, FTabStr + Str);
end;

procedure TFmDB_Converte_AAPA.BtExecutaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    QrPsq.Close;
    QrPsq.SQL.Text := MeSQL1.Text;
    QrPsq.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDB_Converte_AAPA.BtOKClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to CGDBF.Items.Count - 1 do
  begin
    if CGDBF.Checked[I] then
    begin
      if FTabela = 'CADGRU.DBF' then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
        'SELECT Codigo ID',
        'FROM prdgruptip ',
        'WHERE Codigo=1',
        '']);
        // Caso n�o tenha sido inserido ainda!
        if QrExiste.RecordCount = 0 then
        begin
          CadastraPrdGrupTip();
        end;
        if (DmProd.QrOpcoesGrad.FieldByName('UsaGrade').AsInteger = 1) and
        (DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger = 0) and
        (DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger = 0) and
        (DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger = 0) then
        begin
          Geral.Mensagembox('Defina a cor e o tamanho �nicos nas op��es de grade!',
          'Aviso', MB_OK+MB_ICONWARNING);
          Grade_Jan.MostraFormOpcoesGrad();
        end;
      end;
    end;
  end;
  //

  if Application.MessageBox('Confirma a importa��o das tabelas selecionadas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> IDYES then
    Exit;
  //
  FCargoSocio := VerificaCadastro('enticargos', 'Codigo', 'CodUsu', 'Nome',
    'S�cio', ncControle);
  FConComprador := VerificaCadastro('enticargos', 'Codigo', 'CodUsu', 'Nome',
    'Comprador', ncControle);
  FPrincipalCli := VerificaCadastro('enticargos', 'Codigo', 'CodUsu', 'Nome',
    'Principal Cliente', ncControle);
  FPrincipalFor := VerificaCadastro('enticargos', 'Codigo', 'CodUsu', 'Nome',
    'Principal Fornecedor', ncControle);
  //
  FFoneFix := VerificaCadastro('entitipcto', 'Codigo', 'CodUsu', 'Nome',
    'Fixo (Tel)', ncControle);
  FFoneCel := VerificaCadastro('entitipcto', 'Codigo', 'CodUsu', 'Nome',
    'Celular', ncControle);
  FFoneFax := VerificaCadastro('entitipcto', 'Codigo', 'CodUsu', 'Nome',
    'Fax', ncControle);
  FEMail := VerificaCadastro('entitipcto', 'Codigo', 'CodUsu', 'Nome',
    'Email', ncControle);
  //
  FETP_SERASA := VerificaCadastro('entitippro', 'Codigo', '', 'Nome',
    'SERASA', ncControle);
  FETP_SCPC := VerificaCadastro('entitippro', 'Codigo', '', 'Nome',
    'SCPC', ncControle);
  FETP_ACIM := VerificaCadastro('entitippro', 'Codigo', '', 'Nome',
    'ACIM', ncControle);
  //
{
  for I := 0 to CGDBF.Items.Count - 1 do
  begin
    if CGDBF.Checked[I] then
    begin
      FTabela := Trim(Copy(CGDBF.Items[I], 1, 10)) + '.DBF';
      TbDBF.Close;
      TbDBF.TableName := FTabela;
      TbDBF.Open;
      FTabStr := 'Tabela: [' + FTabela + '] ';
      Avisa('');
      //
      if FTabela = 'CADCLI.DBF' then
        ImportaTabela_CadCli()
      else
      if FTabela = 'CADFOR.DBF' then
        ImportaTabela_CadFor()
      else
      if FTabela = 'CADGRU.DBF' then
        ImportaTabela_CadGru()
      else
      if FTabela = 'CADPAT.DBF' then
        ImportaTabela_CadPat()
      //
      //...
      //
      else Geral.MensagemBox('A tabela "' + FTabela +
      '" n�o tem implementa��o de convers�o!', 'Aviso', MB_OK+MB_ICONINFORMATION);
    end;
  end;
}
  for I := 0 to CGDBF.Items.Count - 1 do
  begin
    if CGDBF.Checked[I] then
    begin
      FTabela := Trim(Copy(CGDBF.Items[I], 1, 10)) + '.DBF';
      //
      if FTabela = 'CADCLI.DBF' then
        //ImportaTabela_CadCli()
      else
      if FTabela = 'CADFOR.DBF' then
        //ImportaTabela_CadFor()
      else
      if FTabela = 'CADGRU.DBF' then
        //ImportaTabela_CadGru()
      else
      if FTabela = 'CADPAT.DBF' then
        //ImportaTabela_CadPat()
      else
      if FTabela = 'CADVDE.DBF' then
        //ImportaTabela_CadVde()
      else
      if FTabela = 'CADCTR.DBF' then
        //ImportaTabela_CadCtr()
      else
      if FTabela = 'CADBCO.DBF' then
        //ImportaTabela_CadBco()
      else
      if FTabela = 'CADNAT.DBF' then
        //ImportaTabela_CadNat()
      else
      if FTabela = 'PAGDUP.DBF' then
        //ImportaTabela_PagDup()
      else
      if FTabela = 'RECDUP.DBF' then
        //ImportaTabela_RecDup()
      //
      //...
      //
      else
      begin
        Geral.MensagemBox('A tabela "' + FTabela +
        '" n�o tem implementa��o de convers�o!',
        'Aviso', MB_OK+MB_ICONINFORMATION);
        //
        Exit;
      end;
    end;
  end;

  //  Cadastros
  if TabelaEstahSelecionada('CADCLI.DBF') then ImportaTabela_CadCli();
  if TabelaEstahSelecionada('CADFOR.DBF') then ImportaTabela_CadFor();
  if TabelaEstahSelecionada('CADGRU.DBF') then ImportaTabela_CadGru();
  //  Loca��o
  if TabelaEstahSelecionada('CADPAT.DBF') then ImportaTabela_CadPat();
  if TabelaEstahSelecionada('CADVDE.DBF') then ImportaTabela_CadVde();
  if TabelaEstahSelecionada('CADCTR.DBF') then ImportaTabela_CadCtr();
  // Financeiro
  if TabelaEstahSelecionada('CADBCO.DBF') then ImportaTabela_CadBco();
  if TabelaEstahSelecionada('CADNAT.DBF') then ImportaTabela_CadNat();
  if TabelaEstahSelecionada('PAGDUP.DBF') then ImportaTabela_PagDup();
  if TabelaEstahSelecionada('RECDUP.DBF') then ImportaTabela_RecDup();

  FTabStr := '';
  Avisa('Atualizando tabela controle!', False);
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, [
  'Entidades'], [], [EdEntCodIni.ValueVariant], [], True);
  //
  Avisa('Cadastrando situac�es!', False);
  CadastraSituacoes();
  //
  Avisa('Importa��o finalizada!', False);
end;

procedure TFmDB_Converte_AAPA.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDB_Converte_AAPA.Button1Click(Sender: TObject);
begin
  CadastraSituacoes();
end;

procedure TFmDB_Converte_AAPA.CadastraGraGru1SemGrade(
  PrdGrupTip, Nivel5, Nivel4, Nivel3, Nivel2, Nivel1, CodUsu: Integer;
  Nome, Referencia: String);
var
  Controle, GraCorCad: Integer;
  GraTamCad, GraGruC, GraGru1, GraTamI: Integer;
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragru1', False, [
  'Nivel5', 'Nivel4', 'Nivel3', 'Nivel2', 'CodUsu',
  'Nome', 'PrdGrupTip', 'Referencia'], [
  'Nivel1'], [
  Nivel5, Nivel4, Nivel3, Nivel2, CodUsu,
  Nome, PrdGrupTip, Referencia], [
  Nivel1], True) then
  begin
    // Definir Grade de tamanhos
    GraTamCad := DmProd.QrOpcoesGrad.FieldByName('UnicaGra').AsInteger;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
    'GraTamCad'], ['Nivel1'], [
    GraTamCad], [Nivel1], True);

    // Registrar a cor
    //Nivel1         := Nivel1;
    Controle       := UMyMod.BuscaEmLivreY_Def('gragruc', 'Controle', stIns, 0);
    GraCorCad      := DmProd.QrOpcoesGrad.FieldByName('UnicaCor').AsInteger;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragruc', False, [
    'Nivel1', 'GraCorCad'], [
    'Controle'], [
    Nivel1, GraCorCad], [
    Controle], True) then
    begin
      // Cadastrar o item �nico da grade
      GraGruC        := Controle;
      GraGru1        := Nivel1;
      GraTamI        := DmProd.QrOpcoesGrad.FieldByName('UnicoTam').AsInteger;
      Controle       := UMyMod.BuscaEmLivreY_Def('gragrux', 'Controle', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragrux', False, [
      'GraGruC', 'GraGru1', 'GraTamI'], [
      'Controle'], [
      GraGruC, GraGru1, GraTamI], [
      Controle], True) then
      begin
        VAR_CADASTRO2 := Controle;
      end;
    end;
  end;
end;

procedure TFmDB_Converte_AAPA.CadastraPrdGrupTip();
var
  Nome, TitNiv1, TitNiv2, TitNiv3, TitNiv4, TitNiv5: String;
  Codigo, CodUsu, MadeBy, Fracio, Gradeado, TipPrd, NivCad, FaixaIni, FaixaFim,
  Customizav, ImpedeCad, LstPrcFisc, Tipo_Item, Genero: Integer;
  PerComissF, PerComissR: Double;
begin
  Codigo         := 1;
  CodUsu         := 1;
  Nome           := 'Patrim�nio';
  MadeBy         := 2;
  Fracio         := 0;
  Gradeado       := 0;
  TipPrd         := 3;
  NivCad         := 5;
  FaixaIni       := 0;
  FaixaFim       := 0;
  TitNiv1        := 'Nivel 1';
  TitNiv2        := 'Nivel 2';
  TitNiv3        := 'Nivel 3';
  TitNiv4        := 'Nivel 4';
  TitNiv5        := 'Nivel 5';
  Customizav     := 0;
  ImpedeCad      := 1;
  LstPrcFisc     := 5;
  PerComissF     := 0;
  PerComissR     := 0;
  Tipo_Item      := 0;
  Genero         := 0;

  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'prdgruptip', False, [
  'CodUsu', 'Nome', 'MadeBy',
  'Fracio', 'Gradeado', 'TipPrd',
  'NivCad', 'FaixaIni', 'FaixaFim',
  'TitNiv1', 'TitNiv2', 'TitNiv3',
  'Customizav', 'ImpedeCad', 'LstPrcFisc',
  'PerComissF', 'PerComissR', 'Tipo_Item',
  'Genero', 'TitNiv4', 'TitNiv5'], [
  'Codigo'], [
  CodUsu, Nome, MadeBy,
  Fracio, Gradeado, TipPrd,
  NivCad, FaixaIni, FaixaFim,
  TitNiv1, TitNiv2, TitNiv3,
  Customizav, ImpedeCad, LstPrcFisc,
  PerComissF, PerComissR, Tipo_Item,
  Genero, TitNiv4, TitNiv5], [
  Codigo], True);
end;

procedure TFmDB_Converte_AAPA.CadastraSituacoes();
var
  Nome, Sigla: String;
  Codigo, Aplicacao: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSituacao, Dmod.MyDB, [
  'SELECT DISTINCT Situacao, ',
  'CHAR(Situacao) SIGLA, ',
  'COUNT(Situacao) ITENS ',
  'FROM gragxpatr ',
  'GROUP BY Situacao ',
  'ORDER BY ITENS DESC ',
  '']);
  QrSituacao.First;
  while not QrSituacao.Eof do
  begin
    Codigo         := QrSituacaoSituacao.Value;
    Nome           := QrSituacaoSIGLA.Value;
    Sigla          := QrSituacaoSIGLA.Value;
    Aplicacao      := 0;
    if QrSituacaoSIGLA.Value = 'D' then
      Aplicacao := 1;
    //
    UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'graglsitu', False, [
    'Nome', 'Sigla', 'Aplicacao'], ['Codigo'], ['Ativo'], [
    Nome, Sigla, Aplicacao], [Codigo], [1], True);
    //
    QrSituacao.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.CGDBFClick(Sender: TObject);
begin
  EdTabela.Text := Trim(Copy(CGDBF.Items[CGDBF.ItemIndex], 1, 10)) + '.DBF';
end;

procedure TFmDB_Converte_AAPA.CGDBFDblClick(Sender: TObject);
begin
  // N�o funciona!
  //EdTabela.Text := Trim(Copy(CGDBF.Items[CGDBF.ItemIndex], 1, 10)) + '.DBF';
end;

procedure TFmDB_Converte_AAPA.EdTabelaChange(Sender: TObject);
begin
  TbDBF.Close;
end;

procedure TFmDB_Converte_AAPA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDB_Converte_AAPA.FormCreate(Sender: TObject);
begin
  FTabLctA := 'lct0001a';
  //
  ImgTipo.SQLType := stLok;
  CGDBF.Value := Trunc(//1 +  // n�o importar carteiras!
    Power(2,  2) + //       4
    Power(2,  4) + //      16
    Power(2,  5) + //      32
    Power(2,  6) + //      64
    Power(2,  9) + //     512
    Power(2, 12) + //    4096
    power(2, 21) + // 2097152
    power(2, 22) + // 4194304
    0);
end;

procedure TFmDB_Converte_AAPA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDB_Converte_AAPA.ReabrePsqEnt(Antigo: String);
begin
  QrPsqEnt.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqEnt, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM entidades ',
  'WHERE Antigo="' + Antigo + '"',
  'AND Codigo <> 0 ',
  'AND TRIM(Antigo) <> "" ',
  '']);
end;

procedure TFmDB_Converte_AAPA.ReabreTabela(Table: TTable);
begin
  Avisa('Abrindo');
  Table.Close;
  Table.Open;
  Avisa('Aberta');
  PB1.Position := 0;
  PB1.Max := Table.RecordCount;
end;

procedure TFmDB_Converte_AAPA.SpeedButton1Click(Sender: TObject);
begin
  TbDBF.Close;
  TbDBF.TableName := EdTabela.Text;
  TbDBF.Open;
end;

function TFmDB_Converte_AAPA.TabelaEstahSelecionada(Tabela: String): Boolean;
var
  I: Integer;
begin
  FTabela := Tabela;
  Result := False;
  //
  for I := 0 to CGDBF.Items.Count - 1 do
  begin
    if CGDBF.Checked[I] then
    begin
      if Trim(Copy(CGDBF.Items[I], 1, 10)) + '.DBF' = FTabela then
      begin
        TbDBF.Close;
        TbDBF.TableName := FTabela;
        TbDBF.Open;
        FTabStr := 'Tabela: [' + FTabela + '] ';
        Avisa('');
        //
        Result := True;
        Exit;
      end;
    end;
  end;
end;

procedure TFmDB_Converte_AAPA.TbDBFAfterOpen(DataSet: TDataSet);
begin
  EdRegistrosTb.ValueVariant := TbDBF.RecordCount;
end;

procedure TFmDB_Converte_AAPA.TbDBFBeforeClose(DataSet: TDataSet);
begin
  EdRegistrosTb.ValueVariant := 0;
end;


function TFmDB_Converte_AAPA.VerificaCadastro(Tabela, Codigo, CodUsu, Nome,
  Texto: String; NovoCodigo: TNovoCodigo): Integer;
var
  Idx: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT ' + Codigo + ' FROM ' + Tabela,
  'WHERE ' + Nome + '="' + Texto + '"',
  '']);
  //
  if Dmod.QrAux.RecordCount > 0 then
    Result := Dmod.QrAux.FieldByName(Codigo).AsInteger
  else begin
    Idx := 0;
    case NovoCodigo of
      ncControle: Idx := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, Tabela, Codigo, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: Idx := UMyMod.BPGS1I32(
        Tabela, Codigo, '', '', tsDef, stIns, 0);
      else
      begin
        Geral.MensagemBox('Tipo de obten��o de novo c�digo indefinido! [2]',
        'ERRO', MB_OK+MB_ICONERROR);
        Halt(0);
        //Exit;
      end;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ' + Tabela + ' SET ');
    Dmod.QrUpd.SQL.Add(Codigo + '=' + Geral.FF0(Idx) + ',');
    if CodUsu <> '' then
      Dmod.QrUpd.SQL.Add(CodUsu + '=' + Geral.FF0(Idx) + ',');
    Dmod.QrUpd.SQL.Add(Nome + '="' + Texto + '"');
    Dmod.QrUpd.ExecSQL;
    //
    Result := Idx;
  end;
end;

//////////////////////// T A B E L A S  ////////////////////////////////////////

procedure TFmDB_Converte_AAPA.ImportaTabela_CadBco();
var
  Nome, Nome2: String;
  Codigo, Tipo, ForneceI: Integer;
  SQLType: TSQLType;
begin
  ReabreTabela(TbCadBco);
  TbCadBco.First;
  while not TbCadBco.Eof do
  begin
    Codigo         := Trunc(TbCadBcoCODBCO.Value);
    Tipo           := 2;
    Nome           := TbCadBcoNOMBCO.Value;
    Nome2          := Nome;
    ForneceI       := -11;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Codigo ID',
    'FROM carteiras ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrExiste.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //  
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Nome);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'carteiras', False, [
    'Nome', 'Nome2', 'ForneceI'], [
    'Codigo', 'Tipo'], [
    Nome, Nome2, ForneceI], [
    Codigo, Tipo], True);
    //
    TbCadBco.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadCli();
const
  Mae = ''; NIRE = ''; FormaSociet = ''; Simples = ''; IEST = ''; Atividade = '';
  CPF_Pai = ''; CPF_Conjuge = ''; SSP = ''; DataRG = ''; CidadeNatal = '';
  EstCivil = ''; UFNatal = ''; Nacionalid = ''; EPais = ''; Ete3 = ''; ECel = '';
  PPais = ''; Pte3 = ''; PCel = '';
  //
  Cliente1 = 'V';
var
  ENDCLI: String;
  EAtividad, EUF, ECEP,
  PAtividad, PUF, PCEP,
  Codigo,    CUF, CCEP,
  Ordem, CodUsu, Tipo, Orgao: Integer;
  //
  CNPJ, IE, RazaoSocial, Fantasia, ELograd, ERua, ENumero, ECompl, EBairro, ECidade, ENatal, ETe1, ETe2, EFax, EContato, EEmail,
  CPF,  RG, Nome,        Apelido,  PLograd, PRua, PNumero, PCompl, PBairro, PCidade, PNatal, PTe1, PTe2, PFax, PContato, PEMail,
  Antigo, xBairro, xLogr, ECOCLI,  CLograd, CRua, CNumero, CCompl, CBairro, CCidade,
  //Numero, Compl, Bairro,
  Pai, Nivel, CreditosU, Observacoes, Comprador, Te1,
  Te2, Fax, Email, Respons1, Respons2, Txt: String;
  LimiCred: Double;
begin
(*
DELETE FROM entidades WHERE Codigo > 10000;
DELETE FROM enticontat;
DELETE FROM entimail;
DELETE FROM entirespon;
DELETE FROM entitel;
DELETE FROM entictas;
DELETE FROM entisrvpro;
*)
  ReabreTabela(TbCadCli);
  TbCadCli.First;
  while not TbCadCli.Eof do
  begin
    // N�o usa: TbCadCliFATCLI
    Antigo := 'C' + TbCadCliCODCLI.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Antigo);
    ReabrePsqEnt(Antigo);
    if QrPsqEnt.RecordCount = 0 then
    begin
      Codigo := Geral.IMV(TbCadCliCODCLI.Value) + EdCliCodIni.ValueVariant - 1;
      CodUsu := Codigo;
      ENDCLI := TbCadCliENDCLI.Value;
      CNPJ := Geral.SoNumero_TT(TbCadCliCGCCLI.Value);
      IE   := TbCadCliINSCLI.Value;
      if Geral.SoNumero_TT(IE) = '' then
        IE := '';
      CPF  := Geral.SoNumero_TT(TbCadCliCPFCLI.Value);
      RG   := TbCadCliRGECLI.Value;
      if Geral.SoNumero_TT(RG) = '' then
        RG := '';
      RazaoSocial := '';
      Fantasia := '';
      Nome := '';
      Apelido := '';
      EAtividad := 0; PAtividad := 0;
      ELograd := ''; ERua := ''; ENumero := ''; ECompl := '';
      PLograd := ''; PRua := ''; PNumero := ''; PCompl := '';
      EBairro := ''; PBairro := '';
      ECidade := ''; PCidade := '';
      EUF := 0; PUF := 0;
      ECEP := 0; PCEP := 0;
      ENatal := '0000-00-00';
      ETe1 := ''; ETe2 := ''; EFax := '';
      PTe1 := ''; PTe2 := ''; PFax := '';
      PNatal := '0000-00-00';
      PContato := ''; EContato := '';
      EEmail := ''; PEmail := '';
      //
      Comprador := Trim(TbCadCliCOMCLI.Value);
      Te1 := Geral.SoNumero_TT(TbCadCliTE1CLI.Value);
      Te2 := Geral.SoNumero_TT(TbCadCliTE2CLI.Value);
      Fax := Geral.SoNumero_TT(TbCadCliFAXCLI.Value);
      Email := Trim(Lowercase(TbCadCliEMACLI.Value));
      //
      PNumero := '';
      PCompl := '';
      xBairro := '';
      xLogr := '';
      PLograd := '';
      PRua := '';
      //
      if Length(CNPJ) >= 14 then
      begin
        Tipo := 0;
        Geral.SeparaEndereco(
          0, ENDCLI, ENumero, ECompl, xBairro, xLogr, ELograd, ERua,
          False, Memo1);
        //
        RazaoSocial := TbCadCliRAZCLI.Value;
        Fantasia := TbCadCliFANCLI.Value;
        EAtividad := Geral.IMV(TbCadCliRAMCLI.Value);
        EBairro := TbCadCliBAICLI.Value;
        ECidade := TbCadCliCIDCLI.Value;
        EUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESTCLI.Value);
        ECEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEPCLI.Value));
        ENatal := Geral.FDT(TbCadCliDTNCLI.Value, 1);
        //
        ETe1 := Te1;
        ETe2 := Te2;
        EFax := Fax;
        EContato := Comprador;
        EEmail := Email;
      end else
      begin
        Tipo := 1;
        Geral.SeparaEndereco(
        0, ENDCLI, PNumero, PCompl, xBairro, xLogr, PLograd, PRua,
        False, Memo1);
        //
        Nome := TbCadCliRAZCLI.Value;
        Apelido := TbCadCliFANCLI.Value;
        PAtividad := Geral.IMV(TbCadCliRAMCLI.Value);
        PBairro := TbCadCliBAICLI.Value;
        PCidade := TbCadCliCIDCLI.Value;
        PUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESTCLI.Value);
        PCEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEPCLI.Value));
        PNatal := Geral.FDT(TbCadCliDTNCLI.Value, 1);
        //
        PTe1 := Te1;
        PTe2 := Te2;
        PFax := Fax;
        PContato := Comprador;
        PEmail := Email;
      end;
      ECOCLI := TbCadCliECOCLI.Value;
      Geral.SeparaEndereco(
        0, ENDCLI, CNumero, CCompl, xBairro, xLogr, CLograd, CRua,
        False, Memo1);
      CBairro := TbCadCliBCOCLI.Value;
      CCidade := TbCadCliCIOCLI.Value;
      CUF := Geral.GetCodigoUF_da_SiglaUF(TbCadCliESOCLI.Value);
      CCEP := Geral.IMV(Geral.SoNumero_TT(TbCadCliCEOCLI.Value));
      //
      Pai := TbCadCliFILCLI.Value;
      Nivel := TbCadCliLIBCLI.Value;
      CreditosU :=  Geral.FDT(TbCadCliDTUCLI.Value, 1);
      LimiCred := TbCadCliLIMCLI.Value;
      try
        Observacoes := TbCadCliOBSCLI.Value;
      except
        Observacoes := '';
      end;
      //
      Respons1 := TbCadCliSO1CLI.Value;
      Respons2 := TbCadCliSO2CLI.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP',*) 'Cliente1',
      (*'Cliente2', 'Cliente3', 'Cliente4',
      'Fornece1', 'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', 'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      (*'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao',*) 'Nivel',
      (*'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD',*) 'CreditosU',
      (*'CreditosV', 'Motivo', 'QuantI1', 
      'QuantI2', 'QuantI3', 'QuantI4', 
      'QuantN1', 'QuantN2', 'QuantN3', 
      'Agenda', 'SenhaQuer', 'Senha1',*) 
      'LimiCred', (*'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo' (*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP,*) Cliente1,
      (*Cliente2, Cliente3, Cliente4,
      Fornece1, Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      (*CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao,*) Nivel,
      (*Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD,*) CreditosU,
      (*CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,*)
      LimiCred, (*Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo (*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        //
        if (Comprador <> '') or (Te1 <> '') or (Te2 <> '') or (Fax <> '') or
        (Email <> '') then
          IncluiContato(Codigo, FConComprador, Comprador, Te1, Te2, Fax, Email);
        //
        //
        // S � C I O S
        //
          // S�cio 1
        if (TbCadCliSO1CLI.Value <> '') (*Nome*)
        or (TbCadCliRG1CLI.Value <> '') (*RG*)
        or (TbCadCliCP1CLI.Value <> '') (*CPF*) then
        //or (TbCadCliDN1CLI.Value <> 0) (*Nasc*) then
          IncluiSocio(Codigo, FCargoSocio,
          TbCadCliSO1CLI.Value, TbCadCliRG1CLI.Value, TbCadCliCP1CLI.Value,
          TbCadCliDN1CLI.Value);
          // S�cio 2
        if (TbCadCliSO2CLI.Value <> '') (*Nome*)
        or (TbCadCliRG2CLI.Value <> '') (*RG*)
        or (TbCadCliCP2CLI.Value <> '') (*CPF*)  then
        //or (TbCadCliDN2CLI.Value <> 0) (*Nasc*) then
          IncluiSocio(Codigo, FCargoSocio,
          TbCadCliSO2CLI.Value, TbCadCliRG2CLI.Value, TbCadCliCP2CLI.Value,
          TbCadCliDN2CLI.Value);

        // P r i n c i p a i s   c l i e n t e s
        if (TbCadCliCL1CLI.Value <> '') (*Nome*)
        or (TbCadCliCF1CLI.Value <> '') (*tel*) then
          // Cliente 1
          IncluiContato(Codigo, FPrincipalCli, TbCadCliCL1CLI.Value,
            TbCadCliCF1CLI.Value, '', '', '');
        if (TbCadCliCL2CLI.Value <> '') (*Nome*)
        or (TbCadCliCF2CLI.Value <> '') (*tel*) then
          // Cliente 2
          IncluiContato(Codigo, FPrincipalCli, TbCadCliCL2CLI.Value,
            TbCadCliCF2CLI.Value, '', '', '');

        // P r i n c i p a i s   f o r n e c e d o r e s
        if (TbCadCliFO1CLI.Value <> '') (*Nome*)
        or (TbCadCliFF1CLI.Value <> '') (*tel*) then
          // Fornecedor 1
          IncluiContato(Codigo, FPrincipalFor, TbCadCliFO1CLI.Value,
            TbCadCliFF1CLI.Value, '', '', '');
        if (TbCadCliFO2CLI.Value <> '') (*Nome*)
        or (TbCadCliFF2CLI.Value <> '') (*tel*) then
          // Fornecedor 2
          IncluiContato(Codigo, FPrincipalFor, TbCadCliFO2CLI.Value,
            TbCadCliFF2CLI.Value, '', '', '');

        // R e f e r e n c i a s   b a n c � r i a s
        if (TbCadCliBC1CLI.Value <> '') (*Txt*)
        or (TbCadCliNB1CLI.Value <> '') (*Bco*)
        or (TbCadCliNA1CLI.Value <> '') (*Ag*) then
        begin
          Ordem := 1;
          IncluiBanco(Codigo, Ordem,
            TbCadCliBC1CLI.Value, TbCadCliNB1CLI.Value, TbCadCliNA1CLI.Value);
        end;
        if (TbCadCliBC2CLI.Value <> '') (*Txt*)
        or (TbCadCliNB2CLI.Value <> '') (*Bco*)
        or (TbCadCliNA2CLI.Value <> '') (*Ag*) then
        begin
          Ordem := 2;
          IncluiBanco(Codigo, Ordem,
            TbCadCliBC2CLI.Value, TbCadCliNB2CLI.Value, TbCadCliNA2CLI.Value);
        end;

        // EntiSrvPro > SPC
        if (TbCadCliSPCCLI.Value <> '') (*Txt*)
        or (TbCadCliNRCCLI.Value <> '') (*Num*)
        or (TbCadCliHORCLI.Value <> '') (*Hora*) then
        begin
          Txt := TbCadCliSPCCLI.Value + TbCadCliNRCCLI.Value;
          if Pos('SERASA', Txt) > 0 then
            Orgao := FETP_SERASA
          else
          if Pos('ACIM', Txt) > 0 then
            Orgao := FETP_SERASA
          else
            Orgao := FETP_SCPC;
          //
          IncluiConsulta(Codigo, Orgao, TbCadCliNRCCLI.Value, TbCadCliDSPCLI.Value,
            TbCadCliHORCLI.Value, TbCadCliSPCCLI.Value);
        end;
      end;
    end;
    TbCadCli.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadCtr();
  procedure IncluiLocFCab();
  var
    DtHrFat: String;
    Codigo, Controle, NumNF: Integer;
    ValLocad, ValConsu, ValUsado, ValFrete, ValDesco, ValTotal: Double;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Controle       := Trunc(TbCadCtrNRCTR.Value);
    DtHrFat        := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    ValLocad       := TbCadCtrVALUNI1.Value + TbCadCtrVALUNI2.Value;
    ValConsu       := 0;
    ValUsado       := 0;
    ValFrete       := TbCadCtrFRETE.Value;
    ValDesco       := 0;
    ValTotal       := TbCadCtrTOTPEDG.Value;
    NumNF          := Trunc(TbCadCtrNRNF.Value);
    //
    //UMyMod.BPGS1I32('locfcab', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stins, 'locfcab', False, [
    'Codigo', 'DtHrFat', 'ValLocad',
    'ValConsu', 'ValUsado', 'ValFrete',
    'ValDesco', 'ValTotal', 'NumNF'], [
    'Controle'], [
    Codigo, DtHrFat, ValLocad,
    ValConsu, ValUsado, ValFrete,
    ValDesco, ValTotal, NumNF], [
    Controle], True);
  end;

  procedure IncluiLctFatRef(Valor: Double);
  var
    Codigo, Controle, Conta: Integer;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Controle       := Trunc(TbCadCtrNRCTR.Value);
    //Conta          := ;
    //Valor          := ;
    //
    Conta := UMyMod.BPGS1I32('lctfatref', 'Conta', '', '', tsPos, stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctfatref', False, [
    'Codigo', 'Controle', 'Valor'], [
    'Conta'], [
    Codigo, Controle, Valor], [
    Conta], True);
  end;

  //

  procedure IncluiLocCCon();
  const
    Empresa = -11;
  var
    DtHrEmi, DtHrBxa, LocalObra, Obs0, Obs1, Obs2: String;
    Codigo, Contrato, Cliente, Vendedor: Integer;
    ValorTot: Double;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    Contrato       := -999;
    Cliente        := DModG.ObtemCodigoDeEntidadePeloAntigo('C' + FormatFloat('00000', TbCadCtrCODCLIP.Value));
    DtHrEmi        := Geral.FDT(TbCadCtrDTEMI.Value, 1);
    DtHrBxa        := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    Vendedor       := TbCadCtrVEND.Value;
    ValorTot       := TbCadCtrTOTPEDG.Value;
    LocalObra      := TbCadCtrENDOBRA.Value;
    Obs0           := TbCadCtrOBS0.Value;
    Obs1           := TbCadCtrOBS1.Value;
    Obs2           := TbCadCtrOBS2.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'locccon', False, [
    'Empresa',
    'Contrato', 'Cliente', 'DtHrEmi',
    'DtHrBxa', 'Vendedor', 'ValorTot',
    'LocalObra', 'Obs0', 'Obs1',
    'Obs2'], [
    'Codigo'], [
    Empresa,
    Contrato, Cliente, DtHrEmi,
    DtHrBxa, Vendedor, ValorTot,
    LocalObra, Obs0, Obs1,
    Obs2], [
    Codigo], True);
  end;

  //

  procedure IncluiLocCPatPri(const GraGruX: Integer; var CtrID: Integer);
    function LiberadoPor(Data: TDateTime): Integer;
    begin
      if Data > 2 then
        Result := -1
      else
        Result := 0;
    end;
  var
    DtHrLocado, DtHrRetorn, LibDtHr, RELIB, HOLIB: String;
    Codigo, LibFunci: Integer;
  begin
    Codigo         := Trunc(TbCadCtrNRCTR.Value);
    //CtrID          := ;
    //GraGruX        := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT1.Value);
    DtHrLocado     := Geral.FDT(TbCadCtrDTEMI.Value, 1);
    DtHrRetorn     := Geral.FDT(TbCadCtrDTBAIXA.Value, 1);
    LibFunci       := LiberadoPor(TbCadCtrDTBAIXA.Value);
    LibDtHr        := Geral.FDT(TbCadCtrDTLIB.Value, 1);
    RELIB          := TbCadCtrRELIB.Value;
    HOLIB          := TbCadCtrHOLIB.Value;
    //
    CtrID := UMyMod.BPGS1I32('loccpatpri', 'CtrID', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'loccpatpri', False, [
    'Codigo', 'GraGruX', 'DtHrLocado',
    'DtHrRetorn', 'LibFunci', 'LibDtHr',
    'RELIB', 'HOLIB'], [
    'CtrID'], [
    Codigo, GraGruX, DtHrLocado,
    DtHrRetorn, LibFunci, LibDtHr,
    RELIB, HOLIB], [
    CtrID], True);
  end;

  //

var
  GraGruX, CtrID: Integer;
  CodX: String;
begin
  ReabreTabela(TbCadCtr);
  TbCadCtr.First;
  while not TbCadCtr.Eof do
  begin
    try
      CodX := Geral.FF0(Trunc(TbCadCtrNRCTR.Value));
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + CodX);
      //
      if TbCadCtrNRCTR.Value <> 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
        'SELECT Codigo ID',
        'FROM locccon ',
        'WHERE Codigo=' + CodX,
        '']);
        if QrExiste.RecordCount = 0 then
        begin
          IncluiLocCCon();
          //

          GraGruX := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT1.Value);
          if GraGruX <> 0 then
          begin
            IncluiLocCPatPri(GraGrux, CtrID);
            if (TbCadCtrDTBAIXA.Value > 1) and (TbCadCtrVALUNI1.Value >= 0.01) then
              IncluiLctFatRef(TbCadCtrVALUNI1.Value);
          end;
          //
          GraGruX := ObtemGraGruXDeCODMATX(TbCadCtrCODMAT2.Value);
          if GraGruX <> 0 then
          begin
            IncluiLocCPatPri(GraGrux, CtrID);
            if (TbCadCtrDTBAIXA.Value > 1) and (TbCadCtrVALUNI2.Value >= 0.01) then
              IncluiLctFatRef(TbCadCtrVALUNI2.Value);
          end;
          //
          if (TbCadCtrDTBAIXA.Value > 1) then
            IncluiLocFCab();
        end;
      end;
    except
      Memo2.Lines.Add('Erro no c�digo: ' + Geral.FFT(TbCadCtrNRCTR.Value, 0, siPositivo));
    end;
    TbCadCtr.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadFor();
const
  Mae = ''; NIRE = ''; FormaSociet = ''; Simples = ''; IEST = ''; Atividade = '';
  CPF_Pai = ''; CPF_Conjuge = ''; SSP = ''; DataRG = ''; CidadeNatal = '';
  EstCivil = ''; UFNatal = ''; Nacionalid = ''; EPais = ''; Ete3 = ''; ECel = '';
  PPais = ''; Pte3 = ''; PCel = '';
  Respons1 = ''; Respons2 = ''; Pai = '';
  //
  Fornece1 = 'V';
var
  ENDFOR: String;
  EAtividad, EUF, ECEP,
  PAtividad, PUF, PCEP,
  Codigo,    //CUF, CCEP,
  //Ordem, Orgao,
  CodUsu, Tipo: Integer;
  //
  CNPJ, IE, RazaoSocial, Fantasia, ELograd, ERua, ENumero, ECompl, EBairro, ECidade, ENatal, ETe1, ETe2, EFax, EContato, EEmail,
  CPF,  RG, Nome,        Apelido,  PLograd, PRua, PNumero, PCompl, PBairro, PCidade, PNatal, PTe1, PTe2, PFax, PContato, PEMail,
  Antigo, xBairro, xLogr,
  //Numero, Compl, Bairro, Txt,
  Observacoes, Te1,
  Te2, Fax: String;
  //
  //LimiCred: Double;
  //ECOFOR, CLograd, CRua, CNumero, CCompl, CBairro, CCidade, Comprador, Email,
  //Respons1, Respons2, Pai, Nivel, CreditosU: String;
begin
(*
DELETE FROM entidades WHERE Codigo > 20000;
DELETE FROM enticontat;
DELETE FROM entimail;
DELETE FROM entirespon;
DELETE FROM entitel;
DELETE FROM entictas;
DELETE FROM entisrvpro;
*)
  ReabreTabela(TbCadFor);
  TbCadFor.First;
  while not TbCadFor.Eof do
  begin
    // N�o usa: TbCadForFATFOR
    Antigo := 'F' + TbCadForCODFOR.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Antigo);
    ReabrePsqEnt(Antigo);
    if QrPsqEnt.RecordCount = 0 then
    begin
      Codigo := Geral.IMV(TbCadForCODFOR.Value) + EdForCodIni.ValueVariant - 1;
      CodUsu := Codigo;
      ENDFOR := TbCadForENDFOR.Value;
      CNPJ := Geral.SoNumero_TT(TbCadForCGCFOR.Value);
      IE   := TbCadForINSFOR.Value;
      if Geral.SoNumero_TT(IE) = '' then
        IE := '';
      CPF  := Geral.SoNumero_TT(TbCadForCPFFOR.Value);
      RG   := TbCadForRGEFOR.Value;
      if Geral.SoNumero_TT(RG) = '' then
        RG := '';
      RazaoSocial := '';
      Fantasia := '';
      Nome := '';
      Apelido := '';
      EAtividad := 0; PAtividad := 0;
      ELograd := ''; ERua := ''; ENumero := ''; ECompl := '';
      PLograd := ''; PRua := ''; PNumero := ''; PCompl := '';
      EBairro := ''; PBairro := '';
      ECidade := ''; PCidade := '';
      EUF := 0; PUF := 0;
      ECEP := 0; PCEP := 0;
      ENatal := '0000-00-00';
      ETe1 := ''; ETe2 := ''; EFax := '';
      PTe1 := ''; PTe2 := ''; PFax := '';
      PNatal := '0000-00-00';
      PContato := ''; EContato := '';
      EEmail := ''; PEmail := '';
      //
      //Comprador := Trim(TbCadForCOMFOR.Value);
      Te1 := Geral.SoNumero_TT(TbCadForTE1FOR.Value);
      Te2 := Geral.SoNumero_TT(TbCadForTE2FOR.Value);
      Fax := Geral.SoNumero_TT(TbCadForFAXFOR.Value);
      //Email := Trim(Lowercase(TbCadForEMAFOR.Value));
      //
      PNumero := '';
      PCompl := '';
      xBairro := '';
      xLogr := '';
      PLograd := '';
      PRua := '';
      //
      if Length(CNPJ) >= 14 then
      begin
        Tipo := 0;
        Geral.SeparaEndereco(
          0, ENDFOR, ENumero, ECompl, xBairro, xLogr, ELograd, ERua,
          False, Memo1);
        //
        RazaoSocial := TbCadForRAZFOR.Value;
        Fantasia := TbCadForFANFOR.Value;
        EAtividad := Geral.IMV(TbCadForRAMFOR.Value);
        EBairro := TbCadForBAIFOR.Value;
        ECidade := TbCadForCIDFOR.Value;
        EUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESTFOR.Value);
        ECEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEPFOR.Value));
        //ENatal := Geral.FDT(TbCadForDTNFOR.Value, 1);
        //
        ETe1 := Te1;
        ETe2 := Te2;
        EFax := Fax;
        //EContato := Comprador;
        //EEmail := Email;
      end else
      begin
        Tipo := 1;
        Geral.SeparaEndereco(
        0, ENDFOR, PNumero, PCompl, xBairro, xLogr, PLograd, PRua,
        False, Memo1);
        //
        Nome := TbCadForRAZFOR.Value;
        Apelido := TbCadForFANFOR.Value;
        PAtividad := Geral.IMV(TbCadForRAMFOR.Value);
        PBairro := TbCadForBAIFOR.Value;
        PCidade := TbCadForCIDFOR.Value;
        PUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESTFOR.Value);
        PCEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEPFOR.Value));
        //PNatal := Geral.FDT(TbCadForDTNFOR.Value, 1);
        //
        PTe1 := Te1;
        PTe2 := Te2;
        PFax := Fax;
        //PContato := Comprador;
        //PEmail := Email;
      end;
      {
      ECOFOR := TbCadForECOFOR.Value;
      Geral.SeparaEndereco(
        0, ENDFOR, CNumero, CCompl, xBairro, xLogr, CLograd, CRua,
        False, Memo1);
      CBairro := TbCadForBCOFOR.Value;
      CCidade := TbCadForCIOFOR.Value;
      CUF := Geral.GetCodigoUF_da_SiglaUF(TbCadForESOFOR.Value);
      CCEP := Geral.IMV(Geral.SoNumero_TT(TbCadForCEOFOR.Value));
      //
      Pai := TbCadForFILFOR.Value;
      Nivel := TbCadForLIBFOR.Value;
      CreditosU :=  Geral.FDT(TbCadForDTUFOR.Value, 1);
      LimiCred := TbCadForLIMFOR.Value;
      //
      Respons1 := TbCadForSO1FOR.Value;
      Respons2 := TbCadForSO2FOR.Value;
      }
      try
        Observacoes := TbCadForOBSFOR.Value;
      except
        Observacoes := '';
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
      'CodUsu', 'RazaoSocial', 'Fantasia',
      'Respons1', 'Respons2', 'Pai',
      'Mae', 'CNPJ', 'IE',
      'NIRE', 'FormaSociet', 'Simples',
      'IEST', 'Atividade', 'Nome',
      'Apelido', 'CPF', 'CPF_Pai',
      'CPF_Conjuge', 'RG', 'SSP',
      'DataRG', 'CidadeNatal', 'EstCivil',
      'UFNatal', 'Nacionalid', 'ELograd',
      'ERua', 'ENumero', 'ECompl',
      'EBairro', 'ECidade', 'EUF',
      'ECEP', 'EPais', 'ETe1',
      'Ete2', 'Ete3', 'ECel',
      'EFax', 'EEmail', 'EContato',
      'ENatal', 'PLograd', 'PRua',
      'PNumero', 'PCompl', 'PBairro',
      'PCidade', 'PUF', 'PCEP',
      'PPais', 'PTe1', 'Pte2',
      'Pte3', 'PCel', 'PFax',
      'PEmail', 'PContato', 'PNatal',
      (*'Sexo', 'Responsavel', 'Profissao',
      'Cargo', 'Recibo', 'DiaRecibo',
      'AjudaEmpV', 'AjudaEmpP', 'Cliente1',
      'Cliente2', 'Cliente3', 'Cliente4',*)
      'Fornece1', (*'Fornece2', 'Fornece3',
      'Fornece4', 'Fornece5', 'Fornece6',
      'Fornece7', 'Fornece8', 'Terceiro',
      'Cadastro', 'Informacoes', 'Logo',
      'Veiculo', 'Mensal',*) 'Observacoes',
      'Tipo', (*'CLograd', 'CRua',
      'CNumero', 'CCompl', 'CBairro',
      'CCidade', 'CUF', 'CCEP',
      'CPais', 'CTel', 'CCel',
      'CFax', 'CContato', 'LLograd',
      'LRua', 'LNumero', 'LCompl',
      'LBairro', 'LCidade', 'LUF',
      'LCEP', 'LPais', 'LTel',
      'LCel', 'LFax', 'LContato',
      'Comissao', 'Situacao', 'Nivel',
      'Grupo', 'Account', 'Logo2',
      'ConjugeNome', 'ConjugeNatal', 'Nome1',
      'Natal1', 'Nome2', 'Natal2',
      'Nome3', 'Natal3', 'Nome4',
      'Natal4', 'CreditosI', 'CreditosL',
      'CreditosF2', 'CreditosD', 'CreditosU',
      'CreditosV', 'Motivo', 'QuantI1',
      'QuantI2', 'QuantI3', 'QuantI4',
      'QuantN1', 'QuantN2', 'QuantN3',
      'Agenda', 'SenhaQuer', 'Senha1',
      'LimiCred', 'Desco', 'CasasApliDesco',
      'TempA', 'TempD', 'Banco',
      'Agencia', 'ContaCorrente', 'FatorCompra',
      'AdValorem', 'DMaisC', 'DMaisD',
      'Empresa', 'CBE', 'SCB',*)
      'PAtividad', 'EAtividad', (*'PCidadeCod',
      'ECidadeCod', 'PPaisCod', 'EPaisCod',*)
      'Antigo' (*, 'CUF2', 'Contab',
      'MSN1', 'PastaTxtFTP', 'PastaPwdFTP',
      'Protestar', 'MultaCodi', 'MultaDias',
      'MultaValr', 'MultaPerc', 'MultaTiVe',
      'JuroSacado', 'CPMF', 'Corrido',
      'CPF_Resp1', 'CliInt', 'AltDtPlaCt',
      'CartPref', 'RolComis', 'Filial',
      'EEndeRef', 'PEndeRef', 'CEndeRef',
      'LEndeRef', 'ECodMunici', 'PCodMunici',
      'CCodMunici', 'LCodMunici', 'CNAE',
      'SUFRAMA', 'ECodiPais', 'PCodiPais',
      'L_CNPJ', 'L_Ativo', 'CNAE20',
      'URL', 'CRT', 'COD_PART'*)], [
      'Codigo'], [
      CodUsu, RazaoSocial, Fantasia,
      Respons1, Respons2, Pai,
      Mae, CNPJ, IE,
      NIRE, FormaSociet, Simples,
      IEST, Atividade, Nome,
      Apelido, CPF, CPF_Pai,
      CPF_Conjuge, RG, SSP,
      DataRG, CidadeNatal, EstCivil,
      UFNatal, Nacionalid, ELograd,
      ERua, ENumero, ECompl,
      EBairro, ECidade, EUF,
      ECEP, EPais, ETe1,
      Ete2, Ete3, ECel,
      EFax, EEmail, EContato,
      ENatal, PLograd, PRua,
      PNumero, PCompl, PBairro,
      PCidade, PUF, PCEP,
      PPais, PTe1, Pte2,
      Pte3, PCel, PFax,
      PEmail, PContato, PNatal,
      (*Sexo, Responsavel, Profissao,
      Cargo, Recibo, DiaRecibo,
      AjudaEmpV, AjudaEmpP, Cliente1,
      Cliente2, Cliente3, Cliente4,*)
      Fornece1, (*Fornece2, Fornece3,
      Fornece4, Fornece5, Fornece6,
      Fornece7, Fornece8, Terceiro,
      Cadastro, Informacoes, Logo,
      Veiculo, Mensal,*) Observacoes,
      Tipo, (*CLograd, CRua,
      CNumero, CCompl, CBairro,
      CCidade, CUF, CCEP,
      CPais, CTel, CCel,
      CFax, CContato, LLograd,
      LRua, LNumero, LCompl,
      LBairro, LCidade, LUF,
      LCEP, LPais, LTel,
      LCel, LFax, LContato,
      Comissao, Situacao, Nivel,
      Grupo, Account, Logo2,
      ConjugeNome, ConjugeNatal, Nome1,
      Natal1, Nome2, Natal2,
      Nome3, Natal3, Nome4,
      Natal4, CreditosI, CreditosL,
      CreditosF2, CreditosD, CreditosU,
      CreditosV, Motivo, QuantI1,
      QuantI2, QuantI3, QuantI4,
      QuantN1, QuantN2, QuantN3,
      Agenda, SenhaQuer, Senha1,
      LimiCred, Desco, CasasApliDesco,
      TempA, TempD, Banco,
      Agencia, ContaCorrente, FatorCompra,
      AdValorem, DMaisC, DMaisD,
      Empresa, CBE, SCB,*)
      PAtividad, EAtividad, (*PCidadeCod,
      ECidadeCod, PPaisCod, EPaisCod,*)
      Antigo (*, CUF2, Contab,
      MSN1, PastaTxtFTP, PastaPwdFTP,
      Protestar, MultaCodi, MultaDias,
      MultaValr, MultaPerc, MultaTiVe,
      JuroSacado, CPMF, Corrido,
      CPF_Resp1, CliInt, AltDtPlaCt,
      CartPref, RolComis, Filial,
      EEndeRef, PEndeRef, CEndeRef,
      LEndeRef, ECodMunici, PCodMunici,
      CCodMunici, LCodMunici, CNAE,
      SUFRAMA, ECodiPais, PCodiPais,
      L_CNPJ, L_Ativo, CNAE20,
      URL, CRT, COD_PART*)], [
      Codigo], True) then
      begin
        // N�o h� outras inclus�es!
      end;
    end;
    TbCadFor.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadGru;
var
  //Teste,
  CodsX, Cod, CodUsu, Nivel2, Nivel3, Nivel4, Nivel5, Nivel6: String;
  //I,
  Itens: Integer;
  ArrNiveis: array of String;
  Nome: String;
begin
  ReabreTabela(TbCadGru);
  TbCadGru.First;
  while not TbCadGru.Eof do
  begin
    Itens := 0;
    CodsX := TbCadGruCODGRU.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + CodsX);
    while CodsX <> '' do
    begin
      Itens := Itens + 1;
      SetLength(ArrNiveis, Itens);
      if Geral.SeparaPrimeiraOcorrenciaDeTexto('.', CodsX, Cod, CodsX) then
        ArrNiveis[Itens-1] := Cod
      else
      begin
        Geral.MensagemBox('Erro fatal na importa��o de grupos de grade!' +
        #13#10 + 'O aplicativo ser� finalizado!', 'ERRO', MB_OK+MB_ICONERROR);
        //
        Halt(0);
      end;
    end;
    Nivel2 := '0';
    Nivel3 := '0';
    Nivel4 := '0';
    Nivel5 := '0';
    //
    Nome := TbCadGruDESGRU.Value;
    case Itens of
      1:
      begin
        Nivel6 := '0';
        Nivel5 := ArrNiveis[0];
        CodUsu := Nivel5;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru5', False, [
        'CodUsu', 'Nome', 'Nivel6'], ['Nivel5'], ['Ativo'], [
        CodUsu, Nome, Nivel6], [Nivel5], [1], True);
      end;
      2:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        CodUsu         := Nivel4;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru4', False, [
        'CodUsu', 'Nome', 'Nivel5'], ['Nivel4'], ['Ativo'], [
        CodUsu, Nome, Nivel5], [Nivel4], [1], True);
      end;
      3:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        Nivel3         := ArrNiveis[2];
        CodUsu         := Nivel3;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru3', False, [
        'CodUsu', 'Nome', 'Nivel5', 'Nivel4'], ['Nivel3'], ['Ativo'], [
        CodUsu, Nome, Nivel5, Nivel4], [Nivel3], [1], True);
      end;
      4:
      begin
        Nivel5         := ArrNiveis[0];
        Nivel4         := ArrNiveis[1];
        Nivel3         := ArrNiveis[2];
        Nivel2         := ArrNiveis[3];
        CodUsu         := Nivel2;
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragru2', False, [
        'CodUsu', 'Nome', 'Nivel5', 'Nivel4', 'Nivel3'], [
        'Nivel2'], ['Ativo'], [
        CodUsu, Nome, Nivel5, Nivel4, Nivel3], [
        Nivel2], [1], True);
      end;
      else Geral.MensagemBox('Item de grupo de patrim�nio n�o importado: ' +
      #13#10 + TbCadGruCODGRU.Value + #13#10 + Nome,
      'Aviso', MB_OK+MB_ICONERROR);
    end;
    //
    TbCadGru.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadNat();
var
  Nome, Nome2, Debito: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  ReabreTabela(TbCadNat);
  TbCadNat.First;
  while not TbCadNat.Eof do
  begin
    Codigo         := TbCadNatCODNAT.Value;
    Nome           := TbCadNatDESNAT.Value;
    Nome2          := Nome;
    Debito         := 'V';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Codigo ID',
    'FROM contas ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    if QrExiste.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Nome);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'contas', False, [
    'Nome', 'Nome2', 'Debito'], [
    'Codigo'], [
    Nome, Nome2, Debito], [
    Codigo], True);
    //
    TbCadNat.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadPat();
  function MarcaCadastrada(Texto: String): Integer;
  const
    Codigo = 0;
  var
    Nome: String;
    Controle: Integer;
  begin
    if Trim(Texto) <> '' then
    begin
      Nome := Texto;
      if Nome = 'DINAPAC' then
        Nome := 'DYNAPAC'
      else
      if Nome = 'ERBELE' then
        Nome := 'EBERLE'
      else
      if Nome = 'JOWA/WACKE' then
        Nome := 'JOWA'
      else
      if Nome = 'KOLBACK' then
        Nome := 'KOLBACH'
      else
      if Nome = 'ROBIN' then
        Nome := 'ROBBIN'
      else
      if Nome = 'VIBROMAK' then
        Nome := 'VIBROMACK'
      else
      if Nome = 'WEGG' then
        Nome := 'WEG'
      ;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrGraFabMar, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM grafabmar ',
      'WHERE Nome="' + Nome + '"',
      '']);
      //
      Result := QrGraFabMarControle.Value;
      if Result = 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY_Def('grafabmar', 'Controle', stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'grafabmar', False, [
        'Codigo', 'Nome'], [
        'Controle'], [
        Codigo, Nome], [
        Controle], True) then
          Result := Controle;
      end;
    end else Result := 0;
  end;
var
  Complem, AquisData, AquisDocu, Modelo, Serie, Voltagem, Potencia,
  Capacid, VendaData, VendaDocu, Observa, AGRPAT, CLVPAT: String;
  GraGruX, Nivel1, Situacao, Agrupador, VendaEnti: Integer;
  AquisValr, AtualValr, ValorMes, ValorQui, ValorSem, ValorDia, VendaValr: Double;
  Marca: Integer;
  //
  //GraGru1
  Cod, CodsX, Referencia, MARPAT: String;
  ArrNiveis: array of String;
  Nivel2, Nivel3, Nivel4, Nivel5,
  I, Itens, PrdGrupTip, CodUsu: Integer;
begin
  ReabreTabela(TbCadPat);
  SetLength(ArrNiveis, 4);
  TbCadPat.First;
  while not TbCadPat.Eof do
  begin
    Referencia := TbCadPatCODPAT.Value;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Referencia);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Nivel1 ID',
    'FROM gragru1 ',
    'WHERE Referencia=''' + Referencia + '''',
    '']);
    // Caso n�o tenha sido inserido ainda!
    if QrExiste.RecordCount = 0 then
    begin
      for I := 0 to 3 do
        ArrNiveis[I] := '';
      Itens := 0;
      CodsX := TbCadPatGRUPAT.Value;
      while CodsX <> '' do
      begin
        Itens := Itens + 1;
        if Geral.SeparaPrimeiraOcorrenciaDeTexto('.', CodsX, Cod, CodsX) then
          ArrNiveis[Itens-1] := Cod
        else
        begin
          Geral.MensagemBox('Erro fatal na importa��o de grupos de grade!' +
          #13#10 + 'O aplicativo ser� finalizado!', 'ERRO', MB_OK+MB_ICONERROR);
          //
          Halt(0);
        end;
      end;
      Nivel5 := Geral.IMV(ArrNiveis[0]);
      Nivel4 := Geral.IMV(ArrNiveis[1]);
      Nivel3 := Geral.IMV(ArrNiveis[2]);
      Nivel2 := Geral.IMV(ArrNiveis[3]);
      //
      Nivel1 := UMyMod.BuscaEmLivreY_Def('gragru1', 'Nivel1', stIns, 0);
      CodUsu := Nivel1;
      PrdGrupTip := 1;
      GraGruX := Nivel1;
      //
      CadastraGraGru1SemGrade(PrdGrupTip, Nivel5, Nivel4, Nivel3,
      Nivel2, Nivel1, CodUsu, TbCadPatNOMPAT.Value, TbCadPatCODPAT.Value);
      //

      MARPAT         := TbCadPatMARPAT.Value;

      Complem        := TbCadPatCOMPAT.Value;
      AquisData      := Geral.FDT(TbCadPatDATPAT.Value, 1);
      AquisDocu      := TbCadPatDOCPAT.Value;
      AquisValr      := TbCadPatVLRPAT.Value;
      Situacao       := Dmod.CodigoDeSitPat(TbCadPatSITPAT.Value);
      AtualValr      := TbCadPatVLEPAT.Value;
      ValorMes       := TbCadPatVLMPAT.Value;
      ValorQui       := TbCadPatVLQPAT.Value;
      ValorSem       := TbCadPatVLSPAT.Value;
      ValorDia       := TbCadPatVLDPAT.Value;
      Agrupador      := 0; // Fazer depois!
      Marca          := MarcaCadastrada(MARPAT);
      Modelo         := TbCadPatMODPAT.Value;
      Serie          := TbCadPatSERPAT.Value;
      Voltagem       := TbCadPatENEPAT.Value;
      Potencia       := TbCadPatPOTPAT.Value;
      Capacid        := ''; // N�o tem
      VendaData      := Geral.FDT(TbCadPatDTVPAT.Value, 1);
      VendaDocu      := TbCadPatNFVPAT.Value;
      VendaValr      := TbCadPatVLVPAT.Value;
      VendaEnti      := DModG.ObtemCodigoDeEntidadePeloAntigo('C' + TbCadPatCLVPAT.Value);  // Cliente!
      Observa        := TbCadPatMOMPAT.Value;
      AGRPAT         := TbCadPatAGRPAT.Value;
      CLVPAT         := TbCadPatCLVPAT.Value;

      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'gragxpatr', False, [
      'GraGruX', 'Complem', 'AquisData',
      'AquisDocu', 'AquisValr', 'Situacao',
      'AtualValr', 'ValorMes', 'ValorQui',
      'ValorSem', 'ValorDia', 'Agrupador',
      'Marca', 'Modelo', 'Serie',
      'Voltagem', 'Potencia', 'Capacid',
      'VendaData', 'VendaDocu', 'VendaValr',
      'VendaEnti', 'Observa',
      'AGRPAT', 'CLVPAT', 'MARPAT'], [
      ], [
      GraGruX, Complem, AquisData,
      AquisDocu, AquisValr, Situacao,
      AtualValr, ValorMes, ValorQui,
      ValorSem, ValorDia, Agrupador,
      Marca, Modelo, Serie,
      Voltagem, Potencia, Capacid,
      VendaData, VendaDocu, VendaValr,
      VendaEnti, Observa,
      AGRPAT, CLVPAT, MARPAT], [
      ], True);
    end;
    //
    TbCadPat.Next;
  end;
  TbCadPat.First;
  PB1.Position := 0;
  QrGraGXPatr.Close;
  UMyMod.AbreQuery(QrGraGXPatr, Dmod.MyDB);
  while not QrGraGXPatr.Eof do
  begin
    GraGruX := QrGraGXPatrGraGRuX.Value;
    Referencia := QrGraGXPatrAGRPAT.Value;
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Geral.FF0(GraGruX));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Nivel1 ID',
    'FROM gragru1 ',
    'WHERE Referencia=''' + Referencia + '''',
    '']);
    // N�o testei Ainda!
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM gragrux ',
    'WHERE GraGru1=' + Geral.FF0(QrExiste.FieldByName('ID').AsInteger),
    '']);
    Agrupador := QrExiste.FieldByName('ID').AsInteger;
    //
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragxpatr', False, [
    'Agrupador'], ['GraGruX'], [Agrupador], [GraGruX], True);
    //
    QrGraGXPatr.Next;
  end;

end;

procedure TFmDB_Converte_AAPA.ImportaTabela_CadVde();
var
  //Senha, DataSenha, SenhaDia, IP_Default,
  Login: String;
  //Perfil, Funcionario, SitSenha,
  Numero: Integer;
begin
  ReabreTabela(TbCadVde);
  TbCadVde.First;
  while not TbCadVde.Eof do
  begin
    Login := TbCadVdeNOMVDE.Value;
    Numero := Trunc(TbCadVdeCODVDE.Value);
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Login);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'senhas', False, [
    'Numero'], ['Login'], [Numero], [Login], True);
    //
    TbCadVde.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_PagDup();
var
  Fornecedor: Integer;
begin
  ReabreTabela(TbPagDup);
  TbPagDup.First;
  while not TbPagDup.Eof do
  begin
    Fornecedor := DModG.ObtemCodigoDeEntidadePeloAntigo('F' +
      FormatFloat('00000', Geral.IMV(TbPagDupFORPAG.Value)));
    //
    //
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbPagDupDTEPAG.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbPagDupDTVPAG.Value, 1);
    FLAN_DataCad       := Geral.FDT(date, 1);
    FLAN_Mez           := 0;
    FLAN_Descricao     := TbPagDupOBSPAG.Value;
    FLAN_Compensado    := Geral.FDT(TbPagDupDTPPAG.Value, 1);
    FLAN_Duplicata     := TbPagDupTITPAG.Value;
    FLAN_Doc2          := TbPagDupNOCPAG.Value;
    FLAN_SerieCH       := Geral.SoLetra_TT(TbPagDupNCHPAG.Value);

    FLAN_Documento     := Geral.IMV('0' + Geral.SoNumero_TT(TbPagDupNCHPAG.Value));
    FLAN_Tipo          := 2;
    FLAN_Credito       := 0;
    FLAN_Debito        := TbPagDupVLRPAG.Value;
    FLAN_Genero        := TbPagDupNATPAG.Value;
    FLAN_SerieNF       := Geral.SoLetra_TT(TbPagDupNNFPAG.Value);
    FLAN_Antigo        := TbPagDupCODPAG.Value;
    FLAN_NotaFiscal    := Geral.IMV('0' + Geral.SoNumero_TT(TbPagDupNNFPAG.Value));
    if TbPagDupDTPPAG.Value > 2 then
    begin
      FLAN_Sit           := 3;
      FLAN_Carteira      := 2; // Banco
    end else
    begin
      FLAN_Sit           := 0;
      FLAN_Carteira      := 3; // Duplicatas
    end;
    FLAN_Controle      := 0;
    FLAN_Sub           := 0;
    FLAN_ID_Pgto       := 0;
    FLAN_Cartao        := 0;
    FLAN_Linha         := 0;
    FLAN_Fornecedor    := Fornecedor;
    FLAN_Cliente       := 0;
    FLAN_MoraDia       := 0;
    FLAN_Multa         := 0;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(Date, 1);
    FLAN_Vendedor      := 0;
    FLAN_Account       := 0;
    FLAN_ICMS_P        := 0;
    FLAN_ICMS_V        := 0;
    FLAN_CliInt        := -11;
    FLAN_Depto         := 0;
    FLAN_DescoPor      := 0;
    FLAN_ForneceI      := 0;
    FLAN_DescoVal      := 0;
    FLAN_NFVal         := 0;
    FLAN_Unidade       := 0;
    FLAN_Qtde          := 0;
    FLAN_FatID         := 0;
    FLAN_FatID_Sub     := 0;
    FLAN_FatNum        := 0;
    FLAN_FatParcela    := 0;
    FLAN_FatParcRef    := 0;
    FLAN_FatGrupo      := 0;
    //
    FLAN_MultaVal      := 0;
    FLAN_TaxasVal      := 0;
    FLAN_MoraVal       := TbPagDupJURPAG.Value;
    FLAN_CtrlIni       := 0;
    FLAN_Nivel         := 0;
    FLAN_CNAB_Sit      := 0;
    FLAN_TipoCH        := 0;
    FLAN_Atrelado      := 0;
    FLAN_SubPgto1      := 0;
    FLAN_MultiPgto     := 0;
    FLAN_EventosCad    := 0;
    FLAN_IndiPag       := 0;
    FLAN_FisicoSrc     := 0;
    FLAN_FisicoCod     := 0;
    //
    FLAN_Emitente      := '';
    FLAN_CNPJCPF       := '';
    FLAN_Banco         := 0;
    FLAN_Agencia       := 0;
    FLAN_ContaCorrente := '';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Geral.FF0(TbPagDup.RecNo));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM ' + FTabLctA,
    'WHERE Data=''' + FLAN_Data + '''',
    'AND Vencimento=''' + FLAN_Vencimento + '''',
    'AND Duplicata=''' + Geral.Substitui(dmkPF.DuplicaBarras(FLAN_Duplicata), '''', ' ') + '''',
    'AND Fornecedor=' + Geral.FF0(FLAN_Fornecedor),
    'AND Debito=' + Geral.FFT_Dot(FLAN_Debito, 2, siNegativo),
    '']);
    if QrExiste.RecordCount = 0 then
    begin
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
    //
    TbPagDup.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.ImportaTabela_RecDup();
var
  Cliente: Integer;
begin
  ReabreTabela(TbRecDup);
  TbRecDup.First;
  while not TbRecDup.Eof do
  begin
    Cliente := DModG.ObtemCodigoDeEntidadePeloAntigo('C' +
      FormatFloat('00000', Geral.IMV(TbRecDupCLIREC.Value)));
    //
    //
    UFinanceiro.LancamentoDefaultVARS;
    //
    FLAN_Data          := Geral.FDT(TbRecDupDTEREC.Value, 1);
    FLAN_Vencimento    := Geral.FDT(TbRecDupDTVREC.Value, 1);
    FLAN_DataCad       := Geral.FDT(date, 1);
    FLAN_Mez           := 0;
    FLAN_Descricao     := TbRecDupOBSREC.Value;
    FLAN_Compensado    := Geral.FDT(TbRecDupDTPREC.Value, 1);
    FLAN_Duplicata     := TbRecDupTITREC.Value;
    FLAN_Doc2          := TbRecDupNRPREC.Value;
    FLAN_SerieCH       := '';

    FLAN_Documento     := 0;//Geral.IMV('0' + Geral.SoNumero_TT(TbRecDupNCHREC.Value));
    FLAN_Tipo          := 2;
    //FLAN_Carteira      := TbRecDupBCOREC.Value;
    FLAN_Credito       := TbRecDupVLRREC.Value;
    FLAN_Debito        := 0;
    FLAN_Genero        := 100;
    FLAN_SerieNF       := Geral.SoLetra_TT(TbRecDupNNFREC.Value);
    FLAN_Antigo        := TbRecDupCODREC.Value;
    FLAN_NotaFiscal    := Geral.IMV('0' + Geral.SoNumero_TT(TbRecDupNNFREC.Value));
    if TbRecDupDTPREC.Value > 2 then
    begin
      FLAN_Sit           := 3;
      FLAN_Carteira      := 2; // Banco
    end else
    begin
      FLAN_Sit           := 0;
      FLAN_Carteira      := 4; // Faturas
    end;
    FLAN_Controle      := 0;
    FLAN_Sub           := 0;
    FLAN_ID_Pgto       := 0;
    FLAN_Cartao        := 0;
    FLAN_Linha         := 0;
    FLAN_Fornecedor    := 0;
    FLAN_Cliente       := Cliente;
    FLAN_MoraDia       := 0;
    FLAN_Multa         := 0;
    FLAN_UserCad       := VAR_USUARIO;
    FLAN_DataDoc       := Geral.FDT(Date, 1);
    FLAN_Vendedor      := 0;
    FLAN_Account       := 0;
    FLAN_ICMS_P        := 0;
    FLAN_ICMS_V        := 0;
    FLAN_CliInt        := -11;
    FLAN_Depto         := 0;
    FLAN_DescoPor      := 0;
    FLAN_ForneceI      := 0;
    FLAN_DescoVal      := 0;
    FLAN_NFVal         := 0;
    FLAN_Unidade       := 0;
    FLAN_Qtde          := 0;
    FLAN_FatID         := 0;
    FLAN_FatID_Sub     := 0;
    FLAN_FatNum        := 0;
    FLAN_FatParcela    := 0;
    FLAN_FatParcRef    := 0;
    FLAN_FatGrupo      := 0;
    //
    FLAN_MultaVal      := 0;
    FLAN_TaxasVal      := 0;
    FLAN_MoraVal       := TbRecDupJURREC.Value;
    FLAN_CtrlIni       := 0;
    FLAN_Nivel         := 0;
    FLAN_CNAB_Sit      := 0;
    FLAN_TipoCH        := 0;
    FLAN_Atrelado      := 0;
    FLAN_SubPgto1      := 0;
    FLAN_MultiPgto     := 0;
    FLAN_EventosCad    := 0;
    FLAN_IndiPag       := 0;
    FLAN_FisicoSrc     := 0;
    FLAN_FisicoCod     := 0;
    //
    FLAN_Emitente      := '';
    FLAN_CNPJCPF       := '';
    FLAN_Banco         := 0;
    FLAN_Agencia       := 0;
    FLAN_ContaCorrente := '';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, FTabStr + Geral.FF0(TbRecDup.RecNo));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
    'SELECT Controle ID',
    'FROM ' + FTabLctA,
    'WHERE Data=''' + FLAN_Data + '''',
    'AND Vencimento=''' + FLAN_Vencimento + '''',
    'AND Duplicata=''' + Geral.Substitui(dmkPF.DuplicaBarras(FLAN_Duplicata), '''', ' ') + '''',
    'AND Cliente=' + Geral.FF0(FLAN_Cliente),
    'AND Credito=' + Geral.FFT_Dot(FLAN_Credito, 2, siNegativo),
    '']);
    if QrExiste.RecordCount = 0 then
    begin
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', FTabLctA, LAN_CTOS, 'Controle');
      //
      UFinanceiro.InsereLancamento(FTabLctA);
    end;
    //
    TbRecDup.Next;
  end;
end;

procedure TFmDB_Converte_AAPA.IncluiBanco(Codigo, Ordem: Integer;
  Nome, Bco, Age: String);
var
  ContaCor, ContaTip, DAC_A, DAC_C, DAC_AC, Observ: String;
  Controle, Banco, Agencia: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entictas', 'Controle', stIns, 0);
  Banco          := 0;
  Agencia        := 0;
  ContaCor       := '';
  ContaTip       := '';
  DAC_A          := '';
  DAC_C          := '';
  DAC_AC         := '';
  Observ         := Nome + ' ' + Bco + ' ' + Age;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entictas', False, [
  'Codigo', 'Ordem', 'Banco',
  'Agencia', 'ContaCor', 'ContaTip',
  'DAC_A', 'DAC_C', 'DAC_AC',
  'Observ'], [
  'Controle'], [
  Codigo, Ordem, Banco,
  Agencia, ContaCor, ContaTip,
  DAC_A, DAC_C, DAC_AC,
  Observ], [
  Controle], True);
end;

procedure TFmDB_Converte_AAPA.IncluiConsulta(Codigo, Orgao: Integer;
  Numero: String; _Data: TDateTime; _Hora, _Resultado: String);
var
  Data, Hora, Resultado: String;
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entisrvpro', 'Controle', stIns, 0);
  Data           := Geral.FDT(_Data, 1);
  Hora           := Geral.SoHora_TT(_Hora, pt2pt60);
  Resultado      := _Resultado + ' ' + _Hora;
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entisrvpro', False, [
  'Codigo', 'Orgao', 'Data',
  'Hora', 'Numero', 'Resultado'], [
  'Controle'], [
  Codigo, Orgao, Data,
  Hora, Numero, Resultado], [
  Controle], True);
end;

procedure TFmDB_Converte_AAPA.IncluiContato(Codigo, Cargo: Integer;
  Nome, Tel, Cel, Fax, Mail: String);
const
  DiarioAdd = 0;
var
  Controle: Integer;
  Telefone, EMail: String;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticontat', False, [
  'Codigo', 'Nome', 'Cargo',
  'DiarioAdd'], [
  'Controle'], [
  Codigo, Nome, Cargo,
  DiarioAdd], [
  Controle], True) then
  begin
    Telefone := Geral.SoNumero_TT(Tel);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneFix, Telefone);
    //
    Telefone := Geral.SoNumero_TT(Cel);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneCel, Telefone);
    //
    Telefone := Geral.SoNumero_TT(Fax);
    if Telefone <> '' then
      IncluiEntiTel(Codigo, Controle, FFoneFax, Telefone);
    //
    ////////////////////////////////////////////////////////////////////////////
    ///
    EMail := Lowercase(Trim(Mail));
    if EMail <> '' then
      IncluiEntiMail(Codigo, Controle, FEMail, EMail);
    //
  end;
end;

procedure TFmDB_Converte_AAPA.IncluiEntiMail(Codigo, Controle,
  EntiTipCto: Integer; EMail: String);
const
  Ordem = 0;
  DiarioAdd = 0;
var
  Conta: Integer;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entimail', False, [
  'Codigo', 'Controle', 'EMail',
  'EntiTipCto', 'Ordem', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, EMail,
  EntiTipCto, Ordem, DiarioAdd], [
  Conta], True);
end;

procedure TFmDB_Converte_AAPA.IncluiEntiTel(Codigo, Controle, EntiTipCto: Integer;
  Tel: String);
const
  Ramal = '';
  DiarioAdd = 0;
var
  Conta: Integer;
  Telefone: String;
begin
  Conta := UMyMod.BuscaEmLivreY_Def('entitel', 'Conta', stIns, 0);
  Telefone := Tel;
  if Copy(Telefone, 1, 2) = '00' then
    Telefone := Copy(Telefone, 2);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entitel', False, [
  'Codigo', 'Controle', 'Telefone',
  'EntiTipCto', 'Ramal', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, Telefone,
  EntiTipCto, Ramal, DiarioAdd], [
  Conta], True);
end;

procedure TFmDB_Converte_AAPA.IncluiSocio(Codigo, CargoSocio: Integer;
  Nome, _RG, _CPF: String; Natal: TDateTime);
var
  Observ, MandatoIni, MandatoFim, CPF, RG, SSP, DataRG, DtaNatal: String;
  Controle, Cargo, (*Assina,*) OrdemLista: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('entirespon', 'Controle', stIns, 0);
  //Nome           := ;
  Cargo          := CargoSocio;
  //Assina         := ;
  OrdemLista     := Controle;
  Observ         := Nome + _RG + _CPF;
  MandatoIni     := '0000-00-00';
  MandatoFim     := '0000-00-00';
  CPF            := Geral.SoNumero_TT(_CPF);
  RG             := Geral.SoNumero_TT(_RG);
  SSP            := Geral.SoLetra_TT(_RG);
  DataRG         := '';
  DtaNatal       := Geral.FDT(Natal, 1);

  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entirespon', False, [
  'Codigo', 'Nome', 'Cargo',
  (*'Assina',*) 'OrdemLista', 'Observ',
  'MandatoIni', 'MandatoFim', 'CPF',
  'RG', 'SSP', 'DataRG',
  'DtaNatal'], [
  'Controle'], [
  Codigo, Nome, Cargo,
  (*Assina,*) OrdemLista, Observ,
  MandatoIni, MandatoFim, CPF,
  RG, SSP, DataRG,
  DtaNatal], [
  Controle], True);
end;

procedure TFmDB_Converte_AAPA.MeSQL1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    BtExecutaClick(Sender);
end;

function TFmDB_Converte_AAPA.ObtemGraGruXDeCODMATX(CODMATX: String): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
  'SELECT ggx.Controle ID',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON ggx.GraGru1=gg1.Nivel1 ',
  'WHERE gg1.Referencia=''' + CODMATX + '''',
  '']);
  //
  Result := QrExiste.FieldByName('ID').AsInteger;
end;

procedure TFmDB_Converte_AAPA.QrPsqAfterOpen(DataSet: TDataSet);
begin
  EdRegistrosQr.ValueVariant := QrPsq.RecordCount;
end;

procedure TFmDB_Converte_AAPA.QrPsqBeforeClose(DataSet: TDataSet);
begin
  EdRegistrosQr.ValueVariant := 0;
end;

end.
