ALTERA��ES DO SISTEMA - 13/11/2020

[+] 1. Verificar se no cadastro ao fazer a imagem da WebCam ele permite alterar  o nome do arquivo da foto.
    >> Implementado item de menu para altera��o da descri��o do item.

[N] 2. Mostrar pre�o de custo na consulta de mercadoria (loja).
    >> J� existe uma grade onde mostra o pre�o m�dio e o pre�o da �ltima compra.

[N] 3. Na loca��o no campo de observa��o que n�o sai na impress�o deixar escrever mesmo com o status locado.
    >> Duplo clique no campo abrir� uma janela de edi��o.

[N] 4. N�o sair na impress�o da observa��o o nome do funcion�rio.
    >> Nome do funcion�rio foi importado na Observa��o Curta 3 que � um campo imprim�vel.

[N] 5. Verificar se � possivel incluir na venda e na loca��o o n�mero da NF vinculada.
    >> �, mas � moroso. Mas o sistema informa se j� existe e qual quando vai tentar emitir.

[+] 6. Como pesquisar o cadastro pelo CPF/CNPJ.
    >> Implementado junto com o Nome/Raz�o Social

[+] 7. No contrato de teste 27966 ao mudar o status para locado ele deu sa�da do estoque.
       por�m ao mudar de locado para aberto ele n�o retornou o estoque. E depois ao mudar
       para locado de novo ele n�o fez a movimenta��o, Teste + 2 x.
    >> Implementado muda�a de estoque ao mudar estatus aberto.

[+] 8. Verificar o que � a coluna dad data com nome de baixa na aba de pesquisa da loca��o.
       (Ano retroativo)
    >> Compatibilidade com vers�o anterior. Data de devolu��o de equipamento sem controle
    de Estoque por quantidade maior que um.

[-] 9. Na aba de devolu��o da loca��o precisa corrigir o nome "Testado na delu��o"
    >> Ok

[N] 10. Na aba de devou��o permitir escolher data e hora de recebimento d� m�quina.
    >> J� tem bot�o ao lado da hora.

[+] 11. Verificar a possibilidade de n�o abrir uma janela para cada item a ser devolvido
        da loca��o.
    >> Implementado item de menu para devolver todos equipamentos e finalizar loca��o.

[^] 12. ... Implementado no 11.
[^] 13. ... Implementado no 11.
[^] 14. ... Implementado no 7.

[+] 15. Erro na pesquisa de loca��o ao pesquisar somente o patrimnio Ex.: 333 C.27965.
    >> Implementado consulta por patrim�nio e Refer�ncia.

[^] 16. ... Implementado no 7.

