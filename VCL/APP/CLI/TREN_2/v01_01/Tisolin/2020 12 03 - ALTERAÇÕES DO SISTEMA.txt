﻿ALTERAÇÕES DO SISTEMA - 03/12/2020

[+] 1. No Contrato 27969 que foi aberto e colocado locado, o mesmo não fez a movimentação de estoque dos produtos.
    >> Não foram informadas as quantidades. Implementado impedimento de confirmação se não informada a quantidade.

[+] 2. [+] Colocar nossa logo no comprovante de devolução dos equipamentos na horizontal,
       [N] e daí pode retirar o nosso nome no cabeçalho pois já tem na logo
       [+] e aumentar a fonte como no xerox
       [-] retirar da impressão número de páginas.
[N] 3. Arrumar o nome do nosso endereço no cupom fiscal (Avenida Doutor Alexandre Rasgulaeff)
       >> O endereço é corrigido no cadastro da entidade -11 em tempo de execução.

[+] 4. Na venda
       [+] aumentar a fonta da impressão, nem que for necessário mudar o logo para horizontal
       [+] colocar no cabeçalho os nosso dados
       [+] em seguida o número da venda com a data e hora da venda
       [+] Observação: manter também a data e hora de impressão
       [+] Se possível colocar na impressão o numero da NF do cliente quando emitida
       [+] e forma de pagamento
       [+] e valor de desconto se houver.

[+] 5. Orçamento
       [+] Colocar logo no orçamento
       [+] aumentar a fonte
       [+] colocar a data do orçamento como no Xerox
       [+] colocar informação do preço à vista e à prazo
       [+] Mostrar valor do desconto oferecido

[?] 6. Ao abrir um serviço está dando erro para pesquisar OP seviço que deseja incluir.

[+] 7. Verificar se é possivel cadastrar código de barras das mercadorias.
       >> Implementado no cadastro de mercadoria de venda e na venda de mercadoria.

[+] 8. [-] Na venda 27973 formate ID 35370 ao fazer a venda o estoque movimentou normal.
           Porém ao voltar o status para orçamento o estoque não retorna.
           E depois em orçamento mudando o status para pedido não dá a saída novamente //
       [+] Colocar trava no sistema para após uma venda ter sido realizada como pedido,
           não ser permitido retornar a mesma para orçamento. E se necessário acrescentar
           algum produto, fazer novo movimento de venda.

[ ] 9. [+] Na venda 27973 Formate ID 35371 Não mostra o desconto oferecido pro cliente na
           impressão, dar opção de mostrar o desconto.
           >> Implementado no item 5.

       [N] Nesta mesma venda ao colocar o status para locado não tinha saldo de frete e eu
           cliquei em prosseguir mesmo assim, daí não fez a movimentaçãode nenhum dos
           produtos lançados no pedido.
           >> Frete não tem estoque.

[N] 10. Verificar se tem como colocar os equipamentos secundário e acessório vinclulados
        ao equipamento principal ao imprimir (tanto no contrato como no comprovante de devolução)
        Ex.: Acessorio de martelo, sair junto com o martelete. C27973
        >> Criar um Movimento para cada principal que tem secundários e acéssorios.

[i] 11. Verificar onde fica o histórico do que já foi devolvido no contrato de locação e com qual
        data e hora.
        >> Clicar botão contrário do mause nas grades
        >> ou clicar no botão imprime item Devoluções

[+] 12. Trava no sistema p/ após mudar o status do atendimento p/ locado não alterar a data e hora.
        de saída.

[X] 13. Calcular a locação se baseando na data de saída e não pelo status.

[+] 14. Estoque:
        [+] Considerar a saída do patrimonio locado já no pedido do atendimento aberto
        [+] Considerar a saída da mercadoria do estoque já no pedido do atendimento aberto

=================== P E R G U N T A S ==========================================

[ ] Porque existem 56 atendimentos de locações que tem data de fechamento mas
    estão com status = 2 (LOCADO)?
    Podemos alterar o status para locado via código (SQL)?
SELECT *
FROM LOCACAO
WHERE STATUS=2
AND FECHAMENTODATA > "1900-01-01"

[ ]


