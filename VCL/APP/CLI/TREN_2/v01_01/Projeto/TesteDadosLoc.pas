unit TesteDadosLoc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, dmkEdit, Vcl.ComCtrls,
  dmkEditDateTimePicker, Data.DB, mySQLDbTables, Vcl.DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask,
  UnAppEnums, dmkCheckGroup, Soap.EncdDecd, UnGrl_Vars;
type
  TFmTesteDadosLoc = class(TForm)
    QrItsLca: TMySQLQuery;
    QrItsLcaCodigo: TIntegerField;
    QrItsLcaCtrID: TIntegerField;
    QrItsLcaItem: TIntegerField;
    QrItsLcaManejoLca: TSmallintField;
    QrItsLcaGraGruX: TIntegerField;
    QrItsLcaValorDia: TFloatField;
    QrItsLcaValorSem: TFloatField;
    QrItsLcaValorQui: TFloatField;
    QrItsLcaValorMes: TFloatField;
    QrItsLcaRetFunci: TIntegerField;
    QrItsLcaRetExUsr: TIntegerField;
    QrItsLcaDtHrLocado: TDateTimeField;
    QrItsLcaDtHrRetorn: TDateTimeField;
    QrItsLcaLibExUsr: TIntegerField;
    QrItsLcaLibFunci: TIntegerField;
    QrItsLcaLibDtHr: TDateTimeField;
    QrItsLcaRELIB: TWideStringField;
    QrItsLcaHOLIB: TWideStringField;
    QrItsLcaQtdeProduto: TIntegerField;
    QrItsLcaValorProduto: TFloatField;
    QrItsLcaQtdeLocacao: TIntegerField;
    QrItsLcaValorLocacao: TFloatField;
    QrItsLcaQtdeDevolucao: TIntegerField;
    QrItsLcaTroca: TSmallintField;
    QrItsLcaCategoria: TWideStringField;
    QrItsLcaCOBRANCALOCACAO: TWideStringField;
    QrItsLcaCobrancaConsumo: TFloatField;
    QrItsLcaCobrancaRealizadaVenda: TWideStringField;
    QrItsLcaCobranIniDtHr: TDateTimeField;
    QrItsLcaSaiDtHr: TDateTimeField;
    QrItsLcaDMenos: TIntegerField;
    QrItsLcaValorFDS: TFloatField;
    QrGraGXPatr: TMySQLQuery;
    QrGraGXPatrEstqSdo: TFloatField;
    QrGraGXPatrControle: TIntegerField;
    QrGraGXPatrReferencia: TWideStringField;
    QrGraGXPatrCOD_GG1: TIntegerField;
    QrGraGXPatrNO_GG1: TWideStringField;
    QrGraGXPatrCPL_EXISTE: TFloatField;
    QrGraGXPatrSituacao: TWordField;
    QrGraGXPatrAtualValr: TFloatField;
    QrGraGXPatrValorMes: TFloatField;
    QrGraGXPatrValorQui: TFloatField;
    QrGraGXPatrValorSem: TFloatField;
    QrGraGXPatrValorDia: TFloatField;
    QrGraGXPatrAgrupador: TIntegerField;
    QrGraGXPatrMarca: TIntegerField;
    QrGraGXPatrModelo: TWideStringField;
    QrGraGXPatrSerie: TWideStringField;
    QrGraGXPatrVoltagem: TWideStringField;
    QrGraGXPatrPotencia: TWideStringField;
    QrGraGXPatrCapacid: TWideStringField;
    QrGraGXPatrTXT_MES: TWideStringField;
    QrGraGXPatrTXT_QUI: TWideStringField;
    QrGraGXPatrTXT_SEM: TWideStringField;
    QrGraGXPatrTXT_DIA: TWideStringField;
    QrGraGXPatrNO_SIT: TWideStringField;
    QrGraGXPatrNO_SITAPL: TIntegerField;
    QrGraGXPatrValorFDS: TFloatField;
    QrGraGXPatrDMenos: TIntegerField;
    DsGraGXPatr: TDataSource;
    Panel1: TPanel;
    Label6: TLabel;
    EdCtrID: TdmkEdit;
    Label2: TLabel;
    EdGraGruX: TdmkEditCB;
    EdReferencia: TdmkEdit;
    Label3: TLabel;
    Label7: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    Label21: TLabel;
    TPSaida: TdmkEditDateTimePicker;
    EdSaida: TdmkEdit;
    TPVolta: TdmkEditDateTimePicker;
    Label1: TLabel;
    EdVolta: TdmkEdit;
    BitBtn1: TBitBtn;
    Memo1: TMemo;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Panel2: TPanel;
    Texto: TBitBtn;
    CGCodHist: TdmkCheckGroup;
    BitBtn2: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    Memo2: TMemo;
    QrItsLcaOriSubstSaiDH: TDateTimeField;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPSaidaChange(Sender: TObject);
    procedure TPVoltaChange(Sender: TObject);
    procedure TPVoltaClick(Sender: TObject);
    procedure TPSaidaClick(Sender: TObject);
    procedure TextoClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    Criado: Boolean;
    procedure WriteIntegerToStream(Stream: TStream; Value: Integer);
    procedure WriteStringToStream(Stream: TStream; const Value: String);
    function  ReadStringFromStream(Stream: TStream): String;
    function  ReadIntegerFromStream(Stream: TStream): Integer;
  public
    { Public declarations }
  end;

var
  FmTesteDadosLoc: TFmTesteDadosLoc;

implementation

uses
UnAppPF, Module, UMySQLModule, DmkDAC_PF, AppListas, dmkGeral;

{$R *.dfm}

const
  TempFile = 'C:\Dermatek\Temp\CertTmp.pfx';
  Path_Para_o_PFX     = 'C:\_Sincro\_MLArend\Clientes\Tisolin\Compras XML\BACKUP_CERT_TISOLIN LOCACOES_SENHA 22121962.pfx';
  Password_para_o_PFX = '22121962';

procedure TFmTesteDadosLoc.BitBtn1Click(Sender: TObject);
const
  GeraLog = True;
  OriSubstSaiDH = 0;
var
  Saida, Volta, DtHrDevolver: TDateTime;
  ValorDia, ValorSem, ValorQui, ValorMes, ValorFDS, ValorLocAAdiantar,
  QtdeLocacao, QtdeProduto, QtdeDevolucao, ValorUnitario, ValDevolParci: Double;
  Log, CalcAdiLOCACAO: String;
  IniCobranca, HrTolerancia: TDateTime;
  DMenos, DiasFDS, DiasUteis: Integer;
  Calculou: Boolean;
  Troca: Byte;
begin
  Screen.Cursor := crHourGlass;
  try
    Saida    := Trunc(TPSaida.Date) + EdSaida.ValueVariant;
    Volta    := Trunc(TPVolta.Date) + EdVolta.ValueVariant;
    DtHrDevolver := Volta;
    //
    DMenos   := QrGraGXPatrDMenos.Value;
    //
    ValorDia := QrGraGXPatrValorDia.Value;
    ValorSem := QrGraGXPatrValorSem.Value;
    ValorQui := QrGraGXPatrValorQui.Value;
    ValorMes := QrGraGXPatrValorMes.Value;
    ValorFDS := QrGraGXPatrValorFDS.Value;
    QtdeLocacao   := 1;
    QtdeProduto   := 3;
    QtdeDevolucao := 1;
    ValDevolParci := 0;
    Troca         := 0;  //False
    HrTolerancia  := 0;

    //
    // For�ar defini��o da data inicial de cobran�a, pois ainda n�o foi definida!
    IniCobranca := 0;
    //
    AppPF.CalculaDadosEntreDatasLocacao_COM_CobrarExtraFDS((*const*) GeraLog,
    (*const*) Saida, Volta, HrTolerancia, OriSubstSaiDH, (*const*) DMenos, (*cons*)ValorDia, ValorSem,
    ValorQui, ValorMes, ValorFDS, (*var*) QtdeLocacao, QtdeProduto, QtdeDevolucao,
    ValDevolParci, Troca, DtHrDevolver, Log, (*var*) IniCobranca, (*var*)DiasFDS,
    DiasUteis, (*var*)CalcAdiLOCACAO, ValorUnitario, ValorLocAAdiantar, Calculou);
    //
    Memo1.Text := Log;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTesteDadosLoc.BitBtn2Click(Sender: TObject);
  function String2Hex(const Buffer: AnsiString): string;
  begin
    SetLength(Result, Length(Buffer) * 2);
    BinToHex(PAnsiChar(Buffer), PChar(Result), Length(Buffer));
  end;
begin
  Edit2.Text := String2Hex(Edit1.Text);
end;

procedure TFmTesteDadosLoc.FormCreate(Sender: TObject);
var
  Filtro: String;
begin
  Criado := False;
  //
  TPSaida.Date := Trunc(Date);
  EdSaida.Text := FormatDateTime('hh:nn', Now());
  TPVolta.Date := Trunc(Date + 1);
  EdVolta.Text := EdSaida.Text;
  Dmod.FiltroGrade(gbsLocar, Filtro);
  Filtro := Filtro + ' AND (gg1.Nivel1=ggx.GraGru1)';
  //
  if Dmod.QrOpcoesTRenGraNivOutr.Value = 7 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
    'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
    'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,  ',
    'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
    'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
    'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
    'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
    'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
    'cpl.ValorFDS, cpl.DMenos ',
    '    ',
    'FROM gragrux ggx   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
    'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
    'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
    'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
    'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle ',
    'LEFT JOIN gragruY    ggy ON ggy.Codigo=ggx.GraGruY ',
    'WHERE ggy.Codigo=' + Geral.FF0(CO_GraGruY_1024_GXPatPri),
    'AND cpl.Aplicacao <> 0 ',
    'AND ggx.Ativo = 1',
    ' ',
    'ORDER BY NO_GG1  ',
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrGraGXPatr, Dmod.MyDB, [
    'SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL,  ',
    'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1,  ',
    'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXISTE,   ',
    'cpl.Situacao, cpl.AtualValr, cpl.ValorMes,  ',
    'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia,  ',
    'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie,  ',
    'cpl.Voltagem, cpl.Potencia, cpl.Capacid, ',
    'cpl.ValorFDS, cpl.DMenos ',
    '    ',
    'FROM gragrux ggx   ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
    'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle   ',
    'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle   ',
    'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo   ',
    'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao ',
    'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle ',
    'WHERE ' + Filtro,
    'AND cpl.Aplicacao <> 0 ',
    ' ',
    'ORDER BY NO_GG1  ',
    '']);
  end;
  Criado := True;
end;

function TFmTesteDadosLoc.ReadIntegerFromStream(Stream: TStream): Integer;
begin
  Stream.ReadBuffer(Result, SizeOf(Integer));
end;

function TFmTesteDadosLoc.ReadStringFromStream(Stream: TStream): String;
var
  Count: Integer;
  U: UTF8String;
begin
  Count := ReadIntegerFromStream(Stream);
  if Count > 0 then
  begin
    SetLength(U, Count);
    Stream.ReadBuffer(PAnsiChar(U)^, Count * SizeOf(AnsiChar));
    Result := String(U); // or UTF8Decode(U) prior to D2009
  end else
    Result := '';
end;

procedure TFmTesteDadosLoc.TextoClick(Sender: TObject);
var
  CodHist: Integer;
  TxtHist: String;
begin
  CodHist := CGCodHist.Value;
  //
  if CodHist <> 0 then
  begin
    TxtHist := Trim(AppPF.TextoDeCodHist_Sigla(CodHist));
    if TxtHist <> EmptyStr then
    begin
      Geral.MB_Info(TxtHist);
    end;
  end;
end;

procedure TFmTesteDadosLoc.TPSaidaChange(Sender: TObject);
begin
  if Criado then
    BitBtn1Click(Self);
end;

procedure TFmTesteDadosLoc.TPSaidaClick(Sender: TObject);
begin
  if Criado then
    BitBtn1Click(Self);
end;

procedure TFmTesteDadosLoc.TPVoltaChange(Sender: TObject);
begin
  if Criado then
    BitBtn1Click(Self);
end;

procedure TFmTesteDadosLoc.TPVoltaClick(Sender: TObject);
begin
  if Criado then
    BitBtn1Click(Self);
end;

procedure TFmTesteDadosLoc.WriteIntegerToStream(Stream: TStream;
  Value: Integer);
begin
  Stream.WriteBuffer(Value, SizeOf(Integer));
end;

procedure TFmTesteDadosLoc.WriteStringToStream(Stream: TStream;
  const Value: String);
var
  U: UTF8String;
  Count: Integer;
begin
  U := UTF8String(Value); // or UTF8Encode(Value) prior to D2009
  Count := Length(U);
  WriteIntegerToStream(Stream, Count);
  if Count > 0 then
    Stream.WriteBuffer(PAnsiChar(U)^, Count * SizeOf(AnsiChar));
end;

(*
object QrGraGXPatrQtdeProduto: TIntegerField
  FieldName = 'QtdeProduto'
  Required = True
end
object QrGraGXPatrQtdeLocacao: TIntegerField
  FieldName = 'QtdeLocacao'
  Required = True
end
*)

end.
