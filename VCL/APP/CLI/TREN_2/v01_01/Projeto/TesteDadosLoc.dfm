object FmTesteDadosLoc: TFmTesteDadosLoc
  Left = 0
  Top = 0
  Caption = 'FmTesteDadosLoc'
  ClientHeight = 653
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 129
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 40
      Top = 40
      Width = 53
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID do item:'
    end
    object Label2: TLabel
      Left = 98
      Top = 40
      Width = 48
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Reduzido:'
    end
    object Label3: TLabel
      Left = 156
      Top = 40
      Width = 56
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Refer'#234'ncia:'
      Enabled = False
    end
    object Label7: TLabel
      Left = 281
      Top = 40
      Width = 50
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
    end
    object Label21: TLabel
      Left = 40
      Top = 83
      Width = 87
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora saida:'
    end
    object Label1: TLabel
      Left = 184
      Top = 83
      Width = 112
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data / hora Devolu'#231#227'o:'
    end
    object Label4: TLabel
      Left = 344
      Top = 84
      Width = 28
      Height = 13
      Caption = '$ Mes'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Left = 420
      Top = 84
      Width = 25
      Height = 13
      Caption = '$ Qui'
      FocusControl = DBEdit2
    end
    object Label8: TLabel
      Left = 496
      Top = 84
      Width = 29
      Height = 13
      Caption = '$ Sem'
      FocusControl = DBEdit3
    end
    object Label9: TLabel
      Left = 572
      Top = 84
      Width = 24
      Height = 13
      Caption = '$ Dia'
      FocusControl = DBEdit4
    end
    object Label10: TLabel
      Left = 648
      Top = 84
      Width = 28
      Height = 13
      Caption = '$ FDS'
      FocusControl = DBEdit5
    end
    object Label11: TLabel
      Left = 724
      Top = 84
      Width = 38
      Height = 13
      Caption = 'DMenos'
      FocusControl = DBEdit6
    end
    object EdCtrID: TdmkEdit
      Left = 40
      Top = 55
      Width = 56
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdGraGruX: TdmkEditCB
      Left = 98
      Top = 55
      Width = 56
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdReferencia: TdmkEdit
      Left = 156
      Top = 55
      Width = 122
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Enabled = False
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 281
      Top = 55
      Width = 484
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Controle'
      ListField = 'NO_GG1'
      ListSource = DsGraGXPatr
      TabOrder = 3
      dmkEditCB = EdGraGruX
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object TPSaida: TdmkEditDateTimePicker
      Left = 40
      Top = 99
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.000000000000000000
      Time = 0.724786689817847200
      TabOrder = 4
      OnClick = TPSaidaClick
      OnChange = TPSaidaChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdSaida: TdmkEdit
      Left = 140
      Top = 99
      Width = 41
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 5
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object TPVolta: TdmkEditDateTimePicker
      Left = 184
      Top = 99
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 41131.000000000000000000
      Time = 0.724786689817847200
      TabOrder = 6
      OnClick = TPVoltaClick
      OnChange = TPVoltaChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
      DatePurpose = dmkdpNone
    end
    object EdVolta: TdmkEdit
      Left = 284
      Top = 99
      Width = 41
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 7
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfLong
      HoraFormat = dmkhfShort
      Texto = '00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object BitBtn1: TBitBtn
      Left = 832
      Top = 64
      Width = 149
      Height = 41
      Caption = 'Calcula'
      TabOrder = 8
      OnClick = BitBtn1Click
    end
    object DBEdit1: TDBEdit
      Left = 344
      Top = 100
      Width = 72
      Height = 21
      DataField = 'ValorMes'
      DataSource = DsGraGXPatr
      TabOrder = 9
    end
    object DBEdit2: TDBEdit
      Left = 420
      Top = 100
      Width = 72
      Height = 21
      DataField = 'ValorQui'
      DataSource = DsGraGXPatr
      TabOrder = 10
    end
    object DBEdit3: TDBEdit
      Left = 496
      Top = 100
      Width = 72
      Height = 21
      DataField = 'ValorSem'
      DataSource = DsGraGXPatr
      TabOrder = 11
    end
    object DBEdit4: TDBEdit
      Left = 572
      Top = 100
      Width = 72
      Height = 21
      DataField = 'ValorDia'
      DataSource = DsGraGXPatr
      TabOrder = 12
    end
    object DBEdit6: TDBEdit
      Left = 724
      Top = 100
      Width = 41
      Height = 21
      DataField = 'DMenos'
      DataSource = DsGraGXPatr
      TabOrder = 13
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 129
    Width = 672
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Lucida Console'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    WantReturns = False
    WordWrap = False
  end
  object DBEdit5: TDBEdit
    Left = 648
    Top = 100
    Width = 72
    Height = 21
    DataField = 'ValorFDS'
    DataSource = DsGraGXPatr
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 672
    Top = 129
    Width = 336
    Height = 435
    Align = alRight
    TabOrder = 3
    object Texto: TBitBtn
      Left = 4
      Top = 172
      Width = 75
      Height = 25
      Caption = 'Texto'
      TabOrder = 0
      OnClick = TextoClick
    end
    object CGCodHist: TdmkCheckGroup
      Left = 1
      Top = 211
      Width = 334
      Height = 223
      Align = alBottom
      Caption = ' Hist'#243'rico para financeiro e recibo: '
      Columns = 2
      Items.Strings = (
        'Adiantamento de loca'#231#227'o'
        'Valor Parcial de loca'#231#227'o'
        'Renova'#231#227'o de loca'#231#227'o'
        'Quita'#231#227'o de loca'#231#227'o'
        'Frete'
        'Venda de Mercadoria'
        'Servi'#231'os'
        'Outros')
      TabOrder = 1
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object BitBtn2: TBitBtn
      Left = 4
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Str to Hex'
      TabOrder = 2
      OnClick = BitBtn2Click
    end
    object Edit1: TEdit
      Left = 4
      Top = 36
      Width = 321
      Height = 21
      TabOrder = 3
      Text = 'yzGYhUx1/XYYzksWB+fPR3Qc50c='
    end
    object Edit2: TEdit
      Left = 4
      Top = 60
      Width = 321
      Height = 21
      TabOrder = 4
    end
  end
  object Memo2: TMemo
    Left = 0
    Top = 564
    Width = 1008
    Height = 89
    Align = alBottom
    Lines.Strings = (
      'Memo2')
    TabOrder = 4
    WantReturns = False
    WordWrap = False
  end
  object QrItsLca: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM loccitslca'
      'WHERE Codigo>0')
    Left = 432
    Top = 271
    object QrItsLcaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItsLcaCtrID: TIntegerField
      FieldName = 'CtrID'
      Required = True
    end
    object QrItsLcaItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrItsLcaManejoLca: TSmallintField
      FieldName = 'ManejoLca'
      Required = True
    end
    object QrItsLcaGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItsLcaValorDia: TFloatField
      FieldName = 'ValorDia'
      Required = True
    end
    object QrItsLcaValorSem: TFloatField
      FieldName = 'ValorSem'
      Required = True
    end
    object QrItsLcaValorQui: TFloatField
      FieldName = 'ValorQui'
      Required = True
    end
    object QrItsLcaValorMes: TFloatField
      FieldName = 'ValorMes'
      Required = True
    end
    object QrItsLcaRetFunci: TIntegerField
      FieldName = 'RetFunci'
      Required = True
    end
    object QrItsLcaRetExUsr: TIntegerField
      FieldName = 'RetExUsr'
      Required = True
    end
    object QrItsLcaDtHrLocado: TDateTimeField
      FieldName = 'DtHrLocado'
      Required = True
    end
    object QrItsLcaDtHrRetorn: TDateTimeField
      FieldName = 'DtHrRetorn'
      Required = True
    end
    object QrItsLcaLibExUsr: TIntegerField
      FieldName = 'LibExUsr'
      Required = True
    end
    object QrItsLcaLibFunci: TIntegerField
      FieldName = 'LibFunci'
      Required = True
    end
    object QrItsLcaLibDtHr: TDateTimeField
      FieldName = 'LibDtHr'
      Required = True
    end
    object QrItsLcaRELIB: TWideStringField
      FieldName = 'RELIB'
    end
    object QrItsLcaHOLIB: TWideStringField
      FieldName = 'HOLIB'
      Size = 5
    end
    object QrItsLcaQtdeProduto: TIntegerField
      FieldName = 'QtdeProduto'
      Required = True
    end
    object QrItsLcaValorProduto: TFloatField
      FieldName = 'ValorProduto'
      Required = True
    end
    object QrItsLcaQtdeLocacao: TIntegerField
      FieldName = 'QtdeLocacao'
      Required = True
    end
    object QrItsLcaValorLocacao: TFloatField
      FieldName = 'ValorLocacao'
      Required = True
    end
    object QrItsLcaQtdeDevolucao: TIntegerField
      FieldName = 'QtdeDevolucao'
      Required = True
    end
    object QrItsLcaTroca: TSmallintField
      FieldName = 'Troca'
      Required = True
    end
    object QrItsLcaCategoria: TWideStringField
      FieldName = 'Categoria'
      Required = True
      Size = 1
    end
    object QrItsLcaCOBRANCALOCACAO: TWideStringField
      FieldName = 'COBRANCALOCACAO'
      Size = 40
    end
    object QrItsLcaCobrancaConsumo: TFloatField
      FieldName = 'CobrancaConsumo'
      Required = True
    end
    object QrItsLcaCobrancaRealizadaVenda: TWideStringField
      FieldName = 'CobrancaRealizadaVenda'
      Required = True
      Size = 1
    end
    object QrItsLcaCobranIniDtHr: TDateTimeField
      FieldName = 'CobranIniDtHr'
      Required = True
    end
    object QrItsLcaSaiDtHr: TDateTimeField
      FieldName = 'SaiDtHr'
      Required = True
    end
    object QrItsLcaDMenos: TIntegerField
      FieldName = 'DMenos'
    end
    object QrItsLcaValorFDS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorFDS'
      Calculated = True
    end
    object QrItsLcaOriSubstSaiDH: TDateTimeField
      FieldName = 'OriSubstSaiDH'
    end
  end
  object QrGraGXPatr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ' SELECT gep.EstqSdo, ggs.Nome NO_SIT, ggs.Aplicacao NO_SITAPL, '
      'ggx.Controle, gg1.Referencia,  gg1.Nivel1 COD_GG1, '
      
        'gg1.Nome NO_GG1,  IF(cpl.GraGruX IS NULL, 0, 1) + 0.000 CPL_EXIS' +
        'TE,  '
      'cpl.Situacao, cpl.AtualValr, cpl.ValorMes, '
      'cpl.ValorQui, cpl.ValorSem, cpl.ValorDia, '
      'cpl.Agrupador, cpl.Marca, cpl.Modelo, cpl.Serie, '
      'cpl.Voltagem, cpl.Potencia, cpl.Capacid, '
      'cpl.ValorFDS, cpl.DMenos'
      '   '
      'FROM gragrux ggx  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN gragxpatr  cpl ON cpl.GraGruX=ggx.Controle  '
      'LEFT JOIN grafabmar  gfm ON cpl.Marca=gfm.Controle  '
      'LEFT JOIN grafabcad  gfc ON gfm.Codigo=gfc.Codigo  '
      'LEFT JOIN graglsitu  ggs ON ggs.Codigo=cpl.Situacao'
      'LEFT JOIN gragruEPat gep ON gep.GraGruX=ggx.Controle'
      'WHERE  gg1.PrdGrupTip=1 AND (gg1.Nivel1=ggx.GraGru1) '
      'AND cpl.Aplicacao <> 0'
      ''
      'ORDER BY NO_GG1 '
      '')
    Left = 340
    Top = 336
    object QrGraGXPatrEstqSdo: TFloatField
      FieldName = 'EstqSdo'
    end
    object QrGraGXPatrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGXPatrReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 25
    end
    object QrGraGXPatrCOD_GG1: TIntegerField
      FieldName = 'COD_GG1'
    end
    object QrGraGXPatrNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Size = 120
    end
    object QrGraGXPatrCPL_EXISTE: TFloatField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraGXPatrSituacao: TWordField
      FieldName = 'Situacao'
    end
    object QrGraGXPatrAtualValr: TFloatField
      FieldName = 'AtualValr'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorMes: TFloatField
      FieldName = 'ValorMes'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorQui: TFloatField
      FieldName = 'ValorQui'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorSem: TFloatField
      FieldName = 'ValorSem'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrValorDia: TFloatField
      FieldName = 'ValorDia'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrAgrupador: TIntegerField
      FieldName = 'Agrupador'
    end
    object QrGraGXPatrMarca: TIntegerField
      FieldName = 'Marca'
    end
    object QrGraGXPatrModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 60
    end
    object QrGraGXPatrSerie: TWideStringField
      FieldName = 'Serie'
      Size = 60
    end
    object QrGraGXPatrVoltagem: TWideStringField
      FieldName = 'Voltagem'
      Size = 30
    end
    object QrGraGXPatrPotencia: TWideStringField
      FieldName = 'Potencia'
      Size = 30
    end
    object QrGraGXPatrCapacid: TWideStringField
      FieldName = 'Capacid'
      Size = 30
    end
    object QrGraGXPatrTXT_MES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MES'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_QUI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_QUI'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_SEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_SEM'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrTXT_DIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DIA'
      Size = 50
      Calculated = True
    end
    object QrGraGXPatrNO_SIT: TWideStringField
      FieldName = 'NO_SIT'
      Size = 30
    end
    object QrGraGXPatrNO_SITAPL: TIntegerField
      FieldName = 'NO_SITAPL'
    end
    object QrGraGXPatrValorFDS: TFloatField
      FieldName = 'ValorFDS'
      DisplayFormat = '#,###,###,#00.00'
    end
    object QrGraGXPatrDMenos: TIntegerField
      FieldName = 'DMenos'
    end
  end
  object DsGraGXPatr: TDataSource
    DataSet = QrGraGXPatr
    Left = 340
    Top = 384
  end
end
