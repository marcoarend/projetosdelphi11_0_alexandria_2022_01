CADCLI
================================================================================
CODCLI:  CODCLI         C�digo do Cliente
RAZCLI:  RAZCLI         Raz�o Social CLiente
FANCLI:  FANCLI         Nome Fantasia Cliente
RAMCLI:  RAMCLI         C�digo do ramo de atividade do cliente (Tabela CADRAM)
ENDCLI:  ENDCLI         Endere�o do cliente
BAICLI:  BAICLI         Bairro do cliente
CIDCLI:  CIDCLI         Cidade do cliente
ESTCLI:  ESTCLI         UF do cliente
CEPCLI:  CEPCLI         CEP do cliente
CGCCLI:  CGCCLI         CNPJ do cliente
INSCLI:  INSCLI         IE do cliente
CPFCLI:  CPFCLI         CPF Cliente
RGECLI:  RGECLI         RG Cliente
REGCLI:  REGCLI       0 ------- n�o usa --------
TE1CLI:  TE1CLI         Telefone 1 cliente
TE2CLI:  TE2CLI         Telefone 2 cliente
FAXCLI:  FAXCLI         Fax Cliente
COMCLI:  COMCLI         Comprador cliente
DTNCLI:  DTNCLI         Data nascimento (cliente? Comprador? 2011/2012?)
FATCLI:  FATCLI      48 0=6, 5=5, S=5, C=5, N=4, 2=4, 3=3, R=3, M=3, .=2, ]=2, 8=2, O=1, A=1, E=1, D=1
ECOCLI:  ECOCLI    1384 Endere�o de cobran�a
BCOCLI:  BCOCLI         Bairro de cobran�a
CIOCLI:  CIOCLI         Cidade da cobran�a
ESOCLI:  ESOCLI         Estado de cobran�a
CEOCLI:  CEOCLI         CEP de cobran�a
SO1CLI:  SO1CLI         S�cio 1
SO2CLI:  SO2CLI         S�cio 2
RG1CLI:  RG1CLI         RG S�cio 1
RG2CLI:  RG2CLI         RG S�cio 2
CP1CLI:  CP1CLI         CPF S�cio 1
CP2CLI:  CP2CLI         CPF S�cio 2
DN1CLI:  DN1CLI         Dta nasc. S�cio 1
DN2CLI:  DN2CLI         Dta nasc. S�cio 2
CL1CLI:  CL1CLI         Principal cliente 1
CL2CLI:  CL2CLI         Principal cliente 2
CF1CLI:  CF1CLI         Telefone Principal cliente 1
CF2CLI:  CF2CLI         Telefone Principal cliente 2
FO1CLI:  FO1CLI         Principal Fornecedor 1
FO2CLI:  FO2CLI         Principal Fornecedor 2
FF1CLI:  FF1CLI         Telefone Principal Fornecedor 1
FF2CLI:  FF2CLI         Telefone Principal Fornecedor 2
BC1CLI:  BC1CLI         \                          Nome banco (+ C/C ?)
NB1CLI:  NB1CLI          | Dados banc�rios Banco 1 N�mero do banco
NA1CLI:  NA1CLI         /                          N�mero da ag�ncia
BC2CLI:  BC2CLI         \                          Nome banco (+ C/C ?)
NB2CLI:  NB2CLI          | Dados banc�rios banco 2 N�mero do banco
NA2CLI:  NA2CLI         /                          N�mero da ag�ncia
FILCLI:  FILCLI         Filia��o do cliente
SPCCLI:  SPCCLI         SPC: Resultado da consulta > "NADA CONSTA"=2372, + 350 tipos de textos diferentes
DSPCLI:  DSPCLI         SPC: Data da consulta
HORCLI:  HORCLI    2748 SPC: Hora da consulta > ("OK"=2491)("N/C"=33)("NC" = 25)("NAO"=18)(10.00=10)(9.00=8)(17.00=6) etc.
NRCCLI:  NRCCLI         SPC: N�mero consulta SPC
EMACLI:  EMACLI         Email do cliente
LIMCLI:  LIMCLI       1 Limite de cr�dito > S� 1 um com 100.000 restante zerado!
DTUCLI:  DTUCLI         Data da �ltima compra
LIBCLI:  LIBCLI         Liberado ou n�o ? (S=3464, N=315, B=6, M=4, ]=3, \=1, A=1, D=1, J=1)
OBSCLI:  OBSCLI         Observa��es em TMemo (Usa?)
TOTCOM:  TOTCOM         Total compra ? (Tudo zerado?)


