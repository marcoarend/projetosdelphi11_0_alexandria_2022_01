unit Academy_MLA;

interface

uses
  Forms, Db, (*DBTables,*) Menus, Controls, Classes, ExtCtrls, UnInternalConsts,
  Messages, Dialogs, Windows, SysUtils, Graphics, StdCtrls, Mask, DBCtrls,
  UnInternalConsts2, jpeg, mySQLDbTables, UnGOTOy, UnMyLinguas, Buttons,
  ComCtrls, dmkGeral, dmkEdit;

type
  TFmAcademy_MLA = class(TForm)
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ProgressBar1: TProgressBar;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtInstall: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    EdLogin: TEdit;
    EdSenha: TEdit;
    EdEmpresa: TEdit;
    LaSenhas: TLabel;
    LaConexao: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure BtInstallClick(Sender: TObject);
    procedure EdSenhaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure LaConexaoClick(Sender: TObject);
    procedure LaConexaoMouseEnter(Sender: TObject);
    procedure LaConexaoMouseLeave(Sender: TObject);
    procedure LaSenhasClick(Sender: TObject);
    procedure LaSenhasMouseEnter(Sender: TObject);
    procedure LaSenhasMouseLeave(Sender: TObject);

  private
    { Private declarations }
    procedure ControleMudou(Sender: TObject);
    procedure BtInstallVisible;

  public
    { Public declarations }
    SkinConta: Integer;
    procedure DefineCores;
    procedure ReloadSkin;

  end;

var
  FmAcademy_MLA: TFmAcademy_MLA;

implementation

uses UnMyObjects, UMySQLModule, Principal, Module, InstallMySQL41, ModuleFin,
  VerificaConexoes, MyListas;

{$R *.DFM}

procedure TFmAcademy_MLA.ControleMudou(Sender: TObject);
begin
  if APP_LIBERADO then
  begin
    MyObjects.ControleCor(Self);
    //MLAGeral.SistemaMetrico;
    //MLAGeral.CustoSincronizado;
  end;
end;

procedure TFmAcademy_MLA.FormCreate(Sender: TObject);
var
  WinPath : Array[0..144] of Char;
  Cam: String;
begin
  Cam := Application.Title+'\Opcoes';
  //
  //MLAGeral.VCLSkinDefineCores(QrAparencias);
  VAR_CAMINHOTXTBMP := 'C:\Dermatek\Skins\VCLSkin\mxskin33.skn';
  MyLinguas.GetInternationalStrings(myliPortuguesBR);
  TMeuDB := 'Academy';
  TLocDB := 'LocAcade';
  VAR_SQLUSER := 'root';
  Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', TMeuDB, ktString);
  Geral.WriteAppKeyCU('CashLoc_Database', 'Dermatek', TLocDB, ktString);
  Geral.WriteAppKeyCU('Cashier_DmkID_APP', 'Dermatek', CO_DMKID_APP, ktInteger);
  //
  DefineCores;

  UMyMod.DefineDatabase(TMeuDB, Application.ExeName);

  Screen.OnActiveControlChange := ControleMudou;
  IC2_LOpcoesTeclaEnter := Geral.ReadAppKeyCU('TeclaEnter', Application.Title, ktInteger,-1);
  if IC2_LOpcoesTeclaEnter = -1 then
  begin
    Geral.WriteAppKeyCU('TeclaEnter', Application.Title, 13, ktInteger);
    IC2_LOpcoesTeclaEnter := 13;
  end;
  IC2_LOpcoesTeclaEnter := Geral.ReadAppKeyCU('TeclaEnter', Application.Title, ktInteger,-1);
  if IC2_LOpcoesTeclaEnter = -1 then
  begin
    Geral.WriteAppKeyCU('TeclaEnter', Application.Title, 13, ktInteger);
    IC2_LOpcoesTeclaEnter := 13;
  end;
  IC2_LOpcoesTeclaTab := Geral.ReadAppKeyCU('TeclaTAB', Application.Title, ktInteger,-1);
  if IC2_LOpcoesTeclaTab = -1 then
  begin
    Geral.WriteAppKeyCU('TeclaTAB', Application.Title, 9, ktInteger);
    IC2_LOpcoesTeclaTab := 9;
  end;
  GetWindowsDirectory(WinPath,144);
  ReloadSkin;
  IC2_COMPANT := nil;
  VAR_BD := 0;
end;

procedure TFmAcademy_MLA.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmAcademy_MLA.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
{
    GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa: TEdit; FmMLA, FmMain:
  TForm; Componente: TComponent; ForcaSenha: Boolean; LaAviso1, LaAviso2: TLabel;
  QrGeraSenha, DmodFinQrCarts: TmySQLQuery);
}
    GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmAcademy_MLA,
    FmPrincipal, BtConfirma, False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);

  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmAcademy_MLA.EdLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
    GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmAcademy_MLA,
    FmPrincipal, BtConfirma, False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);

  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmAcademy_MLA.DefineCores;
//var
  //Aparencia: Integer;
begin
  IC2_AparenciasTituloPainelItem   := clBtnFace;
  IC2_AparenciasTituloPainelTexto  := clWindowText;
  IC2_AparenciasDadosPainel        := clBtnFace;
  IC2_AparenciasDadosTexto         := clWindowText;
  IC2_AparenciasGradeAItem         := clWindow;
  IC2_AparenciasGradeATitulo       := clWindowText;
  IC2_AparenciasGradeATexto        := clWindowText;
  IC2_AparenciasGradeAGrade        := clBtnFace;
  IC2_AparenciasGradeBItem         := clWindow;
  IC2_AparenciasGradeBTitulo       := clWindowText;
  IC2_AparenciasGradeBTexto        := clWindowText;
  IC2_AparenciasGradeBGrade        := clBtnFace;
  IC2_AparenciasEditAItem          := clWindow;
  IC2_AparenciasEditATexto         := clWindowText;
  IC2_AparenciasEditBItem          := clBtnFace;
  IC2_AparenciasEditBTexto         := clWindowText;
  IC2_AparenciasConfirmaPainelItem := clBtnFace;
  IC2_AparenciasControlePainelItem := clBtnFace;
  IC2_AparenciasFormPesquisa       := clBtnFace;
  IC2_AparenciasLabelPesquisa      := clWindowText;
  IC2_AparenciasLabelDigite        := clWindowText;
  IC2_AparenciasEditPesquisaItem   := clWindow;
  IC2_AparenciasEditPesquisaTexto  := clWindowText;
  IC2_AparenciasEditItemIn         := $00FF8000;
  IC2_AparenciasEditItemOut        := clWindow;
  IC2_AparenciasEditTextIn         := $00D5FAFF;
  IC2_AparenciasEditTextOut        := clWindowText;
(*  if Geral.ReadAppKey('Versao', Application.Title, ktInteger, 0) <> 0 then
  begin
    Aparencia := Geral.IMV(Geral.ReadAppKey('Aparencia',
    Application.Title, ktString, '-1000'));
    if Aparencia = -1000 then
       Geral.WriteAppKey('Aparencia', Application.Title, '0', ktString,
    HKEY_LOCAL_MACHINE);
    QrAparencias.Close;
    QrAparencias.Params[0].AsInteger := Aparencia;
    QrAparencias.Open;
    if GOTOy.Registros(QrAparencias)>0 then
    begin
      IC2_AparenciasTituloPainelItem := QrAparenciasTituloPainelItem.Value;
      IC2_AparenciasTituloPainelTexto := QrAparenciasTituloPainelTexto.Value;
      IC2_AparenciasDadosPainel := QrAparenciasDadosPainel.Value;
      IC2_AparenciasDadosTexto := QrAparenciasDadosTexto.Value;
      IC2_AparenciasGradeAItem := QrAparenciasGradeAItem.Value;
      IC2_AparenciasGradeATitulo := QrAparenciasGradeATitulo.Value;
      IC2_AparenciasGradeATexto := QrAparenciasGradeATexto.Value;
      IC2_AparenciasGradeAGrade := QrAparenciasGradeAGrade.Value;
      IC2_AparenciasGradeBItem := QrAparenciasGradeBItem.Value;
      IC2_AparenciasGradeBTitulo := QrAparenciasGradeBTitulo.Value;
      IC2_AparenciasGradeBTexto := QrAparenciasGradeBTexto.Value;
      IC2_AparenciasGradeBGrade := QrAparenciasGradeBGrade.Value;
      IC2_AparenciasEditAItem := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditATexto := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditBItem := QrAparenciasEditBItem.Value;
      IC2_AparenciasEditBTexto := QrAparenciasEditBTexto.Value;
      IC2_AparenciasConfirmaPainelItem := QrAparenciasConfirmaPainelItem.Value;
      IC2_AparenciasControlePainelItem := QrAparenciasControlePainelItem.Value;
      IC2_AparenciasFormPesquisa := QrAparenciasFormPesquisa.Value;
      IC2_AparenciasLabelPesquisa := QrAparenciasLabelPesquisa.Value;
      IC2_AparenciasLabelDigite := QrAparenciasLabelDigite.Value;
      IC2_AparenciasEditPesquisaItem := QrAparenciasEditPesquisaItem.Value;
      IC2_AparenciasEditPesquisaTexto := QrAparenciasEditPesquisaTexto.Value;
      IC2_AparenciasEditItemIn         := $00D9FFFF;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := $00A00000;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemIn         := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
    QrAparencias.Close;
  end;*)
end;

procedure TFmAcademy_MLA.ReloadSkin;
begin
end;

procedure TFmAcademy_MLA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FmPrincipal.Close;
end;

procedure TFmAcademy_MLA.BtConfirmaClick(Sender: TObject);
begin
  GOTOy.AnaliseSenhaEmpresa(EdLogin, EdSenha, EdEmpresa, FmAcademy_MLA,
  FmPrincipal, BtConfirma, False(*ForcaSenha*), LaAviso1, LaAviso2, nil, nil);
end;

procedure TFmAcademy_MLA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAcademy_MLA.FormHide(Sender: TObject);
begin
  if VAR_TERMINATE = False then
  begin
    FmPrincipal.BringToFront;
    VAR_SHOW_MASTER_POPUP := True;
    try
      FmPrincipal.AcoesIniciaisDoAplicativo();
      //FmPrincipal.AtualizaAtivosEVencidos;
    except
      //
    end;
  end;
end;

procedure TFmAcademy_MLA.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAcademy_MLA.BtDesisteClick(Sender: TObject);
begin
  VAR_TERMINATE := True;
  EdLogin.Font.Color := clWhite;
  Close;
end;

procedure TFmAcademy_MLA.BtInstallClick(Sender: TObject);
begin
  Application.CreateForm(TFmInstallMySQL41,FmInstallMySQL41);
  FmInstallMySQL41.FSenha := EdSenha.Text;
  FmInstallMySQL41.FLogin := EdLogin.Text;
  FmInstallMySQL41.ShowModal;
  FmInstallMySQL41.Destroy;
end;

procedure TFmAcademy_MLA.BtInstallVisible;
begin
end;

procedure TFmAcademy_MLA.EdSenhaExit(Sender: TObject);
begin
  BtInstallVisible;
end;

procedure TFmAcademy_MLA.FormShow(Sender: TObject);
begin
  VAR_SHOW_MASTER_POPUP := False;
end;

procedure TFmAcademy_MLA.LaConexaoClick(Sender: TObject);
begin
  Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
end;

procedure TFmAcademy_MLA.LaConexaoMouseEnter(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [fsUnderline];
end;

procedure TFmAcademy_MLA.LaConexaoMouseLeave(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [];
end;

procedure TFmAcademy_MLA.LaSenhasClick(Sender: TObject);
begin
  Geral.AbrirAppAuxiliar(12);
end;

procedure TFmAcademy_MLA.LaSenhasMouseEnter(Sender: TObject);
begin
  LaSenhas.Font.Color := clBlue;
  LaSenhas.Font.Style := [fsUnderline];
end;

procedure TFmAcademy_MLA.LaSenhasMouseLeave(Sender: TObject);
begin
  LaSenhas.Font.Color := clBlue;
  LaSenhas.Font.Style := [];
end;

end.
