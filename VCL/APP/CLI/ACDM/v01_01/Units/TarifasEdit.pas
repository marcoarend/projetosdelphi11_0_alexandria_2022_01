unit TarifasEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, UnMLAGeral, StdCtrls, Buttons, UnInternalConsts,
  ComCtrls, dmkGeral, dmkEdit, dmkEditDateTimePicker, dmkLabel, UnDmkEnums;

type
  TFmTarifasedit = class(TForm)
    Panel2: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    TPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    Panel3: TPanel;
    BtCancela: TBitBtn;
    BtConfirma: TBitBtn;
    Label10: TLabel;
    EdTaxa: TdmkEdit;
    EdPreco: TdmkEdit;
    Label2: TLabel;
    LaTipo: TdmkLabel;
    EdValPer: TdmkEdit;
    Label3: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdTaxaExit(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValPerChange(Sender: TObject);
    procedure EdPrecoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
    FDataAnt: TDateTime;
  end;

var
  FmTarifasedit: TFmTarifasedit;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, Tarifas;

procedure TFmTarifasedit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTarifasedit.FormCreate(Sender: TObject);
begin
  TPData.Date := Int(Date);
end;

procedure TFmTarifasedit.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTarifasedit.BtConfirmaClick(Sender: TObject);
var
  Data: String;
  Preco, Taxa, ValPer: Double;
begin
  Dmod.QrUpd.SQL.Clear;
{
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('INSERT INTO TarifasIts SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE TarifasIts SET ');
  Dmod.QrUpd.SQL.Add('Preco=:P0, Taxa=:P1, Data=:P2, ');
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('UserCad=:Pa, DataCad=:Pb, Codigo=:Pc')
  else
    Dmod.QrUpd.SQL.Add('UserAlt=:Pa, DataAlt=:Pb WHERE Codigo=:Pc AND Data=:Pd');
  Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(EdPreco.Text);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdTaxa.Text);
  Dmod.QrUpd.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Dmod.QrUpd.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[05].AsInteger := FCodigo;
  if LaTipo.Caption = CO_ALTERACAO then
    Dmod.QrUpd.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, FDataAnt);
  Dmod.QrUpd.ExecSQL;
}
  Data   := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Preco  := EdPreco.ValueVariant;
  Taxa   := EdTaxa.ValueVariant;
  ValPer := EdValPer.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'tarifasits', False, [
  'Preco', 'Taxa', 'ValPer'], [
  'Codigo', 'Data'], [
  Preco, Taxa, ValPer], [
  FCodigo, Data], True) then
    Close;
end;

procedure TFmTarifasedit.DBGrid1DblClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTarifasedit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmTarifasedit.EdPrecoChange(Sender: TObject);
begin
  if EdPreco.Focused then
  begin
    if FmTarifas.QrTarifasQtdePer.Value <> 0 then
      EdValPer.ValueVariant :=
        EdPreco.ValueVariant / FmTarifas.QrTarifasQtdePer.Value
    else
      EdValPer.ValueVariant := 0;
  end;
end;

procedure TFmTarifasedit.EdTaxaExit(Sender: TObject);
begin
  EdTaxa.Text := Geral.TFT(EdTaxa.Text, 2, siPositivo);
end;

procedure TFmTarifasedit.EdValPerChange(Sender: TObject);
begin
  if EdValPer.Focused then
    EdPreco.ValueVariant :=
      EdValPer.ValueVariant * FmTarifas.QrTarifasQtdePer.Value;
end;

end.
