object FmTarifas: TFmTarifas
  Left = 368
  Top = 194
  Caption = 'TAR-CADAS-001 :: Cadastro de Tarifa'
  ClientHeight = 503
  ClientWidth = 770
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 770
    Height = 444
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 384
      Width = 768
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 642
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 843
      Height = 168
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
      object Label9: TLabel
        Left = 10
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 84
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 655
        Top = 113
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Qtde:'
      end
      object Label5: TLabel
        Left = 10
        Top = 59
        Width = 104
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupo de Tarifas:'
      end
      object SpeedButton5: TSpeedButton
        Left = 655
        Top = 79
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 10
        Top = 30
        Width = 70
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 84
        Top = 30
        Width = 664
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtiva: TCheckBox
        Left = 689
        Top = 84
        Width = 56
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativa.'
        Checked = True
        State = cbChecked
        TabOrder = 4
      end
      object RGTipoPer: TRadioGroup
        Left = 10
        Top = 108
        Width = 636
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Meses'
          'Semanas'
          'Dias')
        TabOrder = 5
      end
      object EdQtdePer: TdmkEdit
        Left = 655
        Top = 133
        Width = 99
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        MaxLength = 255
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnExit = EdQtdePerExit
      end
      object EdGrupo: TdmkEditCB
        Left = 10
        Top = 79
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGrupo
        IgnoraDBLookupComboBox = False
      end
      object CBGrupo: TdmkDBLookupComboBox
        Left = 79
        Top = 79
        Width = 572
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTarifasGru
        TabOrder = 3
        dmkEditCB = EdGrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 770
    Height = 444
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 384
      Width = 768
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 82
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        ExplicitWidth = 31
        ExplicitHeight = 16
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 295
        Top = 1
        Width = 472
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 345
          Top = 5
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiClick
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 768
      Height = 168
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 10
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 84
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 645
        Top = 108
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Qtde:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 10
        Top = 59
        Width = 104
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupo de Tarifas:'
      end
      object DBEdCodigo: TDBEdit
        Left = 10
        Top = 30
        Width = 70
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTarifas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 84
        Top = 30
        Width = 664
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTarifas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBCheckBox1: TDBCheckBox
        Left = 694
        Top = 84
        Width = 64
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativa.'
        DataField = 'Ativa'
        DataSource = DsTarifas
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 10
        Top = 108
        Width = 631
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Per'#237'odo: '
        Columns = 3
        DataField = 'TipoPer'
        DataSource = DsTarifas
        Items.Strings = (
          'Meses'
          'Semanas'
          'Dias')
        ParentBackground = True
        TabOrder = 3
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBEdit1: TDBEdit
        Left = 645
        Top = 128
        Width = 100
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'QtdePer'
        DataSource = DsTarifas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 10
        Top = 79
        Width = 70
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Grupo'
        DataSource = DsTarifas
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 84
        Top = 79
        Width = 606
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_GRUPO'
        DataSource = DsTarifas
        TabOrder = 6
      end
    end
    object DBGrade: TDBGrid
      Left = 1
      Top = 222
      Width = 843
      Height = 151
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataSource = DsTarifasIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGradeDrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Data'
          Title.Alignment = taCenter
          Title.Caption = 'In'#237'cio vig'#234'ncia'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Taxa'
          Title.Alignment = taRightJustify
          Title.Caption = 'Taxa de matr'#237'cula'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPer'
          Title.Alignment = taRightJustify
          Title.Caption = 'Pre'#231'o per'#237'odo'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Alignment = taRightJustify
          Title.Caption = 'Pre'#231'o plano'
          Width = 110
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 770
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cadastro de Tarifa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 668
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 666
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 390
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitWidth = 388
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsTarifas: TDataSource
    DataSet = QrTarifas
    Left = 488
    Top = 161
  end
  object QrTarifas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTarifasBeforeOpen
    AfterOpen = QrTarifasAfterOpen
    AfterScroll = QrTarifasAfterScroll
    SQL.Strings = (
      'SELECT gru.Nome NO_GRUPO, tar.* '
      'FROM tarifas tar'
      'LEFT JOIN tarifasgru gru ON gru.Codigo=tar.Grupo'
      'WHERE tar.Codigo > 0')
    Left = 460
    Top = 161
    object QrTarifasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTarifasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTarifasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTarifasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTarifasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTarifasCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrTarifasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTarifasAtiva: TSmallintField
      FieldName = 'Ativa'
    end
    object QrTarifasTipoPer: TIntegerField
      FieldName = 'TipoPer'
    end
    object QrTarifasQtdePer: TIntegerField
      FieldName = 'QtdePer'
    end
    object QrTarifasGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTarifasNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Size = 100
    end
  end
  object QrTarifasIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTarifasBeforeOpen
    SQL.Strings = (
      'SELECT * FROM tarifasits'
      'WHERE Codigo=:P0'
      'ORDER BY Data DESC')
    Left = 460
    Top = 189
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTarifasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTarifasItsData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTarifasItsPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTarifasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTarifasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTarifasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTarifasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTarifasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTarifasItsTaxa: TFloatField
      FieldName = 'Taxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTarifasItsValPer: TFloatField
      FieldName = 'ValPer'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsTarifasIts: TDataSource
    DataSet = QrTarifasIts
    Left = 488
    Top = 189
  end
  object PMInclui: TPopupMenu
    Left = 396
    Top = 220
    object NovoPlano1: TMenuItem
      Caption = 'Nova &Tarifa'
      OnClick = NovoPlano1Click
    end
    object NovoPreo1: TMenuItem
      Caption = 'Novo P&re'#231'o'
      OnClick = NovoPreo1Click
    end
  end
  object PMAltera: TPopupMenu
    Left = 424
    Top = 220
    object AlteraPlano1: TMenuItem
      Caption = 'Altera &Plano'
      OnClick = AlteraPlano1Click
    end
    object AlteraPreo1: TMenuItem
      Caption = 'Altera P&re'#231'o'
      OnClick = AlteraPreo1Click
    end
  end
  object PMExclui: TPopupMenu
    Left = 452
    Top = 220
    object Preo1: TMenuItem
      Caption = 'P&re'#231'o'
      OnClick = Preo1Click
    end
  end
  object QrTarifasGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tarifasgru'
      'ORDER BY Nome')
    Left = 152
    Top = 228
    object QrTarifasGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTarifasGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsTarifasGru: TDataSource
    DataSet = QrTarifasGru
    Left = 180
    Top = 228
  end
end
