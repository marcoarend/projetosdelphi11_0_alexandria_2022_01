object FmRetornoBematech: TFmRetornoBematech
  Left = 446
  Top = 214
  Caption = 'Impressora Bematech'
  ClientHeight = 266
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 3
    Top = 4
    Width = 486
    Height = 209
    Caption = ' Retorno da Impressora '
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    object Label2: TLabel
      Left = 152
      Top = 20
      Width = 22
      Height = 13
      Caption = 'ACK'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 208
      Top = 20
      Width = 22
      Height = 13
      Caption = 'NAK'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 40
      Width = 233
      Height = 161
      Caption = ' ST1 '
      Color = clBtnFace
      ParentColor = False
      TabOrder = 0
      object Label4: TLabel
        Left = 16
        Top = 24
        Width = 117
        Height = 13
        Caption = 'BIT 7 - Fim de Papel'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 16
        Top = 40
        Width = 116
        Height = 13
        Caption = 'BIT 6 - Pouco Papel'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 16
        Top = 56
        Width = 132
        Height = 13
        Caption = 'BIT 5 - Erro no Rel'#243'gio'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 16
        Top = 72
        Width = 164
        Height = 13
        Caption = 'BIT 4 - Impressora em ERRO'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 16
        Top = 88
        Width = 199
        Height = 13
        Caption = 'BIT 3 - CMD n'#227'o iniciado com ESC'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 16
        Top = 104
        Width = 162
        Height = 13
        Caption = 'BIT 2 - Comando Inexistente'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 16
        Top = 120
        Width = 123
        Height = 13
        Caption = 'BIT 1 - Cupom Aberto'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 16
        Top = 136
        Width = 198
        Height = 13
        Caption = 'BIT 0 - N'#186' de Par'#226'metros Inv'#225'lidos'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 244
      Top = 40
      Width = 233
      Height = 161
      Caption = ' ST2 '
      TabOrder = 1
      object Label12: TLabel
        Left = 16
        Top = 24
        Width = 197
        Height = 13
        Caption = 'BIT 7 - Tipo de Par'#226'metro Inv'#225'lido'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 16
        Top = 40
        Width = 171
        Height = 13
        Caption = 'BIT 6 - Mem'#243'ria Fiscal Lotada'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 17
        Top = 56
        Width = 143
        Height = 13
        Caption = 'BIT 5 - CMOS n'#227'o Vol'#225'til'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 16
        Top = 72
        Width = 190
        Height = 13
        Caption = 'BIT 4 - Al'#237'quota N'#227'o Programada'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 16
        Top = 88
        Width = 143
        Height = 13
        Caption = 'BIT 3 - Al'#237'quotas lotadas'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 16
        Top = 104
        Width = 191
        Height = 13
        Caption = 'BIT 2 - Cancelamento '#241' Permitido'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 16
        Top = 120
        Width = 189
        Height = 13
        Caption = 'BIT 1 - CGC/IE n'#227'o Programados'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 16
        Top = 136
        Width = 185
        Height = 13
        Caption = 'BIT 0 - Comando n'#227'o Executado'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RadioButton1: TRadioButton
      Left = 136
      Top = 19
      Width = 17
      Height = 17
      Caption = 'RadioButton1'
      TabOrder = 2
    end
    object RadioButton2: TRadioButton
      Left = 192
      Top = 19
      Width = 15
      Height = 17
      Caption = 'RadioButton2'
      TabOrder = 3
    end
  end
  object BtSaida: TBitBtn
    Left = 206
    Top = 218
    Width = 90
    Height = 48
    Cursor = crHandPoint
    Hint = 'Sai da janela atual'
    Caption = '&Sa'#237'da'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
      333333333333337FF3333333333333903333333333333377FF33333333333399
      03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
      99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
      99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
      03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
      33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
      33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
      3333777777333333333333333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = BtSaidaClick
  end
end
