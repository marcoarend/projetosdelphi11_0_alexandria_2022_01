object FmMatriPer: TFmMatriPer
  Left = 422
  Top = 238
  Caption = 'MTR-CADAS-004 :: Per'#237'odo de Atividade'
  ClientHeight = 279
  ClientWidth = 553
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 49
    Width = 553
    Height = 171
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 15
      Top = 54
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 98
      Top = 54
      Width = 134
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o do per'#237'odo:'
    end
    object Label3: TLabel
      Left = 15
      Top = 5
      Width = 57
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Matr'#237'cula:'
    end
    object Label4: TLabel
      Left = 98
      Top = 5
      Width = 74
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Renovac'#227'o:'
    end
    object Label5: TLabel
      Left = 266
      Top = 5
      Width = 60
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Atividade:'
    end
    object Label6: TLabel
      Left = 182
      Top = 5
      Width = 42
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Turma:'
    end
    object Label7: TLabel
      Left = 15
      Top = 108
      Width = 95
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Dia da semana:'
      FocusControl = DBEdit1
    end
    object Label8: TLabel
      Left = 374
      Top = 108
      Width = 34
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'In'#237'cio:'
      FocusControl = DBEdit2
    end
    object Label9: TLabel
      Left = 458
      Top = 108
      Width = 32
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Final:'
      FocusControl = DBEdit3
    end
    object SpeedButton1: TSpeedButton
      Left = 512
      Top = 74
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object CBPeriodo: TdmkDBLookupComboBox
      Left = 98
      Top = 74
      Width = 414
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Conta'
      ListField = 'Nome'
      ListSource = DsPeriodos
      TabOrder = 1
      dmkEditCB = EdPeriodo
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdPeriodo: TdmkEditCB
      Left = 15
      Top = 74
      Width = 80
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPeriodo
      IgnoraDBLookupComboBox = False
    end
    object EdCodigo: TdmkEdit
      Left = 15
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 98
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdConta: TdmkEdit
      Left = 182
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdItem: TdmkEdit
      Left = 266
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object DBEdit1: TDBEdit
      Left = 15
      Top = 128
      Width = 355
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'NOMEDSEM'
      DataSource = DsPeriodos
      ReadOnly = True
      TabOrder = 6
    end
    object DBEdit2: TDBEdit
      Left = 374
      Top = 128
      Width = 79
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'HIni'
      DataSource = DsPeriodos
      ReadOnly = True
      TabOrder = 7
    end
    object DBEdit3: TDBEdit
      Left = 458
      Top = 128
      Width = 79
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'HFim'
      DataSource = DsPeriodos
      ReadOnly = True
      TabOrder = 8
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 220
    Width = 553
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 17
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 426
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 553
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Per'#237'odo de Atividade'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 450
      Top = 1
      Width = 101
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 449
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object DsPeriodos: TDataSource
    DataSet = QrPeriodos
    Left = 236
    Top = 58
  end
  object QrPeriodos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tt.Conta, pe.Nome, pe.DSem, '
      'pe.HIni, pe.HFim, CASE 1'
      'WHEN pe.DSem=0 THEN "Domingo"'
      'WHEN pe.DSem=1 THEN "Segunda-feira"'
      'WHEN pe.DSem=2 THEN "Ter'#231'a-feira"'
      'WHEN pe.DSem=3 THEN "Quarta-feira"'
      'WHEN pe.DSem=4 THEN "Quinta-feira"'
      'WHEN pe.DSem=5 THEN "Sexta-feira"'
      'WHEN pe.DSem=6 THEN "S'#225'bado" END NOMEDSEM'
      'FROM turmastmp tt'
      'LEFT JOIN periodos pe ON pe.Codigo=tt.Periodo'
      'WHERE tt.Codigo=:P0'
      'AND tt.Controle=:P1'
      'ORDER BY Nome, DSem, HIni, HFim')
    Left = 208
    Top = 58
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPeriodosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPeriodosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPeriodosDSem: TIntegerField
      FieldName = 'DSem'
    end
    object QrPeriodosHIni: TTimeField
      FieldName = 'HIni'
    end
    object QrPeriodosHFim: TTimeField
      FieldName = 'HFim'
    end
    object QrPeriodosNOMEDSEM: TWideStringField
      FieldName = 'NOMEDSEM'
      Size = 13
    end
  end
end
