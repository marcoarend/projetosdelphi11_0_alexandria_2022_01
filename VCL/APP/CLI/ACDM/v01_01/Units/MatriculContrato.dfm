object FmMatriculContrato: TFmMatriculContrato
  Left = 373
  Top = 214
  Caption = 'CON-PRINT-001 :: Impress'#227'o de Contrato'
  ClientHeight = 175
  ClientWidth = 763
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel2: TPanel
    Left = 0
    Top = 116
    Width = 763
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 20
      Top = 5
      Width = 110
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 650
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 763
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Impress'#227'o de Contrato'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 761
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 49
    Width = 763
    Height = 67
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 114
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Escolha o contrato:'
    end
    object SpeedButton1: TSpeedButton
      Left = 724
      Top = 30
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object CBEscolha: TdmkDBLookupComboBox
      Left = 79
      Top = 30
      Width = 645
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Titulo'
      ListSource = DsEscolha
      TabOrder = 1
      dmkEditCB = EdEscolha
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEscolha: TdmkEditCB
      Left = 10
      Top = 30
      Width = 69
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEscolha
      IgnoraDBLookupComboBox = False
    end
  end
  object QrEscolha: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cartas'
      'ORDER BY Titulo')
    Left = 124
    Top = 72
    object QrEscolhaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEscolhaTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrEscolhaTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEscolhaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEscolhaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEscolhaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEscolhaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEscolhaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsEscolha: TDataSource
    DataSet = QrEscolha
    Left = 152
    Top = 72
  end
  object QrAluno: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAlunoCalcFields
    SQL.Strings = (
      'SELECT ent.*,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTIDADE,'
      'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial'
      'ELSE acm.Nome END NOMEACCOUNT,'
      'eng.Nome NOMEENTIGRUPO,'
      'mot.Descricao NOMEMOTIVO,'
      'euf.Nome NOMEEUF, puf.nome NOMEPUF,'
      'cuf.Nome NOMECUF, luf.nome NOMELUF,'
      'elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD,'
      'elo.Trato NOMEETRATO, plo.Trato NOMEPTRATO,'
      'clo.Trato NOMECTRATO, llo.Trato NOMELTRATO'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.EUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.PUF'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'WHERE ent.Codigo=:P0')
    Left = 200
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAlunoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAlunoRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrAlunoFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrAlunoRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrAlunoRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrAlunoPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrAlunoMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrAlunoCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrAlunoIE: TWideStringField
      FieldName = 'IE'
    end
    object QrAlunoIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrAlunoNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrAlunoApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrAlunoCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrAlunoRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrAlunoELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrAlunoERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrAlunoENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrAlunoECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrAlunoEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrAlunoECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrAlunoEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrAlunoECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrAlunoEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrAlunoETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrAlunoEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrAlunoEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrAlunoECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrAlunoEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrAlunoEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrAlunoEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrAlunoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrAlunoPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrAlunoPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrAlunoPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrAlunoPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrAlunoPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrAlunoPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrAlunoPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrAlunoPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrAlunoPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrAlunoPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrAlunoPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrAlunoPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrAlunoPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrAlunoPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrAlunoPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrAlunoPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrAlunoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrAlunoSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrAlunoResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrAlunoProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrAlunoCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrAlunoRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrAlunoDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrAlunoAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrAlunoAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrAlunoCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrAlunoCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrAlunoFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrAlunoFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrAlunoFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrAlunoFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrAlunoTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrAlunoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrAlunoInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrAlunoLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrAlunoVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrAlunoMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrAlunoObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAlunoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrAlunoCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrAlunoCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrAlunoCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrAlunoCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrAlunoCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrAlunoCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrAlunoCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrAlunoCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrAlunoCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrAlunoCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrAlunoCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrAlunoCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrAlunoCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrAlunoLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrAlunoLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrAlunoLNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object QrAlunoLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrAlunoLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrAlunoLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrAlunoLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrAlunoLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrAlunoLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrAlunoLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrAlunoLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrAlunoLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrAlunoLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrAlunoComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrAlunoSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrAlunoNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrAlunoGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrAlunoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrAlunoLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrAlunoConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrAlunoConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrAlunoNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrAlunoNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrAlunoNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrAlunoNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrAlunoNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrAlunoNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrAlunoNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrAlunoNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrAlunoCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrAlunoCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrAlunoCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrAlunoCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrAlunoCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrAlunoCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrAlunoMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrAlunoQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrAlunoQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrAlunoQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrAlunoQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrAlunoQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrAlunoQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrAlunoAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrAlunoSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrAlunoSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrAlunoLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrAlunoDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrAlunoCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrAlunoTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrAlunoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlunoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlunoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlunoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlunoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAlunoNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrAlunoNOMEACCOUNT: TWideStringField
      FieldName = 'NOMEACCOUNT'
      Size = 100
    end
    object QrAlunoNOMEENTIGRUPO: TWideStringField
      FieldName = 'NOMEENTIGRUPO'
      Size = 100
    end
    object QrAlunoNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrAlunoNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Required = True
      Size = 2
    end
    object QrAlunoNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
    object QrAlunoNOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Required = True
      Size = 2
    end
    object QrAlunoNOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Required = True
      Size = 2
    end
    object QrAlunoNOMEELOGRAD: TWideStringField
      FieldName = 'NOMEELOGRAD'
      Size = 10
    end
    object QrAlunoNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrAlunoNOMECLOGRAD: TWideStringField
      FieldName = 'NOMECLOGRAD'
      Size = 10
    end
    object QrAlunoNOMELLOGRAD: TWideStringField
      FieldName = 'NOMELLOGRAD'
      Size = 10
    end
    object QrAlunoPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 50
      Calculated = True
    end
    object QrAlunoPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCPF_TXT'
      Size = 50
      Calculated = True
    end
    object QrAlunoPCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEP_TXT'
      Calculated = True
    end
    object QrAlunoNOMEETRATO: TWideStringField
      FieldName = 'NOMEETRATO'
      Size = 10
    end
    object QrAlunoNOMEPTRATO: TWideStringField
      FieldName = 'NOMEPTRATO'
      Size = 10
    end
    object QrAlunoNOMECTRATO: TWideStringField
      FieldName = 'NOMECTRATO'
      Size = 10
    end
    object QrAlunoNOMELTRATO: TWideStringField
      FieldName = 'NOMELTRATO'
      Size = 10
    end
    object QrAlunoPNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNUM_TXT'
      Size = 30
      Calculated = True
    end
    object QrAlunoPMEU_BAIRRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PMEU_BAIRRO'
      Size = 255
      Calculated = True
    end
  end
  object frxContrato: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38008.523706979200000000
    ReportOptions.LastChange = 39724.611074942100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxContratoGetValue
    Left = 256
    Top = 44
    Datasets = <
      item
        DataSet = frxDsAluno
        DataSetName = 'frxDsAluno'
      end
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxDsEscolha'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 25.322200000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        RowCount = 1
        Stretched = True
        object Rich1: TfrxRichView
          Width = 680.220470000000000000
          Height = 25.322200000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDBDataset1
          DataSetName = 'frxDsEscolha'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C636F6C6F72
            74626C203B5C726564305C677265656E305C626C7565303B7D0D0A7B5C2A5C67
            656E657261746F722052696368656432302031302E302E31343339337D5C7669
            65776B696E64345C756331200D0A5C706172645C6366315C66305C667331365C
            7061720D0A7D0D0A00}
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 18.188310000000000000
        Top = 105.826840000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          Left = 495.716450000000000000
          Top = 0.188310000000001300
          Width = 183.842610000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Page#] de [TotalPages#]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsAluno: TfrxDBDataset
    UserName = 'frxDsAluno'
    CloseDataSource = False
    DataSet = QrAluno
    BCDToCurrency = False
    Left = 228
    Top = 44
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDsEscolha'
    CloseDataSource = False
    DataSet = QrEscolha
    BCDToCurrency = False
    Left = 180
    Top = 72
  end
  object QrRespo: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAlunoCalcFields
    SQL.Strings = (
      'SELECT ent.Nome, ent.Nacionalid, ent.Profissao, '
      'ent.PRua, IF(ent.PNumero=0,"S/N", ent.PNumero) PNumero, '
      'ent.PCompl, ent.PBairro,'
      'ent.PUF, ent.PCidade, ent.PPais, ent.PCEP,'
      
        'IF(DataRG="1899-12-30","", DATE_FORMAT(DataRG, "%d/%m/%Y")) Data' +
        'RG_TXT,'
      'IF(ent.EstCivil=0, "", lec.Nome) NO_EstCivil,'
      'PTe1, PTe2, PCel, PEMail, CPF, RG, SSP, llo.Nome NO_PLograd,'
      'ufp.Nome UF_TXT'
      'FROM entidades ent '
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.PLograd'
      'LEFT JOIN ufs ufp ON ufp.Codigo=ent.PUF'
      'WHERE ent.Codigo=:P0')
    Left = 284
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRespoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrRespoNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrRespoProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrRespoPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrRespoPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrRespoPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrRespoPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrRespoPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrRespoPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrRespoDataRG_TXT: TWideStringField
      FieldName = 'DataRG_TXT'
      Size = 10
    end
    object QrRespoNO_EstCivil: TWideStringField
      FieldName = 'NO_EstCivil'
      Size = 10
    end
    object QrRespoPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrRespoCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrRespoRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrRespoSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrRespoNO_PLograd: TWideStringField
      FieldName = 'NO_PLograd'
      Size = 10
    end
    object QrRespoUF_TXT: TWideStringField
      FieldName = 'UF_TXT'
      Required = True
      Size = 2
    end
    object QrRespoPNumero: TWideStringField
      FieldName = 'PNumero'
      Size = 11
    end
    object QrRespoPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrRespoPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrRespoPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrRespoPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
  end
end
