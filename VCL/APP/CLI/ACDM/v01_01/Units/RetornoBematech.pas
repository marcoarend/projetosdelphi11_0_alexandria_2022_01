unit RetornoBematech;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TFmRetornoBematech = class(TForm)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmRetornoBematech: TFmRetornoBematech;

implementation

{$R *.DFM}

procedure TFmRetornoBematech.BtSaidaClick(Sender: TObject);
begin
  RadioButton1.Checked := false;
  RadioButton2.Checked := false;
  label4.Enabled  := false;
  label5.Enabled  := false;
  label6.Enabled  := false;
  label7.Enabled  := false;
  label8.Enabled  := false;
  label9.Enabled  := false;
  label10.Enabled := false;
  label11.Enabled := false;
  label12.Enabled := false;
  label13.Enabled := false;
  label14.Enabled := false;
  label15.Enabled := false;
  label16.Enabled := false;
  label17.Enabled := false;
  label18.Enabled := false;
  label19.Enabled := false;
  Close;
end;

end.
