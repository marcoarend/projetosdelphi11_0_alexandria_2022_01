unit SWMS_Mtng_Cab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Menus, ComCtrls, Grids,
  DBGrids, dmkImage, dmkEditDateTimePicker, OleCtrls, SHDocVw, SOAPServer,
  (*XML*)XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB,(*FIM XML*)
  Variants, dmkCheckBox, UnitListas, dmkDBGrid, dmkMemo, dmkRadioGroup, frxClass,
  frxDBSet, UnDmkProcFunc, UnDmkEnums;

type
  MyArrStr = array of String;
  TLadoPior = (lpFirst, lpLast);
  TFmSWMS_Mtng_Cab = class(TForm)
    DsSWMS_Mtng_Cab: TDataSource;
    QrSWMS_Mtng_Cab: TmySQLQuery;
    dmkPermissoes1: TdmkPermissoes;
    PMCab: TPopupMenu;
    QrSWMS_Mtng_Its: TmySQLQuery;
    DsSWMS_Mtng_Its: TDataSource;
    PMIts: TPopupMenu;
    PnTitulo: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    PainelEdita: TPanel;
    GBRodaPe2: TGroupBox;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBAviso0304: TGroupBox;
    Panel4: TPanel;
    LaAviso4: TLabel;
    LaAviso3: TLabel;
    PainelDados: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel9: TPanel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GBRodaPe1: TGroupBox;
    PainelControle: TPanel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtProva: TBitBtn;
    BtIts: TBitBtn;
    BtCab: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdNome: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdCodigo: TdmkDBEdit;
    GBAviso0102: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrSWMS_Mtng_CabCodigo: TIntegerField;
    QrSWMS_Mtng_CabCodUsu: TIntegerField;
    QrSWMS_Mtng_CabCNPJ_EMP: TWideStringField;
    QrSWMS_Mtng_CabNome: TWideStringField;
    QrSWMS_Mtng_CabLocal: TWideStringField;
    QrSWMS_Mtng_CabDtInscrIni: TDateField;
    QrSWMS_Mtng_CabDtInscrFim: TDateField;
    QrSWMS_Mtng_CabDataEvenIni: TDateField;
    QrSWMS_Mtng_CabHoraEvenIni: TTimeField;
    QrSWMS_Mtng_CabDataEvenFim: TDateField;
    QrSWMS_Mtng_CabHoraEvenFim: TTimeField;
    QrSWMS_Mtng_CabSenhaC: TWideStringField;
    QrSWMS_Mtng_CabContatoNome: TWideStringField;
    QrSWMS_Mtng_CabContatoFone: TWideStringField;
    QrSWMS_Mtng_CabContatoMail: TWideStringField;
    QrSWMS_Mtng_CabRegulam: TWideMemoField;
    QrSWMS_Mtng_CabXML_Resul: TWideMemoField;
    QrSWMS_Mtng_CabDtHrResul: TDateTimeField;
    QrSWMS_Mtng_CabObserv_0: TWideStringField;
    QrSWMS_Mtng_CabObserv_1: TWideStringField;
    QrSWMS_Mtng_CabObserv_2: TWideStringField;
    QrSWMS_Mtng_CabObserv_3: TWideStringField;
    QrSWMS_Mtng_CabObserv_4: TWideStringField;
    QrSWMS_Mtng_CabObserv_5: TWideStringField;
    QrSWMS_Mtng_CabObserv_6: TWideStringField;
    QrSWMS_Mtng_CabObserv_7: TWideStringField;
    QrSWMS_Mtng_CabObserv_8: TWideStringField;
    QrSWMS_Mtng_CabObserv_9: TWideStringField;
    GroupBox4: TGroupBox;
    Panel7: TPanel;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Incluinovacompeticao1: TMenuItem;
    Alteracompeticaoatual1: TMenuItem;
    Excluicompeticaoatual1: TMenuItem;
    QrSWMS_Mtng_ItsCodigo: TIntegerField;
    QrSWMS_Mtng_ItsControle: TIntegerField;
    QrSWMS_Mtng_ItsSenhaP: TWideStringField;
    QrSWMS_Mtng_ItsCNPJ_PAR: TWideStringField;
    QrSWMS_Mtng_ItsIP: TWideStringField;
    QrSWMS_Mtng_ItsProtocol: TWideStringField;
    QrSWMS_Mtng_ItsXML: TWideMemoField;
    QrSWMS_Mtng_ItsDataHora: TDateTimeField;
    QrSWMS_Mtng_CabContatoFone_TXT: TWideStringField;
    GBReg: TGroupBox;
    LaReg1: TLabel;
    LaReg3: TLabel;
    TabSheet4: TTabSheet;
    N1: TMenuItem;
    Regulamento1: TMenuItem;
    TbRegulam: TmySQLTable;
    TbRegulamCodigo: TIntegerField;
    DsRegulam: TDataSource;
    TbRegulamRegulam: TWideMemoField;
    DBMemo1: TDBMemo;
    WebBrowser1: TWebBrowser;
    Edita1: TMenuItem;
    Limpa1: TMenuItem;
    Panel12: TPanel;
    GroupBox7: TGroupBox;
    Label35: TLabel;
    Label36: TLabel;
    Label23: TLabel;
    EdPasseC: TdmkEdit;
    TPDtInscrIni: TdmkEditDateTimePicker;
    TpDtInscrFim: TdmkEditDateTimePicker;
    QrSWMS_Mtng_CabSincronia: TSmallintField;
    Incluinovaequipe1: TMenuItem;
    QrSWMS_Mtng_ItsNome: TWideStringField;
    QrSWMS_Mtng_ItsDataHora_TXT: TWideStringField;
    N2: TMenuItem;
    Sincronizar1: TMenuItem;
    QrSWMS_Mtng_CabAtivo: TSmallintField;
    QrSWMS_Mtng_ItsAtivo: TSmallintField;
    Label29: TLabel;
    EdCNPJ_EMP: TdmkEdit;
    QrSWMS_Mtng_CabCNPJ_EMP_TXT: TWideStringField;
    N3: TMenuItem;
    Sincronizar2: TMenuItem;
    XMLDocument: TXMLDocument;
    QrLocIts: TmySQLQuery;
    QrLocItsControle: TIntegerField;
    QrCompetiPart: TmySQLQuery;
    QrCompetiPartNivel1: TIntegerField;
    QrCompetiPartNivel2: TIntegerField;
    QrCompetiPartCodigo: TIntegerField;
    QrCompetiPartControle: TIntegerField;
    QrCompetiPartEstilo: TIntegerField;
    QrCompetiPartMetros: TIntegerField;
    QrCompetiPartNadador: TIntegerField;
    QrCompetiPartCategoria: TIntegerField;
    QrCompetiPartTempo: TFloatField;
    DsCompetiPart: TDataSource;
    QrCompetiPartTempo_TXT: TWideStringField;
    QrSWMS_Mtng_ItsCNPJ_PAR_TXT: TWideStringField;
    Panel13: TPanel;
    DBGrid1: TDBGrid;
    QrCompetiPartNO_ESTILO: TWideStringField;
    QrCompetiPartNO_METROS: TWideStringField;
    QrCompetiPartNO_CATEGORIA: TWideStringField;
    QrCompetiPartNO_NADADOR: TWideStringField;
    Label27: TLabel;
    EdCategoriaIni: TdmkEdit;
    EdCategoriaIni_TXT: TdmkEdit;
    Label28: TLabel;
    EdCategoriaFim: TdmkEdit;
    EdCategoriaFim_TXT: TdmkEdit;
    QrSWMS_Mtng_CabCategoriaIni: TIntegerField;
    QrSWMS_Mtng_CabCategoriaFim: TIntegerField;
    Excluiequipeselecionada1: TMenuItem;
    PMProva: TPopupMenu;
    Incluinovaprova1: TMenuItem;
    DsSWMS_Mtng_Prv: TDataSource;
    QrSWMS_Mtng_Prv: TmySQLQuery;
    QrSWMS_Mtng_PrvCodigo: TIntegerField;
    QrSWMS_Mtng_PrvControle: TIntegerField;
    QrSWMS_Mtng_PrvCodTxt: TWideStringField;
    QrSWMS_Mtng_PrvEstilo: TIntegerField;
    QrSWMS_Mtng_PrvMetros: TIntegerField;
    QrSWMS_Mtng_PrvSexo: TWideStringField;
    Alteraaprovaselecionada1: TMenuItem;
    QrSWMS_Mtng_PrvPiscDescr: TWideStringField;
    QrSWMS_Mtng_PrvPiscCompr: TIntegerField;
    QrSWMS_Mtng_PrvPiscRaias: TIntegerField;
    QrSWMS_Mtng_PrvNO_ESTILO: TWideStringField;
    QrSWMS_Mtng_PrvNO_METROS: TWideStringField;
    N4: TMenuItem;
    Definecategoriasdaprovaselecionada1: TMenuItem;
    QrSWMS_Mtng_PrvCategoriaIni: TIntegerField;
    QrSWMS_Mtng_PrvCategoriaFim: TIntegerField;
    QrSWMS_Mtng_PrvNO_CATEGORIAINI: TWideStringField;
    QrSWMS_Mtng_PrvNO_CATEGORIAFIM: TWideStringField;
    DsSemCat: TDataSource;
    QrSemCat: TmySQLQuery;
    QrSemCatNascim: TDateField;
    QrSemCatNome: TWideStringField;
    QrSemCatSexo: TWideStringField;
    QrSemCatNivel1: TIntegerField;
    QrSemCatNivel2: TIntegerField;
    QrSemCatCodigo: TIntegerField;
    QrSemCatControle: TIntegerField;
    QrSemCatEstilo: TIntegerField;
    QrSemCatMetros: TIntegerField;
    QrSemCatNadador: TIntegerField;
    QrSemCatCategoria: TIntegerField;
    QrSemCatTempo: TFloatField;
    QrSemCatCategPart: TIntegerField;
    QrSemCatswms_mtng_prv: TIntegerField;
    QrTem: TmySQLQuery;
    QrPrvs: TmySQLQuery;
    Button1: TButton;
    Edpart: TdmkEdit;
    EdRais: TdmkEdit;
    QrSWMS_Mtng_PrvPiorRaia: TSmallintField;
    Unica1: TMenuItem;
    Multiplas1: TMenuItem;
    N5: TMenuItem;
    Participaes1: TMenuItem;
    N6: TMenuItem;
    Redefinirtodafaseclassificatria1: TMenuItem;
    Exclusodasprovasselecionadas1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Panel14: TPanel;
    GradeProvas: TdmkDBGrid;
    Panel15: TPanel;
    GroupBox9: TGroupBox;
    RGOrdProva1: TdmkRadioGroup;
    RGOrdProva2: TdmkRadioGroup;
    RGOrdProva3: TdmkRadioGroup;
    RGOrdProva4: TdmkRadioGroup;
    Panel16: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    EdCodUsu: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdLocal: TdmkEdit;
    Label5: TLabel;
    TPDataEvenIni: TdmkEditDateTimePicker;
    Label6: TLabel;
    EdHoraEvenIni: TdmkEdit;
    Label10: TLabel;
    TPDataEvenFim: TdmkEditDateTimePicker;
    Label11: TLabel;
    EdHoraEvenFim: TdmkEdit;
    Label12: TLabel;
    EdContatoNome: TdmkEdit;
    Label13: TLabel;
    EdContatoFone: TdmkEdit;
    Label34: TLabel;
    EdContatoMail: TdmkEdit;
    RGFases: TdmkRadioGroup;
    GroupBox10: TGroupBox;
    CkAtivo: TdmkCheckBox;
    EdSincronia: TdmkEdit;
    QrSWMS_Mtng_CabOrdProva1: TIntegerField;
    QrSWMS_Mtng_CabOrdProva2: TIntegerField;
    QrSWMS_Mtng_CabOrdProva3: TIntegerField;
    QrSWMS_Mtng_CabOrdProva4: TIntegerField;
    QrSWMS_Mtng_CabFases: TSmallintField;
    GroupBox11: TGroupBox;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    Panel11: TPanel;
    Panel18: TPanel;
    GroupBox1: TGroupBox;
    LaSincro1: TLabel;
    LaSincro3: TLabel;
    GroupBox5: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label14: TLabel;
    Label24: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdCategoriaIni_TXT: TdmkEdit;
    DBEdCategoriaFim_TXT: TdmkEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    Panel17: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit19: TDBEdit;
    DBRadioGroup5: TDBRadioGroup;
    Panel19: TPanel;
    GroupBox8: TGroupBox;
    EdObserv_0: TdmkEdit;
    EdObserv_1: TdmkEdit;
    EdObserv_2: TdmkEdit;
    EdObserv_3: TdmkEdit;
    EdObserv_4: TdmkEdit;
    EdObserv_5: TdmkEdit;
    EdObserv_6: TdmkEdit;
    EdObserv_7: TdmkEdit;
    EdObserv_8: TdmkEdit;
    EdObserv_9: TdmkEdit;
    GroupBox12: TGroupBox;
    Label32: TLabel;
    EdOrdem1: TdmkEdit;
    EdOrdem2: TdmkEdit;
    Label33: TLabel;
    EdOrdem3: TdmkEdit;
    Label37: TLabel;
    EdOrdem4: TdmkEdit;
    Label38: TLabel;
    EdOrdem5: TdmkEdit;
    Label39: TLabel;
    EdOrdem6: TdmkEdit;
    Label40: TLabel;
    EdOrdem7: TdmkEdit;
    Label41: TLabel;
    Estilos1: TMenuItem;
    EdFator1: TdmkEdit;
    EdFator2: TdmkEdit;
    EdFator3: TdmkEdit;
    EdFator4: TdmkEdit;
    EdFator5: TdmkEdit;
    EdFator6: TdmkEdit;
    EdFator7: TdmkEdit;
    Label42: TLabel;
    Label43: TLabel;
    Panel20: TPanel;
    GroupBox3: TGroupBox;
    DBEdit18: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit9: TDBEdit;
    Panel21: TPanel;
    Label44: TLabel;
    DBGrid2: TDBGrid;
    QrSWMS_Mtng_Sty: TmySQLQuery;
    DsSWMS_Mtng_Sty: TDataSource;
    QrSWMS_Mtng_StyCodigo: TIntegerField;
    QrSWMS_Mtng_StyEstilo: TIntegerField;
    QrSWMS_Mtng_StyOrdem: TIntegerField;
    QrSWMS_Mtng_StyFatorPtos: TIntegerField;
    QrSWMS_Mtng_StyNO_ESTILO: TWideStringField;
    QrSWMS_Mtng_PrvSEQ: TIntegerField;
    QrPart: TmySQLQuery;
    QrPartNivel1: TIntegerField;
    QrPartNivel2: TIntegerField;
    QrPartCodigo: TIntegerField;
    QrPartControle: TIntegerField;
    QrPartEstilo: TIntegerField;
    QrPartMetros: TIntegerField;
    QrPartNadador: TIntegerField;
    QrPartCategoria: TIntegerField;
    QrPartTempo: TFloatField;
    QrProv: TmySQLQuery;
    QrProvControle: TIntegerField;
    QrPartCategPart: TIntegerField;
    QrPartSexo: TWideStringField;
    PB1: TProgressBar;
    DBGrid4: TDBGrid;
    QrFaseA: TmySQLQuery;
    DsFaseA: TDataSource;
    QrLarg: TmySQLQuery;
    QrPrvsControle: TIntegerField;
    MeLista: TMemo;
    QrLargSerieA: TIntegerField;
    QrLargRaiaA: TIntegerField;
    QrLargNO_NADADOR: TWideStringField;
    QrLargNivel2: TIntegerField;
    QrLargControle: TIntegerField;
    QrLargTempo: TFloatField;
    QrFaseASerieA: TIntegerField;
    QrFaseARaiaA: TIntegerField;
    QrFaseANO_NADADOR: TWideStringField;
    QrFaseANivel2: TIntegerField;
    QrFaseAControle: TIntegerField;
    QrFaseATempo: TFloatField;
    QrPrvsPiscRaias: TIntegerField;
    QrPrvsPiorRaia: TSmallintField;
    QrFaseATEMPO_STR: TWideStringField;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    DBGrid3: TDBGrid;
    N7: TMenuItem;
    Participaes2: TMenuItem;
    Participantes1: TMenuItem;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    QrNadadores: TmySQLQuery;
    DsNadadores: TDataSource;
    QrNadadoresNivel1: TIntegerField;
    QrNadadoresNivel2: TIntegerField;
    QrNadadoresCodigo: TIntegerField;
    QrNadadoresCodTxt: TWideStringField;
    QrNadadoresNome: TWideStringField;
    QrNadadoresNascim: TDateField;
    QrNadadoresSexo: TWideStringField;
    QrNadadoresCPF: TWideStringField;
    QrNadadoresFone: TWideStringField;
    QrNadadoresLk: TIntegerField;
    QrNadadoresDataCad: TDateField;
    QrNadadoresDataAlt: TDateField;
    QrNadadoresUserCad: TIntegerField;
    QrNadadoresUserAlt: TIntegerField;
    QrNadadoresAlterWeb: TSmallintField;
    QrNadadoresAtivo: TSmallintField;
    QrNadadoresImportado: TSmallintField;
    DBGrid5: TDBGrid;
    QrNadadoresCPF_TXT: TWideStringField;
    QrNadadoresFone_TXT: TWideStringField;
    GroupBox13: TGroupBox;
    Panel23: TPanel;
    Label45: TLabel;
    EdFiltroNadNome: TEdit;
    GroupBox14: TGroupBox;
    Panel24: TPanel;
    Label47: TLabel;
    Label46: TLabel;
    EdFiltroPartCateg: TEdit;
    EdFiltroPartNome: TEdit;
    Label48: TLabel;
    EdFiltroPartEstilo: TEdit;
    Label49: TLabel;
    EdFiltroPartMetros: TEdit;
    Inclui2: TMenuItem;
    Altera2: TMenuItem;
    QrCompetiPartImportado: TSmallintField;
    frxSWMS_Mtng_Cab_03_00: TfrxReport;
    QrSers: TmySQLQuery;
    QrSersSerieA: TIntegerField;
    frxDsSWMS_Mtng_Prv: TfrxDBDataset;
    QrSWMS_Mtng_PrvNO_SEXO: TWideStringField;
    frxDsFaseA: TfrxDBDataset;
    QrFaseANO_EQUIPE: TWideStringField;
    QrFaseATempoA: TFloatField;
    QrFaseATEMPOA_STR: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSWMS_Mtng_CabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSWMS_Mtng_CabBeforeOpen(DataSet: TDataSet);
    procedure BtCabClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrSWMS_Mtng_CabBeforeClose(DataSet: TDataSet);
    procedure CabecalhoDaLiturgiaAtual1Click(Sender: TObject);
    procedure ItemDeLiturgiaSelecionado1Click(Sender: TObject);
    procedure QrSWMS_Mtng_CabAfterScroll(DataSet: TDataSet);
    procedure BtProvaClick(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Incluinovacompeticao1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure Alteracompeticaoatual1Click(Sender: TObject);
    procedure QrSWMS_Mtng_CabCalcFields(DataSet: TDataSet);
    procedure Edita1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Incluinovaequipe1Click(Sender: TObject);
    procedure QrSWMS_Mtng_ItsCalcFields(DataSet: TDataSet);
    procedure Sincronizar1Click(Sender: TObject);
    procedure Sincronizar2Click(Sender: TObject);
    procedure QrSWMS_Mtng_ItsBeforeClose(DataSet: TDataSet);
    procedure QrSWMS_Mtng_ItsAfterScroll(DataSet: TDataSet);
    procedure QrCompetiPartCalcFields(DataSet: TDataSet);
    procedure EdCategoriaIni_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaIniChange(Sender: TObject);
    procedure EdCategoriaFimChange(Sender: TObject);
    procedure EdCategoriaFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaFim_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24Change(Sender: TObject);
    procedure DBEdit25Change(Sender: TObject);
    procedure Excluicompeticaoatual1Click(Sender: TObject);
    procedure Excluiequipeselecionada1Click(Sender: TObject);
    procedure Recria1Click(Sender: TObject);
    procedure Alteraaprovaselecionada1Click(Sender: TObject);
    procedure Definecategoriasdaprovaselecionada1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Unica1Click(Sender: TObject);
    procedure Multiplas1Click(Sender: TObject);
    procedure Reorganizarfaseclassificatria1Click(Sender: TObject);
    procedure ExclusodeProvas1Click(Sender: TObject);
    procedure Redefinirtodafaseclassificatria1Click(Sender: TObject);
    procedure Exclusodasprovasselecionadas1Click(Sender: TObject);
    procedure QrSWMS_Mtng_PrvCalcFields(DataSet: TDataSet);
    procedure QrSWMS_Mtng_PrvAfterScroll(DataSet: TDataSet);
    procedure QrSWMS_Mtng_PrvBeforeClose(DataSet: TDataSet);
    procedure QrFaseACalcFields(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure QrNadadoresCalcFields(DataSet: TDataSet);
    procedure EdFiltroNadNomeChange(Sender: TObject);
    procedure EdFiltroPartNomeChange(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
    procedure frxSWMS_Mtng_Cab_03_00GetValue(const VarName: string;
      var Value: Variant);
    procedure DBGrid4DblClick(Sender: TObject);
  private
    { Private declarations }
    //
{
    FCampos, FIndices: array of String;
    FValFld, FValIdx: array of Variant;
}
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //function  CopiaParteTexto(Tag: String; XML: WideString): WideString;
    function  InsereDadosTabelas(CNPJ_PAR, IP, Protocol: String;
              DataHora: TDateTime; XML: WideString): Boolean;
    procedure OrganizaFase1();
    function  DistribuicaoNasRaias(QtdAtletas, QtdRaias: Integer; LadoPior:
              TLadoPior): MyArrStr;
    function  DistribuicaoNasRaias2(QtdAtletas, QtdRaias: Integer; LadoPior:
              TLadoPior; OrdemCtrl, OrdemNiv2: array of Integer): MyArrStr;
    procedure CadastroNadadores(SQLType: TSQLType);
    procedure CadastroCompetiPart(SQLType: TSQLType);
  public
    { Public declarations }
    //
    procedure ReopenSWMS_Mtng_Its(Controle: Integer);
    procedure ReopenSWMS_Mtng_Prv(Controle: Integer);
    procedure ReopenSWMS_Mtng_Sty(Estilo: Integer);
    procedure ReopenSWMS_Envi_CompetiPart(Controle: Integer);
    procedure ReopenSWMS_Envi_Nadadores(Codigo: Integer);
    procedure ReopenFaseClassificatoria(Controle: Integer);
    procedure SetaValorCampo(var Campos: array of String; var Values: array of Variant;
              NomeNoh: String; Valor: Variant; Index: Integer);
    procedure InsUpdPrv(SQLType: TSQLType);
    //
  end;

var
  FmSWMS_Mtng_Cab: TFmSWMS_Mtng_Cab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, Textos, WBEdit, SWMS_Mtng_Its, Principal,
MyListas, SWMS_Mtng_PrvUni, SWMS_Mtng_PrvMul, UCreate, ModuleGeral, SelIts, SWMS_Mtng_Cat,
  swms_envi_nadadores, swms_envi_competipart, SWMS_Mtng_Tem;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSWMS_Mtng_Cab.Limpa1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := QrSWMS_Mtng_CabCodigo.Value;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_mtng_cab', False, [
  'Regulam'], ['Codigo'], [
  ''], [Codigo], True);
  //
  LocCod(Codigo, Codigo);
  Screen.Cursor := crDefault;
end;

procedure TFmSWMS_Mtng_Cab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSWMS_Mtng_Cab.Va(Para: TVaiPara);
begin
  DefParams;
  GOTOy.GoNew(Para, QrSWMS_Mtng_CabCodigo.Value, GBReg, [LaReg1,LaReg3]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSWMS_Mtng_Cab.DefParams;
begin
  VAR_GOTOTABELA := 'swms_mtng_cab';
  VAR_GOTOMYSQLTABLE := QrSWMS_Mtng_Cab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM swms_mtng_cab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome LIKE :P0');
  //
end;

function TFmSWMS_Mtng_Cab.DistribuicaoNasRaias(QtdAtletas, QtdRaias: Integer;
LadoPior: TLadoPior):
MyArrStr;
var
  //N, K, SumVagas, SeqRaia,
  I, R, Raia, Y, Series, Serie, SerVagas, ResVagas, Resto: Integer;
begin
  Series := QtdAtletas div QtdRaias;
  if QtdAtletas mod QtdRaias > 0 then
    Series := Series + 1;
  SetLength(Result, Series * QtdRaias);
  I := 0;
  for Serie := 1 to Series do
  begin
    for Raia := 1 to QtdRaias do
    begin
      //Y := ((Serie - 1) * QtdRaias) + Raia;
      Result[I] := '|' + FormatFloat('000', I+1) + '|' +
        FormatFloat('00', Serie) + '|' + FormatFloat('00', Raia) + '|';
      I := I + 1;
    end;
  end;
  //
  R := 0;
  //N := 0;
  Serie := 0;
  ResVagas := 0;
  //SerVagas := 0;
  //SumVagas := 0;
  for I := 1 to QtdAtletas do
  begin
    if ResVagas = 0 then
    begin
      //SumVagas := SumVagas + SerVagas;
      //
      Resto := QtdAtletas - I + 1;
      SerVagas := Resto div (Series - Serie);
      {
      if SerVagas = 0 then
        SerVagas := QtdRaias;
      }
      ResVagas := SerVagas;
      Serie := Serie + 1;
      //SeqRaia := 0;
      R := QtdRaias - SerVagas;
    end;
    Raia := 0;
    case LadoPior of
      lpFirst:
      begin
        case QtdRaias of
          02: Raia := PiorAMelhorRaias02F[R];
          03: Raia := PiorAMelhorRaias03F[R];
          04: Raia := PiorAMelhorRaias04F[R];
          05: Raia := PiorAMelhorRaias05F[R];
          06: Raia := PiorAMelhorRaias06F[R];
          07: Raia := PiorAMelhorRaias07F[R];
          08: Raia := PiorAMelhorRaias08F[R];
          09: Raia := PiorAMelhorRaias09F[R];
          10: Raia := PiorAMelhorRaias10F[R];
          11: Raia := PiorAMelhorRaias11F[R];
          12: Raia := PiorAMelhorRaias12F[R];
          13: Raia := PiorAMelhorRaias13F[R];
          14: Raia := PiorAMelhorRaias14F[R];
          15: Raia := PiorAMelhorRaias15F[R];
          16: Raia := PiorAMelhorRaias16F[R];
          17: Raia := PiorAMelhorRaias17F[R];
          else Raia := 0;
        end;
      end;
      lpLast:
      begin
        case QtdRaias of
          02: Raia := PiorAMelhorRaias02L[R];
          03: Raia := PiorAMelhorRaias03L[R];
          04: Raia := PiorAMelhorRaias04L[R];
          05: Raia := PiorAMelhorRaias05L[R];
          06: Raia := PiorAMelhorRaias06L[R];
          07: Raia := PiorAMelhorRaias07L[R];
          08: Raia := PiorAMelhorRaias08L[R];
          09: Raia := PiorAMelhorRaias09L[R];
          10: Raia := PiorAMelhorRaias10L[R];
          11: Raia := PiorAMelhorRaias11L[R];
          12: Raia := PiorAMelhorRaias12L[R];
          13: Raia := PiorAMelhorRaias13L[R];
          14: Raia := PiorAMelhorRaias14L[R];
          15: Raia := PiorAMelhorRaias15L[R];
          16: Raia := PiorAMelhorRaias16L[R];
          17: Raia := PiorAMelhorRaias17L[R];
          else Raia := 0;
        end;
      end;
    end;
    Y := ((Serie -1) * QtdRaias) + Raia;
    Result[Y] := Result[Y] + FormatFloat('000', I) + '|';
    //
    R := R + 1;
    ResVagas := ResVagas - 1;
  end;
end;

function TFmSWMS_Mtng_Cab.DistribuicaoNasRaias2(QtdAtletas, QtdRaias: Integer;
  LadoPior: TLadoPior; OrdemCtrl, OrdemNiv2: array of Integer): MyArrStr;
var
  I, R, Raia, Y, Series, Serie, SerVagas, ResVagas, Resto: Integer;
  SerieA, RaiaA, Nivel2, Controle, _x_: Integer;
begin
  Series := QtdAtletas div QtdRaias;
  if QtdAtletas mod QtdRaias > 0 then
    Series := Series + 1;
  SetLength(Result, Series * QtdRaias);
  I := 0;
  for Serie := 1 to Series do
  begin
    for Raia := 1 to QtdRaias do
    begin
      //Y := ((Serie - 1) * QtdRaias) + Raia;
      Result[I] := '|' + FormatFloat('000', I+1) + '|' +
        FormatFloat('00', Serie) + '|' + FormatFloat('00', Raia) + '|';
      I := I + 1;
    end;
  end;
  //
  R := 0;
  Serie := 0;
  ResVagas := 0;
  //SerVagas := 0;
  for I := 1 to QtdAtletas do
  begin
    if ResVagas = 0 then
    begin
      Resto := QtdAtletas - I + 1;
      SerVagas := Resto div (Series - Serie);
      ResVagas := SerVagas;
      Serie := Serie + 1;
      R := QtdRaias - SerVagas;
    end;
    Raia := 0;
    case LadoPior of
      lpFirst:
      begin
        case QtdRaias of
          02: Raia := PiorAMelhorRaias02F[R];
          03: Raia := PiorAMelhorRaias03F[R];
          04: Raia := PiorAMelhorRaias04F[R];
          05: Raia := PiorAMelhorRaias05F[R];
          06: Raia := PiorAMelhorRaias06F[R];
          07: Raia := PiorAMelhorRaias07F[R];
          08: Raia := PiorAMelhorRaias08F[R];
          09: Raia := PiorAMelhorRaias09F[R];
          10: Raia := PiorAMelhorRaias10F[R];
          11: Raia := PiorAMelhorRaias11F[R];
          12: Raia := PiorAMelhorRaias12F[R];
          13: Raia := PiorAMelhorRaias13F[R];
          14: Raia := PiorAMelhorRaias14F[R];
          15: Raia := PiorAMelhorRaias15F[R];
          16: Raia := PiorAMelhorRaias16F[R];
          17: Raia := PiorAMelhorRaias17F[R];
          else Raia := 0;
        end;
      end;
      lpLast:
      begin
        case QtdRaias of
          02: Raia := PiorAMelhorRaias02L[R];
          03: Raia := PiorAMelhorRaias03L[R];
          04: Raia := PiorAMelhorRaias04L[R];
          05: Raia := PiorAMelhorRaias05L[R];
          06: Raia := PiorAMelhorRaias06L[R];
          07: Raia := PiorAMelhorRaias07L[R];
          08: Raia := PiorAMelhorRaias08L[R];
          09: Raia := PiorAMelhorRaias09L[R];
          10: Raia := PiorAMelhorRaias10L[R];
          11: Raia := PiorAMelhorRaias11L[R];
          12: Raia := PiorAMelhorRaias12L[R];
          13: Raia := PiorAMelhorRaias13L[R];
          14: Raia := PiorAMelhorRaias14L[R];
          15: Raia := PiorAMelhorRaias15L[R];
          16: Raia := PiorAMelhorRaias16L[R];
          17: Raia := PiorAMelhorRaias17L[R];
          else Raia := 0;
        end;
      end;
    end;
    Y := ((Serie -1) * QtdRaias) + Raia;
    Result[Y] := Result[Y] + FormatFloat('000', I) + '|';
    //
    R := R + 1;
    ResVagas := ResVagas - 1;
    //
    _x_ := High(OrdemNiv2) + 1;
    //if I > _x_ then
    if I <= _x_ then
    begin
      SerieA := Serie;
      RaiaA := Raia + 1;
      Nivel2 := OrdemNiv2[I - 1];
      Controle := OrdemCtrl[I - 1];
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_envi_competipart', False, [
      'SerieA', 'RaiaA'], [
      'Nivel2', 'Controle'], [
      SerieA, RaiaA], [
      Nivel2, Controle], True);
    end;
  end;
end;

procedure TFmSWMS_Mtng_Cab.EdCategoriaFimChange(Sender: TObject);
begin
  EdCategoriaFim_TXT.Text :=
    UnListas.CategoriasNatacao_Get(Geral.IMV(EdCategoriaFim.Text));
end;

procedure TFmSWMS_Mtng_Cab.EdCategoriaFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaFim.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_Cab.EdCategoriaFim_TXTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaFim.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_Cab.EdCategoriaIniChange(Sender: TObject);
begin
  EdCategoriaIni_TXT.Text :=
    UnListas.CategoriasNatacao_Get(Geral.IMV(EdCategoriaIni.Text));
end;

procedure TFmSWMS_Mtng_Cab.EdCategoriaIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaIni.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_Cab.EdCategoriaIni_TXTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaIni.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_Cab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'swms_mtng_cab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmSWMS_Mtng_Cab.EdFiltroNadNomeChange(Sender: TObject);
begin
  ReopenSWMS_Envi_Nadadores(0);
end;

procedure TFmSWMS_Mtng_Cab.EdFiltroPartNomeChange(Sender: TObject);
begin
  ReopenSWMS_Envi_CompetiPart(0);
end;

procedure TFmSWMS_Mtng_Cab.Edita1Click(Sender: TObject);
begin
  TbRegulam.Refresh;
  if TbRegulam.Locate('Codigo', QrSWMS_Mtng_CabCodigo.Value, []) then
  begin
    Application.CreateForm(TFmWBEdit, FmWBEdit);
    FmWBEdit.FAtributesSize := 10;
    FmWBEdit.FAtributesName := 'Arial';
    FmWBEdit.FSaveSit    := 1;
    FmWBEdit.FTabela     := TbRegulam;
    FmWBEdit.FDBMemo     := DBMemo1;
    FmWBEdit.FCampo      := TbRegulamRegulam;
    FmWBEdit.FQuery      := Dmod.QrUpd;
(*
    FmWBEdit.LBItensMD.Sorted := False;
    with FmWBEdit.LBItensMD.Items do
    begin
      Add('[NOME_DA_TAG] Texto explicativo');
    end;
    FmWBEdit.LBItensMD.Sorted := True;
*)
    //
    FmWBEdit.ShowModal;
    FmWBEdit.Destroy;
    LocCod(QrSWMS_Mtng_CabCodigo.Value, QrSWMS_Mtng_CabCodigo.Value);
  end else
    Geral.MB_Erro('Defini��o do regulamento falhou!');
end;

procedure TFmSWMS_Mtng_Cab.Excluicompeticaoatual1Click(Sender: TObject);
begin
{
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da liturgia?', 'swms_mtng_cab',
    'Codigo', QrSWMS_Mtng_CabCodigo.Value);
  LocCod(0,0);
}
end;

procedure TFmSWMS_Mtng_Cab.Excluiequipeselecionada1Click(Sender: TObject);
begin
{
  UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item?', 'liturestrf',
    'Controle', QrSWMS_Mtng_ItsControle.Value);
  ReopenSWMS_Mtng_Its(QrSWMS_Mtng_ItsControle.Value);
}
end;

procedure TFmSWMS_Mtng_Cab.Exclusodasprovasselecionadas1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrSWMS_Mtng_Prv, TDBGrid(GradeProvas),
  'swms_mtng_prv', ['Controle'], ['Controle'], istSelecionados, '');
end;

procedure TFmSWMS_Mtng_Cab.ExclusodeProvas1Click(Sender: TObject);
begin
{
  ver se j� n�o h� vinculos com participa��es!
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrSWMS_Mtng_Prv, TDBGrid(GradeProvas),
  'swms_mtng_prv', ['Controle'], ['Controle'], istPergunta, '');
}
end;

procedure TFmSWMS_Mtng_Cab.Inclui1Click(Sender: TObject);
begin
  CadastroNadadores(stIns);
end;

procedure TFmSWMS_Mtng_Cab.Inclui2Click(Sender: TObject);
begin
  CadastroCompetiPart(stIns);
end;

procedure TFmSWMS_Mtng_Cab.Incluinovacompeticao1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrSWMS_Mtng_Cab, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'swms_mtng_cab');
end;

procedure TFmSWMS_Mtng_Cab.Incluinovaequipe1Click(Sender: TObject);
begin
  if UmyMod.FormInsUpd_Cria(TFmSWMS_Mtng_Its, FmSWMS_Mtng_Its, afmoNegarComAviso,
  QrSWMS_Mtng_Its, stIns) then
  begin
    FmSWMS_Mtng_Its.ShowModal;
    FmSWMS_Mtng_Its.Destroy;
  end;
end;

function TFmSWMS_Mtng_Cab.InsereDadosTabelas(CNPJ_PAR, IP, Protocol: String;
  DataHora: TDateTime; XML: WideString): Boolean;
var
  FnFld, FnIdx: Integer;
  Campos, Indice: array of String;
  ValFld, ValIdx: array of Variant;
  Codigo, Controle: Integer;
//function TDmod.InsereDadosTabela(IdxTable: Integer): Boolean;
  procedure DomToTree(XmlNode: IXMLNode; Tabela: String; Nivel: Integer);
  var
    I: Integer;
    AttrNode: IXMLNode;
    NomeNoh, Noh: String;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;

    // add the node itself
    //if XmlNode.NodeName = 'detNadador' then
    NomeNoh := Lowercase(XmlNode.NodeName);
    Noh := Copy(NomeNoh, 1, 3);
    //
    if Noh = 'fld' then
    begin
      FnFld := FnFld + 1;
      SetLength(Campos, FnFld);
      SetLength(ValFld, FnFld);
      SetaValorCampo(Campos, ValFld, NomeNoh, XmlNode.NodeValue, FnFld - 1);
    end else
    if (Noh = 'det') or (Noh = 'fim') then
    begin
      if (FnFld > 0) or (FnIdx > 0) then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
        Campos, Indice, ValFld, ValIdx, True);
      end;
      FnFld := Nivel;
      Fnidx := 0;
      SetLength(Campos, FnFld);
      SetLength(ValFld, FnFld);
      if Nivel > 0 then
      begin
        Campos[0] := 'Nivel1';
        ValFld[0] := Codigo;
      end;
      if Nivel > 1 then
      begin
        Campos[1] := 'Nivel2';
        ValFld[1] := Controle;
      end;
    end;
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      NomeNoh := Lowercase(AttrNode.NodeName);
      Noh := Copy(NomeNoh, 1, 3);
      //
      if Noh = 'fld' then
      begin
        FnIdx := FnIdx + 1;
        SetLength(Indice, FnIdx);
        SetLength(ValIdx, FnIdx);
        SetaValorCampo(Indice, ValIdx, NomeNoh, AttrNode.NodeValue, FnIdx - 1)
      end;
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree(xmlNode.ChildNodes.Nodes[I], Tabela, Nivel);
  end;
//const
  //Arquivo = 'C:\Dermatek\XML\SWMS\Enviados\EnviParticipacao_00005.XML';
var
  xmlNodeB, xmlNodeC: IXMLNode;
  //
  j: Integer;
  X: IXMLNode;
  MyCursor: TCursor;
  NomeNoh: String;
  idLote, fld_Str_SenhaC, fld_Str_SenhaP, fld_Str_CodTxt,
  fld_Str_Professor: String;
  //
  Stream: TMemoryStream;
  Linhas: TStringList;

begin
  Result := False;
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Codigo := 0;
    Controle := 0;
    XMLDocument.Active := False;
    Stream := TMemoryStream.Create;
    Linhas := TStringList.Create;
    try
      Linhas.Text := XML;
      Linhas.SaveToStream(Stream);
      try
        XMLDocument.LoadFromStream(Stream);
      except
        Exit;
      end;
    finally
      Stream.Free;
      Linhas.Free;
    end;
    //XMLDocument.FileName := Arquivo;
    XMLDocument.Active := True;
    //XMLDocument.DocumentElement.NodeValue;   // Erro
    xmlNodeB := XMLDocument.DocumentElement.ChildNodes.First;
    while xmlNodeB <> nil do
    begin
      for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
      begin
        X := xmlNodeB.AttributeNodes.Nodes[J];
      end;
      //
      if xmlNodeB.ChildNodes.Count = 1 then
      begin
        if (xmlNodeB.NodeType = ntElement) then
        begin
          if xmlNodeB.NodeName = 'idLote' then
            idLote := Geral.SoNumero_TT(xmlNodeB.NodeValue)
          else
          if xmlNodeB.NodeName = 'fld_Str_SenhaC' then
            fld_Str_SenhaC := xmlNodeB.NodeValue
          else
          if xmlNodeB.NodeName = 'fld_Str_SenhaP' then
            fld_Str_SenhaP := xmlNodeB.NodeValue
          else
          if xmlNodeB.NodeName = 'fld_Str_CodTxt' then
            fld_Str_CodTxt := xmlNodeB.NodeValue
          else
          if xmlNodeB.NodeName = 'fld_Str_Professor' then
            fld_Str_Professor := xmlNodeB.NodeValue
          else
          if xmlNodeB.NodeName <> '#Text' then
          Geral.MensagemBox(xmlNodeB.NodeName + #13#10 + xmlNodeB.NodeValue,
          'N� B desconhecido', MB_OK+MB_ICONINFORMATION);
        end
      end else
      begin
        Codigo := QrSWMS_Mtng_CabCodigo.Value;
        if fld_Str_SenhaC <> QrSWMS_Mtng_CabSenhaC.Value  then
        begin
          Geral.MensagemBox('Senha da competi��o n�o confere: ' + #13#10 +
          'Minha Senha: "' + QrSWMS_Mtng_CabSenhaC.Value + '"' + #13#10 +
          'Senha Equipe: "' + fld_Str_SenhaC + '"', 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end;
        QrLocIts.Close;
        QrLocIts.Params[00].AsInteger := Codigo;
        QrLocIts.Params[01].AsString  := fld_Str_SenhaP;
        QrLocIts.Open;
        if (QrLocIts.RecordCount = 0) or (QrLocItsControle.Value = 0) then
        begin
          Geral.MensagemBox('Senha da equipe n�o confere: ' + #13#10 +
          'Senha Equipe: "' + fld_Str_SenhaC + '"', 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end else Controle := QrLocItsControle.Value;
        xmlNodeC := xmlNodeB.ChildNodes.First;
        while xmlNodeC <> nil do
        begin
          FnFld := 0;
          FnIdx := 0;
          //N := N + 1;
          NomeNoh := xmlNodeC.NodeName;
          if NomeNoh = 'enviEmpresa' then
          begin
            if UMyMod.ExcluiRegistroInt2('', 'swms_envi_empresa',
            'Nivel1', 'Nivel2', Codigo, Controle) = ID_YES then
            DomToTree(xmlNodeC, 'swms_envi_empresa', 2);
          end
          else
          if NomeNoh = 'enviNadadores' then
          begin
            if UMyMod.ExcluiRegistroIntArr('', 'swms_envi_nadadores',
            ['Nivel1', 'Nivel2', 'Importado'], [Codigo, Controle, 1]) = ID_YES then
            DomToTree(xmlNodeC, 'swms_envi_nadadores', 2);
          end
          else
          if NomeNoh = 'enviCompetiPart' then
          begin
            if UMyMod.ExcluiRegistroIntArr('', 'swms_envi_competipart',
            ['Nivel1', 'Nivel2', 'Importado'], [Codigo, Controle, 1]) = ID_YES then
            DomToTree(xmlNodeC, 'swms_envi_competipart', 2);
          end
          else
            Geral.MensagemBox(xmlNodeC.NodeName, 'Nome do n� C', MB_OK+MB_ICONINFORMATION);
          //
          xmlNodeC := xmlNodeC.NextSibling;
        end;
      end;
      xmlNodeB := xmlNodeB.NextSibling;
    end;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_mtng_its', False, [
    'CNPJ_PAR', 'IP', 'Protocol',
    'XML', 'DataHora', 'CodTxt',
    'Professor'], [
    'Codigo', 'Controle'], [
    CNPJ_PAR, IP, Protocol,
    XML, DataHora, fld_Str_CodTxt,
    fld_Str_Professor], [Codigo, Controle], True);

    //
    Result := True;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmSWMS_Mtng_Cab.InsUpdPrv(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmSWMS_Mtng_PrvUni, FmSWMS_Mtng_PrvUni, afmoNegarComAviso,
  QrSWMS_Mtng_Prv, SQLType) then
  begin
    FmSWMS_Mtng_PrvUni.ShowModal;
    FmSWMS_Mtng_PrvUni.Destroy;
  end;
end;

procedure TFmSWMS_Mtng_Cab.ItemDeLiturgiaSelecionado1Click(Sender: TObject);
begin
{
################################################################################
######################  Liberar este texto ap�s criar form #####################
################################################################################

  if UmyMod.FormInsUpd_Cria(TFmSWMS_Mtng_Its, FmSWMS_Mtng_Its, afmoNegarComAviso,
    QrSWMS_Mtng_Its, stUpd)then
  begin
    FmSWMS_Mtng_Its.CkContinuar.Checked := False;
    //
    FmSWMS_Mtng_Its.ShowModal;
    FmSWMS_Mtng_Its.Destroy;
  end;
}
end;

procedure TFmSWMS_Mtng_Cab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSWMS_Mtng_Cab.Multiplas1Click(Sender: TObject);
begin
  if MyObjects.CriaForm_AcessoTotal(TFmSWMS_Mtng_PrvMul, FmSWMS_Mtng_PrvMul) then
  begin
    FmSWMS_Mtng_PrvMul.ShowModal;
    FmSWMS_Mtng_PrvMul.Destroy;
  end;
end;

procedure TFmSWMS_Mtng_Cab.Unica1Click(Sender: TObject);
begin
  InsUpdPrv(stIns);
end;

procedure TFmSWMS_Mtng_Cab.OrganizaFase1();
const
  Fase = 0;
var
  //Codigo, Indice, Serie, Raia, SeqSegCres,
  CategPart, Nivel2, Controle: Integer;
  //Continua: Boolean;
  //Res: MyArrStr;
  //I, N, R: Integer;
  Lista: TStringList;
begin
  Screen.Cursor := crHourGlass;
  Lista := TStringList.Create;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando categoria dos atletas!');
    QrSemCat.Close;
    QrSemCat.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
    QrSemCat.Open;
    if QrSemCat.RecordCount > 0 then
    begin
      QrSemCat.First;
      while not QrSemCat.Eof do
      begin
        Nivel2    := QrSemCatNivel2.Value;
        Controle  := QrSemCatControle.Value;
        CategPart := UnListas.ConfiguraCategoria(QrSWMS_Mtng_CabDataEvenIni.Value,
          QrSemCatNascim.Value, QrSWMS_Mtng_CabCategoriaIni.Value);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_envi_competipart', False, [
        'CategPart'], ['Nivel2', 'Controle'], [
        CategPart], [Nivel2, Controle], True);
        //
        QrSemCat.Next;
      end;
      //
{
      MyObjects.CriaForm_AcessoTotal(TFmSWMS_Mtng_Cat, FmSWMS_Mtng_Cat);
      FmSWMS_Mtng_Cat.ShowModal;
      FmSWMS_Mtng_Cat.Destroy;
}
    end;
{
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Organizando s�ries!');
    QrTem.Close;
    QrTem.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
    QrTem.Open;
    if QrTem.RecordCount = 0 then
      Continua := True
    else
      Continua := Geral.MensagemBox(
      'J� existem informa��es de tomada de tempo de prova!' + #13#10 +
      'Estas informa��es ser�o perdidas. Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES;
    //
}
{
    if Continua then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM swms_mtng_ser ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM swms_mtng_tem ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrSWMS_Mtng_Prv.First;
      while not QrSWMS_Mtng_Prv.Eof do
      begin
        Codigo := QrSWMS_Mtng_PrvCodigo.Value;
        Controle := QrSWMS_Mtng_PrvControle.Value;
        //
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT sec.*');
        Dmod.QrAux.SQL.Add('FROM swms_envi_competipart sec');
        Dmod.QrAux.SQL.Add('LEFT JOIN swms_envi_nadadores sen ');
        Dmod.QrAux.SQL.Add('     ON sen.Nivel1=sec.Nivel1');
        Dmod.QrAux.SQL.Add('     AND sen.Nivel2=sec.Nivel2');
        Dmod.QrAux.SQL.Add('     AND sen.Codigo=sec.Nadador');
        Dmod.QrAux.SQL.Add('WHERE sec.Nivel1=:P0');
        Dmod.QrAux.SQL.Add('AND sec.Estilo=:P1');
        Dmod.QrAux.SQL.Add('AND sec.Metros=:P2');
        Dmod.QrAux.SQL.Add('AND sec.Categoria BETWEEN :P3 AND :P4');
        if QrSWMS_Mtng_PrvSexo.Value = 'M' then
          Dmod.QrAux.SQL.Add('AND sen.Sexo="M"')
        else
        if QrSWMS_Mtng_PrvSexo.Value = 'F' then
          Dmod.QrAux.SQL.Add('AND sen.Sexo="F"');
        Dmod.QrAux.Params[00].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
        Dmod.QrAux.Params[01].AsInteger := QrSWMS_Mtng_PrvEstilo.Value;
        Dmod.QrAux.Params[02].AsInteger := QrSWMS_Mtng_PrvMetros.Value;
        Dmod.QrAux.Params[03].AsInteger := QrSWMS_Mtng_PrvCategoriaIni.Value;
        Dmod.QrAux.Params[04].AsInteger := QrSWMS_Mtng_PrvCategoriaFim.Value;
        Dmod.QrAux.Open;
        if Dmod.QrAux.RecordCount > 0 then
        begin
          Dmod.QrAux.First;
        //begin
          N := Dmod.QrAux.RecordCount;
          R := QrSWMS_Mtng_PrvPiscRaias.Value;
          if QrSWMS_Mtng_PrvPiorRaia.Value = 0 then
            Res := DistribuicaoNasRaias(N, R, lpFirst)
          else
            Res := DistribuicaoNasRaias(N, R, lpLast);
          for I := Low(Res) to High(Res) do
          begin
            Lista.Clear;
            Geral.MyExtractStrings(['|'], [' '], PChar(Res[I]), Lista);
            Indice := Geral.IMV(Lista[0]);
            Serie := Geral.IMV(Lista[1]);
            Raia := Geral.IMV(Lista[2]);
            if Lista.Count > 3 then
              SeqSegCres := Geral.IMV(Lista[3])
            else
              SeqSegCres := 0;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'swms_mtng_tem', False, [
            'Codigo', 'SeqSegCres', 'Raia'], [
            'Controle', 'Fase', 'Serie', 'Indice'], [
            Codigo, SeqSegCres, Raia], [
            Controle, Fase, Serie, Indice], True);
            //
          end;
        end;
        QrSWMS_Mtng_Prv.Next;
      end;
    end;
}    
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
    if Lista <> nil then
      Lista.Free;
  end;
end;

procedure TFmSWMS_Mtng_Cab.PMCabPopup(Sender: TObject);
begin
  UMyMod.HabilitaMenuItemInt([Alteracompeticaoatual1, Excluicompeticaoatual1,
    Regulamento1, Sincronizar1], QrSWMS_Mtng_Cab, '', 1, 0);
end;

procedure TFmSWMS_Mtng_Cab.PMExcluiPopup(Sender: TObject);
{
var
  Habilita1, Habilita2, Habilita3: Boolean;
}
begin
{
################################################################################
######################  Seguir este texto para excluir #########################
################################################################################
  Habilita1 := (QrLiturLinha.State = dsBrowse) and (QrLiturLinha.RecordCount > 0);
  ExcluiSubitemDoItemSelecionado1.Enabled := Habilita1;
  Habilita2 := (QrSWMS_Mtng_Its.State = dsBrowse) and (QrSWMS_Mtng_Its.RecordCount > 0);
  ExcluiItemDaLiturgiaAtual1.Enabled := (not Habilita1) and Habilita2;
  Habilita3 := (QrSWMS_Mtng_Cab.State = dsBrowse) and (QrSWMS_Mtng_Cab.RecordCount > 0);
  ExcluiLiturgiaAtual1.Enabled := (not Habilita2) and Habilita3;
}
end;

procedure TFmSWMS_Mtng_Cab.CabecalhoDaLiturgiaAtual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrSWMS_Mtng_Cab, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'swms_mtng_cab');
end;

procedure TFmSWMS_Mtng_Cab.CadastroCompetiPart(SQLType: TSQLType);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl3.ActivePageIndex := 0;
  if UMyMod.FormInsUpd_Cria(TFmSWMS_Envi_CompetiPart, FmSWMS_Envi_CompetiPart, afmoNegarComAviso,
  QrCompetiPart, SQLType) then
  begin
    FmSWMS_Envi_CompetiPart.EdNivel1.ValueVariant := QrSWMS_Mtng_ItsCodigo.Value;
    FmSWMS_Envi_CompetiPart.EdNivel2.ValueVariant := QrSWMS_Mtng_ItsControle.Value;
    FmSWMS_Envi_CompetiPart.ReopenSWMS_Envi_Nadadores();
    FmSWMS_Envi_CompetiPart.ShowModal;
    FmSWMS_Envi_CompetiPart.Destroy;
  end;
end;

procedure TFmSWMS_Mtng_Cab.CadastroNadadores(SQLType: TSQLType);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl3.ActivePageIndex := 1;
  //
  if UMyMod.FormInsUpd_Cria(TFmSWMS_Envi_Nadadores, FmSWMS_Envi_Nadadores, afmoNegarComAviso,
  QrNadadores, SQLType) then
  begin
    FmSWMS_Envi_Nadadores.EdNivel1.ValueVariant := QrSWMS_Mtng_ItsCodigo.Value;
    FmSWMS_Envi_Nadadores.EdNivel2.ValueVariant := QrSWMS_Mtng_ItsControle.Value;
    FmSWMS_Envi_Nadadores.ShowModal;
    FmSWMS_Envi_Nadadores.Destroy;
  end;
end;

{
function TFmSWMS_Mtng_Cab.CopiaParteTexto(Tag: String;
  XML: WideString): WideString;
var
  I, F: Integer;
begin
  I := pos('<' + Tag +' ', XML);
  F := pos('</' + Tag + '>', XML) + Length(Tag) + 3;
  Result := Copy(XML, I, F - I);
end;
}

procedure TFmSWMS_Mtng_Cab.CriaOForm;
begin
  ImgTipo.SQLType := stLok;
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSWMS_Mtng_Cab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSWMS_Mtng_Cab.Recria1Click(Sender: TObject);
begin
// Parei aqui
end;

procedure TFmSWMS_Mtng_Cab.Redefinirtodafaseclassificatria1Click(
  Sender: TObject);
var
  Res: MyArrStr;
  // Indice, Serie, Raia, SeqSegCres,
  swms_mtng_prv, Nivel2, Controle, I, N, R, 
  LocCtrl: Integer;
  //Lista: TStringList;
  OrdemCtrl, OrdemNiv2: array of Integer;
begin
  Screen.Cursor := crHourGlass;
  //Lista := TStringList.Create;
  try
    if QrSWMS_Mtng_Prv.State <> dsInactive then
      LocCtrl := QrSWMS_Mtng_PrvControle.Value
    else
      LocCtrl := 0;
    PB1.Position := 0;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enquadrando nadadores nas categorias!');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPart, Dmod.MyDB, [
    'SELECT n.Sexo, c.* ',
    'FROM swms_envi_competipart c ',
    'LEFT JOIN swms_envi_nadadores n ON n.Nivel1=c.Nivel1 ',
    '  AND n.Nivel2=c.Nivel2 AND c.Nadador=n.Codigo ',
    'WHERE c.Nivel1=' + FormatFloat('0', QrSWMS_Mtng_CabCodigo.Value)
    ]);
    PB1.Max := QrPart.RecordCount;
    QrPart.First;
    while not QrPart.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Enquadrando nadadores nas categorias.. ' +
      FormatFloat('0', QrPart.RecNo) + ' de ' +
      FormatFloat('0', QrPart.RecordCount));
  {
    SELECT Controle
    FROM swms_mtng_prv
    WHERE Codigo=:P0
    AND Estilo=:P1
    AND Metros=:P2
    AND Sexo=:P3
    AND :P4 BETWEEN CategoriaIni AND CategoriaFim
  }
      QrProv.Close;
      QrProv.Params[00].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
      QrProv.Params[01].AsInteger := QrPartEstilo.Value;
      QrProv.Params[02].AsInteger := QrPartMetros.Value;
      QrProv.Params[03].AsString  := QrPartSexo.Value;
      QrProv.Params[04].AsInteger := QrPartCategoria.Value;
      QrProv.Open;
      //
      swms_mtng_prv := QrProvControle.Value;
      Nivel2 := QrPartNivel2.Value;
      Controle := QrPartControle.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_envi_competipart', False, [
      'swms_mtng_prv'], ['Nivel2', 'Controle'], [
      swms_mtng_prv], [Nivel2, Controle], True);
      //
      QrPart.Next;
    end;

    //

    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Organizando largadas!');
    QrPrvs.Close;
    QrPrvs.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
    QrPrvs.Open;
    ReopenSWMS_Mtng_Prv(0);
    QrPrvs.First;
    while not QrPrvs.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Organizando largadas.. ' +
      FormatFloat('0', QrPrvs.RecNo) + ' de ' +
      FormatFloat('0', QrPrvs.RecordCount));
      //
      QrLarg.Close;
      QrLarg.Params[0].AsInteger := QrPrvsControle.Value;
      QrLarg.Open;
      if QrLarg.RecordCount > 0 then
      begin
        for I := 0 to High(OrdemCtrl) do
          OrdemCtrl[I] := 0;
        for I := 0 to High(OrdemNiv2) do
          OrdemNiv2[I] := 0;
        SetLength(OrdemCtrl, QrLarg.RecordCount);
        SetLength(OrdemNiv2, QrLarg.RecordCount);
        QrLarg.First;
        while not QrLarg.Eof do
        begin
          OrdemCtrl[QrLarg.RecNo - 1] := QrLargControle.Value;
          OrdemNiv2[QrLarg.RecNo - 1] := QrLargNivel2.Value;
          //
          QrLarg.Next;
        end;
        //
        N := QrLarg.RecordCount;
        R := QrPrvsPiscRaias.Value;
        if QrPrvsPiorRaia.Value = 0 then
          Res := DistribuicaoNasRaias2(N, R, lpFirst, OrdemCtrl, OrdemNiv2)
        else
          Res := DistribuicaoNasRaias2(N, R, lpLast, OrdemCtrl, OrdemNiv2);
      end;
      //
      QrPrvs.Next;
    end;


    // Series

{   Precisa Fazer para imprimir?
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Organizando series!');
    QrPrvs.Close;
    QrPrvs.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
    QrPrvs.Open;
    QrPrvs.First;
    while not QrPrvs.Eof do
    begin
      QrSers.Close;
      QrSers.Params[0].AsInteger := QrPrvsControle.Value;
      QrSers.Open;
      //
      //
      QrPrvs.Next;
    end;
    //
}
    //
    if LocCtrl <> 0 then
      QrSWMS_Mtng_Prv.Locate('Controle', LocCtrl, []);
    //  
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    PB1.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSWMS_Mtng_Cab.ReopenFaseClassificatoria(Controle: Integer);
begin
  QrFaseA.Close;
  QrFaseA.Params[0].AsInteger := QrSWMS_Mtng_PrvControle.Value;
  QrFaseA.Open;
  //
  QrFaseA.Locate('Controle', Controle, []);
end;

procedure TFmSWMS_Mtng_Cab.ReopenSWMS_Envi_CompetiPart(Controle: Integer);
var
  FiltroNome, FiltroEstilo, FiltroCateg, FiltroMetros: String;
  //sSexo,
  sEstilo, sMetros, sCatego: String;
begin
  sEstilo := UnListas.ArrayToTexto('sec.Estilo', 'NO_ESTILO', pvPos, True, sListaEstilosNatacao);
  sMetros := UnListas.ArrayToTexto('sec.Metros', 'NO_METROS', pvPos, True, sListaMetrosNatacao);
  sCatego := UnListas.ArrayToTexto('sec.Categoria', 'NO_CATEGORIA', pvPos, True, sListaCategoriasNatacao);
  //
  if Trim(EdFiltroPartNome.Text) = '' then
    FiltroNome := ''
  else
    FiltroNome := 'AND sen.Nome LIKE "%' + EdFiltroPartNome.Text + '%"';
  //
  if Trim(EdFiltroPartEstilo.Text) = '' then
    FiltroEstilo := ''
  else
    FiltroEstilo := 'AND ' +
    UnListas.ArrayToTexto('sec.Estilo', '', pvNo, True, sListaEstilosNatacao) +
    ' LIKE "%' + EdFiltroPartEstilo.Text + '%"';
  //
  if Trim(EdFiltroPartCateg.Text) = '' then
    FiltroCateg := ''
  else
    FiltroCateg := 'AND ' +
    UnListas.ArrayToTexto('sec.Categoria', '', pvNo, True, sListaCategoriasNatacao) +
    ' LIKE "%' + EdFiltroPartCateg.Text + '%"';
  //
  if Trim(EdFiltroPartMetros.Text) = '' then
    FiltroMetros := ''
  else
    FiltroMetros := 'AND ' +
    UnListas.ArrayToTexto('sec.Metros', '', pvNo, True, sListaMetrosNatacao) +
    ' LIKE "%' + EdFiltroPartMetros.Text + '%"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCompetiPart, Dmod.MyDB, [
  'SELECT sen.Nome NO_NADADOR, ',
  sEstilo,
  sMetros,
  sCatego,
  'sec.* ',
  'FROM swms_envi_competipart sec ',
  'LEFT JOIN swms_envi_nadadores sen ON sen.Nivel1=sec.Nivel1 ',
  '     AND sen.Nivel2=sec.Nivel2' ,
  '     AND sen.Codigo=sec.Nadador ',
  'WHERE sec.Nivel1=' + FormatFloat('0', QrSWMS_Mtng_ItsCodigo.Value),
  'AND sec.Nivel2=' + FormatFloat('0', QrSWMS_Mtng_ItsControle.Value),
  FiltroNome,
  FiltroEstilo,
  FiltroCateg,
  FiltroMetros
  ]);
  //
  QrCompetiPart.Locate('Controle', Controle, []);
end;

procedure TFmSWMS_Mtng_Cab.ReopenSWMS_Envi_Nadadores(Codigo: Integer);
var
  FiltroNome: String;
begin
  if Trim(EdFiltroNadNome.Text) = '' then
    FiltroNome := ''
  else
    FiltroNome := 'AND Nome LIKE "%' + EdFiltroNadNome.Text + '%"';
  UnDmkDAC_PF.AbreMySQLQuery0(QrNadadores, Dmod.MyDB, [
  'SELECT * ',
  'FROM swms_envi_nadadores ',
  'WHERE Nivel1=' + FormatFloat('0', QrSWMS_Mtng_ItsCodigo.Value),
  'AND Nivel2=' + FormatFloat('0', QrSWMS_Mtng_ItsControle.Value),
  FiltroNome
  ]);
  //
  QrNadadores.Locate('Codigo', Codigo, []);
end;

procedure TFmSWMS_Mtng_Cab.ReopenSWMS_Mtng_Its(Controle: Integer);
begin
  QrSWMS_Mtng_Its.Close;
  QrSWMS_Mtng_Its.Params[0].AsInteger := QrSWMS_Mtng_CabCodigo.Value;
  QrSWMS_Mtng_Its.Open;
  //
  QrSWMS_Mtng_Its.Locate('Controle', Controle, []);
end;

procedure TFmSWMS_Mtng_Cab.ReopenSWMS_Mtng_Prv(Controle: Integer);
const
  ListaOrdem: array[0..3] of String = ('sty.Ordem, prv.Estilo', 'prv.Metros',
  'prv.CategoriaIni, prv.CategoriaFim', 'prv.Sexo');
var
  //sSexo, 
  sEstilo, sMetros, sCatIni, sCatFim, Ordem: String;
begin
  sEstilo := UnListas.ArrayToTexto('prv.Estilo', 'NO_ESTILO', pvPos, True, sListaEstilosNatacao);
  sMetros := UnListas.ArrayToTexto('prv.Metros', 'NO_METROS', pvPos, True, sListaMetrosNatacao);
  //sSexo   := UnListas.ArrayToTexto('Sexo', 'NO_SEXO', pvPos, True, sListaSexos);
  sCatIni := UnListas.ArrayToTexto('prv.CategoriaIni', 'NO_CATEGORIAINI', pvPos, True, sListaCategoriasNatacao);
  sCatFim := UnListas.ArrayToTexto('prv.CategoriaFim', 'NO_CATEGORIAFIM', pvPos, True, sListaCategoriasNatacao);
  Ordem   := ListaOrdem[QrSWMS_Mtng_CabOrdProva1.Value] + ',' +
             ListaOrdem[QrSWMS_Mtng_CabOrdProva2.Value] + ',' +
             ListaOrdem[QrSWMS_Mtng_CabOrdProva3.Value] + ',' +
             ListaOrdem[QrSWMS_Mtng_CabOrdProva4.Value];
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSWMS_Mtng_Prv, Dmod.MyDB, [
  'SELECT prv.Codigo, prv.Controle, prv.CodTxt, prv.Estilo,',
  sEstilo,
  'prv.Metros,',
  sMetros,
  'prv.Sexo, prv.PiscDescr, prv.PiscCompr, prv.PiscRaias,',
  'prv.CategoriaIni,',
  sCatIni,
  'prv.CategoriaFim,',
  sCatFim,
  'prv.PiorRaia',
  'FROM swms_mtng_prv prv',
  'LEFT JOIN swms_mtng_sty sty ON sty.Codigo=prv.Codigo AND sty.Estilo=prv.Estilo',
  'WHERE prv.Codigo=' + FormatFloat('0', QrSWMS_Mtng_CabCodigo.Value),
  'ORDER BY ' + Ordem
  ]);
  //
  //MLAGeral.LeMeuSQLy(QrSWMS_Mtng_Prv, '', nil, True, True);
  QrSWMS_Mtng_Prv.Locate('Controle', Controle, []);
end;

procedure TFmSWMS_Mtng_Cab.ReopenSWMS_Mtng_Sty(Estilo: Integer);
var
  sEstilo: String;
begin
  sEstilo := UnListas.ArrayToTexto('Estilo', 'NO_ESTILO', pvNo, True, sListaEstilosNatacao);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSWMS_Mtng_Sty, Dmod.MyDB, [
  'SELECT Codigo, Estilo, Ordem, FatorPtos,',
  sEstilo,
  'FROM swms_mtng_sty',
  'WHERE Codigo=' + FormatFloat('0', QrSWMS_Mtng_CabCodigo.Value),
  'AND Ordem > 0',
  'ORDER BY Ordem, Estilo'
  ]);
  //
  QrSWMS_Mtng_Sty.Locate('Estilo', Estilo, []);
end;

procedure TFmSWMS_Mtng_Cab.Reorganizarfaseclassificatria1Click(Sender: TObject);
begin
 // Fazer
  OrganizaFase1();
end;

procedure TFmSWMS_Mtng_Cab.DBEdit24Change(Sender: TObject);
begin
  DBEdCategoriaIni_TXT.Text :=
    UnListas.CategoriasNatacao_Get(QrSWMS_Mtng_CabCategoriaIni.Value);
end;

procedure TFmSWMS_Mtng_Cab.DBEdit25Change(Sender: TObject);
begin
  DBEdCategoriaFim_TXT.Text :=
    UnListas.CategoriasNatacao_Get(QrSWMS_Mtng_CabCategoriaFim.Value);
end;

procedure TFmSWMS_Mtng_Cab.DBGrid4DblClick(Sender: TObject);
var
  TempoA: Double;
  Nivel2, Controle: Integer;
begin
  MyObjects.CriaForm_AcessoTotal(TFmSWMS_Mtng_Tem, FmSWMS_Mtng_Tem);
  MyObjects.Informa2(FmSWMS_Mtng_Tem.LaAviso1, FmSWMS_Mtng_Tem.LaAviso2, True,
    QrFaseANO_NADADOR.Value);
  FmSWMS_Mtng_Tem.ShowModal;
  if FmSWMS_Mtng_Tem.FConfirmou then
  begin
    TempoA := FmSWMS_Mtng_Tem.EdTempo.ValueVariant;
    Nivel2 := QrFaseANivel2.Value;
    Controle := QrFaseAControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_envi_competipart', False, [
    'TempoA'], ['Nivel2', 'Controle'], [
    TempoA], [Nivel2, Controle], True) then
      ReopenFaseClassificatoria(Controle);
  end;
  FmSWMS_Mtng_Tem.Destroy;
end;

procedure TFmSWMS_Mtng_Cab.Definecategoriasdaprovaselecionada1Click(
  Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1, False);
  for Codigo := Low(sListaCategoriasNatacao) to High(sListaCategoriasNatacao) do
  begin
    Nome := sListaCategoriasNatacao[Codigo];
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'pesqesel', False, [
    'Nome'], ['Codigo'], [Nome], [Codigo], False);
  end;
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'DELETE FROM pesqesel',
    'WHERE Codigo IN (',
    '     SELECT Categoria',
    '     FROM ' + TMeuDB + '.swms_mtng_cts',
    '     WHERE Codigo=' + FormatFloat('0', QrSWMS_Mtng_CabCodigo.Value),
    ')']);
  //
  MyObjects.CriaForm_AcessoTotal(TFmSelIts, FmSelIts);
  FmSelIts.ShowModal;
  FmSelIts.Destroy;
end;

procedure TFmSWMS_Mtng_Cab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSWMS_Mtng_Cab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSWMS_Mtng_Cab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSWMS_Mtng_Cab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSWMS_Mtng_Cab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSWMS_Mtng_Cab.Altera1Click(Sender: TObject);
begin
  CadastroNadadores(stUpd);
end;

procedure TFmSWMS_Mtng_Cab.Altera2Click(Sender: TObject);
begin
  CadastroCompetiPart(stUpd);
end;

procedure TFmSWMS_Mtng_Cab.Alteraaprovaselecionada1Click(Sender: TObject);
begin
  InsUpdPrv(stUpd);
end;

procedure TFmSWMS_Mtng_Cab.Alteracompeticaoatual1Click(Sender: TObject);
var
  Estilo, Ordem, FatorPtos: Integer;
  Num: String;
begin
  for Estilo := 1 to 7 do
  begin
    if QrSWMS_Mtng_Sty.Locate('Estilo', Estilo, []) then
    begin
      Ordem := QrSWMS_Mtng_StyOrdem.Value;
      FatorPtos := QrSWMS_Mtng_StyFatorPtos.Value;
    end else begin
      Ordem := 0;
      FatorPtos := Estilo div 6 + 1;
    end;
    Num := FormatFloat('0', Estilo);
    if (Self.FindComponent('EdOrdem' + Num) as TdmkEdit) <> nil then
      TdmkEdit(Self.FindComponent('EdOrdem' + Num) as TdmkEdit).ValueVariant := Ordem;
    if (Self.FindComponent('EdFator' + Num) as TdmkEdit) <> nil then
      TdmkEdit(Self.FindComponent('EdFator' + Num) as TdmkEdit).ValueVariant := FatorPtos;
  end;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrSWMS_Mtng_Cab, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'swms_mtng_cab');
end;

procedure TFmSWMS_Mtng_Cab.BtItsClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmSWMS_Mtng_Cab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSWMS_Mtng_CabCodigo.Value;
  Close;
end;

procedure TFmSWMS_Mtng_Cab.Button1Click(Sender: TObject);
var
  Res: MyArrStr;
  I, N, R: Integer;
begin
  N := EdPart.ValueVariant;
  R := EdRais.ValueVariant;
  Res := DistribuicaoNasRaias(N, R, lpFirst);
  MeLista.Lines.Clear;
  for I := Low(Res) to High(Res) do
    //Geral.MyExtractStrings(['|'], [' '], PChar(MeImportado.Lines[0]), Lista);
    MeLista.Lines.Add(Res[I]);
end;

procedure TFmSWMS_Mtng_Cab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Num: String;
  Ordem, FatorPtos, Estilo: Integer;
begin
  EdSincronia.ValueVariant := 0;
  Nome := EdNome.Text;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o.') then
    Exit;
  if MyObjects.FIC(Length(EdCNPJ_EMP.Text) = 0, EdNome, 'Informe o CNPJ.') then
    Exit;
  if CkAtivo.Checked = False then
    if Geral.MensagemBox('Tem certeza que deseja inativar esta competi��o?' +
    #13#10 + 'Ao inativar e sincronizar, ela ser� exclu�da do webservice!',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    Exit ;
  Codigo := UMyMod.BuscaEmLivreY_Def('swms_mtng_cab', 'Codigo', ImgTipo.SQLType,
    QrSWMS_Mtng_CabCodigo.Value);
  EdCodUsu.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmSWMS_Mtng_Cab, PainelEdita,
    'swms_mtng_cab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM swms_mtng_sty WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    for Estilo := 1 to 7 do
    begin
      Ordem := 0;
      FatorPtos := 0;
      Num := FormatFloat('0', Estilo);
      if (Self.FindComponent('EdOrdem' + Num) as TdmkEdit) <> nil then
        Ordem := TdmkEdit(Self.FindComponent('EdOrdem' + Num) as TdmkEdit).ValueVariant;
      if (Self.FindComponent('EdFator' + Num) as TdmkEdit) <> nil then
        FatorPtos := TdmkEdit(Self.FindComponent('EdFator' + Num) as TdmkEdit).ValueVariant;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'swms_mtng_sty', False, [
      'Ordem', 'FatorPtos'], ['Codigo', 'Estilo'], [
      Ordem, FatorPtos], [
      Codigo, Estilo], True);
    end;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSWMS_Mtng_Cab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'swms_mtng_cab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'swms_mtng_cab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'swms_mtng_cab', 'Codigo');
end;

procedure TFmSWMS_Mtng_Cab.BtProvaClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  MyObjects.MostraPopUpDeBotao(PMProva, BtProva);
end;

procedure TFmSWMS_Mtng_Cab.BtCabClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmSWMS_Mtng_Cab.FormCreate(Sender: TObject);
begin
  QrSWMS_Mtng_Prv.Database := Dmod.MyDB;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  PainelEdita.Align  := alClient;
  CriaOForm;
end;

procedure TFmSWMS_Mtng_Cab.SbNumeroClick(Sender: TObject);
begin
  GOTOy.CodigoNew(QrSWMS_Mtng_CabCodigo.Value, GBReg, [LaReg1,LaReg3]);
end;

procedure TFmSWMS_Mtng_Cab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxSWMS_Mtng_Cab_03_00, 'Anota��o de tempos');
end;

procedure TFmSWMS_Mtng_Cab.SbNomeClick(Sender: TObject);
begin
  GOTOy.NomeNew(GBReg, [LaReg1,LaReg3]);
end;

procedure TFmSWMS_Mtng_Cab.SbNovoClick(Sender: TObject);
begin
  //GOTOy.CodUsuNew(QrSWMS_Mtng_CabCodUsu.Value, GBReg, [LaReg1,LaReg3]);
end;

procedure TFmSWMS_Mtng_Cab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSWMS_Mtng_Cab.QrCompetiPartCalcFields(DataSet: TDataSet);
begin
  QrCompetiPartTempo_TXT.Value := FormatDateTime('hh:nn:ss,zzz',
    QrCompetiPartTempo.Value);
{
  QrCompetiPartNO_ESTILO.Value := sListaEstilosNatacao[QrCompetiPartEstilo.Value];
  QrCompetiPartNO_METROS.Value := sListaMetrosNatacao[QrCompetiPartMetros.Value];
  QrCompetiPartNO_CATEGORIA.Value := sListaCategoriasNatacao[QrCompetiPartCategoria.Value];
}
end;

procedure TFmSWMS_Mtng_Cab.QrFaseACalcFields(DataSet: TDataSet);
begin
  QrFaseATEMPO_STR.Value := FormatDateTime('hh:nn:ss,zzz', QrFaseATempo.Value);
  QrFaseATEMPOA_STR.Value := FormatDateTime('hh:nn:ss,zzz', QrFaseATempoA.Value);
end;

procedure TFmSWMS_Mtng_Cab.QrNadadoresCalcFields(DataSet: TDataSet);
begin
  QrNadadoresFone_TXT.Value  := Geral.FormataTelefone_TT(QrNadadoresFone.Value);
  QrNadadoresCPF_TXT.Value  := Geral.FormataCNPJ_TT(QrNadadoresCPF.Value);
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_CabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_CabAfterScroll(DataSet: TDataSet);
const
  MyHTM = 'C:\_Teste_.htm';
begin
  ReopenSWMS_Mtng_Its(0);
  ReopenSWMS_Mtng_Prv(0);
  ReopenSWMS_Mtng_Sty(0);
  //
  TbRegulam.Close;
  TbRegulam.Filter := 'Codigo=' + FormatFloat('0', QrSWMS_Mtng_CabCodigo.Value);
  TbRegulam.Filtered := True;
  TbRegulam.Open;
  //
  if FileExists(MyHTM) then
    DeleteFile(MyHTM);
  DBMemo1.Lines.SaveToFile(MyHTM);
  WebBrowser1.Navigate(MyHTM);
  WebBrowser1.Visible := True;
  //
  if QrSWMS_Mtng_CabSincronia.Value = 0 then
  begin
    LaSincro1.Caption := 'N�o sincronizado';
    LaSincro3.Caption := 'N�o sincronizado';
    //
    LaSincro1.Font.Color := clSilver;
    LaSincro3.Font.Color := clRed;
  end else
  begin
    LaSincro1.Caption := 'Sincronizado';
    LaSincro3.Caption := 'Sincronizado';
    //
    LaSincro1.Font.Color := clSilver;
    LaSincro3.Font.Color := clBlue;
  end;
  LaSincro1.Visible := True;
  LaSincro3.Visible := True;
end;

procedure TFmSWMS_Mtng_Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSWMS_Mtng_Cab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSWMS_Mtng_CabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'swms_mtng_cab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSWMS_Mtng_Cab.SetaValorCampo(var Campos: array of String; var Values:
array of Variant; NomeNoh: String; Valor: Variant; Index: Integer);
var
  Tipo, Campo: String;
begin
  Campo := Copy(NomeNoh, 9);
  Campos[Index] := Campo;
  if Valor <> Null then
  begin
    Tipo := Copy(NomeNoh, 5, 3);
    //
    if Tipo = 'int' then
       Values[Index] := FormatFloat('0', Geral.IMV(Valor))
    else
    if Tipo = 'i64' then
       Values[Index] := FormatFloat('0', Geral.I64(Valor))
    else
    if Tipo = 'dbl' then
       Values[Index] := FormatFloat('0.000000000000000', Geral.DMV(Valor))
    else
    if Tipo = 'dta' then
       Values[Index] := Geral.FDT(Geral.ValidaData_YYYY_MM_DD(Valor), 1)
    else
    if Tipo = 'hra' then
    begin
      if Campo = 'tempo' then
        Values[Index] := 0.00000000000000 + StrToTime(Valor)// milisegundos s�o float!
      else
        Values[Index] := StrToTime(Valor)// milisegundos s�o float!
    end else
    //if Tipo = 'chr', 'txt', 'str', 'num',  then
       Values[Index] := Valor
  end else
  Values[Index] := '';
end;

procedure TFmSWMS_Mtng_Cab.Sincronizar1Click(Sender: TObject);
var
  ResCab, ResIts: ArrayOfString;
  //
  Codigo, CodUsu, CategoriaIni, CategoriaFim: Integer;
  CNPJ_EMP, Nome, Local, DtInscrIni, DtInscrFim, DataEvenIni, HoraEvenIni,
  DataEvenFim, HoraEvenFim, SenhaC, ContatoNome, ContatoFone, ContatoMail,
  Observ_0, Observ_1, Observ_2, Observ_3, Observ_4, Observ_5, Observ_6,
  Observ_7, Observ_8, Observ_9: String;
  Ativo: Boolean;
  Regulam: WideString;
  //
  Controle: Integer;
  Senha: String;
  //
  Sincronia: Integer;
begin
  Codigo       := QrSWMS_Mtng_CabCodigo.Value;
  CodUsu       := QrSWMS_Mtng_CabCodUsu.Value;
  CNPJ_EMP     := QrSWMS_Mtng_CabCNPJ_EMP.Value;
  Nome         := QrSWMS_Mtng_CabNome.Value;
  Local        := QrSWMS_Mtng_CabLocal.Value;
  DtInscrIni   := Geral.FDT(QrSWMS_Mtng_CabDtInscrIni.Value, 1);
  DtInscrFim   := Geral.FDT(QrSWMS_Mtng_CabDtInscrFim.Value, 1);
  DataEvenIni  := Geral.FDT(QrSWMS_Mtng_CabDataEvenIni.Value, 1);
  HoraEvenIni  := Geral.FDT(QrSWMS_Mtng_CabHoraEvenIni.Value, 100);
  DataEvenFim  := Geral.FDT(QrSWMS_Mtng_CabDataEvenFim.Value, 1);
  HoraEvenFim  := Geral.FDT(QrSWMS_Mtng_CabHoraEvenFim.Value, 100);
  SenhaC       := QrSWMS_Mtng_CabSenhaC.Value;
  ContatoNome  := QrSWMS_Mtng_CabContatoNome.Value;
  ContatoFone  := QrSWMS_Mtng_CabContatoFone.Value;
  ContatoMail  := QrSWMS_Mtng_CabContatoMail.Value;
  Observ_0     := QrSWMS_Mtng_CabObserv_0.Value;
  Observ_1     := QrSWMS_Mtng_CabObserv_1.Value;
  Observ_2     := QrSWMS_Mtng_CabObserv_2.Value;
  Observ_3     := QrSWMS_Mtng_CabObserv_3.Value;
  Observ_4     := QrSWMS_Mtng_CabObserv_4.Value;
  Observ_5     := QrSWMS_Mtng_CabObserv_5.Value;
  Observ_6     := QrSWMS_Mtng_CabObserv_6.Value;
  Observ_7     := QrSWMS_Mtng_CabObserv_7.Value;
  Observ_8     := QrSWMS_Mtng_CabObserv_8.Value;
  Observ_9     := QrSWMS_Mtng_CabObserv_9.Value;
  Ativo        := QrSWMS_Mtng_CabAtivo.Value = 1;
  Regulam      := QrSWMS_Mtng_CabRegulam.Value;
  CategoriaIni := QrSWMS_Mtng_CabCategoriaIni.Value;
  CategoriaFim := QrSWMS_Mtng_CabCategoriaFim.Value;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando dados gerais da competi��o!');
  ResCab := GetVCLWebServicePortType.SWMSAtualizaCompet(
  Codigo, CodUsu, CNPJ_EMP, Nome, Local,
      DtInscrIni, DtInscrFim, DataEvenIni, HoraEvenIni, DataEvenFim, HoraEvenFim,
      SenhaC, CategoriaIni, CategoriaFim,
      ContatoNome, ContatoFone, ContatoMail, Regulam,
      Observ_0, Observ_1, Observ_2, Observ_3, Observ_4, Observ_5, Observ_6,
      Observ_7, Observ_8, Observ_9, Ativo);
  if Geral.IMV(ResCab[0]) = 1 then
  begin
    QrSWMS_Mtng_Its.First;
    while not QrSWMS_Mtng_Its.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando dados da equipe "' +
        QrSWMS_Mtng_ItsNome.Value + '"');
      Controle := QrSWMS_Mtng_ItsControle.Value;
      Senha := QrSWMS_Mtng_ItsSenhaP.Value;
      Ativo := QrSWMS_Mtng_ItsAtivo.Value = 1;
      ResIts := GetVCLWebServicePortType.SWMSAtualizaCompetIts(Codigo, Controle,
      Senha, Ativo);
      if Geral.IMV(ResIts[0]) <> 1 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'ERRO: ' + ResIts[1]);
        Exit;
      end;
      //
      QrSWMS_Mtng_Its.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    Sincronia := 1;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'swms_mtng_cab', False, [
    'Sincronia'], [ 'Codigo'], [
    Sincronia], [Codigo], True);
  end else MyObjects.Informa2(LaAviso1, LaAviso2, False, ResCab[1]);
  LocCod(Codigo, Codigo);
end;

procedure TFmSWMS_Mtng_Cab.Sincronizar2Click(Sender: TObject);
var
  Res: ArrayOfstring;
  //
  cRet: Integer;
  Mensagem, CNPJ_PAR, IP, Protocol: String;
  DataHora: TDateTime;
  XML: WideString;
  Mudou: Boolean;
begin
  Mudou := True;
  QrSWMS_Mtng_Its.First;
  while not QrSWMS_Mtng_Its.Eof do
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Verificando alera��es na equipe: ' + QrSWMS_Mtng_ItsNome.Value);
    Res := GetVCLWebServicePortType.SWMSObtemXML(QrSWMS_Mtng_ItsCodigo.Value,
      QrSWMS_Mtng_ItsControle.Value);
    cRet := Geral.IMV(Res[0]);
    Mensagem := (Res[1]);
    if cRet = 1 then
    begin
      CNPJ_PAR := (Res[2]);
      IP := (Res[3]);
      Protocol := (Res[4]);
      XML := (Res[5]);
      DataHora := Geral.ValidaData_YYYY_MM_DD(Res[6]);
      if DataHora <> QrSWMS_Mtng_ItsDataHora.Value then
      begin
        if InsereDadosTabelas(CNPJ_PAR, IP, Protocol, DataHora, XML) then
          Mudou := True
        else  
          Geral.MensagemBox('Equipe: ' + QrSWMS_Mtng_ItsNome.Value + #13#10 +
          'N�o foi poss�vel ler os dados do XML!',
          'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end else Geral.MensagemBox('Equipe: ' + QrSWMS_Mtng_ItsNome.Value + #13#10 +
    'Retorno de erro ' + FormatFloat('0', cRet) + #13#10 + Mensagem,
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    QrSWMS_Mtng_Its.Next;
  end;
  LocCod(QrSWMS_Mtng_CabCodigo.Value, QrSWMS_Mtng_CabCodigo.Value);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  if Mudou then
    OrganizaFase1();
end;

procedure TFmSWMS_Mtng_Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2, LaAviso3, LaAviso4], Caption, True, taLeftJustify, 10, 10, 20);
end;

procedure TFmSWMS_Mtng_Cab.frxSWMS_Mtng_Cab_03_00GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_TITULO') = 0 then
    Value := QrSWMS_Mtng_CabNome.Value
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_CabBeforeClose(DataSet: TDataSet);
begin
  QrSWMS_Mtng_Its.Close;
  TbRegulam.Close;
  WebBrowser1.Visible := False;
  LaSincro1.Visible := False;
  LaSincro3.Visible := False;
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_CabBeforeOpen(DataSet: TDataSet);
begin
  QrSWMS_Mtng_CabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_CabCalcFields(DataSet: TDataSet);
begin
  QrSWMS_Mtng_CabContatoFone_TXT.Value := Geral.FormataTelefone_TT(
    QrSWMS_Mtng_CabContatoFone.Value);
  QrSWMS_Mtng_CabCNPJ_EMP_TXT.Value := Geral.FormataCNPJ_TT(QrSWMS_Mtng_CabCNPJ_EMP.Value);
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_ItsAfterScroll(DataSet: TDataSet);
begin
  ReopenSWMS_Envi_CompetiPart(0);
  ReopenSWMS_Envi_Nadadores(0);
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_ItsBeforeClose(DataSet: TDataSet);
begin
  QrCompetiPart.Close;
  QrNadadores.Close;
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_ItsCalcFields(DataSet: TDataSet);
begin
  QrSWMS_Mtng_ItsDataHora_TXT.Value := Geral.FDT(
    QrSWMS_Mtng_ItsDataHora.Value, 106, True);
  QrSWMS_Mtng_ItsCNPJ_PAR_TXT.Value :=
    Geral.FormataCNPJ_TT(QrSWMS_Mtng_ItsCNPJ_PAR.Value);
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_PrvAfterScroll(DataSet: TDataSet);
begin
  ReopenFaseClassificatoria(0);
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_PrvBeforeClose(DataSet: TDataSet);
begin
  QrFaseA.Close;
end;

procedure TFmSWMS_Mtng_Cab.QrSWMS_Mtng_PrvCalcFields(DataSet: TDataSet);
begin
  QrSWMS_Mtng_PrvSEQ.Value := QrSWMS_Mtng_Prv.RecNo;
  if QrSWMS_Mtng_PrvSexo.Value = 'F' then
    QrSWMS_Mtng_PrvNO_SEXO.Value := 'Feminino'
  else
  if QrSWMS_Mtng_PrvSexo.Value = 'M' then
    QrSWMS_Mtng_PrvNO_SEXO.Value := 'Masculino'
  else
    QrSWMS_Mtng_PrvNO_SEXO.Value := '? ? ? ? ?'
{
  QrSWMS_Mtng_PrvNomeCompleto.Value :=
    FormatFloat('0', QrSWMS_Mtng_Prv.RecNo) + '� PROVA ' +
    sListaMetro2Natacao[QrSWMS_Mtng_PrvMetros.Value] + 'm ' +
    sListaEstilosNatacao[QrSWMS_Mtng_PrvEstilo.Value] + ' ' +
    sListaCategoriasNatacao[QrSWMS_Mtng_PrvCategoriaIni.Value
}
end;

end.

