unit Periodos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, ImgList, Menus, dmkGeral, dmkEdit, UnDmkProcFunc;

type
  TFmPeriodos = class(TForm)
    PainelDados: TPanel;
    DsPeriodos: TDataSource;
    QrPeriodos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Painel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrPeriodosCodigo: TIntegerField;
    QrPeriodosNome: TWideStringField;
    QrPeriodosLk: TIntegerField;
    QrPeriodosDataCad: TDateField;
    QrPeriodosDataAlt: TDateField;
    QrPeriodosUserCad: TIntegerField;
    QrPeriodosUserAlt: TIntegerField;
    Label3: TLabel;
    EdHIni: TdmkEdit;
    Label8: TLabel;
    EdHFim: TdmkEdit;
    RGDSem: TRadioGroup;
    QrPeriodosDSem: TIntegerField;
    QrPeriodosHIni: TTimeField;
    QrPeriodosHFim: TTimeField;
    QrPeriodosHINI_TXT: TWideStringField;
    QrPeriodosHFIM_TXT: TWideStringField;
    QrPeriodosDSEM_TXT: TWideStringField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    QrPeriodosTempo_TXT: TWideStringField;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    QrLoc: TmySQLQuery;
    QrPeriodos2: TmySQLQuery;
    QrPeriodos2Codigo: TIntegerField;
    QrPeriodos2Nome: TWideStringField;
    QrPeriodos2Lk: TIntegerField;
    QrPeriodos2DataCad: TDateField;
    QrPeriodos2DataAlt: TDateField;
    QrPeriodos2UserCad: TIntegerField;
    QrPeriodos2UserAlt: TIntegerField;
    QrPeriodos2DSem: TIntegerField;
    QrPeriodos2HIni: TTimeField;
    QrPeriodos2HFim: TTimeField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrade: TDBGrid;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    BitBtn3: TBitBtn;
    Grade: TStringGrid;
    Button1: TButton;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Shape1: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape2: TShape;
    Shape5: TShape;
    PMInclui: TPopupMenu;
    Umperodo1: TMenuItem;
    Mltiplosperodos1: TMenuItem;
    PainelMultiplos: TPanel;
    Panel9: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    Panel10: TPanel;
    GroupBox1: TGroupBox;
    CkDom: TCheckBox;
    CkSeg: TCheckBox;
    CkQua: TCheckBox;
    CkTer: TCheckBox;
    CkSex: TCheckBox;
    CkQui: TCheckBox;
    CkSab: TCheckBox;
    Label7: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    EdIni01: TdmkEdit;
    EdFim01: TdmkEdit;
    EdIni02: TdmkEdit;
    EdFim02: TdmkEdit;
    EdIni03: TdmkEdit;
    EdFim03: TdmkEdit;
    EdIni04: TdmkEdit;
    EdIni05: TdmkEdit;
    EdIni06: TdmkEdit;
    EdFim06: TdmkEdit;
    EdFim05: TdmkEdit;
    EdFim04: TdmkEdit;
    EdIni07: TdmkEdit;
    EdIni08: TdmkEdit;
    EdIni09: TdmkEdit;
    EdFim09: TdmkEdit;
    EdFim08: TdmkEdit;
    EdFim07: TdmkEdit;
    EdIni11: TdmkEdit;
    EdFim11: TdmkEdit;
    EdFim12: TdmkEdit;
    EdIni12: TdmkEdit;
    EdIni13: TdmkEdit;
    EdIni14: TdmkEdit;
    EdIni15: TdmkEdit;
    EdIni16: TdmkEdit;
    EdIni17: TdmkEdit;
    EdIni18: TdmkEdit;
    EdIni19: TdmkEdit;
    EdFim19: TdmkEdit;
    EdFim18: TdmkEdit;
    EdFim17: TdmkEdit;
    EdFim16: TdmkEdit;
    EdFim15: TdmkEdit;
    EdFim14: TdmkEdit;
    EdFim13: TdmkEdit;
    EdIni21: TdmkEdit;
    EdFim21: TdmkEdit;
    EdFim22: TdmkEdit;
    EdIni22: TdmkEdit;
    EdIni23: TdmkEdit;
    EdFim23: TdmkEdit;
    EdFim24: TdmkEdit;
    EdIni24: TdmkEdit;
    EdIni25: TdmkEdit;
    EdFim25: TdmkEdit;
    EdFim26: TdmkEdit;
    EdIni26: TdmkEdit;
    EdIni27: TdmkEdit;
    EdFim27: TdmkEdit;
    EdFim28: TdmkEdit;
    EdIni28: TdmkEdit;
    EdIni29: TdmkEdit;
    EdFim29: TdmkEdit;
    EdIni31: TdmkEdit;
    EdFim31: TdmkEdit;
    EdFim32: TdmkEdit;
    EdIni32: TdmkEdit;
    EdIni33: TdmkEdit;
    EdFim33: TdmkEdit;
    EdFim34: TdmkEdit;
    EdIni34: TdmkEdit;
    EdIni35: TdmkEdit;
    EdFim35: TdmkEdit;
    EdFim36: TdmkEdit;
    EdIni36: TdmkEdit;
    EdIni37: TdmkEdit;
    EdFim37: TdmkEdit;
    EdFim38: TdmkEdit;
    EdIni38: TdmkEdit;
    EdIni39: TdmkEdit;
    EdFim39: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPeriodosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPeriodosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPeriodosBeforeOpen(DataSet: TDataSet);
    procedure EdHIniExit(Sender: TObject);
    procedure EdHFimExit(Sender: TObject);
    procedure QrPeriodosCalcFields(DataSet: TDataSet);
    procedure BitBtn3Click(Sender: TObject);
    procedure GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Button1Click(Sender: TObject);
    procedure Umperodo1Click(Sender: TObject);
    procedure Mltiplosperodos1Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    FLista : Array[0..10081] of Integer;
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiMultiplosRegistros;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo: Integer;
              Painel: TPanel);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ColoreGrade;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPeriodos: TFmPeriodos;
const
  FFormatFloat = '00000';
  FXadrez = clGray;

implementation

uses UnMyObjects, Module, MyVCLSkin;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPeriodos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPeriodos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPeriodosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPeriodos.DefParams;
begin
  VAR_GOTOTABELA := 'Periodos';
  VAR_GOTOMYSQLTABLE := QrPeriodos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM periodos');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPeriodos.MostraEdicao(Mostra : Boolean; Status : String; Codigo:
 Integer; Painel: TPanel);
var
  i, j: Integer;
begin
  if Mostra then
  begin
    Painel.Visible         := True;
    PainelDados.Visible    := False;
    EdNome.Text            := CO_VAZIO;
    EdNome.Visible         := True;
    PainelControle.Visible := False;
    //
    if Status = CO_INCLUSAO then
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo)
    else
      EdCodigo.Text := DBEdCodigo.Text;
    //
    if Painel = PainelEdita then
    begin
      EdNome.Text      := DBEdNome.Text;
      RGDSem.ItemIndex := QrPeriodosDSem.Value;
      EdHIni.Text      := MLAGeral.THT(FormatDateTime(VAR_FORMATTIME, QrPeriodosHIni.Value));
      EdHFim.Text      := MLAGeral.THT(FormatDateTime(VAR_FORMATTIME, QrPeriodosHFim.Value));
      EdNome.SetFocus;
    end;
    if Painel = PainelMultiplos then
    begin
      for i := 0 to 3 do
      begin
        for j := 1 to 9 do
        begin
          if FindComponent('EdIni'+IntToStr(i)+IntToStr(j)) <> nil then
            TdmkEdit(FindComponent('EdIni'+IntToStr(i)+IntToStr(j))).Text := '00:00';
          if FindComponent('EdIni'+IntToStr(i)+IntToStr(j)) <> nil then
            TdmkEdit(FindComponent('EdFim'+IntToStr(i)+IntToStr(j))).Text := '00:00';
        end;
      end;
      CkDom.Checked := False;
      CkSeg.Checked := False;
      CkTer.Checked := False;
      CkQua.Checked := False;
      CkQui.Checked := False;
      CkSex.Checked := False;
      CkSab.Checked := False;
      EdIni01.SetFocus;
    end;
  end else begin
    PainelControle.Visible  := True;
    PainelDados.Visible     := True;
    PainelEdita.Visible     := False;
    PainelMultiplos.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPeriodos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPeriodos.AlteraRegistro;
var
  Periodos : Integer;
begin
  Periodos := QrPeriodosCodigo.Value;
  if QrPeriodosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Periodos, Dmod.MyDB, 'Periodos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Periodos, Dmod.MyDB, 'Periodos', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0, PainelEdita);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPeriodos.IncluiRegistro;
var
  Cursor : TCursor;
  Periodos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Periodos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                  'Periodos', 'Periodos', 'Codigo');
    //
    if Length(FormatFloat(FFormatFloat, Periodos))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Periodos, PainelEdita);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmPeriodos.IncluiMultiplosRegistros;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    MostraEdicao(True, CO_INCLUSAO, 0, PainelMultiplos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmPeriodos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPeriodos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPeriodos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPeriodos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPeriodos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPeriodos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPeriodos.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmPeriodos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPeriodos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPeriodosCodigo.Value;
  Close;
end;

procedure TFmPeriodos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
  HIni, HFim: String;
  HI, HF: TTime;
begin
  if EdHIni.Text   = CO_VAZIO then EdHIni.Text   := '00:00';
  if EdHFim.Text   = CO_VAZIO then EdHFim.Text   := '00:00';
  //
  HIni := FormatDateTime(VAR_FORMATTIME, StrToDateTime(EdHIni.Text));
  HFim := FormatDateTime(VAR_FORMATTIME, StrToDateTime(EdHFim.Text));
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(RGDSem.ItemIndex < 0, nil, 'Defina o dia da semana!') then Exit;
  //
  HI := StrToTime(MLAGeral.THT(EdHIni.Text));
  HF := StrToTime(MLAGeral.THT(EdHFim.Text));
  //
  if HF < HI then
  begin
    if Geral.MB_Pergunta('Hora final n�o pode ser inferior a inicial.'+
      'Se o per�odo termina no dia posterior, confirme, caso contr�rio cancele.'+
      'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  Dmod.QrUpdU.SQL.Clear;
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO periodos SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE periodos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, DSem=:P1, HIni=:P2, HFim=:P3,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := RGDSem.ItemIndex;
  Dmod.QrUpdU.Params[02].AsString  := HIni;
  Dmod.QrUpdU.Params[03].AsString  := HFim;
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Periodos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0, PainelDados);
  LocCod(Codigo,Codigo);
end;

procedure TFmPeriodos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Periodos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Periodos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0, Paineldados);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Periodos', 'Codigo');
end;

procedure TFmPeriodos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPeriodosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPeriodos.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmPeriodos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPeriodos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmPeriodos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPeriodos.QrPeriodosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPeriodos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Periodos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPeriodos.QrPeriodosAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrPeriodosCodigo.Value, False);
end;

procedure TFmPeriodos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPeriodosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Periodos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPeriodos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPeriodos.QrPeriodosBeforeOpen(DataSet: TDataSet);
begin
  QrPeriodosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPeriodos.EdHIniExit(Sender: TObject);
begin
  TdmkEdit(Sender).Text := MLAGeral.THT(TdmkEdit(Sender).Text);
end;

procedure TFmPeriodos.EdHFimExit(Sender: TObject);
begin
  TdmkEdit(Sender).Text := MLAGeral.THT(TdmkEdit(Sender).Text);
end;

procedure TFmPeriodos.QrPeriodosCalcFields(DataSet: TDataSet);
var
  Tempo: TTime;
begin
  Tempo := QrPeriodosHFim.Value-QrPeriodosHIni.Value;
  if Tempo < 0 then Tempo := 1+Tempo;
  QrPeriodosHINI_TXT.Value := FormatDateTime(VAR_FORMATTIME2, QrPeriodosHIni.Value);
  QrPeriodosHFIM_TXT.Value := FormatDateTime(VAR_FORMATTIME2, QrPeriodosHFim.Value);
  QrPeriodosDSEM_TXT.Value := MLAGeral.VerificaDiaSemana(QrPeriodosDSem.Value+1);
  QrPeriodosTempo_TXT.Value := FormatDateTime(VAR_FORMATTIME2, Tempo);
end;

procedure TFmPeriodos.ColoreGrade;
var
  i: Integer;
  Mi, Mf: Integer;
begin
  Screen.Cursor := crHourGlass;
  for i := 1 to 10080 do FLista[i] := 0;
  FLista[00000] := -1;
  FLista[10081] := -1;
  QrPeriodos2.Close;
  QrPeriodos2.Open;
  //Memo1.Lines.Clear;
  while not QrPeriodos2.Eof do
  begin
    Mi := Trunc((QrPeriodos2HIni.Value+QrPeriodos2DSem.Value)*1440);
    Mf := Trunc((QrPeriodos2HFim.Value+QrPeriodos2DSem.Value)*1440);
    for i := Mi+1 to Mf-1 do
    begin
      FLista[i] := FLista[i] + 1;
    end;
    FLista[Mi] := -1;
    FLista[Mf] := -1;
    QrPeriodos2.Next;
  end;
  //for i := 0 to 10081 do Memo1.Lines.Add(FormatFloat('00000', i)+'-'+IntToStr(FLista[i]));
  Grade.Update;
  Screen.Cursor := crDefault;
end;

procedure TFmPeriodos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  DBGrade.Align      := alClient;
  PageControl1.Align := alClient;
  CriaOForm;
  //
  TabSheet3.TabVisible         := False;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPeriodos.BitBtn3Click(Sender: TObject);
begin
  ColoreGrade;
end;

procedure TFmPeriodos.GradeDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Item: Integer;
  Ativo: Boolean;
begin
  Item := FLista[ARow*1440+ACol];
  if gdFocused in State then Ativo := True else Ativo := False;
  MeuVCLSkin.DrawStringGrid(Grade, Rect, 0, Item, Ativo);
  Label7.Caption := IntToStr(ARow*1440+ACol);
  if gdFocused in State then
      Grade.Canvas.DrawFocusRect(Rect);
end;

procedure TFmPeriodos.Button1Click(Sender: TObject);
begin
  Grade.Update;
end;

procedure TFmPeriodos.Umperodo1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmPeriodos.Mltiplosperodos1Click(Sender: TObject);
begin
  IncluiMultiplosRegistros;
end;

procedure TFmPeriodos.BtDesiste2Click(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Periodos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Periodos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0, Paineldados);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Periodos', 'Codigo');
end;

procedure TFmPeriodos.BtConfirma2Click(Sender: TObject);
var
  i, j, k, m, Conta: Integer;
  Nome : String;
  HIni, HFim: String;
  HI, HF: TTime;
  Continua: Boolean;
begin
  Cursor := crHourGlass;
  m := 0;
  Conta := 0;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO periodos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, DSem=:P1, HIni=:P2, HFim=:P3,');
  Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz');
  for k := 0 to 6 do
  begin
    case k of
      1: Continua := CkDom.Checked;
      2: Continua := CkSeg.Checked;
      3: Continua := CkTer.Checked;
      4: Continua := CkQua.Checked;
      5: Continua := CkQui.Checked;
      6: Continua := CkSex.Checked;
      7: Continua := CkSab.Checked;
      else Continua := False;
    end;
    if Continua then begin
      for i := 0 to 3 do
      begin
        for j := 1 to 9 do
        begin
          Continua := True;
          HIni     := '';
          HFim     := '';
          //
          if FindComponent('EdIni'+IntToStr(i)+IntToStr(j)) <> nil then
            HIni := TdmkEdit(FindComponent('EdIni'+IntToStr(i)+IntToStr(j))).Text;
          if FindComponent('EdIni'+IntToStr(i)+IntToStr(j)) <> nil then
            HFim := TdmkEdit(FindComponent('EdFim'+IntToStr(i)+IntToStr(j))).Text;
          //
          HIni := FormatDateTime(VAR_FORMATTIME, StrToDateTime(HIni));
          HFim := FormatDateTime(VAR_FORMATTIME, StrToDateTime(HFim));
          HI := StrToTime(MLAGeral.THT(HIni));
          HF := StrToTime(MLAGeral.THT(HFim));
          //
          if HF < HI then
          begin
            if Geral.MB_Pergunta('Hora final n�o pode ser inferior a inicial.' +
              'Se o per�odo termina no dia posterior, confirme, caso contr�rio cancele.'+
              'Deseja continuar assim mesmo?') <> ID_YES
            then
              Continua := False;
          end;
          if (HI = 0) and (HF=0) then Continua := False;
          if Continua then
          begin
            Nome := IntToStr(k)+' - ['+
              FormatDateTime(VAR_FORMATTIME2, HI)+'] > ['+
              FormatDateTime(VAR_FORMATTIME2, HF)+']';
            m := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Periodos', 'Periodos', 'Codigo');
            Dmod.QrUpdU.Params[00].AsString  := Nome;
            Dmod.QrUpdU.Params[01].AsInteger := k-1;
            Dmod.QrUpdU.Params[02].AsString  := HIni;
            Dmod.QrUpdU.Params[03].AsString  := HFim;
            Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
            Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
            Dmod.QrUpdU.Params[06].AsInteger := m;
            Dmod.QrUpdU.ExecSQL;
            Conta := Conta +1;
          end;
        end;
      end;
    end;
  end;
  if conta = 0 then
  begin
    Geral.MB_Aviso('Nenhum per�odo foi inclu�do!');
  end else
  begin
    Geral.MB_Aviso(IntToStr(Conta)+' per�odos foram inclu�dos!');
    //
    UMyMod.UpdUnlockY(m, Dmod.MyDB, 'Periodos', 'Codigo');
    MostraEdicao(False, CO_TRAVADO, 0, PainelDados);
    LocCod(m,m);
  end;
  Cursor := crDefault;
end;

end.

