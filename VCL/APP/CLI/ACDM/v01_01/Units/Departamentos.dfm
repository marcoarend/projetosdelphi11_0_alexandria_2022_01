�
 TFMDEPARTAMENTOS 0�&  TPF0TFmDepartamentosFmDepartamentosLeftpTop� Caption*DEP-CADAS-001 :: Cadastro de DepartamentosClientHeight�ClientWidthNColor	clBtnFaceConstraints.MinHeight@Constraints.MinWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreateOnResize
FormResizePixelsPerInchx
TextHeight TPanelPainelEditaLeft Top;WidthNHeightqMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientColorclBtnShadowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible TPanelPainelConfirmaLeftTop5WidthKHeight;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder  TBitBtn
BtConfirmaTagLeft
TopWidthoHeight1CursorcrHandPointHint   Confima Inclusão / alteraçãoMargins.LeftMargins.TopMargins.RightMargins.BottomCaption	&ConfirmaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrder OnClickBtConfirmaClick  TPanelPanel2Left�TopWidth� Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight
BevelOuterbvNoneTabOrder TBitBtn	BtDesisteTagLeft
TopWidthoHeight1CursorcrHandPointHint   Cancela inclusão / alteraçãoMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&DesisteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrder OnClickBtDesisteClick    TPanel
PainelEditLeftTopWidth�Height� Margins.LeftMargins.TopMargins.RightMargins.BottomTabOrder TLabelLabel9LeftTop
Width/HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Código:  TLabelLabel10Left� Top
WidthAHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Descrição:  TdmkEditEdCodigoLeftTopWidth{HeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.Color4_~ Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder 
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZerosNoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto00UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  TdmkEditEdNomeLeft� TopWidthXHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	MaxLength� TabOrder
FormatTypedmktfStringMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortUpdTypeutYesObrigatorioPermiteNuloValueVariant    ValWarn    TPanelPainelDadosLeft Top;WidthNHeightqMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientColorclAppWorkSpaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPainelControleLeftTop5WidthKHeight;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder  TLabel
LaRegistroLeft� TopWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientCaption[N]: 0  TPanelPanel5LeftTopWidth� Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeft
BevelOuterbvNoneParentColor	TabOrder  TBitBtnSpeedButton4TagLeft� TopWidth1Height1CursorcrHandPointHintltimo registroMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrder OnClickSpeedButton4Click  TBitBtnSpeedButton3TagLeftlTopWidth2Height1CursorcrHandPointHint   Próximo registroMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton3Click  TBitBtnSpeedButton2TagLeft;TopWidth1Height1CursorcrHandPointHintRegistro anteriorMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton2Click  TBitBtnSpeedButton1TagLeft
TopWidth1Height1CursorcrHandPointHintPrimeiro registroMargins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton1Click   TPanelPanel3LeftgTopWidth�Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight
BevelOuterbvNoneParentColor	TabOrder TBitBtnBtExcluiTagLeft� TopWidthoHeight1CursorcrHandPointHintExclui banco atualMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&ExcluiEnabled	NumGlyphsParentShowHintShowHint	TabOrder   TBitBtnBtAlteraTagLeftvTopWidthoHeight1CursorcrHandPointHintAltera banco atualMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&Altera	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtAlteraClick  TBitBtnBtIncluiTag
LeftTopWidthoHeight1CursorcrHandPointHintInclui novo bancoMargins.LeftMargins.TopMargins.RightMargins.BottomCaption&Inclui	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtIncluiClick  TPanelPanel1LeftaTop Width� Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight
BevelOuterbvNoneTabOrder TBitBtnBtSaidaTagLeftTopWidthoHeight1CursorcrHandPointHintSai da janela atualMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   &Saída	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtSaidaClick     TPanel
PainelDataLeftTopWidth�Height� Margins.LeftMargins.TopMargins.RightMargins.BottomTabOrder TLabelLabel1LeftTop
Width/HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Código:FocusControl
DBEdCodigo  TLabelLabel2Left� Top
WidthAHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Descrição:FocusControlDBEdNome  TDBEdit
DBEdCodigoLeftTopWidth{HeightHint   Nº do bancoMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	DataFieldCodigo
DataSourceDsDepartamentosEnabledFont.CharsetDEFAULT_CHARSET
Font.Color4_~ Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 	MaxLength
ParentFontParentShowHintReadOnly	ShowHint	TabOrder   TDBEditDBEdNomeLeft� TopWidth�HeightHintNome do bancoMargins.LeftMargins.TopMargins.RightMargins.BottomColorclWhite	DataFieldNome
DataSourceDsDepartamentosEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrder    TPanelPainelTituloLeft Top WidthNHeight;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalTop	AlignmenttaLeftJustifyCaption7                              Cadastro de DepartamentosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style ParentColor	
ParentFontTabOrder TLabelLaTipoLeft�TopWidtheHeight9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight	AlignmenttaCenterAutoSizeCaptionTravadoFont.CharsetANSI_CHARSET
Font.Color4_~ Font.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	  TImageImage1LeftTopWidth�Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientTransparent	  TPanelPainel2LeftTopWidthHeight9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalLeftFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style ParentColor	
ParentFontTabOrder  TBitBtn	SbImprimeTagLeftTopWidth1Height1Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrder OnClickSbImprimeClick  TBitBtnSbNovoTagLeft9TopWidth1Height1Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbNovoClick  TBitBtnSbNumeroTagLeftlTopWidth2Height1Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbNumeroClick  TBitBtnSbNomeTagLeft� TopWidth1Height1Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbNomeClick  TBitBtnSbQueryTag	Left� TopWidth1Height1Margins.LeftMargins.TopMargins.RightMargins.Bottom	NumGlyphsTabOrderOnClickSbQueryClick    TDataSourceDsDepartamentosDataSetQrDepartamentosLeft�Top�   TmySQLQueryQrDepartamentosDatabase	Dmod.MyDB
BeforeOpenQrDepartamentosBeforeOpen	AfterOpenQrDepartamentosAfterOpenAfterScrollQrDepartamentosAfterScrollSQL.StringsSELECT * FROM departamentosWHERE Codigo > 0 Left�Top�  TIntegerFieldQrDepartamentosCodigo	FieldNameCodigo  TStringFieldQrDepartamentosNome	FieldNameNomeSize2  TIntegerFieldQrDepartamentosLk	FieldNameLk  
TDateFieldQrDepartamentosDataCad	FieldNameDataCad  
TDateFieldQrDepartamentosDataAlt	FieldNameDataAlt  TIntegerFieldQrDepartamentosUserCad	FieldNameUserCad  TIntegerFieldQrDepartamentosUserAlt	FieldNameUserAlt    