object FmChamadasNew: TFmChamadasNew
  Left = 367
  Top = 198
  Caption = 'CHA-LISTA-002 :: Nova Chamada'
  ClientHeight = 411
  ClientWidth = 522
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 0
    Width = 522
    Height = 352
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 59
      Width = 522
      Height = 293
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      ParentColor = True
      TabOrder = 0
      object Label3: TLabel
        Left = 138
        Top = 226
        Width = 63
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'In'#237'cio aula:'
      end
      object Label4: TLabel
        Left = 212
        Top = 226
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'In'#237'. interv.:'
      end
      object Label6: TLabel
        Left = 286
        Top = 226
        Width = 60
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fim interv.'
      end
      object Label8: TLabel
        Left = 359
        Top = 226
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Fim aula:'
      end
      object Label5: TLabel
        Left = 15
        Top = 10
        Width = 42
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Turma:'
        FocusControl = EdTurma
      end
      object Label7: TLabel
        Left = 15
        Top = 64
        Width = 38
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Curso:'
        FocusControl = EdCurso
      end
      object Label9: TLabel
        Left = 15
        Top = 118
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sala:'
        FocusControl = EdSala
      end
      object Label10: TLabel
        Left = 15
        Top = 172
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Professor:'
        FocusControl = EdProfessor
      end
      object Label1: TLabel
        Left = 15
        Top = 226
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label2: TLabel
        Left = 433
        Top = 226
        Width = 60
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tot.horas:'
      end
      object EdHoraI: TdmkEdit
        Left = 138
        Top = 246
        Width = 68
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00:00'
        ValWarn = False
        OnExit = EdHoraIExit
      end
      object EdIntervI: TdmkEdit
        Left = 212
        Top = 246
        Width = 67
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00:00'
        ValWarn = False
        OnExit = EdIntervIExit
      end
      object EdIntervF: TdmkEdit
        Left = 286
        Top = 246
        Width = 67
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00:00'
        ValWarn = False
        OnExit = EdIntervFExit
      end
      object EdHoraF: TdmkEdit
        Left = 359
        Top = 246
        Width = 68
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '00:00'
        ValWarn = False
        OnExit = EdHoraFExit
      end
      object EdTurma: TdmkEditCB
        Left = 15
        Top = 30
        Width = 79
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTurma
        IgnoraDBLookupComboBox = False
      end
      object CBTurma: TdmkDBLookupComboBox
        Left = 98
        Top = 30
        Width = 419
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTurmas
        TabOrder = 5
        dmkEditCB = EdTurma
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCurso: TdmkEditCB
        Left = 15
        Top = 84
        Width = 79
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCurso
        IgnoraDBLookupComboBox = False
      end
      object CBCurso: TdmkDBLookupComboBox
        Left = 98
        Top = 84
        Width = 419
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCursos
        TabOrder = 7
        dmkEditCB = EdCurso
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSala: TdmkEditCB
        Left = 15
        Top = 138
        Width = 79
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSala
        IgnoraDBLookupComboBox = False
      end
      object CBSala: TdmkDBLookupComboBox
        Left = 98
        Top = 138
        Width = 419
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSalas
        TabOrder = 9
        dmkEditCB = EdSala
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProfessor: TdmkEditCB
        Left = 15
        Top = 192
        Width = 79
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProfessor
        IgnoraDBLookupComboBox = False
      end
      object CBProfessor: TdmkDBLookupComboBox
        Left = 98
        Top = 192
        Width = 419
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProfessores
        TabOrder = 11
        dmkEditCB = EdProfessor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TDateTimePicker
        Left = 15
        Top = 246
        Width = 119
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 38521.601804467600000000
        Time = 38521.601804467600000000
        TabOrder = 12
      end
      object EdTempo: TdmkEdit
        Left = 433
        Top = 246
        Width = 80
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taCenter
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdHoraFExit
      end
    end
    object PainelTitulo: TPanel
      Left = 0
      Top = 0
      Width = 522
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = 'Nova Chamada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -38
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 1
      object LaTipo: TLabel
        Left = 420
        Top = 1
        Width = 101
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        Alignment = taCenter
        AutoSize = False
        Caption = 'Travado'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8281908
        Font.Height = -18
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        Visible = False
      end
      object Image1: TImage
        Left = 1
        Top = 1
        Width = 419
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Transparent = True
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 352
    Width = 522
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 389
      Top = 5
      Width = 111
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Desiste'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtDesisteClick
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 30
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
  end
  object QrCursos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cursos'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 336
    Top = 112
    object QrCursosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCursosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCursos: TDataSource
    DataSet = QrCursos
    Left = 364
    Top = 112
  end
  object DsProfessores: TDataSource
    DataSet = QrProfessores
    Left = 364
    Top = 200
  end
  object QrProfessores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome')
    Left = 336
    Top = 200
    object QrProfessoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProfessoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrSalas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM salas'
      'ORDER BY Nome')
    Left = 336
    Top = 156
    object QrSalasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSalasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsSalas: TDataSource
    DataSet = QrSalas
    Left = 364
    Top = 156
  end
  object QrTurmas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM turmas'
      'ORDER BY Nome')
    Left = 336
    Top = 68
    object QrTurmasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTurmasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsTurmas: TDataSource
    DataSet = QrTurmas
    Left = 364
    Top = 68
  end
  object QrAlunos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mtu.Codigo MATRICULA'
      'FROM matritur mtu'
      'LEFT JOIN matriren  ren ON ren.Controle=mtu.Controle'
      'LEFT JOIN matricul  mat ON mat.Codigo=mtu.Codigo'
      'WHERE mtu.Turma=:P0'
      'AND ren.Ativo=1'
      'AND mat.Ativo=1')
    Left = 140
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAlunosMATRICULA: TIntegerField
      FieldName = 'MATRICULA'
      Required = True
    end
  end
end
