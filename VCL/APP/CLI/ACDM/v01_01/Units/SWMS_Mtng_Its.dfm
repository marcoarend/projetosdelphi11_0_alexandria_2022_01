object FmSWMS_Mtng_Its: TFmSWMS_Mtng_Its
  Left = 339
  Top = 185
  Caption = 'MET-GEREN-002 :: Cadastro de Equipe'
  ClientHeight = 358
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  GlassFrame.Left = 100
  GlassFrame.Top = 100
  GlassFrame.SheetOfGlass = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 205
    Width = 617
    Height = 38
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 189
    ExplicitHeight = 27
    object CkContinuar: TCheckBox
      Left = 12
      Top = 4
      Width = 141
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 617
    Height = 69
    Align = alTop
    Caption = ' Dados da competi'#231#227'o: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label4: TLabel
      Left = 72
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Left = 132
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label2: TLabel
      Left = 528
      Top = 20
      Width = 59
      Height = 13
      Caption = 'ID inscri'#231#227'o:'
      FocusControl = DBEdit1
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 38
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmSWMS_Mtng_Cab.DsSWMS_Mtng_Cab
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdit1: TDBEdit
      Left = 72
      Top = 38
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'CodUsu'
      DataSource = FmSWMS_Mtng_Cab.DsSWMS_Mtng_Cab
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 132
      Top = 38
      Width = 392
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object EdControle: TdmkEdit
      Left = 528
      Top = 38
      Width = 80
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 117
    Width = 617
    Height = 88
    Align = alTop
    Caption = ' Dados da equipe: '
    TabOrder = 1
    object Label1: TLabel
      Left = 12
      Top = 20
      Width = 81
      Height = 13
      Caption = 'Nome da equipe:'
    end
    object Label23: TLabel
      Left = 540
      Top = 20
      Width = 42
      Height = 13
      Caption = 'Passe P:'
    end
    object EdNome: TdmkEdit
      Left = 12
      Top = 36
      Width = 525
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdSenhaP: TdmkEdit
      Left = 540
      Top = 36
      Width = 68
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'SenhaP'
      UpdCampo = 'SenhaP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object CkAtivo: TdmkCheckBox
      Left = 12
      Top = 64
      Width = 49
      Height = 17
      Caption = 'Ativo.'
      Checked = True
      State = cbChecked
      TabOrder = 2
      QryCampo = 'Ativo'
      UpdCampo = 'Ativo'
      UpdType = utYes
      ValCheck = '1'
      ValUncheck = '2'
      OldValor = #0
    end
  end
  object PnTitulo: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object GB_R: TGroupBox
      Left = 569
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 521
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 241
        Height = 32
        Caption = 'Cadastro de Equipe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 241
        Height = 32
        Caption = 'Cadastro de Equipe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 241
        Height = 32
        Alignment = taCenter
        Caption = 'Cadastro de Equipe'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 300
    Width = 617
    Height = 58
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 273
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 12
      Width = 120
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 478
      Top = 12
      Width = 120
      Height = 40
      Cursor = crHandPoint
      Caption = '&Fechar'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 243
    Width = 617
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 216
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 613
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
