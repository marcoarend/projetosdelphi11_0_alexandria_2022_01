object FmMatricul: TFmMatricul
  Left = 369
  Top = 171
  Caption = 'MTR-CADAS-001 :: Cadastro de Matr'#237'culas'
  ClientHeight = 678
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 619
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 559
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 448
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        ExplicitWidth = 31
        ExplicitHeight = 16
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 661
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 458
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSaidaClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 345
          Top = 5
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
        object BtRefresh: TBitBtn
          Tag = 18
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Refresh'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtRefreshClick
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 210
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object Label1: TLabel
        Left = 15
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 113
        Top = 10
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
        FocusControl = DBEdAluno
      end
      object Label4: TLabel
        Left = 674
        Top = 10
        Width = 254
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Respons'#225'vel pelo menor ou incapacitado:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 15
        Top = 59
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 862
        Top = 59
        Width = 53
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contrato:'
        FocusControl = DBEdit4
      end
      object Label7: TLabel
        Left = 113
        Top = 59
        Width = 33
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo:'
        FocusControl = DBEdit5
      end
      object Label11: TLabel
        Left = 158
        Top = 59
        Width = 39
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pre'#231'o:'
        FocusControl = DBEdit1
      end
      object Label23: TLabel
        Left = 364
        Top = 59
        Width = 70
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Valor pago:'
        FocusControl = DBEdit6
      end
      object Label24: TLabel
        Left = 468
        Top = 59
        Width = 76
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Desconto:'
        FocusControl = DBEdit7
      end
      object Label3: TLabel
        Left = 551
        Top = 59
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Pendente:'
        FocusControl = DBEdit8
      end
      object Label25: TLabel
        Left = 655
        Top = 59
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conveni'#234'ncia:'
        FocusControl = DBEdit9
      end
      object Label26: TLabel
        Left = 758
        Top = 59
        Width = 94
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Total pendente:'
        FocusControl = DBEdit10
      end
      object Label27: TLabel
        Left = 261
        Top = 59
        Width = 90
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Com desconto:'
        FocusControl = DBEdit11
      end
      object Label28: TLabel
        Left = 1130
        Top = 84
        Width = 95
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        AutoSize = False
        Caption = '0.000'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label29: TLabel
        Left = 1086
        Top = 84
        Width = 100
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tempo abertura:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object SpeedButton7: TSpeedButton
        Left = 645
        Top = 30
        Width = 26
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object Label15: TLabel
        Left = 960
        Top = 10
        Width = 66
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Orientador:'
        FocusControl = DBEdit12
      end
      object Label31: TLabel
        Left = 15
        Top = 108
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Observa'#231#245'es:'
        FocusControl = DBEdit13
      end
      object DBEdCodigo: TDBEdit
        Left = 15
        Top = 30
        Width = 95
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdAluno: TDBEdit
        Left = 113
        Top = 30
        Width = 532
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMEALUNO'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 674
        Top = 30
        Width = 282
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMERESPONS'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 15
        Top = 79
        Width = 95
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'DataC'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 862
        Top = 79
        Width = 217
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Contrato'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 113
        Top = 79
        Width = 41
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'ATIVO_TXT'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnChange = DBEdit5Change
      end
      object DBEdit1: TDBEdit
        Left = 158
        Top = 79
        Width = 98
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Preco'
        DataSource = DsMatriRen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 364
        Top = 79
        Width = 99
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Valor'
        DataSource = DsMatriRen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 468
        Top = 79
        Width = 80
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'P_DESCO'
        DataSource = DsMatriRen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
      end
      object DBGrid6: TDBGrid
        Left = 1
        Top = 160
        Width = 1237
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        DataSource = DsMatriRen
        TabOrder = 9
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SEQ'
            Title.Alignment = taCenter
            Title.Caption = 'Seq.'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMETARIFA'
            Title.Caption = 'Plano'
            Width = 250
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DataI'
            Title.Alignment = taCenter
            Title.Caption = 'In'#237'cio'
            Width = 52
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DataF'
            Title.Alignment = taCenter
            Title.Caption = 'Final'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Taxa'
            Title.Alignment = taRightJustify
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Preco'
            Title.Alignment = taRightJustify
            Title.Caption = 'Pre'#231'o'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRECO_T'
            Title.Alignment = taRightJustify
            Title.Caption = 'Total'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Alignment = taRightJustify
            Title.Caption = 'Valor pago'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'P_DESCO'
            Title.Alignment = taRightJustify
            Title.Caption = '% Desconto'
            Width = 72
            Visible = True
          end>
      end
      object DBEdit8: TDBEdit
        Left = 551
        Top = 79
        Width = 99
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'PENDENTE'
        DataSource = DsMatriPen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnChange = DBEdit8Change
      end
      object DBEdit9: TDBEdit
        Left = 655
        Top = 79
        Width = 98
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'QuantN2'
        DataSource = DsCliProd
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnChange = DBEdit9Change
      end
      object DBEdit10: TDBEdit
        Left = 758
        Top = 79
        Width = 99
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'PENDENTE_TOTAL'
        DataSource = DsMatriRen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        OnChange = DBEdit10Change
      end
      object DBEdit11: TDBEdit
        Left = 261
        Top = 79
        Width = 98
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'ACobrar'
        DataSource = DsMatriRen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 960
        Top = 30
        Width = 272
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMEORIENTADOR'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 15
        Top = 128
        Width = 1064
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Observ'
        DataSource = DsMatricul
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 15
      end
    end
    object PainelMovimento: TPanel
      Left = 1
      Top = 232
      Width = 1239
      Height = 327
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Splitter2: TSplitter
        Left = 311
        Top = 1
        Width = 4
        Height = 325
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ExplicitHeight = 360
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 310
        Height = 325
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitHeight = 360
        object DBGrid1: TDBGrid
          Left = 0
          Top = 134
          Width = 310
          Height = 191
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsMatriRen
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -15
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = 'Seq.'
              Width = 30
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_ATIVO'
              Title.Alignment = taCenter
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DataI'
              Title.Alignment = taCenter
              Title.Caption = 'In'#237'cio'
              Width = 52
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DataF'
              Title.Alignment = taCenter
              Title.Caption = 'Final'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Pago'
              Width = 60
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 310
          Height = 134
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvLowered
          TabOrder = 1
          object Label8: TLabel
            Left = 1
            Top = 1
            Width = 308
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            Caption = 'Gerenciamento de Planos'
            ExplicitWidth = 156
          end
          object BtRefaz: TBitBtn
            Tag = 10058
            Left = 155
            Top = 25
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Renova'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtRefazClick
          end
          object BtRefazInclui: TBitBtn
            Tag = 10
            Left = 5
            Top = 25
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Inclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtRefazIncluiClick
          end
          object BtRefazAltera: TBitBtn
            Tag = 11
            Left = 5
            Top = 79
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Altera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtRefazAlteraClick
          end
          object BtRefazExclui: TBitBtn
            Tag = 12
            Left = 155
            Top = 79
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Exclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtRefazExcluiClick
          end
        end
      end
      object Panel2: TPanel
        Left = 315
        Top = 1
        Width = 923
        Height = 325
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitHeight = 360
        object Splitter1: TSplitter
          Left = 0
          Top = 149
          Width = 923
          Height = 4
          Cursor = crVSplit
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          ExplicitTop = 183
          ExplicitWidth = 922
        end
        object Panel6: TPanel
          Left = 0
          Top = 153
          Width = 923
          Height = 172
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 188
          object GridPagtos: TDBGrid
            Left = 0
            Top = 80
            Width = 923
            Height = 92
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsPagtos
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'FatParcela'
                Title.Caption = 'N'#186
                Width = 22
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Alignment = taRightJustify
                Title.Caption = 'Valor'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                ReadOnly = True
                Title.Caption = 'Situa'#231#227'o'
                Width = 69
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 79
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECARTEIRA'
                Title.Caption = 'Carteira'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECONTA'
                Title.Caption = 'Conta (Plano de contas)'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sub'
                Width = 25
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATRAZODD'
                Title.Caption = 'Atrz dd'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATUALIZADO'
                Title.Caption = 'Atualizado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Width = 152
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emitente'
                Width = 160
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CNPJCPF_TXT'
                Title.Caption = 'CNPJ/CPF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia'
                Title.Caption = 'Ag'#234'ncia'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ContaCorrente'
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 923
            Height = 80
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 1
            object Label22: TLabel
              Left = 1
              Top = 1
              Width = 921
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              Caption = 'Faturamento do Plano Selecionado'
              ExplicitWidth = 212
            end
            object BtPagtosInclui: TBitBtn
              Tag = 10
              Left = 5
              Top = 25
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Inclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtPagtosIncluiClick
            end
            object BtPagtosExclui: TBitBtn
              Tag = 12
              Left = 120
              Top = 25
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Exclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtPagtosExcluiClick
            end
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 923
          Height = 149
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 184
          object Panel9: TPanel
            Left = 519
            Top = 0
            Width = 404
            Height = 149
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitHeight = 184
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 404
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object Label21: TLabel
                Left = 1
                Top = 1
                Width = 402
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Hor'#225'rios da atividade '
                ExplicitWidth = 133
              end
              object BtRelogioInclui: TBitBtn
                Tag = 10
                Left = 5
                Top = 25
                Width = 128
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Inclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtRelogioIncluiClick
              end
              object BtRelogioExclui: TBitBtn
                Tag = 12
                Left = 133
                Top = 25
                Width = 128
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Exclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtRelogioExcluiClick
              end
              object BtHorarioGerencia: TBitBtn
                Tag = 10063
                Left = 261
                Top = 25
                Width = 128
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Gerencia'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtHorarioGerenciaClick
              end
            end
            object DBGrid2: TDBGrid
              Left = 0
              Top = 80
              Width = 404
              Height = 69
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsMatriPer
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEPERIODO'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEDSEM'
                  Title.Caption = 'Dia da semana'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'HIni'
                  Title.Caption = 'In'#237'cio'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'HFim'
                  Title.Caption = 'Final'
                  Width = 56
                  Visible = True
                end>
            end
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 321
            Height = 149
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitHeight = 184
            object Panel12: TPanel
              Left = 0
              Top = 0
              Width = 321
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object Label10: TLabel
                Left = 1
                Top = 1
                Width = 319
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = ' Turmas do plano selecinado'
                ExplicitWidth = 175
              end
              object BtTurmasInclui: TBitBtn
                Tag = 10
                Left = 5
                Top = 25
                Width = 148
                Height = 49
                Cursor = crHandPoint
                HelpContext = 2070
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Inclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtTurmasIncluiClick
              end
              object BtTurmasExclui: TBitBtn
                Tag = 12
                Left = 155
                Top = 25
                Width = 148
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Exclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtTurmasExcluiClick
              end
            end
            object DBGrid3: TDBGrid
              Left = 0
              Top = 80
              Width = 321
              Height = 69
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsMatriTur
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'IDTURMA'
                  Title.Caption = 'Refer'#234'ncia'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMETURMA'
                  Title.Caption = 'Descri'#231#227'o'
                  Visible = True
                end>
            end
          end
          object Panel13: TPanel
            Left = 321
            Top = 0
            Width = 198
            Height = 149
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            ExplicitHeight = 184
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 198
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object Label20: TLabel
                Left = 1
                Top = 1
                Width = 196
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Cursos / Atividades da turma selecionada'
                ExplicitWidth = 249
              end
              object BtAtividadesExclui: TBitBtn
                Tag = 12
                Left = 135
                Top = 25
                Width = 128
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Exclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtAtividadesExcluiClick
              end
              object BtAtividadesInclui: TBitBtn
                Tag = 10
                Left = 5
                Top = 25
                Width = 128
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Inclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtAtividadesIncluiClick
              end
            end
            object DBGrid4: TDBGrid
              Left = 0
              Top = 80
              Width = 198
              Height = 69
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsMatriAti
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Refere'
                  Title.Caption = 'Refer'#234'ncia'
                  Width = 69
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROFESSOR'
                  Title.Caption = 'Professor'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECURSO'
                  Title.Caption = 'Nome do curso / atividade'
                  Width = 178
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 619
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 559
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 5
        Top = 5
        Width = 111
        Height = 50
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel15: TPanel
        Left = 1114
        Top = 1
        Width = 124
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 5
          Top = 4
          Width = 111
          Height = 50
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1009
      Height = 268
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
      object Label9: TLabel
        Left = 15
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label12: TLabel
        Left = 15
        Top = 59
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label13: TLabel
        Left = 15
        Top = 108
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label17: TLabel
        Left = 473
        Top = 108
        Width = 49
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Instrutor:'
      end
      object Label18: TLabel
        Left = 158
        Top = 108
        Width = 53
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contrato:'
      end
      object Label19: TLabel
        Left = 414
        Top = 108
        Width = 33
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo:'
      end
      object Bevel1: TBevel
        Left = 414
        Top = 128
        Width = 55
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
      end
      object SpeedButton5: TSpeedButton
        Left = 965
        Top = 79
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 965
        Top = 128
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object Label14: TLabel
        Left = 15
        Top = 160
        Width = 492
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Respons'#225'vel em caso de menor ou incapacitado: [F3] apenas parent' +
          'es [F4] todos'
      end
      object SpeedButton8: TSpeedButton
        Left = 965
        Top = 177
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton8Click
      end
      object Label16: TLabel
        Left = 116
        Top = 10
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object Label30: TLabel
        Left = 15
        Top = 207
        Width = 85
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Observa'#231#245'es:'
        FocusControl = EdObserv
      end
      object EdCodigo: TdmkEdit
        Left = 15
        Top = 30
        Width = 95
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBAluno: TdmkDBLookupComboBox
        Left = 84
        Top = 79
        Width = 877
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsAlunos
        TabOrder = 4
        dmkEditCB = EdAluno
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdAluno: TdmkEditCB
        Left = 15
        Top = 79
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdAlunoChange
        DBLookupComboBox = CBAluno
        IgnoraDBLookupComboBox = False
      end
      object TPDataC: TdmkEditDateTimePicker
        Left = 15
        Top = 128
        Width = 138
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 38528.802122210600000000
        Time = 38528.802122210600000000
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object CkAtivo: TCheckBox
        Left = 417
        Top = 132
        Width = 45
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sim'
        TabOrder = 7
      end
      object EdOrientador: TdmkEditCB
        Left = 473
        Top = 128
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBOrientador
        IgnoraDBLookupComboBox = False
      end
      object CBOrientador: TdmkDBLookupComboBox
        Left = 542
        Top = 128
        Width = 419
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsOrientadores
        TabOrder = 9
        dmkEditCB = EdOrientador
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdContrato: TdmkEdit
        Left = 158
        Top = 128
        Width = 252
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRespons: TdmkEditCB
        Left = 15
        Top = 177
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRespons
        IgnoraDBLookupComboBox = False
      end
      object CBRespons: TdmkDBLookupComboBox
        Left = 84
        Top = 177
        Width = 877
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsRespons
        TabOrder = 11
        OnKeyDown = CBResponsKeyDown
        dmkEditCB = EdRespons
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmpresa: TdmkEditCB
        Left = 116
        Top = 30
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 185
        Top = 30
        Width = 776
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdObserv: TdmkEdit
        Left = 15
        Top = 227
        Width = 946
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ERua'
        UpdCampo = 'ERua'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Cadastro de Matr'#237'culas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 861
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitWidth = 860
    end
    object LaTipo: TdmkLabel
      Left = 1139
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 1138
    end
    object Painel2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsMatricul: TDataSource
    DataSet = QrMatricul
    Left = 244
    Top = 33
  end
  object QrMatricul: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMatriculBeforeOpen
    AfterOpen = QrMatriculAfterOpen
    BeforeClose = QrMatriculBeforeClose
    AfterClose = QrMatriculAfterClose
    AfterScroll = QrMatriculAfterScroll
    OnCalcFields = QrMatriculCalcFields
    SQL.Strings = (
      'SELECT'
      'CASE WHEN al.Tipo=0 THEN al.RazaoSocial'
      'ELSE al.Nome END NOMEALUNO,'
      'CASE WHEN oi.Tipo=0 THEN oi.RazaoSocial'
      'ELSE oi.Nome END NOMEORIENTADOR,'
      're.Nome NOMERESPONS, ma.*'
      'FROM matricul ma'
      'LEFT JOIN entidades al ON al.Codigo=ma.Aluno'
      'LEFT JOIN entidades oi ON oi.Codigo=ma.Orientador'
      'LEFT JOIN entidades re ON re.Codigo=ma.Respons'
      'WHERE ma.Codigo > 0')
    Left = 216
    Top = 33
    object QrMatriculNOMEALUNO: TWideStringField
      FieldName = 'NOMEALUNO'
      Size = 100
    end
    object QrMatriculNOMEORIENTADOR: TWideStringField
      FieldName = 'NOMEORIENTADOR'
      Size = 100
    end
    object QrMatriculCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'matricul.Codigo'
      Required = True
    end
    object QrMatriculAluno: TIntegerField
      FieldName = 'Aluno'
      Origin = 'matricul.Aluno'
      Required = True
    end
    object QrMatriculOrientador: TIntegerField
      FieldName = 'Orientador'
      Origin = 'matricul.Orientador'
      Required = True
    end
    object QrMatriculDataC: TDateField
      FieldName = 'DataC'
      Origin = 'matricul.DataC'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMatriculContrato: TWideStringField
      FieldName = 'Contrato'
      Origin = 'matricul.Contrato'
      Size = 30
    end
    object QrMatriculAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'matricul.Ativo'
      Required = True
    end
    object QrMatriculLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'matricul.Lk'
    end
    object QrMatriculDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'matricul.DataCad'
    end
    object QrMatriculDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'matricul.DataAlt'
    end
    object QrMatriculUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'matricul.UserCad'
    end
    object QrMatriculUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'matricul.UserAlt'
    end
    object QrMatriculATIVO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATIVO_TXT'
      Size = 3
      Calculated = True
    end
    object QrMatriculAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'matricul.AlterWeb'
    end
    object QrMatriculRespons: TIntegerField
      FieldName = 'Respons'
      Origin = 'matricul.Respons'
    end
    object QrMatriculNOMERESPONS: TWideStringField
      FieldName = 'NOMERESPONS'
      Origin = 'entidades.Nome'
      Size = 100
    end
    object QrMatriculEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMatriculObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
  end
  object QrMatriRen: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMatriRenAfterOpen
    BeforeClose = QrMatriRenBeforeClose
    AfterClose = QrMatriRenAfterClose
    AfterScroll = QrMatriRenAfterScroll
    OnCalcFields = QrMatriRenCalcFields
    SQL.Strings = (
      'SELECT tar.Nome NOMETARIFA, mr.* '
      'FROM matriren mr'
      'LEFT JOIN tarifas tar ON tar.Codigo=mr.Periodo'
      'WHERE mr.Codigo=:P0'
      'ORDER BY DataI DESC')
    Left = 272
    Top = 33
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMatriRenCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMatriRenDataI: TDateField
      FieldName = 'DataI'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMatriRenDataF: TDateField
      FieldName = 'DataF'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMatriRenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMatriRenLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMatriRenDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMatriRenDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMatriRenUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMatriRenUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMatriRenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMatriRenSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrMatriRenP_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'P_DESCO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMatriRenAtivo: TIntegerField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMatriRenTotalFator: TFloatField
      FieldName = 'TotalFator'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMatriRenPreco: TFloatField
      FieldName = 'Preco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMatriRenPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrMatriRenTaxa: TFloatField
      FieldName = 'Taxa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMatriRenNOMETARIFA: TWideStringField
      FieldName = 'NOMETARIFA'
      Size = 100
    end
    object QrMatriRenPRECO_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO_T'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMatriRenDESCO_V: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DESCO_V'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMatriRenACobrar: TFloatField
      FieldName = 'ACobrar'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMatriRenPENDENTE_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PENDENTE_TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMatriRenTarBase: TIntegerField
      FieldName = 'TarBase'
    end
    object QrMatriRenDesativar: TSmallintField
      FieldName = 'Desativar'
    end
    object QrMatriRenNO_ATIVO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_ATIVO'
      Size = 1
      Calculated = True
    end
    object QrMatriRenEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMatriRenCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object DsMatriRen: TDataSource
    DataSet = QrMatriRen
    Left = 300
    Top = 33
  end
  object QrAlunos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END NOMEENTIDADE, '
      'CASE WHEN Tipo=0 THEN ENatal ELSE PNatal END Natal'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'AND Ativo=1'
      ''
      'ORDER BY NOMEENTIDADE')
    Left = 264
    Top = 365
    object QrAlunosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrAlunosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlunosNatal: TDateField
      FieldName = 'Natal'
    end
  end
  object DsAlunos: TDataSource
    DataSet = QrAlunos
    Left = 292
    Top = 365
  end
  object DsOrientadores: TDataSource
    DataSet = QrOrientadores
    Left = 348
    Top = 365
  end
  object QrOrientadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo '
      'FROM entidades'
      'WHERE Cliente2="V"'
      'AND Ativo=1'
      ''
      'ORDER BY NOMEENTIDADE')
    Left = 320
    Top = 365
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPagtosAfterOpen
    OnCalcFields = QrPagtosCalcFields
    SQL.Strings = (
      'SELECT la.*, ca.Nome NOMECARTEIRA,'
      'ca.Tipo CARTEIRATIPO'
      'FROM lct0001a la, Carteiras ca'
      'WHERE ca.Codigo=la.Carteira'
      'AND FatID=2101'
      'AND FatNum=:P0'
      'ORDER BY la.Vencimento, la.FatParcela')
    Left = 376
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagtosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagtosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagtosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '###,##0.00'
    end
    object QrPagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '###,##0.00'
    end
    object QrPagtosCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagtosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagtosFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrPagtosLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPagtosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagtosLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagtosOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagtosLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagtosPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagtosMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagtosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagtosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagtosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagtosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagtosProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagtosDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagtosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrPagtosNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPagtosVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrPagtosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrPagtosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagtosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagtosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagtosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPagtosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPagtosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrPagtosCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 30
      Calculated = True
    end
    object QrPagtosSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '###,##0.00'
      Calculated = True
    end
    object QrPagtosNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 30
      Calculated = True
    end
    object QrPagtosVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      DisplayFormat = 'dd/mm/yy'
      Calculated = True
    end
    object QrPagtosATRAZODD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRAZODD'
      Calculated = True
    end
    object QrPagtosATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '###,##0.00'
      Calculated = True
    end
    object QrPagtosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPagtosICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrPagtosICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrPagtosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrPagtosDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPagtosDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrPagtosForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrPagtosQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPagtosCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrPagtosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrPagtosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagtosCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPagtosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 404
    Top = 365
  end
  object QrPagtos2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.*, ca.Nome NOMECARTEIRA,'
      'ca.Nome2 NOMECART2,'
      'ca.Tipo CARTEIRATIPO'
      'FROM lct0001a la, Carteiras ca'
      'WHERE ca.Codigo=la.Carteira'
      'AND FatID=2101'
      'AND FatNum=:P0'
      'ORDER BY la.Vencimento, la.FatParcela')
    Left = 432
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtos2Data: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagtos2Tipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagtos2Carteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagtos2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtos2Sub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagtos2Autorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagtos2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagtos2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtos2NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagtos2Debito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagtos2Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagtos2Compensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagtos2Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtos2Sit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagtos2Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPagtos2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtos2FatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPagtos2FatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtos2ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagtos2ID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagtos2Fatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagtos2Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtos2Local: TIntegerField
      FieldName = 'Local'
    end
    object QrPagtos2Cartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagtos2Linha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagtos2OperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagtos2Lancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagtos2Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagtos2Mez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagtos2Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagtos2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagtos2MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagtos2Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrPagtos2Protesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagtos2DataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagtos2CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrPagtos2Nivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPagtos2Vendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrPagtos2Account: TIntegerField
      FieldName = 'Account'
    end
    object QrPagtos2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagtos2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagtos2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagtos2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPagtos2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPagtos2NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrPagtos2CARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtos2FatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtos2NOMECART2: TWideStringField
      FieldName = 'NOMECART2'
      Size = 100
    end
  end
  object QrLoclancto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM l c t 0 0 0 1 a'
      'WHERE Tipo=1')
    Left = 460
    Top = 365
    object QrLoclanctoData: TDateField
      FieldName = 'Data'
    end
    object QrLoclanctoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLoclanctoCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLoclanctoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLoclanctoSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLoclanctoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLoclanctoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLoclanctoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLoclanctoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLoclanctoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLoclanctoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLoclanctoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLoclanctoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLoclanctoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLoclanctoVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLoclanctoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLoclanctoFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLoclanctoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLoclanctoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrLoclanctoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLoclanctoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLoclanctoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLoclanctoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLoclanctoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLoclanctoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLoclanctoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLoclanctoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLoclanctoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLoclanctoMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLoclanctoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLoclanctoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLoclanctoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLoclanctoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLoclanctoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLoclanctoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLoclanctoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLoclanctoNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLoclanctoVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLoclanctoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLoclanctoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLoclanctoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLoclanctoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLoclanctoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLoclanctoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLoclanctoFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object PMExclui: TPopupMenu
    Left = 576
    Top = 381
    object Pagamento1: TMenuItem
      Caption = 'Pagamento &Atual'
      OnClick = Pagamento1Click
    end
    object Todospagamentosdestarenovao1: TMenuItem
      Caption = '&Todos pagamentos desta renova'#231#227'o'
      OnClick = Todospagamentosdestarenovao1Click
    end
  end
  object QrMatriTur: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMatriTurAfterOpen
    AfterClose = QrMatriTurAfterClose
    AfterScroll = QrMatriTurAfterScroll
    SQL.Strings = (
      'SELECT tur.ID IDTURMA, '
      'tur.Nome NOMETURMA, mtu.* '
      'FROM matritur mtu'
      'LEFT JOIN turmas tur ON tur.Codigo=mtu.Turma'
      'WHERE mtu.Codigo=:P0'
      'AND mtu.Controle=:P1')
    Left = 328
    Top = 33
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMatriTurCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMatriTurControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMatriTurConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMatriTurTurma: TIntegerField
      FieldName = 'Turma'
      Required = True
    end
    object QrMatriTurLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMatriTurDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMatriTurDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMatriTurUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMatriTurUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMatriTurFator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
    object QrMatriTurIDTURMA: TWideStringField
      FieldName = 'IDTURMA'
      Required = True
      Size = 30
    end
    object QrMatriTurNOMETURMA: TWideStringField
      FieldName = 'NOMETURMA'
      Required = True
      Size = 100
    end
  end
  object DsMatriTur: TDataSource
    DataSet = QrMatriTur
    Left = 356
    Top = 33
  end
  object DsMatriAti: TDataSource
    DataSet = QrMatriAti
    Left = 412
    Top = 33
  end
  object QrMatriAti: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMatriAtiAfterOpen
    BeforeClose = QrMatriAtiBeforeClose
    AfterScroll = QrMatriAtiAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEPROFESSOR,'
      'tui.Refere, cur.Nome NOMECURSO, '
      'tui.Professor, maa.* '
      'FROM matriati maa'
      'LEFT JOIN turmasits tui ON tui.Controle=maa.Atividade'
      'LEFT JOIN cursos    cur ON cur.Codigo=tui.Curso'
      'LEFT JOIN entidades ent ON ent.Codigo=tui.Professor'
      'WHERE maa.Codigo=:P0'
      'AND maa.Controle=:P1'
      'AND maa.Conta=:P2')
    Left = 384
    Top = 33
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMatriAtiNOMECURSO: TWideStringField
      FieldName = 'NOMECURSO'
      Size = 50
    end
    object QrMatriAtiCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMatriAtiControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMatriAtiConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMatriAtiAtividade: TIntegerField
      FieldName = 'Atividade'
      Required = True
    end
    object QrMatriAtiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMatriAtiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMatriAtiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMatriAtiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMatriAtiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMatriAtiItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrMatriAtiRefere: TWideStringField
      FieldName = 'Refere'
      Required = True
      Size = 30
    end
    object QrMatriAtiNOMEPROFESSOR: TWideStringField
      FieldName = 'NOMEPROFESSOR'
      Size = 100
    end
    object QrMatriAtiProfessor: TIntegerField
      FieldName = 'Professor'
    end
  end
  object QrMatriPer: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMatriPerAfterOpen
    SQL.Strings = (
      'SELECT map.Codigo, map.Controle, map.Conta,'
      'map.Periodo, per.Nome NOMEPERIODO, per.DSem,'
      'per.HIni, per.HFim, map.Item, map.SubItem, CASE 1 '
      'WHEN per.DSem=0 THEN "Domingo"'
      'WHEN per.DSem=1 THEN "Segunda-feira"'
      'WHEN per.DSem=2 THEN "Ter'#231'a-feira"'
      'WHEN per.DSem=3 THEN "Quarta-feira"'
      'WHEN per.DSem=4 THEN "Quinta-feira"'
      'WHEN per.DSem=5 THEN "Sexta-feira"'
      'WHEN per.DSem=6 THEN "S'#225'bado" END NOMEDSEM'
      'FROM matriper map'
      'LEFT JOIN turmastmp ttm ON ttm.Conta=map.Periodo'
      'LEFT JOIN periodos  per ON per.Codigo=ttm.Periodo'
      'WHERE map.Codigo=:P0'
      'AND map.Controle=:P1'
      'AND map.Conta=:P2'
      'AND map.Item=:P3'
      'ORDER BY per.DSem,  per.HIni, per.HFim')
    Left = 440
    Top = 33
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMatriPerCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMatriPerControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMatriPerConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMatriPerPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrMatriPerNOMEPERIODO: TWideStringField
      FieldName = 'NOMEPERIODO'
      Size = 50
    end
    object QrMatriPerDSem: TIntegerField
      FieldName = 'DSem'
    end
    object QrMatriPerHIni: TTimeField
      FieldName = 'HIni'
      DisplayFormat = 'hh:nn'
    end
    object QrMatriPerHFim: TTimeField
      FieldName = 'HFim'
      DisplayFormat = 'hh:nn'
    end
    object QrMatriPerNOMEDSEM: TWideStringField
      FieldName = 'NOMEDSEM'
      Size = 13
    end
    object QrMatriPerItem: TIntegerField
      FieldName = 'Item'
      Required = True
    end
    object QrMatriPerSubItem: TIntegerField
      FieldName = 'SubItem'
      Required = True
    end
  end
  object DsMatriPer: TDataSource
    DataSet = QrMatriPer
    Left = 468
    Top = 33
  end
  object PMImprime: TPopupMenu
    Left = 14
    Top = 49
    object Contrato1: TMenuItem
      Caption = '&Contrato'
      OnClick = Contrato1Click
    end
    object Recibo1: TMenuItem
      Caption = '&Recibo Bematech'
      OnClick = Recibo1Click
    end
    object ReciboMatricial1: TMenuItem
      Caption = 'Recibo Matricial'
      OnClick = ReciboMatricial1Click
    end
  end
  object TbImpReciC: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Tipo=1'
    Filtered = True
    TableName = 'impreci'
    Left = 116
    Top = 372
    object TbImpReciCTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object TbImpReciCLinha: TAutoIncField
      FieldName = 'Linha'
    end
    object TbImpReciCTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object TbImpReciCLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbImpReciCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbImpReciCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbImpReciCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbImpReciCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbImpReciCModo: TIntegerField
      FieldName = 'Modo'
    end
    object TbImpReciCNegr: TIntegerField
      FieldName = 'Negr'
    end
    object TbImpReciCItal: TIntegerField
      FieldName = 'Ital'
    end
    object TbImpReciCSubl: TIntegerField
      FieldName = 'Subl'
    end
    object TbImpReciCExpa: TIntegerField
      FieldName = 'Expa'
    end
    object TbImpReciCNOMEMODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMODO'
      Calculated = True
    end
  end
  object TbImpReciR: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Tipo=2'
    Filtered = True
    TableName = 'impreci'
    Left = 144
    Top = 372
    object TbImpReciRTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object TbImpReciRLinha: TAutoIncField
      FieldName = 'Linha'
    end
    object TbImpReciRTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object TbImpReciRLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbImpReciRDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbImpReciRDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbImpReciRUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbImpReciRUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbImpReciRModo: TIntegerField
      FieldName = 'Modo'
    end
    object TbImpReciRNegr: TIntegerField
      FieldName = 'Negr'
    end
    object TbImpReciRItal: TIntegerField
      FieldName = 'Ital'
    end
    object TbImpReciRSubl: TIntegerField
      FieldName = 'Subl'
    end
    object TbImpReciRExpa: TIntegerField
      FieldName = 'Expa'
    end
    object TbImpReciRNOMEMODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMODO'
      Calculated = True
    end
  end
  object QrCliProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.QuantN2,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE'
      'FROM entidades en '
      'WHERE en.Codigo=:P0')
    Left = 496
    Top = 33
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliProdCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliProdQuantN2: TFloatField
      FieldName = 'QuantN2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCliProdNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsCliProd: TDataSource
    DataSet = QrCliProd
    Left = 524
    Top = 33
  end
  object QrMatriPen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mr.Valor-mr.ACobrar) PENDENTE'
      'FROM matriren mr'
      'LEFT JOIN matricul  mc ON mc.Codigo=mr.Codigo'
      'WHERE mr.ACobrar > mr.Valor'
      'AND mc.Aluno=:P0')
    Left = 552
    Top = 33
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMatriPenPENDENTE: TFloatField
      FieldName = 'PENDENTE'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsMatriPen: TDataSource
    DataSet = QrMatriPen
    Left = 580
    Top = 33
  end
  object QrRespons: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Codigo '
      'CASE WHEN Tipo=0 THEN ENatal ELSE PNatal END Natal'
      'FROM entidades'
      'WHERE Tipo=1'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 488
    Top = 365
    object QrResponsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrResponsNatal: TDateField
      FieldName = 'Natal'
    end
    object QrResponsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsRespons: TDataSource
    DataSet = QrRespons
    Left = 516
    Top = 365
  end
end
