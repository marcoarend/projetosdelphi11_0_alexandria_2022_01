object FmImpPagtos: TFmImpPagtos
  Left = 234
  Top = 181
  Caption = 'MTR-PRINT-001 :: Impress'#227'o de Pagamentos'
  ClientHeight = 546
  ClientWidth = 741
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 741
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 682
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 623
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 572
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Vencimento de Matr'#237'culas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 572
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Vencimento de Matr'#237'culas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 572
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Vencimento de Matr'#237'culas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 59
    Width = 741
    Height = 347
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 1
    object Label4: TLabel
      Left = 64
      Top = 10
      Width = 37
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Aluno:'
      FocusControl = EdAluno
    end
    object Label5: TLabel
      Left = 64
      Top = 64
      Width = 58
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Empresa:'
      FocusControl = EdEmpresa
    end
    object TPIni: TDateTimePicker
      Left = 64
      Top = 148
      Width = 119
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 37853.604540590300000000
      Time = 37853.604540590300000000
      TabOrder = 5
    end
    object TPFim: TDateTimePicker
      Left = 187
      Top = 148
      Width = 119
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 37853.604540590300000000
      Time = 37853.604540590300000000
      TabOrder = 7
    end
    object GroupBox1: TGroupBox
      Left = 310
      Top = 118
      Width = 297
      Height = 56
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Situa'#231#227'o: '
      TabOrder = 8
      object CkVencido: TCheckBox
        Left = 10
        Top = 20
        Width = 86
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vencidos'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CkAberto: TCheckBox
        Left = 103
        Top = 20
        Width = 87
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Em Aberto'
        TabOrder = 1
      end
      object CkQuitado: TCheckBox
        Left = 202
        Top = 20
        Width = 86
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quitados'
        TabOrder = 2
      end
    end
    object RG1: TRadioGroup
      Left = 64
      Top = 182
      Width = 149
      Height = 124
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Ordem 1: '
      ItemIndex = 0
      Items.Strings = (
        'Sem ordena'#231#227'o'
        'Empresa'
        'Aluno'
        'Situa'#231#227'o'
        'Turma (1'#170')')
      TabOrder = 9
      OnClick = RG1Click
    end
    object CkGrade: TCheckBox
      Left = 507
      Top = 271
      Width = 70
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grade.'
      Checked = True
      State = cbChecked
      TabOrder = 15
    end
    object RG2: TRadioGroup
      Left = 207
      Top = 182
      Width = 149
      Height = 124
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Ordem 2: '
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'Sem ordena'#231#227'o'
        'Empresa'
        'Aluno'
        'Situa'#231#227'o'
        'Turma (1'#170')')
      TabOrder = 11
      OnClick = RG2Click
    end
    object RG3: TRadioGroup
      Left = 350
      Top = 182
      Width = 148
      Height = 124
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Ordem 3: '
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'Sem ordena'#231#227'o'
        'Empresa'
        'Aluno'
        'Situa'#231#227'o'
        'Turma (1'#170')')
      TabOrder = 13
      OnClick = RG3Click
    end
    object EdAluno: TdmkEditCB
      Left = 64
      Top = 30
      Width = 79
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAluno
      IgnoraDBLookupComboBox = False
    end
    object CBAluno: TdmkDBLookupComboBox
      Left = 148
      Top = 30
      Width = 459
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsAlunos
      TabOrder = 1
      dmkEditCB = EdAluno
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEmpresa: TdmkEditCB
      Left = 64
      Top = 84
      Width = 79
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 148
      Top = 84
      Width = 459
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'CodCliInt'
      ListField = 'Nome'
      ListSource = DsEmpresas
      TabOrder = 3
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object Ck1: TCheckBox
      Left = 64
      Top = 315
      Width = 70
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Agrupa.'
      TabOrder = 10
    end
    object Ck2: TCheckBox
      Left = 207
      Top = 315
      Width = 70
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Agrupa.'
      TabOrder = 12
    end
    object Ck3: TCheckBox
      Left = 350
      Top = 315
      Width = 70
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Agrupa.'
      TabOrder = 14
    end
    object CkIni: TCheckBox
      Left = 64
      Top = 123
      Width = 119
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Vencto inicial:'
      TabOrder = 4
    end
    object CkFim: TCheckBox
      Left = 187
      Top = 123
      Width = 115
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Vencto final:'
      TabOrder = 6
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 406
    Width = 741
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 737
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 460
    Width = 741
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 562
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 560
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
    end
  end
  object DsAlunos: TDataSource
    DataSet = QrAlunos
    Left = 244
    Top = 61
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 244
    Top = 105
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eci.CodCliInt, eci.CodEnti,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'#11
      'FROM enticliint eci'
      'LEFT JOIN entidades ent ON ent.Codigo = eci.CodEnti'
      'ORDER BY Nome')
    Left = 216
    Top = 105
    object QrEmpresasCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrEmpresasCodEnti: TIntegerField
      FieldName = 'CodEnti'
    end
    object QrEmpresasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrAlunos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY Nome, Codigo')
    Left = 216
    Top = 61
    object QrAlunosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrAlunosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrPagtosAnt: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPagtosAntCalcFields
    SQL.Strings = (
      'SELECT la.*, en1.Nome NOMEALUNO,'
      'en2.RazaoSocial NOMEEMPRESA,'
      'ma.Codigo MATRICULA, CASE 1'
      'WHEN (la.sit=0) AND (la.Vencimento<NOW())    THEN -2'
      'WHEN (la.sit=1) AND (la.Vencimento<NOW())'
      'THEN -1  WHEN (la.sit=0) AND (la.Vencimento>NOW())    THEN  0'
      'WHEN (la.sit=1) AND (la.Vencimento>NOW())    THEN  1'
      'WHEN (la.sit=2)'
      'THEN  2'
      'WHEN (la.sit=3)'
      'THEN  3'
      'ELSE -3 END REALSIT FROM lanctos la'
      'LEFT JOIN entidades en1 ON en1.Codigo=la.Cliente'
      'LEFT JOIN entidades en2 ON en2.Codigo=la.Cliente'
      'LEFT JOIN matriculas ma ON ma.Codigo=la.FatNum'
      'WHERE Data BETWEEN 20030101 AND 20061231'
      'AND(    ((la.sit=0) AND (la.Vencimento<NOW()))'
      'OR ((la.sit=1) AND (la.Vencimento<NOW()))'
      'OR ((la.sit=0) AND (la.Vencimento>NOW()))'
      'OR ((la.sit=1) AND (la.Vencimento>NOW()))'
      'OR (la.sit=2)'
      'OR (la.sit=3))'
      'ORDER BY REALSIT')
    Left = 8
    Top = 149
    object QrPagtosAntData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagtosAntTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagtosAntCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagtosAntControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosAntSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagtosAntAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagtosAntGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagtosAntDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosAntNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagtosAntDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagtosAntCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagtosAntCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagtosAntDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosAntSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagtosAntVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPagtosAntFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosAntFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPagtosAntFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosAntID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagtosAntID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagtosAntFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagtosAntBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosAntLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPagtosAntCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagtosAntLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagtosAntOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagtosAntLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagtosAntPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagtosAntMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagtosAntFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagtosAntCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagtosAntMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagtosAntMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagtosAntProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagtosAntDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagtosAntCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrPagtosAntNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPagtosAntVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrPagtosAntAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrPagtosAntLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagtosAntDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagtosAntDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagtosAntUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPagtosAntUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPagtosAntNOMEALUNO: TWideStringField
      FieldName = 'NOMEALUNO'
      Required = True
      Size = 100
    end
    object QrPagtosAntNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Required = True
      Size = 100
    end
    object QrPagtosAntMATRICULA: TIntegerField
      FieldName = 'MATRICULA'
      Required = True
    end
    object QrPagtosAntREALSIT: TLargeintField
      FieldName = 'REALSIT'
    end
    object QrPagtosAntNOMEEMPRESA2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEEMPRESA2'
      Size = 255
      Calculated = True
    end
    object QrPagtosAntNOMEALUNO2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALUNO2'
      Size = 255
      Calculated = True
    end
    object QrPagtosAntNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Calculated = True
    end
    object QrPagtosAntSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrPagtosAntVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      Calculated = True
    end
    object QrPagtosAntMULTAREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MULTAREAL'
      Calculated = True
    end
    object QrPagtosAntATRAZODD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRAZODD'
      Calculated = True
    end
    object QrPagtosAntNOMEVENCIDO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEVENCIDO'
      Size = 50
      Calculated = True
    end
    object QrPagtosAntATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      Calculated = True
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPagtosCalcFields
    SQL.Strings = (
      'SELECT tu.Nome NOMETURMA, en3.Nome NOMERESPONS,'
      'la.*, en1.Nome NOMEALUNO,'
      '       en2.RazaoSocial NOMEEMPRESA,'
      '       ma.Codigo MATRICULA, '
      'CASE 1 '
      '       WHEN (la.sit=0) AND (la.Vencimento<NOW())    THEN -2'
      '       WHEN (la.sit=1) AND (la.Vencimento<NOW())    THEN -1'
      '       WHEN (la.sit=0) AND (la.Vencimento>NOW())    THEN  0'
      'WHEN (la.sit=1) AND (la.Vencimento>NOW())    THEN  1'
      '       WHEN (la.sit=2)                              THEN  2'
      '       WHEN (la.sit=3)                              THEN  3'
      'ELSE -3 END REALSIT'
      'FROM lanctos la'
      'LEFT JOIN entidades en1  ON en1.Codigo=la.Cliente'
      'LEFT JOIN entidades en2  ON en2.Codigo=la.Cliente'
      'LEFT JOIN matriculas ma  ON ma.Codigo=la.FatNum'
      'LEFT JOIN matricursos mc ON mc.Codigo=ma.Codigo'
      'LEFT JOIN turmas tu      ON tu.Codigo=mc.Turma  '
      'LEFT JOIN entidades en3  ON en3.Codigo=tu.Respons'
      'WHERE Data BETWEEN 20030101 AND 20061231'
      'AND(    ((la.sit=0) AND (la.Vencimento<NOW()))'
      #9#9#9'      OR ((la.sit=1) AND (la.Vencimento<NOW()))'
      #9#9#9'      OR ((la.sit=0) AND (la.Vencimento>NOW()))'
      #9#9#9'      OR ((la.sit=1) AND (la.Vencimento>NOW()))'
      #9#9#9'      OR (la.sit=2) '
      #9#9#9'      OR (la.sit=3))'
      'ORDER BY REALSIT')
    Left = 412
    Top = 217
    object QrPagtosNOMEEMPRESA2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEEMPRESA2'
      Size = 255
      Calculated = True
    end
    object QrPagtosNOMEALUNO2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALUNO2'
      Size = 255
      Calculated = True
    end
    object QrPagtosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Calculated = True
    end
    object QrPagtosSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrPagtosVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      Calculated = True
    end
    object QrPagtosMULTAREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MULTAREAL'
      Calculated = True
    end
    object QrPagtosATRAZODD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRAZODD'
      Calculated = True
    end
    object QrPagtosNOMEVENCIDO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEVENCIDO'
      Size = 50
      Calculated = True
    end
    object QrPagtosATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      Calculated = True
    end
    object QrPagtosNOMETURMA: TWideStringField
      FieldName = 'NOMETURMA'
      Size = 100
    end
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagtosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagtosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagtosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagtosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagtosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagtosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagtosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagtosFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPagtosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagtosLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagtosOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagtosLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagtosPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagtosMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagtosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagtosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagtosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagtosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagtosProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagtosDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagtosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrPagtosNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPagtosVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrPagtosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrPagtosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagtosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagtosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagtosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPagtosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPagtosNOMEALUNO: TWideStringField
      FieldName = 'NOMEALUNO'
      Size = 100
    end
    object QrPagtosNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrPagtosMATRICULA: TIntegerField
      FieldName = 'MATRICULA'
    end
    object QrPagtosREALSIT: TLargeintField
      FieldName = 'REALSIT'
    end
    object QrPagtosNOMERESPONS: TWideStringField
      FieldName = 'NOMERESPONS'
      Size = 100
    end
  end
  object frxPagtosMatr: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38475.900256435200000000
    ReportOptions.LastChange = 42233.426318391210000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GH1, Engine do'
      '  begin'
      
        '  if <VFR_ORD1> >  0 then GH1.Visible := True else GH1.Visible :' +
        '= False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39#39';'
      
        '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'[frxDsPagtos."NOMEEM' +
        'PRESA"]'#39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'[frxDsPagtos."NOMEAL' +
        'UNO"]'#39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'[frxDsPagtos."SIT"]'#39 +
        ';'
      '  end'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GF1, Engine do'
      '  begin'
      '//  if VFR_ORD(VFR_ORD) > -1 then'
      '//  GF1.Visible := True else GF1.Visible := False;'
      
        '  if <VFR_ORD1> >  0 then GF1.Visible := True else GF1.Visible :' +
        '= False;'
      '  end'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GH2, Engine do'
      '  begin'
      
        '  if <VFR_ORD2> >  0 then GH2.Visible := True else GH2.Visible :' +
        '= False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39#39';'
      
        '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'[frxDsPagtos."NOMEEM' +
        'PRESA"]'#39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'[frxDsPagtos."NOMEAL' +
        'UNO"]'#39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'[frxDsPagtos."SIT"]'#39 +
        ';'
      '  end'
      'end;'
      ''
      'procedure GH3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GH3, Engine do'
      '  begin'
      
        '  if <VFR_ORD3> >  0 then GH3.Visible := True else GH3.Visible :' +
        '= False;'
      '  //'
      '  if <VFR_ORD3> =  0 then GH3.Condition := '#39#39';'
      
        '  if <VFR_ORD3> =  1 then GH3.Condition := '#39'[frxDsPagtos."NOMEEM' +
        'PRESA"]'#39';'
      
        '  if <VFR_ORD3> =  2 then GH3.Condition := '#39'[frxDsPagtos."NOMEAL' +
        'UNO"]'#39';'
      
        '  if <VFR_ORD3> =  3 then GH3.Condition := '#39'[frxDsPagtos."SIT"]'#39 +
        ';'
      '  end'
      'end;'
      ''
      'procedure GF3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GF3, Engine do'
      '  begin'
      
        '  if <VFR_ORD3> >  0 then GF3.Visible := True else GF3.Visible :' +
        '= False;'
      '  end'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GF2, Engine do'
      '  begin'
      
        '  if <VFR_ORD2> >  0 then GF2.Visible := True else GF2.Visible :' +
        '= False;'
      '  end'
      'end;'
      ''
      'procedure Memo5OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo5, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo5.FrameTyp := 15 else Memo5.FrameT' +
        'yp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo16OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo16, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo16.FrameTyp := 15 else Memo16.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo23OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo23, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo23.FrameTyp := 15 else Memo23.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo12.FrameTyp := 15 else Memo12.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo25OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo25, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo25.FrameTyp := 15 else Memo25.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo13.FrameTyp := 15 else Memo13.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo27OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo27, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo27.FrameTyp := 15 else Memo27.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo29OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo29, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo29.FrameTyp := 15 else Memo29.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo33OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo33, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo33.FrameTyp := 15 else Memo33.Fram' +
        'eTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo6OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo6, Engine do'
      '  begin'
      
        '    //if <VFR_GRADE> then Memo6.FrameTyp := 15 else Memo6.FrameT' +
        'yp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo57OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo57, Engine do'
      '  begin'
      '    //if <VFR_GRADE> then FrameTyp := 15 else FrameTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo59OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo59, Engine do'
      '  begin'
      '    //if <VFR_GRADE> then FrameTyp := 15 else FrameTyp := 0;'
      '  end'
      'end;'
      ''
      'procedure Memo61OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo61, Engine do'
      '  begin'
      '    //if <VFR_GRADE> then FrameTyp := 15 else FrameTyp := 0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxPagtosMatrGetValue
    Left = 440
    Top = 216
    Datasets = <
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagtos
        DataSetName = 'frxDsPagtos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 287.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 1084.725110000000000000
        object Memo32: TfrxMemoView
          Left = 606.000000000000000000
          Top = 8.440940000000001000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 50.000000000000000000
          Top = 4.440940000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 222.000000000000000000
          Top = 32.440940000000000000
          Width = 552.000000000000000000
          Height = 26.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 687.874460000000000000
        Width = 1084.725110000000000000
        object Memo31: TfrxMemoView
          Left = 888.582870000000000000
          Top = 9.259429999999953000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 976.582870000000000000
          Top = 9.259429999999953000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 108.000000000000000000
        Top = 113.385900000000000000
        Width = 1084.725110000000000000
        object Memo19: TfrxMemoView
          Left = 50.000000000000000000
          Top = 2.173159999999996000
          Width = 967.559060000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[NOMEREL_001]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = 50.000000000000000000
          Top = 26.173160000000040000
          Width = 960.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 54.000000000000000000
          Top = 54.173160000000010000
          Width = 963.559060000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo17: TfrxMemoView
          Left = 54.000000000000000000
          Top = 30.173160000000040000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Memo.UTF8W = (
            'Per'#237'odo:')
        end
        object Memo18: TfrxMemoView
          Left = 122.000000000000000000
          Top = 30.173160000000040000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 454.000000000000000000
          Top = 30.173160000000040000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Memo.UTF8W = (
            'Situa'#231#245'es:')
        end
        object Memo21: TfrxMemoView
          Left = 522.000000000000000000
          Top = 30.173160000000010000
          Width = 495.559060000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SITUACOES]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 54.047241649999990000
          Top = 82.173159999999990000
          Width = 47.779530000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 102.047244090000000000
          Top = 82.173159999999990000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 145.779530000000000000
          Top = 82.173159999999990000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 185.779530000000000000
          Top = 82.173159999999990000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Docum.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 225.779530000000000000
          Top = 82.173159999999990000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Matricula')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 265.779530000000000000
          Top = 82.173159999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 329.779530000000000000
          Top = 82.173159999999990000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Mora/dia')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 369.779530000000000000
          Top = 82.173159999999990000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Atzo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 461.779530000000000000
          Top = 82.173159999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 409.779530000000000000
          Top = 82.173159999999990000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 54.000000000000000000
          Top = 58.173160000000040000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Memo.UTF8W = (
            'Aluno:')
        end
        object Memo52: TfrxMemoView
          Left = 122.000000000000000000
          Top = 58.173160000000040000
          Width = 328.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[ALUNO]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 454.000000000000000000
          Top = 58.173160000000040000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Memo.UTF8W = (
            'Empresa:')
        end
        object Memo55: TfrxMemoView
          Left = 522.000000000000000000
          Top = 58.173160000000010000
          Width = 495.559060000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[PATROCINADORA]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 525.779530000000000000
          Top = 82.173159999999990000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Situa'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 589.779530000000000000
          Top = 82.173159999999990000
          Width = 308.220470000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Turma')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 898.000000000000000000
          Top = 82.173159999999990000
          Width = 120.188976380000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Respons'#225'vel')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        Height = 30.000000000000000000
        Top = 597.165740000000000000
        Width = 1084.725110000000000000
        object Line4: TfrxLineView
          Left = -6.000000000000000000
          Top = 26.613789999999990000
          Width = 1024.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo14: TfrxMemoView
          Left = 58.598330000000000000
          Top = 3.779530000000022000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 393.826771653543400000
          Top = 3.779530000000022000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."SALDO">)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 605.858267716535400000
          Top = 3.779530000000022000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 541.984251968503900000
          Top = 3.779530000000022000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."MULTAREAL">)]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 408.189240000000000000
        Width = 1084.725110000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagtos
        DataSetName = 'frxDsPagtos'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 58.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo5OnBeforePrint'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagtos."VENCIDO"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 102.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo16OnBeforePrint'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagtos."Data"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 146.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagtos."Controle"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 186.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagtos."Documento"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 226.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo25OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagtos."MATRICULA"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 266.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagtos."SALDO"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 330.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo27OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagtos."MoraDia"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 370.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo29OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,##0;-#,##0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagtos."ATRAZODD"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 462.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo33OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagtos."ATUALIZADO"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 410.000000000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagtos."MULTAREAL"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 526.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo57OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsPagtos."NOMESIT"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 590.000000000000000000
          Width = 308.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo59OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsPagtos."NOMETURMA"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 898.000000000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo61OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsPagtos."NOMERESPONS"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 283.464750000000000000
        Width = 1084.725110000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPagtos."NOMEEMPRESA"'
        object Memo1: TfrxMemoView
          Left = 54.000000000000000000
          Width = 680.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 548.031849999999900000
        Width = 1084.725110000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo39: TfrxMemoView
          Left = 58.000000000000000000
          Top = 3.747680000000060000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 86.000000000000000000
          Top = 3.747680000000060000
          Width = 304.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 394.000000000000000000
          Top = 3.747679999999946000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."SALDO">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 606.000000000000000000
          Top = 3.747679999999946000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 542.000000000000000000
          Top = 3.747679999999946000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."MULTAREAL">)]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 325.039580000000000000
        Width = 1084.725110000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPagtos."NOMEALUNO"'
        object Memo7: TfrxMemoView
          Left = 94.000000000000000000
          Width = 644.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 366.614410000000000000
        Width = 1084.725110000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsPagtos."SIT"'
        object Memo15: TfrxMemoView
          Left = 138.000000000000000000
          Width = 608.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 449.764070000000000000
        Width = 1084.725110000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Left = 58.000000000000000000
          Top = 2.235929999999996000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 86.000000000000000000
          Top = 2.235929999999996000
          Width = 304.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 394.000000000000000000
          Top = 2.235929999999996000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."SALDO">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 606.000000000000000000
          Top = 2.235929999999996000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 542.000000000000000000
          Top = 2.235929999999996000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."MULTAREAL">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 498.897960000000000000
        Width = 1084.725110000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo40: TfrxMemoView
          Left = 58.000000000000000000
          Top = 1.102039999999988000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 86.000000000000000000
          Top = 1.102039999999988000
          Width = 304.000000000000000000
          Height = 18.000000000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 394.000000000000000000
          Top = 1.102039999999988000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."SALDO">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 606.000000000000000000
          Top = 1.102039999999988000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 542.000000000000000000
          Top = 1.102039999999988000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagtos."MULTAREAL">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPagtos: TfrxDBDataset
    UserName = 'frxDsPagtos'
    CloseDataSource = False
    DataSet = QrPagtos
    BCDToCurrency = False
    Left = 468
    Top = 216
  end
end
