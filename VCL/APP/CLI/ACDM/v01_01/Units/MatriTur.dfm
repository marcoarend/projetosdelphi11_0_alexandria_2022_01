�
 TFMMATRITUR 0�  TPF0TFmMatriTur
FmMatriTurLeft�TopCaptionMTR-CADAS-003 :: Turma do PlanoClientHeightClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnCreate
FormCreateOnResize
FormResizePixelsPerInchx
TextHeight TPanelPainelDadosLeft Top Width�Height� Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TPanelPanel3Left Top;Width�Height� Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientParentColor	TabOrder  TLabelLabel1LeftTopWidth9HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption
Matricula:FocusControlEdCodigo  TLabelLabel2LeftTop6Width*HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionTurma:FocusControlEdTurma  TLabelLabel3LeftTop6Width"HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionFator:FocusControlEdFator  TLabelLabel4LeftvTopWidthJHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Renovação:FocusControl
EdControle  TLabelLabel5LeftTopgWidth_HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionNome da turma:FocusControlDBEdit1  TSpeedButtonSpeedButton1Left�TopJWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption...OnClickSpeedButton1Click  TdmkEditEdCodigoLeftTopWidthbHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	AlignmenttaRightJustifyColor	clBtnFaceEnabledFont.CharsetDEFAULT_CHARSET
Font.Color4_~ Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder 
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  
TdmkEditCBEdTurmaLeftTopJWidthEHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBTurmaIgnoraDBLookupComboBox  TdmkDBLookupComboBoxCBTurmaLeftTTopJWidth�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldCodigo	ListFieldID
ListSourceDsTurmasTabOrder	dmkEditCBEdTurmaUpdTypeutYesLocF7SQLMasc$#  TdmkEditEdFatorLeftTopJWidthbHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfDoubleMskTypefmtNoneDecimalSize	LeftZeros NoEnterToTabNoForceUppercaseValMin0,01ValMax1000000ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0,01UpdTypeutYesObrigatorioPermiteNuloValueVariant أp=
ף�?ValWarn  TdmkEdit
EdControleLeftvTopWidthcHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	AlignmenttaRightJustifyColor	clBtnFaceEnabledFont.CharsetDEFAULT_CHARSET
Font.Color4_~ Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  	TCheckBoxCkAdicionarLeft� TopWidth� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption#   Adicionar todos cursos e horários.Checked	State	cbCheckedTabOrder  TDBEditDBEdit1LeftTop{WidthmHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	DataFieldNome
DataSourceDsTurmasTabOrder   TPanelPainelTituloLeft Top Width�Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalTopCaptionTurma da PlanoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style ParentColor	
ParentFontTabOrder TImageImage1LeftTopWidth%Height9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientTransparent	  	TdmkLabelLaTipoLeft&TopWidtheHeight9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight	AlignmenttaCenterAutoSizeCaptionTravadoFont.CharsetANSI_CHARSET
Font.Color4_~ Font.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	UpdTypeutYesSQLTypestLok    TPanelPainelConfirmaLeft Top� Width�Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder TBitBtn	BtDesisteTagLeft�TopWidthoHeight1Margins.LeftMargins.TopMargins.RightMargins.BottomCaption&Desiste	NumGlyphsTabOrderOnClickBtDesisteClick  TBitBtn
BtConfirmaTagLeftTopWidthnHeight1Margins.LeftMargins.TopMargins.RightMargins.BottomCaption	&Confirma	NumGlyphsTabOrder OnClickBtConfirmaClick   TmySQLQueryQrTurmasDatabase	Dmod.MyDBSQL.StringsSELECT * FROM turmasWHERE Codigo>0ORDER BY ID Left$Top8 TIntegerFieldQrTurmasCodigo	FieldNameCodigo  TStringFieldQrTurmasNome	FieldNameNomeSized  TStringField
QrTurmasID	FieldNameIDSize  TIntegerFieldQrTurmasGrupo	FieldNameGrupo  TSmallintFieldQrTurmasAtiva	FieldNameAtiva  TFloatFieldQrTurmasTotalPeso	FieldName	TotalPeso  TIntegerFieldQrTurmasRespons	FieldNameRespons  TIntegerFieldQrTurmasLotacao	FieldNameLotacao  TIntegerField
QrTurmasLk	FieldNameLk  
TDateFieldQrTurmasDataCad	FieldNameDataCad  
TDateFieldQrTurmasDataAlt	FieldNameDataAlt  TIntegerFieldQrTurmasUserCad	FieldNameUserCad  TIntegerFieldQrTurmasUserAlt	FieldNameUserAlt   TDataSourceDsTurmasDataSetQrTurmasLeft@Top8  TmySQLQueryQrSomaDatabase	Dmod.MyDBSQL.StringsSELECT SUM(Fator) FatorFROM matriturWHERE Codigo=:P0 LeftTop	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TFloatFieldQrSomaFator	FieldNameFator   TmySQLQueryQrTurmasItsDatabase	Dmod.MyDBSQL.StringsSELECT Controle FROM turmasitsWHERE Codigo=:P0     Left(Top	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrTurmasItsControle	FieldNameControle   TmySQLQueryQrTurmasTmpDatabase	Dmod.MyDBSQL.StringsSELECT Conta FROM turmastmpWHERE Codigo=:P0AND Controle=:P1 LeftDTop	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown DataType	ftUnknownNameP1	ParamType	ptUnknown   TIntegerFieldQrTurmasTmpConta	FieldNameConta    