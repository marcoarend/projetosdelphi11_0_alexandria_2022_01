object FmMatriAti: TFmMatriAti
  Left = 422
  Top = 238
  Caption = 'MTR-CADAS-005 :: Atividade da Matr'#237'cula'
  ClientHeight = 287
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 49
    Width = 548
    Height = 179
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 15
      Top = 54
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 98
      Top = 54
      Width = 294
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o da Atividade cadastrada para a turma:'
    end
    object Label3: TLabel
      Left = 15
      Top = 5
      Width = 57
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Matr'#237'cula:'
    end
    object Label4: TLabel
      Left = 98
      Top = 5
      Width = 74
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Renovac'#227'o:'
    end
    object Label6: TLabel
      Left = 182
      Top = 5
      Width = 42
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Turma:'
    end
    object Label7: TLabel
      Left = 15
      Top = 108
      Width = 61
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Professor:'
      FocusControl = DBEdit1
    end
    object Label8: TLabel
      Left = 374
      Top = 108
      Width = 36
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Local:'
      FocusControl = DBEdit2
    end
    object SpeedButton1: TSpeedButton
      Left = 513
      Top = 74
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label5: TLabel
      Left = 1
      Top = 162
      Width = 546
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Alignment = taCenter
      Caption = 
        'Somente s'#227'o mostradas as atividades cadastradas na turma selecio' +
        'nada!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitWidth = 515
    end
    object CBAtividade: TdmkDBLookupComboBox
      Left = 98
      Top = 74
      Width = 414
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Controle'
      ListField = 'NOMECURSO'
      ListSource = DsAtividades
      TabOrder = 1
      dmkEditCB = EdAtividade
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdAtividade: TdmkEditCB
      Left = 15
      Top = 74
      Width = 80
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAtividade
      IgnoraDBLookupComboBox = False
    end
    object EdCodigo: TdmkEdit
      Left = 15
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 98
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdConta: TdmkEdit
      Left = 182
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object DBEdit1: TdmkDBEdit
      Left = 15
      Top = 128
      Width = 355
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'NOMEPROFESSOR'
      DataSource = DsAtividades
      ReadOnly = True
      TabOrder = 5
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit2: TDBEdit
      Left = 374
      Top = 128
      Width = 164
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'NOMESALA'
      DataSource = DsAtividades
      ReadOnly = True
      TabOrder = 6
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 228
    Width = 548
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Left = 17
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Left = 426
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 548
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Atividade da Matr'#237'cula'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 446
      Top = 1
      Width = 100
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 445
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object DsAtividades: TDataSource
    DataSet = QrAtividades
    Left = 236
    Top = 58
  end
  object QrAtividades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tui.Controle, tui.Curso, tui.Sala, tui.Professor,'
      'sal.Nome NOMESALA, cur.Nome NOMECURSO, CASE'
      'WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEPROFESSOR'
      'FROM turmasits tui'
      'LEFT JOIN salas     sal ON sal.Codigo=tui.Sala'
      'LEFT JOIN cursos    cur ON cur.Codigo=tui.Curso'
      'LEFT JOIN entidades ent ON ent.Codigo=tui.Professor'
      'WHERE tui.Codigo=:P0')
    Left = 208
    Top = 58
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtividadesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAtividadesCurso: TIntegerField
      FieldName = 'Curso'
      Required = True
    end
    object QrAtividadesSala: TIntegerField
      FieldName = 'Sala'
      Required = True
    end
    object QrAtividadesProfessor: TIntegerField
      FieldName = 'Professor'
      Required = True
    end
    object QrAtividadesNOMESALA: TWideStringField
      FieldName = 'NOMESALA'
      Required = True
      Size = 50
    end
    object QrAtividadesNOMECURSO: TWideStringField
      FieldName = 'NOMECURSO'
      Size = 50
    end
    object QrAtividadesNOMEPROFESSOR: TWideStringField
      FieldName = 'NOMEPROFESSOR'
      Size = 100
    end
  end
end
