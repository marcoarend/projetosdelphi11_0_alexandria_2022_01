unit Matricul;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, Menus, dmkGeral, dmkEdit, dmkEditCB, dmkDBLookupComboBox,
  dmkEditDateTimePicker, dmkLabel, UnDmkProcFunc, UnDmkEnums, DmkDAC_PF;

type
  TFmMatricul = class(TForm)
    PainelDados: TPanel;
    DsMatricul: TDataSource;
    QrMatricul: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    Painel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdAluno: TDBEdit;
    Label2: TLabel;
    QrMatriculNOMEALUNO: TWideStringField;
    QrMatriculNOMEORIENTADOR: TWideStringField;
    QrMatriculCodigo: TIntegerField;
    QrMatriculAluno: TIntegerField;
    QrMatriculOrientador: TIntegerField;
    QrMatriculDataC: TDateField;
    QrMatriculContrato: TWideStringField;
    QrMatriculAtivo: TSmallintField;
    QrMatriculLk: TIntegerField;
    QrMatriculDataCad: TDateField;
    QrMatriculDataAlt: TDateField;
    QrMatriculUserCad: TIntegerField;
    QrMatriculUserAlt: TIntegerField;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    PainelMovimento: TPanel;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrMatriRen: TmySQLQuery;
    DsMatriRen: TDataSource;
    QrMatriRenCodigo: TIntegerField;
    QrMatriRenDataI: TDateField;
    QrMatriRenDataF: TDateField;
    QrMatriRenValor: TFloatField;
    QrMatriRenLk: TIntegerField;
    QrMatriRenDataCad: TDateField;
    QrMatriRenDataAlt: TDateField;
    QrMatriRenUserCad: TIntegerField;
    QrMatriRenUserAlt: TIntegerField;
    QrMatriRenControle: TIntegerField;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    CBAluno: TdmkDBLookupComboBox;
    EdAluno: TdmkEditCB;
    TPDataC: TdmkEditDateTimePicker;
    Bevel1: TBevel;
    CkAtivo: TCheckBox;
    EdOrientador: TdmkEditCB;
    CBOrientador: TdmkDBLookupComboBox;
    QrAlunos: TmySQLQuery;
    DsAlunos: TDataSource;
    QrAlunosNOMEENTIDADE: TWideStringField;
    QrAlunosCodigo: TIntegerField;
    DsOrientadores: TDataSource;
    QrOrientadores: TmySQLQuery;
    StringField1: TWideStringField;
    IntegerField1: TIntegerField;
    EdContrato: TdmkEdit;
    QrMatriculATIVO_TXT: TWideStringField;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    QrMatriRenSEQ: TIntegerField;
    Panel6: TPanel;
    GridPagtos: TDBGrid;
    QrPagtos: TmySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosControle: TIntegerField;
    QrPagtosSub: TSmallintField;
    QrPagtosAutorizacao: TIntegerField;
    QrPagtosGenero: TIntegerField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosNotaFiscal: TIntegerField;
    QrPagtosDebito: TFloatField;
    QrPagtosCredito: TFloatField;
    QrPagtosCompensado: TDateField;
    QrPagtosDocumento: TFloatField;
    QrPagtosSit: TIntegerField;
    QrPagtosVencimento: TDateField;
    QrPagtosID_Pgto: TIntegerField;
    QrPagtosID_Sub: TSmallintField;
    QrPagtosFatura: TWideStringField;
    QrPagtosBanco: TIntegerField;
    QrPagtosLocal: TIntegerField;
    QrPagtosCartao: TIntegerField;
    QrPagtosLinha: TIntegerField;
    QrPagtosOperCount: TIntegerField;
    QrPagtosLancto: TIntegerField;
    QrPagtosPago: TFloatField;
    QrPagtosMez: TIntegerField;
    QrPagtosFornecedor: TIntegerField;
    QrPagtosCliente: TIntegerField;
    QrPagtosMoraDia: TFloatField;
    QrPagtosMulta: TFloatField;
    QrPagtosProtesto: TDateField;
    QrPagtosDataDoc: TDateField;
    QrPagtosCtrlIni: TIntegerField;
    QrPagtosNivel: TIntegerField;
    QrPagtosVendedor: TIntegerField;
    QrPagtosAccount: TIntegerField;
    QrPagtosLk: TIntegerField;
    QrPagtosDataCad: TDateField;
    QrPagtosDataAlt: TDateField;
    QrPagtosUserCad: TIntegerField;
    QrPagtosUserAlt: TIntegerField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrPagtosNOMESIT: TWideStringField;
    QrPagtosSALDO: TFloatField;
    QrPagtosNOMETIPO: TWideStringField;
    QrPagtosVENCIDO: TDateField;
    QrPagtosATRAZODD: TFloatField;
    QrPagtosATUALIZADO: TFloatField;
    QrPagtosCliInt: TIntegerField;
    DsPagtos: TDataSource;
    QrPagtos2: TmySQLQuery;
    QrPagtos2Data: TDateField;
    QrPagtos2Tipo: TSmallintField;
    QrPagtos2Carteira: TIntegerField;
    QrPagtos2Controle: TIntegerField;
    QrPagtos2Sub: TSmallintField;
    QrPagtos2Autorizacao: TIntegerField;
    QrPagtos2Genero: TIntegerField;
    QrPagtos2Descricao: TWideStringField;
    QrPagtos2NotaFiscal: TIntegerField;
    QrPagtos2Debito: TFloatField;
    QrPagtos2Credito: TFloatField;
    QrPagtos2Compensado: TDateField;
    QrPagtos2Documento: TFloatField;
    QrPagtos2Sit: TIntegerField;
    QrPagtos2Vencimento: TDateField;
    QrPagtos2FatID: TIntegerField;
    QrPagtos2FatID_Sub: TIntegerField;
    QrPagtos2FatParcela: TIntegerField;
    QrPagtos2ID_Pgto: TIntegerField;
    QrPagtos2ID_Sub: TSmallintField;
    QrPagtos2Fatura: TWideStringField;
    QrPagtos2Banco: TIntegerField;
    QrPagtos2Local: TIntegerField;
    QrPagtos2Cartao: TIntegerField;
    QrPagtos2Linha: TIntegerField;
    QrPagtos2OperCount: TIntegerField;
    QrPagtos2Lancto: TIntegerField;
    QrPagtos2Pago: TFloatField;
    QrPagtos2Mez: TIntegerField;
    QrPagtos2Fornecedor: TIntegerField;
    QrPagtos2Cliente: TIntegerField;
    QrPagtos2MoraDia: TFloatField;
    QrPagtos2Multa: TFloatField;
    QrPagtos2Protesto: TDateField;
    QrPagtos2DataDoc: TDateField;
    QrPagtos2CtrlIni: TIntegerField;
    QrPagtos2Nivel: TIntegerField;
    QrPagtos2Vendedor: TIntegerField;
    QrPagtos2Account: TIntegerField;
    QrPagtos2Lk: TIntegerField;
    QrPagtos2DataCad: TDateField;
    QrPagtos2DataAlt: TDateField;
    QrPagtos2UserCad: TIntegerField;
    QrPagtos2UserAlt: TIntegerField;
    QrPagtos2NOMECARTEIRA: TWideStringField;
    QrPagtos2CARTEIRATIPO: TIntegerField;
    QrLoclancto: TmySQLQuery;
    QrLoclanctoData: TDateField;
    QrLoclanctoTipo: TSmallintField;
    QrLoclanctoCarteira: TIntegerField;
    QrLoclanctoControle: TIntegerField;
    QrLoclanctoSub: TSmallintField;
    QrLoclanctoAutorizacao: TIntegerField;
    QrLoclanctoGenero: TIntegerField;
    QrLoclanctoDescricao: TWideStringField;
    QrLoclanctoNotaFiscal: TIntegerField;
    QrLoclanctoDebito: TFloatField;
    QrLoclanctoCredito: TFloatField;
    QrLoclanctoCompensado: TDateField;
    QrLoclanctoDocumento: TFloatField;
    QrLoclanctoSit: TIntegerField;
    QrLoclanctoVencimento: TDateField;
    QrLoclanctoFatID: TIntegerField;
    QrLoclanctoFatID_Sub: TIntegerField;
    QrLoclanctoFatParcela: TIntegerField;
    QrLoclanctoID_Pgto: TIntegerField;
    QrLoclanctoID_Sub: TSmallintField;
    QrLoclanctoFatura: TWideStringField;
    QrLoclanctoBanco: TIntegerField;
    QrLoclanctoLocal: TIntegerField;
    QrLoclanctoCartao: TIntegerField;
    QrLoclanctoLinha: TIntegerField;
    QrLoclanctoOperCount: TIntegerField;
    QrLoclanctoLancto: TIntegerField;
    QrLoclanctoPago: TFloatField;
    QrLoclanctoMez: TIntegerField;
    QrLoclanctoFornecedor: TIntegerField;
    QrLoclanctoCliente: TIntegerField;
    QrLoclanctoMoraDia: TFloatField;
    QrLoclanctoMulta: TFloatField;
    QrLoclanctoProtesto: TDateField;
    QrLoclanctoDataDoc: TDateField;
    QrLoclanctoCtrlIni: TIntegerField;
    QrLoclanctoNivel: TIntegerField;
    QrLoclanctoVendedor: TIntegerField;
    QrLoclanctoAccount: TIntegerField;
    QrLoclanctoLk: TIntegerField;
    QrLoclanctoDataCad: TDateField;
    QrLoclanctoDataAlt: TDateField;
    QrLoclanctoUserCad: TIntegerField;
    QrLoclanctoUserAlt: TIntegerField;
    PMExclui: TPopupMenu;
    Panel4: TPanel;
    Splitter1: TSplitter;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGrid2: TDBGrid;
    Panel11: TPanel;
    Panel12: TPanel;
    DBGrid3: TDBGrid;
    Panel13: TPanel;
    Panel14: TPanel;
    DBGrid4: TDBGrid;
    QrMatriTur: TmySQLQuery;
    DsMatriTur: TDataSource;
    DsMatriAti: TDataSource;
    QrMatriAti: TmySQLQuery;
    QrMatriAtiNOMECURSO: TWideStringField;
    QrMatriAtiCodigo: TIntegerField;
    QrMatriAtiControle: TIntegerField;
    QrMatriAtiConta: TIntegerField;
    QrMatriAtiAtividade: TIntegerField;
    QrMatriAtiLk: TIntegerField;
    QrMatriAtiDataCad: TDateField;
    QrMatriAtiDataAlt: TDateField;
    QrMatriAtiUserCad: TIntegerField;
    QrMatriAtiUserAlt: TIntegerField;
    QrMatriTurCodigo: TIntegerField;
    QrMatriTurControle: TIntegerField;
    QrMatriTurConta: TIntegerField;
    QrMatriTurTurma: TIntegerField;
    QrMatriTurLk: TIntegerField;
    QrMatriTurDataCad: TDateField;
    QrMatriTurDataAlt: TDateField;
    QrMatriTurUserCad: TIntegerField;
    QrMatriTurUserAlt: TIntegerField;
    QrMatriTurFator: TFloatField;
    Label8: TLabel;
    BtRefaz: TBitBtn;
    BtRefazInclui: TBitBtn;
    BtRefazAltera: TBitBtn;
    BtRefazExclui: TBitBtn;
    Label10: TLabel;
    BtTurmasInclui: TBitBtn;
    BtTurmasExclui: TBitBtn;
    QrMatriTurIDTURMA: TWideStringField;
    QrMatriPer: TmySQLQuery;
    DsMatriPer: TDataSource;
    QrMatriAtiItem: TIntegerField;
    BtAtividadesExclui: TBitBtn;
    BtAtividadesInclui: TBitBtn;
    Label20: TLabel;
    Label21: TLabel;
    BtRelogioInclui: TBitBtn;
    BtRelogioExclui: TBitBtn;
    QrMatriPerCodigo: TIntegerField;
    QrMatriPerControle: TIntegerField;
    QrMatriPerConta: TIntegerField;
    QrMatriPerPeriodo: TIntegerField;
    QrMatriPerNOMEPERIODO: TWideStringField;
    QrMatriPerDSem: TIntegerField;
    QrMatriPerHIni: TTimeField;
    QrMatriPerHFim: TTimeField;
    QrMatriPerNOMEDSEM: TWideStringField;
    QrMatriPerItem: TIntegerField;
    QrMatriPerSubItem: TIntegerField;
    Splitter2: TSplitter;
    QrMatriTurNOMETURMA: TWideStringField;
    Label22: TLabel;
    BtPagtosInclui: TBitBtn;
    BtPagtosExclui: TBitBtn;
    Pagamento1: TMenuItem;
    Todospagamentosdestarenovao1: TMenuItem;
    QrMatriAtiRefere: TWideStringField;
    BtRefresh: TBitBtn;
    PMImprime: TPopupMenu;
    Contrato1: TMenuItem;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    Label23: TLabel;
    DBEdit6: TDBEdit;
    Label24: TLabel;
    DBEdit7: TDBEdit;
    QrMatriRenP_DESCO: TFloatField;
    QrMatriRenAtivo: TIntegerField;
    QrMatriRenTotalFator: TFloatField;
    QrMatriRenPreco: TFloatField;
    QrMatriRenPeriodo: TIntegerField;
    QrMatriRenTaxa: TFloatField;
    DBGrid6: TDBGrid;
    QrMatriRenNOMETARIFA: TWideStringField;
    QrMatriRenPRECO_T: TFloatField;
    Recibo1: TMenuItem;
    TbImpReciC: TmySQLTable;
    TbImpReciCTipo: TIntegerField;
    TbImpReciCLinha: TAutoIncField;
    TbImpReciCTexto: TWideStringField;
    TbImpReciCLk: TIntegerField;
    TbImpReciCDataCad: TDateField;
    TbImpReciCDataAlt: TDateField;
    TbImpReciCUserCad: TIntegerField;
    TbImpReciCUserAlt: TIntegerField;
    TbImpReciCModo: TIntegerField;
    TbImpReciCNegr: TIntegerField;
    TbImpReciCItal: TIntegerField;
    TbImpReciCSubl: TIntegerField;
    TbImpReciCExpa: TIntegerField;
    TbImpReciCNOMEMODO: TWideStringField;
    TbImpReciR: TmySQLTable;
    TbImpReciRTipo: TIntegerField;
    TbImpReciRLinha: TAutoIncField;
    TbImpReciRTexto: TWideStringField;
    TbImpReciRLk: TIntegerField;
    TbImpReciRDataCad: TDateField;
    TbImpReciRDataAlt: TDateField;
    TbImpReciRUserCad: TIntegerField;
    TbImpReciRUserAlt: TIntegerField;
    TbImpReciRModo: TIntegerField;
    TbImpReciRNegr: TIntegerField;
    TbImpReciRItal: TIntegerField;
    TbImpReciRSubl: TIntegerField;
    TbImpReciRExpa: TIntegerField;
    TbImpReciRNOMEMODO: TWideStringField;
    QrMatriRenDESCO_V: TFloatField;
    QrMatriRenACobrar: TFloatField;
    DBEdit8: TDBEdit;
    Label3: TLabel;
    DBEdit9: TDBEdit;
    Label25: TLabel;
    DBEdit10: TDBEdit;
    Label26: TLabel;
    QrCliProd: TmySQLQuery;
    DsCliProd: TDataSource;
    QrCliProdCodigo: TIntegerField;
    QrCliProdQuantN2: TFloatField;
    QrCliProdNOMECLIENTE: TWideStringField;
    QrMatriRenPENDENTE_TOTAL: TFloatField;
    QrMatriPen: TmySQLQuery;
    DsMatriPen: TDataSource;
    QrMatriPenPENDENTE: TFloatField;
    QrMatriAtiNOMEPROFESSOR: TWideStringField;
    QrMatriAtiProfessor: TIntegerField;
    ReciboMatricial1: TMenuItem;
    DBEdit11: TDBEdit;
    Label27: TLabel;
    QrPagtosICMS_P: TFloatField;
    QrPagtosICMS_V: TFloatField;
    QrPagtosDuplicata: TWideStringField;
    QrPagtosDepto: TIntegerField;
    QrPagtosDescoPor: TIntegerField;
    QrPagtosForneceI: TIntegerField;
    QrPagtosQtde: TFloatField;
    QrPagtosCNPJCPF: TWideStringField;
    QrPagtosEmitente: TWideStringField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosCNPJCPF_TXT: TWideStringField;
    Label28: TLabel;
    Label29: TLabel;
    QrPagtosFatID: TIntegerField;
    QrPagtosFatID_Sub: TIntegerField;
    QrPagtosFatNum: TFloatField;
    QrPagtosFatParcela: TIntegerField;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    QrPagtos2FatNum: TFloatField;
    QrLoclanctoFatNum: TFloatField;
    SpeedButton7: TSpeedButton;
    LaTipo: TdmkLabel;
    EdRespons: TdmkEditCB;
    Label14: TLabel;
    CBRespons: TdmkDBLookupComboBox;
    SpeedButton8: TSpeedButton;
    QrRespons: TmySQLQuery;
    DsRespons: TDataSource;
    Panel15: TPanel;
    BtDesiste: TBitBtn;
    QrMatriculAlterWeb: TSmallintField;
    QrMatriculRespons: TIntegerField;
    QrMatriculNOMERESPONS: TWideStringField;
    DBEdit12: TDBEdit;
    Label15: TLabel;
    QrResponsNome: TWideStringField;
    QrResponsCodigo: TIntegerField;
    QrPagtos2NOMECART2: TWideStringField;
    BtHorarioGerencia: TBitBtn;
    QrMatriRenTarBase: TIntegerField;
    QrMatriRenDesativar: TSmallintField;
    QrMatriRenNO_ATIVO: TWideStringField;
    QrPagtosAgencia: TIntegerField;
    QrMatriRenEmpresa: TIntegerField;
    QrMatriRenCodCliInt: TIntegerField;
    Label16: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrMatriculEmpresa: TIntegerField;
    QrAlunosNatal: TDateField;
    QrResponsNatal: TDateField;
    QrPagtosNOMECONTA: TWideStringField;
    EdObserv: TdmkEdit;
    Label30: TLabel;
    QrMatriculObserv: TWideStringField;
    Label31: TLabel;
    DBEdit13: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMatriculAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrMatriculAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMatriculBeforeOpen(DataSet: TDataSet);
    procedure QrMatriculCalcFields(DataSet: TDataSet);
    procedure DBEdit5Change(Sender: TObject);
    procedure QrMatriRenCalcFields(DataSet: TDataSet);
    procedure Atual1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Todos1Click(Sender: TObject);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure QrMatriRenAfterScroll(DataSet: TDataSet);
    procedure QrMatriTurAfterScroll(DataSet: TDataSet);
    procedure BtRefazIncluiClick(Sender: TObject);
    procedure BtRefazAlteraClick(Sender: TObject);
    procedure BtRefazExcluiClick(Sender: TObject);
    procedure BtRefazClick(Sender: TObject);
    procedure BtTurmasIncluiClick(Sender: TObject);
    procedure BtTurmasExcluiClick(Sender: TObject);
    procedure BtAtividadesIncluiClick(Sender: TObject);
    procedure BtAtividadesExcluiClick(Sender: TObject);
    procedure QrMatriAtiAfterScroll(DataSet: TDataSet);
    procedure QrMatriAtiAfterOpen(DataSet: TDataSet);
    procedure QrMatriTurAfterOpen(DataSet: TDataSet);
    procedure QrMatriRenAfterOpen(DataSet: TDataSet);
    procedure QrMatriPerAfterOpen(DataSet: TDataSet);
    procedure BtRelogioIncluiClick(Sender: TObject);
    procedure BtRelogioExcluiClick(Sender: TObject);
    procedure QrMatriTurAfterClose(DataSet: TDataSet);
    procedure BtPagtosIncluiClick(Sender: TObject);
    procedure Pagamento1Click(Sender: TObject);
    procedure Todospagamentosdestarenovao1Click(Sender: TObject);
    procedure QrMatriRenAfterClose(DataSet: TDataSet);
    procedure QrMatriculAfterClose(DataSet: TDataSet);
    procedure QrPagtosAfterOpen(DataSet: TDataSet);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtPagtosExcluiClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Contrato1Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure DBEdit8Change(Sender: TObject);
    procedure DBEdit9Change(Sender: TObject);
    procedure DBEdit10Change(Sender: TObject);
    procedure ReciboMatricial1Click(Sender: TObject);
    procedure QrMatriculBeforeClose(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure QrMatriRenBeforeClose(DataSet: TDataSet);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtHorarioGerenciaClick(Sender: TObject);
    procedure QrMatriAtiBeforeClose(DataSet: TDataSet);
    procedure CBResponsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAlunoChange(Sender: TObject);
  private
    FTbLctA: String;
    FMatriRen: Integer;
    FTempoX: TTime;
    FRespParen: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenMatriAti(Item: Integer);
    procedure ReopenMatriTur;
    procedure ReopenPagtos();
    procedure ReopenMatriRen(Controle: Integer);
    procedure DefineVarDup;
    procedure ExcluiItemDuplicata;
    procedure ExcluiTodasParcelas;
    procedure IncluiNovoPeriodo;
    procedure RenovaPeriodo;
    procedure AlteraRenovacao;
    procedure ExcluiRenovacao;
    procedure IncluiTurma;
    procedure ExcluiTurma;
    procedure ExcluiAtividade;
    procedure ExcluiPeriodo;
    //
    function  ParcelasToTxt(): String;
    function  CursosEHorarios(): String;
    procedure ReopenCliProd(Cliente: Integer);
    function  ImprimeCupom(Saida: Integer): Boolean;
    function  ImprimeLinhaCupom(Saida: Integer; Texto: String;
              TpoLtra, Italic, Sublin, expand, enfat: Integer): Integer;
    //
    function  ValidaTabelaFinanceiro(): Boolean;
    procedure ConfiguraRespons(Todos: Boolean);
    procedure ReopenRespon();
  public
    { Public declarations }
    FIniciado: Boolean;
    FLocMa1: Integer;
    FPlural: String;
    procedure ReopenMatriPer();
    procedure AtualizaAtivo(Codigo, Controle, Desativar: Integer);
  end;

var
  FmMatricul: TFmMatricul;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MatriRen, MatriTur, UnInternalConsts3, MatriPer,
  MatriAti, Principal, MatriculContrato, DotPrint, MyDBCheck, ModuleGeral,
  MatriPer2, ModuloBemaNaoFiscal, UnPagtos;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMatricul.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMatricul.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMatriculCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

function TFmMatricul.ValidaTabelaFinanceiro: Boolean;
begin
  Result := True;
  //
  if FTbLctA = '' then
  begin
    Geral.MB_Erro('Tabela de lan�amentos financeiros n�o definida!');
    Result := False;
  end;
end;

procedure TFmMatricul.DefParams;
begin
  VAR_GOTOTABELA := 'Matricul';
  VAR_GOTOMYSQLTABLE := QrMatricul;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := 'al.Nome';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT');
  VAR_SQLx.Add('CASE WHEN al.Tipo=0 THEN al.RazaoSocial');
  VAR_SQLx.Add('ELSE al.Nome END NOMEALUNO,');
  VAR_SQLx.Add('CASE WHEN oi.Tipo=0 THEN oi.RazaoSocial');
  VAR_SQLx.Add('ELSE oi.Nome END NOMEORIENTADOR,');
  VAR_SQLx.Add('re.Nome NOMERESPONS, ma.*');
  VAR_SQLx.Add('FROM matricul ma');
  VAR_SQLx.Add('LEFT JOIN entidades al ON al.Codigo=ma.Aluno');
  VAR_SQLx.Add('LEFT JOIN entidades oi ON oi.Codigo=ma.Orientador');
  VAR_SQLx.Add('LEFT JOIN entidades re ON re.Codigo=ma.Respons');
  VAR_SQLx.Add('WHERE ma.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND ma.Codigo=:P0');
  //
  VAR_SQLa.Add('AND al.Nome Like :P0');
  //
end;

procedure TFmMatricul.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible    := True;
    PainelDados.Visible    := False;
    PainelControle.Visible := False;
    //
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    if SQLType = stIns then
    begin
      EdEmpresa.Enabled := True;
      CBEmpresa.Enabled := True;
      //
      EdCodigo.ValueVariant  := FormatFloat(FFormatFloat, Codigo);
      EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
      CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
      CkAtivo.Checked        := True;
      EdObserv.ValueVariant  := '';
    end else begin
      EdEmpresa.Enabled := False;
      CBEmpresa.Enabled := False;
      //
      EdCodigo.ValueVariant     := DBEdCodigo.Text;
      EdEmpresa.ValueVariant    := DmodG.ObtemFilialDeEntidade(QrMatriculEmpresa.Value);
      CBEmpresa.KeyValue        := DmodG.ObtemFilialDeEntidade(QrMatriculEmpresa.Value);
      EdContrato.Text           := QrMatriculContrato.Value;
      EdAluno.ValueVariant      := Geral.FF0(QrMatriculAluno.Value);
      CBAluno.KeyValue          := QrMatriculAluno.Value;
      EdOrientador.ValueVariant := Geral.FF0(QrMatriculOrientador.Value);
      CBOrientador.KeyValue     := QrMatriculOrientador.Value;
      EdRespons.ValueVariant    := QrMatriculRespons.Value;
      CBRespons.KeyValue        := QrMatriculRespons.Value;
      TPDataC.Date              := QrMatriculDataC.Value;
      CkAtivo.Checked           := QrMatriculAtivo.Value > 0;
      EdObserv.ValueVariant     := QrMatriculObserv.Value;
    end;
    EdAluno.SetFocus;
  end else begin
    PainelControle.Visible := True;
    PainelDados.Visible    := True;
    PainelEdita.Visible    := False;
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmMatricul.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  FTempoX := Now;
  Va(vpLast);
end;

procedure TFmMatricul.AlteraRegistro;
var
  Matricul : Integer;
begin
  Matricul := QrMatriculCodigo.Value;
  if QrMatriculCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Matricul, Dmod.MyDB, 'Matricul', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Matricul, Dmod.MyDB, 'Matricul', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
  BtRefreshClick(Self);
end;

procedure TFmMatricul.IncluiRegistro;
var
  Cursor : TCursor;
  Matricul : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Matricul := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Matricul', 'Matricul', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Matricul))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, Matricul);
  finally
    Screen.Cursor := Cursor;
  end;
  BtRefreshClick(Self);
end;

procedure TFmMatricul.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMatricul.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMatricul.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMatricul.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMatricul.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMatricul.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMatricul.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdAluno.ValueVariant;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, False, False,
    nil, nil, False, uetCliente1);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdAluno, CBAluno, QrAlunos, VAR_CADASTRO);
    EdAluno.SetFocus;
  end;
end;

procedure TFmMatricul.SpeedButton6Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdOrientador.ValueVariant;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, False, False,
    nil, nil, False, uetCliente2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdOrientador, CBOrientador, QrOrientadores, VAR_CADASTRO);
    EdOrientador.SetFocus;
  end;
end;

procedure TFmMatricul.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(QrMatriculCodigo.Value, fmcadEntidade2, fmcadEntidade2, True);
  //
  if VAR_CADASTRO <> 0 then
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
end;

procedure TFmMatricul.SpeedButton8Click(Sender: TObject);
var
  Codigo, Aba: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if FRespParen then
  begin
    Codigo := EdAluno.ValueVariant;
    Aba    := 1;
  end else
  begin
    Codigo := EdRespons.ValueVariant;
    Aba    := 0;
  end;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, False, False,
    nil, nil, False, uetTerceiro, Aba);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdRespons, CBRespons, QrRespons, VAR_CADASTRO);
    EdRespons.SetFocus;
  end;
end;

procedure TFmMatricul.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
  FmPrincipal.AtualizaAtivosEVencidos;
end;

procedure TFmMatricul.BtHorarioGerenciaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriPer2, FmMatriPer2, afmoNegarComAviso) then
  begin
    FmMatriPer2.EdCodigo.Text    := Geral.FF0(QrMatriAtiCodigo.Value);
    FmMatriPer2.EdControle.Text  := Geral.FF0(QrMatriAtiControle.Value);
    //FmMatriPer2.EdConta.Text     := Geral.FF0(QrMatriAtiConta.Value);
    //FmMatriPer2.EdItem.Text      := Geral.FF0(QrMatriAtiItem.Value);
    //
    FmMatriPer2.PreparaPeriodos(QrMatriTurTurma.Value, QrMatriAtiAtividade.Value);
    //
    FmMatriPer2.ShowModal;
    FmMatriPer2.Destroy;
    ReopenMatriPer;
  end;
end;

procedure TFmMatricul.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
  FmPrincipal.AtualizaAtivosEVencidos;
end;

procedure TFmMatricul.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMatriculCodigo.Value;
  Close;
end;

procedure TFmMatricul.BtConfirmaClick(Sender: TObject);
var
  Codigo, Aluno, Orientador, Respons, Empresa, Ativo, AlunoIdade,
  ResponsIdade: Integer;
  DataC, Contrato, Observ: String;
begin
  Aluno := EdAluno.ValueVariant;
  //
  if MyObjects.FIC(Aluno = 0, EdAluno, 'Defina um Cliente!') then Exit;
  //
  Orientador   := EdOrientador.ValueVariant;
  Respons      := EdRespons.ValueVariant;
  DataC        := Geral.FDT(TPDataC.Date, 1);
  Contrato     := EdContrato.Text;
  Empresa      := EdEmpresa.ValueVariant;
  Ativo        := Geral.BoolToInt(CkAtivo.Checked);
  AlunoIdade   := dmkPF.CalculaIdade(QrAlunosNatal.Value);
  ResponsIdade := dmkPF.CalculaIdade(QrResponsNatal.Value);
  Observ       := EdObserv.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  if MyObjects.FIC((AlunoIdade < 18) and (AlunoIdade <> 0) and (Respons = 0), EdRespons,
    'Para alunos menores de 18 anos voc� dever� informar o respons�vel') then Exit;
  if MyObjects.FIC((ResponsIdade < 18) and (ResponsIdade <> 0) and (Respons <> 0), EdRespons,
    'O respons�vel deve ter 18 anos ou mais!') then Exit;
  //
  (*
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO matricul SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE matricul SET ');
  Dmod.QrUpdU.SQL.Add('Aluno=:P0, DataC=:P1, Contrato=:P2, Ativo=:P3, ');
  Dmod.QrUpdU.SQL.Add('Orientador=:P4, Respons=:P5, ');
  //
  if LaTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsInteger := Aluno;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataC.Date);
  Dmod.QrUpdU.Params[02].AsString  := EdContrato.Text;
  Dmod.QrUpdU.Params[03].AsInteger := MLAGeral.BoolToInt(CkAtivo.Checked);
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdOrientador.Text);
  //
  Dmod.QrUpdU.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[07].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  *)
  Codigo := EdCodigo.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'matricul', False,
  [
  'Aluno', 'Orientador', 'DataC',
  'Contrato', 'Respons', 'Empresa',
  'Observ', 'Ativo'
  ], ['Codigo'],
  [
  Aluno, Orientador, DataC,
  Contrato, Respons, DModG.QrEmpresasCodigo.Value,
  Observ, Ativo
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Matricul', 'Codigo');
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmMatricul.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Matricul', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Matricul', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Matricul', 'Codigo');
end;

procedure TFmMatricul.FormCreate(Sender: TObject);
begin
  BtInclui.Visible      := FmPrincipal.FMatriculaNaoAutomatica;
  PainelEdita.Align     := alClient;
  PainelDados.Align     := alClient;
  PainelEdit.Align      := alClient;
  PainelMovimento.Align := alClient;
  FRespParen            := True;
  //
  FTbLctA := '';
  //
  CriaOForm;
  //
  UMyMod.AbreQuery(QrAlunos, DMod.MyDB);
  UMyMod.AbreQuery(QrOrientadores, DMod.MyDB);
  //
  ReopenRespon;
end;

procedure TFmMatricul.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMatriculCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMatricul.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMatricul.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmMatricul.QrMatriculAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //
  if QrMatricul.State = dsBrowse then
    BtRefazInclui.Enabled := Geral.IntToBool_0(QrMatricul.RecordCount) and (QrMatriculUserAlt.Value <> 0)
  else
    BtRefazInclui.Enabled := False;
end;

procedure TFmMatricul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if not FIniciado then
  begin
    FIniciado := True;
    (*if FCurso1 > 0 then
    begin
       FCurso2 := FCurso1;
       FCurso1 := 0;
       Nova1.Click;
    end;*)
    if FLocMa1 > 0 then LocCod(FLocMa1, FLocMa1);
  end;
end;

procedure TFmMatricul.QrMatriculAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrMatriculCodigo.Value, False);
  // Deve ser antes
  ReopenCliProd(QrMatriculCodigo.Value);
  ReopenMatriRen(FMatriRen);
  //
  if FTempoX > 0 then
  begin
    FTempoX         := Now - FTempoX;
    Label28.Caption := FormatFloat('0.000', FTempoX * 86400);
    FTempoX         := 0;
  end;
end;

procedure TFmMatricul.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMatriculCodigo.Value,
  CuringaLoc.CriaForm('mat.Codigo', 'ent.Nome', 'Matricul', Dmod.MyDB, '', False,
  ' mat LEFT JOIN entidades ent ON ent.Codigo=mat.Aluno'));
end;

procedure TFmMatricul.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmMatricul.QrMatriculBeforeOpen(DataSet: TDataSet);
begin
  QrMatriculCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMatricul.ReopenMatriRen(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMatriRen, Dmod.MyDB, [
    'SELECT mr.Empresa, ec.CodCliInt, ',
    'tar.Nome NOMETARIFA, mr.* ',
    'FROM matriren mr ',
    'LEFT JOIN tarifas tar ON tar.Codigo=mr.Periodo ',
    'LEFT JOIN enticliint ec ON ec.CodEnti=mr.Empresa ',
    'WHERE mr.Codigo=' + Geral.FF0(QrMatriculCodigo.Value),
    'ORDER BY DataI DESC ',
    '']);
  //
  if Controle <> 0 then
    QrMatriRen.Locate('Controle', Controle, []);
end;

procedure TFmMatricul.ReopenCliProd(Cliente: Integer);
begin
  QrCliProd.Close;
  QrCliProd.Params[0].AsInteger := Cliente;
  QrCliProd.Open;
  //
  QrMatriPen.Close;
  QrMatriPen.Params[0].AsInteger := Cliente;
  QrMatriPen.Open;
end;

procedure TFmMatricul.QrMatriculCalcFields(DataSet: TDataSet);
begin
  QrMatriculATIVO_TXT.Value := dmkPF.IntToSimNao(QrMatriculAtivo.Value);
end;

procedure TFmMatricul.DBEdit5Change(Sender: TObject);
begin
  DBEdit5.Font.Color := MLAGeral.CorSimNao(QrMatriculAtivo.Value);
end;

procedure TFmMatricul.IncluiNovoPeriodo;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmMatriRen, FmMatriRen, afmoNegarComAviso) then
  begin
    FmMatriRen.FCodigo   := QrMatriculCodigo.Value;
    FmMatriRen.FControle := QrMatriRenControle.Value;
    FmMatriRen.FEmpresa  := QrMatriculEmpresa.Value;
    //FmMatriRen.FRenoRef  := QrMatriRenRenoRef.Value;
    FmMatriRen.LaTipo.SQLType := stIns;
    //FmMatriRen.CkRenoRef.Visible := False;
    FmMatriRen.CkRenoRef.Checked := False;
    FmMatriRen.Caption           := 'MTR-CADAS-002 :: Cria��o de Matr�cula';
    FmMatriRen.ShowModal;
    FmMatriRen.Destroy;
    LocCod(QrMatriculCodigo.Value, QrMatriculCodigo.Value);
    ReopenMatriRen(0);
    QrMatriRen.First;
  end;
end;

procedure TFmMatricul.RenovaPeriodo;

begin
  if DBCheck.CriaFm(TFmMatriRen, FmMatriRen, afmoNegarComAviso) then
  begin
    FmMatriRen.FCodigo   := QrMatriculCodigo.Value;
    FmMatriRen.FControle := QrMatriRenControle.Value;
    FmMatriRen.FEmpresa  := QrMatriculEmpresa.Value;
    //FmMatriRen.FRenoRef  := QrMatriRenRenoRef.Value;
    FmMatriRen.LaTipo.SQLType := stIns;
    //FmMatriRen.CkRenoRef.Visible := True;
    FmMatriRen.CkRenoRef.Checked := True;
    //FmMatriRen.CkRenoRef.Enabled := False;
    FmMatriRen.Caption := 'MTR-CADAS-002 :: Renova��o de Matr�cula';
    if QrMatriRen.RecordCount > 0 then
    begin
      FmMatriRen.TPIni.Date := QrMatriRenDataF.Value+1;
      FmMatriRen.TPFim.Date := QrMatriRenDataF.Value+1;
    end else begin
      FmMatriRen.TPIni.Date := Date;
      FmMatriRen.TPFim.Date := Date;
    end;
    FmMatriRen.ShowModal;
    FmMatriRen.Destroy;
    ReopenMatriRen(0);
  end;
end;

procedure TFmMatricul.QrMatriRenCalcFields(DataSet: TDataSet);
var
  Preco: Double;
begin
  QrMatriRenSEQ.Value := QrMatriRen.RecordCount - QrMatriRen.RecNo+1;
  //
  QrMatriRenPRECO_T.Value := QrMatriRenPreco.Value + QrMatriRenTaxa.Value;
  QrMatriRenDESCO_V.Value := QrMatriRenPRECO_T.Value - QrMatriRenACobrar.Value;
  //
  Preco := QrMatriRenPreco.Value + QrMatriRenTaxa.Value;
  if Preco = 0 then QrMatriRenP_DESCO.Value := 0 else QrMatriRenP_DESCO.Value :=
  ((Preco - QrMatriRenACobrar.Value) / Preco) * 100;
  //
  QrMatriRenPENDENTE_TOTAL.Value :=
    QrMatriPenPENDENTE.Value + QrCliProdQuantN2.Value;
  //
  if QrMatriRenAtivo.Value = 1 then
    QrMatriRenNO_ATIVO.Value := 'S'
  else
    QrMatriRenNO_ATIVO.Value := 'N';
end;

procedure TFmMatricul.DefineVarDup;
begin
  // (N�o)Mudei 2010-06-06
  IC3_ED_FatNum := QrMatriRenControle.Value;
  //IC3_ED_FatNum := QrMatriRenCodigo.Value;
  // fim 2010-06-06
  IC3_ED_NF := 0;
  IC3_ED_Data := Date;
end;

procedure TFmMatricul.ExcluiItemDuplicata;
var
  FatNum: Double;
  FatID, FatParcela: Integer;
begin
  if not ValidaTabelaFinanceiro() then Exit;
  //
  if QrPagtos.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� pagamento cadastrado!');
    Exit;
  end;
  FatParcela := QrPagtosFatParcela.Value;
  FatNum     := QrPagtosFatNum.Value;
  FatID      := QrPagtosFatID.Value;
  //
  if (FatParcela = 0) and (FatNum=0) and (FatID=0) then Exit;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o deste pagamento?') = ID_YES then
  begin
    Dmod.QrUpdU.Close;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM ' + FTbLctA + ' WHERE FatID=:P0');
    Dmod.QrUpdU.SQL.Add('AND FatNum=:P1 AND FatParcela=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := FatID;
    Dmod.QrUpdU.Params[1].AsFloat   := FatNum;
    Dmod.QrUpdU.Params[2].AsInteger := FatParcela;
    Dmod.QrUpdU.ExecSQL;

    Dmod.QrUpdU.Close;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE ' + FTbLctA + ' SET FatParcela=FatParcela-1');
    Dmod.QrUpdU.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := FatParcela;
    Dmod.QrUpdU.Params[1].AsInteger := FatID;
    Dmod.QrUpdU.Params[2].AsFloat   := FatNum;
    Dmod.QrUpdU.ExecSQL;

    Dmod.RecalcSaldoCarteira(QrPagtosTipo.Value, QrPagtosCarteira.Value, 0,
      FTbLctA);

    Dmod.AtualizaPagamentoMatricula(QrMatriRenControle.Value, FTbLctA);

    LocCod(QrMatriculCodigo.Value, QrMatriculCodigo.Value);
  end;
end;

procedure TFmMatricul.ReopenPagtos();
begin
  if not ValidaTabelaFinanceiro() then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
    'SELECT la.*, co.Nome NOMECONTA, ',
    'ca.Nome NOMECARTEIRA, ca.Tipo CARTEIRATIPO ',
    'FROM ' + FTbLctA + ' la ',
    'LEFT JOIN carteiras ca ON ca.Codigo = la.Carteira ',
    'LEFT JOIN contas co ON co.Codigo = la.Genero ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_2101),
    'AND FatNum=' + Geral.FF0(QrMatriRenControle.Value),
    'ORDER BY la.Vencimento, la.FatParcela ',
    '']);
end;

procedure TFmMatricul.ReopenRespon;
var
  Aluno: Integer;
begin
  if FRespParen then
  begin
    Aluno := EdAluno.ValueVariant;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrRespons, Dmod.MyDB, [
      'SELECT ent.Codigo, ent.Nome, ',
      'CASE WHEN ent.Tipo=0 THEN ent.ENatal ELSE ent.PNatal END Natal ',
      'FROM enticontat con ',
      'LEFT JOIN entidades ent ON ent.Codigo = con.Entidade ',
      'WHERE con.Codigo=' + Geral.FF0(Aluno),
      'ORDER BY Nome ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRespons, Dmod.MyDB, [
      'SELECT Codigo, Nome, ',
      'CASE WHEN Tipo=0 THEN ENatal ELSE PNatal END Natal ',
      'FROM entidades ',
      'WHERE Tipo=1 ',
      'ORDER BY Nome ',
      '']);
  end;
end;

procedure TFmMatricul.ReopenMatriTur;
begin
  QrMatriTur.Close;
  QrMatriTur.Params[0].AsInteger := QrMatriRenCodigo.Value;
  QrMatriTur.Params[1].AsInteger := QrMatriRenControle.Value;
  QrMatriTur.Open;
end;

procedure TFmMatricul.ReopenMatriAti(Item: Integer);
begin
  QrMatriAti.Close;
  QrMatriAti.Params[0].AsInteger := QrMatriTurCodigo.Value;
  QrMatriAti.Params[1].AsInteger := QrMatriTurControle.Value;
  QrMatriAti.Params[2].AsInteger := QrMatriTurConta.Value;
  QrMatriAti.Open;
  //
  if Item <> 0 then
    QrMatriAti.Locate('Item', Item, []);
end;

procedure TFmMatricul.ReopenMatriPer();
begin
  QrMatriPer.Close;
  QrMatriPer.Params[0].AsInteger := QrMatriAtiCodigo.Value;
  QrMatriPer.Params[1].AsInteger := QrMatriAtiControle.Value;
  QrMatriPer.Params[2].AsInteger := QrMatriAtiConta.Value;
  QrMatriPer.Params[3].AsInteger := QrMatriAtiItem.Value;
  QrMatriPer.Open;
end;

procedure TFmMatricul.Atual1Click(Sender: TObject);
begin
  ExcluiItemDuplicata;
end;

procedure TFmMatricul.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
  FmPrincipal.AtualizaAtivosEVencidos;
end;

procedure TFmMatricul.Todos1Click(Sender: TObject);
begin
  ExcluiTodasParcelas;
end;

procedure TFmMatricul.ExcluiTodasParcelas;
var
  FatNum: Double;
  FatID: Integer;
begin
  if not ValidaTabelaFinanceiro() then Exit;
  //
  if QrPagtos.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� pagamento cadastrado');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o de todas parcelas de pagamento?') = ID_YES then
  begin
    FatNum := QrPagtosFatNum.Value;
    FatID := QrPagtosFatID.Value;
    Dmod.QrUpdU.Close;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM ' + FTbLctA + ' WHERE FatID=:P0');
    Dmod.QrUpdU.SQL.Add('AND FatNum=:P1');
    Dmod.QrUpdU.Params[0].AsInteger := FatID;
    Dmod.QrUpdU.Params[1].AsFloat   := FatNum;
    Dmod.QrUpdU.ExecSQL;
    //
    Dmod.RecalcSaldoCarteira(QrPagtosTipo.Value, QrPagtosCarteira.Value, 0, FTbLctA);
    Dmod.AtualizaPagamentoMatricula(QrMatriRenControle.Value, FTbLctA);
    //
    LocCod(QrMatriculCodigo.Value, QrMatriculCodigo.Value);
  end;
end;

procedure TFmMatricul.QrPagtosCalcFields(DataSet: TDataSet);
begin
  if QrPagtosSit.Value = -1 then
     QrPagtosNOMESIT.Value := CO_IMPORTACAO
  else
    if QrPagtosSit.Value = 1 then
     QrPagtosNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrPagtosSit.Value = 2 then
       QrPagtosNOMESIT.Value := CO_QUITADA
  else
    if QrPagtosSit.Value = 3 then
       QrPagtosNOMESIT.Value := CO_COMPENSADA
  else
    if QrPagtosVencimento.Value < Date then
       QrPagtosNOMESIT.Value := CO_VENCIDA
  else
       QrPagtosNOMESIT.Value := CO_EMABERTO;
  case QrPagtosSit.Value of
    0: QrPagtosSALDO.Value := QrPagtosCredito.Value - QrPagtosDebito.Value;
    1: QrPagtosSALDO.Value := (QrPagtosCredito.Value - QrPagtosDebito.Value) + QrPagtosPago.Value;
    else QrPagtosSALDO.Value := 0;
  end;

  case QrPagtosTipo.Value of
    0: QrPagtosNOMETIPO.Value := CO_CAIXA;
    1: QrPagtosNOMETIPO.Value := CO_BANCO;
    2: QrPagtosNOMETIPO.Value := CO_EMISS;
  end;

  //

  if (QrPagtosVencimento.Value < Date) and (QrPagtosSit.Value<2) then
    QrPagtosVENCIDO.Value := QrPagtosVencimento.Value;
//  if QrPagtosVencimento.Value < Date then
//    QrPagtosNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagtosVencimento.Value)
//  else QrPagtosNOMEVENCIDO.Value := CO_VAZIO;
  if QrPagtosVENCIDO.Value > 0 then
     QrPagtosATRAZODD.Value := Trunc(Date) - QrPagtosVENCIDO.Value
  else QrPagtosATRAZODD.Value := 0;
  if QrPagtosATRAZODD.Value > 0 then QrPagtosATUALIZADO.Value :=
  (QrPagtosATRAZODD.Value * QrPagtosMoraDia.Value) +
  QrPagtosMulta.Value else QrPagtosATUALIZADO.Value := 0;
  if QrPagtosSit.Value<2 then QrPagtosATUALIZADO.Value :=
  QrPagtosATUALIZADO.Value + QrPagtosSALDO.Value;
  //
  QrPagtosCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrPagtosCNPJCPF.Value);
end;

procedure TFmMatricul.QrMatriRenAfterScroll(DataSet: TDataSet);
var
  EntCliInt, CodCliInt: Integer;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  EntCliInt := QrMatriRenEmpresa.Value;
  CodCliInt := QrMatriRenCodCliInt.Value;
  //
  if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
  //
  {$IFDEF DEFINE_VARLCT}
  DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, FTbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //
  ReopenPagtos();
  ReopenMatriTur();
  //
  if QrMatriRen.RecNo = 1 then
  begin
    BtRefaz.Enabled := True;
    BtRefazAltera.Enabled := True;
  end else begin
    BtRefaz.Enabled := False;
    BtRefazAltera.Enabled := False;
  end;
end;

procedure TFmMatricul.QrMatriRenBeforeClose(DataSet: TDataSet);
begin
  FTbLctA := '';
  //
  BtRefresh.Enabled := False;
end;

procedure TFmMatricul.AlteraRenovacao;
begin
  if DBCheck.CriaFm(TFmMatriRen, FmMatriRen, afmoNegarComAviso) then
  begin
    FmMatriRen.FCodigo    := QrMatriculCodigo.Value;
    FmMatriRen.FControle  := QrMatriRenControle.Value;
    FmMatriRen.FEmpresa   := QrMatriRenEmpresa.Value;
    // at� 2011-07-16
    //FmMatriRen.FRenoRef   := QrMatriRenRenoRef.Value;
    //N�o salva o plano na tabela ent�o n�o tem como redenir
    //FmMatriRen.EdTarifa.ValueVariant := QrMatricul
    // de 2011-07-16 em diante.
    //
    FmMatriRen.EdTarifa.ValueVariant := QrMatriRenTarBase.Value;
    FmMatriRen.CBTarifa.KeyValue := QrMatriRenTarBase.Value;
    FmMatriRen.CkDesativar.Checked := Geral.IntToBool(QrMatriRenDesativar.Value);
    // fim "de 2011-07-16 em diante."
    //
    FmMatriRen.TPIni.Date := QrMatriRenDataI.Value;
    FmMatriRen.TPFim.Date := QrMatriRenDataF.Value;
    FmMatriRen.LaTipo.SQLType := stUpd;
    FmMatriRen.CkRenoRef.Visible := False;
    //FmMatriRen.EdTarifa.Text := N�o � poss�vel;
    FmMatriRen.EdACobrar.Text := Geral.FFT(QrMatriRenACobrar.Value, 2, siPositivo);
    FmMatriRen.Caption := 'MTR-CADAS-002 :: Cria��o de Matr�cula';
    FmMatriRen.ShowModal;
    FmMatriRen.Destroy;
    LocCod(QrMatriculCodigo.Value, QrMatriculCodigo.Value);
    Dmod.AtualizaPagamentoMatricula(QrMatriRenControle.Value, FTbLctA);
    ReopenMatriRen(QrMatriRenControle.Value);
  end;
end;

procedure TFmMatricul.IncluiTurma;
begin
  if DBCheck.CriaFm(TFmMatriTur, FmMatriTur, afmoNegarComAviso) then
  begin
    FmMatriTur.EdCodigo.Text   := Geral.FF0(QrMatriRenCodigo.Value);
    FmMatriTur.EdControle.Text := Geral.FF0(QrMatriRenControle.Value);
    FmMatriTur.LaTipo.SQLType  := stIns;
    FmMatriTur.ShowModal;
    FmMatriTur.Destroy;
    ReopenMatriTur;
  end;  
end;

procedure TFmMatricul.QrMatriTurAfterScroll(DataSet: TDataSet);
begin
  ReopenMatriAti(0);
end;

procedure TFmMatricul.BtRefazIncluiClick(Sender: TObject);
begin
  IncluiNovoPeriodo;
end;

procedure TFmMatricul.BtRefazAlteraClick(Sender: TObject);
begin
  AlteraRenovacao;
end;

procedure TFmMatricul.BtRefazExcluiClick(Sender: TObject);
begin
  ExcluiRenovacao;
  BtRefreshClick(Self);
end;

procedure TFmMatricul.ExcluiRenovacao;
begin
  if (QrMatriRenSEQ.Value <> QrMatriRen.RecordCount) then
  begin
    Geral.MB_Aviso('Este per�odo n�o pode ser exclu�do pois j� existe outro posterior!');
    Exit;
  end;
  if (QrPagtos.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Este per�odo n�o pode ser exclu�do pois possui pagamentos relacionados!');
    Exit;
  end;
  if (QrMatriTur.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Este per�odo n�o pode ser exclu�do pois possui turmas relacionada!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o do pe�odo?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM matriren WHERE Codigo=:P0 AND Controle=:P1');
      Dmod.QrUpd.Params[00].AsInteger := QrMatriRenCodigo.Value;
      Dmod.QrUpd.Params[01].AsInteger := QrMatriRenControle.Value;
      Dmod.QrUpd.ExecSQL;
      LocCod(QrMatriculCodigo.Value, QrMatriculCodigo.Value);
      ReopenMatriRen(0);
    end;
end;

procedure TFmMatricul.BtRefazClick(Sender: TObject);
begin
  RenovaPeriodo;
end;

procedure TFmMatricul.BtTurmasIncluiClick(Sender: TObject);
begin
  IncluiTurma;
end;

procedure TFmMatricul.BtTurmasExcluiClick(Sender: TObject);
begin
  ExcluiTurma;
end;

procedure TFmMatricul.ExcluiTurma;
begin
  if (QrMatriAti.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Esta turma n�o pode ser exclu�da pois possui atividade relacionada!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a retirada da turma?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM matritur WHERE Codigo=:P0');
      Dmod.QrUpd.SQL.Add('AND Controle=:P1 AND Conta=:P2');
      Dmod.QrUpd.Params[00].AsInteger := QrMatriTurCodigo.Value;
      Dmod.QrUpd.Params[01].AsInteger := QrMatriTurControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrMatriTurConta.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenMatriTur;
    end;
end;

procedure TFmMatricul.BtAtividadesIncluiClick(Sender: TObject);
var
  Item: Integer;
begin
  if DBCheck.CriaFm(TFmMatriAti, FmMatriAti, afmoNegarComAviso) then
  begin
    Item := QrMatriTurCodigo.Value;
    FmMatriAti.EdCodigo.Text    := Geral.FF0(QrMatriTurCodigo.Value);
    FmMatriAti.EdControle.Text  := Geral.FF0(QrMatriTurControle.Value);
    FmMatriAti.EdConta.Text     := Geral.FF0(QrMatriTurConta.Value);
    FmMatriAti.QrAtividades.Close;
    FmMatriAti.QrAtividades.Params[0].AsInteger := QrMatriTurTurma.Value;
    FmMatriAti.QrAtividades.Open;
    FmMatriAti.ShowModal;
    if FmMatriAti.FConta <> 0 then
      Item := FmMatriAti.FConta;
    FmMatriAti.Destroy;
    ReopenMatriAti(Item);
  end;
end;

procedure TFmMatricul.BtAtividadesExcluiClick(Sender: TObject);
begin
  ExcluiAtividade;
end;

procedure TFmMatricul.EdAlunoChange(Sender: TObject);
begin
  if EdAluno.ValueVariant <> 0 then
    ReopenRespon;
end;

procedure TFmMatricul.ExcluiAtividade;
begin
  if (QrMatriPer.RecordCount > 0) then
  begin
    Geral.MB_Aviso('Esta atividade n�o pode ser exclu�da pois possui periodo relacionado!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a retirada da atividade?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM matriati WHERE Codigo=:P0');
      Dmod.QrUpd.SQL.Add('AND Controle=:P1 AND Conta=:P2 AND Item=:P3');
      Dmod.QrUpd.Params[00].AsInteger := QrMatriAtiCodigo.Value;
      Dmod.QrUpd.Params[01].AsInteger := QrMatriAtiControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrMatriAtiConta.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrMatriAtiItem.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenMatriAti(0);
    end;
end;

procedure TFmMatricul.QrMatriAtiAfterScroll(DataSet: TDataSet);
begin
  ReopenMatriPer;
end;

procedure TFmMatricul.QrMatriAtiBeforeClose(DataSet: TDataSet);
begin
  QrMatriPer.Close;
  BtRelogioExclui.Enabled := False;
  BtRelogioInclui.Enabled := False;
  BtHorarioGerencia.Enabled := False;
end;

procedure TFmMatricul.QrMatriAtiAfterOpen(DataSet: TDataSet);
begin
  BtAtividadesExclui.Enabled := Geral.IntToBool_0(QrMatriAti.RecordCount);
  BtRelogioInclui.Enabled := Geral.IntToBool_0(QrMatriAti.RecordCount);
  BtHorarioGerencia.Enabled := QrMatriAti.RecordCount > 0;
end;

procedure TFmMatricul.QrMatriTurAfterOpen(DataSet: TDataSet);
begin
  BtTurmasExclui.Enabled := Geral.IntToBool_0(QrMatriTur.RecordCount);
  BtAtividadesInclui.Enabled := Geral.IntToBool_0(QrMatriTur.RecordCount);
end;

procedure TFmMatricul.QrMatriRenAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrMatriRen.RecordCount > 0;
  BtRefaz.Enabled        := Habilita;
  BtRefazAltera.Enabled  := Habilita;
  BtRefazExclui.Enabled  := Habilita;
  BtTurmasInclui.Enabled := Habilita;
  BtPagtosInclui.Enabled := Habilita;
end;

procedure TFmMatricul.QrMatriPerAfterOpen(DataSet: TDataSet);
begin
  BtRelogioExclui.Enabled := Geral.IntToBool_0(QrMatriPer.RecordCount);
end;

procedure TFmMatricul.BtRelogioIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriPer, FmMatriPer, afmoNegarComAviso) then
  begin
    FmMatriPer.EdCodigo.Text    := Geral.FF0(QrMatriAtiCodigo.Value);
    FmMatriPer.EdControle.Text  := Geral.FF0(QrMatriAtiControle.Value);
    FmMatriPer.EdConta.Text     := Geral.FF0(QrMatriAtiConta.Value);
    FmMatriPer.EdItem.Text      := Geral.FF0(QrMatriAtiItem.Value);
    FmMatriPer.QrPeriodos.Close;
    FmMatriPer.QrPeriodos.Params[00].AsInteger := QrMatriTurTurma.Value;
    FmMatriPer.QrPeriodos.Params[01].AsInteger := QrMatriAtiAtividade.Value;
    FmMatriPer.QrPeriodos.Open;
    FmMatriPer.ShowModal;
    FmMatriPer.Destroy;
    ReopenMatriPer;
  end;
end;

procedure TFmMatricul.BtRelogioExcluiClick(Sender: TObject);
begin
  ExcluiPeriodo;
end;

procedure TFmMatricul.ExcluiPeriodo;
begin
  if Geral.MB_Pergunta('Confirma a retirada do Per�odo?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM matriper WHERE Codigo=:P0');
      Dmod.QrUpd.SQL.Add('AND Controle=:P1 AND Conta=:P2 AND Item=:P3');
      Dmod.QrUpd.SQL.Add('AND SubItem=:P4');
      Dmod.QrUpd.Params[00].AsInteger := QrMatriPerCodigo.Value;
      Dmod.QrUpd.Params[01].AsInteger := QrMatriPerControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrMatriPerConta.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrMatriPerItem.Value;
      Dmod.QrUpd.Params[04].AsInteger := QrMatriPerSubItem.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenMatriPer;
    end;
end;

procedure TFmMatricul.QrMatriTurAfterClose(DataSet: TDataSet);
begin
  QrMatriAti.Close;
  BtAtividadesExclui.Enabled := False;
  BtAtividadesInclui.Enabled := False;
end;

procedure TFmMatricul.BtPagtosIncluiClick(Sender: TObject);
var
  Terceiro, Cod: Integer;
begin
  if not ValidaTabelaFinanceiro() then Exit;
  //
  DefineVarDup;
  Cod      := QrMatriRenControle.Value;
  Terceiro := QrMatriculAluno.Value;
  //
  UPagtos.Pagto(QrPagtos, tpCred, Cod, Terceiro, VAR_FATID_2101, 0, stIns,
    'Pagamento de Matr�cula', QrMatriRenValor.Value, VAR_USUARIO, 0,
    FmPrincipal.FEntInt, mmNenhum, 0, 0, True, False, 0, 0, 0, 0, 0, FTbLctA);
  //
  Dmod.AtualizaPagamentoMatricula(QrMatriRenControle.Value, FTbLctA);
  LocCod(QrMatriculCodigo.Value, QrMatriculCodigo.Value);
end;

procedure TFmMatricul.Pagamento1Click(Sender: TObject);
begin
  ExcluiItemDuplicata;
end;

procedure TFmMatricul.Todospagamentosdestarenovao1Click(Sender: TObject);
begin
  ExcluiTodasParcelas;
end;

procedure TFmMatricul.QrMatriRenAfterClose(DataSet: TDataSet);
begin
  QrMatriTur.Close;
  QrPagtos.Close;
  BtPagtosInclui.Enabled := False;
  BtPagtosExclui.Enabled := False;
  BtTurmasExclui.Enabled := False;
  BtTurmasInclui.Enabled := False;
  BtRefaz.Enabled        := False;
  BtRefazAltera.Enabled  := False;
end;

procedure TFmMatricul.QrMatriculAfterClose(DataSet: TDataSet);
begin
  QrMatriRen.Close;
  //
  BtRefazExclui.Enabled := False;
  BtRefazInclui.Enabled := False;
end;

procedure TFmMatricul.QrPagtosAfterOpen(DataSet: TDataSet);
begin
  BtPagtosExclui.Enabled := Geral.IntToBool_0(QrPagtos.RecordCount);
end;

procedure TFmMatricul.BtRefreshClick(Sender: TObject);
begin
  if QrMatriRen.State <> dsInactive then
  begin
    QrMatriRen.First;
    AtualizaAtivo(QrMatriRenCodigo.Value, QrMatriRenControle.Value,
      QrMatriRenDesativar.Value);
    FmPrincipal.AtualizaAtivosEVencidos;
  end;
end;

procedure TFmMatricul.AtualizaAtivo(Codigo, Controle, Desativar: Integer);
var
  Ativo: String;
begin
  if Desativar = 1 then
    Ativo := '0'
  else
    Ativo := '1';
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE matriren SET Ativo=0 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle<>:P1');
  Dmod.QrUpd.Params[0].AsInteger := Codigo;
  Dmod.QrUpd.Params[1].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE matriren SET Ativo=' + Ativo);
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Codigo;
  Dmod.QrUpd.Params[1].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmMatricul.BtPagtosExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtPagtosExclui);
end;

procedure TFmMatricul.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmMatricul.CBResponsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    ConfiguraRespons(False)
  else if Key = VK_F4 then
    ConfiguraRespons(True);
end;

procedure TFmMatricul.ConfiguraRespons(Todos: Boolean);
begin
  FRespParen := not Todos;
  //
  ReopenRespon;
end;

procedure TFmMatricul.Contrato1Click(Sender: TObject);
  function PeriodoMatriRen(): String;
  begin
    Result := Geral.FDT(QrMatriRenDataI.Value, 2) + ' � ' +
              Geral.FDT(QrMatriRenDataF.Value, 2);
  end;
begin
  if DBCheck.CriaFm(TFmMatriculContrato, FmMatriculContrato, afmoNegarComAviso) then
  begin
    if QrMatriculRespons.Value <> 0 then
      FmMatriculContrato.FRespo  := QrMatriculRespons.Value
    else
      FmMatriculContrato.FRespo  := QrMatriculAluno.Value;
    FmMatriculContrato.FAluno    := QrMatriculAluno.Value;
    FmMatriculContrato.FHorari   := CursosEHorarios;
    FmMatriculContrato.FInvest   := Geral.FFT(QrMatriRenValor.Value, 2, siPositivo);
    FmMatriculContrato.FParcel   := ParcelasToTxt();
    FmMatriculContrato.FDataCo   := FormatDateTime(VAR_FORMATDATE6, QrMatriculDataC.Value);
    FmMatriculContrato.FContra   := Geral.FF0(QrMatriculCodigo.Value);
    FmMatriculContrato.FPlural   := FPlural;
    FmMatriculContrato.FPeriod   := PeriodoMatriRen();
    if QrMatriculContrato.Value <> '' then FmMatriculContrato.FContra :=
    FmMatriculContrato.FContra   + '.' + QrMatriculContrato.Value;

    FmMatriculContrato.ShowModal;
    FmMatriculContrato.Destroy;
  end;
end;

function TFmMatricul.ParcelasToTxt(): String;
var
  Texto: String;
begin
  if not ValidaTabelaFinanceiro() then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos2, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA, ',
    'ca.Nome2 NOMECART2, ',
    'ca.Tipo CARTEIRATIPO ',
    'FROM ' + FTbLctA + ' la, Carteiras ca ',
    'WHERE ca.Codigo=la.Carteira ',
    'AND FatID=' + Geral.FF0(VAR_FATID_2101),
    'AND FatNum=' + Geral.FF0(QrMatriRenControle.Value),
    'ORDER BY la.Vencimento, la.FatParcela ',
    '']);
  //
  Texto := 'Forma de Pagamento (Carteira)                      - Vencimento - Valor da parcela'+Chr(13)+Chr(10);
  while not QrPagtos2.Eof do
  begin
    Texto := Texto +
    Geral.CompletaString(QrPagtos2NOMECART2.Value, ' ', 50, taLeftJustify, False) +
    ' - ' + FormatDateTime(VAR_FORMATDATE2, QrPagtos2Vencimento.Value)+
    ' - '+Geral.CompletaString(Geral.TFT(FloatToStr(QrPagtos2Credito.Value),
    2, siPositivo), ' ', 16, taRightJustify, False)+ Chr(13)+Chr(10);
    //
    QrPagtos2.Next;
  end;
  Result := Texto;
end;

function TFmMatricul.CursosEHorarios: String;
var
  Texto: String;
  Conta: Integer;
begin
  Conta := 0;
  if QrMatriTur.State <> dsInactive then
  begin
    QrMatriTur.First;
    while not QrMatriTur.Eof do
    begin
      QrMatriAti.First;
      while not QrMatriAti.Eof do
      begin
        Conta := Conta +1;
        QrMatriPer.First;
        while not QrMatriPer.Eof do
        begin
          Texto := Texto + Geral.CompletaString(QrMatriTurNOMETURMA.Value,
          ' ', 20, taLeftJustify, False)+Geral.CompletaString(
          QrMatriAtiNOMECURSO.Value, ' ', 20, taLeftJustify, False)+
          Geral.CompletaString(QrMatriPerNOMEDSEM.Value, ' ', 15,
          taLeftJustify, False)+FormatDateTime('hh:nn', QrMatriPerHIni.Value)+
          ' as '+FormatDateTime('hh:nn', QrMatriPerHFim.Value)+Chr(13)+Chr(10);
          QrMatriPer.Next;
        end;
        QrMatriAti.Next;
      end;
      QrMatriTur.Next;
    end;
    if Texto <> '' then Texto := 'TURMA               '+'CURSO               '+
    'Dia da semana  '+'Hor�rio'+Chr(13)+Chr(10)+Texto;
    if Conta > 1 then FPlural := 's' else FPlural := '';
    Result := Texto;
  end else Result := 'CURSOS E HOR�RIOS N�O DEFINIDOS';
end;

procedure TFmMatricul.Recibo1Click(Sender: TObject);
begin
  TbImpReciC.Close;
  TbImpReciR.Close;
  TbImpReciC.Open;
  TbImpReciR.Open;
  if FmPrincipal.InicializaBEMA_NaoFiscal = 1 then
  begin
    ImprimeCupom(0);
    FechaPorta();
  end;
end;

function TFmMatricul.ImprimeCupom(Saida: Integer): Boolean;
var
  //Comando: Integer;
  Text1, Text2: String;
const
  L = Chr(13) + Chr(10);
begin
  //Result := False;
  try
  while not TbImpReciC.Eof do
  begin
    (*comando :=*) ImprimeLinhaCupom(Saida, TbImpReciCTexto.Value+L ,
                         TbImpReciCModo.Value,
                         TbImpReciCItal.Value,
                         TbImpReciCSubl.Value,
                         TbImpReciCExpa.Value,
                         TbImpReciCNegr.Value);
    TbImpReciC.Next;
  end;
////////////////////////////////////////////////////////////////////////////////
  (*comando :=*) ImprimeLinhaCupom(Saida, Copy('CLIENTE: '+
  Geral.FF0(QrMatriculAluno.Value)+' - '+QrMatriculNOMEALUNO.Value, 1, 60)+
  L, 1, 0, 0, 0, 0);
  (*comando :=*) ImprimeLinhaCupom(Saida, 'PER�ODO DO PLANO: '+
    FormatDateTime(VAR_FORMATDATE2, QrMatriRenDataI.Value)+' AT� '+
    FormatDateTime(VAR_FORMATDATE2, QrMatriRenDataF.Value)+L, 1, 0,0,0,0);
  if QrMatriRenTaxa.Value > 0 then
  begin
    Text1 := 'TAXA DE MATR�CULA';
    Text2 := Geral.FFT(QrMatriRenTaxa.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    (*comando :=*) ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;
  if QrMatriRenPreco.Value > 0 then
  begin
    Text1 := QrMatriRenNOMETARIFA.Value;
    Text2 := Geral.FFT(QrMatriRenPreco.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    (*comando :=*) ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;
  if QrMatriRenPRECO_T.Value > 0 then
  begin
    ImprimeLinhaCupom(Saida, Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
    Text1 := 'TOTAL';
    Text2 := Geral.FFT(QrMatriRenPRECO_T.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    (*comando :=*) ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;
  if QrMatriRenDESCO_V.Value > 0 then
  begin
    //ImprimeLinhaCupom(Saida, Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
    Text1 := 'DESCONTO';
    Text2 := Geral.FFT(QrMatriRenDESCO_V.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    (*comando :=*) ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;
  //if QrMatriRenValor.Value > 0 then
  //begin
    //ImprimeLinhaCupom(Saida, Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
    Text1 := 'VALOR PAGO';
    Text2 := Geral.FFT(QrMatriRenValor.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    (*comando :=*) ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
    if QrMatriRenPENDENTE_TOTAL.Value < 0 then
    begin
      //ImprimeLinhaCupom(Saida, Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
      Text1 := 'PENDENCIAS';
      Text2 := Geral.FFT(-QrMatriRenPENDENTE_TOTAL.Value, 2, siPositivo);
      Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
               Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
      (*comando :=*) ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
    end;
    ImprimeLinhaCupom(Saida, Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
    Text1 := 'EMISS�O: '+FormatDateTime(VAR_FORMATDATE6, Date);
    Text2 := 'HORA: '+FormatDateTime(VAR_FORMATTIME, Now);
    Text1 := Geral.CompletaString(Text1, ' ', 45, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 15, taRightJustify, False)+L;
    (*comando :=*) ImprimeLinhaCupom(Saida, Uppercase(Text1), 1, 0,0,0,0);
  //end;
////////////////////////////////////////////////////////////////////////////////
  while not TbImpReciR.Eof do
  begin
    (*comando :=*) ImprimeLinhaCupom(Saida, TbImpReciRTexto.Value+L ,
                         TbImpReciRModo.Value,
                         TbImpReciRItal.Value,
                         TbImpReciRSubl.Value,
                         TbImpReciRExpa.Value,
                         TbImpReciRNegr.Value);
    TbImpReciR.Next;
  end;
  Result := True;
  except
    raise;
    Result := False;
  end;
  (*if comando = 0 then
  begin
    MessageDlg('Problemas na impress�o do texto formatado.' + #10 +
      'Poss�veis causas: Impressora desligada, off-line ou sem papel',
      mtError, [mbOk], 0 );
    Result := False;
  end;*)
end;

procedure TFmMatricul.DBEdit8Change(Sender: TObject);
begin
  if QrMatriPenPENDENTE.Value < 0 then
    DBEdit8.Font.Color := clRed else DBEdit8.Font.Color := clBlue;
end;

procedure TFmMatricul.DBEdit9Change(Sender: TObject);
begin
  if QrCliProdQuantN2.Value < 0 then
    DBEdit9.Font.Color := clRed else DBEdit9.Font.Color := clBlue;
end;

procedure TFmMatricul.DBEdit10Change(Sender: TObject);
begin
  if QrMatriRenPENDENTE_TOTAL.Value < 0 then
    DBEdit10.Font.Color := clRed else DBEdit10.Font.Color := clBlue;
end;

function TFmMatricul.ImprimeLinhaCupom(Saida: Integer; Texto: String;
TpoLtra, Italic, Sublin, Expand, Enfat: Integer): Integer;
begin
  Result := 0;
  case Saida of
    0: Result := FormataTX(Texto, TpoLtra, Italic, Sublin, Expand, Enfat);
    1:
    begin
      try
        FmDotPrint.RE1.Lines.Add(Texto);
        Result := 1;
      except
        Result := 2;
      end;
    end;
  end;
end;

procedure TFmMatricul.ReciboMatricial1Click(Sender: TObject);
begin
  TbImpReciC.Close;
  TbImpReciC.Open;
  TbImpReciR.Close;
  TbImpReciR.Open;
  //
  if DBCheck.CriaFm(TFmDotPrint, FmDotPrint, afmoNegarComAviso) then
  begin
    FmDotPrint.FCPI := 10;
    FmDotPrint.RE1.Lines.Clear;
    ImprimeCupom(1);
    FmDotPrint.ShowModal;
    FmDotPrint.Destroy;
  end;
end;

procedure TFmMatricul.QrMatriculBeforeClose(DataSet: TDataSet);
begin
  FTempoX := Now;
  //
  Label28.Caption := '0.000';
end;
// parei aqui
//terminar menor

end.

