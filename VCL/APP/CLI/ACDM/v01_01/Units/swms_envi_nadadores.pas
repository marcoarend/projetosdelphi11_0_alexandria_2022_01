unit swms_envi_nadadores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkMemoBar, UnDmkEnums;

type
  TFmSWMS_Envi_Nadadores = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label29: TLabel;
    Label48: TLabel;
    Label112: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdCodTxt: TdmkEdit;
    TPNascim: TdmkEditDateTimePicker;
    EdCPF: TdmkEdit;
    EdFone: TdmkEdit;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdSexo: TdmkEdit;
    Panel3: TPanel;
    CkContinuar: TCheckBox;
    EdImportado: TdmkEdit;
    EdNivel1: TdmkEdit;
    EdNivel2: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    //function Nadadores_Gera(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmSWMS_Envi_Nadadores: TFmSWMS_Envi_Nadadores;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, SWMS_Mtng_Cab;

{$R *.DFM}

procedure TFmSWMS_Envi_Nadadores.BtOKClick(Sender: TObject);
var
  Nome, CodTxt, CPF, Sexo, Fone: String;
  Nascim: TDateTime;
  Codigo: Integer;
begin
  Nome := Trim(EdNome.Text);
  CodTxt := Trim(EdCodTxt.Text);
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  Nascim := TPNascim.Date;
  Sexo := EdSexo.Text;
  Codigo := EdCodigo.ValueVariant;
  Fone := Geral.SoNumero_TT(EdFone.Text);
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Informe o nome!') then
    Exit;
  if MyObjects.FIC(CodTxt = '', EdCodTxt, 'Informe o c�digo!') then
    Exit;
(*
  if USWMS.FIC(CPF = '', EdCPF, 'Informe o CPF do nadador ou respons�vel!') then
    Exit;
*)
  if MyObjects.FIC(Nascim < 2, TPNascim, 'Informe a data de nascimento!') then
    Exit;
  if MyObjects.FIC((Sexo <> 'M') and (Sexo <> 'F'), EdSexo, 'Informe o sexo (M ou F)!') then
    Exit;
  if (CPF <> '') and (CPF <> '00000000000') then
  begin
    if MyObjects.FIC(Dmod.CPFCadadstrado(CPF, Codigo) <> 0, nil,
      'O CPF informado j� est� cadastrado!') then
      Exit;
  end else EdCPF.Text := '00000000000';
  //
(*
  if ImgTipo.SQLType = stIns then
    Dmod.TbNadadores.Insert
  else
    Dmod.TbNadadores.Edit;
  //
  Dmod.TbNadadoresCodigo.Value := UMemMod.BuscaNovoCodigo_Int(Dmod.QrAux,
  'nadadores', 'Codigo', [], [], ImgTipo.SQLType, Codigo, siPositivo, EdCodigo);
  Dmod.TbNadadoresCodTxt.Value := CodTxt;
  Dmod.TbNadadoresCPF.Value := CPF;
  Dmod.TbNadadoresNascim.Value := Nascim;
  Dmod.TbNadadoresNome.Value := Nome;
  Dmod.TbNadadoresSexo.Value := Sexo;
  Dmod.TbNadadoresFone.Value := Fone;
  Dmod.TbNadadores.Post;
  //
*)
  Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux,
  'swms_envi_nadadores', 'Codigo', [], [], ImgTipo.SQLType, Codigo, siNegativo, EdCodigo);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'swms_envi_nadadores',
  Codigo, Dmod.QrUpd) then
  begin
    //
{
    MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_XML);
    Dmod.Atualiza_XML_Nadadores();
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
}
    FmSWMS_Mtng_Cab.ReopenSWMS_Envi_Nadadores(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType        := stIns;
      EdCodigo.ValueVariant  := 0;
      EdNome.ValueVariant    := '';
      EdCodTxt.ValueVariant  := '';
      EdSexo.ValueVariant    := '';
      EdFone.ValueVariant    := '';
      EdCPF.ValueVariant     := '';
      TPNascim.Date          := 0;
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmSWMS_Envi_Nadadores.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSWMS_Envi_Nadadores.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSWMS_Envi_Nadadores.FormCreate(Sender: TObject);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
end;

procedure TFmSWMS_Envi_Nadadores.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
