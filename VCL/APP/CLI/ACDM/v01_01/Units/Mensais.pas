unit Mensais;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, Grids,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, DBGrids,
  ComCtrls, dmkGeral, dmkEdit, UnDmkProcFunc, UnDmkEnums;

type
  TFmMensais = class(TForm)
    PainelDados: TPanel;
    DsMensais: TDataSource;
    QrMensais: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill002: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrMensaisCodigo: TIntegerField;
    QrMensaisEncerrado: TIntegerField;
    QrMensaisReceitas: TFloatField;
    QrMensaisDespesas: TFloatField;
    QrMensaisLk: TIntegerField;
    QrMensaisDataCad: TDateField;
    QrMensaisDataAlt: TDateField;
    QrMensaisUserCad: TIntegerField;
    QrMensaisUserAlt: TIntegerField;
    QrMensaisMES2: TWideStringField;
    Label3: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    Label4: TLabel;
    QrLocCodigo: TmySQLQuery;
    QrLocCodigoCodigo: TIntegerField;
    PainelItens: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    DsRecMesIts: TDataSource;
    QrRecMesTot: TmySQLQuery;
    DsRecMesTot: TDataSource;
    QrRecMesTotCredito: TFloatField;
    Label5: TLabel;
    DBEdit01: TDBEdit;
    DBGFiltrado: TDBGrid;
    DBGrid1: TDBGrid;
    QrRecMesIts: TmySQLQuery;
    QrRecMesItsData: TDateField;
    QrRecMesItsTipo: TSmallintField;
    QrRecMesItsCarteira: TIntegerField;
    QrRecMesItsAutorizacao: TIntegerField;
    QrRecMesItsGenero: TIntegerField;
    QrRecMesItsDescricao: TWideStringField;
    QrRecMesItsNotaFiscal: TIntegerField;
    QrRecMesItsDebito: TFloatField;
    QrRecMesItsCredito: TFloatField;
    QrRecMesItsCompensado: TDateField;
    QrRecMesItsDocumento: TFloatField;
    QrRecMesItsSit: TIntegerField;
    QrRecMesItsVencimento: TDateField;
    QrRecMesItsLk: TIntegerField;
    QrRecMesItsFatID: TIntegerField;
    QrRecMesItsFatParcela: TIntegerField;
    QrRecMesItsCONTA: TIntegerField;
    QrRecMesItsNOMECONTA: TWideStringField;
    QrRecMesItsNOMEEMPRESA: TWideStringField;
    QrRecMesItsNOMESUBGRUPO: TWideStringField;
    QrRecMesItsNOMEGRUPO: TWideStringField;
    QrRecMesItsNOMECONJUNTO: TWideStringField;
    QrRecMesItsNOMESIT: TWideStringField;
    QrRecMesItsAno: TFloatField;
    QrRecMesItsMENSAL: TWideStringField;
    QrRecMesItsMENSAL2: TWideStringField;
    QrRecMesItsBanco: TIntegerField;
    QrRecMesItsLocal: TIntegerField;
    QrRecMesItsFatura: TWideStringField;
    QrRecMesItsSub: TSmallintField;
    QrRecMesItsCartao: TIntegerField;
    QrRecMesItsLinha: TIntegerField;
    QrRecMesItsPago: TFloatField;
    QrRecMesItsSALDO: TFloatField;
    QrRecMesItsID_Sub: TSmallintField;
    QrRecMesItsMez: TIntegerField;
    QrRecMesItsFornecedor: TIntegerField;
    QrRecMesItscliente: TIntegerField;
    QrRecMesItsMoraDia: TFloatField;
    QrRecMesItsNOMECLIENTE: TWideStringField;
    QrRecMesItsNOMEFORNECEDOR: TWideStringField;
    QrRecMesItsTIPOEM: TWideStringField;
    QrRecMesItsNOMERELACIONADO: TWideStringField;
    QrRecMesItsOperCount: TIntegerField;
    QrRecMesItsLancto: TIntegerField;
    QrRecMesItsMulta: TFloatField;
    QrRecMesItsATRASO: TFloatField;
    QrRecMesItsJUROS: TFloatField;
    QrRecMesItsDataDoc: TDateField;
    QrRecMesItsNivel: TIntegerField;
    QrRecMesItsVendedor: TIntegerField;
    QrRecMesItsAccount: TIntegerField;
    QrRecMesItsMes2: TLargeintField;
    QrRecMesItsProtesto: TDateField;
    QrRecMesItsDataCad: TDateField;
    QrRecMesItsDataAlt: TDateField;
    QrRecMesItsUserCad: TSmallintField;
    QrRecMesItsUserAlt: TSmallintField;
    QrRecMesItsControle: TIntegerField;
    QrRecMesItsID_Pgto: TIntegerField;
    QrRecMesItsCtrlIni: TIntegerField;
    QrRecMesItsFatID_Sub: TIntegerField;
    QrRecMesItsICMS_P: TFloatField;
    QrRecMesItsICMS_V: TFloatField;
    QrRecMesItsDuplicata: TWideStringField;
    QrRecMesItsCOMPENSADO_TXT: TWideStringField;
    QrRecMesItsCliInt: TIntegerField;
    QrRecMesItsNOMECARTEIRA: TWideStringField;
    QrRecMesItsSALDOCARTEIRA: TFloatField;
    QrRecMesItsNOMECLIENTEINTERNO: TWideStringField;
    QrRecMesItsDepto: TIntegerField;
    QrRecMesItsPrazo: TSmallintField;
    QrRecDiaIts: TmySQLQuery;
    DsRecDiaIts: TDataSource;
    DsRecDiaTot: TDataSource;
    QrRecDiaTot: TmySQLQuery;
    QrRecDiaItsData: TDateField;
    QrRecDiaItsTipo: TSmallintField;
    QrRecDiaItsCarteira: TIntegerField;
    QrRecDiaItsAutorizacao: TIntegerField;
    QrRecDiaItsGenero: TIntegerField;
    QrRecDiaItsDescricao: TWideStringField;
    QrRecDiaItsNotaFiscal: TIntegerField;
    QrRecDiaItsDebito: TFloatField;
    QrRecDiaItsCredito: TFloatField;
    QrRecDiaItsCompensado: TDateField;
    QrRecDiaItsDocumento: TFloatField;
    QrRecDiaItsSit: TIntegerField;
    QrRecDiaItsVencimento: TDateField;
    QrRecDiaItsLk: TIntegerField;
    QrRecDiaItsFatID: TIntegerField;
    QrRecDiaItsFatParcela: TIntegerField;
    QrRecDiaItsCONTA: TIntegerField;
    QrRecDiaItsNOMECONTA: TWideStringField;
    QrRecDiaItsNOMEEMPRESA: TWideStringField;
    QrRecDiaItsNOMESUBGRUPO: TWideStringField;
    QrRecDiaItsNOMEGRUPO: TWideStringField;
    QrRecDiaItsNOMECONJUNTO: TWideStringField;
    QrRecDiaItsNOMESIT: TWideStringField;
    QrRecDiaItsAno: TFloatField;
    QrRecDiaItsMENSAL: TWideStringField;
    QrRecDiaItsMENSAL2: TWideStringField;
    QrRecDiaItsBanco: TIntegerField;
    QrRecDiaItsLocal: TIntegerField;
    QrRecDiaItsFatura: TWideStringField;
    QrRecDiaItsSub: TSmallintField;
    QrRecDiaItsCartao: TIntegerField;
    QrRecDiaItsLinha: TIntegerField;
    QrRecDiaItsPago: TFloatField;
    QrRecDiaItsSALDO: TFloatField;
    QrRecDiaItsID_Sub: TSmallintField;
    QrRecDiaItsMez: TIntegerField;
    QrRecDiaItsFornecedor: TIntegerField;
    QrRecDiaItscliente: TIntegerField;
    QrRecDiaItsMoraDia: TFloatField;
    QrRecDiaItsNOMECLIENTE: TWideStringField;
    QrRecDiaItsNOMEFORNECEDOR: TWideStringField;
    QrRecDiaItsTIPOEM: TWideStringField;
    QrRecDiaItsNOMERELACIONADO: TWideStringField;
    QrRecDiaItsOperCount: TIntegerField;
    QrRecDiaItsLancto: TIntegerField;
    QrRecDiaItsMulta: TFloatField;
    QrRecDiaItsATRASO: TFloatField;
    QrRecDiaItsJUROS: TFloatField;
    QrRecDiaItsDataDoc: TDateField;
    QrRecDiaItsNivel: TIntegerField;
    QrRecDiaItsVendedor: TIntegerField;
    QrRecDiaItsAccount: TIntegerField;
    QrRecDiaItsMes2: TLargeintField;
    QrRecDiaItsProtesto: TDateField;
    QrRecDiaItsDataCad: TDateField;
    QrRecDiaItsDataAlt: TDateField;
    QrRecDiaItsUserCad: TSmallintField;
    QrRecDiaItsUserAlt: TSmallintField;
    QrRecDiaItsControle: TIntegerField;
    QrRecDiaItsID_Pgto: TIntegerField;
    QrRecDiaItsCtrlIni: TIntegerField;
    QrRecDiaItsFatID_Sub: TIntegerField;
    QrRecDiaItsICMS_P: TFloatField;
    QrRecDiaItsICMS_V: TFloatField;
    QrRecDiaItsDuplicata: TWideStringField;
    QrRecDiaItsCOMPENSADO_TXT: TWideStringField;
    QrRecDiaItsCliInt: TIntegerField;
    QrRecDiaItsNOMECARTEIRA: TWideStringField;
    QrRecDiaItsSALDOCARTEIRA: TFloatField;
    QrRecDiaItsNOMECLIENTEINTERNO: TWideStringField;
    QrRecDiaItsDepto: TIntegerField;
    QrRecDiaItsPrazo: TSmallintField;
    DBEdit02: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit03: TDBEdit;
    QrRecMesTotTOTAL: TFloatField;
    QrRecDiaTotCredito: TFloatField;
    TabSheet4: TTabSheet;
    Panel2: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit04: TDBEdit;
    DBEdit05: TDBEdit;
    DBEdit06: TDBEdit;
    PageControl3: TPageControl;
    TabSheet5: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet6: TTabSheet;
    DBGrid3: TDBGrid;
    QrDesDiaIts: TmySQLQuery;
    DsDesDiaIts: TDataSource;
    DsDesDiaTot: TDataSource;
    QrDesDiaTot: TmySQLQuery;
    QrDesMesIts: TmySQLQuery;
    DsDesMesIts: TDataSource;
    DsDesMesTot: TDataSource;
    QrDesMesTot: TmySQLQuery;
    QrDesDiaItsData: TDateField;
    QrDesDiaItsTipo: TSmallintField;
    QrDesDiaItsCarteira: TIntegerField;
    QrDesDiaItsAutorizacao: TIntegerField;
    QrDesDiaItsGenero: TIntegerField;
    QrDesDiaItsDescricao: TWideStringField;
    QrDesDiaItsNotaFiscal: TIntegerField;
    QrDesDiaItsDebito: TFloatField;
    QrDesDiaItsCredito: TFloatField;
    QrDesDiaItsCompensado: TDateField;
    QrDesDiaItsDocumento: TFloatField;
    QrDesDiaItsSit: TIntegerField;
    QrDesDiaItsVencimento: TDateField;
    QrDesDiaItsLk: TIntegerField;
    QrDesDiaItsFatID: TIntegerField;
    QrDesDiaItsFatNum: TIntegerField;
    QrDesDiaItsFatParcela: TIntegerField;
    QrDesDiaItsCONTA: TIntegerField;
    QrDesDiaItsNOMECONTA: TWideStringField;
    QrDesDiaItsNOMEEMPRESA: TWideStringField;
    QrDesDiaItsNOMESUBGRUPO: TWideStringField;
    QrDesDiaItsNOMEGRUPO: TWideStringField;
    QrDesDiaItsNOMECONJUNTO: TWideStringField;
    QrDesDiaItsNOMESIT: TWideStringField;
    QrDesDiaItsAno: TFloatField;
    QrDesDiaItsMENSAL: TWideStringField;
    QrDesDiaItsMENSAL2: TWideStringField;
    QrDesDiaItsBanco: TIntegerField;
    QrDesDiaItsLocal: TIntegerField;
    QrDesDiaItsFatura: TWideStringField;
    QrDesDiaItsSub: TSmallintField;
    QrDesDiaItsCartao: TIntegerField;
    QrDesDiaItsLinha: TIntegerField;
    QrDesDiaItsPago: TFloatField;
    QrDesDiaItsSALDO: TFloatField;
    QrDesDiaItsID_Sub: TSmallintField;
    QrDesDiaItsMez: TIntegerField;
    QrDesDiaItsFornecedor: TIntegerField;
    QrDesDiaItscliente: TIntegerField;
    QrDesDiaItsMoraDia: TFloatField;
    QrDesDiaItsNOMECLIENTE: TWideStringField;
    QrDesDiaItsNOMEFORNECEDOR: TWideStringField;
    QrDesDiaItsTIPOEM: TWideStringField;
    QrDesDiaItsNOMERELACIONADO: TWideStringField;
    QrDesDiaItsOperCount: TIntegerField;
    QrDesDiaItsLancto: TIntegerField;
    QrDesDiaItsMulta: TFloatField;
    QrDesDiaItsATRASO: TFloatField;
    QrDesDiaItsJUROS: TFloatField;
    QrDesDiaItsDataDoc: TDateField;
    QrDesDiaItsNivel: TIntegerField;
    QrDesDiaItsVendedor: TIntegerField;
    QrDesDiaItsAccount: TIntegerField;
    QrDesDiaItsMes2: TLargeintField;
    QrDesDiaItsProtesto: TDateField;
    QrDesDiaItsDataCad: TDateField;
    QrDesDiaItsDataAlt: TDateField;
    QrDesDiaItsUserCad: TSmallintField;
    QrDesDiaItsUserAlt: TSmallintField;
    QrDesDiaItsControle: TIntegerField;
    QrDesDiaItsID_Pgto: TIntegerField;
    QrDesDiaItsCtrlIni: TIntegerField;
    QrDesDiaItsFatID_Sub: TIntegerField;
    QrDesDiaItsICMS_P: TFloatField;
    QrDesDiaItsICMS_V: TFloatField;
    QrDesDiaItsDuplicata: TWideStringField;
    QrDesDiaItsCOMPENSADO_TXT: TWideStringField;
    QrDesDiaItsCliInt: TIntegerField;
    QrDesDiaItsNOMECARTEIRA: TWideStringField;
    QrDesDiaItsSALDOCARTEIRA: TFloatField;
    QrDesDiaItsNOMECLIENTEINTERNO: TWideStringField;
    QrDesDiaItsDepto: TIntegerField;
    QrDesDiaItsPrazo: TSmallintField;
    QrDesMesItsData: TDateField;
    QrDesMesItsTipo: TSmallintField;
    QrDesMesItsCarteira: TIntegerField;
    QrDesMesItsAutorizacao: TIntegerField;
    QrDesMesItsGenero: TIntegerField;
    QrDesMesItsDescricao: TWideStringField;
    QrDesMesItsNotaFiscal: TIntegerField;
    QrDesMesItsDebito: TFloatField;
    QrDesMesItsCredito: TFloatField;
    QrDesMesItsCompensado: TDateField;
    QrDesMesItsDocumento: TFloatField;
    QrDesMesItsSit: TIntegerField;
    QrDesMesItsVencimento: TDateField;
    QrDesMesItsLk: TIntegerField;
    QrDesMesItsFatID: TIntegerField;
    QrDesMesItsFatNum: TIntegerField;
    QrDesMesItsFatParcela: TIntegerField;
    QrDesMesItsCONTA: TIntegerField;
    QrDesMesItsNOMECONTA: TWideStringField;
    QrDesMesItsNOMEEMPRESA: TWideStringField;
    QrDesMesItsNOMESUBGRUPO: TWideStringField;
    QrDesMesItsNOMEGRUPO: TWideStringField;
    QrDesMesItsNOMECONJUNTO: TWideStringField;
    QrDesMesItsNOMESIT: TWideStringField;
    QrDesMesItsAno: TFloatField;
    QrDesMesItsMENSAL: TWideStringField;
    QrDesMesItsMENSAL2: TWideStringField;
    QrDesMesItsBanco: TIntegerField;
    QrDesMesItsLocal: TIntegerField;
    QrDesMesItsFatura: TWideStringField;
    QrDesMesItsSub: TSmallintField;
    QrDesMesItsCartao: TIntegerField;
    QrDesMesItsLinha: TIntegerField;
    QrDesMesItsPago: TFloatField;
    QrDesMesItsSALDO: TFloatField;
    QrDesMesItsID_Sub: TSmallintField;
    QrDesMesItsMez: TIntegerField;
    QrDesMesItsFornecedor: TIntegerField;
    QrDesMesItscliente: TIntegerField;
    QrDesMesItsMoraDia: TFloatField;
    QrDesMesItsNOMECLIENTE: TWideStringField;
    QrDesMesItsNOMEFORNECEDOR: TWideStringField;
    QrDesMesItsTIPOEM: TWideStringField;
    QrDesMesItsNOMERELACIONADO: TWideStringField;
    QrDesMesItsOperCount: TIntegerField;
    QrDesMesItsLancto: TIntegerField;
    QrDesMesItsMulta: TFloatField;
    QrDesMesItsATRASO: TFloatField;
    QrDesMesItsJUROS: TFloatField;
    QrDesMesItsDataDoc: TDateField;
    QrDesMesItsNivel: TIntegerField;
    QrDesMesItsVendedor: TIntegerField;
    QrDesMesItsAccount: TIntegerField;
    QrDesMesItsMes2: TLargeintField;
    QrDesMesItsProtesto: TDateField;
    QrDesMesItsDataCad: TDateField;
    QrDesMesItsDataAlt: TDateField;
    QrDesMesItsUserCad: TSmallintField;
    QrDesMesItsUserAlt: TSmallintField;
    QrDesMesItsControle: TIntegerField;
    QrDesMesItsID_Pgto: TIntegerField;
    QrDesMesItsCtrlIni: TIntegerField;
    QrDesMesItsFatID_Sub: TIntegerField;
    QrDesMesItsICMS_P: TFloatField;
    QrDesMesItsICMS_V: TFloatField;
    QrDesMesItsDuplicata: TWideStringField;
    QrDesMesItsCOMPENSADO_TXT: TWideStringField;
    QrDesMesItsCliInt: TIntegerField;
    QrDesMesItsNOMECARTEIRA: TWideStringField;
    QrDesMesItsSALDOCARTEIRA: TFloatField;
    QrDesMesItsNOMECLIENTEINTERNO: TWideStringField;
    QrDesMesItsDepto: TIntegerField;
    QrDesMesItsPrazo: TSmallintField;
    QrDesMesTotTOTAL: TFloatField;
    QrDesDiaTotDebito: TFloatField;
    QrDesMesTotDebito: TFloatField;
    TabSheet7: TTabSheet;
    QrSalas: TmySQLQuery;
    DsSalas: TDataSource;
    QrSalasCodigo: TIntegerField;
    QrSalasNome: TWideStringField;
    QrSalasLotacao: TIntegerField;
    QrSalasLk: TIntegerField;
    QrSalasDataCad: TDateField;
    QrSalasDataAlt: TDateField;
    QrSalasUserCad: TIntegerField;
    QrSalasUserAlt: TIntegerField;
    QrSalasArea: TFloatField;
    QrSalaswats_h: TFloatField;
    QrSalasDepartamento: TIntegerField;
    Panel4: TPanel;
    QrSalasTot: TmySQLQuery;
    DsSalasTot: TDataSource;
    QrSalasTotLotacao: TFloatField;
    QrSalasTotArea: TFloatField;
    QrSalasTotwats_h: TFloatField;
    Panel7: TPanel;
    Panel8: TPanel;
    QrSDArea: TmySQLQuery;
    DsSDArea: TDataSource;
    QrSDAreaDebito: TFloatField;
    GroupBox1: TGroupBox;
    DBEdit07: TDBEdit;
    Label11: TLabel;
    GroupBox2: TGroupBox;
    Label17: TLabel;
    DBEdit010: TDBEdit;
    DBEdit08: TDBEdit;
    Label12: TLabel;
    DBEdit011: TDBEdit;
    Label18: TLabel;
    GroupBox3: TGroupBox;
    Label13: TLabel;
    DBEdit09: TDBEdit;
    QrSalasTotCUSTO_AREA: TFloatField;
    QrSalasCUSTO_AREA: TFloatField;
    QrSOcup: TmySQLQuery;
    DsSOcup: TDataSource;
    QrSOcupSala: TIntegerField;
    QrSOcupCARGA: TFloatField;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    DBGrid4: TDBGrid;
    Panel13: TPanel;
    DBGrid5: TDBGrid;
    QrSDesoc: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    IntegerField6: TIntegerField;
    DsSDesoc: TDataSource;
    QrSalasOCUPACAO: TIntegerField;
    QrSalasHORAS_OCUP: TWideStringField;
    QrSalasCUSTO_HORAAREA: TFloatField;
    QrSalasCUSTO_MINUTOAREA: TFloatField;
    QrSalasCUSTO_HORATOTAL: TFloatField;
    Label21: TLabel;
    EdCustokw: TdmkEdit;
    QrMensaisCustokw: TFloatField;
    DBEdit012: TDBEdit;
    Label19: TLabel;
    QrSalasCUSTO_WATS: TFloatField;
    Label20: TLabel;
    DBEdit013: TDBEdit;
    Label22: TLabel;
    DBEdit014: TDBEdit;
    Label23: TLabel;
    DBEdit015: TDBEdit;
    QrSDkw: TmySQLQuery;
    DsSDkw: TDataSource;
    QrSDkwDebito: TFloatField;
    QrSalasTotEXCEDEWATS: TFloatField;
    QrSOcuT: TmySQLQuery;
    DsSOcuT: TDataSource;
    QrSalasTotCUSTO_WATS: TFloatField;
    QrSOcuTTOTAL: TFloatField;
    Label24: TLabel;
    DBEdit016: TDBEdit;
    QrSalasCUSTO_WATSHORA: TFloatField;
    QrSalasCUSTO_WATSMINUTO: TFloatField;
    QrProfDes: TmySQLQuery;
    DsProfDes: TDataSource;
    QrProfDesDebito: TFloatField;
    TabSheet8: TTabSheet;
    Panel6: TPanel;
    GroupBox4: TGroupBox;
    Label25: TLabel;
    DBEdit017: TDBEdit;
    DBGrid6: TDBGrid;
    DsProfIts: TDataSource;
    QrProfIts: TmySQLQuery;
    QrProfItsProfessor: TIntegerField;
    QrProfItsCarga: TFloatField;
    QrProfItsNOMEPROFESSOR: TWideStringField;
    QrProfSal: TmySQLQuery;
    DsProfSal: TDataSource;
    QrProfSalEntidade: TIntegerField;
    QrProfSalDebito: TFloatField;
    QrProfItsCUSTOS: TFloatField;
    QrProfItsCUSTO_MINUTO: TFloatField;
    QrProfItsCUSTO_HORA: TFloatField;
    QrProfItsCARGA_HORAS: TWideStringField;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    QrCursos: TmySQLQuery;
    DsCursos: TDataSource;
    DsCur: TDataSource;
    QrCur: TmySQLQuery;
    QrCursosCurso: TIntegerField;
    DBGrid7: TDBGrid;
    QrCurCodigo: TIntegerField;
    QrCurNome: TWideStringField;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    QrCurPC: TmySQLQuery;
    DsCurPC: TDataSource;
    QrCurPCCurso: TIntegerField;
    QrCurPCCREDITO: TFloatField;
    QrCurPC2: TFloatField;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    QrMatPC: TmySQLQuery;
    DsMatPC: TDataSource;
    DBGrid8: TDBGrid;
    QrMatPCCurso: TIntegerField;
    QrMatPCCredito: TFloatField;
    QrMat: TmySQLQuery;
    DsMat: TDataSource;
    QrMatCodigo: TIntegerField;
    QrMatNome: TWideStringField;
    QrMatPC2: TFloatField;
    DBGrid9: TDBGrid;
    QrTur: TmySQLQuery;
    DsTur: TDataSource;
    QrTurCodigo: TIntegerField;
    QrTurNome: TWideStringField;
    QrTurCredito: TFloatField;
    QrRecMesItsFatNum: TFloatField;
    QrRecDiaItsFatNum: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMensaisAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrMensaisAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMensaisBeforeOpen(DataSet: TDataSet);
    procedure QrMensaisCalcFields(DataSet: TDataSet);
    procedure QrRecMesItsCalcFields(DataSet: TDataSet);
    procedure QrRecDiaItsCalcFields(DataSet: TDataSet);
    procedure QrRecMesTotCalcFields(DataSet: TDataSet);
    procedure QrDesDiaItsCalcFields(DataSet: TDataSet);
    procedure QrDesMesItsCalcFields(DataSet: TDataSet);
    procedure QrDesMesTotCalcFields(DataSet: TDataSet);
    procedure QrSalasTotCalcFields(DataSet: TDataSet);
    procedure QrSalasCalcFields(DataSet: TDataSet);
    procedure EdCustokwExit(Sender: TObject);
    procedure QrProfItsCalcFields(DataSet: TDataSet);
  private
    FMensal: Integer;
    FDataI, FDataF: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; Status: String; Periodo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenRecMes;
    procedure ReopenDesMes;
    procedure ReopenSalas;
    procedure ReopenProfessores;
    //
    procedure ReopenCursos;
  public
    { Public declarations }
  end;

var
  FmMensais: TFmMensais;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMensais.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMensais.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMensaisCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMensais.DefParams;
begin
  VAR_GOTOTABELA := 'Mensais';
  VAR_GOTOMYSQLTABLE := QrMensais;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM mensais');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMensais.MostraEdicao(Mostra: Boolean; Status: String; Periodo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    PainelControle.Visible:=False;
    LaTipo.Caption := Status;
    if Status = CO_INCLUSAO then
    begin
      if Periodo = 0 then Periodo := Geral.Periodo2000(Date);
      CBMes.ItemIndex := MLAGeral.MesDoPeriodo(Periodo)-1;
      CBAno.Text      := IntToStr(MLAGeral.AnoDoPeriodo(Periodo));
      CBMes.Enabled := True;
      CBAno.Enabled := True;
    end else begin
      CBMes.ItemIndex := MLAGeral.MesDoPeriodo(Periodo)-1;
      CBAno.Text      := IntToStr(MLAGeral.AnoDoPeriodo(Periodo));
      CBMes.Enabled := False;
      CBAno.Enabled := False;
    end;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmMensais.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMensais.AlteraRegistro;
var
  Mensais : Integer;
begin
  Mensais := QrMensaisCodigo.Value;
  if QrMensaisCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Mensais, Dmod.MyDB, 'Mensais', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Mensais, Dmod.MyDB, 'Mensais', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, QrMensaisCodigo.Value);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMensais.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(True, CO_INCLUSAO, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmMensais.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMensais.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMensais.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMensais.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMensais.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMensais.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMensais.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmMensais.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmMensais.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMensaisCodigo.Value;
  Close;
end;

procedure TFmMensais.BtConfirmaClick(Sender: TObject);
var
  Ano, Mes : word;
  Codigo: Integer;
  CustoKw: Double;
begin
  Ano := Geral.IMV(CBAno.Text);
  //
  if Ano = 0 then
    Exit;
  //
  Mes    := CBMes.ItemIndex + 1;
  Codigo := ((Ano - 2000) * 12) + Mes;
  //
  QrLocCodigo.Close;
  QrLocCodigo.Params[0].AsInteger := Codigo;
  QrLocCodigo.Open;
  //
  if (QrLocCodigo.RecordCount > 0) and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    if Geral.MB_Pergunta('Este m�s j� foi aberto!. Deseja localiz�-lo?') = ID_YES then
      LocCod(QrMensaisCodigo.Value, QrMensaisCodigo.Value);
  end else
  begin
    CustoKw := Geral.DMV(EdCustokw.Text);
    //
    if MyObjects.FIC(Custokw < 0.0000, EdCustokw, 'Informe o custo da energia el�trica (kw)!') then Exit;
    //
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add(MLAGeral.AcaoSQL(LaTipo.Caption));
    Dmod.QrUpdM.SQL.Add(' Mensais SET Custokw=:P0');
    Dmod.QrUpdM.SQL.Add(MLAGeral.LigaSQL(LaTipo.Caption, 1));
    Dmod.QrUpdM.SQL.Add(' Codigo=:P1');
    Dmod.QrUpdM.Params[0].AsFloat   := Custokw;
    Dmod.QrUpdM.Params[1].AsInteger := Codigo;
    Dmod.QrUpdM.ExecSQL;
    LocCod(QrMensaisCodigo.Value, Codigo);
  end;
  QrLocCodigo.Close;
  //
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmMensais.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := 0;
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Mensais', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Mensais', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Mensais', 'Codigo');
end;

procedure TFmMensais.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia : Word;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    Add(IntToStr(Ano-2));
    Add(IntToStr(Ano-1));
    Add(IntToStr(Ano));
    Add(IntToStr(Ano+1));
    Add(IntToStr(Ano+2));
  end;
  CBAno.ItemIndex := 2;
  CBMes.ItemIndex := (Mes - 1);
  //////////////////////////////////////////////////////////////////////////
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelItens.Align := alClient;
  CriaOForm;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  QrCursos.Open;
end;

procedure TFmMensais.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMensaisCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMensais.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMensais.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmMensais.QrMensaisAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMensais.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Mensais', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmMensais.QrMensaisAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrMensaisCodigo.Value, False);
  FMensal := MLAGeral.PeriodoToAnoMes(QrMensaisCodigo.Value);
  FDataI := MLAGeral.PrimeiroDiaDoPeriodo(QrMensaisCodigo.Value, dtSystem);
  FDataF := MLAGeral.UltimoDiaDoPeriodo(QrMensaisCodigo.Value, dtSystem);
  //
  ReopenRecMes;
  ReopenDesMes;
  ReopenSalas;
  ReopenProfessores;
  //
  ReopenCursos;
end;

procedure TFmMensais.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMensaisCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Mensais', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMensais.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmMensais.QrMensaisBeforeOpen(DataSet: TDataSet);
begin
  QrMensaisCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMensais.QrMensaisCalcFields(DataSet: TDataSet);
begin
  QrMensaisMES2.Value := MLAGeral.PrimeiroDiaDoPeriodo(
    QrMensaisCodigo.Value, dtTexto);
end;

procedure TFmMensais.QrRecMesItsCalcFields(DataSet: TDataSet);
begin
  if QrRecMesItsMes2.Value > 0 then
    QrRecMesItsMENSAL.Value := FormatFloat('00', QrRecMesItsMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrRecMesItsAno.Value), 3, 2)
   else QrRecMesItsMENSAL.Value := CO_VAZIO;
  if QrRecMesItsMes2.Value > 0 then
    QrRecMesItsMENSAL2.Value := FormatFloat('0000', QrRecMesItsAno.Value)+'/'+
    FormatFloat('00', QrRecMesItsMes2.Value)+'/01'
   else QrRecMesItsMENSAL2.Value := CO_VAZIO;

//  if QrRecMesItsSit.Value = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if QrRecMesItsSit.Value = -1 then
     QrRecMesItsNOMESIT.Value := CO_IMPORTACAO
  else
    if QrRecMesItsSit.Value = 1 then
     QrRecMesItsNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrRecMesItsSit.Value = 2 then
       QrRecMesItsNOMESIT.Value := CO_QUITADA
  else if QrRecMesItsSit.Value = 3 then
  begin
    // Caixa com prazo
    if (QrRecMesItsPrazo.Value = 0) and (QrRecMesItsTipo.Value=0) then
    begin
      if QrRecMesItsVencimento.Value < Date then
      QrRecMesItsNOMESIT.Value := CO_QUIT_AUTOM
      else
      QrRecMesItsNOMESIT.Value := CO_PREDATADO;
    end else QrRecMesItsNOMESIT.Value := CO_COMPENSADA;
  end else
    if QrRecMesItsVencimento.Value < Date then
       QrRecMesItsNOMESIT.Value := CO_VENCIDA
  else
       QrRecMesItsNOMESIT.Value := CO_EMABERTO;
  case QrRecMesItsSit.Value of
    0: QrRecMesItsSALDO.Value := QrRecMesItsCredito.Value - QrRecMesItsDebito.Value;
    1: QrRecMesItsSALDO.Value := (QrRecMesItsCredito.Value - QrRecMesItsDebito.Value) - QrRecMesItsPago.Value;
    else QrRecMesItsSALDO.Value := 0;
  end;
  QrRecMesItsNOMERELACIONADO.Value := CO_VAZIO;
  if QrRecMesItscliente.Value <> 0 then
  QrRecMesItsNOMERELACIONADO.Value := QrRecMesItsNOMECLIENTE.Value;
  if QrRecMesItsFornecedor.Value <> 0 then
  QrRecMesItsNOMERELACIONADO.Value := QrRecMesItsNOMERELACIONADO.Value +
  QrRecMesItsNOMEFORNECEDOR.Value;
  //
  if QrRecMesItsVencimento.Value > Date then QrRecMesItsATRASO.Value := 0
    else QrRecMesItsATRASO.Value := Date - QrRecMesItsVencimento.Value;
  //
  QrRecMesItsJUROS.Value :=
    Trunc(QrRecMesItsATRASO.Value * QrRecMesItsMoraDia.Value * 100)/100;
  //
  if QrRecMesItsCompensado.Value = 0 then
     QrRecMesItsCOMPENSADO_TXT.Value := '' else
     QrRecMesItsCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE2, QrRecMesItsCompensado.Value);
end;

procedure TFmMensais.QrRecDiaItsCalcFields(DataSet: TDataSet);
begin
  if QrRecDiaItsMes2.Value > 0 then
    QrRecDiaItsMENSAL.Value := FormatFloat('00', QrRecDiaItsMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrRecDiaItsAno.Value), 3, 2)
   else QrRecDiaItsMENSAL.Value := CO_VAZIO;
  if QrRecDiaItsMes2.Value > 0 then
    QrRecDiaItsMENSAL2.Value := FormatFloat('0000', QrRecDiaItsAno.Value)+'/'+
    FormatFloat('00', QrRecDiaItsMes2.Value)+'/01'
   else QrRecDiaItsMENSAL2.Value := CO_VAZIO;

//  if QrRecDiaItsSit.Value = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if QrRecDiaItsSit.Value = -1 then
     QrRecDiaItsNOMESIT.Value := CO_IMPORTACAO
  else
    if QrRecDiaItsSit.Value = 1 then
     QrRecDiaItsNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrRecDiaItsSit.Value = 2 then
       QrRecDiaItsNOMESIT.Value := CO_QUITADA
  else if QrRecDiaItsSit.Value = 3 then
  begin
    // Caixa com prazo
    if (QrRecDiaItsPrazo.Value = 0) and (QrRecDiaItsTipo.Value=0) then
    begin
      if QrRecDiaItsVencimento.Value < Date then
      QrRecDiaItsNOMESIT.Value := CO_QUIT_AUTOM
      else
      QrRecDiaItsNOMESIT.Value := CO_PREDATADO;
    end else QrRecDiaItsNOMESIT.Value := CO_COMPENSADA;
  end else
    if QrRecDiaItsVencimento.Value < Date then
       QrRecDiaItsNOMESIT.Value := CO_VENCIDA
  else
       QrRecDiaItsNOMESIT.Value := CO_EMABERTO;
  case QrRecDiaItsSit.Value of
    0: QrRecDiaItsSALDO.Value := QrRecDiaItsCredito.Value - QrRecDiaItsDebito.Value;
    1: QrRecDiaItsSALDO.Value := (QrRecDiaItsCredito.Value - QrRecDiaItsDebito.Value) - QrRecDiaItsPago.Value;
    else QrRecDiaItsSALDO.Value := 0;
  end;
  QrRecDiaItsNOMERELACIONADO.Value := CO_VAZIO;
  if QrRecDiaItscliente.Value <> 0 then
  QrRecDiaItsNOMERELACIONADO.Value := QrRecDiaItsNOMECLIENTE.Value;
  if QrRecDiaItsFornecedor.Value <> 0 then
  QrRecDiaItsNOMERELACIONADO.Value := QrRecDiaItsNOMERELACIONADO.Value +
  QrRecDiaItsNOMEFORNECEDOR.Value;
  //
  if QrRecDiaItsVencimento.Value > Date then QrRecDiaItsATRASO.Value := 0
    else QrRecDiaItsATRASO.Value := Date - QrRecDiaItsVencimento.Value;
  //
  QrRecDiaItsJUROS.Value :=
    Trunc(QrRecDiaItsATRASO.Value * QrRecDiaItsMoraDia.Value * 100)/100;
  //
  if QrRecDiaItsCompensado.Value = 0 then
     QrRecDiaItsCOMPENSADO_TXT.Value := '' else
     QrRecDiaItsCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE2, QrRecDiaItsCompensado.Value);
end;

procedure TFmMensais.ReopenRecMes;
begin
  QrRecDiaIts.Close;
  QrRecDiaIts.Params[0].AsString  := FDataI;
  QrRecDiaIts.Params[1].AsString  := FDataF;
  QrRecDiaIts.Open;
  //
  QrRecDiaTot.Close;
  QrRecDiaTot.Params[0].AsString  := FDataI;
  QrRecDiaTot.Params[1].AsString  := FDataF;
  QrRecDiaTot.Open;
  //
  QrRecMesIts.Close;
  QrRecMesIts.Params[0].AsInteger := FMensal;
  QrRecMesIts.Open;
  //
  QrRecMesTot.Close;
  QrRecMesTot.Params[0].AsInteger := FMensal;
  QrRecMesTot.Open;
  //
end;

procedure TFmMensais.ReopenDesMes;
begin
  QrDesDiaIts.Close;
  QrDesDiaIts.Params[0].AsString  := FDataI;
  QrDesDiaIts.Params[1].AsString  := FDataF;
  QrDesDiaIts.Open;
  //
  QrDesDiaTot.Close;
  QrDesDiaTot.Params[0].AsString  := FDataI;
  QrDesDiaTot.Params[1].AsString  := FDataF;
  QrDesDiaTot.Open;
  //
  QrDesMesIts.Close;
  QrDesMesIts.Params[0].AsInteger := FMensal;
  QrDesMesIts.Open;
  //
  QrDesMesTot.Close;
  QrDesMesTot.Params[0].AsInteger := FMensal;
  QrDesMesTot.Open;
  //
end;

procedure TFmMensais.QrRecMesTotCalcFields(DataSet: TDataSet);
begin
  QrRecMesTotTOTAL.Value := QrRecMesTotCredito.Value + QrRecDiaTotCredito.Value;
end;

procedure TFmMensais.QrDesDiaItsCalcFields(DataSet: TDataSet);
begin
  if QrDesDiaItsMes2.Value > 0 then
    QrDesDiaItsMENSAL.Value := FormatFloat('00', QrDesDiaItsMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrDesDiaItsAno.Value), 3, 2)
   else QrDesDiaItsMENSAL.Value := CO_VAZIO;
  if QrDesDiaItsMes2.Value > 0 then
    QrDesDiaItsMENSAL2.Value := FormatFloat('0000', QrDesDiaItsAno.Value)+'/'+
    FormatFloat('00', QrDesDiaItsMes2.Value)+'/01'
   else QrDesDiaItsMENSAL2.Value := CO_VAZIO;

//  if QrDesDiaItsSit.Value = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if QrDesDiaItsSit.Value = -1 then
     QrDesDiaItsNOMESIT.Value := CO_IMPORTACAO
  else
    if QrDesDiaItsSit.Value = 1 then
     QrDesDiaItsNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrDesDiaItsSit.Value = 2 then
       QrDesDiaItsNOMESIT.Value := CO_QUITADA
  else if QrDesDiaItsSit.Value = 3 then
  begin
    // Caixa com prazo
    if (QrDesDiaItsPrazo.Value = 0) and (QrDesDiaItsTipo.Value=0) then
    begin
      if QrDesDiaItsVencimento.Value < Date then
      QrDesDiaItsNOMESIT.Value := CO_QUIT_AUTOM
      else
      QrDesDiaItsNOMESIT.Value := CO_PREDATADO;
    end else QrDesDiaItsNOMESIT.Value := CO_COMPENSADA;
  end else
    if QrDesDiaItsVencimento.Value < Date then
       QrDesDiaItsNOMESIT.Value := CO_VENCIDA
  else
       QrDesDiaItsNOMESIT.Value := CO_EMABERTO;
  case QrDesDiaItsSit.Value of
    0: QrDesDiaItsSALDO.Value := QrDesDiaItsCredito.Value - QrDesDiaItsDebito.Value;
    1: QrDesDiaItsSALDO.Value := (QrDesDiaItsCredito.Value - QrDesDiaItsDebito.Value) - QrDesDiaItsPago.Value;
    else QrDesDiaItsSALDO.Value := 0;
  end;
  QrDesDiaItsNOMERELACIONADO.Value := CO_VAZIO;
  if QrDesDiaItscliente.Value <> 0 then
  QrDesDiaItsNOMERELACIONADO.Value := QrDesDiaItsNOMECLIENTE.Value;
  if QrDesDiaItsFornecedor.Value <> 0 then
  QrDesDiaItsNOMERELACIONADO.Value := QrDesDiaItsNOMERELACIONADO.Value +
  QrDesDiaItsNOMEFORNECEDOR.Value;
  //
  if QrDesDiaItsVencimento.Value > Date then QrDesDiaItsATRASO.Value := 0
    else QrDesDiaItsATRASO.Value := Date - QrDesDiaItsVencimento.Value;
  //
  QrDesDiaItsJUROS.Value :=
    Trunc(QrDesDiaItsATRASO.Value * QrDesDiaItsMoraDia.Value * 100)/100;
  //
  if QrDesDiaItsCompensado.Value = 0 then
     QrDesDiaItsCOMPENSADO_TXT.Value := '' else
     QrDesDiaItsCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE2, QrDesDiaItsCompensado.Value);
end;

procedure TFmMensais.QrDesMesItsCalcFields(DataSet: TDataSet);
begin
  if QrDesMesItsMes2.Value > 0 then
    QrDesMesItsMENSAL.Value := FormatFloat('00', QrDesMesItsMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrDesMesItsAno.Value), 3, 2)
   else QrDesMesItsMENSAL.Value := CO_VAZIO;
  if QrDesMesItsMes2.Value > 0 then
    QrDesMesItsMENSAL2.Value := FormatFloat('0000', QrDesMesItsAno.Value)+'/'+
    FormatFloat('00', QrDesMesItsMes2.Value)+'/01'
   else QrDesMesItsMENSAL2.Value := CO_VAZIO;

//  if QrDesMesItsSit.Value = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if QrDesMesItsSit.Value = -1 then
     QrDesMesItsNOMESIT.Value := CO_IMPORTACAO
  else
    if QrDesMesItsSit.Value = 1 then
     QrDesMesItsNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrDesMesItsSit.Value = 2 then
       QrDesMesItsNOMESIT.Value := CO_QUITADA
  else if QrDesMesItsSit.Value = 3 then
  begin
    // Caixa com prazo
    if (QrDesMesItsPrazo.Value = 0) and (QrDesMesItsTipo.Value=0) then
    begin
      if QrDesMesItsVencimento.Value < Date then
      QrDesMesItsNOMESIT.Value := CO_QUIT_AUTOM
      else
      QrDesMesItsNOMESIT.Value := CO_PREDATADO;
    end else QrDesMesItsNOMESIT.Value := CO_COMPENSADA;
  end else
    if QrDesMesItsVencimento.Value < Date then
       QrDesMesItsNOMESIT.Value := CO_VENCIDA
  else
       QrDesMesItsNOMESIT.Value := CO_EMABERTO;
  case QrDesMesItsSit.Value of
    0: QrDesMesItsSALDO.Value := QrDesMesItsCredito.Value - QrDesMesItsDebito.Value;
    1: QrDesMesItsSALDO.Value := (QrDesMesItsCredito.Value - QrDesMesItsDebito.Value) - QrDesMesItsPago.Value;
    else QrDesMesItsSALDO.Value := 0;
  end;
  QrDesMesItsNOMERELACIONADO.Value := CO_VAZIO;
  if QrDesMesItscliente.Value <> 0 then
  QrDesMesItsNOMERELACIONADO.Value := QrDesMesItsNOMECLIENTE.Value;
  if QrDesMesItsFornecedor.Value <> 0 then
  QrDesMesItsNOMERELACIONADO.Value := QrDesMesItsNOMERELACIONADO.Value +
  QrDesMesItsNOMEFORNECEDOR.Value;
  //
  if QrDesMesItsVencimento.Value > Date then QrDesMesItsATRASO.Value := 0
    else QrDesMesItsATRASO.Value := Date - QrDesMesItsVencimento.Value;
  //
  QrDesMesItsJUROS.Value :=
    Trunc(QrDesMesItsATRASO.Value * QrDesMesItsMoraDia.Value * 100)/100;
  //
  if QrDesMesItsCompensado.Value = 0 then
     QrDesMesItsCOMPENSADO_TXT.Value := '' else
     QrDesMesItsCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE2, QrDesMesItsCompensado.Value);
end;

procedure TFmMensais.QrDesMesTotCalcFields(DataSet: TDataSet);
begin
  QrDesMesTotTOTAL.Value := QrDesMesTotDebito.Value + QrDesDiaTotDebito.Value;
end;

procedure TFmMensais.ReopenSalas;
var
  Salas: String;
begin
  Salas := '';
  QrSalas.Close;
  QrSalasTot.Close;
  QrSDArea.Close;
  QrSOcup.Close;
  QrSOcuT.Close;
  //
  QrSOcuT.Params[0].AsString  := FDataI;
  QrSOcuT.Params[1].AsString  := FDataF;
  QrSOcuT.Open;
  //
  QrSOcup.Params[0].AsString  := FDataI;
  QrSOcup.Params[1].AsString  := FDataF;
  QrSOcup.Open;
  while not QrSOcup.Eof do
  begin
    Salas := Salas + IntToStr(QrSOcupSala.Value)+',';
    QrSOcup.Next;
  end;
  Salas := Salas + '-1000';
  //
  QrSDArea.Params[0].AsInteger := FMensal;
  QrSDArea.Params[1].AsString  := FDataI;
  QrSDArea.Params[2].AsString  := FDataF;
  QrSDArea.Open;
  //
  QrSDkw.Params[0].AsInteger := FMensal;
  QrSDkw.Params[1].AsString  := FDataI;
  QrSDkw.Params[2].AsString  := FDataF;
  QrSDkw.Open;
  //
  QrSalasTot.SQL[4] := Salas;
  QrSalasTot.Open;
  //
  QrSalas.SQL[2] := Salas;
  QrSalas.Open;
  //
  QrSDesoc.SQL[2] := Salas;
  QrSDesoc.Open;
  //
end;

procedure TFmMensais.ReopenProfessores;
begin
  QrProfSal.Close;
  QrProfSal.Params[0].AsInteger := FMensal;
  QrProfSal.Params[1].AsString  := FDataI;
  QrProfSal.Params[2].AsString  := FDataF;
  QrProfSal.Open;
  //
  QrProfDes.Close;
  QrProfDes.Params[0].AsInteger := FMensal;
  QrProfDes.Params[1].AsString  := FDataI;
  QrProfDes.Params[2].AsString  := FDataF;
  QrProfDes.Open;
  //
  QrProfIts.Close;
  QrProfIts.Params[0].AsString  := FDataI;
  QrProfIts.Params[1].AsString  := FDataF;
  QrProfIts.Open;
  //
end;

procedure TFmMensais.QrSalasTotCalcFields(DataSet: TDataSet);
begin
  if QrSalasTotArea.Value <=0 then
  QrSalasTotCUSTO_AREA.Value := 0 else
  QrSalasTotCUSTO_AREA.Value :=
  QrSDAreaDebito.Value / QrSalasTotArea.Value;
  //
  QrSalasTotCUSTO_WATS.Value := QrMensaisCustokw.Value * QrSOcuTTOTAL.Value;
  QrSalasTotEXCEDEWATS.Value := QrSDkwDebito.Value - QrSalasTotCUSTO_WATS.Value;
end;

procedure TFmMensais.QrSalasCalcFields(DataSet: TDataSet);
begin
  QrSalasCUSTO_AREA.Value := QrSalasArea.Value * QrSalasTotCUSTO_AREA.Value;
  QrSalasHORAS_OCUP.Value :=
    FormatFloat('0', QrSalasOcupacao.Value div 60) + ':' +
    FormatFloat('00', QrSalasOcupacao.Value mod 60)+' ';
  if QrSalasOCUPACAO.Value <= 0 then
  begin
    QrSalasCUSTO_MINUTOAREA.Value := 0;
    QrSalasCUSTO_HORAAREA.Value   := 0;
  end else begin
    QrSalasCUSTO_MINUTOAREA.Value := QrSalasCUSTO_AREA.Value / QrSalasOCUPACAO.Value;
    QrSalasCUSTO_HORAAREA.Value   := QrSalasCUSTO_MINUTOAREA.Value * 60;
  end;
  QrSalasCUSTO_WATSHORA.Value := QrSalaswats_h.Value *
    QrMensaisCustokw.Value / 1000;
  QrSalasCUSTO_WATSMINUTO.Value := QrSalasCUSTO_WATSHORA.Value / 60;
  QrSalasCUSTO_WATS.Value := QrSalasCUSTO_WATSHORA.Value *
    QrSalasOCUPACAO.Value / 60;
end;

procedure TFmMensais.EdCustokwExit(Sender: TObject);
begin
  EdCustoKw.Text := Geral.TFT(EdCustoKw.Text, 4, siPositivo);
end;

procedure TFmMensais.QrProfItsCalcFields(DataSet: TDataSet);
begin
  if QrProfItsCarga.Value <= 0 then
  QrProfItsCUSTO_MINUTO.Value := 0 else
  QrProfItsCUSTO_MINUTO.Value :=
  QrProfItsCUSTOS.Value / QrProfItsCarga.Value;
  //
  QrProfItsCUSTO_HORA.Value := QrProfItsCUSTO_MINUTO.Value * 60;
  //
  QrProfItsCARGA_HORAS.Value :=
    FormatFloat('0', Trunc(QrProfItsCarga.Value) div 60) + ':' +
    FormatFloat('00', Trunc(QrProfItsCarga.Value) mod 60)+' ';
end;

procedure TFmMensais.ReopenCursos;
var
  Cursos: String;
begin
  Cursos := 'WHERE Codigo in (';
  //
  QrCursos.Close;
  QrCursos.Params[0].AsString  := FDataI;
  QrCursos.Params[1].AsString  := FDataF;
  QrCursos.Open;
  while not QrCursos.Eof do
  begin
    Cursos := Cursos + IntToStr(QrCursosCurso.Value)+',';
    QrCursos.Next;
  end;
  //
  QrCurPC.Close;
  QrCurPC.Params[0].AsString  := FDataI;
  QrCurPC.Params[1].AsString  := FDataF;
  QrCurPC.Open;
  while not QrCurPC.Eof do
  begin
    Cursos := Cursos + IntToStr(QrCurPCCurso.Value)+',';
    QrCurPC.Next;
  end;
  //
  //
  QrMatPC.Close;
  QrMatPC.Params[0].AsString  := FDataI;
  QrMatPC.Params[1].AsString  := FDataF;
  QrMatPC.Open;
  while not QrMatPC.Eof do
  begin
    Cursos := Cursos + IntToStr(QrMatPCCurso.Value)+',';
    QrMatPC.Next;
  end;
  //
  Cursos := Cursos + '-1000)';
  //
  QrCur.Close;
  QrCur.SQL[2] := Cursos;
  QrCur.Open;
  //
  QrMat.Close;
  QrMat.SQL[2] := Cursos;
  QrMat.Open;
  //
  QrTur.Close;
  QrTur.Params[0].AsString  := FDataI;
  QrTur.Params[1].AsString  := FDataF;
  QrTur.Open;
  (*QrSDArea.Params[0].AsInteger := FMensal;
  QrSDArea.Params[1].AsString  := FDataI;
  QrSDArea.Params[2].AsString  := FDataF;
  QrSDArea.Open;*)
  //
end;

end.

