unit TurmasGru;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, UnDmkProcFunc;

type
  TFmTurmasGru = class(TForm)
    PainelDados: TPanel;
    DsTurmasGru: TDataSource;
    QrTurmasGru: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Painel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrTurmasGruCodigo: TIntegerField;
    QrTurmasGruNome: TWideStringField;
    QrTurmasGruLk: TIntegerField;
    QrTurmasGruDataCad: TDateField;
    QrTurmasGruDataAlt: TDateField;
    QrTurmasGruUserCad: TIntegerField;
    QrTurmasGruUserAlt: TIntegerField;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTurmasGruAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrTurmasGruAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTurmasGruBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTurmasGru: TFmTurmasGru;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTurmasGru.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTurmasGru.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTurmasGruCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTurmasGru.DefParams;
begin
  VAR_GOTOTABELA := 'TurmasGru';
  VAR_GOTOMYSQLTABLE := QrTurmasGru;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM turmasgru');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTurmasGru.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
    end;
    EdNome.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmTurmasGru.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTurmasGru.AlteraRegistro;
var
  TurmasGru : Integer;
begin
  TurmasGru := QrTurmasGruCodigo.Value;
  if QrTurmasGruCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(TurmasGru, Dmod.MyDB, 'TurmasGru', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(TurmasGru, Dmod.MyDB, 'TurmasGru', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmTurmasGru.IncluiRegistro;
var
  Cursor : TCursor;
  TurmasGru : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    TurmasGru := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                   'TurmasGru', 'TurmasGru', 'Codigo');
    //
    if Length(FormatFloat(FFormatFloat, TurmasGru))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, TurmasGru);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmTurmasGru.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTurmasGru.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTurmasGru.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTurmasGru.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTurmasGru.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTurmasGru.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTurmasGru.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmTurmasGru.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmTurmasGru.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTurmasGruCodigo.Value;
  Close;
end;

procedure TFmTurmasGru.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, nil, 'Defina uma descri��o!') then Exit;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  Dmod.QrUpdU.SQL.Clear;
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO turmasgru SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE turmasgru SET ');
  //
  Dmod.QrUpdU.SQL.Add('Nome=:P0,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TurmasGru', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmTurmasGru.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if LaTipo.Caption = CO_INCLUSAO then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'TurmasGru', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TurmasGru', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TurmasGru', 'Codigo');
end;

procedure TFmTurmasGru.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmTurmasGru.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTurmasGruCodigo.Value,LaRegistro.Caption);
end;

procedure TFmTurmasGru.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmTurmasGru.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTurmasGru.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTurmasGru.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmTurmasGru.QrTurmasGruAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTurmasGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'TurmasGru', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmTurmasGru.QrTurmasGruAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrTurmasGruCodigo.Value, False);
end;

procedure TFmTurmasGru.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTurmasGruCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'TurmasGru', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTurmasGru.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTurmasGru.QrTurmasGruBeforeOpen(DataSet: TDataSet);
begin
  QrTurmasGruCodigo.DisplayFormat := FFormatFloat;
end;

end.

