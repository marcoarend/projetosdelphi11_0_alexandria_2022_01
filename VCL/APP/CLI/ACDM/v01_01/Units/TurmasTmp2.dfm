object FmTurmasTmp2: TFmTurmasTmp2
  Left = 422
  Top = 238
  Caption = 'TUR-CADAS-005 :: Per'#237'odos da Turma'
  ClientHeight = 852
  ClientWidth = 502
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 49
    Width = 502
    Height = 139
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 15
      Top = 5
      Width = 42
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Turma:'
    end
    object Label4: TLabel
      Left = 98
      Top = 5
      Width = 53
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Controle:'
    end
    object SpeedButton5: TSpeedButton
      Left = 182
      Top = 23
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      Visible = False
      OnClick = SpeedButton5Click
    end
    object EdCodigo: TdmkEdit
      Left = 15
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 98
      Top = 25
      Width = 80
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Color = clBtnFace
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object BtTodos: TBitBtn
      Tag = 127
      Left = 208
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Todos'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtTodosClick
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 321
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Nenhum'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtNenhumClick
    end
    object RGPeriodos: TRadioGroup
      Left = 1
      Top = 59
      Width = 500
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = ' Filtro por dia da semana: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Domingo'
        'Segunda'
        'Ter'#231'a'
        'Quarta'
        'Quinta'
        'Sexta'
        'S'#225'bado')
      TabOrder = 4
      OnClick = RGPeriodosClick
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 793
    Width = 502
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 17
      Top = 4
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel1: TPanel
      Left = 372
      Top = 1
      Width = 129
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 502
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Per'#237'odos da Turma'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 400
      Top = 1
      Width = 101
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 399
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object DBGrid1: TdmkDBGridDAC
    Left = 0
    Top = 188
    Width = 502
    Height = 605
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    SQLFieldsToChange.Strings = (
      'Ativo')
    SQLIndexesOnUpdate.Strings = (
      'Codigo')
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'Ativo'
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEDSEM'
        Title.Caption = 'Dia semana'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HIni'
        Title.Caption = 'Hora ini.'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HFim'
        Title.Caption = 'Hora fim'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o'
        Width = 150
        Visible = True
      end>
    Color = clWindow
    DataSource = DsPeriodos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SQLTable = 'Horarios0'
    EditForceNextYear = False
    Columns = <
      item
        Expanded = False
        FieldName = 'Ativo'
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEDSEM'
        Title.Caption = 'Dia semana'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HIni'
        Title.Caption = 'Hora ini.'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HFim'
        Title.Caption = 'Hora fim'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o'
        Width = 150
        Visible = True
      end>
  end
  object DsPeriodos: TDataSource
    DataSet = QrPeriodos
    Left = 32
    Top = 8
  end
  object QrPeriodos: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM Horarios0')
    Left = 4
    Top = 8
    object QrPeriodosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrPeriodosDSem: TIntegerField
      FieldName = 'DSem'
    end
    object QrPeriodosNOMEDSEM: TWideStringField
      FieldName = 'NOMEDSEM'
      Size = 13
    end
    object QrPeriodosHIni: TTimeField
      FieldName = 'HIni'
    end
    object QrPeriodosHFim: TTimeField
      FieldName = 'HFim'
    end
    object QrPeriodosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPeriodosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tt.Conta'
      'FROM turmastmp tt'
      'WHERE tt.Codigo=:P0'
      'AND tt.Controle=:P1'
      'AND tt.Periodo=:P2'
      ''
      '')
    Left = 60
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
end
