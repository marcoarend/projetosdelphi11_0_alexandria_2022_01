unit CreateApl;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (ntrtt_Tarifas);
  TAcaoCreate = (acDrop, acCreate, acFind);
  TAplCreate = class(TObject)

  private
    { Private declarations }
  public
    { Public declarations }
    function CriaSeNaoExisteTempTable(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
    function NomeTabela(const Tabela: TNomeTabRecriaTempTable;
             const NomeTab: String; var Nome: String): Boolean;
    function RecriaTempTable(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean;
             Repeticoes: Integer = 1; NomeTab: String = ''): String;
  end;

var
  AplCriar: TAplCreate;

implementation

uses UnMyObjects, Module, ModuleGeral;


{ TECreate }

function TAplCreate.CriaSeNaoExisteTempTable(Tabela: TNomeTabRecriaTempTable;
  Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes: Integer;
  NomeTab: String): String;
const
  Campo = 'Registros';
var
  Nome: String;
  Q: TmySQLQuery;
begin
  if not NomeTabela(Tabela, NomeTab, Nome) then
    Exit;
  Q := TmySQLQuery.Create(Dmod);
  try
    Q.DataBase := Qry.Database;
    //
    Q.SQL.Add('SHOW TABLES FROM ' + TmySQLDatabase(Qry.Database).DatabaseName);
    Q.SQL.Add('LIKE "' + LowerCase(Nome) + '"');
    Q.Open;
    //
    if Q.RecordCount = 0 then
      Result :=
        RecriaTempTable(Tabela, Qry, UniqueTableName, Repeticoes, Nome)
    else
      Result := Nome;
    //
    Q.Close;
  finally
    Q.Free;
  end;
end;

function TAplCreate.NomeTabela(const Tabela: TNomeTabRecriaTempTable;
const NomeTab: String; var Nome: String): Boolean;
begin
  Nome := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_Tarifas:           Nome := Lowercase('_Tarifas_');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Result := False;
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end else Result := True;
end;

function TAplCreate.RecriaTempTable(Tabela: TNomeTabRecriaTempTable;
  Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes: Integer;
  NomeTab: String): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
{
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_Tarifas:           Nome := Lowercase('_Tarifas_');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
}
  if not NomeTabela(Tabela, NomeTab, Nome) then
    Exit;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_Tarifas:
    begin
      Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  Nome                 varchar(100) NOT NULL                      ,');
      Qry.SQL.Add('  Ativa                tinyint(1)   NOT NULL                      ,');
      Qry.SQL.Add('  TipoPer              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  QtdePer              int(11)      NOT NULL                      ,');
      Qry.SQL.Add('  ValPer               double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Taxa                 double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Preco                double(15,2) NOT NULL                      ,');
      Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL                       ');
      Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci                   ');
      Qry.ExecSQL;
    end;
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.
