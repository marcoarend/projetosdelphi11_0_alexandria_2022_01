object FmTurmasImp: TFmTurmasImp
  Left = 499
  Top = 270
  Caption = 'TUR-PRINT-001 :: Lista de Turmas'
  ClientHeight = 218
  ClientWidth = 375
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 375
    Height = 130
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 100
      Width = 18
      Height = 13
      Caption = '>>>'
    end
    object RGOrdem: TRadioGroup
      Left = 1
      Top = 1
      Width = 373
      Height = 48
      Align = alTop
      Caption = ' Ordem: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Grupo'
        'Turma'
        'Instrutor')
      TabOrder = 0
    end
    object Progress1: TProgressBar
      Left = 1
      Top = 112
      Width = 373
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
    object RGSituacao: TRadioGroup
      Left = 1
      Top = 49
      Width = 373
      Height = 48
      Align = alTop
      Caption = ' Situa'#231#227'o da turma: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Ativa'
        'Inativa'
        'Ambos')
      TabOrder = 2
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 170
    Width = 375
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtCancela: TBitBtn
      Left = 269
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sair'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtCancelaClick
      NumGlyphs = 2
    end
    object BtImprime: TBitBtn
      Left = 13
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Preview do Relat'#243'rio'
      Caption = '&Imprimir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtImprimeClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 375
    Height = 40
    Align = alTop
    Caption = 'Lista de Turmas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 371
      Height = 36
      Align = alClient
      Transparent = True
      ExplicitWidth = 362
    end
  end
  object QrTurmas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTurmasCalcFields
    SQL.Strings = (
      'SELECT DISTINCT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMERESPONS, YEAR(tur.DataCad) ANO,'
      'tur.Nome, tur.ID, tui.Controle, tur.Lotacao, tur.Ativa,'
      'tug.Nome NOMEGRUPO'
      'FROM turmas tur '
      'LEFT JOIN turmasits tui ON tur.Codigo=tui.Codigo'
      'LEFT JOIN turmasgru tug ON tug.Codigo=tur.Grupo'
      'LEFT JOIN entidades ent ON ent.Codigo=tur.Respons'
      '')
    Left = 204
    Top = 80
    object QrTurmasNOMERESPONS: TWideStringField
      FieldName = 'NOMERESPONS'
      Size = 100
    end
    object QrTurmasANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrTurmasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTurmasID: TWideStringField
      FieldName = 'ID'
      Size = 30
    end
    object QrTurmasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTurmasLotacao: TIntegerField
      FieldName = 'Lotacao'
    end
    object QrTurmasAtiva: TSmallintField
      FieldName = 'Ativa'
    end
    object QrTurmasSituacao: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Situacao'
      Size = 30
      Calculated = True
    end
    object QrTurmasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object QrAlunos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tui.Controle,'
      '(COUNT(map.SubItem)+0.000) QtdAlunos, per.Dsem'
      'FROM matriper map'
      'LEFT JOIN matriren mar ON mar.Controle=map.Controle'
      'LEFT JOIN turmastmp ttm ON ttm.Conta=map.Periodo'
      'LEFT JOIN turmasits tui ON tui.Controle=ttm.Controle'
      'LEFT JOIN periodos  per ON per.Codigo=ttm.Periodo'
      'WHERE mar.Ativo=1'
      'GROUP BY tui.Controle, ttm.Conta')
    Left = 176
    Top = 80
    object QrAlunosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAlunosDsem: TIntegerField
      FieldName = 'Dsem'
    end
    object QrAlunosQtdAlunos: TFloatField
      FieldName = 'QtdAlunos'
      Required = True
    end
  end
  object QrTurmasT: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrTurmasTCalcFields
    SQL.Strings = (
      'SELECT * FROM turmast'
      ''
      '')
    Left = 232
    Top = 80
    object QrTurmasTControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTurmasTAno: TIntegerField
      FieldName = 'Ano'
    end
    object QrTurmasTRefere: TWideStringField
      FieldName = 'Refere'
      Size = 50
    end
    object QrTurmasTNomeTurma: TWideStringField
      FieldName = 'NomeTurma'
      Size = 50
    end
    object QrTurmasTNomeInstrutor: TWideStringField
      FieldName = 'NomeInstrutor'
      Size = 100
    end
    object QrTurmasTLotacao: TIntegerField
      FieldName = 'Lotacao'
    end
    object QrTurmasTDom: TIntegerField
      FieldName = 'Dom'
    end
    object QrTurmasTSeg: TIntegerField
      FieldName = 'Seg'
    end
    object QrTurmasTTer: TIntegerField
      FieldName = 'Ter'
    end
    object QrTurmasTQua: TIntegerField
      FieldName = 'Qua'
    end
    object QrTurmasTQui: TIntegerField
      FieldName = 'Qui'
    end
    object QrTurmasTSex: TIntegerField
      FieldName = 'Sex'
    end
    object QrTurmasTSab: TIntegerField
      FieldName = 'Sab'
    end
    object QrTurmasTSituacao: TWideStringField
      FieldName = 'Situacao'
    end
    object QrTurmasTNomeGrupo: TWideStringField
      FieldName = 'NomeGrupo'
      Size = 50
    end
    object QrTurmasTSdo_Dom: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Dom'
      Size = 21
      Calculated = True
    end
    object QrTurmasTSdo_Seg: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Seg'
      Size = 21
      Calculated = True
    end
    object QrTurmasTSdo_Ter: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Ter'
      Size = 21
      Calculated = True
    end
    object QrTurmasTSdo_Qua: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Qua'
      Size = 21
      Calculated = True
    end
    object QrTurmasTSdo_Qui: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Qui'
      Size = 21
      Calculated = True
    end
    object QrTurmasTSdo_Sex: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Sex'
      Size = 21
      Calculated = True
    end
    object QrTurmasTSdo_Sab: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Sdo_Sab'
      Size = 21
      Calculated = True
    end
  end
  object PMImprime: TPopupMenu
    Left = 160
    Top = 176
    object Matricial1: TMenuItem
      Caption = '&Matricial'
      OnClick = Matricial1Click
    end
    object TintaLaser1: TMenuItem
      Caption = '&Tinta/Laser'
      OnClick = TintaLaser1Click
    end
  end
  object frxDsTurmasT: TfrxDBDataset
    UserName = 'frxDsTurmasT'
    CloseDataSource = False
    DataSet = QrTurmasT
    BCDToCurrency = False
    Left = 288
    Top = 80
  end
  object frxTurmasT: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VAR_GRUPO> = 0 THEN Memo5.Memo.Text := '#39'[frxDsTurmasT."Nom' +
        'eGrupo"]'#39' else'
      
        '  if <VAR_GRUPO> = 1 THEN Memo5.Memo.Text := '#39'[frxDsTurmasT."Nom' +
        'eTurma"]'#39' else        '
      
        '  if <VAR_GRUPO> = 2 THEN Memo5.Memo.Text := '#39'[frxDsTurmasT."Nom' +
        'eInstrutor"]'#39';'
      '  //        '
      
        '  if <VAR_ORDEM> = 0 THEN GH1.Condition := '#39'frxDsTurmasT."NomeGr' +
        'upo"'#39' else           '
      
        '  if <VAR_ORDEM> = 1 THEN GH1.Condition := '#39'frxDsTurmasT."NomeTu' +
        'rma"'#39' else        '
      
        '  if <VAR_ORDEM> = 2 THEN GH1.Condition := '#39'frxDsTurmasT."NomeIn' +
        'strutor"'#39';'
      'end.')
    OnGetValue = frxTurmasTGetValue
    Left = 260
    Top = 80
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsTurmasT
        DataSetName = 'frxDsTurmasT'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader3: TfrxPageHeader
        Height = 69.921296460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo22: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'P'#195#161'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Visible = False
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'frxDsEmp."NO_EMPRESA"')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        Height = 42.582560000000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsTurmasT."NomeGrupo"'
        object Memo5: TfrxMemoView
          Top = 2.582560000000000000
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsTurmasT."NomeGrupo"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Top = 26.582560000000000000
          Width = 26.456692913385830000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Ano')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 26.456692913385830000
          Top = 26.582560000000000000
          Width = 75.590551181102360000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Refer'#195#170'ncia')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 102.047244094488200000
          Top = 26.582560000000000000
          Width = 151.181102362204700000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Descri'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 616.062992125984300000
          Top = 26.582560000000000000
          Width = 49.133858270000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Situa'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 589.606299212598400000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Sab')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 563.149606299212600000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Sex')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 536.692913385826800000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Qui')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 510.236220472440900000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Qua')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 483.779527559055100000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Ter')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 457.322834645669300000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Seg')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 430.866141732283500000
          Top = 26.582560000000000000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Dom')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 404.409448818897600000
          Top = 26.582560000000000000
          Width = 26.456692913385830000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'M'#195#161'x')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 253.228346456692900000
          Top = 26.582560000000000000
          Width = 151.181102362204700000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            'Instrutor')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        Height = 36.000000000000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        object Memo10: TfrxMemoView
          Top = 7.621830000000000000
          Width = 680.314960629921300000
          Height = 20.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Memo.UTF8 = (
            'SUB-TOTAL:  [COUNT(DadosMestre1)]')
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 16.000000000000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsTurmasT
        DataSetName = 'frxDsTurmasT'
        RowCount = 0
        object Memo11: TfrxMemoView
          Width = 26.456692913385830000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Ano'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsTurmasT."Ano"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 26.456692913385830000
          Width = 75.590551181102360000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Refere'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsTurmasT."Refere"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 102.047244094488200000
          Width = 151.181102362204700000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'NomeTurma'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsTurmasT."NomeTurma"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 616.062992125984300000
          Width = 49.133858270000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Situacao'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Situacao"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 589.606299212598400000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Sab'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Sab"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 563.149606299212600000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Sex'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Sex"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 536.692913385826800000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Qui'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Qui"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 510.236220472440900000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Qua'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Qua"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 483.779527559055100000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Ter'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Ter"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 457.322834645669300000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Seg'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Seg"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 430.866141732283500000
          Width = 26.456692910000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'Sdo_Dom'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsTurmasT."Sdo_Dom"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 404.409448818897600000
          Width = 26.456692913385830000
          Height = 16.000000000000000000
          ShowHint = False
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsTurmasT."Lotacao">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 253.228346456692900000
          Width = 151.181102362204700000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'NomeInstrutor'
          DataSet = frxDsTurmasT
          DataSetName = 'frxDsTurmasT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsTurmasT."NomeInstrutor"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 32.031230000000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          Top = 12.031230000000000000
          Width = 680.314960629921300000
          Height = 20.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Memo.UTF8 = (
            '       TOTAL:  [COUNT(DadosMestre1)]')
        end
      end
    end
  end
end
