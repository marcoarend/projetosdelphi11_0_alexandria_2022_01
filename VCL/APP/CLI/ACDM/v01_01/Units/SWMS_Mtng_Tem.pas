unit SWMS_Mtng_Tem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmSWMS_Mtng_Tem = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Label1: TLabel;
    EdTempo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirmou: Boolean;
  end;

  var
  FmSWMS_Mtng_Tem: TFmSWMS_Mtng_Tem;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmSWMS_Mtng_Tem.BtOKClick(Sender: TObject);
begin
  FConfirmou := True;
  Close;
end;

procedure TFmSWMS_Mtng_Tem.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSWMS_Mtng_Tem.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
  ImgTipo.SQLType := stUpd;
end;

procedure TFmSWMS_Mtng_Tem.FormCreate(Sender: TObject);
begin
  FConfirmou := False;
end;

procedure TFmSWMS_Mtng_Tem.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
