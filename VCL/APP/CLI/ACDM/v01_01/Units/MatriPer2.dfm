object FmMatriPer2: TFmMatriPer2
  Left = 422
  Top = 238
  Caption = 'MTR-CADAS-006 :: Per'#237'odos de Atividade'
  ClientHeight = 613
  ClientWidth = 966
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 49
    Width = 966
    Height = 61
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 5
      Top = 5
      Width = 57
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Matr'#237'cula:'
    end
    object Label4: TLabel
      Left = 69
      Top = 5
      Width = 74
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Renovac'#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 148
      Top = 25
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      Visible = False
      OnClick = SpeedButton1Click
    end
    object EdCodigo: TdmkEdit
      Left = 5
      Top = 25
      Width = 60
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdControle: TdmkEdit
      Left = 69
      Top = 25
      Width = 75
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object BtTodos: TBitBtn
      Tag = 127
      Left = 267
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Todos'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtTodosClick
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 380
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Nenhum'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtNenhumClick
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 554
    Width = 966
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 17
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel1: TPanel
      Left = 836
      Top = 1
      Width = 129
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 966
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taLeftJustify
    Caption = '     Per'#237'odos de Atividade'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 864
      Top = 1
      Width = 101
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 863
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object DBGrid1: TdmkDBGridDAC
    Left = 0
    Top = 110
    Width = 966
    Height = 444
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    SQLFieldsToChange.Strings = (
      'Ativo')
    SQLIndexesOnUpdate.Strings = (
      'Codigo'
      'Controle'
      'Conta'
      'Item'
      'Periodo')
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'Ativo'
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMETURMA'
        Title.Caption = 'Turma'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECURSO'
        Title.Caption = 'Atividade'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEDSEM'
        Title.Caption = 'Dia semana'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HIni'
        Title.Caption = 'Hora ini.'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HFim'
        Title.Caption = 'Hora fim'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o do hor'#225'rio'
        Width = 150
        Visible = True
      end>
    Color = clWindow
    DataSource = DsPeriodos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -14
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    SQLTable = 'Horarios1'
    EditForceNextYear = False
    Columns = <
      item
        Expanded = False
        FieldName = 'Ativo'
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMETURMA'
        Title.Caption = 'Turma'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECURSO'
        Title.Caption = 'Atividade'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMEDSEM'
        Title.Caption = 'Dia semana'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HIni'
        Title.Caption = 'Hora ini.'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HFim'
        Title.Caption = 'Hora fim'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o do hor'#225'rio'
        Width = 150
        Visible = True
      end>
  end
  object DsPeriodos: TDataSource
    DataSet = QrPeriodos
    Left = 364
    Top = 4
  end
  object QrPeriodos: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM Horarios1'
      '')
    Left = 336
    Top = 4
    object QrPeriodosAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrPeriodosCodigo: TIntegerField
      FieldName = 'Codigo'
      MaxValue = 1
    end
    object QrPeriodosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPeriodosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPeriodosItem: TIntegerField
      FieldName = 'Item'
    end
    object QrPeriodosSubItem: TIntegerField
      FieldName = 'SubItem'
    end
    object QrPeriodosTURMA: TIntegerField
      FieldName = 'TURMA'
    end
    object QrPeriodosTURMAITS: TIntegerField
      FieldName = 'TURMAITS'
    end
    object QrPeriodosCurso: TIntegerField
      FieldName = 'Curso'
    end
    object QrPeriodosNOMECURSO: TWideStringField
      FieldName = 'NOMECURSO'
      Size = 50
    end
    object QrPeriodosREFTURMAITS: TWideStringField
      FieldName = 'REFTURMAITS'
      Size = 30
    end
    object QrPeriodosNOMETURMA: TWideStringField
      FieldName = 'NOMETURMA'
      Size = 100
    end
    object QrPeriodosIDTURMA: TWideStringField
      FieldName = 'IDTURMA'
      Size = 30
    end
    object QrPeriodosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrPeriodosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPeriodosDSem: TIntegerField
      FieldName = 'DSem'
    end
    object QrPeriodosHIni: TTimeField
      FieldName = 'HIni'
    end
    object QrPeriodosHFim: TTimeField
      FieldName = 'HFim'
    end
    object QrPeriodosNOMEDSEM: TWideStringField
      FieldName = 'NOMEDSEM'
      Size = 7
    end
  end
end
