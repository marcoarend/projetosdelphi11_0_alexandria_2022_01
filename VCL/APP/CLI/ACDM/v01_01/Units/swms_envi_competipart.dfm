object FmSWMS_Envi_Competipart: TFmSWMS_Envi_Competipart
  Left = 339
  Top = 185
  Caption = 'MET-GEREN-005 :: Participante de Competi'#231#227'o'
  ClientHeight = 668
  ClientWidth = 759
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  GlassFrame.Left = 100
  GlassFrame.Top = 100
  GlassFrame.SheetOfGlass = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 489
    Width = 759
    Height = 38
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 5
    object CkContinuar: TCheckBox
      Left = 15
      Top = 5
      Width = 173
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object EdImportado: TdmkEdit
      Left = 172
      Top = 12
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Importado'
      UpdCampo = 'Importado'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNivel1: TdmkEdit
      Left = 246
      Top = 12
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nivel1'
      UpdCampo = 'Nivel1'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNivel2: TdmkEdit
      Left = 320
      Top = 12
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nivel2'
      UpdCampo = 'Nivel2'
      UpdType = utIdx
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 59
    Width = 759
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados da competi'#231#227'o: '
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 15
      Top = 30
      Width = 16
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 138
      Top = 25
      Width = 65
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label2: TLabel
      Left = 571
      Top = 30
      Width = 73
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ID inscri'#231#227'o:'
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 34
      Top = 22
      Width = 99
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmSWMS_Mtng_Cab.DsSWMS_Mtng_Cab
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 207
      Top = 22
      Width = 359
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      DataSource = FmSWMS_Mtng_Cab.DsSWMS_Mtng_Cab
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object EdControle: TdmkEdit
      Left = 650
      Top = 22
      Width = 98
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 114
    Width = 759
    Height = 50
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Dados do participante: '
    TabOrder = 1
    object Label1: TLabel
      Left = 15
      Top = 25
      Width = 130
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nadador participante:'
    end
    object SBNadador: TSpeedButton
      Left = 719
      Top = 17
      Width = 29
      Height = 30
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
    end
    object EdNadador: TdmkEditCB
      Left = 148
      Top = 17
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Nadador'
      UpdCampo = 'Nadador'
      UpdType = utYes
      Obrigatorio = True
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdNadadorChange
      DBLookupComboBox = CBNadador
      IgnoraDBLookupComboBox = False
    end
    object CBNadador: TdmkDBLookupComboBox
      Left = 217
      Top = 17
      Width = 498
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsNadadores
      NullValueKey = 16430
      ParentFont = False
      TabOrder = 1
      dmkEditCB = EdNadador
      QryCampo = 'Nadador'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 164
    Width = 759
    Height = 156
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Configura'#231#227'o da prova:'
    TabOrder = 2
    object RGEstilo: TdmkRadioGroup
      Left = 2
      Top = 18
      Width = 755
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Estilo: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        #39'Nenhum'#39','
        #39'Livre'#39','
        #39'Costas'#39','
        #39'Peito'#39','
        #39'Borboleta'#39','
        #39'Medley'#39','
        #39'Revezam. livre'#39','
        #39'Revezam. estilos'#39)
      TabOrder = 0
      QryCampo = 'Estilo'
      UpdCampo = 'Estilo'
      UpdType = utYes
      OldValor = 0
    end
    object RGMetros: TdmkRadioGroup
      Left = 2
      Top = 86
      Width = 755
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Dist'#226'ncia em metros: '
      Columns = 9
      ItemIndex = 0
      Items.Strings = (
        #39'0'#39','
        #39'1 x 20'#39','
        #39'1 x 25'#39','
        #39'1 x 40'#39','
        #39'1 x 50'#39','
        #39'1 x 80'#39','
        #39'1 x 100'#39','
        #39'1 x 200'#39','
        #39'1 x 400'#39','
        #39'1 x 800'#39','
        #39'1 x 1500'#39','
        #39'4 x 20'#39','
        #39'4 x 25'#39','
        #39'4 x 40'#39','
        #39'4 x 50'#39','
        #39'4 x 80'#39','
        #39'4 x 100'#39','
        #39'4 x 200'#39)
      TabOrder = 1
      QryCampo = 'Metros'
      UpdCampo = 'Metros'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnTitulo: TPanel
    Left = 0
    Top = 0
    Width = 759
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 8
    object GB_R: TGroupBox
      Left = 700
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 641
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 392
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Participante de Competi'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 392
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Participante de Competi'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 392
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Caption = 'Participante de Competi'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 320
    Width = 759
    Height = 41
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    TabOrder = 3
    object Label6: TLabel
      Left = 10
      Top = 15
      Width = 437
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 
        'Tempo do competidor na configura'#231#227'o da prova x dist'#226'ncia selecio' +
        'nada:'
    end
    object EdTempo: TdmkEdit
      Left = 537
      Top = 7
      Width = 109
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfExtended
      Texto = '00:00:00,000'
      QryCampo = 'Tempo'
      UpdCampo = 'Tempo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 597
    Width = 759
    Height = 71
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 6
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 15
      Width = 147
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 588
      Top = 15
      Width = 148
      Height = 49
      Cursor = crHandPoint
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Fechar'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 527
    Width = 759
    Height = 70
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 7
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 755
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object RGCategoria: TdmkRadioGroup
    Left = 0
    Top = 361
    Width = 759
    Height = 128
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Categoria: '
    Columns = 6
    ItemIndex = 0
    Items.Strings = (
      #39'Nenhum'#39
      #39'1'#39
      #39'2'#39
      #39'3'#39
      #39'4'#39
      #39'Mamadeira'#39
      #39'Pr'#233'-fraldinha'#39
      #39'Fraldinha'#39
      #39'Pr'#233'-mirim'#39
      #39'Mirin I'#39
      #39'Mirin II'#39
      #39'Petiz I'#39
      #39'Petiz II'#39
      #39'Infantil I'#39
      #39'Infantil II'#39
      #39'Juvenil I'#39
      #39'Juvenil II'#39
      #39'Junior I'#39
      #39'Junior II'#39
      #39'Senior'#39
      #39'Pr'#233'-master A'#39
      #39'Master B'#39
      #39'Master C'#39
      #39'Master D'#39
      #39'Master E'#39
      #39'Master F'#39
      #39'Master G'#39
      #39'Master H'#39
      #39'Master I'#39
      #39'A definir'#39)
    TabOrder = 4
    QryCampo = 'Categoria'
    UpdCampo = 'Categoria'
    UpdType = utYes
    OldValor = 0
  end
  object QrNadadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM swms_envi_nadadores'
      'WHERE Nivel1=:P0'
      'AND Nivel2=:P1'
      'ORDER BY Nome')
    Left = 148
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNadadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNadadoresCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 60
    end
    object QrNadadoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNadadoresNascim: TDateField
      FieldName = 'Nascim'
    end
    object QrNadadoresSexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object QrNadadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrNadadoresFone: TWideStringField
      FieldName = 'Fone'
    end
  end
  object DsNadadores: TDataSource
    DataSet = QrNadadores
    Left = 176
    Top = 128
  end
end
