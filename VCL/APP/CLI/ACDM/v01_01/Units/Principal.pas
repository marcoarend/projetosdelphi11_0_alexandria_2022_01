unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinStore, WinSkinData, UnMsgInt, Mask, DBCtrls,
  MyAppConsts, frxClass, frxDBSet, UnFinanceiro, dmkGeral, Variants, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, ShellApi, UnDmkProcFunc, dmkEditDateTimePicker,
  dmkDBGrid, AdvToolBar, AdvShapeButton, AdvGlowButton, AdvPreviewMenu,
  AdvToolBarStylers, AdvOfficeHint, AdvMenus, Vcl.Imaging.pngimage, UnDmkEnums,
  dmkMemo;

type
  TcpCalc = (cpJurosMes, cpMulta);
  TFmPrincipal = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Ferramentas1: TMenuItem;
    Opes1: TMenuItem;
    Timer1: TTimer;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    Turmas1: TMenuItem;
    Cursos2: TMenuItem;
    Salas2: TMenuItem;
    PMCheques: TPopupMenu;
    Apenasum1: TMenuItem;
    Vrios1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ImgPrincipal: TImage;
    TabSheet2: TTabSheet;
    PaxTurmas: TPanel;
    DBGTurmas: TdmkDBGrid;
    GroupBox1: TGroupBox;
    CkTurmaDes: TCheckBox;
    CkTurmaEmF: TCheckBox;
    CkTurmaAti: TCheckBox;
    QrTurmas: TmySQLQuery;
    QrTurmasCodigo: TIntegerField;
    QrTurmasNome: TWideStringField;
    QrTurmasID: TWideStringField;
    QrTurmasAtiva: TSmallintField;
    QrTurmasLk: TIntegerField;
    QrTurmasDataCad: TDateField;
    QrTurmasDataAlt: TDateField;
    QrTurmasUserCad: TIntegerField;
    QrTurmasUserAlt: TIntegerField;
    DsTurmas: TDataSource;
    QrTurmasNOMEATIVA: TWideStringField;
    Panel3: TPanel;
    DsMatriculados: TDataSource;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel4: TPanel;
    Panel5: TPanel;
    CkAbertos: TCheckBox;
    Label2: TLabel;
    QrTurMatriPagtos: TmySQLQuery;
    DsTurMatriPagtos: TDataSource;
    QrTurMatriPagtosVencimento: TDateField;
    QrTurMatriPagtosCredito: TFloatField;
    QrTurMatriPagtosCompensado: TDateField;
    QrTurMatriPagtosDescricao: TWideStringField;
    QrTurMatriPagtosNOMECARTEIRA: TWideStringField;
    QrTurMatriPagtosCARTEIRATIPO: TIntegerField;
    QrTurMatriPagtosCOMPENSADOREAL: TWideStringField;
    QrTurMatriPagtosSit: TIntegerField;
    QrTurMatriPagtosNOMESIT: TWideStringField;
    QrTurMatriPagtosSALDO: TFloatField;
    QrTurMatriPagtosDebito: TFloatField;
    QrTurMatriPagtosNOMETIPO: TWideStringField;
    QrTurMatriPagtosTipo: TSmallintField;
    QrTurMatriPagtosPago: TFloatField;
    QrTurMatriPagtosVENCIDO: TDateField;
    QrTurMatriPagtosATRAZODD: TFloatField;
    QrTurMatriPagtosMoraDia: TFloatField;
    QrTurMatriPagtosMulta: TFloatField;
    GridPagtos: TDBGrid;
    CkQuitados: TCheckBox;
    QrTurMatriPagtosFatParcela: TIntegerField;
    QrTurMatriPagtosDocumento: TFloatField;
    QrTurMatriPagtosControle: TIntegerField;
    QrTurMatriPagtosCtrlIni: TIntegerField;
    QrTurMatriPagtosSub: TSmallintField;
    QrLoclancto: TmySQLQuery;
    QrLoclanctoData: TDateField;
    QrLoclanctoTipo: TSmallintField;
    QrLoclanctoCarteira: TIntegerField;
    QrLoclanctoControle: TIntegerField;
    QrLoclanctoSub: TSmallintField;
    QrLoclanctoAutorizacao: TIntegerField;
    QrLoclanctoGenero: TIntegerField;
    QrLoclanctoDescricao: TWideStringField;
    QrLoclanctoNotaFiscal: TIntegerField;
    QrLoclanctoDebito: TFloatField;
    QrLoclanctoCredito: TFloatField;
    QrLoclanctoCompensado: TDateField;
    QrLoclanctoDocumento: TFloatField;
    QrLoclanctoSit: TIntegerField;
    QrLoclanctoVencimento: TDateField;
    QrLoclanctoFatID: TIntegerField;
    QrLoclanctoFatID_Sub: TIntegerField;
    QrLoclanctoFatParcela: TIntegerField;
    QrLoclanctoID_Pgto: TIntegerField;
    QrLoclanctoID_Sub: TSmallintField;
    QrLoclanctoFatura: TWideStringField;
    QrLoclanctoBanco: TIntegerField;
    QrLoclanctoLocal: TIntegerField;
    QrLoclanctoCartao: TIntegerField;
    QrLoclanctoLinha: TIntegerField;
    QrLoclanctoOperCount: TIntegerField;
    QrLoclanctoLancto: TIntegerField;
    QrLoclanctoPago: TFloatField;
    QrLoclanctoMez: TIntegerField;
    QrLoclanctoFornecedor: TIntegerField;
    QrLoclanctoCliente: TIntegerField;
    QrLoclanctoMoraDia: TFloatField;
    QrLoclanctoMulta: TFloatField;
    QrLoclanctoProtesto: TDateField;
    QrLoclanctoDataDoc: TDateField;
    QrLoclanctoCtrlIni: TIntegerField;
    QrLoclanctoNivel: TIntegerField;
    QrLoclanctoVendedor: TIntegerField;
    QrLoclanctoAccount: TIntegerField;
    QrLoclanctoLk: TIntegerField;
    QrLoclanctoDataCad: TDateField;
    QrLoclanctoDataAlt: TDateField;
    QrLoclanctoUserCad: TIntegerField;
    QrLoclanctoUserAlt: TIntegerField;
    QrTurMatriPagtosCarteira: TIntegerField;
    QrTurmasIts: TmySQLQuery;
    QrTurmasItsCodigo: TIntegerField;
    QrTurmasItsControle: TIntegerField;
    QrTurmasItsSala: TIntegerField;
    QrTurmasItsCurso: TIntegerField;
    QrTurmasItsProfessor: TIntegerField;
    QrTurmasItsLk: TIntegerField;
    QrTurmasItsDataCad: TDateField;
    QrTurmasItsDataAlt: TDateField;
    QrTurmasItsUserCad: TIntegerField;
    QrTurmasItsUserAlt: TIntegerField;
    QrTurmasItsNOMESALA: TWideStringField;
    QrTurmasItsNOMECURSO: TWideStringField;
    QrTurmasItsNOMEPROFESSOR: TWideStringField;
    DsTurmasIts: TDataSource;
    DsTurmasTmp: TDataSource;
    QrTurmasTmp: TmySQLQuery;
    QrTurmasTmpDIASEMANA: TWideStringField;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    PaxTurmasMatriculados: TPanel;
    Panel7: TPanel;
    DBGMatriculados: TdmkDBGrid;
    BtLista1: TBitBtn;
    BtEntidades2_1: TBitBtn;
    BtImprime1: TBitBtn;
    TabSheet5: TTabSheet;
    PaxCursos: TPanel;
    Panel9: TPanel;
    QrCursos: TmySQLQuery;
    DsCursos: TDataSource;
    GroupBox2: TGroupBox;
    CkCursoDes: TCheckBox;
    CkCursoAti: TCheckBox;
    DBGrid5: TDBGrid;
    QrCursosCodigo: TIntegerField;
    QrCursosNome: TWideStringField;
    QrCursosAtivo: TIntegerField;
    PageControl3: TPageControl;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Minhasconfiguraes1: TMenuItem;
    Aplicar1: TMenuItem;
    QrMatriculados: TmySQLQuery;
    PmAlunos1: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    Desativa1: TMenuItem;
    QrCertificados: TmySQLQuery;
    QrCertificadosNome_da_Turma: TWideStringField;
    QrCertificadosNome_do_Curso: TWideStringField;
    QrCertificadosNome_do_Aluno: TWideStringField;
    QrCertificadosNome_do_Mes: TWideStringField;
    QrCertificadosDia: TLargeintField;
    QrCertificadosMes: TLargeintField;
    QrCertificadosAno: TLargeintField;
    TabSheet12: TTabSheet;
    Panel2: TPanel;
    Panel6: TPanel;
    CkAlunosAtivos: TCheckBox;
    DBGrid10: TDBGrid;
    QrAlunos: TmySQLQuery;
    DsAlunos: TDataSource;
    CkAlunosInativos: TCheckBox;
    PageControl5: TPageControl;
    TabSheet13: TTabSheet;
    TabSheet15: TTabSheet;
    QrAluTurmas: TmySQLQuery;
    DsAluTurmas: TDataSource;
    QrAluTurmasTurma: TIntegerField;
    QrAluTurmasNOMETURMA: TWideStringField;
    QrAluTurMatric: TmySQLQuery;
    DsAluTurMatric: TDataSource;
    QrAluTurMatricSala: TIntegerField;
    QrAluTurMatricCurso: TIntegerField;
    QrAluTurMatricProfessor: TIntegerField;
    QrAluTurMatricCarga: TFloatField;
    QrAluTurMatricNOMECURSO: TWideStringField;
    QrAluTurMatricNOMESALA: TWideStringField;
    QrAluTurMatricNOMEPROFESSOR: TWideStringField;
    QrAluTurmasMATRICULA: TIntegerField;
    Splitter4: TSplitter;
    PMExcluir: TPopupMenu;
    Retirarturmadamatrcula1: TMenuItem;
    Excluirtodamatrcula1: TMenuItem;
    QrAluPagtos: TmySQLQuery;
    DsAluPagtos: TDataSource;
    TabSheet14: TTabSheet;
    Panel14: TPanel;
    DBGrid13: TDBGrid;
    Panel15: TPanel;
    BtExclui: TBitBtn;
    Splitter5: TSplitter;
    PageControl7: TPageControl;
    TabSheet18: TTabSheet;
    DBGrid14: TDBGrid;
    QrAluPagtosVencimento: TDateField;
    QrAluPagtosCredito: TFloatField;
    QrAluPagtosDebito: TFloatField;
    QrAluPagtosCompensado: TDateField;
    QrAluPagtosDescricao: TWideStringField;
    QrAluPagtosSit: TIntegerField;
    QrAluPagtosTipo: TSmallintField;
    QrAluPagtosPago: TFloatField;
    QrAluPagtosMoraDia: TFloatField;
    QrAluPagtosMulta: TFloatField;
    QrAluPagtosFatParcela: TIntegerField;
    QrAluPagtosDocumento: TFloatField;
    QrAluPagtosControle: TIntegerField;
    QrAluPagtosCtrlIni: TIntegerField;
    QrAluPagtosSub: TSmallintField;
    QrAluPagtosCarteira: TIntegerField;
    QrAluPagtosNOMECARTEIRA: TWideStringField;
    QrAluPagtosCARTEIRATIPO: TIntegerField;
    QrAluPagtosCOMPENSADOREAL: TWideStringField;
    QrAluPagtosNOMESIT: TWideStringField;
    QrAluPagtosSALDO: TFloatField;
    QrAluPagtosNOMETIPO: TWideStringField;
    QrAluPagtosVENCIDO: TDateField;
    QrAluPagtosATRAZODD: TFloatField;
    QrTurMatriPagtosATUALIZADO: TFloatField;
    QrAluPagtosATUALIZADO: TFloatField;
    QrAluCursos: TmySQLQuery;
    DsAluCursos: TDataSource;
    QrAluCursosMATRICULA: TIntegerField;
    QrAluCursosSala: TIntegerField;
    QrAluCursosCurso: TIntegerField;
    QrAluCursosProfessor: TIntegerField;
    QrAluCursosCarga: TFloatField;
    QrAluCursosNOMECURSO: TWideStringField;
    QrAluCursosNOMESALA: TWideStringField;
    QrAluCursosNOMEPROFESSOR: TWideStringField;
    Panel8: TPanel;
    DBGrid12: TDBGrid;
    QrAluCursosNOMETURMA: TWideStringField;
    Resetar21: TMenuItem;
    TabSheet17: TTabSheet;
    StringGrid1: TStringGrid;
    QrHoje: TmySQLQuery;
    DsHoje: TDataSource;
    Timer3: TTimer;
    Painelx: TPanel;
    BtRefresh: TBitBtn;
    EdAtz: TEdit;
    DsHojeAlunos: TDataSource;
    QrHojeAlunos: TmySQLQuery;
    BtLista2: TBitBtn;
    Panel18: TPanel;
    PageControl6: TPageControl;
    TabSheet16: TTabSheet;
    DBGrid11: TDBGrid;
    TabSheet19: TTabSheet;
    Panel19: TPanel;
    Label7: TLabel;
    CkAluPagtosA: TCheckBox;
    CkAluPagtosQ: TCheckBox;
    BtQuita2: TBitBtn;
    BtImprime3: TBitBtn;
    QrAluPgTot: TmySQLQuery;
    DsAluPgTot: TDataSource;
    DBGAluPgTot: TDBGrid;
    QrAluPgTotCOMPENSADOREAL: TWideStringField;
    QrAluPgTotNOMESIT: TWideStringField;
    QrAluPgTotSALDO: TFloatField;
    QrAluPgTotNOMETIPO: TWideStringField;
    QrAluPgTotVENCIDO: TDateField;
    QrAluPgTotATRAZODD: TFloatField;
    QrAluPgTotVencimento: TDateField;
    QrAluPgTotCredito: TFloatField;
    QrAluPgTotDebito: TFloatField;
    QrAluPgTotCompensado: TDateField;
    QrAluPgTotDescricao: TWideStringField;
    QrAluPgTotSit: TIntegerField;
    QrAluPgTotTipo: TSmallintField;
    QrAluPgTotPago: TFloatField;
    QrAluPgTotMoraDia: TFloatField;
    QrAluPgTotMulta: TFloatField;
    QrAluPgTotFatParcela: TIntegerField;
    QrAluPgTotDocumento: TFloatField;
    QrAluPgTotControle: TIntegerField;
    QrAluPgTotCtrlIni: TIntegerField;
    QrAluPgTotSub: TSmallintField;
    QrAluPgTotCarteira: TIntegerField;
    QrAluPgTotNOMECARTEIRA: TWideStringField;
    QrAluPgTotCARTEIRATIPO: TIntegerField;
    QrAluPgTotATUALIZADO: TFloatField;
    CkAluPagtosV: TCheckBox;
    QrAluPgTotNOMEALUNO: TWideStringField;
    PMQuita2: TPopupMenu;
    Panel20: TPanel;
    RGAluPgTotOrdem: TRadioGroup;
    TabSheet20: TTabSheet;
    Panel21: TPanel;
    TPAPPIni: TdmkEditDateTimePicker;
    TPAPPFim: TdmkEditDateTimePicker;
    Label8: TLabel;
    Label9: TLabel;
    QrAluPgPer: TmySQLQuery;
    DsAluPgPer: TDataSource;
    DBGrid17: TDBGrid;
    QrAluPgPerCOMPENSADOREAL: TWideStringField;
    QrAluPgPerNOMESIT: TWideStringField;
    QrAluPgPerNOMETIPO: TWideStringField;
    QrAluPgPerVENCIDO: TDateField;
    QrAluPgPerATRAZODD: TFloatField;
    QrAluPgPerVencimento: TDateField;
    QrAluPgPerCredito: TFloatField;
    QrAluPgPerDebito: TFloatField;
    QrAluPgPerCompensado: TDateField;
    QrAluPgPerDescricao: TWideStringField;
    QrAluPgPerSit: TIntegerField;
    QrAluPgPerTipo: TSmallintField;
    QrAluPgPerPago: TFloatField;
    QrAluPgPerMoraDia: TFloatField;
    QrAluPgPerMulta: TFloatField;
    QrAluPgPerFatParcela: TIntegerField;
    QrAluPgPerDocumento: TFloatField;
    QrAluPgPerControle: TIntegerField;
    QrAluPgPerCtrlIni: TIntegerField;
    QrAluPgPerSub: TSmallintField;
    QrAluPgPerCarteira: TIntegerField;
    QrAluPgPerNOMECARTEIRA: TWideStringField;
    QrAluPgPerCARTEIRATIPO: TIntegerField;
    QrAluPgPerATUALIZADO: TFloatField;
    QrAluPgPerSALDO: TFloatField;
    QrAluPgPerNOMEALUNO: TWideStringField;
    PMAluPagPer: TPopupMenu;
    Lista1: TMenuItem;
    Avisos1: TMenuItem;
    QrAluPgPerNOMEORIENTADOR: TWideStringField;
    QrAluPgPerSEXOA: TWideStringField;
    QrAluPgPerVALOR_TXT: TWideStringField;
    QrAluPgPerACRESCIMOS: TWideStringField;
    BtMatricula2: TBitBtn;
    QrAluPgPerVENCER_TXT: TWideStringField;
    TabSheet21: TTabSheet;
    Panel22: TPanel;
    DBGrid18: TDBGrid;
    QrAluMatri: TmySQLQuery;
    DsAluMatri: TDataSource;
    BtMatricula3: TBitBtn;
    CkAluMatriInati: TCheckBox;
    CkAluMatriAtivo: TCheckBox;
    Label10: TLabel;
    EdAluno: TdmkEdit;
    TPAula: TdmkEditDateTimePicker;
    Label11: TLabel;
    BtImprime4: TBitBtn;
    Panel13: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    GradeHoje: TDBGrid;
    QrHojeCham: TmySQLQuery;
    DsHojeCham: TDataSource;
    QrHojeChamCodigo: TIntegerField;
    QrHojeChamTurma: TIntegerField;
    QrHojeChamData: TDateField;
    QrHojeChamHoraI: TTimeField;
    QrHojeChamHoraF: TTimeField;
    QrHojeChamIntervI: TTimeField;
    QrHojeChamIntervF: TTimeField;
    QrHojeChamLk: TIntegerField;
    QrHojeChamDataCad: TDateField;
    QrHojeChamDataAlt: TDateField;
    QrHojeChamUserCad: TIntegerField;
    QrHojeChamUserAlt: TIntegerField;
    QrHojeChamIts: TmySQLQuery;
    DsHojeChamIts: TDataSource;
    QrHojeChamItsMatricula: TIntegerField;
    QrHojeChamItsNOMEALUNO: TWideStringField;
    QrHojeChamItsPresente: TIntegerField;
    Splitter3: TSplitter;
    Panel24: TPanel;
    DBGrid20: TDBGrid;
    QrAluMatriCursos: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    DateField3: TDateField;
    FloatField1: TFloatField;
    SmallintField1: TSmallintField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField1: TWideStringField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    DateField4: TDateField;
    DateField5: TDateField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    SmallintField2: TSmallintField;
    StringField2: TWideStringField;
    IntegerField9: TIntegerField;
    StringField3: TWideStringField;
    DsAluMatricursos: TDataSource;
    Panel26: TPanel;
    QrAlunos2: TmySQLQuery;
    DsAlunos2: TDataSource;
    QrAlunos2SEQ: TIntegerField;
    QrHojeChamCarga: TIntegerField;
    QrHojeChamCurso: TIntegerField;
    QrHojeChamSala: TIntegerField;
    QrHojeChamProfessor: TIntegerField;
    QrHojeChamItsPresenca: TIntegerField;
    Panel25: TPanel;
    DBGrid21: TDBGrid;
    Panel27: TPanel;
    BtChamada: TBitBtn;
    BtExclui1: TBitBtn;
    Panel28: TPanel;
    DBGrid15: TDBGrid;
    BtExclui2: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    QrHojeChamItsALUNO: TIntegerField;
    Panelx: TPanel;
    Departamentos2: TMenuItem;
    QrHojeChamNOMESALA: TWideStringField;
    QrHojeChamNOMEPROFESSOR: TWideStringField;
    N1: TMenuItem;
    Opes2: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    Contas2: TMenuItem;
    QrCNC: TmySQLQuery;
    QrCNCCodigo: TIntegerField;
    QrCNCCursoMat: TIntegerField;
    QrCNCTurma: TIntegerField;
    QrCNCCursoTur: TIntegerField;
    QrMAT: TmySQLQuery;
    QrMATCodigo: TIntegerField;
    QrSomaMAT: TmySQLQuery;
    QrSomaMATFator: TFloatField;
    StatusBar: TStatusBar;
    Perodos1: TMenuItem;
    relatrios1: TMenuItem;
    ListadeTurmas1: TMenuItem;
    QrAlunosAtivos: TmySQLQuery;
    QrMatriculadosNOMEALUNO: TWideStringField;
    QrMatriculadosAluno: TIntegerField;
    QrMatriculadosMATRICULA: TIntegerField;
    QrMatriculadosSEQ: TIntegerField;
    QrMatriculadosTurma: TIntegerField;
    QrTurmasTmpCodigo: TIntegerField;
    QrTurmasTmpControle: TIntegerField;
    QrTurmasTmpConta: TIntegerField;
    QrTurmasTmpPeriodo: TIntegerField;
    QrTurmasTmpLk: TIntegerField;
    QrTurmasTmpDataCad: TDateField;
    QrTurmasTmpDataAlt: TDateField;
    QrTurmasTmpUserCad: TIntegerField;
    QrTurmasTmpUserAlt: TIntegerField;
    QrTurmasTmpCodigo_1: TIntegerField;
    QrTurmasTmpNome: TWideStringField;
    QrTurmasTmpDSem: TIntegerField;
    QrTurmasTmpHIni: TTimeField;
    QrTurmasTmpHFim: TTimeField;
    QrTurmasTmpLk_1: TIntegerField;
    QrTurmasTmpDataCad_1: TDateField;
    QrTurmasTmpDataAlt_1: TDateField;
    QrTurmasTmpUserCad_1: TIntegerField;
    QrTurmasTmpUserAlt_1: TIntegerField;
    QrAlunos2Aluno: TIntegerField;
    QrAlunos2NOMEALUNO: TWideStringField;
    QrAluTurMatricMATRICULA: TIntegerField;
    QrAlunosAtivosQtdAlunos: TFloatField;
    QrMatriculado: TmySQLQuery;
    QrMatriculadoCliente1: TWideStringField;
    QrMatriculadoCodigo: TIntegerField;
    QrMatriculasAtivas: TmySQLQuery;
    QrMatriculasAtivasQtdMatriculas: TFloatField;
    Gruposde1: TMenuItem;
    TabSheet6: TTabSheet;
    QrMensVencidas: TmySQLQuery;
    DBGMensVencidas: TdmkDBGrid;
    Panel10: TPanel;
    BtRefresh1: TBitBtn;
    Label1: TLabel;
    TPMensVence: TdmkEditDateTimePicker;
    DsMensVencidas: TDataSource;
    QrMensVencidasNOMEALUNO: TWideStringField;
    QrMensVencidasCodigo: TIntegerField;
    QrMensVencidasControle: TIntegerField;
    QrMensVencidasAtivo: TIntegerField;
    QrMensVencidasDataI: TDateField;
    QrMensVencidasDataF: TDateField;
    QrMensVencidasValor: TFloatField;
    QrMensVencidasTotalFator: TFloatField;
    QrMensVencidasLk: TIntegerField;
    QrMensVencidasDataCad: TDateField;
    QrMensVencidasDataAlt: TDateField;
    QrMensVencidasUserCad: TIntegerField;
    QrMensVencidasUserAlt: TIntegerField;
    QrMensVencidasMESES: TLargeintField;
    QrAlunosAluno: TIntegerField;
    QrAlunosNOMEALUNO: TWideStringField;
    QrAlunosSEQ: TIntegerField;
    QrAluMatriCodigo: TIntegerField;
    QrAluMatriAluno: TIntegerField;
    QrAluMatriOrientador: TIntegerField;
    QrAluMatriDataC: TDateField;
    QrAluMatriContrato: TWideStringField;
    QrAluMatriAtivo: TSmallintField;
    QrAluMatriLk: TIntegerField;
    QrAluMatriDataCad: TDateField;
    QrAluMatriDataAlt: TDateField;
    QrAluMatriUserCad: TIntegerField;
    QrAluMatriUserAlt: TIntegerField;
    QrAluMatriNOMEORIENTADOR: TWideStringField;
    QrHojeTURMA: TIntegerField;
    QrHojeNOMETURMA: TWideStringField;
    QrHojeSala: TIntegerField;
    QrHojeCurso: TIntegerField;
    QrHojeProfessor: TIntegerField;
    QrHojeNOMESALA: TWideStringField;
    QrHojeNOMECURSO: TWideStringField;
    QrHojeNOMEPROFESSOR: TWideStringField;
    QrHojeAtiva: TSmallintField;
    QrHojeCodigo: TIntegerField;
    QrHojeControle: TIntegerField;
    QrHojeDsem: TIntegerField;
    QrHojeHIni: TTimeField;
    QrHojeHFim: TTimeField;
    QrHojeAlunosNOMEALUNO: TWideStringField;
    QrHojeAlunosAluno: TIntegerField;
    QrMensVencidasALUNO: TIntegerField;
    BtMatricula4: TBitBtn;
    TabSheet7: TTabSheet;
    Panel11: TPanel;
    Panel31: TPanel;
    DBGrid7: TDBGrid;
    QrCliProd: TmySQLQuery;
    DsCliProd: TDataSource;
    BtAtualiza: TBitBtn;
    Label3: TLabel;
    EdLocCli: TEdit;
    QrCliProdCodigo: TIntegerField;
    QrCliProdNOMECLIENTE: TWideStringField;
    QrCliProdSEQ: TIntegerField;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    DBGrid19: TDBGrid;
    Panel36: TPanel;
    BtMercadoria1: TBitBtn;
    BtDinheiro: TBitBtn;
    QrCliVen: TmySQLQuery;
    DsCliVen: TDataSource;
    QrCliVenCodigo: TLargeintField;
    QrCliVenControle: TLargeintField;
    QrCliVenProduto: TIntegerField;
    QrCliVenQtde: TFloatField;
    QrCliVenPreco: TFloatField;
    QrCliVenValorReal: TFloatField;
    QrCliVenCusto: TFloatField;
    QrCliVenLk: TIntegerField;
    QrCliVenDataCad: TDateField;
    QrCliVenDataAlt: TDateField;
    QrCliVenUserCad: TIntegerField;
    QrCliVenUserAlt: TIntegerField;
    QrCliVenCliente: TIntegerField;
    QrCliVenNOMEMERCADORIA: TWideStringField;
    QrCliPag: TmySQLQuery;
    QrCliPagData: TDateField;
    QrCliPagTipo: TSmallintField;
    QrCliPagCarteira: TIntegerField;
    QrCliPagAutorizacao: TIntegerField;
    QrCliPagGenero: TIntegerField;
    QrCliPagDescricao: TWideStringField;
    QrCliPagNotaFiscal: TIntegerField;
    QrCliPagDebito: TFloatField;
    QrCliPagCredito: TFloatField;
    QrCliPagCompensado: TDateField;
    QrCliPagDocumento: TFloatField;
    QrCliPagSit: TIntegerField;
    QrCliPagVencimento: TDateField;
    QrCliPagLk: TIntegerField;
    QrCliPagFatID: TIntegerField;
    QrCliPagFatParcela: TIntegerField;
    QrCliPagNOMESIT: TWideStringField;
    QrCliPagNOMETIPO: TWideStringField;
    QrCliPagNOMECARTEIRA: TWideStringField;
    QrCliPagID_Sub: TSmallintField;
    QrCliPagSub: TIntegerField;
    QrCliPagFatura: TWideStringField;
    QrCliPagBanco: TIntegerField;
    QrCliPagLocal: TIntegerField;
    QrCliPagCartao: TIntegerField;
    QrCliPagLinha: TIntegerField;
    QrCliPagPago: TFloatField;
    QrCliPagMez: TIntegerField;
    QrCliPagFornecedor: TIntegerField;
    QrCliPagCliente: TIntegerField;
    QrCliPagControle: TIntegerField;
    QrCliPagID_Pgto: TIntegerField;
    DsCliPag: TDataSource;
    GridF: TDBGrid;
    Tarifas2: TMenuItem;
    Recibo1: TMenuItem;
    TabSheet8: TTabSheet;
    Panel37: TPanel;
    Label4: TLabel;
    TPCaixaIni: TdmkEditDateTimePicker;
    BtRefresh2: TBitBtn;
    QrCaixa: TmySQLQuery;
    QrCaixaCodigo: TIntegerField;
    QrCaixaControle: TIntegerField;
    QrCaixaAtivo: TIntegerField;
    QrCaixaDataI: TDateField;
    QrCaixaDataF: TDateField;
    QrCaixaValor: TFloatField;
    QrCaixaTotalFator: TFloatField;
    QrCaixaLk: TIntegerField;
    QrCaixaDataCad: TDateField;
    QrCaixaDataAlt: TDateField;
    QrCaixaUserCad: TIntegerField;
    QrCaixaUserAlt: TIntegerField;
    QrCaixaPreco: TFloatField;
    QrCaixaPeriodo: TIntegerField;
    QrCaixaTaxa: TFloatField;
    DsCaixa: TDataSource;
    DBGrid9: TDBGrid;
    DBGrid22: TDBGrid;
    Splitter6: TSplitter;
    QrCaixaNOMETARIFA: TWideStringField;
    QrCaixaNOMEALUNO: TWideStringField;
    QrCaixaTOTAL: TFloatField;
    QrCaixaDESCO: TFloatField;
    QrCaixa2: TmySQLQuery;
    DsCaixa2: TDataSource;
    QrCaixa2Credito: TFloatField;
    QrCaixa2NOMECLIENTE: TWideStringField;
    QrCliDeb: TmySQLQuery;
    DsCliDeb: TDataSource;
    QrCliCre: TmySQLQuery;
    DsCliCre: TDataSource;
    QrCliDebValorReal: TFloatField;
    QrCliCreCredito: TFloatField;
    QrCliProdQuantN2: TFloatField;
    Splitter7: TSplitter;
    Mercadorias3: TMenuItem;
    Cadastro2: TMenuItem;
    Entrada2: TMenuItem;
    Controlados2: TMenuItem;
    Nocontrolados2: TMenuItem;
    Grupos3: TMenuItem;
    Inicial2: TMenuItem;
    Refresh2: TMenuItem;
    Relatrios3: TMenuItem;
    Balanos2: TMenuItem;
    QrCaixa3: TmySQLQuery;
    DsCaixa3: TDataSource;
    QrCaixa4: TmySQLQuery;
    DsCaixa4: TDataSource;
    QrCaixa3Taxa: TFloatField;
    QrCaixa3Preco: TFloatField;
    QrCaixa3Valor: TFloatField;
    QrCaixa3TOTAL: TFloatField;
    QrCaixa3DESCO: TFloatField;
    QrCaixa4Credito: TFloatField;
    QrCaixa3MERCA: TFloatField;
    QrCaixa3SALDO: TFloatField;
    BtImprime: TBitBtn;
    QrMatriRenAti: TmySQLQuery;
    Refresh3: TMenuItem;
    QrMatriRenVen: TmySQLQuery;
    QrMatriRenAtiCodigo: TIntegerField;
    QrMatriRenPen: TmySQLQuery;
    IntegerField10: TIntegerField;
    QrMRP: TmySQLQuery;
    QrMRPNOMECLIENTE: TWideStringField;
    QrMRPCodigo: TIntegerField;
    QrMRPAtivo: TIntegerField;
    QrMRPDataI: TDateField;
    QrMRPDataF: TDateField;
    QrMRPValor: TFloatField;
    QrMRPACobrar: TFloatField;
    DsMRP: TDataSource;
    QrMRPSEQ: TIntegerField;
    QrMRPABERTO: TFloatField;
    ListadePendncias1: TMenuItem;
    BtImprime5: TBitBtn;
    PMImprime5: TPopupMenu;
    Bematech1: TMenuItem;
    Matricial1: TMenuItem;
    TbImpReciC: TmySQLTable;
    TbImpReciCTipo: TIntegerField;
    TbImpReciCLinha: TAutoIncField;
    TbImpReciCTexto: TWideStringField;
    TbImpReciCLk: TIntegerField;
    TbImpReciCDataCad: TDateField;
    TbImpReciCDataAlt: TDateField;
    TbImpReciCUserCad: TIntegerField;
    TbImpReciCUserAlt: TIntegerField;
    TbImpReciCModo: TIntegerField;
    TbImpReciCNegr: TIntegerField;
    TbImpReciCItal: TIntegerField;
    TbImpReciCSubl: TIntegerField;
    TbImpReciCExpa: TIntegerField;
    TbImpReciCNOMEMODO: TWideStringField;
    TbImpReciR: TmySQLTable;
    TbImpReciRTipo: TIntegerField;
    TbImpReciRLinha: TAutoIncField;
    TbImpReciRTexto: TWideStringField;
    TbImpReciRLk: TIntegerField;
    TbImpReciRDataCad: TDateField;
    TbImpReciRDataAlt: TDateField;
    TbImpReciRUserCad: TIntegerField;
    TbImpReciRUserAlt: TIntegerField;
    TbImpReciRModo: TIntegerField;
    TbImpReciRNegr: TIntegerField;
    TbImpReciRItal: TIntegerField;
    TbImpReciRSubl: TIntegerField;
    TbImpReciRExpa: TIntegerField;
    TbImpReciRNOMEMODO: TWideStringField;
    QrProdPen: TmySQLQuery;
    IntegerField11: TIntegerField;
    FloatField2: TFloatField;
    StringField4: TWideStringField;
    DsProdPen: TDataSource;
    QrMatriPen: TmySQLQuery;
    QrMatriPenPENDENTE: TFloatField;
    DsMatriPen: TDataSource;
    TabSheet9: TTabSheet;
    QrCxa: TmySQLQuery;
    QrCxaNOMEMATRI: TWideStringField;
    QrCxaNOMEVENDA: TWideStringField;
    QrCxaCredito: TFloatField;
    QrCxaFatID: TIntegerField;
    QrCxaControle: TIntegerField;
    QrCxaAluno: TIntegerField;
    DBGrid23: TDBGrid;
    DsCxa: TDataSource;
    Panel38: TPanel;
    Label5: TLabel;
    TPCxaData: TdmkEditDateTimePicker;
    BtRefresh3: TBitBtn;
    BtImprime6: TBitBtn;
    QrCxaNOMEALUNO: TWideStringField;
    QrCxaFONTE: TWideStringField;
    QrSumCxa: TmySQLQuery;
    QrSumCxaCredito: TFloatField;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DsSumCxa: TDataSource;
    BtExclui4: TBitBtn;
    PmExclui4: TPopupMenu;
    Pagamento1: TMenuItem;
    Mercadoria1: TMenuItem;
    QrCliPagFatID_Sub: TIntegerField;
    QrCliPagOperCount: TIntegerField;
    QrCliPagLancto: TIntegerField;
    QrCliPagCliInt: TIntegerField;
    QrCliPagMoraDia: TFloatField;
    QrCliPagMulta: TFloatField;
    QrCliPagProtesto: TDateField;
    QrCliPagDataDoc: TDateField;
    QrCliPagCtrlIni: TIntegerField;
    QrCliPagNivel: TIntegerField;
    QrCliPagVendedor: TIntegerField;
    QrCliPagAccount: TIntegerField;
    QrCliPagICMS_P: TFloatField;
    QrCliPagICMS_V: TFloatField;
    QrCliPagDuplicata: TWideStringField;
    QrCliPagDepto: TIntegerField;
    QrCliPagDescoPor: TIntegerField;
    QrCliPagDataCad: TDateField;
    QrCliPagDataAlt: TDateField;
    QrCliPagUserCad: TIntegerField;
    QrCliPagUserAlt: TIntegerField;
    QrCliPagForneceI: TIntegerField;
    QrCliPagQtde: TFloatField;
    QrCliPagCNPJCPF: TWideStringField;
    QrCliPagEmitente: TWideStringField;
    QrCliPagContaCorrente: TWideStringField;
    QrCliPagCNPJCPF_TXT: TWideStringField;
    Label12: TLabel;
    TPCaixaFim: TdmkEditDateTimePicker;
    frxAlunos: TfrxReport;
    frxDsMatriculados: TfrxDBDataset;
    frxTurMatriPagtos: TfrxReport;
    frxDsTurMatriPagtos: TfrxDBDataset;
    frxAlunos2: TfrxReport;
    frxDsAlunos2: TfrxDBDataset;
    frxPagtos: TfrxReport;
    frxDsAluPagtos: TfrxDBDataset;
    frxDsAluPgTot: TfrxDBDataset;
    frxPagtos1: TfrxReport;
    frxDsAluPgPer: TfrxDBDataset;
    frxPagtos2: TfrxReport;
    frxAvisos2: TfrxReport;
    frxCxa: TfrxReport;
    frxDsCxa: TfrxDBDataset;
    frxMRP: TfrxReport;
    frxDsMRP: TfrxDBDataset;
    QrLoclanctoFatNum: TFloatField;
    QrAluPagtosFatNum: TFloatField;
    QrAluPgTotFatNum: TFloatField;
    QrAluPgPerFatNum: TFloatField;
    QrCliPagFatNum: TFloatField;
    QrCxaFatNum: TFloatField;
    QrCxaCliente: TIntegerField;
    frxDsTurmas: TfrxDBDataset;
    QrMatriculadosMATRIREN: TIntegerField;
    frxDsAlunos: TfrxDBDataset;
    QrAluPgPerMATRICULA: TWideStringField;
    Corrrigetarifas1: TMenuItem;
    Correes1: TMenuItem;
    CorrigeFatID1: TMenuItem;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvOfficeHint1: TAdvOfficeHint;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvPreviewMenu1: TAdvPreviewMenu;
    ImgAbout: TImage;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager12: TAdvPage;
    AdvToolBar4: TAdvToolBar;
    AGBCompeticoes: TAdvGlowButton;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar9: TAdvToolBar;
    AdvToolBar11: TAdvToolBar;
    AGBMenuAdv: TAdvGlowMenuButton;
    AGBTheme: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AGBBackup: TAdvGlowButton;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    ATB_Atualiza: TAdvToolBarButton;
    ATB_Login: TAdvToolBarButton;
    ATB_Backup: TAdvToolBarButton;
    AdvPage1: TAdvPage;
    AdvToolBar6: TAdvToolBar;
    AGBEntidadesA: TAdvGlowButton;
    AGBEntidadesB: TAdvGlowButton;
    AdvToolBar5: TAdvToolBar;
    AdvGlowButton1: TAdvGlowButton;
    AdvPage2: TAdvPage;
    AdvToolBar7: TAdvToolBar;
    AGBAtividades: TAdvGlowButton;
    AGBSalas: TAdvGlowButton;
    AGBGrupoTurmas: TAdvGlowButton;
    AGBTurmas: TAdvGlowButton;
    AGBDepartamentos: TAdvGlowButton;
    AGBPeriodos: TAdvGlowButton;
    AdvPage3: TAdvPage;
    AdvToolBar8: TAdvToolBar;
    AGBMatriculas: TAdvGlowButton;
    AdvToolBar10: TAdvToolBar;
    AGBGruposTarifas: TAdvGlowButton;
    AGBPlanosDePagto: TAdvGlowButton;
    AGBPlanosReajusTotal: TAdvGlowButton;
    AdvPage4: TAdvPage;
    AdvPage5: TAdvPage;
    AdvToolBar13: TAdvToolBar;
    AdvGlowButton11: TAdvGlowButton;
    AdvToolBar12: TAdvToolBar;
    AGBGruposMercadorias: TAdvGlowButton;
    AGBMercadoriasControladas: TAdvGlowButton;
    AGBMercadoriasNaoControladas: TAdvGlowButton;
    AdvPage6: TAdvPage;
    AdvToolBar14: TAdvToolBar;
    AdvGlowButton4: TAdvGlowButton;
    AdvGlowButton6: TAdvGlowButton;
    AdvToolBar17: TAdvToolBar;
    AGBProdEntrada: TAdvGlowButton;
    AGBProdInicial: TAdvGlowButton;
    AdvToolBar18: TAdvToolBar;
    AGBProdInventario: TAdvGlowButton;
    AGBProdRel: TAdvGlowButton;
    AGBProdRefresh: TAdvGlowButton;
    AGBCartas: TAdvGlowButton;
    PopupMenu1: TPopupMenu;
    TabSheet10: TTabSheet;
    Panel1: TPanel;
    BtRefresh10: TBitBtn;
    BtMatricula10: TBitBtn;
    DsMatriRenVen: TDataSource;
    DBGNaoRenovados: TdmkDBGrid;
    AdvGlowButton61: TAdvGlowButton;
    QrCliPagAgencia: TIntegerField;
    QrTurMatriPagtosData: TDateField;
    QrTurMatriPagtosFatNum: TFloatField;
    QrAluPagtosData: TDateField;
    QrAluPgTotData: TDateField;
    Panel39: TPanel;
    BtAutom: TBitBtn;
    QrMatriRenVenNOMEALUNO: TWideStringField;
    QrMatriRenVenDATAF: TDateField;
    QrMatriRenVenmrEmc: TLargeintField;
    QrMatriRenVenPqDt: TLargeintField;
    QrMatriRenVenPorQue: TLargeintField;
    QrMatriRenVenCodigo: TIntegerField;
    QrMatriRenVenAluno: TIntegerField;
    QrMatriRenVenOrientador: TIntegerField;
    QrMatriRenVenDataC: TDateField;
    QrMatriRenVenContrato: TWideStringField;
    QrMatriRenVenRespons: TIntegerField;
    QrMatriRenVenDATAF_TXT: TWideStringField;
    AdvToolBar2: TAdvToolBar;
    AdvGlowButton58: TAdvGlowButton;
    AdvGlowButton12: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    AdvGlowButton9: TAdvGlowButton;
    AdvGlowButton7: TAdvGlowButton;
    AdvGlowButton10: TAdvGlowButton;
    AdvPMVerificaBD: TAdvPopupMenu;
    VerificaBDServidor1: TMenuItem;
    VerificaTabelasterceiros1: TMenuItem;
    AdvGlowMenuButton2: TAdvGlowMenuButton;
    TabSheet22: TTabSheet;
    GradeA: TStringGrid;
    Panel40: TPanel;
    Button1: TButton;
    EdXLSName: TEdit;
    RGFormaExporta: TRadioGroup;
    Memo1: TMemo;
    BalloonHint1: TBalloonHint;
    TySuporte: TTrayIcon;
    TmSuporte: TTimer;
    AdvToolBarButton13: TAdvToolBarButton;
    AdvGlowButton25: TAdvGlowButton;
    AdvGlowButton24: TAdvGlowButton;
    AdvGlowButton29: TAdvGlowButton;
    AdvToolBarButton12: TAdvToolBarButton;
    AGBNewFinMigra: TAdvGlowButton;
    AGBOpcoesDiversas: TAdvGlowButton;
    AdvPage7: TAdvPage;
    AdvToolBar15: TAdvToolBar;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton5: TAdvGlowButton;
    AdvGlowButton30: TAdvGlowButton;
    AGBFiliais: TAdvGlowButton;
    AdvGlowMenuButton5: TAdvGlowMenuButton;
    QrMatriculadosEmpresa: TIntegerField;
    QrMatriculadosCodCliInt: TIntegerField;
    QrAlunosCodCliInt: TIntegerField;
    QrAlunosEmpresa: TIntegerField;
    Label13: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    DBGrid49: TDBGrid;
    Label19: TLabel;
    EdEmpresa2: TdmkEditCB;
    CBEmpresa2: TdmkDBLookupComboBox;
    QrAluPgTotMez: TIntegerField;
    QrAluPgTotDuplicata: TWideStringField;
    QrAluPgTotDoc2: TWideStringField;
    QrAluPgTotSerieCH: TWideStringField;
    QrAluPgTotGenero: TIntegerField;
    QrAluPgTotSerieNF: TWideStringField;
    QrAluPgTotNotaFiscal: TIntegerField;
    QrAluPgTotFornecedor: TIntegerField;
    QrAluPgTotCliente: TIntegerField;
    QrAluPgTotVendedor: TIntegerField;
    QrAluPgTotAccount: TIntegerField;
    QrAluPgTotICMS_P: TFloatField;
    QrAluPgTotICMS_V: TFloatField;
    QrAluPgTotCliInt: TIntegerField;
    QrAluPgTotDepto: TIntegerField;
    QrAluPgTotDescoPor: TIntegerField;
    QrAluPgTotForneceI: TIntegerField;
    QrAluPgTotDescoVal: TFloatField;
    QrAluPgTotNFVal: TFloatField;
    QrAluPgTotUnidade: TIntegerField;
    QrAluPgTotQtde: TFloatField;
    QrAluPgTotFatID: TIntegerField;
    QrAluPgTotFatID_Sub: TIntegerField;
    QrAluPgTotMultaVal: TFloatField;
    QrAluPgTotMoraVal: TFloatField;
    QrAluPgTotReparcel: TIntegerField;
    QrAluPgTotPrazo: TSmallintField;
    QrAluPgPerPrazo: TSmallintField;
    QrAluPgPerReparcel: TIntegerField;
    QrAluPgTotBancoCar: TIntegerField;
    CompensarnacontacorrenteBanco1: TMenuItem;
    Desfazercompensao1: TMenuItem;
    N4: TMenuItem;
    PagarRolarcomemissochequeetc1: TMenuItem;
    QrAluPagtosMez: TIntegerField;
    QrAluPagtosDuplicata: TWideStringField;
    QrAluPagtosDoc2: TWideStringField;
    QrAluPagtosSerieCH: TWideStringField;
    QrAluPagtosGenero: TIntegerField;
    QrAluPagtosSerieNF: TWideStringField;
    QrAluPagtosNotaFiscal: TIntegerField;
    QrAluPagtosFornecedor: TIntegerField;
    QrAluPagtosCliente: TIntegerField;
    QrAluPagtosVendedor: TIntegerField;
    QrAluPagtosAccount: TIntegerField;
    QrAluPagtosICMS_P: TFloatField;
    QrAluPagtosICMS_V: TFloatField;
    QrAluPagtosCliInt: TIntegerField;
    QrAluPagtosDepto: TIntegerField;
    QrAluPagtosDescoPor: TIntegerField;
    QrAluPagtosForneceI: TIntegerField;
    QrAluPagtosDescoVal: TFloatField;
    QrAluPagtosNFVal: TFloatField;
    QrAluPagtosUnidade: TIntegerField;
    QrAluPagtosQtde: TFloatField;
    QrAluPagtosFatID: TIntegerField;
    QrAluPagtosFatID_Sub: TIntegerField;
    QrAluPagtosMultaVal: TFloatField;
    QrAluPagtosMoraVal: TFloatField;
    QrAluPagtosReparcel: TIntegerField;
    QrAluPagtosPrazo: TSmallintField;
    QrAluPagtosBancoCar: TIntegerField;
    QrAluPagtosNOMECONJUNTO: TWideStringField;
    QrAluPgTotNOMECONJUNTO: TWideStringField;
    QrAluPagtosNOMEGRUPO: TWideStringField;
    QrAluPgTotNOMEGRUPO: TWideStringField;
    QrAluPagtosNOMERELACIONADO: TWideStringField;
    QrAluPgTotNOMERELACIONADO: TWideStringField;
    QrAluPagtosNOMECLIENTE: TWideStringField;
    QrAluPgTotNOMECLIENTE: TWideStringField;
    QrAluPagtosNOMEFORNECEDOR: TWideStringField;
    QrAluPgTotNOMEFORNECEDOR: TWideStringField;
    QrAluPgTotNOMEEMPRESA: TWideStringField;
    QrAluPagtosNOMEEMPRESA: TWideStringField;
    QrAluPagtosNOMESUBGRUPO: TWideStringField;
    QrAluPgTotNOMESUBGRUPO: TWideStringField;
    QrAluPagtosNOMECONTA: TWideStringField;
    QrAluPgTotNOMECONTA: TWideStringField;
    QrAluPagtosNivel: TIntegerField;
    QrAluPgTotNivel: TIntegerField;
    QrAluPagtosMENSAL2: TWideStringField;
    QrAluPgTotMENSAL2: TWideStringField;
    QrAluPagtosMes2: TLargeintField;
    QrAluPgTotMes2: TLargeintField;
    QrAluPagtosAno: TFloatField;
    QrAluPgTotAno: TFloatField;
    QrAluPagtosDataDoc: TDateField;
    QrAluPgTotDataDoc: TDateField;
    TmVersao: TTimer;
    AdvPage8: TAdvPage;
    AdvToolBar29: TAdvToolBar;
    AdvGlowButton38: TAdvGlowButton;
    VerificaDBWeb1: TMenuItem;
    AdvToolBar19: TAdvToolBar;
    AGBAtualiza: TAdvGlowButton;
    AdvGlowButton19: TAdvGlowButton;
    AdvGlowButton59: TAdvGlowButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure BtRecicloClick(Sender: TObject);
    procedure Instituies1Click(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Turmas1Click(Sender: TObject);
    procedure Cursos2Click(Sender: TObject);
    procedure Salas2Click(Sender: TObject);
    procedure CkTurmaDesClick(Sender: TObject);
    procedure QrTurmasCalcFields(DataSet: TDataSet);
    procedure CkTurmaEmFClick(Sender: TObject);
    procedure CkTurmaAtiClick(Sender: TObject);
    procedure QrTurmasAfterScroll(DataSet: TDataSet);
    procedure QrMatriculadosAfterScroll(DataSet: TDataSet);
    procedure QrMatriculadosBeforeClose(DataSet: TDataSet);
    procedure QrTurmasBeforeClose(DataSet: TDataSet);
    procedure QrTurMatriPagtosCalcFields(DataSet: TDataSet);
    procedure CkQuitadosClick(Sender: TObject);
    procedure CkAbertosClick(Sender: TObject);
    procedure QrTurmasItsAfterScroll(DataSet: TDataSet);
    procedure QrTurmasItsBeforeClose(DataSet: TDataSet);
    procedure BtLista1Click(Sender: TObject);
    procedure QrTurmasTmpCalcFields(DataSet: TDataSet);
    procedure BtEntidades2_1Click(Sender: TObject);
    procedure BtImprime1Click(Sender: TObject);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CkCursoAtiClick(Sender: TObject);
    procedure CkCursoDesClick(Sender: TObject);
    procedure Aplicar1Click(Sender: TObject);
    procedure QrMatriculadosCalcFields(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure CkAlunosAtivosClick(Sender: TObject);
    procedure CkAlunosInativosClick(Sender: TObject);
    procedure QrAlunosCalcFields(DataSet: TDataSet);
    procedure QrAlunosAfterScroll(DataSet: TDataSet);
    procedure QrAluTurmasAfterScroll(DataSet: TDataSet);
    procedure QrAluTurmasBeforeClose(DataSet: TDataSet);
    procedure QrAlunosBeforeClose(DataSet: TDataSet);
    procedure Retirarturmadamatrcula1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Excluirtodamatrcula1Click(Sender: TObject);
    procedure QrAluPagtosCalcFields(DataSet: TDataSet);
    procedure Resetar21Click(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure GradeHojeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrHojeAfterScroll(DataSet: TDataSet);
    procedure QrHojeBeforeClose(DataSet: TDataSet);
    procedure BtLista2Click(Sender: TObject);
    procedure QrAluPgTotCalcFields(DataSet: TDataSet);
    procedure CkAluPagtosQClick(Sender: TObject);
    procedure CkAluPagtosAClick(Sender: TObject);
    procedure CkAluPagtosVClick(Sender: TObject);
    procedure BtQuita2Click(Sender: TObject);
    procedure BtImprime3Click(Sender: TObject);
    procedure PMQuita2Popup(Sender: TObject);
    procedure RGAluPgTotOrdemClick(Sender: TObject);
    procedure QrAluPgPerCalcFields(DataSet: TDataSet);
    procedure TPAPPIniChange(Sender: TObject);
    procedure TPAPPFimChange(Sender: TObject);
    procedure Lista1Click(Sender: TObject);
    procedure Avisos1Click(Sender: TObject);
    procedure BtMatricula2Click(Sender: TObject);
    procedure BtMatricula3Click(Sender: TObject);
    procedure CkAluMatriAtivoClick(Sender: TObject);
    procedure CkAluMatriInatiClick(Sender: TObject);
    procedure EdAlunoChange(Sender: TObject);
    procedure TPAulaChange(Sender: TObject);
    procedure QrHojeChamAfterScroll(DataSet: TDataSet);
    procedure BtImprime4Click(Sender: TObject);
    procedure BtChamadaClick(Sender: TObject);
    procedure QrHojeChamItsCalcFields(DataSet: TDataSet);
    procedure DBGrid15DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrAlunos2CalcFields(DataSet: TDataSet);
    procedure QrHojeChamBeforeClose(DataSet: TDataSet);
    procedure BtExclui1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrHojeChamAfterOpen(DataSet: TDataSet);
    procedure QrHojeChamItsAfterOpen(DataSet: TDataSet);
    procedure Departamentos2Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Opes2Click(Sender: TObject);
    procedure Reabrirtabelas1Click(Sender: TObject);
    procedure Contas2Click(Sender: TObject);
    procedure Perodos1Click(Sender: TObject);
    procedure ListadeTurmas1Click(Sender: TObject);
    procedure QrAlunosAtivosAfterOpen(DataSet: TDataSet);
    procedure Gruposde1Click(Sender: TObject);
    procedure BtRefresh1Click(Sender: TObject);
    procedure TPMensVenceChange(Sender: TObject);
    procedure BtMatricula4Click(Sender: TObject);
    procedure EdLocCliChange(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    procedure QrCliProdCalcFields(DataSet: TDataSet);
    procedure BtMercadoria1Click(Sender: TObject);
    procedure QrCliProdAfterScroll(DataSet: TDataSet);
    procedure QrCliProdAfterClose(DataSet: TDataSet);
    procedure QrCliProdAfterOpen(DataSet: TDataSet);
    procedure QrCliPagCalcFields(DataSet: TDataSet);
    procedure BtDinheiroClick(Sender: TObject);
    procedure Tarifas2Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure BtRefresh2Click(Sender: TObject);
    procedure TPCaixaIniChange(Sender: TObject);
    procedure QrCaixaCalcFields(DataSet: TDataSet);
    procedure DBGrid9DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Entrada2Click(Sender: TObject);
    procedure Controlados2Click(Sender: TObject);
    procedure Nocontrolados2Click(Sender: TObject);
    procedure Grupos3Click(Sender: TObject);
    procedure Inicial2Click(Sender: TObject);
    procedure Refresh2Click(Sender: TObject);
    procedure Relatrios3Click(Sender: TObject);
    procedure TabSheet8Hide(Sender: TObject);
    procedure QrCaixa3CalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure Refresh3Click(Sender: TObject);
    procedure QrMRPCalcFields(DataSet: TDataSet);
    procedure ListadePendncias1Click(Sender: TObject);
    procedure DBGrid19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtImprime5Click(Sender: TObject);
    procedure Bematech1Click(Sender: TObject);
    procedure Matricial1Click(Sender: TObject);
    procedure BtRefresh3Click(Sender: TObject);
    procedure TPCxaDataChange(Sender: TObject);
    procedure QrCxaCalcFields(DataSet: TDataSet);
    procedure BtImprime6Click(Sender: TObject);
    procedure BtExclui4Click(Sender: TObject);
    procedure Pagamento1Click(Sender: TObject);
    procedure GridFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Mercadoria1Click(Sender: TObject);
    procedure frxAlunos2GetValue(const VarName: String;
      var Value: Variant);
    procedure frxPagtos1GetValue(const VarName: String;
      var Value: Variant);
    procedure frxCxaGetValue(const VarName: String; var Value: Variant);
    procedure Corrrigetarifas1Click(Sender: TObject);
    procedure CorrigeFatID1Click(Sender: TObject);
    procedure AGBCompeticoesClick(Sender: TObject);
    procedure ATB_LoginClick(Sender: TObject);
    procedure AGBEntidadesAClick(Sender: TObject);
    procedure AGBEntidadesBClick(Sender: TObject);
    procedure AGBAtividadesClick(Sender: TObject);
    procedure AGBSalasClick(Sender: TObject);
    procedure AGBTurmasClick(Sender: TObject);
    procedure AGBGrupoTurmasClick(Sender: TObject);
    procedure AGBDepartamentosClick(Sender: TObject);
    procedure AGBMatriculasClick(Sender: TObject);
    procedure AGBPeriodosClick(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton11Click(Sender: TObject);
    procedure AGBGruposMercadoriasClick(Sender: TObject);
    procedure AGBMercadoriasControladasClick(Sender: TObject);
    procedure AGBMercadoriasNaoControladasClick(Sender: TObject);
    procedure AGBGruposTarifasClick(Sender: TObject);
    procedure AGBPlanosDePagtoClick(Sender: TObject);
    procedure AGBPlanosReajusTotalClick(Sender: TObject);
    procedure AGBProdEntradaClick(Sender: TObject);
    procedure AGBProdInicialClick(Sender: TObject);
    procedure AGBProdInventarioClick(Sender: TObject);
    procedure AGBProdRelClick(Sender: TObject);
    procedure AGBProdRefreshClick(Sender: TObject);
    procedure AGBBackupClick(Sender: TObject);
    procedure AGBCartasClick(Sender: TObject);
    procedure AGBOpcoesDiversasClick(Sender: TObject);
    procedure AdvGlowButton4Click(Sender: TObject);
    procedure AdvGlowButton6Click(Sender: TObject);
    procedure BtRefresh10Click(Sender: TObject);
    procedure BtMatricula10Click(Sender: TObject);
    procedure AdvGlowButton61Click(Sender: TObject);
    procedure ATB_BackupClick(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure QrMatriRenVenCalcFields(DataSet: TDataSet);
    procedure AdvGlowButton12Click(Sender: TObject);
    procedure AdvGlowButton58Click(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
    procedure AdvGlowButton9Click(Sender: TObject);
    procedure AdvGlowButton7Click(Sender: TObject);
    procedure AdvGlowButton10Click(Sender: TObject);
    procedure VerificaTabelasterceiros1Click(Sender: TObject);
    procedure VerificaBDServidor1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ATB_AtualizaClick(Sender: TObject);
    procedure AGBAtualizaClick(Sender: TObject);
    procedure TmSuporteTime(Sender: TObject);
    procedure AdvToolBarButton13Click(Sender: TObject);
    procedure AdvGlowButton25Click(Sender: TObject);
    procedure AdvGlowButton24Click(Sender: TObject);
    procedure AdvGlowButton29Click(Sender: TObject);
    procedure AdvToolBarButton12Click(Sender: TObject);
    procedure AGBNewFinMigraClick(Sender: TObject);
    procedure AdvGlowButton19Click(Sender: TObject);
    procedure AdvGlowButton3Click(Sender: TObject);
    procedure AdvGlowButton30Click(Sender: TObject);
    procedure AGBFiliaisClick(Sender: TObject);
    procedure AGBThemeClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Limpar1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure TPCaixaFimChange(Sender: TObject);
    procedure EdEmpresa2Change(Sender: TObject);
    procedure TPAPPIniClick(Sender: TObject);
    procedure TPAPPFimClick(Sender: TObject);
    procedure CompensarnacontacorrenteBanco1Click(Sender: TObject);
    procedure PagarRolarcomemissochequeetc1Click(Sender: TObject);
    procedure Desfazercompensao1Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure AdvGlowButton38Click(Sender: TObject);
    procedure VerificaDBWeb1Click(Sender: TObject);
    procedure AdvGlowButton59Click(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    //FTamLinha, FFoiExpand: Integer;
    //FArqPrn: TextFile;
    //FPrintedLine: Boolean;
    FAplicaInfoRegEdit: Boolean;
    FHojeAluno: Integer;
    FAluPagtos_TabLctA: String;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
    //
    procedure CadastroMatriculas(Curso, LocMatricula: Integer);
    procedure ReopenHoje;
    procedure ReopenTurmas(Turma: Integer);
    procedure ReopenCursos(Curso: Integer);
    procedure ReopenMatriculados(LocMatricula: Integer);
    procedure ReopenTurMatriPagtos(EntCliInt, CodCliInt: Integer);
    procedure ReopenAluPagtos(Todos, Aluno, Periodo: Boolean; EntCliInt, CodCliInt: Integer);
    procedure ReabreTurmasIts;
    procedure ReabreTurmasTmp;
    procedure TurmasAfterScroll;
    procedure ResetaDadosComponentes(MinhaGrade: TStringGrid);
    procedure ReopenQrHojeChamIts(Matricula: Integer);
    procedure ReopenHojeCham(Codigo: Integer);
    //
    function ImprimeFechamentoDia: Boolean;
    function ImprimeCupom(Saida: Integer): Boolean;
    function ImprimeLinhaCupom(Saida: Integer; Texto: String;
              TpoLtra, Italic, Sublin, expand, enfat: Integer): Integer;
    procedure SelecionaImagemdefundo;
    procedure SkinMenu(Index: integer);
    // provisorio
    procedure PreencheGradeTeste();
    //procedure DefineVarsCliInt(CliInt: Integer);
    procedure AppIdle(Sender: TObject; var Done: Boolean);
  public
    { Public declarations }
    FEntInt: Integer;
    FACobrar: Boolean;
    FMatricula,
    FAlunoMatric,
    FAlunoAtual,
    FAlunoTurma: Integer;
    FAtivosAtualizados: Boolean;
    FMatriculaNaoAutomatica: Boolean;
    FProdutosControlados: Boolean;
    // Bematech
    iRetorno: Integer;         // Vari�vel com o retorno da fun��o
    FTipoNovoEnti: Integer;

    procedure RetornoCNAB;
    procedure ShowHint(Sender: TObject);
    procedure CadastroDeContasNiv();
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    //
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    function CartaoDeFatura: Integer;
    function CompensacaoDeFatura: String;
    function PreparaMinutosSQL: Boolean;
    function AdicionaMinutosSQL(HI, HF: TTime): Boolean;
    procedure SalvaInfoRegEdit(Dest: Integer; MinhaGrade: TStringGrid);
    procedure AplicaInfoRegEdit(Forca: Boolean);

    procedure MostraBackup;

    procedure CadastroTurmas(Codigo: Integer);
    procedure CadastroCursos(Codigo: Integer);
    procedure CadastroSalas(Codigo: Integer);
    procedure CadastroDeOpcoes();
    procedure CadastroDeTarifas(Codigo, Grupo: Integer);
    procedure CadastroDePeriodos(Codigo: Integer);
    procedure CadastroTarifasGru(Codigo, Tarifa: Integer);

    procedure ReabreAlunos(Aluno: Integer);
    procedure ReopenAlunosTurmas;
    procedure ReopenAlunosMatric;
    procedure ReopenAluTurMatric;
    procedure ReopenAlunosCursos;
    procedure ReopenMensVencidas;
    procedure ReopenCliVen;
    procedure ReopenCaixa();

    procedure AtualizaAlunosAtivos;
    procedure VerificaCadastroDeMatricula(Aluno: Integer);
    procedure CadastroDeGruposDeTurmas(Codigo: Integer);

    function InicializaBEMA_NaoFiscal: Integer;
    procedure AtualizaAtivosEVencidos;
    function PendCli(Cliente: Integer): Double;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
              Entidade: Integer; AbrirEmAba: Boolean);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure CriaPages;
    procedure CriaMinhasEtiquetas;
    procedure AcoesIniciaisDoAplicativo;
    procedure MostraLogoff();
    procedure ReCaptionComponentesDeForm(Form: TForm);
    procedure CadastroDeDepartamentos(Codigo: Integer);

    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;

    //procedure CadastroProdutos(Controlados: Boolean);
    //procedure CadastroGruposProdutos();
    //procedure EntradaProdutos();
    //procedure InicialProdutos();
    //procedure BalancosMercadorias;
    //procedure ImprimeMercadorias;
    //procedure RefreshProdutos;
    //procedure RefreshMercadorias;
    //procedure IncluiVendas;
    //procedure ExcluiPagtoProduto;
    //procedure ExcluiMercadoria;
    //procedure ReopenCliProd(Cliente: Integer);
    //procedure AtualizaSaldoCliente(Cliente: Integer);
    //procedure ReopenCliPag;
  end;

var
  FmPrincipal: TFmPrincipal;

const
  FCamFiltardo = 'Cashier\Filtrado';

implementation

uses UnMyObjects, Module, Entidades, VerifiDB, MyGlyfs, MyDBCheck, Academy_MLA,
  Opcoes, MyListas, MyVCLSkin, Cursos, Salas, Matricul, Turmas, ImpPagtos,
  Cartas, CalcPercent, UnGOTOy, UnInternalConsts3, Chamada, ChamadasNew,
  UnitAcademy, ChamadasIts, Departamentos, Mensais, Periodos, TurmasGru,
  TurmasImp, Tarifas, ModuleLct2, UnFinanceiroJan, RetornoBematech, ImpRecil,
  MatrRefresh, DotPrint, UCreate, ContasNiv, Entidade2, Pages, MultiEtiq,
  ModuleGeral, ModuloBemaNaoFiscal, ModuleFin, Backup3, Matriz, TarifasGru,
  TarifasNovas, About, SWMS_Mtng_Cab, QuitaDoc2, DmkExcel, DmkDAC_PF,
  {$IFDEF UsaWSuport} UnDmkWeb, {$ENDIF} VerifiDBTerceiros, LinkRankSkin,
  ParamsEmp, UnFixBugs, UnDmkWeb_Jan;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Marcar quando for criar uma fun��o de atualiza��o
  try
    if FixBug = 0 then
      Atualiza��o1()
    else if FixBug = 1 then
      Atualiza��o2()
    else if FixBug = 2 then
      Atualiza��o3()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*))
  then
    Geral.MB_Aviso('Vers�o difere do arquivo!');
  //
  if not FALiberar then
    Timer1.Enabled := True;
    //
  AplicaInfoRegEdit(False);
  if FAtivosAtualizados then
    AtualizaAlunosAtivos;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem: String;
begin
  Application.OnException := MyObjects.MostraErro;
  VAR_TYPE_LOG      := ttlCliIntUni;
  VAR_ADMIN         := 'admin';
  FEntInt := -11;
  //VAR_CALCULADORA_COMPONENTCLASS := TFmCalculadora;
  //VAR_CALCULADORA_REFERENCE      := FmCalculadora;
  VAR_USA_TAG_BITBTN := True;
  //
  FTipoNovoEnti := 0;
  //VAR_MASTER1 := Master1;
  VAR_CAD_POPUP := PMGeral;

  //

  VAR_TIPO_TAB_LCT := 1;
  VAR_MULTIPLAS_TAB_LCT := True;

  VAR_ECF_IF        := 1;
  VAR_STLOGIN       := StatusBar.Panels[01];
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_LOGOFFDE      := nil;
  //
  VAR_DEF_CLI1 := True;
  SalvaInfoRegEdit(0, StringGrid1);
  Geral.DefineFormatacoes;
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnException := MyObjects.MostraErro;
  Application.OnIdle      := AppIdle;
  //
  Imagem := Geral.ReadAppKey(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg', HKEY_LOCAL_MACHINE);
  if FileExists(Imagem) then ImgPrincipal.Picture.LoadFromFile(Imagem);
  //
  MenuStyle := Geral.ReadAppKey('MenuStyle', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  SkinMenu(MenuStyle);
  //
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_CAD_POPUP := PMGeral;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  //PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  PageControl7.ActivePageIndex := 0;
  //PageControl8.ActivePageIndex := 0;
  //PageControl9.ActivePageIndex := 0;
  //
  TPAPPIni.Date := Date;
  TPAPPFim.Date := Date;
  TPAula.Date   := Date;
  //
  StringGrid1.Align := alClient;
  ///////////////////////////////////////////////////////////////////////////////
  TPMensVence.Date := Date;
  TPCaixaIni.Date  := Date;
  TPCaixaFim.Date  := Date;
  TPCxaData.Date   := Date;
  //
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  PreencheGradeTeste();
  //
  TabSheet22.TabVisible := False;
  TabSheet7.TabVisible  := False;
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfTrue);
  //
  //2016-03-22 => N�o est� sendo utilizado pelo cliente caso precise reativar usar a grade
  AdvPage5.TabVisible  := False;
  TabSheet7.TabVisible := False;
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FmAcademy_MLA.Hide;
  Geral.WriteAppKey('Terceiro', Application.Title, VAR_TERCEIRO, ktInteger, HKEY_LOCAL_MACHINE);
  Geral.WriteAppKey('EmissFat', Application.Title, VAR_EMISSFAT, ktInteger, HKEY_LOCAL_MACHINE);
  //Geral.WriteAppKey('Dias', Application.Title, Date - TPDataIni.Date, ktInteger, HKEY_LOCAL_MACHINE);
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  SalvaInfoRegEdit(1, nil);
  Application.Terminate;
end;

procedure TFmPrincipal.PreencheGradeTeste();
var
  I, J: Integer;
begin
  for I := 0 to GradeA.ColCount - 1 do
  begin
    GradeA.Cells[I, 0] := 'Coluna ' + Geral.FF0(I);
    for J := 1 to GradeA.RowCount - 1 do
    begin
      GradeA.Cells[I, J] := Geral.FF0(I  + (J * GradeA.ColCount));
    end;
  end;
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.AdvGlowButton10Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeNiveisPlano;
end;

procedure TFmPrincipal.AdvGlowButton11Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton12Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeSubGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton19Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.AdvGlowButton1Click(Sender: TObject);
var
  Cod_Txt: String;
  Cod_Int, i: Integer;
begin
  Cod_Txt := '1';
  if InputQuery('Verifica��o de cadastros', 'Informe a numera��o final:',
  Cod_Txt) then
  begin
    Screen.Cursor := crHourGlass;
    try
      Cod_Int := Geral.IMV(Cod_Txt);
      if Cod_Int > 0 then
      begin
        DMod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('INSERT INTO entidades SET Codigo=:P0, CodUsu=:P1, Tipo=1');
        for i := 1 to Cod_int do
        begin
          DMod.QrAux.Close;
          DMod.QrAux.SQL.Clear;
          DMod.QrAux.SQL.Add('SELECT Codigo FROM entidades WHERE Codigo=' +
          FormatFloat('0', i));
          DMod.QrAux.Open;
          //
          if DMod.QrAux.RecordCount = 0 then
          begin
            DMod.QrUpd.Params[00].AsInteger := i;
            DMod.QrUpd.Params[01].AsInteger := i;
            DMod.QrUpd.ExecSQL;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPrincipal.AdvGlowButton24Click(Sender: TObject);
begin
  FinanceiroJan.ImpressaoDoPlanoDeContas;
end;

procedure TFmPrincipal.AdvGlowButton25Click(Sender: TObject);
begin
  FinanceiroJan.SaldoDeContas(0);
end;

procedure TFmPrincipal.AdvGlowButton29Click(Sender: TObject);
begin
  FinanceiroJan.CadastroContasLnk;
end;

procedure TFmPrincipal.AdvGlowButton2Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeGrupos(0);
end;

procedure TFmPrincipal.AdvGlowButton30Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton38Click(Sender: TObject);
begin
  DmkWeb_Jan.MostraWOpcoes(Dmod.MyDBn);
end;

procedure TFmPrincipal.AdvGlowButton3Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AdvGlowButton4Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChamada, FmChamada, afmoNegarComAviso) then
  begin
    FmChamada.ShowModal;
    FmChamada.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton58Click(Sender: TObject);
var
  DmLctX: TDataModule;
begin
  if UnDmkDAC_PF.CriaDataModule(FmPrincipal, TDmLct2, DmLctX) then
  begin
    DmLct2.GerenciaEmpresa();
  end;
  {
  //DefineVarsCliInt(FEntInt);
  if DBCheck.CriaFm(TFmSelfGer, FmSelfGer, afmoNegarComAviso) then
  begin
    //DmodFin.ReabreCarteiras(

    (*
    FmSelfGer.BtEspecificos.Visible := True;
    FmSelfGer.BtEspecificos.Caption := 'Desig';
    MenuItem := TMenuItem.Create(FmSelfGer);
    MenuItem.Caption := 'Gerencia des�gnios';
    MenuItem.OnClick := CadastroDeDesignioSefGer;
    FmSelfGer.PMEspecificos.Items.Add(MenuItem);
    *)

    FmSelfGer.FGeradorTxt := 'Fatura';
    FmSelfGer.ShowModal;
    FmSelfGer.Destroy;
    Application.OnHint := ShowHint;
    (*
    FCliInt := 0;
    FEntInt := 0;
    *)
  end;
  }
end;

procedure TFmPrincipal.AdvGlowButton59Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.AdvGlowButton61Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeCarteiras(0);
end;

procedure TFmPrincipal.AdvGlowButton6Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmImpPagtos, FmImpPagtos, afmoNegarComAviso) then
  begin
    FmImpPagtos.ShowModal;
    FmImpPagtos.Destroy;
  end;
end;

procedure TFmPrincipal.AdvGlowButton7Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDePlano(0);
end;

procedure TFmPrincipal.AdvGlowButton9Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeConjutos(0);
end;

procedure TFmPrincipal.AdvToolBarButton12Click(Sender: TObject);
begin
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, deftfInverse);
end;

procedure TFmPrincipal.AdvToolBarButton13Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte(False, 2, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.AGBAtividadesClick(Sender: TObject);
begin
  CadastroCursos(0);
end;

procedure TFmPrincipal.AGBAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AGBBackupClick(Sender: TObject);
begin
  MostraBackup();
end;

procedure TFmPrincipal.AGBCartasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 3;
    FmCartas.ShowModal;
    FmCartas.Destroy;
  end;
end;

procedure TFmPrincipal.AGBCompeticoesClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSWMS_Mtng_Cab, FmSWMS_Mtng_Cab, afmoNegarComAviso) then
  begin
    FmSWMS_Mtng_Cab.ShowModal;
    FmSWMS_Mtng_Cab.Destroy;
  end;
end;

procedure TFmPrincipal.AGBDepartamentosClick(Sender: TObject);
begin
  CadastroDeDepartamentos(0);
end;

procedure TFmPrincipal.AGBEntidadesAClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidades, fmcadEntidades);
end;

procedure TFmPrincipal.AGBEntidadesBClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmPrincipal.AGBFiliaisClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

procedure TFmPrincipal.AGBGruposMercadoriasClick(Sender: TObject);
begin
  //CadastroGruposProdutos();
end;

procedure TFmPrincipal.AGBGruposTarifasClick(Sender: TObject);
begin
  CadastroTarifasGru(0, 0);
end;

procedure TFmPrincipal.AGBGrupoTurmasClick(Sender: TObject);
begin
  CadastroDeGruposDeTurmas(0);
end;

procedure TFmPrincipal.AGBMatriculasClick(Sender: TObject);
begin
  CadastroMatriculas(0,0);
end;

procedure TFmPrincipal.AGBMercadoriasControladasClick(Sender: TObject);
begin
  //CadastroProdutos(True);
end;

procedure TFmPrincipal.AGBMercadoriasNaoControladasClick(Sender: TObject);
begin
  //CadastroProdutos(False);
end;

procedure TFmPrincipal.AGBNewFinMigraClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  //
  DmLct2.MigraLctsParaTabLct();
end;

procedure TFmPrincipal.AGBOpcoesDiversasClick(Sender: TObject);
begin
  CadastroDeOpcoes();
end;

procedure TFmPrincipal.AGBPeriodosClick(Sender: TObject);
begin
  CadastroDePeriodos(0);
end;

procedure TFmPrincipal.AGBPlanosDePagtoClick(Sender: TObject);
begin
  CadastroDeTarifas(0, 0);
end;

procedure TFmPrincipal.AGBPlanosReajusTotalClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTarifasNovas, FmTarifasNovas, afmoNegarComAviso) then
  begin
    FmTarifasNovas.ShowModal;
    FmTarifasNovas.Destroy;
  end;
end;

procedure TFmPrincipal.AGBProdEntradaClick(Sender: TObject);
begin
  //EntradaProdutos();
end;

procedure TFmPrincipal.AGBProdInicialClick(Sender: TObject);
begin
  //InicialProdutos();
end;

procedure TFmPrincipal.AGBProdInventarioClick(Sender: TObject);
begin
  //BalancosMercadorias();
end;

procedure TFmPrincipal.AGBProdRefreshClick(Sender: TObject);
begin
  //RefreshProdutos();
end;

procedure TFmPrincipal.AGBProdRelClick(Sender: TObject);
begin
  //ImprimeMercadorias();
end;

procedure TFmPrincipal.AGBSalasClick(Sender: TObject);
begin
  CadastroSalas(0);
end;

procedure TFmPrincipal.AGBThemeClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.AGBTurmasClick(Sender: TObject);
begin
  CadastroTurmas(0);
  ReopenTurmas(QrTurmasCodigo.Value);
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar      := True;
  FmAcademy_MLA.Show;
  Enabled := False;
  FmAcademy_MLA.Refresh;
  FmAcademy_MLA.EdSenha.Text := FmAcademy_MLA.EdSenha.Text+'*';
  FmAcademy_MLA.EdSenha.Refresh;
  FmAcademy_MLA.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Geral.MB_Erro('Imposs�vel criar Modulo de dados!');
    FmAcademy_MLA.Close;
    Exit;
  end;
  FmAcademy_MLA.EdSenha.Text := FmAcademy_MLA.EdSenha.Text+'*';
  FmAcademy_MLA.EdSenha.Refresh;
  FmAcademy_MLA.ReloadSkin;
  FmAcademy_MLA.EdLogin.Text := '';
  FmAcademy_MLA.EdSenha.Text := '';
  FmAcademy_MLA.EdSenha.Refresh;
  FmAcademy_MLA.EdLogin.ReadOnly := False;
  FmAcademy_MLA.EdSenha.ReadOnly := False;
  FmAcademy_MLA.EdLogin.SetFocus;
  //FmAcademy_MLA.ReloadSkin;
  FmAcademy_MLA.Refresh;

end;

procedure TFmPrincipal.MostraBackup;
begin
  DModG.MostraBackup3();
end;

procedure TFmPrincipal.CadastroDeOpcoes;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoBoss) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.BtRecicloClick(Sender: TObject);
begin
  (*
  if DBCheck.CriaFm(TMatriculasSit, MatriculasSit, afmoNegarComAviso) then
  begin
    FmMatriculasSit.ShowModal;
    FmMatriculasSit.Destroy;
  end;
  *)
end;

procedure TFmPrincipal.MostraLogoff();
begin
  FmPrincipal.Enabled := False;
  //
  FmAcademy_MLA.Show;
  FmAcademy_MLA.EdLogin.Text := '';
  FmAcademy_MLA.EdSenha.Text := '';
  FmAcademy_MLA.EdLogin.SetFocus;
end;

procedure TFmPrincipal.CadastroCursos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCursos, FmCursos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCursos.LocCod(Codigo, Codigo);
    FmCursos.Caption := 'Atividades';
    FmCursos.ShowModal;
    FmCursos.Destroy;
  end;
end;

procedure TFmPrincipal.Instituies1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmInstituicoes, FmInstituicoes, afmoNegarComAviso) then
  begin
    FmInstituicoes.ShowModal;
    FmInstituicoes.Destroy;
  end;
}
end;

procedure TFmPrincipal.CadastroSalas(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmSalas, FmSalas, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmSalas.LocCod(Codigo, Codigo);
    FmSalas.ShowModal;
    FmSalas.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroMatriculas(Curso, LocMatricula: Integer);
begin
  if DBCheck.CriaFm(TFmMatricul, FmMatricul, afmoNegarComAviso) then
  begin
    FmMatricul.FLocMa1 := LocMatricula;
    FmMatricul.ShowModal;
    FmMatricul.Destroy;
  end;
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(0, fmcadSelecionar, fmcadSelecionar);
end;

procedure TFmPrincipal.Turmas1Click(Sender: TObject);
begin
  CadastroTurmas(0);
  ReopenTurmas(QrTurmasCodigo.Value);
end;

procedure TFmPrincipal.CadastroTarifasGru(Codigo, Tarifa: Integer);
begin
  if DBCheck.CriaFm(TFmTarifasGru, FmTarifasGru, afmoNegarComAviso) then
  begin
    if (Codigo <> 0) or (Tarifa <> 0) then
      FmTarifasGru.LocCod(Codigo, Codigo);
    //
    if Tarifa <> 0 then
      FmTarifasGru.ReopenTarifas(Tarifa);
    //
    FmTarifasGru.ShowModal;
    FmTarifasGru.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroTurmas(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTurmas, FmTurmas, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTurmas.LocCod(Codigo, Codigo);
    FmTurmas.ShowModal;
    FmTurmas.Destroy;
  end;
end;

procedure TFmPrincipal.Cursos2Click(Sender: TObject);
begin
  CadastroCursos(0);
end;

procedure TFmPrincipal.Salas2Click(Sender: TObject);
begin
  CadastroSalas(0);
end;

procedure TFmPrincipal.ReopenTurmas(Turma: Integer);
var
  S0, S1, S2: Integer;
begin
  QrTurmas.Close;
  if CkTurmaDes.Checked then S0 := 0 else S0 := -1000;
  if CkTurmaEmF.Checked then S1 := 1 else S1 := -1000;
  if CkTurmaAti.Checked then S2 := 2 else S2 := -1000;
  //
  QrTurmas.Params[0].AsInteger := S0;
  QrTurmas.Params[1].AsInteger := S1;
  QrTurmas.Params[2].AsInteger := S2;
  QrTurmas.Open;
  //
  QrTurmas.Locate('Codigo', Turma, []);
end;

procedure TFmPrincipal.ReopenCursos(Curso: Integer);
var
  S0, S1: Integer;
begin
  QrCursos.Close;
  if CkCursoDes.Checked then S0 := 0 else S0 := -1000;
  if CkCursoAti.Checked then S1 := 1 else S1 := -1000;
  //
  QrCursos.Params[0].AsInteger := S0;
  QrCursos.Params[1].AsInteger := S1;
  QrCursos.Open;
  //
  QrCursos.Locate('Codigo', Curso, []);
end;

procedure TFmPrincipal.CkTurmaDesClick(Sender: TObject);
begin
  ReopenTurmas(QrTurmasCodigo.Value);
end;

procedure TFmPrincipal.CkTurmaEmFClick(Sender: TObject);
begin
  ReopenTurmas(QrTurmasCodigo.Value);
end;

procedure TFmPrincipal.CkTurmaAtiClick(Sender: TObject);
begin
  ReopenTurmas(QrTurmasCodigo.Value);
end;

procedure TFmPrincipal.QrTurmasCalcFields(DataSet: TDataSet);
var
  N: String;
begin
  case QrTurmasAtiva.Value of
    0: N := 'Desativada';
    1: N := 'Em Forma��o';
    2: N := 'Ativa';
  end;
  QrTurmasNOMEATIVA.Value := N;
end;

procedure TFmPrincipal.QrTurmasAfterScroll(DataSet: TDataSet);
begin
  TurmasAfterScroll;
end;

procedure TFmPrincipal.TurmasAfterScroll;
begin
  ReopenMatriculados(FMatricula);
  ReabreTurmasIts;
end;

procedure TFmPrincipal.ReopenMatriculados(LocMatricula: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMatriculados, Dmod.MyDB, [
    'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
    'ELSE ent.Nome END NOMEALUNO, mat.Aluno, ',
    'mat.Codigo MATRICULA, ren.Controle MATRIREN, ',
    'mtu.Turma, mat.Empresa, eci.CodCliInt ',
    'FROM matritur mtu ',
    'LEFT JOIN matricul  mat ON mat.Codigo=mtu.Codigo ',
    'LEFT JOIN matriren  ren ON ren.Controle=mtu.Controle ',
    'LEFT JOIN entidades ent ON ent.Codigo=mat.Aluno ',
    'LEFT JOIN enticliint eci ON eci.CodEnti=mat.Empresa ',
    'WHERE mat.Ativo=1 ',
    'AND ren.Ativo=1 ',
    'AND mtu.Turma=' + Geral.FF0(QrTurmasCodigo.Value),
    'ORDER BY NOMEALUNO ',
    '']);
  //
  QrMatriculados.Locate('MATRICULA', LocMatricula, []);
end;

procedure TFmPrincipal.QrMatriculadosAfterScroll(DataSet: TDataSet);
begin
  ReopenTurMatriPagtos(QrMatriculadosEmpresa.Value, QrMatriculadosCodCliInt.Value);
  FMatricula := QrMatriculadosMATRICULA.Value;
end;

procedure TFmPrincipal.ReopenTurMatriPagtos(EntCliInt, CodCliInt: Integer);
var
  Conta: Integer;
  TbLctA, Sit: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
  //
  Conta := 0;
  //
  if CkAbertos.Checked then
    Conta := 1;
  if CkQuitados.Checked then
    Conta := Conta + 2;
  //
  case Conta of
    0: Sit := 'AND Sit < -1000';
    1: Sit := 'AND SIT < 2';
    2: Sit := 'AND Sit > 1';
    3: Sit := ''; // Todos
  end;
  {$IFDEF DEFINE_VARLCT}
  DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //
  QrTurMatriPagtos.Close;
  QrTurMatriPagtos.SQL.Clear;
  QrTurMatriPagtos.SQL.Add('SELECT la.Data, la.Vencimento, la.Credito, la.Debito,');
  QrTurMatriPagtos.SQL.Add('la.Compensado, la.Descricao, la.Sit, la.Tipo,');
  QrTurMatriPagtos.SQL.Add('la.Pago, la.MoraDia, la.Multa, la.FatParcela,');
  QrTurMatriPagtos.SQL.Add('la.Documento, la.Controle, la.CtrlIni, la.Sub,');
  QrTurMatriPagtos.SQL.Add('la.Carteira, ca.Nome NOMECARTEIRA,');
  QrTurMatriPagtos.SQL.Add('ca.Tipo CARTEIRATIPO, la.FatNum');
  QrTurMatriPagtos.SQL.Add('FROM ' + TbLctA + ' la, Carteiras ca');
  QrTurMatriPagtos.SQL.Add('WHERE ca.Codigo=la.Carteira');
  QrTurMatriPagtos.SQL.Add('AND FatID=2101');
  // Mudei 2010-06-06 - N�o funcionava. Porque?
  //QrTurMatriPagtos.SQL.Add('AND FatNum='+FormatFloat('0', QrMatriculadosMATRICULA.Value));
  QrTurMatriPagtos.SQL.Add('AND FatNum='+FormatFloat('0', QrMatriculadosMATRIREN.Value));
  // Fim 2010-06-06
  QrTurMatriPagtos.SQL.Add(Sit);
  QrTurMatriPagtos.SQL.Add('ORDER BY la.Vencimento, la.FatParcela');
  QrTurMatriPagtos.Open;
end;

procedure TFmPrincipal.ReopenAluPagtos(Todos, Aluno, Periodo: Boolean;
  EntCliInt, CodCliInt: Integer);
var
  Conta: Integer;
  TbLctA, Sit, Ord, Ini, Fim: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
  //
  Conta := 0;
  //
  {$IFDEF DEFINE_VARLCT}
  DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //
  FAluPagtos_TabLctA := TbLctA;
  //
  if CkAluPagtosV.Checked then Conta := Conta + 1;
  if CkAluPagtosA.Checked then Conta := Conta + 2;
  if CkAluPagtosQ.Checked then Conta := Conta + 4;
  case Conta of
    0: Sit := 'AND Sit < -1000';
    1: Sit := 'AND ((Sit < 2) AND (Vencimento<NOW()))';
    2: Sit := 'AND ((Sit < 2) AND (Vencimento>=NOW()))';
    3: Sit := 'AND Sit < 2';
    4: Sit := 'AND Sit > 1';
    5: Sit := 'AND (((Sit < 2) AND (Vencimento<NOW())) OR (Sit>1))';
    6: Sit := 'AND (((Sit < 2) AND (Vencimento>=NOW())) OR (Sit>1))';
    7: Sit := ''; // Todos
  end;
  //
  if Aluno then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrAluPagtos, Dmod.MyDB, [
      'SELECT la.Data, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, la.Genero, ',
      'la.SerieNF, la.NotaFiscal, la.Fornecedor, la.Cliente, la.Vendedor, ',
      'la.Account, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, la.DescoPor, ',
      'la.ForneceI, la.DescoVal, la.NFVal, la.Unidade, la.Qtde, la.FatID, ',
      'la.FatID_Sub, la.MultaVal, la.MoraVal, la.Reparcel, ca.Prazo, ca.Banco BancoCar, ',
      'la.Vencimento, la.Credito, la.Debito, la.Nivel, ',
      'la.Compensado, la.Descricao, la.Sit, la.Tipo, ',
      'la.Pago, la.MoraDia, la.Multa, la.FatParcela, ',
      'la.Documento, la.Controle, la.CtrlIni, la.Sub, ',
      'la.Carteira, ca.Nome NOMECARTEIRA, cj.Nome NOMECONJUNTO, ',
      'gr.Nome NOMEGRUPO, ca.Tipo CARTEIRATIPO, la.FatNum, ',
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial ',
      'ELSE cl.Nome END NOMECLIENTE, ',
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
      'ELSE fo.Nome END NOMEFORNECEDOR, ',
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial ',
      'ELSE em.Nome END NOMEEMPRESA, sg.Nome NOMESUBGRUPO, ct.Nome NOMECONTA, ',
      'MOD(la.Mez, 100) Mes2, ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, la.DataDoc ',
      'FROM ' + TbLctA + ' la, Carteiras ca ',
      'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0 ',
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo ',
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo ',
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente ',
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor ',
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa ',
      'WHERE ca.Codigo=la.Carteira ',
      'AND FatID=' + Geral.FF0(VAR_FATID_2101),
      Sit,
      'AND Cliente=' + Geral.FF0(QrAlunosAluno.Value),
      'ORDER BY la.Vencimento, la.FatParcela ',
      '']);
  end;
  //
  if QrAluPgTot.State = dsInactive then Todos := True;
  if Todos then begin
    if RGAluPgTotOrdem.ItemIndex = 0 then
      Ord := 'ORDER BY NOMEALUNO, la.Vencimento, la.FatParcela'
    else
      Ord := 'ORDER BY la.Vencimento, la.FatParcela';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAluPgTot, Dmod.MyDB, [
      'SELECT la.Data, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, ',
      'la.Genero, la.SerieNF, la.NotaFiscal, la.Fornecedor, la.Cliente, ',
      'la.Vendedor, la.Account, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, ',
      'la.DescoPor, la.ForneceI, la.DescoVal, la.NFVal, la.Unidade, la.Qtde, ',
      'la.FatID, la.FatID_Sub, la.MultaVal, la.MoraVal, ',
      'la.Vencimento, la.Credito, la.Debito, ca.Prazo, la.Reparcel, ',
      'la.Compensado, la.Descricao, la.Sit, la.Tipo, la.Nivel, ',
      'la.Pago, la.MoraDia, la.Multa, la.FatParcela, ',
      'la.Documento, la.Controle, la.CtrlIni, la.Sub, ',
      'la.Carteira, ca.Nome NOMECARTEIRA, cj.Nome NOMECONJUNTO, ',
      'gr.Nome NOMEGRUPO, ca.Tipo CARTEIRATIPO, la.FatNum, ',
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
      'ELSE en.Nome END NOMEALUNO, ca.Banco BancoCar, ',
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial ',
      'ELSE cl.Nome END NOMECLIENTE, ',
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
      'ELSE fo.Nome END NOMEFORNECEDOR, ',
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial ',
      'ELSE em.Nome END NOMEEMPRESA, sg.Nome NOMESUBGRUPO, ct.Nome NOMECONTA, ',
      'MOD(la.Mez, 100) Mes2, ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, la.DataDoc ',
      'FROM ' + TbLctA + ' la ',
      'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0 ',
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo ',
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo ',
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto ',
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
      'LEFT JOIN entidades en ON en.Codigo=la.Cliente ',
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente ',
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor ',
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa ',
      'WHERE FatID=' + Geral.FF0(VAR_FATID_2101),
      Sit,
      Ord,
      '']);
  end;
  if QrAluPgPer.State = dsInactive then Periodo := True;
  if Periodo then
  begin
    Ini := FormatDateTime(VAR_FORMATDATE, TPAPPIni.Date);
    Fim := FormatDateTime(VAR_FORMATDATE, TPAPPFim.Date);
    QrAluPgPer.Close;
    QrAluPgPer.SQL.Clear;
    QrAluPgPer.SQL.Add('SELECT la.Vencimento, la.Credito, la.Debito, la.Compensado, ');
    QrAluPgPer.SQL.Add('la.Descricao, la.Sit, la.Tipo, la.Pago, la.MoraDia, la.Multa, ');
    QrAluPgPer.SQL.Add('la.FatParcela, la.Documento, la.Controle, la.CtrlIni, la.Sub,');
    QrAluPgPer.SQL.Add('la.Carteira, ca.Nome NOMECARTEIRA, ca.Tipo CARTEIRATIPO, ');
    QrAluPgPer.SQL.Add('la.FatNum, ro.Nome NOMEORIENTADOR, ');
    QrAluPgPer.SQL.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEALUNO,');
    QrAluPgPer.SQL.Add('CASE WHEN en.Sexo="F" THEN "a" ELSE "o" END SEXOA, ');
    QrAluPgPer.SQL.Add('CONCAT(ma.Codigo,".",ma.Contrato) MATRICULA, ca.Prazo, la.Reparcel ');
    QrAluPgPer.SQL.Add('FROM ' + TbLctA + ' la');
    QrAluPgPer.SQL.Add('LEFT JOIN carteiras  ca ON ca.Codigo=la.Carteira');
    QrAluPgPer.SQL.Add('LEFT JOIN entidades  en ON en.Codigo=la.Cliente');
    QrAluPgPer.SQL.Add('LEFT JOIN matriren   mr ON mr.Controle=la.FatNum');
    QrAluPgPer.SQL.Add('LEFT JOIN matricul ma ON ma.Codigo=mr.Codigo');
    QrAluPgPer.SQL.Add('LEFT JOIN entidades  ro ON ro.Codigo=ma.Orientador');
    QrAluPgPer.SQL.Add('WHERE FatID=2101');
    QrAluPgPer.SQL.Add('AND la.Vencimento BETWEEN "'+Ini+'" AND "'+Fim+'"');
    QrAluPgPer.SQL.Add(Sit);
    QrAluPgPer.SQL.Add('ORDER BY NOMEALUNO');
    QrAluPgPer.Open;
  end;
end;

procedure TFmPrincipal.QrMatriculadosBeforeClose(DataSet: TDataSet);
begin
  QrTurMatriPagtos.Close;
end;

procedure TFmPrincipal.QrTurmasBeforeClose(DataSet: TDataSet);
begin
  QrMatriculados.Close;
  QrTurmasIts.Close;
end;

procedure TFmPrincipal.QrTurMatriPagtosCalcFields(DataSet: TDataSet);
begin
  if QrTurMatriPagtosCompensado.Value = 0 then
     QrTurMatriPagtosCOMPENSADOREAL.Value := ''
  else
     QrTurMatriPagtosCOMPENSADOREAL.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrTurMatriPagtosCompensado.Value);
////////////////////////////////////////////////////////////////////////////////
  if QrTurMatriPagtosSit.Value = -1 then
     QrTurMatriPagtosNOMESIT.Value := CO_IMPORTACAO
  else
    if QrTurMatriPagtosSit.Value = 1 then
     QrTurMatriPagtosNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrTurMatriPagtosSit.Value = 2 then
       QrTurMatriPagtosNOMESIT.Value := CO_QUITADA
  else
    if QrTurMatriPagtosSit.Value = 3 then
       QrTurMatriPagtosNOMESIT.Value := CO_COMPENSADA
  else
    if QrTurMatriPagtosVencimento.Value < Date then
       QrTurMatriPagtosNOMESIT.Value := CO_VENCIDA
  else
       QrTurMatriPagtosNOMESIT.Value := CO_EMABERTO;
  case QrTurMatriPagtosSit.Value of
    0: QrTurMatriPagtosSALDO.Value := QrTurMatriPagtosCredito.Value - QrTurMatriPagtosDebito.Value;
    1: QrTurMatriPagtosSALDO.Value := (QrTurMatriPagtosCredito.Value - QrTurMatriPagtosDebito.Value) + QrTurMatriPagtosPago.Value;
    else QrTurMatriPagtosSALDO.Value := 0;
  end;

  case QrTurMatriPagtosTipo.Value of
    0: QrTurMatriPagtosNOMETIPO.Value := CO_CAIXA;
    1: QrTurMatriPagtosNOMETIPO.Value := CO_BANCO;
    2: QrTurMatriPagtosNOMETIPO.Value := CO_EMISS;
  end;
  if (QrTurMatriPagtosVencimento.Value < Date) and (QrTurMatriPagtosSit.Value<2) then
    QrTurMatriPagtosVENCIDO.Value := QrTurMatriPagtosVencimento.Value;
//  if QrTurMatriPagtosVencimento.Value < Date then
//    QrTurMatriPagtosNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrTurMatriPagtosVencimento.Value)
//  else QrTurMatriPagtosNOMEVENCIDO.Value := CO_VAZIO;
  if QrTurMatriPagtosVENCIDO.Value > 0 then
     QrTurMatriPagtosATRAZODD.Value := Trunc(Date) - QrTurMatriPagtosVENCIDO.Value
  else QrTurMatriPagtosATRAZODD.Value := 0;
  if QrTurMatriPagtosATRAZODD.Value > 0 then QrTurMatriPagtosATUALIZADO.Value :=
  (QrTurMatriPagtosATRAZODD.Value * QrTurMatriPagtosMoraDia.Value) +
  QrTurMatriPagtosMulta.Value else QrTurMatriPagtosATUALIZADO.Value := 0;
  if QrTurMatriPagtosSit.Value<2 then QrTurMatriPagtosATUALIZADO.Value :=
  QrTurMatriPagtosATUALIZADO.Value + QrTurMatriPagtosSALDO.Value;

end;

procedure TFmPrincipal.CkQuitadosClick(Sender: TObject);
begin
  if (QrMatriculados.State <> dsInactive) and (QrMatriculados.RecordCount > 0) then
  begin
    ReopenTurMatriPagtos(QrMatriculadosEmpresa.Value, QrMatriculadosCodCliInt.Value);
  end;
end;

procedure TFmPrincipal.CkAbertosClick(Sender: TObject);
begin
  if (QrMatriculados.State <> dsInactive) and (QrMatriculados.RecordCount > 0) then
  begin
    ReopenTurMatriPagtos(QrMatriculadosEmpresa.Value, QrMatriculadosCodCliInt.Value);
  end;
end;

procedure TFmPrincipal.ReabreTurmasIts;
begin
  QrTurmasIts.Close;
  QrTurmasIts.Params[0].AsInteger := QrTurmasCodigo.Value;
  QrTurmasIts.Open;
end;

procedure TFmPrincipal.ReabreTurmasTmp;
begin
  QrTurmasTmp.Close;
  QrTurmasTmp.Params[0].AsInteger := QrTurmasItsCodigo.Value;
  QrTurmasTmp.Params[1].AsInteger := QrTurmasItsControle.Value;
  QrTurmasTmp.Open;
end;

procedure TFmPrincipal.QrTurmasItsAfterScroll(DataSet: TDataSet);
begin
  ReabreTurmasTmp;
end;

procedure TFmPrincipal.QrTurmasItsBeforeClose(DataSet: TDataSet);
begin
  QrTurmasTmp.Close;
end;

procedure TFmPrincipal.BtLista1Click(Sender: TObject);
begin
  if (QrMatriculados.State <> dsInactive) and (QrMatriculados.RecordCount > 0) then
  begin
    MyObjects.frxDefineDataSets(frxAlunos, [
      DmodG.frxDsDono,
      frxDsMatriculados,
      frxDsTurmas
      ]);
    //
    MyObjects.frxMostra(frxAlunos, 'Listagem de Alunos por Turma');
  end;
end;

procedure TFmPrincipal.QrTurmasTmpCalcFields(DataSet: TDataSet);
begin
  QrTurmasTmpDIASEMANA.Value := MLAGeral.NomediaDaSemana(QrTurmasTmpDSem.Value+1);
end;

procedure TFmPrincipal.BtEntidades2_1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmAlunos1, BtEntidades2_1);
end;

procedure TFmPrincipal.BtImprime1Click(Sender: TObject);
begin
  if (QrTurmas.State <> dsInactive) and (QrTurmas.RecordCount > 0) then
  begin
    MyObjects.frxDefineDataSets(frxTurMatriPagtos, [
      DmodG.frxDsDono,
      frxDsTurmas,
      frxDsMatriculados,
      frxDsTurMatriPagtos
      ]);
    //
    MyObjects.frxMostra(frxTurMatriPagtos, 'Listagem de Alunos por Turma');
  end;
end;

procedure TFmPrincipal.DBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid5, Rect, 1, QrCursosAtivo.Value);
end;

procedure TFmPrincipal.CkCursoAtiClick(Sender: TObject);
begin
  ReopenCursos(QrCursosCodigo.Value);
end;

procedure TFmPrincipal.CkCursoDesClick(Sender: TObject);
begin
  ReopenCursos(QrCursosCodigo.Value);
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  ShowMessage('Faturando n�o implementado');
  Result := 0;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  ShowMessage('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal.CompensarnacontacorrenteBanco1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  EntCliInt, CodCliInt, Parc2, Parc3: Integer;
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      EntCliInt := QrAlunosEmpresa.Value;
      CodCliInt := QrAlunosCodCliInt.Value;
      //
      if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
      //
      {$IFDEF DEFINE_VARLCT}
      DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
      {$ELSE}
      TbLctA := VAR_LCT;
      {$ENDIF}
      //
      case PageControl6.ActivePageIndex of
        0:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT * ',
            'FROM carteiras ',
            'WHERE Codigo=' + Geral.FF0(QrAluPagtosCarteira.Value),
            '']);
          UFinanceiro.QuitacaoDeDocumentos(DBGrid11, Qry, QrAluPagtos, TbLctA);
        end;
        1:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT * ',
            'FROM carteiras ',
            'WHERE Codigo=' + Geral.FF0(QrAluPgTotCarteira.Value),
            '']);
          UFinanceiro.QuitacaoDeDocumentos(DBGAluPgTot, Qry, QrAluPgTot, TbLctA);
        end;
      end;
      if (PageControl6.ActivePageIndex in [0,1]) and (QrAluPagtos.State = dsBrowse) then
      begin
        if QrAluPagtos.State = dsBrowse then
          Parc2 := QrAluPagtosFatParcela.Value
        else
          Parc2 := 0;
        //
        if QrAluPgTot.State = dsBrowse then
          Parc3 := QrAluPgTotFatParcela.Value
        else
          Parc3 := 0;
        //
        ReopenAluPagtos(True, True, True, EntCliInt, CodCliInt);
        //
        if Parc2 <> 0 then
          QrAluPagtos.Locate('FatParcela', Parc2, []);
        if Parc3 <> 0 then
          QrAluPgTot.Locate('FatParcela', Parc3, []);
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  if DBCheck.CriaFm(TFmCalcPercent, FmCalcPercent, afmoNegarComAviso) then
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.CriaMinhasEtiquetas;
begin
  if DBCheck.CriaFm(TFmMultiEtiq, FmMultiEtiq, afmoNegarComAviso) then
  begin
    FmMultiEtiq.ShowModal;
    FmMultiEtiq.Destroy;
  end;
end;

procedure TFmPrincipal.CriaPages;
begin
  if DBCheck.CriaFm(TFmPages, FmPages, afmoNegarComAviso) then
  begin
    FmPages.ShowModal;
    FmPages.Destroy;
  end;
end;

procedure TFmPrincipal.SalvaInfoRegEdit(Dest: Integer; MinhaGrade: TStringGrid);
var
  Cam, N: String;
  i, j, L, ARow: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
begin
  ARow := 0;
  Cam := Application.Title+'\InitialConfig';
  if Dest = 1 then
  begin
    if not Aplicar1.Checked then Exit;
    Geral.WriteAppKey('WindowState', Cam, WindowState, ktInteger, HKey_LOCAL_MACHINE);
    Geral.WriteAppKey('WindowWidth', Cam, Width, ktInteger, HKey_LOCAL_MACHINE);
    Geral.WriteAppKey('WindowHeigh', Cam, Height, ktInteger, HKey_LOCAL_MACHINE);
    Geral.WriteAppKey('WindowAplic', Cam, Aplicar1.Checked, ktBoolean, HKey_LOCAL_MACHINE);
  end else if (Dest = 0) and (MinhaGrade <> nil) then
  begin
    MinhaGrade.ColWidths[1] := 128;
    MinhaGrade.ColWidths[3] := 128;
    MinhaGrade.ColWidths[4] := 128;
    MinhaGrade.ColWidths[5] := 128;
    MinhaGrade.Cells[0, 0] := 'Componente';
    MinhaGrade.Cells[1, 0] := 'Nome do componente';
    MinhaGrade.Cells[2, 0] := 'Comprimento';
    MinhaGrade.Cells[3, 0] := 'Coluna';
    MinhaGrade.Cells[4, 0] := 'Campo';
    MinhaGrade.Cells[5, 0] := 'T�tulo';
  end;
  for i := 0 to ComponentCount -1 do
  begin
    if Components[i] is TPanel then
    begin
      Painel := TPanel(Components[i]);
      N := Painel.Name;
      //if Copy(N, 1, 3) = 'Pax' then
      if Painel.Align <> alClient then
      begin
        L := Painel.Width;
        if Dest = 1 then Geral.WriteAppKey(N, Cam, L, ktInteger, HKey_LOCAL_MACHINE)
        else if (Dest = 0) and (MinhaGrade<>nil) then
        begin
          ARow                      := ARow + 1;
          MinhaGrade.RowCount       := ARow + 1;
          MinhaGrade.Cells[0, ARow] := 'TPanel';
          MinhaGrade.Cells[1, ARow] := Painel.Name;
          MinhaGrade.Cells[2, ARow] := Geral.FF0(Painel.Width);
        end;
      end;
    end;
    if Components[i] is TDBGrid then
    begin
      Grade := TDBGrid(Components[i]);
      for j := 0 to Grade.Columns.Count-1 do
      begin
        if Dest = 1 then
        begin
          Geral.WriteAppKey(Geral.FF0(j), Cam+'\Grades\'+Grade.Name+'\Pos',
          Grade.Columns[j].FieldName, ktString, HKey_LOCAL_MACHINE);
          Geral.WriteAppKey(Geral.FF0(j), Cam+'\Grades\'+Grade.Name+'\Title',
          Grade.Columns[j].Title.Caption, ktString, HKey_LOCAL_MACHINE);
          Geral.WriteAppKey(Geral.FF0(j), Cam+'\Grades\'+
          Grade.Name+'\Width', Grade.Columns[j].Width, ktInteger, HKey_LOCAL_MACHINE);
        end else if (Dest = 0) and (MinhaGrade<>nil) then
        begin
          ARow := ARow + 1;
          MinhaGrade.RowCount := ARow + 1;
          MinhaGrade.Cells[0, ARow] := 'TDBGrid';
          MinhaGrade.Cells[1, ARow] := Grade.Name;
          MinhaGrade.Cells[2, ARow] := Geral.FF0(Grade.Columns[j].Width);
          MinhaGrade.Cells[3, ARow] := Geral.FF0(j);
          MinhaGrade.Cells[4, ARow] := Grade.Columns[j].FieldName;
          MinhaGrade.Cells[5, ARow] := Grade.Columns[j].Title.Caption;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.AplicaInfoRegEdit(Forca: Boolean);
var
  Cam, N: String;
  //W, H,
  i, L, j: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
  Query: TmySQLQuery;
begin
  if Forca then FAplicaInfoRegEdit := False;
  if FAplicaInfoRegEdit then Exit;
  FAplicaInfoRegEdit := True;
  Cam := Application.Title+'\InitialConfig';
  if Forca or Geral.ReadAppKey('WindowAplic', Cam, ktBoolean, False,
  HKey_LOCAL_MACHINE) then
  begin
    Aplicar1.Checked := True;
    WindowState := Geral.ReadAppKey('WindowState', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
    //W := Geral.ReadAppKey('WindowWidth', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
    //H := Geral.ReadAppKey('WindowHeigh', Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
    for i := 0 to ComponentCount -1 do
    begin
      ////////////////////////////////////////////////////////////////////////
      if Components[i] is TmySQLQuery then
      begin
        Query := TmySQLQuery(Components[i]);
        for j := 0 to Query.FieldCount-1 do
        begin
          if Query.Fields[j] is TDateField then
          TDateField(Query.Fields[j]).DisplayFormat := VAR_FORMATDATEi;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      if Components[i] is TPanel then
      begin
        Painel := TPanel(Components[i]);
        N := Painel.Name;
        //if Copy(N, 1, 3) = 'Pax' then
        if Painel.Align <> alClient then
        begin
          L := Geral.ReadAppKey(N, Cam, ktInteger, 0, HKEY_LOCAL_MACHINE);
          if L > 0 then
          Painel.Width := L;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      if Components[i] is TDBGrid then
      begin
        Grade := TDBGrid(Components[i]);
        for j := 0 to Grade.Columns.Count-1 do
        begin
          N := Geral.ReadAppKey(Geral.FF0(j), Cam+'\Grades\'+
          Grade.Name+'\Pos', ktString, '', HKey_LOCAL_MACHINE);
          if N <> '' then Grade.Columns[j].FieldName := N;
          N := Geral.ReadAppKey(Geral.FF0(j), Cam+'\Grades\'+
          Grade.Name+'\Title', ktString, '', HKey_LOCAL_MACHINE);
          if N <> '' then Grade.Columns[j].Title.Caption := N;
          L := Geral.ReadAppKey(Geral.FF0(j), Cam+'\Grades\'+
          Grade.Name+'\Width', ktInteger, 0, HKey_LOCAL_MACHINE);
          if L > 0 then Grade.Columns[j].Width := L;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.Aplicar1Click(Sender: TObject);
begin
  Aplicar1.Checked := not Aplicar1.Checked;
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  Done := True;
  //
  if DModG <> nil then
    DmodG.ExecutaPing(FmAcademy_MLA, [Dmod.MyDB, Dmod.MyDBn, DModG.MyPID_DB, DModG.AllID_DB]);
end;

procedure TFmPrincipal.ATB_AtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.ATB_BackupClick(Sender: TObject);
begin
  MostraBackup;
end;

procedure TFmPrincipal.ATB_LoginClick(Sender: TObject);
begin
  MostraLogoff();
end;

procedure TFmPrincipal.QrMatriculadosCalcFields(DataSet: TDataSet);
begin
  QrMatriculadosSEQ.Value := QrMatriculados.RecNo;
end;

procedure TFmPrincipal.QrMatriRenVenCalcFields(DataSet: TDataSet);
begin
  QrMatriRenVenDATAF_TXT.Value := Geral.FDT(QrMatriRenVenDATAF.Value, 3);
end;

procedure TFmPrincipal.Inclui1Click(Sender: TObject);
begin
  CadastroMatriculas(QrTurmasCodigo.Value, 0);
  ReopenMatriculados(VAR_CADASTRO);
end;

procedure TFmPrincipal.Exclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Ao excluir este aluno da turma, sua '+
  'matr�cula e seus pagamentos N�O ser�o excluidos. Para poder exclu�los ' +
  'antes de excluir o aluno da turma informe n�o. ' + sLineBreak +
  'Deseja continuar assim mesmo?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM matritur');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Turma=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrMatriculadosMATRICULA.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrMatriculadosTurma.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  TurmasAfterScroll;
end;

procedure TFmPrincipal.Desativa1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a desativa��o do aluno "' +
    QrMatriculadosNOMEALUNO.Value +'" ?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE matricul SET Ativo=0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=' + Geral.FF0(QrMatriculadosMATRICULA.Value));
    Dmod.QrUpd.ExecSQL;
    //
    ReopenMatriculados(FMatricula);
  end;
end;

procedure TFmPrincipal.CkAlunosAtivosClick(Sender: TObject);
begin
  ReabreAlunos(FAlunoAtual);
end;

procedure TFmPrincipal.CkAlunosInativosClick(Sender: TObject);
begin
  ReabreAlunos(FAlunoAtual);
end;

procedure TFmPrincipal.ReabreAlunos(Aluno: Integer);
var
  Ativos, Inativos: String;
begin
  Ativos   := Geral.FF0(MLAGeral.BoolToInt2(CkAlunosAtivos.Checked, 1, -1000));
  Inativos := Geral.FF0(MLAGeral.BoolToInt2(CkAlunosInativos.Checked, 0, -1000));
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAlunos, Dmod.MyDB, [
    'SELECT DISTINCT ma.Aluno, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
    'ELSE en.Nome END NOMEALUNO, ma.Empresa, ec.CodCliInt ',
    'FROM matricul ma ',
    'LEFT JOIN entidades en ON en.Codigo=ma.Aluno ',
    'LEFT JOIN enticliint ec ON ec.CodEnti=ma.Empresa ',
    'WHERE ma.Ativo in (' + Ativos + ', ' + Inativos + ') ',
    'ORDER BY NOMEALUNO ',
    '']);
  //
  QrAlunos.Locate('Aluno', Aluno, []);
end;

procedure TFmPrincipal.QrAlunosCalcFields(DataSet: TDataSet);
begin
  QrAlunosSEQ.Value := QrAlunos.RecNo;
end;

procedure TFmPrincipal.QrAlunosAfterScroll(DataSet: TDataSet);
begin
  FAlunoAtual := QrAlunosAluno.Value;
  ReopenAlunosTurmas;
  ReopenAlunosMatric;
  ReopenAluPagtos(False, True, False, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
  ReopenAlunosCursos;
end;

procedure TFmPrincipal.ReopenAlunosTurmas;
begin
  QrAluTurmas.Close;
  QrAluTurmas.Params[0].AsInteger := FAlunoAtual;
  QrAluTurmas.Open;
  //
  QrAluTurmas.Locate('Turma', FAlunoTurma, []);
end;

procedure TFmPrincipal.ReopenAlunosMatric;
begin
  QrAluMatri.Close;
  QrAluMatri.Params[0].AsInteger := QrAlunosAluno.Value;
  QrAluMatri.Params[1].AsInteger := MLAGeral.BoolToInt2(CkAluMatriAtivo.Checked, 1, -1000);
  QrAluMatri.Params[2].AsInteger := MLAGeral.BoolToInt2(CkAluMatriInati.Checked, 0, -1000);
  QrAluMatri.Open;
  //
  QrAluMatri.Locate('Codigo', FAlunoMatric, []);
end;

(*procedure TFmPrincipal.ReopenAlunosPagtos;
begin
  QrAluPagtos.Close;
  QrAluPagtos.Params[0].AsInteger := FAlunoAtual;
  QrAluPagtos.Open;
  //
  //QrAlunosPagtos.Locate('??', FAlunoTurma, []);
end;*)

procedure TFmPrincipal.ReopenAlunosCursos;
begin
  QrAluCursos.Close;
  QrAluCursos.Params[0].AsInteger := FAlunoAtual;
  QrAluCursos.Open;
  //
  //QrAlunosCursos.Locate('??', FAlunoTurma, []);
end;

procedure TFmPrincipal.QrAluTurmasAfterScroll(DataSet: TDataSet);
begin
  ReopenAluTurMatric;
end;

procedure TFmPrincipal.ReopenAluTurMatric;
begin
  QrAluTurMatric.Close;
  QrAluTurMatric.Params[0].AsInteger := QrAlunosAluno.Value;
  QrAluTurMatric.Params[1].AsInteger := QrAluTurmasTurma.Value;
  QrAluTurMatric.Params[2].AsInteger := QrAluTurmasMATRICULA.Value;
  QrAluTurMatric.Open;
end;

procedure TFmPrincipal.QrAluTurmasBeforeClose(DataSet: TDataSet);
begin
  QrAluTurMatric.Close;
end;

procedure TFmPrincipal.QrAlunosBeforeClose(DataSet: TDataSet);
begin
  QrAluTurmas.Close;
  QrAluPagtos.Close;
  QrAluCursos.Close;
end;

procedure TFmPrincipal.Retirarturmadamatrcula1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada desta turma da matr�cula ' +
    Geral.FF0(QrAluTurmasMATRICULA.Value) + '?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM matricursos ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Turma=:P1');
    Dmod.QrUpd.Params[0].ASInteger := QrAluTurmasMATRICULA.Value;
    Dmod.QrUpd.Params[1].ASInteger := QrAluTurmasTurma.Value;
    Dmod.QrUpd.ExecSQL;
    ReopenAlunosTurmas;
  end;
end;

procedure TFmPrincipal.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExcluir, BtExclui);
end;

procedure TFmPrincipal.Excluirtodamatrcula1Click(Sender: TObject);
var
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
  begin
    {$IFDEF DEFINE_VARLCT}
    DModG.Def_EM_ABD(TMeuDB, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value,
      DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
    {$ELSE}
    TbLctA := VAR_LCT;
    {$ENDIF}
    //
    if Geral.MB_Pergunta('Confirma a exclus�o de toda matr�cula ' +
      Geral.FF0(QrAluTurmasMATRICULA.Value)+'?' + sLineBreak +
      'Todos pagamentos e turmas desta matr�cula ser�o excluidos!' +
      sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM matricursos ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].ASInteger := QrAluTurmasMATRICULA.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + TbLctA);
      Dmod.QrUpd.SQL.Add('WHERE FatID=2101 AND FatNum=:P0');
      Dmod.QrUpd.Params[0].ASInteger := QrAluTurmasMATRICULA.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM matricul ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].ASInteger := QrAluTurmasMATRICULA.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReabreAlunos(FAlunoAtual);
    end;
  end;
end;

procedure TFmPrincipal.QrAluPagtosCalcFields(DataSet: TDataSet);
begin
  if QrAluPagtosCompensado.Value = 0 then
     QrAluPagtosCOMPENSADOREAL.Value := ''
  else
     QrAluPagtosCOMPENSADOREAL.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrAluPagtosCompensado.Value);
////////////////////////////////////////////////////////////////////////////////
  QrAluPagtosNOMESIT.Value := UFinanceiro.NomeSitLancto(QrAluPagtosSit.Value,
                               QrAluPagtosTipo.Value, QrAluPagtosPrazo.Value,
                               QrAluPagtosVencimento.Value, QrAluPagtosReparcel.Value);
  //
  QrAluPagtosNOMERELACIONADO.Value := UFinanceiro.NomeRelacionado(QrAluPagtosCliente.Value,
    QrAluPagtosFornecedor.Value, QrAluPagtosNOMECLIENTE.Value, QrAluPagtosNOMEFORNECEDOR.Value);
  //
  case QrAluPagtosSit.Value of
    0: QrAluPagtosSALDO.Value := QrAluPagtosCredito.Value - QrAluPagtosDebito.Value;
    1: QrAluPagtosSALDO.Value := (QrAluPagtosCredito.Value - QrAluPagtosDebito.Value) + QrAluPagtosPago.Value;
    else QrAluPagtosSALDO.Value := 0;
  end;

  case QrAluPagtosTipo.Value of
    0: QrAluPagtosNOMETIPO.Value := CO_CAIXA;
    1: QrAluPagtosNOMETIPO.Value := CO_BANCO;
    2: QrAluPagtosNOMETIPO.Value := CO_EMISS;
  end;
  if (QrAluPagtosVencimento.Value < Date) and (QrAluPagtosSit.Value<2) then
    QrAluPagtosVENCIDO.Value := QrAluPagtosVencimento.Value;
//  if QrAluPagtosVencimento.Value < Date then
//    QrAluPagtosNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrAluPagtosVencimento.Value)
//  else QrAluPagtosNOMEVENCIDO.Value := CO_VAZIO;
  if QrAluPagtosVENCIDO.Value > 0 then
     QrAluPagtosATRAZODD.Value := Trunc(Date) - QrAluPagtosVENCIDO.Value
  else QrAluPagtosATRAZODD.Value := 0;
  if QrAluPagtosATRAZODD.Value > 0 then QrAluPagtosATUALIZADO.Value :=
  (QrAluPagtosATRAZODD.Value * QrAluPagtosMoraDia.Value) +
  QrAluPagtosMulta.Value else QrAluPagtosATUALIZADO.Value := 0;
  if QrAluPagtosSit.Value<2 then QrAluPagtosATUALIZADO.Value :=
  QrAluPagtosATUALIZADO.Value + QrAluPagtosSALDO.Value;
end;

procedure TFmPrincipal.Resetar21Click(Sender: TObject);
begin
  Timer3.Enabled := False;
  ResetaDadosComponentes(StringGrid1);
  Timer3.Enabled := True;
end;

procedure TFmPrincipal.ResetaDadosComponentes(MinhaGrade: TStringGrid);
var
  i, c: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
begin
  for i := 1 to MinhaGrade.RowCount-1 do
  begin
    if MinhaGrade.Cells[0, i] = 'TDBGrid' then
    begin
      c := StrToInt(MinhaGrade.Cells[3, i]);
      Grade := TDBGrid(FindComponent(MinhaGrade.Cells[1, i]));
      Grade.Columns[c].Width := StrToInt(MinhaGrade.Cells[2, i]);
      Grade.Columns[c].FieldName := MinhaGrade.Cells[4, i];
      Grade.Columns[c].Title.Caption := MinhaGrade.Cells[5, i];
    end;
    if MinhaGrade.Cells[0, i] = 'TPanel' then
    begin
      if TPanel(FindComponent(MinhaGrade.Cells[1, i])).Name = '' then
        Geral.MB_Erro('ERRO! Painel sem nome!')
      else
      begin
        Painel := TPanel(FindComponent(MinhaGrade.Cells[1, i]));
        Painel.Width := StrToInt(MinhaGrade.Cells[2, i]);
      end;
    end;
  end;
  MLAGeral.ResetaConfiguiracoesIniciais(True, Aplicar1.Checked);
  SalvaInfoRegEdit(1, nil);
end;

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  ReopenHoje;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
const
  ShowExcel = False;
var
  Tempo: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  Tempo := Now();
  try
    case RGFormaExporta.ItemIndex of
      0: UnDmkExcel.SaveAsExcelFileA(GradeA, 'Planilha A', EdXLSName.Text);
      1: UnDmkExcel.SaveAsExcelFile_B(GradeA, EdXLSName.Text);
      2: UnDmkExcel.StringGridToExcelSheet(GradeA, 'Planilha C', EdXLSName.Text, ShowExcel);
    end;
    Tempo := Now() - Tempo;
    Geral.MB_Info('Tempo: ' + Geral.FDT(Tempo, 100));
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.ReopenHoje;
begin
  QrHoje.Close;
  QrHoje.Params[0].AsInteger := DayOfWeek(TPAula.Date)-1;
  QrHoje.Open;
end;

procedure TFmPrincipal.Timer3Timer(Sender: TObject);
begin
  GradeHoje.Refresh;
end;

procedure TFmPrincipal.TmSuporteTime(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte,
    AdvToolBarButton13, BalloonHint1);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(ATB_Atualiza, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.GradeHojeDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
  Fon: Boolean;
  Hora: TDateTime;
begin
  Hora := Time;
  if (Column.FieldName = 'HIni')
  or (Column.FieldName = 'HFim') then
  begin
    if QrHojeAtiva.Value = 1 then Cor := clGray else
    begin
      if int(Date) <> int(TPAula.Date) then Cor := clBlack else
      if QrHojeHIni.Value > Hora then Cor := clBlue else
      if QrHojeHFim.Value < Hora then Cor := clRed  else
      Cor := clGreen;
      //if Cor <> clGreen then Fon := False else Fon := True;
    end;
    Fon := True;
    with GradeHoje.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
  EdAtz.Text := TimeToStr(Now);
end;

procedure TFmPrincipal.QrHojeAfterScroll(DataSet: TDataSet);
begin
  QrHojeAlunos.Close;
  QrHojeAlunos.Params[0].AsInteger := QrHojeTURMA.Value;
  QrHojeAlunos.Open;
  QrHojeAlunos.Locate('Aluno', FHojeAluno, []);
  //
  ReopenHojeCham(0);
end;

procedure TFmPrincipal.ReopenHojeCham(Codigo: Integer);
begin
  QrHojeCham.Close;
  QrHojeCham.Params[0].AsInteger := QrHojeTURMA.Value;
  QrHojeCham.Open;
end;

procedure TFmPrincipal.QrHojeBeforeClose(DataSet: TDataSet);
begin
  QrHojeAlunos.Close;
  QrHojeCham.Close;
end;

procedure TFmPrincipal.BtLista2Click(Sender: TObject);
var
  i: integer;
begin
  i := MLAGeral.BoolToInt2(CkAlunosAtivos.Checked, 1, 0) +
       MLAGeral.BoolToInt2(CkAlunosInativos.Checked, 2, 0);
  if i > 0 then
  begin
    QrAlunos2.Close;
    QrAlunos2.Params[0].AsInteger := MLAGeral.BoolToInt2(CkAlunosAtivos.Checked, 1, -1000);
    QrAlunos2.Params[1].AsInteger := MLAGeral.BoolToInt2(CkAlunosInativos.Checked, 0, -1000);
    QrAlunos2.Open;
    //
    MyObjects.frxDefineDataSets(frxAlunos2, [
      DmodG.frxDsDono,
      frxDsAlunos2
      ]);
    //
    MyObjects.frxMostra(frxAlunos2, 'Rela��o de Alunos');
  end else
    Geral.MB_Erro('Defina pelo menos um status (ativo, inativo)!');
end;

procedure TFmPrincipal.QrAluPgTotCalcFields(DataSet: TDataSet);
begin
  QrAluPgTotMENSAL2.Value := UFinanceiro.Mensal2(QrAluPgTotMes2.Value, QrAluPgTotAno.Value);

  if QrAluPgTotCompensado.Value = 0 then
     QrAluPgTotCOMPENSADOREAL.Value := ''
  else
     QrAluPgTotCOMPENSADOREAL.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrAluPgTotCompensado.Value);
////////////////////////////////////////////////////////////////////////////////
  QrAluPgTotNOMESIT.Value := UFinanceiro.NomeSitLancto(QrAluPgTotSit.Value,
                               QrAluPgTotTipo.Value, QrAluPgTotPrazo.Value,
                               QrAluPgTotVencimento.Value, QrAluPgTotReparcel.Value);
  //
  QrAluPgTotNOMERELACIONADO.Value := UFinanceiro.NomeRelacionado(QrAluPgTotCliente.Value,
    QrAluPgTotFornecedor.Value, QrAluPgTotNOMECLIENTE.Value, QrAluPgTotNOMEFORNECEDOR.Value);
  //
  case QrAluPgTotSit.Value of
    0: QrAluPgTotSALDO.Value := QrAluPgTotCredito.Value - QrAluPgTotDebito.Value;
    1: QrAluPgTotSALDO.Value := (QrAluPgTotCredito.Value - QrAluPgTotDebito.Value) + QrAluPgTotPago.Value;
    else QrAluPgTotSALDO.Value := 0;
  end;

  case QrAluPgTotTipo.Value of
    0: QrAluPgTotNOMETIPO.Value := CO_CAIXA;
    1: QrAluPgTotNOMETIPO.Value := CO_BANCO;
    2: QrAluPgTotNOMETIPO.Value := CO_EMISS;
  end;
  if (QrAluPgTotVencimento.Value < Date) and (QrAluPgTotSit.Value<2) then
    QrAluPgTotVENCIDO.Value := QrAluPgTotVencimento.Value;
//  if QrAluPgTotVencimento.Value < Date then
//    QrAluPgTotNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrAluPgTotVencimento.Value)
//  else QrAluPgTotNOMEVENCIDO.Value := CO_VAZIO;
  if QrAluPgTotVENCIDO.Value > 0 then
     QrAluPgTotATRAZODD.Value := Trunc(Date) - QrAluPgTotVENCIDO.Value
  else QrAluPgTotATRAZODD.Value := 0;
  if QrAluPgTotATRAZODD.Value > 0 then QrAluPgTotATUALIZADO.Value :=
  (QrAluPgTotATRAZODD.Value * QrAluPgTotMoraDia.Value) +
  QrAluPgTotMulta.Value else QrAluPgTotATUALIZADO.Value := 0;
  if QrAluPgTotSit.Value<2 then QrAluPgTotATUALIZADO.Value :=
  QrAluPgTotATUALIZADO.Value + QrAluPgTotSALDO.Value;
end;

procedure TFmPrincipal.CkAluPagtosQClick(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(True, True, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.CkAluPagtosAClick(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(True, True, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.CkAluPagtosVClick(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(True, True, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.BtQuita2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita2, BtQuita2);
end;

procedure TFmPrincipal.BtImprime3Click(Sender: TObject);
begin
  case PageControl6.ActivePageIndex of
    0:
    begin
      MyObjects.frxDefineDataSets(frxPagtos, [
        DmodG.frxDsDono,
        frxDsAlunos,
        frxDsAluPagtos
        ]);
      //
      MyObjects.frxMostra(frxPagtos, 'Pagamentos de alunos');
    end;
    1:
    begin
      Geral.MB_Aviso('Relat�rio n�o implementado! Solicite � DERMATEK!');//MyObjects.frxMostra(frxPagtos1, 'Pagamentos de alunos');
    end;
    2:
    begin
      MyObjects.MostraPopUpDeBotao(PMAluPagPer, BtImprime3);
    end;
  end;
end;


procedure TFmPrincipal.Desfazercompensao1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  EntCliInt, CodCliInt, Parc1, Parc2, Parc3: Integer;
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      EntCliInt := QrAlunosEmpresa.Value;
      CodCliInt := QrAlunosCodCliInt.Value;
      //
      if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
      //
      {$IFDEF DEFINE_VARLCT}
      DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
      {$ELSE}
      TbLctA := VAR_LCT;
      {$ENDIF}
      //
      case PageControl6.ActivePageIndex of
        0:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT * ',
            'FROM carteiras ',
            'WHERE Codigo=' + Geral.FF0(QrAluPagtosCarteira.Value),
            '']);
          UFinanceiro.ReverterPagtoEmissao(QrAluPagtos, Qry, True, True, False, TbLctA);
        end;
        1:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT * ',
            'FROM carteiras ',
            'WHERE Codigo=' + Geral.FF0(QrAluPgTotCarteira.Value),
            '']);
            UFinanceiro.ReverterPagtoEmissao(QrAluPgTot, Qry, True, True, False, TbLctA);
        end;
      end;
      if (PageControl6.ActivePageIndex in [0,1]) and (QrAluPagtos.State = dsBrowse) then
      begin
        Parc1 := QrTurMatriPagtosFatParcela.Value;
        //
        if QrAluPagtos.State = dsBrowse then
          Parc2 := QrAluPagtosFatParcela.Value
        else
          Parc2 := 0;
        //
        if QrAluPgTot.State = dsBrowse then
          Parc3 := QrAluPgTotFatParcela.Value
        else
          Parc3 := 0;
        //
        if QrTurMatriPagtos.State = dsBrowse then
        begin
          ReopenTurMatriPagtos(QrMatriculadosEmpresa.Value, QrMatriculadosCodCliInt.Value);
          //
          if Parc1 <> 0 then
            QrTurMatriPagtos.Locate('FatParcela', Parc1, []);
        end;
        if QrAluPagtos.State = dsBrowse then
        begin
          ReopenAluPagtos(True, True, True, EntCliInt, CodCliInt);
          //
          if Parc2 <> 0 then
            QrAluPagtos.Locate('FatParcela', Parc2, []);
          if Parc3 <> 0 then
            QrAluPgTot.Locate('FatParcela', Parc3, []);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmPrincipal.PMQuita2Popup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0);
  Enab2 := PageControl6.ActivePageIndex in [0,1];
  //
  CompensarnacontacorrenteBanco1.Enabled := Enab and Enab2;
  PagarRolarcomemissochequeetc1.Enabled  := Enab and Enab2;
  Desfazercompensao1.Enabled             := Enab and Enab2;
end;

procedure TFmPrincipal.RGAluPgTotOrdemClick(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(True, False, False, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.QrAluPgPerCalcFields(DataSet: TDataSet);
var
  Multa, Mora: Double;
begin
  QrAluPgPerVALOR_TXT.Value := Geral.FFT(QrAluPgPerCredito.Value, 2, siNegativo);
////////////////////////////////////////////////////////////////////////////////
  if QrAluPgPerCompensado.Value = 0 then
     QrAluPgPerCOMPENSADOREAL.Value := ''
  else
     QrAluPgPerCOMPENSADOREAL.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrAluPgPerCompensado.Value);
////////////////////////////////////////////////////////////////////////////////
  (*
  if QrAluPgPerSit.Value = -1 then
     QrAluPgPerNOMESIT.Value := CO_IMPORTACAO
  else
    if QrAluPgPerSit.Value = 1 then
     QrAluPgPerNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrAluPgPerSit.Value = 2 then
       QrAluPgPerNOMESIT.Value := CO_QUITADA
  else
    if QrAluPgPerSit.Value = 3 then
       QrAluPgPerNOMESIT.Value := CO_COMPENSADA
  else
    if QrAluPgPerVencimento.Value < Date then
       QrAluPgPerNOMESIT.Value := CO_VENCIDA
  else
       QrAluPgPerNOMESIT.Value := CO_EMABERTO;
  case QrAluPgPerSit.Value of
    0: QrAluPgPerSALDO.Value := QrAluPgPerCredito.Value - QrAluPgPerDebito.Value;
    1: QrAluPgPerSALDO.Value := (QrAluPgPerCredito.Value - QrAluPgPerDebito.Value) + QrAluPgPerPago.Value;
    else QrAluPgPerSALDO.Value := 0;
  end;
  *)

  QrAluPgPerNOMESIT.Value := UFinanceiro.NomeSitLancto(QrAluPgPerSit.Value,
                               QrAluPgPerTipo.Value, QrAluPgPerPrazo.Value,
                               QrAluPgPerVencimento.Value, QrAluPgPerReparcel.Value);


  case QrAluPgPerTipo.Value of
    0: QrAluPgPerNOMETIPO.Value := CO_CAIXA;
    1: QrAluPgPerNOMETIPO.Value := CO_BANCO;
    2: QrAluPgPerNOMETIPO.Value := CO_EMISS;
  end;
  if (QrAluPgPerVencimento.Value < Date) and (QrAluPgPerSit.Value<2) then
    QrAluPgPerVENCIDO.Value := QrAluPgPerVencimento.Value;
//  if QrAluPgPerVencimento.Value < Date then
//    QrAluPgPerNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrAluPgPerVencimento.Value)
//  else QrAluPgPerNOMEVENCIDO.Value := CO_VAZIO;
  if QrAluPgPerVENCIDO.Value > 0 then
  begin
     QrAluPgPerATRAZODD.Value := Trunc(Date) - QrAluPgPerVENCIDO.Value;
     Multa := int(Dmod.QrControleMulta.Value*QrAluPgPerCredito.Value)/100;
     Mora  := int(Dmod.QrControleMoraDD.Value*QrAluPgPerATRAZODD.Value*
              QrAluPgPerCredito.Value)/100;
     QrAluPgPerACRESCIMOS.Value := 'Ap�s vencido, o valor foi acrescido de '+
     'multa de '+FloatToStr(Dmod.QrControleMulta.Value)+'% ('+VAR_MOEDA+
     Geral.FFT(Multa, 2, siPositivo)+')e mora de '+
     FloatToStr(Dmod.QrControleMoraDD.Value)+'% ao dia ('+VAR_MOEDA+
     Geral.FFT(Mora, 2, siPositivo)+'),  totalizando '+VAR_MOEDA+
     Geral.FFT(Mora+Multa+QrAluPgPerCredito.Value, 2, siPositivo)+'.'+
     ' Valores v�lidos para hoje '+FormatDateTime(VAR_FORMATDATE2, Date)+'.';
     QrAluPgPerVENCER_TXT.Value := 'venceu';
  end else begin
    QrAluPgPerATRAZODD.Value := 0;
     QrAluPgPerACRESCIMOS.Value := 'Ap�s o vencimento, acarretar� multa de '+
     FloatToStr(Dmod.QrControleMulta.Value)+'% e mora de '+
     FloatToStr(Dmod.QrControleMoraDD.Value)+'% ao dia.';
     QrAluPgPerVENCER_TXT.Value := 'vencer�';
  end;
  if QrAluPgPerATRAZODD.Value > 0 then QrAluPgPerATUALIZADO.Value :=
  (QrAluPgPerATRAZODD.Value * QrAluPgPerMoraDia.Value) +
  QrAluPgPerMulta.Value else QrAluPgPerATUALIZADO.Value := 0;
  if QrAluPgPerSit.Value<2 then QrAluPgPerATUALIZADO.Value :=
  QrAluPgPerATUALIZADO.Value + QrAluPgPerSALDO.Value;
  //
end;

procedure TFmPrincipal.TPAPPIniChange(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(False, False, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.TPAPPIniClick(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(False, False, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.TPAPPFimChange(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(False, False, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.TPAPPFimClick(Sender: TObject);
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
    ReopenAluPagtos(False, False, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKey('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString, HKEY_LOCAL_MACHINE);
  end;
end;

procedure TFmPrincipal.Limpar1Click(Sender: TObject);
begin
  ImgPrincipal.Picture := nil;
  Geral.WriteAppKey('ImagemFundo', Application.Title, '', ktString, HKEY_LOCAL_MACHINE);
end;

procedure TFmPrincipal.Lista1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxPagtos2, [
    DmodG.frxDsDono,
    frxDsAluPgPer
    ]);
  //
  MyObjects.frxMostra(frxPagtos2, 'Pagamentos de alunos');
end;

procedure TFmPrincipal.Avisos1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxAvisos2, [
    DmodG.frxDsDono,
    frxDsAluPgPer
    ]);
  //
  MyObjects.frxMostra(frxAvisos2, 'Avisos');
end;

procedure TFmPrincipal.BtMatricula10Click(Sender: TObject);
begin
  CadastroMatriculas(0, QrMatriRenVenCodigo.Value);
end;

procedure TFmPrincipal.BtMatricula2Click(Sender: TObject);
begin
  CadastroMatriculas(0, QrMatriculadosMATRICULA.Value);
end;

procedure TFmPrincipal.BtMatricula3Click(Sender: TObject);
begin
  CadastroMatriculas(0, QrAluMatriCodigo.Value);
  ReopenAlunosMatric;
end;

procedure TFmPrincipal.CkAluMatriAtivoClick(Sender: TObject);
begin
  ReopenAlunosMatric;
end;

procedure TFmPrincipal.CkAluMatriInatiClick(Sender: TObject);
begin
  ReopenAlunosMatric;
end;

procedure TFmPrincipal.EdAlunoChange(Sender: TObject);
begin
  if QrAlunos.State = dsBrowse then QrAlunos.Locate('NOMEALUNO', EdAluno.Text,
    [loPartialKey, loCaseInsensitive]);
end;

procedure TFmPrincipal.TPAulaChange(Sender: TObject);
begin
  ReopenHoje;
end;

procedure TFmPrincipal.QrHojeChamAfterScroll(DataSet: TDataSet);
begin
  ReopenQrHojeChamIts(0);
end;

procedure TFmPrincipal.ReopenQrHojeChamIts(Matricula: Integer);
begin
  QrHojeChamIts.Close;
  QrHojeChamIts.Params[0].AsInteger := QrHojeChamCodigo.Value;
  QrHojeChamIts.Open;
  //
  QrHojeChamIts.Locate('Matricula', Matricula, []);
end;

procedure TFmPrincipal.BtImprime4Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChamada, FmChamada, afmoNegarComAviso) then
  begin
    FmChamada.ShowModal;
    FmChamada.Destroy;
  end;  
end;

procedure TFmPrincipal.BtChamadaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChamadasNew, FmChamadasNew, afmoNegarComAviso) then
  begin
    FmChamadasNew.EdTurma.Text     := Geral.FF0(QrHojeTURMA.Value);
    FmChamadasNew.CBTurma.KeyValue := QrHojeTURMA.Value;
    //
    FmChamadasNew.EdCurso.Text     := Geral.FF0(QrHojeCurso.Value);
    FmChamadasNew.CBCurso.KeyValue := QrHojeCurso.Value;
    //
    FmChamadasNew.EdSala.Text     := Geral.FF0(QrHojeSala.Value);
    FmChamadasNew.CBSala.KeyValue := QrHojeSala.Value;
    //
    FmChamadasNew.EdProfessor.Text     := Geral.FF0(QrHojeProfessor.Value);
    FmChamadasNew.CBProfessor.KeyValue := QrHojeProfessor.Value;
    //
    FmChamadasNew.EdHoraI.Text   := MLAGeral.THT(FormatDateTime(VAR_FORMATTIME, QrHojeHIni.Value));
    FmChamadasNew.EdHoraF.Text   := MLAGeral.THT(FormatDateTime(VAR_FORMATTIME, QrHojeHFim.Value));
    //
    FmChamadasNew.TPData .Date := TPAula.Date;
    //
    FmChamadasNew.ShowModal;
    ReopenHojeCham(FmChamadasNew.FCodigo);
    FmChamadasNew.Destroy;
  end;
end;

procedure TFmPrincipal.QrHojeChamItsCalcFields(DataSet: TDataSet);
begin
  if QrHojeChamItsPresenca.Value > 0.001 then QrHojeChamItsPresente.Value := 1
  else QrHojeChamItsPresente.Value := 0;
end;

procedure TFmPrincipal.DBGrid15DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Presente' then
    MeuVCLSkin.DrawGridPercent(DBGrid15, Rect, 1, QrHojeChamItsPresenca.Value,
    QrHojeChamCarga.Value);
end;

procedure TFmPrincipal.QrAlunos2CalcFields(DataSet: TDataSet);
begin
  QrAlunos2SEQ.Value := QrAlunos2.RecNo;
end;

procedure TFmPrincipal.QrHojeChamBeforeClose(DataSet: TDataSet);
begin
  QrHojeChamIts.Close;
end;

procedure TFmPrincipal.BtExclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma e exclus�o da aula do dia ' +
    FormatDateTime(VAR_FORMATDATE2, QrHojeChamData.Value) + ' e todas as suas ' +
    'presen�as?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM chamadasits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrHojeChamCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM chamadas WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrHojeChamCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenHojeCham(0);
  end;
end;

procedure TFmPrincipal.BtIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChamadasIts, FmChamadasIts, afmoNegarComAviso) then
  begin
    FmChamadasIts.FCodigo        := QrHojeChamCodigo.Value;
    FmChamadasIts.FMaxMin        := QrHojeChamCarga.Value;
    FmChamadasIts.FTurma         := QrHojeTURMA.Value;
    FmChamadasIts.LaTipo.Caption := CO_INCLUSAO;
    FmChamadasIts.ShowModal;
    ReopenQrHojeChamIts(FmChamadasIts.QrAlunosMATRICULA.Value);
    FmChamadasIts.Destroy;
  end;  
end;

procedure TFmPrincipal.BtExclui2Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma e exclus�o de ' + QrHojeChamItsNOMEALUNO.Value +
    ' da aula do dia ' + FormatDateTime(VAR_FORMATDATE2, QrHojeChamData.Value) +
    'presen�as?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM chamadasits ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Matricula=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrHojeChamCodigo.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrHojeChamItsMatricula.Value;
    Dmod.QrUpd.ExecSQL;
    ReopenHojeCham(0);
  end;
end;

procedure TFmPrincipal.BtAlteraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmChamadasIts, FmChamadasIts, afmoNegarComAviso) then
  begin
    FmChamadasIts.FCodigo          := QrHojeChamCodigo.Value;
    FmChamadasIts.FTurma           := QrHojeTURMA.Value;
    FmChamadasIts.FMaxMin          := QrHojeChamCarga.Value;
    FmChamadasIts.EdMinutos.Text   := Geral.FF0(QrHojeChamItsPresenca.Value);
    FmChamadasIts.EdAluno.Text     := Geral.FF0(QrHojeChamItsALUNO.Value);
    FmChamadasIts.CBAluno.KeyValue := QrHojeChamItsALUNO.Value;
    FmChamadasIts.LaTipo.Caption   := CO_ALTERACAO;
    FmChamadasIts.EdAluno.Enabled  := False;
    FmChamadasIts.CBAluno.Enabled  := False;
    //
    FmChamadasIts.ShowModal;
    ReopenQrHojeChamIts(FmChamadasIts.QrAlunosMATRICULA.Value);
    FmChamadasIts.Destroy;
  end;  
end;

procedure TFmPrincipal.QrHojeChamAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := Geral.IntToBool_0(QrHojeCham.RecordCount);
end;

procedure TFmPrincipal.QrHojeChamItsAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := Geral.IntToBool_0(QrHojeChamIts.RecordCount);
  BtExclui2.Enabled := BtAltera.Enabled;
end;

(*
procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := CliInt;
  DmodG.QrCliIntUni.Open;
  //
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
  DModG.DefineDataMinima(FEntInt);
  //
  DmLct2.QrCrt.Close;
  DmLct2.QrLct.Close;
end;
*)

procedure TFmPrincipal.CadastroDeDepartamentos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmDepartamentos, FmDepartamentos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmDepartamentos.LocCod(Codigo, Codigo);
    FmDepartamentos.ShowModal;
    FmDepartamentos.Destroy;
  end;  
end;

procedure TFmPrincipal.Departamentos2Click(Sender: TObject);
begin
  CadastroDeDepartamentos(0);
end;

procedure TFmPrincipal.EdEmpresa2Change(Sender: TObject);
begin
  ReopenCaixa();
end;

procedure TFmPrincipal.PageControl1Change(Sender: TObject);
var
  Maximiza: TDefTruOrFalse;
begin
  if PageControl1.ActivePageIndex = 0 then
    Maximiza := deftfTrue
  else
    Maximiza := deftfFalse;
  //
  MyObjects.MaximizaAdvToolBarPager(AdvToolBarPager1, Maximiza);
  //
  if (PageControl1.ActivePageIndex in [7, 8]) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
    CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
    EdEmpresa2.ValueVariant := DmodG.QrFiliLogFilial.Value;
    CBEmpresa2.KeyValue     := DmodG.QrFiliLogFilial.Value;
  end;
end;

procedure TFmPrincipal.Opes2Click(Sender: TObject);
begin
  CadastroDeOpcoes;
end;

procedure TFmPrincipal.Reabrirtabelas1Click(Sender: TObject);
begin
  //MLAGeral.ReabrirtabelasFormAtivo(Sender);
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa
end;

procedure TFmPrincipal.Contas2Click(Sender: TObject);
begin
  FinanceiroJan.CadastroDeContas(0);
end;

procedure TFmPrincipal.Perodos1Click(Sender: TObject);
begin
  CadastroDePeriodos(0);
end;

procedure TFmPrincipal.CadastroDePeriodos(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPeriodos, FmPeriodos, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPeriodos.LocCod(Codigo, Codigo);
    FmPeriodos.ShowModal;
    FmPeriodos.Destroy;
  end;  
end;

procedure TFmPrincipal.CadastroDeGruposDeTurmas(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmTurmasGru, FmTurmasGru, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmTurmasGru.LocCod(Codigo, Codigo);
    FmTurmasGru.ShowModal;
    FmTurmasGru.Destroy;
  end;  
end;

procedure TFmPrincipal.ListadeTurmas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTurmasImp, FmTurmasImp, afmoNegarComAviso) then
  begin
    FmTurmasImp.ShowModal;
    FmTurmasImp.Destroy;
  end;  
end;

procedure TFmPrincipal.QrAlunosAtivosAfterOpen(DataSet: TDataSet);
begin
  // ??
  QrMatriculasAtivas.Close;
  QrMatriculasAtivas.Open;
  //StatusBar.Panels[11].Text := Geral.FF0(QrAlunosAtivos.RecordCount)+'/'+
  //FloatToStr(QrMatriculasAtivasQtdMatriculas.Value);
end;

procedure TFmPrincipal.AtualizaAlunosAtivos;
begin
  FAtivosAtualizados := true;
  QrAlunosAtivos.Close;
  QrAlunosAtivos.Open;
end;

procedure TFmPrincipal.VerificaBDServidor1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoLiberado) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaCadastroDeMatricula(Aluno: Integer);
var
  Empresa: Integer;
begin
  if FMatriculaNaoAutomatica then Exit;
  QrMatriculado.Close;
  QrMatriculado.Params[0].AsInteger := Aluno;
  QrMatriculado.Open;
  //
  if (QrMatriculadoCliente1.Value = 'V')
  and (QrMatriculadoCodigo.Value = 0) then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, 0);
    //
    Empresa := DmodG.QrFiliLogCodigo.Value;
    //
    if Empresa <> 0 then
    begin
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('INSERT INTO matricul SET ');
      Dmod.QrUpdU.SQL.Add('Aluno=:P0, DataC=:P1, Empresa=:P2, Ativo=:P3, ');
      Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz');
      Dmod.QrUpdU.Params[00].AsInteger := Aluno;
      Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdU.Params[02].AsInteger := Empresa;
      Dmod.QrUpdU.Params[03].AsInteger := 1;
      //
      Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
      Dmod.QrUpdU.Params[06].AsInteger := Aluno;
      Dmod.QrUpdU.ExecSQL;
      UMyMod.UpdUnlockY(Aluno, Dmod.MyDB, 'Matricul', 'Codigo');
    end;
  end;
end;

procedure TFmPrincipal.VerificaDBWeb1Click(Sender: TObject);
begin
  DmkWeb_Jan.MostraVerifiDBi(DmodG.QrOpcoesGerl, Dmod.MyDBn, VAR_VERIFI_DB_CANCEL);
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao: Integer;
  Arq: String;
begin
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Academy', 'Academy',
    Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO, CO_DMKID_APP,
    DModG.ObtemAgora(), Memo1, dtExec, Versao, Arq, False, ApenasVerifica,
    BalloonHint1);
end;

procedure TFmPrincipal.VerificaTabelasterceiros1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

procedure TFmPrincipal.Gruposde1Click(Sender: TObject);
begin
  CadastroDeGruposDeTurmas(0);
end;

procedure TFmPrincipal.BtRefresh10Click(Sender: TObject);
begin
  AtualizaAtivosEVencidos();
end;

procedure TFmPrincipal.BtRefresh1Click(Sender: TObject);
begin
  ReopenMensVencidas;
end;

procedure TFmPrincipal.ReopenMensVencidas;
var
  Data: String;
begin
  Data := FormatDateTime(VAR_FORMATDATE, TPMensVence.Date);
  //
  QrMensVencidas.Close;
  QrMensVencidas.Params[00].AsString := Data;
  QrMensVencidas.Params[01].AsString := Data;
  QrMensVencidas.Params[02].AsString := Data;
  QrMensVencidas.Open;
end;

procedure TFmPrincipal.TPMensVenceChange(Sender: TObject);
begin
  ReopenMensVencidas;
end;

procedure TFmPrincipal.BtMatricula4Click(Sender: TObject);
begin
  CadastroMatriculas(0, QrMensVencidasCodigo.Value);
end;

(*
N�o est� sendo utilizado pelo cliente caso precise reativar usar a grade

procedure TFmPrincipal.CadastroProdutos(Controlados: Boolean);
begin
  FProdutosControlados := Controlados;
  if DBCheck.CriaFm(TFmProdutos, FmProdutos, afmoNegarComAviso) then
  begin
    if Controlados then FmProdutos.FTitulo := 'Mercadorias Controladas'
    else FmProdutos.FTitulo := 'Mercadorias N�o Controladas';
    FmProdutos.ShowModal;
    FmProdutos.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroGruposProdutos();
begin
  if DBCheck.CriaFm(TFmProdutosG, FmProdutosG, afmoNegarComAviso) then
  begin
    FmProdutosG.ShowModal;
    FmProdutosG.Destroy;
  end;
end;

procedure TFmPrincipal.EntradaProdutos();
begin
  if DBCheck.CriaFm(TFmProdutosC, FmProdutosC, afmoNegarComAviso) then
  begin
    FmProdutosC.ShowModal;
    FmProdutosC.Destroy;
  end;
end;

procedure TFmPrincipal.InicialProdutos();
begin
  if DBCheck.CriaFm(TFmProdutosI, FmProdutosI, afmoNegarComAviso) then
  begin
    FmProdutosI.ShowModal;
    FmProdutosI.Destroy;
  end;
end;

procedure TFmPrincipal.BalancosMercadorias;
begin
  if DBCheck.CriaFm(TFmProdutosBal, FmProdutosBal, afmoNegarComAviso) then
  begin
    FmProdutosBal.ShowModal;
    FmProdutosBal.Destroy;
  end;
end;

procedure TFmPrincipal.ImprimeMercadorias;
begin
  if DBCheck.CriaFm(TFmProdutosImprime, FmProdutosImprime, afmoNegarComAviso) then
  begin
    FmProdutosImprime.ShowModal;
    FmProdutosImprime.Destroy;
  end;
end;

procedure TFmPrincipal.RefreshProdutos;
begin
  if DBCheck.CriaFm(TFmProdutosRefresh, FmProdutosRefresh, afmoNegarComAviso) then
  begin
    FmProdutosRefresh.ShowModal;
    FmProdutosRefresh.Destroy;
  end;
end;

procedure TFmPrincipal.RefreshMercadorias;
begin
  if DBCheck.CriaFm(TFmMercadoriasAtualiza, FmMercadoriasAtualiza, afmoNegarComAviso) then
  begin
    FmMercadoriasAtualiza.ShowModal;
    FmMercadoriasAtualiza.Destroy;
  end;
end;

procedure TFmPrincipal.IncluiVendas;
var
  Venda: Integer;
begin
  Screen.Cursor := crHourGlass;
  Venda := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'VendasPro',
  'VendasPro', 'Codigo');
  if DBCheck.CriaFm(TFmCliPro, FmCliPro, afmoNegarComAviso) then
  begin
    with FmCliPro do
    begin
      EdResponsavelCod.Text  := Geral.FF0(QrCliProdCodigo.Value);
      EdResponsavelNome.Text := QrCliProdNOMECLIENTE.Value;
      EdRecibo.Text          := Geral.FF0(Venda);
      ShowModal;
      Destroy;
      AtualizaSaldoCliente(QrCliProdCodigo.Value);
    end;
  end;
  ReopenCliProd(QrCliProdCodigo.Value);
end;

procedure TFmPrincipal.ExcluiPagtoProduto;
begin
  //
  if Geral.MB_Pergunta('Confirma a exclus�o do pagamento?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM l a n c t o s ');
    Dmod.QrUpd.SQL.Add('WHERE FatID=2105 AND Controle=:P0');
    //
    Dmod.QrUpd.Params[0].AsInteger := QrCliPagControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    AtualizaSaldoCliente(QrCliProdCodigo.Value);
    ReopenCliProd(QrCliProdCodigo.Value);
  end;
end;

procedure TFmPrincipal.ExcluiMercadoria;
var
  Mercadoria: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da mercadoria?') = ID_YES then
  begin
    Mercadoria := QrCliVenProduto.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM vendaspro WHERE Codigo=:P0');
    Dmod.QrUpd.SQL.Add('AND Controle=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrCliVenCodigo.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrCliVenControle.Value;
    Dmod.QrUpd.ExecSQL;
    AtualizaSaldoCliente(QrCliProdCodigo.Value);
    UnAcademy.AtualizaEstoqueMercadoria(Mercadoria, aeMsg, '');
    ReopenCliVen;
    //
    BtAtualizaClick(Self);
  end;
end;

procedure TFmPrincipal.ReopenCliProd(Cliente: Integer);
begin
  QrCliProd.Close;
  QrCliProd.Open;
  if Cliente = 0 then EdLocCliChange(Self)
  else QrCliProd.Locate('Codigo', Cliente, []);
end;

procedure TFmPrincipal.AtualizaSaldoCliente(Cliente: Integer);
begin
  QrCliCre.Close;
  QrCliCre.Params[0].AsInteger := Cliente;
  QrCliCre.Open;
  //
  QrCliDeb.Close;
  QrCliDeb.Params[0].AsInteger := Cliente;
  QrCliDeb.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE entidades SET QuantN2=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsFloat   := QrCliCreCredito.Value-QrCliDebValorReal.Value;
  Dmod.QrUpd.Params[1].AsInteger := Cliente;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmPrincipal.ReopenCliPag;
begin
  QrCliPag.Close;
  QrCliPag.Params[0].AsInteger := QrCliProdCodigo.Value;
  QrCliPag.Open;
  //
  //QrCliVen.Last;
end;
*)

procedure TFmPrincipal.EdLocCliChange(Sender: TObject);
begin
  if QrCliProd.State = dsBrowse then QrCliProd.Locate('NOMECLIENTE',
    EdLocCli.Text, [loPartialKey, loCaseInsensitive]);
end;

procedure TFmPrincipal.BtAtualizaClick(Sender: TObject);
begin
  (*
  if QrCliProd.State = dsBrowse then ReopenCliProd(QrCliProdCodigo.Value);
  ReopenCliProd(0);
  *)
end;

procedure TFmPrincipal.BtAutomClick(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  if (QrAluPgTot.State <> dsInactive) and (QrAluPgTot.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM carteiras ',
        'WHERE Codigo=' + Geral.FF0(QrAluPgTotCarteira.Value),
        '']);
      UFinanceiro.QuitacaoAutomaticaDmk(TDmkDBGrid(DBGAluPgTot), QrAluPgTot,
        Qry, FAluPagtos_TabLctA);
      //
      if QrAluPagtos.State = dsBrowse then
      begin
        ReopenAluPagtos(True, True, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
      end;
    finally
      Qry.Free;
    end;
  end;
end;
(*
procedure TFmPrincipal.BtAutomClick(Sender: TObject);
  procedure QuitaAtual();
  begin
    case PageControl6.ActivePageIndex of
      0: CompensarContaCorrenteBanco(2, 2, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
      1: CompensarContaCorrenteBanco(3, 2, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
    end;
  end;
const
  DefJuros = 0.00;
  DefMulta = 0.00;
var
  i, Controle: Integer;
  Jur, Mul: Double;
  DataSel: TDateTime;
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
  begin
    if DBGAluPgTot.SelectedRows.Count > 1 then
    begin
      if Geral.MensagemBox('Confirma a quita��o autom�tica '+
      'dos itens selecionados?', 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
      ID_YES then
      begin
        with DBGAluPgTot.DataSource.DataSet do
        for i:= 0 to DBGAluPgTot.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGAluPgTot.SelectedRows.Items[i]));
          QuitaAtual();
        end;
      end;
    end else QuitaAtual();
    //
    if QrAluPagtos.State = dsBrowse then
    begin
      ReopenAluPagtos(True, True, True, QrAlunosEmpresa.Value, QrAlunosCodCliInt.Value);
      //QrAluPagtos.Locate('FatParcela', Parc2, []);
      //QrAluPgTot.Locate('FatParcela', Parc3, []);
    end;
  end;
end;
*)

procedure TFmPrincipal.QrCliProdCalcFields(DataSet: TDataSet);
begin
  QrCliProdSEQ.Value := QrCliProd.RecNo;
end;

procedure TFmPrincipal.BtMercadoria1Click(Sender: TObject);
begin
  //IncluiVendas;
end;

procedure TFmPrincipal.QrCliProdAfterScroll(DataSet: TDataSet);
begin
  ReopenCliVen;
  //ReopenCliPag;
  if QrCliProdCodigo.Value = -2 then BtDinheiro.Enabled := False
  else BtDinheiro.Enabled := True;
end;

procedure TFmPrincipal.ReopenCliVen;
begin
  QrCliVen.Close;
  QrCliVen.Params[0].AsInteger := QrCliProdCodigo.Value;
  QrCliVen.Open;
  //
  //QrCliVen.Last;
end;

procedure TFmPrincipal.QrCliProdAfterClose(DataSet: TDataSet);
begin
  BtMercadoria1.Enabled := False;
  BtDinheiro.Enabled := False;
end;

procedure TFmPrincipal.QrCliProdAfterOpen(DataSet: TDataSet);
begin
  BtMercadoria1.Enabled := True;
end;

procedure TFmPrincipal.QrCliPagCalcFields(DataSet: TDataSet);
begin
  if QrCliPagSit.Value = 1 then
     QrCliPagNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrCliPagSit.Value = 2 then
       QrCliPagNOMESIT.Value := CO_QUITADA
  else
    if QrCliPagVencimento.Value < Date then
       QrCliPagNOMESIT.Value := CO_VENCIDA
  else
       QrCliPagNOMESIT.Value := CO_EMABERTO;

  case QrCliPagTipo.Value of
    0: QrCliPagNOMETIPO.Value := CO_CAIXA;
    1: QrCliPagNOMETIPO.Value := CO_BANCO;
    2: QrCliPagNOMETIPO.Value := CO_EMISS;
  end;
  //
  QrCliPagCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrCliPagCNPJCPF.Value);
end;

procedure TFmPrincipal.BtDinheiroClick(Sender: TObject);
(*
var
  Terceiro, Cod : Integer;
  Valor: Double;
*)
begin
(*
  IC3_ED_FatNum := QrCliVenCodigo.Value;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Cod           := QrCliVenCodigo.Value;
  Terceiro      := QrCliProdCodigo.Value;
  Valor         := QrCliVenValorReal.Value;

  UCash.Pagto(QrCliPag, tpCred, Cod, Terceiro, VAR_FATID_2105, 0, stIns,
  'Vanda de Mercadorias Diversas', Valor, VAR_USUARIO, 0, FEntInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0);

  AtualizaSaldoCliente(QrCliProdCodigo.Value);
  ReopenCliProd(QrCliProdCodigo.Value);
*)
end;

procedure TFmPrincipal.CadastroDeTarifas(Codigo, Grupo: Integer);
begin
  if DBCheck.CriaFm(TFmTarifas, FmTarifas, afmoNegarComAviso) then
  begin
    FmTarifas.FGrupo := Grupo;
    //
    if Codigo <> 0 then
      FmTarifas.LocCod(Codigo, Codigo);
    FmTarifas.ShowModal;
    FmTarifas.Destroy;
  end;
end;

procedure TFmPrincipal.Tarifas2Click(Sender: TObject);
begin
  CadastroDeTarifas(0, 0);
end;

function TFmPrincipal.InicializaBEMA_NaoFiscal: Integer;
var
  Comando: Integer;
begin
  // driver
  (*CBImpressoras.Items.Assign(Printer.Printers);
  CBImpressoras.ItemIndex := Printer.PrinterIndex;
  CBSelectCodigo.ItemIndex := 0;

  // inicializa o combo Porta de Comunica��o
   If (ComboBox1.ItemIndex) = -1 Then
       ComboBox1.ItemIndex := 0;

  // Abre a porta de comunicacao
  comando := IniciaPorta(Pchar(ComboBox1.Text));*)
  comando := IniciaPorta('LPT1');
  If comando <= 0 Then
  begin
    MessageDlg('Problemas ao abrir a porta de Comunica��o.', mtError, [mbOk], 0 );
    Result := comando;
    Exit;
  end;
  // inicializa o combo ModeloImpressora com MP20 CI
  (*If (ComboModeloImpressora.ItemIndex) = -1 Then
      ComboModeloImpressora.ItemIndex := 0;*)

  // Configura o modelo da impressora para MP20 CI
  comando := ConfiguraModeloImpressora(1);
  if comando = -2 then
  begin
      MessageDlg('Par�metro inv�lido na fun��o "ConfiguraModeloImpressora."', mtError, [mbOk], 0 );
      Result := Comando;
      Exit;
    //ComboModeloImpressora.OnChange(self);
  end;
  Result := comando;
end;

procedure TFmPrincipal.Recibo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmImpReci, FmImpReci, afmoNegarComAviso) then
  begin
    FmImpReci.ShowModal;
    FmImpReci.Destroy;
  end;
end;

procedure TFmPrincipal.BtRefresh2Click(Sender: TObject);
begin
  ReopenCaixa();
end;

procedure TFmPrincipal.ReopenCaixa();
var
  TbLctA, Ini, Fim: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
  EntCliInt, CodCliInt: Integer;
begin
  CodCliInt := EdEmpresa2.ValueVariant;
  EntCliInt := DModG.QrEmpresasCodigo.Value;
  //
  if MyObjects.FIC(CodCliInt = 0, EdEmpresa2, 'Empresa n�o definida!') then Exit;
  //
  if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
  //
  {$IFDEF DEFINE_VARLCT}
  DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //
  Ini := FormatDateTime(VAR_FORMATDATE, TPCaixaIni.Date);
  Fim := FormatDateTime(VAR_FORMATDATE, TPCaixaFim.Date);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaixa, Dmod.MyDB, [
    'SELECT tar.Nome NOMETARIFA, ',
    'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ',
    'ELSE ent.Nome END NOMEALUNO, ren.* ',
    'FROM matriren ren ',
    'LEFT JOIN tarifas   tar ON tar.Codigo=ren.Periodo ',
    'LEFT JOIN matricul  mat ON mat.Codigo=ren.Codigo ',
    'LEFT JOIN entidades ent ON ent.Codigo=mat.Aluno ',
    'WHERE ren.DataCad BETWEEN ' + Ini + ' AND ' + Fim + ' ',
    'AND mat.Empresa=' + Geral.FF0(EntCliInt),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaixa2, Dmod.MyDB, [
    'SELECT SUM(la.Credito) Credito, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
    'ELSE en.Nome END NOMECLIENTE ',
    'FROM ' + TbLctA + ' la ',
    'LEFT JOIN entidades en ON en.Codigo=la.Cliente ',
    'WHERE la.FatID=' + Geral.FF0(VAR_FATID_2105),
    'AND la.Data BETWEEN ' + Ini + ' AND ' + Fim + ' ',
    'GROUP BY la.Cliente ',
    'ORDER BY NOMECLIENTE ',
    '']);
  // Deve ser antes da QrCaixa3
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaixa4, Dmod.MyDB, [
    'SELECT SUM(la.Credito) Credito ',
    'FROM ' + TbLctA + ' la ',
    'WHERE la.FatID=' + Geral.FF0(VAR_FATID_2105),
    'AND la.Data BETWEEN ' + Ini + ' AND ' + Fim + ' ',
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCaixa3, Dmod.MyDB, [
    'SELECT SUM(ren.Taxa) Taxa, SUM(ren.Preco) Preco, ',
    'SUM(ren.Valor) Valor ',
    'FROM matriren ren',
    'LEFT JOIN matricul  mat ON mat.Codigo=ren.Codigo ',
    'WHERE ren.DataCad BETWEEN ' + Ini + ' AND ' + Fim + ' ',
    'AND mat.Empresa=' + Geral.FF0(EntCliInt),
    '']);
end;

procedure TFmPrincipal.TPCaixaFimChange(Sender: TObject);
begin
  ReopenCaixa();
end;

procedure TFmPrincipal.TPCaixaIniChange(Sender: TObject);
begin
  ReopenCaixa();
end;

procedure TFmPrincipal.QrCaixaCalcFields(DataSet: TDataSet);
begin
  QrCaixaTOTAL.Value := QrCaixaTaxa.Value + QrCaixaPreco.Value;
  QrCaixaDESCO.Value := QrCaixaTOTAL.Value - QrCaixaValor.Value;
end;

procedure TFmPrincipal.DBGrid9DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid9, Rect, 1, QrCaixaAtivo.Value);
end;

procedure TFmPrincipal.Entrada2Click(Sender: TObject);
begin
  //EntradaProdutos();
end;

procedure TFmPrincipal.Controlados2Click(Sender: TObject);
begin
  //CadastroProdutos(True);
end;

procedure TFmPrincipal.CorrigeFatID1Click(Sender: TObject);
var
  Query: TmySQLQuery;
  CodCliInt, CodEnti: Integer;
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  Screen.Cursor := crHourGlass;
  Query         := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDb, [
      'SELECT * ',
      'FROM enticliint ',
      '']);
    Query.First;
    while not Query.Eof do
    begin
      CodCliInt := Query.FieldByName('CodCliInt').AsInteger;
      CodEnti   := Query.FieldByName('CodEnti').AsInteger;
      //
      {$IFDEF DEFINE_VARLCT}
      DModG.Def_EM_ABD(TMeuDB, CodEnti, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
      {$ELSE}
      TbLctA := VAR_LCT;
      {$ENDIF}
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + TbLctA);
      Dmod.QrUpd.SQL.Add('SET FatID=FatID+2000');
      Dmod.QrUpd.SQL.Add('WHERE FatID > 0 AND FatID < 2000');
      Dmod.QrUpd.ExecSQL;
      //
      Query.Next;
    end;
    Geral.MensagemBox('FatID atualizados!', 'Informa��o', MB_OK+MB_ICONINFORMATION)
  finally
    Query.Free;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.Corrrigetarifas1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE tarifasits its, tarifas tar');
    Dmod.QrUpd.SQL.Add('SET its.ValPer=its.Preco/tar.QtdePer');
    Dmod.QrUpd.SQL.Add('WHERE its.Codigo=tar.Codigo');
    Dmod.QrUpd.ExecSQL;
    //
    Geral.MensagemBox('Valores por per�odo atualizados conforme pre�o do plano!',
    'Informa��o', MB_OK+MB_ICONINFORMATION)
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.Nocontrolados2Click(Sender: TObject);
begin
  //CadastroProdutos(True);
end;

procedure TFmPrincipal.Grupos3Click(Sender: TObject);
begin
  //CadastroGruposProdutos;
end;

procedure TFmPrincipal.Inicial2Click(Sender: TObject);
begin
  //InicialProdutos;
end;

procedure TFmPrincipal.Refresh2Click(Sender: TObject);
begin
  //RefreshProdutos;
end;

procedure TFmPrincipal.Relatrios3Click(Sender: TObject);
begin
  //ImprimeMercadorias;
end;

procedure TFmPrincipal.TabSheet8Hide(Sender: TObject);
begin
  QrCaixa.Close;
  QrCaixa2.Close;
end;

procedure TFmPrincipal.QrCaixa3CalcFields(DataSet: TDataSet);
begin
  QrCaixa3TOTAL.Value := QrCaixa3Taxa.Value + QrCaixa3Preco.Value;
  QrCaixa3DESCO.Value := QrCaixa3TOTAL.Value - QrCaixa3Valor.Value;
  //
  QrCaixa3MERCA.Value := QrCaixa4Credito.Value;
  QrCaixa3SALDO.Value := QrCaixa3MERCA.Value + QrCaixa3Valor.Value;
end;

procedure TFmPrincipal.BtImprimeClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Este relat�rio ser� impresso na impressora ' +
    'Bematech, e pode ser demorado. Deseja continuar?') = ID_YES then
  begin
    ReopenCaixa();
    if InicializaBEMA_NaoFiscal = 1 then
    begin
      ImprimeFechamentoDia;
      FechaPorta();
    end;
  end;  
end;

function TFmPrincipal.ImprimeFechamentoDia: Boolean;
var
  i, Comando: Integer;
  Text1, Text2: String;
const
  L = Chr(13) + Chr(10);
begin
  Result := True;
  //comando :=
  FormataTX('     ESCOLA DE NATA��O E ACADEMIA'+L, 3, 0,0,0,0);
  //comando :=
  FormataTX('       GYMNASIUM'+L, 2, 0,0,1,0);
  FormataTX(Geral.CompletaString('', '-', 60, taCenter, False)+L, 1, 0,0,0,0);
  FormataTX('MOV.: '+FormatDateTime(VAR_FORMATDATE2, TPCaixaIni.Date)+'-'+
  FormatDateTime(VAR_FORMATDATE2, TPCaixaFim.Date)+L, 2, 0,0,1,0);
  FormataTX(Geral.CompletaString('', '-', 60, taCenter, False)+L, 1, 0,0,0,0);
  //comando :=
  FormataTX('PAGAMENTOS DE PLANOS'+L, 2, 0,0,1,0);
  QrCaixa.First;
  while not QrCaixa.Eof do
  begin
  ////////////////////////////////////////////////////////////////////////////////
    //comando :=
    FormataTX(Copy('CLIENTE: '+QrCaixa2NOMECLIENTE.Value, 1, 60)+L, 1, 0, 0, 0, 0);
    //comando :=
    FormataTX('PER�ODO DO PLANO: '+
      FormatDateTime(VAR_FORMATDATE2, QrCaixaDataI.Value)+' AT� '+
      FormatDateTime(VAR_FORMATDATE2, QrCaixaDataF.Value)+L, 1, 0,0,0,0);
    if QrCaixaTaxa.Value > 0 then
    begin
      Text1 := 'TAXA DE MATR�CULA';
      Text2 := Geral.FFT(QrCaixaTaxa.Value, 2, siPositivo);
      Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
               Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
      //comando :=
      FormataTX(Text1, 1, 0,0,0,0);
    end;
    if QrCaixaPreco.Value > 0 then
    begin
      Text1 := QrCaixaNOMETARIFA.Value;
      Text2 := Geral.FFT(QrCaixaPreco.Value, 2, siPositivo);
      Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
               Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
      //comando :=
      FormataTX(Text1, 1, 0,0,0,0);
    end;
    if QrCaixaTOTAL.Value > 0 then
    begin
      FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
      Text1 := 'TOTAL';
      Text2 := Geral.FFT(QrCaixaTOTAL.Value, 2, siPositivo);
      Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
               Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
      //comando :=
      FormataTX(Text1, 1, 0,0,0,0);
    end;
    if QrCaixaDESCO.Value > 0 then
    begin
      //FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
      Text1 := 'DESCONTO';
      Text2 := Geral.FFT(QrCaixaDESCO.Value, 2, siPositivo);
      Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
               Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
      //comando :=
      FormataTX(Text1, 1, 0,0,0,0);
    end;
    //if QrCaixaValor.Value > 0 then
    //begin
      //FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
      Text1 := 'VALOR PAGO';
      Text2 := Geral.FFT(QrCaixaValor.Value, 2, siPositivo);
      Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
               Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
      //comando :=
      FormataTX(Text1, 1, 0,0,0,0);
      FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
    QrCaixa.Next;
    //end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //comando :=
  FormataTX('PAGAMENTOS DE VENDAS'+L, 2, 0,0,1,0);
  QrCaixa2.First;
  while not QrCaixa2.Eof do
  begin
    Text1 := QrCaixa2NOMECLIENTE.Value;
    Text2 := Geral.FFT(QrCaixa2Credito.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    //comando :=
    FormataTX(Text1, 1, 0,0,0,0);
    QrCaixa2.Next;
  end;
  //comando :=
  FormataTX(' '+L, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  //comando :=
  FormataTX('TOTAIS GERAIS'+L, 2, 0,0,1,0);
  //comando :=
  FormataTX(' '+L, 1, 0,0,0,0);
  Text1 := 'SOMA TAXAS';
  Text2 := Geral.FFT(QrCaixa3Taxa.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'SOMA PLANOS';
  Text2 := Geral.FFT(QrCaixa3Preco.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'TOTAL TAXAS + PLANOS';
  Text2 := Geral.FFT(QrCaixa3TOTAL.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'TOTAL DESCONTOS';
  Text2 := Geral.FFT(QrCaixa3DESCO.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'TOTAL PAGO PLANOS';
  Text2 := Geral.FFT(QrCaixa3Valor.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'TOTAL RECEBIMENTOS VENDAS';
  Text2 := Geral.FFT(QrCaixa4Credito.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'TOTAL GERAL';
  Text2 := Geral.FFT(QrCaixa3SALDO.Value, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 14, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  FormataTX(Text1, 2, 0,0,1,0);
  //////////////////////////////////////////////////////////////////////////////
  FormataTX(Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
  //////////////////////////////////////////////////////////////////////////////
  Text1 := 'EMISS�O: '+FormatDateTime(VAR_FORMATDATE6, Date);
  Text2 := 'HORA: '+FormatDateTime(VAR_FORMATTIME, Now);
  Text1 := Geral.CompletaString(Text1, ' ', 45, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 15, taRightJustify, False)+L;
  //comando :=
  FormataTX(Uppercase(Text1), 1, 0,0,0,0);
  for i := 1 to 12 do comando := FormataTX(''+L, 1, 0,0,0,0);
  if comando = 0 then
  begin
    MessageDlg('Problemas na impress�o do texto formatado.' + #10 +
      'Poss�veis causas: Impressora desligada, off-line ou sem papel',
      mtError, [mbOk], 0 );
    Result := False;
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if DModG <> nil then
    begin
      DModG.MyPID_DB_Cria();
      AtualizaAtivosEVencidos;
      //DefineVarsCliInt(FEntInt);
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
      UFixBugs.MostraFixBugs(['']);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.AtualizaAtivosEVencidos();
var
  Ativos, Total: Integer;
begin
  QrMatriRenAti.Close;
  QrMatriRenAti.Open;
  //
  QrMatriRenVen.Close;
  // 2011-07-16 troquei para sysdate
  //QrMatriRenVen.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, Date);
  QrMatriRenVen.Open;
  //
  QrMatriRenPen.Close;
  QrMatriRenPen.Open;
  //
  Total := QrMatriRenAti.RecordCount;
  Ativos := Total - QrMatriRenVen.RecordCount;
{ TODO : Verificar porque ativos est� igual a zero }
  StatusBar.Panels[11].Text := Geral.FF0(Ativos) + ' de ' + Geral.FF0(Total);
  StatusBar.Panels[13].Text := Geral.FF0(QrMatriRenVen.RecordCount);
  StatusBar.Panels[15].Text := Geral.FF0(QrMatriRenPen.RecordCount);
end;

procedure TFmPrincipal.Refresh3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriRefresh, FmMatriRefresh, afmoNegarComAviso) then
  begin
    FmMatriRefresh.ShowModal;
    FmMatriRefresh.Destroy;
  end;
end;

procedure TFmPrincipal.QrMRPCalcFields(DataSet: TDataSet);
begin
  QrMRPSEQ.Value := QrMRP.Recno;
  QrMRPABERTO.Value := QrMRPACobrar.Value-QrMRPValor.Value;
end;

procedure TFmPrincipal.ListadePendncias1Click(Sender: TObject);
begin
  UMyMod.AbreQuery(QrMRP, DMod.MyDB);
  //
  MyObjects.frxDefineDataSets(frxCxa, [
    DmodG.frxDsDono,
    frxDsMRP
    ]);
  //
  MyObjects.frxMostra(frxMRP, 'Pend�ncias');
end;

procedure TFmPrincipal.DBGrid19KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if (Key=VK_DELETE) and (Shift=[ssCtrl]) then ExcluiMercadoria;
end;

procedure TFmPrincipal.BtImprime5Click(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMImprime5, BtImprime5);
end;

procedure TFmPrincipal.Bematech1Click(Sender: TObject);
begin
  (*
  TbImpReciC.Close;
  TbImpReciR.Close;
  TbImpReciC.Open;
  TbImpReciR.Open;
  if FmPrincipal.InicializaBEMA_NaoFiscal = 1 then
  begin
    ImprimeCupom(0);
    FechaPorta();
  end;
  *)
end;

procedure TFmPrincipal.Matricial1Click(Sender: TObject);
begin
  (*
  TbImpReciC.Close;
  TbImpReciR.Close;
  TbImpReciC.Open;
  TbImpReciR.Open;
  //
  if DBCheck.CriaFm(TFmDotPrint, FmDotPrint, afmoNegarComAviso) then
  begin
    FmDotPrint.FCPI := 10;
    FmDotPrint.RE1.Lines.Clear;
    ImprimeCupom(1);
    FmDotPrint.ShowModal;
    FmDotPrint.Destroy;
  end;
  *)
end;

function TFmPrincipal.ImprimeCupom(Saida: Integer): Boolean;
var
  Comando: Integer;
  Text1, Text2, Text3: String;
  i: Integer;
  Antes, Credito, Debito, Saldo: Double;
const
  L = Chr(13) + Chr(10);
begin
  Result := True;
  Saldo := PendCli(QrCliProdCodigo.Value);
  while not TbImpReciC.Eof do
  begin
    //comando :=
    ImprimeLinhaCupom(Saida, TbImpReciCTexto.Value+L ,
                         TbImpReciCModo.Value,
                         TbImpReciCItal.Value,
                         TbImpReciCSubl.Value,
                         TbImpReciCExpa.Value,
                         TbImpReciCNegr.Value);
    TbImpReciC.Next;
  end;
  Debito:= 0;
  Credito:=0;
  with DBGrid19.DataSource.DataSet do
  for i:= 0 to DBGrid19.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBGrid19.SelectedRows.Items[i]));
    Debito := Debito + QrCliVenValorReal.Value;
  end;
  with GridF.DataSource.DataSet do
  for i:= 0 to GridF.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(GridF.SelectedRows.Items[i]));
    Credito := Credito + QrCliPagCredito.Value;
  end;
  //
  Antes := Saldo-Credito+Debito;
////////////////////////////////////////////////////////////////////////////////
  if Antes <> 0 then
  begin
    Text1 := 'SALDO ANTERIOR';
    Text2 := Geral.FFT(Antes, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    //comando :=
    ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;

////////////////////////////////////////////////////////////////////////////////
  //comando :=
  ImprimeLinhaCupom(Saida,
    Copy('CLIENTE: '+QrCliProdNOMECLIENTE.Value, 1, 60)+L, 1, 0, 0, 0, 0);
  with DBGrid19.DataSource.DataSet do
  for i:= 0 to DBGrid19.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBGrid19.SelectedRows.Items[i]));
    Text1 := QrCliVenNOMEMERCADORIA.Value;
    Text2 := Geral.FFT(QrCliVenQtde.Value, 2, siPositivo);
    Text3 := Geral.FFT(QrCliVenValorReal.Value, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 40, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+
             Geral.CompletaString(Text3, ' ', 10, taRightJustify, False)+L;
    //comando :=
    ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;
  //
  Text1 := 'TOTAL';
  Text2 := Geral.FFT(Debito, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
  //comando :=
  ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  //
  Text1 := 'PAGAMENTO';
  Text2 := Geral.FFT(Credito, 2, siPositivo);
  Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    //comando :=
    ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  //
////////////////////////////////////////////////////////////////////////////////
  if Saldo <> 0 then
  begin
    Text1 := 'SALDO ATUAL';
    Text2 := Geral.FFT(Saldo, 2, siPositivo);
    Text1 := Geral.CompletaString(Text1, '.', 50, taLeftJustify, False)+
             Geral.CompletaString(Text2, ' ', 10, taRightJustify, False)+L;
    //comando :=
    ImprimeLinhaCupom(Saida, Text1, 1, 0,0,0,0);
  end;
////////////////////////////////////////////////////////////////////////////////
  ImprimeLinhaCupom(Saida, Geral.CompletaString('', '-', 60, taCenter, False), 1, 0,0,0,0);
  Text1 := 'EMISS�O: '+FormatDateTime(VAR_FORMATDATE6, Date);
  Text2 := 'HORA: '+FormatDateTime(VAR_FORMATTIME, Now);
  Text1 := Geral.CompletaString(Text1, ' ', 45, taLeftJustify, False)+
           Geral.CompletaString(Text2, ' ', 15, taRightJustify, False)+L;
  comando := ImprimeLinhaCupom(Saida, Uppercase(Text1), 1, 0,0,0,0);
////////////////////////////////////////////////////////////////////////////////
  while not TbImpReciR.Eof do
  begin
    comando := ImprimeLinhaCupom(Saida, TbImpReciRTexto.Value+L ,
                         TbImpReciRModo.Value,
                         TbImpReciRItal.Value,
                         TbImpReciRSubl.Value,
                         TbImpReciRExpa.Value,
                         TbImpReciRNegr.Value);
    TbImpReciR.Next;
  end;
  if comando = 0 then
  begin
    MessageDlg('Problemas na impress�o do texto formatado.' + #10 +
      'Poss�veis causas: Impressora desligada, off-line ou sem papel',
      mtError, [mbOk], 0 );
    Result := False;
  end;
end;

function TFmPrincipal.ImprimeLinhaCupom(Saida: Integer; Texto: String;
TpoLtra, Italic, Sublin, Expand, Enfat: Integer): Integer;
begin
  Result := -High(Integer);
  //
  case Saida of
    0: Result := FormataTX(Texto, TpoLtra, Italic, Sublin, Expand, Enfat);
    1:
    begin
      try
        FmDotPrint.RE1.Lines.Add(Texto);
        Result := 1;
      except
        Result := 2;
      end;
    end;
  end;
end;

function TFmPrincipal.PendCli(Cliente: Integer): Double;
begin
  QrProdPen.Close;
  QrProdPen.Params[0].AsInteger := Cliente;
  QrProdPen.Open;
  //
  QrMatriPen.Close;
  QrMatriPen.Params[0].AsInteger := Cliente;
  QrMatriPen.Open;
  //
  Result := QrCliProdQuantN2.Value + QrMatriPenPENDENTE.Value;
end;

procedure TFmPrincipal.BtRefresh3Click(Sender: TObject);
var
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
  EntCliInt, CodCliInt: Integer;
begin
  CodCliInt := EdEmpresa.ValueVariant;
  EntCliInt := DModG.QrEmpresasCodigo.Value;
  //
  if MyObjects.FIC(CodCliInt = 0, EdEmpresa, 'Empresa n�o definida!') then Exit;
  //
  if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
  //
  {$IFDEF DEFINE_VARLCT}
  DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCxa, Dmod.MyDB, [
    'SELECT DISTINCT ',
    'CASE WHEN en1.Tipo=0 THEN en1.RazaoSocial ',
    'ELSE en1.Nome END NOMEMATRI, ',
    'CASE WHEN en2.Tipo=0 THEN en2.RazaoSocial ',
    'ELSE en2.Nome END NOMEVENDA, ',
    'lan.Credito, lan.FatID, lan.FatNum, ',
    'lan.Controle, mcu.Aluno, lan.Cliente ',
    'FROM ' + TbLctA + ' lan ',
    'LEFT JOIN matriren  mre ON mre.Controle=lan.FatNum ',
    'LEFT JOIN matricul  mcu ON mcu.Codigo=mre.Codigo ',
    'LEFT JOIN entidades en1 ON en1.Codigo=mcu.Aluno ',
    'LEFT JOIN entidades en2 ON en2.Codigo=lan.Cliente ',
    'WHERE lan.FatID IN ('+ Geral.FF0(VAR_FATID_2101) +','+ Geral.FF0(VAR_FATID_2105) +') ',
    'AND lan.Data=' + FormatDateTime(VAR_FORMATDATE, TPCxaData.Date),
    'AND lan.UserCad=' + Geral.FF0(VAR_USUARIO),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCxa, Dmod.MyDB, [
    'SELECT SUM(lan.Credito) Credito ',
    'FROM ' + TbLctA + ' lan ',
    'WHERE lan.FatID IN ('+ Geral.FF0(VAR_FATID_2101) +','+ Geral.FF0(VAR_FATID_2105) +') ',
    'AND lan.DATA=' + FormatDateTime(VAR_FORMATDATE, TPCxaData.Date),
    'AND lan.UserCad=' + Geral.FF0(VAR_USUARIO),
    '']);
end;

procedure TFmPrincipal.TPCxaDataChange(Sender: TObject);
begin
  QrCxa.Close;
end;

procedure TFmPrincipal.QrCxaCalcFields(DataSet: TDataSet);
begin
  case QrCxaFatID.Value of
    2101: QrCxaNOMEALUNO.Value := QrCxaNOMEMATRI.Value;
    2105: QrCxaNOMEALUNO.Value := QrCxaNOMEVENDA.Value;
    else QrCxaNOMEALUNO.Value := '?';
  end;
  case QrCxaFatID.Value of
    2101: QrCxaFONTE.Value := 'Matr�cula';
    2105: QrCxaFONTE.Value := 'Conveni�ncia';
    else QrCxaFONTE.Value := '?';
  end;
end;

procedure TFmPrincipal.BtImprime6Click(Sender: TObject);
begin
  if QrCxa.State = dsBrowse then
  begin
    MyObjects.frxDefineDataSets(frxCxa, [
      DmodG.frxDsMaster,
      frxDsCxa
      ]);
    //
    MyObjects.frxMostra(frxCxa, 'Fechamento de caixa');
  end else
    Geral.MB_Aviso('N�o h� pesquisa a imprimir');
end;

procedure TFmPrincipal.BtExclui4Click(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PmExclui4, BtExclui4);
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKey('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  SkinMenu(TMenuItem(Sender).Tag);
end;

procedure TFmPrincipal.Pagamento1Click(Sender: TObject);
begin
  //ExcluiPagtoProduto;
end;

procedure TFmPrincipal.PagarRolarcomemissochequeetc1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  EntCliInt, CodCliInt, Lancto, Parc1, Parc2, Parc3: Integer;
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
begin
  if (QrAlunos.State <> dsInactive) and (QrAlunos.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      EntCliInt := QrAlunosEmpresa.Value;
      CodCliInt := QrAlunosCodCliInt.Value;
      //
      if (EntCliInt = 0) or (CodCliInt = 0) then Exit;
      //
      {$IFDEF DEFINE_VARLCT}
      DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
      {$ELSE}
      TbLctA := VAR_LCT;
      {$ENDIF}
      //
      case PageControl6.ActivePageIndex of
        0:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT * ',
            'FROM carteiras ',
            'WHERE Codigo=' + Geral.FF0(QrAluPagtosCarteira.Value),
            '']);
          UFinanceiro.PagarRolarEmissao(Qry, QrAluPagtos, TbLctA, True, Lancto);
        end;
        1:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT * ',
            'FROM carteiras ',
            'WHERE Codigo=' + Geral.FF0(QrAluPgTotCarteira.Value),
            '']);
          UFinanceiro.PagarRolarEmissao(Qry, QrAluPgTot, TbLctA, True, Lancto);
        end;
      end;
      if (PageControl6.ActivePageIndex in [0,1]) and (QrAluPagtos.State = dsBrowse) then
      begin
        Parc1 := QrTurMatriPagtosFatParcela.Value;
        //
        if QrAluPagtos.State = dsBrowse then
          Parc2 := QrAluPagtosFatParcela.Value
        else
          Parc2 := 0;
        //
        if QrAluPgTot.State = dsBrowse then
          Parc3 := QrAluPgTotFatParcela.Value
        else
          Parc3 := 0;
        //
        if QrTurMatriPagtos.State = dsBrowse then
        begin
          ReopenTurMatriPagtos(EntCliInt, CodCliInt);
          //
          if Parc1 <> 0 then
            QrTurMatriPagtos.Locate('FatParcela', Parc1, []);
        end;
        if QrAluPagtos.State = dsBrowse then
        begin
          ReopenAluPagtos(True, True, True, EntCliInt, CodCliInt);
          //
          if Parc2 <> 0 then
            QrAluPagtos.Locate('FatParcela', Parc2, []);
          if Parc3 <> 0 then
            QrAluPgTot.Locate('FatParcela', Parc3, []);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmPrincipal.GridFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if (Key=VK_DELETE) and (Shift=[ssCtrl]) then ExcluiPagtoProduto;
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.Mercadoria1Click(Sender: TObject);
begin
  //ExcluiMercadoria;
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade1: TStringGrid;
  Entidade: Integer; AbrirEmAba: Boolean);
begin
  VerificaCadastroDeMatricula(VAR_ENTIDADE);
  AtualizaAlunosAtivos;
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade: TStringGrid);
begin
  //
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.frxAlunos2GetValue(const VarName: String;
  var Value: Variant);
var
  i: integer;
begin
  if VarName = 'TITULO' then
  begin
    i := MLAGeral.BoolToInt2(CkAlunosAtivos.Checked, 1, 0) +
         MLAGeral.BoolToInt2(CkAlunosInativos.Checked, 2, 0);
    case i of
      1: Value := 'Rela��o de Alunos Ativos';
      2: Value := 'Rela��o de Alunos Inativos';
      3: Value := 'Rela��o de Alunos Ativos e Inativos';
      else Value := 'Rela��o Inv�lida de Alunos';
    end;
  end;
end;

procedure TFmPrincipal.frxPagtos1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VAR_MULTA' then
    Value := Geral.FFT(Dmod.QrControleMulta.Value, 2, siPositivo)
  else if VarName = 'VAR_MORADD' then
    Value := Geral.FFT(Dmod.QrControleMoraDD.Value, 2, siPositivo)
  else if VarName = 'PERIODO' then
  begin
    Value := FormatDateTime(VAR_FORMATDATE6, TPAPPIni.Date)+' at� '+
                FormatDateTime(VAR_FORMATDATE6, TPAPPFim.Date);
  end else if VarName = 'STATUS_PGTO' then
  begin
    case (MLAGeral.BoolToInt2(CkAluPagtosV.Checked, 1, 0)+
          MLAGeral.BoolToInt2(CkAluPagtosA.Checked, 2, 0)+
          MLAGeral.BoolToInt2(CkAluPagtosQ.Checked, 4, 0)) of
      1: Value := 'Vencidos';
      2: Value := 'Abertos';
      3: Value := 'Vencidos e abertos';
      4: Value := 'Quitados';
      5: Value := 'Vencidos e quitados';
      6: Value := 'Abertos e quitados';
      7: Value := 'Vencidos, abertos e quitados';
      else Value := '***ERRO STUATUS***';
    end;
  end else if VarName = 'STATUS_ALUNO' then
  begin
    case (MLAGeral.BoolToInt2(CkAlunosAtivos.Checked, 1, 0)+
          MLAGeral.BoolToInt2(CkAlunosInativos.Checked, 2, 0)) of
      1: Value := 'Ativos';
      2: Value := 'Inativos';
      3: Value := 'Ativos e inativos';
      else Value := '***ERRO STUATUS***';
    end;
  end;
end;

procedure TFmPrincipal.frxCxaGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_LOGADO' then Value := VAR_USUARIO
  else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE2, TPCxaData.Date);
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  {
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
  }
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
begin
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.CadastroDeContasNiv();
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

// TODO LIST
// Reajuste de pre�os
// Avisar que n�o tem pre�o cadastrado para a data antiga


{ TODO : Ver novo contrato! }

{ TODO : Corrigir FatID de 101 para 2101! > UPDATE l a n c t o s  SET FatID=2101 WHERE FatID=101;}

{ TODO : Quitar todos pagamentos
UPDATE l a n c t o s
SET Carteira=-1, Tipo=1, Sit=3
WHERE FatID=2101;

}
end.

