unit SWMS_Mtng_Its;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, ABSMain, AdvGlowButton, dmkImage, dmkRadioGroup,
  ComCtrls, ToolWin, dmkCheckBox, UnDmkEnums;

type
  TFmSWMS_Mtng_Its = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    PnTitulo: TPanel;
    EdControle: TdmkEdit;
    Label2: TLabel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox5: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdNome: TdmkEdit;
    EdSenhaP: TdmkEdit;
    Label23: TLabel;
    CkAtivo: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmSWMS_Mtng_Its: TFmSWMS_Mtng_Its;

implementation

uses SWMS_Mtng_Cab, Module, UnInternalConsts, Principal, UnMyObjects,
UMySQLModule;

{$R *.DFM}

procedure TFmSWMS_Mtng_Its.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome,
    'Informe a equipe') then
    Exit;
  if MyObjects.FIC(Length(Trim(EdSenhaP.Text)) <> 6, EdSenhaP,
    'Informe uma palavra passe com 6 caracteres!') then
    Exit;
  if UMyMod.VerificaDuplicado1(Dmod.MyDB, 'swms_mtng_its', ['Codigo', 'SenhaP'], [
    FmSWMS_Mtng_Cab.QrSWMS_Mtng_CabCodigo.Value, EdSenhaP.Text],
    'Controle', EdControle.ValueVariant) then
    Exit;
  //
  if CkAtivo.Checked = False then
    if Geral.MensagemBox('Tem certeza que deseja inativar esta competi��o?' +
    #13#10 + 'Ao inativar e sincronizar, ela ser� exclu�da do webservice!',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    Exit ;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_SQL);
  Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'swms_mtng_its', 'Controle',
  [], [], ImgTipo.SQLType, FMSWMS_Mtng_Cab.Qrswms_mtng_itsControle.Value,
  siPositivo, EdControle);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'swms_mtng_its',
  Controle, Dmod.QrUpd) then
  begin
    //
    //
    FMSWMS_Mtng_Cab.ReopenSWMS_Mtng_Its(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdNome.ValueVariant       := '';
      EdSenhaP.ValueVariant     := '';
      EdNome.SetFocus;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Close;
  end;
end;

procedure TFmSWMS_Mtng_Its.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSWMS_Mtng_Its.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSWMS_Mtng_Its.FormCreate(Sender: TObject);
begin
{
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
  UMemMod.AbreABSQuery1(QrNadadores, [
  'SELECT * FROM nadadores', 'ORDER BY Nome;']);
}
end;

procedure TFmSWMS_Mtng_Its.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 10, 10, 20);
end;

end.
