object FmSalasEquip: TFmSalasEquip
  Left = 408
  Top = 245
  Caption = 'EQE-CADAS-001 :: Equipamento Eletr'#244'nico'
  ClientHeight = 134
  ClientWidth = 619
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 619
    Height = 46
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 65
      Height = 13
      Caption = 'Equipamento:'
    end
    object Label3: TLabel
      Left = 512
      Top = 4
      Width = 80
      Height = 13
      Caption = 'Fator uso (0 a 1):'
    end
    object Label2: TLabel
      Left = 420
      Top = 4
      Width = 86
      Height = 13
      Caption = 'Consumo watts/h:'
    end
    object Label4: TLabel
      Left = 328
      Top = 4
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object EdNome: TdmkEdit
      Left = 12
      Top = 20
      Width = 313
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdFator: TdmkEdit
      Left = 512
      Top = 20
      Width = 89
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdFatorExit
    end
    object Edwattsh: TdmkEdit
      Left = 420
      Top = 20
      Width = 89
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnExit = EdwattshExit
    end
    object EdQtde: TdmkEdit
      Left = 328
      Top = 20
      Width = 89
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdQtdeExit
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 86
    Width = 619
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 510
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 619
    Height = 40
    Align = alTop
    Caption = 'Equipamento Eletr'#244'nico'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 536
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
      ExplicitLeft = 543
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 535
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 541
      ExplicitHeight = 36
    end
  end
  object DsTransportadoras: TDataSource
    DataSet = QrTransportadoras
    Left = 236
    Top = 110
  end
  object QrTransportadoras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE Codigo>0'
      'AND Fornece2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 208
    Top = 110
    object QrTransportadorasNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrTransportadorasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
end
