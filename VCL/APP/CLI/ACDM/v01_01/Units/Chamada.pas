unit Chamada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, UnMLAGeral, StdCtrls, Buttons, UnInternalConsts,
  Mask, DBCtrls, ComCtrls, Db, mySQLDbTables, DotPrint, Menus, frxClass,
  frxDBSet, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DmkDAC_PF;

type
  TFmChamada = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    QrDdSem: TmySQLQuery;
    BtPreview: TBitBtn;
    QrAtividades: TmySQLQuery;
    DsAtividades: TDataSource;
    Panel4: TPanel;
    Label15: TLabel;
    CBAtividade: TdmkDBLookupComboBox;
    QrAtividadesNOMEPROFESSOR: TWideStringField;
    QrAtividadesNOMETURMA: TWideStringField;
    QrAtividadesNOMECURSO: TWideStringField;
    QrAtividadesNOMELOCAL: TWideStringField;
    QrAtividadesCodigo: TIntegerField;
    QrAtividadesControle: TIntegerField;
    QrAtividadesSala: TIntegerField;
    QrAtividadesCurso: TIntegerField;
    QrAtividadesProfessor: TIntegerField;
    QrAtividadesPeso: TFloatField;
    QrAtividadesTotalPeso: TFloatField;
    QrAtividadesCarga: TFloatField;
    QrAtividadesHonorarios: TFloatField;
    QrAtividadesDataI: TDateField;
    QrAtividadesLk: TIntegerField;
    QrAtividadesDataCad: TDateField;
    QrAtividadesDataAlt: TDateField;
    QrAtividadesUserCad: TIntegerField;
    QrAtividadesUserAlt: TIntegerField;
    QrAtividadesRefere: TWideStringField;
    QrAtividadesREFERETURMA: TWideStringField;
    QrDdSemCodigo: TIntegerField;
    QrDdSemNome: TWideStringField;
    QrDdSemDSem: TIntegerField;
    QrDdSemHIni: TTimeField;
    QrDdSemHFim: TTimeField;
    QrDdSemLk: TIntegerField;
    QrDdSemDataCad: TDateField;
    QrDdSemDataAlt: TDateField;
    QrDdSemUserCad: TIntegerField;
    QrDdSemUserAlt: TIntegerField;
    QrAlunos: TmySQLQuery;
    QrAlunosCodigo: TIntegerField;
    QrAlunosAluno: TIntegerField;
    QrAlunosAtividade: TIntegerField;
    QrAlunosNOMEALUNO: TWideStringField;
    QrEscolha: TmySQLQuery;
    QrEscolhaNOMEPROFESSOR: TWideStringField;
    QrEscolhaNOMETURMA: TWideStringField;
    QrEscolhaREFERETURMA: TWideStringField;
    QrEscolhaNOMECURSO: TWideStringField;
    QrEscolhaNOMELOCAL: TWideStringField;
    QrEscolhaCodigo: TIntegerField;
    QrEscolhaControle: TIntegerField;
    QrEscolhaSala: TIntegerField;
    QrEscolhaCurso: TIntegerField;
    QrEscolhaProfessor: TIntegerField;
    QrEscolhaPeso: TFloatField;
    QrEscolhaTotalPeso: TFloatField;
    QrEscolhaCarga: TFloatField;
    QrEscolhaHonorarios: TFloatField;
    QrEscolhaDataI: TDateField;
    QrEscolhaLk: TIntegerField;
    QrEscolhaDataCad: TDateField;
    QrEscolhaDataAlt: TDateField;
    QrEscolhaUserCad: TIntegerField;
    QrEscolhaUserAlt: TIntegerField;
    QrEscolhaRefere: TWideStringField;
    QrDdSem1: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    TimeField1: TTimeField;
    TimeField2: TTimeField;
    IntegerField3: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    QrMax: TmySQLQuery;
    QrDSem: TmySQLQuery;
    QrMaxITEM: TIntegerField;
    QrDSemDSem: TIntegerField;
    TbChamada1: TmySQLTable;
    TbChamada2: TmySQLTable;
    TbChamada2T_I: TIntegerField;
    TbChamada2Alu: TIntegerField;
    TbChamada2Nom: TWideStringField;
    TbChamada2S01: TWideStringField;
    TbChamada2S02: TWideStringField;
    TbChamada2S03: TWideStringField;
    TbChamada2S04: TWideStringField;
    TbChamada2S05: TWideStringField;
    TbChamada2S06: TWideStringField;
    TbChamada2S07: TWideStringField;
    TbChamada2S08: TWideStringField;
    TbChamada2S09: TWideStringField;
    TbChamada2S10: TWideStringField;
    TbChamada2S11: TWideStringField;
    TbChamada2S12: TWideStringField;
    TbChamada2S13: TWideStringField;
    TbChamada2S14: TWideStringField;
    TbChamada2S15: TWideStringField;
    TbChamada2S16: TWideStringField;
    TbChamada2S17: TWideStringField;
    TbChamada2S18: TWideStringField;
    TbChamada2S19: TWideStringField;
    TbChamada2S20: TWideStringField;
    TbChamada2S21: TWideStringField;
    TbChamada2S22: TWideStringField;
    TbChamada2S23: TWideStringField;
    TbChamada2S24: TWideStringField;
    TbChamada2S25: TWideStringField;
    TbChamada2S26: TWideStringField;
    TbChamada2S27: TWideStringField;
    TbChamada2S28: TWideStringField;
    TbChamada2S29: TWideStringField;
    TbChamada2S30: TWideStringField;
    TbChamada2S31: TWideStringField;
    DsChamada1: TDataSource;
    DsChamada2: TDataSource;
    TbChamada1T_I: TIntegerField;
    TbChamada1D01: TDateField;
    TbChamada1D02: TDateField;
    TbChamada1D03: TDateField;
    TbChamada1D04: TDateField;
    TbChamada1D05: TDateField;
    TbChamada1D06: TDateField;
    TbChamada1D07: TDateField;
    TbChamada1D08: TDateField;
    TbChamada1D09: TDateField;
    TbChamada1D10: TDateField;
    TbChamada1D11: TDateField;
    TbChamada1D12: TDateField;
    TbChamada1D13: TDateField;
    TbChamada1D14: TDateField;
    TbChamada1D15: TDateField;
    TbChamada1D16: TDateField;
    TbChamada1D17: TDateField;
    TbChamada1D18: TDateField;
    TbChamada1D19: TDateField;
    TbChamada1D20: TDateField;
    TbChamada1D21: TDateField;
    TbChamada1D22: TDateField;
    TbChamada1D23: TDateField;
    TbChamada1D24: TDateField;
    TbChamada1D25: TDateField;
    TbChamada1D26: TDateField;
    TbChamada1D27: TDateField;
    TbChamada1D28: TDateField;
    TbChamada1D29: TDateField;
    TbChamada1D30: TDateField;
    TbChamada1D31: TDateField;
    Label1: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    Label2: TLabel;
    TbChamada1W01: TWideStringField;
    TbChamada1W02: TWideStringField;
    TbChamada1W03: TWideStringField;
    TbChamada1W04: TWideStringField;
    TbChamada1W05: TWideStringField;
    TbChamada1W06: TWideStringField;
    TbChamada1W07: TWideStringField;
    TbChamada1H01: TWideStringField;
    TbChamada1H02: TWideStringField;
    TbChamada1H03: TWideStringField;
    TbChamada1H04: TWideStringField;
    TbChamada1H05: TWideStringField;
    TbChamada1H06: TWideStringField;
    TbChamada1H07: TWideStringField;
    TbChamada1Ref: TWideStringField;
    TbChamada1Tur: TWideStringField;
    TbChamada2SEQ: TIntegerField;
    TbChamada1DS01: TWideStringField;
    TbChamada1DS02: TWideStringField;
    TbChamada1DS03: TWideStringField;
    TbChamada1DS04: TWideStringField;
    TbChamada1DS05: TWideStringField;
    TbChamada1DS06: TWideStringField;
    TbChamada1DS07: TWideStringField;
    TbChamada1DS08: TWideStringField;
    TbChamada1DS09: TWideStringField;
    TbChamada1DS10: TWideStringField;
    TbChamada1DS11: TWideStringField;
    TbChamada1DS12: TWideStringField;
    TbChamada1DS13: TWideStringField;
    TbChamada1DS14: TWideStringField;
    TbChamada1DS15: TWideStringField;
    TbChamada1DS16: TWideStringField;
    TbChamada1DS17: TWideStringField;
    TbChamada1DS18: TWideStringField;
    TbChamada1DS19: TWideStringField;
    TbChamada1DS20: TWideStringField;
    TbChamada1DS21: TWideStringField;
    TbChamada1DS22: TWideStringField;
    TbChamada1DS23: TWideStringField;
    TbChamada1DS24: TWideStringField;
    TbChamada1DS25: TWideStringField;
    TbChamada1DS26: TWideStringField;
    TbChamada1DS27: TWideStringField;
    TbChamada1DS28: TWideStringField;
    TbChamada1DS29: TWideStringField;
    TbChamada1DS30: TWideStringField;
    TbChamada1DS31: TWideStringField;
    TbChamada1DT01: TWideStringField;
    TbChamada1DT02: TWideStringField;
    TbChamada1DT03: TWideStringField;
    TbChamada1DT04: TWideStringField;
    TbChamada1DT05: TWideStringField;
    TbChamada1DT06: TWideStringField;
    TbChamada1DT07: TWideStringField;
    TbChamada1DT08: TWideStringField;
    TbChamada1DT09: TWideStringField;
    TbChamada1DT10: TWideStringField;
    TbChamada1DT11: TWideStringField;
    TbChamada1DT12: TWideStringField;
    TbChamada1DT13: TWideStringField;
    TbChamada1DT14: TWideStringField;
    TbChamada1DT15: TWideStringField;
    TbChamada1DT16: TWideStringField;
    TbChamada1DT17: TWideStringField;
    TbChamada1DT18: TWideStringField;
    TbChamada1DT19: TWideStringField;
    TbChamada1DT20: TWideStringField;
    TbChamada1DT21: TWideStringField;
    TbChamada1DT22: TWideStringField;
    TbChamada1DT23: TWideStringField;
    TbChamada1DT24: TWideStringField;
    TbChamada1DT25: TWideStringField;
    TbChamada1DT26: TWideStringField;
    TbChamada1DT27: TWideStringField;
    TbChamada1DT28: TWideStringField;
    TbChamada1DT29: TWideStringField;
    TbChamada1DT30: TWideStringField;
    TbChamada1DT31: TWideStringField;
    QrAlunosTipo: TSmallintField;
    QrAlunosENatal: TDateField;
    QrAlunosPNatal: TDateField;
    QrAlunosNATAL: TDateField;
    TbChamada2Nas: TWideStringField;
    Progress2: TProgressBar;
    Progress1: TProgressBar;
    TbChamada1NOMEPROFESSOR: TWideStringField;
    Label7: TLabel;
    EdProfessor: TdmkEditCB;
    CBProfessor: TdmkDBLookupComboBox;
    QrProfessores: TmySQLQuery;
    DsProfessores: TDataSource;
    QrProfessoresCodigo: TIntegerField;
    QrProfessoresNOMEENTIDADE: TWideStringField;
    PMImprime: TPopupMenu;
    TintaLaser1: TMenuItem;
    Matricial1: TMenuItem;
    frxChamadaA10: TfrxReport;
    frxDsChamada1: TfrxDBDataset;
    frxDsChamada2: TfrxDBDataset;
    EdAtividade: TdmkEditCB;
    DBEdit5: TDBEdit;
    Panel5: TPanel;
    BtSaida: TBitBtn;
    frxChamadaB10: TfrxReport;
    RGFormatoImp: TRadioGroup;
    TbChamada2Ani: TWideStringField;
    QrAlunosTe1: TWideStringField;
    TbChamada2Tel: TWideStringField;
    QrAlunosDataF: TDateField;
    TbChamada2Ren: TWideStringField;
    frxChamadaB12: TfrxReport;
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPreviewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbChamada2CalcFields(DataSet: TDataSet);
    procedure TbChamada1CalcFields(DataSet: TDataSet);
    procedure QrAlunosCalcFields(DataSet: TDataSet);
    procedure TintaLaser1Click(Sender: TObject);
    procedure Matricial1Click(Sender: TObject);
    procedure frxChamadaA10GetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    FLista: array[01..31] of Integer;
    FPrese: array[01..31] of String[1];
    FPagina: Integer;
    FTitulo: String;
    FTextos: TMyTextos_136;
    FAlinha: TMyAlinha_136;
    FTamCol: TMyTamCol_136;
    procedure ImprimeMatricial;
    procedure DefineLinhaA;
    procedure DefineLinhaB;
    procedure DefineLinhaC;
    procedure DefineLinhaD;
    procedure DefineLinhaE;
    procedure DefineLinhaF;
    function ConfiguraLinhaE(Texto: String): String;
    procedure PreparaRelatorio();
  public
    { Public declarations }
  end;

var
  FmChamada: TFmChamada;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal, UCreate, UnMsgInt, ModuleGeral,
  UMySQLModule, UnDmkProcFunc;

const
  FMaxItens = 35;
  TA01 = 06;
  TA02 = 20; // 026
  TA03 = 30; // 056
  TA04 = 30; // 086
  TA05 = 05;
  TA06 = 05;
  TA07 = 05;
  TA08 = 05;
  TA09 = 05;
  TA10 = 05;
  TA11 = 05;
  TA12 = 05; // 126
  TA13 = 10; // 136


procedure TFmChamada.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmChamada.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChamada.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmChamada.PreparaRelatorio();
var
  WDom, WSeg, WTer, WQua, WQui, WSex, WSab,
  HDom, HSeg, HTer, HQua, HQui, HSex, HSab, Ani, Ren: String;
  Dom, Seg, Ter, Qua, Qui, Sex, Sab,
  Dom1, Seg1, Ter1, Qua1, Qui1, Sex1, Sab1,
  //k, i, l, m, n,
  Periodo, i, k, d, Atividade, Professor: Integer;
  DataI, DataF: TDateTime;
  Ano, Mes: Word;
  AniAno, AniMes, AniDia: Word;
begin
  Ano := Geral.IMV(CBAno.Text);
  if Ano = 0 then
  begin
    Geral.MB_Aviso('Informe o ano!');
    Exit;
  end;
  Mes := CBMes.ItemIndex + 1;
  Periodo := ((Ano - 2000) * 12) + Mes;
  DataI := MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo);
  DataF := MLAGeral.UltimoDiaDoPeriodo_Date(Periodo);
  /////////////////////////////////////////////////////////
  Atividade := Geral.IMV(EdAtividade.Text);
  QrEscolha.Close;
  if Atividade = 0 then QrEscolha.SQL[09] := ''
  else QrEscolha.SQL[09] := 'WHERE tui.Controle='+IntToStr(Atividade);
  /////////////////////////////////////////////////////////
  Professor := Geral.IMV(EdProfessor.Text);
  QrEscolha.Close;
  if Professor = 0 then QrEscolha.SQL[10] := ''
  else begin
    if Atividade = 0 then
      QrEscolha.SQL[10] := 'WHERE tui.Professor='+IntToStr(Professor)
    else
      QrEscolha.SQL[10] := 'AND tui.Professor='+IntToStr(Professor)
  end;
  /////////////////////////////////////////////////////////
  QrEscolha.Open;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DELETE FROM chamada1');
  DmodG.QrUpdPID1.ExecSQL;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DELETE FROM chamada2');
  DmodG.QrUpdPID1.ExecSQL;
  //
  Progress1.Position := 0;
  Progress1.Max := QrEscolha.RecordCount;
  while not QrEscolha.Eof do
  begin
    Progress1.Position :=   Progress1.Position +1;
    for i := 1 to 31 do FLista[i] := 0;
    QrDdSem.Close;
    QrDdSem.Params[0].AsInteger := QrEscolhaControle.Value;
    QrDdSem.Open;
    Dom := 0;
    Seg := 0;
    Ter := 0;
    Qua := 0;
    Qui := 0;
    Sex := 0;
    Sab := 0;
    WDom := '';
    WSeg := '';
    WTer := '';
    WQua := '';
    WQui := '';
    WSex := '';
    WSab := '';
    HDom := '';
    HSeg := '';
    HTer := '';
    HQua := '';
    HQui := '';
    HSex := '';
    HSab := '';
    while not QrDdSem.Eof do
    begin
      case QrDdSemDSem.Value of
        0: Dom := 1;
        1: Seg := 2;
        2: Ter := 3;
        3: Qua := 4;
        4: Qui := 5;
        5: Sex := 6;
        6: Sab := 7;
      end;
      case QrDdSemDSem.Value of
        0: WDom := 'DOMINGO';
        1: WSeg := 'SEGUNDA';
        2: WTer := 'TER�A';
        3: WQua := 'QUARTA';
        4: WQui := 'QUINTA';
        5: WSex := 'SEXTA';
        6: WSab := 'S�BADO';
      end;
      case QrDdSemDSem.Value of
        0: HDom := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
        1: HSeg := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
        2: HTer := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
        3: HQua := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
        4: HQui := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
        5: HSex := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
        6: HSab := FormatDateTime(VAR_FORMATTIME2, QrDdSemHIni.Value)+' '+FormatDateTime(VAR_FORMATTIME2, QrDdSemHFim.Value);
      end;
      QrDdSem.Next;
    end;
    //
    k := 0;
    for i := Trunc(DataI) to Trunc(DataF) do
    begin
      if DayOfWeek(i) in [Dom, Seg, Ter, Qua, Qui, Sex, Sab] then
      begin
        //DecodeDate(i, Ano, Mes, Dia);
        k := k +1;
        FLista[k] := i;
      end;
    end;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO chamada1 SET ');
    DmodG.QrUpdPID1.SQL.Add(' NOMEPROFESSOR="'+QrEscolhaNOMEPROFESSOR.Value+'"');
    DmodG.QrUpdPID1.SQL.Add(',T_I='+IntToStr(QrEscolhaControle.Value));
    DmodG.QrUpdPID1.SQL.Add(',D01="'+FormatDateTime(VAR_FORMATDATE, FLista[01])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D02="'+FormatDateTime(VAR_FORMATDATE, FLista[02])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D03="'+FormatDateTime(VAR_FORMATDATE, FLista[03])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D04="'+FormatDateTime(VAR_FORMATDATE, FLista[04])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D05="'+FormatDateTime(VAR_FORMATDATE, FLista[05])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D06="'+FormatDateTime(VAR_FORMATDATE, FLista[06])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D07="'+FormatDateTime(VAR_FORMATDATE, FLista[07])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D08="'+FormatDateTime(VAR_FORMATDATE, FLista[08])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D09="'+FormatDateTime(VAR_FORMATDATE, FLista[09])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D10="'+FormatDateTime(VAR_FORMATDATE, FLista[10])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D11="'+FormatDateTime(VAR_FORMATDATE, FLista[11])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D12="'+FormatDateTime(VAR_FORMATDATE, FLista[12])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D13="'+FormatDateTime(VAR_FORMATDATE, FLista[13])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D14="'+FormatDateTime(VAR_FORMATDATE, FLista[14])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D15="'+FormatDateTime(VAR_FORMATDATE, FLista[15])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D16="'+FormatDateTime(VAR_FORMATDATE, FLista[16])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D17="'+FormatDateTime(VAR_FORMATDATE, FLista[17])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D18="'+FormatDateTime(VAR_FORMATDATE, FLista[18])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D19="'+FormatDateTime(VAR_FORMATDATE, FLista[19])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D20="'+FormatDateTime(VAR_FORMATDATE, FLista[20])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D21="'+FormatDateTime(VAR_FORMATDATE, FLista[21])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D22="'+FormatDateTime(VAR_FORMATDATE, FLista[22])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D23="'+FormatDateTime(VAR_FORMATDATE, FLista[23])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D24="'+FormatDateTime(VAR_FORMATDATE, FLista[24])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D25="'+FormatDateTime(VAR_FORMATDATE, FLista[25])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D26="'+FormatDateTime(VAR_FORMATDATE, FLista[26])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D27="'+FormatDateTime(VAR_FORMATDATE, FLista[27])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D28="'+FormatDateTime(VAR_FORMATDATE, FLista[28])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D29="'+FormatDateTime(VAR_FORMATDATE, FLista[29])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D30="'+FormatDateTime(VAR_FORMATDATE, FLista[30])+'"');
    DmodG.QrUpdPID1.SQL.Add(',D31="'+FormatDateTime(VAR_FORMATDATE, FLista[31])+'"');
    DmodG.QrUpdPID1.SQL.Add(',W01="'+WDOM+'"');
    DmodG.QrUpdPID1.SQL.Add(',W02="'+WSEG+'"');
    DmodG.QrUpdPID1.SQL.Add(',W03="'+WTER+'"');
    DmodG.QrUpdPID1.SQL.Add(',W04="'+WQUA+'"');
    DmodG.QrUpdPID1.SQL.Add(',W05="'+WQUI+'"');
    DmodG.QrUpdPID1.SQL.Add(',W06="'+WSEX+'"');
    DmodG.QrUpdPID1.SQL.Add(',W07="'+WSAB+'"');
    DmodG.QrUpdPID1.SQL.Add(',H01="'+HDOM+'"');
    DmodG.QrUpdPID1.SQL.Add(',H02="'+HSEG+'"');
    DmodG.QrUpdPID1.SQL.Add(',H03="'+HTER+'"');
    DmodG.QrUpdPID1.SQL.Add(',H04="'+HQUA+'"');
    DmodG.QrUpdPID1.SQL.Add(',H05="'+HQUI+'"');
    DmodG.QrUpdPID1.SQL.Add(',H06="'+HSEX+'"');
    DmodG.QrUpdPID1.SQL.Add(',H07="'+HSAB+'"');
    DmodG.QrUpdPID1.SQL.Add(',Ref="'+QrEscolhaRefere.Value+'"');
    DmodG.QrUpdPID1.SQL.Add(',Tur="'+QrEscolhaNOMETURMA.Value+'"');
    DmodG.QrUpdPID1.ExecSQL;
    //
{   2012/01/25
    QrAlunos.Close;
    QrAlunos.Params[0].AsInteger := QrEscolhaControle.Value; //Atividade
    QrAlunos.Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrAlunos, Dmod.MyDB, [
    'SELECT DISTINCT mcu.Codigo, mcu.Aluno, maa.Atividade, ',
    'CASE WHEN alu.Tipo=0 THEN alu.RazaoSocial ',
    'ELSE alu.Nome END NOMEALUNO, ',
    'alu.Tipo, alu.ENatal, alu.PNatal,',
    'IF(alu.Tipo=0,alu.ETe1,alu.PTe1) Te1, ',
    'ren.DataF ', // Adicionado
    'FROM matriper map ',
    'LEFT JOIN matricul mcu ON mcu.Codigo=map.Codigo ',
    'LEFT JOIN matriati maa ON maa.Item=map.Item ',
    'LEFT JOIN entidades alu ON alu.Codigo=mcu.Aluno ',
    'LEFT JOIN matriren ren ON ren.Codigo=map.Codigo AND ren.Ativo=1 ', // adicionado
    'WHERE maa.Atividade=' + Geral.FF0(QrEscolhaControle.Value),
    'AND mcu.Ativo=1 ',
    'ORDER BY NOMEALUNO ',
    '']);
    // Fim 2012/01/25
    QrAlunos.DisableControls;
    QrAlunos.First;
    Progress2.Position := 0;
    Progress2.Max := QrAlunos.RecordCount;
    while not QrAlunos.Eof do
    begin
      Progress2.Position := Progress2.Position+1;
      Update;
      Application.ProcessMessages;
      for i := 1 to 31 do
      begin
        FPrese[i] := '';
        if FLista[i] > 0 then
        begin
          QrMax.Close;
          QrMax.Params[0].AsInteger := QrEscolhaControle.Value;
          QrMax.Params[1].AsInteger := QrAlunosAluno.Value;
          QrMax.Open;
          //
          QrDSem.Close;
          QrDSem.Params[0].AsInteger := QrMaxITEM.Value;
          QrDSem.Open;
          Dom1 := -1;
          Seg1 := -1;
          Ter1 := -1;
          Qua1 := -1;
          Qui1 := -1;
          Sex1 := -1;
          Sab1 := -1;
          while not QrDSem.Eof do
          begin
            case QrDSemDSem.Value of
              0: Dom1 := 1;
              1: Seg1 := 2;
              2: Ter1 := 3;
              3: Qua1 := 4;
              4: Qui1 := 5;
              5: Sex1 := 6;
              6: Sab1 := 7;
            end;
            QrDSem.Next;
          end;
          d := DayOfWeek(FLista[i]);
          if not (d in [Dom1, Seg1, Ter1, Qua1, Qui1, Sex1, Sab1])
          then FPrese[i] := 'X'
        end;
      end;
      DecodeDate(QrAlunosNATAL.Value, AniAno, AniMes, AniDia);
      if (AniMes = Mes) and (QrAlunosNATAL.Value > 2) then
        Ani := IntToStr(AniDia) + ' (' + IntToStr(Ano - AniAno) + ' anos)'
      else
        Ani := '';
      //
      if QrAlunosDataF.Value < 2 then
        Ren := ''
      else
        Ren := Geral.FDT(QrAlunosDataF.Value, 2);
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO chamada2 SET ');
      DmodG.QrUpdPID1.SQL.Add(' T_I='+IntToStr(QrEscolhaControle.Value));
      DmodG.QrUpdPID1.SQL.Add(',Alu='+IntToStr(QrAlunosAluno.Value));
      DmodG.QrUpdPID1.SQL.Add(',Nom="'+QrAlunosNOMEALUNO.Value+'"');
      DmodG.QrUpdPID1.SQL.Add(',Tel="'+Geral.FormataTelefone_TT_Curto(QrAlunosTe1.Value)+'"');
      DmodG.QrUpdPID1.SQL.Add(',Nas="'+Geral.FDT(QrAlunosNATAL.Value, 2, True)+'"');
      DmodG.QrUpdPID1.SQL.Add(',Ani="'+Ani+'"');
      DmodG.QrUpdPID1.SQL.Add(',Ren="'+Ren+'"');
      DmodG.QrUpdPID1.SQL.Add(',S01="'+FPrese[01]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S02="'+FPrese[02]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S03="'+FPrese[03]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S04="'+FPrese[04]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S05="'+FPrese[05]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S06="'+FPrese[06]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S07="'+FPrese[07]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S08="'+FPrese[08]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S09="'+FPrese[09]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S10="'+FPrese[10]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S11="'+FPrese[11]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S12="'+FPrese[12]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S13="'+FPrese[13]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S14="'+FPrese[14]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S15="'+FPrese[15]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S16="'+FPrese[16]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S17="'+FPrese[17]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S18="'+FPrese[18]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S19="'+FPrese[19]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S20="'+FPrese[20]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S21="'+FPrese[21]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S22="'+FPrese[22]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S23="'+FPrese[23]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S24="'+FPrese[24]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S25="'+FPrese[25]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S26="'+FPrese[26]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S27="'+FPrese[27]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S28="'+FPrese[28]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S29="'+FPrese[29]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S30="'+FPrese[30]+'"');
      DmodG.QrUpdPID1.SQL.Add(',S31="'+FPrese[31]+'"');
      DmodG.QrUpdPID1.ExecSQL;
      //
      QrAlunos.Next;
    end;
    QrAlunos.EnableControls;
    QrEscolha.Next;
  end;
  FPagina := 0;
  Progress1.Position := 0;
  Progress2.Position := 0;
end;

procedure TFmChamada.BtPreviewClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtPreview);
end;

procedure TFmChamada.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia : Word;
  i: integer;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date+28, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    for i := Ano-50 to Ano+50 do Add(IntToStr(i));
  end;
  CBAno.ItemIndex := 50;
  CBMes.ItemIndex := (Mes - 1);
  //////////////////////////////////////////////////////////////////////////
  QrAtividades.Open;
  QrProfessores.Open;
  TbChamada1.Close;
  TbChamada1.Database := DModG.MyPID_DB;
  TbChamada2.Close;
  TbChamada2.Database := DModG.MyPID_DB;
  //
  Ucriar.RecriaTempTable('Chamada1', DmodG.QrUpdPID1, False);
  Ucriar.RecriaTempTable('Chamada2', DmodG.QrUpdPID1, False);
  ////////////////////////////////////////
  CBMes.Enabled := True;
  CBAno.Enabled := True;
  /////////////////////////////////////////
end;

procedure TFmChamada.TbChamada2CalcFields(DataSet: TDataSet);
begin
  TbChamada2SEQ.Value := TbChamada2.RecNo;
end;

procedure TFmChamada.TbChamada1CalcFields(DataSet: TDataSet);
var
  i, j: integer;
begin
  for i := 1 to 31 do
  begin
    if TbChamada1.FieldByName('D'+FormatFloat('00', i)).AsDateTime > 1 then
    begin
      TbChamada1.FieldByName('DT'+FormatFloat('00', i)).AsString :=
      FormatDateTime('dd', TbChamada1.FieldByName('D'+FormatFloat('00', i)).AsDateTime);
      j := DayOfWeek(TbChamada1.FieldByName('D'+FormatFloat('00', i)).AsDateTime);
      case j of
        1: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := 'Do';
        2: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := '2�';
        3: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := '3�';
        4: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := '4�';
        5: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := '5�';
        6: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := '6�';
        7: TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := 'Sa';
      end;
    end else begin
      TbChamada1.FieldByName('DT'+FormatFloat('00', i)).AsString := '';
      TbChamada1.FieldByName('DS'+FormatFloat('00', i)).AsString := '';
    end;
  end;
end;

procedure TFmChamada.QrAlunosCalcFields(DataSet: TDataSet);
begin
  if QrAlunosTipo.Value = 0 then
     QrAlunosNATAL.Value := QrAlunosENatal.Value
  else QrAlunosNATAL.Value := QrAlunosPNatal.Value;
end;

procedure TFmChamada.ImprimeMatricial;
const
  ITENS = 37;
  //
var
  i: Integer;
  Divisor, Data: String;
  //SubTotal, Total: Integer;
//var
  TextoCG: String;
  //CampoCG: TWideStringField;
begin
  PreparaRelatorio;
  ////////////////////////////////////////////////////////////////////////////////
  VAR_LOCCOLUNASIMP := 136;
  ////////////////////////////////////////////////////////////////////////////////
  Data := FormatDateTime('hh:nn:ss "  "dd/mm/yyyy', Now());
  //ColsImpCompress := Trunc(VAR_LOCCOLUNASIMP * 17 / 10);
  Divisor := '';
  for i := 1 to VAR_LOCCOLUNASIMP do Divisor := Divisor + '=';
  //
  Application.CreateForm(TFmDotPrint, FmDotPrint);
  with FmDotPrint do
  begin
    with RE1 do
    begin
      Lines.Clear;
      AdicionaLinhaTexto(Geral.CompletaString(Data, ' ', 136, taRightJustify,
        True), [fdpCompress], []);
      AdicionaLinhaTexto(FTitulo, [fdpNormalSize], []);
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //
      //Total := 0;
      //////////////////////////////////////////////////////////////////////////
      //CampoCG := nil;
      TextoCG := '';
      TbChamada1.Close;
      TbChamada1.Open;
      TbChamada1.First;
      while not TbChamada1.Eof do
      begin
        AdicionaLinhaTexto(TbChamada1NOMEPROFESSOR.Value, [fdpDoubleSize], []);
        AdicionaLinhaTexto('', [fdpNormalSize], []);
        DefineLinhaA;
        AdicionaLinhaColunas(09, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        DefineLinhaB;
        AdicionaLinhaColunas(09, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        DefineLinhaC;
        AdicionaLinhaColunas(35, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        DefineLinhaD;
        AdicionaLinhaColunas(35, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        //
        TbChamada2.Close;
        TbChamada2.Open;
        TbChamada2.First;
        while not TbChamada2.Eof do
        begin
          DefineLinhaE;
          AdicionaLinhaColunas(35, FTextos, FAlinha, FTamCol, [fdpCompress], '');
          TbChamada2.Next;
        end;
        for i := 1 to 8 do
        begin
          DefineLinhaF;
          AdicionaLinhaColunas(35, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        end;
        while (RE1.Lines.Count div 63) <> (RE1.Lines.Count / 63) do
          AdicionaLinhaTexto('', [fdpCompress], []);

        TbChamada1.Next;
      end;
      //////////////////////////////////////////////////////////////////////////
    end;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmChamada.DefineLinhaA;
const
  Maximo = 9;
var
  i: Integer;
begin
  FTextos[01] := 'REFER�NCIA';
  FTextos[02] := 'TURMA';
  FTextos[03] := TbChamada1W01.Value;
  FTextos[04] := TbChamada1W02.Value;
  FTextos[05] := TbChamada1W03.Value;
  FTextos[06] := TbChamada1W04.Value;
  FTextos[07] := TbChamada1W05.Value;
  FTextos[08] := TbChamada1W06.Value;
  FTextos[09] := TbChamada1W07.Value;
  for i := Maximo + 1 to FMaxItens do FTextos[i] := '';
  FAlinha[01] := taCenter;
  FAlinha[02] := taCenter;
  FAlinha[03] := taCenter;
  FAlinha[04] := taCenter;
  FAlinha[05] := taCenter;
  FAlinha[06] := taCenter;
  FAlinha[07] := taCenter;
  FAlinha[08] := taCenter;
  FAlinha[09] := taCenter;
  FTamCol[01] := 013;
  FTamCol[02] := 032;
  FTamCol[03] := 013;
  FTamCol[04] := 013;
  FTamCol[05] := 013;
  FTamCol[06] := 013;
  FTamCol[07] := 013;
  FTamCol[08] := 013;
  FTamCol[09] := 013;
end;

procedure TFmChamada.DefineLinhaB;
const
  Maximo = 9;
var
  i: Integer;
begin
  FTextos[01] := TbChamada1Ref.Value;
  FTextos[02] := TbChamada1Tur.Value;
  FTextos[03] := TbChamada1H01.Value;
  FTextos[04] := TbChamada1H02.Value;
  FTextos[05] := TbChamada1H03.Value;
  FTextos[06] := TbChamada1H04.Value;
  FTextos[07] := TbChamada1H05.Value;
  FTextos[08] := TbChamada1H06.Value;
  FTextos[09] := TbChamada1H07.Value;
  for i := Maximo + 1 to FMaxItens do FTextos[i] := '';
  FAlinha[01] := taCenter;
  FAlinha[02] := taCenter;
  FAlinha[03] := taCenter;
  FAlinha[04] := taCenter;
  FAlinha[05] := taCenter;
  FAlinha[06] := taCenter;
  FAlinha[07] := taCenter;
  FAlinha[08] := taCenter;
  FAlinha[09] := taCenter;
  FTamCol[01] := 013;
  FTamCol[02] := 032;
  FTamCol[03] := 013;
  FTamCol[04] := 013;
  FTamCol[05] := 013;
  FTamCol[06] := 013;
  FTamCol[07] := 013;
  FTamCol[08] := 013;
  FTamCol[09] := 013;
end;

procedure TFmChamada.DefineLinhaC;
const
  Maximo = 35;
var
  i: Integer;
begin
  FTextos[01] := '';
  FTextos[02] := '';
  FTextos[03] := '';
  FTextos[04] := '';
  FTextos[05] := TbChamada1DT01.Value;
  FTextos[06] := TbChamada1DT02.Value;
  FTextos[07] := TbChamada1DT03.Value;
  FTextos[08] := TbChamada1DT04.Value;
  FTextos[09] := TbChamada1DT05.Value;
  FTextos[10] := TbChamada1DT06.Value;
  FTextos[11] := TbChamada1DT07.Value;
  FTextos[12] := TbChamada1DT08.Value;
  FTextos[13] := TbChamada1DT09.Value;
  FTextos[14] := TbChamada1DT10.Value;
  FTextos[15] := TbChamada1DT11.Value;
  FTextos[16] := TbChamada1DT12.Value;
  FTextos[17] := TbChamada1DT13.Value;
  FTextos[18] := TbChamada1DT14.Value;
  FTextos[19] := TbChamada1DT15.Value;
  FTextos[20] := TbChamada1DT16.Value;
  FTextos[21] := TbChamada1DT17.Value;
  FTextos[22] := TbChamada1DT18.Value;
  FTextos[23] := TbChamada1DT19.Value;
  FTextos[24] := TbChamada1DT20.Value;
  FTextos[25] := TbChamada1DT21.Value;
  FTextos[26] := TbChamada1DT22.Value;
  FTextos[27] := TbChamada1DT23.Value;
  FTextos[28] := TbChamada1DT24.Value;
  FTextos[29] := TbChamada1DT25.Value;
  FTextos[30] := TbChamada1DT26.Value;
  FTextos[31] := TbChamada1DT27.Value;
  FTextos[32] := TbChamada1DT28.Value;
  FTextos[33] := TbChamada1DT29.Value;
  FTextos[34] := TbChamada1DT30.Value;
  FTextos[35] := TbChamada1DT31.Value;
  //for i := Maximo + 1 to FMaxItens do FTextos[i] := '';
  for i := 1 to 35 do FAlinha[i] := taCenter;
  for i := 5 to 35 do FTamCol[i] := 003;
  FTamCol[01] := 004;
  FTamCol[02] := 008;
  FTamCol[03] := 021;
  FTamCol[04] := 010;
end;

procedure TFmChamada.DefineLinhaD;
const
  Maximo = 35;
{
var
  i: Integer;
}
begin
  FTextos[01] := 'N�';
  FTextos[02] := 'C�D';
  FTextos[03] := 'NOME DO ALUNO';
  FTextos[04] := 'NASC';
  FTextos[05] := TbChamada1DS01.Value;
  FTextos[06] := TbChamada1DS02.Value;
  FTextos[07] := TbChamada1DS03.Value;
  FTextos[08] := TbChamada1DS04.Value;
  FTextos[09] := TbChamada1DS05.Value;
  FTextos[10] := TbChamada1DS06.Value;
  FTextos[11] := TbChamada1DS07.Value;
  FTextos[12] := TbChamada1DS08.Value;
  FTextos[13] := TbChamada1DS09.Value;
  FTextos[14] := TbChamada1DS10.Value;
  FTextos[15] := TbChamada1DS11.Value;
  FTextos[16] := TbChamada1DS12.Value;
  FTextos[17] := TbChamada1DS13.Value;
  FTextos[18] := TbChamada1DS14.Value;
  FTextos[19] := TbChamada1DS15.Value;
  FTextos[20] := TbChamada1DS16.Value;
  FTextos[21] := TbChamada1DS17.Value;
  FTextos[22] := TbChamada1DS18.Value;
  FTextos[23] := TbChamada1DS19.Value;
  FTextos[24] := TbChamada1DS20.Value;
  FTextos[25] := TbChamada1DS21.Value;
  FTextos[26] := TbChamada1DS22.Value;
  FTextos[27] := TbChamada1DS23.Value;
  FTextos[28] := TbChamada1DS24.Value;
  FTextos[29] := TbChamada1DS25.Value;
  FTextos[30] := TbChamada1DS26.Value;
  FTextos[31] := TbChamada1DS27.Value;
  FTextos[32] := TbChamada1DS28.Value;
  FTextos[33] := TbChamada1DS29.Value;
  FTextos[34] := TbChamada1DS30.Value;
  FTextos[35] := TbChamada1DS31.Value;
  //for i := 1 to 35 do FAlinha[i] := taCenter;
end;

procedure TFmChamada.DefineLinhaE;
const
  Maximo = 35;
var
  i: Integer;
begin
  FTextos[01] := FormatFloat('00', TbChamada2SEQ.Value);
  FTextos[02] := FormatFloat('000000', TbChamada2Alu.Value);
  FTextos[03] := TbChamada2Nom.Value;
  FTextos[04] := Copy(TbChamada2Nas.Value, 1, 6)+Copy(TbChamada2Nas.Value, 9, 2);
  FTextos[05] := ConfiguraLinhaE(TbChamada2S01.Value);
  FTextos[06] := ConfiguraLinhaE(TbChamada2S02.Value);
  FTextos[07] := ConfiguraLinhaE(TbChamada2S03.Value);
  FTextos[08] := ConfiguraLinhaE(TbChamada2S04.Value);
  FTextos[09] := ConfiguraLinhaE(TbChamada2S05.Value);
  FTextos[10] := ConfiguraLinhaE(TbChamada2S06.Value);
  FTextos[11] := ConfiguraLinhaE(TbChamada2S07.Value);
  FTextos[12] := ConfiguraLinhaE(TbChamada2S08.Value);
  FTextos[13] := ConfiguraLinhaE(TbChamada2S09.Value);
  FTextos[14] := ConfiguraLinhaE(TbChamada2S10.Value);
  FTextos[15] := ConfiguraLinhaE(TbChamada2S11.Value);
  FTextos[16] := ConfiguraLinhaE(TbChamada2S12.Value);
  FTextos[17] := ConfiguraLinhaE(TbChamada2S13.Value);
  FTextos[18] := ConfiguraLinhaE(TbChamada2S14.Value);
  FTextos[19] := ConfiguraLinhaE(TbChamada2S15.Value);
  FTextos[20] := ConfiguraLinhaE(TbChamada2S16.Value);
  FTextos[21] := ConfiguraLinhaE(TbChamada2S17.Value);
  FTextos[22] := ConfiguraLinhaE(TbChamada2S18.Value);
  FTextos[23] := ConfiguraLinhaE(TbChamada2S19.Value);
  FTextos[24] := ConfiguraLinhaE(TbChamada2S20.Value);
  FTextos[25] := ConfiguraLinhaE(TbChamada2S21.Value);
  FTextos[26] := ConfiguraLinhaE(TbChamada2S22.Value);
  FTextos[27] := ConfiguraLinhaE(TbChamada2S23.Value);
  FTextos[28] := ConfiguraLinhaE(TbChamada2S24.Value);
  FTextos[29] := ConfiguraLinhaE(TbChamada2S25.Value);
  FTextos[30] := ConfiguraLinhaE(TbChamada2S26.Value);
  FTextos[31] := ConfiguraLinhaE(TbChamada2S27.Value);
  FTextos[32] := ConfiguraLinhaE(TbChamada2S28.Value);
  FTextos[33] := ConfiguraLinhaE(TbChamada2S29.Value);
  FTextos[34] := ConfiguraLinhaE(TbChamada2S30.Value);
  FTextos[35] := ConfiguraLinhaE(TbChamada2S31.Value);
  // for i := Maximo + 1 to FMaxItens do FTextos[i] := '';
  for i := 1 to 35 do FAlinha[i] := taCenter;
  for i := 5 to 35 do FTamCol[i] := 003;
  FTamCol[01] := 004;
  FTamCol[02] := 008;
  FTamCol[03] := 021;
  FTamCol[04] := 010;
end;

function TFmChamada.ConfiguraLinhaE(Texto: String): String;
begin
  if Texto = '' then Texto := ' ';
  Result := '['+Texto+']';
end;

procedure TFmChamada.DefineLinhaF;
const
  Maximo = 35;
var
  i: Integer;
begin
  FTextos[01] := '';
  FTextos[02] := '';
  FTextos[03] := '';
  FTextos[04] := '';
  for i := 5 to 35 do FTextos[i] := '[ ]';
  for i := 1 to 35 do FAlinha[i] := taCenter;
  for i := 5 to 35 do FTamCol[i] := 003;
  FTamCol[01] := 004;
  FTamCol[02] := 008;
  FTamCol[03] := 021;
  FTamCol[04] := 010;
end;

procedure TFmChamada.TintaLaser1Click(Sender: TObject);
begin
  PreparaRelatorio();
  TbChamada1.Close;
  TbChamada1.Open;
  case RGFormatoImp.ItemIndex of
    0: MyObjects.frxMostra(frxChamadaA10, 'Chamada modelo A-10');
    1: MyObjects.frxMostra(frxChamadaB10, 'Chamada modelo B-10');
    2: MyObjects.frxMostra(frxChamadaB12, 'Chamada modelo B-12');
    else Geral.MensagemBox('Relat�rio n�o implementado', 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmChamada.Matricial1Click(Sender: TObject);
begin
  ImprimeMatricial;
end;

procedure TFmChamada.frxChamadaA10GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'MES_REL' then Value :=
    dmkPF.VerificaMes(CBMes.ItemIndex+1, True) + ' DE ' + CBAno.Text
  else if VarName = 'VAR_FIM' then
  begin
    FPagina := FPagina+1;
    if Geral.IMV(EdAtividade.Text) > 0 then Value := True else
    if FPagina < TbChamada1.RecordCount then Value := False else Value := True;
  end;
end;

end.

