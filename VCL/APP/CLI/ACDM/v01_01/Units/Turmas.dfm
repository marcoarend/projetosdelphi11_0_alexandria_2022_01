object FmTurmas: TFmTurmas
  Left = 336
  Top = 173
  Caption = 'TUR-CADAS-001 :: Cadastro de Turmas'
  ClientHeight = 608
  ClientWidth = 1246
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1246
    Height = 490
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Label3: TLabel
      Left = 15
      Top = 5
      Width = 42
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Turma:'
      FocusControl = EdCod
    end
    object Label5: TLabel
      Left = 15
      Top = 59
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome:'
      FocusControl = EdCod
    end
    object LaID: TLabel
      Left = 15
      Top = 113
      Width = 79
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Identifica'#231#227'o:'
      FocusControl = EdCod
    end
    object Label10: TLabel
      Left = 15
      Top = 167
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grupo:'
      FocusControl = EdTurmasGru
    end
    object LaPorcentagem: TLabel
      Left = 404
      Top = 113
      Width = 52
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Lota'#231#227'o:'
      Transparent = True
    end
    object Label9: TLabel
      Left = 15
      Top = 217
      Width = 157
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Professor ou respons'#225'vel:'
      FocusControl = EdRespons
    end
    object SpeedButton5: TSpeedButton
      Left = 453
      Top = 187
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object SpeedButton6: TSpeedButton
      Left = 453
      Top = 236
      Width = 26
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton6Click
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 430
      Width = 1244
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 9
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel4: TPanel
        Left = 1058
        Top = 1
        Width = 185
        Height = 57
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 67
          Top = 3
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdCod: TdmkEdit
      Left = 15
      Top = 25
      Width = 79
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNom: TdmkEdit
      Left = 15
      Top = 79
      Width = 464
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 100
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdID: TdmkEdit
      Left = 15
      Top = 133
      Width = 385
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 30
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object RGAti: TRadioGroup
      Left = 15
      Top = 271
      Width = 464
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Status da turma: '
      Columns = 3
      ItemIndex = 1
      Items.Strings = (
        'Desativada'
        'Em Forma'#231#227'o'
        'Ativa')
      TabOrder = 8
    end
    object EdTurmasGru: TdmkEditCB
      Left = 15
      Top = 187
      Width = 79
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTurmasGru
      IgnoraDBLookupComboBox = False
    end
    object CBTurmasGru: TdmkDBLookupComboBox
      Left = 94
      Top = 187
      Width = 355
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTurmasGru
      TabOrder = 5
      dmkEditCB = EdTurmasGru
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdLotacao: TdmkEdit
      Left = 404
      Top = 133
      Width = 74
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdLotacaoExit
    end
    object EdRespons: TdmkEditCB
      Left = 15
      Top = 236
      Width = 79
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBRespons
      IgnoraDBLookupComboBox = False
    end
    object CBRespons: TdmkDBLookupComboBox
      Left = 94
      Top = 236
      Width = 355
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOME'
      ListSource = DsRespons
      TabOrder = 7
      dmkEditCB = EdRespons
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1246
    Height = 490
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1244
      Height = 109
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 5
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo'
        FocusControl = DBEdCod
      end
      object Label6: TLabel
        Left = 113
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNom
      end
      object Label2: TLabel
        Left = 12
        Top = 54
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Identifica'#231#227'o:'
        FocusControl = DBEdID
      end
      object Label4: TLabel
        Left = 155
        Top = 54
        Width = 63
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Peso total:'
        FocusControl = DBEdit1
      end
      object Label7: TLabel
        Left = 591
        Top = 54
        Width = 157
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Professor ou respons'#225'vel:'
        FocusControl = DBEdit2
      end
      object Label8: TLabel
        Left = 876
        Top = 5
        Width = 52
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lota'#231#227'o:'
        FocusControl = DBEdLot
      end
      object Label11: TLabel
        Left = 620
        Top = 5
        Width = 40
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupo:'
        FocusControl = DBEdit1
      end
      object DBEdCod: TDBEdit
        Left = 12
        Top = 25
        Width = 98
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTurmas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdNom: TDBEdit
        Left = 113
        Top = 25
        Width = 504
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Nome'
        DataSource = DsTurmas
        TabOrder = 1
      end
      object DBRGAti: TDBRadioGroup
        Left = 254
        Top = 54
        Width = 331
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Status da Turma: '
        Columns = 3
        DataField = 'Ativa'
        DataSource = DsTurmas
        Items.Strings = (
          'Desativada'
          'Em forma'#231#227'o'
          'Ativa')
        ParentBackground = True
        TabOrder = 2
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBEdID: TDBEdit
        Left = 12
        Top = 74
        Width = 137
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ID'
        DataSource = DsTurmas
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Left = 155
        Top = 74
        Width = 90
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TotalPeso'
        DataSource = DsTurmas
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 591
        Top = 73
        Width = 375
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMERESPONS'
        DataSource = DsTurmas
        TabOrder = 5
      end
      object DBEdLot: TDBEdit
        Left = 876
        Top = 25
        Width = 90
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Lotacao'
        DataSource = DsTurmas
        TabOrder = 6
      end
      object DBEdit3: TDBEdit
        Left = 620
        Top = 25
        Width = 253
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMEGRUPO'
        DataSource = DsTurmas
        TabOrder = 7
      end
    end
    object PainelDados1: TPanel
      Left = 1
      Top = 133
      Width = 973
      Height = 367
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 1
        Top = 117
        Width = 971
        Height = 4
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ExplicitWidth = 970
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 971
        Height = 116
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        DataSource = DsTurmasIts
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Refere'
            Title.Caption = 'Refer'#234'ncia'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECURSO'
            Title.Caption = 'Curso / Atividade'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPROFESSOR'
            Title.Caption = 'Professor / Instrutor'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMESALA'
            Title.Caption = 'Local'
            Width = 90
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Peso'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PORCENTAGEM'
            Title.Caption = '% Peso'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CARGA_MIN'
            Title.Caption = 'Carga'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Honorarios'
            Title.Caption = 'Honor'#225'rios'
            Width = 75
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 1
        Top = 121
        Width = 971
        Height = 245
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsTurmasTmp
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o do per'#237'odo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DIASEMANA'
            Title.Caption = 'Dia da semana'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'HIni'
            Title.Alignment = taCenter
            Title.Caption = 'In'#237'cio'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'HFim'
            Title.Alignment = taCenter
            Title.Caption = 'Final'
            Visible = True
          end>
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1246
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                                  Cadastro de Turmas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 1144
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 1143
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 866
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitWidth = 865
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 549
    Width = 1246
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object LaRegistro: TLabel
      Left = 213
      Top = 1
      Width = 455
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = '[N]: 0'
      ExplicitWidth = 31
      ExplicitHeight = 16
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 212
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 158
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 108
        Top = 5
        Width = 50
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 59
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 10
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 668
      Top = 1
      Width = 577
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 458
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtExclui: TBitBtn
        Left = -615
        Top = -556
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 118
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 5
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
      end
      object BitBtn1: TBitBtn
        Tag = 12
        Left = 231
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BitBtn1Click
      end
    end
  end
  object DsTurmas: TDataSource
    DataSet = QrTurmas
    Left = 544
    Top = 129
  end
  object QrTurmas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTurmasBeforeOpen
    AfterOpen = QrTurmasAfterOpen
    AfterScroll = QrTurmasAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'else en.Nome END NOMERESPONS, '
      'gr.Nome NOMEGRUPO, tu.*'
      'FROM turmas tu'
      'LEFT JOIN entidades en ON en.Codigo=tu.Respons'
      'LEFT JOIN turmasgru gr ON gr.Codigo=tu.Grupo')
    Left = 516
    Top = 129
    object QrTurmasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTurmasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTurmasID: TWideStringField
      FieldName = 'ID'
      Size = 30
    end
    object QrTurmasAtiva: TSmallintField
      FieldName = 'Ativa'
    end
    object QrTurmasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTurmasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTurmasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTurmasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTurmasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTurmasTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTurmasRespons: TIntegerField
      FieldName = 'Respons'
    end
    object QrTurmasNOMERESPONS: TWideStringField
      FieldName = 'NOMERESPONS'
      Size = 100
    end
    object QrTurmasLotacao: TIntegerField
      FieldName = 'Lotacao'
      Required = True
    end
    object QrTurmasGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrTurmasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object PMInclui: TPopupMenu
    OnPopup = PMIncluiPopup
    Left = 568
    Top = 401
    object Turma2: TMenuItem
      Caption = '&Turma'
      OnClick = Turma2Click
    end
    object Curso2: TMenuItem
      Caption = '&Curso/Atividade'
      OnClick = Curso2Click
    end
    object Horrio2: TMenuItem
      Caption = '&Hor'#225'rio'
      object Umaum1: TMenuItem
        Caption = '&Um a um'
        OnClick = Umaum1Click
      end
      object Gerenciatodos1: TMenuItem
        Caption = '&Gerencia todos'
        OnClick = Gerenciatodos1Click
      end
    end
  end
  object PMAltera: TPopupMenu
    OnPopup = PMAlteraPopup
    Left = 660
    Top = 401
    object Turma1: TMenuItem
      Caption = '&Turma'
      OnClick = Turma1Click
    end
    object Curso1: TMenuItem
      Caption = '&Curso/Atividade'
      OnClick = Curso1Click
    end
    object Horrio1: TMenuItem
      Caption = '&Hor'#225'rio'
      Enabled = False
      OnClick = Horrio1Click
    end
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 756
    Top = 405
    object Turma3: TMenuItem
      Caption = '&Turma'
      Enabled = False
      OnClick = Turma3Click
    end
    object Curso3: TMenuItem
      Caption = '&Curso/Atividade'
      OnClick = Curso3Click
    end
    object Horrio3: TMenuItem
      Caption = '&Hor'#225'rio'
      OnClick = Horrio3Click
    end
  end
  object QrTurmasTmp: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTurmasTmpCalcFields
    SQL.Strings = (
      'SELECT tt.*, pe.* '
      'FROM turmastmp tt'
      'LEFT JOIN periodos pe ON pe.Codigo=tt.Periodo'
      'WHERE tt.Codigo=:P0'
      'AND tt.Controle=:P1'
      'ORDER BY DSem, HIni')
    Left = 516
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTurmasTmpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTurmasTmpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTurmasTmpConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrTurmasTmpLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTurmasTmpDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTurmasTmpDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTurmasTmpUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTurmasTmpUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTurmasTmpPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrTurmasTmpDIASEMANA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DIASEMANA'
      Size = 30
      Calculated = True
    end
    object QrTurmasTmpCodigo_1: TIntegerField
      FieldName = 'Codigo_1'
    end
    object QrTurmasTmpNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrTurmasTmpDSem: TIntegerField
      FieldName = 'DSem'
    end
    object QrTurmasTmpHIni: TTimeField
      FieldName = 'HIni'
      DisplayFormat = 'hh:nn'
    end
    object QrTurmasTmpHFim: TTimeField
      FieldName = 'HFim'
      DisplayFormat = 'hh:nn'
    end
    object QrTurmasTmpLk_1: TIntegerField
      FieldName = 'Lk_1'
    end
    object QrTurmasTmpDataCad_1: TDateField
      FieldName = 'DataCad_1'
    end
    object QrTurmasTmpDataAlt_1: TDateField
      FieldName = 'DataAlt_1'
    end
    object QrTurmasTmpUserCad_1: TIntegerField
      FieldName = 'UserCad_1'
    end
    object QrTurmasTmpUserAlt_1: TIntegerField
      FieldName = 'UserAlt_1'
    end
  end
  object DsTurmasTmp: TDataSource
    DataSet = QrTurmasTmp
    Left = 544
    Top = 233
  end
  object QrTurmasIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrTurmasItsAfterOpen
    AfterScroll = QrTurmasItsAfterScroll
    OnCalcFields = QrTurmasItsCalcFields
    SQL.Strings = (
      'SELECT ti.*,'
      'sa.Nome NOMESALA,'
      'cu.Nome NOMECURSO,'
      'pr.Nome NOMEPROFESSOR '
      'FROM turmasits ti, Salas sa,'
      'Cursos cu, Entidades pr'
      'WHERE ti.Codigo=:P0'
      'AND sa.Codigo=ti.Sala'
      'AND cu.Codigo=ti.Curso'
      'AND pr.Codigo=ti.Professor'
      'ORDER BY cu.Nome')
    Left = 516
    Top = 181
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTurmasItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTurmasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTurmasItsSala: TIntegerField
      FieldName = 'Sala'
      Required = True
    end
    object QrTurmasItsCurso: TIntegerField
      FieldName = 'Curso'
      Required = True
    end
    object QrTurmasItsProfessor: TIntegerField
      FieldName = 'Professor'
      Required = True
    end
    object QrTurmasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTurmasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTurmasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTurmasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTurmasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTurmasItsNOMESALA: TWideStringField
      FieldName = 'NOMESALA'
      Required = True
      Size = 50
    end
    object QrTurmasItsNOMECURSO: TWideStringField
      FieldName = 'NOMECURSO'
      Required = True
      Size = 50
    end
    object QrTurmasItsNOMEPROFESSOR: TWideStringField
      FieldName = 'NOMEPROFESSOR'
      Required = True
      Size = 100
    end
    object QrTurmasItsPeso: TFloatField
      FieldName = 'Peso'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrTurmasItsPORCENTAGEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PORCENTAGEM'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrTurmasItsTotalPeso: TFloatField
      FieldName = 'TotalPeso'
      Required = True
    end
    object QrTurmasItsCarga: TFloatField
      FieldName = 'Carga'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrTurmasItsHonorarios: TFloatField
      FieldName = 'Honorarios'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrTurmasItsDataI: TDateField
      FieldName = 'DataI'
      Required = True
    end
    object QrTurmasItsRefere: TWideStringField
      FieldName = 'Refere'
      Required = True
      Size = 30
    end
    object QrTurmasItsCARGA_MIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CARGA_MIN'
      Size = 5
      Calculated = True
    end
  end
  object DsTurmasIts: TDataSource
    DataSet = QrTurmasIts
    Left = 544
    Top = 181
  end
  object QrSPCM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Peso) Peso '
      'FROM turmasits'
      'WHERE Codigo=:P0')
    Left = 524
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSPCMPeso: TFloatField
      FieldName = 'Peso'
    end
  end
  object QrRespons: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOME'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NOME')
    Left = 612
    Top = 145
    object QrResponsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrResponsNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object DsRespons: TDataSource
    DataSet = QrRespons
    Left = 640
    Top = 145
  end
  object QrTurmasGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM turmasgru'
      'ORDER BY Nome'
      '')
    Left = 728
    Top = 213
  end
  object DsTurmasGru: TDataSource
    DataSet = QrTurmasGru
    Left = 756
    Top = 213
  end
end
