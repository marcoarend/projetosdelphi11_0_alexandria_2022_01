object FmSWMS_Mtng_PrvUni: TFmSWMS_Mtng_PrvUni
  Left = 339
  Top = 185
  Caption = 'MET-GEREN-003 :: Cadastro de Prova'
  ClientHeight = 561
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 630
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 582
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 534
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 226
        Height = 32
        Caption = 'Cadastro de Prova'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 226
        Height = 32
        Caption = 'Cadastro de Prova'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 226
        Height = 32
        Caption = 'Cadastro de Prova'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 117
    Width = 630
    Height = 244
    Align = alTop
    Caption = ' Dados da Prova: '
    TabOrder = 1
    object PainelEdit: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 38
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 224
        Top = 12
        Width = 82
        Height = 13
        Caption = 'Sexo (M, F ou A):'
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 217
        Height = 38
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label29: TLabel
          Left = 8
          Top = 12
          Width = 66
          Height = 13
          Caption = 'C'#243'digo prova:'
          FocusControl = EdCodTxt
        end
        object EdCodTxt: TdmkEdit
          Left = 80
          Top = 10
          Width = 128
          Height = 21
          MaxLength = 20
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CodTxt'
          UpdCampo = 'CodTxt'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
      object EdSexo: TdmkEdit
        Left = 312
        Top = 10
        Width = 20
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 1
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sexo'
        UpdCampo = 'Sexo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdSexoChange
      end
      object EdSexo_Txt: TdmkEdit
        Left = 332
        Top = 10
        Width = 105
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GroupBox4: TGroupBox
      Left = 2
      Top = 53
      Width = 626
      Height = 189
      Align = alClient
      Caption = ' Configura'#231#227'o da prova:'
      TabOrder = 1
      object RGMetros: TdmkRadioGroup
        Left = 2
        Top = 15
        Width = 622
        Height = 86
        Align = alTop
        Caption = ' Dist'#226'ncia em metros: '
        Columns = 9
        ItemIndex = 0
        Items.Strings = (
          #39'0'#39','
          #39'1 x 20'#39','
          #39'1 x 25'#39','
          #39'1 x 40'#39','
          #39'1 x 50'#39','
          #39'1 x 80'#39','
          #39'1 x 100'#39','
          #39'1 x 200'#39','
          #39'1 x 400'#39','
          #39'1 x 800'#39','
          #39'1 x 1500'#39','
          #39'4 x 20'#39','
          #39'4 x 25'#39','
          #39'4 x 40'#39','
          #39'4 x 50'#39','
          #39'4 x 80'#39','
          #39'4 x 100'#39','
          #39'4 x 200'#39)
        TabOrder = 0
        QryCampo = 'Metros'
        UpdCampo = 'Metros'
        UpdType = utYes
        OldValor = 0
      end
      object Panel5: TPanel
        Left = 2
        Top = 101
        Width = 622
        Height = 86
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object RGEstilo: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 488
          Height = 86
          Align = alLeft
          Caption = ' Estilo: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            #39'Nenhum'#39','
            #39'Livre'#39','
            #39'Costas'#39','
            #39'Peito'#39','
            #39'Borboleta'#39','
            #39'Medley'#39','
            #39'Revezam. livre'#39','
            #39'Revezam. estilos'#39)
          TabOrder = 0
          QryCampo = 'Estilo'
          UpdCampo = 'Estilo'
          UpdType = utYes
          OldValor = 0
        end
        object Panel6: TPanel
          Left = 488
          Top = 0
          Width = 134
          Height = 86
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label27: TLabel
            Left = 4
            Top = 4
            Width = 98
            Height = 13
            Caption = 'Categoria inicial [F3]:'
          end
          object Label28: TLabel
            Left = 4
            Top = 44
            Width = 91
            Height = 13
            Caption = 'Categoria final [F3]:'
          end
          object EdCategoriaIni: TdmkEdit
            Left = 4
            Top = 20
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CategoriaIni'
            UpdCampo = 'CategoriaIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCategoriaIniChange
            OnKeyDown = EdCategoriaIniKeyDown
          end
          object EdCategoriaIni_TXT: TdmkEdit
            Left = 28
            Top = 20
            Width = 96
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Nenhuma'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Nenhuma'
            OnKeyDown = EdCategoriaIni_TXTKeyDown
          end
          object EdCategoriaFim: TdmkEdit
            Left = 4
            Top = 59
            Width = 24
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CategoriaFim'
            UpdCampo = 'CategoriaFim'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCategoriaFimChange
            OnKeyDown = EdCategoriaFimKeyDown
          end
          object EdCategoriaFim_TXT: TdmkEdit
            Left = 28
            Top = 59
            Width = 96
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Nenhuma'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Nenhuma'
            OnKeyDown = EdCategoriaFim_TXTKeyDown
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 491
    Width = 630
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 484
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 421
    Width = 630
    Height = 26
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object CkContinuar: TCheckBox
      Left = 12
      Top = 4
      Width = 141
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 447
    Width = 630
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 48
    Width = 630
    Height = 69
    Align = alTop
    Caption = ' Dados da competi'#231#227'o: '
    Enabled = False
    TabOrder = 0
    object Label10: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label11: TLabel
      Left = 72
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label12: TLabel
      Left = 132
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label13: TLabel
      Left = 528
      Top = 20
      Width = 45
      Height = 13
      Caption = 'ID Prova:'
      FocusControl = DBEdit1
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 38
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmSWMS_Mtng_Cab.DsSWMS_Mtng_Cab
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdit1: TDBEdit
      Left = 72
      Top = 38
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'CodUsu'
      DataSource = FmSWMS_Mtng_Cab.DsSWMS_Mtng_Cab
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 132
      Top = 38
      Width = 392
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object EdControle: TdmkEdit
      Left = 528
      Top = 38
      Width = 80
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 361
    Width = 630
    Height = 60
    Align = alTop
    Caption = ' Dados da Piscina: '
    TabOrder = 6
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 82
      Height = 13
      Caption = 'Nome da piscina:'
    end
    object Label2: TLabel
      Left = 528
      Top = 16
      Width = 35
      Height = 13
      Caption = 'Metros:'
    end
    object Label3: TLabel
      Left = 572
      Top = 16
      Width = 30
      Height = 13
      Caption = 'Raias:'
    end
    object EdPiscDescr: TdmkEdit
      Left = 12
      Top = 32
      Width = 380
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'PiscDescr'
      UpdCampo = 'PiscDescr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdPiscCompr: TdmkEdit
      Left = 528
      Top = 32
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PiscCompr'
      UpdCampo = 'PiscCompr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdPiscRaias: TdmkEdit
      Left = 572
      Top = 32
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PiscRaias'
      UpdCampo = 'PiscRaias'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object RGPiorRaia: TdmkRadioGroup
      Left = 400
      Top = 8
      Width = 125
      Height = 48
      Caption = ' Pior raia: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Prim.'
        #218'ltim.')
      TabOrder = 1
      QryCampo = 'PiorRaia'
      UpdCampo = 'PiorRaia'
      UpdType = utYes
      OldValor = 0
    end
  end
end
