unit Departamentos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, UnDmkProcFunc;

type
  TFmDepartamentos = class(TForm)
    PainelDados: TPanel;
    DsDepartamentos: TDataSource;
    QrDepartamentos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Painel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrDepartamentosCodigo: TIntegerField;
    QrDepartamentosNome: TWideStringField;
    QrDepartamentosLk: TIntegerField;
    QrDepartamentosDataCad: TDateField;
    QrDepartamentosDataAlt: TDateField;
    QrDepartamentosUserCad: TIntegerField;
    QrDepartamentosUserAlt: TIntegerField;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDepartamentosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrDepartamentosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDepartamentosBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmDepartamentos: TFmDepartamentos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDepartamentos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDepartamentos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDepartamentosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDepartamentos.DefParams;
begin
  VAR_GOTOTABELA := 'Departamentos';
  VAR_GOTOMYSQLTABLE := QrDepartamentos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM departamentos');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmDepartamentos.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
    end;
    EdNome.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmDepartamentos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDepartamentos.AlteraRegistro;
var
  Departamentos : Integer;
begin
  Departamentos := QrDepartamentosCodigo.Value;
  if QrDepartamentosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Departamentos, Dmod.MyDB, 'Departamentos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Departamentos, Dmod.MyDB, 'Departamentos', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmDepartamentos.IncluiRegistro;
var
  Cursor : TCursor;
  Departamentos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Departamentos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                     'Departamentos', 'Departamentos', 'Codigo');
    //
    if Length(FormatFloat(FFormatFloat, Departamentos))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Departamentos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmDepartamentos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDepartamentos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDepartamentos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDepartamentos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDepartamentos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDepartamentos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDepartamentos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmDepartamentos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmDepartamentos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDepartamentosCodigo.Value;
  Close;
end;

procedure TFmDepartamentos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO departamentos SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE departamentos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Departamentos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmDepartamentos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Departamentos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Departamentos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Departamentos', 'Codigo');
end;

procedure TFmDepartamentos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmDepartamentos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDepartamentosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmDepartamentos.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmDepartamentos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDepartamentos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmDepartamentos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmDepartamentos.QrDepartamentosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDepartamentos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Departamentos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmDepartamentos.QrDepartamentosAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrDepartamentosCodigo.Value, False);
end;

procedure TFmDepartamentos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDepartamentosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Departamentos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDepartamentos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmDepartamentos.QrDepartamentosBeforeOpen(DataSet: TDataSet);
begin
  QrDepartamentosCodigo.DisplayFormat := FFormatFloat;
end;

end.

