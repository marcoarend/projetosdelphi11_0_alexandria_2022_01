unit Cursos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkEdit, UnDmkProcFunc, UnDmkEnums;

type
  TFmCursos = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    DsCursos: TDataSource;
    QrCursos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Panel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    QrCursosLk: TIntegerField;
    QrCursosDataCad: TDateField;
    QrCursosDataAlt: TDateField;
    QrCursosUserCad: TIntegerField;
    QrCursosUserAlt: TIntegerField;
    QrCursosCodigo: TSmallintField;
    QrCursosNome: TWideStringField;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    CkAtivo: TCheckBox;
    QrCursosAtivo: TIntegerField;
    DBCkAtivo: TDBCheckBox;
    Label8: TLabel;
    EdCarga: TdmkEdit;
    QrCursosCarga: TFloatField;
    Label3: TLabel;
    DBEdCarga: TDBEdit;
    EdNome: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrCursosCARGA_MIN: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCursosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCursosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCursosBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrCursosCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCursos: TFmCursos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCursos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCursos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCursosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCursos.DefParams;
begin
  VAR_GOTOTABELA := 'Cursos';
  VAR_GOTOMYSQLTABLE := QrCursos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cursos');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCursos.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible    := True;
    PainelDados.Visible    := False;
    PainelControle.Visible := False;
    //
    EdNome.Text    := CO_VAZIO;
    EdNome.Visible := True;
    //
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.Text        := FormatFloat(FFormatFloat, Codigo);
      EdCarga.ValueVariant := '00:00';
      CkAtivo.Checked      := True;
    end else
    begin
      EdCodigo.Text   := DBEdit1.Text;
      EdNome.Text     := DBEdNome.Text;
      EdCarga.Text    := AppPF.Carga_HoraToTime(QrCursosCarga.Value);
      CkAtivo.Checked := DBCkAtivo.Checked;
    end;
    EdNome.SetFocus;
  end else
  begin
    PainelControle.Visible := True;
    PainelDados.Visible    := True;
    PainelEdita.Visible    := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmCursos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCursos.AlteraRegistro;
var
  Cursos : Integer;
begin
  Cursos := QrCursosCodigo.Value;
  if QrCursosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Cursos, Dmod.MyDB, 'Cursos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Cursos, Dmod.MyDB, 'Cursos', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCursos.IncluiRegistro;
var
  Cursor : TCursor;
  Cursos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Cursos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Cursos', 'Cursos', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Cursos))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Cursos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCursos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCursos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCursos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCursos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCursos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCursos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCursos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCursos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCursos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCursosCodigo.Value;
  Close;
end;

procedure TFmCursos.BtConfirmaClick(Sender: TObject);
var
  Codigo, Ativo: Integer;
  Carga: Double;
  Nome : String;
begin
  if EdCarga.Text = CO_VAZIO then
    EdCarga.Text := '00:00';
  //
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if CkAtivo.Checked then
    Ativo := 1
  else
    Ativo := 0;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  Carga  := AppPF.Carga_TimeToHora(StrToTime(EdCarga.Text));
  //
  Dmod.QrUpdU.SQL.Clear;
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO cursos SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE cursos SET ');
  //
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Ativo=:P1, Carga=:P2,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Ativo;
  Dmod.QrUpdU.Params[02].AsFloat   := Carga;
  //
  Dmod.QrUpdU.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[05].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cursos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmCursos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Cursos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cursos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cursos', 'Codigo');
end;

procedure TFmCursos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
end;

procedure TFmCursos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCursosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCursos.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCursos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCursos.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCursos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCursos.QrCursosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCursos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Cursos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmCursos.QrCursosAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrCursosCodigo.Value, False);
end;

procedure TFmCursos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCursosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Cursos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCursos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCursos.QrCursosBeforeOpen(DataSet: TDataSet);
begin
  QrCursosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCursos.QrCursosCalcFields(DataSet: TDataSet);
begin
  QrCursosCARGA_MIN.Value := AppPF.Carga_HoraToTime(QrCursosCarga.Value);
end;

end.

