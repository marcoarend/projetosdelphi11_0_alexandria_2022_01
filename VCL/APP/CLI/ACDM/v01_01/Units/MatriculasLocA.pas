unit MatriculasLocA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  Grids, DBGrids, ZCF2, UnMLAGeral, UnInternalConsts;


type
  TFmMatriculasLocA = class(TForm)
    PainelControle: TPanel;
    PainelDados: TPanel;
    PainelTitulo: TPanel;
    GroupBox1: TGroupBox;
    BitBtn1: TBitBtn;
    DBGrid1: TDBGrid;
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMatriculasLocA: TFmMatriculasLocA;

implementation

uses Matriculas;
{$R *.DFM}

procedure TFmMatriculasLocA.BitBtn1Click(Sender: TObject);
begin
  Close;
end;
procedure VerificaOrigem;
begin
  VAR_CODIGO := FmMatriculas.QrLocACodigo.Value;
end;

procedure TFmMatriculasLocA.DBGrid1CellClick(Column: TColumn);
begin
  VerificaOrigem;
end;

procedure TFmMatriculasLocA.DBGrid1DblClick(Sender: TObject);
begin
  VerificaOrigem;
  Close;
end;

procedure TFmMatriculasLocA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
