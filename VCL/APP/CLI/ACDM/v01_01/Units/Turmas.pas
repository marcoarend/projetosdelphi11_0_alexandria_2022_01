unit Turmas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmTurmas = class(TForm)
    PainelDados: TPanel;
    DsTurmas: TDataSource;
    QrTurmas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Panel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    DBEdCod: TDBEdit;
    DBEdNom: TDBEdit;
    DBRGAti: TDBRadioGroup;
    DBEdID: TDBEdit;
    PainelDados1: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    PMInclui: TPopupMenu;
    Turma2: TMenuItem;
    Curso2: TMenuItem;
    Horrio2: TMenuItem;
    PMAltera: TPopupMenu;
    Turma1: TMenuItem;
    Curso1: TMenuItem;
    Horrio1: TMenuItem;
    PMExclui: TPopupMenu;
    Turma3: TMenuItem;
    Curso3: TMenuItem;
    Horrio3: TMenuItem;
    QrTurmasCodigo: TIntegerField;
    QrTurmasNome: TWideStringField;
    QrTurmasID: TWideStringField;
    QrTurmasAtiva: TSmallintField;
    QrTurmasLk: TIntegerField;
    QrTurmasDataCad: TDateField;
    QrTurmasDataAlt: TDateField;
    QrTurmasUserCad: TIntegerField;
    QrTurmasUserAlt: TIntegerField;
    Label3: TLabel;
    EdCod: TdmkEdit;
    Label5: TLabel;
    EdNom: TdmkEdit;
    LaID: TLabel;
    EdID: TdmkEdit;
    RGAti: TRadioGroup;
    QrTurmasTmp: TmySQLQuery;
    DsTurmasTmp: TDataSource;
    QrTurmasIts: TmySQLQuery;
    DsTurmasIts: TDataSource;
    QrTurmasItsCodigo: TIntegerField;
    QrTurmasItsControle: TIntegerField;
    QrTurmasItsSala: TIntegerField;
    QrTurmasItsCurso: TIntegerField;
    QrTurmasItsProfessor: TIntegerField;
    QrTurmasItsLk: TIntegerField;
    QrTurmasItsDataCad: TDateField;
    QrTurmasItsDataAlt: TDateField;
    QrTurmasItsUserCad: TIntegerField;
    QrTurmasItsUserAlt: TIntegerField;
    QrTurmasItsNOMESALA: TWideStringField;
    QrTurmasItsNOMECURSO: TWideStringField;
    QrTurmasItsNOMEPROFESSOR: TWideStringField;
    QrTurmasItsPeso: TFloatField;
    QrSPCM: TmySQLQuery;
    Splitter1: TSplitter;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    QrTurmasTotalPeso: TFloatField;
    QrTurmasItsPORCENTAGEM: TFloatField;
    QrTurmasItsTotalPeso: TFloatField;
    QrSPCMPeso: TFloatField;
    Label10: TLabel;
    EdTurmasGru: TdmkEditCB;
    CBTurmasGru: TdmkDBLookupComboBox;
    QrRespons: TmySQLQuery;
    DsRespons: TDataSource;
    QrResponsCodigo: TIntegerField;
    QrResponsNOME: TWideStringField;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    QrTurmasRespons: TIntegerField;
    QrTurmasNOMERESPONS: TWideStringField;
    QrTurmasItsCarga: TFloatField;
    QrTurmasItsHonorarios: TFloatField;
    QrTurmasItsDataI: TDateField;
    QrTurmasTmpCodigo: TIntegerField;
    QrTurmasTmpControle: TIntegerField;
    QrTurmasTmpConta: TIntegerField;
    QrTurmasTmpLk: TIntegerField;
    QrTurmasTmpDataCad: TDateField;
    QrTurmasTmpDataAlt: TDateField;
    QrTurmasTmpUserCad: TIntegerField;
    QrTurmasTmpUserAlt: TIntegerField;
    QrTurmasTmpPeriodo: TIntegerField;
    QrTurmasTmpDIASEMANA: TWideStringField;
    QrTurmasTmpCodigo_1: TIntegerField;
    QrTurmasTmpNome: TWideStringField;
    QrTurmasTmpDSem: TIntegerField;
    QrTurmasTmpHIni: TTimeField;
    QrTurmasTmpHFim: TTimeField;
    QrTurmasTmpLk_1: TIntegerField;
    QrTurmasTmpDataCad_1: TDateField;
    QrTurmasTmpDataAlt_1: TDateField;
    QrTurmasTmpUserCad_1: TIntegerField;
    QrTurmasTmpUserAlt_1: TIntegerField;
    QrTurmasItsRefere: TWideStringField;
    LaPorcentagem: TLabel;
    EdLotacao: TdmkEdit;
    QrTurmasLotacao: TIntegerField;
    Label8: TLabel;
    DBEdLot: TDBEdit;
    Label9: TLabel;
    EdRespons: TdmkEditCB;
    CBRespons: TdmkDBLookupComboBox;
    QrTurmasGru: TmySQLQuery;
    DsTurmasGru: TDataSource;
    Label11: TLabel;
    QrTurmasGrupo: TIntegerField;
    QrTurmasNOMEGRUPO: TWideStringField;
    DBEdit3: TDBEdit;
    BitBtn1: TBitBtn;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Umaum1: TMenuItem;
    Gerenciatodos1: TMenuItem;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    QrTurmasItsCARGA_MIN: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTurmasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrTurmasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTurmasBeforeOpen(DataSet: TDataSet);
    procedure Turma2Click(Sender: TObject);
    procedure Curso2Click(Sender: TObject);
    procedure Turma1Click(Sender: TObject);
    procedure Curso1Click(Sender: TObject);
    procedure Horrio1Click(Sender: TObject);
    procedure Turma3Click(Sender: TObject);
    procedure Curso3Click(Sender: TObject);
    procedure Horrio3Click(Sender: TObject);
    procedure QrTurmasItsAfterOpen(DataSet: TDataSet);
    procedure QrTurmasItsAfterScroll(DataSet: TDataSet);
    procedure QrTurmasTmpCalcFields(DataSet: TDataSet);
    procedure QrTurmasItsCalcFields(DataSet: TDataSet);
    procedure EdLotacaoExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Gerenciatodos1Click(Sender: TObject);
    procedure Umaum1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
    procedure PMExcluiPopup(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    //procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure IncluiRegistro1;
    procedure IncluiRegistro2;
    procedure AlteraRegistro;
    procedure AlteraRegistro1;
    procedure ReabreSub1;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure AtualizaTotalPeso(Turma: Integer);
  public
    { Public declarations }
    procedure ReopenTurmasTmp();
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTurmas: TFmTurmas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, TurmasEdit, TurmasTmp, MyDBCheck, Principal,
ModuleGeral, TurmasTmp2, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTurmas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTurmas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTurmasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTurmas.DefParams;
begin
  VAR_GOTOTABELA := 'Turmas';
  VAR_GOTOMYSQLTABLE := QrTurmas;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('else en.Nome END NOMERESPONS,');
  VAR_SQLx.Add('gr.Nome NOMEGRUPO, tu.*');
  VAR_SQLx.Add('FROM turmas tu');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=tu.Respons');
  VAR_SQLx.Add('LEFT JOIN turmasgru gr ON gr.Codigo=tu.Grupo');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE tu.Codigo > 0');
  //
  VAR_SQL1.Add('AND tu.Codigo=:P0');
  //
  VAR_SQLa.Add('AND tu.Nome Like :P0');
  //
end;

procedure TFmTurmas.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible    := True;
    PainelDados.Visible    := False;
    EdNom.Text             := CO_VAZIO;
    EdNom.Visible          := True;
    PainelControle.Visible := False;
    //
    if Status = CO_INCLUSAO then
    begin
      EdCod.Text := FormatFloat(FFormatFloat, Codigo);
    end else
    begin
      EdCod.Text           := DBEdCod.Text;
      EdLotacao.Text       := DBEdLot.Text;
      EdNom.Text           := DBEdNom.Text;
      EdID.Text            := DBEdID .Text;
      RGAti.ItemIndex      := DBRGAti.ItemIndex;
      EdRespons.Text       := Geral.FF0(QrTurmasRespons.Value);
      CBRespons.KeyValue   := QrTurmasRespons.Value;
      EdTurmasGru.Text     := Geral.FF0(QrTurmasGrupo.Value);
      CBTurmasGru.KeyValue := QrTurmasGrupo.Value;
    end;
    EdNom.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmTurmas.PMAlteraPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrTurmas.State <> dsInactive) and (QrTurmas.RecordCount > 0);
  Enab2 := (QrTurmasIts.State <> dsInactive) and (QrTurmasIts.RecordCount > 0);
  Enab3 := (QrTurmasTmp.State <> dsInactive) and (QrTurmasTmp.RecordCount > 0);
  //
  Turma1.Enabled  := Enab;
  Curso1.Enabled  := Enab and Enab2;
  Horrio1.Enabled := Enab and Enab2 and Enab3;
end;

procedure TFmTurmas.PMExcluiPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrTurmas.State <> dsInactive) and (QrTurmas.RecordCount > 0);
  Enab2 := (QrTurmasIts.State <> dsInactive) and (QrTurmasIts.RecordCount > 0);
  Enab3 := (QrTurmasTmp.State <> dsInactive) and (QrTurmasTmp.RecordCount > 0);
  //
  Turma3.Enabled  := False;
  Curso3.Enabled  := Enab and Enab2;
  Horrio3.Enabled := Enab and Enab2 and Enab3;
end;

procedure TFmTurmas.PMIncluiPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrTurmas.State <> dsInactive) and (QrTurmas.RecordCount > 0);
  Enab2 := (QrTurmasIts.State <> dsInactive) and (QrTurmasIts.RecordCount > 0);
  //
  Curso2.Enabled  := Enab;
  Horrio2.Enabled := Enab and Enab2;
end;

procedure TFmTurmas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTurmas.AlteraRegistro;
var
  Turmas : Integer;
begin
  Turmas := QrTurmasCodigo.Value;
  if QrTurmasCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Turmas, Dmod.MyDB, 'Turmas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Turmas, Dmod.MyDB, 'Turmas', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmTurmas.IncluiRegistro;
var
  Cursor : TCursor;
  Turmas : Integer;
begin
  Cursor        := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Turmas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                'Turmas', 'Turmas', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Turmas))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Turmas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmTurmas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTurmas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTurmas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTurmas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTurmas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTurmas.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroDeGruposDeTurmas(EdTurmasGru.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTurmasGru, CBTurmasGru, QrTurmasGru, VAR_CADASTRO);
    EdTurmasGru.SetFocus;
  end;
end;

procedure TFmTurmas.SpeedButton6Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdRespons.ValueVariant;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, False, False,
    nil, nil, False, uetCliente2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdRespons, CBRespons, QrRespons, VAR_CADASTRO);
    EdRespons.SetFocus;
  end;
end;

procedure TFmTurmas.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmTurmas.BitBtn1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BitBtn1);
end;

procedure TFmTurmas.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmTurmas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTurmasCodigo.Value;
  Close;
end;

procedure TFmTurmas.BtConfirmaClick(Sender: TObject);
var
  Codigo, Respons: Integer;
  Nome: String;
begin
  Nome    := EdNom.ValueVariant;
  Codigo  := EdCod.ValueVariant;
  Respons := EdRespons.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNom, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Respons < 1, EdRespons, 'Defina um professor/respons�vel!') then Exit;
  //
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO turmas SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE turmas SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ID=:P1, Ativa=:P2, Respons=:P3, ');
  Dmod.QrUpdU.SQL.Add('Lotacao=:P4, Grupo=:P5, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := EdID.Text;
  Dmod.QrUpdU.Params[02].AsInteger := RGAti.ItemIndex;
  Dmod.QrUpdU.Params[03].AsInteger := Respons;
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdLotacao.Text);
  Dmod.QrUpdU.Params[05].AsInteger := Geral.IMV(EdTurmasGru.Text);
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Turmas', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmTurmas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCod.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Turmas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Turmas', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Turmas', 'Codigo');
end;

procedure TFmTurmas.FormCreate(Sender: TObject);
begin
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelDados1.Align := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrRespons, DMod.MyDB);
  UMyMod.AbreQuery(QrTurmasGru, DMod.MyDB);
end;

procedure TFmTurmas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTurmasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmTurmas.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmTurmas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTurmas.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmTurmas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmTurmas.QrTurmasAfterOpen(DataSet: TDataSet);
begin
  ReabreSub1;
end;

procedure TFmTurmas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Turmas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmTurmas.QrTurmasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrTurmasCodigo.Value, False);
end;

procedure TFmTurmas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTurmasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Turmas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTurmas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTurmas.Gerenciatodos1Click(Sender: TObject);
begin
  if (QrTurmasItsCodigo.Value = 0) or (QrTurmasItsControle.Value = 0) then
  begin
    Geral.MB_Aviso('N�o h� curso cadastrado!');
    Exit;
  end;
  Refresh;
  if DBCheck.CriaFm(TFmTurmasTmp2, FmTurmasTmp2, afmoNegarComAviso) then
  begin
    with FmTurmasTmp2 do
    begin
      PreparaPeriodos();
      EdCodigo.Text   := FormatFloat('000000', QrTurmasItsCodigo.Value);
      EdControle.Text := FormatFloat('000000', QrTurmasItsControle.Value);
      LaTipo.Caption  := CO_INCLUSAO;
      ShowModal;
      Destroy;
    end;
    DefParams;
    ReopenTurmasTmp();
  end;
end;

procedure TFmTurmas.QrTurmasBeforeOpen(DataSet: TDataSet);
begin
  QrTurmasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTurmas.Turma2Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmTurmas.Curso2Click(Sender: TObject);
begin
  IncluiRegistro1;
end;

procedure TFmTurmas.Turma1Click(Sender: TObject);
begin
  if QrTurmasCodigo.Value = 0 then
  begin
    Geral.MB_Aviso('Este registro n�o pode ser alterado!');
    Exit;
  end;
  AlteraRegistro;
end;

procedure TFmTurmas.Curso1Click(Sender: TObject);
begin
  AlteraRegistro1;
end;

procedure TFmTurmas.Horrio1Click(Sender: TObject);
begin
  //AlteraRegistro2;
end;

procedure TFmTurmas.Turma3Click(Sender: TObject);
begin
  //AlteraRegistro2;
end;

procedure TFmTurmas.Umaum1Click(Sender: TObject);
begin
  IncluiRegistro2;
end;

procedure TFmTurmas.Curso3Click(Sender: TObject);
begin
  if QrTurmasIts.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� curso cadastrado!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o do curso desta turma?') = ID_YES then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM turmasits ');
    Dmod.QrUpdU.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1');
    Dmod.QrUpdU.Params[0].AsInteger := QrTurmasItsCodigo.Value;
    Dmod.QrUpdU.Params[1].AsInteger := QrTurmasItsControle.Value;
    Dmod.QrUpdU.ExecSQL;
  end;
  AtualizaTotalPeso(QrTurmasCodigo.Value);
  ReabreSub1;
end;

procedure TFmTurmas.Horrio3Click(Sender: TObject);
begin
  if QrTurmasTmp.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� hor�rio cadastrado!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o do hor�rio deste curso?') = ID_YES then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM turmastmp ');
    Dmod.QrUpdU.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1 AND Conta=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := QrTurmasTmpCodigo.Value;
    Dmod.QrUpdU.Params[1].AsInteger := QrTurmasTmpControle.Value;
    Dmod.QrUpdU.Params[2].AsInteger := QrTurmasTmpConta.Value;
    Dmod.QrUpdU.ExecSQL;
  end;
  ReopenTurmasTmp();
end;

procedure TFmTurmas.IncluiRegistro1;
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'TurmasIts', 'TurmasIts', 'Controle');
  //
  if (QrTurmasCodigo.Value = 0) then
  begin
    Geral.MB_Aviso('N�o h� Turma cadastrada!');
    Exit;
  end;
  Refresh;
  if DBCheck.CriaFm(TFmTurmasEdit, FmTurmasEdit, afmoNegarComAviso) then
  begin
    with FmTurmasEdit do
    begin
      EdCodigo.ValueVariant    := FormatFloat('000000', QrTurmasCodigo.Value);
      EdControle.ValueVariant  := Geral.FF0(Controle);
      LaTipo.Caption           := CO_INCLUSAO;
      EdPeso.ValueVariant      := '1,00';
      EdCarga.Text             := '00:00';
      EdRefere.ValueVariant    := QrTurmasID.Value;
      EdProfessor.ValueVariant := QrTurmasRespons.Value;
      ShowModal;
      Destroy;
    end;
    AtualizaTotalPeso(QrTurmasCodigo.Value);
    DefParams;
    ReabreSub1;
  end;  
end;

procedure TFmTurmas.IncluiRegistro2;
begin
  if (QrTurmasItsCodigo.Value=0) or (QrTurmasItsControle.Value=0) then
  begin
    Geral.MB_Aviso('N�o h� curso cadastrado!');
    Exit;
  end;
  Refresh;
  if DBCheck.CriaFm(TFmTurmasTmp, FmTurmasTmp, afmoNegarComAviso) then
  begin
    with FmTurmasTmp do
    begin
      EdCodigo.Text   := FormatFloat('000000', QrTurmasItsCodigo.Value);
      EdControle.Text := FormatFloat('000000', QrTurmasItsControle.Value);
      LaTipo.Caption  := CO_INCLUSAO;
      ShowModal;
      Destroy;
    end;
    DefParams;
    ReopenTurmasTmp();
  end;
end;

procedure TFmTurmas.AlteraRegistro1;
begin
  if (QrTurmasItsCodigo.Value=0) or (QrTurmasItsControle.Value=0) then
  begin
    Geral.MB_Aviso('N�o h� curso cadastrada!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmTurmasEdit, FmTurmasEdit, afmoNegarComAviso) then
  begin
    with FmTurmasEdit do
    begin
      LaTipo.Caption       := CO_ALTERACAO;
      EdCodigo.Text        := FormatFloat('000000', QrTurmasItsCodigo.Value);
      EdControle.Text      := FormatFloat('000', QrTurmasItsControle.Value);
      EdProfessor.Text     := Geral.FF0(QrTurmasItsProfessor.Value);
      CBProfessor.KeyValue := QrTurmasItsProfessor.Value;
      EdSala.Text          := Geral.FF0(QrTurmasItsSala.Value);
      CBSala.KeyValue      := QrTurmasItsSala.Value;
      EdCurso.Text         := Geral.FF0(QrTurmasItsCurso.Value);
      CBCurso.KeyValue     := QrTurmasItsCurso.Value;
      EdPeso.Text          := Geral.FFT(QrTurmasItsPeso.Value, 2, siPositivo);
      EdCarga.Text         := AppPF.Carga_HoraToTime(QrTurmasItsCarga.Value);
      EdHonorarios.Text    := Geral.FFT(QrTurmasItsHonorarios.Value, 2, siPositivo);
      TPIni.Date           := QrTurmasItsDataI.Value;
      EdRefere.Text        := QrTurmasItsRefere.Value;
      ShowModal;
      Destroy;
    end;
    AtualizaTotalPeso(QrTurmasCodigo.Value);
    DefParams;
    ReabreSub1;
  end;
end;

procedure TFmTurmas.ReabreSub1;
begin
  QrTurmasIts.Close;
  QrTurmasIts.Params[0].AsInteger := QrTurmasCodigo.Value;
  UMyMod.AbreQuery(QrTurmasIts, DMod.MyDB);
end;

procedure TFmTurmas.ReopenTurmasTmp();
begin
  QrTurmasTmp.Close;
  QrTurmasTmp.Params[0].AsInteger := QrTurmasItsCodigo.Value;
  QrTurmasTmp.Params[1].AsInteger := QrTurmasItsControle.Value;
  UMyMod.AbreQuery(QrTurmasTmp, DMod.MyDB);
end;

procedure TFmTurmas.QrTurmasItsAfterOpen(DataSet: TDataSet);
begin
  ReopenTurmasTmp();
end;

procedure TFmTurmas.QrTurmasItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTurmasTmp();
end;

procedure TFmTurmas.QrTurmasTmpCalcFields(DataSet: TDataSet);
begin
  QrTurmasTmpDIASEMANA.Value := MLAGeral.NomeDiadaSemana(QrTurmasTmpDSem.Value+1);
end;

procedure TFmTurmas.AtualizaTotalPeso(Turma: Integer);
begin
  QrSPCM.Close;
  QrSPCM.Params[0].AsInteger := Turma;
  UMyMod.AbreQuery(QrSPCM, DMod.MyDB);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE turmas SET TotalPeso=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsFloat   := QrSPCMPeso.Value;
  Dmod.QrUpd.Params[1].AsInteger := Turma;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE turmasits SET TotalPeso=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsFloat   := QrSPCMPeso.Value;
  Dmod.QrUpd.Params[1].AsInteger := Turma;
  Dmod.QrUpd.ExecSQL;
  //
  QrSPCM.Close;
  LocCod(Turma, Turma);
end;

procedure TFmTurmas.QrTurmasItsCalcFields(DataSet: TDataSet);
begin
  QrTurmasItsCARGA_MIN.Value := AppPF.Carga_HoraToTime(QrTurmasItsCarga.Value);
  //
  if QrTurmasTotalPeso.Value <> 0 then
    QrTurmasItsPORCENTAGEM.Value := QrTurmasItsPeso.Value / QrTurmasItsTotalPeso.Value * 100
  else
    QrTurmasItsPORCENTAGEM.Value := 0;
end;

procedure TFmTurmas.EdLotacaoExit(Sender: TObject);
begin
  EdLotacao.Text := Geral.TFT(EdLotacao.Text, 0, siPositivo);
end;

end.

