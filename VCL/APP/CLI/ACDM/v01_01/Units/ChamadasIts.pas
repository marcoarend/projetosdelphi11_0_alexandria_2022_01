unit ChamadasIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,   UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, UnDmkProcFunc, dmkGeral, UnDmkEnums;

type
  TFmChamadasIts = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    CBAluno: TdmkDBLookupComboBox;
    DsAlunos: TDataSource;
    EdAluno: TdmkEditCB;
    EdMinutos: TdmkEdit;
    Label3: TLabel;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrAlunos: TmySQLQuery;
    QrLoc: TmySQLQuery;
    QrLocRegistros: TIntegerField;
    QrAlunosMATRICULA: TIntegerField;
    QrAlunosNOMEALUNO: TWideStringField;
    QrAlunosCodigo: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdMinutosExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTurma, FCodigo, FMaxMin: Integer;
  end;

var
  FmChamadasIts: TFmChamadasIts;

implementation

uses UnMyObjects, Module, Entidades;

{$R *.DFM}

procedure TFmChamadasIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChamadasIts.BtConfirmaClick(Sender: TObject);
var
  Presenca, Matricula: Integer;
begin
  if Geral.IMV(EdAluno.Text) < 1 then
  begin
    Geral.MB_Aviso('Informe um Aluno!');
    EdAluno.SetFocus;
    Exit;
  end else Matricula := QrAlunosMATRICULA.Value;
  Presenca := Geral.IMV(EdMinutos.Text);
  //
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    QrLoc.Close;
    QrLoc.Params[0].AsInteger := FCodigo;
    QrLoc.Params[1].AsInteger := Matricula;
    QrLoc.Open;
    if QrLocRegistros.Value > 0 then
    begin
      Geral.MB_Aviso('Este aluno j� tem presen�a!');
      EdAluno.SetFocus;
      Exit;
    end;
  end;  
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(MLAGeral.AcaoSQL(LaTipo.Caption));
  Dmod.QrUpd.SQL.Add('  ChamadasIts SET ');
  Dmod.QrUpd.SQL.Add('  Presenca    =:P00');
  Dmod.QrUpd.SQL.Add(MLAGeral.LigaSQL(LaTipo.Caption, 1));
  Dmod.QrUpd.SQL.Add('  Codigo      =:P01');
  Dmod.QrUpd.SQL.Add(MLAGeral.LigaSQL(LaTipo.Caption, 2));
  Dmod.QrUpd.SQL.Add('  Matricula   =:P02');
  Dmod.QrUpd.Params[00].AsInteger := Presenca;
  Dmod.QrUpd.Params[01].AsInteger := FCodigo;
  Dmod.QrUpd.Params[02].AsInteger := Matricula;
  Dmod.QrUpd.ExecSQL;
  Close;
end;

procedure TFmChamadasIts.EdMinutosExit(Sender: TObject);
begin
  EdMinutos.Text := MLAGeral.TFT_MinMax(EdMinutos.Text, 0, FMaxMin, 0, siPositivo);
end;

procedure TFmChamadasIts.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmChamadasIts.FormActivate(Sender: TObject);
begin
  QrAlunos.Close;
  QrAlunos.Params[0].AsInteger := FTurma;
  QrAlunos.Open;
  MyObjects.CorIniComponente();
end;

end.
