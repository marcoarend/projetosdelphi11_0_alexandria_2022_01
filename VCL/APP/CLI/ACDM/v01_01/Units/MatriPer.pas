unit MatriPer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkGeral, Variants,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, UnDmkEnums;

type
  TFmMatriPer = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CBPeriodo: TdmkDBLookupComboBox;
    DsPeriodos: TDataSource;
    EdPeriodo: TdmkEditCB;
    QrPeriodos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdControle: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdConta: TdmkEdit;
    EdItem: TdmkEdit;
    QrPeriodosConta: TIntegerField;
    QrPeriodosNome: TWideStringField;
    QrPeriodosDSem: TIntegerField;
    QrPeriodosHIni: TTimeField;
    QrPeriodosHFim: TTimeField;
    QrPeriodosNOMEDSEM: TWideStringField;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    Label9: TLabel;
    DBEdit3: TDBEdit;
    SpeedButton1: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMatriPer: TFmMatriPer;

implementation

uses UnMyObjects, Module, TurmasTmp, MyDBCheck;

{$R *.DFM}

procedure TFmMatriPer.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriPer.BtConfirmaClick(Sender: TObject);
var
  Conta, Periodo: Integer;
begin
  Periodo := StrToInt(EdPeriodo.Text);
  //
  if MyObjects.FIC(CBPeriodo.KeyValue = 0, EdPeriodo, 'Informe um per�odo!') then Exit;
  //
  Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
           'MatriPer', 'MatriPer', 'Codigo');
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO MatriPer SET ');
  Dmod.QrUpdM.SQL.Add('Codigo=:P0, Controle=:P1, Conta=:P2, Item=:P3, ');
  Dmod.QrUpdM.SQL.Add('SubItem=:P4, Periodo=:P5');
  //
  Dmod.QrUpdM.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdM.Params[01].AsInteger := Geral.IMV(EdControle.Text);
  Dmod.QrUpdM.Params[02].AsInteger := Geral.IMV(EdConta.Text);
  Dmod.QrUpdM.Params[03].AsInteger := Geral.IMV(EdItem.Text);
  Dmod.QrUpdM.Params[04].AsInteger := Conta;
  Dmod.QrUpdM.Params[05].AsInteger := Periodo;
  Dmod.QrUpdM.ExecSQL;
  Close;
end;

procedure TFmMatriPer.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmMatriPer.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmTurmasTmp, FmTurmasTmp, afmoNegarComAviso) then
  begin
    FmTurmasTmp.ShowModal;
    FmTurmasTmp.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdPeriodo, CBPeriodo, QrPeriodos, VAR_CADASTRO);
  end;
end;

procedure TFmMatriPer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
