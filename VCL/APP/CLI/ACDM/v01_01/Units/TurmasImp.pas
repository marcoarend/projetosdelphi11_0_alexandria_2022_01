unit TurmasImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, UnMLAGeral, StdCtrls, Buttons, UnInternalConsts,
    
     Db, mySQLDbTables,
  ComCtrls, Menus, DotPrint, frxClass, frxDBSet, dmkGeral;

type
  TFmTurmasImp = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    BtCancela: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    RGOrdem: TRadioGroup;
    BtImprime: TBitBtn;
    QrTurmas: TmySQLQuery;
    QrAlunos: TmySQLQuery;
    QrTurmasNOMERESPONS: TWideStringField;
    QrTurmasANO: TLargeintField;
    QrTurmasNome: TWideStringField;
    QrTurmasID: TWideStringField;
    QrTurmasControle: TIntegerField;
    QrTurmasLotacao: TIntegerField;
    QrAlunosControle: TIntegerField;
    QrAlunosDsem: TIntegerField;
    Progress1: TProgressBar;
    Label1: TLabel;
    QrTurmasAtiva: TSmallintField;
    QrTurmasSituacao: TWideStringField;
    QrTurmasT: TmySQLQuery;
    QrTurmasTControle: TIntegerField;
    QrTurmasTAno: TIntegerField;
    QrTurmasTRefere: TWideStringField;
    QrTurmasTNomeTurma: TWideStringField;
    QrTurmasTNomeInstrutor: TWideStringField;
    QrTurmasTLotacao: TIntegerField;
    QrTurmasTDom: TIntegerField;
    QrTurmasTSeg: TIntegerField;
    QrTurmasTTer: TIntegerField;
    QrTurmasTQua: TIntegerField;
    QrTurmasTQui: TIntegerField;
    QrTurmasTSex: TIntegerField;
    QrTurmasTSab: TIntegerField;
    QrTurmasTSituacao: TWideStringField;
    QrTurmasNOMEGRUPO: TWideStringField;
    QrTurmasTNomeGrupo: TWideStringField;
    QrAlunosQtdAlunos: TFloatField;
    RGSituacao: TRadioGroup;
    PMImprime: TPopupMenu;
    Matricial1: TMenuItem;
    TintaLaser1: TMenuItem;
    QrTurmasTSdo_Dom: TWideStringField;
    QrTurmasTSdo_Seg: TWideStringField;
    QrTurmasTSdo_Ter: TWideStringField;
    QrTurmasTSdo_Qua: TWideStringField;
    QrTurmasTSdo_Qui: TWideStringField;
    QrTurmasTSdo_Sex: TWideStringField;
    QrTurmasTSdo_Sab: TWideStringField;
    frxDsTurmasT: TfrxDBDataset;
    frxTurmasT: TfrxReport;
    procedure FormActivate(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrTurmasCalcFields(DataSet: TDataSet);
    procedure QrTurmasTCalcFields(DataSet: TDataSet);
    procedure TintaLaser1Click(Sender: TObject);
    procedure Matricial1Click(Sender: TObject);
    procedure PreparaRelatorio;
    procedure frxTurmasTGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FTitulo: String;
    FTextos: TMyTextos_136;
    FAlinha: TMyAlinha_136;
    FTamCol: TMyTamCol_136;
    procedure ImprimeMatricial;
    procedure DefineLinhaA;
  public
    { Public declarations }
  end;

var
  FmTurmasImp: TFmTurmasImp;

implementation

{$R *.DFM}

uses UnMyObjects, UCreate, Module;

const
  TA01 = 06;
  TA02 = 20; // 026
  TA03 = 30; // 056
  TA04 = 30; // 086
  TA05 = 05;
  TA06 = 05;
  TA07 = 05;
  TA08 = 05;
  TA09 = 05;
  TA10 = 05;
  TA11 = 05;
  TA12 = 05; // 126
  TA13 = 10; // 136

procedure TFmTurmasImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTurmasImp.BtCancelaClick(Sender: TObject);
begin
  Close;
  Ucriar.RecriaTabelaLocal('TurmasT', 0);
end;

procedure TFmTurmasImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmTurmasImp.FormCreate(Sender: TObject);
begin
  Ucriar.RecriaTabelaLocal('TurmasT', 1);
  Label1.Caption := '';
end;

procedure TFmTurmasImp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmTurmasImp.QrTurmasCalcFields(DataSet: TDataSet);
begin
  case QrTurmasAtiva.Value of
    0: QrTurmasSituacao.Value := 'INATIVA';
    1: QrTurmasSituacao.Value := 'FORMANDO';
    2: QrTurmasSituacao.Value := 'ATIVA';
    else QrTurmasSituacao.Value := '?????';
  end;
end;

procedure TFmTurmasImp.QrTurmasTCalcFields(DataSet: TDataSet);
begin
  if QrTurmasTDom.Value = -1000000 then QrTurmasTSdo_Dom.Value := '.' else
  QrTurmasTSdo_Dom.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTDom.Value);
  if QrTurmasTSeg.Value = -1000000 then QrTurmasTSdo_Seg.Value := '.' else
  QrTurmasTSdo_Seg.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTSeg.Value);
  if QrTurmasTTer.Value = -1000000 then QrTurmasTSdo_Ter.Value := '.' else
  QrTurmasTSdo_Ter.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTTer.Value);
  if QrTurmasTQua.Value = -1000000 then QrTurmasTSdo_Qua.Value := '.' else
  QrTurmasTSdo_Qua.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTQua.Value);
  if QrTurmasTQui.Value = -1000000 then QrTurmasTSdo_Qui.Value := '.' else
  QrTurmasTSdo_Qui.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTQui.Value);
  if QrTurmasTSex.Value = -1000000 then QrTurmasTSdo_Sex.Value := '.' else
  QrTurmasTSdo_Sex.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTSex.Value);
  if QrTurmasTSab.Value = -1000000 then QrTurmasTSdo_Sab.Value := '.' else
  QrTurmasTSdo_Sab.Value := IntToStr(QrTurmasTLotacao.Value - QrTurmasTSab.Value);
end;

procedure TFmTurmasImp.PreparaRelatorio;
var
  DD: String;
const
  Titulo = 'LISTA DE TURMAS ';
begin
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM turmast');
  Dmod.QrUpdL.ExecSQL;
  //
  Label1.Caption := 'Abrindo tabela de turmas';
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO turmast SET ');
  Dmod.QrUpdL.SQL.Add('Ano           =:P00,');
  Dmod.QrUpdL.SQL.Add('Refere        =:P01,');
  Dmod.QrUpdL.SQL.Add('NomeTurma     =:P02,');
  Dmod.QrUpdL.SQL.Add('NomeInstrutor =:P03,');
  Dmod.QrUpdL.SQL.Add('Lotacao       =:P04,');
  Dmod.QrUpdL.SQL.Add('Controle      =:P05,');
  Dmod.QrUpdL.SQL.Add('Situacao      =:P06,');
  Dmod.QrUpdL.SQL.Add('NomeGrupo     =:P07,');
  Dmod.QrUpdL.SQL.Add('Dom           =-1000000,');
  Dmod.QrUpdL.SQL.Add('Seg           =-1000000,');
  Dmod.QrUpdL.SQL.Add('Ter           =-1000000,');
  Dmod.QrUpdL.SQL.Add('Qua           =-1000000,');
  Dmod.QrUpdL.SQL.Add('Qui           =-1000000,');
  Dmod.QrUpdL.SQL.Add('Sex           =-1000000,');
  Dmod.QrUpdL.SQL.Add('Sab           =-1000000');
  //
  QrTurmas.Close;
  QrTurmas.Open;
  Progress1.Position := 0;
  Progress1.Max := QrTurmas.RecordCount;
  Label1.Caption := 'Preparando turmas';
  while not QrTurmas.Eof do
  begin
    Progress1.Position := Progress1.Position +1;
    Dmod.QrUpdL.Params[00].AsInteger := QrTurmasANO.Value;
    Dmod.QrUpdL.Params[01].AsString  := QrTurmasID.Value;
    Dmod.QrUpdL.Params[02].AsString  := QrTurmasNome.Value;
    Dmod.QrUpdL.Params[03].AsString  := QrTurmasNOMERESPONS.Value;
    Dmod.QrUpdL.Params[04].AsInteger := QrTurmasLotacao.Value;
    Dmod.QrUpdL.Params[05].AsInteger := QrTurmasControle.Value;
    Dmod.QrUpdL.Params[06].AsString  := QrTurmasSituacao.Value;
    Dmod.QrUpdL.Params[07].AsString  := QrTurmasNOMEGRUPO.Value;
    Dmod.QrUpdL.ExecSQL;
    QrTurmas.Next;
  end;
  Label1.Caption := 'Abrindo tabela de matr�culas';
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE turmast SET ');
  Dmod.QrUpdL.SQL.Add(' ');//Adi��o posterior
  Dmod.QrUpdL.SQL.Add('WHERE Controle=:P0');
  QrAlunos.Close;
  QrAlunos.Open;
  Progress1.Position := 0;
  Progress1.Max := QrAlunos.RecordCount;
  Label1.Caption := 'Definindo matr�culas';
  while not QrAlunos.Eof do
  begin
    Progress1.Position := Progress1.Position +1;
    case QrAlunosDsem.Value of
      0: DD := 'Dom';
      1: DD := 'Seg';
      2: DD := 'Ter';
      3: DD := 'Qua';
      4: DD := 'Qui';
      5: DD := 'Sex';
      6: DD := 'Sab';
    end;
    Dmod.QrUpdL.SQL[1] := (DD+'='+FloatToStr(QrAlunosQtdAlunos.Value));
    Dmod.QrUpdL.Params[00].AsInteger := QrAlunosControle.Value;
    Dmod.QrUpdL.ExecSQL;
    QrAlunos.Next;
  end;
  QrTurmasT.Close;
  case RGSituacao.ItemIndex of
    0: QrTurmasT.SQL[1] := 'WHERE Situacao="Ativa"';
    1: QrTurmasT.SQL[1] := 'WHERE Situacao="Inativa"';
    2: QrTurmasT.SQL[1] := ' ';
    else QrTurmasT.SQL[1] := 'ERRO SITUA��O';
  end;
  case RGOrdem.ItemIndex of
    0: QrTurmasT.SQL[2] := 'ORDER BY NomeGrupo, Refere';
    1: QrTurmasT.SQL[2] := 'ORDER BY NomeTurma, Refere';
    2: QrTurmasT.SQL[2] := 'ORDER BY NomeInstrutor, NomeGrupo, NomeTurma';
    else QrTurmasT.SQL[2] := 'ERRO ORDEM';
  end;
  case RGSituacao.ItemIndex of
    0: FTitulo := Titulo + 'ATIVAS ';
    1: FTitulo := Titulo + 'INATIVAS ';
    2: FTitulo := Titulo + ' ';
    else FTitulo := Titulo + '*** ERRO SITUA��O *** ';
  end;
  case RGOrdem.ItemIndex of
    0: FTitulo := FTitulo + 'POR GRUPOS ';
    1: FTitulo := FTitulo + 'POR TURMAS ';
    2: FTitulo := FTitulo + 'PRO INSTRUTORES ';
    else FTitulo := FTitulo + '*** ERRO ORDEM ***';
  end;
  QrTurmasT.Open;
  Label1.Caption := '';
  Progress1.Position := 0;
end;

procedure TFmTurmasImp.TintaLaser1Click(Sender: TObject);
begin
  PreparaRelatorio;
  MyObjects.frxMostra(frxTurmasT, 'Turmas');
end;

procedure TFmTurmasImp.Matricial1Click(Sender: TObject);
begin
  PreparaRelatorio;
  ImprimeMatricial;
end;

procedure TFmTurmasImp.ImprimeMatricial;
const
  ITENS = 37;
  //
var
  i: Integer;
  Divisor, Data: String;
  SubTotal(*, Total*): Integer;
//var
  TextoCG: String;
  CampoCG: TWideStringField;
begin
  SubTotal := 0;
  PreparaRelatorio;
  ////////////////////////////////////////////////////////////////////////////////
  VAR_LOCCOLUNASIMP := 136;
  ////////////////////////////////////////////////////////////////////////////////
  Data := FormatDateTime('hh:nn:ss "  "dd/mm/yyyy', Now());
  //ColsImpCompress := Trunc(VAR_LOCCOLUNASIMP * 17 / 10);
  Divisor := '';
  for i := 1 to VAR_LOCCOLUNASIMP do Divisor := Divisor + '=';
  //
  Application.CreateForm(TFmDotPrint, FmDotPrint);
  with FmDotPrint do
  begin
    with RE1 do
    begin
      Lines.Clear;
      AdicionaLinhaTexto(Geral.CompletaString(Data, ' ', 136, taRightJustify,
        True), [fdpCompress], []);
      AdicionaLinhaTexto(FTitulo, [fdpNormalSize], []);
      AdicionaLinhaTexto(Divisor, [fdpCompress], []);
      //
      //Total := 0;
      //////////////////////////////////////////////////////////////////////////
      CampoCG := nil;
      TextoCG := '';
      QrTurmasT.First;
      while not QrTurmasT.Eof do
      begin
        case RGOrdem.ItemIndex of
          0: CampoCG := QrTurmasTNomeGrupo;
          1: CampoCG := QrTurmasTNomeTurma;
          2: CampoCG := QrTurmasTNomeInstrutor;
        end;
        if TextoCG <> CampoCG.Value then
        begin
          if QrTurmasT.RecNo > 1 then
          begin
            AdicionaLinhaTexto(Divisor, [fdpCompress], []);
            AdicionaLinhaTexto('SUB-TOTAL: '+IntToStr(SubTotal), [fdpNormalSize], []);
            SubTotal := 0;
            AdicionaLinhaTexto('', [fdpNormalSize], []);
            AdicionaLinhaTexto('', [fdpNormalSize], []);
          end;
          TextoCG := CampoCG.Value;
          AdicionaLinhaTexto(TextoCG, [fdpDoubleSize], []);
          AdicionaLinhaTexto('', [fdpNormalSize], []);
          DefineLinhaA;
          AdicionaLinhaColunas(13, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        end;
        FTextos[01] := IntToStr(QrTurmasTAno.Value);
        FTextos[02] := QrTurmasTRefere.Value;
        FTextos[03] := QrTurmasTNomeTurma.Value;
        FTextos[04] := QrTurmasTNomeInstrutor.Value;
        FTextos[05] := IntToStr(QrTurmasTLotacao.Value);
        FTextos[06] := QrTurmasTSdo_Dom.Value;
        FTextos[07] := QrTurmasTSdo_Seg.Value;
        FTextos[08] := QrTurmasTSdo_Ter.Value;
        FTextos[09] := QrTurmasTSdo_Qua.Value;
        FTextos[10] := QrTurmasTSdo_Qui.Value;
        FTextos[11] := QrTurmasTSdo_Sex.Value;
        FTextos[12] := QrTurmasTSdo_Sab.Value;
        FTextos[13] := QrTurmasTSituacao.Value;
        AdicionaLinhaColunas(13, FTextos, FAlinha, FTamCol, [fdpCompress], '');
        SubTotal := SubTotal + 1;
        QrTurmasT.Next;
      end;
      //////////////////////////////////////////////////////////////////////////
    end;
    AdicionaLinhaTexto(Divisor, [fdpCompress], []);
    AdicionaLinhaTexto('SUB-TOTAL: '+IntToStr(SubTotal), [fdpNormalSize], []);
    AdicionaLinhaTexto('', [fdpCompress], []);
    AdicionaLinhaTexto(Divisor, [fdpCompress], []);
    AdicionaLinhaTexto('TOTAL GERAL: '+IntToStr(QrTurmasT.RecordCount), [fdpNormalSize], []);
    ShowModal;
    Destroy;
  end;
end;

procedure TFmTurmasImp.DefineLinhaA;
begin
  FTextos[01] := 'ANO';
  FTextos[02] := 'REFER�NCIA';
  FTextos[03] := 'DESCRI��O';
  FTextos[04] := 'INSTRUTOR';
  FTextos[05] := 'M�X';
  FTextos[06] := 'DOM';
  FTextos[07] := 'SEG';
  FTextos[08] := 'TER';
  FTextos[09] := 'QUA';
  FTextos[10] := 'QUI';
  FTextos[11] := 'SEX';
  FTextos[12] := 'SAB';
  FTextos[13] := 'SITUA��O';
  FAlinha[01] := taLeftJustify;
  FAlinha[02] := taLeftJustify;
  FAlinha[03] := taLeftJustify;
  FAlinha[04] := taLeftJustify;
  FAlinha[05] := taRightJustify;
  FAlinha[06] := taRightJustify;
  FAlinha[07] := taRightJustify;
  FAlinha[08] := taRightJustify;
  FAlinha[09] := taRightJustify;
  FAlinha[10] := taRightJustify;
  FAlinha[11] := taRightJustify;
  FAlinha[12] := taRightJustify;
  FAlinha[13] := taCenter;
  FTamCol[01] := TA01;
  FTamCol[02] := TA02;
  FTamCol[03] := TA03;
  FTamCol[04] := TA04;
  FTamCol[05] := TA05;
  FTamCol[06] := TA06;
  FTamCol[07] := TA07;
  FTamCol[08] := TA08;
  FTamCol[09] := TA09;
  FTamCol[10] := TA10;
  FTamCol[11] := TA11;
  FTamCol[12] := TA12;
  FTamCol[13] := TA13;
  //
end;

procedure TFmTurmasImp.frxTurmasTGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'TITULO' then Value := FTitulo else
  if VarName = 'VAR_ORDEM' then Value := RGOrdem.ItemIndex else
  if VarName = 'VAR_GRUPO' then Value := RGOrdem.ItemIndex
  else
  ;
end;

end.

