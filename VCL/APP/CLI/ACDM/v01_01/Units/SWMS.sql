CREATE TABLE swms_mtng_cab(
  Codigo int(11) NOT NULL,
  CNPJ_EMP varchar(32) NULL,
  Descricao varchar(255) NULL,
  DtAbertura date NULL,
  DtEncerra date NULL,
  DataIni date NULL,
  HoraIni time NULL,
  DataFim date NULL,
  HoraFim time NULL, 
  SenhaC varchar(6) NULL, 
  Lk int(11) NULL,
  DataCad date NULL,
  DataAlt date NULL,
  UserCad int(11) NULL,
  UserAlt int(11) NULL,
  AlterWeb tinyint(1) NULL,
  Ativo tinyint(1) NULL,
  PRIMARY KEY  (Codigo),
  KEY SenhaC (SenhaC)
)ENGINE=MyISAM;

CREATE TABLE swms_mtng_its(
  Codigo int(11) NOT NULL,
  CNPJ_PAR varchar(32) NOT NULL,
  SenhaP varchar(6) NULL,
  XML mediumtext NULL,
  DataHora datetime NULL,
  Lk int(11) NULL,
  DataCad date NULL,
  DataAlt date NULL,
  UserCad int(11) NULL,
  UserAlt int(11) NULL,
  AlterWeb tinyint(1) NULL,
  Ativo tinyint(1) NULL,
  PRIMARY KEY (Codigo, CNPJ_PAR)
)ENGINE=MyISAM;