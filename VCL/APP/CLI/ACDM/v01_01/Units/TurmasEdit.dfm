�
 TFMTURMASEDIT 0�+  TPF0TFmTurmasEditFmTurmasEditLeftXTop� Caption"   TUR-CADAS-002 :: Edição de TurmaClientHeight{ClientWidth|Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnCreate
FormCreateOnResize
FormResizePixelsPerInchx
TextHeight TPanelPainelDadosLeft Top Width|Height@Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClient
BevelOuterbvNoneTabOrder  TPanelPanel3Left Top;Width|HeightMargins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientParentColor	TabOrder  TLabelLabel1LeftTopWidth*HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionTurma:FocusControlEdCodigo  TLabelLabel2LeftTopqWidth� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionProfessor ou instrutor:FocusControlEdProfessor  TLabelLabel6LeftTop� Width$HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionLocal:FocusControlEdSala  TLabelLabel3LeftTop;WidthsHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionCurso ou atividade:FocusControlEdCurso  TLabelLabel4LeftbTopWidth5HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption	Controle:FocusControl
EdControle  TLabelLabel5Left� TopWidth3HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption	Peso ** :FocusControlEdPeso  TLabelLabel8LeftTop;WidthbHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Carga horária **:FocusControlEdCarga  TLabelLabel9LeftTopqWidthFHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Honorários:FocusControlEdHonorarios  TLabelLabel10Left
TopWidth~HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Início das atividades:  TLabelLabel11Left�TopWidthEHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption   Referência:FocusControlEdRefere  TSpeedButtonSpeedButton1Left�TopOWidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption...OnClickSpeedButton1Click  TSpeedButtonSpeedButton2Left�Top� WidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption...OnClickSpeedButton2Click  TSpeedButtonSpeedButton3LeftTTop� WidthHeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption...OnClickSpeedButton3Click  TLabelLabel7LeftTop� Width�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaptionC   ** Peso => Peso no cálculo de rentabilidade do curso ou atividade.  TLabelLabel12LeftTop� Width� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomCaption)   **  Carga horária => Informar em minutos  TdmkEditEdCodigoLeftTopWidthOHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	AlignmenttaRightJustifyColor	clBtnFaceEnabledFont.CharsetDEFAULT_CHARSET
Font.Color4_~ Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder 
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  
TdmkEditCBEdProfessorLeftTop� WidthOHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBProfessorIgnoraDBLookupComboBox  TdmkDBLookupComboBoxCBProfessorLeftbTop� Width�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldCodigo	ListFieldNome
ListSourceDsProfessoresTabOrder		dmkEditCBEdProfessorUpdTypeutYesLocF7SQLMasc$#  
TdmkEditCBEdSalaLeftTop� WidthOHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBSalaIgnoraDBLookupComboBox  TdmkDBLookupComboBoxCBSalaLeftbTop� Width�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldCodigo	ListFieldNome
ListSourceDsSalasTabOrder	dmkEditCBEdSalaUpdTypeutYesLocF7SQLMasc$#  
TdmkEditCBEdCursoLeftTopOWidthOHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnOnChangeEdCursoChangeDBLookupComboBoxCBCursoIgnoraDBLookupComboBox  TdmkDBLookupComboBoxCBCursoLeftbTopOWidth�HeightMargins.LeftMargins.TopMargins.RightMargins.BottomKeyFieldCodigo	ListFieldNome
ListSourceDsCursosTabOrder	dmkEditCBEdCursoUpdTypeutYesLocF7SQLMasc$#  TdmkEdit
EdControleLeftbTopWidthOHeightMargins.LeftMargins.TopMargins.RightMargins.BottomTabStop	AlignmenttaRightJustifyColor	clBtnFaceEnabledFont.CharsetDEFAULT_CHARSET
Font.Color4_~ Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  TdmkEditEdPesoLeft� TopWidthOHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfDoubleMskTypefmtNoneDecimalSize	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0,00UpdTypeutYesObrigatorioPermiteNuloValueVariant          ValWarnOnExit
EdPesoExit  TdmkEditEdCargaLeftTopOWidthbHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaCenterFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatType	dmktfTimeMskTypefmtNoneDecimalSize	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto00:00UpdTypeutYesObrigatorioPermiteNuloValueVariant          ValWarn  TdmkEditEdHonorariosLeftTop� WidthbHeightMargins.LeftMargins.TopMargins.RightMargins.Bottom	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder

FormatTypedmktfDoubleMskTypefmtNoneDecimalSize	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0,00UpdTypeutYesObrigatorioPermiteNuloValueVariant          ValWarnOnExitEdHonorariosExit  TDateTimePickerTPIniLeft
TopWidth� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomDate H�~~kU�@Time H�~~kU�@TabOrder  TdmkEditEdRefereLeft�TopWidth� HeightMargins.LeftMargins.TopMargins.RightMargins.BottomFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
FormatTypedmktfStringMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortUpdTypeutYesObrigatorioPermiteNuloValueVariant    ValWarn   TPanelPainelTituloLeft Top Width|Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalTopCaption   Edição de TurmaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style ParentColor	
ParentFontTabOrder TLabelLaTipoLeftTopWidtheHeight9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalRight	AlignmenttaCenterAutoSizeCaptionTravadoFont.CharsetANSI_CHARSET
Font.Color4_~ Font.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	  TImageImage1LeftTopWidthHeight9Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalClientTransparent	    TPanelPainelConfirmaLeft Top@Width|Height;Margins.LeftMargins.TopMargins.RightMargins.BottomAlignalBottomTabOrder TBitBtn
BtConfirmaTagLeftTopWidthnHeight1Margins.LeftMargins.TopMargins.RightMargins.BottomCaption	&Confirma	NumGlyphsTabOrder OnClickBtConfirmaClick  TPanelPanel1Left�TopWidth� Height9AlignalRight
BevelOuterbvNoneTabOrder TBitBtn	BtDesisteTagLeft=TopWidthoHeight1Margins.LeftMargins.TopMargins.RightMargins.BottomCaption&Desiste	NumGlyphsTabOrder OnClickBtDesisteClick    TmySQLQueryQrCursosDatabase	Dmod.MyDBSQL.StringsSELECT Codigo, Nome, CargaFROM cursosWHERE Codigo>0ORDER BY Nome Left� Topl TIntegerFieldQrCursosCodigo	FieldNameCodigo  TStringFieldQrCursosNome	FieldNameNomeSize2  TFloatFieldQrCursosCarga	FieldNameCarga   TDataSourceDsCursosDataSetQrCursosLeft Topl  TmySQLQueryQrSalasDatabase	Dmod.MyDBSQL.StringsSELECT Codigo, Nome
FROM salasORDER BY Nome Left� Top�  TIntegerFieldQrSalasCodigo	FieldNameCodigo  TStringFieldQrSalasNome	FieldNameNomeSize2   TDataSourceDsSalasDataSetQrSalasLeft Top�   TmySQLQueryQrProfessoresDatabase	Dmod.MyDBSQL.StringsSELECT Codigo, NomeFROM entidadesWHERE Cliente2="V"ORDER BY Nome Left� Top�  TIntegerFieldQrProfessoresCodigo	FieldNameCodigo  TStringFieldQrProfessoresNome	FieldNameNomeSized   TDataSourceDsProfessoresDataSetQrProfessoresLeft Top�    