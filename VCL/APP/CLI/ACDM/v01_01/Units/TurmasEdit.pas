unit TurmasEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs, Grids,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UnMLAGeral, ComCtrls,
  UMySQLModule, DBGrids, UnMsgInt, mySQLDbTables, dmkGeral, Variants, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, UnDmkEnums;

type
  TFmTurmasEdit = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    BtConfirma: TBitBtn;
    Label2: TLabel;
    EdProfessor: TdmkEditCB;
    CBProfessor: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdSala: TdmkEditCB;
    CBSala: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdCurso: TdmkEditCB;
    CBCurso: TdmkDBLookupComboBox;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrCursos: TmySQLQuery;
    DsCursos: TDataSource;
    QrCursosCodigo: TIntegerField;
    QrCursosNome: TWideStringField;
    QrSalas: TmySQLQuery;
    DsSalas: TDataSource;
    QrProfessores: TmySQLQuery;
    DsProfessores: TDataSource;
    QrProfessoresCodigo: TIntegerField;
    QrProfessoresNome: TWideStringField;
    QrSalasCodigo: TIntegerField;
    QrSalasNome: TWideStringField;
    Label4: TLabel;
    EdControle: TdmkEdit;
    Label5: TLabel;
    EdPeso: TdmkEdit;
    Label8: TLabel;
    EdCarga: TdmkEdit;
    QrCursosCarga: TFloatField;
    Label9: TLabel;
    EdHonorarios: TdmkEdit;
    Label10: TLabel;
    TPIni: TDateTimePicker;
    Label11: TLabel;
    EdRefere: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label7: TLabel;
    Label12: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCursoChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesoExit(Sender: TObject);
    procedure EdHonorariosExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    { Private declarations }
    FChange: Boolean;
  public
    { Public declarations }
  end;

var
  FmTurmasEdit: TFmTurmasEdit;
  Fechar : Boolean;

implementation

uses UnMyObjects, Module, MyDBCheck, Principal, ModuleGeral, Salas, UnAppPF;


{$R *.DFM}

procedure TFmTurmasEdit.BtConfirmaClick(Sender: TObject);
var
  Turma, Controle, Curso, Professor, Sala: Integer;
  Carga: Double;
  Refere: String;
begin
  if EdCarga.Text = CO_VAZIO then
    EdCarga.Text := '00:00';
  //
  Turma     := Geral.IMV(EdCodigo.Text);
  Controle  := Geral.IMV(EdControle.Text);
  Refere    := EdRefere.ValueVariant;
  Curso     := EdCurso.ValueVariant;
  Professor := EdProfessor.ValueVariant;
  Sala      := EdSala.ValueVariant;
  Carga     := AppPF.Carga_TimeToHora(StrToTime(EdCarga.Text));
  //
  if MyObjects.FIC(Turma < 1, nil, 'Turma n�o informada!') then Exit;
  if MyObjects.FIC(Refere = '', EdRefere, 'Informe uma refer�ncia!') then Exit;
  if MyObjects.FIC(Curso = 0, EdCurso, 'Informe o Curso!') then Exit;
  if MyObjects.FIC(Professor = 0, EdProfessor, 'Informe o professor!') then Exit;
  if MyObjects.FIC(CBProfessor.Text = CO_VAZIO, EdProfessor,
    'Verifique no cadastro de professores se o codigo selecionado pertence a um cadastro habilitado como professor na guia "Miscel�nea"!') then Exit;
  if MyObjects.FIC(Sala = 0, EdSala, 'Informe o Sala!') then Exit;
  //
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO TurmasIts SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE      TurmasIts SET ');
  Dmod.QrUpdU.SQL.Add('  Curso      =:P0');
  Dmod.QrUpdU.SQL.Add(', Sala       =:P1');
  Dmod.QrUpdU.SQL.Add(', Professor  =:P2');
  Dmod.QrUpdU.SQL.Add(', Peso       =:P3');
  Dmod.QrUpdU.SQL.Add(', Carga      =:P4');
  Dmod.QrUpdU.SQL.Add(', Honorarios =:P5');
  Dmod.QrUpdU.SQL.Add(', DataI      =:P6');
  Dmod.QrUpdU.SQL.Add(', Refere     =:P7');
  if LaTipo.Caption = CO_INCLUSAO then
  begin
     Dmod.QrUpdU.SQL.Add(', Codigo=:Px');
     Dmod.QrUpdU.SQL.Add(', Controle=:Py');
  end else begin
    Dmod.QrUpdU.SQL.Add('WHERE Codigo=:Px ');
    Dmod.QrUpdU.SQL.Add('AND   Controle=:Py ');
  end;
  Dmod.QrUpdU.Params[0].AsInteger := CBCurso.KeyValue;
  Dmod.QrUpdU.Params[1].AsInteger := CBSala.KeyValue;
  Dmod.QrUpdU.Params[2].AsInteger := CBProfessor.KeyValue;
  Dmod.QrUpdU.Params[3].AsFloat   := Geral.DMV(EdPeso.Text);
  Dmod.QrUpdU.Params[4].AsFloat   := Carga;
  Dmod.QrUpdU.Params[5].AsFloat   := Geral.DMV(EdHonorarios.Text);
  Dmod.QrUpdU.Params[6].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  Dmod.QrUpdU.Params[7].AsString  := EdRefere.Text;
  //
  Dmod.QrUpdU.Params[8].AsInteger := Turma;
  Dmod.QrUpdU.Params[9].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  // Usado em FmMatriAti
  VAR_CADASTRO2 := CBCurso.KeyValue;
  Close;
end;

procedure TFmTurmasEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTurmasEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdPeso.SetFocus;
  TPIni.SetFocus;
  FChange := True;
end;

procedure TFmTurmasEdit.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrProfessores, DMod.MyDB);
  UMyMod.AbreQuery(QrSalas, DMod.MyDB);
  UMyMod.AbreQuery(QrCursos, DMod.MyDB);
  //
  TPIni.Date := Date;
end;

procedure TFmTurmasEdit.EdCursoChange(Sender: TObject);
begin
  if FChange then
    EdCarga.Text := AppPF.Carga_HoraToTime(QrCursosCarga.Value);
end;

procedure TFmTurmasEdit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTurmasEdit.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroCursos(EdCurso.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCurso, CBCurso, QrCursos, VAR_CADASTRO);
    EdCurso.SetFocus;
  end;
end;

procedure TFmTurmasEdit.SpeedButton2Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdProfessor.ValueVariant;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, False, False,
    nil, nil, False, uetCliente2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdProfessor, CBProfessor, QrProfessores, VAR_CADASTRO);
    EdProfessor.SetFocus;
  end;
end;

procedure TFmTurmasEdit.SpeedButton3Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroSalas(EdSala.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSala, CBSala, QrSalas, VAR_CADASTRO);
    EdSala.SetFocus;
  end;
end;

procedure TFmTurmasEdit.EdPesoExit(Sender: TObject);
begin
  EdPeso.Text := MLAGeral.TFT_MinMax(EdPeso.Text, 0.01, 1000000, 2, siPositivo);
end;

procedure TFmTurmasEdit.EdHonorariosExit(Sender: TObject);
begin
  EdHonorarios.Text := Geral.TFT(EdHonorarios.Text, 2, siPositivo);
end;

end.
