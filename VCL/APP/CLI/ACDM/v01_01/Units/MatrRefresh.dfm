object FmMatriRefresh: TFmMatriRefresh
  Left = 522
  Top = 364
  Width = 299
  Height = 96
  Caption = 'Aguarde...'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Progress: TProgressBar
    Left = 0
    Top = 21
    Width = 291
    Height = 17
    Min = 0
    Max = 100
    TabOrder = 0
  end
  object QrMatricul: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Ativo '
      'FROM matricul'
      'ORDER BY Codigo')
    Left = 16
    Top = 16
    object QrMatriculCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMatriculAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 44
    Top = 16
  end
  object QrMatriRen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, DataF '
      'FROM matriren '
      'WHERE Codigo=:P0'
      'ORDER BY DataF DESC')
    Left = 72
    Top = 17
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMatriRenCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMatriRenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMatriRenDataF: TDateField
      FieldName = 'DataF'
      Required = True
    end
  end
end

