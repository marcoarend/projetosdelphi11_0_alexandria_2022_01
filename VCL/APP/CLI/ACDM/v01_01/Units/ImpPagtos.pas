unit ImpPagtos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs, Grids,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UnMLAGeral, ComCtrls,
  DBGrids, UnMsgInt, mySQLDbTables, frxClass, frxDBSet, dmkGeral, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmImpPagtos = class(TForm)
    DsAlunos: TDataSource;
    DsEmpresas: TDataSource;
    QrEmpresas: TmySQLQuery;
    QrAlunos: TmySQLQuery;
    QrAlunosNome: TWideStringField;
    QrAlunosCodigo: TIntegerField;
    QrPagtosAnt: TmySQLQuery;
    QrPagtosAntData: TDateField;
    QrPagtosAntTipo: TSmallintField;
    QrPagtosAntCarteira: TIntegerField;
    QrPagtosAntControle: TIntegerField;
    QrPagtosAntSub: TSmallintField;
    QrPagtosAntAutorizacao: TIntegerField;
    QrPagtosAntGenero: TIntegerField;
    QrPagtosAntDescricao: TWideStringField;
    QrPagtosAntNotaFiscal: TIntegerField;
    QrPagtosAntDebito: TFloatField;
    QrPagtosAntCredito: TFloatField;
    QrPagtosAntCompensado: TDateField;
    QrPagtosAntDocumento: TFloatField;
    QrPagtosAntSit: TIntegerField;
    QrPagtosAntVencimento: TDateField;
    QrPagtosAntFatID: TIntegerField;
    QrPagtosAntFatID_Sub: TIntegerField;
    QrPagtosAntFatParcela: TIntegerField;
    QrPagtosAntID_Pgto: TIntegerField;
    QrPagtosAntID_Sub: TSmallintField;
    QrPagtosAntFatura: TWideStringField;
    QrPagtosAntBanco: TIntegerField;
    QrPagtosAntLocal: TIntegerField;
    QrPagtosAntCartao: TIntegerField;
    QrPagtosAntLinha: TIntegerField;
    QrPagtosAntOperCount: TIntegerField;
    QrPagtosAntLancto: TIntegerField;
    QrPagtosAntPago: TFloatField;
    QrPagtosAntMez: TIntegerField;
    QrPagtosAntFornecedor: TIntegerField;
    QrPagtosAntCliente: TIntegerField;
    QrPagtosAntMoraDia: TFloatField;
    QrPagtosAntMulta: TFloatField;
    QrPagtosAntProtesto: TDateField;
    QrPagtosAntDataDoc: TDateField;
    QrPagtosAntCtrlIni: TIntegerField;
    QrPagtosAntNivel: TIntegerField;
    QrPagtosAntVendedor: TIntegerField;
    QrPagtosAntAccount: TIntegerField;
    QrPagtosAntLk: TIntegerField;
    QrPagtosAntDataCad: TDateField;
    QrPagtosAntDataAlt: TDateField;
    QrPagtosAntUserCad: TIntegerField;
    QrPagtosAntUserAlt: TIntegerField;
    QrPagtosAntNOMEALUNO: TWideStringField;
    QrPagtosAntNOMEEMPRESA: TWideStringField;
    QrPagtosAntMATRICULA: TIntegerField;
    QrPagtosAntREALSIT: TLargeintField;
    QrPagtosAntNOMEEMPRESA2: TWideStringField;
    QrPagtosAntNOMEALUNO2: TWideStringField;
    QrPagtosAntNOMESIT: TWideStringField;
    QrPagtosAntSALDO: TFloatField;
    QrPagtosAntVENCIDO: TDateField;
    QrPagtosAntMULTAREAL: TFloatField;
    QrPagtosAntATRAZODD: TFloatField;
    QrPagtosAntNOMEVENCIDO: TWideStringField;
    QrPagtosAntATUALIZADO: TFloatField;
    QrPagtos: TmySQLQuery;
    QrPagtosNOMEEMPRESA2: TWideStringField;
    QrPagtosNOMEALUNO2: TWideStringField;
    QrPagtosNOMESIT: TWideStringField;
    QrPagtosSALDO: TFloatField;
    QrPagtosVENCIDO: TDateField;
    QrPagtosMULTAREAL: TFloatField;
    QrPagtosATRAZODD: TFloatField;
    QrPagtosNOMEVENCIDO: TWideStringField;
    QrPagtosATUALIZADO: TFloatField;
    QrPagtosNOMETURMA: TWideStringField;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosControle: TIntegerField;
    QrPagtosSub: TSmallintField;
    QrPagtosAutorizacao: TIntegerField;
    QrPagtosGenero: TIntegerField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosNotaFiscal: TIntegerField;
    QrPagtosDebito: TFloatField;
    QrPagtosCredito: TFloatField;
    QrPagtosCompensado: TDateField;
    QrPagtosDocumento: TFloatField;
    QrPagtosSit: TIntegerField;
    QrPagtosVencimento: TDateField;
    QrPagtosFatID: TIntegerField;
    QrPagtosFatID_Sub: TIntegerField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosID_Pgto: TIntegerField;
    QrPagtosID_Sub: TSmallintField;
    QrPagtosFatura: TWideStringField;
    QrPagtosBanco: TIntegerField;
    QrPagtosLocal: TIntegerField;
    QrPagtosCartao: TIntegerField;
    QrPagtosLinha: TIntegerField;
    QrPagtosOperCount: TIntegerField;
    QrPagtosLancto: TIntegerField;
    QrPagtosPago: TFloatField;
    QrPagtosMez: TIntegerField;
    QrPagtosFornecedor: TIntegerField;
    QrPagtosCliente: TIntegerField;
    QrPagtosMoraDia: TFloatField;
    QrPagtosMulta: TFloatField;
    QrPagtosProtesto: TDateField;
    QrPagtosDataDoc: TDateField;
    QrPagtosCtrlIni: TIntegerField;
    QrPagtosNivel: TIntegerField;
    QrPagtosVendedor: TIntegerField;
    QrPagtosAccount: TIntegerField;
    QrPagtosLk: TIntegerField;
    QrPagtosDataCad: TDateField;
    QrPagtosDataAlt: TDateField;
    QrPagtosUserCad: TIntegerField;
    QrPagtosUserAlt: TIntegerField;
    QrPagtosNOMEALUNO: TWideStringField;
    QrPagtosNOMEEMPRESA: TWideStringField;
    QrPagtosMATRICULA: TIntegerField;
    QrPagtosREALSIT: TLargeintField;
    QrPagtosNOMERESPONS: TWideStringField;
    frxPagtosMatr: TfrxReport;
    frxDsPagtos: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel3: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    GroupBox1: TGroupBox;
    CkVencido: TCheckBox;
    CkAberto: TCheckBox;
    CkQuitado: TCheckBox;
    RG1: TRadioGroup;
    CkGrade: TCheckBox;
    RG2: TRadioGroup;
    RG3: TRadioGroup;
    EdAluno: TdmkEditCB;
    CBAluno: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Ck1: TCheckBox;
    Ck2: TCheckBox;
    Ck3: TCheckBox;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    QrEmpresasCodCliInt: TIntegerField;
    QrEmpresasCodEnti: TIntegerField;
    QrEmpresasNome: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPagtosAntCalcFields(DataSet: TDataSet);
    procedure RG1Click(Sender: TObject);
    procedure RG2Click(Sender: TObject);
    procedure RG3Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure frxPagtosMatrGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    function VerificaSituacoes: String;
    procedure ConfiguraRGx(Item, Valor: Integer);
  public
    { Public declarations }
  end;

var
  FmImpPagtos: TFmImpPagtos;
  Fechar : Boolean;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmImpPagtos.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImpPagtos.FormActivate(Sender: TObject);
begin
  TPIni.SetFocus;
  MyObjects.CorIniComponente();
  EdAluno.SetFocus;
end;

procedure TFmImpPagtos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImpPagtos.BtImprimeClick(Sender: TObject);
var
  TbLctA: String;
  {$IFDEF DEFINE_VARLCT}
  TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
  {$ENDIF}
  Liga, Ordem, Virgula: String;
  Aluno, Empresa, EntCliInt, CodCliInt: Integer;
const
  Campo : Array[0..4] of String = ('','NOMEEMPRESA','NOMEALUNO','REALSIT', 'NOMETURMA');
begin
  CodCliInt := QrEmpresasCodCliInt.Value;
  EntCliInt := QrEmpresasCodEnti.Value;
  //
  {$IFDEF DEFINE_VARLCT}
  DModG.Def_EM_ABD(TMeuDB, EntCliInt, CodCliInt, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //erro ?
  if  (CkAberto.Checked = False)
  and (CkVencido.Checked = False)
  and (CkQuitado.Checked = False) then
  begin
    Geral.MB_Aviso('Informe pelo menos uma "Situa��o"');
    CkVencido.SetFocus;
    Exit;
  end;
  Virgula := CO_VAZIO;
  Liga := ' AND (';
  //
  if RG1.ItemIndex > 0 then Virgula := ', ';
  Ordem := Campo[RG1.ItemIndex];
  if RG2.ItemIndex > 0 then
  begin
    Ordem := Ordem + Virgula + Campo[RG2.ItemIndex];
     Virgula := ', ';
  end;
  if RG3.ItemIndex > 0 then
  begin
    Ordem := Ordem + Virgula + Campo[RG3.ItemIndex];
  end;
  //
  if Ordem <> CO_VAZIO then Ordem := 'ORDER BY '+ Ordem;
  QrPagtos.Close;
  QrPagtos.SQL.Clear;
  QrPagtos.SQL.Add('SELECT la.*,  en1.Nome NOMEALUNO,');
  (**)QrPagtos.SQL.Add('en3.Nome NOMERESPONS, tu.Nome NOMETURMA, ');
  QrPagtos.SQL.Add('       en2.RazaoSocial NOMEEMPRESA,');
  QrPagtos.SQL.Add('       ma.Codigo MATRICULA,');
  QrPagtos.SQL.Add('CASE 1');
  QrPagtos.SQL.Add('       WHEN (la.sit=0) AND (la.Vencimento<NOW())    THEN -2');
  QrPagtos.SQL.Add('       WHEN (la.sit=1) AND (la.Vencimento<NOW())    THEN -1');
  QrPagtos.SQL.Add('       WHEN (la.sit=0) AND (la.Vencimento>NOW())    THEN  0');
  QrPagtos.SQL.Add('       WHEN (la.sit=1) AND (la.Vencimento>NOW())    THEN  1');
  QrPagtos.SQL.Add('       WHEN (la.sit=2)                              THEN  2');
  QrPagtos.SQL.Add('       WHEN (la.sit=3)                              THEN  3');
  QrPagtos.SQL.Add('       ELSE -3 END REALSIT');
  QrPagtos.SQL.Add('FROM ' + TbLctA + ' la');
  QrPagtos.SQL.Add('LEFT JOIN entidades en1 ON en1.Codigo=la.Cliente');
  QrPagtos.SQL.Add('LEFT JOIN entidades en2 ON en2.Codigo=la.Cliente');
  QrPagtos.SQL.Add('LEFT JOIN matricul ma ON ma.Codigo=la.FatNum');
  (**)QrPagtos.SQL.Add('LEFT JOIN matritur mc ON mc.Codigo=ma.Codigo');
  (**)QrPagtos.SQL.Add('LEFT JOIN turmas tu      ON tu.Codigo=mc.Turma');
  (**)QrPagtos.SQL.Add('LEFT JOIN entidades en3  ON en3.Codigo=tu.Respons');
  //
  QrPagtos.SQL.Add('WHERE la.Controle > 0 ');
  //
  Aluno := EdAluno.ValueVariant;
  if Aluno <> 0 then
    QrPagtos.SQL.Add('AND en1.Codigo=' + Geral.FF0(Aluno));
  //
  //
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
    QrPagtos.SQL.Add('AND en2.Codigo=' + Geral.FF0(Empresa));
  //
  if CkIni.Checked and CkFim.Checked then
  begin
    QrPagtos.SQL.Add('       AND la.Vencimento BETWEEN '''+
                             FormatDateTime(VAR_FORMATDATE, TPIni.Date)+''' AND '''+
                             FormatDateTime(VAR_FORMATDATE, TPFim.Date)+'''');
  end else if CkIni.Checked then
    QrPagtos.SQL.Add('       AND la.Vencimento = '''+
                             FormatDateTime(VAR_FORMATDATE, TPIni.Date)+'''')
  else if CkFim.Checked then
    QrPagtos.SQL.Add('       AND la.Vencimento = '''+
                             FormatDateTime(VAR_FORMATDATE, TPFim.Date)+'''');
  if CkVencido.Checked then
  begin
    QrPagtos.SQL.Add('     '+Liga+'((la.sit=0) AND (la.Vencimento<NOW()))');
    QrPagtos.SQL.Add('	        OR ((la.sit=1) AND (la.Vencimento<NOW()))');
    Liga := ' OR ';
  end;
  if CkAberto.Checked then
  begin
    QrPagtos.SQL.Add('     '+Liga+'((la.sit=0) AND (la.Vencimento>NOW()))');
    QrPagtos.SQL.Add('	        OR ((la.sit=1) AND (la.Vencimento>NOW()))');
    Liga := ' OR ';
  end;
  if CkQuitado.Checked then
  begin
    QrPagtos.SQL.Add('	  '+Liga+' (la.sit=2)');
    QrPagtos.SQL.Add('	        OR (la.sit=3)');
  end;
  QrPagtos.SQL.Add(')');
  QrPagtos.SQL.Add(ORDEM);
  UMyMod.AbreQuery(QrPagtos, Dmod.MyDB);
  //
  MyObjects.frxDefineDataSets(frxPagtosMatr, [
    frxDsPagtos,
    DModG.frxDsMaster
    ]);
  //
  MyObjects.frxMostra(frxPagtosMatr, 'Vencimento de matr�culas');
  QrPagtos.Close;
end;

function TFmImpPagtos.VerificaSituacoes: String;
var
  Lista: TStringList;
  i: integer;
begin
  Lista := TStringList.Create;
  if CkVencido.Checked then Lista.Add('Vencidos');
  if CkAberto.Checked  then Lista.Add('Abertos');
  if CkQuitado.Checked then Lista.Add('Quitados');
  //
  Result := '';
  for i := Lista.Count -1 downto 0 do
  begin
    case i of
      0: Result := Lista[i] + Result;
      1:
      begin
        if Lista.Count = 2 then Result := ' e ' + Lista[i] + Result
        else Result := ', ' + Lista[i] + Result
      end;
      else Result := ' e ' + Lista[i] + Result;
    end;
  end;
  Lista.Free;
end;

procedure TFmImpPagtos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  UMyMod.AbreQuery(QrAlunos, DMod.MyDB);
  UMyMod.AbreQuery(QrEmpresas, DMod.MyDB);
  //
  TPIni.Date := Date;
  TPFim.Date := Date;
end;

procedure TFmImpPagtos.QrPagtosAntCalcFields(DataSet: TDataSet);
begin
  if QrPagtosSit.Value = -1 then
     QrPagtosNOMESIT.Value := CO_IMPORTACAO
  else if QrPagtosSit.Value = 1 then
     QrPagtosNOMESIT.Value := CO_PAGTOPARCIAL
  else if QrPagtosSit.Value = 2 then
       QrPagtosNOMESIT.Value := CO_QUITADA
  else if QrPagtosSit.Value = 3 then
       QrPagtosNOMESIT.Value := CO_COMPENSADA
  else if QrPagtosVencimento.Value < Date then
       QrPagtosNOMESIT.Value := CO_VENCIDA
  else QrPagtosNOMESIT.Value := CO_EMABERTO;
  case QrPagtosSit.Value of
    0: QrPagtosSALDO.Value := QrPagtosCredito.Value - QrPagtosDebito.Value;
    1: QrPagtosSALDO.Value := (QrPagtosCredito.Value - QrPagtosDebito.Value) + QrPagtosPago.Value;
    else QrPagtosSALDO.Value := 0;
  end;
  if (QrPagtosVencimento.Value < Date) and (QrPagtosSit.Value<2) then
  begin
    QrPagtosVENCIDO.Value := QrPagtosVencimento.Value;
    QrPagtosMULTAREAL.Value := QrPagtosMulta.Value;
  end;
  if QrPagtosVencimento.Value < Date then
    QrPagtosNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagtosVencimento.Value)
  else QrPagtosNOMEVENCIDO.Value := CO_VAZIO;
  if QrPagtosVENCIDO.Value > 0 then
     QrPagtosATRAZODD.Value := Trunc(Date) - QrPagtosVENCIDO.Value
  else QrPagtosATRAZODD.Value := 0;
  if QrPagtosATRAZODD.Value > 0 then QrPagtosATUALIZADO.Value :=
  (QrPagtosATRAZODD.Value * QrPagtosMoraDia.Value) +
  QrPagtosMulta.Value else QrPagtosATUALIZADO.Value := 0;
  if QrPagtosSit.Value<2 then QrPagtosATUALIZADO.Value :=
  QrPagtosATUALIZADO.Value + QrPagtosSALDO.Value;
  //
  if Trim(QrPagtosNOMEEMPRESA.Value) = CO_VAZIO then
    QrPagtosNOMEEMPRESA2.Value := '# Empresa n�o definida #'
  else QrPagtosNOMEEMPRESA2.Value := QrPagtosNOMEEMPRESA.Value;
  if Trim(QrPagtosNOMEALUNO.Value) = CO_VAZIO then
    QrPagtosNOMEALUNO2.Value := '# Aluno n�o definido #'
  else QrPagtosNOMEALUNO2.Value := QrPagtosNOMEALUNO.Value;
end;

procedure TFmImpPagtos.RG1Click(Sender: TObject);
begin
  ConfiguraRGx(1, RG1.ItemIndex);
end;

procedure TFmImpPagtos.ConfiguraRGx(Item, Valor: Integer);
begin
  if Valor = 0 then
  begin
    if Item < 3 then RG3.ItemIndex := 0;
    if Item < 3 then RG3.Enabled  := False;
    if Item < 3 then Ck3.Checked  := False;
    if Item < 3 then Ck3.Enabled  := False;
    //
    if Item < 2 then RG2.ItemIndex := 0;
    if Item < 2 then RG2.Enabled  := False;
    if Item < 2 then Ck2.Checked  := False;
    if Item < 2 then Ck2.Enabled  := False;
  end else begin
    if Item = 1 then RG2.Enabled  := True;
    if Item = 1 then Ck2.Enabled  := True;
    //
    if Item = 2 then RG3.Enabled  := True;
    if Item = 2 then Ck3.Enabled  := True;
    //
  end;
end;

procedure TFmImpPagtos.RG2Click(Sender: TObject);
begin
  ConfiguraRGx(2, RG2.ItemIndex);
end;

procedure TFmImpPagtos.RG3Click(Sender: TObject);
begin
  ConfiguraRGx(3, RG3.ItemIndex);
end;

procedure TFmImpPagtos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImpPagtos.QrPagtosCalcFields(DataSet: TDataSet);
begin
  if QrPagtosSit.Value = -1 then
     QrPagtosNOMESIT.Value := CO_IMPORTACAO
  else if QrPagtosSit.Value = 1 then
     QrPagtosNOMESIT.Value := CO_PAGTOPARCIAL
  else if QrPagtosSit.Value = 2 then
       QrPagtosNOMESIT.Value := CO_QUITADA
  else if QrPagtosSit.Value = 3 then
       QrPagtosNOMESIT.Value := CO_COMPENSADA
  else if QrPagtosVencimento.Value < Date then
       QrPagtosNOMESIT.Value := CO_VENCIDA
  else QrPagtosNOMESIT.Value := CO_EMABERTO;
  case QrPagtosSit.Value of
    0: QrPagtosSALDO.Value := QrPagtosCredito.Value - QrPagtosDebito.Value;
    1: QrPagtosSALDO.Value := (QrPagtosCredito.Value - QrPagtosDebito.Value) + QrPagtosPago.Value;
    else QrPagtosSALDO.Value := 0;
  end;
  if (QrPagtosVencimento.Value < Date) and (QrPagtosSit.Value<2) then
  begin
    QrPagtosVENCIDO.Value := QrPagtosVencimento.Value;
    QrPagtosMULTAREAL.Value := QrPagtosMulta.Value;
  end;
  if QrPagtosVencimento.Value < Date then
    QrPagtosNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagtosVencimento.Value)
  else QrPagtosNOMEVENCIDO.Value := CO_VAZIO;
  if QrPagtosVENCIDO.Value > 0 then
     QrPagtosATRAZODD.Value := Trunc(Date) - QrPagtosVENCIDO.Value
  else QrPagtosATRAZODD.Value := 0;
  if QrPagtosATRAZODD.Value > 0 then QrPagtosATUALIZADO.Value :=
  (QrPagtosATRAZODD.Value * QrPagtosMoraDia.Value) +
  QrPagtosMulta.Value else QrPagtosATUALIZADO.Value := 0;
  if QrPagtosSit.Value<2 then QrPagtosATUALIZADO.Value :=
  QrPagtosATUALIZADO.Value + QrPagtosSALDO.Value;
  //
  if Trim(QrPagtosNOMEEMPRESA.Value) = CO_VAZIO then
    QrPagtosNOMEEMPRESA2.Value := '# Empresa n�o definida #'
  else QrPagtosNOMEEMPRESA2.Value := QrPagtosNOMEEMPRESA.Value;
  if Trim(QrPagtosNOMEALUNO.Value) = CO_VAZIO then
    QrPagtosNOMEALUNO2.Value := '# Aluno n�o definido #'
  else QrPagtosNOMEALUNO2.Value := QrPagtosNOMEALUNO.Value;
end;

procedure TFmImpPagtos.frxPagtosMatrGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'PERIODO' then Value :=
      FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
  //
  if VarName = 'NOMEREL_001' then Value := 'RELAT�RIO DE D�BITOS';
  //
  if VarName = 'VFR_LA1NOME' then
  begin
    case RG1.ItemIndex of
      0: Value := CO_VAZIO;
      1: Value := QrPagtosNOMEEMPRESA2.Value;
      2: Value := QrPagtosNOMEALUNO2.Value;
      3: Value := QrPagtosNOMESIT.Value;
      4: Value := QrPagtosNOMETURMA.Value;
      else Value := 'ERRO - VFR_LA1NOME';
    end;
  end;
  //
  if VarName = 'VFR_LA2NOME' then
  begin
    case RG2.ItemIndex of
      0: Value := CO_VAZIO;
      1: Value := QrPagtosNOMEEMPRESA2.Value;
      2: Value := QrPagtosNOMEALUNO2.Value;
      3: Value := QrPagtosNOMESIT.Value;
      4: Value := QrPagtosNOMETURMA.Value;
      else Value := 'ERRO - VFR_LA2NOME';
    end;
  end;
  //
  if VarName = 'VFR_LA3NOME' then
  begin
    case RG3.ItemIndex of
      0: Value := CO_VAZIO;
      1: Value := QrPagtosNOMEEMPRESA2.Value;
      2: Value := QrPagtosNOMEALUNO2.Value;
      3: Value := QrPagtosNOMESIT.Value;
      4: Value := QrPagtosNOMETURMA.Value;
      else Value := 'ERRO - VFR_LA3NOME';
    end;
  end;
  //
  if VarName = 'SITUACOES' then
    Value := VerificaSituacoes;
  //
  if VarName = 'ALUNO' then
    if CBAluno.KeyValue = NULL then
    Value := 'TODOS' else Value := CBAluno.Text;
  //
  if VarName = 'PATROCINADORA' then
    if CBEmpresa.KeyValue = NULL then
    Value := 'TODAS' else Value := CBEmpresa.Text;

  //

  if VarName = 'VFR_GRADE' then
    if CkGrade.Checked then Value := True else Value := False;
  //
  if VarName = 'VFR_ORD1' then
    if Ck1.Checked then Value := RG1.ItemIndex else Value := -1;
  if VarName = 'VFR_ORD2' then
    if Ck2.Checked then Value := RG2.ItemIndex else Value := -1;
  if VarName = 'VFR_ORD3' then
    if Ck3.Checked then Value := RG3.ItemIndex else Value := -1;
end;

{
SELECT la.*,  en1.Nome NOMEALUNO,
en3.Nome NOMERESPONS, tur.Nome NOMETURMA,
       en2.RazaoSocial NOMEEMPRESA,
       ren.Codigo MATRICULA,
CASE 1
       WHEN (la.sit=0) AND (la.Vencimento<NOW())    THEN -2
       WHEN (la.sit=1) AND (la.Vencimento<NOW())    THEN -1
       WHEN (la.sit=0) AND (la.Vencimento>NOW())    THEN  0
       WHEN (la.sit=1) AND (la.Vencimento>NOW())    THEN  1
       WHEN (la.sit=2)                              THEN  2
       WHEN (la.sit=3)                              THEN  3
       ELSE -3 END REALSIT
FROM ' + TbLctA + ' la
LEFT JOIN entidades en1 ON en1.Codigo=la.Cliente
LEFT JOIN entidades en2 ON en2.Codigo=la.Cliente
LEFT JOIN matriren ren ON ren.Controle=la.FatNum
LEFT JOIN matricul  cul ON cul.Codigo=ren.Codigo
LEFT JOIN matritur mtu ON mtu.Codigo=ren.Codigo AND mtu.Controle=ren.Controle
LEFT JOIN turmas tur      ON tur.Codigo=mtu.Turma
LEFT JOIN entidades en3  ON en3.Codigo=tur.Respons
WHERE la.Controle > 0
AND 
     (
          (
               (la.Sit=0) AND (la.Vencimento< SYSDATE())
          )
     ) OR (
          (
               (la.Sit=1) AND (la.Vencimento < SYSDATE())
          )
     )

ORDER BY Data
}

end.

