object FmTarifasNovas: TFmTarifasNovas
  Left = 339
  Top = 185
  Caption = 'TAR-CADAS-003 :: Tarifas Novas'
  ClientHeight = 502
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Confirmar'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtTudo: TBitBtn
      Tag = 127
      Left = 400
      Top = 4
      Width = 100
      Height = 40
      Caption = '&Todos'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtTudoClick
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 510
      Top = 4
      Width = 100
      Height = 40
      Caption = '&Nenhum'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtNenhumClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Tarifas Novas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 406
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 82
        Height = 13
        Caption = 'V'#225'lida a partir de:'
      end
      object Label2: TLabel
        Left = 480
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Percentual:'
      end
      object Label3: TLabel
        Left = 564
        Top = 4
        Width = 64
        Height = 13
        Caption = 'Arredondar $:'
      end
      object Label4: TLabel
        Left = 244
        Top = 4
        Width = 67
        Height = 13
        Caption = 'Pre'#231'os atuais:'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 12
        Top = 20
        Width = 112
        Height = 21
        Date = 40614.701046388890000000
        Time = 40614.701046388890000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object BtTaxa: TBitBtn
        Left = 908
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Taxa'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTaxaClick
      end
      object EdPercentual: TdmkEdit
        Left = 480
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object BitBtn1: TBitBtn
        Left = 644
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Reajustar'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
      object EdRound: TdmkEdit
        Left = 564
        Top = 20
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.000000000000000000
        ValWarn = False
      end
      object BitBtn2: TBitBtn
        Left = 148
        Top = 4
        Width = 90
        Height = 40
        Caption = 'R&einiciar'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BitBtn2Click
      end
      object TPAtuais: TdmkEditDateTimePicker
        Left = 244
        Top = 20
        Width = 112
        Height = 21
        Date = 40614.701046388890000000
        Time = 40614.701046388890000000
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 1
      Top = 49
      Width = 1006
      Height = 356
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o da tarifa'
          Width = 584
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativa'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoPer'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdePer'
          Title.Caption = 'Qtde'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Taxa'
          Title.Caption = '$ Taxa de matr'#237'cula'
          Width = 94
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPer'
          Title.Caption = '$ Pre'#231'o per'#237'odo'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = '$ Pre'#231'o plano'
          Width = 92
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTarifas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      OnKeyDown = dmkDBGridDAC1KeyDown
      SQLTable = '_tarifas_'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o da tarifa'
          Width = 584
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativa'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoPer'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdePer'
          Title.Caption = 'Qtde'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Taxa'
          Title.Caption = '$ Taxa de matr'#237'cula'
          Width = 94
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPer'
          Title.Caption = '$ Pre'#231'o per'#237'odo'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Preco'
          Title.Caption = '$ Pre'#231'o plano'
          Width = 92
          Visible = True
        end>
    end
  end
  object TbTarifas: TmySQLTable
    Database = Dmod.ZZDB
    TableName = '_tarifas_'
    Left = 444
    Top = 192
    object TbTarifasCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbTarifasNome: TWideStringField
      FieldName = 'Nome'
      ReadOnly = True
      Size = 100
    end
    object TbTarifasAtiva: TSmallintField
      FieldName = 'Ativa'
      ReadOnly = True
      MaxValue = 1
    end
    object TbTarifasTipoPer: TIntegerField
      FieldName = 'TipoPer'
      ReadOnly = True
    end
    object TbTarifasQtdePer: TIntegerField
      FieldName = 'QtdePer'
      ReadOnly = True
    end
    object TbTarifasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object TbTarifasValPer: TFloatField
      FieldName = 'ValPer'
      DisplayFormat = '#,###,##0.00'
    end
    object TbTarifasTaxa: TFloatField
      FieldName = 'Taxa'
      DisplayFormat = '#,###,##0.00'
    end
    object TbTarifasPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsTarifas: TDataSource
    DataSet = TbTarifas
    Left = 472
    Top = 192
  end
end
