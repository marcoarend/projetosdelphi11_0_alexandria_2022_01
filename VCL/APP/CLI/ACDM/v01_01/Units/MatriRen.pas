unit MatriRen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, ComCtrls, Grids,
  DBGrids, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkLabel, UnDmkEnums;

type
  TFmMatriRen = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label8: TLabel;
    TPIni: TdmkEditDateTimePicker;
    TPFim: TdmkEditDateTimePicker;
    Label10: TLabel;
    CkRenoRef: TCheckBox;
    Progress3: TProgressBar;
    Progress1: TProgressBar;
    Progress2: TProgressBar;
    Label2: TLabel;
    EdTarifa: TdmkEditCB;
    CBTarifa: TdmkDBLookupComboBox;
    QrTarifas: TmySQLQuery;
    DsTarifas: TDataSource;
    QrTarifasCodigo: TIntegerField;
    QrTarifasNome: TWideStringField;
    QrTarifasIts: TmySQLQuery;
    DsTarifasIts: TDataSource;
    QrLoc: TmySQLQuery;
    QrTarifasItsCodigo: TIntegerField;
    QrTarifasItsData: TDateField;
    QrTarifasItsPreco: TFloatField;
    QrTarifasItsLk: TIntegerField;
    QrTarifasItsDataCad: TDateField;
    QrTarifasItsDataAlt: TDateField;
    QrTarifasItsUserCad: TIntegerField;
    QrTarifasItsUserAlt: TIntegerField;
    QrTarifasItsTaxa: TFloatField;
    QrLocMAXDATA: TDateField;
    QrTarifasPERIODO: TWideStringField;
    QrTarifasAtiva: TSmallintField;
    QrTarifasLk: TIntegerField;
    QrTarifasDataCad: TDateField;
    QrTarifasDataAlt: TDateField;
    QrTarifasUserCad: TIntegerField;
    QrTarifasUserAlt: TIntegerField;
    QrTarifasTipoPer: TIntegerField;
    QrTarifasQtdePer: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    CkTaxa: TCheckBox;
    Label5: TLabel;
    EdACobrar: TdmkEdit;
    SpeedButton1: TSpeedButton;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    LaTipo: TdmkLabel;
    CkDesativar: TCheckBox;
    QrTarifasGrupo: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TPFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPrecoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrTarifasCalcFields(DataSet: TDataSet);
    procedure TPIniChange(Sender: TObject);
    procedure QrTarifasAfterScroll(DataSet: TDataSet);
    procedure EdACobrarExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPIniExit(Sender: TObject);
    procedure EdTarifaChange(Sender: TObject);
    procedure CkTaxaClick(Sender: TObject);
  private
    { Private declarations }
    procedure DefineDataFinal;
    procedure ConfiguraValor;
  public
    { Public declarations }
    FCodigo, FControle, FEmpresa: Integer;
  end;

var
  FmMatriRen: TFmMatriRen;

implementation

uses UnMyObjects, Module, Matricul, Principal, MyDBCheck;

{$R *.DFM}

procedure TFmMatriRen.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriRen.BtConfirmaClick(Sender: TObject);
var
  DataI, DataF: String;
  Codigo, Controle, Conta, Item, SubItem, Periodo, Desativar, TarBase, Empresa: Integer;
  Taxa, Preco, ACobrar: Double;
begin
  // 2011-07-16
  Desativar  := Geral.BoolToInt(CkDesativar.Checked);
  TarBase    := EdTarifa.ValueVariant;
  Empresa    := FEmpresa;
  //
  if LaTipo.SQLType = stIns then
  // Fim 2011-07-16
  begin
    if (QrTarifasIts.State = dsInactive) or (QrTarifasIts.RecordCount=0) then
    begin
      Geral.MB_Aviso('N�o h� tarifa definida para o per�odo!');
      Exit;
    end;
  end;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    if CkTaxa.Checked then Taxa := QrTarifasItsTaxa.Value else Taxa := 0;
    DataI      := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
    DataF      := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
    Periodo    := QrTarifasItsCodigo.Value;
    Preco      := QrTarifasItsPreco.Value;
    ACobrar    := EdACobrar.ValueVariant;
    Codigo     := FCodigo;
    Controle := UMyMod.BuscaEmLivreY_Def('matriren', 'Controle', LaTipo.SQLType, FControle);
    // 2011-07-16
    //if
{
    UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'matriren', False, [
    'Controle', 'Periodo', 'DataF',
    'Preco', 'Taxa', 'ACobrar',
    'Ativo'], [
    'Codigo', 'DataI'], [
    Controle, Periodo, DataF,
    Preco, Taxa, ACobrar,
    Ativo], [
    Codigo, DataI], True);
    //
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'matriren', False,
    [
    'Codigo', 'Periodo', 'DataI',
    'DataF', (*'Valor', 'TotalFator',*) // ???
    'Preco', 'Taxa', 'ACobrar',
    'TarBase', 'Desativar', 'Empresa'
    ], ['Controle'],
    [
    Codigo, Periodo, DataI,
    DataF, (*Valor, TotalFator,*) // ????
    Preco, Taxa, ACobrar,
    TarBase, Desativar, FEmpresa
    ], [Controle], True);
    // Fim 2011-07-16
    //
    if (CkRenoRef.Checked) and (CkRenoRef.Visible) then
    begin
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('INSERT INTO MatriTur SET ');
      Dmod.QrUpdU.SQL.Add('  Fator      =:P1 ');
      Dmod.QrUpdU.SQL.Add(', Turma      =:P2');
      Dmod.QrUpdU.SQL.Add(', Codigo     =:Pa');
      Dmod.QrUpdU.SQL.Add(', Controle   =:Pb');
      Dmod.QrUpdU.SQL.Add(', Conta      =:Pc');
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO MatriAti SET ');
      Dmod.QrUpd.SQL.Add('Atividade=:P0, Codigo=:P1, Controle=:P2, Conta=:P3, Item=:P4');
      //
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('INSERT INTO MatriPer SET ');
      Dmod.QrUpdM.SQL.Add('Periodo=:P0, Codigo=:P1, Controle=:P2, Conta=:P3, Item=:P4, SubItem=:P5');
      //
      FmMatricul.QrMatriTur.First;
      Progress1.Position := 0;
      Progress1.Max := FmMatricul.QrMatriTur.RecordCount;
      while not FmMatricul.QrMatriTur.Eof do
      begin
        Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'MatriTur', 'MatriTur', 'Codigo');
        Dmod.QrUpdU.Params[0].AsFloat   := FmMatricul.QrMatriTurFator.Value;
        Dmod.QrUpdU.Params[1].AsInteger := FmMatricul.QrMatriTurTurma.Value;
        Dmod.QrUpdU.Params[2].AsInteger := FCodigo;
        Dmod.QrUpdU.Params[3].AsInteger := Controle;
        Dmod.QrUpdU.Params[4].AsInteger := Conta;
        Dmod.QrUpdU.ExecSQL;
        //
        FmMatricul.QrMatriAti.First;
        Progress2.Position := 0;
        Progress2.Max := FmMatricul.QrMatriAti.RecordCount;
        while not FmMatricul.QrMatriAti.Eof do
        begin
          Item := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'MatriAti', 'MatriAti', 'Codigo');
          //
          Dmod.QrUpd.Params[00].AsInteger := FmMatricul.QrMatriAtiAtividade.Value;
          Dmod.QrUpd.Params[01].AsInteger := FCodigo;
          Dmod.QrUpd.Params[02].AsInteger := Controle;
          Dmod.QrUpd.Params[03].AsInteger := Conta;
          Dmod.QrUpd.Params[04].AsInteger := Item;
          Dmod.QrUpd.ExecSQL;
          FmMatricul.QrMatriPer.First;
          Progress3.Position := 0;
          Progress3.Max := FmMatricul.QrMatriPer.RecordCount;
          while not FmMatricul.QrMatriPer.Eof do
          begin
            SubItem := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'MatriPer', 'MatriPer', 'Codigo');
            //
            Dmod.QrUpdM.Params[00].AsInteger := FmMatricul.QrMatriPerPeriodo.Value;
            Dmod.QrUpdM.Params[01].AsInteger := FCodigo;
            Dmod.QrUpdM.Params[02].AsInteger := Controle;
            Dmod.QrUpdM.Params[03].AsInteger := Conta;
            Dmod.QrUpdM.Params[04].AsInteger := Item;
            Dmod.QrUpdM.Params[05].AsInteger := SubItem;
            Dmod.QrUpdM.ExecSQL;
            Progress3.Position := Progress3.Position + 1;
            FmMatricul.QrMatriPer.Next;
          end;
          //
          Progress2.Position := Progress2.Position + 1;
          FmMatricul.QrMatriAti.Next;
        end;
        //
        Progress1.Position := Progress1.Position + 1;
        FmMatricul.QrMatriTur.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  FmMatricul.AtualizaAtivo(FCodigo, Controle, Desativar);
  FmPrincipal.AtualizaAtivosEVencidos();
  Close;
end;

procedure TFmMatriRen.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmMatriRen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMatriRen.TPFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin

  end;
end;

procedure TFmMatriRen.EdPrecoExit(Sender: TObject);
begin
  //EdPreco.Text := Geral.TFT(EdPreco.Text, 2, siPositivo);
end;

procedure TFmMatriRen.CkTaxaClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Deseja atualizar o valor real?') = ID_YES then
    ConfiguraValor;
end;

procedure TFmMatriRen.ConfiguraValor;
var
  CobrMatri: Boolean;
  Matricula, Valor: Double;
begin
  Matricula := QrTarifasItsTaxa.Value;
  Valor     := QrTarifasItsPreco.Value;
  CobrMatri := CkTaxa.Checked;
  //
  if CobrMatri then
    EdACobrar.ValueVariant := Matricula + Valor
  else
    EdACobrar.ValueVariant := Valor;
end;

procedure TFmMatriRen.EdTarifaChange(Sender: TObject);
begin
  if EdTarifa.ValueVariant <> 0 then
    ConfiguraValor;
end;

procedure TFmMatriRen.FormCreate(Sender: TObject);
begin
  TPIni.Date := Date;
  TPFim.Date := Date;
  //
  UMyMod.AbreQuery(QrTarifas, DMod.MyDB);
end;

procedure TFmMatriRen.QrTarifasCalcFields(DataSet: TDataSet);
var
  TxtPer: String;
begin
  if QrTarifasQtdePer.Value > 1 then
  begin
    case QrTarifasTipoPer.Value of
      0: TxtPer := 'meses';
      1: TxtPer := 'semanas';
      2: TxtPer := 'dias';
      else TxtPer := '???';
    end;
  end else begin
    case QrTarifasTipoPer.Value of
      0: TxtPer := 'm�s';
      1: TxtPer := 'semana';
      2: TxtPer := 'dia';
      else TxtPer := '???';
    end;
  end;
  QrTarifasPERIODO.Value := IntToStr(QrTarifasQtdePer.Value)+' '+TxtPer;
  //
  DefineDataFinal;
end;

procedure TFmMatriRen.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  //
  FmPrincipal.CadastroTarifasGru(QrTarifasGrupo.Value, EdTarifa.ValueVariant);
  //
  if VAR_CADASTRO2 <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTarifa, CBTarifa, QrTarifas, VAR_CADASTRO2);
    EdTarifa.SetFocus;
  end;
end;

procedure TFmMatriRen.DefineDataFinal;
begin
  QrTarifasIts.Close;
  if QrTarifas.State = dsBrowse then
  begin
    case QrTarifasTipoPer.Value of
      0: TPFim.Date := IncMonth(TPIni.Date, QrTarifasQtdePer.Value) -1;
      1: TPFim.Date := TPIni.Date + (QrTarifasQtdePer.Value *7) -1;
      2: TPFim.Date := TPIni.Date + QrTarifasQtdePer.Value -1;
      else TPFim.Date := TPIni.Date;
    end;
    QrLoc.Close;
    QrLoc.Params[0].AsInteger := QrTarifasCodigo.Value;
    QrLoc.Params[1].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
    QrLoc.Open;
    //
    QrTarifasIts.Close;
    if QrLoc.RecordCount > 0 then
    begin
      QrTarifasIts.Params[0].AsInteger := QrTarifasCodigo.Value;
      QrTarifasIts.Params[1].AsString  :=
        FormatDateTime(VAR_FORMATDATE, QrLocMAXDATA.Value);
      QrTarifasIts.Open;
    end;
  end else TPFim.Date := TPIni.Date;
end;

procedure TFmMatriRen.TPIniChange(Sender: TObject);
begin
  DefineDataFinal;
end;

procedure TFmMatriRen.TPIniClick(Sender: TObject);
begin
  DefineDataFinal;
end;

procedure TFmMatriRen.TPIniExit(Sender: TObject);
begin
  DefineDataFinal;
end;

procedure TFmMatriRen.QrTarifasAfterScroll(DataSet: TDataSet);
begin
  DefineDataFinal;
end;

procedure TFmMatriRen.EdACobrarExit(Sender: TObject);
begin
  EdACobrar.Text := Geral.TFT(EdACobrar.Text, 2, siPositivo);
end;

end.
