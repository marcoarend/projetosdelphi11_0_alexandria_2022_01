unit TarifasNovas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, dmkDBGridDAC, UnDmkProcFunc;

type
  //THackDBGrid = class(TDBGrid);
  TFmTarifasNovas = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    TPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    TbTarifas: TmySQLTable;
    DsTarifas: TDataSource;
    dmkDBGridDAC1: TdmkDBGridDAC;
    TbTarifasCodigo: TIntegerField;
    TbTarifasNome: TWideStringField;
    TbTarifasAtiva: TSmallintField;
    TbTarifasTipoPer: TIntegerField;
    TbTarifasQtdePer: TIntegerField;
    TbTarifasAtivo: TSmallintField;
    TbTarifasValPer: TFloatField;
    TbTarifasTaxa: TFloatField;
    TbTarifasPreco: TFloatField;
    BtTaxa: TBitBtn;
    EdPercentual: TdmkEdit;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    EdRound: TdmkEdit;
    Label3: TLabel;
    BitBtn2: TBitBtn;
    TPAtuais: TdmkEditDateTimePicker;
    Label4: TLabel;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtTaxaClick(Sender: TObject);
    procedure dmkDBGridDAC1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure Reiniciar(Inserir: Boolean);
    procedure AtivaTodos(Ativo: Integer);
  public
    { Public declarations }
  end;

  var
  FmTarifasNovas: TFmTarifasNovas;

implementation

uses UnMyObjects, ModuleGeral, CreateApl, UnInternalConsts, Module, UMySQLModule;

{$R *.DFM}

procedure TFmTarifasNovas.AtivaTodos(Ativo: Integer);
var
  Codigo: Integer;
begin
  Codigo := TbTarifasCodigo.Value;
  TbTarifas.DisableControls;
  try
    Codigo := TbTarifasCodigo.Value;
    TbTarifas.First;
    while not TbTarifas.Eof do
    begin
      TbTarifas.Edit;
      TbTarifasAtivo.Value := Ativo;
      TbTarifas.Post;
      //
      TbTarifas.Next;
    end;
  finally
    TbTarifas.EnableControls;
    TbTarifas.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmTarifasNovas.BitBtn1Click(Sender: TObject);
var
  ValPer, Perc, Arred: Double;
  Codigo: Integer;
begin
  if Geral.MensagemBox('Confirma o reajuste de ' + EdPercentual.Text +
  ' com arredondamento a cada $ ' + EdRound.Text + '?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    TbTarifas.DisableControls;
    Perc := EdPercentual.ValueVariant;
    Arred := EdRound.ValueVariant;
    Codigo := TbTarifasCodigo.Value;
    try
      TbTarifas.First;
      while not TbTarifas.Eof do
      begin
        ValPer := TbTarifasValPer.Value;
        ValPer := ValPer + (ValPer * Perc / 100);
        ValPer := Round(ValPer / Arred) * Arred;
        //
        TbTarifas.Edit;
        TbTarifasValPer.Value := ValPer;
        TbTarifasPreco.Value := ValPer * TbTarifasQtdePer.Value;
        TbTarifas.Post;
        //
        TbTarifas.Next;
      end;
    finally
      TbTarifas.EnableControls;
      TbTarifas.Locate('Codigo', Codigo, []);
    end;
  end;
end;

procedure TFmTarifasNovas.BitBtn2Click(Sender: TObject);
begin
  Reiniciar(True);
end;

procedure TFmTarifasNovas.BtNenhumClick(Sender: TObject);
begin
  AtivaTodos(0);
end;

procedure TFmTarifasNovas.BtOKClick(Sender: TObject);
var
  Preco, Taxa, ValPer: Double;
  Codigo: Integer;
  Data: String;
begin
  if Geral.MensagemBox(
  'Confirma a inclus�o / altera��o das tarifas para o dia ' +
  Geral.FDT(TPData.Date, 2) + '?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    TbTarifas.DisableControls;
    try
      Data := Geral.FDT(TPData.Date, 1);
      TbTarifas.First;
      while not TbTarifas.Eof do
      begin
        Codigo := TbTarifasCodigo.Value;
        Preco  := TbTarifasPreco.Value;
        ValPer := TbTarifasValPer.Value;
        Taxa   := TbTarifasTaxa.Value;
        //
        UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'tarifasits', False, [
        'Preco', 'Taxa', 'ValPer'], [
        'Codigo', 'Data'], [
        'Preco', 'Taxa', 'ValPer'], [
        Preco, Taxa, ValPer], [
        Codigo, Data], [
        Preco, Taxa, ValPer], True);
        //
        TbTarifas.Next;
      end;
      Reiniciar(False);
      Close;
    finally
      TbTarifas.EnableControls;
    end;
  end;
end;

procedure TFmTarifasNovas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTarifasNovas.BtTaxaClick(Sender: TObject);
const
  Casas = 2;
var
  Valor: Double;
begin
  Valor := TbTarifasTaxa.Value;
  if dmkPF.ObtemValorDouble(Valor, Casas) then
  begin
    TbTarifas.DisableControls;
    try
      TbTarifas.First;
      while not TbTarifas.Eof do
      begin
        TbTarifas.Edit;
        TbTarifasTaxa.Value := Valor;
        TbTarifas.Post;
        //
        TbTarifas.Next;
      end;
    finally
      TbTarifas.EnableControls;
    end;
  end;
end;

procedure TFmTarifasNovas.BtTudoClick(Sender: TObject);
begin
  AtivaTodos(1);
end;

procedure TFmTarifasNovas.dmkDBGridDAC1CellClick(Column: TColumn);
const
  Casas = 2;
var
  Valor: Double;
begin
  if Column.FieldName = 'ValPer' then
  begin
    Valor := TbTarifasValPer.Value;
    if dmkPF.ObtemValorDouble(Valor, Casas) then
    begin
      TbTarifas.Edit;
      TbTarifasValPer.Value := Valor;
      TbTarifasPreco.Value := Valor * TbTarifasQtdePer.Value;
      TbTarifas.Post;
      //
      TbTarifas.Next;
    end;
  end else
  if Column.FieldName = 'Preco' then
  begin
    Valor := TbTarifasPreco.Value;
    if dmkPF.ObtemValorDouble(Valor, Casas) then
    begin
      TbTarifas.Edit;
      TbTarifasPreco.Value := Valor;
      TbTarifasValPer.Value := Valor / TbTarifasQtdePer.Value;
      TbTarifas.Post;
      //
      TbTarifas.Next;
    end;
  end;
end;

procedure TFmTarifasNovas.dmkDBGridDAC1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  Casas = 2;
var
  Valor: Double;
begin
  if Key = VK_RETURN then
  begin
    Valor := TbTarifasValPer.Value;
    if dmkPF.ObtemValorDouble(Valor, Casas) then
    begin
      TbTarifas.Edit;
      TbTarifasValPer.Value := Valor;
      TbTarifasPreco.Value := Valor * TbTarifasQtdePer.Value;
      TbTarifas.Post;
      //
      TbTarifas.Next;
    end;
  end;
end;

procedure TFmTarifasNovas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTarifasNovas.FormCreate(Sender: TObject);
begin
  TPData.Date := Date;
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MAX(Data) Data FROM TarifasIts');
  Dmod.QrAux.Open;
  TPAtuais.Date := Dmod.QrAux.FieldByName('Data').AsDateTime;
  //
  AplCriar.CriaSeNaoExisteTempTable(ntrtt_Tarifas, DModG.QrUpdPID1, False);
  TbTarifas.Close;
  TbTarifas.Database := DmodG.MyPID_DB;
  TbTarifas.Open;
  if TbTarifas.RecordCount = 0 then
    Reiniciar(True);
end;

procedure TFmTarifasNovas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmTarifasNovas.Reiniciar(Inserir: Boolean);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM _tarifas_');
  DModG.QrUpdPID1.ExecSQL;
  //
  if Inserir then
  begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO _tarifas_');
    DModG.QrUpdPID1.SQL.Add('SELECT Codigo, Nome, Ativa, TipoPer, QtdePer,');
    DModG.QrUpdPID1.SQL.Add('0 ValPer, 0 Taxa, 0 Preco, 1 Ativo');
    DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.tarifas');
    DModG.QrUpdPID1.ExecSQL;
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE _tarifas_ tar, ' + TMeuDB + '.tarifasits its');
    DModG.QrUpdPID1.SQL.Add('SET tar.Preco=its.Preco, tar.Taxa=its.Taxa,');
    DModG.QrUpdPID1.SQL.Add('tar.ValPer=its.Preco/tar.QtdePer');
    DModG.QrUpdPID1.SQL.Add('WHERE tar.Codigo=its.Codigo');
    DModG.QrUpdPID1.SQL.Add('AND its.Data=:P0');
    DModG.QrUpdPID1.SQL.Add('');
    DModG.QrUpdPID1.Params[0].AsString := Geral.FDT(TPAtuais.Date, 1);
    DModG.QrUpdPID1.ExecSQL;
    //
  end;
  TbTarifas.Close;
  TbTarifas.Open;
end;

end.
