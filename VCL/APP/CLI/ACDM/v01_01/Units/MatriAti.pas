unit MatriAti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkGeral, Variants,
  dmkDBEdit, dmkEdit, dmkEditCB, dmkDBLookupComboBox, UnDmkEnums;

type
  TFmMatriAti = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CBAtividade: TdmkDBLookupComboBox;
    DsAtividades: TDataSource;
    EdAtividade: TdmkEditCB;
    QrAtividades: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdConta: TdmkEdit;
    Label7: TLabel;
    DBEdit1: TdmkDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    QrAtividadesCurso: TIntegerField;
    QrAtividadesSala: TIntegerField;
    QrAtividadesProfessor: TIntegerField;
    QrAtividadesNOMESALA: TWideStringField;
    QrAtividadesNOMECURSO: TWideStringField;
    QrAtividadesNOMEPROFESSOR: TWideStringField;
    QrAtividadesControle: TIntegerField;
    SpeedButton1: TSpeedButton;
    Label5: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConta: Integer;
  end;

var
  FmMatriAti: TFmMatriAti;

implementation

uses UnMyObjects, Module, MyDBCheck, Turmas;

{$R *.DFM}

procedure TFmMatriAti.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriAti.BtConfirmaClick(Sender: TObject);
var
  Atividade: Integer;
begin
  Atividade := StrToInt(EdAtividade.Text);
  //
  if MyObjects.FIC(CBAtividade.KeyValue = 0, EdAtividade, 'Informe um per�odo!') then Exit;
  //
  FConta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'MatriAti', 'MatriAti', 'Codigo');
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO MatriAti SET ');
  Dmod.QrUpdM.SQL.Add('Codigo=:P0, Controle=:P1, Conta=:P2, Item=:P3, ');
  Dmod.QrUpdM.SQL.Add('Atividade=:P4');
  //
  Dmod.QrUpdM.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdM.Params[01].AsInteger := Geral.IMV(EdControle.Text);
  Dmod.QrUpdM.Params[02].AsInteger := Geral.IMV(EdConta.Text);
  Dmod.QrUpdM.Params[03].AsInteger := FConta;
  Dmod.QrUpdM.Params[04].AsInteger := Atividade;
  Dmod.QrUpdM.ExecSQL;
  Close;
end;

procedure TFmMatriAti.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmMatriAti.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  //

  if DBCheck.CriaFm(TFmTurmas, FmTurmas, afmoNegarComAviso) then
  begin
    FmTurmas.ShowModal;
    FmTurmas.Destroy;
{
    // N�o tem como saber a atividade?
    EdAtividade.ValueVariant := 0;
    CBAtividade.KeyValue := NULL;
}
    if VAR_CADASTRO2 <> 0 then
      UMyMod.SetaCodigoPesquisado(EdAtividade,
      CBAtividade, QrAtividades, VAR_CADASTRO2, 'Controle');
  end;
end;

procedure TFmMatriAti.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
