unit TarifasGru;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.Menus, dmkDBGrid;

type
  TFmTarifasGru = class(TForm)
    PainelDados: TPanel;
    DsTarifasGru: TDataSource;
    QrTarifasGru: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    PainelData: TPanel;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdCodigo: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    QrTarifasGruCodigo: TIntegerField;
    QrTarifasGruNome: TWideStringField;
    QrTarifas: TmySQLQuery;
    DsTarifas: TDataSource;
    DBGTarifas: TdmkDBGrid;
    DBGTarifasIts: TDBGrid;
    QrTarifasIts: TmySQLQuery;
    DsTarifasIts: TDataSource;
    QrTarifasCodigo: TIntegerField;
    QrTarifasNome: TWideStringField;
    QrTarifasAtiva: TSmallintField;
    QrTarifasTipoPer: TIntegerField;
    QrTarifasQtdePer: TIntegerField;
    QrTarifasItsCodigo: TIntegerField;
    QrTarifasItsData: TDateField;
    QrTarifasItsPreco: TFloatField;
    QrTarifasItsTaxa: TFloatField;
    QrTarifasItsValPer: TFloatField;
    QrTarifasTipoPer_TXT: TWideStringField;
    PMInclui: TPopupMenu;
    Grupodetarifas1: TMenuItem;
    Gerenciatarifas1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTarifasGruAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTarifasGruBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrTarifasGruBeforeClose(DataSet: TDataSet);
    procedure QrTarifasGruAfterScroll(DataSet: TDataSet);
    procedure QrTarifasAfterScroll(DataSet: TDataSet);
    procedure QrTarifasBeforeClose(DataSet: TDataSet);
    procedure QrTarifasCalcFields(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure DBGTarifasItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGTarifasDblClick(Sender: TObject);
    procedure Grupodetarifas1Click(Sender: TObject);
    procedure Gerenciatarifas1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenTarifasIts(Data: TDateTime);
    procedure CadastroDeTarifas();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenTarifas(Codigo: Integer);
  end;

var
  FmTarifasGru: TFmTarifasGru;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, TarifasNovas, MyDBCheck, Principal, UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTarifasGru.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTarifasGru.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTarifasGruCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTarifasGru.DefParams;
begin
  VAR_GOTOTABELA := 'tarifasgru';
  VAR_GOTOMYSQLTABLE := QrTarifasGru;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, Nome');
  VAR_SQLx.Add('FROM tarifasgru');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTarifasGru.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmTarifasGru.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTarifasGru.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTarifasGru.ReopenTarifas(Codigo: Integer);
begin
  QrTarifas.Close;
  QrTarifas.Params[0].AsInteger := QrTarifasGruCodigo.Value;
  QrTarifas.Open;
  //
  QrTarifas.Locate('Codigo', Codigo, []);
end;

procedure TFmTarifasGru.ReopenTarifasIts(Data: TDateTime);
begin
  QrTarifasIts.Close;
  QrTarifasIts.Params[0].AsInteger := QrTarifasCodigo.Value;
  QrTarifasIts.Open;
  //
  QrTarifasIts.Locate('Data', Data, []);
end;

procedure TFmTarifasGru.DBGTarifasDblClick(Sender: TObject);
begin
  CadastroDeTarifas;
end;

procedure TFmTarifasGru.DBGTarifasItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  AppPF.ConfiguraGradeTarifasPreco(QrTarifasIts, DBGTarifasIts, Column, Rect);
end;

procedure TFmTarifasGru.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTarifasGru.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTarifasGru.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTarifasGru.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTarifasGru.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTarifasGru.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTarifasGru, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'tarifasgru');
end;

procedure TFmTarifasGru.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO  := QrTarifasGruCodigo.Value;
  VAR_CADASTRO2 := QrTarifasCodigo.Value;
  Close;
end;

procedure TFmTarifasGru.CadastroDeTarifas;
var
  Grupo, Codigo: Integer;
begin
  Grupo  := QrTarifasGruCodigo.Value;
  Codigo := QrTarifasCodigo.Value;
  //
  FmPrincipal.CadastroDeTarifas(Codigo, Grupo);
  //
  LocCod(Grupo, Grupo);
  ReopenTarifas(Codigo);
end;

procedure TFmTarifasGru.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('tarifasgru', 'Codigo', LaTipo.SQLType,
    QrTarifasGruCodigo.Value);
  EdCodigo.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmTarifasGru, PainelEdit,
    'tarifasgru', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTarifasGru.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'tarifasgru', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tarifasgru', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'tarifasgru', 'Codigo');
end;

procedure TFmTarifasGru.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmTarifasGru.FormCreate(Sender: TObject);
begin
  DBGTarifasIts.Align := alClient;
  PainelEdit.Align    := alClient;
  CriaOForm;
end;

procedure TFmTarifasGru.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTarifasGruCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTarifasGru.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmTarifasGru.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTarifasGru.SbNovoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTarifasNovas, FmTarifasNovas, afmoNegarComAviso) then
  begin
    FmTarifasNovas.ShowModal;
    FmTarifasNovas.Destroy;
  end;
end;

procedure TFmTarifasGru.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmTarifasGru.QrTarifasAfterScroll(DataSet: TDataSet);
begin
  ReopenTarifasIts(0);
end;

procedure TFmTarifasGru.QrTarifasBeforeClose(DataSet: TDataSet);
begin
  QrTarifasIts.Close;
end;

procedure TFmTarifasGru.QrTarifasCalcFields(DataSet: TDataSet);
const
  Tipo: Array[0..2] of TColetivoUnidadeTempo = (cutMeses, cutSemanas, cutDias);
begin
  QrTarifasTipoPer_TXT.Value := Geral.ColetivoDeUnidadedeTempo(
    QrTarifasQtdePer.Value, Tipo[QrTarifasTipoPer.Value], dmkccFistUpper);
end;

procedure TFmTarifasGru.QrTarifasGruAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTarifasGru.QrTarifasGruAfterScroll(DataSet: TDataSet);
begin
  ReopenTarifas(0);
end;

procedure TFmTarifasGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTarifasGru.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTarifasGruCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tarifasgru', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTarifasGru.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTarifasGru.Gerenciatarifas1Click(Sender: TObject);
begin
  CadastroDeTarifas;
end;

procedure TFmTarifasGru.Grupodetarifas1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrTarifasGru, [PainelDados],
    [PainelEdita], EdNome, LaTipo, 'tarifasgru');
end;

procedure TFmTarifasGru.QrTarifasGruBeforeClose(DataSet: TDataSet);
begin
  QrTarifas.Close;
end;

procedure TFmTarifasGru.QrTarifasGruBeforeOpen(DataSet: TDataSet);
begin
  QrTarifasGruCodigo.DisplayFormat := FFormatFloat;
end;

end.

