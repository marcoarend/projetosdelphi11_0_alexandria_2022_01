unit ChamadasNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UnMLAGeral, ComCtrls,
  Grids, UMySQLModule, DBGrids, UnMsgInt, mySQLDbTables, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmChamadasNew = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    Label3: TLabel;
    Label4: TLabel;
    EdHoraI: TdmkEdit;
    EdIntervI: TdmkEdit;
    Label6: TLabel;
    EdIntervF: TdmkEdit;
    Label8: TLabel;
    EdHoraF: TdmkEdit;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label5: TLabel;
    EdTurma: TdmkEditCB;
    CBTurma: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdCurso: TdmkEditCB;
    CBCurso: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdSala: TdmkEditCB;
    CBSala: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdProfessor: TdmkEditCB;
    CBProfessor: TdmkDBLookupComboBox;
    QrCursos: TmySQLQuery;
    QrCursosCodigo: TIntegerField;
    QrCursosNome: TWideStringField;
    DsCursos: TDataSource;
    DsProfessores: TDataSource;
    QrProfessores: TmySQLQuery;
    QrProfessoresCodigo: TIntegerField;
    QrProfessoresNome: TWideStringField;
    QrSalas: TmySQLQuery;
    QrSalasCodigo: TIntegerField;
    QrSalasNome: TWideStringField;
    DsSalas: TDataSource;
    TPData: TDateTimePicker;
    Label1: TLabel;
    QrTurmas: TmySQLQuery;
    DsTurmas: TDataSource;
    QrTurmasCodigo: TIntegerField;
    QrTurmasNome: TWideStringField;
    Label2: TLabel;
    EdTempo: TdmkEdit;
    QrAlunos: TmySQLQuery;
    QrAlunosMATRICULA: TIntegerField;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdHoraIExit(Sender: TObject);
    procedure EdIntervIExit(Sender: TObject);
    procedure EdIntervFExit(Sender: TObject);
    procedure EdHoraFExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaTempo;
    function AlunosDaTurma: Integer;
  public
    FCodigo: Integer;
    FConta: Integer;
    { Public declarations }
  end;

var
  FmChamadasNew: TFmChamadasNew;
  Fechar : Boolean;

implementation

uses UnMyObjects, Module;


{$R *.DFM}

procedure TFmChamadasNew.BtConfirmaClick(Sender: TObject);
var
  Tur, Cur, Sal, Pro: Integer;
  HoraI, HoraF, IntervI, IntervF: String;
  Presenca: Integer;
begin
  if AlunosDaTurma < 1 then Exit;
  Tur := Geral.IMV(EdTurma.Text);
  Cur := Geral.IMV(EdCurso.Text);
  Sal := Geral.IMV(EdSala.Text);
  Pro := Geral.IMV(EdProfessor.Text);
  //
  if MyObjects.FIC(Tur < 1, EdTurma, 'Defina uma turma!') then Exit;
  if MyObjects.FIC(Cur < 1, EdCurso, 'Defina um curso!') then Exit;
  if MyObjects.FIC(Sal < 1, EdSala, 'Defina uma sala!') then Exit;
  if MyObjects.FIC(Pro < 1, EdProfessor, 'Defina um professor!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    if EdHoraI.Text   = CO_VAZIO then EdHoraI.Text   := '00:00';
    if EdHoraF.Text   = CO_VAZIO then EdHoraF.Text   := '00:00';
    if EdIntervI.Text = CO_VAZIO then EdIntervI.Text := '00:00';
    if EdIntervF.Text = CO_VAZIO then EdIntervF.Text := '00:00';
    //
    HoraI   := FormatDateTime(VAR_FORMATTIME, StrToDateTime(EdHoraI.Text));
    HoraF   := FormatDateTime(VAR_FORMATTIME, StrToDateTime(EdHoraF.Text));
    IntervI := FormatDateTime(VAR_FORMATTIME, StrToDateTime(EdIntervI.Text));
    IntervF := FormatDateTime(VAR_FORMATTIME, StrToDateTime(EdIntervF.Text));
    Presenca := MLAGeral.TTM(EdTempo.Text);
    //
    FCodigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Chamadas', 'Chamadas', 'Codigo');
    //////////////////////////////////////////////////////////////////////////////
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO Chamadas SET ');
    Dmod.QrUpd.SQL.Add('  Turma      =:P00');
    Dmod.QrUpd.SQL.Add(', Curso      =:P01');
    Dmod.QrUpd.SQL.Add(', Sala       =:P02');
    Dmod.QrUpd.SQL.Add(', Professor  =:P03');
    Dmod.QrUpd.SQL.Add(', Data       =:P04');
    Dmod.QrUpd.SQL.Add(', HoraI      =:P05');
    Dmod.QrUpd.SQL.Add(', HoraF      =:P06');
    Dmod.QrUpd.SQL.Add(', IntervI    =:P07');
    Dmod.QrUpd.SQL.Add(', IntervF    =:P08');
    Dmod.QrUpd.SQL.Add(', Carga      =:P09');
    Dmod.QrUpd.SQL.Add(', Codigo     =:P10');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Tur;
    Dmod.QrUpd.Params[01].AsInteger := Cur;
    Dmod.QrUpd.Params[02].AsInteger := Sal;
    Dmod.QrUpd.Params[03].AsInteger := Pro;
    Dmod.QrUpd.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
    Dmod.QrUpd.Params[05].AsString  := HoraI;
    Dmod.QrUpd.Params[06].AsString  := HoraF;
    Dmod.QrUpd.Params[07].AsString  := IntervI;
    Dmod.QrUpd.Params[08].AsString  := IntervF;
    Dmod.QrUpd.Params[09].AsInteger := Presenca;
    Dmod.QrUpd.Params[10].AsInteger := FCodigo;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ChamadasIts SET ');
    Dmod.QrUpd.SQL.Add('  Matricula   =:P00');
    Dmod.QrUpd.SQL.Add(', Presenca    =:P01');
    Dmod.QrUpd.SQL.Add(', Codigo      =:P02');
    Dmod.QrUpd.SQL.Add('');
    QrAlunos.First;
    while not QrAlunos.Eof do
    begin
      Dmod.QrUpd.Params[00].AsInteger := QrAlunosMATRICULA.Value;
      Dmod.QrUpd.Params[01].AsInteger := Presenca;
      Dmod.QrUpd.Params[02].AsInteger := FCodigo;
      Dmod.QrUpd.ExecSQL;
      QrAlunos.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmChamadasNew.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChamadasNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdHoraI.SetFocus;
  CalculaTempo;
end;

procedure TFmChamadasNew.EdHoraIExit(Sender: TObject);
begin
  EdHoraI.Text := MLAGeral.THT(EdHoraI.Text);
  CalculaTempo;
end;

procedure TFmChamadasNew.EdIntervIExit(Sender: TObject);
begin
  EdIntervI.Text := MLAGeral.THT(EdIntervI.Text);
  CalculaTempo;
end;

procedure TFmChamadasNew.EdIntervFExit(Sender: TObject);
begin
  EdIntervF.Text := MLAGeral.THT(EdIntervF.Text);
  CalculaTempo;
end;

procedure TFmChamadasNew.EdHoraFExit(Sender: TObject);
begin
  EdHoraF.Text := MLAGeral.THT(EdHoraF.Text);
  CalculaTempo;
end;

procedure TFmChamadasNew.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmChamadasNew.CalculaTempo;
begin
  EdTempo.Text := MLAGeral.HTT(
  MLAGeral.PTH(EdHoraI.Text, EdIntervI.Text, EdIntervF.Text, EdHoraF.Text),
  4, True);
end;

procedure TFmChamadasNew.FormCreate(Sender: TObject);
begin
  QrTurmas.Open;
  QrCursos.Open;
  QrSalas.Open;
  QrProfessores.Open;
end;

function TFmChamadasNew.AlunosDaTurma: Integer;
begin
  QrAlunos.Close;
  QrAlunos.Params[0].AsInteger := Geral.IMV(EdTurma.Text);
  QrAlunos.Open;
  if QrAlunos.RecordCount < 1 then
    Geral.MB_Aviso('Esta turma n�o tem alunos!');
  Result := QrAlunos.RecordCount;
end;

end.
