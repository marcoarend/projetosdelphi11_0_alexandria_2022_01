unit TurmasTmp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkGeral, Variants,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, UnDmkEnums;

type
  TFmTurmasTmp = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CBPeriodo: TdmkDBLookupComboBox;
    DsPeriodos: TDataSource;
    EdPeriodo: TdmkEditCB;
    QrPeriodos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrPeriodosCodigo: TIntegerField;
    QrPeriodosNome: TWideStringField;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdControle: TdmkEdit;
    SpeedButton5: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmTurmasTmp: TFmTurmasTmp;

implementation

uses UnMyObjects, Module, MyDBCheck, Principal;

{$R *.DFM}

procedure TFmTurmasTmp.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTurmasTmp.BtConfirmaClick(Sender: TObject);
var
  Conta, Periodo: Integer;
begin
  Periodo := StrToInt(EdPeriodo.Text);
  //
  if MyObjects.FIC(CBPeriodo.KeyValue = 0, EdPeriodo, 'Informe um per�odo!') then Exit;
  //
  Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
             'TurmasTmp', 'TurmasTmp', 'Codigo');
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO TurmasTmp SET ');
  Dmod.QrUpdM.SQL.Add('Codigo=:P0, Controle=:P1, Conta=:P2, Periodo=:P3');
  //
  Dmod.QrUpdM.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdM.Params[01].AsInteger := Geral.IMV(EdControle.Text);
  Dmod.QrUpdM.Params[02].AsInteger := Conta;
  Dmod.QrUpdM.Params[03].AsInteger := Periodo;
  Dmod.QrUpdM.ExecSQL;
  Close;
end;

procedure TFmTurmasTmp.FormCreate(Sender: TObject);
begin
  QrPeriodos.Open;
end;

procedure TFmTurmasTmp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmTurmasTmp.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroDePeriodos(EdPeriodo.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPeriodo, CBPeriodo, QrPeriodos, VAR_CADASTRO);
    EdPeriodo.SetFocus;
  end;
end;

procedure TFmTurmasTmp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
