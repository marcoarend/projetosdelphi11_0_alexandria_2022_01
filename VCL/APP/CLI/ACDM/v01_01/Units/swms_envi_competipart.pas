unit swms_envi_competipart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, DBCtrls, DmkDAC_PF, 
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, ABSMain, AdvGlowButton, dmkImage, dmkRadioGroup,
  ComCtrls, ToolWin, mySQLDbTables, UnDmkEnums;

type
  TFmSWMS_Envi_Competipart = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    EdNadador: TdmkEditCB;
    CBNadador: TdmkDBLookupComboBox;
    SBNadador: TSpeedButton;
    QrNadadores: TmySQLQuery;
    DsNadadores: TDataSource;
    PnTitulo: TPanel;
    RGEstilo: TdmkRadioGroup;
    RGMetros: TdmkRadioGroup;
    EdControle: TdmkEdit;
    Label2: TLabel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox4: TGroupBox;
    Label6: TLabel;
    EdTempo: TdmkEdit;
    GBRodaPe: TGroupBox;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox5: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RGCategoria: TdmkRadioGroup;
    QrNadadoresCodigo: TIntegerField;
    QrNadadoresCodTxt: TWideStringField;
    QrNadadoresNome: TWideStringField;
    QrNadadoresNascim: TDateField;
    QrNadadoresSexo: TWideStringField;
    QrNadadoresCPF: TWideStringField;
    QrNadadoresFone: TWideStringField;
    EdImportado: TdmkEdit;
    EdNivel1: TdmkEdit;
    EdNivel2: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdNadadorChange(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraCategoria();
  public
    { Public declarations }
    procedure ReopenSWMS_Envi_Nadadores();
  end;

  var
  FmSWMS_Envi_Competipart: TFmSWMS_Envi_Competipart;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, SWMS_Mtng_Cab,
  UnInternalConsts, MyDBCheck, UnitListas;

{$R *.DFM}

procedure TFmSWMS_Envi_Competipart.BtOKClick(Sender: TObject);
var
  Controle: Integer;
  Duplicado: Variant;
  Pode: Boolean;
begin
  if MyObjects.FIC(EdNadador.ValueVariant = 0, EdNadador,
    'Informe o participante') then
    Exit;
  if MyObjects.FIC(RGEstilo.ItemIndex < 1, RGEstilo, 'Informe o estilo') then
    Exit;
  if MyObjects.FIC(RGMetros.ItemIndex < 1, RGMetros, 'Informe a dist�ncia') then
    Exit;

  Pode :=
  (RGCategoria.ItemIndex >= FmSWMS_Mtng_Cab.QrSWMS_Mtng_CabCategoriaIni.Value)
  and
  (RGCategoria.ItemIndex <= FmSWMS_Mtng_Cab.QrSWMS_Mtng_CabCategoriaFim.Value);
  if MyObjects.FIC(Pode = False, nil, 'Categoria n�o participa da competi��o!') then
    Exit;
  //
  Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'swms_envi_competipart', 'Controle',
  [], [], ImgTipo.SQLType, FmSWMS_Mtng_Cab.QrCompetiPartControle.Value,
  siPositivo, EdControle);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'swms_envi_competipart',
  Controle, Dmod.QrUpd) then
  begin
    FmSWMS_Mtng_Cab.ReopenSWMS_Envi_CompetiPart(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdNadador.ValueVariant    := 0;
      CBNadador.KeyValue        := Null;
      RGEstilo.ItemIndex        := 0;
      RGMetros.ItemIndex        := 0;
      EdNadador.SetFocus;
    end else Close;
  end;
end;

procedure TFmSWMS_Envi_Competipart.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSWMS_Envi_Competipart.ConfiguraCategoria();
begin
  RGCategoria.ItemIndex :=
  UnListas.ConfiguraCategoria(FmSWMS_Mtng_Cab.QrSWMS_Mtng_CabDataEvenIni.Value,
    QrNadadoresNascim.Value, FmSWMS_Mtng_Cab.QrSWMS_Mtng_CabCategoriaIni.Value);
end;

procedure TFmSWMS_Envi_Competipart.EdNadadorChange(Sender: TObject);
begin
  ConfiguraCategoria();
end;

procedure TFmSWMS_Envi_Competipart.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSWMS_Envi_Competipart.FormCreate(Sender: TObject);
begin
  MyObjects.ConfiguraRadioGroup(RGEstilo, sListaEstilosNatacao, 4, 0);
  MyObjects.ConfiguraRadioGroup(RGMetros, sListaMetrosNatacao, 6, 0);
  MyObjects.ConfiguraRadioGroup(RGCategoria, sListaCategoriasNatacao, 6, 0);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
end;

procedure TFmSWMS_Envi_Competipart.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 10, 10, 20);
end;

procedure TFmSWMS_Envi_Competipart.ReopenSWMS_Envi_Nadadores();
var
  Cod_TXT, Ctr_TXT: String;
begin
  Cod_TXT := FormatFloat('0', FmSWMS_Mtng_Cab.QrSWMS_Mtng_ItsCodigo.Value);
  Ctr_TXT := FormatFloat('0', FmSWMS_Mtng_Cab.QrSWMS_Mtng_ItsControle.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNadadores, Dmod.MyDB, [
  'SELECT * ',
  'FROM swms_envi_nadadores ',
  'WHERE Nivel1=' + Cod_TXT,
  'AND Nivel2=' + Ctr_TXT
  ]);
  //
  QrNadadores.Locate('Codigo', EdNadador.ValueVariant, []);
end;

end.
