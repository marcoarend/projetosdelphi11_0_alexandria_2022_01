unit SWMS_Mtng_PrvMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkRadioGroup, UnDmkEnums;

type
  TFmSWMS_Mtng_PrvMul = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    EdControle: TdmkEdit;
    GroupBox4: TGroupBox;
    CGMetros: TdmkCheckGroup;
    CkContinuar: TCheckBox;
    GroupBox5: TGroupBox;
    EdPiscDescr: TdmkEdit;
    Label1: TLabel;
    EdPiscCompr: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdPiscRaias: TdmkEdit;
    Panel5: TPanel;
    CGEstilo: TdmkCheckGroup;
    RGPiorRaia: TdmkRadioGroup;
    QrMetros: TmySQLQuery;
    DsMetros: TDataSource;
    QrMetrosCodigo: TIntegerField;
    QrMetrosNome: TWideStringField;
    QrMetrosAtivo: TSmallintField;
    QrEstilo: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    SmallintField1: TSmallintField;
    DsEstilo: TDataSource;
    QrCateg1: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    SmallintField2: TSmallintField;
    DsCateg1: TDataSource;
    QrCateg2: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    SmallintField3: TSmallintField;
    DsCateg2: TDataSource;
    RGSexo: TRadioGroup;
    GroupBox6: TGroupBox;
    Panel6: TPanel;
    Label27: TLabel;
    Label28: TLabel;
    EdCategoriaIni: TdmkEdit;
    EdCategoriaIni_TXT: TdmkEdit;
    EdCategoriaFim: TdmkEdit;
    EdCategoriaFim_TXT: TdmkEdit;
    RGForma: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCategoriaIniChange(Sender: TObject);
    procedure EdCategoriaFimChange(Sender: TObject);
    procedure EdCategoriaFim_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaIni_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSexoChange(Sender: TObject);
    procedure CGMetrosClick(Sender: TObject);
    procedure CGEstiloClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmSWMS_Mtng_PrvMul: TFmSWMS_Mtng_PrvMul;

implementation

uses SWMS_Mtng_Cab, Module, Principal, UnMyObjects, UMySQLModule, UnitListas,
UCreate, ModuleGeral;

{$R *.DFM}

procedure TFmSWMS_Mtng_PrvMul.BtOKClick(Sender: TObject);
  procedure InsereProvas(Metros, Estilo, CategoriaIni, CategoriaFim: Integer);
    procedure InsereProvaSexo(Metros, Estilo, CategoriaIni, CategoriaFim:
    Integer; Sexo: String);
    const
      CodTxt = '';
    var
      Codigo, Controle, PiscCompr, PiscRaias, PiorRaia: Integer;
      PiscDescr: String;
    begin
      Codigo := FmSWMS_Mtng_Cab.QrSWMS_Mtng_CabCodigo.Value;
      PiscDescr := EdPiscDescr.Text;
      PiscCompr := EdPiscCompr.ValueVariant;
      PiscRaias := EdPiscRaias.ValueVariant;
      PiorRaia := RGPiorRaia.ItemIndex;
      //
      Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'swms_mtng_prv', 'Controle',
      [], [], ImgTipo.SQLType, FMSWMS_Mtng_Cab.QrSWMS_Mtng_PrvControle.Value,
      siPositivo, EdControle);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'swms_mtng_prv', False, [
      'Codigo', 'CodTxt', 'Estilo',
      'Metros', 'Sexo', 'PiscDescr',
      'PiscCompr', 'PiscRaias', 'CategoriaIni',
      'CategoriaFim', 'PiorRaia'], [
      'Controle'], [
      Codigo, CodTxt, Estilo,
      Metros, Sexo, PiscDescr,
      PiscCompr, PiscRaias, CategoriaIni,
      CategoriaFim, PiorRaia], [
      Controle], True);
    end;
  begin
    case RGSexo.Itemindex of
      1: InsereProvaSexo(Metros, Estilo, CategoriaIni, CategoriaFim, 'F');
      2: InsereProvaSexo(Metros, Estilo, CategoriaIni, CategoriaFim, 'M');
      3: InsereProvaSexo(Metros, Estilo, CategoriaIni, CategoriaFim, 'A');
      4:
      begin
        InsereProvaSexo(Metros, Estilo, CategoriaIni, CategoriaFim, 'F');
        InsereProvaSexo(Metros, Estilo, CategoriaIni, CategoriaFim, 'M');
      end;
    end;
  end;
const
  tag = ' x ';
var
  p, m, d, n, k, I, J, L, Ini, Fim: Integer;
  x: String;
begin
(*
  if MyObjects.FIC(Trim(EdCodTxt.Text) = '', EdCodTxt,
    'Informe o c�digo da prova') then
    Exit;
*)
  if MyObjects.FIC((RGSexo.ItemIndex < 1), RGSexo, 'Informe o(s) sexo(s)!') then
    Exit;
  if MyObjects.FIC(CGMetros.Value = 0, CGMetros,
    'Informe pelo menos uma dist�ncia!') then
    Exit;
  if MyObjects.FIC(CGEstilo.Value = 0, CGEstilo,
    'Informe peloo menos um estilo!') then
    Exit;
  if MyObjects.FIC(Trim(EdPiscDescr.Text) = '', EdPiscDescr,
    'Informe a descri��o da piscina') then
    Exit;
  d := EdPiscCompr.ValueVariant;
  if MyObjects.FIC(d < 1, EdPiscCompr,
    'Informe o comprimento em metros da piscina!') then
    Exit;
  if MyObjects.FIC(EdPiscRaias.ValueVariant < 1, EdPiscRaias,
    'Informe a quantidade de raias da piscina!') then
    Exit;
  if MyObjects.FIC(EdCategoriaIni.ValueVariant < 1, EdCategoriaIni,
    'Informe a categoria inicial!') then
    Exit;
  if MyObjects.FIC(RGPiorRaia.ItemIndex < 0, RGPiorRaia,
    'Informe a pior raia da piscina!') then
    Exit;
  if MyObjects.FIC(EdCategoriaFim.ValueVariant < 1, EdCategoriaFim,
    'Informe a categoria final!') then
    Exit;
  if MyObjects.FIC(EdCategoriaFim.ValueVariant < EdCategoriaIni.ValueVariant, EdCategoriaFim,
    'A categoria final deve ser igual ou superior � inicial!') then
    Exit;
  //
  k := 2;
  for I := 1 to CGMetros.Items.Count - 1 do
  begin
    if Geral.IntInConjunto(k, CGMetros.Value) then
    begin
      x := CGMetros.Items[I];
      p := pos(tag, x);
      if p > 0 then
      begin
        x := Copy(x, p + Length(tag));
        m := Geral.IMV(Trim(x));
        if m > 0 then
        begin
          n := m mod Trunc(d);
          if MyObjects.FIC(n <> 0, nil,
          'Comprimento da piscina n�o comporta a dist�ncia de ' +
            CGMetros.Items[I] + ' metros!') then
          Exit;
        end;
      end;
    end;
    k := k + k;
  end;

  //
  Screen.Cursor := crHourGlass;
  try
    for I := 1 to CGMetros.Items.Count - 1 do
    begin
      if CGMetros.Checked[I] then
      begin
        for J := 1 to CGEstilo.Items.Count - 1 do
        begin
          if CGEstilo.Checked[J] then
          begin
            Ini := EdCategoriaIni.ValueVariant;
            Fim := EdCategoriaFim.ValueVariant;
            if RGForma.ItemIndex = 1 then
            begin
              InsereProvas(I, J, Ini, Fim);
            end else
            begin
              for L := Ini to Fim do
                InsereProvas(I, J, L, L);
            end;
          end;
        end;
      end;
    end;

    MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_SQL);

    //
    FMSWMS_Mtng_Cab.ReopenSWMS_Mtng_Prv(0);
    FMSWMS_Mtng_Cab.QrSWMS_Mtng_Prv.Last;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      //EdCodTxt.ValueVariant     := '';
      RGSexo.ItemIndex          := 0;
      CGEstilo.Value            := 0;
      CGMetros.Value            := 0;
      EdCategoriaIni.ValueVariant := 0;
      EdCategoriaFim.ValueVariant := 0;
      RGSexo.SetFocus;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSWMS_Mtng_PrvMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSWMS_Mtng_PrvMul.CGEstiloClick(Sender: TObject);
begin
  if CGEstilo.ItemIndex = 0 then
  begin
    if Geral.IntInConjunto(1, CGEstilo.Value) then
      CGEstilo.SetMaxValue
    else
      CGEstilo.Value := 0;
  end;
end;

procedure TFmSWMS_Mtng_PrvMul.CGMetrosClick(Sender: TObject);
begin
  if CGMetros.ItemIndex = 0 then
  begin
    if Geral.IntInConjunto(1, CGMetros.Value) then
      CGMetros.SetMaxValue
    else
      CGMetros.Value := 0;
  end;
end;

procedure TFmSWMS_Mtng_PrvMul.EdCategoriaFimChange(Sender: TObject);
begin
  EdCategoriaFim_TXT.Text :=
    UnListas.CategoriasNatacao_Get(Geral.IMV(EdCategoriaFim.Text));
end;

procedure TFmSWMS_Mtng_PrvMul.EdCategoriaFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaFim.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvMul.EdCategoriaFim_TXTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaFim.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvMul.EdCategoriaIniChange(Sender: TObject);
begin
  EdCategoriaIni_TXT.Text :=
    UnListas.CategoriasNatacao_Get(Geral.IMV(EdCategoriaIni.Text));
end;

procedure TFmSWMS_Mtng_PrvMul.EdCategoriaIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaIni.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvMul.EdCategoriaIni_TXTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaIni.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvMul.EdSexoChange(Sender: TObject);
begin
{
  if EdSexo.Text = 'A' then
    EdSexo_Txt.Text := 'Ambos'
  else
  if EdSexo.Text = 'F' then
    EdSexo_Txt.Text := 'Feminino'
  else
  if EdSexo.Text = 'M' then
    EdSexo_Txt.Text := 'Masculino'
  else
    EdSexo_Txt.Text := '? ? ? ? ?';
}
end;

procedure TFmSWMS_Mtng_PrvMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
end;

procedure TFmSWMS_Mtng_PrvMul.FormCreate(Sender: TObject);
begin
  MyObjects.ConfiguraCheckGroup(CGEstilo, sListaEstilosNatacao, 4, 0, True);
  MyObjects.ConfiguraCheckGroup(CGMetros, sListaMetrosNatacao, 6, 0, True);
  //MyObjects.ConfiguraRadioGroup(RGCategoria, sListaCategoriasNatacao, 6, 0);
  //
  UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1, False, 1, '_metros_');
  UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1, False, 1, '_estilo_');
  UCriar.RecriaTempTableNovo(ntrttPesqESel, DModG.QrUpdPID1, False, 1, '_catego_');
end;

procedure TFmSWMS_Mtng_PrvMul.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
