unit UnitListas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral;


const

  sListaSexos: array[0..3] of String = ('N', 'M', 'F', 'A');

  sListaEstilosNatacao: array[0..7] of String = (
'Nenhum',
'Livre',
'Costas',
'Peito',
'Borboleta',
'Medley',
'Revezam. livre',
'Revezam. estilos'
);

  sListaMetrosNatacao: array[0..17] of String = (
'0',
'1 x 20',
'1 x 25',
'1 x 40',
'1 x 50',
'1 x 80',
'1 x 100',
'1 x 200',
'1 x 400',
'1 x 800',
'1 x 1500',
'4 x 20',
'4 x 25',
'4 x 40',
'4 x 50',
'4 x 80',
'4 x 100',
'4 x 200'
);

  sListaMetro2Natacao: array[0..17] of String = (
'0',
'20',
'25',
'40',
'50',
'80',
'100',
'200',
'400',
'800',
'1500',
'4 x 20',
'4 x 25',
'4 x 40',
'4 x 50',
'4 x 80',
'4 x 100',
'4 x 200'
);

  sListaCategoriasNatacao: array[0..29] of String = (
'Nenhum',
'1',
'2',
'3',
'4',
'Mamadeira',
'Pr�-fraldinha',
'Fraldinha',
'Pr�-mirim',
'Mirin I',
'Mirin II',
'Petiz I',
'Petiz II',
'Infantil I',
'Infantil II',
'Juvenil I',
'Juvenil II',
'Junior I',
'Junior II',
'Senior',
'Pr�-master A',
'Master B',
'Master C',
'Master D',
'Master E',
'Master F',
'Master G',
'Master H',
'Master I',
'A definir'
);


type
  TUnitListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    //
    function ArrayToTexto(Campo, NO_Campo: String; PosVirgula: TPosVirgula;
             IniZero: Boolean; MyArr: array of String): String;
    //
    function CategoriasNatacao_Get(Codigo: Integer): String;
    function CategoriasNatacao_ItensLista(): MyArrayLista;
    function CategoriasNatacao_MontaLista(): String;
    //
    function ConfiguraCategoria(DataI_Even, Nascim_Atleta: TDateTime;
             CategIni_Even: Integer): Integer;
  end;

var
  UnListas: TUnitListas;

const
  PiorAMelhorRaias02F: array[0..01] of Integer = (0,1);
  PiorAMelhorRaias02L: array[0..01] of Integer = (1,0);
  PiorAMelhorRaias03F: array[0..02] of Integer = (0,2,1);
  PiorAMelhorRaias03L: array[0..02] of Integer = (1,2,0);

  PiorAMelhorRaias04F: array[0..03] of Integer = (0,3,1,2);
  PiorAMelhorRaias04L: array[0..03] of Integer = (3,0,2,1);
  PiorAMelhorRaias05F: array[0..04] of Integer = (0,4,1,3,2);
  PiorAMelhorRaias05L: array[0..04] of Integer = (4,0,3,1,2);

  PiorAMelhorRaias06F: array[0..05] of Integer = (0,5,1,4,2,3);
  PiorAMelhorRaias06L: array[0..05] of Integer = (5,0,4,1,3,2);
  PiorAMelhorRaias07F: array[0..06] of Integer = (0,6,1,5,2,4,3);
  PiorAMelhorRaias07L: array[0..06] of Integer = (6,0,5,1,4,2,3);

  PiorAMelhorRaias08F: array[0..07] of Integer = (0,7,1,6,2,5,3,4);
  PiorAMelhorRaias08L: array[0..07] of Integer = (7,0,6,1,5,2,4,3);
  PiorAMelhorRaias09F: array[0..08] of Integer = (0,8,1,7,2,6,3,5,4);
  PiorAMelhorRaias09L: array[0..08] of Integer = (8,0,7,1,6,2,5,3,4);

  PiorAMelhorRaias10F: array[0..09] of Integer = (0,9,1,8,2,7,3,6,4,5);
  PiorAMelhorRaias10L: array[0..09] of Integer = (9,0,8,1,7,2,6,3,5,4);
  PiorAMelhorRaias11F: array[0..10] of Integer = (0,10,1,9,2,8,3,7,4,6,5);
  PiorAMelhorRaias11L: array[0..10] of Integer = (10,0,9,1,8,2,7,3,6,4,5);

  PiorAMelhorRaias12F: array[0..11] of Integer = (0,11,1,10,2,9,3,8,4,7,5,6);
  PiorAMelhorRaias12L: array[0..11] of Integer = (11,0,10,1,9,2,8,3,7,4,6,5);
  PiorAMelhorRaias13F: array[0..12] of Integer = (0,12,1,11,2,10,3,9,4,8,5,7,6);
  PiorAMelhorRaias13L: array[0..12] of Integer = (12,0,11,1,10,2,9,3,8,4,7,5,6);

  PiorAMelhorRaias14F: array[0..13] of Integer = (0,13,1,12,2,11,3,10,4,9,5,8,6,7);
  PiorAMelhorRaias14L: array[0..13] of Integer = (13,0,12,1,11,2,10,3,9,4,8,5,7,6);
  PiorAMelhorRaias15F: array[0..14] of Integer = (0,14,1,13,2,12,3,11,4,10,5,9,6,8,7);
  PiorAMelhorRaias15L: array[0..14] of Integer = (14,0,13,1,12,2,11,3,10,4,9,5,8,6,7);

  PiorAMelhorRaias16F: array[0..15] of Integer = (0,15,1,14,2,13,3,12,4,11,5,10,6,9,7,8);
  PiorAMelhorRaias16L: array[0..15] of Integer = (15,0,14,1,13,2,12,3,11,4,10,5,9,6,8,7);
  PiorAMelhorRaias17F: array[0..16] of Integer = (0,16,1,15,2,14,3,13,4,12,5,11,6,10,7,9,8);
  PiorAMelhorRaias17L: array[0..16] of Integer = (16,0,15,1,14,2,13,3,12,4,11,5,10,6,9,7,8);

implementation

uses UnMyObjects, SelOnStringGrid;

{ TUnitListas }

function TUnitListas.ArrayToTexto(Campo, NO_Campo: String; PosVirgula:
TPosVirgula; IniZero: Boolean; MyArr: array of String): String;
var
  I: Integer;
begin
{
ELT(Estilo+1, "Teste0", "teste1", "Teste 2", "Testet3", "Testet4") NO_ESTILO
}
  Result := ' ELT(' + Campo;
  if IniZero then
    Result := Result + ' + 1,';
  for I := Low(MyArr) to High(MyArr) do
  begin
    Result := Result + '"' + MyArr[I];
    if I = High(MyArr) then
      Result := Result + '") ' + NO_Campo
    else
      Result := Result + '",'
  end;
  case PosVirgula of
    pvNo: ;
    pvPre: Result := ',' + Result;
    pvPos: Result := Result + ',';
  end;
end;

function TUnitListas.CategoriasNatacao_Get(Codigo: Integer): String;
begin
// C�digo do Regime Tribut�rio
  Result := sListaCategoriasNatacao[Codigo];
{
  case Codigo of
    1: Result := '';
    2: Result := '';
    3: Result := '';
    else Result := '';
  end;
}
end;

function TUnitListas.CategoriasNatacao_ItensLista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha, I: Integer;
begin
  SetLength(Result, 0);
  Linha := 0;
  //
  for I := Low(sListaCategoriasNatacao) to High(sListaCategoriasNatacao) do
    AddLinha(Result, Linha, FormatFloat('0', I), sListaCategoriasNatacao[I]);
  //AddLinha(Result, I, '2', 'Simples Nacional - excesso de sublimite de receita bruta.');
  //AddLinha(Result, I, '3', 'Regime Normal.');
end;

function TUnitListas.CategoriasNatacao_MontaLista: String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  // N�o pode, pois o  SWMS n�o tem MyDBCheck
  //if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  if MyObjects.CriaForm_AcessoTotal(TFmSelOnStringGrid, FmSelOnStringGrid)  then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 44;
      Grade.ColWidths[02] := 715;
      Width := 800;
      FColSel := 1;
      Caption := 'CRT';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CategoriasNatacao_ItensLista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnitListas.ConfiguraCategoria(DataI_Even, Nascim_Atleta: TDateTime;
CategIni_Even: Integer): Integer;
var
  Ano, Mes, Dia: Word;
  AAtu, AAlu, Anos, Idade20: Integer;
  Fez: Boolean;
begin
  DecodeDate(DataI_Even, Ano, Mes, Dia);
  AAtu := Ano;
  DecodeDate(Nascim_Atleta, Ano, Mes, Dia);
  AAlu := Ano;
  Anos := Trunc(DataI_Even - Nascim_Atleta);
  //Fez := Anos = AAtu - AAlu; // Ver se precisa antes de eliminar!
  //
  if CategIni_Even < 18 then
    Idade20 := 19
  else
    Idade20 := 20;
  if Anos > 20 then
    Anos := AAtu - AAlu;
  case Anos of
    00..18: Result := Anos;
        19: Result := Anos -1;
        20: Result := Idade20;//29;
    21..24: Result := 20;
    25..29: Result := 21;
    30..34: Result := 22;
    35..39: Result := 23;
    40..44: Result := 24;
    45..49: Result := 25;
    50..54: Result := 26;
    55..59: Result := 27;
    else Result := 28;
  end;
end;

end.
