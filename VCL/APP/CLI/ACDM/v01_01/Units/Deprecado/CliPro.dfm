object FmCliPro: TFmCliPro
  Left = 370
  Top = 246
  Caption = 'MER-VENDA-001 :: Venda de Produtos'
  ClientHeight = 296
  ClientWidth = 598
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 598
    Height = 208
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 598
      Height = 92
      Align = alTop
      TabOrder = 1
      object Label3: TLabel
        Left = 12
        Top = 8
        Width = 52
        Height = 13
        Caption = 'Atendente:'
      end
      object Label5: TLabel
        Left = 12
        Top = 48
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 472
        Top = 8
        Width = 37
        Height = 13
        Caption = 'Recibo:'
      end
      object Label6: TLabel
        Left = 472
        Top = 48
        Width = 26
        Height = 13
        Caption = 'D&ata:'
        FocusControl = TPDataCad
      end
      object EdAtendeteCod: TdmkEdit
        Left = 12
        Top = 24
        Width = 65
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        MaxLength = 8
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdResponsavelCod: TdmkEdit
        Left = 12
        Top = 64
        Width = 65
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        MaxLength = 8
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdResponsavelNome: TdmkEdit
        Left = 80
        Top = 64
        Width = 388
        Height = 21
        TabStop = False
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdAtendenteNome: TdmkEdit
        Left = 80
        Top = 24
        Width = 388
        Height = 21
        TabStop = False
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRecibo: TdmkEdit
        Left = 472
        Top = 24
        Width = 112
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        MaxLength = 8
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPDataCad: TdmkEditDateTimePicker
        Left = 472
        Top = 64
        Width = 112
        Height = 21
        Date = 38541.731161747710000000
        Time = 38541.731161747710000000
        TabOrder = 5
        TabStop = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 92
      Width = 598
      Height = 116
      Align = alClient
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 77
        Height = 13
        Caption = 'Mercadoria [F4]:'
      end
      object Label7: TLabel
        Left = 12
        Top = 48
        Width = 42
        Height = 13
        Caption = 'Estoque:'
        FocusControl = DBEdit2
      end
      object Label2: TLabel
        Left = 88
        Top = 48
        Width = 40
        Height = 13
        Caption = '$ Pre'#231'o:'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 168
        Top = 48
        Width = 54
        Height = 13
        Caption = '$ Sub-total:'
      end
      object Label15: TLabel
        Left = 248
        Top = 48
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label11: TLabel
        Left = 328
        Top = 48
        Width = 60
        Height = 13
        Caption = '% Desconto:'
      end
      object Label8: TLabel
        Left = 412
        Top = 48
        Width = 58
        Height = 13
        Caption = '$ Desconto:'
      end
      object Label10: TLabel
        Left = 496
        Top = 48
        Width = 36
        Height = 13
        Caption = '$ Total:'
      end
      object EdProduto: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdProdutoKeyDown
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object DBEdit2: TDBEdit
        Left = 12
        Top = 64
        Width = 73
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'EstqQ'
        DataSource = DsProdutos
        ReadOnly = True
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 88
        Top = 64
        Width = 77
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'PrecoV'
        DataSource = DsProdutos
        ReadOnly = True
        TabOrder = 3
      end
      object EdSubTo: TdmkEdit
        Left = 168
        Top = 64
        Width = 77
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtde: TdmkEdit
        Left = 248
        Top = 64
        Width = 76
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdQtdeExit
      end
      object EdDescoPerc: TdmkEdit
        Left = 328
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdDescoPercExit
      end
      object EdDescoVal: TdmkEdit
        Left = 412
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdDescoValExit
      end
      object EdTotal: TdmkEdit
        Left = 496
        Top = 64
        Width = 90
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdTotalExit
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 517
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdutos
        TabOrder = 1
        OnKeyDown = CBProdutoKeyDown
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkContinuar: TCheckBox
        Left = 16
        Top = 92
        Width = 153
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 9
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 248
    Width = 598
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 487
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 598
    Height = 40
    Align = alTop
    Caption = 'Venda de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 596
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 594
      ExplicitHeight = 36
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrProdutosAfterScroll
    OnCalcFields = QrProdutosCalcFields
    SQL.Strings = (
      'SELECT * FROM produtos'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 204
    Top = 50
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdutosCUSTOMEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOMEDIO'
      Calculated = True
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 232
    Top = 50
  end
end
