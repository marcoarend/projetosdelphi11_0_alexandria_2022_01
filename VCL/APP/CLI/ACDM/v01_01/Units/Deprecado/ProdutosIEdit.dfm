object FmProdutosIEdit: TFmProdutosIEdit
  Left = 556
  Top = 313
  Caption = 'MER-ESTOQ-002 :: Estoque Inicial de Produtos'
  ClientHeight = 163
  ClientWidth = 602
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 602
    Height = 67
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 12
      Width = 56
      Height = 13
      Caption = 'Mercadoria:'
    end
    object Label4: TLabel
      Left = 356
      Top = 12
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label10: TLabel
      Left = 428
      Top = 12
      Width = 64
      Height = 13
      Caption = 'Valor unit'#225'rio:'
    end
    object Label6: TLabel
      Left = 500
      Top = 12
      Width = 50
      Height = 13
      Caption = 'Valor total:'
    end
    object EdMercadoria: TdmkEdit
      Left = 8
      Top = 28
      Width = 44
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdPecas: TdmkEdit
      Left = 356
      Top = 28
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdPecasExit
    end
    object EdPreco: TdmkEdit
      Left = 428
      Top = 28
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdPrecoExit
    end
    object EdVTota: TdmkEdit
      Left = 500
      Top = 28
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdVTotaExit
    end
    object EdDescricao: TEdit
      Left = 52
      Top = 28
      Width = 301
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 1
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 115
    Width = 602
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel1: TPanel
      Left = 497
      Top = 1
      Width = 104
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 3
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 602
    Height = 48
    Align = alTop
    Caption = 'Estoque Inicial de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 523
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 552
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 522
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitTop = 5
      ExplicitWidth = 704
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtos'
      'ORDER BY Nome')
    Left = 152
    Top = 10
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrProdutosEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
    object QrProdutosEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrProdutosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 180
    Top = 10
  end
end
