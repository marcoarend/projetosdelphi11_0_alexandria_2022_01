unit ProdutosBalEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnMLAGeral, UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, UMySQLModule, UnitAcademy, Variants,
  dmkGeral, dmkEdit, UnDmkEnums;

type
  TFmProdutosBalEdit = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    Label1: TLabel;
    EdMercadoria: TdmkEdit;
    Label4: TLabel;
    EdPecas: TdmkEdit;
    Label10: TLabel;
    EdPreco: TdmkEdit;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label6: TLabel;
    EdVTota: TdmkEdit;
    EdDescricao: TEdit;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdPecasExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdVTotaExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaOnEdit(Tipo: Integer);
  public
    { Public declarations }
  end;

var
  FmProdutosBalEdit: TFmProdutosBalEdit;

implementation

uses UnMyObjects, Module, ProdutosBal;

{$R *.DFM}


procedure TFmProdutosBalEdit.CalculaOnEdit(Tipo: Integer);
var
  Pecas, Preco, VTota: Double;
begin
  Pecas := Geral.DMV(EdPecas.Text);
  Preco := Geral.DMV(EdPreco.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if Tipo <> 1 then VTota := Pecas * Preco
  else if Pecas > 0 then Preco := VTota / Pecas
  else Preco := 0;
  //
  EdPecas.Text := Geral.TFT(FloatToStr(Pecas), 3, siPositivo);
  EdPreco.Text := Geral.TFT(FloatToStr(Preco), 4, siPositivo);
  EdVTota.Text := Geral.TFT(FloatToStr(VTota), 2, siPositivo);
end;

procedure TFmProdutosBalEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosBalEdit.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  Conta, Pecas, VTota: Double;
  Merca: String;
begin
  Pecas := Geral.DMV(EdPecas.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if VTota <= 0 then
  begin
    ShowMessage('Defina o valor.');
    EdPreco.SetFocus;
    Exit;
  end;
  //
  if Trim(EdDescricao.Text) <> NULL then Merca := EdMercadoria.Text else
  begin
    ShowMessage('Defina a mercadoria.');
    EdMercadoria.SetFocus;
    Exit;
  end;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Conta := UMyMod.BuscaEmLivreY_Double(
      Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('INSERT INTO BalancosIts SET EstqQ=:P0, ');
      Dmod.QrUpdU.SQL.Add('EstqV=:P1, Produto=:P2, Periodo=:P3, Conta=:P4');
    end
    else
    begin
      Conta := FmProdutosBal.QrBalancosItsConta.Value;
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('UPDATE BalancosIts SET EstqQ=:P0, ');
      Dmod.QrUpdU.SQL.Add('EstqV=:P1  WHERE Produto=:P2 AND Periodo=:P3 AND Conta=:P4');
    end;
    Dmod.QrUpdU.Params[00].AsFloat := Pecas;
    Dmod.QrUpdU.Params[01].AsFloat := VTota;
    Dmod.QrUpdU.Params[02].AsString := Merca;
    Dmod.QrUpdU.Params[03].AsInteger := UnAcademy.VerificaBalanco;
      Dmod.QrUpdU.Params[04].AsFloat := Conta;
    Dmod.QrUpdU.ExecSQL;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  Close;
end;

procedure TFmProdutosBalEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CalculaOnEdit(1);
  EdPreco.SetFocus;
  EdPecas.SetFocus;
end;

procedure TFmProdutosBalEdit.EdPecasExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmProdutosBalEdit.EdPrecoExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmProdutosBalEdit.EdVTotaExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

procedure TFmProdutosBalEdit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
