object FmProdutosCNew: TFmProdutosCNew
  Left = 461
  Top = 239
  Caption = 'MER-ENTRA-002 :: Entrada de Produtos'
  ClientHeight = 335
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 484
    Height = 41
    Align = alTop
    Caption = 'Entrada de Produtos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LaTipo: TLabel
      Left = 409
      Top = 4
      Width = 80
      Height = 29
      Alignment = taCenter
      AutoSize = False
      Caption = 'Altera'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 41
    Width = 484
    Height = 246
    Align = alClient
    TabOrder = 1
    object LaLancamento: TLabel
      Left = 8
      Top = 4
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label10: TLabel
      Left = 64
      Top = 4
      Width = 36
      Height = 13
      Caption = 'Pedido:'
      Enabled = False
    end
    object LaSitPedido: TLabel
      Left = 104
      Top = 4
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object Label17: TLabel
      Left = 124
      Top = 4
      Width = 68
      Height = 13
      Caption = 'Data Emiss'#227'o:'
    end
    object Label11: TLabel
      Left = 240
      Top = 4
      Width = 65
      Height = 13
      Caption = 'Data entrada:'
    end
    object Label6: TLabel
      Left = 8
      Top = 44
      Width = 131
      Height = 13
      Caption = 'Fornecedor (representante):'
    end
    object Label19: TLabel
      Left = 8
      Top = 132
      Width = 75
      Height = 13
      Caption = 'Transportadora:'
    end
    object Label4: TLabel
      Left = 8
      Top = 192
      Width = 32
      Height = 13
      Caption = 'N'#186' NF:'
    end
    object Label1: TLabel
      Left = 132
      Top = 192
      Width = 62
      Height = 13
      Caption = '%RICMS NF:'
    end
    object Label9: TLabel
      Left = 276
      Top = 192
      Width = 43
      Height = 13
      Caption = 'Conhec.:'
    end
    object Label8: TLabel
      Left = 344
      Top = 192
      Width = 54
      Height = 13
      Caption = 'Valor Frete:'
    end
    object Label2: TLabel
      Left = 420
      Top = 192
      Width = 57
      Height = 13
      Caption = '%RICMS F.:'
    end
    object Label12: TLabel
      Left = 196
      Top = 192
      Width = 52
      Height = 13
      Caption = 'Valor juros:'
    end
    object Label13: TLabel
      Left = 76
      Top = 192
      Width = 54
      Height = 13
      Caption = '%ICMS NF:'
    end
    object Label3: TLabel
      Left = 8
      Top = 88
      Width = 91
      Height = 13
      Caption = 'Fabricante (marca):'
    end
    object Label5: TLabel
      Left = 356
      Top = 4
      Width = 123
      Height = 13
      Caption = 'Lote [F4 para ('#250'ltimo + 1)]:'
    end
    object SpeedButton1: TSpeedButton
      Left = 455
      Top = 60
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 455
      Top = 104
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 455
      Top = 148
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton3Click
    end
    object EdCodigo: TdmkEdit
      Left = 8
      Top = 20
      Width = 53
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdPedido: TdmkEdit
      Left = 64
      Top = 20
      Width = 57
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object TPDataC: TdmkEditDateTimePicker
      Left = 124
      Top = 20
      Width = 112
      Height = 21
      Date = 37463.997129768500000000
      Time = 37463.997129768500000000
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object TPDataE: TdmkEditDateTimePicker
      Left = 240
      Top = 20
      Width = 112
      Height = 21
      Date = 37463.997129768500000000
      Time = 37463.997129768500000000
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdRepresentante: TdmkEditCB
      Left = 8
      Top = 60
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBRepresentante
      IgnoraDBLookupComboBox = False
    end
    object CBRepresentante: TdmkDBLookupComboBox
      Left = 76
      Top = 60
      Width = 380
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsRepresentantes
      TabOrder = 6
      dmkEditCB = EdRepresentante
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTransportador: TdmkEditCB
      Left = 8
      Top = 148
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBTransportador
      IgnoraDBLookupComboBox = False
    end
    object CBTransportador: TdmkDBLookupComboBox
      Left = 76
      Top = 148
      Width = 380
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsTransportadores
      TabOrder = 10
      dmkEditCB = EdTransportador
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdNFC: TdmkEdit
      Left = 8
      Top = 208
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdRICMS: TdmkEdit
      Left = 132
      Top = 208
      Width = 61
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdRICMSExit
    end
    object EdNFF: TdmkEdit
      Left = 276
      Top = 208
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdFrete: TdmkEdit
      Left = 344
      Top = 208
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 16
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdRICMF: TdmkEdit
      Left = 420
      Top = 208
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdJuros: TdmkEdit
      Left = 196
      Top = 208
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdICMS: TdmkEdit
      Left = 76
      Top = 208
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdFabricante: TdmkEditCB
      Left = 8
      Top = 104
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBFabricante
      IgnoraDBLookupComboBox = False
    end
    object CBFabricante: TdmkDBLookupComboBox
      Left = 76
      Top = 104
      Width = 380
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsFabricantes
      TabOrder = 8
      dmkEditCB = EdFabricante
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdLote: TdmkEdit
      Left = 356
      Top = 20
      Width = 121
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnKeyDown = EdLoteKeyDown
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 287
    Width = 484
    Height = 48
    Align = alBottom
    TabOrder = 2
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 389
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
  end
  object QrRepresentantes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 60
    Top = 12
    object QrRepresentantesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRepresentantesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 14
    end
  end
  object DsRepresentantes: TDataSource
    DataSet = QrRepresentantes
    Left = 92
    Top = 12
  end
  object QrTransportadores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 8
    object QrTransportadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 14
    end
  end
  object DsTransportadores: TDataSource
    DataSet = QrTransportadores
    Left = 216
    Top = 8
  end
  object QrFabricantes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 128
    Top = 8
    object QrFabricantesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFabricantesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 9
    end
  end
  object DsFabricantes: TDataSource
    DataSet = QrFabricantes
    Left = 156
    Top = 8
  end
  object QrMaxLote: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Lote) Lote '
      'FROM produtosc')
    Left = 288
    Top = 8
    object QrMaxLoteLote: TIntegerField
      FieldName = 'Lote'
      Origin = 'DNRECOVERY.produtosc.Lote'
    end
  end
  object QrColetor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Coletor'
      'FROM produtosc'
      'WHERE Lote=:P0'
      'GROUP BY Coletor')
    Left = 260
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrColetorColetor: TIntegerField
      FieldName = 'Coletor'
      Origin = 'DNRECOVERY.produtosc.Coletor'
    end
  end
end
