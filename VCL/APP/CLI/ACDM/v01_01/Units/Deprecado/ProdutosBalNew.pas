unit ProdutosBalNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, UnMLAGeral, DBCtrls,   UnGOTOy, UnInternalConsts, UnMsgInt,
  StdCtrls, Db, (*DBTables,*) Buttons, UMySQLModule, mySQLDbTables, UnitAcademy,
  dmkGeral;

type
  TFmProdutosBalNew = class(TForm)
    QrLocPeriodo: TmySQLQuery;
    QrLocPeriodoPeriodo: TIntegerField;
    Panel1: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label2: TLabel;
    CBMes: TComboBox;
    Label3: TLabel;
    CBAno: TComboBox;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmProdutosBalNew: TFmProdutosBalNew;

implementation

uses UnMyObjects, Module, Principal, ProdutosBal;

{$R *.DFM}

procedure TFmProdutosBalNew.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia : Word;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    Add(IntToStr(Ano-2));
    Add(IntToStr(Ano-1));
    Add(IntToStr(Ano));
    Add(IntToStr(Ano+1));
    Add(IntToStr(Ano+2));
  end;
  CBAno.ItemIndex := 2;
  CBMes.ItemIndex := (Mes - 1);
  //QrTerceiros.Open;
end;

procedure TFmProdutosBalNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosBalNew.BtConfirmaClick(Sender: TObject);
var
  Ano, Mes, Dia : word;
  Periodo, Atual: Integer;
begin
  Ano := Geral.IMV(CBAno.Text);
  if Ano = 0 then Exit;
  Mes := CBMes.ItemIndex + 1;
  Periodo := ((Ano - 2000) * 12) + Mes;
  QrLocPeriodo.Close;
  QrLocPeriodo.Params[0].AsInteger := Periodo;
  QrLocPeriodo.Open;
  if GOTOy.Registros(QrLocPeriodo) > 0 then
  begin
    Application.MessageBox(PChar('Este per�odo de balan�o j� existe!'), 'Aviso',
    MB_ICONWARNING+MB_OK);
    Exit;
  end;
  // A - Impede criar do m�s futuro (pela data do sistema)
  DecodeDate(Date, Ano, Mes, Dia);
  Atual := ((Ano - 2000) * 12) + Mes;
  if Periodo >= Atual then
  begin
    Application.MessageBox('M�s inv�lido. [Futuro]', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  // fim A
  // B - Impede criar m�s anterior ao m�ximo
  if Periodo < UnAcademy.VerificaBalanco then
  begin
    Application.MessageBox('M�s inv�lido. J� existe m�s posterior ao desejado.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //Fim B
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO Balancos SET Periodo=:P0');
  Dmod.QrUpdM.Params[0].AsFloat := Periodo;
  Dmod.QrUpdM.ExecSQL;
  QrLocPeriodo.Close;
  VAR_PERIODO := Periodo;
  FmProdutosBal.DefParams;
  FmProdutosBal.LocCod(FmProdutosBal.QrBalancosPeriodo.Value, Periodo);
  FmProdutosBal.MostraEdicao(True, CO_INCLUSAO, 0);
  UMyMod.UpdLockY(Periodo, Dmod.MyDB, 'Balancos', 'Periodo');
  Close;
end;

procedure TFmProdutosBalNew.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosBalNew.FormPaint(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
