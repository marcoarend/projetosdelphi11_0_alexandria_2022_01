object FmProdutosImprime: TFmProdutosImprime
  Left = 362
  Top = 174
  Caption = 'MER-PRINT-001 :: Relat'#243'rios de Produtos'
  ClientHeight = 476
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 494
    Height = 388
    Align = alClient
    TabOrder = 0
    object PainelDados: TPanel
      Left = 1
      Top = 193
      Width = 492
      Height = 36
      Align = alTop
      TabOrder = 0
      object Label11: TLabel
        Left = 8
        Top = 12
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object SpeedButton1: TSpeedButton
        Left = 436
        Top = 8
        Width = 21
        Height = 21
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object EdPesqNome: TdmkEdit
        Left = 44
        Top = 8
        Width = 389
        Height = 21
        MaxLength = 13
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 492
      Height = 192
      Align = alTop
      TabOrder = 1
      object LaMercadoria: TLabel
        Left = 8
        Top = 104
        Width = 56
        Height = 13
        Caption = 'Mercadoria:'
        Visible = False
      end
      object LaDataI: TLabel
        Left = 8
        Top = 148
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
        Visible = False
      end
      object LaDataF: TLabel
        Left = 172
        Top = 148
        Width = 48
        Height = 13
        Caption = 'Data final:'
        Visible = False
      end
      object RGTipo: TRadioGroup
        Left = 8
        Top = 4
        Width = 485
        Height = 35
        Caption = ' Tipo de relat'#243'rio: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Estoque'
          'Balan'#231'o'
          'Hist'#243'rico'
          'Vendas'
          'Estoque em...')
        TabOrder = 0
        OnClick = RGTipoClick
      end
      object RGOrdem: TRadioGroup
        Left = 8
        Top = 44
        Width = 241
        Height = 35
        Caption = ' Ordem: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Alfab'#233'tica'
          'Num'#233'rica')
        TabOrder = 1
      end
      object RGGRade: TRadioGroup
        Left = 252
        Top = 44
        Width = 241
        Height = 35
        Caption = ' Grade: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Sim'
          'N'#227'o')
        TabOrder = 2
      end
      object CkPositivo: TCheckBox
        Left = 8
        Top = 84
        Width = 181
        Height = 17
        Caption = 'Somente produtos com estoque.'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
      object EdProduto: TdmkEditCB
        Left = 8
        Top = 120
        Width = 44
        Height = 21
        Alignment = taRightJustify
        MaxLength = 13
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProduto
        IgnoraDBLookupComboBox = False
      end
      object TPDataI: TdmkEditDateTimePicker
        Left = 8
        Top = 164
        Width = 121
        Height = 21
        Date = 37863.615402245400000000
        Time = 37863.615402245400000000
        TabOrder = 5
        Visible = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataF: TdmkEditDateTimePicker
        Left = 172
        Top = 164
        Width = 121
        Height = 21
        Date = 37863.615402245400000000
        Time = 37863.615402245400000000
        TabOrder = 6
        Visible = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object CkCompactar: TCheckBox
        Left = 200
        Top = 84
        Width = 97
        Height = 17
        Caption = 'Compactar.'
        TabOrder = 7
      end
      object CkCorLetras: TCheckBox
        Left = 284
        Top = 100
        Width = 113
        Height = 17
        Caption = 'Colorir importantes.'
        TabOrder = 8
      end
      object CBCorFundo: TCheckBox
        Left = 284
        Top = 80
        Width = 97
        Height = 17
        Caption = 'Cor de fundo.'
        TabOrder = 9
      end
      object CBProduto: TdmkDBLookupComboBox
        Left = 52
        Top = 120
        Width = 429
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsArtigos
        TabOrder = 10
        OnKeyDown = CBProdutoKeyDown
        dmkEditCB = EdProduto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGPesq: TDBGrid
      Left = 1
      Top = 229
      Width = 492
      Height = 158
      Align = alClient
      DataSource = DsPesq
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGPesqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 86
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 380
          Visible = True
        end>
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 428
    Width = 494
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 5
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Preview'
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 394
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
    object Progress: TProgressBar
      Left = 120
      Top = 24
      Width = 265
      Height = 16
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 494
    Height = 40
    Align = alTop
    Caption = 'Relat'#243'rios de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 492
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 498
      ExplicitHeight = 36
    end
  end
  object DsProdutosG: TDataSource
    DataSet = QrProdutosG
    Left = 348
    Top = 4
  end
  object DsArtigos: TDataSource
    DataSet = QrArtigos
    Left = 220
    Top = 162
  end
  object QrArtigos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtos'
      'ORDER BY Nome')
    Left = 192
    Top = 160
    object QrArtigosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArtigosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrArtigosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrArtigosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrArtigosPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrArtigosPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrArtigosICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrArtigosRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrArtigosIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrArtigosRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrArtigosFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrArtigosJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrArtigosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrArtigosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrArtigosEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrArtigosEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrArtigosImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrArtigosControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrArtigosCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrArtigosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrArtigosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrArtigosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrArtigosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrArtigosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrProdutosG: TmySQLQuery
    Database = DModG.MyLocDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM produtosg'
      'ORDER BY Nome')
    Left = 320
    Top = 4
    object QrProdutosGCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProdutosGNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object QrBalAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cu.EstqQ) EstqQ, SUM(cu.EstqV) EstqV,'
      'cu.Periodo, cu.Produto, ar.Nome DESCRICAO'
      'FROM balancosits cu, Produtos ar'
      'WHERE cu.Periodo=:P0'
      'AND ar.Codigo=cu.Produto'
      'GROUP BY cu.Produto')
    Left = 300
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBalAntEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrBalAntEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
    object QrBalAntPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalAntProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrBalAntDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
  end
  object QrBalNow: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cu.EstqQ) EstqQ,'
      'SUM(cu.EstqV) EstqV,'
      'cu.Periodo, cu.Produto, '
      'ar.Nome DESCRICAO'
      'FROM balancosits cu, Produtos ar'
      'WHERE cu.Periodo=:P0'
      'AND ar.Codigo=cu.Produto'
      'GROUP BY cu.Produto')
    Left = 328
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBalNowEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrBalNowEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
    object QrBalNowPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalNowProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrBalNowDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
  end
  object QrAntD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ei.Pecas) EstqQ, SUM(ei.CTotal) EstqV,'
      'ar.Nome DESCRICAO,'
      'ei.Produto'
      'FROM produtosdits ei, ProdutosD en, '
      'Produtos ar'
      'WHERE en.Cancelado="F"'
      'AND en.Data BETWEEN :P0 AND :P1'
      'AND en.Codigo=ei.Codigo'
      'AND ar.Codigo=ei.Produto'
      'GROUP BY ei.Codigo')
    Left = 300
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntDEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrAntDEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
    object QrAntDDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object QrAntDProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
  end
  object QrAntE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ei.Bruto) EstqQ, SUM(ei.Custo) EstqV,'
      'ar.Nome DESCRICAO, ar.Codigo PRODUTO'
      'FROM produtoscits ei, ProdutosC en, Produtos ar'
      'WHERE en.Cancelado="F"'
      'AND en.DataC BETWEEN :P0 AND :P1'
      'AND en.Codigo=ei.Codigo'
      'AND ar.Codigo=ei.Produto'
      'GROUP BY ei.Codigo')
    Left = 328
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntEEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrAntEEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
    object QrAntEDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object QrAntEPRODUTO: TIntegerField
      FieldName = 'PRODUTO'
      Required = True
    end
  end
  object QrAntR: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ei.Pecas) EstqQ, SUM(ei.CTotal) EstqV,'
      'ar.Nome DESCRICAO, ei.Produto'
      'FROM produtosrits ei, ProdutosR en, Produtos ar'
      'WHERE en.Cancelado="F"'
      'AND en.Data BETWEEN :P0 AND :P1'
      'AND en.Codigo=ei.Codigo'
      'AND ar.Codigo=ei.Produto'
      'GROUP BY ei.Codigo')
    Left = 300
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntREstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrAntREstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
    object QrAntRDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object QrAntRProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
  end
  object QrAntV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ei.Qtde) EstqQ, SUM(ei.ValorReal) EstqV,'
      'ar.Nome DESCRICAO, ei.Produto'
      'FROM vendaspro ei, Vendas en, '
      'Produtos ar'
      'WHERE en.Cancelado="F"'
      'AND en.DataCad BETWEEN :P0 AND :P1'
      'AND en.Codigo=ei.Codigo'
      'AND ar.Codigo=ei.Produto'
      'GROUP BY ei.Codigo')
    Left = 328
    Top = 236
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAntVEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrAntVEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
    object QrAntVDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object QrAntVProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
  end
  object QrMerc: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT Produto'
      'FROM bal'
      'WHERE Produto=:P0')
    Left = 300
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMercProduto: TIntegerField
      FieldName = 'Produto'
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos'
      'WHERE Periodo<:P0')
    Left = 328
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMaxPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrEntradas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ei.*, en.DataC'
      'FROM produtoscits ei, ProdutosC en'
      'WHERE ei.Produto=:P0'
      'AND en.Cancelado = "F"'
      'AND en.DataC BETWEEN :P1 AND :P2')
    Left = 332
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEntradasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntradasControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntradasConta: TAutoIncField
      FieldName = 'Conta'
    end
    object QrEntradasProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrEntradasPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrEntradasBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrEntradasTara: TFloatField
      FieldName = 'Tara'
      Required = True
    end
    object QrEntradasImpur: TFloatField
      FieldName = 'Impur'
      Required = True
    end
    object QrEntradasDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrEntradasValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrEntradasCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrEntradasDataC: TDateField
      FieldName = 'DataC'
      Required = True
    end
  end
  object QrVendas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vp.*'
      'FROM vendaspro vp'
      'WHERE vp.Produto=:P0'
      'AND vp.DataCad BETWEEN :P1 AND :P2')
    Left = 360
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrVendasCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVendasControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVendasProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrVendasCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrVendasQtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrVendasPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrVendasValorReal: TFloatField
      FieldName = 'ValorReal'
      Required = True
    end
    object QrVendasCusto: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrVendasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVendasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVendasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVendasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVendasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrDevolucoes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ei.*, en.Data '
      'FROM produtosdits ei, ProdutosD en'
      'WHERE ei.Produto=:P0'
      'AND en.Cancelado = "F"'
      'AND en.Data BETWEEN :P1 AND :P2')
    Left = 388
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDevolucoesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDevolucoesControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrDevolucoesConta: TLargeintField
      FieldName = 'Conta'
      Required = True
    end
    object QrDevolucoesUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrDevolucoesPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrDevolucoesPrecoUnit: TFloatField
      FieldName = 'PrecoUnit'
      Required = True
    end
    object QrDevolucoesDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrDevolucoesVIPI: TFloatField
      FieldName = 'VIPI'
    end
    object QrDevolucoesVICMS: TFloatField
      FieldName = 'VICMS'
    end
    object QrDevolucoesIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrDevolucoesICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrDevolucoesFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrDevolucoesCTotal: TFloatField
      FieldName = 'CTotal'
    end
    object QrDevolucoesVTotal: TFloatField
      FieldName = 'VTotal'
    end
    object QrDevolucoesProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrDevolucoesData: TDateField
      FieldName = 'Data'
    end
  end
  object QrRecompras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ei.*, en.Data '
      'FROM produtosrits ei, ProdutosR en'
      'WHERE ei.Produto=:P0'
      'AND en.Cancelado = "F"'
      'AND en.Data BETWEEN :P1 AND :P2')
    Left = 416
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrRecomprasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRecomprasControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrRecomprasConta: TLargeintField
      FieldName = 'Conta'
      Required = True
    end
    object QrRecomprasUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrRecomprasPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrRecomprasPrecoUnit: TFloatField
      FieldName = 'PrecoUnit'
      Required = True
    end
    object QrRecomprasDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrRecomprasVIPI: TFloatField
      FieldName = 'VIPI'
    end
    object QrRecomprasVICMS: TFloatField
      FieldName = 'VICMS'
    end
    object QrRecomprasIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrRecomprasICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrRecomprasFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrRecomprasCTotal: TFloatField
      FieldName = 'CTotal'
    end
    object QrRecomprasVTotal: TFloatField
      FieldName = 'VTotal'
    end
    object QrRecomprasProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrRecomprasData: TDateField
      FieldName = 'Data'
    end
  end
  object QrBalancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ei.* '
      'FROM balancosits ei, Balancos en'
      'WHERE ei.Produto=:P0'
      'AND ei.Periodo BETWEEN :P1 AND :P2'
      ' ')
    Left = 444
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrBalancosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalancosProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrBalancosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrBalancosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrBalancosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrBalancosAnteQ: TFloatField
      FieldName = 'AnteQ'
    end
    object QrBalancosAnteV: TFloatField
      FieldName = 'AnteV'
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProdutosCalcFields
    SQL.Strings = (
      'SELECT  * FROM produtos'
      '')
    Left = 116
    Top = 228
    object QrProdutosCUSTOMEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOMEDIO'
      Calculated = True
    end
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrBalanco: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrBalancoCalcFields
    SQL.Strings = (
      'SELECT * FROM bal')
    Left = 116
    Top = 256
    object QrBalancoQSAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QSAL'
      Calculated = True
    end
    object QrBalancoQDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QDIF'
      Calculated = True
    end
    object QrBalancoVDIF: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VDIF'
      Calculated = True
    end
    object QrBalancoProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrBalancoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
    object QrBalancoNomeForecedor: TWideStringField
      FieldName = 'NomeForecedor'
      Size = 50
    end
    object QrBalancoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrBalancoEstqQAnt: TFloatField
      FieldName = 'EstqQAnt'
    end
    object QrBalancoEstqVAnt: TFloatField
      FieldName = 'EstqVAnt'
    end
    object QrBalancoEstqQEnt: TFloatField
      FieldName = 'EstqQEnt'
    end
    object QrBalancoEstqVEnt: TFloatField
      FieldName = 'EstqVEnt'
    end
    object QrBalancoEstqQVen: TFloatField
      FieldName = 'EstqQVen'
    end
    object QrBalancoEstqVVen: TFloatField
      FieldName = 'EstqVVen'
    end
    object QrBalancoEstqQDev: TFloatField
      FieldName = 'EstqQDev'
    end
    object QrBalancoEstqVDev: TFloatField
      FieldName = 'EstqVDev'
    end
    object QrBalancoEstqQRec: TFloatField
      FieldName = 'EstqQRec'
    end
    object QrBalancoEstqVRec: TFloatField
      FieldName = 'EstqVRec'
    end
    object QrBalancoEstqQNow: TFloatField
      FieldName = 'EstqQNow'
    end
    object QrBalancoEstqVNow: TFloatField
      FieldName = 'EstqVNow'
    end
  end
  object QrEstq: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrEstqCalcFields
    SQL.Strings = (
      'SELECT * FROM estq'
      'ORDER BY Data, Tipo')
    Left = 116
    Top = 284
    object QrEstqProduto: TWideStringField
      FieldName = 'Produto'
      Origin = 'DBMLOCAL.estq.Produto'
      Size = 8
    end
    object QrEstqDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMLOCAL.estq.Descricao'
      Size = 1
    end
    object QrEstqNomeForecedor: TWideStringField
      FieldName = 'NomeForecedor'
      Origin = 'DBMLOCAL.estq.NomeForecedor'
      Size = 1
    end
    object QrEstqFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMLOCAL.estq.Fornecedor'
    end
    object QrEstqEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMLOCAL.estq.EstqQ'
    end
    object QrEstqEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMLOCAL.estq.EstqV'
    end
    object QrEstqData: TDateField
      FieldName = 'Data'
      Origin = 'DBMLOCAL.estq.Data'
    end
    object QrEstqTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'DBMLOCAL.estq.Tipo'
    end
    object QrEstqAcumQ: TFloatField
      FieldName = 'AcumQ'
      Origin = 'DBMLOCAL.estq.Data'
    end
    object QrEstqAcumV: TFloatField
      FieldName = 'AcumV'
      Origin = 'DBMLOCAL.estq.Tipo'
    end
    object QrEstqNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 30
      Calculated = True
    end
    object QrEstqACUM2Q: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ACUM2Q'
      Calculated = True
    end
    object QrEstqACUM2V: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ACUM2V'
      Calculated = True
    end
    object QrEstqDIF2Q: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIF2Q'
      Calculated = True
    end
    object QrEstqDIF2V: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIF2V'
      Calculated = True
    end
    object QrEstqVALQ2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALQ2'
      Calculated = True
    end
    object QrEstqVALV2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALV2'
      Calculated = True
    end
  end
  object QrVendido1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVendido1CalcFields
    SQL.Strings = (
      'SELECT ar.Nome DESCRICAO, vi.*,'
      '((vi.Qtde*vi.Preco)-vi.ValorReal) Desconto'
      'FROM vendaspro vi, Produtos ar'
      'WHERE ar.Codigo=vi.Produto'
      'AND vi.DataCad BETWEEN "2005-10-01" AND "2005-10-31"'
      'ORDER BY vi.DataCad')
    Left = 116
    Top = 340
    object QrVendido1MARGEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MARGEM'
      Calculated = True
    end
    object QrVendido1CUSTOUNIT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOUNIT'
      Calculated = True
    end
    object QrVendido1DESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object QrVendido1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVendido1Codigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVendido1Controle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVendido1Produto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrVendido1Qtde: TFloatField
      FieldName = 'Qtde'
      Required = True
    end
    object QrVendido1Preco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrVendido1ValorReal: TFloatField
      FieldName = 'ValorReal'
      Required = True
    end
    object QrVendido1Custo: TFloatField
      FieldName = 'Custo'
      Required = True
    end
    object QrVendido1Desconto: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Desconto'
      Required = True
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtos'
      'WHERE Codigo= :P0'
      'ORDER BY Nome')
    Left = 300
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPesqNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrPesqPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrPesqPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrPesqICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrPesqRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrPesqIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrPesqRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrPesqFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrPesqJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrPesqEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrPesqEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrPesqEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrPesqEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrPesqImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrPesqControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrPesqCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 328
    Top = 358
  end
  object QrVendido2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVendido2CalcFields
    SQL.Strings = (
      'SELECT ar.Nome DESCRICAO, vi.Produto, SUM(vi.Qtde) Qtde,'
      'SUM(vi.ValorReal) ValorReal, SUM(vi.Qtde*vi.Preco) BRUTO,  '
      'SUM((vi.Qtde*vi.Preco)-vi.ValorReal) DESCONTO, SUM(Custo) Custo'
      'FROM vendaspro vi, Produtos ar'
      'WHERE ar.Codigo=vi.Produto'
      'AND vi.DataCad BETWEEN "2005-10-01" AND "2005-10-31"'
      'GROUP BY vi.Produto'
      'ORDER BY DESCRICAO')
    Left = 116
    Top = 368
    object QrVendido2DESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Required = True
      Size = 100
    end
    object QrVendido2Produto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrVendido2Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrVendido2ValorReal: TFloatField
      FieldName = 'ValorReal'
    end
    object QrVendido2BRUTO: TFloatField
      FieldName = 'BRUTO'
    end
    object QrVendido2DESCONTO: TFloatField
      FieldName = 'DESCONTO'
    end
    object QrVendido2CUSTOUNIT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOUNIT'
      Calculated = True
    end
    object QrVendido2MARGEM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MARGEM'
      Calculated = True
    end
    object QrVendido2Custo: TFloatField
      FieldName = 'Custo'
    end
    object QrVendido2PRECOMEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOMEDIO'
      Calculated = True
    end
  end
  object frxProdutos: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38033.741673344900000000
    ReportOptions.LastChange = 39724.622732557900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo9OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo9, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo9.FrameTyp:=15 else'
      '  Memo9.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo10.FrameTyp:=15 else'
      '  Memo10.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo11.FrameTyp:=15 else'
      '  Memo11.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo12.FrameTyp:=15 else'
      '  Memo12.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo13.FrameTyp:=15 else'
      '  Memo13.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo15OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo15, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo15.FrameTyp:=15 else'
      '  Memo15.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo17OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo17, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo17.FrameTyp:=15 else'
      '  Memo17.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxProdutosGetValue
    Left = 172
    Top = 228
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsProdutos
        DataSetName = 'frxDsProdutos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 557.779530000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 5.779530000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 177.779530000000000000
          Top = 28.661410000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 359.055350000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 590.000000000000000000
          Top = 9.196660000000010000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 678.000000000000000000
          Top = 9.196660000000010000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -6.000000000000000000
          Top = 5.196660000000010000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 48.614100000000000000
        Top = 113.385900000000000000
        Width = 755.906000000000000000
        object Line3: TfrxLineView
          Left = -6.000000000000000000
          Top = 22.614100000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo1: TfrxMemoView
          Left = 1.779530000000000000
          Top = 30.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 81.779530000000000000
          Top = 30.614100000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 437.779530000000000000
          Top = 30.614100000000000000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 513.779530000000000000
          Top = 30.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 593.779530000000000000
          Top = 30.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo m'#233'dio')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 657.779530000000000000
          Top = 30.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o V.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 373.779530000000000000
          Top = 30.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o C.')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 222.992270000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsProdutos
        DataSetName = 'frxDsProdutos'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = 1.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProdutos."Codigo"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 437.779530000000000000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProdutos."EstqQ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 513.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProdutos."EstqV"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 593.779530000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProdutos."CUSTOMEDIO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 657.779530000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 373.779530000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 81.779530000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsProdutos."Nome"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 302.362400000000000000
        Width = 755.906000000000000000
        object Memo14: TfrxMemoView
          Left = 370.000000000000000000
          Top = 2.110079999999980000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 430.000000000000000000
          Top = 2.110079999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsProdutos."EstqQ">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 510.000000000000000000
          Top = 2.110079999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsProdutos."EstqV">)]')
          ParentFont = False
        end
      end
      object Memo19: TfrxMemoView
        Left = -6.000000000000000000
        Top = 112.000000000000000000
        Width = 732.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NOMEREL_002]')
        ParentFont = False
      end
    end
  end
  object frxDsProdutos: TfrxDBDataset
    UserName = 'frxDsProdutos'
    CloseDataSource = False
    DataSet = QrProdutos
    BCDToCurrency = False
    Left = 144
    Top = 228
  end
  object frxBalanco: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38033.741675196800000000
    ReportOptions.LastChange = 39724.625757187500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo9OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo9, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo9.FrameTyp:=15 else'
      '  Memo9.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo10.FrameTyp:=15 else'
      '  Memo10.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo11.FrameTyp:=15 else'
      '  Memo11.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo12.FrameTyp:=15 else'
      '  Memo12.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo13.FrameTyp:=15 else'
      '  Memo13.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo15OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo15, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo15.FrameTyp:=15 else'
      '  Memo15.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo17OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo17, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo17.FrameTyp:=15 else'
      '  Memo17.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo23OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo23, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo23.FrameTyp:=15 else'
      '  Memo23.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo24OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo24, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo24.FrameTyp:=15 else'
      '  Memo24.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo25OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo25, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo25.FrameTyp:=15 else'
      '  Memo25.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo26OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo26, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo26.FrameTyp:=15 else'
      '  Memo26.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 172
    Top = 256
    Datasets = <
      item
        DataSet = frxDsBalanco
        DataSetName = 'frxDsBalanco'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 557.779530000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 5.779530000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 177.779530000000000000
          Top = 4.661410000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 177.779530000000000000
          Top = 44.661410000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            'PER'#205'ODO: ')
        end
        object Memo29: TfrxMemoView
          Left = 245.779530000000000000
          Top = 44.661410000000000000
          Width = 480.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            '[PERIODO]')
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 374.173470000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 586.220470000000000000
          Top = 6.739949999999971000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 674.220470000000000000
          Top = 6.739949999999971000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -9.779530000000000000
          Top = 2.739949999999960000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 64.614100000000000000
        Top = 113.385900000000000000
        Width = 755.906000000000000000
        object Line3: TfrxLineView
          Left = -6.000000000000000000
          Top = 22.614100000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo1: TfrxMemoView
          Left = -2.000000000000000000
          Top = 46.614100000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 42.000000000000000000
          Top = 46.614100000000000000
          Width = 132.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 234.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Compras')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 294.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Devol.C')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 354.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Vendas')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 414.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Devol.F')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 174.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Inicial')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 234.000000000000000000
          Top = 27.614100000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Entradas')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 354.000000000000000000
          Top = 26.614100000000000000
          Width = 120.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Sa'#237'das')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 474.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 534.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Balan'#231'o')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 594.000000000000000000
          Top = 46.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 654.000000000000000000
          Top = 46.614100000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsBalanco
        DataSetName = 'frxDsBalanco'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = -2.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."Produto"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 234.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."EstqQEnt"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 294.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."EstqQRec"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 354.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."EstqQVen"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 414.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."EstqQDev"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 174.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."EstqQAnt"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 42.000000000000000000
          Width = 132.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsBalanco."Descricao"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 474.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."QSAL"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 534.000000000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."EstqQNow"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 594.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo25OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."QDIF"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 658.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo26OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsBalanco."VDIF"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 317.480520000000000000
        Width = 755.906000000000000000
        object Memo14: TfrxMemoView
          Left = 550.000000000000000000
          Top = 6.110079999999980000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 658.000000000000000000
          Top = 6.110079999999980000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBalanco."VDIF">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 594.000000000000000000
          Top = 6.110079999999980000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsBalanco."QDIF">)]')
          ParentFont = False
        end
      end
      object Memo19: TfrxMemoView
        Left = -6.000000000000000000
        Top = 112.000000000000000000
        Width = 732.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NOMEREL_003]')
        ParentFont = False
      end
    end
  end
  object frxDsBalanco: TfrxDBDataset
    UserName = 'frxDsBalanco'
    CloseDataSource = False
    DataSet = QrBalanco
    BCDToCurrency = False
    Left = 144
    Top = 256
  end
  object frxDsEstq: TfrxDBDataset
    UserName = 'frxDsEstq'
    CloseDataSource = False
    DataSet = QrEstq
    BCDToCurrency = False
    Left = 144
    Top = 284
  end
  object frxEstq: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38033.741677048600000000
    ReportOptions.LastChange = 39724.627779756900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo10.FrameTyp:=15 else'
      '  Memo10.FrameTyp:=0;'
      '  //'
      '  Memo10.DisplayFormat := VARF_FMT(VARF_FMT);'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo11.FrameTyp:=15 else'
      '  Memo11.FrameTyp:=0;'
      '  //'
      '  Memo11.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo12.FrameTyp:=15 else'
      '  Memo12.FrameTyp:=0;'
      '  //'
      '  Memo12.DisplayFormat := VARF_FMT(VARF_FMT);'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo13.FrameTyp:=15 else'
      '  Memo13.FrameTyp:=0;'
      '  //'
      '  Memo13.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo15OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo15, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo15.FrameTyp:=15 else'
      '  Memo15.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo2, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo2.FrameTyp:=15 else'
      '  Memo2.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo18OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo18, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo18.FrameTyp:=15 else'
      '  Memo18.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo20OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo20, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo20.FrameTyp:=15 else'
      '  Memo20.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 172
    Top = 284
    Datasets = <
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo28: TfrxMemoView
          Left = 174.000000000000000000
          Top = 44.661410000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            'PER'#205'ODO: ')
        end
        object Memo29: TfrxMemoView
          Left = 242.000000000000000000
          Top = 44.661410000000000000
          Width = 480.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            '[PERIODO2]')
        end
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.000000000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.000000000000000000
          Top = 4.661410000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 374.173470000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 590.000000000000000000
          Top = 10.519480000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 678.000000000000000000
          Top = 10.519480000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -6.000000000000000000
          Top = 6.519479999999990000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 64.614100000000000000
        Top = 113.385900000000000000
        Width = 755.906000000000000000
        object Line3: TfrxLineView
          Left = -6.000000000000000000
          Top = 22.614100000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo3: TfrxMemoView
          Left = 238.000000000000000000
          Top = 46.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 318.000000000000000000
          Top = 46.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 398.000000000000000000
          Top = 46.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 478.000000000000000000
          Top = 46.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 154.000000000000000000
          Top = 46.614100000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 238.000000000000000000
          Top = 26.614100000000000000
          Width = 160.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 398.000000000000000000
          Top = 26.614100000000000000
          Width = 160.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Acumulado')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = -2.000000000000000000
          Top = 46.614100000000000000
          Width = 156.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Tipo de movimenta'#231#227'o')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 558.000000000000000000
          Top = 46.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 638.000000000000000000
          Top = 46.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 558.000000000000000000
          Top = 26.614100000000000000
          Width = 160.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Diferen'#231'a do Balan'#231'o')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 238.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."VALQ2"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 318.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."VALV2"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 398.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."ACUM2Q"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 478.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."ACUM2V"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 154.000000000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = -2.000000000000000000
          Width = 156.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEstq."NOMETIPO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 558.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."DIF2Q"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 638.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo20OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."DIF2V"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 317.480520000000000000
        Width = 755.906000000000000000
      end
      object Memo19: TfrxMemoView
        Left = -6.000000000000000000
        Top = 112.000000000000000000
        Width = 732.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NOMEREL_004]')
        ParentFont = False
      end
    end
  end
  object frxVendido1: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38033.741678912010000000
    ReportOptions.LastChange = 39724.633119745400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo10.FrameTyp:=15 else'
      '  Memo10.FrameTyp:=0;'
      '  //'
      '  Memo10.DisplayFormat := VARF_FMT(VARF_FMT);'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo11.FrameTyp:=15 else'
      '  Memo11.FrameTyp:=0;'
      '  //'
      '  Memo11.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo12.FrameTyp:=15 else'
      '  Memo12.FrameTyp:=0;'
      '  //'
      '  Memo12.DisplayFormat := VARF_FMT(VARF_FMT);'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo13.FrameTyp:=15 else'
      '  Memo13.FrameTyp:=0;'
      '  //'
      '  Memo13.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo15OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo15, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo15.FrameTyp:=15 else'
      '  Memo15.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo18OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo18, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo18.FrameTyp:=15 else'
      '  Memo18.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo20OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo20, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo20.FrameTyp:=15 else'
      '  Memo20.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo2, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo2.FrameTyp:=15 else'
      '  Memo2.FrameTyp:=0;'
      '  //'
      '  Memo2.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo23OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo23, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo23.FrameTyp:=15 else'
      '  Memo23.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo17OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo17, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  FrameTyp:=15 else'
      '  FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 172
    Top = 340
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsVendido1
        DataSetName = 'frxDsVendido1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 557.779530000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 5.779530000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 177.779530000000000000
          Top = 4.661410000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 177.779530000000000000
          Top = 44.661410000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            'PER'#205'ODO: ')
        end
        object Memo29: TfrxMemoView
          Left = 245.779530000000000000
          Top = 44.661410000000000000
          Width = 480.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            '[PERIODO2]')
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 359.055350000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 590.000000000000000000
          Top = 10.519480000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 678.000000000000000000
          Top = 10.519480000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -6.000000000000000000
          Top = 6.519479999999990000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 48.393630000000000000
        Top = 113.385900000000000000
        Width = 755.906000000000000000
        object Memo19: TfrxMemoView
          Left = -6.000000000000000000
          Top = 2.393629999999990000
          Width = 732.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL_005]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = -6.000000000000000000
          Top = 26.393630000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo3: TfrxMemoView
          Left = 298.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 358.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo Unit.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 418.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o Unit.')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 478.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo Total')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 238.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 598.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 658.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Margem')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 538.000000000000000000
          Top = 30.393630000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = -2.000000000000000000
          Top = 30.393630000000000000
          Width = 240.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 222.992270000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsVendido1
        DataSetName = 'frxDsVendido1'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 305.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."Qtde"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 365.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."CUSTOUNIT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 425.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."Preco"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 485.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."Custo"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 245.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."DataCad"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 605.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."ValorReal"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 665.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo20OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."MARGEM"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 545.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."Desconto"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 41.559060000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsVendido1."DESCRICAO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 1.559060000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido1."Codigo"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 302.362400000000000000
        Width = 755.906000000000000000
        object Memo8: TfrxMemoView
          Left = 601.779530000000000000
          Top = 2.991960000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido1."ValorReal">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 1.779530000000000000
          Top = 2.991960000000010000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 301.779530000000000000
          Top = 2.991960000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido1."Qtde">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 481.779530000000000000
          Top = 2.991960000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido1."Custo">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 661.779530000000000000
          Top = 2.991960000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[((([SUM(<frxDsVendido1."ValorReal"])]) - (SUM(<frxDsVendido1."C' +
              'usto">))) / ((SUM(<frxDsVendido1."Custo">)/10000))))]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 541.779530000000000000
          Top = 2.991960000000010000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido1."Desconto">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsVendido1: TfrxDBDataset
    UserName = 'frxDsVendido1'
    CloseDataSource = False
    DataSet = QrVendido1
    BCDToCurrency = False
    Left = 144
    Top = 340
  end
  object frxDsVendido2: TfrxDBDataset
    UserName = 'frxDsVendido2'
    CloseDataSource = False
    DataSet = QrVendido2
    BCDToCurrency = False
    Left = 144
    Top = 368
  end
  object frxVendido2: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38033.741678912010000000
    ReportOptions.LastChange = 39724.636133715300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo10.FrameTyp:=15 else'
      '  Memo10.FrameTyp:=0;'
      '  //'
      '  Memo10.DisplayFormat := VARF_FMT(VARF_FMT);'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo11.FrameTyp:=15 else'
      '  Memo11.FrameTyp:=0;'
      '  //'
      '  Memo11.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo12.FrameTyp:=15 else'
      '  Memo12.FrameTyp:=0;'
      '  //'
      '  Memo12.DisplayFormat := VARF_FMT(VARF_FMT);'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo13.FrameTyp:=15 else'
      '  Memo13.FrameTyp:=0;'
      '  //'
      '  Memo13.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo18OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo18, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo18.FrameTyp:=15 else'
      '  Memo18.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo20OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo20, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo20.FrameTyp:=15 else'
      '  Memo20.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo2, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo2.FrameTyp:=15 else'
      '  Memo2.FrameTyp:=0;'
      '  //'
      '  Memo2.DisplayFormat := VARF_FMT2(VARF_FMT2);'
      '  end'
      'end;'
      ''
      'procedure Memo23OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo23, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo23.FrameTyp:=15 else'
      '  Memo23.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo17OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo17, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  FrameTyp:=15 else'
      '  FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 172
    Top = 368
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsVendido2
        DataSetName = 'frxDsVendido2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 557.779530000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 5.779530000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 177.779530000000000000
          Top = 4.661410000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 177.779530000000000000
          Top = 44.661410000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            'PER'#205'ODO: ')
        end
        object Memo29: TfrxMemoView
          Left = 245.779530000000000000
          Top = 44.661410000000000000
          Width = 480.000000000000000000
          Height = 18.000000000000000000
          Memo.UTF8W = (
            '[PERIODO2]')
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 355.275820000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 597.559060000000000000
          Top = 10.519480000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 685.559060000000000000
          Top = 10.519480000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = 1.559060000000000000
          Top = 6.519479999999990000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 44.614100000000000000
        Top = 113.385900000000000000
        Width = 755.906000000000000000
        object Line3: TfrxLineView
          Left = -2.220470000000000000
          Top = 22.614100000000000000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo3: TfrxMemoView
          Left = 241.779530000000000000
          Top = 26.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 321.779530000000000000
          Top = 26.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo Unit.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 401.779530000000000000
          Top = 26.614100000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o Unit.')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 481.779530000000000000
          Top = 26.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo Total')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 601.779530000000000000
          Top = 26.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 661.779530000000000000
          Top = 26.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Margem')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 541.779530000000000000
          Top = 26.614100000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 1.779530000000000000
          Top = 26.614100000000000000
          Width = 240.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 219.212740000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsVendido2
        DataSetName = 'frxDsVendido2'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 245.559060000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."Qtde"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 325.559060000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."CUSTOUNIT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 405.559060000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."PRECOMEDIO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 485.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."Custo"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 605.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."ValorReal"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 665.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo20OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."MARGEM"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 545.559060000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."DESCONTO"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 41.559060000000000000
          Width = 204.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsVendido2."DESCRICAO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 1.559060000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVendido2."Produto"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 298.582870000000000000
        Width = 755.906000000000000000
        object Memo8: TfrxMemoView
          Left = 601.779530000000000000
          Top = 2.991959999999950000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido2."ValorReal">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 1.779530000000000000
          Top = 2.991959999999950000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 241.779530000000000000
          Top = 2.991959999999950000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido2."Qtde">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 481.779530000000000000
          Top = 2.991959999999950000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido2."Custo">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 661.779530000000000000
          Top = 2.991959999999950000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[((([SUM(<frxDsVendido2."ValorReal">)) - (SUM(<frxDsVendido2."Cu' +
              'sto">))) / ((SUM(<frxDsVendido2."Custo">)/10000))))]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 541.779530000000000000
          Top = 2.991959999999950000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVendido2."DESCONTO">)]')
          ParentFont = False
        end
      end
      object Memo19: TfrxMemoView
        Left = -2.220470000000000000
        Top = 112.000000000000000000
        Width = 732.000000000000000000
        Height = 18.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[NOMEREL_005]')
        ParentFont = False
      end
    end
  end
end
