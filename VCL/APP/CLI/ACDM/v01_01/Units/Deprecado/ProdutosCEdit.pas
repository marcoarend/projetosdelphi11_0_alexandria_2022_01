unit ProdutosCEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnMLAGeral, UnInternalConsts,
  UnMsgInt, ExtCtrls, UMySQLModule, UnInternalConsts2, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, UnDmkEnums;

type
  TFmProdutosCEdit = class(TForm)
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    PainelTitulo: TPanel;
    PainelDados: TPanel;
    PainelControle: TPanel;
    Label1: TLabel;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdBruto: TdmkEdit;
    Label10: TLabel;
    EdTotal: TdmkEdit;
    Label5: TLabel;
    EdValKg: TdmkEdit;
    Label2: TLabel;
    EdCodigo: TdmkEdit;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label3: TLabel;
    EdLiqui: TdmkEdit;
    Label4: TLabel;
    EdTara: TdmkEdit;
    Label6: TLabel;
    EdImpur: TdmkEdit;
    Label7: TLabel;
    EdPreco: TdmkEdit;
    EdDesco: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdSubTo: TdmkEdit;
    QrProdutosCodigo: TIntegerField;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosJuros: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosImprime: TWideStringField;
    QrProdutosLk: TIntegerField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    QrProdutosControla: TWideStringField;
    QrProdutosCasas: TIntegerField;
    SpeedButton1: TSpeedButton;
    Panel1: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdBrutoExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdTotalExit(Sender: TObject);
    procedure EdTaraExit(Sender: TObject);
    procedure EdImpurExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdDescoExit(Sender: TObject);
    procedure EdSubToExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CBProdutoClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaOnEdit(Tipo: Integer);
  public
    { Public declarations }
  end;

var
  FmProdutosCEdit: TFmProdutosCEdit;

implementation

uses UnMyObjects, ProdutosC, Module, Produtos;

{$R *.DFM}


procedure TFmProdutosCEdit.CalculaOnEdit(Tipo: Integer);
var
  PesoS, Preco, Bruto, Tara, SubTo, Impur, Desco, Total, Valkg, Liqui: Double;
begin
  Preco := Geral.DMV(EdPreco.Text);
  Bruto := Geral.DMV(EdBruto.Text);
  Tara  := Geral.DMV(EdTara.Text);
  SubTo := Geral.DMV(EdSubTo.Text);
  Impur := Geral.DMV(EdImpur.Text);
  Desco := Geral.DMV(EdDesco.Text);
  Total := Geral.DMV(EdTotal.Text);
  PesoS := Bruto - Tara;
  if PesoS < 0 then
  begin
    Application.MessageBox('Bruto menos Tara n�o pode ser negativo.', 'Erro',
    MB_OK+MB_ICONERROR);
    PesoS := 0;
  end;
  Liqui := PesoS - Impur;
  if Liqui < 0 then
  begin
    Application.MessageBox('Peso l�quido n�o pode ser negativo.', 'Erro',
    MB_OK+MB_ICONERROR);
    Liqui := 0;
  end;
  if Tipo in [0,1,2,4] then
  begin
    SubTo := Preco * PesoS;
    if SubTo < 0 then SubTo := 0;
    Desco := Impur * Preco;
    if Desco<0 then Desco := 0;
    Total := (Preco * PesoS) - Desco;
    if Total<0 then Total := 0;
  end;
  if Tipo = 3 then
  begin
    if PesoS>0 then Preco := SubTo / PesoS else Preco := 0;
    Desco := Impur * Preco;
    if Desco<0 then Desco := 0;
    Total := (Preco * PesoS) - Desco;
    if Total<0 then Total := 0;
  end;
  if Tipo = 5 then
  begin
    Total := (Preco * PesoS) - Desco;
    if Total<0 then Total := 0;
  end;
  if Tipo = 6 then
    Desco := (Preco * PesoS) - Total;
  ////
  if Liqui>0 then Valkg := Total/Liqui else Valkg:=0;
  ////
  EdPreco.Text := Geral.TFT(FloatToStr(Preco), 4, siNegativo);
  EdBruto.Text := Geral.TFT(FloatToStr(Bruto), QrProdutosCasas.Value, siNegativo);
  EdTara.Text  := Geral.TFT(FloatToStr(Tara),  3, siNegativo);
  EdSubTo.Text := Geral.TFT(FloatToStr(SubTo), 2, siNegativo);
  EdImpur.Text := Geral.TFT(FloatToStr(Impur), 3, siNegativo);
  EdLiqui.Text := Geral.TFT(FloatToStr(Liqui), 3, siNegativo);
  EdValkg.Text := Geral.TFT(FloatToStr(Valkg), 4, siNegativo);
  EdDesco.Text := Geral.TFT(FloatToStr(Desco), 2, siNegativo);
  EdTotal.Text := Geral.TFT(FloatToStr(Total), 2, siNegativo);
end;

procedure TFmProdutosCEdit.CBProdutoClick(Sender: TObject);
begin
  EdPreco.Text := Geral.TFT(FloatToStr(QrProdutosPrecoC.Value), 4, siPositivo);
end;

procedure TFmProdutosCEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosCEdit.FormCreate(Sender: TObject);
begin
  QrProdutos.Open;
end;

procedure TFmProdutosCEdit.SpeedButton1Click(Sender: TObject);
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'Produtos', 0) then Exit;
    Application.CreateForm(TFmProdutos, FmProdutos);
    FmProdutos.ShowModal;
    FmProdutos.Destroy;
    QrProdutos.Close;
    QrProdutos.Open;
    EdProduto.Text     := IntToStr(VAR_PRODUTO);
    CBProduto.KeyValue := VAR_PRODUTO;
    CBProduto.SetFocus;
  finally
    Cursor := VAR_CURSOR;
  end;

end;

procedure TFmProdutosCEdit.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  Controle, Preco, Bruto, Tara, Impur, Desco, Total: Double;
  Conta, Produto, Codigo: Integer;
begin
  Produto := CBProduto.KeyValue;
  if Produto < 1 then
  begin
    Application.MessageBox(FIN_MSG_DEFPRODUTO, 'Erro', MB_OK+MB_ICONERROR);
    EdProduto.SetFocus;
    Exit;
  end;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
//  try
    Codigo := FmProdutosC.QrProdutosCCodigo.Value;
    Conta := EdCodigo.ValueVariant;

    Produto := CBProduto.KeyValue;
    Preco := Geral.DMV(EdPreco.Text);
    Bruto := Geral.DMV(EdBruto.Text);
    Tara  := Geral.DMV(EdTara.Text);
    Impur := Geral.DMV(EdImpur.Text);
    Desco := Geral.DMV(EdDesco.Text);
    Total := Geral.DMV(EdTotal.Text);
    if Total<= 0 then
    begin
      Application.MessageBox(PChar('Defina o valor do item.'), 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    if (Bruto-Tara-Impur)<=0 then
    begin
      Application.MessageBox(PChar('Defina o peso l�quido do item.'), 'Erro', MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    Dmod.QrUpd.SQL.Clear;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Controle := UMyMod.BuscaEmLivreY_Double(Dmod.MyDB, 'Livres', 'Controle', 'ProdutosCIts', 'ProdutosCIts', 'Codigo');
(*      Dmod.QrUpd.SQL.Add('UPDATE ProdutosCIts SET Conta=Conta+1');
      Dmod.QrUpd.SQL.Add('WHERE Conta>=:P0 AND Codigo=:P1 AND Controle=');
      Dmod.QrUpd.Params[0].AsInteger := Conta;
      Dmod.QrUpd.Params[1].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;*)
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO ProdutosCIts SET ');
    end else
    begin
      Dmod.QrUpd.SQL.Add('UPDATE ProdutosCIts SET ');
      Controle := FmProdutosC.QrProdutosCItsControle.Value;
    end;
    Dmod.QrUpd.SQL.Add('  Produto  =:P0');
    Dmod.QrUpd.SQL.Add(', Preco    =:P1');
    Dmod.QrUpd.SQL.Add(', Bruto    =:P2');
    Dmod.QrUpd.SQL.Add(', Tara     =:P3');
    Dmod.QrUpd.SQL.Add(', Impur    =:P4');
    Dmod.QrUpd.SQL.Add(', Desco    =:P5');
    Dmod.QrUpd.SQL.Add(', Valor    =:P6');
    if LaTipo.Caption = CO_INCLUSAO then
      Dmod.QrUpd.SQL.Add(', Controle =:P7, Codigo   =:Py')
    else
      Dmod.QrUpd.SQL.Add('WHERE Controle=:px AND Codigo=:Py AND Conta=:Pz');
    //
    Dmod.QrUpd.Params[0].AsInteger := Produto;
    Dmod.QrUpd.Params[1].AsFloat := Preco;
    Dmod.QrUpd.Params[2].AsFloat := Bruto;
    Dmod.QrUpd.Params[3].AsFloat := Tara;
    Dmod.QrUpd.Params[4].AsFloat := Impur;
    Dmod.QrUpd.Params[5].AsFloat := Desco;
    Dmod.QrUpd.Params[6].AsFloat := Total;
    Dmod.QrUpd.Params[7].AsFloat := Controle;
    Dmod.QrUpd.Params[8].AsInteger := Codigo;
    if LaTipo.Caption = CO_ALTERACAO then
      Dmod.QrUpd.Params[9].AsInteger := Conta;
    Dmod.QrUpd.ExecSQL;

//  except
    Screen.Cursor := Cursor;
    //Exit;
//  end;
  Close;
end;

procedure TFmProdutosCEdit.EdBrutoExit(Sender: TObject);
begin
  if not MLAGeral.CasasCorretas(EdBruto.Text, QrProdutosCasas.Value, True) then
  begin
    EdBruto.SetFocus;
    Exit;
  end;
  EdBruto.Text := Geral.TFT(EdBruto.Text, QrProdutosCasas.Value, siPositivo);
  CalculaOnEdit(1);
end;

procedure TFmProdutosCEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdBruto.SetFocus;
  EdProduto.SetFocus;
  CalculaOnEdit(0);
end;

procedure TFmProdutosCEdit.EdTotalExit(Sender: TObject);
begin
  EdTotal.Text := Geral.TFT(EdTotal.Text, 2, siPositivo);
  CalculaOnEdit(6);
end;

procedure TFmProdutosCEdit.EdTaraExit(Sender: TObject);
begin
  EdTara.Text := Geral.TFT(EdTara.Text, 3, siPositivo);
  CalculaOnEdit(2);
end;

procedure TFmProdutosCEdit.EdImpurExit(Sender: TObject);
begin
  EdImpur.Text := Geral.TFT(EdImpur.Text, 3, siPositivo);
  CalculaOnEdit(4);
end;

procedure TFmProdutosCEdit.EdPrecoExit(Sender: TObject);
begin
  EdPreco.Text := Geral.TFT(EdPreco.Text, 4, siPositivo);
  CalculaOnEdit(0);
end;

procedure TFmProdutosCEdit.EdDescoExit(Sender: TObject);
begin
  EdDesco.Text := Geral.TFT(EdDesco.Text, 2, siPositivo);
  CalculaOnEdit(5);
end;

procedure TFmProdutosCEdit.EdSubToExit(Sender: TObject);
begin
  EdSubTo.Text := Geral.TFT(EdSubTo.Text, 2, siPositivo);
  CalculaOnEdit(3);
end;

end.
