object FmMercadoriasAtualiza: TFmMercadoriasAtualiza
  Left = 309
  Top = 292
  Caption = 'Atualiza'#231#227'o de Mercadorias'
  ClientHeight = 219
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 171
    Width = 372
    Height = 48
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 173
    ExplicitWidth = 380
    object BtRefresh: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Inicia atualiza'#231#227'o nas mercadorias'
      Caption = '&Refresh'
      TabOrder = 0
      OnClick = BtRefreshClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Left = 266
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 372
    Height = 131
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 380
    ExplicitHeight = 133
    object LaAguarde: TLabel
      Left = 24
      Top = 16
      Width = 49
      Height = 13
      Caption = 'Aguarde...'
    end
    object PnTempo: TLabel
      Left = 84
      Top = 16
      Width = 21
      Height = 13
      Caption = '0:00'
    end
    object Label1: TLabel
      Left = 20
      Top = 76
      Width = 50
      Height = 13
      Caption = 'Registros: '
    end
    object PnRegistros: TLabel
      Left = 72
      Top = 76
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label2: TLabel
      Left = 148
      Top = 76
      Width = 59
      Height = 13
      Caption = 'Mercadoria: '
    end
    object PnMercadoria: TLabel
      Left = 212
      Top = 76
      Width = 48
      Height = 13
      Caption = '00000000'
    end
    object Progress1: TProgressBar
      Left = 20
      Top = 96
      Width = 337
      Height = 16
      TabOrder = 0
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 372
    Height = 40
    Align = alTop
    
    
    
    
    Caption = 'Atualiza'#231#227'o de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    ExplicitWidth = 380
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 376
      Height = 36
      Align = alClient
      Transparent = True
    end
  end
  object Timer1: TTimer
    Left = 240
    Top = 56
  end
  object QrCustos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM produtos'
      'ORDER BY Codigo')
    Left = 144
    Top = 56
    object QrCustosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
