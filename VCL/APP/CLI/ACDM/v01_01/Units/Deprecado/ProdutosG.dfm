object FmProdutosG: TFmProdutosG
  Left = 346
  Top = 177
  Caption = 'MER-GRUPR-001 :: Grupo de Produtos'
  ClientHeight = 250
  ClientWidth = 713
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 713
    Height = 154
    Align = alClient
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 135
      Height = 13
      Caption = 'Nome do grupo de produtos:'
      FocusControl = DBEdNome
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 105
      Width = 711
      Height = 48
      Align = alBottom
      TabOrder = 0
      Visible = False
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 612
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
    object DBEdCodigo: TDBEdit
      Left = 16
      Top = 24
      Width = 100
      Height = 21
      Hint = 'N'#186' do banco'
      TabStop = False
      DataField = 'Codigo'
      DataSource = DsProdutosG
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 3
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
    end
    object DBEdNome: TDBEdit
      Left = 16
      Top = 72
      Width = 280
      Height = 21
      Hint = 'Nome do banco'
      DataField = 'Nome'
      DataSource = DsProdutosG
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object EdCodigo: TdmkEdit
      Left = 124
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdNome: TEdit
      Left = 304
      Top = 72
      Width = 280
      Height = 21
      MaxLength = 100
      TabOrder = 4
      Visible = False
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 202
    Width = 713
    Height = 48
    Align = alBottom
    TabOrder = 0
    object LaRegistro: TLabel
      Left = 173
      Top = 1
      Width = 26
      Height = 13
      Align = alClient
      Caption = '[N]: 0'
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
        NumGlyphs = 2
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
        NumGlyphs = 2
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
        NumGlyphs = 2
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
        NumGlyphs = 2
      end
    end
    object Panel3: TPanel
      Left = 243
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Exclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        NumGlyphs = 2
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
        NumGlyphs = 2
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 713
    Height = 48
    Align = alTop
    Caption = '     Grupo de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 634
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 641
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 216
      Top = 1
      Width = 418
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 217
      ExplicitTop = 2
      ExplicitWidth = 424
      ExplicitHeight = 44
    end
    object PainelBotoes: TPanel
      Left = 1
      Top = 1
      Width = 215
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object QrProdutosG: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrProdutosGAfterOpen
    AfterScroll = QrProdutosGAfterScroll
    SQL.Strings = (
      'SELECT * FROM produtosg'
      'WHERE Codigo > 0')
    Left = 432
    Top = 72
    object QrProdutosGCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DNSTORE001.produtosg.Codigo'
      DisplayFormat = '000'
    end
    object QrProdutosGNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DNSTORE001.produtosg.Nome'
      Size = 128
    end
    object QrProdutosGLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNSTORE001.produtosg.Lk'
    end
  end
  object DsProdutosG: TDataSource
    DataSet = QrProdutosG
    Left = 460
    Top = 72
  end
end
