unit ProdutosNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UnMLAGeral,
  UmySQLModule, ComCtrls, Grids, DBGrids, UnMsgInt, mySQLDbTables, dmkGeral,
  Variants, dmkDBLookupComboBox, dmkEditCB, dmkEdit, UnDmkEnums;

type
  TFmProdutosNew = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    Label2: TLabel;
    EdNome: TdmkEdit;
    EdPrecoC: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdICMS: TdmkEdit;
    Label5: TLabel;
    EdRICMS: TdmkEdit;
    Label6: TLabel;
    EdIPI: TdmkEdit;
    Label7: TLabel;
    EdRIPI: TdmkEdit;
    Label8: TLabel;
    EdFrete: TdmkEdit;
    Label9: TLabel;
    EdPrecoV: TdmkEdit;
    Label10: TLabel;
    EdJuros: TdmkEdit;
    Label11: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    CkAtivo: TCheckBox;
    QrProdutosG: TmySQLQuery;
    DsProdutosG: TDataSource;
    QrProdutosGCodigo: TIntegerField;
    QrProdutosGNome: TWideStringField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    EdNome2: TdmkEdit;
    Label12: TLabel;
    CkImprime: TCheckBox;
    EdCasas: TdmkEdit;
    Label13: TLabel;
    CkControla: TCheckBox;
    SpeedButton1: TSpeedButton;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdNome2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmProdutosNew: TFmProdutosNew;
  Fechar : Boolean;

implementation

uses UnMyObjects, Module, Entidades, Produtos, ProdutosG, Principal, MyDBCheck;


{$R *.DFM}

procedure TFmProdutosNew.BtConfirmaClick(Sender: TObject);
var
  Produto, Grupo: Integer;
  Juros, PrecoC, PrecoV, ICMS, RICMS, IPI, RIPI, Frete: Double;
  Ativo, Imprime, Controlado: String[1];
begin

  if trim(EdNome.Text)=CO_VAZIO then
  begin
    Application.MessageBox('Informe um nome para o produto.',
    'Falta de informa��es', MB_OK+MB_ICONEXCLAMATION);
    EdNome.SetFocus;
    Exit;
  end;

  if CBGrupo.KeyValue = NULL then
  begin
    Application.MessageBox('Informe um grupo para o produto.',
    'Falta de informa��es', MB_OK+MB_ICONEXCLAMATION);
    EdGrupo.SetFocus;
    Exit;
  end else Grupo := CBGrupo.KeyValue;

  Produto := Geral.IMV(EdCodigo.Text);
  PrecoC  := Geral.DMV(EdPrecoC.Text);
  PrecoV  := Geral.DMV(EdPrecoV.Text);
  ICMS    := Geral.DMV(EdICMS.Text);
  RICMS   := Geral.DMV(EdRICMS.Text);
  IPI     := Geral.DMV(EdIPI.Text);
  RIPI    := Geral.DMV(EdRIPI.Text);
  Frete   := Geral.DMV(EdFrete.Text);
  Juros   := Geral.DMV(EdJuros.Text);
  if CkAtivo.Checked then Ativo := '1' else Ativo := '0';
  if CkImprime.Checked then Imprime := 'V' else Imprime := 'F';
  if RICMS > ICMS then
  begin
    Application.MessageBox('R.ICMS n�o pode ser superior ao ICMS.',
    'Erro nas informa��es', MB_OK+MB_ICONERROR);
    EdRICMS.SetFocus;
    Exit;
  end;

  if RIPI > IPI then
  begin
    Application.MessageBox('R.IPI n�o pode ser superior ao IPI.',
    'Erro nas informa��es', MB_OK+MB_ICONERROR);
    EdRIPI.SetFocus;
    Exit;
  end;

  if CkControla.Checked then Controlado := 'V' else Controlado := 'F';
  //
  Dmod.QrUpdU.SQL.Clear;
  if (LaTipo.Caption = CO_INCLUSAO) or (LaTipo.Caption = CO_DUPLICACAO) then
       Dmod.QrUpdU.SQL.Add('INSERT INTO Produtos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE      Produtos SET ');
  Dmod.QrUpdU.SQL.Add('Nome       =:P00');
  Dmod.QrUpdU.SQL.Add(', PrecoC   =:P01');
  Dmod.QrUpdU.SQL.Add(', PrecoV   =:P02');
  Dmod.QrUpdU.SQL.Add(', ICMS     =:P03');
  Dmod.QrUpdU.SQL.Add(', RICMS    =:P04');
  Dmod.QrUpdU.SQL.Add(', IPI      =:P05');
  Dmod.QrUpdU.SQL.Add(', RIPI     =:P06');
  Dmod.QrUpdU.SQL.Add(', Frete    =:P07');
  Dmod.QrUpdU.SQL.Add(', Juros    =:P08');
  Dmod.QrUpdU.SQL.Add(', Ativo    =:P09');
  Dmod.QrUpdU.SQL.Add(', Grupo    =:P10');
  Dmod.QrUpdU.SQL.Add(', Imprime  =:P11');
  Dmod.QrUpdU.SQL.Add(', Nome2    =:P12');
  Dmod.QrUpdU.SQL.Add(', Controla =:P13');
  if (LaTipo.Caption = CO_INCLUSAO) or (LaTipo.Caption = CO_DUPLICACAO) then
       Dmod.QrUpdU.SQL.Add(', DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add(', DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := EdNome.Text;
  Dmod.QrUpdU.Params[01].AsFloat   := PrecoC;
  Dmod.QrUpdU.Params[02].AsFloat   := PrecoV;
  Dmod.QrUpdU.Params[03].AsFloat   := ICMS;
  Dmod.QrUpdU.Params[04].AsFloat   := RICMS;
  Dmod.QrUpdU.Params[05].AsFloat   := IPI;
  Dmod.QrUpdU.Params[06].AsFloat   := RIPI;
  Dmod.QrUpdU.Params[07].AsFloat   := Frete;
  Dmod.QrUpdU.Params[08].AsFloat   := Juros;
  Dmod.QrUpdU.Params[09].AsString  := Ativo;
  Dmod.QrUpdU.Params[10].AsInteger := Grupo;
  Dmod.QrUpdU.Params[11].AsString  := Imprime;
  Dmod.QrUpdU.Params[12].AsString  := EdNome2.Text;
  Dmod.QrUpdU.Params[13].AsString  := Controlado;
  //
  Dmod.QrUpdU.Params[14].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[15].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[16].AsInteger := Produto;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Produto, Dmod.MyDB, 'Produtos', 'Codigo');
  Close;
end;

procedure TFmProdutosNew.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if (LaTipo.Caption = CO_INCLUSAO) or (LaTipo.Caption = CO_DUPLICACAO) then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Produtos', Codigo)
  else UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Produtos', 'Codigo');
  FmProdutos.LaTipo.Caption := CO_TRAVADO;
  Close;
end;

procedure TFmProdutosNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdPrecoC.SetFocus;
  EdNome.SetFocus;
end;

procedure TFmProdutosNew.FormCreate(Sender: TObject);
begin
  QrProdutosG.Open;
end;

procedure TFmProdutosNew.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, FmProdutos.FTitulo, Image1, PainelTitulo, True, 0);
end;

procedure TFmProdutosNew.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmProdutosG, FmProdutosG, afmoNegarComAviso) then
  begin
    FmProdutosG.ShowModal;
    FmProdutosG.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdGrupo, CBGrupo, QrProdutosG, VAR_CADASTRO);
  end;
end;

procedure TFmProdutosNew.EdNome2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F4 then EdNome2.Text := EdNome.Text;
end;

end.
