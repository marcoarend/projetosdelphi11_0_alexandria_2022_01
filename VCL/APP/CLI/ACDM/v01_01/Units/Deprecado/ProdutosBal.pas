unit ProdutosBal;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, Grids, DBGrids, Menus, mySQLDbTables,
  ComCtrls, UnitAcademy, dmkGeral, Variants, UnDmkProcFunc;

type
  TFmProdutosBal = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtTrava: TBitBtn;
    DsBalancos: TDataSource;
    DBGFunci: TDBGrid;
    DsBalancosIts: TDataSource;
    BtIncluiIts: TBitBtn;
    BtAlteraIts: TBitBtn;
    BtExcluiIts: TBitBtn;
    DsArtigo: TDataSource;
    PainelDados1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    RGInsumo: TRadioGroup;
    RGFornece: TRadioGroup;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PainelDados2: TPanel;
    Progress1: TProgressBar;
    Panel1: TPanel;
    Panel2: TPanel;
    PnRegistros: TPanel;
    Panel4: TPanel;
    PnTempo: TPanel;
    BtRefresh: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    QrBalancos: TmySQLQuery;
    QrBalancosIts: TmySQLQuery;
    QrArtigo: TmySQLQuery;
    QrAtualiza: TmySQLQuery;
    QrMax: TmySQLQuery;
    QrBalancosPeriodo: TIntegerField;
    QrBalancosEstqQ: TFloatField;
    QrBalancosEstqV: TFloatField;
    QrBalancosDataCad: TDateField;
    QrBalancosDataAlt: TDateField;
    QrBalancosUserCad: TIntegerField;
    QrBalancosUserAlt: TIntegerField;
    QrBalancosConfirmado: TWideStringField;
    QrBalancosLk: TIntegerField;
    QrSoma: TmySQLQuery;
    QrBalancosPeriodo2: TWideStringField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill002: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    QrArtigoCodigo: TIntegerField;
    QrArtigoNome: TWideStringField;
    QrBalancosItsPeriodo: TIntegerField;
    QrBalancosItsProduto: TIntegerField;
    QrBalancosItsConta: TIntegerField;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrBalancosItsAnteQ: TFloatField;
    QrBalancosItsAnteV: TFloatField;
    QrBalancosItsNome: TWideStringField;
    QrBalancosItsGrupo: TIntegerField;
    QrBalancosItsNOMEGRUPO: TWideStringField;
    QrBalancosItsNOMEMERCADORIA: TWideStringField;
    QrSomaEstqQ: TFloatField;
    QrSomaEstqV: TFloatField;
    QrMaxPeriodo: TIntegerField;
    QrAtualizaProduto: TIntegerField;
    BtPrivativo: TBitBtn;
    QrProdutos: TmySQLQuery;
    QrProdutosCodigo: TIntegerField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEstqV: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrBalancosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrBalancosCalcFields(DataSet: TDataSet);
    procedure DBGFunciKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure QrBalancosItsAfterOpen(DataSet: TDataSet);
    procedure BtExcluiItsClick(Sender: TObject);
    procedure RGInsumoClick(Sender: TObject);
    procedure RGForneceClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrBalancosItsCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrBalancosAfterScroll(DataSet: TDataSet);
    procedure BtPrivativoClick(Sender: TObject);
  private

    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure ReindexaTabela(Locate: Boolean);
    procedure AdicionaProduto;
    procedure AlteraProduto;
    procedure SomaBalanco;

  public
    { Public declarations }

    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
  end;

var
  FmProdutosBal: TFmProdutosBal;

implementation
  uses UnMyObjects, Module, ProdutosBalNew, ProdutosBalEdit, Principal, ProdutosImp,
  ProdutosImprime, UnDmkEnums;

{$R *.DFM}

var
  TimerIni: TDateTime;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutosBal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmProdutosBal.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrBalancosPeriodo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdutosBal.DefParams;
begin
  VAR_GOTOTABELA := 'Balancos';
  VAR_GOTOMYSQLTABLE := QrBalancos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_PERIODO;
  VAR_GOTONOME := '';//CO_NOME;
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM balancos');
  VAR_SQLx.Add('WHERE Periodo > 0');
  //
  VAR_SQL1.Add('AND Periodo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmProdutosBal.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelControle.Visible:=False;
    PainelConfirma.Visible:=True;
  end else begin
    PainelControle.Visible:=True;
    PainelConfirma.Visible:=False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmProdutosBal.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProdutosBal.AlteraRegistro;
begin
  MostraEdicao(True, CO_INCLUSAO, 0);
  UMyMod.UpdLockY(QrBalancosPeriodo.Value, Dmod.MyDB, 'Balancos', 'Periodo');
end;

procedure TFmProdutosBal.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmProdutosBalNew, FmProdutosBalNew);
  finally
    Screen.Cursor := Cursor;
  end;
  FmProdutosBalNew.ShowModal;
  FmProdutosBalNew.Destroy;
end;

procedure TFmProdutosBal.QueryPrincipalAfterOpen;
begin
  if (QrBalancosPeriodo.Value = UnAcademy.VerificaBalanco) then
    BtAltera.Enabled := True
  else
  begin
    QrMax.Close;
    QrMax.Open;
    if QrMaxPeriodo.Value <> QrBalancosPeriodo.Value then
    begin
      BtAltera.Enabled := False;
      ReindexaTabela(False);
    end else BtAltera.Enabled := True;
  end;
  ///
end;

procedure TFmProdutosBal.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProdutosBal.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutosBal.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutosBal.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutosBal.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutosBal.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmProdutosBal.BtSaidaClick(Sender: TObject);
begin
  VAR_CAIXA := QrBalancosPeriodo.Value;
  Close;
end;

procedure TFmProdutosBal.BtTravaClick(Sender: TObject);
var
  Periodo : Integer;
begin
  SomaBalanco;
  Periodo := QrBalancosPeriodo.Value;
  UMyMod.UpdUnlockY(Periodo, Dmod.MyDB, 'Balancos', 'Periodo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Periodo, Periodo);
  //
  try
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE produtos SET EstqQ=0, EstqV=0');
    Dmod.QrUpdU.ExecSQL;
    //
    QrAtualiza.Close;
    //QrAtualiza.Params[0].AsInteger := Periodo;
    QrAtualiza.Open;
    Progress1.Position := 0;
    Progress1.Max := GOTOy.Registros(QrAtualiza);
    PainelDados2.Visible := True;
    PainelDados2.Refresh;
    PainelControle.Refresh;
    Panel1.Refresh;
    Panel2.Refresh;
    TimerIni := Now();
    while not QrAtualiza.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
      PnRegistros.Refresh;
      PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
      PnTempo.Refresh;
      UnAcademy.AtualizaEstoqueMercadoria(QrAtualizaProduto.Value, aeMsg, CO_VAZIO);
      QrAtualiza.Next;
    end;
    Application.MessageBox('Atualiza��o de estoques finalizada', 'Informa��o',
    MB_OK+MB_ICONINFORMATION);
    PainelDados2.Visible := False;
    QrAtualiza.Close;
    if QrBalancosConfirmado.Value = 'F' then FmPrincipal.RefreshMercadorias;
  except
    raise;
    Application.MessageBox('Ocorreu um erro nas atualiza��es de estoque.',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmProdutosBal.FormCreate(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmProdutosBal.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrBalancosPeriodo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosBal.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmProdutosBal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmProdutosBal.QrBalancosAfterOpen(DataSet: TDataSet);
begin
  GOTOy.BtEnabled(QrBalancosPeriodo.Value, False);
  QueryPrincipalAfterOpen;
end;

procedure TFmProdutosBal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosBal.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmProdutosBal.QrBalancosCalcFields(DataSet: TDataSet);
begin
  QrBalancosPeriodo2.Value := ' Balan�o de '+
  Geral.Maiusculas(MLAGeral.PrimeiroDiaDoPeriodo(QrBalancosPeriodo.Value, dtTexto), False);
end;

procedure TFmProdutosBal.DBGFunciKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if LaTipo.Caption <> CO_TRAVADO then
  begin
    if (key=13)        then AlteraProduto;
    if (key=VK_INSERT) then AdicionaProduto;
  end;
end;

procedure TFmProdutosBal.ReindexaTabela(Locate: Boolean);
var
  Indice, Produto : Integer;
  Ordem, Descricao: String;
  Fornecedo: Double;
begin
  Indice := (RGInsumo.ItemIndex*10)+ RGFornece.ItemIndex;
  case Indice of
     0 : Ordem := 'ORDER BY pg.Codigo, ar.Nome';
     1 : Ordem := 'ORDER BY ar.Nome';
    10 : Ordem := 'ORDER BY pg.Codigo, cu.Produto';
    11 : Ordem := 'ORDER BY cu.Produto';
  end;
  //
  Fornecedo := VAR_LOCATE011;
  Descricao := VAR_LOCATE012;
  Produto   := VAR_LOCATE014;
  //
  QrBalancosIts.Close;
  QrBalancosIts.SQL.Clear;
  QrBalancosIts.SQL.Add('SELECT cu.*, ar.Nome, ar.Grupo,');
  QrBalancosIts.SQL.Add('pg.Nome NOMEGRUPO, ar.Nome NOMEMERCADORIA');
  QrBalancosIts.SQL.Add('FROM balancosits cu, Produtos ar,');
  QrBalancosIts.SQL.Add('ProdutosG pg');
  QrBalancosIts.SQL.Add('WHERE cu.Periodo=:P0');
  QrBalancosIts.SQL.Add('AND ar.Codigo=cu.Produto');
  QrBalancosIts.SQL.Add('AND pg.Codigo=ar.Grupo');
  QrBalancosIts.SQL.Add('');
  QrBalancosIts.SQL.Add(Ordem);
  QrBalancosIts.Params[0].AsInteger := QrBalancosPeriodo.Value;
  QrBalancosIts.Open;
  if Locate then
  begin
    case Indice of
       0: QrBalancosIts.Locate('Grupo;NOMEMERCADORIA', VarArrayOf([Fornecedo, Descricao]), []);
       1: QrBalancosIts.Locate('NOMEMERCADORIA', Descricao, []);
      10: QrBalancosIts.Locate('Grupo;Produto', VarArrayOf([Fornecedo, Produto]), []);
      11: QrBalancosIts.Locate('Produto', Produto, []);
     end;
  end;
end;

procedure TFmProdutosBal.AdicionaProduto;
var
  ClickedOK: Boolean;
  CodTxt: String;
begin
  ClickedOK := InputQuery('Edi��o de Mercadoria', 'Informe o c�digo:', CodTxt);
  if ClickedOK then
  begin
    QrArtigo.Close;
    QrArtigo.Params[0].AsString := CodTxt;
    QrArtigo.Open;
    if GOTOy.Registros(QrArtigo)>0 then
    begin
      //VAR_LOCATE011 := QrArtigoFORNECEDO.Value;
      VAR_LOCATE012 := QrArtigoNome.Value;
      VAR_LOCATE014 := QrArtigoCodigo.Value;
      Application.CreateForm(TFmProdutosBalEdit, FmProdutosBalEdit);
      with FmProdutosBalEdit do
      begin
        LaTipo.Caption := CO_INCLUSAO;
        EdMercadoria.Text := IntToStr(QrArtigoCodigo.Value);
        EdDescricao.Text := QrArtigoNome.Value;
        ShowModal;
        Destroy;
      end;
    end else ShowMessage('Mercadoria inexistente');
    QrArtigo.Close;
  end;
  SomaBalanco;
  ReindexaTabela(True);
end;

procedure TFmProdutosBal.AlteraProduto;
var
  Produto: Integer;
begin
  //VAR_LOCATE011 := QrBalancosItsFORNECEDO.Value;
  VAR_LOCATE012 := QrBalancosItsNOMEGRUPO.Value;
  VAR_LOCATE014 := QrBalancosItsProduto.Value;
  Produto := QrBalancosItsProduto.Value;
  Application.CreateForm(TFmProdutosBalEdit, FmProdutosBalEdit);
  with FmProdutosBalEdit do
  begin
    LaTipo.Caption := CO_ALTERACAO;
    EdMercadoria.Text := IntToStr(Produto);
    EdDescricao.Text := QrBalancosItsNOMEMERCADORIA.Value;
    EdPecas.Text := FloatToStr(QrBalancosItsEstqQ.Value);
    EdVTota.Text := FloatToStr(QrBalancosItsEstqV.Value);
    ShowModal;
    Destroy;
  end;
  SomaBalanco;
  ReindexaTabela(True);
end;

procedure TFmProdutosBal.BtIncluiItsClick(Sender: TObject);
begin
  AdicionaProduto;
end;

procedure TFmProdutosBal.BtAlteraItsClick(Sender: TObject);
begin
  AlteraProduto;
end;

procedure TFmProdutosBal.QrBalancosItsAfterOpen(DataSet: TDataSet);
begin
  if GOTOy.Registros(QrBalancosIts) > 0 then
  begin
    BtAlteraIts.Enabled := True;
    BtExclui.Enabled := False;
  end else begin
    BtAlteraIts.Enabled := False;
    if GOTOy.Registros(QrBalancos) > 0 then
    BtExclui.Enabled := True else BtExclui.Enabled := False;
  end;
end;

procedure TFmProdutosBal.SomaBalanco;
begin
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrBalancosPeriodo.Value;
  QrSoma.Open;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE balancos SET ');
  Dmod.QrUpdU.SQL.Add('EstqQ=:P0, EstqV=:P1 ');
  Dmod.QrUpdU.SQL.Add('WHERE Periodo=:P2');
  Dmod.QrUpdU.Params[0].AsFloat   := QrSomaEstqQ.Value;
  Dmod.QrUpdU.Params[1].AsFloat   := QrSomaEstqV.Value;
  Dmod.QrUpdU.Params[2].AsInteger := QrBalancosPeriodo.Value;
  Dmod.QrUpdU.ExecSQL;
  //
  LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
end;

procedure TFmProdutosBal.BtExcluiItsClick(Sender: TObject);
var
  Periodo: Integer;
  Produto: Integer;
  Conta: Double;
begin
  QrBalancosIts.DisableControls;
  if Application.MessageBox('Deseja excluir este item de balan�o?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    Periodo := QrBalancosItsPeriodo.Value;
    Produto := QrBalancosItsProduto.Value;
    Conta   := QrBalancosItsConta.Value;
    //
    QrBalancosIts.Prior;
    if QrBalancosItsConta.Value = Conta then QrBalancosIts.Next;
    //VAR_LOCATE011 := QrBalancosItsFORNECEDO.Value;
    VAR_LOCATE012 := QrBalancosItsNOMEMERCADORIA.Value;
    VAR_LOCATE014 := QrBalancosItsProduto.Value;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM balancosits WHERE Periodo=:P0 ');
    Dmod.QrUpdU.SQL.Add('AND Produto=:P1 AND Conta=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := Periodo;
    Dmod.QrUpdU.Params[1].AsInteger  := Produto;
    Dmod.QrUpdU.Params[2].AsFloat   := Conta;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
  QrBalancosIts.EnableControls;
  SomaBalanco;
end;

procedure TFmProdutosBal.RGInsumoClick(Sender: TObject);
begin
  ReindexaTabela(True);
end;

procedure TFmProdutosBal.RGForneceClick(Sender: TObject);
begin
  ReindexaTabela(True);
end;

procedure TFmProdutosBal.BtExcluiClick(Sender: TObject);
var
  Periodo: Integer;
begin
  if Application.MessageBox('Deseja excluir este balan�o?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    Periodo := QrBalancosPeriodo.Value;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM balancos WHERE Periodo=:P0 ');
    Dmod.QrUpdU.Params[0].AsInteger := Periodo;
    Dmod.QrUpdU.ExecSQL;
    //
    QrMax.Close;
    QrMax.Open;
    //
    LocCod(QrMaxPeriodo.Value, QrMaxPeriodo.Value);
  end;
end;

procedure TFmProdutosBal.BtRefreshClick(Sender: TObject);
var
  Periodo: Integer;
  Sit: String;
begin
  if QrBalancosConfirmado.Value = 'V' then Sit := 'F' else Sit := 'V';
  Periodo := QrBalancosPeriodo.Value;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE balancos SET Confirmado="'+Sit
  +'" WHERE Periodo='+IntToStr(Periodo));
  Dmod.QrUpdM.ExecSQL;
  LocCod(Periodo, Periodo);
end;

procedure TFmProdutosBal.QrBalancosItsCalcFields(DataSet: TDataSet);
begin
  QrBalancosItsNOMEGRUPO.Value := 'N�o dispon�vel ainda.';
end;

procedure TFmProdutosBal.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, QrBalancosPeriodo2.Value, Image1, PainelTitulo, True, 5);
end;

procedure TFmProdutosBal.SbImprimeClick(Sender: TObject);
begin
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'ProdutosImp', 0) then Exit;
  Application.CreateForm(TFmProdutosImprime, FmProdutosImprime);
  //FmProdutosImp.ShowModal;
  with FmProdutosImprime do
  begin
    RGTipo.ItemIndex := 3;
    ImprimeBalanco;
    Destroy;
  end;
end;

procedure TFmProdutosBal.QrBalancosAfterScroll(DataSet: TDataSet);
begin
  PainelTitulo.Caption := '                              '+QrBalancosPeriodo2.Value;
  MLAGeral.LoadTextBitmapToPanel(0, 0, QrBalancosPeriodo2.Value, Image1, PainelTitulo, True, 5);
  ReindexaTabela(False);
end;

procedure TFmProdutosBal.BtPrivativoClick(Sender: TObject);
var
  Conta: Double;
begin
  if Application.MessageBox(PChar('Todos itens deste balan�o ser�o excluidos, '
  +'e ser�o inclu�dos itens para zerar estoques negativos. Deseja executar '+
  'esta a��o assim mesmo?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM balancosits WHERE Periodo=:P0 ');
    Dmod.QrUpdU.Params[0].AsInteger := QrBalancosPeriodo.Value;
    Dmod.QrUpdU.ExecSQL;
    //
  end;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE produtos SET EstqQ=0, EstqV=0');
  Dmod.QrUpdU.ExecSQL;

  //////////////////////////////////////////////////////////////////////////////

  QrAtualiza.Close;
  QrAtualiza.Open;
  Progress1.Position := 0;
  Progress1.Max := QrAtualiza.RecordCount;
  PainelDados2.Visible := True;
  PainelDados2.Refresh;
  PainelControle.Refresh;
  Panel1.Refresh;
  Panel2.Refresh;
  TimerIni := Now();
  while not QrAtualiza.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    UnAcademy.AtualizaEstoqueMercadoria(QrAtualizaProduto.Value, aeNenhum, CO_VAZIO);
    QrAtualiza.Next;
  end;
  //PainelDados2.Visible := False;
  QrAtualiza.Close;

  //////////////////////////////////////////////////////////////////////////////

  QrProdutos.Close;
  QrProdutos.Open;
  Progress1.Position := 0;
  Progress1.Max := QrProdutos.RecordCount;
  while not QrProdutos.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    //
    Conta := UMyMod.BuscaEmLivreY_Double(
    Dmod.MyDB, 'Livres', 'Controle', 'BalancosIts','BalancosIts','Conta');
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO balancosits SET EstqQ=:P0, ');
    Dmod.QrUpdU.SQL.Add('EstqV=:P1, Produto=:P2, Periodo=:P3, Conta=:P4');
    Dmod.QrUpdU.Params[00].AsFloat   := -QrProdutosEstqQ.Value;
    Dmod.QrUpdU.Params[01].AsFloat   := -QrProdutosEstqV.Value;
    Dmod.QrUpdU.Params[02].AsInteger :=  QrProdutosCodigo.Value;
    Dmod.QrUpdU.Params[03].AsInteger :=  QrBalancosPeriodo.Value;
    Dmod.QrUpdU.Params[04].AsFloat   :=  Conta;
    Dmod.QrUpdU.ExecSQL;
    QrProdutos.Next;
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrProdutos.Close;
  PainelDados2.Visible := False;
  LocCod(QrBalancosPeriodo.Value, QrBalancosPeriodo.Value);
end;

end.

