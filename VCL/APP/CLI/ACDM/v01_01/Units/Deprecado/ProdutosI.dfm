object FmProdutosI: TFmProdutosI
  Left = 355
  Top = 179
  Caption = 'MER-ESTOQ-001 :: Estoque Inicial de Produtos'
  ClientHeight = 442
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelGrid: TPanel
    Left = 0
    Top = 0
    Width = 694
    Height = 392
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 49
      Width = 692
      Height = 342
      Align = alClient
      DataSource = DsProdutos
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnKeyDown = DBGrid1KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 278
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EIniQ'
          Title.Alignment = taRightJustify
          Title.Caption = 'Quantidade'
          Width = 91
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EIniV'
          Title.Alignment = taRightJustify
          Title.Caption = 'Custo total'
          Width = 97
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRECOMEDIOINI'
          Title.Alignment = taRightJustify
          Title.Caption = 'Custo Inicial'
          Width = 101
          Visible = True
        end>
    end
    object PainelTitulo: TPanel
      Left = 1
      Top = 1
      Width = 692
      Height = 48
      Align = alTop
      Caption = 'Estoque Inicial de Produtos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 1
      object Image1: TImage
        Left = 1
        Top = 1
        Width = 690
        Height = 46
        Align = alClient
        Transparent = True
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 550
        ExplicitHeight = 44
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 392
    Width = 694
    Height = 50
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 140
      Top = 6
      Width = 66
      Height = 13
      Caption = 'Estoque qtde:'
    end
    object Label2: TLabel
      Left = 232
      Top = 6
      Width = 68
      Height = 13
      Caption = 'Estoque valor:'
    end
    object Label3: TLabel
      Left = 344
      Top = 6
      Width = 88
      Height = 13
      Caption = 'Pre'#231'o m'#233'dio estq.:'
    end
    object RGInsumo: TRadioGroup
      Left = 0
      Top = 2
      Width = 133
      Height = 42
      Caption = ' Ordem das Mercadorias: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Nome'
        'Codigo')
      TabOrder = 0
      OnClick = RGInsumoClick
    end
    object EdPecas: TDBEdit
      Left = 140
      Top = 22
      Width = 90
      Height = 21
      DataField = 'EstqQ'
      DataSource = DsProdutos
      TabOrder = 1
    end
    object EdValor: TDBEdit
      Left = 232
      Top = 22
      Width = 109
      Height = 21
      DataField = 'EstqV'
      DataSource = DsProdutos
      TabOrder = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 578
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object DBEdit1: TDBEdit
      Left = 344
      Top = 22
      Width = 89
      Height = 21
      DataField = 'PRECOMEDIOESTQ'
      DataSource = DsProdutos
      TabOrder = 4
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 104
    Top = 76
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProdutosCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM produtos'
      'WHERE Codigo>0'
      'ORDER BY Codigo')
    Left = 76
    Top = 76
    object QrProdutosPRECOMEDIOINI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOMEDIOINI'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrProdutosPRECOMEDIOESTQ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECOMEDIOESTQ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrProdutosEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
    object QrProdutosEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrProdutosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrProduto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 76
    Top = 104
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrProdutoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProdutoNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrProdutoPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrProdutoPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrProdutoICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrProdutoRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrProdutoIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrProdutoRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrProdutoFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrProdutoJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrProdutoEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrProdutoEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutoEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrProdutoEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrProdutoImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrProdutoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdutoControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrProdutoCasas: TIntegerField
      FieldName = 'Casas'
    end
    object QrProdutoEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
    object QrProdutoEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrProdutoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProdutoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
end
