unit Produtos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ClipBrd, ZCF2, Grids, DBGrids, ResIntStrings, UnMsgInt, UnGOTOy,
  UnInternalConsts, UnMLAGeral, UmySQLModule, ComCtrls, Menus, dmkGeral,
  UnInternalConsts3, mySQLDbTables, UnMySQLCuringa, UnitAcademy,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmProdutos = class(TForm)
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaCredito: TFloatField;
    QrSomaDebito: TFloatField;
    PainelDados: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrProdutosCodigo: TIntegerField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosLk: TIntegerField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    QrProdutosCUSTOMEDIO: TFloatField;
    QrProdutosCUSTO: TFloatField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    QrProdutosJuros: TFloatField;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNOMEGRUPO: TWideStringField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    PainelNavega: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    QrProdutosImprime: TWideStringField;
    Label3: TLabel;
    DBEdit6: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBEdit17: TDBEdit;
    Label17: TLabel;
    QrProdutosControla: TWideStringField;
    QrProdutosCasas: TIntegerField;
    BtRefresh: TBitBtn;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosEstqV_G: TFloatField;
    QrProdutosEstqQ_G: TFloatField;
    QrProdutosAlterWeb: TSmallintField;
    QrProdutosAtivo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrProdutosAfterScroll(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrProdutosCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
  private
    { Private declarations }
    procedure CriaOForm;
    procedure SubQuery1Reopen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);

  public
    FTitulo: String;
    { Public declarations }
  end;

var
  FmProdutos: TFmProdutos;

implementation

uses UnMyObjects, Curinga, Module, ProdutosNew, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmProdutos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrProdutosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutos.DefParams;
var
Controlado: String[1];
begin
  if FmPrincipal.FProdutosControlados then
     Controlado := 'V' else Controlado := 'F';
  VAR_GOTOTABELA := 'Produtos';
  VAR_GOTOmySQLTABLE := QrProdutos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT pr.*, gr.Nome NOMEGRUPO');
  VAR_SQLx.Add('FROM produtos pr, ProdutosG gr');
  VAR_SQLx.Add('WHERE pr.Controla="'+Controlado+'"');
  VAR_SQLx.Add('AND gr.Codigo=pr.Grupo');
  //
  VAR_SQL1.Add('AND pr.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pr.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Controla="'+Controlado+'"';
end;

procedure TFmProdutos.SubQuery1Reopen;
begin
  //
end;

procedure TFmProdutos.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

procedure TFmProdutos.AlteraRegistro;
var
  Produto : Integer;
begin
  Produto := QrProdutosCodigo.Value;
  if QrProdutosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Produto, Dmod.MyDB, 'Produtos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Produto, Dmod.MyDB, 'Produtos', 'Codigo');
      GOTOy.BotoesSb(LaTipo.Caption);
      Application.CreateForm(TFmProdutosNew,FmProdutosNew);
      with FmProdutosNew do
      begin
        LaTipo.Caption := CO_ALTERACAO;
        EdCodigo.Text := FormatFloat('000', Produto);
        //
        EdNome.Text := QrProdutosNome.Value;
        EdPrecoC.Text := Geral.TFT(FloatToStr(QrProdutosPrecoC.Value), 3, siPositivo);
        EdPrecoV.Text := Geral.TFT(FloatToStr(QrProdutosPrecoV.Value), 3, siPositivo);
        EdICMS.Text := Geral.TFT(FloatToStr(QrProdutosICMS.Value), 2, siPositivo);
        EdRICMS.Text := Geral.TFT(FloatToStr(QrProdutosRICMS.Value), 2, siPositivo);
        EdIPI.Text := Geral.TFT(FloatToStr(QrProdutosIPI.Value), 2, siPositivo);
        EdRIPI.Text := Geral.TFT(FloatToStr(QrProdutosRIPI.Value), 2, siPositivo);
        EdFrete.Text := Geral.TFT(FloatToStr(QrProdutosFrete.Value), 2, siPositivo);
        EdJuros.Text := Geral.TFT(FloatToStr(QrProdutosJuros.Value), 2, siPositivo);
        EdGrupo.ValueVariant := QrProdutosGrupo.Value;
        CBGrupo.KeyValue     := QrProdutosGrupo.Value;
        if QrProdutosControla.Value = 'V' then CkControla.Checked := True;
        CkAtivo.Checked := QrProdutosAtivo.Value > 0;
        CkImprime.Checked := QrProdutosImprime.Value = 'V';
      end;
    finally
      Screen.Cursor := Cursor;
    end;
    FmProdutosNew.ShowModal;
    FmProdutosNew.Destroy;
    DefParams;
    LocCod(QrProdutosCodigo.Value, Produto);
  end;
end;

procedure TFmProdutos.IncluiRegistro;
var
  Cursor : TCursor;
  Produto : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Produto := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Produtos', 'Produtos', 'Codigo');
    Application.CreateForm(TFmProdutosNew,FmProdutosNew);
    with FmProdutosNew do
    begin
      EdCodigo.Text := FormatFloat('000', Produto);
      LaTipo.Caption := CO_INCLUSAO;
      GOTOy.BotoesSb(LaTipo.Caption);
    end;
  finally
    Screen.Cursor := Cursor;
  end;
  FmProdutosNew.ShowModal;
  FmProdutosNew.Destroy;
  DefParams;
  LocCod(QrProdutosCodigo.Value, Produto);
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////


procedure TFmProdutos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutos.BtSaidaClick(Sender: TObject);
begin
  VAR_PRODUTO := QrProdutosCodigo.Value;
  Close;
end;

procedure TFmProdutos.FormCreate(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmProdutos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrProdutosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmProdutos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmProdutos.QrProdutosAfterScroll(DataSet: TDataSet);
begin
  SubQuery1Reopen;
  BtAltera.Enabled := GOTOy.BtEnabled(QrProdutosCodigo.Value, False);
end;

procedure TFmProdutos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmProdutos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmProdutos.QrProdutosCalcFields(DataSet: TDataSet);
var
  IPI, RIPI, ICMS, RICMS, Frete, Juros, PrecoC: Double;
begin
  IPI    := QrProdutosIPI.Value / 100;
  RIPI   := QrProdutosRIPI.Value / 100;
  ICMS   := QrProdutosICMS.Value / 100;
  RICMS  := QrProdutosRICMS.Value / 100;
  PrecoC := QrProdutosPrecoC.Value;
  Frete  := QrProdutosFrete.Value;
  Juros  := QrProdutosJuros.Value;
  QrProdutosCUSTO.Value := (PrecoC / (1 - (ICMS-RICMS))) * (1 + (IPI-RIPI)) +
  Frete + Juros;
  if QrProdutosEstqQ.Value <> 0 then
    QrProdutosCUSTOMEDIO.Value := QrProdutosEstqV.Value / QrProdutosEstqQ.Value
  else QrProdutosCUSTOMEDIO.Value := 0;
end;

procedure TFmProdutos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, FTitulo, Image1, PainelTitulo, True, 35);
end;

procedure TFmProdutos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProdutosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Produtos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProdutos.BtRefreshClick(Sender: TObject);
begin
  UnAcademy.AtualizaEstoqueMercadoria(QrProdutosCodigo.Value, aeMsg, CO_VAZIO);
  LocCod(QrProdutosCodigo.Value, QrProdutosCodigo.Value);
end;

{
  object QrMatriculasLDDHApXnH: TLabel
    Left = 536
    Top = 4
    Width = 28
    Height = 13
    Caption = 'Inicio:'
  end
}
end.

