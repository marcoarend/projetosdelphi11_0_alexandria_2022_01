unit ProdutosIEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnMLAGeral, UnInternalConsts,
  UnMsgInt, ExtCtrls, UnInternalConsts2, mySQLDbTables, dmkGeral, Variants,
  dmkEdit, UnDmkEnums;

type
  TFmProdutosIEdit = class(TForm)
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    PainelDados: TPanel;
    PainelControle: TPanel;
    Label1: TLabel;
    EdMercadoria: TdmkEdit;
    Label4: TLabel;
    EdPecas: TdmkEdit;
    Label10: TLabel;
    EdPreco: TdmkEdit;
    BtConfirma: TBitBtn;
    Label6: TLabel;
    EdVTota: TdmkEdit;
    EdDescricao: TEdit;
    QrProdutosCodigo: TIntegerField;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosJuros: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosImprime: TWideStringField;
    QrProdutosLk: TIntegerField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    QrProdutosControla: TWideStringField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrProdutosCasas: TIntegerField;
    QrProdutosEstqV_G: TFloatField;
    QrProdutosEstqQ_G: TFloatField;
    QrProdutosAlterWeb: TSmallintField;
    QrProdutosAtivo: TSmallintField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdPecasExit(Sender: TObject);
    procedure EdPrecoExit(Sender: TObject);
    procedure EdVTotaExit(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaOnEdit(Tipo: Integer);
  public
    { Public declarations }
  end;

var
  FmProdutosIEdit: TFmProdutosIEdit;

implementation

uses UnMyObjects, Module;

{$R *.DFM}


procedure TFmProdutosIEdit.CalculaOnEdit(Tipo: Integer);
var           Pecas, Preco, VTota: Double;
begin
  Pecas := Geral.DMV(EdPecas.Text);
  Preco := Geral.DMV(EdPreco.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if Tipo <> 1 then VTota := Pecas * Preco
  else if Pecas > 0 then Preco := VTota / Pecas
  else Preco := 0;
  //
  EdPecas.Text := Geral.TFT(FloatToStr(Pecas), 3, siPositivo);
  EdPreco.Text := Geral.TFT(FloatToStr(Preco), 4, siPositivo);
  EdVTota.Text := Geral.TFT(FloatToStr(VTota), 2, siPositivo);
end;

procedure TFmProdutosIEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosIEdit.FormCreate(Sender: TObject);
begin
  QrProdutos.Open;
end;

procedure TFmProdutosIEdit.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  Pecas, VTota: Double;
  Merca: String;
begin
  Pecas := Geral.DMV(EdPecas.Text);
//  Preco := Geral.DMV(EdPreco.Text);
  VTota := Geral.DMV(EdVTota.Text);
  //
  if Trim(EdDescricao.Text) <> NULL then Merca := EdMercadoria.Text else
  begin
    Application.MessageBox(PChar('Defina a mercadoria.'), 'Erro', MB_OK+MB_ICONERROR);
    EdMercadoria.SetFocus;
    Exit;
  end;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE Produtos SET EIniQ=:P0, ');
    Dmod.QrUpdU.SQL.Add('EIniV=:P1  WHERE Codigo=:P2');
    Dmod.QrUpdU.Params[00].AsFloat := Pecas;
    Dmod.QrUpdU.Params[01].AsFloat := VTota;
    Dmod.QrUpdU.Params[02].AsString := Merca;
    Dmod.QrUpdU.ExecSQL;
  except
    Screen.Cursor := Cursor;
    Exit;
  end;
  Close;
end;

procedure TFmProdutosIEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CalculaOnEdit(1);
  EdPreco.SetFocus;
  EdPecas.SetFocus;
end;

procedure TFmProdutosIEdit.EdPecasExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmProdutosIEdit.EdPrecoExit(Sender: TObject);
begin
  CalculaOnEdit(0);
end;

procedure TFmProdutosIEdit.EdVTotaExit(Sender: TObject);
begin
  CalculaOnEdit(1);
end;

end.
