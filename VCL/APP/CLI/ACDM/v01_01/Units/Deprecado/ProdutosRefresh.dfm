object FmProdutosRefresh: TFmProdutosRefresh
  Left = 389
  Top = 159
  Caption = 'Refresh em Mercadorias'
  ClientHeight = 444
  ClientWidth = 684
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 0
    Top = 48
    Width = 684
    Height = 348
    Align = alClient
    Caption = 'Panel5'
    TabOrder = 0
    ExplicitWidth = 692
    ExplicitHeight = 350
    object Memo1: TMemo
      Left = 1
      Top = 17
      Width = 682
      Height = 330
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Lines.Strings = (
        'Avisos:')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      ExplicitWidth = 690
      ExplicitHeight = 332
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 682
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel1'
      TabOrder = 1
      ExplicitWidth = 690
      object Painel2: TPanel
        Left = 0
        Top = 0
        Width = 228
        Height = 16
        Align = alLeft
        BevelOuter = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8148270
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 0
      end
      object Panel3: TPanel
        Left = 228
        Top = 0
        Width = 60
        Height = 16
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Registros:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8148270
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 1
      end
      object PnRegistros: TPanel
        Left = 288
        Top = 0
        Width = 76
        Height = 16
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8148270
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 2
      end
      object Panel4: TPanel
        Left = 364
        Top = 0
        Width = 48
        Height = 16
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Tempo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8148270
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 3
      end
      object PnTempoD: TPanel
        Left = 412
        Top = 0
        Width = 72
        Height = 16
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0:00:00  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8148270
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 4
      end
      object Progress1: TProgressBar
        Left = 484
        Top = 0
        Width = 134
        Height = 16
        Align = alClient
        Position = 50
        Smooth = True
        TabOrder = 5
      end
      object PnTempoC: TPanel
        Left = 618
        Top = 0
        Width = 72
        Height = 16
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0:00:00  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8148270
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 6
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 396
    Width = 684
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 398
    ExplicitWidth = 692
    object BtSaida: TBitBtn
      Left = 578
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSaidaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
        333333333333337FF3333333333333903333333333333377FF33333333333399
        03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
        99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
        99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
        03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
        33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
        33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
        3333777777333333333333333333333333333333333333333333}
      NumGlyphs = 2
    end
    object BtRefresh: TBitBtn
      Left = 28
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inicia refresh nas mercadorias'
      Caption = '&Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtRefreshClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333CCCCC3333333333F77777FFF333333CCCCCCCCC
        3333333777333777FF3333CCC33333CCC3333377333333777FF33CCC3333333C
        CC3337733333333377F33CC333333333CC3337733333333337FFCC3333333333
        3CC377F333333333377FCC33333333333CC377F3333333333373CC3333333333
        333377F3333333333333CC3333333333333377F33333333FFFFFCC33333333CC
        CCC377FF33333377777F3CC33333333CCCC3373FF3333337777F3CCC33333333
        CCC33773FF33333F777F33CCC33333CCCCC333773FF33377777F333CCCCCCCCC
        33C3333777333777337333333CCCCC3333333333377777333333}
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 684
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Refresh em Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    ExplicitWidth = 692
    object LaTipo: TLabel
      Left = 612
      Top = 2
      Width = 78
      Height = 44
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 57
      Top = 2
      Width = 555
      Height = 44
      Align = alClient
      Transparent = True
    end
    object PainelBotoes: TPanel
      Left = 2
      Top = 2
      Width = 55
      Height = 44
      Align = alLeft
      
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0

      object SbImprime: TBitBtn
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
          0003377777777777777308888888888888807F33333333333337088888888888
          88807FFFFFFFFFFFFFF7000000000000000077777777777777770F8F8F8F8F8F
          8F807F333333333333F708F8F8F8F8F8F9F07F333333333337370F8F8F8F8F8F
          8F807FFFFFFFFFFFFFF7000000000000000077777777777777773330FFFFFFFF
          03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
          03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
          33333337F3F37F3733333330F08F0F0333333337F7337F7333333330FFFF0033
          33333337FFFF7733333333300000033333333337777773333333}
        NumGlyphs = 2
      end
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM produtos'
      'ORDER BY Codigo')
    Left = 56
    Top = 105
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Left = 84
    Top = 105
  end
end
