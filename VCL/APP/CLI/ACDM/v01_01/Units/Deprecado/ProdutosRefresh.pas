unit ProdutosRefresh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls,   UnitAcademy,
     UnGOTOy,
   Buttons, Db, mySQLDbTables, UnMLAGeral, UnInternalConsts,
  dmkGeral;

type
  TFmProdutosRefresh = class(TForm)
    Panel5: TPanel;
    PainelControle: TPanel;
    BtSaida: TBitBtn;
    Memo1: TMemo;
    Panel1: TPanel;
    Painel2: TPanel;
    Panel3: TPanel;
    PnRegistros: TPanel;
    Panel4: TPanel;
    PnTempoD: TPanel;
    Progress1: TProgressBar;
    BtRefresh: TBitBtn;
    QrProdutos: TmySQLQuery;
    QrProdutosCodigo: TIntegerField;
    Timer1: TTimer;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    PnTempoC: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmProdutosRefresh: TFmProdutosRefresh;

implementation

uses UnMyObjects, Module, Principal;

{$R *.DFM}

procedure TFmProdutosRefresh.FormCreate(Sender: TObject);
begin
  Progress1.Position := 0;
end;

procedure TFmProdutosRefresh.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 16);
end;

procedure TFmProdutosRefresh.BtRefreshClick(Sender: TObject);
var
  TimerIni: TDateTime;
  Tempo: Double;
  Aviso: TTipoAviso;
begin
  Memo1.Lines.Clear;
  Painel2.Caption := 'Aguarde... Atualizando estoques.';
  Painel2.Refresh;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE Produtos SET EstqQ=0, EstqV=0');
  Dmod.QrUpd.ExecSQL;
  //
  QrProdutos.Close;
  QrProdutos.Open;
  Progress1.Position := 0;
  Progress1.Max := GOTOy.Registros(QrProdutos);
  TimerIni := Now();
  while not QrProdutos.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    Tempo := Now() - TimerIni;
    PnTempoC.Caption := FormatDateTime(VAR_FORMATTIME, Tempo)+'  ';
    PnTempoC.Refresh;
    Tempo := (Tempo / Progress1.Position) * (Progress1.Max - Progress1.Position);
    PnTempoD.Caption := FormatDateTime(VAR_FORMATTIME, Tempo)+'  ';
    PnTempoD.Refresh;
    Aviso := aeMemo;
    UnAcademy.AtualizaEstoqueMercadoria(QrProdutosCodigo.Value, Aviso, 'Memo1');
    QrProdutos.Next;
  end;
  Timer1.Enabled := False;
  Application.MessageBox('Atualização de estoques finalizada', 'Informação',
    MB_OK+MB_ICONINFORMATION);
  QrProdutos.Close;
  Painel2.Caption := '';
end;

procedure TFmProdutosRefresh.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosRefresh.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
