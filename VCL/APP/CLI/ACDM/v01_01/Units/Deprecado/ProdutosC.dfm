object FmProdutosC: TFmProdutosC
  Left = 333
  Top = 174
  Caption = 'MER-ENTRA-001 :: Entrada de Produtos'
  ClientHeight = 564
  ClientWidth = 799
  Color = clBtnFace
  Constraints.MinHeight = 600
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 799
    Height = 468
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelConfirma: TPanel
      Left = 1
      Top = 419
      Width = 797
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtTrava: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Trava'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtTravaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 700
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = False
      end
      object BtInsere: TBitBtn
        Tag = 10
        Left = 104
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Insere item'
        Caption = '&Insere'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtInsereClick
      end
      object BtEdita: TBitBtn
        Tag = 11
        Left = 202
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Edita item'
        Caption = '&Edita'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtEditaClick
      end
    end
    object PainelCabeclho: TPanel
      Left = 1
      Top = 1
      Width = 797
      Height = 144
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 493
        Height = 144
        Align = alLeft
        Caption = ' Lan'#231'amento:'
        Enabled = False
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 65
          Height = 13
          Caption = 'N'#186' do lan'#231'to: '
          FocusControl = DBEdit1
        end
        object Label2: TLabel
          Left = 92
          Top = 56
          Width = 65
          Height = 13
          Caption = 'Data entrada:'
          FocusControl = DBEdit2
        end
        object Label4: TLabel
          Left = 8
          Top = 96
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
          FocusControl = EdNF
        end
        object Label7: TLabel
          Left = 188
          Top = 96
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
          FocusControl = DBEdit7
        end
        object Label12: TLabel
          Left = 430
          Top = 96
          Width = 45
          Height = 13
          Caption = '%RICMS:'
          FocusControl = DBEdit11
        end
        object Label3: TLabel
          Left = 74
          Top = 96
          Width = 44
          Height = 13
          Caption = 'Valor NF:'
          FocusControl = EdValorNF
        end
        object Label5: TLabel
          Left = 72
          Top = 16
          Width = 131
          Height = 13
          Caption = 'Fornecedor (representante):'
        end
        object Label20: TLabel
          Left = 8
          Top = 56
          Width = 67
          Height = 13
          Caption = 'Data emiss'#227'o:'
          FocusControl = DBEdit4
        end
        object Label21: TLabel
          Left = 283
          Top = 96
          Width = 52
          Height = 13
          Caption = 'Valor juros:'
          FocusControl = DBEdit8
        end
        object Label22: TLabel
          Left = 372
          Top = 96
          Width = 37
          Height = 13
          Caption = '%ICMS:'
          FocusControl = DBEdit9
        end
        object Label8: TLabel
          Left = 180
          Top = 56
          Width = 91
          Height = 13
          Caption = 'Fabricante (marca):'
        end
        object Label9: TLabel
          Left = 422
          Top = 16
          Width = 24
          Height = 13
          Caption = 'Lote:'
          FocusControl = DBEdit10
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 32
          Width = 61
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsProdutosC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 92
          Top = 72
          Width = 82
          Height = 21
          DataField = 'DataC'
          DataSource = DsProdutosC
          TabOrder = 1
        end
        object EdNF: TDBEdit
          Left = 8
          Top = 112
          Width = 64
          Height = 21
          DataField = 'NFC'
          DataSource = DsProdutosC
          TabOrder = 2
        end
        object DBEdit7: TDBEdit
          Left = 188
          Top = 112
          Width = 93
          Height = 21
          DataField = 'Peso'
          DataSource = DsProdutosC
          TabOrder = 4
        end
        object DBEdit11: TDBEdit
          Left = 430
          Top = 112
          Width = 55
          Height = 21
          DataField = 'RICMS'
          DataSource = DsProdutosC
          TabOrder = 3
        end
        object EdValorNF: TDBEdit
          Left = 74
          Top = 112
          Width = 111
          Height = 21
          DataField = 'Total'
          DataSource = DsProdutosC
          TabOrder = 6
        end
        object DBEdit5: TDBEdit
          Left = 72
          Top = 32
          Width = 345
          Height = 21
          DataField = 'NOMEFORNECEDOR'
          DataSource = DsProdutosC
          TabOrder = 5
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 72
          Width = 80
          Height = 21
          DataField = 'DataE'
          DataSource = DsProdutosC
          TabOrder = 7
        end
        object DBEdit8: TDBEdit
          Left = 283
          Top = 112
          Width = 86
          Height = 21
          DataField = 'Juros'
          DataSource = DsProdutosC
          TabOrder = 8
        end
        object DBEdit9: TDBEdit
          Left = 372
          Top = 112
          Width = 53
          Height = 21
          DataField = 'ICMS'
          DataSource = DsProdutosC
          TabOrder = 9
        end
        object DBEdit6: TDBEdit
          Left = 180
          Top = 72
          Width = 305
          Height = 21
          DataField = 'NOMECOLETOR'
          DataSource = DsProdutosC
          TabOrder = 10
        end
        object DBEdit10: TDBEdit
          Left = 422
          Top = 32
          Width = 64
          Height = 21
          DataField = 'Lote'
          DataSource = DsProdutosC
          TabOrder = 11
        end
      end
      object GroupBox3: TGroupBox
        Left = 493
        Top = 0
        Width = 304
        Height = 144
        Align = alClient
        Caption = ' Transporte: '
        TabOrder = 1
        object Label13: TLabel
          Left = 8
          Top = 16
          Width = 75
          Height = 13
          Caption = 'Transportadora:'
          FocusControl = DBEdit12
        end
        object Label14: TLabel
          Left = 8
          Top = 56
          Width = 53
          Height = 13
          Caption = 'Conhecim.:'
          FocusControl = DBEdit13
        end
        object Label15: TLabel
          Left = 88
          Top = 56
          Width = 54
          Height = 13
          Caption = 'Valor Frete:'
          FocusControl = EdFrete
        end
        object Label6: TLabel
          Left = 156
          Top = 56
          Width = 45
          Height = 13
          Caption = '%RICMS:'
          FocusControl = DBEdit3
        end
        object DBEdit12: TDBEdit
          Left = 8
          Top = 32
          Width = 197
          Height = 21
          DataField = 'NOMETRANSPORTADOR'
          DataSource = DsProdutosC
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 8
          Top = 72
          Width = 76
          Height = 21
          DataField = 'NFF'
          DataSource = DsProdutosC
          TabOrder = 1
        end
        object EdFrete: TDBEdit
          Left = 88
          Top = 72
          Width = 65
          Height = 21
          DataField = 'Frete'
          DataSource = DsProdutosC
          TabOrder = 2
        end
        object DBEdit3: TDBEdit
          Left = 156
          Top = 72
          Width = 49
          Height = 21
          DataField = 'RICMF'
          DataSource = DsProdutosC
          TabOrder = 3
        end
        object GroupBox4: TGroupBox
          Left = 211
          Top = 12
          Width = 86
          Height = 105
          Caption = ' Confer'#234'ncia: '
          TabOrder = 4
          object Label17: TLabel
            Left = 8
            Top = 60
            Width = 40
            Height = 13
            Caption = '$ Pag.F:'
          end
          object Label18: TLabel
            Left = 8
            Top = 16
            Width = 44
            Height = 13
            Caption = '$ Pag. T:'
          end
          object EdPagF: TdmkEdit
            Left = 8
            Top = 76
            Width = 69
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPagT: TdmkEdit
            Left = 8
            Top = 32
            Width = 69
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object PainelGrids: TPanel
      Left = 1
      Top = 145
      Width = 797
      Height = 274
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 2
      object Grupo1: TGroupBox
        Left = 0
        Top = 0
        Width = 797
        Height = 166
        Align = alTop
        Caption = ' Itens da entrada (valores em $): '
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 793
          Height = 149
          Align = alClient
          DataSource = DsProdutosCIts
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnKeyDown = DBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'Conta'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPRODUTO'
              Title.Caption = 'Produto'
              Width = 324
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Bruto'
              Title.Caption = 'Quantidade'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor total'
              Width = 88
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CUSTOUNITARIO'
              Title.Alignment = taRightJustify
              Title.Caption = 'Custo unit.'
              Width = 72
              Visible = True
            end>
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 166
        Width = 797
        Height = 108
        Align = alClient
        Caption = ' Pagamentos:'
        TabOrder = 1
        object GroupBoxT: TGroupBox
          Left = 2
          Top = 15
          Width = 392
          Height = 91
          Align = alLeft
          Caption = ' Tranportadora: '
          TabOrder = 0
          object GridT: TDBGrid
            Left = 2
            Top = 15
            Width = 388
            Height = 74
            Align = alClient
            DataSource = DsEmissT
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = GridTKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'FatParcela'
                Title.Caption = 'N'#186
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                ReadOnly = True
                Title.Caption = 'Situa'#231#227'o'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECARTEIRA'
                Title.Caption = 'Carteira'
                Width = 300
                Visible = True
              end>
          end
        end
        object GroupBoxF: TGroupBox
          Left = 394
          Top = 15
          Width = 401
          Height = 91
          Align = alClient
          Caption = ' Fornecedor: '
          TabOrder = 1
          object GridF: TDBGrid
            Left = 2
            Top = 15
            Width = 397
            Height = 74
            Align = alClient
            DataSource = DsEmissF
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = GridFKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'FatParcela'
                Title.Caption = 'N'#186
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                ReadOnly = True
                Title.Caption = 'Situa'#231#227'o'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECARTEIRA'
                Title.Caption = 'Carteira'
                Width = 300
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 516
    Width = 799
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaRegistro: TLabel
      Left = 173
      Top = 1
      Width = 26
      Height = 13
      Align = alClient
      Caption = '[N]: 0'
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 329
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 799
    Height = 48
    Align = alTop
    Caption = 'Entrada de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 720
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 712
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 216
      Top = 1
      Width = 504
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 217
      ExplicitTop = 2
      ExplicitWidth = 495
      ExplicitHeight = 44
    end
    object PainelBotoes: TPanel
      Left = 1
      Top = 1
      Width = 215
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
      end
    end
  end
  object QrTransporte: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 114
    Top = 243
    object QrTransporteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransporteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 14
    end
  end
  object DsTransporte: TDataSource
    DataSet = QrTransporte
    Left = 142
    Top = 243
  end
  object QrProdutosC: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrProdutosCAfterOpen
    AfterScroll = QrProdutosCAfterScroll
    OnCalcFields = QrProdutosCCalcFields
    SQL.Strings = (
      'SELECT pc.*,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEFORNECEDOR, '
      'CASE 1 WHEN tr.Tipo=0 THEN tr.RazaoSocial'
      'ELSE tr.Nome END NOMETRANSPORTADOR, '
      'CASE 1 WHEN co.Tipo=0 THEN co.RazaoSocial'
      'ELSE co.Nome END NOMECOLETOR '
      'FROM produtosc pc, Entidades fo, '
      'Entidades tr, Entidades co'
      'WHERE pc.Codigo>0'
      'AND fo.Codigo=pc.Fornece'
      'AND tr.Codigo=pc.Tranporta'
      'AND co.Codigo=pc.Coletor')
    Left = 16
    Top = 244
    object QrProdutosCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosCDataC: TDateField
      FieldName = 'DataC'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProdutosCDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProdutosCFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrProdutosCTranporta: TIntegerField
      FieldName = 'Tranporta'
    end
    object QrProdutosCColetor: TIntegerField
      FieldName = 'Coletor'
    end
    object QrProdutosCLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '0'
    end
    object QrProdutosCNFC: TIntegerField
      FieldName = 'NFC'
    end
    object QrProdutosCNFF: TIntegerField
      FieldName = 'NFF'
    end
    object QrProdutosCFrete: TFloatField
      FieldName = 'Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosCICMS: TFloatField
      FieldName = 'ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCRICMS: TFloatField
      FieldName = 'RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCRICMF: TFloatField
      FieldName = 'RICMF'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosCNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 12
    end
    object QrProdutosCNOMETRANSPORTADOR: TWideStringField
      FieldName = 'NOMETRANSPORTADOR'
      Size = 4
    end
    object QrProdutosCNOMECOLETOR: TWideStringField
      FieldName = 'NOMECOLETOR'
      Size = 32
    end
  end
  object DsProdutosC: TDataSource
    DataSet = QrProdutosC
    Left = 44
    Top = 244
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 86
    Top = 243
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 14
    end
  end
  object DsProdutosCIts: TDataSource
    DataSet = QrProdutosCIts
    Left = 42
    Top = 271
  end
  object QrEmissT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissTCalcFields
    SQL.Strings = (
      'SELECT la.*, ca.Nome NOMECARTEIRA'
      'FROM lanctos la, Carteiras ca'
      'WHERE ca.Codigo=la.Carteira'
      'AND FatID=2103'
      'AND FatNum=:P0'
      '')
    Left = 12
    Top = 420
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmissTCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrEmissTDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrEmissTNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrEmissTDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissTCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'DBMMONEY.lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrEmissTSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'DBMMONEY.lanctos.Sit'
    end
    object QrEmissTVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'DBMMONEY.lanctos.FatID'
    end
    object QrEmissTFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'DBMMONEY.lanctos.FatParcela'
    end
    object QrEmissTData: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissTTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrEmissTAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'DBMMONEY.lanctos.Autorizacao'
    end
    object QrEmissTGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'DBMMONEY.lanctos.Genero'
    end
    object QrEmissTCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrEmissTLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
    object QrEmissTNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissTNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissTNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissTID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'DBMMONEY.lanctos.ID_Sub'
    end
    object QrEmissTSub: TIntegerField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrEmissTFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'DBMMONEY.lanctos.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissTBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'DBMMONEY.lanctos.Banco'
    end
    object QrEmissTLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'DBMMONEY.lanctos.Local'
    end
    object QrEmissTCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'DBMMONEY.lanctos.Cartao'
    end
    object QrEmissTLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'DBMMONEY.lanctos.Linha'
    end
    object QrEmissTPago: TFloatField
      FieldName = 'Pago'
      Origin = 'DBMMONEY.lanctos.Pago'
    end
    object QrEmissTMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'DBMMONEY.lanctos.Mez'
    end
    object QrEmissTFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrEmissTCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrEmissTControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissTID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissTFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissTCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsEmissT: TDataSource
    DataSet = QrEmissT
    Left = 40
    Top = 420
  end
  object QrEmissF: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissFCalcFields
    SQL.Strings = (
      'SELECT la.*, ca.Nome NOMECARTEIRA'
      'FROM lanctos la, Carteiras ca'
      'WHERE ca.Codigo=la.Carteira'
      'AND FatID=2102'
      'AND FatNum=:P0')
    Left = 404
    Top = 424
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmissFData: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissFTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrEmissFCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrEmissFAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'DBMMONEY.lanctos.Autorizacao'
    end
    object QrEmissFGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'DBMMONEY.lanctos.Genero'
    end
    object QrEmissFDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrEmissFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrEmissFDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEmissFCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrEmissFCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'DBMMONEY.lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissFDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrEmissFSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'DBMMONEY.lanctos.Sit'
    end
    object QrEmissFVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmissFLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
    object QrEmissFFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'DBMMONEY.lanctos.FatID'
    end
    object QrEmissFFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'DBMMONEY.lanctos.FatParcela'
    end
    object QrEmissFNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrEmissFNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 10
      Calculated = True
    end
    object QrEmissFNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrEmissFID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'DBMMONEY.lanctos.ID_Sub'
    end
    object QrEmissFSub: TIntegerField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrEmissFFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'DBMMONEY.lanctos.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrEmissFBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'DBMMONEY.lanctos.Banco'
    end
    object QrEmissFLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'DBMMONEY.lanctos.Local'
    end
    object QrEmissFCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'DBMMONEY.lanctos.Cartao'
    end
    object QrEmissFLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'DBMMONEY.lanctos.Linha'
    end
    object QrEmissFPago: TFloatField
      FieldName = 'Pago'
      Origin = 'DBMMONEY.lanctos.Pago'
    end
    object QrEmissFMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'DBMMONEY.lanctos.Mez'
    end
    object QrEmissFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrEmissFCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrEmissFControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissFID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmissFFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmissFCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsEmissF: TDataSource
    DataSet = QrEmissF
    Left = 432
    Top = 424
  end
  object QrConfT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Bruto-Tara-Impur)  Peso,'
      'SUM(Valor) Valor'
      'FROM produtoscits'
      'WHERE Codigo=:P0')
    Left = 85
    Top = 274
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfTPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrConfTValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSumF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Debito '
      'FROM lanctos'
      'WHERE FatID=2102'
      'AND FatNum=:P0'
      '')
    Left = 456
    Top = 425
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumFDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
  end
  object QrSumT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Debito '
      'FROM lanctos'
      'WHERE FatID=2103'
      'AND FatNum=:P0'
      '')
    Left = 68
    Top = 421
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
  end
  object PMInsere: TPopupMenu
    Left = 532
    Top = 5
    object Itemdeinsumo1: TMenuItem
      Caption = '&Item de produto'
      OnClick = Itemdeinsumo1Click
    end
    object DuplicataTransportadora1: TMenuItem
      Caption = 'Duplicata &transportadora'
      OnClick = DuplicataTransportadora1Click
    end
    object Duplicatafornecedor1: TMenuItem
      Caption = 'Duplicata &fornecedor'
      OnClick = Duplicatafornecedor1Click
    end
  end
  object PMEdita: TPopupMenu
    Left = 564
    Top = 5
    object MenuItem1: TMenuItem
      Caption = '&Item de produto'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = 'Duplicata &transportadora'
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = 'Duplicata &fornecedor'
      OnClick = MenuItem3Click
    end
  end
  object QrProdutosCIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProdutosCItsCalcFields
    SQL.Strings = (
      'SELECT ci.*, pr.Nome NOMEPRODUTO '
      'FROM produtoscits ci, Produtos pr'
      'WHERE ci.Codigo=:P0'
      'AND pr.Codigo=ci.Produto')
    Left = 14
    Top = 272
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutosCItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DNRECOVERY.produtoscits.Codigo'
    end
    object QrProdutosCItsConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'DNRECOVERY.produtoscits.Conta'
    end
    object QrProdutosCItsProduto: TIntegerField
      FieldName = 'Produto'
      Origin = 'DNRECOVERY.produtoscits.Produto'
    end
    object QrProdutosCItsPreco: TFloatField
      FieldName = 'Preco'
      Origin = 'DNRECOVERY.produtoscits.Preco'
      DisplayFormat = '#,##0.0000'
    end
    object QrProdutosCItsBruto: TFloatField
      FieldName = 'Bruto'
      Origin = 'DNRECOVERY.produtoscits.Bruto'
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosCItsTara: TFloatField
      FieldName = 'Tara'
      Origin = 'DNRECOVERY.produtoscits.Tara'
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosCItsImpur: TFloatField
      FieldName = 'Impur'
      Origin = 'DNRECOVERY.produtoscits.Impur'
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosCItsDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'DNRECOVERY.produtoscits.Desco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNRECOVERY.produtoscits.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCItsNOMEPRODUTO: TWideStringField
      FieldName = 'NOMEPRODUTO'
      Origin = 'DNRECOVERY.produtos.Nome'
      Size = 128
    end
    object QrProdutosCItsPESOLIQUIDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PESOLIQUIDO'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrProdutosCItsCUSTOUNITARIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOUNITARIO'
      DisplayFormat = '#,##0.0000'
      Calculated = True
    end
    object QrProdutosCItsCusto: TFloatField
      FieldName = 'Custo'
      Origin = 'DNRECOVERY.produtoscits.Custo'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrProdutosCItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Bruto-Tara-Impur) Peso,'
      'SUM(Valor) Valor,'
      'SUM(Custo) Custo'
      'FROM produtoscits'
      'WHERE Codigo=:P0')
    Left = 113
    Top = 274
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSomaCusto: TFloatField
      FieldName = 'Custo'
    end
  end
  object QrColetor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 170
    Top = 243
    object QrColetorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColetorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 9
    end
  end
end
