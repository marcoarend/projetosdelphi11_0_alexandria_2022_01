unit ProdutosCNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, Db, (*DBTables,*) ComCtrls,
  UnInternalConsts2, UnGOTOy, UnInternalConsts, UnMLAGeral, UMySQLModule,
  mySQLDbTables, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, UnDmkEnums;

type
  TFmProdutosCNew = class(TForm)
    QrRepresentantes: TmySQLQuery;
    DsRepresentantes: TDataSource;
    QrTransportadores: TmySQLQuery;
    DsTransportadores: TDataSource;
    PainelTitulo: TPanel;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaLancamento: TLabel;
    EdCodigo: TdmkEdit;
    EdPedido: TdmkEdit;
    Label10: TLabel;
    LaSitPedido: TLabel;
    Label17: TLabel;
    TPDataC: TdmkEditDateTimePicker;
    Label11: TLabel;
    TPDataE: TdmkEditDateTimePicker;
    Label6: TLabel;
    EdRepresentante: TdmkEditCB;
    CBRepresentante: TdmkDBLookupComboBox;
    Label19: TLabel;
    EdTransportador: TdmkEditCB;
    CBTransportador: TdmkDBLookupComboBox;
    EdNFC: TdmkEdit;
    Label4: TLabel;
    Label1: TLabel;
    EdRICMS: TdmkEdit;
    Label9: TLabel;
    EdNFF: TdmkEdit;
    Label8: TLabel;
    EdFrete: TdmkEdit;
    Label2: TLabel;
    EdRICMF: TdmkEdit;
    LaTipo: TLabel;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    EdJuros: TdmkEdit;
    Label12: TLabel;
    Label13: TLabel;
    EdICMS: TdmkEdit;
    Label3: TLabel;
    EdFabricante: TdmkEditCB;
    CBFabricante: TdmkDBLookupComboBox;
    QrFabricantes: TmySQLQuery;
    DsFabricantes: TDataSource;
    Label5: TLabel;
    EdLote: TdmkEdit;
    QrMaxLote: TmySQLQuery;
    QrMaxLoteLote: TIntegerField;
    QrFabricantesCodigo: TIntegerField;
    QrFabricantesNOMEENTIDADE: TWideStringField;
    QrRepresentantesCodigo: TIntegerField;
    QrRepresentantesNOMEENTIDADE: TWideStringField;
    QrTransportadoresCodigo: TIntegerField;
    QrTransportadoresNOMEENTIDADE: TWideStringField;
    QrColetor: TmySQLQuery;
    QrColetorColetor: TIntegerField;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdRICMSExit(Sender: TObject);
    procedure EdLoteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    { Private declarations }
//    procedure ControlChange(Sender : TObject);
  public
    { Public declarations }
  end;

var
  FmProdutosCNew: TFmProdutosCNew;

implementation

uses UnMyObjects, Principal, ProdutosC, Module, Entidades;

{$R *.DFM}

procedure TFmProdutosCNew.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then
     UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ProdutosC', Codigo)
  else
     UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ProdutosC', 'Codigo');
  FmProdutosC.LaTipo.Caption := CO_TRAVADO;
  FmProdutosC.PainelConfirma.Visible:=False;
  FmProdutosC.PainelControle.Visible:=True;
  Close;
end;

procedure TFmProdutosCNew.BtConfirmaClick(Sender: TObject);
var
  ICMS, RICMS, RICMF : Double;
  Lote, Codigo : Integer;
  ErrLote: Boolean;
begin
  ErrLote := False;
  Codigo := Geral.IMV(EdCodigo.Text);
  Lote := Geral.IMV(EdLote.Text);
(*  if Lote < 1 then
  begin
    Application.MessageBox('Defina o n�mero do lote.', 'Erro', MB_OK+MB_ICONERROR);
    EdLote.SetFocus;
    Exit;
  end;*)
  ICMS := Geral.DMV(EdICMS.Text);
  RICMS := Geral.DMV(EdRICMS.Text);
  RICMF := Geral.DMV(EdRICMF.Text);
  if ICMS > 99 then
  begin
    Application.MessageBox(PChar('O ICMS da N.F. � inv�lido.'), 'Erro', MB_OK+MB_ICONERROR);
    EdICMS.SetFocus;
    Exit;
  end;
  if RICMS > 99 then
  begin
    Application.MessageBox(PChar('O retorno de ICMS da N.F. � inv�lido.'), 'Erro', MB_OK+MB_ICONERROR);
    EdRICMS.SetFocus;
    Exit;
  end;
  if RICMF > 99 then
  begin
    Application.MessageBox(PChar('O retorno de ICMS do frete � inv�lido.'), 'Erro', MB_OK+MB_ICONERROR);
    EdRICMF.SetFocus;
    Exit;
  end;
  if CBRepresentante.KeyValue = Null then CBRepresentante.KeyValue := 0;
  if CBTransportador.KeyValue = Null then CBTransportador.KeyValue := 0;
  if CBFabricante.KeyValue = Null then CBFabricante.KeyValue := 0;
(*  if CBFabricante.KeyValue = 0 then
  begin
    Application.MessageBox(PChar('Defina um Fabricante.'), 'Erro', MB_OK+MB_ICONERROR);
    EdFabricante.SetFocus;
    Exit;
  end;*)
  QrColetor.Close;
  QrColetor.Params[0].AsInteger := Lote;
  QrColetor.Open;
  if GOTOy.Registros(QrColetor) > 0 then
  begin
    while not QrColetor.Eof do
    begin
      if QrColetorColetor.Value <> CBFabricante.KeyValue then ErrLote := True;
      QrColetor.Next;
    end;
  end;
  QrColetor.Close;
  if ErrLote then
  begin
    Application.MessageBox(
    'Este n�mero de lote j� pertence a outro centro coletor', 'Erro',
    MB_OK+MB_ICONERROR);
    EdLote.SetFocus;
    Exit;
  end;
  //
  Dmod.QrUpdU.Close;
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO ProdutosC SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE ProdutosC SET ');
  Dmod.QrUpdU.SQL.Add('  DataC          =:P00');
  Dmod.QrUpdU.SQL.Add(', DataE          =:P01');
  Dmod.QrUpdU.SQL.Add(', Fornece        =:P02');
  Dmod.QrUpdU.SQL.Add(', Tranporta      =:P03');
  Dmod.QrUpdU.SQL.Add(', NFC            =:P04');
  Dmod.QrUpdU.SQL.Add(', NFF            =:P05');
  Dmod.QrUpdU.SQL.Add(', Frete          =:P06');
  Dmod.QrUpdU.SQL.Add(', Juros          =:P07');
  Dmod.QrUpdU.SQL.Add(', ICMS           =:P08');
  Dmod.QrUpdU.SQL.Add(', RICMS          =:P09');
  Dmod.QrUpdU.SQL.Add(', RICMF          =:P10');
  Dmod.QrUpdU.SQL.Add(', Coletor        =:P11');
  Dmod.QrUpdU.SQL.Add(', Lote           =:P12');
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add(', Codigo       =:Px')
  else Dmod.QrUpdU.SQL.Add('WHERE Codigo=:Px');
    //
  Dmod.QrUpdU.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPDataC.Date);
  Dmod.QrUpdU.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPDataE.Date);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdRepresentante.Text);
  Dmod.QrUpdU.Params[03].AsInteger := Geral.IMV(EdTransportador.Text);
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdNFC.Text);
  Dmod.QrUpdU.Params[05].AsInteger := Geral.IMV(EdNFF.Text);
  Dmod.QrUpdU.Params[06].AsFloat := Geral.DMV(EdFrete.Text);
  Dmod.QrUpdU.Params[07].AsFloat := Geral.DMV(EdJuros.Text);
  Dmod.QrUpdU.Params[08].AsFloat := ICMS;
  Dmod.QrUpdU.Params[09].AsFloat := RICMS;
  Dmod.QrUpdU.Params[10].AsFloat := RICMF;
  Dmod.QrUpdU.Params[11].AsInteger := Geral.IMV(EdFabricante.Text);
  Dmod.QrUpdU.Params[12].AsFloat := Lote;
  Dmod.QrUpdU.Params[13].AsFloat := Codigo;
  Dmod.QrUpdU.ExecSQL;
  FmProdutosC.LaTipo.Caption := LaTipo.Caption;
  Close;
end;

procedure TFmProdutosCNew.FormCreate(Sender: TObject);
begin
  QrFabricantes.Open;
  QrRepresentantes.Open;
  QrTransportadores.Open;
end;

procedure TFmProdutosCNew.SpeedButton1Click(Sender: TObject);
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'Cadastros', 0) then Exit;
    Application.CreateForm(TFmEntidades, FmEntidades);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    QrRepresentantes.Close;
    QrRepresentantes.Open;
    QrFabricantes.Close;
    QrFabricantes.Open;
    QrTransportadores.Close;
    QrTransportadores.Open;
    EdRepresentante.Text     := IntToStr(VAR_ENTIDADE);
    CBRepresentante.KeyValue := VAR_ENTIDADE;
    CBRepresentante.SetFocus;
  finally
    Cursor := VAR_CURSOR;
  end;

end;

procedure TFmProdutosCNew.SpeedButton2Click(Sender: TObject);
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'Cadastros', 0) then Exit;
    Application.CreateForm(TFmEntidades, FmEntidades);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    QrTransportadores.Close;
    QrTransportadores.Open;
    QrFabricantes.Close;
    QrFabricantes.Open;
    QrRepresentantes.Close;
    QrRepresentantes.Open;
    EdFabricante.Text     := IntToStr(VAR_ENTIDADE);
    CBFabricante.KeyValue := VAR_ENTIDADE;
    CBFabricante.SetFocus;
  finally
    Cursor := VAR_CURSOR;
  end;
end;

procedure TFmProdutosCNew.SpeedButton3Click(Sender: TObject);
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'Cadastros', 0) then Exit;
    Application.CreateForm(TFmEntidades, FmEntidades);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    QrTransportadores.Close;
    QrTransportadores.Open;
    QrFabricantes.Close;
    QrFabricantes.Open;
    QrRepresentantes.Close;
    QrRepresentantes.Open;
    EdTransportador.Text     := IntToStr(VAR_ENTIDADE);
    CBTransportador.KeyValue := VAR_ENTIDADE;
    CBTransportador.SetFocus;
  finally
    Cursor := VAR_CURSOR;
  end;
end;

procedure TFmProdutosCNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if LaTipo.Caption = CO_ALTERACAO then BtConfirma.Setfocus;
end;

procedure TFmProdutosCNew.EdRICMSExit(Sender: TObject);
begin
  EdRICMS.Text := MLAGeral.TFT_MinMax(EdRICMS.Text, 0,
  Geral.DMV(EdICMS.Text), 2, siPositivo);
end;

procedure TFmProdutosCNew.EdLoteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    QrMaxLote.Close;
    QrMaxLote.Open;
    EdLote.Text := IntToStr(QrMaxLoteLote.Value+1);
    QrMaxLote.Close;
  end;  
end;

end.
