unit ProdutosC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ComCtrls, Grids, DBGrids, ZCF2, ResIntStrings, UnGOTOy, UnitAcademy,
  UnInternalConsts, UnInternalConsts2, UnMsgInt, UMySQLModule, Menus,
  UnMLAGeral, UnInternalConsts3, mySQLDbTables, dmkEdit, dmkGeral,
  UnDmkProcFunc, UnDmkEnums;

type
  StatusVP = (vpErro, vpLiq, VPBruto);
  TFmProdutosC = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtTrava: TBitBtn;
    BtDesiste: TBitBtn;
    PainelCabeclho: TPanel;
    PainelGrids: TPanel;
    Grupo1: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    EdNF: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    QrTransporte: TmySQLQuery;
    DsTransporte: TDataSource;
    GroupBox5: TGroupBox;
    GroupBoxT: TGroupBox;
    GroupBoxF: TGroupBox;
    GridT: TDBGrid;
    GridF: TDBGrid;
    QrProdutosC: TmySQLQuery;
    DsProdutosC: TDataSource;
    Label3: TLabel;
    EdValorNF: TDBEdit;
    QrFornecedor: TmySQLQuery;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    GroupBox3: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    EdFrete: TDBEdit;
    DsProdutosCIts: TDataSource;
    QrEmissT: TmySQLQuery;
    DsEmissT: TDataSource;
    QrEmissF: TmySQLQuery;
    DsEmissF: TDataSource;
    BtInsere: TBitBtn;
    BtEdita: TBitBtn;
    QrConfT: TmySQLQuery;
    QrEmissTCarteira: TIntegerField;
    QrEmissTDescricao: TWideStringField;
    QrEmissTNotaFiscal: TIntegerField;
    QrEmissTDebito: TFloatField;
    QrEmissTCompensado: TDateField;
    QrEmissTDocumento: TFloatField;
    QrEmissTSit: TIntegerField;
    QrEmissTVencimento: TDateField;
    QrEmissTFatID: TIntegerField;
    QrEmissTFatParcela: TIntegerField;
    QrEmissFData: TDateField;
    QrEmissFTipo: TSmallintField;
    QrEmissFCarteira: TIntegerField;
    QrEmissFAutorizacao: TIntegerField;
    QrEmissFGenero: TIntegerField;
    QrEmissFDescricao: TWideStringField;
    QrEmissFNotaFiscal: TIntegerField;
    QrEmissFDebito: TFloatField;
    QrEmissFCredito: TFloatField;
    QrEmissFCompensado: TDateField;
    QrEmissFDocumento: TFloatField;
    QrEmissFSit: TIntegerField;
    QrEmissFVencimento: TDateField;
    QrEmissFLk: TIntegerField;
    QrEmissFFatID: TIntegerField;
    QrEmissFFatParcela: TIntegerField;
    QrEmissTData: TDateField;
    QrEmissTTipo: TSmallintField;
    QrEmissTAutorizacao: TIntegerField;
    QrEmissTGenero: TIntegerField;
    QrEmissTCredito: TFloatField;
    QrEmissTLk: TIntegerField;
    QrEmissTNOMESIT: TWideStringField;
    QrEmissFNOMESIT: TWideStringField;
    QrSumF: TmySQLQuery;
    QrSumT: TmySQLQuery;
    QrSumTDebito: TFloatField;
    QrSumFDebito: TFloatField;
    QrEmissTNOMETIPO: TWideStringField;
    QrEmissFNOMETIPO: TWideStringField;
    QrEmissTNOMECARTEIRA: TWideStringField;
    QrEmissFNOMECARTEIRA: TWideStringField;
    Label20: TLabel;
    DBEdit4: TDBEdit;
    PMInsere: TPopupMenu;
    Itemdeinsumo1: TMenuItem;
    DuplicataTransportadora1: TMenuItem;
    Duplicatafornecedor1: TMenuItem;
    PMEdita: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Label21: TLabel;
    DBEdit8: TDBEdit;
    Label22: TLabel;
    DBEdit9: TDBEdit;
    QrProdutosCIts: TmySQLQuery;
    QrEmissTID_Sub: TSmallintField;
    QrEmissTSub: TIntegerField;
    QrEmissTFatura: TWideStringField;
    QrEmissTBanco: TIntegerField;
    QrEmissTLocal: TIntegerField;
    QrEmissTCartao: TIntegerField;
    QrEmissTLinha: TIntegerField;
    QrEmissTPago: TFloatField;
    QrEmissTMez: TIntegerField;
    QrEmissFID_Sub: TSmallintField;
    QrEmissFSub: TIntegerField;
    QrEmissFFatura: TWideStringField;
    QrEmissFBanco: TIntegerField;
    QrEmissFLocal: TIntegerField;
    QrEmissFCartao: TIntegerField;
    QrEmissFLinha: TIntegerField;
    QrEmissFPago: TFloatField;
    QrEmissFMez: TIntegerField;
    QrEmissFFornecedor: TIntegerField;
    QrEmissFCliente: TIntegerField;
    QrEmissTFornecedor: TIntegerField;
    QrEmissTCliente: TIntegerField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    QrSoma: TmySQLQuery;
    QrProdutosCItsCUSTOUNITARIO: TFloatField;
    GroupBox4: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    EdPagF: TdmkEdit;
    EdPagT: TdmkEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    QrColetor: TmySQLQuery;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    QrTransporteCodigo: TIntegerField;
    QrTransporteNOMEENTIDADE: TWideStringField;
    QrColetorCodigo: TIntegerField;
    QrColetorNOMEENTIDADE: TWideStringField;
    QrProdutosCItsCodigo: TIntegerField;
    QrProdutosCItsConta: TIntegerField;
    QrProdutosCItsProduto: TIntegerField;
    QrProdutosCItsPreco: TFloatField;
    QrProdutosCItsBruto: TFloatField;
    QrProdutosCItsTara: TFloatField;
    QrProdutosCItsImpur: TFloatField;
    QrProdutosCItsDesco: TFloatField;
    QrProdutosCItsValor: TFloatField;
    QrProdutosCItsNOMEPRODUTO: TWideStringField;
    QrProdutosCItsPESOLIQUIDO: TFloatField;
    QrConfTPeso: TFloatField;
    QrConfTValor: TFloatField;
    QrSomaPeso: TFloatField;
    QrSomaValor: TFloatField;
    QrSomaCusto: TFloatField;
    QrProdutosCItsCusto: TFloatField;
    QrProdutosCCodigo: TIntegerField;
    QrProdutosCDataC: TDateField;
    QrProdutosCDataE: TDateField;
    QrProdutosCFornece: TIntegerField;
    QrProdutosCTranporta: TIntegerField;
    QrProdutosCColetor: TIntegerField;
    QrProdutosCLote: TIntegerField;
    QrProdutosCNFC: TIntegerField;
    QrProdutosCNFF: TIntegerField;
    QrProdutosCFrete: TFloatField;
    QrProdutosCJuros: TFloatField;
    QrProdutosCValor: TFloatField;
    QrProdutosCTotal: TFloatField;
    QrProdutosCPeso: TFloatField;
    QrProdutosCICMS: TFloatField;
    QrProdutosCRICMS: TFloatField;
    QrProdutosCRICMF: TFloatField;
    QrProdutosCLk: TIntegerField;
    QrProdutosCNOMEFORNECEDOR: TWideStringField;
    QrProdutosCNOMETRANSPORTADOR: TWideStringField;
    QrProdutosCNOMECOLETOR: TWideStringField;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrProdutosCItsControle: TLargeintField;
    QrEmissTControle: TIntegerField;
    QrEmissTID_Pgto: TIntegerField;
    QrEmissFControle: TIntegerField;
    QrEmissFID_Pgto: TIntegerField;
    QrEmissFFatNum: TFloatField;
    QrEmissTFatNum: TFloatField;
    QrEmissFCliInt: TIntegerField;
    QrEmissTCliInt: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GridFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrProdutosCAfterOpen(DataSet: TDataSet);
    procedure QrProdutosCItsCalcFields(DataSet: TDataSet);
    procedure QrEmissTCalcFields(DataSet: TDataSet);
    procedure QrEmissFCalcFields(DataSet: TDataSet);
    procedure QrProdutosCAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure Duplicatafornecedor1Click(Sender: TObject);
    procedure Itemdeinsumo1Click(Sender: TObject);
    procedure DuplicataTransportadora1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrProdutosCCalcFields(DataSet: TDataSet);
    procedure BtInsereClick(Sender: TObject);
    procedure BtEditaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure PerguntaAQuery;
    procedure CriaOForm;
//    procedure LocalizaNome(Nome: String);
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro(Pedido : Integer);
    procedure AlteraRegistro(EnableBtDesiste, VerificaLk : Boolean);
    procedure IncluiSubRegistro;
    procedure ExcluiSubRegistro;
    procedure AlteraSubRegistro;
    //procedure IncluiItemDuplicataT;
    //procedure AlteraItemDuplicataT;
    procedure ExcluiItemDuplicataT;
    //procedure IncluiItemDuplicataF;
    //procedure AlteraItemDuplicataF;
    procedure ExcluiItemDuplicataF;

    procedure TravaOForm;

    //procs do form

    procedure DistribuiCustoDoFrete;
    procedure IniciaAlteracao(Codigo : Integer; EnableBtDesiste : Boolean);
    procedure DefineVarDup;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
//    procedure AtualizaDadosProduto;

  public
    { Public declarations }
    procedure InsereRegCondicionado(Pedido : Integer);
    procedure SubQuery1Reopen;
  end;

var
  FmProdutosC: TFmProdutosC;
  EPQPedido : Integer;

implementation

uses UnMyObjects, Curinga, Principal, Module, MyPagtos, ProdutosCNew,
  ProdutosCEdit, UnFinanceiro;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutosC.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmProdutosC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrProdutosCCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdutosC.DefParams;
begin
  VAR_GOTOTABELA := 'ProdutosC';
  VAR_GOTOmySQLTABLE := QrProdutosC;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pc.*,');
  VAR_SQLx.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  VAR_SQLx.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  VAR_SQLx.Add('CASE 1 WHEN tr.Tipo=0 THEN tr.RazaoSocial');
  VAR_SQLx.Add('ELSE tr.Nome END NOMETRANSPORTADOR,');
  VAR_SQLx.Add('CASE 1 WHEN co.Tipo=0 THEN co.RazaoSocial');
  VAR_SQLx.Add('ELSE co.Nome END NOMECOLETOR');
  VAR_SQLx.Add('FROM produtosc pc, Entidades fo,');
  VAR_SQLx.Add('Entidades tr, Entidades co');
  VAR_SQLx.Add('WHERE pc.Codigo>0');
  VAR_SQLx.Add('AND fo.Codigo=pc.Fornece');
  VAR_SQLx.Add('AND tr.Codigo=pc.Tranporta');
  VAR_SQLx.Add('AND co.Codigo=pc.Coletor');
  //
  VAR_SQL1.Add('AND pc.Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

(*procedure TFmProdutosC.AtualizaDadosProduto;
var
  ICMS, RICMS, IPI, RIPI, Frete, Juros, Preco, TValor, Pecas, Valor: Double;
begin
  TValor := QrProdutosCValor.Value;
  Pecas := QrProdutosCItsPecas.Value;
  Valor := QrProdutosCItsValor.Value;
  if (TValor > 0) and (Pecas > 0) then
  begin
    Frete := (QrProdutosCFrete.Value / TValor / Pecas) * Valor;
    Juros := (QrProdutosCJuros.Value / TValor / Pecas) * Valor;
  end else begin
    Frete := 0;
    Juros := 0;
  end;
  ICMS  := QrProdutosCICMS.Value;
  RICMS := QrProdutosCRICMS.Value;
//  RICMF := QrProdutosCRICMF.Value;
  IPI   := QrProdutosCItsIPI.Value;
  RIPI  := QrProdutosCItsRIPI.Value;
  if Pecas > 0 then Preco := (Valor / Pecas) * (1 - (ICMS / 100)) else Preco := 0;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE produtos SET PrecoC=:P0, ');
  Dmod.QrUpd.SQL.Add('IPI=:P1, RIPI=:P2, Frete=:P3, ');
  Dmod.QrUpd.SQL.Add('ICMS=:P4, RICMS=:P5, Juros=:P6');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Px');
  Dmod.QrUpd.Params[0].AsFloat := Preco;
  Dmod.QrUpd.Params[1].AsFloat := IPI;
  Dmod.QrUpd.Params[2].AsFloat := RIPI;
  Dmod.QrUpd.Params[3].AsFloat := Frete;
  Dmod.QrUpd.Params[4].AsFloat := ICMS;
  Dmod.QrUpd.Params[5].AsFloat := RICMS;
  Dmod.QrUpd.Params[6].AsFloat := Juros;
  Dmod.QrUpd.Params[7].AsInteger := QrProdutosCItsProduto.Value;
  Dmod.QrUpd.ExecSQL;
end;*)

procedure TFmProdutosC.DefineVarDup;
begin
  IC3_ED_FatNum := QrProdutosCCodigo.Value;
  IC3_ED_NF := Geral.IMV(EdNF.Text);
  IC3_ED_Data   := QrProdutosCDataC.Value;
  IC3_ED_Vencto := QrProdutosCDataC.Value;
end;

procedure TFmProdutosC.InsereRegCondicionado(Pedido : Integer);
//var
//  Status : StatusVP;
begin
  IncluiRegistro(Pedido);
  //Status := VerificaPesos(False);
  //if Status <> vpErro then DistribuiCustoDoFrete(Status);
  DistribuiCustoDoFrete;
end;

procedure TFmProdutosC.ExcluiItemDuplicataT;
var
  FatID, FatParcela, Sub : Integer;
  FatNum, Controle: Double;
  //
  Data: TDateTime;
  Tipo, Carteira: Integer;
begin
  if GOTOy.Registros(QrEmissT) = 0 then Exit;
  FatParcela := QrEmissTFatParcela.Value;
  FatNum     := QrEmissTFatNum.Value;
  FatID      := QrEmissTFatID.Value;
  Controle   := QrEmissTControle.Value;
  Sub        := QrEmissTSub.Value;
  Data       := QrEmissTData.Value;
  Tipo       := QrEmissTTipo.Value;
  Carteira   := QrEmissTCarteira.Value;
  //
  if (FatParcela = 0) and (FatNum=0) and (FatID=0) then Exit;
  if Application.MessageBox('Confirma a exclus�o deste registro?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    //UCash.ExcluiUnicoLancto(Controle, Sub, FatID, FatNum, FatParcela);
    (*UFinanceiroAtualizar
    UFinanceiro.ExcluiLct_Unico(True, Dmod.MyDB, Data, Tipo, Carteira, Controle, Sub, False);
    *)

    Dmod.RecalcSaldoCarteira(QrEmissTTipo.Value, QrEmissTCarteira.Value, 0);

    QrEmissT.Close;
    QrEmissT.Params[0].AsFloat  := FatNum;
    QrEmissT.Open;
    QrEmissT.Locate('FatParcela', FatParcela, []);
  end;
end;

procedure TFmProdutosC.ExcluiItemDuplicataF;
var
  Sub, FatID, FatParcela: Integer;
  FatNum, Controle: Double;
  //
  Data: TDateTime;
  Tipo, Carteira: Integer;
begin
  if GOTOy.Registros(QrEmissF) = 0 then Exit;
  FatParcela := QrEmissFFatParcela.Value;
  FatNum     := QrEmissFFatNum.Value;
  FatID      := QrEmissFFatID.Value;
  Controle   := QrEmissFControle.Value;
  Sub        := QrEmissFSub.Value;
  Data       := QrEmissTData.Value;
  Tipo       := QrEmissTTipo.Value;
  Carteira   := QrEmissTCarteira.Value;
  if (FatParcela = 0) and (FatNum=0) and (FatID=0) then Exit;
  if Application.MessageBox('Confirma a exclus�o deste registro?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    //UCash.ExcluiUnicoLancto(Controle, Sub, FatID, FatNum, FatParcela);
    (* UFinanceiroAtualizar
    UFinanceiro.ExcluiLct_Unico(True, Dmod.MyDB, Data, Tipo, Carteira, Controle, Sub, False);
    *)
    Dmod.RecalcSaldoCarteira(QrEmissFTipo.Value, QrEmissFCarteira.Value, 0);
    //
    QrEmissF.Close;
    QrEmissF.Params[0].AsFloat := FatNum;
    QrEmissF.Open;
    QrEmissF.Locate('FatParcela', FatParcela, []);
  end;
end;

(*procedure TFmProdutosC.DistribuiCustoDoFrete(Status : StatusVP);
var
  CFkg, RICMS, RICMSF : Double;
  Peso : String;
begin
  CFkg := 0;
  Peso := 'TotalPeso';
  RICMSF := (100-QrProdutosCRICMSF.VAlue)/100;
  RICMS := (100-QrProdutosCRICMS.Value)/100;
  if Status = vpLiq then
  begin
    if QrProdutosCPesoL.Value > 0 then
    CFkg := (QrProdutosCFrete.Value / QrProdutosCPesoL.Value) * RICMSF;
  end
  else if Status = vpBruto then
  begin
    if QrProdutosCPesoB.Value > 0 then
    CFkg := (QrProdutosCFrete.Value / QrProdutosCPesoB.Value) * RICMSF;
    Peso := '(Volumes*PesoVB)';
  end
  else
  begin
    Application.MessageBox(PChar('Imposs�vel distribuir custo do frete: Status desconhecido.'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  QrUpd.Close;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Clear;
  QrUpd.SQL.Add('UPDATE produtoscits SET');
  QrUpd.SQL.Add('TotalCusto=((ValorItem*(1+((IPI-RIPI)/100)))*:P0)+('+Peso+'*:P1)');
  QrUpd.SQL.Add('WHERE Codigo=:P2');

  QrUpd.Params[0].AsFloat := RICMS;
  QrUpd.Params[1].AsFloat := CFkg;
  QrUpd.Params[2].AsInteger := QrProdutosCCodigo.Value;

  QrUpd.ExecSQL;

  QueryPrincipalAfterOpen;
  //
end; *)

procedure TFmProdutosC.DistribuiCustoDoFrete;
var
  Frete, RICMS, RICMSF, CFin: Double;
  Entrada: Integer;
begin
  Entrada := QrProdutosCCodigo.Value;
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := Entrada;
  QrSoma.Open;
  ///
  Frete := 0;
  CFin := 0;
  RICMSF := (100-QrProdutosCRICMF.Value)/100;
  RICMS := (100-QrProdutosCRICMS.Value)/100;
  if QrSomaValor.Value > 0 then
  begin
    Frete := (QrProdutosCFrete.Value / QrSomaValor.Value) * RICMSF;
    CFin  := (QrProdutosCJuros.Value / QrSomaValor.Value);
  end;
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE produtoscits SET');
  Dmod.QrUpd.SQL.Add('Custo=Valor+(Valor*:P0) ');
  Dmod.QrUpd.SQL.Add('- (Valor * (1- :P1)) + (Valor* :P2) ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P3');
  Dmod.QrUpd.Params[0].AsFloat := CFin;
  Dmod.QrUpd.Params[1].AsFloat := RICMS;
  Dmod.QrUpd.Params[2].AsFloat := Frete;
  Dmod.QrUpd.Params[3].AsInteger := Entrada;
  Dmod.QrUpd.ExecSQL;
  //
//  QrSoma.Close;
//  QrSoma.Params[0].AsInteger := Entrada;
//  QrSoma.Open;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE produtosc SET ');
  Dmod.QrUpdU.SQL.Add('Valor=:P0, Peso=:P1, ');
  Dmod.QrUpdU.SQL.Add('Total=:P2 WHERE Codigo=:P3');
  Dmod.QrUpdU.SQL.Add('');
  Dmod.QrUpdU.Params[0].AsFloat := QrSomaValor.Value;
  Dmod.QrUpdU.Params[1].AsFloat := QrSomaPeso.Value;
  Dmod.QrUpdU.Params[2].AsFloat := QrSomaCusto.Value;// + QrProdutosCJuros.Value;
  Dmod.QrUpdU.Params[3].AsInteger := Entrada;
  Dmod.QrUpdU.ExecSQL;
  //
  LocCod(Entrada, Entrada);
end;

procedure TFmProdutosC.ExcluiSubRegistro;
var
  Codigo, Conta, Produto : Integer;
  Controle: Double;
begin
  Produto := QrProdutosCItsProduto.Value;
  Codigo := QrProdutosCItsCodigo.Value;
  Conta := QrProdutosCItsConta.Value;
  Controle := QrProdutosCItsControle.Value;
  if Application.MessageBox('Confirma a exclus�o deste registro?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM produtoscits WHERE Codigo=:P0');
    Dmod.QrUpd.SQL.Add('AND Conta=:P1 AND Controle=:P2');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.Params[1].AsInteger := Conta;
    Dmod.QrUpd.Params[2].AsFloat := Controle;
    Dmod.QrUpd.ExecSQL;

    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE produtoscits SET Conta=Conta-1');
    Dmod.QrUpd.SQL.Add('WHERE Conta>:P0 AND Codigo=:P1');
    Dmod.QrUpd.Params[0].AsInteger := Conta;
    Dmod.QrUpd.Params[1].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;

    QrProdutosCIts.Close;
    QrProdutosCIts.Params[0].AsInteger := Codigo;
    QrProdutosCIts.Open;
    QrProdutosCIts.Locate('Conta', Conta, []);

    UnAcademy.AtualizaEstoqueMercadoria(Produto, aeMsg, CO_VAZIO);
  end;
end;

procedure TFmProdutosC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
  QrFornecedor.Open;
  QrColetor.Open;
{
  with FmProdutosC do
    Width := 800;
    if Screen.Width = 640 then // 480
      begin
        Application.MessageBox(PChar('Para melhor resolu��o desta janela,' + Chr(13) +
                    'modifique a configura��o de v�deo' + Chr(13) +
                    'para 600 x 800 pixels.'), 'Erro', MB_OK+MB_ICONERROR);
        Top := 0;
        Left := 0;
        Height := 450;
      end
    else
    if Screen.Width = 800 then  // 600
      begin
        Top := 40;
        Left := 0;
        Height := 530;
      end
    else                       //  1024 * 768
      begin
        Top := 120;
        Left := 112;
        Height := 600;
    end;
}
end;

procedure TFmProdutosC.TravaOForm;
begin
  LaTipo.caption := CO_TRAVADO;
  PainelConfirma.Visible:=False;
  PainelControle.Visible:=True;
  if QrProdutosCCodigo.Value > 0 then
    BtAltera.Enabled := True;
  GOTOy.BotoesSb(LaTipo.Caption);
  UMyMod.UpdUnlockY(QrProdutosCCodigo.Value, Dmod.MyDB, 'ProdutosC', 'Codigo');
end;

procedure TFmProdutosC.AlteraSubRegistro;
var
  Cursor : TCursor;
  Seq, Codigo : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  Seq := QrProdutosCItsConta.Value;
  Codigo := QrProdutosCCodigo.Value;
  try
    Application.CreateForm(TFmProdutosCEdit,FmProdutosCEdit);
    FmProdutosCEdit.EdCodigo.Enabled := False;
    FmProdutosCEdit.LaTipo.Caption := CO_ALTERACAO;
    FmProdutosCEdit.EdCodigo.ValueVariant := QrProdutosCItsConta.Value;

    FmProdutosCEdit.EdProduto.Text := Geral.TFT(IntToStr(QrProdutosCItsProduto.Value), 0, siPositivo);
    FmProdutosCEdit.CBProduto.KeyValue := QrProdutosCItsProduto.Value;
    FmProdutosCEdit.EdPreco.Text := Geral.TFT(FloatToStr(QrProdutosCItsPreco.Value), 4, siPositivo);
    FmProdutosCEdit.EdDesco.Text := Geral.TFT(FloatToStr(QrProdutosCItsDesco.Value), 2, siPositivo);
    //
    FmProdutosCEdit.EdBruto.Text := FloatToStr(QrProdutosCItsBruto.Value); // casas decimais variadas
    FmProdutosCEdit.EdTara.Text  := Geral.TFT(FloatToStr(QrProdutosCItsTara.Value),  3, siPositivo);
    FmProdutosCEdit.EdImpur.Text := Geral.TFT(FloatToStr(QrProdutosCItsImpur.Value), 3, siPositivo);
    //
  finally
    Screen.Cursor := Cursor;
  end;
  FmProdutosCEdit.ShowModal;
  FmProdutosCEdit.Destroy;
  DefParams;
  QrProdutosCIts.Close;
  QrProdutosCIts.Params[0].AsInteger := Codigo;
  QrProdutosCIts.Open;
  QrProdutosCIts.Locate('Conta', Seq, []);
end;

procedure TFmProdutosC.AlteraRegistro(EnableBtDesiste, VerificaLk : Boolean);
var
  ProdutosC : Integer;
begin
  ProdutosC := QrProdutosCCodigo.Value;
  if QrProdutosCCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not VerificaLk then IniciaAlteracao(ProdutosC, EnableBtDesiste)
  else
    if UMyMod.SelLockY(ProdutosC, Dmod.MyDB, 'ProdutosC', 'Codigo') then Exit
    else  IniciaAlteracao(ProdutosC, EnableBtDesiste);
end;

procedure TFmProdutosC.IniciaAlteracao(Codigo : Integer; EnableBtDesiste : Boolean);
begin
  try
    UMyMod.UpdLockY(Codigo, Dmod.MyDB, 'ProdutosC', 'Codigo');
    PainelControle.Visible:=False;
    PainelConfirma.Visible:=True;

    LaTipo.Caption := CO_ALTERACAO;
    GOTOy.BotoesSb(LaTipo.Caption);
    Application.CreateForm(TFmProdutosCNew,FmProdutosCNew);
    with FmProdutosCNew do
    begin
    //fazer reteio juros
      BtDesiste.Enabled := EnableBtDesiste;
      LaTipo.Caption := CO_ALTERACAO;
      EdCodigo.Text := IntToStr(Codigo);
      EdLote.Text := IntToStr(QrProdutosCLote.Value);
      EdFabricante.Text := IntToStr(QrProdutosCColetor.Value);
      EdRepresentante.Text := IntToStr(QrProdutosCFornece.Value);
      EdTransportador.Text := IntToStr(QrProdutosCTranporta.Value);
      CBRepresentante.KeyValue := QrProdutosCFornece.Value;
      CBTransportador.KeyValue := QrProdutosCTranporta.Value;
      CBFabricante.KeyValue := QrProdutosCColetor.Value;
      EdNFC.Text := IntToStr(QrProdutosCNFC.Value);
      EdNFF.Text := IntToStr(QrProdutosCNFF.Value);
      EdICMS.Text := Geral.TFT(FloatToStr(QrProdutosCICMS.Value), 2, siPositivo);
      EdRICMS.Text := Geral.TFT(FloatToStr(QrProdutosCRICMS.Value), 2, siPositivo);
      EdRICMF.Text := Geral.TFT(FloatToStr(QrProdutosCRICMF.Value), 2, siPositivo);
      EdFrete.Text := Geral.TFT(FloatToStr(QrProdutosCFrete.Value), 2, siPositivo);
      EdJuros.Text := Geral.TFT(FloatToStr(QrProdutosCJuros.Value), 2, siPositivo);
      TPDataC.Date := QrProdutosCDataC.Value;
      TPDataE.Date := QrProdutosCDataE.Value;
    end;
    GOTOy.BotoesSb(LaTipo.Caption);
  finally
  Screen.Cursor := Cursor;
  end;
  FmProdutosCNew.ShowModal;
  FmProdutosCNew.Destroy;
  DefParams;
  LocCod(Codigo, Codigo);
end;

procedure TFmProdutosC.IncluiRegistro(Pedido : Integer);
var
  Cursor : TCursor;
  ProdutosC : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ProdutosC := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ProdutosC', 'ProdutosC', 'Codigo');
    Application.CreateForm(TFmProdutosCNew,FmProdutosCNew);
    FmProdutosCNew.LaTipo.Caption := CO_INCLUSAO;
    FmProdutosCNew.EdCodigo.Text := FormatFloat('000', ProdutosC);
    FmProdutosCNew.TPDataC.Date := Now;
    FmProdutosCNew.TPDataE.Date := Now;
    // Dados do Pedido (se existir)
(*    if EPQPedido > 0 then
    begin
      QrPedido.Close;
      QrPedido.Params[0].AsInteger := FmProdutosCPed.CBPedido.KeyValue;
      QrPedido.Open;
      FmProdutosCNew.EdPedido.Text := IntToStr(Pedido);
      FmProdutosCNew.LaSitPedido.Caption := IntToStr(EPQPedido);
      FmProdutosCNew.EdFornecedor.Text := IntToStr(QrPedidoFornece.Value);
      FmProdutosCNew.EdTransportador.Text := IntToStr(QrPedidoTransporte.Value);
      FmProdutosCNew.CBFornecedor.KeyValue := QrPedidoFornece.Value;
      FmProdutosCNew.CBTransportador.KeyValue := QrPedidoTransporte.Value;
      FmProdutosCNew.EdkgBruto.Text := FloatToStr(QrPedidoPesoB.Value);
      FmProdutosCNew.EdkgLiquido.Text := FloatToStr(QrPedidoPesoL.Value);
      FmProdutosCNew.EdValorNF.Text := FloatToStr(QrPedidoValor.Value);
    end;*)
    PainelControle.Visible:=False;
    PainelConfirma.Visible:=True;
    GOTOy.BotoesSb(LaTipo.Caption);
  finally
    Screen.Cursor := Cursor;
  end;
  FmProdutosCNew.ShowModal;
  FmProdutosCNew.Destroy;
  EPQPedido := 0;//zera aviso de  entrada de pedido
  DefParams;
  LocCod(ProdutosC, ProdutosC);
end;

procedure TFmProdutosC.QueryPrincipalAfterOpen;
begin
  SubQuery1Reopen;
end;

procedure TFmProdutosC.DefineONomeDoForm;
begin
end;

procedure TFmProdutosC.SubQuery1Reopen;
begin
  QrProdutosCIts.Close;
  QrProdutosCIts.Params[0].AsInteger := QrProdutosCCodigo.Value;
  QrProdutosCIts.Open;

  QrEmissT.Close;
  QrEmissT.Params[0].AsInteger := QrProdutosCCodigo.Value;
  QrEmissT.Open;

  QrEmissF.Close;
  QrEmissF.Params[0].AsInteger := QrProdutosCCodigo.Value;
  QrEmissF.Open;

end;

procedure TFmProdutosC.IncluiSubRegistro;
var
  Cursor : TCursor;
  Conta, Codigo : Integer;
begin
  Conta := 1;
  Codigo := FmProdutosC.QrProdutosCCodigo.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmProdutosCEdit,FmProdutosCEdit);
    FmProdutosCEdit.LaTipo.Caption := CO_INCLUSAO;
    if FmProdutosC.QrProdutosCItsConta.Value > 0 then
       Conta := FmProdutosC.QrProdutosCItsConta.Value+1;
    FmProdutosCEdit.EdCodigo.ValMax := FormatFloat('0', QrProdutosCIts.RecordCount + 1);
    FmProdutosCEdit.EdCodigo.ValueVariant := Conta;
  finally
    Screen.Cursor := Cursor;
  end;
  FmProdutosCEdit.ShowModal;
  FmProdutosCEdit.Destroy;
  DefParams;
  Conta := QrProdutosCItsConta.Value;
  QrProdutosCIts.Close;
  QrProdutosCIts.Params[0].AsInteger := Codigo;
  QrProdutosCIts.Open;
  QrProdutosCIts.Locate('Conta', Conta, []);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProdutosC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutosC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutosC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutosC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutosC.BtIncluiClick(Sender: TObject);
begin
  InsereRegCondicionado(0);
end;

procedure TFmProdutosC.BtAlteraClick(Sender: TObject);
//var
//  Status : StatusVP;
begin
  if UnAcademy.ImpedePeloBalanco(QrProdutosCDataC.Value) then exit;
  AlteraRegistro(True, True);
  //Status := VerificaPesos(False);
  //if Status <> vpErro then DistribuiCustoDoFrete(Status);
  DistribuiCustoDoFrete;
end;

procedure TFmProdutosC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosC.BtTravaClick(Sender: TObject);
var
  Erro : Double;
begin
  DistribuiCustoDoFrete;
  //recalcula de novo
  DistribuiCustoDoFrete;
  //
  QrConfT.Close;
  QrConfT.Params[0].AsInteger := QrProdutosCCodigo.Value;
  QrConfT.Open;
  QrSumT.Close;
  QrSumT.Params[0].AsInteger := QrProdutosCCodigo.Value;
  QrSumT.Open;
  QrSumF.Close;
  QrSumF.Params[0].AsInteger := QrProdutosCCodigo.Value;
  QrSumF.Open;
  EdPagT.Text := Geral.TFT(FloatToStr(QrSumTDebito.Value-QrProdutosCFrete.Value), 2, siNegativo);
  EdPagF.Text := Geral.TFT(FloatToStr(QrSumFDebito.Value-(QrProdutosCTotal.Value-QrProdutosCFrete.Value)), 2, siNegativo);

  Erro :=
  -Geral.DMV(EdPagT.Text)
  -Geral.DMV(EdPagF.Text);

  if (Erro > 0.009) or (Erro < -0.009) then
  begin
    Application.MessageBox('Ocorreu um erro na confer�ncia!', 'Erro',
    MB_OK+MB_ICONERROR);
    AlteraRegistro(False, False);
    DistribuiCustoDoFrete;
    Exit;
  end;
  QrProdutosCIts.DisableControls;
  QrProdutosCIts.First;
  while not QrProdutosCIts.Eof do
  begin
    UnAcademy.AtualizaEstoqueMercadoria(QrProdutosCItsProduto.Value, aeMsg, CO_VAZIO);
    QrProdutosCIts.Next;
  end;
  QrProdutosCIts.EnableControls;
  TravaOForm;
  LocCod(QrProdutosCCodigo.Value, QrProdutosCCodigo.Value);
end;

procedure TFmProdutosC.FormCreate(Sender: TObject);
begin
  CriaOForm;
  EPQPedido := 0;// sem pedido
end;

procedure TFmProdutosC.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_TAB) then GridT.SetFocus;
  if (Key=VK_TAB) and (Shift = [ssShift]) then DBGrid1.SetFocus;
  if LaTipo.Caption <> CO_TRAVADO then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiSubRegistro;
  end;
end;

procedure TFmProdutosC.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrProdutosCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProdutosC.SbNomeClick(Sender: TObject);
begin
  PerguntaAQuery;
end;

procedure TFmProdutosC.SbQueryClick(Sender: TObject);
begin
  PerguntaAQuery;
end;

procedure TFmProdutosC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LATipo.Caption);
end;

procedure TFmProdutosC.GridFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_TAB) then BtTrava.SetFocus;
  if (Key=VK_TAB) and (Shift = [ssShift]) then GridT.SetFocus;
  if LaTipo.Caption <> CO_TRAVADO then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiItemDuplicataF;
  end;
end;

procedure TFmProdutosC.GridTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_TAB) then GridF.SetFocus;
  if (Key=VK_TAB) and (Shift = [ssShift]) then DBGrid1.SetFocus;
  if LaTipo.Caption <> CO_TRAVADO then
  begin
    if (key=VK_DELETE) and (Shift = [ssCtrl]) then ExcluiItemDuplicataT;
  end;

end;

procedure TFmProdutosC.QrProdutosCAfterOpen(DataSet: TDataSet);
begin
    GOTOy.BtEnabled(QrProdutosCCodigo.Value, False);
  QueryPrincipalAfterOpen;
end;

procedure TFmProdutosC.QrProdutosCItsCalcFields(DataSet: TDataSet);
begin
  QrProdutosCItsPESOLIQUIDO.Value :=
  QrProdutosCItsBruto.Value -
  QrProdutosCItsTara.Value -
  QrProdutosCItsImpur.Value;
  if QrProdutosCItsPESOLIQUIDO.Value > 0 then
     QrProdutosCItsCUSTOUNITARIO.Value :=
     QrProdutosCItsCusto.Value /
     QrProdutosCItsPESOLIQUIDO.Value
  else
     QrProdutosCItsCUSTOUNITARIO.Value := 0;
end;

procedure TFmProdutosC.QrEmissTCalcFields(DataSet: TDataSet);
begin
  if QrEmissTSit.Value = 1 then
     QrEmissTNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrEmissTSit.Value = 2 then
       QrEmissTNOMESIT.Value := CO_QUITADA
  else
    if QrEmissTVencimento.Value < Date then
       QrEmissTNOMESIT.Value := CO_VENCIDA
  else
       QrEmissTNOMESIT.Value := CO_EMABERTO;

  case QrEmissTTipo.Value of
    0: QrEmissTNOMETIPO.Value := CO_CAIXA;
    1: QrEmissTNOMETIPO.Value := CO_BANCO;
    2: QrEmissTNOMETIPO.Value := CO_EMISS;
  end;

end;

procedure TFmProdutosC.QrEmissFCalcFields(DataSet: TDataSet);
begin
  if QrEmissFSit.Value = 1 then
     QrEmissFNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrEmissFSit.Value = 2 then
       QrEmissFNOMESIT.Value := CO_QUITADA
  else
    if QrEmissFVencimento.Value < Date then
       QrEmissFNOMESIT.Value := CO_VENCIDA
  else
       QrEmissFNOMESIT.Value := CO_EMABERTO;

  case QrEmissFTipo.Value of
    0: QrEmissFNOMETIPO.Value := CO_CAIXA;
    1: QrEmissFNOMETIPO.Value := CO_BANCO;
    2: QrEmissFNOMETIPO.Value := CO_EMISS;
  end;

end;

procedure TFmProdutosC.QrProdutosCAfterScroll(DataSet: TDataSet);
begin
  SubQuery1Reopen;
  if QrProdutosCCodigo.Value > 0 then BtAltera.Enabled := True
  else BtAltera.Enabled := False;
end;

procedure TFmProdutosC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosC.Duplicatafornecedor1Click(Sender: TObject);
var
  Terceiro, Cod : Integer;
  Valor: Double;
begin
  DefineVarDup;
  Cod       := FmProdutosC.QrProdutosCCodigo.Value;
  Terceiro  := FmProdutosC.QrProdutosCFornece.Value;
  Valor     := FmProdutosC.QrProdutosCValor.Value+FmProdutosC.QrProdutosCJuros.Value;
  (* UCashierDesativado
  UCash.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_2102, 0, stIns,
  'Compra de Mercadorias Diversas', Valor, VAR_USUARIO, 0, FmPrincipal.FEntInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0);
  *)
  SubQuery1Reopen;
  //IncluiItemDuplicataF;
end;

(*procedure TFmProdutosC.IncluiItemDuplicataF;
var
  Cursor : TCursor;
  Parcela, Codigo : Integer;
begin
  VAR_FATID := 102;
  Parcela := 1;
  Codigo := FmProdutosC.QrProdutosCCodigo.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmMyPagtos,FmMyPagtos);
    FmMyPagtos.LaTipo.Caption := CO_INCLUSAO;
    FmMyPagtos.LaDefDuplicata.Caption := 'Compra de Produtos';
    if FmProdutosC.QrEmissFFatParcela.Value > 0 then
       Parcela := FmProdutosC.QrEmissFFatParcela.Value+1;
    FmMyPagtos.UDCodigo.Max := QrEmissF.RecordCount + 1;
    FmMyPagtos.UDCodigo.Position := Parcela;
    FmMyPagtos.TPVencimento.Date := Now();
    FmMyPagtos.EdCodigo.Enabled := True;
    //
    FmMyPagtos.LaCredor.Visible := True;
    FmMyPagtos.EdCredor.Visible := True;
    FmMyPagtos.CBCredor.Visible := True;
    FmMyPagtos.EdCredor.Text := IntToStr(QrProdutosCFornece.Value);
    FmMyPagtos.CBCredor.KeyValue := QrProdutosCFornece.Value;
  finally
    Screen.Cursor := Cursor;
  end;
  FmMyPagtos.ShowModal;
  FmMyPagtos.Destroy;
  Parcela := QrEmissFFatParcela.Value;
  QrEmissF.Close;
  QrEmissF.Params[0].AsInteger := Codigo;
  QrEmissF.Open;
  QrEmissF.Locate('FatParcela', Parcela, []);
end;*)

procedure TFmProdutosC.Itemdeinsumo1Click(Sender: TObject);
var
  ProdutoC: Integer;
begin
  ProdutoC := QrProdutosCCodigo.Value;
  IncluiSubRegistro;
  DistribuiCustoDoFrete;
  //UnAcademy.AtualizaEstoqueMercadoria(Produto, aeMsg, CO_VAZIO);
  LocCod(ProdutoC, ProdutoC);
end;

procedure TFmProdutosC.DuplicataTransportadora1Click(Sender: TObject);
var
  Terceiro, Cod : Integer;
  Valor: Double;
begin
  DefineVarDup;
  Cod       := FmProdutosC.QrProdutosCCodigo.Value;
  Terceiro  := FmProdutosC.QrProdutosCTranporta.Value;
  Valor     := FmProdutosC.QrProdutosCFrete.Value;
  (* UCashierDesativado
  UCash.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_2103, 0, stIns,
  'Tranporte de Mercadorias Compradas', Valor, VAR_USUARIO, 0, FmPrincipal.FEntInt, mmNenhum,
  0, 0, True, False, 0, 0, 0, 0);
  *)
  SubQuery1Reopen;
  //IncluiItemDuplicataT;
end;

(*procedure TFmProdutosC.IncluiItemDuplicataT;
var
  Cursor : TCursor;
  Parcela, Codigo : Integer;
begin
  VAR_FATID := 103;
  Parcela := 1;
  Codigo := FmProdutosC.QrProdutosCCodigo.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmMyPagtos,FmMyPagtos);
    FmMyPagtos.LaTipo.Caption := CO_INCLUSAO;
    FmMyPagtos.LaDefDuplicata.Caption := 'Transporte de Produtos';
    if FmProdutosC.QrEmissTFatParcela.Value > 0 then
       Parcela := FmProdutosC.QrEmissTFatParcela.Value+1;
    FmMyPagtos.UDCodigo.Max := QrEmissT.RecordCount + 1;
    FmMyPagtos.UDCodigo.Position := Parcela;
    FmMyPagtos.TPVencimento.Date := Now();
    FmMyPagtos.EdCodigo.Enabled := True;
    //
    FmMyPagtos.LaCredor.Visible := True;
    FmMyPagtos.EdCredor.Visible := True;
    FmMyPagtos.CBCredor.Visible := True;
    FmMyPagtos.EdCredor.Text := IntToStr(QrProdutosCTranporta.Value);
    FmMyPagtos.CBCredor.KeyValue := QrProdutosCTranporta.Value;
  finally
    Screen.Cursor := Cursor;
  end;
  FmMyPagtos.ShowModal;
  FmMyPagtos.Destroy;
  Parcela := QrEmissTFatParcela.Value;
  QrEmissT.Close;
  QrEmissT.Params[0].AsInteger := Codigo;
  QrEmissT.Open;
  QrEmissT.Locate('FatParcela', Parcela, []);
end;*)

procedure TFmProdutosC.MenuItem2Click(Sender: TObject);
(*begin
  IC3_ED_Controle := QrEmissTControle.Value;
  DefineVarDup;
  AlteraItemDuplicataT;*)
var
  Terceiro, Cod : Integer;
  Valor: Double;
begin
  if QrEmissT.RecordCount = 0 then Exit;
  if (QrEmissTTipo.Value = 2) and (QrEmissTSit.Value = 2) then
  begin
    Application.MessageBox(VAR_MSG_EMISSBXNAOEDIT, 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  IC3_ED_Controle := QrEmissTControle.Value;
  DefineVarDup;
  Cod       := QrProdutosCCodigo.Value;
  Terceiro  := QrProdutosCTranporta.Value;
  Valor     := QrEmissTDebito.Value;
  (* UCashierDesativado
  UCash.Pagto(QrEmissT, tpDeb, Cod, Terceiro, VAR_FATID_2103, 0, stUpd,
  'Tranporte de Mercadorias Compradas', Valor, VAR_USUARIO, 0, FmPrincipal.FEntInt, mmNenhum,
  0, 0, True, False, 0, 0, 0, 0);
  *)
  SubQuery1Reopen;
  //AlteraItemDuplicataT;
end;

(*procedure TFmProdutosC.AlteraItemDuplicataT;
var
  Cursor : TCursor;
  Parcela, Codigo : Integer;
begin
  if QrEmissT.RecordCount = 0 then Exit;
  if (QrEmissTTipo.Value = 2) and (QrEmissTSit.Value = 2) then
  begin
    Application.MessageBox(PChar(VAR_MSG_EMISSBXNAOEDIT), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  VAR_FATID := 103;
  Parcela := QrEmissTFatParcela.Value;
  Codigo := QrProdutosCCodigo.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmMyPagtos,FmMyPagtos);
    FmMyPagtos.LaTipo.Caption := CO_ALTERACAO;
    FmMyPagtos.LaDefDuplicata.Caption := 'Transporte de produtos';
    FmMyPagtos.UDCodigo.Enabled := False;
    FmMyPagtos.EdCodigo.Enabled := False;

    FmMyPagtos.UDCodigo.Position := Parcela;
    FmMyPagtos.RGTipo.ItemIndex := QrEmissTTipo.Value;
    FmMyPagtos.EdCarteira.Text := IntToStr(QrEmissTCarteira.Value);
    FmMyPagtos.CBCarteira.KeyVAlue := QrEmissTCarteira.Value;
    FmMyPagtos.EdValor.Text :=
      Geral.TFT(FloatToStr(QrEmissTDebito.Value), 2, siPositivo);
    FmMyPagtos.EdDocumento.Text := FloatToStr(QrEmissTDocumento.Value);
    FmMyPagtos.TPVencimento.Date := QrEmissTVencimento.Value;
    //
    FmMyPagtos.LaCredor.Visible := True;
    FmMyPagtos.EdCredor.Visible := True;
    FmMyPagtos.CBCredor.Visible := True;
    FmMyPagtos.EdCredor.Text := IntToStr(QrEmissTFornecedor.Value);
    FmMyPagtos.CBCredor.KeyValue := QrEmissTFornecedor.Value;
  finally
    Screen.Cursor := Cursor;
  end;
  FmMyPagtos.ShowModal;
  FmMyPagtos.Destroy;
  Parcela := QrEmissTFatParcela.Value;
  QrEmissT.Close;
  QrEmissT.Params[0].AsInteger := Codigo;
  QrEmissT.Open;
  QrEmissT.Locate('FatParcela', Parcela, []);
end;*)

procedure TFmProdutosC.MenuItem1Click(Sender: TObject);
var
  ProdutoC, Produto: Integer;
begin
  ProdutoC := QrProdutosCCodigo.Value;
  Produto := QrProdutosCItsProduto.Value;
  AlteraSubRegistro;
  DistribuiCustoDoFrete;
    UnAcademy.AtualizaEstoqueMercadoria(Produto, aeMsg, CO_VAZIO);
  LocCod(ProdutoC, ProdutoC);
end;

procedure TFmProdutosC.MenuItem3Click(Sender: TObject);
(*begin
  IC3_ED_Controle := QrEmissFControle.Value;
  DefineVarDup;
  AlteraItemDuplicataF;*)
var
  Terceiro, Cod : Integer;
  Valor: Double;
begin
  if QrEmissF.RecordCount = 0 then Exit;
  if (QrEmissFTipo.Value = 2) and (QrEmissFSit.Value = 2) then
  begin
    Application.MessageBox(VAR_MSG_EMISSBXNAOEDIT, 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  IC3_ED_Controle := QrEmissFControle.Value;
  DefineVarDup;
  Cod       := QrProdutosCCodigo.Value;
  Terceiro  := QrProdutosCFornece.Value;
  Valor     := QrEmissFDebito.Value;
  (* UCashierDesativado
  UCash.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_2102, 0, stUpd,
  'Compra de Mercadorias Diversas', Valor, VAR_USUARIO, 0, FmPrincipal.FEntInt, mmNenhum,
  0, 0, True, False, 0, 0, 0, 0);
  *)
  SubQuery1Reopen;
  //AlteraItemDuplicataF;
end;

(*procedure TFmProdutosC.AlteraItemDuplicataF;
var
  Cursor : TCursor;
  Parcela, Codigo : Integer;
begin
  if QrEmissF.RecordCount = 0 then Exit;
  if (QrEmissFTipo.Value = 2) and (QrEmissFSit.Value = 2) then
  begin
    Application.MessageBox(PChar(VAR_MSG_EMISSBXNAOEDIT), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  VAR_FATID := 102;
  Parcela := QrEmissFFatParcela.Value;
  Codigo := QrProdutosCCodigo.Value;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmMyPagtos,FmMyPagtos);
    FmMyPagtos.LaTipo.Caption := CO_ALTERACAO;
    FmMyPagtos.LaDefDuplicata.Caption := 'Compra de Produtos';
    FmMyPagtos.UDCodigo.Enabled := False;
    FmMyPagtos.EdCodigo.Enabled := False;

    FmMyPagtos.UDCodigo.Position := Parcela;
    FmMyPagtos.RGTipo.ItemIndex := QrEmissFTipo.Value;
    FmMyPagtos.EdCarteira.Text := IntToStr(QrEmissFCarteira.Value);
    FmMyPagtos.CBCarteira.KeyVAlue := QrEmissFCarteira.Value;
    FmMyPagtos.EdValor.Text :=
      Geral.TFT(FloatToStr(QrEmissFDebito.Value), 2, siPositivo);
    FmMyPagtos.EdDocumento.Text := FloatToStr(QrEmissFDocumento.Value);
    FmMyPagtos.TPVencimento.Date := QrEmissFVencimento.Value;
    //
    FmMyPagtos.LaCredor.Visible := True;
    FmMyPagtos.EdCredor.Visible := True;
    FmMyPagtos.CBCredor.Visible := True;
    FmMyPagtos.EdCredor.Text := IntToStr(QrEmissFFornecedor.Value);
    FmMyPagtos.CBCredor.KeyValue := QrEmissFFornecedor.Value;
  finally
    Screen.Cursor := Cursor;
  end;
  FmMyPagtos.ShowModal;
  FmMyPagtos.Destroy;
  Parcela := QrEmissFFatParcela.Value;
  QrEmissF.Close;
  QrEmissF.Params[0].AsInteger := Codigo;
  QrEmissF.Open;
  QrEmissF.Locate('FatParcela', Parcela, []);
end;*)

procedure TFmProdutosC.BtExcluiClick(Sender: TObject);
var
  Cancela, FatParcela, FatID: Integer;
  FatNum: Double;
begin
  Cancela := 0;
  if Application.MessageBox('Confirma a exclus�o de toda nota fiscal?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    QrEmissF.First;
    while not QrEmissF.Eof do
    begin
      if (QrEmissFTipo.Value = 2) and  (QrEmissFSit.Value <> 0) then
      Cancela := Cancela + 1;
      QrEmissF.Next;
    end;
    QrEmissT.First;
    while not QrEmissT.Eof do
    begin
      if (QrEmissTTipo.Value = 2) and  (QrEmissTSit.Value <> 0) then
      Cancela := Cancela + 1;
      QrEmissT.Next;
    end;
    if Cancela > 0 then
    begin
      Application.MessageBox(PChar('Exclus�o cancelada. Existem '+IntToStr(Cancela)
      +' duplicatas que j� foram baixadas ou parcialmente pagas.'), 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Dmod.QrUpdM.SQL.Clear;
    //
    //Evitar Erro grav�ssimo
    //UCash.ExcluiTodosLct(QrEmissFFatID.Value, QrEmissFFatNum.Value);
    //UCash.ExcluiTodosLct(QrEmissTFatID.Value, QrEmissTFatNum.Value);
    (* UFinanceiroAtualizar
    UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB,
      QrEmissFFatID.Value, QrEmissFFatNum.Value,
      QrEmissFCliInt.Value, QrEmissFCarteira.Value, False);
    UFinanceiro.ExcluiLct_FatNum(Dmod.MyDB,
      QrEmissTFatID.Value, QrEmissTFatNum.Value,
      QrEmissTCliInt.Value, QrEmissFCarteira.Value, False);
    *)
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM produtoscits WHERE Codigo='''+IntToStr(QrProdutosCCodigo.Value)+'''');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM produtosc WHERE Codigo='''+IntToStr(QrProdutosCCodigo.Value)+'''');
    Dmod.QrUpd.ExecSQL;
    //
    QrEmissF.First;
    while not QrEmissF.Eof do
    begin
      FatParcela := QrEmissFFatParcela.Value;
      FatNum := QrEmissFFatNum.Value;
      FatID := QrEmissFFatID.Value;
      if (FatParcela<>0) and (FatNum<>0) and (FatID<>0) then
        Dmod.RecalcSaldoCarteira(QrEmissFTipo.Value, QrEmissFCarteira.Value, 0);
      QrEmissF.Next;
    end;

    QrEmissT.First;
    while not QrEmissT.Eof do
    begin
      FatParcela := QrEmissTFatParcela.Value;
      FatNum := QrEmissTFatNum.Value;
      FatID := QrEmissTFatID.Value;
      if (FatParcela <> 0) and (FatNum<>0) and (FatID<>0) then
        Dmod.RecalcSaldoCarteira(QrEmissTTipo.Value, QrEmissTCarteira.Value, 0);
      QrEmissT.Next;
    end;
  end;

  QrProdutosCIts.First;
  while not QrProdutosCIts.Eof do
  begin
    UnAcademy.AtualizaEstoqueMercadoria(QrProdutosCItsProduto.Value, aeMsg, CO_VAZIO);
    QrProdutosCIts.Next;
  end;
  LocCod(QrProdutosCCodigo.Value, QrProdutosCCodigo.Value);
end;

procedure TFmProdutosC.PerguntaAQuery;
//var
//  Cursor : TCursor;
begin
(*  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmProdutosCPesq, FmProdutosCPesq);
    FmProdutosCPesq.ShowModal;
    FmProdutosCPesq.Destroy;
  finally
    Screen.Cursor := Cursor;
  end;
  if VAR_ProdutosCLANCTO <> -1 then LocCod(VAR_ProdutosCLANCTO, VAR_ProdutosCLANCTO);*)
end;

procedure TFmProdutosC.QrProdutosCCalcFields(DataSet: TDataSet);
begin
(*  if QrProdutosCPecas.Value > 0 then
  QrProdutosCFRETEKG.Value := QrProdutosCFrete.Value / QrProdutosCPesoL.Value
  else QrProdutosCFRETEKG.Value := 0;*)
end;

procedure TFmProdutosC.BtInsereClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmInsere, BtInsere);
end;

procedure TFmProdutosC.BtEditaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmEdita, BtEdita);
end;

procedure TFmProdutosC.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
  Grupo1.Height := MLAGeral.OrganizaAlturasGrades(PainelGrids.Height, 38, 73,
                   QrProdutosCIts, QrEmissF, QrEmissT);
  GroupBoxT.Width := (GroupBoxT.Width + GroupBoxF.Width) div 2;
end;

end.

