object FmProdutosCEdit: TFmProdutosCEdit
  Left = 455
  Top = 267
  Caption = 'MER-ENTRA-003 :: Itens de Entrada de Produtos'
  ClientHeight = 270
  ClientWidth = 500
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 48
    Width = 500
    Height = 29
    Align = alTop
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 29
      Height = 13
      Caption = 'Linha:'
    end
    object EdCodigo: TdmkEdit
      Left = 60
      Top = 4
      Width = 57
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clWhite
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 77
    Width = 500
    Height = 145
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 56
      Height = 13
      Caption = 'Mercadoria:'
    end
    object Label15: TLabel
      Left = 8
      Top = 52
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label10: TLabel
      Left = 352
      Top = 52
      Width = 36
      Height = 13
      Caption = '$ Total:'
    end
    object Label5: TLabel
      Left = 224
      Top = 100
      Width = 42
      Height = 13
      Caption = 'Valor kg:'
      Visible = False
    end
    object Label3: TLabel
      Left = 152
      Top = 100
      Width = 54
      Height = 13
      Caption = 'L'#237'quido kg:'
      Visible = False
    end
    object Label4: TLabel
      Left = 8
      Top = 100
      Width = 40
      Height = 13
      Caption = 'Tara kg:'
      Visible = False
    end
    object Label6: TLabel
      Left = 80
      Top = 100
      Width = 66
      Height = 13
      Caption = 'Impurezas kg:'
      Visible = False
    end
    object Label7: TLabel
      Left = 88
      Top = 52
      Width = 48
      Height = 13
      Caption = '$ Unit'#225'rio:'
    end
    object Label8: TLabel
      Left = 260
      Top = 52
      Width = 58
      Height = 13
      Caption = '$ Desconto:'
    end
    object Label9: TLabel
      Left = 168
      Top = 52
      Width = 54
      Height = 13
      Caption = '$ Sub-total:'
    end
    object SpeedButton1: TSpeedButton
      Left = 472
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object EdProduto: TdmkEditCB
      Left = 8
      Top = 20
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBProduto
      IgnoraDBLookupComboBox = False
    end
    object CBProduto: TdmkDBLookupComboBox
      Left = 76
      Top = 20
      Width = 393
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProdutos
      TabOrder = 1
      OnClick = CBProdutoClick
      dmkEditCB = EdProduto
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdBruto: TdmkEdit
      Left = 8
      Top = 68
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdBrutoExit
    end
    object EdTotal: TdmkEdit
      Left = 352
      Top = 68
      Width = 141
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdTotalExit
    end
    object EdValKg: TdmkEdit
      Left = 224
      Top = 116
      Width = 68
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 8
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdLiqui: TdmkEdit
      Left = 152
      Top = 116
      Width = 68
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 7
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdTara: TdmkEdit
      Left = 8
      Top = 116
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdTaraExit
    end
    object EdImpur: TdmkEdit
      Left = 80
      Top = 116
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdImpurExit
    end
    object EdPreco: TdmkEdit
      Left = 88
      Top = 68
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdPrecoExit
    end
    object EdDesco: TdmkEdit
      Left = 260
      Top = 68
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdDescoExit
    end
    object EdSubTo: TdmkEdit
      Left = 168
      Top = 68
      Width = 88
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdSubToExit
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 222
    Width = 500
    Height = 48
    Align = alBottom
    TabOrder = 2
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 407
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 500
    Height = 48
    Align = alTop
    Caption = 'Itens de Entrada de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object LaTipo: TLabel
      Left = 421
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 552
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 420
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitTop = 5
      ExplicitWidth = 704
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM produtos'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 188
    Top = 10
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Size = 1
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 216
    Top = 10
  end
end
