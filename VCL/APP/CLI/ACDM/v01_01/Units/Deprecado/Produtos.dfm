object FmProdutos: TFmProdutos
  Left = 363
  Top = 193
  Hint = '='
  Caption = 'MER-PRODU-001 :: Cadastro de Produtos'
  ClientHeight = 391
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 884
    Height = 273
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Enabled = False
    TabOrder = 1
    object Label1: TLabel
      Left = 15
      Top = 5
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label4: TLabel
      Left = 118
      Top = 113
      Width = 35
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'ICMS:'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 222
      Top = 113
      Width = 48
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'R.ICMS:'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 325
      Top = 113
      Width = 18
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'IPI:'
      FocusControl = DBEdit2
    end
    object Label8: TLabel
      Left = 15
      Top = 113
      Width = 93
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pre'#231'o de custo:'
      FocusControl = DBEdit7
    end
    object Label2: TLabel
      Left = 428
      Top = 113
      Width = 31
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'R.IPI:'
      FocusControl = DBEdit8
    end
    object Label6: TLabel
      Left = 532
      Top = 113
      Width = 34
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Frete:'
      FocusControl = DBEdit3
    end
    object Label9: TLabel
      Left = 118
      Top = 167
      Width = 37
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Custo:'
      FocusControl = DBEdit9
    end
    object Label10: TLabel
      Left = 222
      Top = 167
      Width = 83
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Estoque qtde:'
      FocusControl = DBEdit10
    end
    object Label11: TLabel
      Left = 325
      Top = 167
      Width = 86
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Estoque valor:'
      FocusControl = DBEdit11
    end
    object Label12: TLabel
      Left = 428
      Top = 167
      Width = 78
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Custo m'#233'dio:'
      FocusControl = DBEdit12
    end
    object Label13: TLabel
      Left = 532
      Top = 167
      Width = 99
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Pre'#231'o de venda:'
      FocusControl = DBEdit13
    end
    object Label14: TLabel
      Left = 15
      Top = 167
      Width = 36
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Juros:'
      FocusControl = DBEdit14
    end
    object Label15: TLabel
      Left = 15
      Top = 217
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grupo:'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Left = 121
      Top = 5
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome:'
      FocusControl = DBEdit1
    end
    object Label16: TLabel
      Left = 15
      Top = 59
      Width = 138
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome para impress'#227'o:'
      FocusControl = DBEdit1
    end
    object Label17: TLabel
      Left = 468
      Top = 217
      Width = 100
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Casas decimais:'
      FocusControl = DBEdit17
    end
    object DBEdit1: TDBEdit
      Left = 15
      Top = 25
      Width = 97
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      DataField = 'Codigo'
      DataSource = DsProdutos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit4: TDBEdit
      Left = 118
      Top = 133
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'ICMS'
      DataSource = DsProdutos
      TabOrder = 5
    end
    object DBEdit5: TDBEdit
      Left = 222
      Top = 133
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'RICMS'
      DataSource = DsProdutos
      TabOrder = 6
    end
    object DBEdit2: TDBEdit
      Left = 325
      Top = 133
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'IPI'
      DataSource = DsProdutos
      TabOrder = 7
    end
    object DBEdit7: TDBEdit
      Left = 15
      Top = 133
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'PrecoC'
      DataSource = DsProdutos
      TabOrder = 4
    end
    object DBEdit8: TDBEdit
      Left = 428
      Top = 133
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'RIPI'
      DataSource = DsProdutos
      TabOrder = 8
    end
    object DBEdit3: TDBEdit
      Left = 532
      Top = 133
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Frete'
      DataSource = DsProdutos
      TabOrder = 9
    end
    object DBEdit9: TDBEdit
      Left = 118
      Top = 187
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'CUSTO'
      DataSource = DsProdutos
      TabOrder = 11
    end
    object DBEdit10: TDBEdit
      Left = 222
      Top = 187
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'EstqQ'
      DataSource = DsProdutos
      TabOrder = 12
    end
    object DBEdit11: TDBEdit
      Left = 325
      Top = 187
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'EstqV'
      DataSource = DsProdutos
      TabOrder = 13
    end
    object DBEdit12: TDBEdit
      Left = 428
      Top = 187
      Width = 99
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'CUSTOMEDIO'
      DataSource = DsProdutos
      TabOrder = 14
    end
    object DBEdit13: TDBEdit
      Left = 532
      Top = 187
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'PrecoV'
      DataSource = DsProdutos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 15
    end
    object DBEdit14: TDBEdit
      Left = 15
      Top = 187
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Juros'
      DataSource = DsProdutos
      TabOrder = 10
    end
    object DBEdit15: TDBEdit
      Left = 15
      Top = 236
      Width = 449
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'NOMEGRUPO'
      DataSource = DsProdutos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
    end
    object DBCheckBox1: TDBCheckBox
      Left = 576
      Top = 241
      Width = 60
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Ativo.'
      DataField = 'Ativo'
      DataSource = DsProdutos
      TabOrder = 18
      ValueChecked = 'V'
      ValueUnchecked = 'F'
    end
    object DBEdit6: TDBEdit
      Left = 118
      Top = 25
      Width = 513
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Nome'
      DataSource = DsProdutos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBEdit16: TDBEdit
      Left = 15
      Top = 79
      Width = 538
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Nome'
      DataSource = DsProdutos
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object DBCheckBox2: TDBCheckBox
      Left = 556
      Top = 84
      Width = 75
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Imprime.'
      DataField = 'Imprime'
      DataSource = DsProdutos
      TabOrder = 3
      ValueChecked = 'V'
      ValueUnchecked = 'F'
    end
    object DBEdit17: TDBEdit
      Left = 468
      Top = 236
      Width = 98
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataField = 'Casas'
      DataSource = DsProdutos
      TabOrder = 17
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 332
    Width = 884
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 0
    object LaRegistro: TLabel
      Left = 213
      Top = 1
      Width = 31
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = '[N]: 0'
    end
    object PainelNavega: TPanel
      Left = 1
      Top = 1
      Width = 212
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 158
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 108
        Top = 5
        Width = 50
        Height = 49
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 59
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 10
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 305
      Top = 1
      Width = 577
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 458
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtSaidaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 231
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 118
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 5
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 345
        Top = 5
        Width = 110
        Height = 49
        Hint = 'Atualiza estoque da mercadoria atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Refresh'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtRefreshClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 884
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                            Cadastro de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 786
      Top = 1
      Width = 96
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 266
      Top = 1
      Width = 520
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PainelBotoes: TPanel
      Left = 1
      Top = 1
      Width = 265
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrProdutosAfterScroll
    OnCalcFields = QrProdutosCalcFields
    SQL.Strings = (
      'SELECT pr.*, gr.Nome NOMEGRUPO '
      'FROM produtos pr, ProdutosG gr'
      'WHERE gr.Codigo=pr.Grupo')
    Left = 525
    Top = 2
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DNSTORE001.produtos.Codigo'
      DisplayFormat = '000'
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
      Origin = 'DNSTORE001.produtos.ICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
      Origin = 'DNSTORE001.produtos.RICMS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'DNSTORE001.produtos.Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
      Origin = 'DNSTORE001.produtos.Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
      Origin = 'DNSTORE001.produtos.PrecoC'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
      Origin = 'DNSTORE001.produtos.PrecoV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosCUSTOMEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOMEDIO'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrProdutosCUSTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'DNSTORE001.produtos.Grupo'
    end
    object QrProdutosNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'DNSTORE001.produtosg.Nome'
      Size = 14
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
      Origin = 'DNRECOVERY.produtos.IPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
      Origin = 'DNRECOVERY.produtos.RIPI'
      DisplayFormat = '#,###,##0.00'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Required = True
      Size = 1
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNSTORE001.produtos.Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Required = True
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
      Required = True
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrProdutosEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
    object QrProdutosEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrProdutosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProdutosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 553
    Top = 2
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito'
      'FROM lanctos'
      'WHERE FatID=14'
      'AND FatNum=:P0'
      '')
    Left = 244
    Top = 5
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DNTEACH.lanctos.Credito'
    end
    object QrSomaDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DNTEACH.lanctos.Debito'
    end
  end
end
