unit CliPro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Buttons, Db, (*DBTables,*) UnMLAGeral, UnInternalConsts,
  UnMsgInt, ExtCtrls, UMySQLModule, UnInternalConsts2,  mySQLDbTables,
  UnitAcademy, UnMySQLCuringa, ComCtrls, dmkGeral, Variants, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkEditDateTimePicker, UnDmkEnums;

type
  TTipoCalculaOnEdit = (coeDescVal, coeDescPerc, coeTotal);
  TFmCliPro = class(TForm)
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrProdutosCodigo: TIntegerField;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosJuros: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosImprime: TWideStringField;
    QrProdutosControla: TWideStringField;
    QrProdutosCasas: TIntegerField;
    QrProdutosLk: TIntegerField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    QrProdutosCUSTOMEDIO: TFloatField;
    Panel1: TPanel;
    Label3: TLabel;
    EdAtendeteCod: TdmkEdit;
    Label5: TLabel;
    EdResponsavelCod: TdmkEdit;
    EdResponsavelNome: TdmkEdit;
    EdAtendenteNome: TdmkEdit;
    Label4: TLabel;
    EdRecibo: TdmkEdit;
    Label6: TLabel;
    TPDataCad: TdmkEditDateTimePicker;
    Panel2: TPanel;
    Label1: TLabel;
    EdProduto: TdmkEditCB;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label9: TLabel;
    EdSubTo: TdmkEdit;
    EdQtde: TdmkEdit;
    Label15: TLabel;
    Label11: TLabel;
    EdDescoPerc: TdmkEdit;
    EdDescoVal: TdmkEdit;
    Label8: TLabel;
    Label10: TLabel;
    EdTotal: TdmkEdit;
    CBProduto: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
    procedure EdDescoPercExit(Sender: TObject);
    procedure EdDescoValExit(Sender: TObject);
    procedure EdTotalExit(Sender: TObject);
    procedure QrProdutosAfterScroll(DataSet: TDataSet);
    procedure QrProdutosCalcFields(DataSet: TDataSet);
    procedure EdProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FLastcoe: TTipoCalculaOnEdit;
    procedure CalculaOnEdit(Tipo: TTipoCalculaOnEdit);
  public
    { Public declarations }
  end;

var
  FmCliPro: TFmCliPro;

implementation

uses UnMyObjects, ProdutosC, Module, Produtos;

{$R *.DFM}


procedure TFmCliPro.CalculaOnEdit(Tipo: TTipoCalculaOnEdit);
var
  Qtde, DescV, DescP, SubTo, Total: Double;
begin
  Qtde  := Geral.DMV(EdQtde.Text);
  DescV := Geral.DMV(EdDescoVal.Text);
  DescP := Geral.DMV(EdDescoPerc.Text);
  Total := Geral.DMV(EdTotal.Text);
  SubTo := Qtde * QrProdutosPrecoV.Value;
  case Tipo of
    coeDescVal:
    begin
      Total := SubTo - DescV;
      if SubTo > 0 then DescP := (DescV / SubTo) * 100 else DescP := 0;
    end;
    coeDescPerc:
    begin
      DescV := (Round(SubTo * DescP))/100;
      Total := SubTo - DescV;
    end;
    coeTotal:
    begin
      DescV := SubTo - Total;
      if SubTo > 0 then DescP := (DescV / SubTo) * 100 else DescP := 0;
    end;
  end;
  EdDescoPerc.Text := Geral.TFT(FloatToStr(DescP), 2, siNegativo);
  EdDescoVal.Text  := Geral.TFT(FloatToStr(DescV), 2, siNegativo);
  EdTotal.Text     := Geral.TFT(FloatToStr(Total), 2, siNegativo);
  EdSubTo.Text     := Geral.TFT(FloatToStr(SubTo), 2, siNegativo);
  FLastcoe := Tipo;
end;

procedure TFmCliPro.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCliPro.FormCreate(Sender: TObject);
begin
  FLastcoe := coeDescPerc;
  QrProdutos.Open;
  TPDataCad.Date := Date;
end;

procedure TFmCliPro.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  Produto, Cliente: Integer;
  Codigo, Controle: Int64;
  Qtde, Preco, ValorReal, Custo: Double;
  Data: String;
begin
  if CBProduto.KeyValue = NULL then
  begin
    Application.MessageBox(FIN_MSG_DEFPRODUTO, 'Erro', MB_OK+MB_ICONERROR);
    EdProduto.SetFocus;
    Exit;
  end else Produto := CBProduto.KeyValue;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  //
  Data      := FormatDateTime(VAR_FORMATDATE, Date);
  Cliente   := Geral.IMV(EdResponsavelCod.Text);
  Codigo    := Geral.IMV(EdRecibo.Text);
  Preco     := QrProdutosPrecoV.Value;
  Qtde      := Geral.DMV(EdQtde.Text);
  ValorReal := Geral.DMV(EdTotal.Text);
  Custo     := QrProdutosCUSTOMEDIO.Value * Qtde;
  if QrProdutosControla.Value = 'V' then
  begin
    if (Qtde > QrProdutosEstqQ.Value) then
    begin
      Application.MessageBox('Estoque escritural insuficiente!', 'Erro',
      MB_OK+MB_ICONERROR);
      EdQtde.SetFocus;
      Screen.Cursor := Cursor;
      Exit;
    end;
    if (Custo > QrProdutosEstqV.Value) then
    begin
      Application.MessageBox('Estoque financeiro insuficiente!', 'Erro',
      MB_OK+MB_ICONERROR);
      EdQtde.SetFocus;
      Screen.Cursor := Cursor;
      Exit;
    end;
    if (Custo < 0) then
    begin
      Application.MessageBox('Custo negativo!', 'Erro',
      MB_OK+MB_ICONERROR);
      EdQtde.SetFocus;
      Screen.Cursor := Cursor;
      Exit;
    end;
  end;
  if ValorReal<= 0 then
  begin
    Application.MessageBox('Defina o valor do item.', 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := Cursor;
    Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'VendasPro', 'VendasPro', 'Controle');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO VendasPro SET ');
  Dmod.QrUpd.SQL.Add('  Produto   =:P00');
  Dmod.QrUpd.SQL.Add(', Preco     =:P01');
  Dmod.QrUpd.SQL.Add(', Qtde      =:P02');
  Dmod.QrUpd.SQL.Add(', ValorReal =:P03');
  Dmod.QrUpd.SQL.Add(', Custo     =:P04');
  Dmod.QrUpd.SQL.Add(', DataCad   =:P05');
  Dmod.QrUpd.SQL.Add(', UserCad   =:P06');
  Dmod.QrUpd.SQL.Add(', Cliente   =:P07');
  Dmod.QrUpd.SQL.Add(', Controle  =:P08');
  Dmod.QrUpd.SQL.Add(', Codigo    =:P09');
  //
  Dmod.QrUpd.Params[00].AsInteger := Produto;
  Dmod.QrUpd.Params[01].AsFloat   := Preco;
  Dmod.QrUpd.Params[02].AsFloat   := Qtde;
  Dmod.QrUpd.Params[03].AsFloat   := ValorReal;
  Dmod.QrUpd.Params[04].AsFloat   := Custo;
  Dmod.QrUpd.Params[05].AsString  := Data;
  Dmod.QrUpd.Params[06].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[07].AsInteger := Cliente;
  //
  Dmod.QrUpd.Params[08].AsFloat   := Controle;
  Dmod.QrUpd.Params[09].AsFloat   := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  if Cliente = -2 then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Lanctos', 'Lanctos', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO Lanctos SET ');
    Dmod.QrUpd.SQL.Add('Data=:P0, Tipo=:P1, Carteira=:P2, Controle=:P3, ');
    Dmod.QrUpd.SQL.Add('Genero=:P4, Descricao=:P5, Credito=:P6, ');
    Dmod.QrUpd.SQL.Add('Compensado=:P7, Sit=:P8, Vencimento=:P9, ');
    Dmod.QrUpd.SQL.Add('FatID=:P10, FatNum=:P11, FatParcela=:P12, ');
    Dmod.QrUpd.SQL.Add('Cliente=:P13, UserCad=:P14, DataCad=:P15');
    Dmod.QrUpd.SQL.Add('');
    //
    Dmod.QrUpd.Params[00].AsString  := Data;
    Dmod.QrUpd.Params[01].AsInteger := 0;
    Dmod.QrUpd.Params[02].AsInteger := 1;
    Dmod.QrUpd.Params[03].AsInteger := Controle;
    Dmod.QrUpd.Params[04].AsInteger := -204;
    Dmod.QrUpd.Params[05].AsString  := 'V/M';
    Dmod.QrUpd.Params[06].AsFloat   := ValorReal;
    Dmod.QrUpd.Params[07].AsString  := Data;
    Dmod.QrUpd.Params[08].AsInteger := 2;
    Dmod.QrUpd.Params[09].AsString  := Data;
    Dmod.QrUpd.Params[10].AsInteger := 2105;
    Dmod.QrUpd.Params[11].AsInteger := Codigo;
    Dmod.QrUpd.Params[12].AsInteger := 1;
    Dmod.QrUpd.Params[13].AsInteger := Cliente;
    Dmod.QrUpd.Params[14].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[15].AsString  := Data;
    Dmod.QrUpd.ExecSQL;
  end;
  UnAcademy.AtualizaEstoqueMercadoria(Produto, aeMsg, CO_VAZIO);
  Screen.Cursor := Cursor;
  if CkContinuar.Checked then
  begin
    QrProdutos.Close;
    QrProdutos.Open;
    EdProduto.Text     := '';
    CBProduto.KeyValue := NULL;
    EdQtde.Text        := '0';
    EdDescoVal.Text    := '';
    EdTotal.Text       := '';
    EdProduto.SetFocus;
  end else Close;
end;

procedure TFmCliPro.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdQtde.SetFocus;
  EdProduto.SetFocus;
end;

procedure TFmCliPro.EdQtdeExit(Sender: TObject);
begin
  EdQtde.Text := Geral.TFT(EdQtde.Text, QrProdutosCasas.Value, siPositivo);
  CalculaOnEdit(FLastcoe);
end;

procedure TFmCliPro.EdDescoPercExit(Sender: TObject);
begin
  EdDescoPerc.Text := Geral.TFT(EdDescoPerc.Text, 2, siPositivo);
  CalculaOnEdit(coeDescPerc);
end;

procedure TFmCliPro.EdDescoValExit(Sender: TObject);
begin
  EdDescoVal.Text := Geral.TFT(EdDescoVal.Text, 2, siPositivo);
  CalculaOnEdit(coeDescVal);
end;

procedure TFmCliPro.EdTotalExit(Sender: TObject);
begin
  EdTotal.Text := Geral.TFT(EdTotal.Text, 2, siPositivo);
  CalculaOnEdit(coeTotal);
end;

procedure TFmCliPro.QrProdutosAfterScroll(DataSet: TDataSet);
begin
  EdQtde.Text := Geral.TFT(EdQtde.Text, QrProdutosCasas.Value, siPositivo);
  CalculaOnEdit(FLastcoe);
end;

procedure TFmCliPro.QrProdutosCalcFields(DataSet: TDataSet);
begin
  if QrProdutosEstqQ.Value > 0 then
     QrProdutosCUSTOMEDIO.Value :=
     QrProdutosEstqV.Value /
     QrProdutosEstqQ.Value else
     QrProdutosCUSTOMEDIO.Value := 0;
end;

procedure TFmCliPro.EdProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then EdProduto.Text := IntToStr(
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Produtos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCliPro.CBProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F4 then
  EdProduto.Text := IntToStr(
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Produtos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCliPro.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
end;

end.
