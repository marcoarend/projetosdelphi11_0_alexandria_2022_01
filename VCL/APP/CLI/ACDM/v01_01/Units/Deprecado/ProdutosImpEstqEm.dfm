object FmProdutosImpEstqEm: TFmProdutosImpEstqEm
  Left = 289
  Top = 278
  Caption = 'Estoque Em...'
  ClientHeight = 330
  ClientWidth = 443
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object QrVendas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vi.Produto, SUM(vi.Qtde) Pecas,'
      'SUM(vi.ValorReal) Valor'
      'FROM vendaspro vi, Vendas ve, Produtos me'
      'WHERE me.Codigo=vi.Produto'
      'AND vi.Codigo=ve.Codigo'
      'AND ve.DataCad<=20031231'
      'AND vi.Produto<>0'
      'GROUP BY vi.Produto')
    Left = 252
    Top = 88
    object QrVendasProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrVendasPecas: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Pecas'
    end
    object QrVendasValor: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Valor'
    end
  end
  object QrEntradas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vi.Produto, SUM(vi.Bruto) Pecas,'
      'SUM(vi.Custo) Valor'
      'FROM produtoscits vi, ProdutosC ve, Produtos me'
      'WHERE me.Codigo=vi.Produto'
      'AND vi.Codigo=ve.Codigo'
      'AND ve.DataC<=20031231'
      'AND vi.Produto<>""'
      'GROUP BY vi.Produto')
    Left = 280
    Top = 88
    object QrEntradasProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrEntradasPecas: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Pecas'
    end
    object QrEntradasValor: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Valor'
    end
  end
  object QrDevolucaos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vi.Mercadoria, SUM(vi.Pecas) Pecas,'
      'SUM(vi.VTotal) Valor'
      'FROM entradaits vi, Entrada ve, Artigos me'
      'WHERE me.Codigo=vi.Mercadoria'
      'AND vi.Codigo=ve.Codigo'
      'AND ve.Data<=20031231'
      'AND vi.Mercadoria<>""'
      'GROUP BY vi.Mercadoria')
    Left = 308
    Top = 88
    object QrDevolucaosMercadoria: TWideStringField
      FieldName = 'Mercadoria'
      Required = True
      Size = 8
    end
    object QrDevolucaosPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrDevolucaosValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrRecompras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vi.Mercadoria, SUM(vi.Pecas) Pecas,'
      'SUM(vi.VTotal) Valor'
      'FROM entradaits vi, Entrada ve, Artigos me'
      'WHERE me.Codigo=vi.Mercadoria'
      'AND vi.Codigo=ve.Codigo'
      'AND ve.Data<=20031231'
      'AND vi.Mercadoria<>""'
      'GROUP BY vi.Mercadoria')
    Left = 336
    Top = 88
    object QrRecomprasMercadoria: TWideStringField
      FieldName = 'Mercadoria'
      Required = True
      Size = 8
    end
    object QrRecomprasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrRecomprasValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrMerc5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT me.Codigo Mercadoria, me.Nome Nome,'
      'me.EIniQ INIPECAS, me.EIniV INIVALOR'
      'FROM produtos me')
    Left = 224
    Top = 88
    object QrMerc5Mercadoria: TIntegerField
      FieldName = 'Mercadoria'
      Required = True
    end
    object QrMerc5Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrMerc5INIPECAS: TFloatField
      FieldName = 'INIPECAS'
      Required = True
    end
    object QrMerc5INIVALOR: TFloatField
      FieldName = 'INIVALOR'
      Required = True
    end
  end
  object QrMercEstq: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrMercEstqCalcFields
    SQL.Strings = (
      'SELECT SUM(EQuant) EQuant, SUM(EValor) EValor,'
      'SUM(SQuant) SQuant, SUM(SValor) SValor,'
      'Mercadoria, Fornecedor, Gestor,'
      'NomeMercadoria, NomeFornecedor,'
      'NomeGestor, Tipo'
      'FROM mercestq'
      'GROUP BY NomeMercadoria')
    Left = 340
    Top = 44
    object QrMercEstqTOTALPESO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALPESO'
      Calculated = True
    end
    object QrMercEstqTOTALVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALVALOR'
      Calculated = True
    end
    object QrMercEstqEQuant: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EQuant'
    end
    object QrMercEstqEValor: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EValor'
    end
    object QrMercEstqSQuant: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SQuant'
    end
    object QrMercEstqSValor: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SValor'
    end
    object QrMercEstqMercadoria: TIntegerField
      FieldName = 'Mercadoria'
    end
    object QrMercEstqFornecedor: TFloatField
      FieldName = 'Fornecedor'
    end
    object QrMercEstqGestor: TFloatField
      FieldName = 'Gestor'
    end
    object QrMercEstqNomeMercadoria: TWideStringField
      FieldName = 'NomeMercadoria'
      Required = True
      Size = 100
    end
    object QrMercEstqNomeFornecedor: TWideStringField
      FieldName = 'NomeFornecedor'
      Required = True
      Size = 100
    end
    object QrMercEstqNomeGestor: TWideStringField
      FieldName = 'NomeGestor'
      Required = True
      Size = 100
    end
    object QrMercEstqTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 30
    end
  end
  object frxMercEstq: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38008.453407974500000000
    ReportOptions.LastChange = 39724.619339189800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GroupHeader1, Engine do'
      '  begin'
      
        '  if VARF_COMPACTAR(VARF_COMPACTAR) then visible := False else v' +
        'isible := true;'
      '  end'
      'end;'
      ''
      'procedure GroupFooter1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GroupFooter1, Engine do'
      '  begin'
      
        '  if VARF_COMPACTAR(VARF_COMPACTAR) then visible := False else v' +
        'isible := true;'
      '  end'
      'end;'
      ''
      'procedure Memo1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo1, Engine do'
      '  begin'
      '  if VARF_COMPACTAR(VARF_COMPACTAR) then'
      '     Memo := '#39'C'#243'digo e nome da mercadoria'#39
      '  else Memo := '#39'Tipo de Movimento'#39';'
      '  end'
      'end;'
      ''
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo7OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo7, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo15OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo15, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo20OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo20, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo21OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo21, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo18OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo18, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo23OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo23, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo24OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo24, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo25OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo25, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo27OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo27, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo22OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo22, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo26OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo26, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo28OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo28, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo29OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo29, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo30OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo30, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  /////'
      
        '  if VARF_COMPACTAR(VARF_COMPACTAR) then Visible := True else Vi' +
        'sible := False;'
      '  ////'
      '  end'
      'end;'
      ''
      'procedure Memo33OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo33, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  //////'
      
        '  if VARF_COMPACTAR(VARF_COMPACTAR) then Visible := True else Vi' +
        'sible := False;'
      '  end'
      'end;'
      ''
      'procedure Memo34OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo34, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then FrameTyp:=15 else FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxMercEstqGetValue
    Left = 284
    Top = 44
    Datasets = <
      item
      end
      item
        DataSet = frxDsMercEstq
        DataSetName = 'frxDsMercEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        Height = 80.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Top = 8.440940000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.000000000000000000
          Top = 4.440940000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.000000000000000000
          Top = 32.440940000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 445.984540000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 542.000000000000000000
          Top = 12.220160000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 630.000000000000000000
          Top = 12.220160000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -6.000000000000000000
          Top = 8.220160000000020000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 45.464440000000000000
        Top = 120.944960000000000000
        Width = 755.906000000000000000
        object Line3: TfrxLineView
          Left = -6.000000000000000000
          Top = 23.243970000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo1: TfrxMemoView
          Left = -2.000000000000000000
          Top = 27.464440000000000000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 242.000000000000000000
          Top = 27.464440000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Entrada Qtde')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 322.000000000000000000
          Top = 27.464440000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Entrada Valor')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 402.000000000000000000
          Top = 27.464440000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Sa'#195#173'da Qtde')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 482.000000000000000000
          Top = 27.464440000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Sa'#195#173'da Valor')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 562.000000000000000000
          Top = 27.464440000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Saldo Qtde')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 642.000000000000000000
          Top = 27.464440000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Saldo Valor')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 18.000000000000000000
        Top = 268.346630000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMercEstq
        DataSetName = 'frxDsMercEstq'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 242.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsMercEstq."EQuant"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 322.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsMercEstq."EValor"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 402.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo7OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsMercEstq."SQuant"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 482.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsMercEstq."SValor"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = -2.000000000000000000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo27OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMercEstq."Mercadoria"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 562.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo30OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[(<frxDsMercEstq."EQuant">-<frxDsMercEstq."SQuant">)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 642.000000000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo33OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[([frxDsMercEstq."EValor"]-[frxDsMercEstq."SValor">)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 34.000000000000000000
          Width = 208.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo34OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMercEstq."NomeMercadoria"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.000000000000000000
        Top = 389.291590000000000000
        Width = 755.906000000000000000
        object Memo14: TfrxMemoView
          Left = 150.000000000000000000
          Top = 8.692639999999980000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Total:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 322.000000000000000000
          Top = 8.692639999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo20OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."EValor">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 402.000000000000000000
          Top = 8.692639999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo21OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."SQuant">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 242.000000000000000000
          Top = 8.692639999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."EQuant">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 482.000000000000000000
          Top = 8.692639999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo25OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."SValor">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 562.000000000000000000
          Top = 8.692639999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo28OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            
              '[([SUM(<frxDsMercEstq."EQuant"])]-[SUM(<frxDsMercEstq."SQuant"])' +
              '])]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 642.000000000000000000
          Top = 8.692639999999980000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo29OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            
              '[([SUM(<frxDsMercEstq."EValor"])]-[SUM(<frxDsMercEstq."SValor"])' +
              '])]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 20.000000000000000000
        Top = 226.771800000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsMercEstq."Mercadoria"'
        object Memo2: TfrxMemoView
          Left = -2.000000000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Mercadoria:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 66.000000000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsMercEstq."Mercadoria"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 170.000000000000000000
          Width = 484.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsMercEstq."NomeMercadoria"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 20.000000000000000000
        Top = 309.921460000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo12: TfrxMemoView
          Left = 245.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."EQuant">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 325.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."EValor">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 405.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."SQuant">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 485.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsMercEstq."SValor">)]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 565.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo22OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            
              '[([SUM(<frxDsMercEstq."EQuant"])]-[SUM(<frxDsMercEstq."SQuant"])' +
              '])]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 645.779530000000000000
          Width = 80.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo26OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            
              '[(SUM(<frxDsMercEstq."EValor"]) - SUM(<frxDsMercEstq."SValor">))' +
              ']')
          ParentFont = False
        end
      end
      object Memo19: TfrxMemoView
        Left = -6.000000000000000000
        Top = 120.188930000000000000
        Width = 732.000000000000000000
        Height = 18.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8 = (
          '[NOMEREL_001]')
        ParentFont = False
      end
    end
  end
  object frxDsMercEstq: TfrxDBDataset
    UserName = 'frxDsMercEstq'
    CloseDataSource = False
    DataSet = QrMercEstq
    BCDToCurrency = False
    Left = 312
    Top = 44
  end
end
