unit ProdutosImprime;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Db, (*DBTables,*) DBCtrls, UnMLAGeral,
  UnInternalConsts, ComCtrls, Mask, mySQLDbTables, UCreate, Grids, DBGrids,
  UnitAcademy, UnGOTOy, frxClass, frxDBSet, dmkGeral, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker;

type
  TFmProdutosImprime = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    DsProdutosG: TDataSource;
    DsArtigos: TDataSource;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrArtigos: TmySQLQuery;
    QrProdutosG: TmySQLQuery;
    QrProdutosGCodigo: TIntegerField;
    QrProdutosGNome: TWideStringField;
    QrBalAnt: TmySQLQuery;
    QrBalNow: TmySQLQuery;
    QrAntD: TmySQLQuery;
    QrAntE: TmySQLQuery;
    QrAntR: TmySQLQuery;
    QrAntV: TmySQLQuery;
    QrMerc: TmySQLQuery;
    QrMax: TmySQLQuery;
    QrEntradas: TmySQLQuery;
    QrVendas: TmySQLQuery;
    QrDevolucoes: TmySQLQuery;
    QrRecompras: TmySQLQuery;
    QrBalancos: TmySQLQuery;
    QrProdutos: TmySQLQuery;
    QrBalanco: TmySQLQuery;
    QrEstq: TmySQLQuery;
    QrVendido1: TmySQLQuery;
    QrProdutosCUSTOMEDIO: TFloatField;
    QrBalancoQSAL: TFloatField;
    QrBalancoQDIF: TFloatField;
    QrBalancoVDIF: TFloatField;
    QrEstqProduto: TWideStringField;
    QrEstqDescricao: TWideStringField;
    QrEstqNomeForecedor: TWideStringField;
    QrEstqFornecedor: TIntegerField;
    QrEstqEstqQ: TFloatField;
    QrEstqEstqV: TFloatField;
    QrEstqData: TDateField;
    QrEstqTipo: TIntegerField;
    QrEstqAcumQ: TFloatField;
    QrEstqAcumV: TFloatField;
    QrEstqNOMETIPO: TWideStringField;
    QrEstqACUM2Q: TFloatField;
    QrEstqACUM2V: TFloatField;
    QrEstqDIF2Q: TFloatField;
    QrEstqDIF2V: TFloatField;
    QrEstqVALQ2: TFloatField;
    QrEstqVALV2: TFloatField;
    QrVendido1MARGEM: TFloatField;
    PainelDados: TPanel;
    Label11: TLabel;
    EdPesqNome: TdmkEdit;
    Panel1: TPanel;
    RGTipo: TRadioGroup;
    RGOrdem: TRadioGroup;
    RGGRade: TRadioGroup;
    CkPositivo: TCheckBox;
    LaMercadoria: TLabel;
    EdProduto: TdmkEditCB;
    LaDataI: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    LaDataF: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    DBGPesq: TDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    Progress: TProgressBar;
    CkCompactar: TCheckBox;
    CkCorLetras: TCheckBox;
    CBCorFundo: TCheckBox;
    QrArtigosCodigo: TIntegerField;
    QrArtigosGrupo: TIntegerField;
    QrArtigosNome: TWideStringField;
    QrArtigosNome2: TWideStringField;
    QrArtigosPrecoC: TFloatField;
    QrArtigosPrecoV: TFloatField;
    QrArtigosICMS: TFloatField;
    QrArtigosRICMS: TFloatField;
    QrArtigosIPI: TFloatField;
    QrArtigosRIPI: TFloatField;
    QrArtigosFrete: TFloatField;
    QrArtigosJuros: TFloatField;
    QrArtigosEstqV: TFloatField;
    QrArtigosEstqQ: TFloatField;
    QrArtigosEIniV: TFloatField;
    QrArtigosEIniQ: TFloatField;
    QrArtigosImprime: TWideStringField;
    QrArtigosControla: TWideStringField;
    QrArtigosCasas: TIntegerField;
    QrArtigosLk: TIntegerField;
    QrArtigosDataCad: TDateField;
    QrArtigosDataAlt: TDateField;
    QrArtigosUserCad: TIntegerField;
    QrArtigosUserAlt: TIntegerField;
    CBProduto: TdmkDBLookupComboBox;
    QrPesqCodigo: TIntegerField;
    QrPesqGrupo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqNome2: TWideStringField;
    QrPesqPrecoC: TFloatField;
    QrPesqPrecoV: TFloatField;
    QrPesqICMS: TFloatField;
    QrPesqRICMS: TFloatField;
    QrPesqIPI: TFloatField;
    QrPesqRIPI: TFloatField;
    QrPesqFrete: TFloatField;
    QrPesqJuros: TFloatField;
    QrPesqEstqV: TFloatField;
    QrPesqEstqQ: TFloatField;
    QrPesqEIniV: TFloatField;
    QrPesqEIniQ: TFloatField;
    QrPesqImprime: TWideStringField;
    QrPesqControla: TWideStringField;
    QrPesqCasas: TIntegerField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrBalAntEstqQ: TFloatField;
    QrBalAntEstqV: TFloatField;
    QrBalAntPeriodo: TIntegerField;
    QrBalAntProduto: TIntegerField;
    QrBalAntDESCRICAO: TWideStringField;
    QrBalNowEstqQ: TFloatField;
    QrBalNowEstqV: TFloatField;
    QrBalNowPeriodo: TIntegerField;
    QrBalNowProduto: TIntegerField;
    QrBalNowDESCRICAO: TWideStringField;
    QrEntradasCodigo: TIntegerField;
    QrEntradasControle: TLargeintField;
    QrEntradasConta: TAutoIncField;
    QrEntradasProduto: TIntegerField;
    QrEntradasPreco: TFloatField;
    QrEntradasBruto: TFloatField;
    QrEntradasTara: TFloatField;
    QrEntradasImpur: TFloatField;
    QrEntradasDesco: TFloatField;
    QrEntradasValor: TFloatField;
    QrEntradasCusto: TFloatField;
    QrEntradasDataC: TDateField;
    QrBalancosPeriodo: TIntegerField;
    QrBalancosProduto: TIntegerField;
    QrBalancosConta: TIntegerField;
    QrBalancosEstqQ: TFloatField;
    QrBalancosEstqV: TFloatField;
    QrBalancosAnteQ: TFloatField;
    QrBalancosAnteV: TFloatField;
    QrDevolucoesCodigo: TIntegerField;
    QrDevolucoesControle: TAutoIncField;
    QrDevolucoesConta: TLargeintField;
    QrDevolucoesUnidade: TIntegerField;
    QrDevolucoesPecas: TFloatField;
    QrDevolucoesPrecoUnit: TFloatField;
    QrDevolucoesDesconto: TFloatField;
    QrDevolucoesVIPI: TFloatField;
    QrDevolucoesVICMS: TFloatField;
    QrDevolucoesIPI: TFloatField;
    QrDevolucoesICMS: TFloatField;
    QrDevolucoesFrete: TFloatField;
    QrDevolucoesCTotal: TFloatField;
    QrDevolucoesVTotal: TFloatField;
    QrDevolucoesProduto: TIntegerField;
    QrDevolucoesData: TDateField;
    QrRecomprasCodigo: TIntegerField;
    QrRecomprasControle: TAutoIncField;
    QrRecomprasConta: TLargeintField;
    QrRecomprasUnidade: TIntegerField;
    QrRecomprasPecas: TFloatField;
    QrRecomprasPrecoUnit: TFloatField;
    QrRecomprasDesconto: TFloatField;
    QrRecomprasVIPI: TFloatField;
    QrRecomprasVICMS: TFloatField;
    QrRecomprasIPI: TFloatField;
    QrRecomprasICMS: TFloatField;
    QrRecomprasFrete: TFloatField;
    QrRecomprasCTotal: TFloatField;
    QrRecomprasVTotal: TFloatField;
    QrRecomprasProduto: TIntegerField;
    QrRecomprasData: TDateField;
    QrAntDEstqQ: TFloatField;
    QrAntDEstqV: TFloatField;
    QrAntDDESCRICAO: TWideStringField;
    QrAntDProduto: TIntegerField;
    QrAntEEstqQ: TFloatField;
    QrAntEEstqV: TFloatField;
    QrAntEDESCRICAO: TWideStringField;
    QrAntEPRODUTO: TIntegerField;
    QrAntREstqQ: TFloatField;
    QrAntREstqV: TFloatField;
    QrAntRDESCRICAO: TWideStringField;
    QrAntRProduto: TIntegerField;
    QrAntVEstqQ: TFloatField;
    QrAntVEstqV: TFloatField;
    QrAntVDESCRICAO: TWideStringField;
    QrAntVProduto: TIntegerField;
    QrMaxPeriodo: TIntegerField;
    QrMercProduto: TIntegerField;
    QrProdutosCodigo: TIntegerField;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosJuros: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosImprime: TWideStringField;
    QrProdutosControla: TWideStringField;
    QrProdutosCasas: TIntegerField;
    QrProdutosLk: TIntegerField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    QrBalancoProduto: TIntegerField;
    QrBalancoDescricao: TWideStringField;
    QrBalancoNomeForecedor: TWideStringField;
    QrBalancoFornecedor: TIntegerField;
    QrBalancoEstqQAnt: TFloatField;
    QrBalancoEstqVAnt: TFloatField;
    QrBalancoEstqQEnt: TFloatField;
    QrBalancoEstqVEnt: TFloatField;
    QrBalancoEstqQVen: TFloatField;
    QrBalancoEstqVVen: TFloatField;
    QrBalancoEstqQDev: TFloatField;
    QrBalancoEstqVDev: TFloatField;
    QrBalancoEstqQRec: TFloatField;
    QrBalancoEstqVRec: TFloatField;
    QrBalancoEstqQNow: TFloatField;
    QrBalancoEstqVNow: TFloatField;
    QrVendido1DESCRICAO: TWideStringField;
    QrVendido1DataCad: TDateField;
    QrVendido1Codigo: TLargeintField;
    QrVendido1Controle: TLargeintField;
    QrVendido1Produto: TIntegerField;
    QrVendido1Qtde: TFloatField;
    QrVendido1Preco: TFloatField;
    QrVendido1ValorReal: TFloatField;
    QrVendido1Custo: TFloatField;
    QrVendido1CUSTOUNIT: TFloatField;
    QrVendido1Desconto: TFloatField;
    QrVendasCodigo: TLargeintField;
    QrVendasControle: TLargeintField;
    QrVendasProduto: TIntegerField;
    QrVendasCliente: TIntegerField;
    QrVendasQtde: TFloatField;
    QrVendasPreco: TFloatField;
    QrVendasValorReal: TFloatField;
    QrVendasCusto: TFloatField;
    QrVendasLk: TIntegerField;
    QrVendasDataCad: TDateField;
    QrVendasDataAlt: TDateField;
    QrVendasUserCad: TIntegerField;
    QrVendasUserAlt: TIntegerField;
    QrVendido2: TmySQLQuery;
    QrVendido2DESCRICAO: TWideStringField;
    QrVendido2Produto: TIntegerField;
    QrVendido2Qtde: TFloatField;
    QrVendido2ValorReal: TFloatField;
    QrVendido2BRUTO: TFloatField;
    QrVendido2DESCONTO: TFloatField;
    QrVendido2CUSTOUNIT: TFloatField;
    QrVendido2MARGEM: TFloatField;
    QrVendido2Custo: TFloatField;
    QrVendido2PRECOMEDIO: TFloatField;
    frxProdutos: TfrxReport;
    frxDsProdutos: TfrxDBDataset;
    frxBalanco: TfrxReport;
    frxDsBalanco: TfrxDBDataset;
    frxDsEstq: TfrxDBDataset;
    frxEstq: TfrxReport;
    frxVendido1: TfrxReport;
    frxDsVendido1: TfrxDBDataset;
    frxDsVendido2: TfrxDBDataset;
    frxVendido2: TfrxReport;
    SpeedButton1: TSpeedButton;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure frProdutosUserFunction(const Name: String; p1, p2,
      p3: Variant; var Val: Variant);
    procedure FormActivate(Sender: TObject);
    procedure QrBalancoCalcFields(DataSet: TDataSet);
    procedure RGTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEstqCalcFields(DataSet: TDataSet);
    procedure QrVendido1CalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure QrProdutosCalcFields(DataSet: TDataSet);
    procedure DBGPesqDblClick(Sender: TObject);
    procedure CBProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrVendido2CalcFields(DataSet: TDataSet);
    procedure frxProdutosGetValue(const VarName: String;
      var Value: Variant);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ImprimeBalanco;
    procedure frxGetValue(const VarName: String; var Value: Variant);
  end;

var
  FmProdutosImprime: TFmProdutosImprime;
  MercI_DataFim, MercI_DataIni: String;
  ProdutosI_Cor: Integer;

implementation

uses UnMyObjects, Module, ProdutosImpEstqEm, UnDmkEnums;

{$R *.DFM}

var
  DataI_Date, DataF_Date: TDateTime;
  //pi_Filtro: String;

procedure TFmProdutosImprime.BtOKClick(Sender: TObject);
var
  Mercadoria, OrdNum, PeriodoI, PeriodoF: Integer;
  DataI, DataF, Ordem, DataPeriodo: String;
  SomaQ, SomaV, TiraQ, TiraV, AcumQ, AcumV: Double;
  Insere: Boolean;
begin
  Ordem := CO_VAZIO;
  //pi_Filtro := '';
  OrdNum := RGOrdem.ItemIndex;
  DataI := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
  DataF := FormatDateTime(VAR_FORMATDATE, TPDataF.Date);
  DataI_Date := StrToDate(MLAGeral.PrimeiroDiaAposPeriodo(QrMaxPeriodo.Value, dtSystem2));
  DataF_Date := StrToDate(MLAGeral.UltimoDiaDoPeriodo(UnAcademy.VerificaBalanco, dtSystem2));
  if RGTipo.Itemindex = 0 then
  begin
    case OrdNum of
       0: Ordem := 'DESCRICAO';
       1: Ordem := 'ar.Codigo';
      else Ordem := '*ERRO NA ORDEM*';
    end;
    QrProdutos.Close;
    QrProdutos.SQL.Clear;
    QrProdutos.SQL.Add('SELECT ar.*, ar.Nome DESCRICAO');
    QrProdutos.SQL.Add('FROM produtos ar');
    if CkPositivo.Checked then
      QrProdutos.SQL.Add('WHERE ((ar.EstqQ>0) OR (ar.EstqV>0))');
    if Ordem <> CO_VAZIO then
      QrProdutos.SQL.Add('ORDER BY '+Ordem);
    QrProdutos.Open;
    MyObjects.frxMostra(frxProdutos, 'Produtos');
    QrProdutos.Close;
  end else if RGTipo.Itemindex = 1 then
  begin
    ImprimeBalanco;
  end else if RGTipo.Itemindex = 2 then
  begin
    Mercadoria := Geral.IMV(EdProduto.Text);
    if Mercadoria = 0 then
    begin
      Application.MessageBox('Defina a mercadoria!.', 'Erro', MB_OK+MB_ICONERROR);
      EdProduto.SetFocus;
      Exit;
    end else Mercadoria := QrArtigosCodigo.Value;
    //
    UCriar.RecriaTabelaLocal('Estq', 1);
    //
    PeriodoI := Geral.Periodo2000(TPDataI.Date);
    PeriodoF := Geral.Periodo2000(TPDataF.Date);
    Dmod.QrInsL.SQL.Clear;
    Dmod.QrInsL.SQL.Add('INSERT INTO estq SET ');
    Dmod.QrInsL.SQL.Add('Data=:P0, Tipo=:P1, EstqQ=:P2, EstqV=:P3');
    Dmod.QrInsL.SQL.Add('');
    /////
    QrEntradas.Close;
    QrEntradas.Params[0].AsInteger := Mercadoria;
    QrEntradas.Params[1].AsString  := DataI;
    QrEntradas.Params[2].AsString  := DataF;
    QrEntradas.Open;
    while not QrEntradas.Eof do
    begin
      Dmod.QrInsL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, QrEntradasDataC.Value);
      Dmod.QrInsL.Params[1].AsInteger := 3;
      Dmod.QrInsL.Params[2].AsFloat   := QrEntradasBruto.Value;
      Dmod.QrInsL.Params[3].AsFloat   := QrEntradasCusto.Value;
      Dmod.QrInsL.ExecSQL;
      QrEntradas.Next;
    end;
    QrEntradas.Close;
    /////
    QrVendas.Close;
    QrVendas.Params[0].AsInteger := Mercadoria;
    QrVendas.Params[1].AsString  := DataI;
    QrVendas.Params[2].AsString  := DataF;
    QrVendas.Open;
    while not QrVendas.Eof do
    begin
      Dmod.QrInsL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, QrVendasDataCad.Value);
      Dmod.QrInsL.Params[1].AsInteger := 4;
      Dmod.QrInsL.Params[2].AsFloat   := -QrVendasQtde.Value;
      Dmod.QrInsL.Params[3].AsFloat   := -QrVendasValorReal.Value;
      Dmod.QrInsL.ExecSQL;
      QrVendas.Next;
    end;
    QrVendas.Close;
    /////
    QrDevolucoes.Close;
    QrDevolucoes.Params[0].AsInteger := Mercadoria;
    QrDevolucoes.Params[1].AsString  := DataI;
    QrDevolucoes.Params[2].AsString  := DataF;
    QrDevolucoes.Open;
    while not QrDevolucoes.Eof do
    begin
      Dmod.QrInsL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, QrDevolucoesData.Value);
      Dmod.QrInsL.Params[1].AsInteger := 5;
      Dmod.QrInsL.Params[2].AsFloat   := -QrDevolucoesPecas.Value;
      Dmod.QrInsL.Params[3].AsFloat   := -QrDevolucoesVTotal.Value;
      Dmod.QrInsL.ExecSQL;
      QrDevolucoes.Next;
    end;
    QrDevolucoes.Close;
    /////
    QrRecompras.Close;
    QrRecompras.Params[0].AsInteger := Mercadoria;
    QrRecompras.Params[1].AsString  := DataI;
    QrRecompras.Params[2].AsString  := DataF;
    QrRecompras.Open;
    while not QrRecompras.Eof do
    begin
      Dmod.QrInsL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, QrRecomprasData.Value);
      Dmod.QrInsL.Params[1].AsInteger := 6;
      Dmod.QrInsL.Params[2].AsFloat   := QrRecomprasPecas.Value;
      Dmod.QrInsL.Params[3].AsFloat   := QrRecomprasVTotal.Value;
      Dmod.QrInsL.ExecSQL;
      QrRecompras.Next;
    end;
    QrRecompras.Close;
    /////
    QrBalancos.Close;
    QrBalancos.Params[0].AsInteger  := Mercadoria;
    QrBalancos.Params[1].AsInteger  := PeriodoI;
    QrBalancos.Params[2].AsInteger  := PeriodoF;
    QrBalancos.Open;
    while not QrBalancos.Eof do
    begin
      Dmod.QrInsL.Params[0].AsString  := MLAGeral.UltimoDiaDoPeriodo(QrBalancosPeriodo.Value, dtSystem);
      Dmod.QrInsL.Params[1].AsInteger := 8;
      Dmod.QrInsL.Params[2].AsFloat   := QrBalancosEstqQ.Value;
      Dmod.QrInsL.Params[3].AsFloat   := QrBalancosEstqV.Value;
      Dmod.QrInsL.ExecSQL;
      QrBalancos.Next;
    end;
    QrBalancos.Close;
    QrEstq.Close;
    QrEstq.SQL.Clear;
    QrEstq.SQL.Add('SELECT * FROM estq');
    QrEstq.SQL.Add('ORDER BY Data, Tipo');
    QrEstq.Open;
    SomaQ := 0;
    SomaV := 0;
    TiraQ := 0;
    TiraV := 0;
    AcumQ := 0;
    AcumV := 0;
    //
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM estq');
    Dmod.QrUpdL.ExecSQL;
    //
    Dmod.QrInsL.SQL.Clear;
    Dmod.QrInsL.SQL.Add('INSERT INTO estq SET ');
    Dmod.QrInsL.SQL.Add('Data=:P0, Tipo=:P1, EstqQ=:P2, EstqV=:P3,');
    Dmod.QrInsL.SQL.Add('AcumQ=:P4, AcumV=:P5');
    /////
    Insere := False;
    while not QrEstq.Eof do
    begin
      if QrEstqTipo.Value < 7 then
      begin
        if Insere then
        begin
          SomaQ := TiraQ - SomaQ;
          SomaV := TiraV - SomaV;
(*          Dmod.QrInsL.Params[0].AsString  := DataPeriodo;
          Dmod.QrInsL.Params[1].AsInteger := 7;
          Dmod.QrInsL.Params[2].AsFloat   := SomaQ;
          Dmod.QrInsL.Params[3].AsFloat   := SomaV;
          Dmod.QrInsL.Params[4].AsFloat   := 0;
          Dmod.QrInsL.Params[5].AsFloat   := 0;
          Dmod.QrInsL.ExecSQL;*)
          Dmod.QrInsL.Params[0].AsString  := DataPeriodo;
          Dmod.QrInsL.Params[1].AsInteger := 9;
          Dmod.QrInsL.Params[2].AsFloat   := SomaQ;
          Dmod.QrInsL.Params[3].AsFloat   := SomaV;
          Dmod.QrInsL.Params[4].AsFloat   := 0;
          Dmod.QrInsL.Params[5].AsFloat   := 0;
          Dmod.QrInsL.ExecSQL;
          Insere := False;
          AcumQ := TiraQ;
          AcumV := TiraV;
          TiraQ := 0;
          TiraV := 0;
        end;
        SomaQ := SomaQ + QrEstqEstqQ.Value;
        SomaV := SomaV + QrEstqEstqV.Value;
        AcumQ := AcumQ + QrEstqEstqQ.Value;
        AcumV := AcumV + QrEstqEstqV.Value;
        Dmod.QrInsL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, QrEstqData.Value);
        Dmod.QrInsL.Params[1].AsInteger := QrEstqTipo.Value;
        Dmod.QrInsL.Params[2].AsFloat   := QrEstqEstqQ.Value;
        Dmod.QrInsL.Params[3].AsFloat   := QrEstqEstqV.Value;
        Dmod.QrInsL.Params[4].AsFloat   := AcumQ;
        Dmod.QrInsL.Params[5].AsFloat   := AcumV;
        Dmod.QrInsL.ExecSQL;
      end;
      if QrEstqTipo.Value = 8 then
      begin
        if Insere = False then
        begin
          AcumQ := 0;
          AcumV := 0;
        end;
        TiraQ := TiraQ + QrEstqEstqQ.Value;
        TiraV := TiraV + QrEstqEstqV.Value;
        AcumQ := AcumQ + QrEstqEstqQ.Value;
        AcumV := AcumV + QrEstqEstqV.Value;
        Insere := True;
        DataPeriodo := FormatDateTime(VAR_FORMATDATE, QrEstqData.Value);
        Dmod.QrInsL.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, QrEstqData.Value);
        Dmod.QrInsL.Params[1].AsInteger := QrEstqTipo.Value;
        Dmod.QrInsL.Params[2].AsFloat   := QrEstqEstqQ.Value;
        Dmod.QrInsL.Params[3].AsFloat   := QrEstqEstqV.Value;
        Dmod.QrInsL.Params[4].AsFloat   := AcumQ;
        Dmod.QrInsL.Params[5].AsFloat   := AcumV;
        Dmod.QrInsL.ExecSQL;
      end;
      QrEstq.Next;
    end;
    QrEstq.SQL.Clear;
    QrEstq.SQL.Add('SELECT * FROM estq');
    QrEstq.SQL.Add('ORDER BY Data, Tipo');
    QrEstq.Open;
    MyObjects.frxMostra(frxEstq, 'Estoque');
    UCriar.RecriaTabelaLocal('Estq', 0);
  end else if RGTipo.Itemindex = 3 then
  begin
    if CkCompactar.Checked then
    begin
      QrVendido2.SQL.Clear;
      QrVendido2.SQL.Add('SELECT ar.Nome DESCRICAO, vi.Produto, SUM(vi.Qtde) Qtde,');
      QrVendido2.SQL.Add('SUM(vi.ValorReal) ValorReal, SUM(vi.Qtde*vi.Preco) BRUTO,');
      QrVendido2.SQL.Add('SUM((vi.Qtde*vi.Preco)-vi.ValorReal) DESCONTO, SUM(Custo) Custo');
      QrVendido2.SQL.Add('FROM vendaspro vi, Produtos ar');
      QrVendido2.SQL.Add('WHERE ar.Codigo=vi.Produto');
      QrVendido2.SQL.Add('AND vi.DataCad BETWEEN "'+DataI+'" AND "'+DataF+'"');
      QrVendido2.SQL.Add('GROUP BY vi.Produto');
      case RGOrdem.ItemIndex of
        0: QrVendido2.SQL.Add('ORDER BY DESCRICAO');
        1: QrVendido2.SQL.Add('ORDER BY DESCRICAO');
        else QrVendido2.SQL.Add('ORDER BY **ERRO ORDEM**');
      end;
      QrVendido2.Open;
      //
      MyObjects.frxMostra(frxVendido2, 'Vendido');
    end else begin
      QrVendido1.SQL.Clear;
      QrVendido1.SQL.Add('SELECT ar.Nome DESCRICAO, vi.*,');
      QrVendido1.SQL.Add('((vi.Qtde*vi.Preco)-vi.ValorReal) Desconto');
      QrVendido1.SQL.Add('FROM vendaspro vi, Produtos ar');
      QrVendido1.SQL.Add('WHERE ar.Codigo=vi.Produto');
      QrVendido1.SQL.Add('AND vi.DataCad BETWEEN "'+DataI+'" AND "'+DataF+'"');
      QrVendido1.SQL.Add('ORDER BY vi.DataCad');
      QrVendido1.Open;
      //
      MyObjects.frxMostra(frxVendido1, 'Vendido');
    end;
  end else if RGTipo.Itemindex = 4 then
  begin
    VAR_NOMETABELAMERCADORIA := 'Produtos';
    VAR_NOMECAMPOMERCADORIA  := 'Codigo';
    Application.CreateForm(TFmProdutosImpEstqEm, FmProdutosImpEstqEm);
    FmProdutosImpEstqEm.ImprimeEstoqueNaData(1, TPDataI.Date);
    FmProdutosImpEstqEm.Destroy;
  end else
    Application.MessageBox('Relat�rio n�o definido', 'Erro', MB_OK+MB_ICONERROR);
  //pi_Filtro := '';
end;

procedure TFmProdutosImprime.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosImprime.frProdutosUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VARF_GRADE'     then
    if RGGrade.ItemIndex = 0 then Val := True else Val := False;
  if Name = 'VARF_FMT'       then Val := '#.###.##0.000;-#.###.##0.000; ';
  if Name = 'VARF_FMT2'      then Val := '"#.###.##0.0000;-#.###.##0.0000; "';
end;

procedure TFmProdutosImprime.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosImprime.QrBalancoCalcFields(DataSet: TDataSet);
begin
  QrBalancoQSAL.Value := QrBalancoEstqQAnt.Value + QrBalancoEstqQEnt.Value +
    QrBalancoEstqQRec.Value - QrBalancoEstqQVen.Value - QrBalancoEstqQDev.Value;
  QrBalancoQDIF.Value := QrBalancoEstqQNow.Value -  QrBalancoQSAL.Value;
  QrBalancoVDIF.Value := QrBalancoEstqVNow.Value -
    QrBalancoEstqVAnt.Value + QrBalancoEstqVEnt.Value +
    QrBalancoEstqVRec.Value - QrBalancoEstqVVen.Value - QrBalancoEstqVDev.Value;
end;

procedure TFmProdutosImprime.RGTipoClick(Sender: TObject);
begin
  CkPositivo.Visible     := False;
  LaMercadoria.Visible   := False;
  EdProduto.Visible      := False;
  CBProduto.Visible      := False;
  LaDataI.Visible        := False;
  LaDataF.Visible        := False;
  TPDataI.Visible        := False;
  TPDataF.Visible        := False;
  LaDataI.Caption := '';
  case RGTipo.ItemIndex of
    0: CkPositivo.Visible := True;
    2:
    begin
      LaMercadoria.Visible   := True;
      EdProduto.Visible      := True;
      CBProduto.Visible      := True;
      LaDataI.Visible        := True;
      LaDataF.Visible        := True;
      TPDataI.Visible        := True;
      TPDataF.Visible        := True;
      LaDataI.Caption        := 'Data inicial:';
    end;
    3:
    begin
      LaMercadoria.Visible   := True;
      EdProduto.Visible      := True;
      CBProduto.Visible      := True;
      LaDataI.Visible        := True;
      LaDataF.Visible        := True;
      TPDataI.Visible        := True;
      TPDataF.Visible        := True;
    end;
    4:
    begin
      LaMercadoria.Visible   := True;
      EdProduto.Visible      := True;
      CBProduto.Visible      := True;
      LaDataI.Visible        := True;
      TPDataI.Visible        := True;
      LaDataI.Caption        := 'Data:';
      CkPositivo.Visible     := True;
      CkCompactar.Visible    := False;
    end;
  end;
end;

procedure TFmProdutosImprime.SpeedButton1Click(Sender: TObject);
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT * FROM produtos');
  QrPesq.SQL.Add('WHERE Nome LIKE :P0');
  QrPesq.SQL.Add('ORDER BY Nome');
  QrPesq.Params[0].AsString := EdPesqNome.Text;
  QrPesq.Open;
end;

procedure TFmProdutosImprime.FormCreate(Sender: TObject);
begin
  TPDataI.Date := Date - 30;
  TPDataF.Date := Date;
  Width  := 510;
  Height := 530;
  QrArtigos.Open;
end;

procedure TFmProdutosImprime.QrEstqCalcFields(DataSet: TDataSet);
begin
  case QrEstqTipo.Value of
    0: QrEstqNOMETIPO.Value := '[DESCONHECIDO]';
    1: QrEstqNOMETIPO.Value := 'Estoque Inicial';
    2: QrEstqNOMETIPO.Value := 'Saldo Anterior';
    3: QrEstqNOMETIPO.Value := 'Compras';
    4: QrEstqNOMETIPO.Value := 'Vendas';
    5: QrEstqNOMETIPO.Value := 'Devolu��es a Fornecedores';
    6: QrEstqNOMETIPO.Value := 'Devolu��es de Clientes';
    7: QrEstqNOMETIPO.Value := 'Diferen�a do Balan�o';
    8: QrEstqNOMETIPO.Value := 'Balan�o [Invent�rio]';
    9: QrEstqNOMETIPO.Value := 'Diferen�a do Balan�o';
    else QrEstqNOMETIPO.Value := '#ERRO TIPO#';
  end;
  //
  if QrEstqTipo.Value  = 9 then
  begin
    QrEstqDIF2Q.Value  := QrEstqEstqQ.Value;
    QrEstqDIF2V.Value  := QrEstqEstqV.Value;
    QrEstqACUM2Q.Value := 0;
    QrEstqACUM2V.Value := 0;
    QrEstqVALQ2.Value  := 0;
    QrEstqVALV2.Value  := 0;
  end else begin
    QrEstqDIF2Q.Value  := 0;
    QrEstqDIF2V.Value  := 0;
    QrEstqACUM2Q.Value := QrEstqAcumQ.Value;
    QrEstqACUM2V.Value := QrEstqAcumV.Value;
    QrEstqVALQ2.Value  := QrEstqEstqQ.Value;
    QrEstqVALV2.Value  := QrEstqEstqV.Value;
  end;

end;

procedure TFmProdutosImprime.QrVendido1CalcFields(DataSet: TDataSet);
begin
  if QrVendido1Qtde.Value <> 0 then
  QrVendido1CUSTOUNIT.Value := QrVendido1Custo.Value / QrVendido1Qtde.Value
  else QrVendido1CUSTOUNIT.Value := 0;
  //
  if QrVendido1Custo.Value <> 0 then
  QrVendido1MARGEM.Value := ((QrVendido1ValorReal.Value -
  QrVendido1Custo.Value) / QrVendido1Custo.Value) * 10000
  else QrVendido1MARGEM.Value := 0;
  //
end;

procedure TFmProdutosImprime.ImprimeBalanco;
var
  DataI, DataF: String;
begin
  UCriar.RecriaTabelaLocal('Bal', 1);
  //
  QrMax.Close;
  QrMax.Params[0].AsInteger := UnAcademy.VerificaBalanco;
  QrMax.Open;
  //
  QrBalAnt.Close;
  QrBalAnt.Params[0].AsInteger := QrMaxPeriodo.Value;
  QrBalAnt.Open;
  Dmod.QrInsL.SQL.Clear;
  Dmod.QrInsL.SQL.Add('INSERT INTO bal SET ');
  Dmod.QrInsL.SQL.Add('Produto=:P0, Descricao=:P1, Fornecedor=:P2, ');
  Dmod.QrInsL.SQL.Add('NomeForecedor=:P3, EstqQAnt=:P4, EstqVAnt=:P5');
  //Dmod.QrInsL.SQL.Add('');
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE bal SET ');
  Dmod.QrUpdL.SQL.Add('EstqQAnt=:P0, EstqVAnt=:P1 WHERE Produto=:P2');
  //Dmod.QrUpdL.SQL.Add('');
  while not QrBalAnt.Eof do
  begin
    QrMerc.Close;
    QrMerc.Params[0].AsInteger := QrBalAntProduto.Value;
    QrMerc.Open;
    if GOTOy.Registros(QrMerc) = 0 then
    begin
      Dmod.QrInsL.Params[0].AsInteger  := QrBalAntProduto.Value;
      Dmod.QrInsL.Params[1].AsString  := QrBalAntDESCRICAO.Value;
      Dmod.QrInsL.Params[2].AsFloat   := 0;//DESCRICAO.Value;
      Dmod.QrInsL.Params[3].AsString  := 'N/D';//QrBalAntNOMEFORNECEDOR.Value;
      Dmod.QrInsL.Params[4].AsFloat   := QrBalAntEstqQ.Value;
      Dmod.QrInsL.Params[5].AsFloat   := QrBalAntEstqV.Value;
      Dmod.QrInsL.ExecSQL;
    end else begin
      Dmod.QrUpdL.Params[0].AsFloat   := QrBalAntEstqQ.Value;
      Dmod.QrUpdL.Params[1].AsFloat   := QrBalAntEstqV.Value;
      Dmod.QrUpdL.Params[2].AsInteger  := QrBalAntProduto.Value;
      Dmod.QrUpdL.ExecSQL;
    end;
    QrBalAnt.Next;
  end;
  //
  QrBalNow.Close;
  QrBalNow.Params[0].AsInteger := UnAcademy.VerificaBalanco;
  QrBalNow.Open;
  Dmod.QrInsL.SQL.Clear;
  Dmod.QrInsL.SQL.Add('INSERT INTO bal SET ');
  Dmod.QrInsL.SQL.Add('Produto=:P0, Descricao=:P1, Fornecedor=:P2, ');
  Dmod.QrInsL.SQL.Add('NomeForecedor=:P3, EstqQNow=:P4, EstqVNow=:P5');
  //Dmod.QrInsL.SQL.Add('');
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE bal SET ');
  Dmod.QrUpdL.SQL.Add('EstqQNow=:P0, EstqVNow=:P1 WHERE Produto=:P2');
  //Dmod.QrUpdL.SQL.Add('');
  while not QrBalNow.Eof do
  begin
    QrMerc.Close;
    QrMerc.Params[0].AsInteger := QrBalNowProduto.Value;
    QrMerc.Open;
    if GOTOy.Registros(QrMerc) = 0 then
    begin
      Dmod.QrInsL.Params[0].AsInteger := QrBalNowProduto.Value;
      Dmod.QrInsL.Params[1].AsString  := QrBalNowDESCRICAO.Value;
      Dmod.QrInsL.Params[2].AsFloat   := 0;//QrBalNowFORNECEDO.Value;
      Dmod.QrInsL.Params[3].AsString  := '';//QrBalNowNOMEFORNECEDOR.Value;
      Dmod.QrInsL.Params[4].AsFloat   := QrBalNowEstqQ.Value;
      Dmod.QrInsL.Params[5].AsFloat   := QrBalNowEstqV.Value;
      Dmod.QrInsL.ExecSQL;
    end else begin
      Dmod.QrUpdL.Params[0].AsFloat   := QrBalNowEstqQ.Value;
      Dmod.QrUpdL.Params[1].AsFloat   := QrBalNowEstqV.Value;
      Dmod.QrUpdL.Params[2].AsInteger := QrBalNowProduto.Value;
      Dmod.QrUpdL.ExecSQL;
    end;
    QrBalNow.Next;
  end;
  //
  DataI := MLAGeral.PrimeiroDiaAposPeriodo(QrMaxPeriodo.Value, dtSystem);
  DataF := MLAGeral.UltimoDiaDoPeriodo(UnAcademy.VerificaBalanco, dtSystem);
  //
  DataI_Date := StrToDate(MLAGeral.PrimeiroDiaAposPeriodo(QrMaxPeriodo.Value, dtSystem2));
  DataF_Date := StrToDate(MLAGeral.UltimoDiaDoPeriodo(UnAcademy.VerificaBalanco, dtSystem2));
  //
  QrAntE.Close;
  QrAntE.Params[0].AsString := DataI;
  QrAntE.Params[1].AsString := DataF;
  QrAntE.Open;
  Dmod.QrInsL.SQL.Clear;
  Dmod.QrInsL.SQL.Add('INSERT INTO bal SET ');
  Dmod.QrInsL.SQL.Add('Produto=:P0, Descricao=:P1, Fornecedor=:P2, ');
  Dmod.QrInsL.SQL.Add('NomeForecedor=:P3, EstqQEnt=:P4, EstqVEnt=:P5');
  //Dmod.QrInsL.SQL.Add('');
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE bal SET ');
  Dmod.QrUpdL.SQL.Add('EstqQEnt=:P0, EstqVEnt=:P1 WHERE Produto=:P2');
  //Dmod.QrUpdL.SQL.Add('');
  while not QrAntE.Eof do
  begin
    QrMerc.Close;
    QrMerc.Params[0].AsInteger := QrAntEPRODUTO.Value;
    QrMerc.Open;
    if GOTOy.Registros(QrMerc) = 0 then
    begin
      Dmod.QrInsL.Params[0].AsInteger := QrAntEProduto.Value;
      Dmod.QrInsL.Params[1].AsString  := QrAntEDESCRICAO.Value;
      Dmod.QrInsL.Params[2].AsFloat   := 0;//QrAntEFORNECEDO.Value;
      Dmod.QrInsL.Params[3].AsString  := '';//QrAntENOMEFORNECEDOR.Value;
      Dmod.QrInsL.Params[4].AsFloat   := QrAntEEstqQ.Value;
      Dmod.QrInsL.Params[5].AsFloat   := QrAntEEstqV.Value;
      Dmod.QrInsL.ExecSQL;
    end else begin
      Dmod.QrUpdL.Params[0].AsFloat   := QrAntEEstqQ.Value;
      Dmod.QrUpdL.Params[1].AsFloat   := QrAntEEstqV.Value;
      Dmod.QrUpdL.Params[2].AsInteger  := QrAntEProduto.Value;
      Dmod.QrUpdL.ExecSQL;
    end;
    QrAntE.Next;
  end;
  //
  QrAntV.Close;
  QrAntV.Params[0].AsString := DataI;
  QrAntV.Params[1].AsString := DataF;
  QrAntV.Open;
  Dmod.QrInsL.SQL.Clear;
  Dmod.QrInsL.SQL.Add('INSERT INTO bal SET ');
  Dmod.QrInsL.SQL.Add('Produto=:P0, Descricao=:P1, Fornecedor=:P2, ');
  Dmod.QrInsL.SQL.Add('NomeForecedor=:P3, EstqQVen=:P4, EstqVVen=:P5');
  //Dmod.QrInsL.SQL.Add('');
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE bal SET ');
  Dmod.QrUpdL.SQL.Add('EstqQVen=:P0, EstqVVen=:P1 WHERE Produto=:P2');
  //Dmod.QrUpdL.SQL.Add('');
  while not QrAntV.Eof do
  begin
    QrMerc.Close;
    QrMerc.Params[0].AsInteger := QrAntVPRODUTO.Value;
    QrMerc.Open;
    if GOTOy.Registros(QrMerc) = 0 then
    begin
      Dmod.QrInsL.Params[0].AsInteger := QrAntVProduto.Value;
      Dmod.QrInsL.Params[1].AsString  := QrAntVDESCRICAO.Value;
      Dmod.QrInsL.Params[2].AsFloat   := 0;//QrAntVFORNECEDO.Value;
      Dmod.QrInsL.Params[3].AsString  := '';//QrAntVNOMEFORNECEDOR.Value;
      Dmod.QrInsL.Params[4].AsFloat   := QrAntVEstqQ.Value;
      Dmod.QrInsL.Params[5].AsFloat   := QrAntVEstqV.Value;
      Dmod.QrInsL.ExecSQL;
    end else begin
      Dmod.QrUpdL.Params[0].AsFloat   := QrAntVEstqQ.Value;
      Dmod.QrUpdL.Params[1].AsFloat   := QrAntVEstqV.Value;
      Dmod.QrUpdL.Params[2].AsInteger := QrAntVProduto.Value;
      Dmod.QrUpdL.ExecSQL;
    end;
    QrAntV.Next;
  end;
  //
  QrAntD.Close;
  QrAntD.Params[0].AsString := DataI;
  QrAntD.Params[1].AsString := DataF;
  QrAntD.Open;
  Dmod.QrInsL.SQL.Clear;
  Dmod.QrInsL.SQL.Add('INSERT INTO bal SET ');
  Dmod.QrInsL.SQL.Add('Produto=:P0, Descricao=:P1, Fornecedor=:P2, ');
  Dmod.QrInsL.SQL.Add('NomeForecedor=:P3, EstqQDev=:P4, EstqVDev=:P5');
  //Dmod.QrInsL.SQL.Add('');
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE bal SET ');
  Dmod.QrUpdL.SQL.Add('EstqQDev=:P0, EstqVDev=:P1 WHERE Produto=:P2');
  //Dmod.QrUpdL.SQL.Add('');
  while not QrAntD.Eof do
  begin
    QrMerc.Close;
    QrMerc.Params[0].AsInteger := QrAntVPRODUTO.Value;
    QrMerc.Open;
    if GOTOy.Registros(QrMerc) = 0 then
    begin
      Dmod.QrInsL.Params[0].AsInteger := QrAntDProduto.Value;
      Dmod.QrInsL.Params[1].AsString  := QrAntDDESCRICAO.Value;
      Dmod.QrInsL.Params[2].AsFloat   := 0;//QrAntDFORNECEDO.Value;
      Dmod.QrInsL.Params[3].AsString  := '';//QrAntDNOMEFORNECEDOR.Value;
      Dmod.QrInsL.Params[4].AsFloat   := QrAntDEstqQ.Value;
      Dmod.QrInsL.Params[5].AsFloat   := QrAntDEstqV.Value;
      Dmod.QrInsL.ExecSQL;
    end else begin
      Dmod.QrUpdL.Params[0].AsFloat   := QrAntDEstqQ.Value;
      Dmod.QrUpdL.Params[1].AsFloat   := QrAntDEstqV.Value;
      Dmod.QrUpdL.Params[2].AsInteger := QrAntDProduto.Value;
      Dmod.QrUpdL.ExecSQL;
    end;
    QrAntD.Next;
  end;
  //
  QrAntR.Close;
  QrAntR.Params[0].AsString := DataI;
  QrAntR.Params[1].AsString := DataF;
  QrAntR.Open;
  Dmod.QrInsL.SQL.Clear;
  Dmod.QrInsL.SQL.Add('INSERT INTO bal SET ');
  Dmod.QrInsL.SQL.Add('Produto=:P0, Descricao=:P1, Fornecedor=:P2, ');
  Dmod.QrInsL.SQL.Add('NomeForecedor=:P3, EstqQRec=:P4, EstqVRec=:P5');
  //Dmod.QrInsL.SQL.Add('');
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE bal SET ');
  Dmod.QrUpdL.SQL.Add('EstqQRec=:P0, EstqVRec=:P1 WHERE Produto=:P2');
  //Dmod.QrUpdL.SQL.Add('');
  while not QrAntR.Eof do
  begin
    QrMerc.Close;
    QrMerc.Params[0].AsInteger := QrAntRPRODUTO.Value;
    QrMerc.Open;
    if GOTOy.Registros(QrMerc) = 0 then
    begin
      Dmod.QrInsL.Params[0].AsInteger := QrAntRProduto.Value;
      Dmod.QrInsL.Params[1].AsString  := QrAntRDESCRICAO.Value;
      Dmod.QrInsL.Params[2].AsFloat   := 0;//QrAntRFORNECEDO.Value;
      Dmod.QrInsL.Params[3].AsString  := '';//QrAntRNOMEFORNECEDOR.Value;
      Dmod.QrInsL.Params[4].AsFloat   := QrAntREstqQ.Value;
      Dmod.QrInsL.Params[5].AsFloat   := QrAntREstqV.Value;
      Dmod.QrInsL.ExecSQL;
    end else begin
      Dmod.QrUpdL.Params[0].AsFloat   := QrAntREstqQ.Value;
      Dmod.QrUpdL.Params[1].AsFloat   := QrAntREstqV.Value;
      Dmod.QrUpdL.Params[2].AsInteger := QrAntRProduto.Value;
      Dmod.QrUpdL.ExecSQL;
    end;
    QrAntR.Next;
  end;
  //
  QrBalanco.Open;
  MyObjects.frxMostra(frxBalanco, 'Balan�o');
  QrBalanco.Close;
  UCriar.RecriaTabelaLocal('Bal', 0);
end;

procedure TFmProdutosImprime.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 10);
end;

procedure TFmProdutosImprime.QrProdutosCalcFields(DataSet: TDataSet);
begin
  if QrProdutosEstqQ.Value > 0 then
     QrProdutosCUSTOMEDIO.Value :=
     QrProdutosEstqV.Value /
     QrProdutosEstqQ.Value  else
     QrProdutosCUSTOMEDIO.Value := 0;
end;

procedure TFmProdutosImprime.DBGPesqDblClick(Sender: TObject);
begin
  CBProduto.KeyValue := QrPesqCodigo.Value;
  EdProduto.Text := IntToStr(QrPesqCodigo.Value);
end;

procedure TFmProdutosImprime.frxGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'NOMEREL_001' then
     Value := 'Relat�rio de Estoque de Mercadorias em '+
     FormatDateTime(VAR_FORMATDATE3, TPDataI.Date);
  if VarName = 'NOMEREL_002' then
     Value := 'Relat�rio de Estoque de Mercadorias';
  if VarName = 'NOMEREL_003' then
     Value := 'Relat�rio do �ltimo Balan�o';
  if VarName = 'NOMEREL_004' then
     Value := 'Hist�rico de Movimento';
  if VarName = 'NOMEREL_005' then
     Value := 'Vendas de Mercadorias (Anal�tico)';
  if VarName = 'NOMEREL_006' then
     Value := 'Vendas de Mercadorias (Sint�tico)';
  //if VarName = 'COD_MERC' then Value := pi_Filtro;
  if VarName = 'PERIODO' then
     Value := FormatDateTime(VAR_FORMATDATE3, DataI_Date) +
     ' at� ' + FormatDateTime(VAR_FORMATDATE3, DataF_DAte);
  if VarName = 'PERIODO2' then
     Value := FormatDateTime(VAR_FORMATDATE3, DataI_Date) +
     ' at� ' + FormatDateTime(VAR_FORMATDATE3, DataF_DAte);
  if VarName = 'MERC_COD' then Value := EdProduto.Text;
  if VarName = 'MERC_NOME' then Value := CBProduto.Text;

  //

  if VarName = 'VARF_FMT'       then Value := '#.###.##0.000;-#.###.##0.000; ';
  if VarName = 'VARF_FMT2'      then Value := '"#.###.##0.0000;-#.###.##0.0000; "';
  if VarName = 'VFR_GRADE'      then
    if RGGrade.ItemIndex = 0 then Value := True else Value := False;
  if VarName = 'VARF_GRADE'     then
    if RGGrade.ItemIndex = 0 then Value := True else Value := False;
  if VarName = 'VFR_CLRED'      then if CkCorLetras.Checked then Value := clRed else Value := clBlack;
  if VarName = 'VFR_CLGREEN'    then if CkCorLetras.Checked then Value := clGreen else Value := clBlack;
  if VarName = 'VFR_CORFUNDO'   then
  begin
    ProdutosI_Cor := MLAGeral.CorImp(FmProdutosImprime.CBCorFundo.Checked,
    (*FmProdutosImprime.CBCompactar.Checked*) False, ProdutosI_Cor, 1);
    Value := ProdutosI_Cor;
  end;

end;

procedure TFmProdutosImprime.CBProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if key = VK_DELETE then CBProduto.KeyValue := NULL;
end;

procedure TFmProdutosImprime.QrVendido2CalcFields(DataSet: TDataSet);
begin
  if QrVendido2Qtde.Value <> 0 then
  begin
    QrVendido2CUSTOUNIT.Value := QrVendido2Custo.Value / QrVendido2Qtde.Value;
    QrVendido2PRECOMEDIO.Value := QrVendido2BRUTO.Value / QrVendido2Qtde.Value;
  end else begin
    QrVendido2CUSTOUNIT.Value := 0;
    QrVendido2PRECOMEDIO.Value := 0;
  end;
  //
  if QrVendido2Custo.Value <> 0 then
  QrVendido2MARGEM.Value := ((QrVendido2ValorReal.Value -
  QrVendido2Custo.Value) / QrVendido2Custo.Value) * 20000
  else QrVendido2MARGEM.Value := 0;
  //
end;

procedure TFmProdutosImprime.frxProdutosGetValue(const VarName: String;
  var Value: Variant);
begin
  frxGetValue(VarName, Value);
end;

end.

