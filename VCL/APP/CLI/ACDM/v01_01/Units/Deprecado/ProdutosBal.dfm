object FmProdutosBal: TFmProdutosBal
  Left = 340
  Top = 174
  Caption = 'Balan'#231'os de Mercadorias'
  ClientHeight = 514
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 418
    Align = alClient
    Color = 13748143
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelConfirma: TPanel
      Left = 1
      Top = 369
      Width = 782
      Height = 48
      Align = alBottom
      TabOrder = 0
      Visible = False
      object BtTrava: TBitBtn
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Trava'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtTravaClick
      end
      object BtIncluiIts: TBitBtn
        Left = 324
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Insere'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiItsClick
      end
      object BtAlteraIts: TBitBtn
        Left = 416
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Edita'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraItsClick
      end
      object BtExcluiIts: TBitBtn
        Left = 508
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiItsClick
      end
      object BtRefresh: TBitBtn
        Left = 152
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Faz / Desfaz'
        TabOrder = 4
        OnClick = BtRefreshClick
      end
      object BtPrivativo: TBitBtn
        Left = 600
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Zera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtPrivativoClick
      end
    end
    object DBGFunci: TDBGrid
      Left = 1
      Top = 51
      Width = 782
      Height = 298
      Align = alClient
      DataSource = DsBalancosIts
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnKeyDown = DBGFunciKeyDown
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Produto'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'digo'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMERCADORIA'
          Title.Caption = 'Mercadoria'
          Width = 315
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqQ'
          Title.Alignment = taRightJustify
          Title.Caption = 'Quantidade'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EstqV'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor total'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEDOR'
          Title.Caption = 'Fornecedor'
          Visible = True
        end>
    end
    object PainelDados1: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 424
        Top = 6
        Width = 68
        Height = 13
        Caption = 'Balan'#231'o Qtde:'
      end
      object Label2: TLabel
        Left = 516
        Top = 6
        Width = 69
        Height = 13
        Caption = 'Balan'#231'o Valor:'
      end
      object RGInsumo: TRadioGroup
        Left = 0
        Top = 2
        Width = 133
        Height = 42
        Caption = ' Ordem das Mercadorias: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Nome'
          'Codigo')
        TabOrder = 0
        OnClick = RGInsumoClick
      end
      object RGFornece: TRadioGroup
        Left = 136
        Top = 2
        Width = 285
        Height = 42
        Caption = ' Odem complementar: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Codigo do Grupo'
          'Listagem das Mercador.')
        TabOrder = 1
        OnClick = RGForneceClick
      end
      object DBEdit1: TDBEdit
        Left = 424
        Top = 22
        Width = 89
        Height = 21
        TabStop = False
        DataField = 'EstqQ'
        DataSource = DsBalancos
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 516
        Top = 22
        Width = 89
        Height = 21
        TabStop = False
        DataField = 'EstqV'
        DataSource = DsBalancos
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 612
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Confirmado.'
        DataField = 'Confirmado'
        DataSource = DsBalancos
        TabOrder = 4
        ValueChecked = 'V'
        ValueUnchecked = 'F'
      end
    end
    object PainelDados2: TPanel
      Left = 1
      Top = 349
      Width = 782
      Height = 20
      Align = alBottom
      TabOrder = 3
      Visible = False
      object Progress1: TProgressBar
        Left = 469
        Top = 1
        Width = 312
        Height = 18
        Align = alClient
        Position = 50
        Smooth = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 228
        Height = 18
        Align = alLeft
        BevelOuter = bvLowered
        Caption = 'Aguarde...    Atualizando estoque...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = True
        ParentFont = False
        TabOrder = 1
      end
      object Panel2: TPanel
        Left = 229
        Top = 1
        Width = 60
        Height = 18
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Registros:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 2
      end
      object PnRegistros: TPanel
        Left = 289
        Top = 1
        Width = 76
        Height = 18
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 3
      end
      object Panel4: TPanel
        Left = 365
        Top = 1
        Width = 48
        Height = 18
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Caption = '  Tempo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 4
      end
      object PnTempo: TPanel
        Left = 413
        Top = 1
        Width = 56
        Height = 18
        Align = alLeft
        Alignment = taRightJustify
        BevelOuter = bvLowered
        Caption = '0:00:00  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 6967296
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 5
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Balan'#231'os de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 475
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill002: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 466
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 2
    object LaRegistro: TLabel
      Left = 173
      Top = 1
      Width = 26
      Height = 13
      Align = alClient
      Caption = '[N]: 0'
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        ModalResult = 5
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 314
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtExclui: TBitBtn
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
      object BtAltera: TBitBtn
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
      end
    end
  end
  object DsBalancos: TDataSource
    DataSet = QrBalancos
    Left = 36
    Top = 133
  end
  object DsBalancosIts: TDataSource
    DataSet = QrBalancosIts
    Left = 36
    Top = 165
  end
  object DsArtigo: TDataSource
    DataSet = QrArtigo
    Left = 36
    Top = 220
  end
  object QrBalancos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBalancosAfterOpen
    AfterScroll = QrBalancosAfterScroll
    OnCalcFields = QrBalancosCalcFields
    SQL.Strings = (
      'SELECT * FROM balancos')
    Left = 8
    Top = 133
    object QrBalancosPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalancosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrBalancosEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrBalancosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBalancosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBalancosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBalancosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBalancosConfirmado: TWideStringField
      FieldName = 'Confirmado'
      Size = 1
    end
    object QrBalancosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBalancosPeriodo2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Periodo2'
      Size = 50
      Calculated = True
    end
  end
  object QrBalancosIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrBalancosItsAfterOpen
    OnCalcFields = QrBalancosItsCalcFields
    SQL.Strings = (
      'SELECT cu.*, ar.Nome, ar.Grupo,'
      'pg.Nome NOMEGRUPO, ar.Nome NOMEMERCADORIA'
      'FROM balancosits cu, Produtos ar,'
      'ProdutosG pg'
      'WHERE cu.Periodo=:P0'
      'AND ar.Codigo=cu.Produto'
      'AND pg.Codigo=ar.Grupo')
    Left = 8
    Top = 165
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBalancosItsPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrBalancosItsProduto: TIntegerField
      FieldName = 'Produto'
    end
    object QrBalancosItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      DisplayFormat = '#,###,##0.000'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrBalancosItsAnteQ: TFloatField
      FieldName = 'AnteQ'
    end
    object QrBalancosItsAnteV: TFloatField
      FieldName = 'AnteV'
    end
    object QrBalancosItsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrBalancosItsGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrBalancosItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrBalancosItsNOMEMERCADORIA: TWideStringField
      FieldName = 'NOMEMERCADORIA'
      Required = True
      Size = 100
    end
  end
  object QrArtigo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 8
    Top = 221
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArtigoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrArtigoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrAtualiza: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo Produto '
      'FROM produtos'
      'ORDER BY Codigo')
    Left = 8
    Top = 253
    object QrAtualizaProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 64
    Top = 253
    object QrMaxPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(EstqQ) EstqQ, SUM(EstqV) EstqV'
      'FROM balancosits'
      'WHERE Periodo=:P0')
    Left = 36
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaEstqQ: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqQ'
    end
    object QrSomaEstqV: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'EstqV'
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, EstqQ, EstqV'
      'FROM produtos'
      'WHERE EstqQ<>0'
      'OR EstqV<>0'
      '')
    Left = 448
    Top = 144
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
    end
  end
end
