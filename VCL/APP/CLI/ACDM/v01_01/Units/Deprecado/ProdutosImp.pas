unit ProdutosImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Db, (*DBTables,*) DBCtrls, UnMLAGeral,
  UnInternalConsts, mySQLDbTables, frxClass, frxDBSet, dmkGeral, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmProdutosImp = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrProdutos: TmySQLQuery;
    QrProdutosCUSTOMEDIO: TFloatField;
    RGOrdem: TRadioGroup;
    RGGRade: TRadioGroup;
    Label11: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    QrProdutosG: TmySQLQuery;
    DsProdutosG: TDataSource;
    QrProdutosGCodigo: TIntegerField;
    QrProdutosGNome: TWideStringField;
    QrProdutosNOMEGRUPO: TWideStringField;
    RGAgrupa: TRadioGroup;
    RGTipo: TRadioGroup;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrProdutosCodigo: TIntegerField;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosJuros: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosImprime: TWideStringField;
    QrProdutosControla: TWideStringField;
    QrProdutosCasas: TIntegerField;
    QrProdutosLk: TIntegerField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    CkSomenteComEstoque: TCheckBox;
    frxProdutos: TfrxReport;
    frxDsProdutos: TfrxDBDataset;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure QrProdutosCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure frxProdutosGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmProdutosImp: TFmProdutosImp;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmProdutosImp.BtOKClick(Sender: TObject);
var
  OrdNum: Integer;
  Ordem: String;
begin
  Ordem := CO_VAZIO;
  OrdNum := RGAgrupa.ItemIndex*10 + RGOrdem.ItemIndex;
  case OrdNum of
     0: Ordem := 'gr.Nome, pr.Nome';
     1: Ordem := 'gr.Nome, pr.Codigo';
    10: Ordem := 'gr.Codigo, pr.Nome';
    11: Ordem := 'gr.Codigo, pr.Codigo';
    else Ordem := '*ERRO NA ORDEM*';
  end;
  QrProdutos.Close;
  QrProdutos.SQL.Clear;
  QrProdutos.SQL.Add('SELECT pr.*, gr.Nome NOMEGRUPO');
  QrProdutos.SQL.Add('FROM produtos pr, ProdutosG gr');
  QrProdutos.SQL.Add('WHERE gr.Codigo=pr.Grupo');
  if CBGrupo.KeyValue <> NULL then
    QrProdutos.SQL.Add('AND pr.Grupo='+IntToStr(CBGrupo.KeyValue));
  if CkSomenteComEstoque.Checked then
    QrProdutos.SQL.Add('AND pr.EstqQ>0');
  if Ordem <> CO_VAZIO then
    QrProdutos.SQL.Add('ORDER BY '+Ordem);
  QrProdutos.Open;
  MyObjects.frxMostra(frxProdutos, 'Produtos');
  QrProdutos.Close;
end;

procedure TFmProdutosImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosImp.QrProdutosCalcFields(DataSet: TDataSet);
begin
  if QrProdutosEstqQ.Value > 0 then
     QrProdutosCUSTOMEDIO.Value :=
     QrProdutosEstqV.Value /
     QrProdutosEstqQ.Value  else
     QrProdutosCUSTOMEDIO.Value := 0;
end;

procedure TFmProdutosImp.FormCreate(Sender: TObject);
begin
  QrProdutosG.Open;
end;

procedure TFmProdutosImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmProdutosImp.frxProdutosGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'NOMEREL_002' then
     Value := 'Relatório de Produtos';
  if VarName = 'GRUPO_PRODUTO' then
    if CBGrupo.KeyValue <> NULL then Value := CBGrupo.Text
    else Value := 'TODOS';
  if VarName = 'VARF_GRADE' then
    if RGGrade.ItemIndex = 0 then Value := True else Value := False;
  if VarName = 'VFR_AGRUPA' then
    if RGAgrupa.ItemIndex = 0 then Value := True else Value := False;

end;

end.

