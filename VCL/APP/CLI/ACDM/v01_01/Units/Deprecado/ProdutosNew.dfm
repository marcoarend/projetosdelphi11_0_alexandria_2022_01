object FmProdutosNew: TFmProdutosNew
  Left = 388
  Top = 261
  Caption = 'Cadastro de Produto'
  ClientHeight = 271
  ClientWidth = 599
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 599
    Height = 175
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 599
      Height = 175
      Align = alClient
      ParentColor = True
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Codigo:'
        FocusControl = EdCodigo
      end
      object Label2: TLabel
        Left = 80
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
        FocusControl = EdCodigo
      end
      object Label3: TLabel
        Left = 12
        Top = 48
        Width = 84
        Height = 13
        Caption = 'Pre'#231'o de compra:'
        FocusControl = EdCodigo
      end
      object Label4: TLabel
        Left = 104
        Top = 48
        Width = 29
        Height = 13
        Caption = 'ICMS:'
        FocusControl = EdCodigo
      end
      object Label5: TLabel
        Left = 148
        Top = 48
        Width = 40
        Height = 13
        Caption = 'R.ICMS:'
        FocusControl = EdCodigo
      end
      object Label6: TLabel
        Left = 192
        Top = 48
        Width = 16
        Height = 13
        Caption = 'IPI:'
        FocusControl = EdCodigo
      end
      object Label7: TLabel
        Left = 236
        Top = 48
        Width = 27
        Height = 13
        Caption = 'R.IPI:'
        FocusControl = EdCodigo
      end
      object Label8: TLabel
        Left = 280
        Top = 48
        Width = 27
        Height = 13
        Caption = 'Frete:'
        FocusControl = EdCodigo
      end
      object Label9: TLabel
        Left = 428
        Top = 48
        Width = 79
        Height = 13
        Caption = 'Pre'#231'o de venda:'
        FocusControl = EdCodigo
      end
      object Label10: TLabel
        Left = 364
        Top = 48
        Width = 28
        Height = 13
        Caption = 'Juros:'
        FocusControl = EdCodigo
      end
      object Label11: TLabel
        Left = 12
        Top = 92
        Width = 91
        Height = 13
        Caption = 'Grupo de produtos:'
      end
      object Label12: TLabel
        Left = 420
        Top = 4
        Width = 126
        Height = 13
        Caption = 'Nome para impress'#227'o [F4]:'
        FocusControl = EdCodigo
      end
      object Label13: TLabel
        Left = 512
        Top = 48
        Width = 76
        Height = 13
        Caption = 'Casas decimais:'
        FocusControl = EdCodigo
      end
      object SpeedButton1: TSpeedButton
        Left = 444
        Top = 108
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 20
        Width = 64
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 80
        Top = 20
        Width = 337
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdPrecoC: TdmkEdit
        Left = 12
        Top = 64
        Width = 85
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdICMS: TdmkEdit
        Left = 104
        Top = 64
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdRICMS: TdmkEdit
        Left = 148
        Top = 64
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdIPI: TdmkEdit
        Left = 192
        Top = 64
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdRIPI: TdmkEdit
        Left = 236
        Top = 64
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '99'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdFrete: TdmkEdit
        Left = 280
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdPrecoV: TdmkEdit
        Left = 428
        Top = 64
        Width = 81
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdJuros: TdmkEdit
        Left = 364
        Top = 64
        Width = 61
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdGrupo: TdmkEditCB
        Left = 12
        Top = 108
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBGrupo
        IgnoraDBLookupComboBox = False
      end
      object CBGrupo: TdmkDBLookupComboBox
        Left = 80
        Top = 108
        Width = 364
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProdutosG
        TabOrder = 13
        dmkEditCB = EdGrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CkAtivo: TCheckBox
        Left = 480
        Top = 112
        Width = 53
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 14
      end
      object EdNome2: TdmkEdit
        Left = 420
        Top = 20
        Width = 177
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnKeyDown = EdNome2KeyDown
      end
      object CkImprime: TCheckBox
        Left = 536
        Top = 112
        Width = 61
        Height = 17
        Caption = 'Imprime.'
        TabOrder = 15
      end
      object EdCasas: TdmkEdit
        Left = 512
        Top = 64
        Width = 85
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '3'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 2
      end
      object CkControla: TCheckBox
        Left = 12
        Top = 140
        Width = 125
        Height = 17
        Caption = 'Controla estoque.'
        TabOrder = 16
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 223
    Width = 599
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 480
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Desiste'
      Caption = '&Desiste'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtDesisteClick
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 24
      Top = 4
      Width = 90
      Height = 40
      Hint = 'Confirma'
      Caption = '&Confirma'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 599
    Height = 48
    Align = alTop
    Caption = 'Cadastro de Produto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 520
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 527
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 519
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 525
      ExplicitHeight = 44
    end
  end
  object QrProdutosG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM produtosg'
      'ORDER BY Nome')
    Left = 68
    Top = 8
    object QrProdutosGCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DNSTORE001.produtosg.Codigo'
    end
    object QrProdutosGNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DNSTORE001.produtosg.Nome'
      Size = 14
    end
  end
  object DsProdutosG: TDataSource
    DataSet = QrProdutosG
    Left = 96
    Top = 8
  end
end
