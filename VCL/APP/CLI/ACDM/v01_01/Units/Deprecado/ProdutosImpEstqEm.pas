unit ProdutosImpEstqEm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, mySQLDbTables, frxClass, frxDBSet,
  dmkGeral, Variants;

type
  TFmProdutosImpEstqEm = class(TForm)
    QrVendas: TmySQLQuery;
    QrEntradas: TmySQLQuery;
    QrDevolucaos: TmySQLQuery;
    QrRecompras: TmySQLQuery;
    QrMerc5: TmySQLQuery;
    QrDevolucaosMercadoria: TWideStringField;
    QrDevolucaosPecas: TFloatField;
    QrDevolucaosValor: TFloatField;
    QrRecomprasMercadoria: TWideStringField;
    QrRecomprasPecas: TFloatField;
    QrRecomprasValor: TFloatField;
    QrMercEstq: TmySQLQuery;
    QrMercEstqTOTALPESO: TFloatField;
    QrMercEstqTOTALVALOR: TFloatField;
    QrMerc5Mercadoria: TIntegerField;
    QrMerc5Nome: TWideStringField;
    QrMerc5INIPECAS: TFloatField;
    QrMerc5INIVALOR: TFloatField;
    QrVendasProduto: TIntegerField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrEntradasProduto: TIntegerField;
    QrEntradasPecas: TFloatField;
    QrEntradasValor: TFloatField;
    QrMercEstqEQuant: TFloatField;
    QrMercEstqEValor: TFloatField;
    QrMercEstqSQuant: TFloatField;
    QrMercEstqSValor: TFloatField;
    QrMercEstqMercadoria: TIntegerField;
    QrMercEstqFornecedor: TFloatField;
    QrMercEstqGestor: TFloatField;
    QrMercEstqNomeMercadoria: TWideStringField;
    QrMercEstqNomeFornecedor: TWideStringField;
    QrMercEstqNomeGestor: TWideStringField;
    QrMercEstqTipo: TWideStringField;
    frxMercEstq: TfrxReport;
    frxDsMercEstq: TfrxDBDataset;
    procedure frPQEstqGetValue(const ParName: String;
      var ParValue: Variant);
    procedure QrMercEstqCalcFields(DataSet: TDataSet);
    procedure frxMercEstqGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeEstoqueNaData(Tipo: Integer; Data: TDate);
    procedure InsereMovimento(Mercadoria: String; PesoI, ValorI: Double);
(*    procedure InserePesagem51(Insumo, Setor, IQ: Integer; PesoI, ValorI:
              Double; NomeInsumo, NomeSetor, NomeIQ: String; Data: TDate);*)
    procedure InsereMovimento2(Mercadoria: String; Fornecedor, Gestor,
              PesoI, ValorI: Double; NomeMercadoria, NomeFornecedor, NomeGestor:
              String; Data: TDate);
  end;

var
  FmProdutosImpEstqEm: TFmProdutosImpEstqEm;

implementation

uses UnMyObjects, ProdutosImprime, Module, UCreate, UnGOTOy, UnitAcademy;

{$R *.DFM}

procedure TFmProdutosImpEstqEm.ImprimeEstoqueNaData(Tipo: Integer; Data: TDate);
var
  Merc: Integer;
  ORDER: String;
begin
  if Trim(VAR_NOMETABELAMERCADORIA) = '' then
  begin
    Application.MessageBox(' Nome da Tabela n�o definido.', 'Erro', MB_OK+MB_ICONERROR);
    Close;
    Exit;
  end;
  if Trim(VAR_NOMECAMPOMERCADORIA) = '' then
  begin
    Application.MessageBox(' Nome do Campo Mercadoria n�o definido.', 'Erro', MB_OK+MB_ICONERROR);
    Close;
    Exit;
  end;
  /////////////////
  case FmProdutosImprime.RGOrdem.ItemIndex of
    0: ORDER := 'ORDER BY NomeMercadoria';
    1: ORDER := 'ORDER BY Mercadoria';
    else ORDER := '';
  end;
  /////////////////
  MercI_DataFim := FormatDateTime(VAR_FORMATDATE, Data);
  if FmProdutosImprime.CBProduto.KeyValue <> NULL then
    Merc := FmProdutosImprime.CBProduto.KeyValue else Merc := 0;
  QrMerc5.Close;
  QrMerc5.SQL.Clear;
  QrMerc5.SQL.Add('SELECT me.Codigo Mercadoria, me.Nome Nome,');
  QrMerc5.SQL.Add('me.EIniQ INIPECAS, me.EIniV INIVALOR');
  QrMerc5.SQL.Add('FROM produtos me');
  if Merc <> 0 then
    QrMerc5.SQL.Add('WHERE me.Codigo='''+IntToStr(Merc)+'''');
  QrMerc5.Open;
  if (Tipo=0) then
  begin
    UCriar.RecriaTabelaLocal('EstqEm', 1);
    if Merc <> 0 then
      InsereMovimento(FormatFloat('0',Merc), QrMerc5INIPECAS.Value, QrMerc5INIVALOR.Value)
    else begin
      FmProdutosImprime.Progress.Max := GOTOy.Registros(QrMerc5);
      FmProdutosImprime.Progress.Position := 0;
      FmProdutosImprime.Progress.Visible := True;
      while not QrMerc5.Eof do
      begin
        FmProdutosImprime.Progress.Position := FmProdutosImprime.Progress.Position + 1;
        if QrMerc5Mercadoria.Value = 0 then
          InsereMovimento(IntToStr(QrMerc5Mercadoria.Value), QrMerc5INIPECAS.Value, QrMerc5INIVALOR.Value);
        QrMerc5.Next;
      end;
    end;
  end;
  if (Tipo=1) then
  begin
    UCriar.RecriaTabelaLocal('MercEstq', 1);
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO mercestq SET ');
    Dmod.QrUpdL.SQL.Add('Mercadoria=:P0, Fornecedor=:P1, Gestor=:P2, Tipo=:P3, ');
    Dmod.QrUpdL.SQL.Add('NomeMercadoria=:P4, NomeFornecedor=:P5, NomeGestor=:P6, ');
    Dmod.QrUpdL.SQL.Add('EQuant=:P7, EValor=:P8, SQuant=:P9, SValor=:P10');
    Dmod.QrUpdL.SQL.Add('');
    if Merc <> 0 then
      InsereMovimento2(FormatFloat('0', Merc), 0, 0,
      QrMerc5IniPecas.Value, QrMerc5IniValor.Value, QrMerc5Nome.Value,
      'FORNECEDOR DESCONHECIDO', 'GESTOR DESCONHECIDO', Data)
    else begin
      FmProdutosImprime.Progress.Max := GOTOy.Registros(QrMerc5);
      FmProdutosImprime.Progress.Position := 0;
      FmProdutosImprime.Progress.Visible := True;
      while not QrMerc5.Eof do
      begin
        FmProdutosImprime.Progress.Position := FmProdutosImprime.Progress.Position + 1;
        InsereMovimento2(IntToStr(QrMerc5Mercadoria.Value), 0,
          0, QrMerc5IniPecas.Value, QrMerc5IniValor.Value, QrMerc5Nome.Value,
          'FORNECEDOR DESCONHECIDO', 'GESTOR DESCONHECIDO', Data);
        QrMerc5.Next;
      end;
    end;
  end;
  if (Tipo=2) then
  begin
  // Nada
  end;
  if Tipo=1 then
  begin
    QrMercEstq.Close;
    QrMercEstq.SQL.Clear;
    if FmProdutosImprime.CkCompactar.Checked then
    begin
      QrMercEstq.SQL.Add('SELECT SUM(EQuant) EQuant, SUM(EValor) EValor,');
      QrMercEstq.SQL.Add('SUM(SQuant) SQuant, SUM(SValor) SValor,');
      QrMercEstq.SQL.Add('Mercadoria, Fornecedor, Gestor,');
      QrMercEstq.SQL.Add('NomeMercadoria, NomeFornecedor,');
      QrMercEstq.SQL.Add('NomeGestor, Tipo');
      QrMercEstq.SQL.Add('FROM mercestq');
      QrMercEstq.SQL.Add('WHERE (EQuant > 0) OR (EValor) > 0');
      QrMercEstq.SQL.Add('GROUP BY Mercadoria');
    end else begin
      QrMercEstq.SQL.Add('SELECT * FROM mercestq');
      QrMercEstq.SQL.Add('WHERE (EQuant > 0) OR (EValor) > 0');
    end;
    QrMercEstq.SQL.Add(ORDER);
    QrMercEstq.Open;
    MyObjects.frxMostra(frxMercEstq, 'Estoque em...');
    UCriar.RecriaTabelaLocal('MercEstq', 0);
  end;
  if Tipo=2 then
  begin
    //nada
  end;
  FmProdutosImprime.Progress.Visible := False;
end;

procedure TFmProdutosImpEstqEm.InsereMovimento(Mercadoria: String; PesoI, ValorI: Double);
begin
(*    MERCADORIAS
SELECT me.Cdigo Codigo, me.Nome Nome,
me.EIniPeso Peso, me.EIniValor Valor,
me.Gestor,
CASE WHEN en.Tipo=0 THEN en.RazaoSocial
ELSE en.Nome END NOMEGESTOR
FROM mercadorias me, Entidades en
WHERE me.Codigo<>""
AND me.Gestor=en.Codigo
*)
(*
SELECT ar.Codigo Codigo, ar.Descricao Nome,
cu.IniPecas Peso, cu.IniValor Valor,
CASE WHEN en.Tipo=0 THEN en.RazaoSocial
ELSE en.Nome END NOMEGESTOR
FROM artigos ar, Custos cu, Entidades en
WHERE ar.Codigo<>""
AND ar.Codigo=cu.Codigo
AND cu.Gestor=en.Codigo
/*)
  QrVendas.Close;
  QrVendas.SQL.Clear;
  QrVendas.SQL.Add('SELECT vi.Mercadoria, SUM(vi.Pecas) Pecas,');
  QrVendas.SQL.Add('SUM(vi.VTotal) Valor');
  QrVendas.SQL.Add('FROM vendasits vi, Vendas ve, '+VAR_NOMETABELAMERCADORIA+' me');
  QrVendas.SQL.Add('WHERE me.'+VAR_NOMECAMPOMERCADORIA+'=vi.Mercadoria');
  QrVendas.SQL.Add('AND vi.Codigo=ve.Codigo');
  QrVendas.SQL.Add('AND ve.Data<='''+MercI_DataFim+'''');
  QrVendas.SQL.Add('AND vi.Mercadoria='+Mercadoria);
  QrVendas.SQL.Add('GROUP BY vi.Mercadoria');
  QrVendas.Open;
  //
  QrEntradas.Close;
  QrEntradas.SQL.Clear;
  QrEntradas.SQL.Add('SELECT vi.Mercadoria, SUM(vi.Pecas) Pecas,');
  QrEntradas.SQL.Add('SUM(vi.VTotal) Valor');
  QrEntradas.SQL.Add('FROM entradaits vi, Entrada ve, '+VAR_NOMETABELAMERCADORIA+' me');
  QrEntradas.SQL.Add('WHERE me.'+VAR_NOMECAMPOMERCADORIA+'=vi.Mercadoria');
  QrEntradas.SQL.Add('AND vi.Codigo=ve.Codigo');
  QrEntradas.SQL.Add('AND ve.Data<='''+MercI_DataFim+'''');
  QrEntradas.SQL.Add('AND vi.Mercadoria='+Mercadoria);
  QrEntradas.SQL.Add('GROUP BY vi.Mercadoria');
  QrEntradas.Open;
  //
  QrDevolucaos.Close;
  QrDevolucaos.SQL.Clear;
  QrDevolucaos.SQL.Add('SELECT vi.Mercadoria, SUM(vi.Pecas) Pecas,');
  QrDevolucaos.SQL.Add('SUM(vi.VTotal) Valor');
  QrDevolucaos.SQL.Add('FROM devolucaoits vi, Devolucao ve, '+VAR_NOMETABELAMERCADORIA+' me');
  QrDevolucaos.SQL.Add('WHERE me.'+VAR_NOMECAMPOMERCADORIA+'=vi.Mercadoria');
  QrDevolucaos.SQL.Add('AND vi.Codigo=ve.Codigo');
  QrDevolucaos.SQL.Add('AND ve.Data<='''+MercI_DataFim+'''');
  QrDevolucaos.SQL.Add('AND vi.Mercadoria='+Mercadoria);
  QrDevolucaos.SQL.Add('GROUP BY vi.Mercadoria');
  QrDevolucaos.Open;
  //
  QrRecompras.Close;
  QrRecompras.SQL.Clear;
  QrRecompras.SQL.Add('SELECT vi.Mercadoria, SUM(vi.Pecas) Pecas,');
  QrRecompras.SQL.Add('SUM(vi.VTotal) Valor');
  QrRecompras.SQL.Add('FROM recompraits vi, Recompra ve, '+VAR_NOMETABELAMERCADORIA+' me');
  QrRecompras.SQL.Add('WHERE me.'+VAR_NOMECAMPOMERCADORIA+'=vi.Mercadoria');
  QrRecompras.SQL.Add('AND vi.Codigo=ve.Codigo');
  QrRecompras.SQL.Add('AND ve.Data<='''+MercI_DataFim+'''');
  QrRecompras.SQL.Add('AND vi.Mercadoria='+Mercadoria);
  QrRecompras.SQL.Add('GROUP BY vi.Mercadoria');
  QrRecompras.Open;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO estqem SET');
  Dmod.QrUpdL.SQL.Add('Mercadoria=:P0, PEntrada=:P1, VEntrada=:P2, PVenda=:P3, ');
  Dmod.QrUpdL.SQL.Add('VVenda=:P4, PDevolucao=:P5, VDevolucao=:P6, PRecompra=:P7, ');
  Dmod.QrUpdL.SQL.Add('VRecompra=:P8, PInicial=:P9, VInicial=:P10, ');
  Dmod.QrUpdL.SQL.Add('NOMEMERCADORIA=:P11, NOMEFORNECEDOR=:P12, NOMEGESTOR=:P13 ');
  Dmod.QrUpdL.Params[00].AsString :=  Mercadoria;
  Dmod.QrUpdL.Params[01].AsFloat :=  QrEntradasPecas.Value;
  Dmod.QrUpdL.Params[02].AsFloat :=  QrEntradasValor.Value;
  Dmod.QrUpdL.Params[03].AsFloat :=  QrVendasPecas.Value;
  Dmod.QrUpdL.Params[04].AsFloat :=  QrVendasValor.Value;
  Dmod.QrUpdL.Params[05].AsFloat :=  QrDevolucaosPecas.Value;
  Dmod.QrUpdL.Params[06].AsFloat :=  QrDevolucaosValor.Value;
  Dmod.QrUpdL.Params[07].AsFloat :=  QrRecomprasPecas.Value;
  Dmod.QrUpdL.Params[08].AsFloat :=  QrRecomprasValor.Value;
  Dmod.QrUpdL.Params[09].AsFloat :=  PesoI;
  Dmod.QrUpdL.Params[10].AsFloat :=  ValorI;
  Dmod.QrUpdL.Params[11].AsString :=  QrMerc5Nome.Value;
  Dmod.QrUpdL.Params[12].AsString :=  '';
  Dmod.QrUpdL.Params[13].AsString :=  'GESTOR DESCONHECIDO';
  Dmod.QrUpdL.ExecSQL;
  //
  QrEntradas.Close;
  QrVendas.Close;
  QrDevolucaos.Close;
  QrRecompras.Close;
end;

procedure TFmProdutosImpEstqEm.InsereMovimento2(Mercadoria: String; Fornecedor, Gestor,
          PesoI, ValorI: Double; NomeMercadoria, NomeFornecedor, NomeGestor:
          String; Data: TDate);
var
  Valores: MyArrayD12;
  Insere, i, x, y: Integer;
  a, b, c, d: Double;
  Txt: String;
begin
  a := 0;
  b := 0;
  c := 0;
  d := 0;
  Valores := UnAcademy.VerificaEstoqueMercadoria(Mercadoria, Data, epAnalitico, True);
  Insere := 0;
  for i := 0 to 5 do
  begin
    x := i * 2 + 1;
    y := x + 1;
    case i of
       0: if Valores[x] > 0 then a := Valores[x] else a := 0;
       1: a := Valores[x];
       2: a := 0;
       3: a := 0;
       4: a := Valores[x];
       5: if Valores[x] > 0 then a := Valores[x] else a := 0;
    end;
    case i of
       0: if Valores[x] > 0 then b := Valores[y] else b := 0;
       1: b := Valores[y];
       2: b := 0;
       3: b := 0;
       4: b := Valores[y];
       5: if Valores[x] > 0 then b := Valores[y] else b := 0;
    end;
    case i of
       0: if Valores[x] < 0 then c := -Valores[x] else c := 0;
       1: c := 0;
       2: c := Valores[x];
       3: c := Valores[x];
       4: c := 0;
       5: if Valores[x] < 0 then c := -Valores[x] else c := 0;
    end;
    case i of
       0: if Valores[x] < 0 then d := -Valores[y] else d := 0;
       1: d := 0;
       2: d := Valores[y];
       3: d := Valores[y];
       4: d := 0;
       5: if Valores[x] < 0 then d := -Valores[y] else d := 0;
    end;
    case i of
       0: Txt := 'Saldo anterior';
       1: Txt := 'Compra';
       2: Txt := 'Venda';
       3: Txt := 'Devolu��o de compra';
       4: Txt := 'Devolu��o de venda';
       5: Txt := 'Diferen�a de balan�o';
    end;
    if (Valores[x] <> 0) or (Valores[y] <> 0) then
    begin
      Insere := Insere + 1;
      Dmod.QrUpdL.Params[00].AsString  := Mercadoria;
      Dmod.QrUpdL.Params[01].AsFloat   := Fornecedor;
      Dmod.QrUpdL.Params[02].AsFloat   := Gestor;
      Dmod.QrUpdL.Params[03].AsString  := Txt;
      Dmod.QrUpdL.Params[04].AsString  := NomeMercadoria;
      Dmod.QrUpdL.Params[05].AsString  := NomeFornecedor;
      Dmod.QrUpdL.Params[06].AsString  := NomeGestor;
      Dmod.QrUpdL.Params[07].AsFloat   := a;
      Dmod.QrUpdL.Params[08].AsFloat   := b;
      Dmod.QrUpdL.Params[09].AsFloat   := c;
      Dmod.QrUpdL.Params[10].AsFloat   := d;
      Dmod.QrUpdL.ExecSQL;
    end;
  end;
  if Insere = 0 then
  begin
    Dmod.QrUpdL.Params[00].AsString  := Mercadoria;
    Dmod.QrUpdL.Params[01].AsFloat   := Fornecedor;
    Dmod.QrUpdL.Params[02].AsFloat   := Gestor;
    Dmod.QrUpdL.Params[03].AsString  := Txt;
    Dmod.QrUpdL.Params[04].AsString  := NomeMercadoria;
    Dmod.QrUpdL.Params[05].AsString  := NomeFornecedor;
    Dmod.QrUpdL.Params[06].AsString  := NomeGestor;
    Dmod.QrUpdL.Params[07].AsFloat   := 0;
    Dmod.QrUpdL.Params[08].AsFloat   := 0;
    Dmod.QrUpdL.Params[09].AsFloat   := 0;
    Dmod.QrUpdL.Params[10].AsFloat   := 0;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmProdutosImpEstqEm.QrMercEstqCalcFields(DataSet: TDataSet);
begin
  QrMercEstqTOTALPESO.Value  := QrMercEstqEQuant.Value  - QrMercEstqSQuant.Value;
  QrMercEstqTOTALVALOR.Value := QrMercEstqEValor.Value  - QrMercEstqSValor.Value;
end;

procedure TFmProdutosImpEstqEm.frPQEstqGetValue(const ParName: String;
  var ParValue: Variant);
begin
  FmProdutosImprime.frxGetValue(ParName, ParValue);
end;

procedure TFmProdutosImpEstqEm.frxMercEstqGetValue(const VarName: String;
var Value: Variant);
begin
  FmProdutosImprime.frxGetValue(VarName, Value);
end;

end.

