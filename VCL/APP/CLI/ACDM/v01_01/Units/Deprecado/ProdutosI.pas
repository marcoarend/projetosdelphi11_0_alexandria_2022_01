unit ProdutosI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, Db, (*DBTables,*) StdCtrls, Buttons, UnitAcademy,
  DBCtrls, Mask, UnInternalConsts, UnInternalConsts2, UnMLAGeral, dmkGeral,
  mySQLDbTables;

type
  TFmProdutosI = class(TForm)
    PainelGrid: TPanel;
    PainelControle: TPanel;
    DBGrid1: TDBGrid;
    RGInsumo: TRadioGroup;
    EdPecas: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdValor: TDBEdit;
    BtSaida: TBitBtn;
    DsProdutos: TDataSource;
    QrProdutos: TmySQLQuery;
    QrProduto: TmySQLQuery;
    QrProdutosPRECOMEDIOESTQ: TFloatField;
    QrProdutosPRECOMEDIOINI: TFloatField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    QrProdutosCodigo: TIntegerField;
    QrProdutosGrupo: TIntegerField;
    QrProdutosNome: TWideStringField;
    QrProdutosNome2: TWideStringField;
    QrProdutosPrecoC: TFloatField;
    QrProdutosPrecoV: TFloatField;
    QrProdutosICMS: TFloatField;
    QrProdutosRICMS: TFloatField;
    QrProdutosIPI: TFloatField;
    QrProdutosRIPI: TFloatField;
    QrProdutosFrete: TFloatField;
    QrProdutosJuros: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEIniV: TFloatField;
    QrProdutosEIniQ: TFloatField;
    QrProdutosImprime: TWideStringField;
    QrProdutosLk: TIntegerField;
    QrProdutosDataCad: TDateField;
    QrProdutosDataAlt: TDateField;
    QrProdutosUserCad: TIntegerField;
    QrProdutosUserAlt: TIntegerField;
    QrProdutosControla: TWideStringField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoGrupo: TIntegerField;
    QrProdutoNome: TWideStringField;
    QrProdutoNome2: TWideStringField;
    QrProdutoPrecoC: TFloatField;
    QrProdutoPrecoV: TFloatField;
    QrProdutoICMS: TFloatField;
    QrProdutoRICMS: TFloatField;
    QrProdutoIPI: TFloatField;
    QrProdutoRIPI: TFloatField;
    QrProdutoFrete: TFloatField;
    QrProdutoJuros: TFloatField;
    QrProdutoEstqV: TFloatField;
    QrProdutoEstqQ: TFloatField;
    QrProdutoEIniV: TFloatField;
    QrProdutoEIniQ: TFloatField;
    QrProdutoImprime: TWideStringField;
    QrProdutoLk: TIntegerField;
    QrProdutoDataCad: TDateField;
    QrProdutoDataAlt: TDateField;
    QrProdutoUserCad: TIntegerField;
    QrProdutoUserAlt: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrProdutosCasas: TIntegerField;
    QrProdutosEstqV_G: TFloatField;
    QrProdutosEstqQ_G: TFloatField;
    QrProdutosAlterWeb: TSmallintField;
    QrProdutosAtivo: TSmallintField;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrProdutoCasas: TIntegerField;
    QrProdutoEstqV_G: TFloatField;
    QrProdutoEstqQ_G: TFloatField;
    QrProdutoAlterWeb: TSmallintField;
    QrProdutoAtivo: TSmallintField;
    procedure FormCreate(Sender: TObject);
    procedure RGInsumoClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure QrProdutosCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReindexaTabela(Codigo: Integer);
    //procedure ReopenQrPQ;
  public
    { Public declarations }
  end;

var
  FmProdutosI: TFmProdutosI;

implementation

uses UnMyObjects, Module, ProdutosIEdit;

{$R *.DFM}

(*procedure TFmProdutosI.ReopenQrPQ;
begin
  QrPQ1.Close;
  QrPQ1.Params[0].AsInteger := TbCustosCodigo.Value;
  QrPQ1.Open;
end;*)

procedure TFmProdutosI.ReindexaTabela(Codigo: Integer);
var
  Ordem: String;
begin
  case RGInsumo.ItemIndex of
     0 : Ordem := 'ORDER BY Nome';
     1 : Ordem := 'ORDER BY Codigo';
  end;
  QrProdutos.Close;
  QrProdutos.SQL.Clear;
  QrProdutos.SQL.Add('SELECT * FROM produtos WHERE Codigo>0');
  QrProdutos.SQL.Add(Ordem);
  QrProdutos.Open;
  if Codigo <> 0 then QrProdutos.Locate('Codigo', Codigo, []);
end;

procedure TFmProdutosI.FormCreate(Sender: TObject);
begin
  ReindexaTabela(0);
end;

procedure TFmProdutosI.RGInsumoClick(Sender: TObject);
begin
  ReindexaTabela(QrProdutosCodigo.Value);
end;

procedure TFmProdutosI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProdutosI.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ClickedOK: Boolean;
  Codigo: String;
begin
  if (key=13) then
  begin
    Codigo := FormatFloat('000', QrProdutosCodigo.Value);
    ClickedOK := InputQuery('Edi��o de Produto', 'Informe o c�digo:', Codigo);
    if ClickedOK then
    begin
      QrProduto.Close;
      QrProduto.Params[0].AsString := Codigo;
      QrProduto.Open;
      Application.CreateForm(TFmProdutosIEdit, FmProdutosIEdit);
      with FmProdutosIEdit do
      begin
        LaTipo.Caption := CO_ALTERACAO;
        EdMercadoria.Text := Codigo;
        EdDescricao.Text := QrProdutoNome.Value;
        EdPecas.Text := FloatToStr(QrProdutoEIniQ.Value);
        EdVTota.Text := FloatToStr(QrProdutoEIniV.Value);
        ShowModal;
        Destroy;
      end;
    end;
    UnAcademy.AtualizaEstoqueMercadoria(Geral.IMV(Codigo), aeMsg, CO_VAZIO);
    ReindexaTabela(Geral.IMV(Codigo));
  end;
end;

procedure TFmProdutosI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosI.QrProdutosCalcFields(DataSet: TDataSet);
begin
  if QrProdutosEIniQ.Value = 0 then QrProdutosPRECOMEDIOINI.Value := 0
  else QrProdutosPRECOMEDIOINI.Value :=
  QrProdutosEIniV.Value / QrProdutosEIniQ.Value;
  if QrProdutosEstqQ.Value = 0 then QrProdutosPRECOMEDIOESTQ.Value := 0
  else QrProdutosPRECOMEDIOESTQ.Value :=
  QrProdutosEstqV.Value / QrProdutosEstqQ.Value;
end;

end.

