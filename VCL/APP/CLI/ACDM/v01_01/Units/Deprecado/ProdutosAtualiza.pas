unit ProdutosAtualiza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Db, (*DBTables,*) ExtCtrls, UnMLAGeral, UnGOTOy,
  mySQLDbTables,   
    
  UnInternalConsts, UnitAcademy, dmkGeral;

type
  TFmMercadoriasAtualiza = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    BtRefresh: TBitBtn;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    LaAguarde: TLabel;
    PnTempo: TLabel;
    Label1: TLabel;
    PnRegistros: TLabel;
    Label2: TLabel;
    Progress1: TProgressBar;
    PnMercadoria: TLabel;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCustos: TmySQLQuery;
    QrCustosCodigo: TIntegerField;
    procedure BtRefreshClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RefreshMercadorias;
  end;

var
  FmMercadoriasAtualiza: TFmMercadoriasAtualiza;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmMercadoriasAtualiza.RefreshMercadorias;
var
  TimerIni: TTime;
begin
  QrCustos.Open;
  Progress1.Position := 0;
  Progress1.Max := GOTOy.Registros(QrCustos);
  TimerIni := Now();
  while not QrCustos.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnMercadoria.Caption := IntToStr(QrCustosCodigo.Value);
    PnRegistros.Refresh;
    PnMercadoria.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    UnAcademy.AtualizaEstoqueMercadoria(QrCustosCodigo.Value, aeMsg, CO_VAZIO);
    QrCustos.Next;
  end;
  Timer1.Enabled := False;
  Application.MessageBox('Atualização das mercadorias finalizada.', 'Informação',
  MB_OK+MB_ICONINFORMATION);
end;

procedure TFmMercadoriasAtualiza.BtRefreshClick(Sender: TObject);
begin
  RefreshMercadorias;
end;

procedure TFmMercadoriasAtualiza.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMercadoriasAtualiza.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMercadoriasAtualiza.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
