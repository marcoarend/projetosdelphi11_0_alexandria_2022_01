unit ProdutosG;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UmySQLModule, UnMySQLCuringa, mySQLDbTables, dmkGeral,
  dmkEdit, UnDmkProcFunc;

type
  TFmProdutosG = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    QrProdutosG: TmySQLQuery;
    DsProdutosG: TDataSource;
    QrProdutosGCodigo: TIntegerField;
    QrProdutosGNome: TWideStringField;
    QrProdutosGLk: TIntegerField;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PainelBotoes: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrProdutosGAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrProdutosGAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmProdutosG: TFmProdutosG;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProdutosG.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmProdutosG.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrProdutosGCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProdutosG.DefParams;
begin
  VAR_GOTOTABELA := 'ProdutosG';
  VAR_GOTOmySQLTABLE := QrProdutosG;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM produtosg');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmProdutosG.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Enabled := True;
    DBEdCodigo.Visible := False;
    DBEdNome.Visible := False;
    EdCodigo.Visible := True;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    PainelControle.Visible:=False;
    PainelConfirma.Visible:=True;
    if Status = CO_INCLUSAO then
      EdCodigo.Text := FormatFloat('000', Codigo)
    else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Enabled := False;
    DBEdCodigo.Visible := True;
    DBEdNome.Visible := True;
    EdCodigo.Visible := False;
    EdNome.Visible := False;
    PainelControle.Visible:=True;
    PainelConfirma.Visible:=False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmProdutosG.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProdutosG.AlteraRegistro;
var
  ProdutosG : Integer;
begin
  ProdutosG := QrProdutosGCodigo.Value;
  if QrProdutosGCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(ProdutosG, Dmod.MyDB, 'ProdutosG', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ProdutosG, Dmod.MyDB, 'ProdutosG', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmProdutosG.IncluiRegistro;
var
  Cursor : TCursor;
  ProdutosG : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ProdutosG := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ProdutosG', 'ProdutosG', 'Codigo');;
    MostraEdicao(True, CO_INCLUSAO, ProdutosG);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmProdutosG.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProdutosG.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmProdutosG.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProdutosG.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProdutosG.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProdutosG.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProdutosG.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmProdutosG.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmProdutosG.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProdutosGCodigo.Value;
  Close;
end;

procedure TFmProdutosG.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO produtosg SET ');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, DataCad=:P1, UserCad=:P2, Codigo=:P3');
    Dmod.QrUpdU.SQL.Add('');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE produtosg SET ');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, DataAlt=:P1, UserAlt=:P2 WHERE Codigo=:P3');
  end;
  Dmod.QrUpdU.Params[00].AsString := Nome;
  Dmod.QrUpdU.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnLockY(Codigo, Dmod.MyDB, 'ProdutosG', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmProdutosG.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ProdutosG', Codigo);
  UMyMod.UpdUnLockY(Codigo, Dmod.MyDB, 'ProdutosG', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnLockY(Codigo, Dmod.MyDB, 'ProdutosG', 'Codigo');
end;

procedure TFmProdutosG.FormCreate(Sender: TObject);
begin
  EdCodigo.Left := 16;
  EdNome.Left := 16;
  CriaOForm;
end;

procedure TFmProdutosG.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrProdutosGCodigo.Value,LaRegistro.Caption);
end;

procedure TFmProdutosG.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmProdutosG.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmProdutosG.QrProdutosGAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmProdutosG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProdutosG.QrProdutosGAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrProdutosGCodigo.Value, False);
end;

procedure TFmProdutosG.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProdutosGCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ProdutosG', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProdutosG.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
end;

end.

