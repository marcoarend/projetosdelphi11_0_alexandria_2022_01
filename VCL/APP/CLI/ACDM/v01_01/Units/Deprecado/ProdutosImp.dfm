object FmProdutosImp: TFmProdutosImp
  Left = 337
  Top = 223
  Caption = 'Relat'#243'rios de Produtos'
  ClientHeight = 317
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 377
    Height = 221
    Align = alClient
    TabOrder = 0
    object Label11: TLabel
      Left = 20
      Top = 140
      Width = 91
      Height = 13
      Caption = 'Grupo de produtos:'
    end
    object RGOrdem: TRadioGroup
      Left = 20
      Top = 76
      Width = 85
      Height = 61
      Caption = ' Ordem: '
      ItemIndex = 0
      Items.Strings = (
        'Alfab'#233'tica'
        'Num'#233'rica')
      TabOrder = 0
    end
    object RGGRade: TRadioGroup
      Left = 280
      Top = 76
      Width = 85
      Height = 61
      Caption = ' Grade: '
      ItemIndex = 0
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 2
    end
    object EdGrupo: TdmkEditCB
      Left = 20
      Top = 156
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBGrupo
    end
    object CBGrupo: TdmkDBLookupComboBox
      Left = 84
      Top = 156
      Width = 281
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProdutosG
      TabOrder = 4
      dmkEditCB = EdGrupo
      UpdType = utYes
    end
    object RGAgrupa: TRadioGroup
      Left = 152
      Top = 76
      Width = 85
      Height = 61
      Caption = ' Agrupa: '
      ItemIndex = 0
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 1
    end
    object RGTipo: TRadioGroup
      Left = 20
      Top = 4
      Width = 345
      Height = 65
      Caption = ' Tipo de relat'#243'rio: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Estoque')
      TabOrder = 6
    end
    object CkSomenteComEstoque: TCheckBox
      Left = 20
      Top = 188
      Width = 345
      Height = 17
      Caption = 'Somente mercadorias com estoque.'
      TabOrder = 5
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 269
    Width = 377
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 5
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 274
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 377
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rios de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 375
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 381
      ExplicitHeight = 44
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProdutosCalcFields
    SQL.Strings = (
      'SELECT pr.*, gr.Nome NOMEGRUPO '
      'FROM produtos pr, ProdutosG gr'
      'WHERE gr.Codigo=pr.Grupo'
      'ORDER BY pr.Nome')
    Left = 140
    Top = 28
    object QrProdutosCUSTOMEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTOMEDIO'
      Calculated = True
    end
    object QrProdutosNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'DNSTORE001.produtosg.Nome'
      Size = 14
    end
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProdutosGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProdutosNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrProdutosPrecoC: TFloatField
      FieldName = 'PrecoC'
      Required = True
    end
    object QrProdutosPrecoV: TFloatField
      FieldName = 'PrecoV'
      Required = True
    end
    object QrProdutosICMS: TFloatField
      FieldName = 'ICMS'
      Required = True
    end
    object QrProdutosRICMS: TFloatField
      FieldName = 'RICMS'
      Required = True
    end
    object QrProdutosIPI: TFloatField
      FieldName = 'IPI'
      Required = True
    end
    object QrProdutosRIPI: TFloatField
      FieldName = 'RIPI'
      Required = True
    end
    object QrProdutosFrete: TFloatField
      FieldName = 'Frete'
      Required = True
    end
    object QrProdutosJuros: TFloatField
      FieldName = 'Juros'
      Required = True
    end
    object QrProdutosEstqV: TFloatField
      FieldName = 'EstqV'
      Required = True
    end
    object QrProdutosEstqQ: TFloatField
      FieldName = 'EstqQ'
      Required = True
    end
    object QrProdutosEIniV: TFloatField
      FieldName = 'EIniV'
      Required = True
    end
    object QrProdutosEIniQ: TFloatField
      FieldName = 'EIniQ'
      Required = True
    end
    object QrProdutosImprime: TWideStringField
      FieldName = 'Imprime'
      Required = True
      Size = 1
    end
    object QrProdutosControla: TWideStringField
      FieldName = 'Controla'
      Required = True
      Size = 1
    end
    object QrProdutosCasas: TIntegerField
      FieldName = 'Casas'
      Required = True
    end
    object QrProdutosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProdutosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProdutosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProdutosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProdutosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrProdutosG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM produtosg'
      'ORDER BY Nome')
    Left = 168
    Top = 24
    object QrProdutosGCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DNSTORE001.produtosg.Codigo'
    end
    object QrProdutosGNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DNSTORE001.produtosg.Nome'
      Size = 14
    end
  end
  object DsProdutosG: TDataSource
    DataSet = QrProdutosG
    Left = 196
    Top = 24
  end
  object frxProdutos: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38047.381368611100000000
    ReportOptions.LastChange = 39724.615010127300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GH1, Engine do'
      '  begin'
      '  if VFR_AGRUPA(VFR_AGRUPA) then'
      '  GH1.Visible := True else'
      '  GH1.Visible := False;'
      '  end'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with GF1, Engine do'
      '  begin'
      '  if VFR_AGRUPA(VFR_AGRUPA) then'
      '  GF1.Visible := True else'
      '  GF1.Visible := False;'
      '  end'
      'end;'
      ''
      'procedure Memo9OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo9, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo9.FrameTyp:=15 else'
      '  Memo9.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo10OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo10, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo10.FrameTyp:=15 else'
      '  Memo10.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo11OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo11, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo11.FrameTyp:=15 else'
      '  Memo11.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo12OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo12, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo12.FrameTyp:=15 else'
      '  Memo12.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo13OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo13, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo13.FrameTyp:=15 else'
      '  Memo13.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo15OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo15, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo15.FrameTyp:=15 else'
      '  Memo15.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'procedure Memo17OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  with Memo17, Engine do'
      '  begin'
      '  if VARF_GRADE(VARF_GRADE) then'
      '  Memo17.FrameTyp:=15 else'
      '  Memo17.FrameTyp:=0;'
      '  end'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxProdutosGetValue
    Left = 128
    Top = 68
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsProdutos
        DataSetName = 'frxDsProdutos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 200.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object ReportTitle1: TfrxReportTitle
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.000000000000000000
          Top = 0.661410000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.000000000000000000
          Top = 28.661410000000000000
          Width = 548.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 506.457020000000000000
        Width = 755.906000000000000000
        object Memo31: TfrxMemoView
          Left = 590.000000000000000000
          Top = 7.779219999999950000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 678.000000000000000000
          Top = 7.779219999999950000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = -6.000000000000000000
          Top = 3.779220000000010000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 96.000000000000000000
        Top = 113.385900000000000000
        Width = 755.906000000000000000
        object Line3: TfrxLineView
          Left = -6.000000000000000000
          Top = 22.614100000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo1: TfrxMemoView
          Left = -2.000000000000000000
          Top = 66.614100000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'C'#195#179'digo')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 42.000000000000000000
          Top = 66.614100000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Produto')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 398.000000000000000000
          Top = 66.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Estq.P'#195#167'.')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 462.000000000000000000
          Top = 66.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Estq Val.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 526.000000000000000000
          Top = 66.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Custo m'#195#169'dio')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 590.000000000000000000
          Top = 66.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Pre'#195#167'o V.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 334.000000000000000000
          Top = 66.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Pre'#195#167'o C.')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 654.000000000000000000
          Top = 66.614100000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 50.000000000000000000
          Top = 30.614100000000000000
          Width = 272.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[GRUPO_PRODUTO]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = -2.000000000000000000
          Top = 30.614100000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            'Grupo(s):')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 18.000000000000000000
        Top = 309.921460000000000000
        Width = 755.906000000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSetName = 'frDsProdutos'
        RowCount = 0
        object Memo9: TfrxMemoView
          Left = -2.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsProdutos."Codigo"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 398.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsProdutos."EstqQ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 462.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsProdutos."EstqV"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 526.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsProdutos."CUSTOMEDIO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 590.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsProdutos."PrecoV"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 334.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsProdutos."PrecoC"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 654.000000000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 42.000000000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsProdutos."Nome"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.000000000000000000
        Top = 449.764070000000000000
        Width = 755.906000000000000000
        object Memo14: TfrxMemoView
          Left = 350.000000000000000000
          Top = 4.251699999999970000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Total:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 398.000000000000000000
          Top = 4.251699999999970000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsProdutos."EstqQ">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 462.000000000000000000
          Top = 4.251699999999970000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsProdutos."EstqV">)]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 268.346630000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = '[frxDsProdutos."Grupo"]'
        object Memo21: TfrxMemoView
          Left = -2.000000000000000000
          Width = 720.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsProdutos."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        Height = 36.000000000000000000
        Top = 351.496290000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo22: TfrxMemoView
          Left = 114.000000000000000000
          Top = 4.062770000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Sub-total:')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 398.000000000000000000
          Top = 4.062770000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsProdutos."EstqQ">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 462.000000000000000000
          Top = 4.062770000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsProdutos."EstqV">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 162.000000000000000000
          Top = 4.062770000000000000
          Width = 168.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsProdutos."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object Memo19: TfrxMemoView
        Left = -6.000000000000000000
        Top = 112.000000000000000000
        Width = 732.000000000000000000
        Height = 18.000000000000000000
        ShowHint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8 = (
          '[NOMEREL_002]')
        ParentFont = False
      end
    end
  end
  object frxDsProdutos: TfrxDBDataset
    UserName = 'frxDsProdutos'
    CloseDataSource = False
    DataSet = QrProdutos
    BCDToCurrency = False
    Left = 156
    Top = 68
  end
end
