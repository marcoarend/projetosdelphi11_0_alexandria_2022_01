object FmProdutosBalEdit: TFmProdutosBalEdit
  Left = 426
  Top = 332
  Caption = 'Edi'#231#227'o de Estoques de Mercadorias'
  ClientHeight = 146
  ClientWidth = 612
  Color = clBtnFace
  Constraints.MaxHeight = 535
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 612
    Height = 58
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 12
      Width = 56
      Height = 13
      Caption = 'Mercadoria:'
    end
    object Label4: TLabel
      Left = 312
      Top = 12
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label10: TLabel
      Left = 400
      Top = 12
      Width = 67
      Height = 13
      Caption = 'Custo unit'#225'rio:'
    end
    object Label6: TLabel
      Left = 496
      Top = 12
      Width = 53
      Height = 13
      Caption = 'Custo total:'
    end
    object EdMercadoria: TdmkEdit
      Left = 12
      Top = 28
      Width = 85
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdPecas: TdmkEdit
      Left = 312
      Top = 28
      Width = 85
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdPecasExit
    end
    object EdPreco: TdmkEdit
      Left = 400
      Top = 28
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdPrecoExit
    end
    object EdVTota: TdmkEdit
      Left = 496
      Top = 28
      Width = 109
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdVTotaExit
    end
    object EdDescricao: TEdit
      Left = 100
      Top = 28
      Width = 205
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 1
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 98
    Width = 612
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Left = 515
      Top = 8
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 612
    Height = 40
    Align = alTop
    Caption = 'Edi'#231#227'o de Estoques de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 533
      Top = 1
      Width = 78
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 540
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 532
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 538
      ExplicitHeight = 36
    end
  end
end
