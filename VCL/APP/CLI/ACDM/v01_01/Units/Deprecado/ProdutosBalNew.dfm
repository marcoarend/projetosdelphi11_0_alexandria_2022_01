object FmProdutosBalNew: TFmProdutosBalNew
  Left = 476
  Top = 253
  Caption = 'Novo Per'#237'odo de Balan'#231'o'
  ClientHeight = 172
  ClientWidth = 333
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 40
    Width = 333
    Height = 84
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 341
    ExplicitHeight = 88
    object Label2: TLabel
      Left = 24
      Top = 28
      Width = 92
      Height = 13
      Caption = 'M'#234's do movimento:'
    end
    object Label3: TLabel
      Left = 216
      Top = 28
      Width = 91
      Height = 13
      Caption = 'Ano do movimento:'
    end
    object CBMes: TComboBox
      Left = 25
      Top = 45
      Width = 182
      Height = 21
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 0
      ParentFont = False
      TabOrder = 0
      Text = 'CBMes'
    end
    object CBAno: TComboBox
      Left = 215
      Top = 45
      Width = 95
      Height = 21
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 0
      ParentFont = False
      TabOrder = 1
      Text = 'CBAno'
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 333
    Height = 40
    Align = alTop
    Caption = 'Novo Per'#237'odo de Balan'#231'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 341
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 337
      Height = 36
      Align = alClient
      Transparent = True
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 124
    Width = 333
    Height = 48
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 128
    ExplicitWidth = 341
    object BtConfirma: TBitBtn
      Left = 24
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF5555555555555A655555
        55555555588FF55555555555AAA65555555555558888F55555555555AAA65555
        555555558888FF555555555AAAAA65555555555888888F55555555AAAAAA6555
        5555558888888FF5555552AA65AAA65555555888858888F555552A65555AA655
        55558885555888FF55555555555AAA65555555555558888F555555555555AA65
        555555555555888FF555555555555AA65555555555555888FF555555555555AA
        65555555555555888FF555555555555AA65555555555555888FF555555555555
        5AA6555555555555588855555555555555555555555555555555}
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Left = 219
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333FFFFF3333333333999999333333333FFF88888F3333339999999999
        333333FF88833388833339993385839993333FF8883F83338833999333555339
        99333F8833F8883338839933338583339933FF83333383333883333333333333
        3993F883333F33333F883333333533333993383333FF83333F88333333858333
        3993333333F888333F883333331513333993FFFFF3F888333F88999993555333
        3993F88888F88833FF889999335553339933F88883F8883FF383999333555339
        9933F888F3F888FF38839999938583999333F88388F383F38833933999999999
        3333383388833388833333333999993333333333338888833333}
      NumGlyphs = 2
    end
  end
  object QrLocPeriodo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Periodo '
      'FROM balancos'
      'WHERE Periodo=:P0')
    Left = 128
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPeriodoPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
end
