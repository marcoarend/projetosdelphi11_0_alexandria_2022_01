unit MatriculasSit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UnMLAGeral,
     
     ComCtrls, Grids,
  DBGrids, UnMsgInt, 
    mySQLDbTables, frxClass;

type
  TFmMatriculasSit = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    Panel3: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    TPIni: TDateTimePicker;
    Label1: TLabel;
    TPFim: TDateTimePicker;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Ck0: TCheckBox;
    Ck1: TCheckBox;
    Ck2: TCheckBox;
    Ck3: TCheckBox;
    Ck4: TCheckBox;
    Ck5: TCheckBox;
    RGOrdem: TRadioGroup;
    CkAgrup: TCheckBox;
    CkGrade: TCheckBox;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrRenova: TmySQLQuery;
    QrRenovaCodigo: TIntegerField;
    QrRenovaAluno: TIntegerField;
    QrRenovaDataC: TDateField;
    QrRenovaDataI: TDateField;
    QrRenovaDataF: TDateField;
    QrRenovaValor: TFloatField;
    QrRenovaRenovacao: TSmallintField;
    QrRenovaAnterior: TIntegerField;
    QrRenovaProxima: TIntegerField;
    QrRenovaLk: TIntegerField;
    QrRenovaDataCad: TDateField;
    QrRenovaDataAlt: TDateField;
    QrRenovaUserCad: TIntegerField;
    QrRenovaUserAlt: TIntegerField;
    QrRenovaProxDataC: TDateField;
    QrRenovaAntDataC: TDateField;
    QrRenovaNOMEALUNO: TWideStringField;
    QrRenovaRENOVASTATUS: TLargeintField;
    QrRenovaNOMERENOVACAO: TWideStringField;
    DataSource1: TDataSource;
    frxRenova: TfrxReport;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frRenovaGetValue(const ParName: String;
      var ParValue: Variant);
    procedure frRenovaUserFunction(const Name: String; p1, p2, p3: Variant;
      var Val: Variant);
    procedure QrRenovaCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxRenovaGetValue(const VarName: String; var Value: Variant);
  private
    { Private declarations }
    function VerificaSituacoes: Integer;
  public
    { Public declarations }
  end;

var
  FmMatriculasSit: TFmMatriculasSit;
  Fechar : Boolean;

implementation

uses UnMyObjects, Module;


{$R *.DFM}

procedure TFmMatriculasSit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriculasSit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMatriculasSit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriculasSit.BtImprimeClick(Sender: TObject);
var
  Situacao, Ou, Ordem: String;
  Sit0, Sit1, Sit2, Sit3, Sit4, Sit5: String;
begin

  case RGOrdem.ItemIndex of
    0: Ordem := 'ORDER BY RENOVASTATUS, en.Nome';
    1: Ordem := 'ORDER BY en.Nome, RENOVASTATUS';
  end;
  //
  Ou := CO_VAZIO;
  Sit4 := '((ma1.Proxima>0) AND (ma2.DataC> ma1.DataF))';
  Sit5 := '((ma1.Proxima>0) AND (ma2.DataC<=ma1.DataF))';
  Sit2 := '((ma1.Proxima=0) AND (ma1.Renovacao=1) AND (NOW()<ma1.DataF))';
  Sit1 := '((ma1.Proxima=0) AND (ma1.Renovacao=1) AND (NOW()>=ma1.DataF))';
  Sit3 := '((ma1.Proxima=0) AND (ma1.Renovacao=3) AND (NOW()<ma1.DataF))';
  Sit0 := '((ma1.Proxima=0) AND (ma1.Renovacao=3) AND (NOW()>=ma1.DataF))';
  //
  if Ck0.Checked then Situacao := Situacao + Sit0;
  if Length(Situacao) > 0 then ou := ' OR ';
  //
  if Ck1.Checked then Situacao := Situacao + Ou + Sit1;
  if Length(Situacao) > 0 then ou := ' OR ';
  //
  if Ck2.Checked then Situacao := Situacao + Ou + Sit2;
  if Length(Situacao) > 0 then ou := ' OR ';
  //
  if Ck3.Checked then Situacao := Situacao + Ou + Sit3;
  if Length(Situacao) > 0 then ou := ' OR ';
  //
  if Ck4.Checked then Situacao := Situacao + Ou + Sit4;
  if Length(Situacao) > 0 then ou := ' OR ';
  //
  if Ck5.Checked then Situacao := Situacao + Ou + Sit5;
  //
  if Length(Situacao) = 0 then
  begin
    Application.MessageBox('Informe pelo menos uma "Situa��o de Renova��o"',
    'Situa��o de Renova��o Ausente', MB_OK+MB_ICONEXCLAMATION);
    Exit;
  end;
  QrRenova.Close;
  QrRenova.SQL.Clear;
  QrRenova.SQL.Add('SELECT ma1.*, ma2.DataC ProxDataC, ma0.DataC AntDataC, en.Nome NOMEALUNO,');
  QrRenova.SQL.Add('CASE 1 ');
  QrRenova.SQL.Add('       WHEN (ma1.Proxima=0) AND (ma1.Renovacao=1) AND (NOW()<ma1.DataF)    THEN 2');
  QrRenova.SQL.Add('       WHEN (ma1.Proxima=0) AND (ma1.Renovacao=1) AND (NOW()>=ma1.DataF)   THEN 1');
  QrRenova.SQL.Add('       WHEN (ma1.Proxima=0) AND (ma1.Renovacao=3) AND (NOW()<ma1.DataF)    THEN 3');
  QrRenova.SQL.Add('       WHEN (ma1.Proxima=0) AND (ma1.Renovacao=3) AND (NOW()>=ma1.DataF)   THEN 0');
  QrRenova.SQL.Add('       WHEN (ma1.Proxima>0) AND (ma2.DataC> ma1.DataF)                     THEN 4');
  QrRenova.SQL.Add('       WHEN (ma1.Proxima>0) AND (ma2.DataC<=ma1.DataF)                     THEN 5');
  QrRenova.SQL.Add('       ELSE -1 END RENOVASTATUS');
  QrRenova.SQL.Add('FROM matriculas ma1');
  QrRenova.SQL.Add('LEFT JOIN matriculas ma0 ON ma0.Codigo=ma1.Anterior');
  QrRenova.SQL.Add('LEFT JOIN matriculas ma2 ON ma2.Codigo=ma1.Proxima');
  QrRenova.SQL.Add('LEFT JOIN entidades en   ON en.Codigo=ma1.Aluno');
  QrRenova.SQL.Add('WHERE  ma1.DataF BETWEEN :P0 AND :P1');
  QrRenova.SQL.Add('       AND (');
  QrRenova.SQL.Add(Situacao);
  QrRenova.SQL.Add(')');
  QrRenova.SQL.Add(ORDEM);
  QrRenova.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrRenova.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //MLAGeral.LeMySQL(QrRenova);
  QrRenova.Open;
  ////
  frRenova.PrepareReport;
  frRenova.ShowPreparedReport;
  QrRenova.Close;
end;

procedure TFmMatriculasSit.frRenovaGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if ParName = 'PERIODO' then ParValue :=
      FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
  //
  if ParName = 'NOMEREL_001' then ParValue := 'SITUA��O DAS MATR�CULAS';
  //
  if ParName = 'VFR_LA1NOME' then
  begin
    case RGOrdem.ItemIndex of
      0: ParValue := QrRenovaNOMERENOVACAO.Value;
      1: ParValue := QrRenovaNOMEALUNO.Value;
      else ParValue := 'ERRO - VFR_LA1NOME';
    end;
  end;
  //
  if ParName = 'SITUACOES' then
  begin
    if VerificaSituacoes < 6 then
    ParValue := 'Parcial' else ParValue := 'Todas';
  end;
end;

procedure TFmMatriculasSit.frRenovaUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if Name = 'VFR_GRADE' then
    if CkGrade.Checked then Val := True else Val := False;
  //
  if Name = 'VFR_ORD' then
    if CkAgrup.Checked then Val := RGOrdem.ItemIndex else Val := -1;
end;

function TFmMatriculasSit.VerificaSituacoes: Integer;
begin
  Result := 0;
  if Ck0.Checked then Result := Result + 1;
  if Ck1.Checked then Result := Result + 1;
  if Ck2.Checked then Result := Result + 1;
  if Ck3.Checked then Result := Result + 1;
  if Ck4.Checked then Result := Result + 1;
  if Ck5.Checked then Result := Result + 1;
end;

procedure TFmMatriculasSit.QrRenovaCalcFields(DataSet: TDataSet);
begin
  if QrRenovaRENOVASTATUS.Value = 0
  then QrRenovaNOMERENOVACAO.Value := 'Vencida'
  else if QrRenovaRENOVASTATUS.Value = 1
  then  QrRenovaNOMERENOVACAO.Value := 'N�o ser� renovada'
  else if QrRenovaRENOVASTATUS.Value = 2
  then QrRenovaNOMERENOVACAO.Value := 'Terminada e n�o ser� renovada'
  else if QrRenovaRENOVASTATUS.Value = 3
  then QrRenovaNOMERENOVACAO.Value := 'Ser� renovada quando vencer'
  else if QrRenovaRENOVASTATUS.Value = 4
  then QrRenovaNOMERENOVACAO.Value := 'J� foi renovada antecipadamente'
  else if QrRenovaRENOVASTATUS.Value = 5
  then QrRenovaNOMERENOVACAO.Value := 'Renovada'
  else QrRenovaNOMERENOVACAO.Value := '* ERRO NO APLICATIVO *';
end;

procedure TFmMatriculasSit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmMatriculasSit.FormCreate(Sender: TObject);
begin
  TPIni.Date := Date;
  TPFim.Date := Date + 30;
end;

procedure TFmMatriculasSit.frxRenovaGetValue(const VarName: String;
  var Value: Variant);
begin
  if ParName = 'PERIODO' then ParValue :=
      FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
      FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
  //
  if ParName = 'NOMEREL_001' then ParValue := 'SITUA��O DAS MATR�CULAS';
  //
  if ParName = 'VFR_LA1NOME' then
  begin
    case RGOrdem.ItemIndex of
      0: ParValue := QrRenovaNOMERENOVACAO.Value;
      1: ParValue := QrRenovaNOMEALUNO.Value;
      else ParValue := 'ERRO - VFR_LA1NOME';
    end;
  end;
  //
  if ParName = 'SITUACOES' then
  begin
    if VerificaSituacoes < 6 then
    ParValue := 'Parcial' else ParValue := 'Todas';
  end;

  if Name = 'VFR_GRADE' then
    if CkGrade.Checked then Val := True else Val := False;
  //
  if Name = 'VFR_ORD' then
    if CkAgrup.Checked then Val := RGOrdem.ItemIndex else Val := -1;
end;

end.

