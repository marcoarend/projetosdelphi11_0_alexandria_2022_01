unit MatrRefresh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Db, mySQLDbTables;

type
  TFmMatriRefresh = class(TForm)
    Progress: TProgressBar;
    QrMatricul: TmySQLQuery;
    QrMatriculCodigo: TIntegerField;
    QrMatriculAtivo: TSmallintField;
    Timer1: TTimer;
    QrMatriRen: TmySQLQuery;
    QrMatriRenCodigo: TIntegerField;
    QrMatriRenControle: TIntegerField;
    QrMatriRenDataF: TDateField;
    procedure Timer1Timer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMatriRefresh: TFmMatriRefresh;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmMatriRefresh.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  QrMatricul.Close;
  QrMatricul.Open;
  Progress.Position := 0;
  Progress.Max := QrMatricul.RecordCount;
  while not QrMatricul.Eof do
  begin
    Progress.Position := Progress.Position + 1;
    Progress.Update;
    QrMatriren.Close;
    QrMatriren.Params[0].AsInteger := QrMatriculCodigo.Value;
    QrMatriren.Open;
    if QrMatriRen.RecordCount > 0 then
    begin
      if QrMatriculAtivo.Value = 0 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE MatriRen SET Ativo=0 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrMatriRenCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
      end else begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE MatriRen SET Ativo=0 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle<>:P1');
        Dmod.QrUpd.Params[0].AsInteger := QrMatriRenCodigo.Value;
        Dmod.QrUpd.Params[1].AsInteger := QrMatriRenControle.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE MatriRen SET Ativo=1 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1');
        Dmod.QrUpd.Params[0].AsInteger := QrMatriRenCodigo.Value;
        Dmod.QrUpd.Params[1].AsInteger := QrMatriRenControle.Value;
        Dmod.QrUpd.ExecSQL;
        //
      end;
    end;
    QrMatricul.Next;
  end;
  Close;
end;

procedure TFmMatriRefresh.FormActivate(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

end.
