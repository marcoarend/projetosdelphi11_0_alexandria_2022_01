unit SWMS_Mtng_PrvUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkDBEdit, dmkRadioGroup, UnDmkEnums;

type
  TFmSWMS_Mtng_PrvUni = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelEdit: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    EdControle: TdmkEdit;
    GroupBox4: TGroupBox;
    RGMetros: TdmkRadioGroup;
    Panel3: TPanel;
    Label29: TLabel;
    EdCodTxt: TdmkEdit;
    CkContinuar: TCheckBox;
    GroupBox5: TGroupBox;
    EdPiscDescr: TdmkEdit;
    Label1: TLabel;
    EdPiscCompr: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdPiscRaias: TdmkEdit;
    Panel5: TPanel;
    RGEstilo: TdmkRadioGroup;
    Panel6: TPanel;
    Label27: TLabel;
    EdCategoriaIni: TdmkEdit;
    EdCategoriaIni_TXT: TdmkEdit;
    Label28: TLabel;
    EdCategoriaFim: TdmkEdit;
    EdCategoriaFim_TXT: TdmkEdit;
    EdSexo: TdmkEdit;
    EdSexo_Txt: TdmkEdit;
    Label4: TLabel;
    RGPiorRaia: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCategoriaIniChange(Sender: TObject);
    procedure EdCategoriaFimChange(Sender: TObject);
    procedure EdCategoriaFim_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaFimKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaIniKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCategoriaIni_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSexoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmSWMS_Mtng_PrvUni: TFmSWMS_Mtng_PrvUni;

implementation

uses SWMS_Mtng_Cab, Module, Principal, UnMyObjects, UMySQLModule, UnitListas;

{$R *.DFM}

procedure TFmSWMS_Mtng_PrvUni.BtOKClick(Sender: TObject);
const
  tag = ' x ';
var
  Controle, p, m, d, n: Integer;
  x: String;
begin
  if MyObjects.FIC(Trim(EdCodTxt.Text) = '', EdCodTxt,
    'Informe o c�digo da prova') then
    Exit;
  if MyObjects.FIC((EdSexo.Text <> 'A') and (EdSexo.Text <> 'F') and
    (EdSexo.Text <> 'M'), EdSexo,
    'Informe o sexo!') then
    Exit;
  if MyObjects.FIC(RGEstilo.ItemIndex < 1, RGEstilo,
    'Informe o estilo!') then
    Exit;
  if MyObjects.FIC(RGMetros.ItemIndex < 1, RGMetros,
    'Informe a dist�ncia!') then
    Exit;
  if MyObjects.FIC(Trim(EdPiscDescr.Text) = '', EdPiscDescr,
    'Informe a descri��o da piscina') then
    Exit;
  d := EdPiscCompr.ValueVariant;
  if MyObjects.FIC(d < 1, EdPiscCompr,
    'Informe o comprimento em metros da piscina!') then
    Exit;
  if MyObjects.FIC(EdPiscRaias.ValueVariant < 1, EdPiscRaias,
    'Informe a quantidade de raias da piscina!') then
    Exit;
  if MyObjects.FIC(EdCategoriaIni.ValueVariant < 1, EdCategoriaIni,
    'Informe a categoria inicial!') then
    Exit;
  if MyObjects.FIC(RGPiorRaia.ItemIndex < 0, RGPiorRaia,
    'Informe a pior raia da piscina!') then
    Exit;
  if MyObjects.FIC(EdCategoriaFim.ValueVariant < 1, EdCategoriaFim,
    'Informe a categoria final!') then
    Exit;
  if MyObjects.FIC(EdCategoriaFim.ValueVariant < EdCategoriaIni.ValueVariant, EdCategoriaFim,
    'A categoria final deve ser igual ou superior � inicial!') then
    Exit;
  //
  x := RGMetros.Items[RGMetros.ItemIndex];
  p := pos(tag, x);
  if p > 0 then
  begin
    x := Copy(x, p + Length(tag));
    m := Geral.IMV(Trim(x));
    if m > 0 then
    begin
      n := m mod Trunc(d);
      if MyObjects.FIC(n <> 0, nil,
      'Comprimento da piscina n�o comporta a dist�ncia selecionada!') then
      Exit;
    end;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, MSG_SAVE_SQL);
  Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'swms_mtng_prv', 'Controle',
  [], [], ImgTipo.SQLType, FMSWMS_Mtng_Cab.QrSWMS_Mtng_PrvControle.Value,
  siPositivo, EdControle);
  //
  if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'swms_mtng_prv',
  Controle, Dmod.QrUpd) then
  begin
    FMSWMS_Mtng_Cab.ReopenSWMS_Mtng_Prv(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdControle.ValueVariant   := 0;
      EdCodTxt.ValueVariant     := '';
      EdSexo.Text               := '';
      RGEstilo.ItemIndex        := 0;
      RGMetros.ItemIndex        := 0;
      EdCategoriaIni.ValueVariant := 0;
      EdCategoriaFim.ValueVariant := 0;
      EdCodTxt.SetFocus;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Close;
  end;
end;

procedure TFmSWMS_Mtng_PrvUni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSWMS_Mtng_PrvUni.EdCategoriaFimChange(Sender: TObject);
begin
  EdCategoriaFim_TXT.Text :=
    UnListas.CategoriasNatacao_Get(Geral.IMV(EdCategoriaFim.Text));
end;

procedure TFmSWMS_Mtng_PrvUni.EdCategoriaFimKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaFim.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvUni.EdCategoriaFim_TXTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaFim.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvUni.EdCategoriaIniChange(Sender: TObject);
begin
  EdCategoriaIni_TXT.Text :=
    UnListas.CategoriasNatacao_Get(Geral.IMV(EdCategoriaIni.Text));
end;

procedure TFmSWMS_Mtng_PrvUni.EdCategoriaIniKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaIni.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvUni.EdCategoriaIni_TXTKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdCategoriaIni.Text := UnListas.CategoriasNatacao_MontaLista();
end;

procedure TFmSWMS_Mtng_PrvUni.EdSexoChange(Sender: TObject);
begin
  if EdSexo.Text = 'A' then
    EdSexo_Txt.Text := 'Ambos'
  else
  if EdSexo.Text = 'F' then
    EdSexo_Txt.Text := 'Feminino'
  else
  if EdSexo.Text = 'M' then
    EdSexo_Txt.Text := 'Masculino'
  else
    EdSexo_Txt.Text := '? ? ? ? ?';
end;

procedure TFmSWMS_Mtng_PrvUni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, MSG_FORM_SHOW);
end;

procedure TFmSWMS_Mtng_PrvUni.FormCreate(Sender: TObject);
begin
  MyObjects.ConfiguraRadioGroup(RGEstilo, sListaEstilosNatacao, 4, 0);
  MyObjects.ConfiguraRadioGroup(RGMetros, sListaMetrosNatacao, 6, 0);
  //MyObjects.ConfiguraRadioGroup(RGCategoria, sListaCategoriasNatacao, 6, 0);
  //
end;

procedure TFmSWMS_Mtng_PrvUni.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
