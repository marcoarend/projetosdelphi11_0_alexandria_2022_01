unit MatriTur;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) ExtDlgs,
  ResIntStrings, UnInternalConsts, UnInternalConsts2, UnMLAGeral, ComCtrls,
  Grids, DBGrids, UnMsgInt, mySQLDbTables, dmkEditCB, dmkEdit,
  dmkDBLookupComboBox, dmkLabel, dmkGeral, UnDmkEnums;

type
  TFmMatriTur = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    BtDesiste: TBitBtn;
    BtConfirma: TBitBtn;
    Label2: TLabel;
    EdTurma: TdmkEditCB;
    CBTurma: TdmkDBLookupComboBox;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrTurmas: TmySQLQuery;
    DsTurmas: TDataSource;
    Label3: TLabel;
    EdFator: TdmkEdit;
    QrSoma: TmySQLQuery;
    QrSomaFator: TFloatField;
    Label4: TLabel;
    EdControle: TdmkEdit;
    CkAdicionar: TCheckBox;
    QrTurmasIts: TmySQLQuery;
    QrTurmasItsControle: TIntegerField;
    QrTurmasTmp: TmySQLQuery;
    QrTurmasTmpConta: TIntegerField;
    QrTurmasCodigo: TIntegerField;
    QrTurmasNome: TWideStringField;
    QrTurmasID: TWideStringField;
    QrTurmasGrupo: TIntegerField;
    QrTurmasAtiva: TSmallintField;
    QrTurmasTotalPeso: TFloatField;
    QrTurmasRespons: TIntegerField;
    QrTurmasLotacao: TIntegerField;
    QrTurmasLk: TIntegerField;
    QrTurmasDataCad: TDateField;
    QrTurmasDataAlt: TDateField;
    QrTurmasUserCad: TIntegerField;
    QrTurmasUserAlt: TIntegerField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    SpeedButton1: TSpeedButton;
    LaTipo: TdmkLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMatriTur: TFmMatriTur;
  Fechar : Boolean;

implementation

uses UnMyObjects, Module, Principal, Matricul, UMySQLModule, MyDBCheck;


{$R *.DFM}

procedure TFmMatriTur.BtConfirmaClick(Sender: TObject);
var
  Codigo, Controle, Conta, Turma, Item, SubItem: Integer;
begin
  Codigo   := Geral.IMV(EdCodigo.Text);
  Controle := Geral.IMV(EdControle.Text);
  Turma    := Geral.IMV(EdTurma.Text);
  //
  if MyObjects.FIC(Codigo < 1, nil, 'Matricula n�o informada!') then Exit;
  if MyObjects.FIC(Controle < 1, nil, 'Renova��o n�o informada!') then Exit;
  if MyObjects.FIC(Turma < 1, nil, 'Informe a Turma!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'MatriTur', 'MatriTur', 'Codigo');
    //
    Dmod.QrUpdU.SQL.Clear;
    //
    if LaTipo.Caption = CO_INCLUSAO then
      Dmod.QrUpdU.SQL.Add('INSERT INTO MatriTur SET ')
    else
      Dmod.QrUpdU.SQL.Add('UPDATE      MatriTur SET ');
    //
    Dmod.QrUpdU.SQL.Add('  Fator       =:P0');
    Dmod.QrUpdU.SQL.Add(', Turma       =:P1');
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpdU.SQL.Add(', Codigo     =:Pa');
      Dmod.QrUpdU.SQL.Add(', Controle   =:Pb');
      Dmod.QrUpdU.SQL.Add(', Conta      =:Pc');
    end else
    begin
      Dmod.QrUpdU.SQL.Add('WHERE Codigo =:Pa');
      Dmod.QrUpdU.SQL.Add('AND Controle =:Pb');
      Dmod.QrUpdU.SQL.Add('AND Conta    =:Pc');
    end;
    Dmod.QrUpdU.Params[0].AsFloat   := Geral.DMV(EdFator.Text);
    Dmod.QrUpdU.Params[1].AsInteger := Turma;
    Dmod.QrUpdU.Params[2].AsInteger := Codigo;
    Dmod.QrUpdU.Params[3].AsInteger := Controle;
    Dmod.QrUpdU.Params[4].AsInteger := Conta;
    Dmod.QrUpdU.ExecSQL;
    //
    QrSoma.Close;
    QrSoma.Params[0].AsInteger := Codigo;
    QrSoma.Open;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE MatriRen SET TotalFator =:P0 WHERE Codigo=:P1');
    Dmod.QrUpdU.Params[0].AsFloat   := QrSomaFator.Value;
    Dmod.QrUpdU.Params[1].AsInteger := Controle;
    Dmod.QrUpdU.ExecSQL;
    //
    if CkAdicionar.Checked then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO MatriAti SET ');
      Dmod.QrUpd.SQL.Add('Atividade=:P0, Codigo=:P1, Controle=:P2, Conta=:P3, Item=:P4');
      //
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('INSERT INTO MatriPer SET ');
      Dmod.QrUpdM.SQL.Add('Periodo=:P0, Codigo=:P1, Controle=:P2, Conta=:P3, Item=:P4, SubItem=:P5');
      //
      QrTurmasIts.Close;
      QrTurmasIts.Params[0].AsInteger := Turma;
      QrTurmasIts.Open;
      while not QrTurmasIts.Eof do
      begin
        Item := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                  'MatriAti', 'MatriAti', 'Codigo');
        //
        Dmod.QrUpd.Params[00].AsInteger := QrTurmasItsControle.Value;
        Dmod.QrUpd.Params[01].AsInteger := Codigo;
        Dmod.QrUpd.Params[02].AsInteger := Controle;
        Dmod.QrUpd.Params[03].AsInteger := Conta;
        Dmod.QrUpd.Params[04].AsInteger := Item;
        Dmod.QrUpd.ExecSQL;
        //////////////////////////////////////////////////////////////////////////
        QrTurmasTmp.Close;
        QrTurmasTmp.Params[0].AsInteger := Turma;
        QrTurmasTmp.Params[1].AsInteger := QrTurmasItsControle.Value;
        QrTurmasTmp.Open;
        while not QrTurmasTmp.Eof do
        begin
          SubItem := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                    'MatriPer', 'MatriPer', 'Codigo');
          //
          Dmod.QrUpdM.Params[00].AsInteger := QrTurmasTmpConta.Value;
          Dmod.QrUpdM.Params[01].AsInteger := Codigo;
          Dmod.QrUpdM.Params[02].AsInteger := Controle;
          Dmod.QrUpdM.Params[03].AsInteger := Conta;
          Dmod.QrUpdM.Params[04].AsInteger := Item;
          Dmod.QrUpdM.Params[05].AsInteger := SubItem;
          Dmod.QrUpdM.ExecSQL;
          QrTurmasTmp.Next;
        end;
        //////////////////////////////////////////////////////////////////////////
        QrTurmasIts.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmMatriTur.BtDesisteClick(Sender: TObject);
begin
  FmMatricul.LaTipo.Caption := CO_TRAVADO;
  Close;
end;

procedure TFmMatriTur.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdTurma.SetFocus;
end;

procedure TFmMatriTur.FormCreate(Sender: TObject);
begin
  QrTurmas.Open;
end;

procedure TFmMatriTur.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmMatriTur.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroTurmas(EdTurma.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTurma, CBTurma, QrTurmas, VAR_CADASTRO);
    EdTurma.SetFocus;
  end;
end;

end.
