object FmSWMS_Mtng_Cab: TFmSWMS_Mtng_Cab
  Left = 368
  Top = 194
  Caption = 'MET-GEREN-001 :: Gerenciamento de Competi'#231#245'es'
  ClientHeight = 852
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 64
    Width = 1241
    Height = 788
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBRodaPe2: TGroupBox
      Left = 0
      Top = 716
      Width = 1241
      Height = 72
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object PainelConfirma: TPanel
        Left = 2
        Top = 11
        Width = 1237
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 10
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object Panel1: TPanel
          Left = 1104
          Top = 0
          Width = 133
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 9
            Top = 2
            Width = 110
            Height = 50
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object EdSincronia: TdmkEdit
          Left = 202
          Top = 18
          Width = 98
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 2
          Visible = False
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdCampo = 'Sincronia'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
      end
    end
    object GBAviso0304: TGroupBox
      Left = 0
      Top = 646
      Width = 1241
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 2
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso4: TLabel
          Left = 16
          Top = 2
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso3: TLabel
          Left = 15
          Top = 1
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 306
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Dados da competi'#231#227'o: '
      TabOrder = 0
      object Panel10: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 210
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox9: TGroupBox
          Left = 960
          Top = 0
          Width = 271
          Height = 210
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Ordem das provas: '
          TabOrder = 0
          object RGOrdProva1: TdmkRadioGroup
            Left = 2
            Top = 18
            Width = 267
            Height = 47
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Ordem 1: '
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Estilo'
              'Dist.'
              'Categ.'
              'Sexo')
            TabOrder = 0
            QryCampo = 'OrdProva1'
            UpdCampo = 'OrdProva1'
            UpdType = utYes
            OldValor = 0
          end
          object RGOrdProva2: TdmkRadioGroup
            Left = 2
            Top = 65
            Width = 267
            Height = 47
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Ordem 2: '
            Columns = 4
            ItemIndex = 1
            Items.Strings = (
              'Estilo'
              'Dist.'
              'Categ.'
              'Sexo')
            TabOrder = 1
            QryCampo = 'OrdProva2'
            UpdCampo = 'OrdProva2'
            UpdType = utYes
            OldValor = 0
          end
          object RGOrdProva3: TdmkRadioGroup
            Left = 2
            Top = 112
            Width = 267
            Height = 47
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Ordem 3: '
            Columns = 4
            ItemIndex = 2
            Items.Strings = (
              'Estilo'
              'Dist.'
              'Categ.'
              'Sexo')
            TabOrder = 2
            QryCampo = 'OrdProva3'
            UpdCampo = 'OrdProva3'
            UpdType = utYes
            OldValor = 0
          end
          object RGOrdProva4: TdmkRadioGroup
            Left = 2
            Top = 159
            Width = 267
            Height = 47
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Ordem 4: '
            Columns = 4
            ItemIndex = 3
            Items.Strings = (
              'Estilo'
              'Dist.'
              'Categ.'
              'Sexo')
            TabOrder = 3
            QryCampo = 'OrdProva4'
            UpdCampo = 'OrdProva4'
            UpdType = utYes
            OldValor = 0
          end
        end
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 960
          Height = 210
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label7: TLabel
            Left = 10
            Top = 5
            Width = 16
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID:'
          end
          object Label8: TLabel
            Left = 84
            Top = 5
            Width = 47
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label9: TLabel
            Left = 286
            Top = 5
            Width = 128
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Descri'#231#227'o do evento:'
          end
          object Label4: TLabel
            Left = 10
            Top = 54
            Width = 192
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Local no qual ocorrer'#225' o evento:'
          end
          object Label5: TLabel
            Left = 10
            Top = 103
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
          end
          object Label6: TLabel
            Left = 153
            Top = 103
            Width = 70
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Hora inicial:'
          end
          object Label10: TLabel
            Left = 226
            Top = 103
            Width = 59
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
          end
          object Label11: TLabel
            Left = 369
            Top = 103
            Width = 60
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Hora final:'
          end
          object Label12: TLabel
            Left = 443
            Top = 103
            Width = 179
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pessoa de contato do evento:'
          end
          object Label13: TLabel
            Left = 812
            Top = 103
            Width = 123
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Telefone de contato:'
          end
          object Label34: TLabel
            Left = 10
            Top = 153
            Width = 107
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'E-mail do contato:'
          end
          object EdCodigo: TdmkEdit
            Left = 10
            Top = 25
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCodUsu: TdmkEdit
            Left = 84
            Top = 25
            Width = 197
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CodUsu'
            UpdCampo = 'CodUsu'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNome: TdmkEdit
            Left = 286
            Top = 25
            Width = 664
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLocal: TdmkEdit
            Left = 10
            Top = 74
            Width = 940
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Local'
            UpdCampo = 'Local'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPDataEvenIni: TdmkEditDateTimePicker
            Left = 10
            Top = 123
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40718.731625057880000000
            Time = 40718.731625057880000000
            TabOrder = 4
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DataEvenIni'
            UpdCampo = 'DataEvenIni'
            UpdType = utYes
          end
          object EdHoraEvenIni: TdmkEdit
            Left = 153
            Top = 123
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 5
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            QryCampo = 'HoraEvenIni'
            UpdCampo = 'HoraEvenIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object TPDataEvenFim: TdmkEditDateTimePicker
            Left = 226
            Top = 123
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40718.731625057880000000
            Time = 40718.731625057880000000
            TabOrder = 6
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DataEvenFim'
            UpdCampo = 'DataEvenFim'
            UpdType = utYes
          end
          object EdHoraEvenFim: TdmkEdit
            Left = 369
            Top = 123
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 7
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            QryCampo = 'HoraEvenFim'
            UpdCampo = 'HoraEvenFim'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdContatoNome: TdmkEdit
            Left = 443
            Top = 123
            Width = 364
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ContatoNome'
            UpdCampo = 'ContatoNome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContatoFone: TdmkEdit
            Left = 812
            Top = 123
            Width = 138
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtTelLongo
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ContatoFone'
            UpdCampo = 'ContatoFone'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContatoMail: TdmkEdit
            Left = 10
            Top = 172
            Width = 940
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'ContatoMail'
            UpdCampo = 'ContatoMail'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object Panel12: TPanel
        Left = 2
        Top = 228
        Width = 1237
        Height = 76
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object GroupBox7: TGroupBox
          Left = 1
          Top = 1
          Width = 862
          Height = 74
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Dados das inscri'#231#245'es: '
          TabOrder = 0
          object Label35: TLabel
            Left = 15
            Top = 20
            Width = 69
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
          end
          object Label36: TLabel
            Left = 158
            Top = 20
            Width = 59
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
          end
          object Label23: TLabel
            Left = 300
            Top = 20
            Width = 54
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Passe C:'
          end
          object Label29: TLabel
            Left = 389
            Top = 20
            Width = 38
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CNPJ:'
            FocusControl = EdCNPJ_EMP
          end
          object Label27: TLabel
            Left = 551
            Top = 20
            Width = 125
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Categoria inicial [F3]:'
          end
          object Label28: TLabel
            Left = 704
            Top = 20
            Width = 115
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Categoria final [F3]:'
          end
          object EdPasseC: TdmkEdit
            Left = 300
            Top = 39
            Width = 84
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SenhaC'
            UpdCampo = 'SenhaC'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPDtInscrIni: TdmkEditDateTimePicker
            Left = 15
            Top = 41
            Width = 138
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40718.731625057880000000
            Time = 40718.731625057880000000
            TabOrder = 0
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtInscrIni'
            UpdCampo = 'DtInscrIni'
            UpdType = utYes
          end
          object TpDtInscrFim: TdmkEditDateTimePicker
            Left = 158
            Top = 41
            Width = 137
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40718.731625057880000000
            Time = 40718.731625057880000000
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'DtInscrFim'
            UpdCampo = 'DtInscrFim'
            UpdType = utYes
          end
          object EdCNPJ_EMP: TdmkEdit
            Left = 389
            Top = 39
            Width = 157
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CNPJ_EMP'
            UpdCampo = 'CNPJ_EMP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCategoriaIni: TdmkEdit
            Left = 551
            Top = 39
            Width = 30
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CategoriaIni'
            UpdCampo = 'CategoriaIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCategoriaIniChange
            OnKeyDown = EdCategoriaIniKeyDown
          end
          object EdCategoriaIni_TXT: TdmkEdit
            Left = 581
            Top = 39
            Width = 118
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Nenhuma'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Nenhuma'
            ValWarn = False
            OnKeyDown = EdCategoriaIni_TXTKeyDown
          end
          object EdCategoriaFim: TdmkEdit
            Left = 704
            Top = 39
            Width = 30
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CategoriaFim'
            UpdCampo = 'CategoriaFim'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCategoriaFimChange
            OnKeyDown = EdCategoriaFimKeyDown
          end
          object EdCategoriaFim_TXT: TdmkEdit
            Left = 734
            Top = 39
            Width = 118
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ReadOnly = True
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = 'Nenhuma'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 'Nenhuma'
            ValWarn = False
            OnKeyDown = EdCategoriaFim_TXTKeyDown
          end
        end
        object RGFases: TdmkRadioGroup
          Left = 961
          Top = 1
          Width = 271
          Height = 74
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Fases: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Classificat'#243'rio'
            'Com Finais')
          TabOrder = 1
          QryCampo = 'Fases'
          UpdCampo = 'Fases'
          UpdType = utYes
          OldValor = 0
        end
        object GroupBox10: TGroupBox
          Left = 863
          Top = 1
          Width = 98
          Height = 74
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' '
          TabOrder = 2
          object CkAtivo: TdmkCheckBox
            Left = 15
            Top = 25
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ativo.'
            Checked = True
            State = cbChecked
            TabOrder = 0
            QryCampo = 'Ativo'
            UpdCampo = 'Ativo'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '2'
            OldValor = #0
          end
        end
      end
    end
    object Panel19: TPanel
      Left = 0
      Top = 306
      Width = 1241
      Height = 276
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 3
      object GroupBox8: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 276
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Observa'#231#245'es (m'#225'x 255 caracteres por linha): '
        TabOrder = 0
        object EdObserv_0: TdmkEdit
          Left = 15
          Top = 20
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_0'
          UpdCampo = 'Observ_0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_1: TdmkEdit
          Left = 15
          Top = 44
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_1'
          UpdCampo = 'Observ_1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_2: TdmkEdit
          Left = 15
          Top = 69
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_2'
          UpdCampo = 'Observ_2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_3: TdmkEdit
          Left = 15
          Top = 94
          Width = 935
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_3'
          UpdCampo = 'Observ_3'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_4: TdmkEdit
          Left = 15
          Top = 118
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_4'
          UpdCampo = 'Observ_4'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_5: TdmkEdit
          Left = 15
          Top = 143
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_5'
          UpdCampo = 'Observ_5'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_6: TdmkEdit
          Left = 15
          Top = 167
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_6'
          UpdCampo = 'Observ_6'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_7: TdmkEdit
          Left = 15
          Top = 192
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_7'
          UpdCampo = 'Observ_7'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_8: TdmkEdit
          Left = 15
          Top = 217
          Width = 935
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_8'
          UpdCampo = 'Observ_8'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdObserv_9: TdmkEdit
          Left = 15
          Top = 241
          Width = 935
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 255
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Observ_9'
          UpdCampo = 'Observ_9'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox12: TGroupBox
        Left = 965
        Top = 0
        Width = 276
        Height = 276
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Estilos: '
        TabOrder = 1
        object Label32: TLabel
          Left = 15
          Top = 44
          Width = 32
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Livre:'
        end
        object Label33: TLabel
          Left = 15
          Top = 74
          Width = 45
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Costas:'
        end
        object Label37: TLabel
          Left = 15
          Top = 103
          Width = 34
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Peito:'
        end
        object Label38: TLabel
          Left = 15
          Top = 133
          Width = 62
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Borboleta:'
        end
        object Label39: TLabel
          Left = 15
          Top = 162
          Width = 48
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Medley:'
        end
        object Label40: TLabel
          Left = 15
          Top = 192
          Width = 73
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Revez. livre:'
        end
        object Label41: TLabel
          Left = 15
          Top = 222
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Revez. estilos:'
        end
        object Label42: TLabel
          Left = 108
          Top = 20
          Width = 44
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ordem:'
        end
        object Label43: TLabel
          Left = 172
          Top = 20
          Width = 63
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Fator ptos.'
        end
        object EdOrdem1: TdmkEdit
          Left = 108
          Top = 39
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdOrdem2: TdmkEdit
          Left = 108
          Top = 69
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdOrdem3: TdmkEdit
          Left = 108
          Top = 98
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdOrdem4: TdmkEdit
          Left = 108
          Top = 128
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdOrdem5: TdmkEdit
          Left = 108
          Top = 158
          Width = 59
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdOrdem6: TdmkEdit
          Left = 108
          Top = 187
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdOrdem7: TdmkEdit
          Left = 108
          Top = 217
          Width = 59
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdFator1: TdmkEdit
          Left = 172
          Top = 39
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdFator2: TdmkEdit
          Left = 172
          Top = 69
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdFator3: TdmkEdit
          Left = 172
          Top = 98
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdFator4: TdmkEdit
          Left = 172
          Top = 128
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 10
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdFator5: TdmkEdit
          Left = 172
          Top = 158
          Width = 59
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object EdFator6: TdmkEdit
          Left = 172
          Top = 187
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 12
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 2
          ValWarn = False
        end
        object EdFator7: TdmkEdit
          Left = 172
          Top = 217
          Width = 59
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 13
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 2
          ValWarn = False
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 64
    Width = 1241
    Height = 788
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    object GBAviso0102: TGroupBox
      Left = 0
      Top = 646
      Width = 1241
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel8: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 15
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 10
          Top = 23
          Width = 1217
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 79
      Width = 1241
      Height = 547
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 0
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Informa'#231#245'es da competi'#231#227'o'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1233
          Height = 516
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox4: TGroupBox
            Left = 0
            Top = 0
            Width = 1233
            Height = 247
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Dados da competi'#231#227'o: '
            TabOrder = 0
            object Panel7: TPanel
              Left = 961
              Top = 18
              Width = 270
              Height = 227
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object GroupBox11: TGroupBox
                Left = 0
                Top = 0
                Width = 270
                Height = 227
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Ordem das Provas: '
                TabOrder = 0
                object DBRadioGroup1: TDBRadioGroup
                  Left = 2
                  Top = 18
                  Width = 266
                  Height = 41
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Ordem 1: '
                  Columns = 4
                  DataField = 'OrdProva1'
                  DataSource = DsSWMS_Mtng_Cab
                  Items.Strings = (
                    'Estilo'
                    'Dist.'
                    'Categ.'
                    'Sexo')
                  ParentBackground = True
                  TabOrder = 0
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                end
                object DBRadioGroup2: TDBRadioGroup
                  Left = 2
                  Top = 59
                  Width = 266
                  Height = 41
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Ordem 2: '
                  Columns = 4
                  DataField = 'OrdProva2'
                  DataSource = DsSWMS_Mtng_Cab
                  Items.Strings = (
                    'Estilo'
                    'Dist.'
                    'Categ.'
                    'Sexo')
                  ParentBackground = True
                  TabOrder = 1
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                end
                object DBRadioGroup3: TDBRadioGroup
                  Left = 2
                  Top = 100
                  Width = 266
                  Height = 40
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Ordem 3: '
                  Columns = 4
                  DataField = 'OrdProva3'
                  DataSource = DsSWMS_Mtng_Cab
                  Items.Strings = (
                    'Estilo'
                    'Dist.'
                    'Categ.'
                    'Sexo')
                  ParentBackground = True
                  TabOrder = 2
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                end
                object DBRadioGroup4: TDBRadioGroup
                  Left = 2
                  Top = 140
                  Width = 266
                  Height = 41
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Ordem 4: '
                  Columns = 4
                  DataField = 'OrdProva4'
                  DataSource = DsSWMS_Mtng_Cab
                  Items.Strings = (
                    'Estilo'
                    'Dist.'
                    'Categ.'
                    'Sexo')
                  ParentBackground = True
                  TabOrder = 3
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                end
                object DBRadioGroup5: TDBRadioGroup
                  Left = 2
                  Top = 181
                  Width = 266
                  Height = 44
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = ' Fases: '
                  Columns = 2
                  DataField = 'Fases'
                  DataSource = DsSWMS_Mtng_Cab
                  Items.Strings = (
                    'Classificat'#243'rio'
                    'Com Finais')
                  ParentBackground = True
                  TabOrder = 4
                  Values.Strings = (
                    '0'
                    '1')
                end
              end
            end
            object Panel11: TPanel
              Left = 2
              Top = 18
              Width = 959
              Height = 227
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Panel18: TPanel
                Left = 0
                Top = 0
                Width = 960
                Height = 227
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox1: TGroupBox
                  Left = 694
                  Top = 154
                  Width = 256
                  Height = 73
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = ' Sincroniza'#231#227'o com o Webservice: '
                  TabOrder = 0
                  object LaSincro1: TLabel
                    Left = 16
                    Top = 27
                    Width = 220
                    Height = 32
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'N'#227'o Sincronizado'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clSilver
                    Font.Height = -28
                    Font.Name = 'Arial'
                    Font.Style = []
                    ParentFont = False
                    Transparent = True
                    Visible = False
                  end
                  object LaSincro3: TLabel
                    Left = 15
                    Top = 26
                    Width = 220
                    Height = 32
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'N'#227'o Sincronizado'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -28
                    Font.Name = 'Arial'
                    Font.Style = []
                    ParentFont = False
                    Transparent = True
                    Visible = False
                  end
                end
                object GroupBox5: TGroupBox
                  Left = 0
                  Top = 154
                  Width = 694
                  Height = 73
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = ' Dados das inscri'#231#245'es: '
                  TabOrder = 1
                  object Label25: TLabel
                    Left = 15
                    Top = 20
                    Width = 69
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Data inicial:'
                  end
                  object Label26: TLabel
                    Left = 98
                    Top = 20
                    Width = 59
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Data final:'
                  end
                  object Label14: TLabel
                    Left = 261
                    Top = 20
                    Width = 38
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'CNPJ:'
                  end
                  object Label24: TLabel
                    Left = 182
                    Top = 20
                    Width = 54
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Passe C:'
                  end
                  object Label30: TLabel
                    Left = 404
                    Top = 20
                    Width = 99
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Categoria inicial:'
                  end
                  object Label31: TLabel
                    Left = 546
                    Top = 20
                    Width = 89
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Categoria final:'
                  end
                  object DBEdit20: TDBEdit
                    Left = 15
                    Top = 39
                    Width = 79
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'DtInscrIni'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 0
                  end
                  object DBEdit21: TDBEdit
                    Left = 98
                    Top = 39
                    Width = 79
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'DtInscrFim'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 1
                  end
                  object DBEdit22: TDBEdit
                    Left = 261
                    Top = 39
                    Width = 138
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'CNPJ_EMP_TXT'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 2
                  end
                  object DBEdit23: TDBEdit
                    Left = 182
                    Top = 39
                    Width = 74
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'SenhaC'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 3
                  end
                  object DBEdCategoriaIni_TXT: TdmkEdit
                    Left = 433
                    Top = 39
                    Width = 109
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 4
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = 'Nenhuma'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 'Nenhuma'
                    ValWarn = False
                  end
                  object DBEdCategoriaFim_TXT: TdmkEdit
                    Left = 576
                    Top = 39
                    Width = 108
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 5
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = 'Nenhuma'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 'Nenhuma'
                    ValWarn = False
                  end
                  object DBEdit24: TDBEdit
                    Left = 404
                    Top = 39
                    Width = 29
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'CategoriaIni'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 6
                    OnChange = DBEdit24Change
                  end
                  object DBEdit25: TDBEdit
                    Left = 546
                    Top = 39
                    Width = 30
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'CategoriaFim'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 7
                    OnChange = DBEdit25Change
                  end
                end
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 960
                  Height = 154
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 2
                  object Label1: TLabel
                    Left = 10
                    Top = 5
                    Width = 33
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Local'
                    FocusControl = DBEdit2
                  end
                  object Label2: TLabel
                    Left = 10
                    Top = 54
                    Width = 69
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Data inicial:'
                  end
                  object Label18: TLabel
                    Left = 153
                    Top = 54
                    Width = 70
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Hora inicial:'
                  end
                  object Label22: TLabel
                    Left = 226
                    Top = 54
                    Width = 59
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Data final:'
                  end
                  object Label19: TLabel
                    Left = 369
                    Top = 54
                    Width = 60
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Hora final:'
                  end
                  object Label20: TLabel
                    Left = 443
                    Top = 54
                    Width = 229
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Pessoa de contato no local do evento:'
                  end
                  object Label21: TLabel
                    Left = 812
                    Top = 54
                    Width = 123
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Telefone de contato:'
                  end
                  object Label3: TLabel
                    Left = 10
                    Top = 103
                    Width = 107
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'E-mail do contato:'
                  end
                  object DBEdit2: TDBEdit
                    Left = 10
                    Top = 25
                    Width = 940
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'Local'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 0
                  end
                  object DBEdit3: TDBEdit
                    Left = 10
                    Top = 74
                    Width = 138
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'DataEvenIni'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 1
                  end
                  object DBEdit4: TDBEdit
                    Left = 153
                    Top = 74
                    Width = 69
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'HoraEvenIni'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 2
                  end
                  object DBEdit6: TDBEdit
                    Left = 226
                    Top = 74
                    Width = 138
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'DataEvenFim'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 3
                  end
                  object DBEdit5: TDBEdit
                    Left = 369
                    Top = 74
                    Width = 69
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'HoraEvenFim'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 4
                  end
                  object DBEdit7: TDBEdit
                    Left = 443
                    Top = 74
                    Width = 364
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'ContatoNome'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 5
                  end
                  object DBEdit8: TDBEdit
                    Left = 812
                    Top = 74
                    Width = 138
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'ContatoFone_TXT'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 6
                  end
                  object DBEdit19: TDBEdit
                    Left = 10
                    Top = 123
                    Width = 940
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'ContatoNome'
                    DataSource = DsSWMS_Mtng_Cab
                    TabOrder = 7
                  end
                end
              end
            end
          end
          object Panel20: TPanel
            Left = 0
            Top = 247
            Width = 1233
            Height = 269
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 953
              Height = 269
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' Observa'#231#245'es (m'#225'x 255 caracteres por linha): '
              TabOrder = 0
              object DBEdit18: TDBEdit
                Left = 10
                Top = 241
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_9'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 0
              end
              object DBEdit17: TDBEdit
                Left = 10
                Top = 217
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_8'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 1
              end
              object DBEdit16: TDBEdit
                Left = 10
                Top = 192
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_7'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 2
              end
              object DBEdit15: TDBEdit
                Left = 10
                Top = 167
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_6'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 3
              end
              object DBEdit14: TDBEdit
                Left = 10
                Top = 143
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_5'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 4
              end
              object DBEdit13: TDBEdit
                Left = 10
                Top = 118
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_4'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 5
              end
              object DBEdit12: TDBEdit
                Left = 10
                Top = 94
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_3'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 6
              end
              object DBEdit11: TDBEdit
                Left = 10
                Top = 69
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_2'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 7
              end
              object DBEdit10: TDBEdit
                Left = 10
                Top = 44
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_1'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 8
              end
              object DBEdit9: TDBEdit
                Left = 10
                Top = 20
                Width = 935
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Observ_0'
                DataSource = DsSWMS_Mtng_Cab
                TabOrder = 9
              end
            end
            object Panel21: TPanel
              Left = 953
              Top = 0
              Width = 280
              Height = 269
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label44: TLabel
                Left = 0
                Top = 0
                Width = 40
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                Caption = 'Estilos'
              end
              object DBGrid2: TDBGrid
                Left = 0
                Top = 16
                Width = 280
                Height = 253
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsSWMS_Mtng_Sty
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -15
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Ordem'
                    Title.Caption = 'Ord.'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_ESTILO'
                    Title.Caption = 'Estilo'
                    Width = 130
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FatorPtos'
                    Title.Caption = 'Fator'
                    Width = 28
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Regulamento '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBMemo1: TDBMemo
          Left = 0
          Top = 0
          Width = 1233
          Height = 110
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataField = 'Regulam'
          DataSource = DsRegulam
          TabOrder = 0
          Visible = False
        end
        object WebBrowser1: TWebBrowser
          Left = 0
          Top = 110
          Width = 1233
          Height = 406
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 1250
          ExplicitHeight = 410
          ControlData = {
            4C000000F3650000922100000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Equipes'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 370
          Height = 516
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          ParentBackground = False
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 1
            Top = 1
            Width = 368
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsSWMS_Mtng_Its
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Nome da equipe'
                Width = 106
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SenhaP'
                Title.Caption = 'Passe P'
                Width = 54
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora_TXT'
                Title.Caption = 'Data/Hora incri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CNPJ_PAR_TXT'
                Title.Caption = 'CNPJ'
                Width = 113
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IP'
                Width = 88
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Protocol'
                Title.Caption = 'Recibo (Protocolo)'
                Visible = True
              end>
          end
        end
        object PageControl3: TPageControl
          Left = 370
          Top = 0
          Width = 863
          Height = 516
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet8
          Align = alClient
          TabOrder = 1
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Participa'#231#245'es '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid3: TDBGrid
              Left = 0
              Top = 85
              Width = 855
              Height = 400
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsCompetiPart
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_NADADOR'
                  Title.Caption = 'Nome do atleta'
                  Width = 218
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CATEGORIA'
                  Title.Caption = 'Categoria'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ESTILO'
                  Title.Caption = 'Estilo'
                  Width = 89
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_METROS'
                  Title.Caption = 'Dist'#226'ncia'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tempo_TXT'
                  Title.Caption = 'Tempo'
                  Width = 75
                  Visible = True
                end>
            end
            object GroupBox14: TGroupBox
              Left = 0
              Top = 0
              Width = 855
              Height = 85
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' Filtro: '
              TabOrder = 1
              object Panel24: TPanel
                Left = 2
                Top = 18
                Width = 851
                Height = 65
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label47: TLabel
                  Left = 409
                  Top = 10
                  Width = 62
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Categoria:'
                end
                object Label46: TLabel
                  Left = 10
                  Top = 10
                  Width = 40
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Nome:'
                end
                object Label48: TLabel
                  Left = 10
                  Top = 39
                  Width = 36
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Estilo:'
                end
                object Label49: TLabel
                  Left = 409
                  Top = 39
                  Width = 59
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Dist'#226'ncia:'
                end
                object EdFiltroPartCateg: TEdit
                  Left = 473
                  Top = 10
                  Width = 365
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  OnChange = EdFiltroPartNomeChange
                end
                object EdFiltroPartNome: TEdit
                  Left = 54
                  Top = 5
                  Width = 351
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 1
                  OnChange = EdFiltroPartNomeChange
                end
                object EdFiltroPartEstilo: TEdit
                  Left = 54
                  Top = 34
                  Width = 351
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  OnChange = EdFiltroPartNomeChange
                end
                object EdFiltroPartMetros: TEdit
                  Left = 473
                  Top = 34
                  Width = 365
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 3
                  OnChange = EdFiltroPartNomeChange
                end
              end
            end
          end
          object TabSheet9: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Participantes'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid5: TDBGrid
              Left = 0
              Top = 55
              Width = 855
              Height = 430
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsNadadores
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CodTxt'
                  Title.Caption = 'C'#243'digo'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CPF_TXT'
                  Title.Caption = 'CPF'
                  Width = 84
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 228
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'Sexo'
                  Width = 30
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nascim'
                  Title.Caption = 'Nascimento'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Fone_TXT'
                  Title.Caption = 'Telefone'
                  Width = 112
                  Visible = True
                end>
            end
            object GroupBox13: TGroupBox
              Left = 0
              Top = 0
              Width = 855
              Height = 55
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' Filtro: '
              TabOrder = 1
              object Panel23: TPanel
                Left = 2
                Top = 18
                Width = 851
                Height = 100
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label45: TLabel
                  Left = 10
                  Top = 10
                  Width = 40
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Nome:'
                end
                object EdFiltroNadNome: TEdit
                  Left = 54
                  Top = 5
                  Width = 784
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  OnChange = EdFiltroNadNomeChange
                end
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Provas'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl2: TPageControl
          Left = 631
          Top = 0
          Width = 602
          Height = 516
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Fase Classificat'#243'ria'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 594
              Height = 379
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              DataSource = DsFaseA
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -15
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid4DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SerieA'
                  Title.Caption = 'S'#233'rie'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'RaiaA'
                  Title.Caption = 'Raia'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_NADADOR'
                  Title.Caption = 'Nome do atleta'
                  Width = 236
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TEMPO_STR'
                  Title.Caption = 'T. informado'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TEMPOA_STR'
                  Title.Caption = 'T. tomado'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_EQUIPE'
                  Title.Caption = 'Nome da equipe'
                  Width = 236
                  Visible = True
                end>
            end
            object MeLista: TMemo
              Left = 0
              Top = 379
              Width = 594
              Height = 106
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -17
              Font.Name = 'Courier New'
              Font.Style = []
              Lines.Strings = (
                'MeLista')
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet7: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Final'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 631
          Height = 516
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GradeProvas: TdmkDBGrid
            Left = 0
            Top = 26
            Width = 631
            Height = 490
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'SEQ'
                Title.Caption = 'Prova'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_METROS'
                Title.Caption = 'Dist'#226'ncia'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ESTILO'
                Title.Caption = 'Estilo'
                Width = 88
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Sexo'
                Title.Alignment = taCenter
                Title.Caption = 'G'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PiscDescr'
                Title.Caption = 'Piscina'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PiscCompr'
                Title.Caption = 'm'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PiscRaias'
                Title.Caption = 'R'
                Width = 16
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CATEGORIAINI'
                Title.Caption = 'Categoria inicial'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CATEGORIAFIM'
                Title.Caption = 'Categoria final'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodTxt'
                Title.Caption = 'Identifica'#231#227'o'
                Width = 68
                Visible = True
              end>
            Color = clWindow
            DataSource = DsSWMS_Mtng_Prv
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'SEQ'
                Title.Caption = 'Prova'
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_METROS'
                Title.Caption = 'Dist'#226'ncia'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ESTILO'
                Title.Caption = 'Estilo'
                Width = 88
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Sexo'
                Title.Alignment = taCenter
                Title.Caption = 'G'
                Width = 20
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PiscDescr'
                Title.Caption = 'Piscina'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PiscCompr'
                Title.Caption = 'm'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PiscRaias'
                Title.Caption = 'R'
                Width = 16
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CATEGORIAINI'
                Title.Caption = 'Categoria inicial'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CATEGORIAFIM'
                Title.Caption = 'Categoria final'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodTxt'
                Title.Caption = 'Identifica'#231#227'o'
                Width = 68
                Visible = True
              end>
          end
          object Panel15: TPanel
            Left = 0
            Top = 0
            Width = 631
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
          end
        end
      end
    end
    object GBRodaPe1: TGroupBox
      Left = 0
      Top = 716
      Width = 1241
      Height = 72
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object PainelControle: TPanel
        Left = 2
        Top = 11
        Width = 1237
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 212
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 158
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 108
            Top = 5
            Width = 50
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 59
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 10
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object Panel3: TPanel
          Left = 587
          Top = 0
          Width = 650
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object BtProva: TBitBtn
            Left = 231
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Prova'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtProvaClick
          end
          object BtIts: TBitBtn
            Left = 118
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Equipes'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtItsClick
          end
          object BtCab: TBitBtn
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Competi'#231#227'o'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtCabClick
          end
          object Panel2: TPanel
            Left = 516
            Top = 0
            Width = 134
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 3
            object BtSaida: TBitBtn
              Tag = 13
              Left = 5
              Top = 5
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object Button1: TButton
            Left = 409
            Top = 20
            Width = 92
            Height = 30
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Button1'
            TabOrder = 4
            Visible = False
            OnClick = Button1Click
          end
          object Edpart: TdmkEdit
            Left = 354
            Top = 5
            Width = 46
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 5
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '15'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 15
            ValWarn = False
          end
          object EdRais: TdmkEdit
            Left = 354
            Top = 30
            Width = 46
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 6
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '4'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 4
            ValWarn = False
          end
        end
        object GBReg: TGroupBox
          Left = 212
          Top = 0
          Width = 375
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' [N]: '
          TabOrder = 2
          object LaReg1: TLabel
            Left = 6
            Top = 22
            Width = 15
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaReg3: TLabel
            Left = 5
            Top = 21
            Width = 15
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Evento: '
      TabOrder = 2
      object Panel6: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label15: TLabel
          Left = 10
          Top = 5
          Width = 16
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
        end
        object Label16: TLabel
          Left = 84
          Top = 5
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
        end
        object Label17: TLabel
          Left = 286
          Top = 5
          Width = 128
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o do evento:'
        end
        object DBEdNome: TdmkDBEdit
          Left = 286
          Top = 25
          Width = 664
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsSWMS_Mtng_Cab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 84
          Top = 25
          Width = 197
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CodUsu'
          DataSource = DsSWMS_Mtng_Cab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 10
          Top = 25
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsSWMS_Mtng_Cab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taRightJustify
        end
      end
    end
  end
  object PnTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 458
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Competi'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 458
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Competi'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 458
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        Caption = 'Gerenciamento de Competi'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsSWMS_Mtng_Cab: TDataSource
    DataSet = QrSWMS_Mtng_Cab
    Left = 40
    Top = 12
  end
  object QrSWMS_Mtng_Cab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSWMS_Mtng_CabBeforeOpen
    AfterOpen = QrSWMS_Mtng_CabAfterOpen
    BeforeClose = QrSWMS_Mtng_CabBeforeClose
    AfterScroll = QrSWMS_Mtng_CabAfterScroll
    OnCalcFields = QrSWMS_Mtng_CabCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM swms_mtng_cab')
    Left = 16
    Top = 12
    object QrSWMS_Mtng_CabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSWMS_Mtng_CabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSWMS_Mtng_CabCNPJ_EMP: TWideStringField
      FieldName = 'CNPJ_EMP'
      Size = 32
    end
    object QrSWMS_Mtng_CabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSWMS_Mtng_CabLocal: TWideStringField
      FieldName = 'Local'
      Size = 255
    end
    object QrSWMS_Mtng_CabDtInscrIni: TDateField
      FieldName = 'DtInscrIni'
    end
    object QrSWMS_Mtng_CabDtInscrFim: TDateField
      FieldName = 'DtInscrFim'
    end
    object QrSWMS_Mtng_CabDataEvenIni: TDateField
      FieldName = 'DataEvenIni'
    end
    object QrSWMS_Mtng_CabHoraEvenIni: TTimeField
      FieldName = 'HoraEvenIni'
    end
    object QrSWMS_Mtng_CabDataEvenFim: TDateField
      FieldName = 'DataEvenFim'
    end
    object QrSWMS_Mtng_CabHoraEvenFim: TTimeField
      FieldName = 'HoraEvenFim'
    end
    object QrSWMS_Mtng_CabSenhaC: TWideStringField
      FieldName = 'SenhaC'
      Size = 6
    end
    object QrSWMS_Mtng_CabContatoNome: TWideStringField
      FieldName = 'ContatoNome'
      Size = 100
    end
    object QrSWMS_Mtng_CabContatoFone: TWideStringField
      FieldName = 'ContatoFone'
    end
    object QrSWMS_Mtng_CabContatoMail: TWideStringField
      FieldName = 'ContatoMail'
      Size = 255
    end
    object QrSWMS_Mtng_CabRegulam: TWideMemoField
      FieldName = 'Regulam'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSWMS_Mtng_CabXML_Resul: TWideMemoField
      FieldName = 'XML_Resul'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSWMS_Mtng_CabDtHrResul: TDateTimeField
      FieldName = 'DtHrResul'
    end
    object QrSWMS_Mtng_CabObserv_0: TWideStringField
      FieldName = 'Observ_0'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_1: TWideStringField
      FieldName = 'Observ_1'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_2: TWideStringField
      FieldName = 'Observ_2'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_3: TWideStringField
      FieldName = 'Observ_3'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_4: TWideStringField
      FieldName = 'Observ_4'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_5: TWideStringField
      FieldName = 'Observ_5'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_6: TWideStringField
      FieldName = 'Observ_6'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_7: TWideStringField
      FieldName = 'Observ_7'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_8: TWideStringField
      FieldName = 'Observ_8'
      Size = 255
    end
    object QrSWMS_Mtng_CabObserv_9: TWideStringField
      FieldName = 'Observ_9'
      Size = 255
    end
    object QrSWMS_Mtng_CabContatoFone_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ContatoFone_TXT'
      Size = 40
      Calculated = True
    end
    object QrSWMS_Mtng_CabSincronia: TSmallintField
      FieldName = 'Sincronia'
    end
    object QrSWMS_Mtng_CabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSWMS_Mtng_CabCNPJ_EMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_EMP_TXT'
      Size = 30
      Calculated = True
    end
    object QrSWMS_Mtng_CabCategoriaIni: TIntegerField
      FieldName = 'CategoriaIni'
    end
    object QrSWMS_Mtng_CabCategoriaFim: TIntegerField
      FieldName = 'CategoriaFim'
    end
    object QrSWMS_Mtng_CabOrdProva1: TIntegerField
      FieldName = 'OrdProva1'
    end
    object QrSWMS_Mtng_CabOrdProva2: TIntegerField
      FieldName = 'OrdProva2'
    end
    object QrSWMS_Mtng_CabOrdProva3: TIntegerField
      FieldName = 'OrdProva3'
    end
    object QrSWMS_Mtng_CabOrdProva4: TIntegerField
      FieldName = 'OrdProva4'
    end
    object QrSWMS_Mtng_CabFases: TSmallintField
      FieldName = 'Fases'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = DBEdit16
    CanUpd01 = DBEdit17
    CanDel01 = DBEdit18
    Left = 68
    Top = 12
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 512
    Top = 612
    object Incluinovacompeticao1: TMenuItem
      Caption = '&Inclui nova competi'#231#227'o'
      OnClick = Incluinovacompeticao1Click
    end
    object Alteracompeticaoatual1: TMenuItem
      Caption = '&Altera competi'#231#227'o atual'
      Enabled = False
      OnClick = Alteracompeticaoatual1Click
    end
    object Excluicompeticaoatual1: TMenuItem
      Caption = 'E&xclui competi'#231#227'o atual'
      Enabled = False
      OnClick = Excluicompeticaoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Regulamento1: TMenuItem
      Caption = '&Regulamento'
      Enabled = False
      object Edita1: TMenuItem
        Caption = '&Altera'
        OnClick = Edita1Click
      end
      object Limpa1: TMenuItem
        Caption = '&Deleta'
        OnClick = Limpa1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Sincronizar1: TMenuItem
      Caption = '&Sincronizar'
      Enabled = False
      OnClick = Sincronizar1Click
    end
    object Estilos1: TMenuItem
      Caption = 'Estil&os'
      Enabled = False
    end
  end
  object QrSWMS_Mtng_Its: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSWMS_Mtng_ItsBeforeClose
    AfterScroll = QrSWMS_Mtng_ItsAfterScroll
    OnCalcFields = QrSWMS_Mtng_ItsCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM swms_mtng_its'
      'WHERE Codigo=:P0')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSWMS_Mtng_ItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'swms_mtng_its.Codigo'
    end
    object QrSWMS_Mtng_ItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'swms_mtng_its.Controle'
    end
    object QrSWMS_Mtng_ItsSenhaP: TWideStringField
      FieldName = 'SenhaP'
      Origin = 'swms_mtng_its.SenhaP'
      Size = 6
    end
    object QrSWMS_Mtng_ItsCNPJ_PAR: TWideStringField
      FieldName = 'CNPJ_PAR'
      Origin = 'swms_mtng_its.CNPJ_PAR'
      Size = 32
    end
    object QrSWMS_Mtng_ItsIP: TWideStringField
      FieldName = 'IP'
      Origin = 'swms_mtng_its.IP'
    end
    object QrSWMS_Mtng_ItsProtocol: TWideStringField
      FieldName = 'Protocol'
      Origin = 'swms_mtng_its.Protocol'
    end
    object QrSWMS_Mtng_ItsXML: TWideMemoField
      FieldName = 'XML'
      Origin = 'swms_mtng_its.XML'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSWMS_Mtng_ItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'swms_mtng_its.DataHora'
    end
    object QrSWMS_Mtng_ItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSWMS_Mtng_ItsDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrSWMS_Mtng_ItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSWMS_Mtng_ItsCNPJ_PAR_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_PAR_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsSWMS_Mtng_Its: TDataSource
    DataSet = QrSWMS_Mtng_Its
    Left = 124
    Top = 12
  end
  object PMIts: TPopupMenu
    Left = 604
    Top = 612
    object Incluinovaequipe1: TMenuItem
      Caption = '&Inclui nova equipe'
      OnClick = Incluinovaequipe1Click
    end
    object Excluiequipeselecionada1: TMenuItem
      Caption = '&Exclui equipe selecionada'
      OnClick = Excluiequipeselecionada1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Sincronizar2: TMenuItem
      Caption = '&Sincronizar'
      OnClick = Sincronizar2Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Participaes2: TMenuItem
      Caption = 'Par&ticipa'#231#245'es'
      object Inclui2: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui2Click
      end
      object Altera2: TMenuItem
        Caption = '&Altera'
        OnClick = Altera2Click
      end
    end
    object Participantes1: TMenuItem
      Caption = 'Parti&cipantes'
      object Inclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui1Click
      end
      object Altera1: TMenuItem
        Caption = '&Altera'
        OnClick = Altera1Click
      end
    end
  end
  object TbRegulam: TmySQLTable
    Database = Dmod.MyDB
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftInteger
      end
      item
        Name = 'CodUsu'
        DataType = ftInteger
      end
      item
        Name = 'CNPJ_EMP'
        DataType = ftString
        Size = 32
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Local'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DtInscrIni'
        DataType = ftDate
      end
      item
        Name = 'DtInscrFim'
        DataType = ftDate
      end
      item
        Name = 'DataEvenIni'
        DataType = ftDate
      end
      item
        Name = 'HoraEvenIni'
        DataType = ftTime
      end
      item
        Name = 'DataEvenFim'
        DataType = ftDate
      end
      item
        Name = 'HoraEvenFim'
        DataType = ftTime
      end
      item
        Name = 'SenhaC'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'ContatoNome'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'ContatoFone'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ContatoMail'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Regulam'
        DataType = ftMemo
        Size = 4
      end
      item
        Name = 'XML_Resul'
        DataType = ftMemo
        Size = 4
      end
      item
        Name = 'DtHrResul'
        DataType = ftDateTime
      end
      item
        Name = 'Observ_0'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_1'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_2'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_3'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_4'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_5'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_6'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_7'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_8'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Observ_9'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Lk'
        DataType = ftInteger
      end
      item
        Name = 'DataCad'
        DataType = ftDate
      end
      item
        Name = 'DataAlt'
        DataType = ftDate
      end
      item
        Name = 'UserCad'
        DataType = ftInteger
      end
      item
        Name = 'UserAlt'
        DataType = ftInteger
      end
      item
        Name = 'AlterWeb'
        DataType = ftSmallint
      end
      item
        Name = 'Ativo'
        DataType = ftSmallint
      end>
    StoreDefs = True
    TableName = 'swms_mtng_cab'
    Left = 609
    Top = 67
    object TbRegulamCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbRegulamRegulam: TWideMemoField
      FieldName = 'Regulam'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsRegulam: TDataSource
    DataSet = TbRegulam
    Left = 637
    Top = 67
  end
  object XMLDocument: TXMLDocument
    Left = 152
    Top = 12
    DOMVendorDesc = 'MSXML'
  end
  object QrLocIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM swms_mtng_its'
      'WHERE Codigo=:P0'
      'AND SenhaP=:P1'
      '')
    Left = 284
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCompetiPart: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCompetiPartCalcFields
    SQL.Strings = (
      'SELECT sen.Nome NO_NADADOR, sec.*'
      'FROM swms_envi_competipart sec'
      'LEFT JOIN swms_envi_nadadores sen ON sen.Nivel1=sec.Nivel1'
      '     AND sen.Nivel2=sec.Nivel2'
      '     AND sen.Codigo=sec.Nadador'
      'WHERE sec.Nivel1=:P0'
      'AND sec.Nivel2=:P1')
    Left = 540
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCompetiPartNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrCompetiPartNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrCompetiPartCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCompetiPartControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCompetiPartEstilo: TIntegerField
      FieldName = 'Estilo'
    end
    object QrCompetiPartMetros: TIntegerField
      FieldName = 'Metros'
    end
    object QrCompetiPartNadador: TIntegerField
      FieldName = 'Nadador'
    end
    object QrCompetiPartCategoria: TIntegerField
      FieldName = 'Categoria'
    end
    object QrCompetiPartTempo: TFloatField
      FieldName = 'Tempo'
    end
    object QrCompetiPartTempo_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Tempo_TXT'
      Size = 40
      Calculated = True
    end
    object QrCompetiPartNO_ESTILO: TWideStringField
      FieldName = 'NO_ESTILO'
      Size = 30
    end
    object QrCompetiPartNO_METROS: TWideStringField
      FieldName = 'NO_METROS'
      Size = 30
    end
    object QrCompetiPartNO_CATEGORIA: TWideStringField
      FieldName = 'NO_CATEGORIA'
      Size = 30
    end
    object QrCompetiPartNO_NADADOR: TWideStringField
      FieldName = 'NO_NADADOR'
      Size = 100
    end
    object QrCompetiPartImportado: TSmallintField
      FieldName = 'Importado'
    end
  end
  object DsCompetiPart: TDataSource
    DataSet = QrCompetiPart
    Left = 568
    Top = 220
  end
  object PMProva: TPopupMenu
    Left = 696
    Top = 612
    object Incluinovaprova1: TMenuItem
      Caption = '&Inclui nova prova'
      object Unica1: TMenuItem
        Caption = #218'nica'
        OnClick = Unica1Click
      end
      object Multiplas1: TMenuItem
        Caption = '&M'#250'ltiplas'
        OnClick = Multiplas1Click
      end
    end
    object Alteraaprovaselecionada1: TMenuItem
      Caption = '&Altera a prova selecionada'
      OnClick = Alteraaprovaselecionada1Click
    end
    object Exclusodasprovasselecionadas1: TMenuItem
      Caption = '&Exclus'#227'o das provas selecionadas'
      OnClick = Exclusodasprovasselecionadas1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Definecategoriasdaprovaselecionada1: TMenuItem
      Caption = 'Define categorias da prova selecionada'
      Enabled = False
      Visible = False
      OnClick = Definecategoriasdaprovaselecionada1Click
    end
    object N5: TMenuItem
      Caption = '-'
      Enabled = False
      Visible = False
    end
    object Participaes1: TMenuItem
      Caption = 'Fase classificat'#243'ria'
      object Redefinirtodafaseclassificatria1: TMenuItem
        Caption = 'Redefinir toda fase classificat'#243'ria'
        OnClick = Redefinirtodafaseclassificatria1Click
      end
      object N6: TMenuItem
        Caption = '-'
        Enabled = False
      end
    end
  end
  object DsSWMS_Mtng_Prv: TDataSource
    DataSet = QrSWMS_Mtng_Prv
    Left = 208
    Top = 12
  end
  object QrSWMS_Mtng_Prv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSWMS_Mtng_PrvBeforeClose
    AfterScroll = QrSWMS_Mtng_PrvAfterScroll
    OnCalcFields = QrSWMS_Mtng_PrvCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM swms_mtng_prv'
      'WHERE Codigo=:P0')
    Left = 180
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSWMS_Mtng_PrvCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'swms_mtng_prv.Codigo'
    end
    object QrSWMS_Mtng_PrvControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'swms_mtng_prv.Controle'
    end
    object QrSWMS_Mtng_PrvCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Origin = 'swms_mtng_prv.CodTxt'
    end
    object QrSWMS_Mtng_PrvEstilo: TIntegerField
      FieldName = 'Estilo'
      Origin = 'swms_mtng_prv.Estilo'
    end
    object QrSWMS_Mtng_PrvNO_ESTILO: TWideStringField
      FieldName = 'NO_ESTILO'
    end
    object QrSWMS_Mtng_PrvMetros: TIntegerField
      FieldName = 'Metros'
      Origin = 'swms_mtng_prv.Metros'
    end
    object QrSWMS_Mtng_PrvNO_METROS: TWideStringField
      FieldName = 'NO_METROS'
      Size = 15
    end
    object QrSWMS_Mtng_PrvSexo: TWideStringField
      FieldName = 'Sexo'
      Origin = 'swms_mtng_prv.Sexo'
      Size = 1
    end
    object QrSWMS_Mtng_PrvPiscDescr: TWideStringField
      FieldName = 'PiscDescr'
      Origin = 'swms_mtng_prv.PiscDescr'
      Size = 60
    end
    object QrSWMS_Mtng_PrvPiscCompr: TIntegerField
      FieldName = 'PiscCompr'
      Origin = 'swms_mtng_prv.PiscCompr'
    end
    object QrSWMS_Mtng_PrvPiscRaias: TIntegerField
      FieldName = 'PiscRaias'
      Origin = 'swms_mtng_prv.PiscRaias'
    end
    object QrSWMS_Mtng_PrvCategoriaIni: TIntegerField
      FieldName = 'CategoriaIni'
    end
    object QrSWMS_Mtng_PrvCategoriaFim: TIntegerField
      FieldName = 'CategoriaFim'
    end
    object QrSWMS_Mtng_PrvNO_CATEGORIAINI: TWideStringField
      FieldName = 'NO_CATEGORIAINI'
      Size = 30
    end
    object QrSWMS_Mtng_PrvNO_CATEGORIAFIM: TWideStringField
      FieldName = 'NO_CATEGORIAFIM'
      Size = 30
    end
    object QrSWMS_Mtng_PrvPiorRaia: TSmallintField
      FieldName = 'PiorRaia'
    end
    object QrSWMS_Mtng_PrvSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrSWMS_Mtng_PrvNO_SEXO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_SEXO'
      Size = 10
      Calculated = True
    end
  end
  object DsSemCat: TDataSource
    DataSet = QrSemCat
    Left = 648
    Top = 12
  end
  object QrSemCat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sen.Nascim, sen.Nome, sen.Sexo, sec.* '
      'FROM swms_envi_competipart sec'
      'LEFT JOIN swms_envi_nadadores sen ON '
      '  sen.Nivel1=sec.Nivel1 AND'
      '  sen.Nivel2=sec.Nivel2 AND'
      '  sen.Codigo=sec.Nadador'
      'WHERE sec.Nivel1=:P0'
      'AND sec.CategPart=0'
      'ORDER BY sen.Nome')
    Left = 620
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSemCatNascim: TDateField
      FieldName = 'Nascim'
    end
    object QrSemCatNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSemCatSexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object QrSemCatNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSemCatNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrSemCatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSemCatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSemCatEstilo: TIntegerField
      FieldName = 'Estilo'
    end
    object QrSemCatMetros: TIntegerField
      FieldName = 'Metros'
    end
    object QrSemCatNadador: TIntegerField
      FieldName = 'Nadador'
    end
    object QrSemCatCategoria: TIntegerField
      FieldName = 'Categoria'
    end
    object QrSemCatTempo: TFloatField
      FieldName = 'Tempo'
    end
    object QrSemCatCategPart: TIntegerField
      FieldName = 'CategPart'
    end
    object QrSemCatswms_mtng_prv: TIntegerField
      FieldName = 'swms_mtng_prv'
    end
  end
  object QrTem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM swms_mtng_tem'
      'WHERE Codigo=:P0'
      'AND Tempo > 0')
    Left = 676
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrPrvs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM swms_mtng_prv'
      'WHERE Codigo=:P0'
      'ORDER BY Controle')
    Left = 812
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrvsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPrvsPiscRaias: TIntegerField
      FieldName = 'PiscRaias'
    end
    object QrPrvsPiorRaia: TSmallintField
      FieldName = 'PiorRaia'
    end
  end
  object QrSWMS_Mtng_Sty: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM swms_mtng_sty'
      'WHERE Codigo=:P0')
    Left = 236
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSWMS_Mtng_StyCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSWMS_Mtng_StyEstilo: TIntegerField
      FieldName = 'Estilo'
    end
    object QrSWMS_Mtng_StyOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrSWMS_Mtng_StyFatorPtos: TIntegerField
      FieldName = 'FatorPtos'
    end
    object QrSWMS_Mtng_StyNO_ESTILO: TWideStringField
      FieldName = 'NO_ESTILO'
      Size = 30
    end
  end
  object DsSWMS_Mtng_Sty: TDataSource
    DataSet = QrSWMS_Mtng_Sty
    Left = 264
    Top = 12
  end
  object QrPart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT n.Sexo, c.* '
      'FROM swms_envi_competipart c'
      'LEFT JOIN swms_envi_nadadores n ON n.Nivel1=c.Nivel1 '
      '  AND n.Nivel2=c.Nivel2 AND c.Nadador=n.Codigo'
      'WHERE c.Nivel1=1')
    Left = 756
    Top = 12
    object QrPartNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPartNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrPartCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPartControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPartEstilo: TIntegerField
      FieldName = 'Estilo'
    end
    object QrPartMetros: TIntegerField
      FieldName = 'Metros'
    end
    object QrPartNadador: TIntegerField
      FieldName = 'Nadador'
    end
    object QrPartCategoria: TIntegerField
      FieldName = 'Categoria'
    end
    object QrPartTempo: TFloatField
      FieldName = 'Tempo'
    end
    object QrPartCategPart: TIntegerField
      FieldName = 'CategPart'
    end
    object QrPartSexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
  end
  object QrProv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle '
      'FROM swms_mtng_prv'
      'WHERE Codigo=:P0'
      'AND Estilo=:P1'
      'AND Metros=:P2'
      'AND Sexo=:P3'
      'AND :P4 BETWEEN CategoriaIni AND CategoriaFim')
    Left = 784
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrProvControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrFaseA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFaseACalcFields
    SQL.Strings = (
      'SELECT smi.Nome NO_EQUIPE, sec.SerieA, sec.RaiaA, '
      'sec.TempoA, sen.Nome NO_NADADOR,'
      'sec.Nivel2, sec.Controle, sec.Tempo'
      'FROM swms_envi_competipart sec'
      'LEFT JOIN swms_envi_nadadores sen ON sen.Nivel1=sec.Nivel1'
      '     AND sen.Nivel2=sec.Nivel2'
      '     AND sen.Codigo=sec.Nadador'
      'LEFT JOIN swms_mtng_its smi ON smi.Codigo=sec.Nivel1'
      '     AND smi.Controle=sec.Nivel2'
      'WHERE sec.swms_mtng_prv=:P0'
      'ORDER BY sec.SerieA, sec.RaiaA, sec.Tempo, sen.Nascim ')
    Left = 680
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaseASerieA: TIntegerField
      FieldName = 'SerieA'
    end
    object QrFaseARaiaA: TIntegerField
      FieldName = 'RaiaA'
    end
    object QrFaseANO_NADADOR: TWideStringField
      FieldName = 'NO_NADADOR'
      Size = 100
    end
    object QrFaseANivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrFaseAControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFaseATempo: TFloatField
      FieldName = 'Tempo'
    end
    object QrFaseATEMPO_STR: TWideStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'TEMPO_STR'
      Calculated = True
    end
    object QrFaseANO_EQUIPE: TWideStringField
      FieldName = 'NO_EQUIPE'
      Size = 100
    end
    object QrFaseATempoA: TFloatField
      FieldName = 'TempoA'
    end
    object QrFaseATEMPOA_STR: TWideStringField
      DisplayWidth = 15
      FieldKind = fkCalculated
      FieldName = 'TEMPOA_STR'
      Size = 15
      Calculated = True
    end
  end
  object DsFaseA: TDataSource
    DataSet = QrFaseA
    Left = 708
    Top = 412
  end
  object QrLarg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(sec.Tempo=0, 0, 1) COM_TEMPO,'
      'sec.SerieA, sec.RaiaA, '
      'sen.Nome NO_NADADOR,'
      'sec.Nivel2, sec.Controle, sec.Tempo'
      'FROM swms_envi_competipart sec'
      'LEFT JOIN swms_envi_nadadores sen ON sen.Nivel1=sec.Nivel1'
      '     AND sen.Nivel2=sec.Nivel2'
      '     AND sen.Codigo=sec.Nadador'
      'WHERE sec.swms_mtng_prv=:P0'
      'ORDER BY COM_TEMPO, sec.Tempo DESC, sen.Nascim DESC')
    Left = 840
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLargSerieA: TIntegerField
      FieldName = 'SerieA'
    end
    object QrLargRaiaA: TIntegerField
      FieldName = 'RaiaA'
    end
    object QrLargNO_NADADOR: TWideStringField
      FieldName = 'NO_NADADOR'
      Size = 100
    end
    object QrLargNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrLargControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLargTempo: TFloatField
      FieldName = 'Tempo'
    end
  end
  object QrNadadores: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNadadoresCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM swms_envi_nadadores'
      'WHERE Nivel1=:P0'
      'AND Nivel2=:P1')
    Left = 540
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNadadoresNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrNadadoresNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrNadadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNadadoresCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 60
    end
    object QrNadadoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNadadoresNascim: TDateField
      FieldName = 'Nascim'
    end
    object QrNadadoresSexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object QrNadadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrNadadoresFone: TWideStringField
      FieldName = 'Fone'
    end
    object QrNadadoresLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNadadoresDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNadadoresDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNadadoresUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNadadoresUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNadadoresAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNadadoresAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNadadoresImportado: TSmallintField
      FieldName = 'Importado'
    end
    object QrNadadoresFone_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Fone_TXT'
      Size = 40
      Calculated = True
    end
    object QrNadadoresCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsNadadores: TDataSource
    DataSet = QrNadadores
    Left = 568
    Top = 248
  end
  object frxSWMS_Mtng_Cab_03_00: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 40738.849828009260000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxSWMS_Mtng_Cab_03_00GetValue
    Left = 596
    Top = 248
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsFaseA
        DataSetName = 'frxDsFaseA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsSWMS_Mtng_Prv
        DataSetName = 'frxDsSWMS_Mtng_Prv'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 86.929175350000000000
        Top = 139.842610000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSWMS_Mtng_Prv
        DataSetName = 'frxDsSWMS_Mtng_Prv'
        KeepTogether = True
        RowCount = 0
        object Memo12: TfrxMemoView
          Left = 3.779530000000000000
          Top = 60.472479999999990000
          Width = 37.795300000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 41.574830000000000000
          Top = 60.472479999999990000
          Width = 37.795300000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Raia')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 79.370130000000000000
          Top = 60.472479999999990000
          Width = 332.598640000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 411.968770000000000000
          Top = 60.472479999999990000
          Width = 151.181200000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Equipe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 563.149970000000000000
          Top = 60.472479999999990000
          Width = 113.385900000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tempo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Top = 3.779529999999994000
          Width = 680.315400000000000000
          Height = 52.913405350000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 3.779530000000000000
          Top = 30.236240000000010000
          Width = 37.795300000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSWMS_Mtng_Prv."SEQ"]'#170)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 41.574830000000000000
          Top = 30.236240000000010000
          Width = 56.692950000000000000
          Height = 22.677165350000000000
          DataField = 'NO_METROS'
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSWMS_Mtng_Prv."NO_METROS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 98.267780000000000000
          Top = 30.236240000000010000
          Width = 132.283550000000000000
          Height = 22.677165350000000000
          DataField = 'NO_ESTILO'
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSWMS_Mtng_Prv."NO_ESTILO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 230.551330000000000000
          Top = 30.236240000000010000
          Width = 170.078850000000000000
          Height = 22.677165350000000000
          DataField = 'NO_CATEGORIAINI'
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSWMS_Mtng_Prv."NO_CATEGORIAINI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 400.630180000000000000
          Top = 30.236240000000010000
          Width = 170.078850000000000000
          Height = 22.677165350000000000
          DataField = 'NO_CATEGORIAFIM'
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSWMS_Mtng_Prv."NO_CATEGORIAFIM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 570.709030000000000000
          Top = 30.236240000000010000
          Width = 105.826771650000000000
          Height = 22.677165350000000000
          DataField = 'NO_SEXO'
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSWMS_Mtng_Prv."NO_SEXO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 3.779530000000000000
          Top = 7.559059999999988000
          Width = 37.795300000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prova')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 41.574830000000000000
          Top = 7.559059999999988000
          Width = 56.692950000000000000
          Height = 22.677165354330710000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Metros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 98.267780000000000000
          Top = 7.559059999999988000
          Width = 132.283550000000000000
          Height = 22.677165354330710000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Estilo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 230.551330000000000000
          Top = 7.559059999999988000
          Width = 170.078850000000000000
          Height = 22.677165354330710000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Categoria inicial')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 400.630180000000000000
          Top = 7.559059999999988000
          Width = 170.078850000000000000
          Height = 22.677165354330710000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Categoria Final')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 570.709030000000000000
          Top = 7.559059999999988000
          Width = 105.826771650000000000
          Height = 22.677165354330710000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'G'#234'nero')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line4: TfrxLineView
          Top = 56.692950000000000000
          Height = 30.236225350000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo18: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date] [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 105.826776540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 113.385900000000000000
          Top = 18.897650000000000000
          Width = 453.543600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 22.677167800000000000
        Top = 275.905690000000000000
        Width = 680.315400000000000000
        DataSet = frxDsFaseA
        DataSetName = 'frxDsFaseA'
        KeepTogether = True
        RowCount = 0
        object Memo17: TfrxMemoView
          Left = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 22.677165354330710000
          DataField = 'SerieA'
          DataSet = frxDsFaseA
          DataSetName = 'frxDsFaseA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFaseA."SerieA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 79.370130000000000000
          Width = 332.598640000000000000
          Height = 22.677165354330710000
          DataField = 'NO_NADADOR'
          DataSet = frxDsFaseA
          DataSetName = 'frxDsFaseA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFaseA."NO_NADADOR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 411.968770000000000000
          Width = 151.181200000000000000
          Height = 22.677165354330710000
          DataField = 'NO_EQUIPE'
          DataSet = frxDsFaseA
          DataSetName = 'frxDsFaseA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFaseA."NO_EQUIPE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 563.149970000000000000
          Width = 113.385900000000000000
          Height = 22.677165350000000000
          DataSet = frxDsSWMS_Mtng_Prv
          DataSetName = 'frxDsSWMS_Mtng_Prv'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 41.574830000000000000
          Width = 37.795300000000000000
          Height = 22.677165354330710000
          DataField = 'RaiaA'
          DataSet = frxDsFaseA
          DataSetName = 'frxDsFaseA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFaseA."RaiaA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Height = 22.677165354330710000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsFaseA."SerieA"'
        object Line11: TfrxLineView
          Height = 3.779530000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Line8: TfrxLineView
          Height = 3.779530000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line9: TfrxLineView
          Top = 3.779530000000022000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Top = 117.165430000000000000
        Width = 680.315400000000000000
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 347.716760000000000000
        Width = 680.315400000000000000
        object Line12: TfrxLineView
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object Line5: TfrxLineView
        Left = 680.315400000000000000
        Top = 196.535560000000000000
        Height = 30.236225350000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line6: TfrxLineView
        Left = 680.315400000000000000
        Top = 275.905690000000000000
        Height = 22.677165350000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line10: TfrxLineView
        Left = 680.315400000000000000
        Top = 249.448980000000000000
        Height = 3.779530000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
      object Line7: TfrxLineView
        Left = 680.315400000000000000
        Top = 321.260050000000000000
        Height = 3.779530000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
      end
    end
  end
  object QrSers: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT SerieA'
      'FROM swms_envi_competipart'
      'WHERE swms_mtng_prv=:P0'
      'ORDER BY SerieA')
    Left = 868
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSersSerieA: TIntegerField
      FieldName = 'SerieA'
    end
  end
  object frxDsSWMS_Mtng_Prv: TfrxDBDataset
    UserName = 'frxDsSWMS_Mtng_Prv'
    CloseDataSource = False
    DataSet = QrSWMS_Mtng_Prv
    BCDToCurrency = False
    Left = 180
    Top = 40
  end
  object frxDsFaseA: TfrxDBDataset
    UserName = 'frxDsFaseA'
    CloseDataSource = False
    DataSet = QrFaseA
    BCDToCurrency = False
    Left = 652
    Top = 412
  end
end
