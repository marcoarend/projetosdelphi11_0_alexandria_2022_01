unit MatriPer2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkGeral, Variants,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, Grids, DBGrids, dmkDBGridDAC;

type
  TFmMatriPer2 = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    DsPeriodos: TDataSource;
    QrPeriodos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdControle: TdmkEdit;
    SpeedButton1: TSpeedButton;
    DBGrid1: TdmkDBGridDAC;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrPeriodosAtivo: TSmallintField;
    QrPeriodosCodigo: TIntegerField;
    QrPeriodosControle: TIntegerField;
    QrPeriodosConta: TIntegerField;
    QrPeriodosItem: TIntegerField;
    QrPeriodosSubItem: TIntegerField;
    QrPeriodosTURMA: TIntegerField;
    QrPeriodosTURMAITS: TIntegerField;
    QrPeriodosCurso: TIntegerField;
    QrPeriodosNOMECURSO: TWideStringField;
    QrPeriodosREFTURMAITS: TWideStringField;
    QrPeriodosNOMETURMA: TWideStringField;
    QrPeriodosIDTURMA: TWideStringField;
    QrPeriodosPeriodo: TIntegerField;
    QrPeriodosNome: TWideStringField;
    QrPeriodosDSem: TIntegerField;
    QrPeriodosHIni: TTimeField;
    QrPeriodosHFim: TTimeField;
    QrPeriodosNOMEDSEM: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
    FHorarios1: String;
    procedure AtivarTodos(Ativo: Integer);
    procedure ReopenPeriodos(Conta: Integer);
  public
    { Public declarations }
    procedure PreparaPeriodos(Turma, Atividade: Integer);
  end;

var
  FmMatriPer2: TFmMatriPer2;

implementation

uses UnMyObjects, Module, TurmasTmp, MyDBCheck, UCreate, ModuleGeral, Matricul;

{$R *.DFM}

procedure TFmMatriPer2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriPer2.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmMatriPer2.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmMatriPer2.AtivarTodos(Ativo: Integer);
var
  Conta: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Conta := QrPeriodosConta.Value;
    DmodG.QrUpdPID1.Close;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FHorarios1 + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Ativo=:P0');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenPeriodos(Conta);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMatriPer2.BtConfirmaClick(Sender: TObject);
var
  Conta, Periodo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrPeriodos.DisableControls;
    QrPeriodos.First;
    while not QrPeriodos.Eof do
    begin
      Periodo := QrPeriodosPeriodo.Value;
      if (QrPeriodosSubItem.Value = 0) and (QrPeriodosAtivo.Value = 1) then
      begin
        Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'MatriPer', 'MatriPer', 'Codigo');
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('INSERT INTO MatriPer SET ');
        Dmod.QrUpdM.SQL.Add('Codigo=:P0, Controle=:P1, Conta=:P2, Item=:P3, ');
        Dmod.QrUpdM.SQL.Add('SubItem=:P4, Periodo=:P5');
        //
        Dmod.QrUpdM.Params[00].AsInteger := QrPeriodosCodigo.Value;
        Dmod.QrUpdM.Params[01].AsInteger := QrPeriodosControle.Value;
        Dmod.QrUpdM.Params[02].AsInteger := QrPeriodosConta.Value;
        Dmod.QrUpdM.Params[03].AsInteger := QrPeriodosItem.Value;
        Dmod.QrUpdM.Params[04].AsInteger := Conta;
        Dmod.QrUpdM.Params[05].AsInteger := Periodo;
        Dmod.QrUpdM.ExecSQL;
      end
      else if (QrPeriodosSubItem.Value <> 0) and (QrPeriodosAtivo.Value = 0) then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM matriper WHERE Codigo=:P0');
        Dmod.QrUpd.SQL.Add('AND Controle=:P1 AND Conta=:P2 AND Item=:P3');
        Dmod.QrUpd.SQL.Add('AND SubItem=:P4');
        Dmod.QrUpd.Params[00].AsInteger := QrPeriodosCodigo.Value;
        Dmod.QrUpd.Params[01].AsInteger := QrPeriodosControle.Value;
        Dmod.QrUpd.Params[02].AsInteger := QrPeriodosConta.Value;
        Dmod.QrUpd.Params[03].AsInteger := QrPeriodosItem.Value;
        Dmod.QrUpd.Params[04].AsInteger := QrPeriodosSubItem.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      QrPeriodos.Next;
    end;
    FmMatricul.ReopenMatriPer();
    Close;
  finally
    Screen.Cursor := crHourGlass;
  end;
end;

procedure TFmMatriPer2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmMatriPer2.PreparaPeriodos(Turma, Atividade: Integer);
begin
{
  QrPeriodos.SQL.Add('SELECT tur.Codigo Turma, tur.Nome NOMETURMA, ');
  QrPeriodos.SQL.Add('tui.Curso, cur.Nome NOMECURSO, ttm.Conta,');
  QrPeriodos.SQL.Add('per.Nome, per.DSem, per.HIni, per.HFim,');
  QrPeriodos.SQL.Add('ELT(per.DSem+1, "Domingo", "Segunda-feira",');
  QrPeriodos.SQL.Add('"Ter�a-feira", "Quarta-feira", "Quinta-feira",');
  QrPeriodos.SQL.Add('"Sexta-feira", "S�bado" ) NOMEDSEM, 0 Ativo');
  QrPeriodos.SQL.Add('FROM Academy.matriper map');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.turmastmp ttm ON ttm.Conta=map.Periodo');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.periodos  per ON per.Codigo=ttm.Periodo');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.matriati maa ON maa.Item=map.Item');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.turmasits tui ON tui.Controle=maa.Atividade');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.turmas tur ON tur.Codigo=tui.Codigo');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.cursos    cur ON cur.Codigo=tui.Curso');
  QrPeriodos.SQL.Add('');
  QrPeriodos.SQL.Add('');
  QrPeriodos.SQL.Add('');
  QrPeriodos.SQL.Add('');
  QrPeriodos.SQL.Add('');
  QrPeriodos.SQL.Add('');

  QrPeriodos.SQL.Add('WHERE maa.Codigo=' + FormatFloat('0', EdCodigo.ValueVariant));
  QrPeriodos.SQL.Add('AND maa.Controle=' + FormatFloat('0', EdControle.ValueVariant));
  QrPeriodos.SQL.Add('ORDER BY NOMECURSO, DSem, HIni, HFim');
}
  QrPeriodos.SQL.Clear;
  QrPeriodos.SQL.Add('INSERT INTO ' + FHorarios1);
  QrPeriodos.SQL.Add('SELECT IF(mtp.SubItem>0,1,0) Ativo,mta.Codigo, mta.Controle, mta.Conta,');
  QrPeriodos.SQL.Add('mta.Item, mtp.SubItem, ttm.Codigo TURMA, ttm.Controle TURMAITS,');
  QrPeriodos.SQL.Add('tui.Curso, cur.Nome NOMECURSO, ');
  QrPeriodos.SQL.Add('tui.Refere REFTURMAITS, tur.Nome NOMETURMA,');
  QrPeriodos.SQL.Add('tur.ID IDTURMA, ttm.Conta Periodo, per.Nome, per.DSem,');
  QrPeriodos.SQL.Add('per.HIni, per.HFim, ELT(DSem+1, "Domingo", "Segunda",');
  QrPeriodos.SQL.Add('"Ter�a", "Quarta", "Quinta", "Sexta", "S�bado")');
  QrPeriodos.SQL.Add('NOMEDSEM');
  QrPeriodos.SQL.Add('FROM '+TMeuDB+'.turmastmp ttm');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.periodos per ON per.Codigo=ttm.Periodo');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.turmasits tui ON tui.Controle=ttm.Controle');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.turmas tur ON tur.Codigo=tui.Codigo');

  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.cursos    cur ON cur.Codigo=tui.Curso');

  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.matritur mtt ON mtt.Turma=tur.Codigo');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.matriati mta ON mta.Conta=mtt.Conta');
  QrPeriodos.SQL.Add('LEFT JOIN '+TMeuDB+'.matriper mtp ON mtp.Item=mta.Item AND mtp.Periodo=ttm.Conta ');
  QrPeriodos.SQL.Add('WHERE mtt.Controle=' + FormatFloat('0', EdControle.ValueVariant));
  QrPeriodos.SQL.Add('ORDER BY NOMECURSO, DSem, HIni, HFim');
  QrPeriodos.ExecSQL;
  //
  //
  ReopenPeriodos(0);
  //
end;

procedure TFmMatriPer2.ReopenPeriodos(Conta: Integer);
begin
  QrPeriodos.Close;
  QrPeriodos.SQL.Clear;
  QrPeriodos.SQL.Add('SELECT * FROM ' + FHorarios1);
  QrPeriodos.Open;
  QrPeriodos.Locate('Conta', Conta, []);
end;

procedure TFmMatriPer2.SpeedButton1Click(Sender: TObject);
begin
{
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmTurmasTmp, FmTurmasTmp, afmoNegarComAviso) then
  begin
    FmTurmasTmp.ShowModal;
    FmTurmasTmp.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(nil, nil, QrPeriodos, VAR_CADASTRO);
  end;
}
end;

procedure TFmMatriPer2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMatriPer2.FormCreate(Sender: TObject);
begin
  QrPeriodos.Close;
  QrPeriodos.Database := DModG.MyPID_DB;
  FHorarios1 := Ucriar.RecriaTempTable('Horarios1', DmodG.QrUpdPID1, False);
end;

end.
