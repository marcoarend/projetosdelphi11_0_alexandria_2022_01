object FmSalas: TFmSalas
  Left = 375
  Top = 184
  Caption = 'ATI-CADAS-002 :: Cadastro de Locais de Atividades'
  ClientHeight = 374
  ClientWidth = 962
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 962
    Height = 315
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 255
      Width = 960
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 382
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 458
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiClick
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 960
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 148
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 512
        Top = 10
        Width = 52
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lota'#231#227'o:'
      end
      object Label5: TLabel
        Left = 586
        Top = 10
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #193'rea m'#178':'
      end
      object Label6: TLabel
        Left = 660
        Top = 10
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'watts/h:'
      end
      object Label8: TLabel
        Left = 734
        Top = 10
        Width = 200
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Departamento (Centro de custos):'
      end
      object DBEdCodigo: TDBEdit
        Left = 20
        Top = 28
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        DataField = 'Codigo'
        DataSource = DsSalas
        ParentBiDiMode = False
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 28
        Width = 360
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        DataField = 'Nome'
        DataSource = DsSalas
        ParentBiDiMode = False
        TabOrder = 1
      end
      object DBEdLotacao: TDBEdit
        Left = 512
        Top = 28
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        DataField = 'Lotacao'
        DataSource = DsSalas
        ParentBiDiMode = False
        TabOrder = 2
      end
      object DBEdArea: TDBEdit
        Left = 586
        Top = 28
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        DataField = 'Area'
        DataSource = DsSalas
        ParentBiDiMode = False
        TabOrder = 3
      end
      object DBEdit2: TDBEdit
        Left = 660
        Top = 28
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        DataField = 'wats_h'
        DataSource = DsSalas
        ParentBiDiMode = False
        TabOrder = 4
      end
      object DBEdit1: TDBEdit
        Left = 734
        Top = 28
        Width = 208
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        DataField = 'NOMEDEPTO'
        DataSource = DsSalas
        ParentBiDiMode = False
        TabOrder = 5
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 108
      Width = 949
      Height = 149
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      DataSource = DsSalasEquip
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Eletroeletr'#244'nico'
          Width = 378
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'wats_h'
          Title.Caption = 'watts/h'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fator'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CONSUMO'
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 962
    Height = 315
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clScrollBar
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 960
      Height = 222
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 10
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 84
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object LaPorcentagem: TLabel
        Left = 443
        Top = 10
        Width = 52
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lota'#231#227'o:'
        Transparent = True
      end
      object Label4: TLabel
        Left = 522
        Top = 10
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #193'rea m'#178':'
        Transparent = True
      end
      object Label7: TLabel
        Left = 601
        Top = 10
        Width = 198
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Departamento (centro de custos):'
      end
      object SpeedButton5: TSpeedButton
        Left = 921
        Top = 30
        Width = 25
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 10
        Top = 30
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 84
        Top = 30
        Width = 354
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdLotacaoExit
      end
      object EdLotacao: TdmkEdit
        Left = 443
        Top = 30
        Width = 74
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdLotacaoExit
      end
      object EdArea: TdmkEdit
        Left = 522
        Top = 30
        Width = 74
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdAreaExit
      end
      object EdDepartamento: TdmkEditCB
        Left = 601
        Top = 30
        Width = 59
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBDepartamento
        IgnoraDBLookupComboBox = False
      end
      object CBDepartamento: TdmkDBLookupComboBox
        Left = 660
        Top = 30
        Width = 261
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsDepartamentos
        TabOrder = 5
        dmkEditCB = EdDepartamento
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 255
      Width = 960
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 822
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 962
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                  Cadastro de Locais de Atividades'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 860
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 582
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object Painel2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsSalas: TDataSource
    DataSet = QrSalas
    Left = 488
    Top = 161
  end
  object QrSalas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSalasBeforeOpen
    AfterOpen = QrSalasAfterOpen
    AfterScroll = QrSalasAfterScroll
    SQL.Strings = (
      'SELECT sa.*, de.Nome NOMEDEPTO '
      'FROM salas sa'
      'LEFT JOIN departamentos de ON de.Codigo=sa.Departamento '
      'WHERE sa.Codigo > 0')
    Left = 460
    Top = 161
    object QrSalasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSalasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSalasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSalasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSalasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSalasCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrSalasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrSalasLotacao: TIntegerField
      FieldName = 'Lotacao'
    end
    object QrSalasArea: TFloatField
      FieldName = 'Area'
      DisplayFormat = '#,##0.00'
    end
    object QrSalaswats_h: TFloatField
      FieldName = 'wats_h'
    end
    object QrSalasDepartamento: TIntegerField
      FieldName = 'Departamento'
    end
    object QrSalasNOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Size = 50
    end
  end
  object QrSalasEquip: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSalasEquipCalcFields
    SQL.Strings = (
      'SELECT * FROM salasequip'
      'WHERE Codigo=:P0')
    Left = 460
    Top = 189
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSalasEquipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSalasEquipControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSalasEquipNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSalasEquipwats_h: TFloatField
      FieldName = 'wats_h'
    end
    object QrSalasEquipLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSalasEquipDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSalasEquipDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSalasEquipUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSalasEquipUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSalasEquipFator: TFloatField
      FieldName = 'Fator'
      DisplayFormat = '#,###0.0000'
    end
    object QrSalasEquipQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###0.000'
    end
    object QrSalasEquipCONSUMO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO'
      DisplayFormat = '#,###0.00'
      Calculated = True
    end
  end
  object DsSalasEquip: TDataSource
    DataSet = QrSalasEquip
    Left = 488
    Top = 189
  end
  object PMInclui: TPopupMenu
    Left = 336
    Top = 232
    object NovaSala1: TMenuItem
      Caption = 'Novo &local de curso / atividade'
      OnClick = NovaSala1Click
    end
    object Eletroeletrniconasalaatual1: TMenuItem
      Caption = '&Eletroeletr'#244'nico no local atual'
      OnClick = Eletroeletrniconasalaatual1Click
    end
  end
  object PMAltera: TPopupMenu
    Left = 420
    Top = 232
    object AlteraSalaatual1: TMenuItem
      Caption = 'Altera &local atual'
      OnClick = AlteraSalaatual1Click
    end
  end
  object PMExclui: TPopupMenu
    Left = 516
    Top = 232
    object ExcluiEletroeletrnicoatual1: TMenuItem
      Caption = 'Exclui &Eletroeletr'#244'nico atual'
      OnClick = ExcluiEletroeletrnicoatual1Click
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(wats_h*Fator*Qtde) wats_h'
      'FROM salasequip'
      'WHERE Codigo=:P0')
    Left = 516
    Top = 161
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomawats_h: TFloatField
      FieldName = 'wats_h'
    end
  end
  object QrDepartamentos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM departamentos'
      'ORDER BY Nome')
    Left = 680
    Top = 68
    object QrDepartamentosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDepartamentosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsDepartamentos: TDataSource
    DataSet = QrDepartamentos
    Left = 708
    Top = 68
  end
end
