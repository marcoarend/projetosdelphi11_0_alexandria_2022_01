unit Despesas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,   
     ExtCtrls, 
     UMySQLModule,
    UnMLAGeral, UnInternalConsts, Db,
  mySQLDbTables, Mask, DBCtrls,   
  ComCtrls,  Grids, DBGrids, UnMsgInt;

type
  TFmDespesas = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PainelControle: TPanel;
    Panel5: TPanel;
    Panel1: TPanel;
    TPData: TDateTimePicker;
    Label3: TLabel;
    PainelData: TPanel;
    Image2: TImage;
    Panel4: TPanel;
    Panel6: TPanel;
    DBGCarteiras: TDBGrid;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    FCalcula: Boolean;
    FData: String;
    procedure ReabreLanctosDia;
    procedure ExcluiItemCarteira(Lancto: Int64; Sub, Genero, Cartao, Sit, Tipo,
              Chamada: Integer; Incondicional: Boolean);
  public
    { Public declarations }
  end;

var
  FmDespesas: TFmDespesas;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal;

procedure TFmDespesas.ExcluiItemCarteira(Lancto: Int64;
 Sub, Genero, Cartao, Sit, Tipo, Chamada: Integer; Incondicional: Boolean);
begin
  if UMyMod.SelLockInt64Y(Lancto, Dmod.MyDB, 'Lanctos', 'Controle') then Exit;
  VAR_LANCTO2 := Lancto;
  if (Cartao > 0) then
  begin
    ShowMessage('Esta emiss�o n�o pode ser exclu�da pois pertence a uma fatura!');
    Exit;
  end;
  if (Sit > 1) and (Tipo = 2) then
  begin
    ShowMessage('Esta emiss�o n�o pode ser exclu�da pois est� quitada!');
    Exit;
  end;
  if (Genero < 0) or (Sub > 0) then
  begin
    if Genero = -1 then
    begin
      ShowMessage('Esta emiss�o n�o pode ser exclu�da pois � uma transfer�ncia!');
      //CriarTransfer(2);
      //Exit;
    end else if Chamada = 1 (*FmCartPagto*) then
      // Exclui normalmente
    else begin
      ShowMessage('Em constru��o. Para editar este item selecione o menu de op��es');
      Exit;
    end;
  end else if (Genero = 0) and (Incondicional=False) then
  begin
    Application.MessageBox('Exclus�o cancelada. Lan�amento sem conta!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox(FIN_MSG_ASKESCLUI, PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM lanctos WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Lancto;
    Dmod.QrUpdM.ExecSQL;
    Dmod.RecalcSaldoCarteira(Dmod.QrCarteirasTipo.Value,
    Dmod.QrCarteirasCodigo.Value, 1);
  end;
end;

procedure TFmDespesas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDespesas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDespesas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(
  0, 0, Caption, Image1, PainelTitulo, True, 35);
  //
  MLAGeral.LoadTextBitmapToPanel(
  0, 0, 'Data: '+FData, Image2, PainelData, True, 35);
end;

procedure TFmDespesas.FormCreate(Sender: TObject);
begin
  FCalcula := True;
  TPData.Date := Date;
  ReabreLanctosDia;
end;

procedure TFmDespesas.TPDataChange(Sender: TObject);
begin
  ReabreLanctosDia;
end;

procedure TFmDespesas.ReabreLanctosDia;
begin
  Dmod.QrLanctos.Close;
  Dmod.QrLanctos.Params[0].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Dmod.QrLanctos.Params[1].AsInteger :=  VAR_CARTEIRADESPESAS;
  Dmod.QrLanctos.Open;
end;


procedure TFmDespesas.BtIncluiClick(Sender: TObject);
begin
  VAR_DATAINSERIR := Date;
  FmPrincipal.CriaFmCartEdit(1, VAR_CARTTIPODESPESAS, 0, 0, 0, 0,
    VAR_CARTEIRADESPESAS, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, date, 0, 0,
    CO_VAZIO, CO_VAZIO);
  ReabreLanctosDia;
end;

procedure TFmDespesas.BtAlteraClick(Sender: TObject);
begin
  FmPrincipal.AlteraLancto;
  ReabreLanctosDia;
end;


procedure TFmDespesas.BtExcluiClick(Sender: TObject);
begin
  ExcluiItemCarteira(Dmod.QrLanctosControle.Value, Dmod.QrLanctosSub.Value,
    Dmod.QrLanctosGenero.Value, Dmod.QrLanctosCartao.Value,
    Dmod.QrLanctosSit.Value, Dmod.QrLanctosTipo.Value, 0, True);
  ReabreLanctosDia;
end;

end.

