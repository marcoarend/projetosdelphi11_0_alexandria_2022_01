object FmMatriRen: TFmMatriRen
  Left = 442
  Top = 253
  Caption = 'MTR-CADAS-002 :: Cria'#231#227'o / Renova'#231#227'o de Matr'#237'cula'
  ClientHeight = 325
  ClientWidth = 759
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 49
    Width = 759
    Height = 217
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Label8: TLabel
      Left = 15
      Top = 64
      Width = 69
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data inicial:'
    end
    object Label10: TLabel
      Left = 300
      Top = 64
      Width = 59
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Data final:'
    end
    object Label2: TLabel
      Left = 15
      Top = 10
      Width = 72
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Tarifa base:'
      FocusControl = EdTarifa
    end
    object Label1: TLabel
      Left = 158
      Top = 64
      Width = 51
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Per'#237'odo:'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Left = 443
      Top = 64
      Width = 91
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Taxa matr'#237'cula:'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 537
      Top = 64
      Width = 85
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor per'#237'odo:'
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Left = 640
      Top = 64
      Width = 61
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Valor real:'
    end
    object SpeedButton1: TSpeedButton
      Left = 709
      Top = 30
      Width = 26
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object TPIni: TdmkEditDateTimePicker
      Left = 15
      Top = 84
      Width = 138
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 38528.802122210600000000
      Time = 38528.802122210600000000
      TabOrder = 2
      OnClick = TPIniClick
      OnChange = TPIniChange
      OnExit = TPIniExit
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object TPFim: TdmkEditDateTimePicker
      Left = 300
      Top = 84
      Width = 138
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Date = 38528.802122210600000000
      Time = 38528.802122210600000000
      Enabled = False
      TabOrder = 4
      OnKeyDown = TPFimKeyDown
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object CkRenoRef: TCheckBox
      Left = 222
      Top = 118
      Width = 289
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Manter as mesmas atividades e hor'#225'rios.'
      Enabled = False
      TabOrder = 9
    end
    object Progress3: TProgressBar
      Left = 1
      Top = 195
      Width = 757
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 13
    end
    object Progress1: TProgressBar
      Left = 1
      Top = 154
      Width = 757
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 11
    end
    object Progress2: TProgressBar
      Left = 1
      Top = 175
      Width = 757
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 12
    end
    object EdTarifa: TdmkEditCB
      Left = 15
      Top = 30
      Width = 69
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdTarifaChange
      DBLookupComboBox = CBTarifa
      IgnoraDBLookupComboBox = False
    end
    object CBTarifa: TdmkDBLookupComboBox
      Left = 84
      Top = 30
      Width = 625
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTarifas
      TabOrder = 1
      dmkEditCB = EdTarifa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object DBEdit1: TDBEdit
      Left = 158
      Top = 84
      Width = 139
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'PERIODO'
      DataSource = DsTarifas
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Left = 443
      Top = 84
      Width = 90
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'Taxa'
      DataSource = DsTarifasIts
      TabOrder = 5
    end
    object DBEdit3: TDBEdit
      Left = 537
      Top = 84
      Width = 98
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Color = clBtnFace
      DataField = 'Preco'
      DataSource = DsTarifasIts
      TabOrder = 6
    end
    object CkTaxa: TCheckBox
      Left = 15
      Top = 118
      Width = 184
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cobrar taxa de inscri'#231#227'o.'
      Checked = True
      State = cbChecked
      TabOrder = 8
      OnClick = CkTaxaClick
    end
    object EdACobrar: TdmkEdit
      Left = 640
      Top = 84
      Width = 95
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdACobrarExit
    end
    object CkDesativar: TCheckBox
      Left = 655
      Top = 118
      Width = 80
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Desativar'
      TabOrder = 10
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 266
    Width = 759
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 17
      Top = 5
      Width = 111
      Height = 49
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel1: TPanel
      Left = 622
      Top = 1
      Width = 136
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 759
    Height = 49
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Cria'#231#227'o / Renova'#231#227'o de Matr'#237'cula'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 656
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 657
      Top = 1
      Width = 101
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
  end
  object QrTarifas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrTarifasAfterScroll
    OnCalcFields = QrTarifasCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM tarifas'
      'WHERE Codigo <> 0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 324
    Top = 196
    object QrTarifasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTarifasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTarifasPERIODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO'
      Size = 50
      Calculated = True
    end
    object QrTarifasAtiva: TSmallintField
      FieldName = 'Ativa'
    end
    object QrTarifasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTarifasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTarifasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTarifasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTarifasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTarifasTipoPer: TIntegerField
      FieldName = 'TipoPer'
    end
    object QrTarifasQtdePer: TIntegerField
      FieldName = 'QtdePer'
    end
    object QrTarifasGrupo: TIntegerField
      FieldName = 'Grupo'
    end
  end
  object DsTarifas: TDataSource
    DataSet = QrTarifas
    Left = 352
    Top = 196
  end
  object QrTarifasIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM tarifasits'
      'WHERE Codigo=:P0'
      'AND Data=:P1'
      '')
    Left = 380
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTarifasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTarifasItsData: TDateField
      FieldName = 'Data'
    end
    object QrTarifasItsPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTarifasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTarifasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTarifasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTarifasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTarifasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTarifasItsTaxa: TFloatField
      FieldName = 'Taxa'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsTarifasIts: TDataSource
    DataSet = QrTarifasIts
    Left = 408
    Top = 196
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Data) MAXDATA'
      'FROM tarifasits'
      'WHERE Codigo=:P0'
      'AND Data<=:P1')
    Left = 284
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocMAXDATA: TDateField
      FieldName = 'MAXDATA'
      Required = True
    end
  end
end
