object FmMensais: TFmMensais
  Left = 368
  Top = 194
  Caption = 'Resultados Mensais'
  ClientHeight = 565
  ClientWidth = 965
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 506
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 446
      Width = 963
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtDesiste: TBitBtn
        Left = 719
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1009
      Height = 241
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 1
      object Label3: TLabel
        Left = 20
        Top = 15
        Width = 117
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's do movimento:'
      end
      object Label4: TLabel
        Left = 256
        Top = 15
        Width = 115
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ano do movimento:'
      end
      object Label21: TLabel
        Left = 379
        Top = 16
        Width = 113
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Custo energia (kw):'
      end
      object CBMes: TComboBox
        Left = 21
        Top = 36
        Width = 224
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Style = csDropDownList
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object CBAno: TComboBox
        Left = 255
        Top = 36
        Width = 117
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Style = csDropDownList
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object EdCustokw: TdmkEdit
        Left = 379
        Top = 34
        Width = 105
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdCustokwExit
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 506
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 446
      Width = 963
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 384
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Left = 458
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtExclui: TBitBtn
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object BtAltera: TBitBtn
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiClick
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 963
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 148
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label19: TLabel
        Left = 546
        Top = 10
        Width = 113
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Custo energia (kw):'
      end
      object Label26: TLabel
        Left = 674
        Top = 5
        Width = 182
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Caixa com vencimento no m'#234's'
      end
      object Label27: TLabel
        Left = 674
        Top = 25
        Width = 187
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Banco com vencimento no m'#234's'
      end
      object Label28: TLabel
        Left = 674
        Top = 44
        Width = 226
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Emiss'#227'o com compensas'#231#227'o no m'#234's'
      end
      object DBEdCodigo: TDBEdit
        Left = 20
        Top = 30
        Width = 123
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsMensais
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 148
        Top = 30
        Width = 395
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'MES2'
        DataSource = DsMensais
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit012: TDBEdit
        Left = 546
        Top = 30
        Width = 115
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Custokw'
        DataSource = DsMensais
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
      end
    end
    object PainelItens: TPanel
      Left = 1
      Top = 128
      Width = 973
      Height = 320
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 2
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 973
        Height = 320
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet7
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Receitas'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 965
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Enabled = False
            TabOrder = 0
            ExplicitWidth = 962
            object Label5: TLabel
              Left = 10
              Top = 5
              Width = 54
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Normais:'
            end
            object Label6: TLabel
              Left = 138
              Top = 5
              Width = 83
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Compet'#234'ncia:'
            end
            object Label7: TLabel
              Left = 266
              Top = 5
              Width = 101
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Total receitas:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBEdit01: TDBEdit
              Left = 10
              Top = 25
              Width = 123
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Credito'
              DataSource = DsRecDiaTot
              TabOrder = 0
            end
            object DBEdit02: TDBEdit
              Left = 138
              Top = 25
              Width = 123
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Credito'
              DataSource = DsRecMesTot
              TabOrder = 1
            end
            object DBEdit03: TDBEdit
              Left = 266
              Top = 25
              Width = 123
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'TOTAL'
              DataSource = DsRecMesTot
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
          end
          object PageControl2: TPageControl
            Left = 0
            Top = 60
            Width = 965
            Height = 229
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet3
            Align = alClient
            TabOrder = 1
            ExplicitHeight = 232
            object TabSheet2: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Receitas normais'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGFiltrado: TDBGrid
                Left = 0
                Top = 0
                Width = 823
                Height = 191
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsRecDiaIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -15
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Data'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERELACIONADO'
                    Title.Caption = 'Empresa'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Descricao'
                    Title.Caption = 'Descri'#231#227'o'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaFiscal'
                    Title.Caption = 'N.F.'
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'Duplicata'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Debito'
                    Title.Caption = 'D'#233'bito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Credito'
                    Title.Caption = 'Cr'#233'dito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Title.Caption = 'Vencim.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COMPENSADO_TXT'
                    Title.Caption = 'Compen.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MENSAL'
                    Title.Caption = 'M'#234's'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESIT'
                    Title.Caption = 'Situa'#231#227'o'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'amento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Sub'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Title.Caption = 'Saldo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_P'
                    Title.Caption = '% ICMS'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_V'
                    Title.Caption = '$ ICMS'
                    Visible = True
                  end>
              end
            end
            object TabSheet3: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Receitas com compet'#234'ncia mensal'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid1: TDBGrid
                Left = 0
                Top = 0
                Width = 953
                Height = 191
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsRecMesIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -15
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Data'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERELACIONADO'
                    Title.Caption = 'Empresa'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Descricao'
                    Title.Caption = 'Descri'#231#227'o'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaFiscal'
                    Title.Caption = 'N.F.'
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'Duplicata'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Debito'
                    Title.Caption = 'D'#233'bito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Credito'
                    Title.Caption = 'Cr'#233'dito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Title.Caption = 'Vencim.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COMPENSADO_TXT'
                    Title.Caption = 'Compen.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MENSAL'
                    Title.Caption = 'M'#234's'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESIT'
                    Title.Caption = 'Situa'#231#227'o'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'amento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Sub'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Title.Caption = 'Saldo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_P'
                    Title.Caption = '% ICMS'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_V'
                    Title.Caption = '$ ICMS'
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Despesas'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 965
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Enabled = False
            TabOrder = 0
            ExplicitWidth = 962
            object Label8: TLabel
              Left = 10
              Top = 5
              Width = 54
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Normais:'
            end
            object Label9: TLabel
              Left = 138
              Top = 5
              Width = 83
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Compet'#234'ncia:'
            end
            object Label10: TLabel
              Left = 266
              Top = 5
              Width = 114
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Total despesas:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBEdit04: TDBEdit
              Left = 10
              Top = 25
              Width = 123
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Debito'
              DataSource = DsDesDiaTot
              TabOrder = 0
            end
            object DBEdit05: TDBEdit
              Left = 138
              Top = 25
              Width = 123
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Debito'
              DataSource = DsDesMesTot
              TabOrder = 1
            end
            object DBEdit06: TDBEdit
              Left = 266
              Top = 25
              Width = 123
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'TOTAL'
              DataSource = DsDesMesTot
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
          end
          object PageControl3: TPageControl
            Left = 0
            Top = 60
            Width = 965
            Height = 229
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet6
            Align = alClient
            TabOrder = 1
            ExplicitHeight = 232
            object TabSheet5: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Despesas normais'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid2: TDBGrid
                Left = 0
                Top = 0
                Width = 823
                Height = 191
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsDesDiaIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -15
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Data'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERELACIONADO'
                    Title.Caption = 'Empresa'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Descricao'
                    Title.Caption = 'Descri'#231#227'o'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaFiscal'
                    Title.Caption = 'N.F.'
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'Duplicata'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Debito'
                    Title.Caption = 'D'#233'bito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Credito'
                    Title.Caption = 'Cr'#233'dito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Title.Caption = 'Vencim.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COMPENSADO_TXT'
                    Title.Caption = 'Compen.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MENSAL'
                    Title.Caption = 'M'#234's'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESIT'
                    Title.Caption = 'Situa'#231#227'o'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'amento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Sub'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Title.Caption = 'Saldo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_P'
                    Title.Caption = '% ICMS'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_V'
                    Title.Caption = '$ ICMS'
                    Visible = True
                  end>
              end
            end
            object TabSheet6: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Despesas com compet'#234'ncia mensal'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGrid3: TDBGrid
                Left = 0
                Top = 0
                Width = 953
                Height = 191
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsDesMesIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -15
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Data'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERELACIONADO'
                    Title.Caption = 'Empresa'
                    Width = 140
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Descricao'
                    Title.Caption = 'Descri'#231#227'o'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NotaFiscal'
                    Title.Caption = 'N.F.'
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'Duplicata'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Debito'
                    Title.Caption = 'D'#233'bito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Credito'
                    Title.Caption = 'Cr'#233'dito'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Vencimento'
                    Title.Caption = 'Vencim.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'COMPENSADO_TXT'
                    Title.Caption = 'Compen.'
                    Width = 51
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MENSAL'
                    Title.Caption = 'M'#234's'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESIT'
                    Title.Caption = 'Situa'#231#227'o'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lan'#231'amento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Sub'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Title.Caption = 'Saldo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_P'
                    Title.Caption = '% ICMS'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ICMS_V'
                    Title.Caption = '$ ICMS'
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet7: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Salas'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 962
            Height = 286
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 962
              Height = 286
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = 'Panel7'
              TabOrder = 0
              object Panel8: TPanel
                Left = 1
                Top = 1
                Width = 960
                Height = 74
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object GroupBox1: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 74
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = 'Lota'#231#227'o: '
                  TabOrder = 0
                  object Label11: TLabel
                    Left = 9
                    Top = 20
                    Width = 44
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Alunos:'
                  end
                  object DBEdit07: TDBEdit
                    Left = 10
                    Top = 39
                    Width = 80
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'Lotacao'
                    DataSource = DsSalasTot
                    TabOrder = 0
                  end
                end
                object GroupBox2: TGroupBox
                  Left = 100
                  Top = 0
                  Width = 275
                  Height = 74
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = ' '#193'rea: '
                  TabOrder = 1
                  object Label17: TLabel
                    Left = 94
                    Top = 20
                    Width = 65
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Custo total:'
                  end
                  object Label12: TLabel
                    Left = 10
                    Top = 20
                    Width = 18
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'm'#178':'
                  end
                  object Label18: TLabel
                    Left = 192
                    Top = 20
                    Width = 55
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Custo m'#178':'
                  end
                  object DBEdit010: TDBEdit
                    Left = 94
                    Top = 39
                    Width = 94
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'Debito'
                    DataSource = DsSDArea
                    TabOrder = 0
                  end
                  object DBEdit08: TDBEdit
                    Left = 10
                    Top = 39
                    Width = 80
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'Area'
                    DataSource = DsSalasTot
                    TabOrder = 1
                  end
                  object DBEdit011: TDBEdit
                    Left = 192
                    Top = 39
                    Width = 80
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'CUSTO_AREA'
                    DataSource = DsSalasTot
                    TabOrder = 2
                  end
                end
                object GroupBox3: TGroupBox
                  Left = 375
                  Top = 0
                  Width = 453
                  Height = 74
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  Caption = ' Energia: '
                  TabOrder = 2
                  object Label13: TLabel
                    Left = 5
                    Top = 20
                    Width = 44
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'watts/h:'
                  end
                  object Label20: TLabel
                    Left = 172
                    Top = 20
                    Width = 74
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '$ Calculado:'
                  end
                  object Label22: TLabel
                    Left = 354
                    Top = 20
                    Width = 77
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '$ Excedente:'
                  end
                  object Label23: TLabel
                    Left = 266
                    Top = 20
                    Width = 65
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Custo total:'
                  end
                  object Label24: TLabel
                    Left = 89
                    Top = 20
                    Width = 47
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'kw total:'
                  end
                  object DBEdit09: TDBEdit
                    Left = 5
                    Top = 39
                    Width = 80
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'wats_h'
                    DataSource = DsSalasTot
                    TabOrder = 0
                  end
                  object DBEdit013: TDBEdit
                    Left = 172
                    Top = 39
                    Width = 85
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'CUSTO_WATS'
                    DataSource = DsSalasTot
                    TabOrder = 1
                  end
                  object DBEdit014: TDBEdit
                    Left = 354
                    Top = 39
                    Width = 85
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'EXCEDEWATS'
                    DataSource = DsSalasTot
                    TabOrder = 2
                  end
                  object DBEdit015: TDBEdit
                    Left = 266
                    Top = 39
                    Width = 85
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'Debito'
                    DataSource = DsSDkw
                    TabOrder = 3
                  end
                  object DBEdit016: TDBEdit
                    Left = 89
                    Top = 39
                    Width = 80
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    DataField = 'TOTAL'
                    DataSource = DsSOcuT
                    TabOrder = 4
                  end
                end
              end
              object Panel9: TPanel
                Left = 1
                Top = 75
                Width = 960
                Height = 209
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Panel10: TPanel
                  Left = 0
                  Top = 0
                  Width = 960
                  Height = 113
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  TabOrder = 0
                  object Panel12: TPanel
                    Left = 1
                    Top = 1
                    Width = 958
                    Height = 22
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvLowered
                    Caption = 'Ocupadas'
                    TabOrder = 0
                  end
                  object DBGrid4: TDBGrid
                    Left = 1
                    Top = 23
                    Width = 958
                    Height = 89
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsSalas
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -15
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Width = 120
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Lotacao'
                        Title.Alignment = taRightJustify
                        Title.Caption = 'Lota'#231#227'o'
                        Width = 50
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Area'
                        Title.Alignment = taRightJustify
                        Title.Caption = #193'rea m'#178
                        Width = 51
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CUSTO_AREA'
                        Title.Alignment = taRightJustify
                        Title.Caption = '$ '#193'rea'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'wats_h'
                        Title.Alignment = taRightJustify
                        Title.Caption = 'watts/h'
                        Width = 45
                        Visible = True
                      end
                      item
                        Alignment = taRightJustify
                        Expanded = False
                        FieldName = 'HORAS_OCUP'
                        Title.Alignment = taRightJustify
                        Title.Caption = 'Ocup. h '
                        Width = 48
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CUSTO_HORAAREA'
                        Title.Alignment = taRightJustify
                        Title.Caption = #193'rea $/h'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CUSTO_WATS'
                        Title.Alignment = taRightJustify
                        Title.Caption = '$ Energia'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CUSTO_WATSHORA'
                        Title.Alignment = taRightJustify
                        Title.Caption = '$ Energ./h'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CUSTO_WATSMINUTO'
                        Title.Caption = '$ kw/min'
                        Visible = True
                      end>
                  end
                end
                object Panel11: TPanel
                  Left = 0
                  Top = 113
                  Width = 960
                  Height = 96
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  TabOrder = 1
                  object Panel13: TPanel
                    Left = 1
                    Top = 1
                    Width = 958
                    Height = 22
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvLowered
                    Caption = 'Desocupadas'
                    TabOrder = 0
                  end
                  object DBGrid5: TDBGrid
                    Left = 1
                    Top = 23
                    Width = 958
                    Height = 72
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsSDesoc
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -15
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Width = 120
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Lotacao'
                        Title.Alignment = taRightJustify
                        Title.Caption = 'Lota'#231#227'o'
                        Width = 50
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Area'
                        Title.Alignment = taRightJustify
                        Title.Caption = #193'rea m'#178
                        Width = 51
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'wats_h'
                        Title.Alignment = taRightJustify
                        Title.Caption = 'watts/h'
                        Width = 45
                        Visible = True
                      end>
                  end
                end
              end
            end
          end
        end
        object TabSheet8: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Professores'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 962
            Height = 74
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox4: TGroupBox
              Left = 0
              Top = 0
              Width = 513
              Height = 74
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = '?: '
              Enabled = False
              TabOrder = 0
              object Label25: TLabel
                Left = 9
                Top = 20
                Width = 66
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Despesas:'
              end
              object DBEdit017: TDBEdit
                Left = 10
                Top = 39
                Width = 80
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'Debito'
                DataSource = DsProfDes
                TabOrder = 0
              end
            end
          end
          object DBGrid6: TDBGrid
            Left = 0
            Top = 74
            Width = 962
            Height = 212
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsProfIts
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Professor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEPROFESSOR'
                Title.Caption = 'Nome do professor'
                Width = 190
                Visible = True
              end
              item
                Alignment = taRightJustify
                Expanded = False
                FieldName = 'CARGA_HORAS'
                Title.Alignment = taCenter
                Title.Caption = 'Horas'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUSTOS'
                Title.Alignment = taRightJustify
                Title.Caption = 'Total custos'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUSTO_HORA'
                Title.Alignment = taRightJustify
                Title.Caption = '$/h'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CUSTO_MINUTO'
                Title.Alignment = taRightJustify
                Title.Caption = '$/min'
                Visible = True
              end>
          end
        end
        object TabSheet9: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Gerais'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TabSheet10: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cursos'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid7: TDBGrid
            Left = 0
            Top = 0
            Width = 962
            Height = 286
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsCur
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet11: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mat'#233'rias'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid8: TDBGrid
            Left = 0
            Top = 0
            Width = 962
            Height = 286
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsMat
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet12: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Turmas'
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid9: TDBGrid
            Left = 0
            Top = 0
            Width = 962
            Height = 286
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsTur
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Resultados Mensais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 863
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 585
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill002: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsMensais: TDataSource
    DataSet = QrMensais
    Left = 748
    Top = 17
  end
  object QrMensais: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMensaisBeforeOpen
    AfterOpen = QrMensaisAfterOpen
    AfterScroll = QrMensaisAfterScroll
    OnCalcFields = QrMensaisCalcFields
    SQL.Strings = (
      'SELECT * FROM mensais'
      'WHERE Codigo > 0')
    Left = 720
    Top = 17
    object QrMensaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMensaisEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrMensaisReceitas: TFloatField
      FieldName = 'Receitas'
    end
    object QrMensaisDespesas: TFloatField
      FieldName = 'Despesas'
    end
    object QrMensaisLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMensaisDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMensaisDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMensaisUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMensaisUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMensaisMES2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES2'
      Size = 30
      Calculated = True
    end
    object QrMensaisCustokw: TFloatField
      FieldName = 'Custokw'
      DisplayFormat = '#,###,##0.0000'
    end
  end
  object QrLocCodigo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM mensais'
      'WHERE Codigo=:P0'
      '')
    Left = 128
    Top = 124
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCodigoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsRecMesIts: TDataSource
    DataSet = QrRecMesIts
    Left = 49
    Top = 337
  end
  object QrRecMesTot: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRecMesTotCalcFields
    SQL.Strings = (
      'SELECT SUM(Credito) Credito'
      'FROM lanctos'
      'WHERE (Genero <-99 or Genero>0) '
      'AND Mez=:P0'
      'AND Credito>0')
    Left = 21
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRecMesTotCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRecMesTotTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsRecMesTot: TDataSource
    DataSet = QrRecMesTot
    Left = 49
    Top = 365
  end
  object QrRecMesIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRecMesItsCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN ci.Tipo=0 THEN ci.RazaoSocial '
      'ELSE ci.Nome END NOMECLIENTEINTERNO,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades ci ON ci.Codigo=la.CliInt'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE (la.Genero <-99 or la.Genero>0) '
      'AND la.Mez=:P0'
      'AND la.Credito>0'
      'ORDER BY la.Data, la.Controle')
    Left = 21
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRecMesItsData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRecMesItsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrRecMesItsCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrRecMesItsAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrRecMesItsGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrRecMesItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrRecMesItsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrRecMesItsDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecMesItsCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecMesItsCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRecMesItsDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrRecMesItsSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrRecMesItsVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRecMesItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrRecMesItsFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrRecMesItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrRecMesItsCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrRecMesItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrRecMesItsNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrRecMesItsNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrRecMesItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrRecMesItsNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrRecMesItsNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrRecMesItsAno: TFloatField
      FieldName = 'Ano'
    end
    object QrRecMesItsMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrRecMesItsMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrRecMesItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrRecMesItsLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrRecMesItsFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrRecMesItsSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrRecMesItsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrRecMesItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrRecMesItsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrRecMesItsSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrRecMesItsID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrRecMesItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrRecMesItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrRecMesItscliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrRecMesItsMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrRecMesItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrRecMesItsNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrRecMesItsTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrRecMesItsNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrRecMesItsOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrRecMesItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrRecMesItsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrRecMesItsATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrRecMesItsJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrRecMesItsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrRecMesItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrRecMesItsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrRecMesItsAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrRecMesItsMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrRecMesItsProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrRecMesItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRecMesItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRecMesItsUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrRecMesItsUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrRecMesItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRecMesItsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrRecMesItsCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrRecMesItsFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrRecMesItsICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecMesItsICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecMesItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrRecMesItsCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrRecMesItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrRecMesItsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrRecMesItsSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRecMesItsNOMECLIENTEINTERNO: TWideStringField
      FieldName = 'NOMECLIENTEINTERNO'
      Size = 100
    end
    object QrRecMesItsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrRecMesItsPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrRecMesItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrRecDiaIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRecDiaItsCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN ci.Tipo=0 THEN ci.RazaoSocial '
      'ELSE ci.Nome END NOMECLIENTEINTERNO,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades ci ON ci.Codigo=la.CliInt'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE (la.Genero <-99 or la.Genero>0)'
      'AND la.Mez=0'
      'AND la.Credito>0'
      'AND la.Data BETWEEN :P0 AND :P1'
      'ORDER BY la.Data, la.Controle')
    Left = 21
    Top = 281
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrRecDiaItsData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRecDiaItsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrRecDiaItsCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrRecDiaItsAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrRecDiaItsGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrRecDiaItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrRecDiaItsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrRecDiaItsDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecDiaItsCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecDiaItsCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRecDiaItsDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrRecDiaItsSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrRecDiaItsVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRecDiaItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrRecDiaItsFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrRecDiaItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrRecDiaItsCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrRecDiaItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrRecDiaItsNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrRecDiaItsNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrRecDiaItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrRecDiaItsNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrRecDiaItsNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrRecDiaItsAno: TFloatField
      FieldName = 'Ano'
    end
    object QrRecDiaItsMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrRecDiaItsMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrRecDiaItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrRecDiaItsLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrRecDiaItsFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrRecDiaItsSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrRecDiaItsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrRecDiaItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrRecDiaItsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrRecDiaItsSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrRecDiaItsID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrRecDiaItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrRecDiaItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrRecDiaItscliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrRecDiaItsMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrRecDiaItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrRecDiaItsNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrRecDiaItsTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrRecDiaItsNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrRecDiaItsOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrRecDiaItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrRecDiaItsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrRecDiaItsATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrRecDiaItsJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrRecDiaItsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrRecDiaItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrRecDiaItsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrRecDiaItsAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrRecDiaItsMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrRecDiaItsProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrRecDiaItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRecDiaItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRecDiaItsUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrRecDiaItsUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrRecDiaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRecDiaItsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrRecDiaItsCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrRecDiaItsFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrRecDiaItsICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecDiaItsICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrRecDiaItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrRecDiaItsCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrRecDiaItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrRecDiaItsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrRecDiaItsSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRecDiaItsNOMECLIENTEINTERNO: TWideStringField
      FieldName = 'NOMECLIENTEINTERNO'
      Size = 100
    end
    object QrRecDiaItsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrRecDiaItsPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrRecDiaItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsRecDiaIts: TDataSource
    DataSet = QrRecDiaIts
    Left = 49
    Top = 281
  end
  object DsRecDiaTot: TDataSource
    DataSet = QrRecDiaTot
    Left = 49
    Top = 309
  end
  object QrRecDiaTot: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito'
      'FROM lanctos'
      'WHERE (Genero <-99 or Genero>0)'
      'AND Mez=0'
      'AND Data BETWEEN :P0 AND :P1')
    Left = 21
    Top = 309
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrRecDiaTotCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrDesDiaIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDesDiaItsCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN ci.Tipo=0 THEN ci.RazaoSocial '
      'ELSE ci.Nome END NOMECLIENTEINTERNO,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades ci ON ci.Codigo=la.CliInt'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE (la.Genero <-99 or la.Genero>0)'
      'AND la.Mez=0'
      'AND la.Debito>0'
      'AND la.Data BETWEEN :P0 AND :P1'
      'ORDER BY la.Data, la.Controle')
    Left = 85
    Top = 281
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDesDiaItsData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDesDiaItsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrDesDiaItsCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrDesDiaItsAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrDesDiaItsGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrDesDiaItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrDesDiaItsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrDesDiaItsDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesDiaItsCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesDiaItsCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDesDiaItsDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrDesDiaItsSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrDesDiaItsVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDesDiaItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrDesDiaItsFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrDesDiaItsFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrDesDiaItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrDesDiaItsCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrDesDiaItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrDesDiaItsNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrDesDiaItsNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrDesDiaItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrDesDiaItsNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrDesDiaItsNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrDesDiaItsAno: TFloatField
      FieldName = 'Ano'
    end
    object QrDesDiaItsMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrDesDiaItsMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrDesDiaItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrDesDiaItsLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrDesDiaItsFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrDesDiaItsSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrDesDiaItsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDesDiaItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrDesDiaItsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrDesDiaItsSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrDesDiaItsID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrDesDiaItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrDesDiaItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDesDiaItscliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrDesDiaItsMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrDesDiaItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrDesDiaItsNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrDesDiaItsTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrDesDiaItsNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrDesDiaItsOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrDesDiaItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrDesDiaItsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrDesDiaItsATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrDesDiaItsJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDesDiaItsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrDesDiaItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrDesDiaItsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrDesDiaItsAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrDesDiaItsMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrDesDiaItsProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrDesDiaItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDesDiaItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDesDiaItsUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrDesDiaItsUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrDesDiaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDesDiaItsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrDesDiaItsCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrDesDiaItsFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrDesDiaItsICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesDiaItsICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesDiaItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrDesDiaItsCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDesDiaItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDesDiaItsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrDesDiaItsSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrDesDiaItsNOMECLIENTEINTERNO: TWideStringField
      FieldName = 'NOMECLIENTEINTERNO'
      Size = 100
    end
    object QrDesDiaItsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDesDiaItsPrazo: TSmallintField
      FieldName = 'Prazo'
    end
  end
  object DsDesDiaIts: TDataSource
    DataSet = QrDesDiaIts
    Left = 113
    Top = 281
  end
  object DsDesDiaTot: TDataSource
    DataSet = QrDesDiaTot
    Left = 113
    Top = 309
  end
  object QrDesDiaTot: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Debito'
      'FROM lanctos'
      'WHERE (Genero <-99 or Genero>0)'
      'AND Mez=0'
      'AND Data BETWEEN :P0 AND :P1')
    Left = 85
    Top = 309
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDesDiaTotDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrDesMesIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDesMesItsCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN ci.Tipo=0 THEN ci.RazaoSocial '
      'ELSE ci.Nome END NOMECLIENTEINTERNO,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades ci ON ci.Codigo=la.CliInt'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE (la.Genero <-99 or la.Genero>0) '
      'AND la.Mez=:P0'
      'AND la.Debito>0'
      'ORDER BY la.Data, la.Controle')
    Left = 85
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDesMesItsData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDesMesItsTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrDesMesItsCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrDesMesItsAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrDesMesItsGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrDesMesItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrDesMesItsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrDesMesItsDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesMesItsCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesMesItsCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDesMesItsDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrDesMesItsSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrDesMesItsVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDesMesItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrDesMesItsFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrDesMesItsFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrDesMesItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrDesMesItsCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrDesMesItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrDesMesItsNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrDesMesItsNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrDesMesItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrDesMesItsNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrDesMesItsNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrDesMesItsAno: TFloatField
      FieldName = 'Ano'
    end
    object QrDesMesItsMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrDesMesItsMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrDesMesItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrDesMesItsLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrDesMesItsFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrDesMesItsSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrDesMesItsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDesMesItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrDesMesItsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrDesMesItsSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrDesMesItsID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrDesMesItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrDesMesItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDesMesItscliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrDesMesItsMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrDesMesItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrDesMesItsNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrDesMesItsTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrDesMesItsNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrDesMesItsOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrDesMesItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrDesMesItsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrDesMesItsATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrDesMesItsJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDesMesItsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrDesMesItsNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrDesMesItsVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrDesMesItsAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrDesMesItsMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrDesMesItsProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrDesMesItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDesMesItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDesMesItsUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrDesMesItsUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrDesMesItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDesMesItsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrDesMesItsCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrDesMesItsFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrDesMesItsICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesMesItsICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDesMesItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrDesMesItsCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDesMesItsCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDesMesItsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrDesMesItsSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrDesMesItsNOMECLIENTEINTERNO: TWideStringField
      FieldName = 'NOMECLIENTEINTERNO'
      Size = 100
    end
    object QrDesMesItsDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDesMesItsPrazo: TSmallintField
      FieldName = 'Prazo'
    end
  end
  object DsDesMesIts: TDataSource
    DataSet = QrDesMesIts
    Left = 113
    Top = 337
  end
  object DsDesMesTot: TDataSource
    DataSet = QrDesMesTot
    Left = 113
    Top = 365
  end
  object QrDesMesTot: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDesMesTotCalcFields
    SQL.Strings = (
      'SELECT SUM(Debito) Debito'
      'FROM lanctos'
      'WHERE (Genero <-99 or Genero>0) '
      'AND Mez=:P0'
      'AND Debito>0')
    Left = 85
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDesMesTotTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDesMesTotDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrSalas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSalasCalcFields
    SQL.Strings = (
      'SELECT * FROM salas'
      'WHERE Codigo in ('
      ''
      ')'
      'ORDER BY Nome')
    Left = 149
    Top = 281
    object QrSalasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSalasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSalasLotacao: TIntegerField
      FieldName = 'Lotacao'
    end
    object QrSalasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSalasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSalasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSalasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSalasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSalasArea: TFloatField
      FieldName = 'Area'
      DisplayFormat = '#,##0.00'
    end
    object QrSalaswats_h: TFloatField
      FieldName = 'wats_h'
      DisplayFormat = '#,###,##0'
    end
    object QrSalasDepartamento: TIntegerField
      FieldName = 'Departamento'
    end
    object QrSalasCUSTO_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSalasOCUPACAO: TIntegerField
      FieldKind = fkLookup
      FieldName = 'OCUPACAO'
      LookupDataSet = QrSOcup
      LookupKeyFields = 'Sala'
      LookupResultField = 'CARGA'
      KeyFields = 'Codigo'
      Lookup = True
    end
    object QrSalasHORAS_OCUP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HORAS_OCUP'
      Size = 10
      Calculated = True
    end
    object QrSalasCUSTO_MINUTOAREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_MINUTOAREA'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrSalasCUSTO_HORAAREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_HORAAREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSalasCUSTO_HORATOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_HORATOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSalasCUSTO_WATS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_WATS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSalasCUSTO_WATSHORA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_WATSHORA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSalasCUSTO_WATSMINUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_WATSMINUTO'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
  end
  object DsSalas: TDataSource
    DataSet = QrSalas
    Left = 177
    Top = 281
  end
  object QrSalasTot: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSalasTotCalcFields
    SQL.Strings = (
      'SELECT SUM(Lotacao) Lotacao,'
      'SUM(Area) Area, SUM(wats_h) wats_h'
      'FROM salas'
      'WHERE Codigo in ('
      ''
      ')')
    Left = 149
    Top = 309
    object QrSalasTotLotacao: TFloatField
      FieldName = 'Lotacao'
      DisplayFormat = '#,###,##0'
    end
    object QrSalasTotArea: TFloatField
      FieldName = 'Area'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSalasTotwats_h: TFloatField
      FieldName = 'wats_h'
      DisplayFormat = '#,###,##0'
    end
    object QrSalasTotCUSTO_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSalasTotCUSTO_WATS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_WATS'
      Calculated = True
    end
    object QrSalasTotEXCEDEWATS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EXCEDEWATS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsSalasTot: TDataSource
    DataSet = QrSalasTot
    Left = 177
    Top = 309
  end
  object QrSDArea: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(la.Debito) Debito'
      'FROM lanctos la'
      'LEFT JOIN contas co ON co.Codigo=la.Genero'
      'WHERE (la.Genero <-99 or la.Genero>0)'
      'AND (la.Mez=:P0 OR (la.Mez=0 AND la.Data BETWEEN :P1 AND :P2))'
      'AND co.Rateio=1')
    Left = 149
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSDAreaDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSDArea: TDataSource
    DataSet = QrSDArea
    Left = 177
    Top = 337
  end
  object QrSOcup: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ch.Sala, SUM(ch.Carga) CARGA'
      'FROM chamadas ch'
      'WHERE ch.Data BETWEEN :P0 AND :P1'
      'GROUP BY Sala')
    Left = 205
    Top = 281
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSOcupSala: TIntegerField
      FieldName = 'Sala'
      Required = True
    end
    object QrSOcupCARGA: TFloatField
      FieldName = 'CARGA'
    end
  end
  object DsSOcup: TDataSource
    DataSet = QrSOcup
    Left = 233
    Top = 281
  end
  object QrSDesoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM salas'
      'WHERE Codigo not in ('
      ''
      ')'
      'ORDER BY Nome')
    Left = 149
    Top = 365
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Lotacao'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Lk'
    end
    object DateField1: TDateField
      FieldName = 'DataCad'
    end
    object DateField2: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField4: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField5: TIntegerField
      FieldName = 'UserAlt'
    end
    object FloatField1: TFloatField
      FieldName = 'Area'
      DisplayFormat = '#,##0.00'
    end
    object FloatField2: TFloatField
      FieldName = 'wats_h'
      DisplayFormat = '#,###,##0'
    end
    object IntegerField6: TIntegerField
      FieldName = 'Departamento'
    end
  end
  object DsSDesoc: TDataSource
    DataSet = QrSDesoc
    Left = 177
    Top = 365
  end
  object QrSDkw: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(la.Debito) Debito'
      'FROM lanctos la'
      'LEFT JOIN contas co ON co.Codigo=la.Genero'
      'WHERE (la.Genero <-99 or la.Genero>0)'
      'AND (la.Mez=:P0 OR (la.Mez=0 AND la.Data BETWEEN :P1 AND :P2))'
      'AND co.Rateio=2')
    Left = 205
    Top = 309
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSDkwDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSDkw: TDataSource
    DataSet = QrSDkw
    Left = 233
    Top = 309
  end
  object QrSOcuT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ch.Carga*sa.wats_h/60000) TOTAL'
      'FROM chamadas ch'
      'LEFT JOIN salas sa ON sa.Codigo=ch.Sala'
      'WHERE ch.Data BETWEEN :P0 AND :P1')
    Left = 205
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSOcuTTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object DsSOcuT: TDataSource
    DataSet = QrSOcuT
    Left = 233
    Top = 337
  end
  object QrProfDes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(la.Debito) Debito'
      'FROM lanctos la'
      'LEFT JOIN contas co ON co.Codigo=la.Genero'
      'WHERE (la.Genero <-99 or la.Genero>0)'
      'AND (la.Mez=:P0 OR (la.Mez=0 AND la.Data BETWEEN :P1 AND :P2))'
      'AND co.Rateio=3')
    Left = 269
    Top = 281
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrProfDesDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsProfDes: TDataSource
    DataSet = QrProfDes
    Left = 297
    Top = 281
  end
  object DsProfIts: TDataSource
    DataSet = QrProfIts
    Left = 297
    Top = 309
  end
  object QrProfIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProfItsCalcFields
    SQL.Strings = (
      'SELECT ch.Professor, SUM(ch.Carga) Carga,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEPROFESSOR'
      'FROM chamadas ch'
      'LEFT JOIN entidades en ON en.Codigo=ch.Professor'
      'WHERE ch.Data BETWEEN :P0 AND :P1'
      'GROUP BY ch.Professor'
      'ORDER BY NOMEPROFESSOR')
    Left = 269
    Top = 309
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProfItsProfessor: TIntegerField
      FieldName = 'Professor'
    end
    object QrProfItsCarga: TFloatField
      FieldName = 'Carga'
    end
    object QrProfItsNOMEPROFESSOR: TWideStringField
      FieldName = 'NOMEPROFESSOR'
      Size = 100
    end
    object QrProfItsCUSTOS: TFloatField
      FieldKind = fkLookup
      FieldName = 'CUSTOS'
      LookupDataSet = QrProfSal
      LookupKeyFields = 'Entidade'
      LookupResultField = 'Debito'
      KeyFields = 'Professor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrProfItsCUSTO_MINUTO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_MINUTO'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
      Calculated = True
    end
    object QrProfItsCUSTO_HORA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSTO_HORA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrProfItsCARGA_HORAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CARGA_HORAS'
      Size = 10
      Calculated = True
    end
  end
  object QrProfSal: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProfItsCalcFields
    SQL.Strings = (
      'SELECT co.Entidade, SUM(la.Debito) Debito'
      'FROM lanctos la'
      'LEFT JOIN contas co ON co.Codigo=la.Genero'
      'WHERE (la.Genero <-99 or la.Genero>0)'
      'AND (la.Mez=:P0 OR (la.Mez=0 AND la.Data BETWEEN :P1 AND :P2))'
      'AND co.Rateio=3'
      'GROUP BY co.Entidade')
    Left = 269
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrProfSalEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrProfSalDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object DsProfSal: TDataSource
    DataSet = QrProfSal
    Left = 297
    Top = 337
  end
  object QrCursos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Curso '
      'FROM chamadas'
      'WHERE Data BETWEEN :P0 AND :P1')
    Left = 397
    Top = 281
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCursosCurso: TIntegerField
      FieldName = 'Curso'
    end
  end
  object DsCursos: TDataSource
    DataSet = QrCursos
    Left = 425
    Top = 281
  end
  object DsCur: TDataSource
    DataSet = QrCur
    Left = 425
    Top = 309
  end
  object QrCur: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM cursos'
      ''
      'ORDER BY Nome')
    Left = 397
    Top = 309
    object QrCurCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCurNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCurPC2: TFloatField
      FieldKind = fkLookup
      FieldName = 'PC'
      LookupDataSet = QrCurPC
      LookupKeyFields = 'Curso'
      LookupResultField = 'CREDITO'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
  end
  object QrCurPC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ma.Curso, SUM(Credito) CREDITO '
      'FROM lanctos la'
      'LEFT JOIN matriculas  ma ON ma.Codigo=la.FatNum'
      'WHERE la.FatID=2101'
      'AND la.Vencimento BETWEEN :P0 AND :P1'
      'GROUP BY ma.Curso')
    Left = 397
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCurPCCurso: TIntegerField
      FieldName = 'Curso'
    end
    object QrCurPCCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
  end
  object DsCurPC: TDataSource
    DataSet = QrCurPC
    Left = 425
    Top = 337
  end
  object QrMatPC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ti.Curso, SUM(la.Credito*ti.Peso/tu.TotalPeso*mc.Fator/ma' +
        '.TotalFator) Credito'
      'FROM lanctos la'
      'LEFT JOIN matriculas   ma ON ma.Codigo=la.FatNum'
      'LEFT JOIN matricursos  mc ON mc.Codigo=ma.Codigo'
      'LEFT JOIN turmas       tu ON tu.Codigo=mc.Turma'
      'LEFT JOIN turmasits    ti ON ti.Codigo=tu.Codigo'
      'WHERE la.FatID=2101'
      'AND la.Vencimento BETWEEN :P0 AND :P1'
      'GROUP BY ti.Curso')
    Left = 461
    Top = 337
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMatPCCurso: TIntegerField
      FieldName = 'Curso'
    end
    object QrMatPCCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object DsMatPC: TDataSource
    DataSet = QrMatPC
    Left = 489
    Top = 337
  end
  object QrMat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM cursos'
      ''
      'ORDER BY Nome')
    Left = 461
    Top = 309
    object QrMatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMatNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrMatPC2: TFloatField
      FieldKind = fkLookup
      FieldName = 'PC'
      LookupDataSet = QrMatPC
      LookupKeyFields = 'Curso'
      LookupResultField = 'Credito'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00'
      Lookup = True
    end
  end
  object DsMat: TDataSource
    DataSet = QrMat
    Left = 489
    Top = 309
  end
  object QrTur: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tu.Codigo, tu.Nome,'
      'SUM(la.Credito*mc.Fator/ma.TotalFator) Credito'
      'FROM lanctos la'
      'LEFT JOIN matriculas   ma ON ma.Codigo=la.FatNum'
      'LEFT JOIN matricursos  mc ON mc.Codigo=ma.Codigo'
      'LEFT JOIN turmas       tu ON tu.Codigo=mc.Turma'
      'WHERE la.FatID=2101'
      'AND la.Vencimento BETWEEN :P0 AND :P1'
      'GROUP BY tu.Codigo')
    Left = 525
    Top = 309
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTurCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTurNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTurCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object DsTur: TDataSource
    DataSet = QrTur
    Left = 553
    Top = 309
  end
end
