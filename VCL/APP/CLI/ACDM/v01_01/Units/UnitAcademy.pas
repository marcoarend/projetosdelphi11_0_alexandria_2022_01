unit UnitAcademy;


interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, dmkGeral, UnDmkEnums;

type
  epPesoValor = (epPeso, epValor, epNenhum);
  epHistTipo  = (epSintetico, epAnalitico);
  MyArrayD12 =  array[1..12] of Real;
  //TTipoAviso = (aeMsg, aeMemo, aeNenhum);
  TUnitAcademy = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //procedure AtualizaEstoque(Produto: String);
    //
    function ImpedePeloBalanco(Data: TDateTime): Boolean;
    function VerificaBalanco: Integer;
    function AtualizaEstoqueMercadoria(Mercadoria: Integer; TipoAviso:
             TTipoAviso; NomeCompo: String): Boolean;
    //function AtualizaEntidade(Entidade: Integer): Boolean;
    function SQLAtualizaEstoque(PesoEstq, ValorEstq, ValorKg,
             Desperdicio: Double; Codigo: Integer; TipoAviso: TTipoAviso;
             NomeCompo: String): Boolean;
    function VerificaEstoqueMercadoria(Mercadoria: String; Data: TDate;
             Tipo: epHistTipo; IncBalDoDia: Boolean): MyArrayD12;

  end;

var
  UnAcademy: TUnitAcademy;

implementation

uses UnMyObjects, Module, GModule;

(*function TUnitAcademy.AtualizaEntidade(Entidade: Integer): Boolean;
begin
  try
    Dmod.QrUEntidade.Close;
    Dmod.QrUEntidade.Params[0].AsInteger := Entidade;
    Dmod.QrUEntidade.Open;
    //
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE entidades SET ');
    Dmod.QrUpdM.SQL.Add('QuantI2=QuantI1+:P0 ');
    Dmod.QrUpdM.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpdM.Params[0].AsInteger := Dmod.QrUEntidadeLocacoes.Value;
    Dmod.QrUpdM.Params[1].AsInteger := Entidade;
    Dmod.QrUpdM.ExecSQL;
    Result := True;
  except;
    raise;
    Result := False;
  end;
end;
*)

function TUnitAcademy.AtualizaEstoqueMercadoria(Mercadoria: Integer; TipoAviso:
TTipoAviso; NomeCompo: String): Boolean;
var
  Peso, Valor: Double;
  Data: String;
  PeriodoBal: Integer;
begin
  PeriodoBal := VerificaBalanco;
  Dmod.QrProduto.Close;
  Dmod.QrProduto.Params[0].AsInteger := Mercadoria;
  Dmod.QrProduto.Open;
  if Dmod.QrProdutoControla.Value = 'V' then
  begin
    if PeriodoBal = 0 then
    begin
      Data  := FormatDateTime(VAR_FORMATDATE, 2);
      Peso  := Dmod.QrProdutoEIniQ.Value;
      Valor := Dmod.QrProdutoEIniV.Value;
    end else begin
      Data := MLAGeral.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem); //2?
      Dmod.QrBalancosIts.Close;
      Dmod.QrBalancosIts.Params[0].AsInteger  := Mercadoria;
      Dmod.QrBalancosIts.Params[1].AsInteger := PeriodoBal;
      Dmod.QrBalancosIts.Open;
      Peso  := Dmod.QrBalancosItsEstqQ.Value;
      Valor := Dmod.QrBalancosItsEstqV.Value;
    end;
    //
    Dmod.QrEntrada.Close;
    Dmod.QrEntrada.Params[0].AsInteger := Mercadoria;
    Dmod.QrEntrada.Params[1].AsString := Data;
    Dmod.QrEntrada.Open;
    //
    Dmod.QrVendas.Close;
    Dmod.QrVendas.Params[0].AsInteger := Mercadoria;
    Dmod.QrVendas.Params[1].AsString := Data;
    Dmod.QrVendas.Open;
    //
    Peso := Peso + Dmod.QrEntradaPeso.Value - Dmod.QrVendasPeso.Value;
    //
    Valor := Valor + Dmod.QrEntradaValor.Value - Dmod.QrVendasValor.Value;
    //
  end else begin
    Peso  := 0;
    Valor := 0;
    TipoAviso := aeNenhum;
  end;
  Result := SQLAtualizaEstoque(Peso, Valor, Dmod.QrProdutoPrecoC.Value, 0,
            Mercadoria, TipoAviso, NomeCompo);
  ////////////////
  Dmod.QrEntrada.Close;
  Dmod.QrProduto.Close;
  Dmod.QrVendas.Close;
end;

function TUnitAcademy.SQLAtualizaEstoque(PesoEstq, ValorEstq, ValorKg,
 Desperdicio: Double; Codigo: Integer; TipoAviso: TTipoAviso;
 NomeCompo: String): Boolean;
var
  //CA, CB, Custo: Double;
  CUSTOMEDIO : Double;
  Erro: Boolean;
  Aviso: String;
  i: Integer;
begin
  //N�o calcula desperd�cio
  Erro := False;
  //Desperdicio := 0;
  if PesoEstq <> 0 then
    CUSTOMEDIO := ValorEstq / PesoEstq else CUSTOMEDIO := 0;
  //CA := CUSTOMEDIO / (1-(Desperdicio /100));// else CA := 0;
  //CB := ValorKg / (1-(Desperdicio /100));
  //if CA > 0 then Custo := CA else Custo := CB;
  Aviso := '';
  if Codigo > 0 then
  begin
    if CUSTOMEDIO < -0.00009 then
    begin
      Aviso := 'ERRO!!!! O custo da mercadoria n� '+IntToStr(Codigo)+' ficou negativo.'+
      ' Pre�o m�dio do estoque = '+Geral.TFT(FloatToStr(CUSTOMEDIO), 4, siNegativo);
      Erro := True;
    end
    else if (CUSTOMEDIO = 0) and (PesoEstq>0.0009) then
    begin
      Aviso := 'ERRO!!! O custo da mercadoria n� '+IntToStr(Codigo)+' ficou nulo.'+
      ' Pre�o m�dio do estoque = '+Geral.TFT(FloatToStr(CUSTOMEDIO), 4, siNegativo)+
      ' Quantidade em estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
      Erro := True;
    end else if (CUSTOMEDIO = 0) and (ValorEstq>0.00009) then
    begin
      Aviso := 'ERRO!!! O custo da mercadoria n� '+IntToStr(Codigo)+' ficou nulo.'+
      ' Pre�o m�dio do estoque = '+Geral.TFT(FloatToStr(CUSTOMEDIO), 4, siNegativo)+
      ' Valor  do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo);
      Erro := True;
    end else if (ValorEstq < -0.00009) and (PesoEstq < -0.0009) then
    begin
      Aviso := 'ERRO!!! A quantidade e o custo da mercadoria n� '+IntToStr(Codigo)+' ficaram negativo.'+
      ' Valor do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo)+
      ' Quantidade em estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
      Erro := True;
    end else if ValorEstq < -0.00009 then
    begin
      Aviso := 'ERRO!!! O valor da mercadoria n� '+IntToStr(Codigo)+' ficou negativo.'+
      ' Valor do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo)+
      ' Quantidade em estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
      Erro := True;
    end else if PesoEstq < -0.0009 then
    begin
      Aviso := 'ERRO!!! A quantidade da mercadoria n� '+IntToStr(Codigo)+' ficou negativo.'+
      ' Valor do estoque = '+Geral.TFT(FloatToStr(ValorEstq), 4, siNegativo)+
      ' Quantidade em estoque = '+Geral.TFT(FloatToStr(PesoEstq), 3, siNegativo);
      Erro := True;
    end;
    if Aviso <> '' then
    begin
      if TipoAviso = aeMemo then
      begin
        for i := 0 to Screen.ActiveForm.ComponentCount -1 do
        begin
          if (Screen.ActiveForm.Components[i] is TMemo) then
          if TMemo(Screen.ActiveForm.Components[i]).Name = NomeCompo then
          begin
            TMemo(Screen.ActiveForm.Components[i]).Lines.Add(Aviso);
            TMemo(Screen.ActiveForm.Components[i]).Refresh;
            Screen.ActiveForm.Update;
            Break;
          end;
        end;
      end;
      if TipoAviso = aeMsg then
        Geral.MB_Aviso(Aviso);
    end;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE produtos SET EstqQ=:P0, EstqV=:P1 ');
    Dmod.QrUpdU.SQL.Add('WHERE Codigo=:Px');
    Dmod.QrUpdU.Params[0].AsFloat   := PesoEstq;
    Dmod.QrUpdU.Params[1].AsFloat   := ValorEstq;
    Dmod.QrUpdU.Params[2].AsInteger := Codigo;
    Dmod.QrUpdU.ExecSQL;
    //
  end;
  Result := Erro;
end;

function TUnitAcademy.VerificaBalanco: Integer;
begin
  Dmod.QrPeriodoBal.Close;
  Dmod.QrPeriodoBal.Open;
  Result := Dmod.QrPeriodoBalPeriodo.Value;
  Dmod.QrPeriodoBal.Close;
end;

/////////////////////////////////////

function TUnitAcademy.ImpedePeloBalanco(Data: TDateTime): Boolean;
var
  Dia: TDateTime;
begin
  Result := True;
  Dia    := StrToDate(MLAGeral.PrimeiroDiaAposPeriodo(VerificaBalanco, dtSystem3));
  //
  if Data >= Dia then
    Result := False
  else
    Geral.MB_Aviso('Data inv�lida. J� existe balan�o com data posterior!');
  // USO:
//  if FmPrincipal.ImpedePeloBalanco(TPData.Date) then Exit;
end;

/////////////////////////////////////////////////

function TUnitAcademy.VerificaEstoqueMercadoria(Mercadoria: String; Data: TDate;
 Tipo: epHistTipo; IncBalDoDia: Boolean): MyArrayD12;
var
  Pecas, Valor, PesoI, ValorI, BalQ, BalV: Double;
  DataI, DataF: String;
  PeriodoBal: Integer;
  Ano, Mes, Dia: Word;
begin
  PesoI  := 0;
  ValorI := 0;
  Result[01] := 0;
  Result[02] := 0;
  Result[03] := 0;
  Result[04] := 0;
  Result[05] := 0;
  Result[06] := 0;
  Result[07] := 0;
  Result[08] := 0;
  Result[09] := 0;
  Result[10] := 0;
  Result[11] := 0;
  Result[12] := 0;
  QvAllY.Close;
  QvAllY.DataBase := DMod.MyDB;
  QvAllY.SQL.Clear;
  QvAllY.SQL.Add('SELECT MAX(Periodo) Periodo FROM balancos');
  QvAllY.SQL.Add('WHERE Periodo<:P0');
  QvAllY.Params[0].AsInteger := Geral.Periodo2000(Data);
  QvAllY.Open;
  PeriodoBal := QvAllY.FieldByName('Periodo').AsInteger;
  //
  QvAllY.Close;
  QvAllY.SQL.Clear;

  QvAllY.SQL.Add('SELECT EIniQ, EIniV FROM produtos');
  QvAllY.SQL.Add('WHERE Codigo=:P0');
  QvAllY.Params[0].AsString := Mercadoria;
  QvAllY.Open;
  if PeriodoBal = 0 then
  begin
    DataI  := FormatDateTime(VAR_FORMATDATE, 0);
    DataF  := FormatDateTime(VAR_FORMATDATE, Data);
    Pecas  := QvAllY.FieldByName('EIniQ').AsFloat;
    Valor  := QvAllY.FieldByName('EIniV').AsFloat;
  end else begin
    DataI := MLAGeral.PrimeiroDiaAposPeriodo(PeriodoBal, dtSystem2);
    DataF := FormatDateTime(VAR_FORMATDATE, Data);
    Dmod.QrBalancosIts.Close;
    Dmod.QrBalancosIts.Params[0].AsString  := Mercadoria;
    Dmod.QrBalancosIts.Params[1].AsInteger := PeriodoBal;
    Dmod.QrBalancosIts.Open;
    Pecas  := Dmod.QrBalancosItsEstqQ.Value;
    Valor := Dmod.QrBalancosItsEstqV.Value;
  end;
  QvAllY.Close;
  //
  QvEntra3.Close;
  QvEntra3.DataBase := DMod.MyDB;
  QvEntra3.Params[0].AsString := Mercadoria;
  QvEntra3.Params[1].AsString := DataI;
  QvEntra3.Params[2].AsString := DataF;
  QvEntra3.Open;
  //
  QvVenda3.Close;
  QvVenda3.DataBase := DMod.MyDB;
  QvVenda3.Params[0].AsString := Mercadoria;
  QvVenda3.Params[1].AsString := DataI;
  QvVenda3.Params[2].AsString := DataF;
  QvVenda3.Open;
  //
  QvDevol3.Close;
  QvDevol3.DataBase := DMod.MyDB;
  QvDevol3.Params[0].AsString := Mercadoria;
  QvDevol3.Params[1].AsString := DataI;
  QvDevol3.Params[2].AsString := DataF;
  QvDevol3.Open;
  //
  QvRecom3.Close;
  QvRecom3.DataBase := DMod.MyDB;
  QvRecom3.Params[0].AsString := Mercadoria;
  QvRecom3.Params[1].AsString := DataI;
  QvRecom3.Params[2].AsString := DataF;
  QvRecom3.Open;
  //
  ///////////////////////

  Pecas := Pecas + QvEntra3.FieldByName('Pecas').AsFloat
               - QvVenda3.FieldByName('Pecas').AsFloat
               - QvDevol3.FieldByName('Pecas').AsFloat
               + QvRecom3.FieldByName('Pecas').AsFloat;

  Valor := Valor + QvEntra3.FieldByName('Valor').AsFloat
                 - QvVenda3.FieldByName('Valor').AsFloat
                 - QvDevol3.FieldByName('Valor').AsFloat
                 + QvRecom3.FieldByName('Valor').AsFloat;
  ///////////////////////
  BalQ := 0;
  BalV := 0;
  if IncBalDoDia then
  begin
    DecodeDate(Data + 1, Ano, Mes, Dia);
    if Dia = 1 then
    begin
      Dmod.QrBalancosIts.Close;
      Dmod.QrBalancosIts.Params[0].AsString  := Mercadoria;
      Dmod.QrBalancosIts.Params[1].AsInteger := Geral.Periodo2000(Data);
      Dmod.QrBalancosIts.Open;
      BalQ  := Dmod.QrBalancosItsEstqQ.Value - Pecas;
      BalV  := Dmod.QrBalancosItsEstqV.Value - Valor;
      Dmod.QrBalancosIts.Close;
    end;
  end;
  ///////////////////////
  if Tipo = epSintetico then
  begin
    Result[1] := Pecas + BalQ;
    Result[2] := Valor + BalV;
  end;
  if Tipo = epAnalitico then
  begin
    Result[01] := PesoI;
    Result[03] := QvEntra3.FieldByName('Pecas').AsFloat;
    Result[05] := QvVenda3.FieldByName('Pecas').AsFloat;
    Result[07] := QvDevol3.FieldByName('Pecas').AsFloat;
    Result[09] := QvRecom3.FieldByName('Pecas').AsFloat;
    Result[11] := BalQ;

    Result[02] := ValorI;
    Result[04] := QvEntra3.FieldByName('Valor').AsFloat;
    Result[06] := QvVenda3.FieldByName('Valor').AsFloat;
    Result[08] := QvDevol3.FieldByName('Valor').AsFloat;
    Result[10] := QvRecom3.FieldByName('Valor').AsFloat;
    Result[12] := BalV;
  end;
  ////////////////
  QvEntra3.Close;
  QvVenda3.Close;
  QvDevol3.Close;
  QvRecom3.Close;
end;

end.

