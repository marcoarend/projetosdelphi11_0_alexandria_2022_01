unit ImpRecil;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, UnMLAGeral, StdCtrls, Buttons, UnInternalConsts,
     ModuloBemaNaoFiscal,
     Db, mySQLDbTables,
  ComCtrls;

type
  TFmImpReci = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    BtCancela: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    TbImpReciC: TmySQLTable;
    DsImpReciC: TDataSource;
    TbImpReciCTipo: TIntegerField;
    TbImpReciCLinha: TAutoIncField;
    TbImpReciCTexto: TWideStringField;
    TbImpReciCLk: TIntegerField;
    TbImpReciCDataCad: TDateField;
    TbImpReciCDataAlt: TDateField;
    TbImpReciCUserCad: TIntegerField;
    TbImpReciCUserAlt: TIntegerField;
    TbImpReciCModo: TIntegerField;
    TbImpReciCNegr: TIntegerField;
    TbImpReciCItal: TIntegerField;
    TbImpReciCSubl: TIntegerField;
    TbImpReciCExpa: TIntegerField;
    BtImprime: TBitBtn;
    TbImpReciCNOMEMODO: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade0: TDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    Grade2: TDBGrid;
    DBGrid3: TDBGrid;
    TbImpReciR: TmySQLTable;
    DsImpReciR: TDataSource;
    TbImpReciRTipo: TIntegerField;
    TbImpReciRLinha: TAutoIncField;
    TbImpReciRTexto: TWideStringField;
    TbImpReciRLk: TIntegerField;
    TbImpReciRDataCad: TDateField;
    TbImpReciRDataAlt: TDateField;
    TbImpReciRUserCad: TIntegerField;
    TbImpReciRUserAlt: TIntegerField;
    TbImpReciRModo: TIntegerField;
    TbImpReciRNegr: TIntegerField;
    TbImpReciRItal: TIntegerField;
    TbImpReciRSubl: TIntegerField;
    TbImpReciRExpa: TIntegerField;
    TbImpReciRNOMEMODO: TWideStringField;
    procedure FormActivate(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbImpReciCBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtImprimeClick(Sender: TObject);
    procedure TbImpReciCCalcFields(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Grade0DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TbImpReciRBeforePost(DataSet: TDataSet);
    procedure Grade2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TbImpReciRCalcFields(DataSet: TDataSet);
    procedure DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmImpReci: TFmImpReci;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal, MyVCLSkin;

procedure TFmImpReci.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImpReci.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImpReci.FormResize(Sender: TObject);
begin
  Panel1.Height := Trunc(Panel2.Height/2);
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmImpReci.TbImpReciCBeforePost(DataSet: TDataSet);
begin
  TbImpReciCTipo.Value := 1;
  if (not TbImpReciCModo.Value in ([1,2,3])) then TbImpReciCModo.Value := 1;
end;

procedure TFmImpReci.FormCreate(Sender: TObject);
begin
  TbImpReciC.Open;
  TbImpReciR.Open;
end;

procedure TFmImpReci.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //TbImpReciC.Close;
end;

procedure TFmImpReci.BtImprimeClick(Sender: TObject);
{
var
  Comando: Integer;
}
const
  L = Chr(13) + Chr(10);
begin
  FmPrincipal.InicializaBEMA_NaoFiscal;
  TbImpReciC.First;
{
  while not TbImpReciC.Eof do
  begin
    comando := FormataTX(TbImpReciCTexto.Value+L ,
                         TbImpReciCModo.Value,
                         TbImpReciCItal.Value,
                         TbImpReciCSubl.Value,
                         TbImpReciCExpa.Value,
                         TbImpReciCNegr.Value);
    TbImpReciC.Next;
  end;
}
{
  while not TbImpReciR.Eof do
  begin
    comando := FormataTX(TbImpReciRTexto.Value+L ,
                         TbImpReciRModo.Value,
                         TbImpReciRItal.Value,
                         TbImpReciRSubl.Value,
                         TbImpReciRExpa.Value,
                         TbImpReciRNegr.Value);
    TbImpReciR.Next;
  end;
}
  FechaPorta();
end;

procedure TFmImpReci.TbImpReciCCalcFields(DataSet: TDataSet);
begin
  case TbImpReciCModo.Value of
    1: TbImpReciCNOMEMODO.Value := 'Condensado';
    2: TbImpReciCNOMEMODO.Value := 'Normal';
    3: TbImpReciCNOMEMODO.Value := 'Elite';
    else TbImpReciCNOMEMODO.Value := '?????';
  end;
end;

procedure TFmImpReci.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Fonte, Alt: Integer;
begin
  //Exit;
  //
  if Column.FieldName      = 'Ital' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbImpReciCItal.Value)
  else if Column.FieldName = 'Subl' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbImpReciCSubl.Value)
  else if Column.FieldName = 'Expa' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbImpReciCExpa.Value)
  else if Column.FieldName = 'Negr' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbImpReciCNegr.Value)
  else if Column.FieldName = 'Texto' then
  begin
    Fonte := TbImpReciCExpa.Value *10 + TbImpReciCModo.Value;
    case Fonte of
      01: Alt := 10;
      02: Alt := 12;
      03: Alt := 15;
      11: Alt := 20;
      12: Alt := 25;
      13: Alt := 30;
      else Alt := 10;
    end;
    with DBGrid1.Canvas do
    begin
      Font.Size := Alt;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmImpReci.Grade0DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //Exit;
  //
  if Column.FieldName      = 'Ital' then
    MeuVCLSkin.DrawGrid(Grade0, Rect, 1, TbImpReciCItal.Value)
  else if Column.FieldName = 'Subl' then
    MeuVCLSkin.DrawGrid(Grade0, Rect, 1, TbImpReciCSubl.Value)
  else if Column.FieldName = 'Expa' then
    MeuVCLSkin.DrawGrid(Grade0, Rect, 1, TbImpReciCExpa.Value)
  else if Column.FieldName = 'Negr' then
    MeuVCLSkin.DrawGrid(Grade0, Rect, 1, TbImpReciCNegr.Value)
end;

procedure TFmImpReci.TbImpReciRBeforePost(DataSet: TDataSet);
begin
  TbImpReciRTipo.Value := 2;
  if not (TbImpReciRModo.Value in ([1,2,3])) then TbImpReciRModo.Value := 1;
end;

procedure TFmImpReci.Grade2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //Exit;
  //
  if Column.FieldName      = 'Ital' then
    MeuVCLSkin.DrawGrid(Grade2, Rect, 1, TbImpReciRItal.Value)
  else if Column.FieldName = 'Subl' then
    MeuVCLSkin.DrawGrid(Grade2, Rect, 1, TbImpReciRSubl.Value)
  else if Column.FieldName = 'Expa' then
    MeuVCLSkin.DrawGrid(Grade2, Rect, 1, TbImpReciRExpa.Value)
  else if Column.FieldName = 'Negr' then
    MeuVCLSkin.DrawGrid(Grade2, Rect, 1, TbImpReciRNegr.Value)
end;

procedure TFmImpReci.TbImpReciRCalcFields(DataSet: TDataSet);
begin
  case TbImpReciRModo.Value of
    1: TbImpReciRNOMEMODO.Value := 'Condensado';
    2: TbImpReciRNOMEMODO.Value := 'Normal';
    3: TbImpReciRNOMEMODO.Value := 'Elite';
    else TbImpReciRNOMEMODO.Value := '?????';
  end;
end;

procedure TFmImpReci.DBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Fonte, Alt: Integer;
begin
  //Exit;
  //
  if Column.FieldName      = 'Ital' then
    MeuVCLSkin.DrawGrid(DBGrid3, Rect, 1, TbImpReciRItal.Value)
  else if Column.FieldName = 'Subl' then
    MeuVCLSkin.DrawGrid(DBGrid3, Rect, 1, TbImpReciRSubl.Value)
  else if Column.FieldName = 'Expa' then
    MeuVCLSkin.DrawGrid(DBGrid3, Rect, 1, TbImpReciRExpa.Value)
  else if Column.FieldName = 'Negr' then
    MeuVCLSkin.DrawGrid(DBGrid3, Rect, 1, TbImpReciRNegr.Value)
  else if Column.FieldName = 'Texto' then
  begin
    Fonte := TbImpReciRExpa.Value *10 + TbImpReciRModo.Value;
    case Fonte of
      01: Alt := 10;
      02: Alt := 12;
      03: Alt := 15;
      11: Alt := 20;
      12: Alt := 25;
      13: Alt := 30;
      else Alt := 10;
    end;
    with DBGrid3.Canvas do
    begin
      Font.Size := Alt;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

end.
