unit Salas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmSalas = class(TForm)
    PainelDados: TPanel;
    DsSalas: TDataSource;
    QrSalas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Painel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrSalasLk: TIntegerField;
    QrSalasDataCad: TDateField;
    QrSalasDataAlt: TDateField;
    QrSalasUserCad: TIntegerField;
    QrSalasUserAlt: TIntegerField;
    QrSalasCodigo: TSmallintField;
    QrSalasNome: TWideStringField;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrSalasLotacao: TIntegerField;
    QrSalasArea: TFloatField;
    QrSalaswats_h: TFloatField;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    DBEdLotacao: TDBEdit;
    DBEdArea: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBGrid1: TDBGrid;
    QrSalasEquip: TmySQLQuery;
    DsSalasEquip: TDataSource;
    Panel2: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    EdNome: TdmkEdit;
    EdLotacao: TdmkEdit;
    LaPorcentagem: TLabel;
    Label4: TLabel;
    EdArea: TdmkEdit;
    QrSalasEquipCodigo: TIntegerField;
    QrSalasEquipControle: TIntegerField;
    QrSalasEquipNome: TWideStringField;
    QrSalasEquipwats_h: TFloatField;
    QrSalasEquipLk: TIntegerField;
    QrSalasEquipDataCad: TDateField;
    QrSalasEquipDataAlt: TDateField;
    QrSalasEquipUserCad: TIntegerField;
    QrSalasEquipUserAlt: TIntegerField;
    PMInclui: TPopupMenu;
    NovaSala1: TMenuItem;
    Eletroeletrniconasalaatual1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraSalaatual1: TMenuItem;
    PMExclui: TPopupMenu;
    ExcluiEletroeletrnicoatual1: TMenuItem;
    QrSoma: TmySQLQuery;
    QrSomawats_h: TFloatField;
    QrSalasEquipFator: TFloatField;
    QrSalasEquipQtde: TFloatField;
    QrSalasEquipCONSUMO: TFloatField;
    Label7: TLabel;
    EdDepartamento: TdmkEditCB;
    CBDepartamento: TdmkDBLookupComboBox;
    QrDepartamentos: TmySQLQuery;
    DsDepartamentos: TDataSource;
    QrDepartamentosCodigo: TIntegerField;
    QrDepartamentosNome: TWideStringField;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    QrSalasDepartamento: TIntegerField;
    QrSalasNOMEDEPTO: TWideStringField;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSalasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrSalasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSalasBeforeOpen(DataSet: TDataSet);
    procedure EdLotacaoExit(Sender: TObject);
    procedure EdAreaExit(Sender: TObject);
    procedure NovaSala1Click(Sender: TObject);
    procedure AlteraSalaatual1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Eletroeletrniconasalaatual1Click(Sender: TObject);
    procedure ExcluiEletroeletrnicoatual1Click(Sender: TObject);
    procedure QrSalasEquipCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenSalasEquip;
    procedure RecalculaConsumos;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmSalas: TFmSalas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, SalasEquip, MyDBCheck, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSalas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSalas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSalasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSalas.DefParams;
begin
  VAR_GOTOTABELA := 'Salas';
  VAR_GOTOMYSQLTABLE := QrSalas;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT sa.*, de.Nome NOMEDEPTO');
  VAR_SQLx.Add('FROM salas sa');
  VAR_SQLx.Add('LEFT JOIN departamentos de ON de.Codigo=sa.Departamento');
  VAR_SQLx.Add('WHERE sa.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND sa.Codigo=:P0');
  //
  VAR_SQLa.Add('AND sa.Nome Like :P0');
  //
end;

procedure TFmSalas.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
    end else begin
      EdCodigo.Text  := DBEdCodigo.Text;
      EdNome.Text    := DBEdNome.Text;
      EdLotacao.Text := DBEdLotacao.Text;
      EdArea.Text    := DBEdArea.Text;
      EdDepartamento.Text := IntToStr(QrSalasDepartamento.Value);
      CBDepartamento.KeyValue := QrSalasDepartamento.Value;
    end;
    EdNome.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmSalas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSalas.AlteraRegistro;
var
  Salas : Integer;
begin
  Salas := QrSalasCodigo.Value;
  if QrSalasCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Salas, Dmod.MyDB, 'Salas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Salas, Dmod.MyDB, 'Salas', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmSalas.IncluiRegistro;
var
  Cursor : TCursor;
  Salas : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Salas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
               'Salas', 'Salas', 'Codigo');
    //
    if Length(FormatFloat(FFormatFloat, Salas))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Salas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmSalas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSalas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSalas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSalas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSalas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSalas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSalas.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroDeDepartamentos(EdDepartamento.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdDepartamento, CBDepartamento, QrDepartamentos, VAR_CADASTRO);
    EdDepartamento.SetFocus;
  end;
end;

procedure TFmSalas.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmSalas.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmSalas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSalasCodigo.Value;
  Close;
end;

procedure TFmSalas.BtConfirmaClick(Sender: TObject);
var
  Departamento, Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, nil, 'Defina uma descri��o!') then Exit;
  //
  Departamento := Geral.IMV(EdDepartamento.Text);
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO salas SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE salas SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Lotacao=:P1, Area=:P2, Departamento=:P3,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdLotacao.Text);
  Dmod.QrUpdU.Params[02].AsFloat   := Geral.DMV(EdArea.Text);
  Dmod.QrUpdU.Params[03].AsInteger := Departamento;
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Salas', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmSalas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Salas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Salas', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Salas', 'Codigo');
end;

procedure TFmSalas.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  DBGrid1.Align     := alClient;
  Panel2.Align      := alClient;
  QrDepartamentos.Open;
  CriaOForm;
end;

procedure TFmSalas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSalasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmSalas.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmSalas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSalas.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmSalas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmSalas.QrSalasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSalas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Salas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmSalas.QrSalasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrSalasCodigo.Value, False);
  ReopenSalasEquip;
end;

procedure TFmSalas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSalasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Salas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSalas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmSalas.QrSalasBeforeOpen(DataSet: TDataSet);
begin
  QrSalasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSalas.EdLotacaoExit(Sender: TObject);
begin
  EdLotacao.Text := Geral.TFT(EdLotacao.Text, 0, siPositivo);
end;

procedure TFmSalas.EdAreaExit(Sender: TObject);
begin
  EdArea.Text := Geral.TFT(EdArea.Text, 2, siPositivo);
end;

procedure TFmSalas.ReopenSalasEquip;
begin
  QrSalasEquip.Close;
  QrSalasEquip.Params[0].AsInteger := QrSalasCodigo.Value;
  QrSalasEquip.Open;
end;

procedure TFmSalas.NovaSala1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmSalas.AlteraSalaatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmSalas.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmSalas.Eletroeletrniconasalaatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSalasEquip, FmSalasEquip, afmoNegarComAviso) then
  begin
    FmSalasEquip.FCodigo := QrSalasCodigo.Value;
    FmSalasEquip.ShowModal;
    FmSalasEquip.Destroy;
    RecalculaConsumos;
    LocCod(QrSalasCodigo.Value, QrSalasCodigo.Value);
  end;
end;

procedure TFmSalas.ExcluiEletroeletrnicoatual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma e exclus�o do eletroeletr�nico selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM salasequip WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrSalasEquipControle.Value;
    Dmod.QrUpd.ExecSQL;
    RecalculaConsumos;
    LocCod(QrSalasCodigo.Value, QrSalasCodigo.Value);
  end;
end;

procedure TFmSalas.RecalculaConsumos;
begin
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrSalasCodigo.Value;
  QrSoma.Open;

  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE salas SET wats_h=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsFloat   := QrSomawats_h.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrSalasCodigo.Value;
  Dmod.QrUpd.ExecSQL;

  QrSoma.Close;
end;

procedure TFmSalas.QrSalasEquipCalcFields(DataSet: TDataSet);
begin
  QrSalasEquipCONSUMO.Value :=
  QrSalasEquipQtde.Value *
  QrSalasEquipwats_h.Value *
  QrSalasEquipFator.Value;
end;

end.

