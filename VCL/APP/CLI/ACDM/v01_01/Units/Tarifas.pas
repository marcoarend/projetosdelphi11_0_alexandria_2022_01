unit Tarifas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  Grids, DBGrids, Menus, dmkEdit, dmkDBLookupComboBox, dmkEditCB, dmkLabel,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmTarifas = class(TForm)
    PainelDados: TPanel;
    DsTarifas: TDataSource;
    QrTarifas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    Panel2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrTarifasLk: TIntegerField;
    QrTarifasDataCad: TDateField;
    QrTarifasDataAlt: TDateField;
    QrTarifasUserCad: TIntegerField;
    QrTarifasUserAlt: TIntegerField;
    QrTarifasCodigo: TSmallintField;
    QrTarifasNome: TWideStringField;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    CkAtiva: TCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrTarifasAtiva: TSmallintField;
    DBGrade: TDBGrid;
    QrTarifasIts: TmySQLQuery;
    DsTarifasIts: TDataSource;
    QrTarifasItsCodigo: TIntegerField;
    QrTarifasItsData: TDateField;
    QrTarifasItsPreco: TFloatField;
    QrTarifasItsLk: TIntegerField;
    QrTarifasItsDataCad: TDateField;
    QrTarifasItsDataAlt: TDateField;
    QrTarifasItsUserCad: TIntegerField;
    QrTarifasItsUserAlt: TIntegerField;
    QrTarifasItsTaxa: TFloatField;
    PMInclui: TPopupMenu;
    NovoPlano1: TMenuItem;
    NovoPreo1: TMenuItem;
    PMAltera: TPopupMenu;
    AlteraPlano1: TMenuItem;
    AlteraPreo1: TMenuItem;
    PMExclui: TPopupMenu;
    Preo1: TMenuItem;
    QrTarifasTipoPer: TIntegerField;
    QrTarifasQtdePer: TIntegerField;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    RGTipoPer: TRadioGroup;
    Label4: TLabel;
    EdQtdePer: TdmkEdit;
    EdGrupo: TdmkEditCB;
    Label5: TLabel;
    CBGrupo: TdmkDBLookupComboBox;
    QrTarifasGru: TmySQLQuery;
    DsTarifasGru: TDataSource;
    QrTarifasGruCodigo: TIntegerField;
    QrTarifasGruNome: TWideStringField;
    SpeedButton5: TSpeedButton;
    QrTarifasGrupo: TIntegerField;
    QrTarifasNO_GRUPO: TWideStringField;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrTarifasItsValPer: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTarifasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrTarifasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTarifasBeforeOpen(DataSet: TDataSet);
    procedure NovoPlano1Click(Sender: TObject);
    procedure NovoPreo1Click(Sender: TObject);
    procedure AlteraPlano1Click(Sender: TObject);
    procedure AlteraPreo1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Preo1Click(Sender: TObject);
    procedure EdQtdePerExit(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure DBGradeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenTarifasIts;
  public
    { Public declarations }
    FGrupo: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTarifas: TFmTarifas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, TarifasEdit, MyDBCheck, TarifasNovas, Principal,
  UnAppPF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTarifas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTarifas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTarifasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTarifas.DefParams;
begin
  VAR_GOTOTABELA := 'Tarifas';
  VAR_GOTOMYSQLTABLE := QrTarifas;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gru.Nome NO_GRUPO, tar.*');
  VAR_SQLx.Add('FROM tarifas tar');
  VAR_SQLx.Add('LEFT JOIN tarifasgru gru ON gru.Codigo=tar.Grupo');
  VAR_SQLx.Add('WHERE tar.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND tar.Codigo=:P0');
  VAR_SQL2.Add('');
  //
  VAR_SQLa.Add('AND tar.Nome Like :P0');
  //
end;

procedure TFmTarifas.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible    := True;
    PainelDados.Visible    := False;
    EdNome.Text            := CO_VAZIO;
    EdNome.Visible         := True;
    PainelControle.Visible :=False;
    //
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
      EdGrupo.ValueVariant  := FGrupo;
      CBGrupo.KeyValue      := FGrupo;
    end else
    begin
      EdCodigo.Text        := DBEdCodigo.Text;
      EdNome.Text          := DBEdNome.Text;
      CkAtiva.Checked      := MLAGeral.IntToBool(QrTarifasAtiva.Value);
      EdQtdePer.Text       := IntToStr(QrTarifasQtdePer.Value);
      RGTipoPer.ItemIndex  := QrTarifasTipoPer.Value;
      EdGrupo.ValueVariant := QrTarifasGrupo.Value;
      CBGrupo.KeyValue     := QrTarifasGrupo.Value;
    end;
    EdNome.SetFocus;
  end else
  begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmTarifas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTarifas.AlteraRegistro;
var
  Tarifas : Integer;
begin
  Tarifas := QrTarifasCodigo.Value;
  if QrTarifasCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Tarifas, Dmod.MyDB, 'Tarifas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Tarifas, Dmod.MyDB, 'Tarifas', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmTarifas.IncluiRegistro;
var
  Cursor : TCursor;
  Tarifas : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Tarifas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                 'Tarifas', 'Tarifas', 'Codigo');
    //
    if Length(FormatFloat(FFormatFloat, Tarifas)) > Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado!');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, Tarifas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmTarifas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTarifas.DBGradeDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  AppPF.ConfiguraGradeTarifasPreco(QrTarifasIts, DBGrade, Column, Rect);
end;

procedure TFmTarifas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTarifas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTarifas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTarifas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTarifas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTarifas.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroTarifasGru(EdGrupo.ValueVariant, 0);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdGrupo, CBGrupo, QrTarifasGru, VAR_CADASTRO);
    EdGrupo.SetFocus;
  end;
end;

procedure TFmTarifas.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmTarifas.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmTarifas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTarifasCodigo.Value;
  Close;
end;

procedure TFmTarifas.BtConfirmaClick(Sender: TObject);
var
  Codigo, Ativa, TipoPer, QtdePer, Grupo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, nil, 'Defina uma descri��o!') then Exit;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  {
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO tarifas SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE tarifas SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Ativa=:P1, TipoPer=:P2, QtdePer=:P3,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := MLAGeral.BoolToInt(CkAtiva.Checked);
  Dmod.QrUpdU.Params[02].AsInteger := RGTipoPer.ItemIndex;
  Dmod.QrUpdU.Params[03].AsInteger := Geral.IMV(EdQtdePer.Text);
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  }
  Ativa   := MLAGeral.BoolToInt(CkAtiva.Checked);
  TipoPer := RGTipoPer.ItemIndex;
  QtdePer := Geral.IMV(EdQtdePer.Text);
  Grupo   := EdGrupo.ValueVariant;
  //? := UMyMod.BuscaEmLivreY_Def('tarifas', ', LaTipo.SQLType, CodAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'tarifas', False, [
  'Nome', 'Ativa',
  'TipoPer', 'QtdePer', 'Grupo'], ['Codigo'
  ], [
  Nome, Ativa,
  TipoPer, QtdePer, Grupo], [Codigo
  ], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Tarifas', 'Codigo');
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmTarifas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Tarifas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Tarifas', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Tarifas', 'Codigo');
end;

procedure TFmTarifas.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  DBGrade.Align     := alClient;
  PainelEdit.Align  := alClient;
  PainelDados.Align := alClient;
  FGrupo            := 0;
  //
  CriaOForm;
  QrTarifasGru.Open;
end;

procedure TFmTarifas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTarifasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmTarifas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTarifas.SbNovoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTarifasNovas, FmTarifasNovas, afmoNegarComAviso) then
  begin
    FmTarifasNovas.ShowModal;
    FmTarifasNovas.Destroy;
  end;
end;

procedure TFmTarifas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmTarifas.QrTarifasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTarifas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Tarifas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmTarifas.QrTarifasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrTarifasCodigo.Value, False);
  ReopenTarifasIts;
end;

procedure TFmTarifas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTarifasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Tarifas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTarifas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTarifas.QrTarifasBeforeOpen(DataSet: TDataSet);
begin
  QrTarifasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTarifas.ReopenTarifasIts;
begin
  QrTarifasIts.Close;
  QrTarifasIts.Params[0].AsInteger := QrTarifasCodigo.Value;
  QrTarifasIts.Open;
end;

procedure TFmTarifas.NovoPlano1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmTarifas.NovoPreo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTarifasEdit, FmTarifasEdit, afmoNegarComAviso) then
  begin
    FmTarifasEdit.LaTipo.SQLType := stIns;
    FmTarifasEdit.FCodigo        := QrTarifasCodigo.Value;
    FmTarifasEdit.TPData.Enabled := True;
    FmTarifasEdit.ShowModal;
    FmTarifasedit.Destroy;
    ReopenTarifasIts;
  end;
end;

procedure TFmTarifas.AlteraPlano1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmTarifas.AlteraPreo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTarifasEdit, FmTarifasEdit, afmoNegarComAviso) then
  begin
    FmTarifasEdit.LaTipo.SQLType := stUpd;
    FmTarifasEdit.FCodigo        := QrTarifasCodigo.Value;
    FmTarifasEdit.TPData.Enabled := False;
    FmTarifasEdit.TPData.Date    := QrTarifasItsData.Value;
    FmTarifasEdit.EdPreco.Text   := Geral.FFT(QrTarifasItsPreco.Value, 2, siPositivo);
    FmTarifasEdit.EdValPer.Text  := Geral.FFT(QrTarifasItsValPer.Value, 2, siPositivo);
    FmTarifasEdit.EdTaxa.Text    := Geral.FFT(QrTarifasItsTaxa.Value, 2, siPositivo);
    //
    //FmTarifasEdit.TPData.Enabled := False;
    FmTarifasEdit.FDataAnt := QrTarifasItsData.Value;
    FmTarifasEdit.ShowModal;
    FmTarifasedit.Destroy;
    ReopenTarifasIts;
  end;
end;

procedure TFmTarifas.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmTarifas.Preo1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do pre�o selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM tarifasits WHERE Codigo=:P0 AND Data=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrTarifasItsCodigo.Value;
    Dmod.QrUpd.Params[1].AsString  := FormatDateTime(VAR_FORMATDATE, QrTarifasItsData.Value);
    Dmod.QrUpd.ExecSQL;
    ReopenTarifasIts;
  end;
end;

procedure TFmTarifas.EdQtdePerExit(Sender: TObject);
begin
  EdQtdePer.Text := MLAGeral.TFT_MinMax(EdQtdePer.Text, 1, 1000000, 0, siPositivo);
end;

end.

