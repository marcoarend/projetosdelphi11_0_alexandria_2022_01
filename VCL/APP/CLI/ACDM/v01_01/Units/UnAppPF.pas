unit UnAppPF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, mySQLDBTables,
  UnInternalConsts2, ComCtrls, dmkGeral, UnDmkEnums, SHDocVw, Vcl.DBGrids;

type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // D A T A   H O R A
    function Carga_HoraToTime(Carga: Double): String;
    function Carga_TimeToHora(Carga: TDateTime): Double;
    // D B G R I D
    procedure ConfiguraGradeTarifasPreco(Query: TmySQLQuery; DBGrid: TDBGrid;
              Column: TColumn; Rect: TRect);
    // O U T R O S
    function  AcaoEspecificaDeApp(Servico: String; CliInt, EntCliInt: Integer;
              Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
    procedure Html_ConfigarImagem(WebBrowser: TWebBrowser);
    procedure Html_ConfigarUrl(WebBrowser: TWebBrowser);
    procedure Html_ConfigarVideo(WebBrowser: TWebBrowser);
  end;

var
  AppPF: TUnAppPF;

implementation

uses UnMyObjects, UnDmkProcFunc;

{ TUnAppPF }

function TUnAppPF.Carga_HoraToTime(Carga: Double): String;
var
  Min: Integer;
  Hor: TDateTime;
begin
  if Carga <> 0 then
  begin
    Min := Trunc(Carga * 60);
    Hor := dmkPF.MinutesToTime(Min);
    //
    Result := Geral.FDT(Hor, 102);
  end else
    Result := '00:00';
end;

function TUnAppPF.Carga_TimeToHora(Carga: TDateTime): Double;
begin
  Result := dmkPF.TimeToMinutes(Carga) / 60;
end;

procedure TUnAppPF.ConfiguraGradeTarifasPreco(Query: TmySQLQuery;
  DBGrid: TDBGrid; Column: TColumn; Rect: TRect);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if Query.RecNo = 1 then
  begin
    Bold := True;
    Cor  := clGreen;
  end else
  begin
    Bold := False;
    Cor  := clBlack;
  end;
  //
  with DBGrid.Canvas do
  begin
    if Bold then
      Font.Style := [fsBold]
    else
      Font.Style := [];
    //
    Font.Color := Cor;
    FillRect(Rect);
    TextOut(Rect.Left+2,rect.Top+2, Column.Field.DisplayText);
  end;
end;

procedure TUnAppPF.Html_ConfigarImagem(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarUrl(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

procedure TUnAppPF.Html_ConfigarVideo(WebBrowser: TWebBrowser);
begin
  //Compatibilidade
end;

function TUnAppPF.AcaoEspecificaDeApp(Servico: String; CliInt,
  EntCliInt: Integer; Query: TmySQLQuery = nil; Query2: TmySQLQuery = nil): Boolean;
begin
  Result := False;
end;

end.
