unit SalasEquip;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkGeral,
  UnDmkEnums;

type
  TFmSalasEquip = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    DsTransportadoras: TDataSource;
    EdNome: TdmkEdit;
    EdFator: TdmkEdit;
    Label3: TLabel;
    QrTransportadoras: TmySQLQuery;
    QrTransportadorasNOMEENTIDADE: TWideStringField;
    QrTransportadorasCodigo: TIntegerField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label2: TLabel;
    Edwattsh: TdmkEdit;
    Label4: TLabel;
    EdQtde: TdmkEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFatorExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdwattshExit(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
  end;

var
  FmSalasEquip: TFmSalasEquip;

implementation

uses UnMyObjects, Module, Salas;

{$R *.DFM}

procedure TFmSalasEquip.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSalasEquip.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'SalasEquip',
  'SalasEquip', 'Controle');
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO SalasEquip SET ');
  Dmod.QrUpdM.SQL.Add('Nome=:P0, wats_h=:P1, Fator=:P2, ');
  Dmod.QrUpdM.SQL.Add('Qtde=:P3, Codigo=:P4, Controle=:P5 ');
  //
  Dmod.QrUpdM.Params[00].AsString  := EdNome.Text;
  Dmod.QrUpdM.Params[01].AsFloat   := Geral.DMV(Edwattsh.Text);
  Dmod.QrUpdM.Params[02].AsFloat   := Geral.DMV(EdFator.Text);
  Dmod.QrUpdM.Params[03].AsFloat   := Geral.DMV(EdQtde.Text);
  Dmod.QrUpdM.Params[04].AsInteger := FCodigo;
  Dmod.QrUpdM.Params[05].AsInteger := Controle;
  Dmod.QrUpdM.ExecSQL;
  Close;
end;

procedure TFmSalasEquip.FormCreate(Sender: TObject);
begin
  QrTransportadoras.Open;
end;

procedure TFmSalasEquip.EdFatorExit(Sender: TObject);
begin
  EdFator.Text := MLAGeral.TFT_MinMax(EdFator.Text, 0, 1, 4, siPositivo);
end;

procedure TFmSalasEquip.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmSalasEquip.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdNome.SetFocus;
end;

procedure TFmSalasEquip.EdwattshExit(Sender: TObject);
begin
  Edwattsh.Text := Geral.TFT(Edwattsh.Text, 0, siPositivo);
end;

procedure TFmSalasEquip.EdQtdeExit(Sender: TObject);
begin
  EdQtde.Text := Geral.TFT(EdQtde.Text, 3, siPositivo);
end;

end.
