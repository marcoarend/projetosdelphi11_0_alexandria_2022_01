unit MatriculContrato;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Db, (*DBTables,*) UnInternalConsts2, UnInternalConsts,
  UnMLAGeral, ExtCtrls, mySQLDbTables, frxClass, frxDBSet, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, UnDmkEnums;

type
  TFmMatriculContrato = class(TForm)
    QrEscolha: TmySQLQuery;
    DsEscolha: TDataSource;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    CBEscolha: TdmkDBLookupComboBox;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    QrEscolhaCodigo: TIntegerField;
    QrEscolhaTitulo: TWideStringField;
    QrEscolhaTexto: TWideMemoField;
    QrEscolhaLk: TIntegerField;
    QrEscolhaDataCad: TDateField;
    QrEscolhaDataAlt: TDateField;
    QrEscolhaUserCad: TIntegerField;
    QrEscolhaUserAlt: TIntegerField;
    QrAluno: TmySQLQuery;
    QrAlunoCodigo: TIntegerField;
    QrAlunoRazaoSocial: TWideStringField;
    QrAlunoFantasia: TWideStringField;
    QrAlunoRespons1: TWideStringField;
    QrAlunoRespons2: TWideStringField;
    QrAlunoPai: TWideStringField;
    QrAlunoMae: TWideStringField;
    QrAlunoCNPJ: TWideStringField;
    QrAlunoIE: TWideStringField;
    QrAlunoIEST: TWideStringField;
    QrAlunoNome: TWideStringField;
    QrAlunoApelido: TWideStringField;
    QrAlunoCPF: TWideStringField;
    QrAlunoRG: TWideStringField;
    QrAlunoELograd: TSmallintField;
    QrAlunoERua: TWideStringField;
    QrAlunoENumero: TIntegerField;
    QrAlunoECompl: TWideStringField;
    QrAlunoEBairro: TWideStringField;
    QrAlunoECidade: TWideStringField;
    QrAlunoEUF: TSmallintField;
    QrAlunoECEP: TIntegerField;
    QrAlunoEPais: TWideStringField;
    QrAlunoETe1: TWideStringField;
    QrAlunoEte2: TWideStringField;
    QrAlunoEte3: TWideStringField;
    QrAlunoECel: TWideStringField;
    QrAlunoEFax: TWideStringField;
    QrAlunoEEmail: TWideStringField;
    QrAlunoEContato: TWideStringField;
    QrAlunoENatal: TDateField;
    QrAlunoPLograd: TSmallintField;
    QrAlunoPRua: TWideStringField;
    QrAlunoPNumero: TIntegerField;
    QrAlunoPCompl: TWideStringField;
    QrAlunoPBairro: TWideStringField;
    QrAlunoPCidade: TWideStringField;
    QrAlunoPUF: TSmallintField;
    QrAlunoPCEP: TIntegerField;
    QrAlunoPPais: TWideStringField;
    QrAlunoPTe1: TWideStringField;
    QrAlunoPte2: TWideStringField;
    QrAlunoPte3: TWideStringField;
    QrAlunoPCel: TWideStringField;
    QrAlunoPFax: TWideStringField;
    QrAlunoPEmail: TWideStringField;
    QrAlunoPContato: TWideStringField;
    QrAlunoPNatal: TDateField;
    QrAlunoSexo: TWideStringField;
    QrAlunoResponsavel: TWideStringField;
    QrAlunoProfissao: TWideStringField;
    QrAlunoCargo: TWideStringField;
    QrAlunoRecibo: TSmallintField;
    QrAlunoDiaRecibo: TSmallintField;
    QrAlunoAjudaEmpV: TFloatField;
    QrAlunoAjudaEmpP: TFloatField;
    QrAlunoCliente1: TWideStringField;
    QrAlunoCliente2: TWideStringField;
    QrAlunoFornece1: TWideStringField;
    QrAlunoFornece2: TWideStringField;
    QrAlunoFornece3: TWideStringField;
    QrAlunoFornece4: TWideStringField;
    QrAlunoTerceiro: TWideStringField;
    QrAlunoCadastro: TDateField;
    QrAlunoInformacoes: TWideStringField;
    QrAlunoLogo: TBlobField;
    QrAlunoVeiculo: TIntegerField;
    QrAlunoMensal: TWideStringField;
    QrAlunoObservacoes: TWideMemoField;
    QrAlunoTipo: TSmallintField;
    QrAlunoCLograd: TSmallintField;
    QrAlunoCRua: TWideStringField;
    QrAlunoCNumero: TIntegerField;
    QrAlunoCCompl: TWideStringField;
    QrAlunoCBairro: TWideStringField;
    QrAlunoCCidade: TWideStringField;
    QrAlunoCUF: TSmallintField;
    QrAlunoCCEP: TIntegerField;
    QrAlunoCPais: TWideStringField;
    QrAlunoCTel: TWideStringField;
    QrAlunoCCel: TWideStringField;
    QrAlunoCFax: TWideStringField;
    QrAlunoCContato: TWideStringField;
    QrAlunoLLograd: TSmallintField;
    QrAlunoLRua: TWideStringField;
    QrAlunoLNumero: TIntegerField;
    QrAlunoLCompl: TWideStringField;
    QrAlunoLBairro: TWideStringField;
    QrAlunoLCidade: TWideStringField;
    QrAlunoLUF: TSmallintField;
    QrAlunoLCEP: TIntegerField;
    QrAlunoLPais: TWideStringField;
    QrAlunoLTel: TWideStringField;
    QrAlunoLCel: TWideStringField;
    QrAlunoLFax: TWideStringField;
    QrAlunoLContato: TWideStringField;
    QrAlunoComissao: TFloatField;
    QrAlunoSituacao: TSmallintField;
    QrAlunoNivel: TWideStringField;
    QrAlunoGrupo: TIntegerField;
    QrAlunoAccount: TIntegerField;
    QrAlunoLogo2: TBlobField;
    QrAlunoConjugeNome: TWideStringField;
    QrAlunoConjugeNatal: TDateField;
    QrAlunoNome1: TWideStringField;
    QrAlunoNatal1: TDateField;
    QrAlunoNome2: TWideStringField;
    QrAlunoNatal2: TDateField;
    QrAlunoNome3: TWideStringField;
    QrAlunoNatal3: TDateField;
    QrAlunoNome4: TWideStringField;
    QrAlunoNatal4: TDateField;
    QrAlunoCreditosI: TIntegerField;
    QrAlunoCreditosL: TIntegerField;
    QrAlunoCreditosF2: TFloatField;
    QrAlunoCreditosD: TDateField;
    QrAlunoCreditosU: TDateField;
    QrAlunoCreditosV: TDateField;
    QrAlunoMotivo: TIntegerField;
    QrAlunoQuantI1: TIntegerField;
    QrAlunoQuantI2: TIntegerField;
    QrAlunoQuantI3: TIntegerField;
    QrAlunoQuantI4: TIntegerField;
    QrAlunoQuantN1: TFloatField;
    QrAlunoQuantN2: TFloatField;
    QrAlunoAgenda: TWideStringField;
    QrAlunoSenhaQuer: TWideStringField;
    QrAlunoSenha1: TWideStringField;
    QrAlunoLimiCred: TFloatField;
    QrAlunoDesco: TFloatField;
    QrAlunoCasasApliDesco: TSmallintField;
    QrAlunoTempD: TFloatField;
    QrAlunoLk: TIntegerField;
    QrAlunoDataCad: TDateField;
    QrAlunoDataAlt: TDateField;
    QrAlunoUserCad: TIntegerField;
    QrAlunoUserAlt: TIntegerField;
    QrAlunoNOMEENTIDADE: TWideStringField;
    QrAlunoNOMEACCOUNT: TWideStringField;
    QrAlunoNOMEENTIGRUPO: TWideStringField;
    QrAlunoNOMEMOTIVO: TWideStringField;
    QrAlunoNOMEEUF: TWideStringField;
    QrAlunoNOMEPUF: TWideStringField;
    QrAlunoNOMECUF: TWideStringField;
    QrAlunoNOMELUF: TWideStringField;
    QrAlunoNOMEELOGRAD: TWideStringField;
    QrAlunoNOMEPLOGRAD: TWideStringField;
    QrAlunoNOMECLOGRAD: TWideStringField;
    QrAlunoNOMELLOGRAD: TWideStringField;
    QrAlunoPTE1_TXT: TWideStringField;
    QrAlunoPCPF_TXT: TWideStringField;
    QrAlunoPCEP_TXT: TWideStringField;
    QrAlunoNOMEETRATO: TWideStringField;
    QrAlunoNOMEPTRATO: TWideStringField;
    QrAlunoNOMECTRATO: TWideStringField;
    QrAlunoNOMELTRATO: TWideStringField;
    QrAlunoPNUM_TXT: TWideStringField;
    QrAlunoPMEU_BAIRRO: TWideStringField;
    frxContrato: TfrxReport;
    frxDsAluno: TfrxDBDataset;
    SpeedButton1: TSpeedButton;
    EdEscolha: TdmkEditCB;
    frxDBDataset1: TfrxDBDataset;
    QrRespo: TmySQLQuery;
    QrRespoNome: TWideStringField;
    QrRespoNacionalid: TWideStringField;
    QrRespoProfissao: TWideStringField;
    QrRespoPRua: TWideStringField;
    QrRespoPCompl: TWideStringField;
    QrRespoPBairro: TWideStringField;
    QrRespoPUF: TSmallintField;
    QrRespoPCidade: TWideStringField;
    QrRespoPPais: TWideStringField;
    QrRespoDataRG_TXT: TWideStringField;
    QrRespoNO_EstCivil: TWideStringField;
    QrRespoPTe1: TWideStringField;
    QrRespoCPF: TWideStringField;
    QrRespoRG: TWideStringField;
    QrRespoSSP: TWideStringField;
    QrRespoNO_PLograd: TWideStringField;
    QrRespoUF_TXT: TWideStringField;
    QrRespoPNumero: TWideStringField;
    QrRespoPCEP: TIntegerField;
    QrRespoPTe2: TWideStringField;
    QrRespoPCel: TWideStringField;
    QrRespoPEMail: TWideStringField;
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAlunoCalcFields(DataSet: TDataSet);
    procedure frxContratoGetValue(const VarName: String;
      var Value: Variant);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FAluno,
    FRespo: Integer;
    FHorari,
    FInvest,
    FParcel,
    FDataCo,
    FContra,
    FPlural,
    FPeriod: String;
  end;

var
  FmMatriculContrato: TFmMatriculContrato;

implementation

uses UnMyObjects, Module, Matricul, MyDBCheck, Cartas, UMySQLModule;

{$R *.DFM}

procedure TFmMatriculContrato.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrEscolha, DMod.MyDB);
end;

procedure TFmMatriculContrato.BtConfirmaClick(Sender: TObject);
begin
  QrAluno.Close;
  QrAluno.Params[0].AsInteger := FAluno;
  UMyMod.AbreQuery(QrAluno, DMod.MyDB);
  //
  QrRespo.Close;
  QrRespo.Params[0].AsInteger := FRespo;
  UMyMod.AbreQuery(QrRespo, DMod.MyDB);
  //
  MyObjects.frxMostra(frxContrato, 'Contrato');
  //
  Close;
end;

procedure TFmMatriculContrato.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriculContrato.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMatriculContrato.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMatriculContrato.QrAlunoCalcFields(DataSet: TDataSet);
begin
  if QrAlunoPNumero.Value = 0 then
     QrAlunoPNUM_TXT.Value := 'S/N' else
     QrAlunoPNUM_TXT.Value := 'N� '+
     FloatToStr(QrAlunoPNumero.Value);
  (*if QrAlunoCNumero.Value = 0 then
     QrAlunoCNUM_TXT.Value := 'S/N' else
     QrAlunoCNUM_TXT.Value :=
     FloatToStr(QrAlunoCNumero.Value);
  if QrAlunoLNumero.Value = 0 then
     QrAlunoLNUM_TXT.Value := 'S/N' else
     QrAlunoLNUM_TXT.Value :=
     FloatToStr(QrAlunoLNumero.Value);*)
  //
  if QrAlunoPBairro.Value = '' then
     QrAlunoPMEU_BAIRRO.Value := '' else
     QrAlunoPMEU_BAIRRO.Value := 'Bairro '+QrAlunoPBairro.Value;
  (*QrAlunoETE1_TXT.Value := Geral.FormataTelefone_TT(QrAlunoETe1.Value);
  QrAlunoETE2_TXT.Value := Geral.FormataTelefone_TT(QrAlunoETe2.Value);
  QrAlunoETE3_TXT.Value := Geral.FormataTelefone_TT(QrAlunoETe3.Value);
  QrAlunoEFAX_TXT.Value := Geral.FormataTelefone_TT(QrAlunoEFax.Value);
  QrAlunoECEL_TXT.Value := Geral.FormataTelefone_TT(QrAlunoECel.Value);
  QrAlunoCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrAlunoCNPJ.Value);*)
  //
  QrAlunoPTE1_TXT.Value := Geral.FormataTelefone_TT(QrAlunoPTe1.Value);
  (*QrAlunoPTE2_TXT.Value := Geral.FormataTelefone_TT(QrAlunoPTe2.Value);
  QrAlunoPTE3_TXT.Value := Geral.FormataTelefone_TT(QrAlunoPTe3.Value);
  QrAlunoPFAX_TXT.Value := Geral.FormataTelefone_TT(QrAlunoPFax.Value);
  QrAlunoPCEL_TXT.Value := Geral.FormataTelefone_TT(QrAlunoPCel.Value);*)
  QrAlunoPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAlunoCPF.Value);
  //
  (*QrAlunoCTEL_TXT.Value := Geral.FormataTelefone_TT(QrAlunoCTel.Value);
  QrAlunoCFAX_TXT.Value := Geral.FormataTelefone_TT(QrAlunoCFax.Value);
  QrAlunoCCEL_TXT.Value := Geral.FormataTelefone_TT(QrAlunoCCel.Value);
  //
  QrAlunoLTEL_TXT.Value := Geral.FormataTelefone_TT(QrAlunoLTel.Value);
  QrAlunoLFAX_TXT.Value := Geral.FormataTelefone_TT(QrAlunoLFax.Value);
  QrAlunoLCEL_TXT.Value := Geral.FormataTelefone_TT(QrAlunoLCel.Value);*)
  //
  //QrAlunoECEP_TXT.Value := MLAGeral.FormataCEP_TT(IntToStr(QrAlunoECEP.Value));
  QrAlunoPCEP_TXT.Value := MLAGeral.FormataCEP_TT(IntToStr(QrAlunoPCEP.Value));
  if (QrAlunoPCEP_TXT.Value <> '00000-000') and (QrAlunoPCEP_TXT.Value <> '')
  then QrAlunoPCEP_TXT.Value := 'CEP '+QrAlunoPCEP_TXT.Value;
  //QrAlunoCCEP_TXT.Value := MLAGeral.FormataCEP_TT(IntToStr(QrAlunoCCEP.Value));
  //QrAlunoLCEP_TXT.Value := MLAGeral.FormataCEP_TT(IntToStr(QrAlunoLCEP.Value));
  //
  (*case QrAlunoUserCad.Value of
    -2: QrAlunoNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrAlunoNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrAlunoNOMECAD2.Value := 'N�o definido';
    else QrAlunoNOMECAD2.Value := QrAlunoNOMECAD.Value;
  end;
  case QrAlunoUserAlt.Value of
    -2: QrAlunoNOMEALT2.Value := 'BOSS [Administrador]';
    -1: QrAlunoNOMEALT2.Value := 'MASTER [Admin.DERMATEK]';
     0: QrAlunoNOMEALT2.Value := '- - -';
    else QrAlunoNOMEALT2.Value := QrAlunoNOMEALT.Value;
  end;*)
end;

procedure TFmMatriculContrato.SpeedButton1Click(Sender: TObject);
var
  Carta: Integer;
begin
  VAR_CADASTRO := 0;
  Carta        := EdEscolha.ValueVariant;
  //
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 3;
    if Carta <> 0 then
      FmCartas.LocCod(Carta, Carta);
    FmCartas.ShowModal;
    FmCartas.Destroy;
    //
    UMyMod.AbreQuery(QrEscolha, DMod.MyDB);
    //
    EdEscolha.ValueVariant := VAR_CADASTRO;
    CBEscolha.KeyValue     := VAR_CADASTRO;
    EdEscolha.SetFocus;
  end;
end;

procedure TFmMatriculContrato.frxContratoGetValue(const VarName: String;
  var Value: Variant);
begin

  if      VarName = '(S)ATIVI' then Value := FPlural
  else if VarName = 'F_NOME  ' then Value := QrAlunoNOMEENTIDADE.Value
  else if VarName = 'F_NASCIM' then Value := Geral.FDT(QrAlunoPNatal.Value, 2)

  else if VarName = 'C_NOME  ' then Value := QrRespoNome.Value
  else if VarName = 'C_RG_NUM' then Value := QrRespoRG.Value
  else if VarName = 'C_RG_ORG' then Value := QrRespoSSP.Value
  else if VarName = 'C_RG_DTA' then Value := QrRespoDataRG_TXT.Value
  else if VarName = 'C_CPF   ' then Value := Geral.FormataCNPJ_TT(QrRespoCPF.Value)
  else if VarName = 'C_NACION' then Value := QrRespoNacionalid.Value
  else if VarName = 'C_ESTCIV' then Value := QrRespoNO_EstCivil.Value
  else if VarName = 'C_PROFIS' then Value := QrRespoProfissao.Value
  else if VarName = 'C_MAIL  ' then Value := QrRespoPEmail.Value
  else if VarName = 'P_T_LOGR' then Value := QrRespoNO_PLograd.Value
  else if VarName = 'P_N_LOGR' then Value := QrRespoPRua.Value
  else if VarName = 'P_N�MERO' then Value := QrRespoPNumero.Value
  else if VarName = 'P_COMPLE' then Value := QrRespoPCompl.Value
  else if VarName = 'P_BAIRRO' then Value := QrRespoPBairro.Value
  else if VarName = 'P_CIDADE' then Value := QrRespoPCidade.Value
  else if VarName = 'P_UF    ' then Value := QrRespoUF_TXT.Value
  else if VarName = 'P_CEP   ' then Value := Geral.FormataCEP_NN(QrRespoPCEP.Value)
  else if VarName = 'P_TEL1  ' then Value := Geral.FormataTelefone_TT_Curto(QrRespoPTe1.Value)
  else if VarName = 'P_TEL2  ' then Value := Geral.FormataTelefone_TT_Curto(QrRespoPTe2.Value)
  else if VarName = 'P_CELU  ' then Value := Geral.FormataTelefone_TT_Curto(QrRespoPCel.Value)

  else if VarName = 'RG      ' then Value := QrAlunoRG.Value
  else if VarName = 'CPF     ' then Value := Geral.FormataCNPJ_TT(QrAlunoCPF.Value)
  else if VarName = 'HORARIOS' then Value := FHorari
  else if VarName = 'INVESTIM' then Value := FInvest
  else if VarName = 'PARCELAS' then Value := FParcel
  else if VarName = 'HOJE    ' then Value := FDataCo
  else if VarName = 'CONTRATO' then Value := FContra
  else if VarName = 'PERIODO ' then Value := FPeriod
  else if VarName = 'OA      ' then
  begin
    if QrAlunoSexo.Value = 'F' then Value := 'a' else Value := 'o';
  end else if VarName = '_A      ' then
  begin
    if QrAlunoSexo.Value = 'F' then Value := 'a' else Value := '';
  end
end;

end.
