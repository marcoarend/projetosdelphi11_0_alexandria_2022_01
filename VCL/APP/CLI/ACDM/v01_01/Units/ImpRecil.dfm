object FmImpReci: TFmImpReci
  Left = 374
  Top = 188
  Caption = 'Textos Impressora N'#227'o Fiscal'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 784
    Height = 406
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 406
      Align = alClient
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 782
        Height = 404
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Cabe'#231'alho'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Grade0: TDBGrid
            Left = 0
            Top = 0
            Width = 782
            Height = 378
            Align = alClient
            DataSource = DsImpReciC
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = Grade0DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Texto'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                Width = 484
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Modo'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEMODO'
                Title.Caption = 'Descri'#231#227'o modo'
                Width = 148
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ital'
                Title.Alignment = taCenter
                Title.Caption = 'I'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'Arial'
                Title.Font.Style = [fsItalic]
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Subl'
                Title.Alignment = taCenter
                Title.Caption = 'S'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsUnderline]
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Expa'
                Title.Alignment = taCenter
                Title.Caption = 'E'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -13
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Negr'
                Title.Alignment = taCenter
                Title.Caption = 'N'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 18
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Preview cabe'#231'alho'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 521
            Height = 378
            Align = alLeft
            DataSource = DsImpReciC
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -40
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGrid1DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Texto'
                Width = 484
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Rodap'#233
          ImageIndex = 2
          object Grade2: TDBGrid
            Left = 0
            Top = 0
            Width = 774
            Height = 376
            Align = alClient
            DataSource = DsImpReciR
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = Grade2DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Texto'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Courier New'
                Font.Style = []
                Width = 484
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Modo'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEMODO'
                Title.Caption = 'Descri'#231#227'o modo'
                Width = 148
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ital'
                Title.Alignment = taCenter
                Title.Caption = 'I'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'Arial'
                Title.Font.Style = [fsItalic]
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Subl'
                Title.Alignment = taCenter
                Title.Caption = 'S'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsUnderline]
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Expa'
                Title.Alignment = taCenter
                Title.Caption = 'E'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -13
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Negr'
                Title.Alignment = taCenter
                Title.Caption = 'N'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 18
                Visible = True
              end>
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Preview rodap'#233
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 521
            Height = 378
            Align = alLeft
            DataSource = DsImpReciR
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -40
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGrid3DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Texto'
                Width = 484
                Visible = True
              end>
          end
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtCancela: TBitBtn
      Left = 689
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sair'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtCancelaClick
      NumGlyphs = 2
    end
    object BtImprime: TBitBtn
      Left = 13
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Imprime'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtImprimeClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 40
    Align = alTop
    Caption = 'Textos Impressora N'#227'o Fiscal'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 36
    end
  end
  object TbImpReciC: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Tipo=1'
    Filtered = True
    BeforePost = TbImpReciCBeforePost
    OnCalcFields = TbImpReciCCalcFields
    TableName = 'impreci'
    Left = 116
    Top = 148
    object TbImpReciCTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object TbImpReciCLinha: TAutoIncField
      FieldName = 'Linha'
    end
    object TbImpReciCTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object TbImpReciCLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbImpReciCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbImpReciCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbImpReciCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbImpReciCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbImpReciCModo: TIntegerField
      FieldName = 'Modo'
    end
    object TbImpReciCNegr: TIntegerField
      FieldName = 'Negr'
    end
    object TbImpReciCItal: TIntegerField
      FieldName = 'Ital'
    end
    object TbImpReciCSubl: TIntegerField
      FieldName = 'Subl'
    end
    object TbImpReciCExpa: TIntegerField
      FieldName = 'Expa'
    end
    object TbImpReciCNOMEMODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMODO'
      Calculated = True
    end
  end
  object DsImpReciC: TDataSource
    DataSet = TbImpReciC
    Left = 144
    Top = 148
  end
  object TbImpReciR: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Tipo=2'
    Filtered = True
    BeforePost = TbImpReciRBeforePost
    OnCalcFields = TbImpReciRCalcFields
    TableName = 'impreci'
    Left = 172
    Top = 148
    object TbImpReciRTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object TbImpReciRLinha: TAutoIncField
      FieldName = 'Linha'
    end
    object TbImpReciRTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
    object TbImpReciRLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbImpReciRDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbImpReciRDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbImpReciRUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbImpReciRUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbImpReciRModo: TIntegerField
      FieldName = 'Modo'
    end
    object TbImpReciRNegr: TIntegerField
      FieldName = 'Negr'
    end
    object TbImpReciRItal: TIntegerField
      FieldName = 'Ital'
    end
    object TbImpReciRSubl: TIntegerField
      FieldName = 'Subl'
    end
    object TbImpReciRExpa: TIntegerField
      FieldName = 'Expa'
    end
    object TbImpReciRNOMEMODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMODO'
      Calculated = True
    end
  end
  object DsImpReciR: TDataSource
    DataSet = TbImpReciR
    Left = 200
    Top = 148
  end
end
