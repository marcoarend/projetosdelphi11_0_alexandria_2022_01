unit TurmasTmp2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkGeral, Variants,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, Grids, DBGrids, dmkDBGridDAC,
  UnDmkEnums;

type
  TFmTurmasTmp2 = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    DsPeriodos: TDataSource;
    QrPeriodos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    Label4: TLabel;
    EdControle: TdmkEdit;
    SpeedButton5: TSpeedButton;
    DBGrid1: TdmkDBGridDAC;
    QrPeriodosAtivo: TSmallintField;
    QrPeriodosHIni: TTimeField;
    QrPeriodosHFim: TTimeField;
    QrPeriodosNome: TWideStringField;
    QrPeriodosCodigo: TIntegerField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrLoc: TmySQLQuery;
    RGPeriodos: TRadioGroup;
    QrPeriodosDSem: TIntegerField;
    QrPeriodosNOMEDSEM: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure RGPeriodosClick(Sender: TObject);
  private
    { Private declarations }
    FHorarios0: String;
    procedure ReopenPeriodos(Codigo: Integer);
    procedure AtivarTodos(Ativo: Integer);
  public
    { Public declarations }
    procedure PreparaPeriodos();
  end;

var
  FmTurmasTmp2: TFmTurmasTmp2;

implementation

uses UnMyObjects, Module, MyDBCheck, Periodos, ModuleGeral, UCreate, Turmas;

{$R *.DFM}

procedure TFmTurmasTmp2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTurmasTmp2.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmTurmasTmp2.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmTurmasTmp2.AtivarTodos(Ativo: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrPeriodosCodigo.Value;
    DmodG.QrUpdPID1.Close;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FHorarios0 + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Ativo=:P0');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenPeriodos(Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTurmasTmp2.BtConfirmaClick(Sender: TObject);
var
  Conta, Periodo: Integer;
begin
  if RGPeriodos.ItemIndex <> 0 then
  begin
    Geral.MensagemBox('Para confirmar � necess�rio que o item selecionado no ' +
    '"Filtro por dia da semana" seja "Todos"', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    QrPeriodos.DisableControls;
    QrPeriodos.First;
    while not QrPeriodos.Eof do
    begin
      Periodo := QrPeriodosCodigo.Value;
      QrLoc.Close;
      QrLoc.Params[00].AsInteger := EdCodigo.ValueVariant;
      QrLoc.Params[01].AsInteger := EdControle.ValueVariant;
      QrLoc.Params[02].AsInteger := Periodo;
      QrLoc.Open;
      if (Qrloc.RecordCount = 0) and (QrPeriodosAtivo.Value = 1) then
      begin
        Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'TurmasTmp', 'TurmasTmp', 'Codigo');
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('INSERT INTO TurmasTmp SET ');
        Dmod.QrUpdM.SQL.Add('Codigo=:P0, Controle=:P1, Conta=:P2, Periodo=:P3');
        //
        Dmod.QrUpdM.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
        Dmod.QrUpdM.Params[01].AsInteger := Geral.IMV(EdControle.Text);
        Dmod.QrUpdM.Params[02].AsInteger := Conta;
        Dmod.QrUpdM.Params[03].AsInteger := Periodo;
        Dmod.QrUpdM.ExecSQL;
      end
      else if (Qrloc.RecordCount = 1) and (QrPeriodosAtivo.Value = 0) then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM turmastmp WHERE Codigo=:P0');
        Dmod.QrUpd.SQL.Add('AND Controle=:P1 AND Periodo=:P2');
        Dmod.QrUpd.Params[00].AsInteger := EdCodigo.ValueVariant;
        Dmod.QrUpd.Params[01].AsInteger := EdControle.ValueVariant;
        Dmod.QrUpd.Params[02].AsInteger := Periodo;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      QrPeriodos.Next;
    end;
    FmTurmas.ReopenTurmasTmp();
    Close;
  finally
    Screen.Cursor := crHourGlass;
  end;
{
var
  Conta, Periodo: Integer;
begin
  Periodo := StrToInt(EdPeriodo.Text);
  //
  if MyObjects.FIC(CBPeriodo.KeyValue = 0, EdPeriodo, 'Informe um per�odo!') then Exit;
  //
  Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'TurmasTmp', 'TurmasTmp', 'Codigo');
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO TurmasTmp SET ');
  Dmod.QrUpdM.SQL.Add('Codigo=:P0, Controle=:P1, Conta=:P2, Periodo=:P3');
  //
  Dmod.QrUpdM.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdM.Params[01].AsInteger := Geral.IMV(EdControle.Text);
  Dmod.QrUpdM.Params[02].AsInteger := Conta;
  Dmod.QrUpdM.Params[03].AsInteger := Periodo;
  Dmod.QrUpdM.ExecSQL;
  Close;
}
end;

procedure TFmTurmasTmp2.FormCreate(Sender: TObject);
begin
  QrPeriodos.Close;
  QrPeriodos.Database := DModG.MyPID_DB;
  FHorarios0 := Ucriar.RecriaTempTable('Horarios0', DmodG.QrUpdPID1, False);
end;

procedure TFmTurmasTmp2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmTurmasTmp2.PreparaPeriodos();
begin
  QrPeriodos.SQL.Clear;
  QrPeriodos.SQL.Add('INSERT INTO ' + FHorarios0);
  QrPeriodos.SQL.Add('SELECT 0 Ativo, DSem, ELT(DSem+1, "Domingo", "Segunda", ');
  QrPeriodos.SQL.Add('"Ter�a","Quarta", "Quinta", "Sexta", "S�bado") NOMEDSEM,');
  QrPeriodos.SQL.Add('HIni, HFim, Nome, Codigo');
  QrPeriodos.SQL.Add('FROM '+TMeuDB+'.periodos');
  QrPeriodos.SQL.Add('ORDER BY Nome');
  QrPeriodos.ExecSQL;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FHorarios0);
  DModG.QrUpdPID1.SQL.Add('Set Ativo=1 WHERE Codigo=:P0');
  FmTurmas.QrTurmasTmp.First;
  while not FmTurmas.QrTurmasTmp.Eof do
  begin
    //
    DModG.QrUpdPID1.Params[0].AsInteger := FmTurmas.QrTurmasTmpPeriodo.Value;
    DModG.QrUpdPID1.ExecSQL;
    FmTurmas.QrTurmasTmp.Next;
  end;
  //
  ReopenPeriodos(0);
end;

procedure TFmTurmasTmp2.RGPeriodosClick(Sender: TObject);
begin
  ReopenPeriodos(QrPeriodosCodigo.Value);
end;

procedure TFmTurmasTmp2.ReopenPeriodos(Codigo: Integer);
begin
  QrPeriodos.Close;
  QrPeriodos.SQL.Clear;
  QrPeriodos.SQL.Add('SELECT * FROM ' + FHorarios0);
  if RGPeriodos.ItemIndex > 0 then
    QrPeriodos.SQL.Add('WHERE DSem=' + IntToStr(RGPeriodos.ItemIndex-1));
  QrPeriodos.Open;
  QrPeriodos.Locate('Codigo', Codigo, []);
end;

procedure TFmTurmasTmp2.SpeedButton5Click(Sender: TObject);
begin
  (* N�o habilitar por que usa tabelas tempor�rias
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmPeriodos, FmPeriodos, afmoNegarComAviso) then
  begin
    FmPeriodos.ShowModal;
    FmPeriodos.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(nil, nil, QrPeriodos, VAR_CADASTRO);
  end;
  *)
end;

procedure TFmTurmasTmp2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
