program Academy;

uses
  Academy_MLA in '..\Units\Academy_MLA.pas' {FmAcademy_MLA},
  Module in '..\Units\Module.pas' {Dmod: TDataModule},
  MyListas in '..\Units\MyListas.pas',
  Chamada in '..\Units\Chamada.pas' {FmChamada},
  ChamadasNew in '..\Units\ChamadasNew.pas' {FmChamadasNew},
  ChamadasIts in '..\Units\ChamadasIts.pas' {FmChamadasIts},
  MatriRen in '..\Units\MatriRen.pas' {FmMatriRen},
  MatriTur in '..\Units\MatriTur.pas' {FmMatriTur},
  MatriAti in '..\Units\MatriAti.pas' {FmMatriAti},
  MatriculContrato in '..\Units\MatriculContrato.pas' {FmMatriculContrato},
  SalasEquip in '..\Units\SalasEquip.pas' {FmSalasEquip},
  TurmasEdit in '..\Units\TurmasEdit.pas' {FmTurmasEdit},
  TurmasGru in '..\Units\TurmasGru.pas' {FmTurmasGru},
  TurmasImp in '..\Units\TurmasImp.pas' {FmTurmasImp},
  TarifasEdit in '..\Units\TarifasEdit.pas' {FmTarifasedit},
  ImpRecil in '..\Units\ImpRecil.pas' {FmImpReci},
  ImpPagtos in '..\Units\ImpPagtos.pas' {FmImpPagtos},
  MatrRefresh in '..\Units\MatrRefresh.pas' {FmMatriRefresh},
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  Matricul in '..\Units\Matricul.pas' {FmMatricul},
  Tarifas in '..\Units\Tarifas.pas' {FmTarifas},
  MatriPer in '..\Units\MatriPer.pas' {FmMatriPer},
  MatriPer2 in '..\Units\MatriPer2.pas' {FmMatriPer2},
  TurmasTmp2 in '..\Units\TurmasTmp2.pas' {FmTurmasTmp2},
  TurmasTmp in '..\Units\TurmasTmp.pas' {FmTurmasTmp},
  Turmas in '..\Units\Turmas.pas' {FmTurmas},
  CreateApl in '..\Units\CreateApl.pas',
  TarifasNovas in '..\Units\TarifasNovas.pas' {FmTarifasNovas},
  TarifasGru in '..\Units\TarifasGru.pas' {FmTarifasGru},
  SWMS_Mtng_Its in '..\Units\SWMS_Mtng_Its.pas' {FmSWMS_Mtng_Its},
  SWMS_Mtng_Cab in '..\Units\SWMS_Mtng_Cab.pas' {FmSWMS_Mtng_Cab},
  SWMS_Mtng_Cat in '..\Units\SWMS_Mtng_Cat.pas' {FmSWMS_Mtng_Cat},
  SWMS_Mtng_PrvUni in '..\Units\SWMS_Mtng_PrvUni.pas' {FmSWMS_Mtng_PrvUni},
  SWMS_Mtng_PrvMul in '..\Units\SWMS_Mtng_PrvMul.pas' {FmSWMS_Mtng_PrvMul},
  swms_envi_nadadores in '..\Units\swms_envi_nadadores.pas' {FmSWMS_Envi_Nadadores},
  swms_envi_competipart in '..\Units\swms_envi_competipart.pas' {FmSWMS_Envi_Competipart},
  SWMS_Mtng_Tem in '..\Units\SWMS_Mtng_Tem.pas' {FmSWMS_Mtng_Tem},
  UnitListas in '..\Units\UnitListas.pas',
  Mensais in '..\Units\Mensais.pas' {FmMensais},
  Forms,
  Windows,
  Messages,
  Dialogs,
  GModule in '..\..\..\..\..\UTL\MySQL\v01_01\GModule.pas' {GMod: TDataModule},
  InstallMySQL41 in '..\..\..\..\..\UTL\MySQL\v01_01\InstallMySQL41.pas' {FmInstallMySQL41},
  ModuleGeral in '..\..\..\..\..\UTL\MySQL\v01_01\ModuleGeral.pas' {DModG: TDataModule},
  MyDBCheck in '..\..\..\..\..\UTL\MySQL\v01_01\MyDBCheck.pas',
  MyInis in '..\..\..\..\..\UTL\MySQL\v01_01\MyInis.pas' {FmMyInis},
  ServerConnect in '..\..\..\..\..\UTL\MySQL\v01_01\ServerConnect.pas' {FmServerConnect},
  ServiceManager in '..\..\..\..\..\UTL\MySQL\v01_01\ServiceManager.pas',
  ServicoManager in '..\..\..\..\..\UTL\MySQL\v01_01\ServicoManager.pas' {FmServicoManager},
  Servidor in '..\..\..\..\..\UTL\MySQL\v01_01\Servidor.pas' {FmServidor},
  UCreate in '..\..\..\..\..\UTL\MySQL\v01_01\UCreate.pas',
  UMySQLModule in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLModule.pas',
  UnGOTOy in '..\..\..\..\..\UTL\MySQL\v01_01\UnGOTOy.pas',
  UnMySQLCuringa in '..\..\..\..\..\UTL\MySQL\v01_01\UnMySQLCuringa.pas',
  VerifiDB in '..\..\..\..\..\UTL\MySQL\v01_01\VerifiDB.pas' {FmVerifiDB},
  VerifiDBLocal in '..\..\..\..\..\UTL\MySQL\v01_01\VerifiDBLocal.pas' {FmVerifiDBLocal},
  VerifiDBUsuario in '..\..\..\..\..\UTL\MySQL\v01_01\VerifiDBUsuario.pas' {FmVerifiDBUsuario},
  EntiCargos in '..\..\..\..\..\MDL\ENT\v02_01\EntiCargos.pas' {FmEntiCargos},
  EntiCEP in '..\..\..\..\..\MDL\ENT\v02_01\CEP\EntiCEP.pas' {FmEntiCEP},
  EntiCfgRel_01 in '..\..\..\..\..\MDL\ENT\v02_01\EntiCfgRel_01.pas' {FmEntiCfgRel_01},
  EntiContat in '..\..\..\..\..\MDL\ENT\v02_01\EntiContat.pas' {FmEntiContat},
  EntiCtas in '..\..\..\..\..\MDL\ENT\v02_01\EntiCtas.pas' {FmEntiCtas},
  EntidadesImp in '..\..\..\..\..\MDL\ENT\v02_01\EntidadesImp.pas' {FmEntidadesImp},
  EntidadesLoc in '..\..\..\..\..\MDL\ENT\v01_01\EntidadesLoc.pas' {FmEntidadesLoc},
  EntiMail in '..\..\..\..\..\MDL\ENT\v02_01\EntiMail.pas' {FmEntiMail},
  EntiProd in '..\..\..\..\..\MDL\ENT\v01_01\EntiProd.pas' {FmEntiProd},
  EntiRespon in '..\..\..\..\..\MDL\ENT\v02_01\EntiRespon.pas' {FmEntiRespon},
  EntiSel in '..\..\..\..\..\MDL\ENT\v02_01\EntiSel.pas' {FmEntiSel},
  EntiTel in '..\..\..\..\..\MDL\ENT\v02_01\EntiTel.pas' {FmEntiTel},
  EntiTipCto in '..\..\..\..\..\MDL\ENT\v02_01\EntiTipCto.pas' {FmEntiTipCto},
  EntiTransfere in '..\..\..\..\..\MDL\ENT\v01_01\EntiTransfere.pas' {FmEntiTransfere},
  EntiTransp in '..\..\..\..\..\MDL\ENT\v02_01\EntiTransp.pas' {FmEntiTransp},
  CalcPercent in '..\..\..\..\..\UTL\_FRM\v01_01\CalcPercent.pas' {FmCalcPercent},
  CfgExpFile in '..\..\..\..\..\UTL\_FRM\v01_01\CfgExpFile.pas' {FmCfgExpFile},
  ConfJanela in '..\..\..\..\..\UTL\_FRM\v01_01\ConfJanela.pas' {FmConfJanela},
  Curinga in '..\..\..\..\..\UTL\_FRM\v01_01\Curinga.pas' {FmCuringa},
  DataDef in '..\..\..\..\..\UTL\_FRM\v01_01\DataDef.pas' {FmDataDef},
  EventosCad in '..\..\..\..\..\UTL\_FRM\v01_01\EventosCad.pas' {FmEventosCad},
  Feriados in '..\..\..\..\..\UTL\_FRM\v01_01\Feriados.pas' {FmFeriados},
  GetData in '..\..\..\..\..\UTL\_FRM\v01_01\GetData.pas' {FmGetData},
  GetPercent in '..\..\..\..\..\UTL\_FRM\v01_01\GetPercent.pas' {FmGetPercent},
  GetPeriodo in '..\..\..\..\..\UTL\_FRM\v01_01\GetPeriodo.pas' {FmGetPeriodo},
  GetValor in '..\..\..\..\..\UTL\_FRM\v01_01\GetValor.pas' {FmGetValor},
  InfoSeq in '..\..\..\..\..\UTL\_FRM\v01_01\InfoSeq.pas' {FmInfoSeq},
  MyDBGridReorder in '..\..\..\..\..\UTL\_FRM\v01_01\MyDBGridReorder.pas' {FmMyDBGridReorder},
  MyGlyfs in '..\..\..\..\..\UTL\_FRM\v01_01\MyGlyfs.pas' {FmMyGlyfs},
  NomeX in '..\..\..\..\..\UTL\_FRM\v01_01\NomeX.pas' {FmNomeX},
  NovaVersao in '..\..\..\..\..\UTL\_FRM\v01_01\NovaVersao.pas' {FmNovaVersao},
  Opcoes in '..\..\..\..\..\UTL\_FRM\v01_01\Opcoes.pas' {FmOpcoes},
  Periodo in '..\..\..\..\..\UTL\_FRM\v01_01\Periodo.pas' {FmPeriodo},
  PesqDescri in '..\..\..\..\..\UTL\_FRM\v01_01\PesqDescri.pas' {FmPesqDescri},
  PesqNome in '..\..\..\..\..\UTL\_FRM\v01_01\PesqNome.pas' {FmPesqNome},
  Ponto in '..\..\..\..\..\UTL\_FRM\v01_01\Ponto.pas' {FmPonto},
  PontoFunci in '..\..\..\..\..\UTL\_FRM\v01_01\PontoFunci.pas' {FmPontoFunci},
  QuaisItens in '..\..\..\..\..\UTL\_FRM\v01_01\QuaisItens.pas' {FmQuaisItens},
  Recibo in '..\..\..\..\..\UTL\_FRM\v01_01\Recibo.pas' {FmRecibo},
  Recibos in '..\..\..\..\..\UTL\_FRM\v01_01\Recibos.pas' {FmRecibos},
  SelCod in '..\..\..\..\..\UTL\_FRM\v01_01\SelCod.pas' {FmSelCod},
  SelOnStringGrid in '..\..\..\..\..\UTL\_FRM\v01_01\SelOnStringGrid.pas' {FmSelOnStringGrid},
  Senha in '..\..\..\..\..\UTL\_FRM\v01_01\Senha.pas' {FmSenha},
  SenhaBoss in '..\..\..\..\..\UTL\_FRM\v01_01\SenhaBoss.pas' {FmSenhaBoss},
  SenhaEspecial in '..\..\..\..\..\UTL\_FRM\v01_01\SenhaEspecial.pas' {FmSenhaEspecial},
  Terminais in '..\..\..\..\..\UTL\_FRM\v01_01\Terminais.pas' {FmTerminais},
  UnitMyXML in '..\..\..\..\..\UTL\XML\v01_01\UnitMyXML.pas',
  IGPM in '..\..\..\..\..\UTL\Tabelas\v01_01\IGPM.pas' {FmIGPM},
  ListServ in '..\..\..\..\..\UTL\Tabelas\v01_01\ListServ.pas' {FmListServ},
  UnCEP in '..\..\..\..\..\MDL\ENT\v02_01\CEP\UnCEP.pas',
  CEP_Result in '..\..\..\..\..\MDL\ENT\v02_01\CEP\CEP_Result.pas' {FmCEP_Result},
  reinit in '..\..\..\..\..\UTL\Textos\v01_01\reinit.pas',
  LctEncerraMes in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LctEncerraMes.pas' {FmLctEncerraMes},
  LctMudaCart in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\LctMudaCart.pas' {FmLctMudaCart},
  LctMudaConta in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LctMudaConta.pas' {FmLctMudaConta},
  LctPgEmCxa in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LctPgEmCxa.pas' {FmLctPgEmCxa},
  LocDefs in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LocDefs.pas' {FmLocDefs},
  LocLancto in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LocLancto.pas' {FmLocLancto},
  LctDuplic in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\LctDuplic.pas' {FmLctDuplic},
  LctEdit in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\LctEdit.pas' {FmLctEdit},
  UnMyPrinters in '..\..\..\..\..\MDL\PRIN\v01_01\UnMyPrinters.pas',
  EmiteCheque_0 in '..\..\..\..\..\MDL\PRIN\v01_01\EmiteCheque_0.pas' {FmEmiteCheque_0},
  EmiteCheque_1 in '..\..\..\..\..\MDL\PRIN\v01_01\EmiteCheque_1.pas' {FmEmiteCheque_1},
  EmiteCheque_3 in '..\..\..\..\..\MDL\PRIN\v01_01\EmiteCheque_3.pas' {FmEmiteCheque_3},
  UnBematech_DP_20 in '..\..\..\..\..\MDL\PRIN\v01_01\UnBematech_DP_20.pas',
  PertoChek in '..\..\..\..\..\MDL\PRIN\v01_01\PertoChek.pas',
  Conjuntos in '..\..\..\..\..\MDL\FIN\v01_01\Planos\Conjuntos.pas' {FmConjuntos},
  Contas in '..\..\..\..\..\MDL\FIN\v01_01\Planos\Contas.pas' {FmContas},
  ContasConfCad in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasConfCad.pas' {FmContasConfCad},
  ContasConfPgto in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasConfPgto.pas' {FmContasConfPgto},
  ContasExclui in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasExclui.pas' {FmContasExclui},
  ContasHistAtz in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasHistAtz.pas' {FmContasHistAtz},
  ContasHistSdo3 in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasHistSdo3.pas' {FmContasHistSdo3},
  ContasLnk in '..\..\..\..\..\MDL\CMEB\v01_01\ContasLnk.pas' {FmContasLnk},
  ContasLnkEdit in '..\..\..\..\..\MDL\CMEB\v01_01\ContasLnkEdit.pas' {FmContasLnkEdit},
  ContasLnkIts in '..\..\..\..\..\MDL\CMEB\v01_01\ContasLnkIts.pas' {FmContasLnkIts},
  ContasMes in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasMes.pas' {FmContasMes},
  ContasMesSelMulCta in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasMesSelMulCta.pas' {FmContasMesSelMulCta},
  ContasNiv in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasNiv.pas' {FmContasNiv},
  ContasNivSel in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasNivSel.pas' {FmContasNivSel},
  ContasSdoImp in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasSdoImp.pas' {FmContasSdoImp},
  GetCarteira in '..\..\..\..\..\MDL\CMEB\v01_01\GetCarteira.pas' {FmGetCarteira},
  Grupos in '..\..\..\..\..\MDL\FIN\v01_01\Planos\Grupos.pas' {FmGrupos},
  NivelEGenero in '..\..\..\..\..\MDL\FIN\v01_01\Planos\NivelEGenero.pas' {FmNivelEGenero},
  PlaCtas in '..\..\..\..\..\MDL\FIN\v01_01\Planos\PlaCtas.pas' {FmPlaCtas},
  Plano in '..\..\..\..\..\MDL\FIN\v01_01\Planos\Plano.pas' {FmPlano},
  SelConta in '..\..\..\..\..\MDL\CMEB\v01_01\SelConta.pas' {FmSelConta},
  SubGrupos in '..\..\..\..\..\MDL\FIN\v01_01\Planos\SubGrupos.pas' {FmSubGrupos},
  SubGruposExclui in '..\..\..\..\..\MDL\FIN\v01_01\Planos\SubGruposExclui.pas' {FmSubGruposExclui},
  Carteiras in '..\..\..\..\..\MDL\FIN\v01_01\Forms\Carteiras.pas' {FmCarteiras},
  CarteirasU in '..\..\..\..\..\MDL\FIN\v01_01\Forms\CarteirasU.pas' {FmCarteirasU},
  CashBal in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\CashBal.pas' {FmCashBal},
  CashPreCta in '..\..\..\..\..\MDL\FIN\v01_01\Forms\CashPreCta.pas' {FmCashPreCta},
  CopiaDoc in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\CopiaDoc.pas' {FmCopiaDoc},
  FinOrfao1 in '..\..\..\..\..\MDL\FIN\v01_01\Forms\FinOrfao1.pas' {FmFinOrfao1},
  FluxoCxa in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\FluxoCxa.pas' {FmFluxoCxa},
  ModuleFin in '..\..\..\..\..\MDL\FIN\v01_01\DataModules\ModuleFin.pas' {DmodFin: TDataModule},
  SelfGer2 in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\SelfGer2.pas' {FmSelfGer2},
  SomaDinheiro in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\SomaDinheiro.pas' {FmSomaDinheiro},
  SomaDinheiroCH in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\SomaDinheiroCH.pas' {FmSomaDinheiroCH},
  Transfer2 in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\Transfer2.pas' {FmTransfer2},
  TransferCtas in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\TransferCtas.pas' {FmTransferCtas},
  UnFinanceiro in '..\..\..\..\..\MDL\FIN\v01_01\Units\UnFinanceiro.pas',
  DockForm in '..\..\..\..\..\UTL\_FRM\v01_01\DockForm.pas' {FmDockForm},
  Resmes in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Resmes.pas' {FmResMes},
  MeuFrx in '..\..\..\..\..\UTL\Print\v01_01\MeuFrx.pas' {FmMeuFrx},
  Concilia in '..\..\..\..\..\MDL\CMEB\v01_01\Concilia.pas' {FmConcilia},
  ConciliaMultiCtas in '..\..\..\..\..\MDL\CMEB\v01_01\ConciliaMultiCtas.pas' {FmConciliaMultiCtas},
  ConciliaOFX in '..\..\..\..\..\MDL\CMEB\v01_01\ConciliaOFX.pas' {FmConciliaOFX},
  ConciliaXLSLoad in '..\..\..\..\..\MDL\CMEB\v01_01\ConciliaXLSLoad.pas' {FmConciliaXLSLoad},
  ModuleBco in '..\..\..\..\..\MDL\BCO\v01_01\Geral\ModuleBco.pas' {DmBco: TDataModule},
  Bancos in '..\..\..\..\..\MDL\BCO\v01_01\Geral\Bancos.pas' {FmBancos},
  MyPagtos in '..\..\..\..\..\MDL\FIN\v01_01\Forms\MyPagtos.pas' {FmMyPagtos},
  QuitaDoc2 in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\QuitaDoc2.pas' {FmQuitaDoc2},
  FormulariosCidades in '..\..\..\..\..\UTL\Etiquetas\v01_01\FormulariosCidades.pas' {FmFormulariosCidades},
  FormulariosEntidades in '..\..\..\..\..\UTL\Etiquetas\v01_01\FormulariosEntidades.pas' {FmFormulariosEntidades},
  FormulariosResult in '..\..\..\..\..\UTL\Etiquetas\v01_01\FormulariosResult.pas' {FmFormulariosResult},
  Maladireta in '..\..\..\..\..\UTL\Etiquetas\v01_01\Maladireta.pas' {FmMaladireta},
  MalaDiretaProfiss in '..\..\..\..\..\UTL\Etiquetas\v01_01\MalaDiretaProfiss.pas' {FmMalaDiretaProfiss},
  MultiEtiq in '..\..\..\..\..\UTL\Etiquetas\v01_01\MultiEtiq.pas' {FmMultiEtiq},
  MultiEtiqNew in '..\..\..\..\..\UTL\Etiquetas\v01_01\MultiEtiqNew.pas' {FmMultiEtiqNew},
  PagesNew in '..\..\..\..\..\UTL\Etiquetas\v01_01\PagesNew.pas' {FmPagesNew},
  Imprime in '..\..\..\..\..\MDL\PRIN\v01_01\Imprime.pas' {FmImprime},
  ImprimeEmpr in '..\..\..\..\..\MDL\PRIN\v01_01\ImprimeEmpr.pas' {FmImprimeEmpr},
  ImprimeView in '..\..\..\..\..\MDL\PRIN\v01_01\ImprimeView.pas' {FmImprimeView},
  ImprimeBand in '..\..\..\..\..\MDL\PRIN\v01_01\ImprimeBand.pas' {FmImprimeBand},
  Pages in '..\..\..\..\..\UTL\Etiquetas\v01_01\Pages.pas' {FmPages},
  UnMyObjects in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  Backup3 in '..\..\..\..\..\UTL\MySQL\v01_01\Backup3.pas' {FmBackup3},
  UnMLAGeral in '..\..\..\..\..\UTL\_UNT\v01_01\UnMLAGeral.pas',
  UnIBGE_DTB_Tabs in '..\..\..\..\..\MDL\ENT\v02_01\IBGE\UnIBGE_DTB_Tabs.pas',
  Entidades in '..\..\..\..\..\MDL\ENT\v01_01\Entidades.pas' {FmEntidades},
  EntidadesNew in '..\..\..\..\..\MDL\ENT\v01_01\EntidadesNew.pas' {FmEntidadesNew},
  Entidade2 in '..\..\..\..\..\MDL\ENT\v02_01\Entidade2.pas' {FmEntidade2},
  UnInternalConsts in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnInternalConsts3 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts3.pas',
  ZCF2 in '..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  UnMsgInt in '..\..\..\..\..\UTL\_UNT\v01_01\UnMsgInt.pas',
  UnInternalConsts2 in '..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  ResIntStrings in '..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnMyVCLref in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVCLref.pas',
  MyVCLSkin in '..\..\..\..\..\UTL\_UNT\v01_01\MyVCLSkin.pas',
  Matriz in '..\..\..\..\..\MDL\EMP\v01_01\Matriz.pas' {FmMatriz},
  EmiteCheque_4 in '..\..\..\..\..\MDL\PRIN\v01_01\EmiteCheque_4.pas' {FmEmiteCheque_4},
  UnLic_Dmk in '..\..\..\..\..\UTL\_UNT\v01_01\UnLic_Dmk.pas',
  GetBiosInformation in '..\..\..\..\..\UTL\_UNT\v01_01\GetBiosInformation.pas',
  LctAjustes in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LctAjustes.pas' {FmLctAjustes},
  UnLock_Dmk in '..\..\..\..\DMK\UnLock\v01_01\UnLock_Dmk.pas' {FmUnLock_Dmk},
  UnitMD5 in '..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  LctEdita in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\LctEdita.pas' {FmLctEdita},
  ABOUT in '..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {AboutBox},
  Entidade2Imp in '..\..\..\..\..\MDL\ENT\v02_01\Entidade2Imp.pas' {FmEntidade2Imp},
  PesqESel in '..\..\..\..\..\UTL\_FRM\v01_01\PesqESel.pas' {FmPesqESel},
  MeuDBUses in '..\..\..\..\..\UTL\MySQL\v01_01\MeuDBUses.pas' {FmMeuDBUses},
  LinkRankSkin in '..\..\..\..\..\UTL\_FRM\v01_01\LinkRankSkin.pas' {FmLinkRankSkin},
  WBEdit in '..\..\..\..\..\UTL\WEB\HTML\v01_01\Editor HTML\WBEdit.pas' {FmWBEdit},
  soapserver in '..\..\..\..\..\MDL\WEB\v01_01\WebService\soapserver.pas',
  SelIts in '..\..\..\..\..\UTL\_FRM\v01_01\SelIts.pas' {FmSelIts},
  SelRadioGroup in '..\..\..\..\..\UTL\_FRM\v01_01\SelRadioGroup.pas' {FmSelRadioGroup},
  IndiPag in '..\..\..\..\..\MDL\FIN\v01_01\Forms\IndiPag.pas' {FmIndiPag},
  ChConfigCab in '..\..\..\..\..\MDL\CHEQ\v01_01\ChConfigCab.pas' {FmChConfigCab},
  ChConfigIts in '..\..\..\..\..\MDL\CHEQ\v01_01\ChConfigIts.pas' {FmChConfigIts},
  DotPrint in '..\..\..\..\..\MDL\PRIN\v01_01\DotPrint.pas' {FmDotPrint},
  ImpDOS in '..\..\..\..\..\MDL\PRIN\v01_01\ImpDOS.pas' {FmImpDOS},
  ZStepCodUni in '..\..\..\..\..\UTL\_FRM\v01_01\ZStepCodUni.pas' {FmZStepCodUni},
  Saldos in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Saldos.pas' {FmSaldos},
  dmkSOAP in '..\..\..\..\..\MDL\WEB\v01_01\WebService\dmkSOAP.pas',
  UnDmkProcFunc in '..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  UCreateEDrop in '..\..\..\..\..\UTL\_UNT\v01_01\UCreateEDrop.pas',
  dmkDAC_PF in '..\..\..\..\..\UTL\MySQL\v01_01\dmkDAC_PF.pas',
  EntiCEP2 in '..\..\..\..\..\MDL\ENT\v02_01\CEP\EntiCEP2.pas' {FmEntiCEP2},
  CfgCadLista in '..\..\..\..\..\UTL\MySQL\v01_01\CfgCadLista.pas',
  CadLista in '..\..\..\..\..\UTL\MySQL\v01_01\CadLista.pas' {FmCadLista},
  CustomFR3Imp in '..\..\..\..\..\UTL\Print\v01_01\CustomFR3Imp.pas' {FmCustomFR3Imp},
  CustomFR3 in '..\..\..\..\..\UTL\Print\v01_01\CustomFR3.pas' {FmCustomFR3},
  cfgAtributos in '..\..\..\..\..\UTL\_FRM\v01_01\cfgAtributos.pas',
  AtrDef in '..\..\..\..\..\UTL\_FRM\v01_01\AtrDef.pas' {FmAtrDef},
  AtrCad in '..\..\..\..\..\UTL\_FRM\v01_01\AtrCad.pas' {FmAtrCad},
  AtrIts in '..\..\..\..\..\UTL\_FRM\v01_01\AtrIts.pas' {FmAtrIts},
  EntiCliInt in '..\..\..\..\..\MDL\EMP\v01_01\EntiCliInt.pas' {FmEntiCliInt},
  EntiMapa in '..\..\..\..\..\MDL\ENT\v02_01\EntiMapa.pas' {FmEntiMapa},
  EntiSrvPro in '..\..\..\..\..\MDL\ENT\v02_01\EntiSrvPro.pas' {FmEntiSrvPro},
  UnPerfJan_Tabs in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnPerfJan_Tabs.pas',
  CadItemGrupo in '..\..\..\..\..\UTL\MySQL\v01_01\CadItemGrupo.pas' {FmCadItemGrupo},
  CadRegistroSimples in '..\..\..\..\..\UTL\MySQL\v01_01\CadRegistroSimples.pas' {FmCadRegistroSimples},
  UnConsultasWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnConsultasWeb.pas',
  EntiRFCPF in '..\..\..\..\..\MDL\ENT\v02_01\EntiRFCPF.pas' {FmEntiRFCPF},
  EntiRFCNPJ in '..\..\..\..\..\MDL\ENT\v02_01\EntiRFCNPJ.pas' {FmEntiRFCNPJ},
  GetDataEUser in '..\..\..\..\..\UTL\_FRM\v01_01\GetDataEUser.pas' {FmGetDataEUser},
  CNAE21Cad in '..\..\..\..\..\UTL\Tabelas\v01_01\CNAE21Cad.pas' {FmCNAE21Cad},
  CNAE21 in '..\..\..\..\..\UTL\Tabelas\v01_01\CNAE21.pas' {FmCNAE21},
  PesqRCNAE21 in '..\..\..\..\..\UTL\Tabelas\v01_01\PesqRCNAE21.pas' {FmPesqRCNAE21},
  PesqCNAE21 in '..\..\..\..\..\UTL\Tabelas\v01_01\PesqCNAE21.pas' {FmPesqCNAE21},
  Textos in '..\..\..\..\..\UTL\Textos\v01_01\Textos.pas' {FmTextos},
  ZForge in '..\..\..\..\..\UTL\_FRM\v01_01\ZForge.pas' {FmZForge},
  VerifiDBTerceiros in '..\..\..\..\..\UTL\MySQL\v01_01\VerifiDBTerceiros.pas' {FmVerifiDBTerceiros},
  NFSe_TbTerc in '..\..\..\..\..\MDL\NFSe\Geral\NFSe_TbTerc.pas',
  Geral_TbTerc in '..\..\..\..\..\UTL\_UNT\v01_01\Geral_TbTerc.pas',
  Cartas in '..\..\..\..\..\MDL\TEXT\v01_01\Cartas.pas' {FmCartas},
  CartaG in '..\..\..\..\..\MDL\TEXT\v01_01\CartaG.pas' {FmCartaG},
  Textos2 in '..\..\..\..\..\UTL\Textos\v01_01\Textos2.pas' {FmTextos2},
  EntiImagens in '..\..\..\..\..\MDL\ENT\v02_01\EntiImagens.pas' {FmEntiImagens},
  EntiRE_IE in '..\..\..\..\..\MDL\ENT\v02_01\EntiRE_IE.pas' {FmEntiRE_IE},
  Entidade2ImpOne in '..\..\..\..\..\MDL\ENT\v02_01\Entidade2ImpOne.pas' {FmEntidade2ImpOne},
  SelCods in '..\..\..\..\..\UTL\_FRM\v01_01\SelCods.pas' {FmSelCods},
  UnDmkWeb in '..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb.pas',
  MyAppConsts in '..\Units\MyAppConsts.pas',
  EntiConEnt in '..\..\..\..\..\MDL\ENT\v02_01\EntiConEnt.pas' {FmEntiConEnt},
  CoresVCLSkin in '..\..\..\..\..\UTL\_FRM\v01_01\CoresVCLSkin.pas' {FmCoresVCLSkin},
  ContatosEnt in '..\..\..\..\..\MDL\ENT\v02_01\ContatosEnt.pas' {FmContatosEnt},
  Cursos in '..\Units\Cursos.pas' {FmCursos},
  Salas in '..\Units\Salas.pas' {FmSalas},
  Periodos in '..\Units\Periodos.pas' {FmPeriodos},
  UnitAcademy in '..\Units\UnitAcademy.pas',
  EntiCfgRel in '..\..\..\..\..\MDL\ENT\v02_01\EntiCfgRel.pas' {FmEntiCfgRel},
  dmkExcel in '..\..\..\..\..\UTL\_UNT\v01_01\dmkExcel.pas',
  SelStr in '..\..\..\..\..\UTL\_FRM\v01_01\SelStr.pas' {FmSelStr},
  UnDmkEnums in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnMyLinguas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  EntiImporCfg in '..\..\..\..\..\MDL\ENT\v02_01\EntiImporCfg.pas' {FmEntiImporCfg},
  FilialUsrs in '..\..\..\..\..\MDL\EMP\v01_01\FilialUsrs.pas' {FmFilialUsrs},
  OrderFieldsImp in '..\..\..\..\..\UTL\_FRM\v01_01\OrderFieldsImp.pas' {FmOrderFieldsImp},
  MailCfg in '..\..\..\..\..\MDL\MAIL\v01_01\MailCfg.pas' {FmMailCfg},
  UnMailEnv in '..\..\..\..\..\MDL\MAIL\v01_01\Units\VCL\UnMailEnv.pas',
  MailEnv in '..\..\..\..\..\MDL\MAIL\v01_01\MailEnv.pas' {FmMailEnv},
  MailEnvEmail in '..\..\..\..\..\MDL\MAIL\v01_01\MailEnvEmail.pas' {FmMailEnvEmail},
  WLogin in '..\..\..\..\..\MDL\WEB\v01_01\WLogin.pas' {FmWLogin},
  WSuporte in '..\..\..\..\..\MDL\HELP\v01_01\Geral\WSuporte.pas' {FmWSuporte},
  WOrdSerIncRap in '..\..\..\..\..\MDL\HELP\v01_01\Geral\WOrdSerIncRap.pas' {FmWOrdSerIncRap},
  FTPUploads in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\FTPUploads.pas' {FmFTPUploads},
  ApplicationConfiguration in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\ApplicationConfiguration.pas',
  FTPSiteInfo in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\FTPSiteInfo.pas',
  UnFTP in '..\..\..\..\..\MDL\ARQ\v01_01\Geral\UnFTP.pas',
  WebBrowser in '..\..\..\..\..\MDL\WEB\v01_01\WebBrowser.pas' {FmWebBrowser},
  WBFuncs in '..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  Restaura2 in '..\..\..\..\..\UTL\MySQL\v01_01\Restaura2.pas' {FmRestaura2},
  Mail_Tabs in '..\..\..\..\..\MDL\MAIL\v01_01\Mail_Tabs.pas',
  ObtemFoto4 in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\ObtemFoto4.pas' {FmObtemFoto4},
  VFrames in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VFrames.pas',
  VSample in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\Common\VSample.pas',
  UnDmkImg in '..\..\..\..\..\UTL\Imagem\v01_01\UnDmkImg.pas',
  UnDmkABS_PF in '..\..\..\..\..\..\MultiOS\AllOS\RDBMs\ABS\UnDmkABS_PF.pas',
  UnEntities in '..\..\..\..\..\MDL\ENT\v02_01\UnEntities.pas',
  UCreateFin in '..\..\..\..\..\MDL\FIN\v01_01\Units\UCreateFin.pas',
  UnProjGroup_Vars in '..\..\..\..\..\..\MultiOS\Aplicativos\Academy\UnProjGroup_Vars.pas',
  UnBancos in '..\..\..\..\..\MDL\BCO\v01_01\Geral\UnBancos.pas',
  UnMail in '..\..\..\..\..\MDL\MAIL\v01_01\Units\VCL\UnMail.pas',
  UnTreeView in '..\..\..\..\..\UTL\TreeView\v01_01\UnTreeView.pas',
  UnMailAllOS in '..\..\..\..\..\MDL\MAIL\v01_01\Units\AllOS\UnMailAllOS.pas',
  Departamentos in '..\Units\Departamentos.pas' {FmDepartamentos},
  RetornoBematech in '..\Units\RetornoBematech.pas' {FmRetornoBematech},
  ModuloBemaNaoFiscal in '..\Units\ModuloBemaNaoFiscal.pas',
  IBGE_LoadTabs in '..\..\..\..\..\MDL\ENT\v02_01\IBGE\IBGE_LoadTabs.pas' {FmIBGE_LoadTabs},
  UnLock_MLA in '..\..\..\..\DMK\UnLock\v01_01\UnLock_MLA.pas' {FmUnLock_MLA},
  UnALL_Jan in '..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Jan.pas',
  AtivaCod in '..\..\..\..\..\UTL\_FRM\v01_01\AtivaCod.pas' {FmAtivaCod},
  UnEmpresas_Tabs in '..\..\..\..\..\MDL\EMP\v01_01\UnEmpresas_Tabs.pas',
  UnALL_Tabs in '..\..\..\..\..\UTL\_UNT\v01_01\UnALL_Tabs.pas',
  UnEnti_Tabs in '..\..\..\..\..\MDL\ENT\v02_01\UnEnti_Tabs.pas',
  UnFinance_Tabs in '..\..\..\..\..\MDL\FIN\v01_01\Units\UnFinance_Tabs.pas',
  UnTextos_Tabs in '..\..\..\..\..\MDL\TEXT\v01_01\UnTextos_Tabs.pas',
  VerificaConexoes in '..\..\..\..\..\UTL\_FRM\v01_01\VerificaConexoes.pas' {FmVerificaConexoes},
  UnDmkListas in '..\..\..\..\..\..\MultiOS\AllOS\Listas\UnDmkListas.pas',
  ReciboMany in '..\..\..\..\..\UTL\_FRM\v01_01\ReciboMany.pas' {FmReciboMany},
  HorVerao in '..\..\..\..\..\UTL\_FRM\v01_01\HorVerao.pas' {FmHorVerao},
  UnFinanceiroJan in '..\..\..\..\..\MDL\FIN\v01_01\Units\UnFinanceiroJan.pas',
  UnPagtos in '..\..\..\..\..\MDL\FIN\v01_01\Units\UnPagtos.pas',
  CartTalCH in '..\..\..\..\..\MDL\FIN\v01_01\Forms\CartTalCH.pas' {FmCartTalCH},
  ModuleLct2 in '..\..\..\..\..\MDL\FIN\v01_01\DataModules\ModuleLct2.pas' {DmLct2: TDataModule},
  EmpresaSel in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\EmpresaSel.pas' {FmEmpresaSel},
  LctGer2 in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\Gerencia\LctGer2.pas' {FmLctGer2},
  ContasConfEmis in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasConfEmis.pas' {FmContasConfEmis},
  ContasMesGer in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasMesGer.pas' {FmContasMesGer},
  FluxCxaDia2 in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\FluxCxaDia2.pas' {FmFluxCxaDia2},
  MudaTexto in '..\..\..\..\..\UTL\_FRM\v01_01\MudaTexto.pas' {FmMudaTexto},
  Extratos2 in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Extratos2.pas' {FmExtratos2},
  RelChAbertos in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\RelChAbertos.pas' {FmRelChAbertos},
  ContasAgr in '..\..\..\..\..\MDL\FIN\v01_01\Planos\ContasAgr.pas' {FmContasAgr},
  PlanRelCab in '..\..\..\..\..\MDL\FIN\v01_01\Planos\PlanRelCab.pas' {FmPlanRelCab},
  APagRec in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\APagRec.pas' {FmAPagRec},
  CreateGeral in '..\..\..\..\..\UTL\MySQL\v01_01\CreateGeral.pas',
  _Empresas in '..\..\..\..\..\UTL\_FRM\v01_01\DockForms\_Empresas.pas' {Fm_Empresas},
  _Indi_Pags in '..\..\..\..\..\UTL\_FRM\v01_01\DockForms\_Indi_Pags.pas' {Fm_Indi_Pags},
  _Pla_ctas in '..\..\..\..\..\UTL\_FRM\v01_01\DockForms\_Pla_ctas.pas' {Fm_Pla_Ctas},
  MovFin in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\MovFin.pas' {FmMovFin},
  PesqContaCtrl in '..\..\..\..\..\UTL\_FRM\v01_01\PesqContaCtrl.pas' {FmPesqContaCtrl},
  CtaGruImp in '..\..\..\..\..\UTL\_FRM\v01_01\CtaGruImp.pas' {FmCtaGruImp},
  CtaGruCta in '..\..\..\..\..\UTL\_FRM\v01_01\CtaGruCta.pas' {FmCtaGruCta},
  ReceDesp in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\ReceDesp.pas' {FmReceDesp},
  ReceDesp2 in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\ReceDesp2.pas' {FmReceDesp2},
  Formularios in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Formularios.pas' {FmFormularios},
  CtaCfgCab in '..\..\..\..\..\MDL\FIN\v01_01\Planos\CtaCfgCab.pas' {FmCtaCfgCab},
  CtaCfgIts in '..\..\..\..\..\MDL\FIN\v01_01\Planos\CtaCfgIts.pas' {FmCtaCfgIts},
  Pesquisas2 in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Pesquisas2.pas' {FmPesquisas2},
  ReceDespFree in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\ReceDespFree.pas' {FmReceDespFree},
  CartNiv2 in '..\..\..\..\..\MDL\FIN\v01_01\Forms\CartNiv2.pas' {FmCartNiv2},
  PlanoImpExp in '..\..\..\..\..\MDL\FIN\v01_01\Planos\PlanoImpExp.pas' {FmPlanoImpExp},
  LctDelLanctoz in '..\..\..\..\..\MDL\FIN\v01_01\Forms\LctDelLanctoz.pas' {FmLctDelLanctoz},
  FinanceiroImp in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\FinanceiroImp.pas' {FmFinanceiroImp},
  LctPgVarios in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\LctPgVarios.pas' {FmLctPgVarios},
  CartPgto in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\CartPgto.pas' {FmCartPgto},
  CartPgtoEdit in '..\..\..\..\..\MDL\FIN\v01_01\Lcts\InsEdit\CartPgtoEdit.pas' {FmCartPgtoEdit},
  PrincipalImp in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\PrincipalImp.pas' {FmPrincipalImp},
  Pesquisas in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Pesquisas.pas' {FmPesquisas},
  Extratos in '..\..\..\..\..\MDL\FIN\v01_01\Relatorios\Extratos.pas' {FmExtratos},
  ParamsEmp in '..\..\..\..\..\MDL\EMP\v01_01\ParamsEmp.pas' {FmParamsEmp},
  UnWAceites in '..\..\..\..\..\MDL\TEXT\v01_01\Geral\UnWAceites.pas',
  WAceite in '..\..\..\..\..\MDL\TEXT\v01_01\Geral\WAceite.pas' {FmWAceite},
  dmkValUsu in '..\..\..\..\..\..\dmkComp\dmkValUsu.pas',
  UnGrl_Consts in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UMySQLDB in '..\..\..\..\..\UTL\MySQL\v01_01\UMySQLDB.pas',
  UnDmkHTML2 in '..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  UnTextos_Jan in '..\..\..\..\..\MDL\TEXT\v01_01\UnTextos_Jan.pas',
  TextosHTML in '..\..\..\..\..\UTL\Textos\v01_01\TextosHTML.pas' {FmTextosHTML},
  TextosHTMLTab in '..\..\..\..\..\UTL\Textos\v01_01\TextosHTMLTab.pas' {FmTextosHTMLTab},
  UnFixBugs in '..\..\..\..\..\UTL\FixBugs\v01_01\UnFixBugs.pas',
  Enti9Digit in '..\..\..\..\..\MDL\ENT\v02_01\Enti9Digit.pas' {FmEnti9Digit},
  MyTreeViewReorder in '..\..\..\..\..\UTL\TreeView\v01_01\MyTreeViewReorder.pas' {FmMyTreeViewReorder},
  UnAppPF in '..\Units\UnAppPF.pas',
  FixBugs in '..\..\..\..\..\UTL\FixBugs\v01_01\FixBugs.pas' {FmFixBugs},
  UnGrl_Vars in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  GerlShowGrid in '..\..\..\..\..\UTL\_FRM\v01_01\GerlShowGrid.pas' {FmGerlShowGrid},
  EntiAssina in '..\..\..\..\..\MDL\ENT\v02_01\EntiAssina.pas' {FmEntiAssina},
  VerifiDBi in '..\..\..\..\..\MDL\WEB\v01_01\VerifiDBi.pas' {FmVerifiDBi},
  UnDmkWeb_Jan in '..\..\..\..\..\MDL\WEB\v01_01\UnDmkWeb_Jan.pas',
  WSincro2 in '..\..\..\..\..\MDL\WEB\v01_01\WSincro2.pas' {FmWSincro2},
  WOpcoes2 in '..\..\..\..\..\MDL\WEB\v02_01\WOpcoes2.pas' {FmWOpcoes},
  PreEmailMsg in '..\..\..\..\..\MDL\MAIL\v01_01\PreEmailMsg.pas' {FmPreEmailMsg},
  PreEmail in '..\..\..\..\..\MDL\MAIL\v01_01\PreEmail.pas' {FmPreEmail},
  UnitDmkTags in '..\..\..\..\..\UTL\_UNT\v01_01\UnitDmkTags.pas',
  UnMeetings_Tabs in '..\Units\MDL\MEET\v01_01\UnMeetings_Tabs.pas',
  LogOff in '..\..\..\..\..\UTL\_FRM\v01_01\LogOff.pas' {FmLogOff},
  Entidade3 in '..\..\..\..\..\MDL\ENT\v03_01\Entidade3.pas' {FmEntidade3},
  EntiGrupos in '..\..\..\..\..\MDL\ENT\v02_01\EntiGrupos.pas' {FmEntiGrupos},
  EntiGruIts in '..\..\..\..\..\MDL\ENT\v02_01\EntiGruIts.pas' {FmEntiGruIts},
  UnReordena in '..\..\..\..\..\UTL\_UNT\v01_01\UnReordena.pas',
  UnComps_Vars in '..\..\..\..\..\..\dmkComp\_Units\UnComps_Vars.pas',
  UnGrl_Geral in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnDmkWeb2 in '..\..\..\..\..\MDL\WEB\v02_01\UnDmkWeb2.pas',
  UnGrlUsuarios in '..\..\..\..\..\..\MultiOS\MDL\USR\v02_01\UnGrlUsuarios.pas',
  UnGrl_DmkDB in '..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_DmkDB.pas',
  UnGrl_REST in '..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_REST.pas',
  OAuth in '..\..\..\..\..\UTL\WEB\HTML\v01_01\OAuth.pas' {FmOAuth},
  UnGrlEnt in '..\..\..\..\..\..\MultiOS\MDL\ENT\UnGrlEnt.pas',
  Web_Tabs in '..\..\..\..\..\MDL\WEB\v02_01\Web_Tabs.pas';

{$R *.RES}

(*
  ObtemFoto3 in '..\..\..\..\..\UTL\Imagem\v01_01\Camera\ObtemFoto3.pas' {FmObtemFoto3},
  ImgMarquee in '..\..\..\..\Outros\Imagem\ImgMarquee.pas',
  FlipReverseRotateLibrary in '..\..\..\..\Outros\Imagem\FlipReverseRotateLibrary.pas',
*)

const
  NomeForm = 'Academy';
  MaxVerif = 5;
var
  Form : PChar;
  i: Integer;
begin
  Form := PChar(NomeForm);
  i := 0;
  while i < MaxVerif do
  begin
    inc(i, 1);
    if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
    begin
      i := MaxVerif;
      CreateMutex(nil, False, Form);
      Application.Initialize;
      Application.Title := 'Academy';
  Application.HelpFile := 'C:\Executaveis\Academy\Academy.chm';
  try
        Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmLinkRankSkin, FmLinkRankSkin);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TDmLct2, DmLct2);
  Application.CreateForm(TDmBco, DmBco);
  Application.CreateForm(TFmAcademy_MLA, FmAcademy_MLA);
  except
        Application.Terminate;
        Exit;
      end;
      Application.Run;
    end else begin
      if i = MaxVerif then
      begin
        Application.MessageBox('O Aplicativo j� est� em uso.', 'Erro',
          MB_OK+MB_ICONERROR);
        SetForeGroundWindow(FindWindow('TFmAcademy_MLA', Form));
      end else begin
        // esperar caso o aplicativo de vers�o mais antiga esteja sendo encerrado
        Sleep(1000);
      end;
    end;
  end;
end.
