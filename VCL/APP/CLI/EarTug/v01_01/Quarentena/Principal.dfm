object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 640
  ClientWidth = 1113
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1113
    Height = 113
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 5
      Top = 5
      Width = 104
      Height = 13
      Caption = 'IP Address to Monitor'
    end
    object LabelAdmin: TLabel
      Left = 302
      Top = 60
      Width = 310
      Height = 13
      Caption = 'Program does not have Administrator Rights, no socket monitoring'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LabelTraffic: TLabel
      Left = 302
      Top = 78
      Width = 33
      Height = 13
      Caption = 'Traffic:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 115
      Top = 5
      Width = 156
      Height = 13
      Caption = 'Adapter to Monitor (xPCap only)'
    end
    object Label1: TLabel
      Left = 633
      Top = 5
      Width = 108
      Height = 26
      Caption = 'Ignore Traffic to/from IPs:'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 760
      Top = 40
      Width = 114
      Height = 13
      Caption = 'Intervalo em segundos:'
    end
    object MonIpList: TListBox
      Left = 5
      Top = 20
      Width = 101
      Height = 90
      ItemHeight = 13
      TabOrder = 0
    end
    object UseWinPCap: TCheckBox
      Left = 300
      Top = 40
      Width = 121
      Height = 17
      Caption = 'Use xCap Driver'
      Enabled = False
      TabOrder = 1
    end
    object doMonitor: TButton
      Left = 302
      Top = 9
      Width = 91
      Height = 25
      Caption = 'Iniciar Monitor'
      TabOrder = 2
      OnClick = doMonitorClick
    end
    object EdExecutavel: TEdit
      Left = 756
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 3
      Text = 'ua-client.exe'
    end
    object AdapterList: TListBox
      Left = 115
      Top = 20
      Width = 181
      Height = 90
      ItemHeight = 13
      TabOrder = 4
      OnClick = AdapterListClick
    end
    object IpMask: TEdit
      Left = 922
      Top = 10
      Width = 97
      Height = 21
      TabOrder = 5
      Text = '255.255.255.0'
    end
    object IgnoreLAN: TCheckBox
      Left = 754
      Top = 10
      Width = 162
      Height = 17
      Caption = 'Ignore LAN Traffic with Mask: '
      TabOrder = 6
    end
    object IgnoreNonIp: TCheckBox
      Left = 510
      Top = 40
      Width = 121
      Height = 17
      Caption = 'Ignore Non-IP Traffic'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 7
    end
    object Promiscuous: TCheckBox
      Left = 425
      Top = 40
      Width = 81
      Height = 17
      Caption = 'Promiscuous'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 8
    end
    object IgnoreIPs: TMemo
      Left = 635
      Top = 32
      Width = 111
      Height = 78
      Lines.Strings = (
        '192.168.1.4'
        '192.168.1.255')
      ScrollBars = ssVertical
      TabOrder = 9
    end
    object EdIntervSum: TdmkEdit
      Left = 920
      Top = 36
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
      OnRedefinido = EdIntervSumRedefinido
    end
    object BtKill: TButton
      Left = 916
      Top = 84
      Width = 75
      Height = 25
      Caption = 'BtKill'
      TabOrder = 11
      OnClick = BtKillClick
    end
  end
  object LogDestinations: TMemo
    Left = 0
    Top = 113
    Width = 1113
    Height = 145
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
    WordWrap = False
  end
  object Memo1: TMemo
    Left = 0
    Top = 258
    Width = 1113
    Height = 186
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 2
    WordWrap = False
  end
  object AutoDisplay: TCheckBox
    Left = 510
    Top = 15
    Width = 121
    Height = 17
    Caption = 'Auto Update Display'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object ListBox1: TListBox
    Left = 0
    Top = 444
    Width = 1113
    Height = 196
    Align = alBottom
    ItemHeight = 13
    TabOrder = 4
    OnClick = AdapterListClick
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 55
    Top = 285
  end
end
