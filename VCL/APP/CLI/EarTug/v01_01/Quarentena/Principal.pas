unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Winapi.WinSock, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  OverbyteIcsWsocket,
  MagentaMonsock, MagentaMonpcap, MagentaPacket32, MagentaPcap,
  MagentaPackhdrs, Magsubs1, MagClasses, dmkEdit;

type
  TForm2 = class(TForm)
{ Ini - Usar Npcap (DLL)}
    AdapterList: TListBox;
    IpMask: TEdit;
    IgnoreLAN: TCheckBox;
    IgnoreNonIp: TCheckBox;
    Promiscuous: TCheckBox;
    IgnoreIPs: TMemo;
    Label1: TLabel;
{ Fim - Usar Npcap (DLL)}
    Panel1: TPanel;
    Label3: TLabel;
    MonIpList: TListBox;
    LabelAdmin: TLabel;
    LabelTraffic: TLabel;
    UseWinPCap: TCheckBox;
    doMonitor: TButton;
    LogDestinations: TMemo;
    Memo1: TMemo;
    Timer: TTimer;
    AutoDisplay: TCheckBox;
    EdExecutavel: TEdit;
    Label4: TLabel;
    ListBox1: TListBox;
    EdIntervSum: TdmkEdit;
    Label2: TLabel;
    BtKill: TButton;
    procedure FormCreate(Sender: TObject);
    procedure doMonitorClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    //
    procedure doReportClick(Sender: TObject);
    procedure AdapterListClick(Sender: TObject);
    procedure EdIntervSumRedefinido(Sender: TObject);
    procedure BtKillClick(Sender: TObject);
  private
    { Private declarations }
    FMeuIP: String;
    //
    procedure PacketEvent(Sender: TObject; PacketInfo: TPacketInfo) ;
    function  IPAddrFromName(HostName: string): string;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

var
{ Ini - Usar Npcap (DLL)}
  MonitorPcap: TMonitorPcap ;
  AdapterIPList: TStringList ;
  AdapterMaskList: TStringList ;
  AdapterBcastList: TStringList ;
{ Fim - Usar Npcap (DLL)}
  MonitorSocket: TMonitorSocket ;
  MonLive: boolean ;
  UpdateTrafficCounter: integer ;
  TrafficClass: TTrafficClass ;


implementation

uses
// DoDmK
  dmkGeral,
// FimDoDmK
  IdStack,
  UnMyVclEvents;


{$R *.dfm}

procedure TForm2.AdapterListClick(Sender: TObject);
var
    I: integer ;
begin
    // Marco
    MonIpList.Items.Clear;
    // Fim Marco
    I := MonitorPcap.GetIPAddresses (AnsiString (MonitorPcap.AdapterNameList [AdapterList.ItemIndex]),
                                        AdapterIPList, AdapterMaskList, AdapterBcastList) ;   // 9 Aug 2010?
    if I = 0 then exit ;
    MonIpList.Items.Assign (AdapterIPList) ;
    if MonIpList.Items.Count > 0 then MonIpList.ItemIndex := 0 ;
    IpMask.Text := AdapterMaskList [0] ;
end;

procedure TForm2.BtKillClick(Sender: TObject);
  function KillProcess (aProcessId : Cardinal) : boolean;
  var
    hProcess : integer;
  begin
    hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
    Result:= False;
    //
    if (hProcess <>0 ) then
    begin
      Result:= TerminateProcess(hProcess, 0);
      exit;
    end;
  end;
  //
begin
  if EdExecutavel.Text <> '' then
  begin
    if not KillProcess(MyVclEvents.FindProcessesByName(EdExecutavel.Text)) then
      ShowMessage('Processo n�o finalizado.')
    else
      ShowMessage('Processo finalizado com sucesso!.');
  end;
end;

procedure TForm2.doMonitorClick(Sender: TObject);
var
    I: integer ;
begin
//Marco
  //Memo1.Lines.Clear;
// Fim Marco
    if MonIpList.ItemIndex < 0 then exit ;
    if MonLive then
    begin
        MonLive := false ;
        LogDestinations.Lines.Add ('Capture Stopped' + CRLF) ;
{ Ini - Usar Npcap (DLL)}
        if UseWinPCap.Checked then
            MonitorPcap.StopMonitor
        else
{ Fim - Usar Npcap (DLL)}
            MonitorSocket.StopMonitor ;
        doMonitor.Caption := 'Start Monitor' ;
    end
    else
    begin
        TrafficClass.Clear ;
        try
{ Ini - Usar Npcap (DLL)}
            if UseWinPCap.Checked then
            begin
                MonitorPcap.MonAdapter := AnsiString (MonitorPcap.AdapterNameList [AdapterList.ItemIndex]);  // 9 Aug 2010
                I := MonitorPcap.GetIPAddresses (MonitorPcap.MonAdapter, AdapterIPList,
                                                    AdapterMaskList, AdapterBcastList) ;
                if I > 0 then
                begin
                    MonitorPcap.Addr := AdapterIPList [0] ;
                    MonitorPcap.AddrMask := AdapterMaskList [0] ;
                end
                else
                begin
                    MonitorPcap.Addr := MonIpList.Items [MonIpList.ItemIndex] ;
                    MonitorPcap.AddrMask := IpMask.Text ;
                end ;
                MonitorPcap.IgnoreData := true ;  // we never want data
                MonitorPcap.IgnoreLAN := IgnoreLAN.Checked ;
                MonitorPcap.IgnoreNonIP := IgnoreNonIP.Checked ;
                MonitorPcap.Promiscuous := Promiscuous.Checked ;
                MonitorPcap.ClearIgnoreIP ;
                if IgnoreIPs.Lines.Count <> 0 then
                begin
                    for I := 0 to Pred (IgnoreIPs.Lines.Count) do
                            MonitorPcap.SetIgnoreIP (IgnoreIPs.Lines [I]) ;
                end ;
                MonitorPcap.StartMonitor ;
                if NOT MonitorPcap.Connected then
                begin
                    LogDestinations.Lines.Add (MonitorPcap.LastError) ;
                    exit ;
                end ;
                LogDestinations.Lines.Add ('Capture Started - ' + AdapterList.Items
                         [AdapterList.ItemIndex] + ' on ' + MonitorPcap.Addr) ;
            end
            else
{ FIm - Usar Npcap (DLL)}
            begin
                MonitorSocket.Addr := MonIpList.Items [MonIpList.ItemIndex] ;
{ Ini - Usar Npcap (DLL)}
                MonitorSocket.AddrMask := IpMask.Text ;
{ Fim - Usar Npcap (DLL)}
                MonitorSocket.IgnoreData := true ;  // we never want data
{ Ini - Usar Npcap (DLL)}
                MonitorSocket.IgnoreLAN := IgnoreLAN.Checked ;
{ Fim - Usar Npcap (DLL)}
                MonitorSocket.ClearIgnoreIP ;
{ Ini - Usar Npcap (DLL)}
                if IgnoreIPs.Lines.Count <> 0 then
                begin
                    for I := 0 to Pred (IgnoreIPs.Lines.Count) do
                            MonitorSocket.SetIgnoreIP (IgnoreIPs.Lines [I]) ;
                end ;
{ Fim - Usar Npcap (DLL)}
                MonitorSocket.StartMonitor ;
                LogDestinations.Lines.Add ('Capture Started - Raw Sockets on ' +
                                                        MonitorSocket.Addr) ;
            end ;
            MonLive := true ;
            doMonitor.Caption := 'Stop Monitor' ;
        //    LogDestinations.Lines.Add (CRLF + sHeaderLine + CRLF) ;
        except
            LogDestinations.Lines.Add ('Failed to Start Monitor - ' + GetExceptMess (ExceptObject)) ;
        end ;
    end ;
end;

procedure TForm2.doReportClick(Sender: TObject);
// Marco
function GetServNameEx (PackType, ServPort: word): string ;
begin
    if PackType = IPPROTO_TCP then
        result := Lowercase (GetServName (ServPort))
    else if PackType = IPPROTO_UDP then
        result := Lowercase (GetServName (ServPort))
    else if PackType = IPPROTO_ICMP then
        result := Lowercase (GetICMPType (ServPort))
    else
        result := GetEtherProtoName (PackType) ;
end ;

// Fim Marco
var
    I: integer ;
    S: string ;
// Marco
    TrafficRec: PTrafficInfo ;
    disploc, disprem: string ;
    //
    AllS: string;
    Achou: Boolean;
// Fim Marco
begin
{
    LogDestinations.Lines.Clear ;
    LogDestinations.Lines.Add (sTrafficHdr) ;
    if TrafficClass.TotTraffic = 0 then exit ;
    TrafficClass.UpdateService ;
    for I := 0 to Pred (TrafficClass.TotTraffic) do
    begin
        S := TrafficClass.GetFmtTrafStr (I) ;
        if S = '' then continue ;  // sanity check
        LogDestinations.Lines.Add (S) ;
    end ;
    LogDestinations.Lines.Add (CRLF + sServiceHdr) ;
    if TrafficClass.TotService = 0 then exit ;
    for I := 0 to Pred (TrafficClass.TotService) do
    begin
        S := TrafficClass.GetFmtServStr (I) ;
        if S = '' then continue ;  // sanity check
        LogDestinations.Lines.Add (S) ;
    end ;
}
    // Teste Marco:
    //LogDestinations.Lines.Clear ;
    //LogDestinations.Lines.Add (sTrafficHdr) ;
    if TrafficClass.TotTraffic = 0 then exit ;
    TrafficClass.UpdateService ;
    AllS := '';
    Achou := False;
    for I := 0 to Pred (TrafficClass.TotTraffic) do
    begin
        S := TrafficClass.GetFmtTrafStr (I) ;
        if S = '' then continue ;  // sanity check
        //LogDestinations.Lines.Add (S) ;
        // Marco
        //Memo1.Lines.Clear;
        if S <> '' then
          AllS := AllS + S + sLineBreak;
        //
        TrafficRec := TrafficClass.GetUnSortTraf(I);
        if not Assigned (TrafficRec) then exit ;  // sanity check
        with TrafficRec^ do
        begin
            disploc := HostLoc ;
            disprem := HostRem ;
            if disploc = '' then disploc := IPToStr (AddrLoc) ;
            if disprem = '' then disprem :=  IPToStr (AddrRem) ;
            if ServName = '' then ServName := GetServNameEx (PackType, ServPort) ;
            {
            Edit1.Text := IPAddrFromName('www30.bhan.com.br');
            if IPAddrFromName('www30.bhan.com.br') = disprem then
              Memo1.Lines.Add('www30.bhan.com.br > ' +
              Format (sTrafficMask, [disploc, disprem, ServName,
                IntToKbyte (BytesSent), '[' + IntToKbyte (PacksSent) + ']',
                    IntToKbyte (BytesRecv), '[' + IntToKbyte (PacksRecv) + ']',
                                        TimeToStr (FirstDT), TimeToStr (LastDT)  ])) ;
            }
            if ServPort = 491 then
            begin
              Achou := True;
              Memo1.Lines.Add(Format (sTrafficMask, [disploc, disprem, ServName,
                IntToKbyte (BytesSent), '[' + IntToKbyte (PacksSent) + ']',
                    IntToKbyte (BytesRecv), '[' + IntToKbyte (PacksRecv) + ']',
                                        TimeToStr (FirstDT), TimeToStr (LastDT)  ]));
              LogDestinations.Text := AllS;
              doMonitorClick(Self);
              doMonitorClick(Self);
            end;
           //
        end ;

    end ;
    if Achou then
    {
    LogDestinations.Lines.Add (CRLF + sServiceHdr) ;
    if TrafficClass.TotService = 0 then exit ;
    for I := 0 to Pred (TrafficClass.TotService) do
    begin
        S := TrafficClass.GetFmtServStr (I) ;
        if S = '' then continue ;  // sanity check
        LogDestinations.Lines.Add (S) ;
    end ;
    }

    // Fim teste Marco
end;

procedure TForm2.EdIntervSumRedefinido(Sender: TObject);
begin
  Timer.Enabled  := False;
  Timer.Interval := EdIntervSum.ValueVariant * 1000;
  Timer.Enabled  := True;
end;

procedure TForm2.FormCreate(Sender: TObject);
var
    I: integer ;
begin

// traffic records
    TrafficClass := TTrafficClass.Create (self) ;

// raw sockets monitoring
    MonitorSocket := TMonitorSocket.Create (self) ;
    MonitorSocket.onPacketEvent := PacketEvent ;
    MonIpList.Items := LocalIPList ;
    if MonIpList.Items.Count > 0 then MonIpList.ItemIndex := 0 ;
    FMeuIP := Geral.ObtemIP(1);
    for I := 0 to MonIpList.Items.Count - 1 do
    begin
      if FMeuIP = MonIpList.Items[I] then
      begin
        MonIpList.ItemIndex := I;
      end;
    end;
{ Ini - Usar Npcap (DLL)}
// winpcap monitoring, needs packet.dll and drivers installed
    if LoadPacketDll then
    begin
        MonitorPcap := TMonitorPcap.Create (self) ;
        MonitorPcap.onPacketEvent := PacketEvent ;
        LogDestinations.Lines.Add ('WinPCap/Npcap version: ' + Pcap_GetPacketVersion) ; // Nov 2018
        ListBox1.Items.Assign (MonitorPcap.AdapterNameList) ; // Marco
        AdapterList.Items.Assign (MonitorPcap.AdapterDescList) ;
        if AdapterList.Items.Count <> 0 then
        begin
            AdapterList.ItemIndex := 0 ;
            if AdapterList.Items.Count >= 2 then  // skip dialup adaptors
            begin
                for I := 0 to AdapterList.Items.Count - 2 do
                begin
                    if Pos ('dial', LowerCase (AdapterList.Items [I])) = 0 then break ;
                    AdapterList.ItemIndex := I + 1 ;
                end ;
            end;
            AdapterList.Enabled := true ;
            UseWinPCap.Enabled := true ;
            Promiscuous.Enabled := true ;
            IgnoreNonIp.Enabled := true ;
            AdapterIPList := TStringList.Create ;
            AdapterMaskList := TStringList.Create ;
            AdapterBcastList := TStringList.Create ;
        end ;
    end ;
{ Fim - Usar Npcap (DLL)}
    // Nov 2018 socket monitoring needs admin rights
    if IsProgAdmin then
        LabelAdmin.Caption := 'Program has Administrator Rights'
    else
    begin
        LabelAdmin.Caption := 'Program does not have Administrator Rights, no socket monitoring';
        UseWinPCap.Checked := true ;
    end;

    MonLive := false ;
end;

function TForm2.IPAddrFromName(HostName: string): string;
begin
  Result := '';
  TIdStack.IncUsage;
  try
    Result := GStack.ResolveHost(hostname);
  finally
    TIdStack.DecUsage;
  end;
end;


procedure TForm2.PacketEvent(Sender: TObject; PacketInfo: TPacketInfo);
begin
  if NOT MonLive then exit ;   // 8 Aug 2008 ignore data once Stopped
  TrafficClass.Add (PacketInfo) ;
end;

procedure TForm2.TimerTimer(Sender: TObject);
var
    TotalTraffic: TServiceInfo ;
begin
    if NOT MonLive then exit ;
{    if UseWinPCap.Checked then
    begin
        with MonitorPcap do
            LabelTraffic.Caption := 'Traffic: Sent ' + IntToKbyte (TotSendBytes) +
                            ', Received ' + IntToKbyte (TotRecvBytes) + CRLF +
                            'Packets Sent ' + IntToCStr (TotSendPackets) +
                            ', Received ' + IntToCStr (TotRecvPackets) ;
    end
    else
    begin
        with MonitorSocket do
            LabelTraffic.Caption := 'Traffic: Sent ' + IntToKbyte (TotSendBytes) +
                            ', Received ' + IntToKbyte (TotRecvBytes) + CRLF +
                            'Packets Sent ' + IntToCStr (TotSendPackets) +
                            ', Received ' + IntToCStr (TotRecvPackets) ;
    end ;   }

    TotalTraffic := TrafficClass.GetTotals ;
    with TotalTraffic do
            LabelTraffic.Caption := 'Traffic: Sent ' + IntToKbyte (BytesSent) +
                            ', Received ' + IntToKbyte (BytesRecv) + CRLF_ +
                            'Packets Sent ' + IntToCStr (PacksSent) +
                            ', Received ' + IntToCStr (PacksRecv) ;

    if NOT AutoDisplay.Checked then exit ;
    inc (UpdateTrafficCounter) ;
    //if UpdateTrafficCounter > 10 then  // update traffic list every 10 seconds
    begin
        UpdateTrafficCounter := 0 ;
        doReportClick (Self) ;
    end ;
end;

end.
