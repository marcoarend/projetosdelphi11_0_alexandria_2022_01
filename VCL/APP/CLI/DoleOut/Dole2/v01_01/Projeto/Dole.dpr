program Dole;

uses
  Vcl.Forms,
  Winapi.Windows,
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  dmkGeral in '..\..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  UnMyObjects in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnGrl_Vars in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkEnums in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkProcFunc in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  UnInternalConsts2 in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts2.pas',
  UnMyJSON in '..\..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnLicVendaApp_Dmk in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  UnGrl_Geral in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  Dmk_DoleOut in '..\..\..\ProjGroup\v01_01\Dmk_DoleOut.pas' {FmDmk_DoleOut},
  LicDados in '..\..\..\ProjGroup\v01_01\LicDados.pas' {FmLicDados},
  OpcoesApp in '..\..\..\ProjGroup\v01_01\OpcoesApp.pas' {FmOpcoesApp},
  ABOUT in '..\..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnMyVclEvents in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  Tarefa in '..\..\..\ProjGroup\v01_01\Tarefa.pas' {FmTarefa},
  Vcl.Themes,
  Vcl.Styles,
  UnProjGroup_Consts in '..\..\..\ProjGroup\v01_01\UnProjGroup_Consts.pas',
  MyListas in '..\..\..\ProjGroup\v01_01\MyListas.pas',
  UnProjGroup_PF in '..\..\..\ProjGroup\v01_01\UnProjGroup_PF.pas',
  UnProjGroup_Vars in '..\..\..\ProjGroup\v01_01\UnProjGroup_Vars.pas',
  UnPrjGruFiles_PF in '..\..\..\ProjGroup\v01_01\UnPrjGruFiles_PF.pas',
  dmkLed in '..\..\..\..\..\..\..\dmkComp\dmkLed.pas',
  AgendaTarefa in '..\..\..\..\..\..\UTL\_FRM\v01_01\AgendaTarefa.pas' {FmAgendaTarefa},
  magsubs1 in '..\..\..\..\..\..\MDL\Magenta\magsubs1.pas',
  Trafego in '..\..\..\Voyez\v01_01\Units\Trafego.pas' {FmTrafego},
  UnMyProcesses in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyProcesses.pas';

{$R *.res}

const
  NomeForm = 'Dole';
var
  Form : PChar;
begin
  Form := PChar(NomeForm);
  if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
  begin
    CreateMutex(nil, False, Form);
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    TStyleManager.TrySetStyle('TabletDark');
    Application.Title := 'Voyez';
    Application.Name  := CO_APPLICATION_NAME_DOLE; // N�o mudar!!! usa para definir diret�rio de dados!!!
    if CO_VERMCW > CO_VERMLA then
      CO_VERSAO := CO_VERMCW
    else
      CO_VERSAO := CO_VERMLA;
    Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
  end
  else
  begin
    Geral.MB_Aviso('O Aplicativo j� esta em uso.');
    SetForeGroundWindow(FindWindow('TFmBlueDerm_dmk', Form));
  end;

end.
