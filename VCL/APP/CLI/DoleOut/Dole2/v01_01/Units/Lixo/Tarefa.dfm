object FmTarefa: TFmTarefa
  Left = 0
  Top = 0
  Caption = 'Configura'#231#227'o de Tarefa'
  ClientHeight = 636
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 321
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 289
      Height = 13
      Caption = 'Nome da configura'#231#227'o:  exemplo: (Windows M'#233'dia Player'#174')'
    end
    object Label2: TLabel
      Left = 12
      Top = 88
      Width = 358
      Height = 13
      Caption = 
        'Nome + extens'#227'o do execut'#225'vel da tarefa alvo: (exemplo: wmplayer' +
        '.exe)'
    end
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 465
      Height = 13
      Caption = 
        'Caminho (pasta) do Execut'#225'vel da tarefa alvo: exemplo: C:\Progra' +
        'm Files\Windows Media Player'
    end
    object Label4: TLabel
      Left = 12
      Top = 128
      Width = 421
      Height = 13
      Caption = 
        'Par'#226'metros extras na execu'#231#227'o do aplicativo da tarefa alvo: (exe' +
        'mplo: -p teste -u 123)'
    end
    object Label5: TLabel
      Left = 12
      Top = 300
      Width = 523
      Height = 13
      Caption = 
        'Intervalo em segundos de an'#225'lise da atividade da tarefa alvo (in' +
        'tervalo de scaneamento do monitoramento):'
    end
    object LaSegWaitOnOSReinit: TLabel
      Left = 232
      Top = 274
      Width = 317
      Height = 13
      Caption = ' Tempo em segundos de espara ao reiniciar o sistema operacional:'
      Enabled = False
    end
    object EdNome: TdmkEdit
      Left = 12
      Top = 24
      Width = 750
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdExecutavel: TdmkEdit
      Left = 12
      Top = 104
      Width = 750
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPasta: TdmkEdit
      Left = 12
      Top = 64
      Width = 750
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object RGEstadoJanela: TRadioGroup
      Left = 12
      Top = 168
      Width = 750
      Height = 41
      Caption = ' Estado da janela principal da tarefa ao executar: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Padr'#227'o do app'
        'Normal'
        'Maximizada'
        'Minimizada')
      TabOrder = 4
    end
    object EdParametros: TdmkEdit
      Left = 12
      Top = 144
      Width = 750
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CkExeOnIni: TdmkCheckBox
      Left = 12
      Top = 272
      Width = 213
      Height = 17
      Caption = 'Executa tarefa ao iniciar o applicativo.  '
      Checked = True
      State = cbChecked
      TabOrder = 8
      OnClick = CkExeOnIniClick
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CkMonitSohProprioProces: TdmkCheckBox
      Left = 12
      Top = 212
      Width = 337
      Height = 17
      Caption = 'Monitorar somente a inst'#226'ncia (Tarefa) iniciada pelo Dole.'
      TabOrder = 5
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object EdSegWaitOnOSReinit: TdmkEdit
      Left = 560
      Top = 270
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '600'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 600
      ValWarn = False
    end
    object CkExecAsAdmin: TdmkCheckBox
      Left = 12
      Top = 232
      Width = 389
      Height = 17
      Caption = 'Executar tarefa como administrador. '
      Enabled = False
      TabOrder = 6
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CkLogInMemo: TdmkCheckBox
      Left = 12
      Top = 252
      Width = 389
      Height = 17
      Caption = 'Log de atividades na janela principal. '
      Checked = True
      State = cbChecked
      TabOrder = 7
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object EdSegWaitScanMonit: TdmkEdit
      Left = 560
      Top = 296
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '600'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 600
      ValWarn = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 321
    Width = 784
    Height = 315
    Align = alClient
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 259
      Width = 782
      Height = 55
      Align = alBottom
      TabOrder = 0
      object BtOK: TBitBtn
        Left = 8
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Salvar'
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Left = 420
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Desiste'
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BtItens: TBitBtn
        Left = 132
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Etapa'
        TabOrder = 2
        OnClick = BtItensClick
      end
      object Button4: TButton
        Left = 624
        Top = 4
        Width = 75
        Height = 25
        Caption = 'Existe?'
        TabOrder = 3
        OnClick = Button4Click
      end
      object Edit1: TEdit
        Left = 708
        Top = 28
        Width = 65
        Height = 21
        ReadOnly = True
        TabOrder = 4
      end
      object EdHProcesso: TdmkEdit
        Left = 708
        Top = 4
        Width = 64
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Kill: TButton
        Left = 624
        Top = 28
        Width = 75
        Height = 25
        Caption = 'Encerra'
        TabOrder = 6
        OnClick = KillClick
      end
      object BtTestar: TBitBtn
        Left = 256
        Top = 8
        Width = 120
        Height = 40
        Caption = 'Testar'
        TabOrder = 7
        OnClick = BtTestarClick
      end
    end
  end
  object PMItens: TPopupMenu
    Left = 184
    Top = 533
    object IncluiEtapaNova1: TMenuItem
      Caption = '&Inclui nova etapa'
      object IncluiClickDeMouse1: TMenuItem
        Caption = 'Click de &Mouse'
      end
      object IncluiComandoTabEnterEtc1: TMenuItem
        Caption = '&Comando de Teclas'
      end
      object IncluiDigitaoDeTexto1: TMenuItem
        Caption = '&Digita'#231#227'o de Texto'
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AlteraEtapaSelecionada1: TMenuItem
      Caption = '&Altera etapa selecionada'
    end
    object ExcluiEtapaSelecionada1: TMenuItem
      Caption = '&Exclui etapa selecionada'
    end
  end
  object PMTestar: TPopupMenu
    Left = 296
    Top = 537
    object odospassos1: TMenuItem
      Caption = '&Todos passos'
    end
    object Atopassoselecionado1: TMenuItem
      Caption = '&At'#233' o passo selecionado'
    end
  end
end
