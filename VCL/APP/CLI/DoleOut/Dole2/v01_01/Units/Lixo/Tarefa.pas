unit Tarefa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons, System.JSON, System.IOUtils,
  //Vcl.Menus,
  dmkEdit, UnDmkProcFunc,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, ShellApi, dmkCheckBox,
  Vcl.Menus;
  //Data.DB, Vcl.Menus, Vcl.Grids, Vcl.DBGrids;

type
  TFmTarefa = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Panel2: TPanel;
    Label2: TLabel;
    EdExecutavel: TdmkEdit;
    Label3: TLabel;
    EdPasta: TdmkEdit;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    BtItens: TBitBtn;
    PMItens: TPopupMenu;
    IncluiClickDeMouse1: TMenuItem;
    IncluiDigitaoDeTexto1: TMenuItem;
    IncluiComandoTabEnterEtc1: TMenuItem;
    N1: TMenuItem;
    AlteraEtapaSelecionada1: TMenuItem;
    ExcluiEtapaSelecionada1: TMenuItem;
    RGEstadoJanela: TRadioGroup;
    Label4: TLabel;
    EdParametros: TdmkEdit;
    IncluiEtapaNova1: TMenuItem;
    Button4: TButton;
    Edit1: TEdit;
    EdHProcesso: TdmkEdit;
    Kill: TButton;
    BtTestar: TBitBtn;
    PMTestar: TPopupMenu;
    odospassos1: TMenuItem;
    Atopassoselecionado1: TMenuItem;
    CkExeOnIni: TdmkCheckBox;
    CkMonitSohProprioProces: TdmkCheckBox;
    EdSegWaitOnOSReinit: TdmkEdit;
    Label5: TLabel;
    CkExecAsAdmin: TdmkCheckBox;
    CkLogInMemo: TdmkCheckBox;
    EdSegWaitScanMonit: TdmkEdit;
    LaSegWaitOnOSReinit: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
    procedure KillClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure CkExeOnIniClick(Sender: TObject);
  private
    { Private declarations }
    //procedure Tabela_Carrega();
  public
    { Public declarations }
    FDBArqName: String;
    FMaiorCodigo: Integer;
    //
    function  Salva(): Boolean;
  end;

var
  FmTarefa: TFmTarefa;

implementation

uses
  UnInternalConsts, UnMyJson, UnMyObjects, dmkGeral, UnDmkEnums,
  //UnAppEnums, UnAppMainPF,
  UnProjGroup_PF, UnMyVclEvents,
  Principal;

{$R *.dfm}

procedure TFmTarefa.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmTarefa.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmTarefa.BtOKClick(Sender: TObject);
begin
  if Salva() then
    Close;
end;

procedure TFmTarefa.BtTestarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTestar, BtTestar);
end;

procedure TFmTarefa.Button4Click(Sender: TObject);
var
  hProcess: Integer;
begin
  hProcess := EdHProcesso.ValueVariant;
  if hProcess <> 0 then
  begin
    if MyVclEvents.FindProcessesByHProcess(hProcess) then
      Edit1.Text := 'Sim'
    else
      Edit1.Text := 'N�o'
  end else
      Edit1.Text := '???'
end;

procedure TFmTarefa.CkExeOnIniClick(Sender: TObject);
begin
  EdSegWaitOnOSReinit.Enabled := CkExeOnIni.Checked;
  LaSegWaitOnOSReinit.Enabled := CkExeOnIni.Checked;
end;

procedure TFmTarefa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Geral.MB_Pergunta('Deseja salvar as altera��es?') = ID_YES then
    Salva();
end;

procedure TFmTarefa.FormCreate(Sender: TObject);
var
  //Dir: String;
  Nome, Pasta, Executavel, Parametros: String;
  MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni: Boolean;
  SegWaitOnOSReinit, SegWaitScanMonit: Integer;
  EstadoJanela: Integer;
begin
  FMaiorCodigo := 0;
(*
  AppMainPF.CriaEcarregaTabela(
  FmPrincipal.DCP_twofish1,
  MemTable,
  MemTableTpAcao,
  MemTableCodigo,
  MemTableOrdem,
  MemTableNome,
  MemTableSegundosA,
  MemTableSegundosD,
  MemTableMousePosX,
  MemTableTexto,
  MemTableMousePosY,
  MemTableMouseEvent,
  MemTableQtdClkSmlt,
  MemTableKeyCode,
  MemTableKeyEvent,
  MemTableShiftState,
  MemTableTipoTecla,
  MemTableTeclEsp,
  MemTableLetra,
  Nome, Pasta, Executavel, Parametros, MonitSohProprioProces,
  ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni,
  SegWaitOnOSReinit, SegWaitScanMonit,
  EstadoJanela, FMaiorCodigo);
*)
  //
  EdNome.ValueVariant       := Nome;
  EdPasta.ValueVariant      := Pasta;
  EdExecutavel.ValueVariant := Executavel;
  EdParametros.ValueVariant := Parametros;
  RGEstadoJanela.ItemIndex  := EstadoJanela;

  CkMonitSohProprioProces.Checked  := MonitSohProprioProces;
  CkExecAsAdmin.Checked            := ExecAsAdmin;
  CkLogInMemo.Checked              := LogInMemo;
  CkExeOnIni.Checked               := ExeOnIni;
  EdSegWaitOnOSReinit.ValueVariant := SegWaitOnOSReinit;
  EdSegWaitScanMonit.ValueVariant  := SegWaitScanMonit;
end;

procedure TFmTarefa.KillClick(Sender: TObject);
  function KillProcess (aProcessId : Cardinal) : boolean;
  var
    hProcess : integer;
  begin
    hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
    Result:= False;
    //
    if (hProcess <>0 ) then
    begin
      Result:= TerminateProcess(hProcess, 0);
      exit;
    end;
  end;
  //
begin
  if EdExecutavel.Text <> '' then
  begin
    if not KillProcess(MyVclEvents.FindProcessesByName(EdExecutavel.Text)) then
      ShowMessage('Processo n�o finalizado.')
    else
      ShowMessage('Processo finalizado com sucesso!.');
  end;
end;

function TFmTarefa.Salva(): Boolean;
var
  i: Integer;
  lJsonObj: TJSONObject;
  JSONColor: TJSONObject;
  //JSONArray : TJSONArray;
  Nome, Pasta, Executavel, Parametros: String;
  EstadoJanela: Integer;
  TxtCript: String;
  MonitSohProprioProces, ParaEtapaOnDeactive, ExecAsAdmin, LogInMemo, ExeOnIni: String;
  SegWaitOnOSReinit, SegWaitScanMonit: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    lJsonObj     := TJSONObject.Create;
    //
    Nome         := EdNome.Text;
    Pasta        := EdPasta.Text;
    Executavel   := EdExecutavel.Text;
    Parametros   := EdParametros.Text;
    //LinCom       := EdLinCom.Text;
    //Processo     := EdProcesso.Text;
    EstadoJanela := dmkPF.EstadoJanelaToHWND_CmdShow(RGEstadoJanela.ItemIndex);


    MonitSohProprioProces := Geral.BoolToNumStr(CkMonitSohProprioProces.Checked);
    ExecAsAdmin           := Geral.BoolToNumStr(CkExecAsAdmin.Checked);
    LogInMemo             := Geral.BoolToNumStr(CkLogInMemo.Checked);
    ExeOnIni              := Geral.BoolToNumStr(CkExeOnIni.Checked);
    SegWaitOnOSReinit     := EdSegWaitOnOSReinit.ValueVariant;
    SegWaitScanMonit      := EdSegWaitScanMonit.ValueVariant;

    lJsonObj.AddPair('Nome', Geral.JsonText(Nome));
    lJsonObj.AddPair('Pasta', Geral.JsonText(Pasta));
    lJsonObj.AddPair('Executavel', Geral.JsonText(Executavel));
    lJsonObj.AddPair('Parametros', Geral.JsonText(Parametros));
    lJsonObj.AddPair('HWNDShow', Geral.FF0(EstadoJanela));
    //lJsonObj.AddPair('Itens', Geral.FF0(MemTable.RecordCount));
    //
    lJsonObj.AddPair('MonitSohProprioProces', Geral.JsonText(MonitSohProprioProces));
    lJsonObj.AddPair('ParaEtapaOnDeactive',   Geral.JsonText(ParaEtapaOnDeactive  ));
    lJsonObj.AddPair('ExecAsAdmin',           Geral.JsonText(ExecAsAdmin          ));
    lJsonObj.AddPair('LogInMemo',             Geral.JsonText(LogInMemo            ));
    lJsonObj.AddPair('ExeOnIni',              Geral.JsonText(ExeOnIni             ));
    lJsonObj.AddPair('SegWaitOnOSReinit',     Geral.FF0(SegWaitOnOSReinit    ));
    lJsonObj.AddPair('SegWaitScanMonit',      Geral.FF0(SegWaitScanMonit     ));
    //
(*
    JSONArray := TJSONArray.Create();
    MemTable.First;
    while not MemTable.Eof do
    begin
      JSONColor := TJSONObject.Create();
      //
      JSONColor.AddPair('Codigo',      Geral.FF0(MemTableCodigo.Value));
      JSONColor.AddPair('Nome',        Geral.JsonText(MemTableNome.Value));
      JSONColor.AddPair('Ordem',       Geral.FF0(MemTableOrdem.Value));
      JSONColor.AddPair('SegundosA',   Geral.FFT_Dot(MemTableSegundosA.Value, 3, siPositivo));
      JSONColor.AddPair('SegundosD',   Geral.FFT_Dot(MemTableSegundosD.Value, 3, siPositivo));
      JSONColor.AddPair('TpAcao',      Geral.FF0(MemTableTpAcao.Value));
      JSONColor.AddPair('MousePosX',   Geral.FF0(MemTableMousePosX.Value));
      JSONColor.AddPair('MousePosY',   Geral.FF0(MemTableMousePosY.Value));
      JSONColor.AddPair('MouseEvent',  Geral.FF0(MemTableMouseEvent.Value));
      JSONColor.AddPair('QtdClkSmlt',  Geral.FF0(MemTableQtdClkSmlt.Value));
      JSONColor.AddPair('KeyCode',     Geral.FF0(MemTableKeyCode.Value));
      JSONColor.AddPair('KeyEvent',    Geral.FF0(MemTableKeyEvent.Value));
      JSONColor.AddPair('Texto',       Geral.JsonText(MemTableTexto.Value));
      JSONColor.AddPair('ShiftState',  Geral.FF0(MemTableShiftState.Value));
      JSONColor.AddPair('TipoTecla',   Geral.FF0(MemTableTipoTecla.Value));
      JSONColor.AddPair('TeclEsp',     Geral.FF0(MemTableTeclEsp.Value));
      JSONColor.AddPair('Letra',       Geral.JsonText(MemTableLetra.Value));

      JSONArray.Add(JSONColor);
      //
      MemTable.Next;
    end;
    lJsonObj.AddPair('TarefasIts', JSONArray);
*)
    //
    TxtCript := ProjGroup_PF.EnCryptJSON(lJsonObj.ToString, FmPrincipal.DCP_twofish1);
    TFile.WriteAllText(ProjGroup_PF.ObtemNomeArquivoTarefa(), TxtCript, TEncoding.ANSI);

    //btnsave.Caption := 'Load Data';
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
