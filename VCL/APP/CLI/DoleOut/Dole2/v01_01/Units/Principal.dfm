object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  Caption = 'Dole'
  ClientHeight = 341
  ClientWidth = 407
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 407
    Height = 341
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 152
      Width = 407
      Height = 53
      Align = alTop
      TabOrder = 0
      object BtExecuta: TButton
        Left = 8
        Top = 8
        Width = 65
        Height = 40
        Caption = 'Executa'
        Enabled = False
        TabOrder = 0
        OnClick = BtExecutaClick
      end
      object BtTarefa: TButton
        Left = 188
        Top = 8
        Width = 85
        Height = 40
        Caption = 'Configura'#231#227'o'
        TabOrder = 1
        OnClick = BtTarefaClick
      end
      object BtContinuar: TButton
        Left = 76
        Top = 8
        Width = 109
        Height = 40
        Caption = 'Continuar usando!'
        Enabled = False
        TabOrder = 2
        Visible = False
        OnClick = BtContinuarClick
      end
      object BtTrafego: TButton
        Left = 276
        Top = 8
        Width = 61
        Height = 40
        Caption = 'Tr'#225'fego'
        TabOrder = 3
        OnClick = BtTrafegoClick
      end
      object BtEncerra: TButton
        Left = 340
        Top = 8
        Width = 61
        Height = 40
        Caption = 'Encerrar'
        TabOrder = 4
        OnClick = BtEncerraClick
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 407
      Height = 152
      Align = alTop
      TabOrder = 1
      object Label7: TLabel
        Left = 316
        Top = 132
        Width = 82
        Height = 13
        Caption = 'v.20.12.25.2118'
      end
      object Panel3: TPanel
        AlignWithMargins = True
        Left = 87
        Top = 4
        Width = 233
        Height = 144
        Margins.Left = 86
        Margins.Right = 86
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object dmkLED1: TdmkLED
          Left = 12
          Top = 4
          Width = 100
          Height = 140
          BrightColor = 7948861
          DimColor = 4665132
          BackColor = 3615019
          Value = 1
          Spacing = 4
          Gap = 2
          SegWidth = 16
          SegShape = ssDoubleEdge
        end
        object dmkLED2: TdmkLED
          Left = 124
          Top = 4
          Width = 100
          Height = 140
          BrightColor = 7948861
          DimColor = 4665132
          BackColor = 3615019
          Value = 5
          Spacing = 4
          Gap = 2
          SegWidth = 16
          SegShape = ssDoubleEdge
        end
      end
    end
    object PnTrafego: TPanel
      Left = 0
      Top = 205
      Width = 407
      Height = 136
      Align = alClient
      TabOrder = 2
      Visible = False
      object Label8: TLabel
        Left = 240
        Top = 8
        Width = 83
        Height = 13
        Caption = 'Pacotes no ciclo: '
      end
      object LaLMCicloPacks: TLabel
        Left = 344
        Top = 8
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label10: TLabel
        Left = 240
        Top = 28
        Width = 72
        Height = 13
        Caption = 'Bytes no ciclo: '
      end
      object LaLMCicloBytes: TLabel
        Left = 344
        Top = 28
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label16: TLabel
        Left = 8
        Top = 8
        Width = 87
        Height = 13
        Caption = 'Hora atualiza'#231#227'o: '
      end
      object LaLMHoraAgora: TLabel
        Left = 112
        Top = 8
        Width = 38
        Height = 13
        Caption = '0:00:00'
      end
      object Label9: TLabel
        Left = 240
        Top = 48
        Width = 87
        Height = 13
        Caption = 'Total de pacotes: '
      end
      object Label11: TLabel
        Left = 240
        Top = 68
        Width = 76
        Height = 13
        Caption = 'Total de bytes: '
      end
      object LaLMUsoApPacks: TLabel
        Left = 344
        Top = 48
        Width = 6
        Height = 13
        Caption = '0'
      end
      object LaLMUsoAPBytes: TLabel
        Left = 344
        Top = 68
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label1: TLabel
        Left = 240
        Top = 88
        Width = 92
        Height = 13
        Caption = 'Pacotes start app: '
      end
      object Label2: TLabel
        Left = 240
        Top = 108
        Width = 81
        Height = 13
        Caption = 'Bytes: start app '
      end
      object LaUsoApIniBytes: TLabel
        Left = 344
        Top = 108
        Width = 6
        Height = 13
        Caption = '0'
      end
      object LaUsoApIniPacks: TLabel
        Left = 344
        Top = 88
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label5: TLabel
        Left = 8
        Top = 28
        Width = 86
        Height = 13
        Caption = 'Tempo em exec.: '
      end
      object LaLMTimeInExcec: TLabel
        Left = 112
        Top = 28
        Width = 38
        Height = 13
        Caption = '0:00:00'
      end
      object Label6: TLabel
        Left = 8
        Top = 108
        Width = 100
        Height = 13
        Caption = 'Tempo ocioso ciclos: '
      end
      object LaTempoOciosoCiclos: TLabel
        Left = 116
        Top = 108
        Width = 44
        Height = 13
        Caption = '00:00:00'
      end
      object Label3: TLabel
        Left = 8
        Top = 60
        Width = 94
        Height = 13
        Caption = 'Tempo ativo ciclos: '
      end
      object LaAtivoCiclos: TLabel
        Left = 116
        Top = 60
        Width = 44
        Height = 13
        Caption = '00:00:00'
      end
      object Label4: TLabel
        Left = 8
        Top = 84
        Width = 104
        Height = 13
        Caption = 'Tempo limbo no ciclo: '
      end
      object LaLimboCiclo: TLabel
        Left = 116
        Top = 84
        Width = 44
        Height = 13
        Caption = '00:00:00'
      end
      object LaLMCicloAtivo: TLabel
        Left = 172
        Top = 60
        Width = 44
        Height = 13
        Caption = '00:00:00'
      end
      object LaLMCicloOcios: TLabel
        Left = 172
        Top = 108
        Width = 44
        Height = 13
        Caption = '00:00:00'
      end
      object LaLMCicloLimbo: TLabel
        Left = 172
        Top = 84
        Width = 44
        Height = 13
        Caption = '00:00:00'
      end
    end
  end
  object TmReadCiclo: TTimer
    Enabled = False
    OnTimer = TmReadCicloTimer
    Left = 12
    Top = 12
  end
  object DCP_twofish1: TDCP_twofish
    Id = 6
    Algorithm = 'Twofish'
    MaxKeySize = 256
    BlockSize = 128
    Left = 180
    Top = 8
  end
  object TmEsgotou: TTimer
    Enabled = False
    OnTimer = TmEsgotouTimer
    Left = 12
    Top = 109
  end
  object TmInExec: TTimer
    Enabled = False
    OnTimer = TmInExecTimer
    Left = 12
    Top = 60
  end
end
