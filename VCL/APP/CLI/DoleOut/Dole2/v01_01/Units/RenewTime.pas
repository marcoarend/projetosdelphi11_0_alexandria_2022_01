unit RenewTime;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TFmRenewTime = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    Label1: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmRenewTime: TFmRenewTime;

implementation

uses Principal;

{$R *.dfm}

procedure TFmRenewTime.Button1Click(Sender: TObject);
begin
  FmRenewTime.Close;
end;

procedure TFmRenewTime.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Timer1.Enabled := False;
end;

procedure TFmRenewTime.FormShow(Sender: TObject);
begin
  FmRenewTime.BringToFront;
end;

procedure TFmRenewTime.Timer1Timer(Sender: TObject);
var
  tmpFormStyle: TFormStyle;
begin
  tmpFormStyle := Application.MainForm.FormStyle;
  Application.MainForm.FormStyle := fsStayOnTop;
  Application.Restore; // optional
  Application.BringToFront;
  Application.MainForm.FormStyle := tmpFormStyle;
end;

end.
