program Voyer;

uses
  Vcl.Forms,
  UnMyVclEvents in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  dmkGeral in '..\..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnMyObjects in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnGrl_Vars in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkEnums in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkProcFunc in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  Vcl.Themes,
  Vcl.Styles,
  UnMyJSON in '..\..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnAppEnums in '..\Units\UnAppEnums.pas',
  UnAppPF in '..\Units\UnAppPF.pas',
  UnLicVendaApp_Dmk in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  UnGrl_Geral in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnGrl_Consts in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  MyListas in '..\Units\MyListas.pas',
  LicDados in '..\Units\LicDados.pas' {FmLicDados},
  OpcoesApp in '..\Units\OpcoesApp.pas' {FmOpcoesApp},
  SntpSend in '..\..\..\SntpSend.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  UnAppMainPF in '..\Units\UnAppMainPF.pas',
  Dmk_Voyer in '..\Units\Dmk_Voyer.pas' {FmDmk_Voyer},
  UnApp_Consts in '..\Units\UnApp_Consts.pas',
  UnApp_Vars in '..\Units\UnApp_Vars.pas',
  ABOUT in '..\..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  UnitMD5 in '..\..\..\..\..\..\UTL\Encrypt\v01_01\UnitMD5.pas',
  UnProjGroup_PF in '..\..\..\..\..\..\..\..\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\DoleOut\ProjGroup\v01_01\UnProjGroup_PF.pas',
  UnProjGroup_Vars in '..\..\..\..\..\..\..\..\ProjetosDelphi10_2_2_Tokyo\VCL\APP\CLI\DoleOut\ProjGroup\v01_01\UnProjGroup_Vars.pas';

{$R *.res}

begin
  //UnMyVclEvents in '..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Luna');
  Application.Title := 'Voyer';
  Application.Name  := 'Voyer';
  if CO_VERMCW > CO_VERMLA then
    CO_VERSAO := CO_VERMCW
  else
    CO_VERSAO := CO_VERMLA;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
