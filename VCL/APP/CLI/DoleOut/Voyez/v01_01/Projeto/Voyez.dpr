program Voyez;

uses
  Vcl.Forms,
  UnMyVclEvents in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyVclEvents.pas',
  dmkGeral in '..\..\..\..\..\..\..\dmkComp\dmkGeral.pas',
  Principal in '..\Units\Principal.pas' {FmPrincipal},
  UnMyObjects in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnMyObjects.pas',
  UnGrl_Vars in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Vars.pas',
  UnMyLinguas in '..\..\..\..\..\..\..\MultiOS\AllOS\Listas\UnMyLinguas.pas',
  UnDmkEnums in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnDmkEnums.pas',
  UnDmkProcFunc in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnDmkProcFunc.pas',
  ZCF2 in '..\..\..\..\..\..\UTL\_UNT\v01_01\ZCF2.pas',
  ResIntStrings in '..\..\..\..\..\..\UTL\_UNT\v01_01\ResIntStrings.pas',
  UnInternalConsts in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnInternalConsts.pas',
  Vcl.Themes,
  Vcl.Styles,
  UnMyJSON in '..\..\..\..\..\..\UTL\JSON\UnMyJSON.pas',
  uLkJSON in '..\..\..\..\..\..\UTL\JSON\uLkJSON.pas',
  UnProjGroup_Vars in '..\..\..\ProjGroup\v01_01\UnProjGroup_Vars.pas',
  UnLicVendaApp_Dmk in '..\..\..\..\..\..\UTL\_UNT\v01_01\UnLicVendaApp_Dmk.pas',
  UnGrl_Geral in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Geral.pas',
  UnGrl_Consts in '..\..\..\..\..\..\..\MultiOS\UTL\_UNT\v01_01\UnGrl_Consts.pas',
  UnGrl_DmkREST in '..\..\..\..\..\..\..\MultiOS\UTL\WEB\HTML\v01_01\UnGrl_DmkREST.pas',
  UnDmkHTML2 in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\UnDmkHTML2.pas',
  WBFuncs in '..\..\..\..\..\..\UTL\WEB\HTML\v01_01\WBFuncs.pas',
  LicDados in '..\..\..\ProjGroup\v01_01\LicDados.pas' {FmLicDados},
  OpcoesApp in '..\..\..\ProjGroup\v01_01\OpcoesApp.pas' {FmOpcoesApp},
  SntpSend in '..\..\..\SntpSend.pas',
  Windows_FMX_ProcFunc in '..\..\..\..\..\..\..\FMX\UTL\_UNT\v01_01\Windows\Windows_FMX_ProcFunc.pas',
  Dmk_DoleOut in '..\..\..\ProjGroup\v01_01\Dmk_DoleOut.pas' {FmDmk_DoleOut},
  ABOUT in '..\..\..\..\..\..\UTL\_FRM\v01_01\ABOUT.pas' {FmAbout},
  Trafego in '..\Units\Trafego.pas' {FmTrafego},
  UnProjGroup_PF in '..\..\..\ProjGroup\v01_01\UnProjGroup_PF.pas',
  MyListas in '..\..\..\ProjGroup\v01_01\MyListas.pas',
  Tarefa in '..\..\..\ProjGroup\v01_01\Tarefa.pas' {FmTarefa},
  UnPrjGruFiles_PF in '..\..\..\ProjGroup\v01_01\UnPrjGruFiles_PF.pas',
  UnProjGroup_Consts in '..\..\..\ProjGroup\v01_01\UnProjGroup_Consts.pas',
  AgendaTarefa in '..\..\..\..\..\..\UTL\_FRM\v01_01\AgendaTarefa.pas' {FmAgendaTarefa};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('TabletDark');
  Application.Title := 'Voyez';
  Application.Name  := CO_APPLICATION_NAME_VOYEZ; // N�o mudar!!! usa para definir diret�rio de dados!!!
  if CO_VERMCW > CO_VERMLA then
    CO_VERSAO := CO_VERMCW
  else
    CO_VERSAO := CO_VERMLA;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
