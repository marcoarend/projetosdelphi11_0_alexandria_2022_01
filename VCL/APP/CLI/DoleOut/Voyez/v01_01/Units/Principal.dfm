object FmPrincipal: TFmPrincipal
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Voyez'
  ClientHeight = 462
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 392
      Top = 16
      Width = 98
      Height = 13
      Caption = 'Tempo de atividade:'
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 881
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Fecha'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtSaidaClick
    end
    object BtAtiva: TBitBtn
      Tag = 13
      Left = 5
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Ativa'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtAtivaClick
    end
    object BtCon: TBitBtn
      Tag = 13
      Left = 133
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Configura'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtConClick
    end
    object BtOpcoesApp: TBitBtn
      Tag = 13
      Left = 625
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'O&p'#231#245'es'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtOpcoesAppClick
    end
    object EdTempoAtividade: TEdit
      Left = 392
      Top = 32
      Width = 225
      Height = 21
      ReadOnly = True
      TabOrder = 4
    end
    object BtSobre: TBitBtn
      Tag = 13
      Left = 753
      Top = 5
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Sobre'
      NumGlyphs = 2
      TabOrder = 5
      OnClick = BtSobreClick
    end
    object BtTrafego: TBitBtn
      Tag = 13
      Left = 261
      Top = 4
      Width = 123
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Tr'#225'fego'
      NumGlyphs = 2
      TabOrder = 6
      OnClick = BtTrafegoClick
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 57
    Width = 1008
    Height = 405
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = ' A'#231#245'es tomadas '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object MeLog: TMemo
        Left = 0
        Top = 0
        Width = 1000
        Height = 377
        Align = alClient
        ReadOnly = True
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Etapas '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 47
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object BtVisualizaItens: TBitBtn
          Left = 8
          Top = 4
          Width = 185
          Height = 40
          Caption = 'Visualizar itens '
          TabOrder = 0
          OnClick = BtVisualizaItensClick
        end
        object Button2: TButton
          Left = 220
          Top = 20
          Width = 217
          Height = 25
          Caption = 'Crypt'
          TabOrder = 1
          Visible = False
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 448
          Top = 18
          Width = 185
          Height = 25
          Caption = 'Button3'
          TabOrder = 2
          Visible = False
          OnClick = Button3Click
        end
      end
    end
  end
  object DCP_twofish1: TDCP_twofish
    Id = 6
    Algorithm = 'Twofish'
    MaxKeySize = 256
    BlockSize = 128
    Left = 148
    Top = 308
  end
  object TmMonitora: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = TmMonitoraTimer
    Left = 360
    Top = 256
  end
  object TmLibera: TTimer
    Enabled = False
    OnTimer = TmLiberaTimer
    Left = 204
    Top = 225
  end
end
