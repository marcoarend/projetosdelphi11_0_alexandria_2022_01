unit Dmk_Voyez;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, dmkEdit,
  Vcl.ExtCtrls;

type
  TFmDmk_Voyez = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    EdAppUserPwd: TdmkEdit;
    Panel2: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTentativas: Integer;
  public
    { Public declarations }
    FResult: Boolean;
  end;

var
  FmDmk_Voyez: TFmDmk_Voyez;

implementation

uses DmkGeral, UnApp_Vars;

{$R *.dfm}

procedure TFmDmk_Voyez.BtDesisteClick(Sender: TObject);
begin
  Halt(0);
end;

procedure TFmDmk_Voyez.BtOKClick(Sender: TObject);
begin
  if AnsiUppercase(EdAppUserPwd.Text) = AnsiUppercase(VAR_SENHA_BOSS) then
  begin
    FResult := True;
    Close;
  end else
  begin
    Geral.MB_Aviso('Senha inv�lida!');
    FTentativas := FTentativas + 1;
    if FTentativas >= 3 then
      Halt(0);
  end;
end;

procedure TFmDmk_Voyez.FormCreate(Sender: TObject);
begin
  FResult     := False;
  FTentativas := 0;
end;

end.
