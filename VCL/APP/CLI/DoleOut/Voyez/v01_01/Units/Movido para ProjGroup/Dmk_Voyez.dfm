object FmDmk_Voyez: TFmDmk_Voyez
  Left = 0
  Top = 0
  Caption = 'Voyer'
  ClientHeight = 172
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 330
    Height = 115
    Align = alClient
    TabOrder = 0
    object Label4: TLabel
      Left = 24
      Top = 40
      Width = 174
      Height = 13
      Caption = 'Minha senha para entrar neste App:'
    end
    object EdAppUserPwd: TdmkEdit
      Left = 24
      Top = 56
      Width = 241
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 115
    Width = 330
    Height = 57
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Left = 8
      Top = 8
      Width = 120
      Height = 40
      Caption = 'OK'
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtDesiste: TBitBtn
      Left = 160
      Top = 8
      Width = 120
      Height = 40
      Caption = 'Desiste'
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
end
