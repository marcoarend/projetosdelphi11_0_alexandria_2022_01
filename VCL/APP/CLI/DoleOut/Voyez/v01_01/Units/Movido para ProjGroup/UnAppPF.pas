unit UnAppPF;

interface

uses Winapi.Windows, Winapi.Messages, Vcl.Forms, Winapi.ShellApi, Winapi.ShlObj,
  System.SysUtils, Vcl.Controls, UnInternalConsts,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, System.JSON, System.IOUtils,
  System.Classes, UnGrl_Vars, UnApp_Vars, Vcl.StdCtrls;
type
  TUnAppPF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  LiberaAppPelaSenha(AppUserPwd: String): Boolean;
    function  LiberaUsoVendaApp1(const DCP_twofish: TDCP_twofish(*; var
              App User Pwd, Exe On Ini: String*)): Boolean;
    function  ObtemDadosLicencaDeArquivo(const Arquivo: String; const DCP_twofish:
              TDCP_twofish; var AtualizadoEm, SerialHD, CPUID, SerialKey:
              String): Boolean;
    function  ObtemDadosOpcoesAppDeArquivo(const Arquivo: String; const DCP_twofish:
              TDCP_twofish): Boolean;
    function  DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
    function  EnCryptJSON(JSON: string; twofish: TDCP_twofish): string;
    //
    procedure EsperaEprocessaMensagensWindows(Segundos: Double);
    function  MouseBotaoEventToWindowsMessage(MouseBotaoEvent: Integer): Integer;
    function  WindowsMessageToMouseBotaoEvent(WindowsMessage: Integer): Integer;

    function  GetSpecialFolderPath(CSIDLFolder: Integer): string;

    function  ObtemNomeArquivoOpcoes(): String;
    function  ObtemNomeArquivoTarefa(): String;
    function  ObtemNomeArquivoLicenca(var Arquivo, Pasta: String): Boolean;

    procedure MostraFormOpcoes();
    procedure MostraFormAbout();

    function UpTime(): string;

    procedure InfoLog(Memo: TMemo; DataHora: TDateTime; Msg: String);

    ////////////////////////////// VOYEZ ///////////////////////////////////////
    ///
    procedure ExecutarNaInicializao();
  end;

var
  AppPF: TUnAppPF;


implementation

uses dmkGeral, UnLicVendaApp_Dmk, UnDmkProcFunc, MyListas, UnMyJSON,
  LicDados, Dmk_Voyez, OpcoesApp, UnMyVclEvents, About;

{ TUnAppPF }

const
  CO_CHAVE = 'SohJesusSalva';

function TUnAppPF.DeCryptJSON(Crypt: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr(CO_CHAVE, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.DecryptString(Crypt); // Descriptografa o JSON.
  twofish.Burn();
end;

function TUnAppPF.EnCryptJSON(JSON: string; twofish: TDCP_twofish): string;
begin
  twofish.InitStr(CO_CHAVE, TDCP_sha1); // inicializa o componente DCP_twofish com a chave.
  Result := twofish.EncryptString(JSON); // Criptografa o JSON.
  twofish.Burn();
end;

procedure TUnAppPF.EsperaEprocessaMensagensWindows(Segundos: Double);
begin
  Application.ProcessMessages;
  if Segundos >= 0.001 then
  begin
    Sleep(Trunc(Segundos * 1000));
    Application.ProcessMessages;
  end;
end;

procedure TUnAppPF.ExecutarNaInicializao;
// UnAppPF.GetSpecialFolderPath(CSIDL_STARTUP) =
//C:\Users\angeloni\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

//Make Vista launch UAC restricted programs at startup with Task Scheduler
//https://www.techrepublic.com/blog/windows-and-office/make-vista-launch-uac-restricted-programs-at-startup-with-task-scheduler/
  //ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
var
  AppPth, WinUsr, WinPwd: String;
begin
  //AppPth  := ExtractFilePath(Application.ExeName);
  AppPth  := Application.ExeName;
  WinUsr := 'angeloni';
  WinPwd := 'eletro26';
  dmkPF.ScheduleRunAtStartup(
    'Sniffer',
    (*'C:\Application.exe'*)AppPth,
    (*'System'*)WinUsr,
    WinPwd);
end;

function TUnAppPF.GetSpecialFolderPath(CSIDLFolder: Integer): string;
var
  FilePath: array [0..MAX_PATH] of char;
begin
  SHGetFolderPath(0, CSIDLFolder, 0, 0, FilePath);
  Result := FilePath;
end;

procedure TUnAppPF.InfoLog(Memo: TMemo; DataHora: TDateTime; Msg: String);
begin
  Memo.Lines.Add(Geral.FDT(DataHora, 0) + '  ' + Msg);
end;

function TUnAppPF.LiberaAppPelaSenha(AppUserPwd: String): Boolean;
begin
  Application.CreateForm(TFmDmk_Voyez, FmDmk_Voyez);
  FmDmk_Voyez.ShowModal;
  Result := FmDmk_Voyez.FResult;
  FmDmk_Voyez.Destroy;
end;

function TUnAppPF.LiberaUsoVendaApp1(const DCP_twofish: TDCP_twofish(*; var
  App User Pwd, Exe On Ini: String*)): Boolean;

  function MostraFormLicDados(var CPF_CNPJ, AtualizadoEm, SerialHD, CPUID,
  SerialKey(*, App User Pwd, Exe On Ini*): String): Boolean;
  begin
    Application.CreateForm(TFmLicDados, FmLicDados);
    FmLicDados.EdCPF_CNPJ.ValueVariant  := VAR_CPF_CNPJ;
    FmLicDados.EdPosicao.ValueVariant   := VAR_Posicao;
    FmLicDados.EdCodigoPIN.ValueVariant := VAR_CodigoPIN;
    FmLicDados.ShowModal;
    CPF_CNPJ     := VAR_CPF_CNPJ;
    AtualizadoEm := FmLicDados.FAtualizadoEm;
    SerialHD     := FmLicDados.FSerialHD;
    CPUID        := FmLicDados.FCPUID;
    SerialKey    := FmLicDados.FSerialKey;
    //App User Pwd   := FmLicDados.FApp User Pwd;
    //Exe On Ini     := FmLicDados.F Exe On Ini;
    //
    Result       := FmLicDados.FLiberado;
    //
    FmLicDados.Destroy;
  end;
  //
  function GeraESalvaJSON(Dir, DirEArquivo, CPF_CNPJ, AtualizadoEm, SerialHD,
  CPUID, SerialKey(*, App User Pwd, Exe On Ini*): String): Boolean;
  var
    i: Integer;
    lJsonObj: TJSONObject;
    JSONColor: TJSONObject;
    JSONArray : TJSONArray;
    Nome, Pasta, Executavel, Parametros: String;
    EstadoJanela: Integer;
    TxtCript: String;
  begin
    Result := False;
    Screen.Cursor := crHourGlass;
    try
      lJsonObj     := TJSONObject.Create;
      //
      lJsonObj.AddPair('CPF_CNPJ', Geral.JsonText(CPF_CNPJ));
      lJsonObj.AddPair('SerialHD', Geral.JsonText(SerialHD));
      lJsonObj.AddPair('CPUID', Geral.JsonText(CPUID));
      lJsonObj.AddPair('SerialKey', Geral.JsonText(SerialKey));
      lJsonObj.AddPair('AtualizadoEm', Geral.JsonText(AtualizadoEm));
      //lJsonObj.AddPair('App User Pwd', Geral.JsonText(AppUserPwd));
      //lJsonObj.AddPair('Exe On Ini', Geral.JsonText(ExeOnIni));
      //
      TxtCript := EnCryptJSON(lJsonObj.ToString, DCP_twofish);
      ForceDirectories(Dir);
      TFile.WriteAllText(DirEArquivo, TxtCript, TEncoding.ANSI);
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

//https://forums.embarcadero.com/thread.jspa?threadID=117722
var
  Arq_AtualizadoEm, Arq_SerialHD, Arq_CPUID, Arq_SerialKey,
  Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey,
  Arquivo, Pasta: String;
  Liberado: Boolean;
  Salvar: Boolean;
  Agora, UltAtz: TDateTime;
begin
  Liberado := False;
  Salvar   := False;
  ObtemNomeArquivoLicenca(Arquivo, Pasta);
  if not FileExists(Arquivo) then
  begin
    Liberado := MostraFormLicDados(VAR_CPF_CNPJ,
      Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
    Salvar := Liberado;
  end else
  begin
    // Abrir arquivo aqui!
    ObtemDadosLicencaDeArquivo(Arquivo, DCP_twofish, Arq_AtualizadoEm,
    Arq_SerialHD, Arq_CPUID, Arq_SerialKey (*App User Pwd, Exe On Ini,*) );

    Maq_SerialKey := DmkPF.CalculaValSerialKey_VendaApp(VAR_CPF_CNPJ, Maq_SerialHD, Maq_CPUID);
    if Arq_SerialKey <> Maq_SerialKey then
    begin
      //App User Pwd := '';
      //Exe On Ini   := '0';
      Liberado := MostraFormLicDados(VAR_CPF_CNPJ,
        Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
      Salvar := Liberado;
    end else
    begin
      //UltAtz := Geral.ValidaDataBR(Arq_AtualizadoEm, (*PermiteZero*) True,
      //(*ForceNextYear*)False, (*MostraMsg*)True);
      UltAtz := Geral.ValidaDataHoraSQL(Arq_AtualizadoEm);
      //
      if LicVendaApp_Dmk.ObtemDataHoraWeb(Agora, 2000) then
      begin
        if Agora - UltAtz > 7 then // mais de uma semana
        begin
          if LicVendaApp_Dmk.LiberaUso(VAR_CPF_CNPJ, VAR_CodigoPIN, CO_DMKID_APP, CO_VERSAO,
          Maq_AtualizadoEm, Maq_SerialHD, Maq_CPUID, Maq_SerialKey) then
          begin
            Liberado := True;
            Salvar   := True;
          end else
           Halt(0);
          Liberado := True;
        end else
          Liberado := True;
      end else
      begin
        // N�o tem internet?
        Liberado := True;
      end;
    end;
  end;
  if Salvar then
  begin
    //if Exe On Ini = '' then
      //Exe On Ini := '0';
    GeraESalvaJSON(Pasta, Arquivo, VAR_CPF_CNPJ, Maq_AtualizadoEm, Maq_SerialHD,
      Maq_CPUID, Maq_SerialKey(*, App User Pwd, Exe On Ini*));
  end;
  Result := Liberado;
end;

procedure TUnAppPF.MostraFormAbout();
begin
  Application.CreateForm(TFmAbout, FmAbout);
(*
  FmAbout.ProductName := Application.Title;
  FmAbout.Version.Caption := 'Vers�o: ' + VersaoTxt(CO_VERSAO);
object ProductName: TLabel
*)
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TUnAppPF.MostraFormOPcoes();
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := AppPF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
  begin
    Application.CreateForm(TFmOpcoesApp, FmOpcoesApp);
    FmOpcoesApp.EdCPF_CNPJ.ValueVariant      := VAR_CPF_CNPJ;
    FmOpcoesApp.EdPosicao.ValueVariant       := VAR_Posicao;
    FmOpcoesApp.EdCodigoPIN.ValueVariant     := VAR_CodigoPIN;
    FmOpcoesApp.EdAppUserPwd.ValueVariant    := VAR_SENHA_BOSS;
    FmOpcoesApp.CkExigeSenhaLoginApp.Checked := VAR_ExigeSenhaLoginApp;
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

function TUnAppPF.MouseBotaoEventToWindowsMessage(
  MouseBotaoEvent: Integer): Integer;
const
  sProcFunc = 'TUnAppPF.MouseBotaoEventToWindowsMessage()';
begin
  case MouseBotaoEvent of
    //Indefinido
    //Esquerdo
    1: Result := wm_lbuttondown;
    //Direto
    2: Result := wm_rbuttondown;
    //Centro
    3: Result := wm_mbuttondown;
    //
    else
    begin
      Result := -1;
      Geral.MB_Erro('Evento de mouse n�o implementado!' + sLineBreak + sProcFunc);
    end;
  end;
end;

function TUnAppPF.ObtemDadosLicencaDeArquivo(const Arquivo: String; const DCP_twofish:
  TDCP_twofish; var AtualizadoEm, SerialHD, CPUID, SerialKey: String): Boolean;
var
  Texto: TStringList;
var
  jsonObj, jSubObj: TJSONObject;
  ja: TJSONArray;
  jv: TJSONValue;
  i: Integer;
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Txt, TxtCript: String;
  EstadoJanela: Integer;
  DtUltimaAtz: TDateTime;
  Liberado: Boolean;
begin
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(Arquivo) then Exit;
  Texto := TStringList.Create;
  try
    Txt := TFile.ReadAllText(Arquivo);
    Txt := DeCryptJSON(Txt, DCP_twofish);
    //
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
    if jsonObj = nil then
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        Exit;
    VAR_CPF_CNPJ  := MyJSON.JObjValStr(jsonObj, 'CPF_CNPJ');
    SerialHD      := MyJSON.JObjValStr(jsonObj, 'SerialHD');
    CPUID         := MyJSON.JObjValStr(jsonObj, 'CPUID');
    SerialKey     := MyJSON.JObjValStr(jsonObj, 'SerialKey');
    AtualizadoEm  := MyJSON.JObjValStr(jsonObj, 'AtualizadoEm');
    //App User Pwd    := MyJSON.JObjValStr(jsonObj, 'App User Pwd');
    //Exe On Ini      := MyJSON.JObjValStr(jsonObj, 'Exe On Ini');
    //if Exe On Ini <> '1' then
      //Exe On Ini := '0';
    DtUltimaAtz   := Geral.ValidaDataHoraSQL(AtualizadoEm);
  finally
    Texto.Free;
  end;
end;

function TUnAppPF.ObtemDadosOpcoesAppDeArquivo(const Arquivo: String;
  const DCP_twofish: TDCP_twofish): Boolean;
var
  Texto: TStringList;
  Txt: String;
  jsonObj: TJSONObject;
begin
  Result := False;
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(Arquivo) then
  begin
    AppPF.MostraFormOpcoes();
    Halt(0);
  end else
  begin
    Texto := TStringList.Create;
    try
      Txt := TFile.ReadAllText(Arquivo);
      Txt := DeCryptJSON(Txt, DCP_twofish);
      //
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
        if jsonObj = nil then
          Exit;
      //
      VAR_CPF_CNPJ           := MyJSON.JObjValStr(jsonObj, 'CPF_CNPJ');
      VAR_SENHA_BOSS         := MyJSON.JObjValStr(jsonObj, 'SENHA_BOSS');
      VAR_Posicao            := MyJSON.JObjValInt(jsonObj, 'Posicao');
      VAR_CodigoPIN          := MyJSON.JObjValStr(jsonObj, 'CodigoPIN');
      VAR_ExigeSenhaLoginApp := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExigeSenhaLoginApp'));
      //
      Result := True;
    finally
      Texto.Free;
    end;
  end;
end;

function TUnAppPF.ObtemNomeArquivoLicenca(var Arquivo, Pasta: String): Boolean;
begin
  Arquivo := '';
  Pasta := GetSpecialFolderPath(CSIDL_COMMON_DOCUMENTS);
  Pasta := IncludeTrailingPathDelimiter(Pasta) + 'Dermatek\Voyez\';
  Arquivo := Pasta + 'Voyez.dmklic';
  Result := True;
end;

function TUnAppPF.ObtemNomeArquivoOpcoes(): String;
var
  Pasta: String;
begin
  Result := '';
  Pasta    := AppPF.GetSpecialFolderPath(CSIDL_COMMON_DOCUMENTS);
  Pasta    := IncludeTrailingPathDelimiter(Pasta) + 'Dermatek\Voyez\';
  //
  Result := Pasta + 'Voyez.dmkopt';
end;

function TUnAppPF.ObtemNomeArquivoTarefa(): String;
var
  Pasta: String;
begin
  Result := '';
  Pasta := CO_DIR_RAIZ_DMK + '\Voyez\Data\';
  ForceDirectories(Pasta);
  //FDBArqName := Pasta + 'TarefaUnica.json';
  Result := Pasta + 'TarefaUnica.dmkafr';
end;

function TUnAppPF.UpTime(): string;
const
  ticksperday: integer = 1000 * 60 * 60 * 24;
  ticksperhour: integer = 1000 * 60 * 60;
  ticksperminute: integer = 1000 * 60;
  tickspersecond: integer = 1000;

var
  t: longword;
  d, h, m, s: integer;

begin
  t := GetTickCount;

  d := t div ticksperday;
  dec(t, d * ticksperday);

  h := t div ticksperhour;
  dec(t, h * ticksperhour);

  m := t div ticksperminute;
  dec(t, m * ticksperminute);

  s := t div tickspersecond;

  Result := IntToStr(d) + ' dias ' + IntToStr(h) + ' horas ' + IntToStr(m)
    + ' minutos ' + IntToStr(s) + ' segundos';
end;

function TUnAppPF.WindowsMessageToMouseBotaoEvent(
  WindowsMessage: Integer): Integer;
const
  sProcFunc = 'TUnAppPF.WindowsMessageToMouseBotaoEvent()';
begin
  case WindowsMessage of
    //Indefinido
    //Esquerdo
    wm_lbuttondown: Result := 1;
    //Direto
    wm_rbuttondown: Result := 2;
    //Centro
    wm_mbuttondown: Result := 3;
    //
    else
    begin
      Result := -1;
      Geral.MB_Erro('Mensagem do windows n�o implementada!' + sLineBreak + sProcFunc);
    end;
  end;
end;

end.
