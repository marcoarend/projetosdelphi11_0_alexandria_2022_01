unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Registry, Data.DB, Vcl.Grids, Vcl.DBGrids, ShellAPI,
(*
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, CheckLst, ComCtrls, ExtCtrls, Spin, Menus, ShellApi,
  UnInternalConsts, MySQLTools, Db, mySQLDbTables, Grids, DBGrids,
  MySQLDump, MySQLBatch, UnDmkSystem, SHFolder, dmkEdit,
  UnDmkEnums, UnGrl_Vars;
  *)
  Winapi.ShlObj, Winapi.KnownFolders, DCPcrypt2, DCPblockciphers, DCPtwofish,
  Datasnap.DBClient, Vcl.ComCtrls, dmkEdit,  UnDmkProcFunc, dmkGeral;
type
  TFmPrincipal = class(TForm)
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtAtiva: TBitBtn;
    DCP_twofish1: TDCP_twofish;
    BtCon: TBitBtn;
    BtOpcoesApp: TBitBtn;
    TmMonitora: TTimer;
    Label1: TLabel;
    EdTempoAtividade: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    BtVisualizaItens: TBitBtn;
    MeLog: TMemo;
    Button2: TButton;
    BtSobre: TBitBtn;
    Button3: TButton;
    TmLibera: TTimer;
    BtTrafego: TBitBtn;
    procedure TrayIcon1Click(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure TmOcultaTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConClick(Sender: TObject);
    procedure BtAtivaClick(Sender: TObject);
    procedure BtOpcoesAppClick(Sender: TObject);
    procedure TmMonitoraTimer(Sender: TObject);
    procedure BtVisualizaItensClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BtSobreClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure TmLiberaTimer(Sender: TObject);
    procedure BtTrafegoClick(Sender: TObject);
  private
    { Private declarations }
    //FHides: Integer;
    FShowed, FAtivou: Boolean;
    //FHideFirst: Boolean;
    //
(*Eliminar
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure AtivaTarefa(Aguarda, Executa: Boolean);
Eliminar*)
  public
    { Public declarations }
    FBtAtivado: Boolean;
    //
    FNome, FPasta, FExecutavel, FParametros: String;
    FMonitSohProprioProces,
    //FParaEtapaOnDeactive,
    FExecDoleAsAdmin, FExecTargetAsAdmin, FLogInMemo, FExeOnIni: Boolean;
    FSegWaitOnOSReinit, FSegWaitScanMonit,
    FHWNDShow, FEstadoJanela, FMaiorCodigo, FHProcesso,
    FIntervalPacks, FMaxSecPcks, FPorta, FCicloTimeSeg, FCicloMinPacks,
    FCicloMinBytes, FPackMinBytes, FMaxTempoOciosoSeg, FTempoPergKillSecs: Integer;
    //FSTUserName, FSTUserPwd: String;

    //
    //procedure ScheduleRunAtStartup();
    procedure ColocaEmPergunta();
  end;

var
  FmPrincipal: TFmPrincipal;

const
  CO_Titulo = 'Reiniciador de Aplicativos';

implementation

{$R *.dfm}

uses UnProjGroup_PF, UnProjGroup_Vars, UnMyObjects, UnMyVclEvents,
  UnPrjGruFiles_PF,
  Trafego;

  (*dmkGeral, UnDmkProcFunc, UnLicVendaApp_Dmk,
  UnMyVclEvents, ShellApi, UnAppPF, UnAppMainPF, OpcoesApp, UnApp_Vars,

(*
UnAppJan, MyDBCheck, UnOVS_ProjGroupVars, UnProjGroup_Jan,
  Module, ModuleGeral, UnProjGroup_PF;
*)


procedure TFmPrincipal.BtVisualizaItensClick(Sender: TObject);
var
  Continua: Boolean;
begin
(*Eliminar
  if DBGEtapas.Visible then
  begin
    DBGEtapas.Visible := False;
    BtVisualizaItens.Caption := 'Visualizar itens';
  end else
  begin
    if VAR_SENHA_BOSS <> EmptyStr then
      Continua := AppPF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
    else
      Continua := True;
    if Continua then
    begin
      DBGEtapas.Visible := True;
      BtVisualizaItens.Caption := 'Ocultar itens';
    end;
  end;
Eliminar*)
end;

procedure TFmPrincipal.BtAtivaClick(Sender: TObject);
begin
  PrjGruFiles_PF.MostraFormAgendaTarefa_ParaBoss();
end;

procedure TFmPrincipal.BtConClick(Sender: TObject);
begin
  PrjGruFiles_PF.MostraFormTarefa_ParaBoss();
end;

procedure TFmPrincipal.BtOpcoesAppClick(Sender: TObject);
begin
  ProjGroup_PF.MostraFormOpcoes();
end;

procedure TFmPrincipal.BtSaidaClick(Sender: TObject);
begin
(*Eliminar
  TrayIcon1.Visible := True;
Eliminar*)

(*
  Application.MainFormOnTaskBar := FShowed;
  FmPrincipal.Hide;
  //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
  Application.MainFormOnTaskBar := FShowed;
*)

end;

procedure TFmPrincipal.BtSobreClick(Sender: TObject);
begin
  ProjGroup_PF.MostraFormABOUT();
end;

procedure TFmPrincipal.BtTrafegoClick(Sender: TObject);
begin
  FmTrafego.FShow := True;
  FmTrafego.Show;
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
(*Eliminar
const
  CPFCNPJs: array[0..2] of String = ('03.143.014/0001-52', '02.582.267/0001-60', '96.734.892/0001-23');
var
  I, J: Integer;
  Texto, CPFCNPJ, CodigoPIN: String;
Eliminar*)
begin
(*Eliminar
  for I := 0 to 9 do
  begin
    Texto := '';
    for J := 0 to 2 do
    begin
      CPFCNPJ := CPFCNPJs[J];
      LicVendaApp_Dmk.ObtemCodigoPinDeCnpj(CPFCNPJ, I + 1, CodigoPIN);
      //
      Texto := Texto + ' [' + CPFCNPJ + ']  [' + CodigoPIN + ']';
    end;
    MeLog.Lines.Add(Texto);
  end;
Eliminar*)
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
(*Eliminar
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      //HProcesso := sei.hProcess;
    end;
  end;
var
  Executavel, Pasta, Parametros: String;
Eliminar*)
begin
(*Eliminar
  Executavel     := 'Sniffer.exe';
  //Executavel     := 'Ativador.exe';
  Pasta          := 'C:\Executaveis\19.0\EarTug\';
  Parametros     := '';
  RunAsAdmin(FmPrincipal.Handle, Executavel, Pasta, Parametros, SW_SHOWNORMAL);
Eliminar*)
end;

procedure TFmPrincipal.ColocaEmPergunta();
begin
  // Compatibilidade   ...  com o Dole
end;

(*Eliminar
procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo,
  Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_CURRENT_USER;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;
Eliminar*)

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
(*Eliminar
// UnAppPF.GetSpecialFolderPath(CSIDL_STARTUP) =
//C:\Users\angeloni\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup

//Make Vista launch UAC restricted programs at startup with Task Scheduler
//https://www.techrepublic.com/blog/windows-and-office/make-vista-launch-uac-restricted-programs-at-startup-with-task-scheduler/


  ExecutaNaInicializacao(True, CO_Titulo, Application.ExeName);
Eliminar*)
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
(*Eliminar
//  Close;
Eliminar*)
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*Eliminar
  if TrayIcon1 <> nil then
    TrayIcon1.Visible := False;
Eliminar*)
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FBtAtivado  := False;
  FExeOnIni   := False;
  FHProcesso  := 0;
(*
  TMeuDB          := 'overseer';
  VAR_RUN_AS_SVC  := True;
  VAR_BDSENHA     := EmptyStr;
  FHides          := 0;
*)
  FShowed         := True;
  FAtivou         := False;
  FNome           := '';
  FPasta          := '';
  FExecutavel     := '';
  FParametros     := '';
  FEstadoJanela   := -1;
  //
(*
  VAR_TYPE_LOG :=  TTypeLog.ttlCliIntUni;
*)
  Geral.DefineFormatacoes;
  dmkPF.ConfigIniApp(0);
  Application.OnMessage := MyObjects.FormMsg;
  //Application.OnIdle    := AppIdle;
  //
  TmLibera.Enabled := True;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
(*Eliminar
  FmPrincipal.Visible := True;
Eliminar*)
end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
//  ExecutaNaInicializacao(False, CO_Titulo, Application.ExeName);
end;

procedure TFmPrincipal.PopupMenu1Popup(Sender: TObject);
begin
(*Eliminar
var
  Valor: String;
begin
  Valor := Geral.ReadAppKeyCU(CO_Titulo, 'Microsoft\Windows\CurrentVersion\Run', ktString, '');
  //
  if Valor <> '' then
  begin
    Executarnainicializao1.Checked   := True;
    NOexecutarnainicializao1.Checked := False;
  end else
  begin
    Executarnainicializao1.Checked   := False;
    NOexecutarnainicializao1.Checked := True;
  end;
  Mostrar1.Enabled := not FmPrincipal.Visible;
Eliminar*)
end;

{
procedure TFmPrincipal.ScheduleRunAtStartup();
var
  ATaskName, AFileName, AUserAccount, AUserPassword: string;
  Retorno: Integer;
begin
  ATaskName     := Application.Name + ' as admin';
  AFileName     := Application.ExeName;
  AUserAccount  := FSTUserName;
  AUserPassword := FSTUserPwd;
  //
  DmkPF.ScheduleRunAtStartup(ATaskName, AFileName, AUserAccount, AUserPassword);
  //DmkPF.UnSheduleRunAtStatup(ATaskName);

  Retorno := ShellExecute(0, nil, 'schtasks', PChar('/query /tn "' + ATaskName + '"'),
    nil, SW_HIDE);
  if Retorno <= 32 then
    ShowMessage(SysErrorMessage(Retorno))
  else
    ShowMessage('"' + Application.Name + '" ativado!');
end;
}

procedure TFmPrincipal.TmLiberaTimer(Sender: TObject);
var
  Continua: Boolean;
begin
  TmLibera.Enabled := False;
  MyObjects.CorIniComponente;
  Continua := False;
  //
  if FAtivou = False then
  begin
    FAtivou := True;
    if ProjGroup_PF.ObtemDadosOpcoesAppDeArquivo(
      ProjGroup_PF.ObtemNomeArquivoOpcoes(), DCP_twofish1) then
    begin
      if ProjGroup_PF.LiberaUsoVendaApp1(DCP_twofish1(*, FApp User Pwd, FExe On Ini*)) then
      begin
        // Liberar tudo por aqui!
        if (VAR_SENHA_BOSS <> EmptyStr) and (VAR_ExigeSenhaLoginApp) then
          Continua := ProjGroup_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
        else
          Continua := True;
      end else
      begin
        Geral.MB_Info('N�O Liberado!');
        Halt(0);
      end;
    end;
  end else
  begin
    Continua := True;
  end;
  //
  //
  if Continua then
  begin
    Continua := PrjGruFiles_PF.CarregaDadosDeArquivo(
    FmPrincipal.DCP_twofish1,
    FNome, FPasta, FExecutavel, FParametros, FMonitSohProprioProces,
    (*FParaEtapaOnDeactive,*) FExecDoleAsAdmin, FExecTargetAsAdmin, FLogInMemo, FExeOnIni,
    FSegWaitOnOSReinit, FSegWaitScanMonit,
    FHWNDShow, FEstadoJanela(*, FMaiorCodigo*),
    FIntervalPacks, FMaxSecPcks, FPorta, FCicloTimeSeg, FCicloMinPacks,
    FCicloMinBytes, FPackMinBytes, FMaxTempoOciosoSeg, FTempoPergKillSecs(*,
    FSTUserName, FSTUserPwd*));
  end;
  if Continua then
  begin
    Application.CreateForm(TFmTrafego, FmTrafego);
  end;
end;

procedure TFmPrincipal.TmMonitoraTimer(Sender: TObject);
begin
(*Eliminar
  AtivaTarefa(False, True);
  EdTempoAtividade.Text := AppPF.UpTime();
Eliminar*)
end;

procedure TFmPrincipal.TmOcultaTimer(Sender: TObject);
begin
(*Eliminar
  TmOculta.Enabled := False;
  BtSaidaClick(Self);
  if (MemTable.State <> dsInactive)  and (FExeOnIni) then
  begin
    AtivaTarefa(True, True);
    //TmMonitora.Interval := FSegWaitScanMonit * 1000;
    TmMonitora.Enabled := True;
  end;
Eliminar*)
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
(*Eliminar
  FmPrincipal.Visible := True;
Eliminar*)
end;

(*
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
object TrayIcon1: TTrayIcon
  BalloonHint = 'Controlador de uso de Aplicativos'
  BalloonTitle = 'Controlador de uso de Aplicativos'
  PopupMenu = PopupMenu1
  OnClick = TrayIcon1Click
  Left = 64
  Top = 316
end

////////////////////////////////////////////////////////////////////////////////

object PopupMenu1: TPopupMenu
  OnPopup = PopupMenu1Popup
  Left = 268
  Top = 316
  object Mostrar1: TMenuItem
    Caption = '&Mostrar'
    OnClick = Mostrar1Click
  end
  object Fechar1: TMenuItem
    Caption = '&Fechar'
    OnClick = Fechar1Click
  end
  object N1: TMenuItem
    Caption = '-'
  end
  object Inicializao1: TMenuItem
    Caption = '&Inicializa'#231#227'o'
    object Executarnainicializao1: TMenuItem
      Caption = '&Executar na inicializa'#231#227'o'
      OnClick = Executarnainicializao1Click
    end
    object NOexecutarnainicializao1: TMenuItem
      Caption = '&N'#195'O executar na inicializa'#231#227'o'
      OnClick = NOexecutarnainicializao1Click
    end
  end
end

////////////////////////////////////////////////////////////////////////////////

object TmOculta: TTimer
  Enabled = False
  OnTimer = TmOcultaTimer
  Left = 412
  Top = 312
end

////////////////////////////////////////////////////////////////////////////////

object MemTable: TClientDataSet
  Aggregates = <>
  FieldDefs = <>
  IndexDefs = <>
  Params = <>
  StoreDefs = True
  Left = 360
  Top = 161
  object MemTableTpAcao: TIntegerField
    FieldName = 'TpAcao'
  end
  object MemTableCodigo: TIntegerField
    FieldName = 'Codigo'
  end
  object MemTableOrdem: TIntegerField
    FieldName = 'Ordem'
  end
  object MemTableNome: TStringField
    FieldName = 'Nome'
    Size = 100
  end
  object MemTableSegundosA: TFloatField
    FieldName = 'SegundosA'
  end
  object MemTableSegundosD: TFloatField
    FieldName = 'SegundosD'
  end
  object MemTableMousePosX: TIntegerField
    FieldName = 'MousePosX'
  end
  object MemTableTexto: TStringField
    FieldName = 'Texto'
    Size = 255
  end
  object MemTableMousePosY: TIntegerField
    FieldName = 'MousePosY'
  end
  object MemTableMouseEvent: TIntegerField
    FieldName = 'MouseEvent'
  end
  object MemTableQtdClkSmlt: TIntegerField
    FieldName = 'QtdClkSmlt'
  end
  object MemTableKeyCode: TIntegerField
    FieldName = 'KeyCode'
  end
  object MemTableKeyEvent: TIntegerField
    FieldName = 'KeyEvent'
  end
  object MemTableShiftState: TIntegerField
    FieldName = 'ShiftState'
  end
  object MemTableTipoTecla: TIntegerField
    FieldName = 'TipoTecla'
  end
  object MemTableTeclEsp: TIntegerField
    FieldName = 'TeclEsp'
  end
  object MemTableLetra: TStringField
    DisplayWidth = 2
    FieldName = 'Letra'
    Size = 2
  end
end
object DataSource1: TDataSource
  DataSet = MemTable
  Left = 360
  Top = 209
end

////////////////////////////////////////////////////////////////////////////////

object DBGEtapas: TDBGrid
  Left = 0
  Top = 47
  Width = 1000
  Height = 330
  Align = alClient
  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
  TabOrder = 1
  TitleFont.Charset = DEFAULT_CHARSET
  TitleFont.Color = clWindowText
  TitleFont.Height = -11
  TitleFont.Name = 'Tahoma'
  TitleFont.Style = []
  Visible = False
  Columns = <
    item
      Expanded = False
      FieldName = 'Ordem'
      Width = 36
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'Codigo'
      Title.Caption = 'ID seq.'
      Width = 38
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'Nome'
      Title.Caption = 'Descri'#231#227'o'
      Width = 258
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'SegundosA'
      Title.Caption = 'Seg.Ant.'
      Width = 44
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'SegundosD'
      Title.Caption = 'Seg.Dep.'
      Width = 44
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'MouseEvent'
      Title.Caption = 'Even.Mouse'
      Width = 44
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'MousePosX'
      Title.Caption = 'Mouse X'
      Width = 44
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'MousePosY'
      Title.Caption = 'Mouse Y'
      Width = 44
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'QtdClkSmlt'
      Title.Caption = 'Tipo click'
      Width = 25
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'Texto'
      Title.Caption = 'Digita'#231#227'o texto'
      Width = 79
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'KeyCode'
      Title.Caption = 'Key code'
      Width = 52
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'KeyEvent'
      Title.Caption = 'Key Event'
      Width = 52
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'ShiftState'
      Title.Caption = 'Shift State'
      Width = 52
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'TipoTecla'
      Width = 52
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'TeclEsp'
      Title.Caption = 'Tecl.Esp.'
      Width = 52
      Visible = True
    end
    item
      Expanded = False
      FieldName = 'Letra'
      Width = 32
      Visible = True
    end>
end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


*)end.
