object frmMain: TfrmMain
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Configura'#231#227'o de An'#225'lise de Tr'#225'fego de Dados'
  ClientHeight = 682
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 651
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 787
      Top = 1
      Height = 649
      Align = alRight
      ExplicitLeft = 801
      ExplicitTop = 25
      ExplicitHeight = 546
    end
    object Panel2: TPanel
      Left = 790
      Top = 1
      Width = 217
      Height = 649
      Align = alRight
      TabOrder = 0
      object Splitter2: TSplitter
        Left = 1
        Top = 122
        Width = 215
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 276
      end
      object ListBox1: TListBox
        Left = 1
        Top = 1
        Width = 215
        Height = 121
        Align = alTop
        ItemHeight = 13
        TabOrder = 0
      end
      object Memo1: TMemo
        Left = 1
        Top = 605
        Width = 215
        Height = 43
        Align = alClient
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 1
        Top = 125
        Width = 215
        Height = 480
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label2: TLabel
          Left = 4
          Top = 4
          Width = 157
          Height = 13
          Caption = 'Atualiza'#231#227'o de status da internet:'
        end
        object Label3: TLabel
          Left = 4
          Top = 304
          Width = 39
          Height = 13
          Caption = 'Pacotes'
        end
        object Label4: TLabel
          Left = 4
          Top = 44
          Width = 199
          Height = 13
          Caption = 'Espera m'#225'xima por pacotes em segundos:'
        end
        object Label5: TLabel
          Left = 72
          Top = 304
          Width = 26
          Height = 13
          Caption = 'Bytes'
        end
        object LaPacotes: TLabel
          Left = 4
          Top = 328
          Width = 6
          Height = 13
          Caption = '0'
        end
        object LaBytes: TLabel
          Left = 72
          Top = 328
          Width = 6
          Height = 13
          Caption = '0'
        end
        object Label6: TLabel
          Left = 4
          Top = 84
          Width = 28
          Height = 13
          Caption = 'Porta:'
        end
        object LaTablPort: TLabel
          Left = 68
          Top = 104
          Width = 9
          Height = 13
          Caption = '...'
        end
        object Label7: TLabel
          Left = 4
          Top = 124
          Width = 138
          Height = 13
          Caption = 'Ciclo de leitura em segundos:'
        end
        object Label8: TLabel
          Left = 4
          Top = 164
          Width = 122
          Height = 13
          Caption = 'M'#237'nimo pacotes por ciclo:'
        end
        object Label9: TLabel
          Left = 4
          Top = 204
          Width = 110
          Height = 13
          Caption = 'M'#237'nimo Bytes por ciclo:'
        end
        object Label10: TLabel
          Left = 4
          Top = 248
          Width = 184
          Height = 13
          Caption = 'Tempo m'#225'ximo "ocioso" em segundos:'
        end
        object Label11: TLabel
          Left = 4
          Top = 352
          Width = 73
          Height = 13
          Caption = 'Tempo ocioso: '
        end
        object LaTempoOcioso: TLabel
          Left = 84
          Top = 352
          Width = 36
          Height = 13
          Caption = '0:00:00'
        end
        object Label12: TLabel
          Left = 4
          Top = 372
          Width = 65
          Height = 13
          Caption = 'Tempo ativo: '
        end
        object LaTempoAtivo: TLabel
          Left = 84
          Top = 372
          Width = 36
          Height = 13
          Caption = '0:00:00'
        end
        object EdIntervalPacks: TdmkEdit
          Left = 4
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '90'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 90
          ValWarn = False
          OnRedefinido = EdIntervalPacksRedefinido
        end
        object EdMaxSecPcks: TdmkEdit
          Left = 4
          Top = 60
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '90'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 90
          ValWarn = False
          OnRedefinido = EdMaxSecPcksRedefinido
        end
        object Button1: TButton
          Left = 16
          Top = 420
          Width = 145
          Height = 33
          Caption = 'Iniciar como administrador'
          TabOrder = 2
          OnClick = Button1Click
        end
        object BtOcultar: TButton
          Left = 16
          Top = 456
          Width = 145
          Height = 25
          Caption = 'Oculta'
          TabOrder = 3
          OnClick = BtOcultarClick
        end
        object EdPorta: TdmkEdit
          Left = 4
          Top = 100
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '491'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 491
          ValWarn = False
          OnRedefinido = EdPortaRedefinido
        end
        object EdCicloTimeSeg: TdmkEdit
          Left = 4
          Top = 140
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '60'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 60
          ValWarn = False
          OnRedefinido = EdCicloTimeSegRedefinido
        end
        object EdCicloMinPacks: TdmkEdit
          Left = 4
          Top = 180
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '50'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 50
          ValWarn = False
          OnRedefinido = EdCicloMinPacksRedefinido
        end
        object EdCicloMinBytes: TdmkEdit
          Left = 4
          Top = 220
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '10000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 10000
          ValWarn = False
          OnRedefinido = EdCicloTimeSegRedefinido
        end
        object EdMaxTempoOciosoSeg: TdmkEdit
          Left = 4
          Top = 264
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '300'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 300
          ValWarn = False
          OnRedefinido = EdMaxTempoOciosoSegRedefinido
        end
      end
    end
    object RichEdit1: TRichEdit
      Left = 1
      Top = 1
      Width = 786
      Height = 649
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      PlainText = True
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 1
      WordWrap = False
      Zoom = 100
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 651
    Width = 1008
    Height = 31
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 92
      Height = 13
      Caption = 'Conex'#245'es de rede: '
    end
    object cbInterfaces_A: TComboBox
      Left = 99
      Top = 6
      Width = 404
      Height = 21
      Style = csDropDownList
      TabOrder = 0
    end
    object BtLista: TButton
      Left = 508
      Top = 7
      Width = 57
      Height = 20
      Caption = 'Lista'
      TabOrder = 1
      OnClick = btnStartStopClick
    end
    object btnStartStop: TButton
      Left = 572
      Top = 7
      Width = 52
      Height = 20
      Caption = 'Ativa'
      TabOrder = 2
      OnClick = btnStartStopClick
    end
    object cbInterfaces_W: TComboBox
      Left = 632
      Top = 6
      Width = 311
      Height = 21
      Style = csDropDownList
      TabOrder = 3
      Visible = False
    end
  end
  object TmHasPacks: TTimer
    Enabled = False
    OnTimer = TmHasPacksTimer
    Left = 92
    Top = 8
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 164
    Top = 216
  end
  object Timer2: TTimer
    OnTimer = Timer2Timer
    Left = 500
    Top = 292
  end
  object TmCiclo: TTimer
    Interval = 60000
    OnTimer = TmCicloTimer
    Left = 164
    Top = 8
  end
  object TmOcioso: TTimer
    Enabled = False
    Interval = 300000
    OnTimer = TmOciosoTimer
    Left = 164
    Top = 56
  end
end
