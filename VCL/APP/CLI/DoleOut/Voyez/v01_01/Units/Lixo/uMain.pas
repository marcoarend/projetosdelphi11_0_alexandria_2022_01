
//  *************************************************
//  * Project   : SnifferDemo
//  * Purpose   : ���������������� ������ ��������.
//  * Author    : ��������� (Rouse_) ������
//  * Version   : 1.01
//  *************************************************
//  �� ������:
//  � �� ������ ����� ����� ����� �������� ���-�� �������������,
//  ������ ���� ������� �������� ��� ������� ��������� �����������
//  ��������, ��� � � ���������� :)
//
//  ��, �� � �������� ��� ��� ����������, ������ ������� �
//  Windows 2000 :)

unit uMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, WinSock, ComCtrls, Vcl.ExtCtrls, dmkEdit,
  ShellAPI, Vcl.Menus;

const
  MAX_PACKET_SIZE = $10000; // 2^16
  SIO_RCVALL = $98000001;
  WSA_VER = $202;  // Marco 2.0:
  MAX_ADAPTER_NAME_LENGTH        = 256;
  MAX_ADAPTER_DESCRIPTION_LENGTH = 128;
  MAX_ADAPTER_ADDRESS_LENGTH     = 8;
  IPHelper = 'iphlpapi.dll';

  // ��� ICMP ������
  ICMP_ECHO             = 8;   // ������
  ICMP_ECHOREPLY        = 0;   // �����

resourcestring
  LOG_STR_0 = ''; //'==============================================================================' + sLineBreak;
  LOG_STR_1 = 'Packet ID: %-5d TTL: %d'; // + sLineBreak;
  LOG_STR_2 = 'Packet size: %-5d bytes type: %s'; // + sLineBreak;
  LOG_STR_3 = 'Source IP      : %15s: %d'; // + sLineBreak;
  LOG_STR_4 = 'Destination IP : %15s: %d'; // + sLineBreak;
  LOG_STR_5 = 'ARP Type: %s, operation: %s'; // + sLineBreak;
  LOG_STR_6 = 'ICMP Type: %s'; // + sLineBreak;
  LOG_STR_7 = ''; //'------------------------------ Packet dump -----------------------------------' + sLineBreak;

type
  USHORT = WORD;
  ULONG = DWORD;
  time_t = Longint;

  // ip ���������
  // ����� �������� � RFC 791
  // http://rtfm.vn.ua/inet/prot/rfc791r.html
  TIPHeader = packed record
    iph_verlen:   UCHAR;    // ������ � ����� ���������
    iph_tos:      UCHAR;    // ��� �������
    iph_length:   USHORT;   // ����� ����� ������
    iph_id:       USHORT;   // �������������
    iph_offset:   USHORT;   // ����� � ��������
    iph_ttl:      UCHAR;    // ����� ����� ������
    iph_protocol: UCHAR;    // ��������
    iph_xsum:     USHORT;   // ����������� �����
    iph_src:      ULONG;    // IP-����� �����������
    iph_dest:     ULONG;    // IP-����� ����������
  end;
  PIPHeader = ^TIPHeader;

  // tcp ���������
  // ����� �������� � RFC 793
  // http://rtfm.vn.ua/inet/prot/rfc793r.html
  TTCPHeader = packed record
    sourcePort: USHORT;       // ���� �����������
    destinationPort: USHORT;  // ���� ����������
    sequenceNumber: ULONG;    // ����� ������������������
    acknowledgeNumber: ULONG; // ����� �������������
    dataoffset: UCHAR;        // �������� �� ������� ������
    flags: UCHAR;             // �����
    windows: USHORT;          // ������ ����
    checksum: USHORT;         // ����������� �����
    urgentPointer: USHORT;    // ���������
  end;
  PTCPHeader = ^TTCPHeader;

  // udp ���������
  // ����� �������� � RFC 768
  // http://rtfm.vn.ua/inet/prot/rfc768r.html
  TUDPHeader = packed record
    sourcePort:       USHORT;  // ���� �����������
    destinationPort:  USHORT;  // ���� ����������
    len:              USHORT;  // ����� ������
    checksum:         USHORT;  // ����������� �����
  end;
  PUDPHeader = ^TUDPHeader;

  // ICMP ���������
  // ����� �������� � RFC 792
  // http://rtfm.vn.ua/inet/prot/rfc792r.html
  TICMPHeader = packed record
   IcmpType      : BYTE;      // ��� ������
   IcmpCode      : BYTE;      // ��� ������
   IcmpChecksum  : WORD;
   IcmpId        : WORD;
   IcmpSeq       : WORD;
   IcmpTimestamp : DWORD;
  end;
  PICMPHeader = ^TICMPHeader;


{  Original
  // ��������� ��� ���������� GetAdaptersInfo
  IP_ADDRESS_STRING = record
    S: array [0..15] of Char;
  end;
  IP_MASK_STRING = IP_ADDRESS_STRING;
  PIP_MASK_STRING = ^IP_MASK_STRING;

  PIP_ADDR_STRING = ^IP_ADDR_STRING;
  IP_ADDR_STRING = record
    Next: PIP_ADDR_STRING;
    IpAddress: IP_ADDRESS_STRING;
    IpMask: IP_MASK_STRING;
    Context: DWORD;
  end;

  PIP_ADAPTER_INFO = ^IP_ADAPTER_INFO;
  IP_ADAPTER_INFO = record
    Next: PIP_ADAPTER_INFO;
    ComboIndex: DWORD;
    AdapterName: array [0..MAX_ADAPTER_NAME_LENGTH + 3] of Char;
    Description: array [0..MAX_ADAPTER_DESCRIPTION_LENGTH + 3] of Char;
    AddressLength: UINT;
    Address: array [0..MAX_ADAPTER_ADDRESS_LENGTH - 1] of BYTE;
    Index: DWORD;
    Type_: UINT;
    DhcpEnabled: UINT;
    CurrentIpAddress: PIP_ADDR_STRING;
    IpAddressList: IP_ADDR_STRING;
    GatewayList: IP_ADDR_STRING;
    DhcpServer: IP_ADDR_STRING;
    HaveWins: BOOL;
    PrimaryWinsServer: IP_ADDR_STRING;
    SecondaryWinsServer: IP_ADDR_STRING;
    LeaseObtained: time_t;
    LeaseExpires: time_t;
  end;
}

  // Ansi
  // ��������� ��� ���������� GetAdaptersInfo
  IP_ADDRESS_STRING_A = record
    S: array [0..15] of AnsiChar;
  end;
  IP_MASK_STRING_A = IP_ADDRESS_STRING_A;
  PIP_MASK_STRING_A = ^IP_MASK_STRING_A;

  PIP_ADDR_STRING_A = ^IP_ADDR_STRING_A;
  IP_ADDR_STRING_A = record
    Next: PIP_ADDR_STRING_A;
    IpAddress: IP_ADDRESS_STRING_A;
    IpMask: IP_MASK_STRING_A;
    Context: DWORD;
  end;

  PIP_ADAPTER_INFO_A = ^IP_ADAPTER_INFO_A;
  IP_ADAPTER_INFO_A = record
    Next: PIP_ADAPTER_INFO_A;
    ComboIndex: DWORD;
    AdapterName: array [0..MAX_ADAPTER_NAME_LENGTH + 3] of AnsiChar;
    Description: array [0..MAX_ADAPTER_DESCRIPTION_LENGTH + 3] of AnsiChar;
    AddressLength: UINT;
    Address: array [0..MAX_ADAPTER_ADDRESS_LENGTH - 1] of BYTE;
    Index: DWORD;
    Type_: UINT;
    DhcpEnabled: UINT;
    CurrentIpAddress: PIP_ADDR_STRING_A;
    IpAddressList: IP_ADDR_STRING_A;
    GatewayList: IP_ADDR_STRING_A;
    DhcpServer: IP_ADDR_STRING_A;
    HaveWins: BOOL;
    PrimaryWinsServer: IP_ADDR_STRING_A;
    SecondaryWinsServer: IP_ADDR_STRING_A;
    LeaseObtained: time_t;
    LeaseExpires: time_t;
  end;

{
  // Original
  // ��������� ��� ���������� GetAdaptersInfo
  IP_WDDRESS_STRING_W = record
    S: array [0..15] of Char;
  end;
  IP_MASK_STRING_W = IP_WDDRESS_STRING_W;
  PIP_MASK_STRING_W = ^IP_MASK_STRING_W;

  PIP_ADDR_STRING_W = ^IP_ADDR_STRING_W;
  IP_ADDR_STRING_W = record
    Next: PIP_ADDR_STRING_W;
    IpAddress: IP_WDDRESS_STRING_W;
    IpMask: IP_MASK_STRING_W;
    Context: DWORD;
  end;

  PIP_ADAPTER_INFO_W = ^IP_ADAPTER_INFO_W;
  IP_ADAPTER_INFO_W = record
    Next: PIP_ADAPTER_INFO_W;
    ComboIndex: DWORD;
    AdapterName: array [0..MAX_ADAPTER_NAME_LENGTH + 3] of AnsiChar;
    Description: array [0..MAX_ADAPTER_DESCRIPTION_LENGTH + 3] of AnsiChar;
    AddressLength: UINT;
    Address: array [0..MAX_ADAPTER_ADDRESS_LENGTH - 1] of BYTE;
    Index: DWORD;
    Type_: UINT;
    DhcpEnabled: UINT;
    CurrentIpAddress: PIP_ADDR_STRING_W;
    IpAddressList: IP_ADDR_STRING_W;
    GatewayList: IP_ADDR_STRING_W;
    DhcpServer: IP_ADDR_STRING_W;
    HaveWins: BOOL;
    PrimaryWinsServer: IP_ADDR_STRING_W;
    SecondaryWinsServer: IP_ADDR_STRING_W;
    LeaseObtained: time_t;
    LeaseExpires: time_t;
  end;
}


  // ����� ��������
  TSnifferThread = class(TThread)
  private
    WSA: TWSAData;
    hSocket: TSocket;
    Addr_in: sockaddr_in;
    Packet: array[0..MAX_PACKET_SIZE - 1] of Byte;
    LogData: String;
    IPv4: String;        // Marco
    Nome: String;        // Marco
    TickCount: UInt64;
    procedure ShowPacket;
  protected
    function InitSocket: Boolean; virtual;
    procedure DeInitSocket(const ExitCode: Integer); virtual;
    procedure Execute; override;
    procedure ParcePacket(const PacketSize: Word); virtual;
    procedure MeuParcePacket(const PacketSize: Word; const IP: String;
              TickCount, TicksPassed: UInt64); virtual;
  public
    Host: String;
  end;

  TOctetosIPv4 = array [0..3] of Byte;
  TfrmMain = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    RichEdit1: TRichEdit;
    Splitter1: TSplitter;
    ListBox1: TListBox;
    Splitter2: TSplitter;
    Memo1: TMemo;
    Panel3: TPanel;
    cbInterfaces_A: TComboBox;
    BtLista: TButton;
    btnStartStop: TButton;
    cbInterfaces_W: TComboBox;
    Label1: TLabel;
    Panel4: TPanel;
    Label2: TLabel;
    EdIntervalPacks: TdmkEdit;
    Label3: TLabel;
    TmHasPacks: TTimer;
    Label4: TLabel;
    EdMaxSecPcks: TdmkEdit;
    Button1: TButton;
    BtOcultar: TButton;
    Timer1: TTimer;
    Label5: TLabel;
    LaPacotes: TLabel;
    LaBytes: TLabel;
    Label6: TLabel;
    EdPorta: TdmkEdit;
    LaTablPort: TLabel;
    Timer2: TTimer;
    TmCiclo: TTimer;
    Label7: TLabel;
    EdCicloTimeSeg: TdmkEdit;
    Label8: TLabel;
    EdCicloMinPacks: TdmkEdit;
    Label9: TLabel;
    EdCicloMinBytes: TdmkEdit;
    Label10: TLabel;
    EdMaxTempoOciosoSeg: TdmkEdit;
    TmOcioso: TTimer;
    Label11: TLabel;
    LaTempoOcioso: TLabel;
    Label12: TLabel;
    LaTempoAtivo: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnStartStopClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TmHasPacksTimer(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure EdPortaRedefinido(Sender: TObject);
    procedure TmCicloTimer(Sender: TObject);
    procedure EdCicloTimeSegRedefinido(Sender: TObject);
    procedure EdCicloMinPacksRedefinido(Sender: TObject);
    procedure EdMaxTempoOciosoSegRedefinido(Sender: TObject);
    procedure EdIntervalPacksRedefinido(Sender: TObject);
    procedure EdMaxSecPcksRedefinido(Sender: TObject);
    procedure TmOciosoTimer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure BtOcultarClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    // TryIcon
    FShowed, FAtivou: Boolean;
    //
    TotalPacketCount: Integer;
    //FSnifferThread: TSnifferThread;
    FArrSnifferThread: array of TSnifferThread;
    // Marco
    FMeuIP: String;
    FMeuIP_Char: PWideChar;
    //FIndex: Integer;
    FMaxSecPcks: Integer;
    FCicloPacks, FCicloBytes, FUsoApPacks, FUsoApBytes, FUserPortToListen, FTablPortToListen,
    FMaxTempoOciosoSeg, FCicloMinPacks, FCicloMinBytes, FTickInicioOcioso,
    FTickInicioAtivo, FTickPassedOcioso, FTickPassedAtivo,
    FTickOnCicloOcios, FTickOnCicloAtivo, FMaxCout, FCountSecs: Integer;

    //Original procedure ReadLanInterfaces;
    //Original
    procedure ReadLanInterfaces_A;
    function  ObtemIP(Retorno: Integer): String;
    procedure ObtemIPs(Retorno: Integer; Lista: TStrings);
    function  ObtemWideChar(s: String): PWideChar;
    //procedure ReadLanInterfaces_W;
    procedure AtualizaConexoesDeRedeAtivas();
    procedure SetIntervalPacotes();
    function  SeparaOctetosIPv4(const IPv4: String; var Octetos: TOctetosIPv4): Boolean;
    function  EhIPv4deRedePrivadaEAtivo(IPv4: String): Boolean;
    procedure RedefinePorta();
    procedure RedefineMaxTempoOcioso();
    procedure RedefineMinTrafficCiclo();
    function  ConvertRawPortToRealPort(RawPort: DWORD) : DWORD;
    function  TickDiff(StartTick, EndTick: DWORD): DWORD;
    function  TicksSince(Tick: DWORD): DWORD;
  end;

  // ��� ������ ������ ������� �� ��������� ������� ������� �����������
  // �� ��������� ���������� � ���������� � ���
{  original
  function GetAdaptersInfo(pAdapterInfo: PIP_ADAPTER_INFO;
    var pOutBufLen: ULONG): DWORD; stdcall; external IPHelper;
}
  //original
  function GetAdaptersInfo(pAdapterInfo: PIP_ADAPTER_INFO_A;
    var pOutBufLen: ULONG): DWORD; stdcall; external IPHelper; (*overload;

  function GetAdaptersInfo(pAdapterInfo: PIP_ADAPTER_INFO_W;
    var pOutBufLen: ULONG): DWORD; stdcall; external IPHelper; overload;*)

const
  // ������� ������������ ��������
  IPHeaderSize = SizeOf(TIPHeader);
  ICMPHeaderSize = SizeOf(TICMPHeader);
  TCPHeaderSize = SizeOf(TTCPHeader);
  UDPHeaderSize = SizeOf(TUDPHeader);

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses dmkGeral, UnDmkProcFunc;

{ TSnifferThread }

// ������������� ���������� ������
function TSnifferThread.InitSocket: Boolean;
var
  PromiscuousMode: Integer;
  WSAStartedID: Integer;
begin
  // iniciar WinSock - Marco > Winsock 2.0 ?
  Result := WSAStartup(WSA_VER, WSA) = NOERROR;  // Erro = 0
  if not Result then
  begin
    LogData := 'Erro: ' + SysErrorMessage(WSAGetLastError);
    Synchronize(ShowPacket);
    Exit;
  end;
  // ������� �����
  hSocket := socket(AF_INET, SOCK_RAW, IPPROTO_IP);
  if hSocket = INVALID_SOCKET then
  begin
    DeInitSocket(WSAGetLastError);
    Exit;
  end;
  FillChar(Addr_in, SizeOf(sockaddr_in), 0);
  Addr_in.sin_family:= AF_INET;
  // ��������� �� ����� ����������� ����� �������
  //Addr_in.sin_addr.s_addr := inet_addr(PChar(Host));
  Addr_in.sin_addr.s_addr := inet_addr(PAnsiChar(Host)); // Marco
  // ��������� ����� � ��������� �������
  if bind(hSocket, Addr_in, SizeOf(sockaddr_in)) <> 0 then
  begin
    DeInitSocket(WSAGetLastError);
    Exit;
  end;
  // ����������� ��������� �� ����� ���� ������� ���������� ����� ��������� - promiscuous mode.
  PromiscuousMode := 1;
  if ioctlsocket(hSocket, SIO_RCVALL, PromiscuousMode) <> 0 then
  begin
    DeInitSocket(WSAGetLastError);
    Exit;
  end;
  Result := True;
end;

procedure TSnifferThread.MeuParcePacket(const PacketSize: Word; const IP: String;
  TickCount, TicksPassed: UInt64);
var
  IPHeader: TIPHeader;
  ICMPHeader: TICMPHeader;
  TCPHeader: TTCPHeader;
  UDPHeader: TUDPHeader;
  SrcPort, DestPort: Word;
  I, Octets, PartOctets: Integer;
  PacketType, DumpData, ExtendedInfo: String;
  Addr, A, B: TInAddr;
  //
  sEI, SorcIP, DestIP: String;
begin
  Inc(frmMain.TotalPacketCount);
  // ������ �� ������ IP ���������
  Move(Packet[0], IPHeader, IPHeaderSize);
  // ����� ����� ����� ������
  (*&�%$#@
  LogData := LOG_STR_0 +
    Format(LOG_STR_1, [frmMain.TotalPacketCount, IPHeader.iph_ttl]);
    *)
  SrcPort := 0;
  DestPort := 0;
  ExtendedInfo := '';
  // ���������� ��� ���������
  case IPHeader.iph_protocol of
    IPPROTO_ICMP: // ICMP
    begin
      PacketType := 'ICMP';
	    // ������ ICMP ���������
      Move(Packet[IPHeaderSize], ICMPHeader, ICMPHeaderSize);
	    // ������� ��� ������
      case ICMPHeader.IcmpCode of
        ICMP_ECHO: ExtendedInfo := Format(LOG_STR_6, ['Echo']);
        ICMP_ECHOREPLY: ExtendedInfo := Format(LOG_STR_6, ['Echo reply']);
      else
        ExtendedInfo := Format(LOG_STR_6, ['Unknown']);
      end;
    end;
    IPPROTO_TCP: // TCP
    begin
      PacketType := 'TCP';
	    // ������ ��� ���������
      Move(Packet[IPHeaderSize], TCPHeader, TCPHeaderSize);
	    // ������� ���� ����������� � ����������
      SrcPort := TCPHeader.sourcePort;
      DestPort := TCPHeader.destinationPort;
    end;
    IPPROTO_UDP: // UDP
    begin
      PacketType := 'UDP';
	    // ������ UDP ���������
      Move(Packet[IPHeaderSize], UDPHeader, UDPHeaderSize);
	    // ������� ���� ����������� � ����������
      SrcPort := UDPHeader.sourcePort;
      DestPort := UDPHeader.destinationPort;
    end;
  else
    PacketType := 'Unsupported (0x' + IntToHex(IPHeader.iph_protocol, 2) + ')';
  end;
  //if ((SrcPort = 60161) or (DestPort = 60161)) and (PacketSize > 72) then
  //if ((SrcPort = 60161) or (DestPort = 60161)) and (PacketSize > 0) then
  //if 1 = 1 then
  if (SrcPort = frmMain.FTablPortToListen) or (DestPort = frmMain.FTablPortToListen) then
  begin
    // ����� ������ ������
    // Original
{(*&�%$#@
    LogData := LogData + Format(LOG_STR_2, [PacketSize, PacketType]);
    if ExtendedInfo <> '' then
      LogData := LogData + ExtendedInfo;

    // ����� IP ����� ����������� � ������
    Addr.S_addr := IPHeader.iph_src;
    LogData := LogData + Format(LOG_STR_3, [inet_ntoa(Addr), SrcPort]);
    // ����� IP ����� ���������� � ������
    Addr.S_addr := IPHeader.iph_dest;
    // Marco
    //LogData := LogData + Format(LOG_STR_4, [inet_ntoa(Addr), DestPort]) + LOG_STR_7;
    LogData := LogData + Format(LOG_STR_4, [inet_ntoa(Addr), DestPort]) +
    '(*' + IntToStr(ConvertRawPortToRealPort(SrcPort)) + '<->' + IntToStr(ConvertRawPortToRealPort(DestPort)) + '*) ' +
    LOG_STR_7;
}
////////////////////////////////////////////////////////////////////////////////
(*  TESTE MEMO
    if ExtendedInfo <> '' then
      sEI := '{' +ExtendedInfo + '}'
    else
      sEI := '';
    // IPs origem e destino
    Addr.S_addr := IPHeader.iph_src;
    SorcIP := Format(LOG_STR_3, [inet_ntoa(Addr), SrcPort]);
    Addr.S_addr := IPHeader.iph_dest;
    DestIP := Format(LOG_STR_4, [inet_ntoa(Addr), DestPort]);
    //
    LogData := FormatDateTime('yy/mm/dd hh:nn:ss', Now()) + ' | '

    + Self.IPV4 + ' | '
    + Self.Nome + ' | '
    + Format(LOG_STR_1, [frmMain.TotalPacketCount, IPHeader.iph_ttl]) + ' | '

    + Format(LOG_STR_2, [PacketSize, PacketType]) + ' | '

    + sEI + ' | '

    + SorcIP + ' | '

    + DestIP + ' | ';
*)
    frmMain.FCicloPacks := frmMain.FCicloPacks + 1;
    frmMain.FCicloBytes := frmMain.FCicloBytes + PacketSize;
    //
    frmMain.FUsoApPacks := frmMain.FUsoApPacks + 1;
    frmMain.FUsoApBytes := frmMain.FUsoApBytes + PacketSize;
{
    if frmMain.TmOcioso.Enabled then
    begin
      frmMain.FTickOnCicloAtivo := 0;
      frmMain.FTickOnCicloOcios := frmMain.TicksSince(frmMain.FTickInicioOcioso);
    end else
    begin
      frmMain.FTickOnCicloAtivo := frmMain.TicksSince(frmMain.FTickInicioAtivo);
      frmMain.FTickOnCicloOcios := 0;
    end;
}
    //


    // ������� ���������� ������ �� ����� (������� �������������� �� ����, ��� ��� ������)
    // ���������� ���-�� ����� �����:
    //
    // ------------------------------ Packet dump -----------------------------------
    // 000000 45 00 00 4E D8 91 00 00 | 80 11 DB 3B C0 A8 02 82     E..N.......;....
    // 000010 C0 A8 02 FF 00 89 00 89 | 00 3A AC 6A 83 BD 01 10     .........:.j....
    // 000020 00 01 00 00 00 00 00 00 | 20 45 43 46 46 45 49 44     ........ ECFFEID
    // 000030 44 43 41 43 41 43 41 43 | 41 43 41 43 41 43 41 43     DCACACACACACACAC
    // 000040 41 43 41 43 41 43 41 43 | 41 00 00 20 00 01           ACACACACA.. ..
  {
    I := 0;
    Octets := 0;
    PartOctets := 0;
    while I < PacketSize do
    begin
      case PartOctets of
        0: LogData := LogData + Format('%.6d ', [Octets]);
        9: LogData := LogData + '| ';
        18:
        begin
          Inc(Octets, 10);
          PartOctets := -1;
          LogData := LogData + '    ' + DumpData + sLineBreak;
          DumpData := '';
        end;
      else
        begin
          LogData := LogData + Format('%s ', [IntToHex(Packet[I], 2)]);
          if Packet[I] in [$19..$7F] then
            DumpData := DumpData + Chr(Packet[I])
          else
            DumpData := DumpData + '.';
          Inc(I);
        end;
      end;
      Inc(PartOctets);
    end;
    if PartOctets <> 0 then
    begin
      PartOctets := (16 - Length(DumpData)) * 3;
      if PartOctets >= 24 then Inc(PartOctets, 2);
      Inc(PartOctets, 4);
      LogData := LogData + StringOfChar(' ', PartOctets) +
        DumpData + sLineBreak + sLineBreak
    end
    else
      LogData := LogData + sLineBreak + sLineBreak;
    // ������� ��� ��� ����������� � Memo
  }
  end else
    LogData := '';
  Synchronize(ShowPacket);
end;

// ���������� ������ ������
procedure TSnifferThread.DeInitSocket(const ExitCode: Integer);
begin
  // ���� ���� ������ - ������� ��
  if ExitCode <> 0 then
  begin
    LogData := 'Erro: ' + SysErrorMessage(ExitCode);
    Synchronize(ShowPacket);
  end;
  // ��������� �����
  if hSocket <> INVALID_SOCKET then closesocket(hSocket);
  // ���������������� WinSock
  WSACleanup;
end;

// ������� ��������� ������ ��������
procedure TSnifferThread.Execute;
var
  PacketSize: Integer;
  TicksPassed: DWORD;
begin
  // ���������� �������������
  if InitSocket then
  try
    // ������ ����� �� �����
    while not Terminated do
    begin
      TicksPassed := FrmMain.TicksSince(Self.TickCount);
      Self.TickCount := GetTickCount64;
      // ���� ��������� ������ (����������� �����)
      PacketSize := recv(hSocket, Packet, MAX_PACKET_SIZE, 0);
      // ���� ���� ������ - ���������� �� ������
      //if PacketSize > SizeOf(TIPHeader) then ParcePacket(PacketSize);
      if PacketSize > SizeOf(TIPHeader) then
        MeuParcePacket(PacketSize, Self.Host, Self.TickCount, TicksPassed);
    end;
  finally
    // � ����� ����������� ������� �������
    DeInitSocket(NO_ERROR);
  end;
end;

// ��������� �������� ������
procedure TSnifferThread.ParcePacket(const PacketSize: Word);
  function ConvertRawPortToRealPort(RawPort : DWORD) : DWORD;
  begin
    Result := (RawPort div 256) + (RawPort mod 256) * 256;
  end;

var
  IPHeader: TIPHeader;
  ICMPHeader: TICMPHeader;
  TCPHeader: TTCPHeader;
  UDPHeader: TUDPHeader;
  SrcPort, DestPort: Word;
  I, Octets, PartOctets: Integer;
  PacketType, DumpData, ExtendedInfo: String;
  Addr, A, B: TInAddr;
begin
  Inc(frmMain.TotalPacketCount);
  // ������ �� ������ IP ���������
  Move(Packet[0], IPHeader, IPHeaderSize);
  // ����� ����� ����� ������
  LogData := LOG_STR_0 +
    Format(LOG_STR_1, [frmMain.TotalPacketCount, IPHeader.iph_ttl]);
  SrcPort := 0;
  DestPort := 0;
  ExtendedInfo := '';
  // ���������� ��� ���������
  case IPHeader.iph_protocol of
    IPPROTO_ICMP: // ICMP
    begin
      PacketType := 'ICMP';
	    // ������ ICMP ���������
      Move(Packet[IPHeaderSize], ICMPHeader, ICMPHeaderSize);
	    // ������� ��� ������
      case ICMPHeader.IcmpCode of
        ICMP_ECHO: ExtendedInfo := Format(LOG_STR_6, ['Echo']);
        ICMP_ECHOREPLY: ExtendedInfo := Format(LOG_STR_6, ['Echo reply']);
      else
        ExtendedInfo := Format(LOG_STR_6, ['Unknown']);
      end;
    end;
    IPPROTO_TCP: // TCP
    begin
      PacketType := 'TCP';
	    // ������ ��� ���������
      Move(Packet[IPHeaderSize], TCPHeader, TCPHeaderSize);
	    // ������� ���� ����������� � ����������
      SrcPort := TCPHeader.sourcePort;
      DestPort := TCPHeader.destinationPort;
    end;
    IPPROTO_UDP: // UDP
    begin
      PacketType := 'UDP';
	    // ������ UDP ���������
      Move(Packet[IPHeaderSize], UDPHeader, UDPHeaderSize);
	    // ������� ���� ����������� � ����������
      SrcPort := UDPHeader.sourcePort;
      DestPort := UDPHeader.destinationPort;
    end;
  else
    PacketType := 'Unsupported (0x' + IntToHex(IPHeader.iph_protocol, 2) + ')';
  end;
  // ����� ������ ������
  LogData := LogData + Format(LOG_STR_2, [PacketSize, PacketType]);
  if ExtendedInfo <> '' then
    LogData := LogData + ExtendedInfo;

  // ����� IP ����� ����������� � ������
  Addr.S_addr := IPHeader.iph_src;
  LogData := LogData + Format(LOG_STR_3, [inet_ntoa(Addr), SrcPort]);
  // ����� IP ����� ���������� � ������
  Addr.S_addr := IPHeader.iph_dest;
  // Marco
  //LogData := LogData + Format(LOG_STR_4, [inet_ntoa(Addr), DestPort]) + LOG_STR_7;
  LogData := LogData + Format(LOG_STR_4, [inet_ntoa(Addr), DestPort]) +
  '(*' + IntToStr(ConvertRawPortToRealPort(SrcPort)) + '<->' + IntToStr(ConvertRawPortToRealPort(DestPort)) + '*) ' +
  LOG_STR_7;


  // ������� ���������� ������ �� ����� (������� �������������� �� ����, ��� ��� ������)
  // ���������� ���-�� ����� �����:
  //
  // ------------------------------ Packet dump -----------------------------------
  // 000000 45 00 00 4E D8 91 00 00 | 80 11 DB 3B C0 A8 02 82     E..N.......;....
  // 000010 C0 A8 02 FF 00 89 00 89 | 00 3A AC 6A 83 BD 01 10     .........:.j....
  // 000020 00 01 00 00 00 00 00 00 | 20 45 43 46 46 45 49 44     ........ ECFFEID
  // 000030 44 43 41 43 41 43 41 43 | 41 43 41 43 41 43 41 43     DCACACACACACACAC
  // 000040 41 43 41 43 41 43 41 43 | 41 00 00 20 00 01           ACACACACA.. ..
  I := 0;
  Octets := 0;
  PartOctets := 0;
  while I < PacketSize do
  begin
    case PartOctets of
      0: LogData := LogData + Format('%.6d ', [Octets]);
      9: LogData := LogData + '| ';
      18:
      begin
        Inc(Octets, 10);
        PartOctets := -1;
        LogData := LogData + '    ' + DumpData + sLineBreak;
        DumpData := '';
      end;
    else
      begin
        LogData := LogData + Format('%s ', [IntToHex(Packet[I], 2)]);
        if Packet[I] in [$19..$7F] then
          DumpData := DumpData + Chr(Packet[I])
        else
          DumpData := DumpData + '.';
        Inc(I);
      end;
    end;
    Inc(PartOctets);
  end;
  if PartOctets <> 0 then
  begin
    PartOctets := (16 - Length(DumpData)) * 3;
    if PartOctets >= 24 then Inc(PartOctets, 2);
    Inc(PartOctets, 4);
    LogData := LogData + StringOfChar(' ', PartOctets) +
      DumpData + sLineBreak + sLineBreak
  end
  else
    LogData := LogData + sLineBreak + sLineBreak;
  // ������� ��� ��� ����������� � Memo
  Synchronize(ShowPacket);
end;

procedure TSnifferThread.ShowPacket;
begin
  {
  //Original
  if LogData <> '' then
  begin
    frmMain.memReport.Lines.BeginUpdate;
    frmMain.memReport.Text :=
      frmMain.memReport.Text + sLineBreak + LogData;
    SendMessage(frmMain.memReport.Handle, WM_VSCROLL, SB_BOTTOM, 0);
    frmMain.memReport.Lines.EndUpdate;
  end;
  }
  //
  {
  //Memo
  if LogData <> '' then
  begin
    frmMain.RichEdit1.Lines.BeginUpdate;
    frmMain.RichEdit1.Text:=frmMain.RichEdit1.Text+sLineBreak+LogData;
    SendMessage(frmMain.RichEdit1.Handle, WM_VSCROLL, SB_BOTTOM, 0);
    frmMain.RichEdit1.Lines.EndUpdate;
  end;
  }
  //Labels
  (*** Evitar desenho em tempo de execu��o
  frmMain.LaCicloPacotes.Caption := IntToStr(frmMain.FCicloPacks);
  frmMain.LaCicloBytes.Caption := IntToStr(frmMain.FCicloBytes);
  //
  frmMain.LaUsoApPacotes.Caption := IntToStr(frmMain.FUsoApPacks);
  frmMain.LaUsoApBytes.Caption := IntToStr(frmMain.FUsoApBytes);
  ***)
  //
end;

{ TfrmMain }

procedure TfrmMain.EdCicloMinPacksRedefinido(Sender: TObject);
begin
  RedefineMinTrafficCiclo();
end;

procedure TfrmMain.EdCicloTimeSegRedefinido(Sender: TObject);
begin
  TmCiclo.Interval := EdCicloTimeSeg.ValueVariant * 1000;
end;

procedure TfrmMain.EdIntervalPacksRedefinido(Sender: TObject);
begin
  SetIntervalPacotes();
end;

procedure TfrmMain.EdMaxSecPcksRedefinido(Sender: TObject);
begin
  FMaxSecPcks := EdMaxSecPcks.ValueVariant;
end;

procedure TfrmMain.EdMaxTempoOciosoSegRedefinido(Sender: TObject);
begin
  RedefineMaxTempoOcioso();
end;

procedure TfrmMain.EdPortaRedefinido(Sender: TObject);
begin
  RedefinePorta();
end;

function TfrmMain.EhIPv4deRedePrivadaEAtivo(IPv4: String): Boolean;
var
  OctetosIP: TOctetosIPv4;
begin
  Result := False;
  if SeparaOctetosIPv4(IPv4, OctetosIP) then
  begin
    //ShowMessage(IntToStr(OctetosIP[0]) + '.' + IntToStr(OctetosIP[1]) + '.' + IntToStr(OctetosIP[2]) + '.' + IntToStr(OctetosIP[3]));
    if OctetosIP[0] = 10 then
      Result := True
    else
    if (OctetosIP[0] = 192) and (OctetosIP[1] = 168) then
      Result := True
    else
    if (OctetosIP[0] = 172) then
    begin
      if (OctetosIP[1] >= 16) then
        if (OctetosIP[1] <= 31) then
          Result := True;
    end else
    if (OctetosIP[0] = 169) and (OctetosIP[1] = 254) then
      Result := True
    else  ; // Ter� mais no futuro?
  end;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I: Integer;
begin
  // Marco - Evitar erro na Thread
  if Length(FArrSnifferThread) > 0 then
  begin
    for I := 0 to Length(FArrSnifferThread) - 1 do
    begin
      if FArrSnifferThread[I] <> nil then
      begin
        FArrSnifferThread[I].Terminate;
        FArrSnifferThread[I] := nil;
        //btnStartStop.Caption := '...';
      end;
    end;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FMaxCout   := 6;
  FCountSecs := FMaxCout;
  //
  FShowed           := True;
  FAtivou           := False;
  FCicloPacks       := 0;
  FCicloBytes       := 0;
  FUsoApPacks       := 0;
  FUsoAPBytes       := 0;
  FTickOnCicloOcios := 0;
  FTickOnCicloAtivo := 0;
  FTickInicioOcioso := 0;
  FTickPassedOcioso := 0;
  FTickPassedAtivo  := 0;
  FTickInicioAtivo  := GetTickCount;
  //
  TotalPacketCount := 0;
  //Original ReadLanInterfaces;
  //Original
  FMaxSecPcks := EdMaxSecPcks.ValueVariant;
  //
  RedefinePorta();
  RedefineMinTrafficCiclo();
  RedefineMaxTempoOcioso();
  //
  AtualizaConexoesDeRedeAtivas();
  SetIntervalPacotes();
end;

function TfrmMain.ObtemIP(Retorno: Integer): String;
type
  TaPInAddr = array [0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe : PHostEnt;
  pptr : PaPInAddr;
  Buffer : array [0..63] of AnsiChar;
  I : Integer;
  GInitData : TWSADATA;
  Res: String;
begin
  WSAStartup($101, GInitData);
  Result := '';
  GetHostName(Buffer, SizeOf(Buffer));
  phe := GetHostByName(buffer);
  if phe = nil then
    Exit;
  pptr := PaPInAddr(Phe^.h_addr_list);
  I := 0;
  while pptr^[I] <> nil do
  begin
    Res := StrPas(inet_ntoa(pptr^[I]^));
    Inc(I);
  end;
  WSACleanup;
  //
  case Retorno of
      0: Result := StrPas(Phe^.h_name);
      1: Result := Res;
    else Result := '';
  end;
end;

procedure TfrmMain.ObtemIPs(Retorno: Integer; Lista: TStrings);
type
  TaPInAddr = array [0..10] of PInAddr;
  PaPInAddr = ^TaPInAddr;
var
  phe : PHostEnt;
  pptr : PaPInAddr;
  Buffer : array [0..63] of AnsiChar;
  I : Integer;
  GInitData : TWSADATA;
  IP, NomeComputador,IP_e_Nome: String;
begin
{
Rede privada
Origem: Wikip�dia, a enciclop�dia livre.

Na arquitetura para endere�amentos da Internet, uma rede privada (private
  network) � uma rede que usa o espa�o privado de endere�os IP, seguindo os
  padr�es estabelecidos pela RFC 1918 para redes IPv4 e RFC 4193 para IPv6.
Estes endere�os s�o associados aos dispositivos que precisam se comunicar com
outros dispositivos em uma rede privada (que n�o faz parte da Internet).
  As redes privadas s�o comuns nos escrit�rios (LAN), pois n�o h� a necessidade
de que todos os computadores de uma organiza��o possuam um IP universalmente
endere��vel.
  Outra raz�o que torna importante os IPs privados, � o n�mero limitado de IPs
p�blicos. O IPv6 foi criado para resolver este �ltimo problema.
  Os Roteadores s�o configurados para descartar qualquer tr�fego que use um IP
privado. Este isolamento garante que uma rede privada tenha uma maior seguran�a
pois n�o � poss�vel, em geral, ao mundo externo criar uma conex�o direta a uma
m�quina que use um IP privado.
  Como as conex�es n�o podem ser feitas entre diferentes redes privadas por meio
da internet, diferentes organiza��es podem usar a mesma faixa de IP sem que haja
conflitos (ou seja, que uma comunica��o chegue acidentalmente a um elemento que
n�o deveria).
  Se um dispositivo em uma rede privada deve se comunicar com outras redes, �
necess�rio que haja um "gateway" para garantir que a rede externa seja vista com
um endere�o que seja "real" (ou p�blico) de maneira que o roteador permita a
comunica��o. Normalmente este gateway ser� um service NAT (��Network address
translation��) ou um Servidor Proxy.
  Isto, por�m, pode causar problemas se a organiza��o tentar conectar redes que
usem os mesmos endere�os privados.


  Os endere�os atualmente reservados a redes privadas na internet s�o:
================================================================================================================================|
Nome         | Faixa de endere�os IP         | N�mero de IPs | Descri��o     | Maior bloco    |Refer�ncia                       |
--------------------------------------------------------------------------------------------------------------------------------|
8-bit block  | 10.0.0.0 � 10.255.255.255     | 16,777,216    | Uma classe A  | 10.0.0.0/8     |\                                |
12-bit block | 172.16.0.0 � 172.31.255.255   | 1,048,576     | 16 classes B  | 172.16.0.0/12  | | RFC 1597 (obsoleto), RFC 1918 |                             |
16-bit block | 192.168.0.0 � 192.168.255.255 | 65,536        | 256 classes C | 192.168.0.0/16 |/                                |
16-bit block | 169.254.0.0 � 169.254.255.255 | 65,536        | Uma classe B  | 169.254.0.0/16 |RFC 3330, RFC 3927               |
================================================================================================================================|

Zeroconf:
  Um segundo conjunto de redes privadas � o link-local address range definida em
RFCs 3330 e 3927.
  A finalidade destes RFCs � fornecer um endere�o IP (e, consequentemente, a
conectividade entre as redes) sem usar um servidor DHCP e sem ter de configurar
a rede manualmente.
  A subrede 169.254/16 foi reservada para esta finalidade. Dentro desta faixa,
as subredes 169.254.0/24 e 169.254.255/24 foram reservadas para uso futuro.
  Se uma rede n�o puder obter um endere�o por meio de DHCP, um endere�o de
  169.254.1.0 a 169.254.254.255 ser� atribu�do aleatoriamente.192.168.1 (*Fim*)
}
  Lista.Clear;
  WSAStartup($101, GInitData);
  GetHostName(Buffer, SizeOf(Buffer));
  phe := GetHostByName(buffer);
  if phe = nil then
    Exit;
  pptr := PaPInAddr(Phe^.h_addr_list);
  NomeComputador := StrPas(Phe^.h_name);
  I := 0;
  while pptr^[I] <> nil do
  begin
    IP   := StrPas(inet_ntoa(pptr^[I]^));
    //
    if EhIPv4deRedePrivadaEAtivo(IP) then
      Lista.Add(IP + ' ' + NomeComputador);
    //
    Inc(I);
  end;
  WSACleanup;
  //
end;

function TfrmMain.ObtemWideChar(s: String): PWideChar;
var
  I, N, K, C, T: Integer;
  Chars: array of Char;
begin
  T := Length(s);
  K := (T + 1) div 2;
  SetLength(Chars, K + 1);
  for I := 0 to K - 1 do
  begin
    C := 0;
    N := (I * 2) + 1;
    if N <= T then
      C := Ord(s[N+1])  * 256;
    C := C + Ord(s[N]);
    Chars[I] := Char(C);
    Memo1.Lines.Add(IntToStr(C));
  end;
  Chars[K] := Char(32);
  Result := PWideChar(Chars);
end;


{ original
// ������ ��� IP ������ �� ���� ��������������
// � ������� ������� �����������
procedure TfrmMain.ReadLanInterfaces;
var
  InterfaceInfo,
  TmpPointer: PIP_ADAPTER_INFO;
  IP: PIP_ADDR_STRING;
  Len: ULONG;
  // Marco
  srcPtr : PChar;
  IP_HumanReadable: String;
begin
  // ������� ������� ������ ��� ���������?
  if GetAdaptersInfo(nil, Len) = ERROR_BUFFER_OVERFLOW then
  begin
    // ����� ������ ���-��
    GetMem(InterfaceInfo, Len);
    try
      // ���������� �������
      if GetAdaptersInfo(InterfaceInfo, Len) = ERROR_SUCCESS then
      begin
        // ����������� ��� ������� ����������
        TmpPointer := InterfaceInfo;
        repeat
          // ����������� ��� IP ������ ������� ����������
          IP := @TmpPointer.IpAddressList;
          repeat
            cbInterfaces.Items.Add(Format('%s - [%s]',
              [IP^.IpAddress.S, TmpPointer.Description]));
            IP := IP.Next;
          until IP = nil;
          TmpPointer := TmpPointer.Next;
        until TmpPointer = nil;
      end;
    finally
      // ����������� ������� ������
      FreeMem(InterfaceInfo);
    end;
  end;
  // ������� - ����� �� �� ���������� ������ ���������?
  if cbInterfaces.Items.Count = 0 then
  begin
    RichEdit1.Text := '������� ���������� �� ����������.' + sLineBreak +
      '����������� ������ ��������� �� ��������.';
    btnStartStop.Enabled := False;
    Exit;
  end
  else
    cbInterfaces.ItemIndex := 0;
end;
}

// original
// Procura e lista adaptadores de rede
// Nesta procedure podemos obter o nome do adaptador!
procedure TfrmMain.ReadLanInterfaces_A();
var
  InterfaceInfo,
  TmpPointer: PIP_ADAPTER_INFO_A;
  IP: PIP_ADDR_STRING_A;
  Len: ULONG;
  // Marco
  srcPtr : PChar;
  //EsteIP,
  IP_HumanReadable: String;
begin
  //FIndex := -1;
  // Testa se h� adaptadores v�lidos
  cbInterfaces_A.Items.Clear;
  InterfaceInfo := nil;
  if GetAdaptersInfo(InterfaceInfo, Len) = ERROR_BUFFER_OVERFLOW then
  begin
    // Aloca mem�ria para listar adaptadores
    GetMem(InterfaceInfo, Len);
    try
      // Obtem lista de adaptadores
      if GetAdaptersInfo(InterfaceInfo, Len) = ERROR_SUCCESS then
      begin
        // Obtem adaptador v�lido
        TmpPointer := InterfaceInfo;
        repeat
          // Obtem dados (Nome e IP) da conex�o do adaptador
          IP := @TmpPointer.IpAddressList;
          repeat
            if  EhIPv4deRedePrivadaEAtivo(IP^.IpAddress.S) then
            begin
              //cbInterfaces_A.Items.Add(Format('%s - [%s]',
                //[IP^.IpAddress.S, TmpPointer.Description]));
              cbInterfaces_A.Items.Add(IP^.IpAddress.S + ' ' + TmpPointer.Description);
            end;
            IP := IP.Next;
          until IP = nil;
          TmpPointer := TmpPointer.Next;
        until TmpPointer = nil;
      end;
    finally
      // libera a mem�ria alocada
      FreeMem(InterfaceInfo);
    end;
  end;
  // Informa se n�o encontrou nenhum adaptador
  if cbInterfaces_A.Items.Count = 0 then
  begin
    RichEdit1.Text := 'Nenhuma conex�o de rede ativa foi localizada!' +
    sLineBreak + 'N�o h� como escutar trafego de rede!';
    btnStartStop.Enabled := False;
    Exit;
  end
  else
    cbInterfaces_A.ItemIndex := 0;
end;

procedure TfrmMain.RedefineMaxTempoOcioso();
begin
  FMaxTempoOciosoSeg := EdMaxTempoOciosoSeg.ValueVariant * 1000;
  TmOcioso.Interval := FMaxTempoOciosoSeg;
end;

procedure TfrmMain.RedefineMinTrafficCiclo();
begin
  FCicloMinPacks := EdCicloMinPacks.ValueVariant;
  FCicloMinBytes := EdCicloMinBytes.ValueVariant;
  //FUsoApMinPacks :=
  //FUsoApMinBytes :=
end;

procedure TfrmMain.RedefinePorta();
begin
  FUserPortToListen := EdPorta.ValueVariant;
  FTablPortToListen := ConvertRawPortToRealPort(FUserPortToListen);
  (*** Evitar desenho em tempo de execu��o
  LaTablPort.Caption := Geral.FF0(FTablPortToListen);
  ***)
end;

function TfrmMain.SeparaOctetosIPv4(const IPv4: String;
  var Octetos: TOctetosIPv4): Boolean;
var
  p, I: Integer;
  s, o: String;
begin
  for I := 0 to 3 do
    Octetos[I] := 0;
  Result := False;
  if (IPv4 = '') or (IPV4 = '0.0.0.0') then Exit;
  //
  s := IPv4;
  //
  for I := 0 to 3 do
  begin
    p := pos('.', s);
    if p > 0 then
    begin
      o := Copy(s, 1, p - 1);
      s := Copy(s, p + 1);
    end else
      o := s;
    if o <> '' then
      Octetos[I] := StrToInt(o);
  end;
  Result := True;
end;

procedure TfrmMain.SetIntervalPacotes();
begin
  TmHasPacks.Enabled := False;
  TmHasPacks.Interval := EdIntervalPacks.ValueVariant * 1000;
  TmHasPacks.Enabled := True;
end;

function TfrmMain.TickDiff(StartTick, EndTick: DWORD): DWORD;
begin
  if EndTick >= StartTick then
    Result := EndTick - StartTick
  else
    Result := High(NativeUInt) - StartTick + EndTick;
end;

function TfrmMain.TicksSince(Tick: DWORD): DWORD;
begin
  Result := TickDiff(Tick, GetTickCount);
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
begin
  if TmOcioso.Enabled then
  begin
    FTickPassedOcioso := TicksSince(FTickInicioOcioso);
    (*** Evitar desenho em tempo de execu��o
    LaTempoOcioso.Caption := FormatDateTime('hh:nn:ss', FTickPassedOcioso / (24*60*60*1000));
    if LaTempoAtivo.Caption <> '0:00:00' then
      LaTempoAtivo.Caption := '0:00:00';
    ***)
  end else
  begin
    FTickPassedAtivo := TicksSince(FTickInicioAtivo);
    (*** Evitar desenho em tempo de execu��o
    LaTempoAtivo.Caption := FormatDateTime('hh:nn:ss', FTickPassedAtivo / (24*60*60*1000));
    if LaTempoOcioso.Caption <> '0:00:00' then
      LaTempoOcioso.Caption := '0:00:00';
    ***)
  end;
end;

procedure TfrmMain.Timer2Timer(Sender: TObject);
begin
  if frmMain.TmOcioso.Enabled then
  begin
    FTickOnCicloAtivo := 0;
    FTickOnCicloOcios := frmMain.TicksSince(frmMain.FTickInicioOcioso);
  end else
  begin
    FTickOnCicloAtivo := frmMain.TicksSince(frmMain.FTickInicioAtivo);
    FTickOnCicloOcios := 0;
  end;
  if FCountSecs >= FMaxCout then //  := 6;
  begin
    FCountSecs := 1;
(*
    Geral.WriteAppKeyCU('CicloPacks', 'DoleOut\Voyer\', FCicloPacks, ktInteger);
    Geral.WriteAppKeyCU('CicloBytes', 'DoleOut\Voyer\', FCicloBytes, ktInteger);
    Geral.WriteAppKeyCU('TickOnCicloAtivo', 'DoleOut\Voyer\', FTickOnCicloAtivo, ktInteger);
    Geral.WriteAppKeyCU('TickOnCicloOcios', 'DoleOut\Voyer\', FTickOnCicloOcios, ktInteger);
    //
*)
    Geral.WriteAppKeyLM('CicloPacks', 'DoleOut\Voyer\', FCicloPacks, ktInteger);
    Geral.WriteAppKeyLM('CicloBytes', 'DoleOut\Voyer\', FCicloBytes, ktInteger);
    Geral.WriteAppKeyLM('UsoApPacks', 'DoleOut\Voyer\', FUsoApPacks, ktInteger);
    Geral.WriteAppKeyLM('UsoApBytes', 'DoleOut\Voyer\', FUsoApBytes, ktInteger);
    Geral.WriteAppKeyLM('TickOnCicloAtivo', 'DoleOut\Voyer\', FTickOnCicloAtivo, ktInteger);
    Geral.WriteAppKeyLM('TickOnCicloOcios', 'DoleOut\Voyer\', FTickOnCicloOcios, ktInteger);
  end else
    FCountSecs := FCountSecs + 1;
end;

procedure TfrmMain.TmCicloTimer(Sender: TObject);
var
  CicloPacks, CicloBytes: Integer;
begin
  CicloPacks := FCicloPacks;
  FCicloPacks := FCicloPacks - CicloPacks;
  //
  CicloBytes := FCicloBytes;
  FCicloBytes := FCicloBytes - CicloBytes;
  //
  frmMain.RichEdit1.Lines.BeginUpdate;
  frmMain.RichEdit1.Text := frmMain.RichEdit1.Text + sLineBreak +
    FormatDateTime('yy/mm/dd hh:nn:ss', Now()) +
    ' | Contagem total de pacotes: ' + Geral.FF0(TotalPacketCount) +
    ' | Porta: ' + Geral.FF0(FUserPortToListen) +
    ' | Pacotes: ' + Geral.FF0(CicloPacks) +
    ' | Bytes ' + Geral.FF0(CicloBytes) + ' | ';
  SendMessage(frmMain.RichEdit1.Handle, WM_VSCROLL, SB_BOTTOM, 0);
  frmMain.RichEdit1.Lines.EndUpdate;
  if (CicloPacks < FCicloMinPacks) or (CicloBytes < FCicloMinBytes) then
  begin
    if TmOcioso.Enabled = False then
    begin
      FTickInicioOcioso := GetTickCount;
      TmOcioso.Enabled := True;
    end;
  end else
  begin
    if TmOcioso.Enabled = True then
    begin
      FTickInicioAtivo := GetTickCount;
      TmOcioso.Enabled := False;
    end;
  end;
  (*** Evitar desenho em tempo de execu��o
  LaCicloPacotes.Caption := IntToStr(frmMain.FCicloPacks);
  LaCicloBytes.Caption := IntToStr(frmMain.FCicloBytes);
  LaUsoApPacotes.Caption := IntToStr(frmMain.FUsoAPPacks);
  LaUsoApBytes.Caption := IntToStr(frmMain.FUsoApBytes);
  ***)
  //
(*
  Geral.WriteAppKeyCU('Packs', 'DoleOut\Voyer\', CicloPacks, ktInteger);
  Geral.WriteAppKeyCU('Bytes', 'DoleOut\Voyer\', CicloBytes, ktInteger);
  Geral.WriteAppKeyCU('TickOnCicloAtivo', 'DoleOut\Voyer\', frmMain.FTickOnCicloAtivo, ktInteger);
  Geral.WriteAppKeyCU('TickOnCicloOcios', 'DoleOut\Voyer\', frmMain.FTickOnCicloOcios, ktInteger);
  //
  Geral.WriteAppKeyLM('Packs', 'DoleOut\Voyer\', CicloPacks, ktInteger);
  Geral.WriteAppKeyLM('Bytes', 'DoleOut\Voyer\', CicloBytes, ktInteger);
  Geral.WriteAppKeyLM('TickOnCicloAtivo', 'DoleOut\Voyer\', frmMain.FTickOnCicloAtivo, ktInteger);
  Geral.WriteAppKeyLM('TickOnCicloOcios', 'DoleOut\Voyer\', frmMain.FTickOnCicloOcios, ktInteger);
*)
end;

procedure TfrmMain.TmHasPacksTimer(Sender: TObject);
var
  I: Integer;
  TickCount: UInt64;
begin
  TickCount := GetTickCount64;
  //
  for I := 0 to Length(FArrSnifferThread) - 1 do
  begin
    // se a conex�o n�o est� enviando pacotes ser� eliminada!
    if (TickCount - FArrSnifferThread[I].TickCount) > (FMaxSecPcks * 1000) then
    begin
      if FArrSnifferThread[I] <> nil then
      begin
        FArrSnifferThread[I].Terminate;
        FArrSnifferThread[I] := nil;
      end
    end;
  end;
  // Procura por novas conex�es
  AtualizaConexoesDeRedeAtivas();
end;

procedure TfrmMain.TmOciosoTimer(Sender: TObject);
begin
  frmMain.RichEdit1.Lines.BeginUpdate;
  frmMain.RichEdit1.Text := frmMain.RichEdit1.Text + sLineBreak + '==================================================================================';
  frmMain.RichEdit1.Text := frmMain.RichEdit1.Text + sLineBreak + '| Hora de perguntar se vai continuar usando: ' + FormatDateTime('hh:nn:ss', Now()) + '             |';
  frmMain.RichEdit1.Text := frmMain.RichEdit1.Text + sLineBreak + '==================================================================================';
  //
  SendMessage(frmMain.RichEdit1.Handle, WM_VSCROLL, SB_BOTTOM, 0);
  frmMain.RichEdit1.Lines.EndUpdate;
end;


{
procedure TfrmMain.ReadLanInterfaces_W
var
  InterfaceInfo,
  TmpPointer: PIP_ADAPTER_INFO_W;
  IP: PIP_ADDR_STRING_W;
  Len: ULONG;
  // Marco
  srcPtr : PChar;
  IP_HumanReadable: String;
begin
  // ������� ������� ������ ��� ���������?
  //if GetAdaptersInfo(nil, Len) = ERROR_BUFFER_OVERFLOW then
  begin
    // ����� ������ ���-��
    GetMem(InterfaceInfo, Len);
    try
      // ���������� �������
      if GetAdaptersInfo(InterfaceInfo, Len) = ERROR_SUCCESS then
      begin
        // ����������� ��� ������� ����������
        TmpPointer := InterfaceInfo;
        repeat
          // ����������� ��� IP ������ ������� ����������
          IP := @TmpPointer.IpAddressList;
          repeat
            cbInterfaces_W.Items.Add(Format('%s - [%s]',
              [IP^.IpAddress.S, TmpPointer.Description]));
            IP := IP.Next;
          until IP = nil;
          TmpPointer := TmpPointer.Next;
        until TmpPointer = nil;
      end;
    finally
      // ����������� ������� ������
      FreeMem(InterfaceInfo);
    end;
  end;
  // ������� - ����� �� �� ���������� ������ ���������?
  if cbInterfaces_W.Items.Count = 0 then
  begin
    RichEdit1.Text := '������� ���������� �� ����������.' + sLineBreak +
      '����������� ������ ��������� �� ��������.';
    btnStartStop.Enabled := False;
    Exit;
  end
  else
    cbInterfaces_W.ItemIndex := 0;
end;
}

// ������ ��������� ������
procedure TfrmMain.AtualizaConexoesDeRedeAtivas();
var
  I, J, K: Integer;
  Achou: Boolean;
  Texto, Nome, IP: String;
  IP_Char: PWideChar;
begin
  ObtemIPs(1, ListBox1.Items);
  FMeuIP := ObtemIP(1);
  FMeuIP_Char := ObtemWideChar(FMeuIP);
  // Lista todas Conex�es ativas
  ReadLanInterfaces_A;
  //ReadLanInterfaces_W;
  // Cria threads para conex�es novas, caso haja.
  if cbInterfaces_A.Items.Count > 0 then
  begin
    // analiza cada conex�o privada ativa
    for I := 0 to cbInterfaces_A.Items.Count - 1 do
    begin
      Texto := cbInterfaces_A.Items[I];
      Nome := Copy(Texto, Pos(' ', Texto) + 1);
      IP   := Copy(Texto, 1, Pos(' ', Texto)-1);
      K := -1;
      Achou := False;
      // verifica se j� n�o h� thread para ela
      for J := 0 to Length(FArrSnifferThread) - 1 do
      begin
        if FArrSnifferThread <> nil then
        begin
          if FArrSnifferThread[J].Nome = Nome then
          begin
            Achou := True;
            Break;
          end;
        end else
        // Aproveita item vazio do array se precisar
        if K = -1 then
          K := 0;
      end;
      // Se n�o achou, cria thread para a conex�o
      if Achou = False then
      begin
        if K = -1 then
        begin
          K := Length(FArrSnifferThread);
          SetLength(FArrSnifferThread, K + 1);
        end;
        FArrSnifferThread[K] := TSnifferThread.Create(True);
        FArrSnifferThread[K].TickCount := GetTickCount64;
        IP_Char := ObtemWideChar(IP);
        FArrSnifferThread[K].Host := IP_Char;
        FArrSnifferThread[K].IPv4 := IP;
        FArrSnifferThread[K].Nome := Nome;
        FArrSnifferThread[K].FreeOnTerminate := True;
        //
        FArrSnifferThread[K].Resume;
      end;
    end;
{
    cbInterfaces_AItemIndex := FIndex;
    if FIndex > -1 then
      btnStartStopClick(Self);
}
  end;
end;

procedure TfrmMain.btnStartStopClick(Sender: TObject);
{
var
  C : array of Char;
  AC : String;
  I: Integer;
  IP_Char: PWideChar;
  IP, Nome: String;
}
begin
{
  if FSnifferThread <> nil then
  begin
    FSnifferThread.Terminate;
    FSnifferThread := nil;
    btnStartStop.Caption := 'Start';
  end
  else
  begin
    FSnifferThread := TSnifferThread.Create(True);
    Nome := Copy(cbInterfaces_A.Text, Pos(' ', cbInterfaces_A.Text) + 1);
    IP := Copy(cbInterfaces_A.Text, 1, Pos(' ', cbInterfaces_A.Text)-1);
    IP_Char := ObtemWideChar(IP);
    FSnifferThread.Host := IP_Char;
    FSnifferThread.IPv4 := IP;
    FSnifferThread.Nome := Nome;


    FSnifferThread.FreeOnTerminate := True;
    FSnifferThread.Resume;
    btnStartStop.Caption := 'Stop';
  end;
}
  AtualizaConexoesDeRedeAtivas();
end;

procedure TfrmMain.BtOcultarClick(Sender: TObject);
begin
  Hide;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
var
  ATaskName, AFileName, AUserAccount, AUserPassword: string;
begin
  ATaskName     := 'Voyez as admin';
  AFileName     := Application.ExeName;
  AUserAccount  := 'angeloni';
  AUserPassword := 'eletro26';
  //
  DmkPF.ScheduleRunAtStartup(ATaskName, AFileName, AUserAccount, AUserPassword);
end;

function TfrmMain.ConvertRawPortToRealPort(RawPort: DWORD): DWORD;
begin
  Result := (RawPort div 256) + (RawPort mod 256) * 256;
end;

end.
