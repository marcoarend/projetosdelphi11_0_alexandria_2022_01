unit UnApp_Consts;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, mysqlDBTables, UnMyLinguas,
  UnInternalConsts, dmkGeral, UnDmkProcFunc,
  UnDmkEnums;

type
  TUnApp_Consts = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
  end;

const
  CO_DIR_APP_DATA  = CO_DIR_RAIZ_DMK + '\Voyez\Data\';
  CO_DBArqName     = CO_DIR_APP_DATA + 'TarefaUnica.json';
  CO_DBCriptArqName = CO_DIR_APP_DATA + 'TarefaUnica.dmkafr';
  //
var
  App_Constst: TUnApp_Consts;

implementation

end.
