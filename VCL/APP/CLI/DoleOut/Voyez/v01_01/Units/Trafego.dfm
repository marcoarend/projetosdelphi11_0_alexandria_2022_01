object FmTrafego: TFmTrafego
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'An'#225'lise de Tr'#225'fego de Dados'
  ClientHeight = 727
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 696
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 694
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel2: TPanel
        Left = 847
        Top = 0
        Width = 159
        Height = 694
        Align = alRight
        TabOrder = 0
        object Splitter2: TSplitter
          Left = 1
          Top = 97
          Width = 157
          Height = 3
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 122
          ExplicitWidth = 276
        end
        object ListBox1: TListBox
          Left = 1
          Top = 31
          Width = 157
          Height = 66
          Align = alTop
          ItemHeight = 13
          TabOrder = 0
        end
        object Panel4: TPanel
          Left = 1
          Top = 100
          Width = 157
          Height = 593
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label2: TLabel
            Left = 4
            Top = 4
            Width = 123
            Height = 13
            Caption = 'Atualizar conex'#245'es ativas:'
          end
          object Label4: TLabel
            Left = 4
            Top = 44
            Width = 133
            Height = 13
            Caption = 'Espera m'#225'xima por pacotes:'
          end
          object Label6: TLabel
            Left = 4
            Top = 84
            Width = 28
            Height = 13
            Caption = 'Porta:'
          end
          object LaTablPort: TLabel
            Left = 68
            Top = 104
            Width = 9
            Height = 13
            Caption = '...'
          end
          object Label7: TLabel
            Left = 4
            Top = 124
            Width = 72
            Height = 13
            Caption = 'Ciclo de leitura:'
          end
          object Label8: TLabel
            Left = 4
            Top = 164
            Width = 122
            Height = 13
            Caption = 'M'#237'nimo pacotes por ciclo:'
          end
          object Label9: TLabel
            Left = 4
            Top = 204
            Width = 110
            Height = 13
            Caption = 'M'#237'nimo Bytes por ciclo:'
          end
          object Label10: TLabel
            Left = 4
            Top = 284
            Width = 118
            Height = 13
            Caption = 'Tempo m'#225'ximo "ocioso":'
          end
          object Label13: TLabel
            Left = 88
            Top = 22
            Width = 49
            Height = 13
            Caption = 'segundos.'
          end
          object Label14: TLabel
            Left = 88
            Top = 62
            Width = 49
            Height = 13
            Caption = 'segundos.'
          end
          object Label15: TLabel
            Left = 88
            Top = 142
            Width = 49
            Height = 13
            Caption = 'segundos.'
          end
          object Label16: TLabel
            Left = 88
            Top = 302
            Width = 49
            Height = 13
            Caption = 'segundos.'
          end
          object Label18: TLabel
            Left = 4
            Top = 244
            Width = 121
            Height = 13
            Caption = 'M'#237'nimo Bytes por pacote:'
          end
          object EdIntervalPacks: TdmkEdit
            Left = 4
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '90'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 90
            ValWarn = False
            OnRedefinido = EdIntervalPacksRedefinido
          end
          object EdMaxSecPcks: TdmkEdit
            Left = 4
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '90'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 90
            ValWarn = False
            OnRedefinido = EdMaxSecPcksRedefinido
          end
          object EdPorta: TdmkEdit
            Left = 4
            Top = 100
            Width = 56
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '491'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 491
            ValWarn = False
            OnRedefinido = EdPortaRedefinido
          end
          object EdCicloTimeSeg: TdmkEdit
            Left = 4
            Top = 140
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '60'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 60
            ValWarn = False
            OnRedefinido = EdCicloTimeSegRedefinido
          end
          object EdCicloMinPacks: TdmkEdit
            Left = 4
            Top = 180
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '50'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 50
            ValWarn = False
            OnRedefinido = EdCicloMinPacksRedefinido
          end
          object EdCicloMinBytes: TdmkEdit
            Left = 4
            Top = 220
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '10000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 10000
            ValWarn = False
            OnRedefinido = EdCicloTimeSegRedefinido
          end
          object EdMaxTempoOciosoSeg: TdmkEdit
            Left = 4
            Top = 300
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '300'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 300
            ValWarn = False
            OnRedefinido = EdMaxTempoOciosoSegRedefinido
          end
          object GroupBox1: TGroupBox
            Left = 4
            Top = 324
            Width = 149
            Height = 113
            Caption = ' Totais:'
            TabOrder = 7
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 145
              Height = 96
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 4
                Top = 4
                Width = 39
                Height = 13
                Caption = 'Pacotes'
              end
              object LaUsoApPacotes: TLabel
                Left = 4
                Top = 20
                Width = 8
                Height = 13
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label5: TLabel
                Left = 72
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Bytes'
              end
              object Label11: TLabel
                Left = 4
                Top = 36
                Width = 73
                Height = 13
                Caption = 'Tempo ocioso: '
              end
              object LaTempoOcioso: TLabel
                Left = 84
                Top = 36
                Width = 51
                Height = 13
                Caption = '00:00:00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object LaUsoApBytes: TLabel
                Left = 72
                Top = 20
                Width = 8
                Height = 13
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label12: TLabel
                Left = 4
                Top = 76
                Width = 65
                Height = 13
                Caption = 'Tempo ativo: '
              end
              object LaTempoAtivo: TLabel
                Left = 84
                Top = 76
                Width = 51
                Height = 13
                Caption = '00:00:00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label20: TLabel
                Left = 4
                Top = 56
                Width = 66
                Height = 13
                Caption = 'Tempo limbo: '
              end
              object LaTempoLimbo: TLabel
                Left = 84
                Top = 56
                Width = 51
                Height = 13
                Caption = '00:00:00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 4
            Top = 440
            Width = 149
            Height = 57
            Caption = ' Ciclo:'
            TabOrder = 8
            object Panel7: TPanel
              Left = 2
              Top = 15
              Width = 145
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label17: TLabel
                Left = 4
                Top = 4
                Width = 39
                Height = 13
                Caption = 'Pacotes'
              end
              object LaCicloPacotes: TLabel
                Left = 4
                Top = 20
                Width = 8
                Height = 13
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label19: TLabel
                Left = 72
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Bytes'
              end
              object LaCicloBytes: TLabel
                Left = 72
                Top = 20
                Width = 8
                Height = 13
                Caption = '0'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
            end
          end
          object EdPackMinBytes: TdmkEdit
            Left = 4
            Top = 260
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '64'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 64
            ValWarn = False
            OnRedefinido = EdPackMinBytesRedefinido
          end
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 157
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          TabOrder = 2
          object BtOcultar: TButton
            Left = 3
            Top = 3
            Width = 151
            Height = 24
            Margins.Left = 10
            Margins.Top = 5
            Margins.Right = 10
            Margins.Bottom = 0
            Align = alClient
            Caption = 'Oculta'
            TabOrder = 0
            OnClick = BtOcultarClick
          end
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 847
        Height = 694
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 491
          Width = 847
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitTop = 0
        end
        object Memo3: TMemo
          Left = 0
          Top = 0
          Width = 706
          Height = 491
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          ExplicitHeight = 649
        end
        object Memo1: TMemo
          Left = 706
          Top = 0
          Width = 141
          Height = 491
          Align = alRight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          ExplicitHeight = 649
        end
        object RichEdit1: TRichEdit
          Left = 0
          Top = 496
          Width = 847
          Height = 198
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          PlainText = True
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 2
          WordWrap = False
          Zoom = 100
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 696
    Width = 1008
    Height = 31
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 92
      Height = 13
      Caption = 'Conex'#245'es de rede: '
    end
    object cbInterfaces_A: TComboBox
      Left = 99
      Top = 6
      Width = 404
      Height = 21
      Style = csDropDownList
      TabOrder = 0
    end
    object BtLista: TButton
      Left = 508
      Top = 7
      Width = 57
      Height = 20
      Caption = 'Atualiza'
      TabOrder = 1
      OnClick = BtListaClick
    end
    object cbInterfaces_W: TComboBox
      Left = 568
      Top = 6
      Width = 429
      Height = 21
      Style = csDropDownList
      TabOrder = 2
    end
  end
  object TmHasPacks: TTimer
    Enabled = False
    OnTimer = TmHasPacksTimer
    Left = 92
    Top = 8
  end
  object TmInfo: TTimer
    OnTimer = TmInfoTimer
    Left = 164
    Top = 216
  end
  object TmSalvaHKLM: TTimer
    OnTimer = TmSalvaHKLMTimer
    Left = 508
    Top = 220
  end
  object TmCiclo: TTimer
    Interval = 60000
    OnTimer = TmCicloTimer
    Left = 164
    Top = 8
  end
  object TmOcioso: TTimer
    Enabled = False
    Interval = 300000
    OnTimer = TmOciosoTimer
    Left = 164
    Top = 56
  end
end
