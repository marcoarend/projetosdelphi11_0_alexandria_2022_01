unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, DCPcrypt2,
  DCPblockciphers, DCPtwofish, dmkLed, UnDmkEnums;

type
  TFmPrincipal = class(TForm)
    TmReadCiclo: TTimer;
    DCP_twofish1: TDCP_twofish;
    TmEsgotou: TTimer;
    Panel4: TPanel;
    Panel1: TPanel;
    BtExecuta: TButton;
    BtTarefa: TButton;
    Panel2: TPanel;
    PnTrafego: TPanel;
    Label8: TLabel;
    LaLMCicloPacks: TLabel;
    Label10: TLabel;
    LaLMCicloBytes: TLabel;
    Label16: TLabel;
    LaLMHoraAgora: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    LaLMUsoApPacks: TLabel;
    LaLMUsoAPBytes: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    LaUsoApIniBytes: TLabel;
    LaUsoApIniPacks: TLabel;
    Label5: TLabel;
    LaLMTimeInExcec: TLabel;
    TmInExec: TTimer;
    Label6: TLabel;
    LaTempoOciosoCiclos: TLabel;
    BtContinuar: TButton;
    Label3: TLabel;
    LaAtivoCiclos: TLabel;
    BtTrafego: TButton;
    Label4: TLabel;
    LaLimboCiclo: TLabel;
    LaLMCicloAtivo: TLabel;
    LaLMCicloOcios: TLabel;
    LaLMCicloLimbo: TLabel;
    BtEncerra: TButton;
    Panel3: TPanel;
    dmkLED1: TdmkLED;
    dmkLED2: TdmkLED;
    LaVersao: TLabel;
    procedure TmReadCicloTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtExecutaClick(Sender: TObject);
    procedure BtTarefaClick(Sender: TObject);
    procedure TmEsgotouTimer(Sender: TObject);
    procedure TmInExecTimer(Sender: TObject);
    procedure BtContinuarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtTrafegoClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
  private
    { Private declarations }
    FCUMaxCout, FCUCountSecs,
    FCUTickOnCicloAtivo, FCUTickOnCicloOcios, FCUTempoAtivo, FCUTempoOcios: Integer;
    FLMMaxCout, FLMCountSecs,
    FLMTempoAtivo, FLMTempoOcios,
    FSecToKill: Integer;
    //FShowed,
    FAtivou, FEmPergunta: Boolean;
    //
    procedure DefineDadosIniciais();
    //procedure DefineTemporizadorKill();
  public
    { Public declarations }
    FBtAtivado: Boolean;
    FExeLehDadosRede: String;
    //
    //FSTUserName, FSTUserPwd
    FNome, FPasta, FExecutavel, FParametros: String;
    FMonitSohProprioProces, (*FParaEtapaOnDeactive,*) FExecDoleAsAdmin,
    FExecTargetAsAdmin, FLogInMemo,
    FExeOnIni, FDesabKillSecs, FEncerraDoleJunto: Boolean;
    FSegWaitOnOSReinit, FSegWaitScanMonit: Integer;
    FHWNDShow, FEstadoJanela(*, FMaiorCodigo,*), FHProcesso,
    FIntervalPacks, FMaxSecPcks, FPorta, FCicloTimeSeg, FCicloMinPacks,
    FCicloMinBytes, FPackMinBytes, FMaxTempoOciosoSeg, FUsoApIniPacks, FUsoApIniBytes,
    FTempoPergKillSecs, FTmpOciPostergaSeg,
    //
    FTickOnExec_Ini, FTickOnExec_Run,
    FThisCiclPacks, FThisCiclBytes,
    FCiclosOcios,
    FCicloCount: Integer;
    //
    procedure ColocaEmPergunta();
    procedure RecarregaDadosParaExecucao();
    procedure IniciaMonitoramentoDoTrafego();
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses dmkGeral, UnDmkProcFunc, UnMyObjects, UnProjGroup_PF, UnProjGroup_Vars,
   UnMyVclEvents, UnPrjGruFiles_PF, magsubs1, Trafego, UnMyProcesses;

{$R *.dfm}

procedure TFmPrincipal.BtContinuarClick(Sender: TObject);
begin
  FEmPergunta := False;
  BtContinuar.Enabled := False;
  TmEsgotou.Enabled := False;
  LaTempoOciosoCiclos.Caption := '00:00:00';
  BtExecutaClick(Self);
  ProjGroup_PF.DefineTemporizadorKill(dmkLED1, dmkLED2, FTempoPergKillSecs,
    FDesabKillSecs);
end;

procedure TFmPrincipal.BtEncerraClick(Sender: TObject);
begin
  if MyProcesses.FindProcessesByName(FExecutavel) > - 1 then
  begin
    if not MyProcesses.KillProcess(MyProcesses.FindProcessesByName(FExecutavel)) then
      ShowMessage('Processo n�o finalizado.')
    else
      FmPrincipal.Close;
  end else
    FmPrincipal.Close;
end;

procedure TFmPrincipal.BtExecutaClick(Sender: TObject);
var
  Continua, Executou: Boolean;
  hProcess: Integer;
  ExeAlvo: String;
begin
  BtExecuta.Enabled := False;
  BtContinuar.Enabled := False;
  Executou := False;
  // ...Verifica e continua s� se existir leitura de trafego de dados na Rede:
  //if Continua then
  begin
    Continua := False;
    if FExecDoleAsAdmin = False then
    begin
      // Ver se o Voyez est� rodando...
      Continua := PrjGruFiles_PF.LocalizaProcesso(FExeLehDadosRede, hProcess);
      if not Continua then
      begin
        Continua := Geral.MB_Pergunta('N�o h� processo de obten��o de retaguarda em execu��o!' +
        sLineBreak + 'Aguarde mais alguns minutos para o in�cio autom�tico do processo!' +
        sLineBreak + 'Caso j� aguardou este tempo AVISE O ADMINISTRADOR DA REDE / TI' +
        sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES;
        //Halt(0);
      end;
    end else
      Continua := True;
  end;

  // Verifica se o execut�vel alvo existe..
  if Continua then
  begin
    ExeAlvo := IncludeTrailingPathDelimiter(FPasta) + FExecutavel;
    if not FileExists(ExeAlvo) then
    begin
      Continua := False;
      Geral.MB_Aviso('O Execut�vel alvo n�o foi localizado!' + sLineBreak +
      ExeAlvo);
      Exit;
    end;
  end;

  // ...Verifica se o execut�vel alvo j� n�o est� rodando!!!....
  if Continua then
  begin
    Continua := False;
    // Ver se o Voyez est� rodando...
    Continua := PrjGruFiles_PF.LocalizaProcesso(FExecutavel, hProcess);
    if not Continua then
    begin
      Executou := PrjGruFiles_PF.ExecutaAppAlvo(Self, FExecTargetAsAdmin, FPasta, FExecutavel,
      FParametros, FHWNDShow, FHProcesso);
      Continua := Executou;
    end else
    begin
      Geral.MB_Info('O processo "' + FNome + '" j� est� em execu��o!');
      Exit;
    end;
  end;

  // ...Ativar contador de tempo em execu��o
  if Continua then
  begin
    FThisCiclPacks := 0;
    FThisCiclBytes := 0;
    FCiclosOcios   := 0;
    FCicloCount    := -1;
    //
    FTickOnExec_Run := 0;
    FTickOnExec_Ini := GetTickCount();
    TmInExec.Enabled := True;
  end;

  if Continua then
  begin
    FUsoApIniPacks          := Geral.ReadAppKeyLM('UsoApPacks', 'DoleOut\Voyer\', ktInteger, 0);
    FUsoApIniBytes          := Geral.ReadAppKeyLM('UsoApBytes', 'DoleOut\Voyer\', ktInteger, 0);
    LaUsoApIniPacks.Caption := IntToStr(FUsoApIniPacks);
    LaUsoApIniBytes.Caption := IntToStr(FUsoApIniBytes);
  end;
  // ...Ativar o Timer de controle de fluxo de dados na porta!
  if Continua then
  begin
    if FExecDoleAsAdmin = False then
    begin
      TmReadCiclo.Enabled := True;
      BtTrafego.Enabled := True;
    end;
    FEmPergunta := False;
    BtContinuar.Enabled := False;
    TmEsgotou.Enabled := False;
    LaTempoOciosoCiclos.Caption := '00:00:00';
    ProjGroup_PF.DefineTemporizadorKill(dmkLED1, dmkLED2, FTempoPergKillSecs,
      FDesabKillSecs);
    //
    BtExecuta.Enabled := True;
    Application.Minimize;
  end;
end;

procedure TFmPrincipal.BtTarefaClick(Sender: TObject);
begin
  PrjGruFiles_PF.MostraFormTarefa_ParaBoss();
  RecarregaDadosParaExecucao();
  FmTrafego.RedefineDiretrizes();
end;

procedure TFmPrincipal.BtTrafegoClick(Sender: TObject);
var
  Continua: Boolean;
begin
  if PnTrafego.Visible = True then
  begin
    FmPrincipal.Height := 250;
    PnTrafego.Visible  := False;
  end else
  begin
    if VAR_SENHA_BOSS <> EmptyStr then
      Continua := ProjGroup_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
    else
      Continua := True;
    if Continua then
    begin
      PnTrafego.Visible := True;
      if FmPrincipal.Height < 380 then
        FmPrincipal.Height := 380;
      if FExecDoleAsAdmin then
      begin
        FmTrafego.Show;
        FmTrafego.FShow := True;
      end;
    end;
  end;
end;

procedure TFmPrincipal.ColocaEmPergunta();
begin
  if (not FEmPergunta) and
  (FTickOnExec_Run >= (FTmpOciPostergaSeg * 1000)) then
  begin
    if MyProcesses.FindProcessesByName(FExecutavel) > - 1 then
    begin
      FEmPergunta := True;
      //
      if FDesabKillSecs then
        FSecToKill := 0
      else
        FSecToKill := FTempoPergKillSecs;
      TmEsgotou.Enabled := True;
      BtContinuar.Enabled := True;
    end;
  end;
end;

{
procedure TFmPrincipal.DefineTemporizadorKill();
begin
  dmkLED1.BrightColor := $00794A3D; // A
  dmkLED1.DimColor    := $00472F2C; // Z
  dmkLED2.BrightColor := $00794A3D; // U
  dmkLED2.DimColor    := $00472F2C; // L
  //
  dmkLED1.Value := FTempoPergKillSecs div 10;
  dmkLED2.Value := FTempoPergKillSecs mod 10;
end;
}

procedure TFmPrincipal.DefineDadosIniciais();
begin
  FCicloCount  := -1;
  FEmPergunta  := False;
  //
  FCUMaxCout   := 6;
  FCUCountSecs := FCUMaxCout;
  //
  FLMMaxCout   := 6;
  FLMCountSecs := FCUMaxCout;
  //
  FUsoApIniPacks := 0;
  FUsoApIniBytes := 0;
  FTickOnExec_Ini := 0;
  FTickOnExec_Run := 0;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
var
  Continua: Boolean;
begin
  FmPrincipal.Left := Screen.Width - FmPrincipal.Width;
  FmPrincipal.Top := 0;
  //
  MyObjects.CorIniComponente;
  Continua := False;
  //
  // Verifica se o programa est� habilitado para a m�quina local:
  if FAtivou = False then
  begin
    FAtivou := True;
    if ProjGroup_PF.ObtemDadosOpcoesAppDeArquivo(
      ProjGroup_PF.ObtemNomeArquivoOpcoes(), DCP_twofish1) then
    begin
      if ProjGroup_PF.LiberaUsoVendaApp1(DCP_twofish1(*, FApp User Pwd, FExe On Ini*)) then
      begin
        // Liberar tudo por aqui!
        if (VAR_SENHA_BOSS <> EmptyStr) and (VAR_ExigeSenhaLoginApp) then
          Continua := ProjGroup_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
        else
          Continua := True;
      end else
      begin
        Geral.MB_Info('N�O Liberado!');
        Halt(0);
      end;
    end;
  end else
  begin
    Continua := True;
  end;
  //
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //
  VAR_LaUsoApPacotes   := nil;
  VAR_LaUsoApBytes     := nil;
  VAR_LaTempoOcioso    := nil;
  VAR_LaTempoAtivo     := nil;
  VAR_LaTempoLimbo     := nil;
  VAR_LaCicloPacotes   := nil;
  VAR_LaCicloBytes     := nil;
  //
  VAR_TmEsgotou        := nil;
  VAR_dmkLED1          := nil;
  VAR_dmkLED2          := nil;
  //
  if MyProcesses.KillProcess(MyProcesses.FindProcessesByName(FExecutavel)) then
    ShowMessage('Processo ativo finalizado!')
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  FExeLehDadosRede := ProjGroup_PF.ObtemNomeExecutavelLeitorTrafegoDadosRede();

  // Qualquer Execut�vel Dermatek
  Geral.DefineFormatacoes;
  dmkPF.ConfigIniApp(0);
  Application.OnMessage := MyObjects.FormMsg;
  //
  // Inicializa��o
  FmPrincipal.Height := 250;
  //
  DefineDadosIniciais();
  RecarregaDadosParaExecucao();
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  IniciaMonitoramentoDoTrafego();
end;

procedure TFmPrincipal.IniciaMonitoramentoDoTrafego();
begin
  if FExecDoleAsAdmin then
  begin
    if not IsProgAdmin then
    begin
      Geral.MB_Aviso(
      'Este programa precisa ser executado como administrador para funcionar corretamente conforme configurado!');
    end else
    begin
      if not Assigned(FmTrafego) then
      begin
        Application.CreateForm(TFmTrafego, FmTrafego);
        // Somente ap�s criar o FmTrafego!
        VAR_LaUsoApPacotes   := LaLMUsoApPacks;
        VAR_LaUsoApBytes     := LaLMUsoAPBytes;
        VAR_LaTempoOcioso    := LaTempoOciosoCiclos;
        VAR_LaTempoAtivo     := LaAtivoCiclos;
        VAR_LaTempoLimbo     := LaLimboCiclo;
        VAR_LaCicloPacotes   := LaLMCicloPacks;
        VAR_LaCicloBytes     := LaLMCicloBytes;
        //
        VAR_TmEsgotou        := TmEsgotou;
        VAR_dmkLED1          := dmkLED1;
        VAR_dmkLED2          := dmkLED2;
        //
        BtTrafego.Enabled := True;
      end;
      //
    end;
  end else
  begin
    if MyProcesses.FindProcessesByName(FExecutavel) > 0 then
      TmReadCiclo.Enabled := True;
  end;
end;

procedure TFmPrincipal.RecarregaDadosParaExecucao();
begin
  // ... (Re)carrega dados para execu��o
  BtExecuta.Enabled := PrjGruFiles_PF.CarregaDadosDeArquivo(
  FmPrincipal.DCP_twofish1,
  FNome, FPasta, FExecutavel, FParametros, FMonitSohProprioProces,
  (*FParaEtapaOnDeactive,*) FExecDoleAsAdmin, FExecTargetAsAdmin, FLogInMemo, FExeOnIni,
  FSegWaitOnOSReinit, FSegWaitScanMonit,
  FHWNDShow, FEstadoJanela(*, FMaiorCodigo*),
  FIntervalPacks, FMaxSecPcks, FPorta, FCicloTimeSeg, FCicloMinPacks,
  FCicloMinBytes, FPackMinBytes, FMaxTempoOciosoSeg, FTempoPergKillSecs,
  FTmpOciPostergaSeg(*,
  FSTUserName, FSTUserPwd*),
  FDesabKillSecs, FEncerraDoleJunto);
  //
  ProjGroup_PF.DefineTemporizadorKill(dmkLED1, dmkLED2, FTempoPergKillSecs,
    FDesabKillSecs);
  //
  FSecToKill := 0;
end;

procedure TFmPrincipal.TmEsgotouTimer(Sender: TObject);
var
  tmpFormStyle: TFormStyle;
begin
  FSecToKill := FSecToKill - (TmEsgotou.Interval div 1000);
  if FSecToKill <= 0 then
  begin
    TmEsgotou.Enabled := False;
    //
    dmkLED1.BrightColor := $00794A3D; // A
    dmkLED1.DimColor    := $00472F2C; // Z
    dmkLED2.BrightColor := $00794A3D; // U
    dmkLED2.DimColor    := $00472F2C; // L
    //
    dmkLED1.Value := 0;
    dmkLED2.Value := 0;
    if not MyProcesses.KillProcess(MyProcesses.FindProcessesByName(FExecutavel)) then
      ShowMessage('Processo n�o finalizado.')
    else
    begin
      if FEncerraDoleJunto then
      begin
        Application.Terminate;
        Exit;
      end else
      begin
        FEmPergunta      := False;
        TmInExec.Enabled := False;
        FTickOnExec_Run  := 0;
        FTickOnExec_Ini  := 0;
        //
        //FCountCiclos   := 0;
        FThisCiclPacks := 0;
        FThisCiclBytes := 0;
        FCiclosOcios   := 0;
        FCicloCount    := -1;
        //
        FTickOnExec_Run := 0;
        FTickOnExec_Ini := 0;
        TmInExec.Enabled := False;
        //
        ShowMessage('Processo inativo finalizado!.');
        BtExecuta.Enabled := True;
        //
        DefineDadosIniciais();
      end;
      //
    end;
  end else
  begin
    dmkLED1.BrightColor := $009CBC1A; // V
    dmkLED1.DimColor    := $00424221; // E
    dmkLED2.BrightColor := $009CBC1A; // R
    dmkLED2.DimColor    := $00424221; // D
    //
    dmkLED1.Value := FSecToKill div 10;
    dmkLED2.Value := FSecToKill mod 10;
    //
    tmpFormStyle := Application.MainForm.FormStyle;
    Application.MainForm.FormStyle := fsStayOnTop;
    Application.Restore; // optional
    Application.BringToFront;
    Application.MainForm.FormStyle := tmpFormStyle;
  end;
end;

procedure TFmPrincipal.TmInExecTimer(Sender: TObject);
begin
  FTickOnExec_Run := GetTickCount() - FTickOnExec_Ini;
  LaLMTimeInExcec.Caption := FormatDateTime('hh:nn:ss', FTickOnExec_Run / (24 * 60 * 60 * 1000));
end;

procedure TFmPrincipal.TmReadCicloTimer(Sender: TObject);
var
  CicloPacks, CicloBytes, UsoApPacks, UsoAPBytes,
  TickOnCicloAtivo, TickOnCicloOcios, CountInatInTmAtiv, MaxTickOcioso,
  CicloCount, SegCiclosOcios: Integer;
begin

  ////////////////////////// LOCAL MACHINE /////////////////////////////////////

  if FLMCountSecs = FLMMaxCout then
  begin
    FLMCountSecs      := 0;
    CicloPacks        := Geral.ReadAppKeyLM('CicloPacks', 'DoleOut\Voyer\', ktInteger, 0);
    CicloBytes        := Geral.ReadAppKeyLM('CicloBytes', 'DoleOut\Voyer\', ktInteger, 0);
    UsoApPacks        := Geral.ReadAppKeyLM('UsoApPacks', 'DoleOut\Voyer\', ktInteger, 0);
    UsoAPBytes        := Geral.ReadAppKeyLM('UsoApBytes', 'DoleOut\Voyer\', ktInteger, 0);
    TickOnCicloAtivo  := Geral.ReadAppKeyLM('TickOnCicloAtivo', 'DoleOut\Voyer\', ktInteger, 0);
    TickOnCicloOcios  := Geral.ReadAppKeyLM('TickOnCicloOcios', 'DoleOut\Voyer\', ktInteger, 0);
    CountInatInTmAtiv := Geral.ReadAppKeyLM('CountInatInTmAtiv', 'DoleOut\Voyer\', ktInteger, 0);
    CicloCount        := Geral.ReadAppKeyLM('CicloCount', 'DoleOut\Voyer\', ktInteger, 0);
    //
    LaLMCicloAtivo.Caption := FormatDateTime('hh:nn:ss', (TickOnCicloAtivo / 60 / 60 / 24 / 1000));
    LaLMCicloOcios.Caption := FormatDateTime('hh:nn:ss', (TickOnCicloOcios / 60 / 60 / 24 / 1000));
    LaLMCicloLimbo.Caption := FormatDateTime('hh:nn:ss', (CountInatInTmAtiv / 60 / 60 / 24 / 1000));
    //
    if TickOnCicloOcios >= (FMaxTempoOciosoSeg * 1000) then
    begin
      // d� alguns segundos ao usu�rio impedir encerramento...
      ColocaEmPergunta();
      //
    end;
    //
    if TickOnCicloAtivo >= 1000 then
    begin
      if TmEsgotou.Enabled then
      begin
        FEmPergunta := False;
        //BtContinuar.Enabled := False;
        TmEsgotou.Enabled := False;
        //LaTempoOciosoCiclos.Caption := '00:00:00';
        ProjGroup_PF.DefineTemporizadorKill(dmkLED1, dmkLED2, FTempoPergKillSecs,
          FDesabKillSecs);
      end;
    end;
    if FCicloCount = -1 then
      FCicloCount := CicloCount;
    if FCicloCount = CicloCount then
    begin
      FThisCiclPacks := CicloPacks;
      FThisCiclBytes := CicloBytes;
      //
    end else
    begin
      FCicloCount := CicloCount;
      if (FThisCiclPacks < FCicloMinPacks)
      or (FThisCiclBytes < FCicloMinBytes) then
      begin
        FCiclosOcios := FCiclosOcios + 1;
        SegCiclosOcios := (FCiclosOcios * FCicloTimeSeg);
        LaTempoOciosoCiclos.Caption := FormatDateTime('hh:nn:ss', (SegCiclosOcios / 60 / 60 / 24));
        if SegCiclosOcios > FMaxTempoOciosoSeg then
        begin
          // d� alguns segundos ao usu�rio impedir encerramento...
          ColocaEmPergunta();
          //
        end;
      end else
      begin
        FCiclosOcios := 0;
        SegCiclosOcios := (FCiclosOcios * FCicloTimeSeg);
        LaTempoOciosoCiclos.Caption := FormatDateTime('hh:nn:ss', (SegCiclosOcios / 60 / 60 / 24));
      end;
    end;
    //
    LaLMCicloPacks.Caption := Geral.FF0(CicloPacks);
    LaLMCicloBytes.Caption := Geral.FF0(CicloBytes);
    LaLMUsoApPacks.Caption := Geral.FF0(UsoApPacks);
    LaLMUsoApBytes.Caption := Geral.FF0(UsoApBytes);
  end else
  begin
    FLMCountSecs := FLMCountSecs + 1;
    LaLMHoraAgora.Caption := FormatDateTime('h:nn:ss', Now());
  end;
end;

end.
