unit UnPrjGruFiles_PF;

interface


uses Winapi.Windows, Vcl.Forms, System.SysUtils, //, Winapi.ShlObj,
  Winapi.Messages,
  Winapi.ShellApi,
(*
  Vcl.Controls,
  //UnInternalConsts,
  //DCPcrypt2,
  //DCPblockciphers,
  //UnProjGroup_Vars,
  //Vcl.StdCtrls; *)
  DCPtwofish,
  DCPsha1,
  System.JSON,
  System.IOUtils,
  System.Classes,
  UnGrl_Vars;

(*
  Winapi.ShellApi, Winapi.ShlObj,
  System.SysUtils, Vcl.Controls,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, System.JSON, System.IOUtils,
  System.Classes,
  //
  Data.DB, Datasnap.DBClient, // TClientDataSet
  UnGrl_Vars*)


type
  TUnApp_PF = class(TObject)
  private
    { Private declarations }
    procedure _CarregaDados(const DCP_twofish: TDCP_twofish;
              var Nome, Pasta, Executavel, Parametros: String;
              var MonitSohProprioProces, (*ParaEtapaOnDeactive,*) ExecAsAdmin,
                  LogInMemo, ExeOnIni: Boolean;
              var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
              var EstadoJanela(*, MaiorCodigo,*): Integer);
  public
    { Public declarations }
    procedure CarregaDadosDeArquivo(
              const DCP_twofish: TDCP_twofish;
              var Nome, Pasta, Executavel, Parametros: String;
              var MonitSohProprioProces, (*ParaEtapaOnDeactive,*) ExecAsAdmin,
                  LogInMemo, ExeOnIni: Boolean;
              var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
              var HWNDShow(*, MaiorCodigo,*): Integer);
    function  ExecutaAppAlvo(const Form: TForm;
              const ExecAsAdmin: Boolean;
              const Pasta, Executavel, Parametros: String;
              const EstadoJanela: Integer;
              var HProcesso: Integer): Boolean;
    function  FinalizaProcesso(Executavel: String): Boolean;
    function  LocalizaProcesso(const Executavel: String; var hProcess: Integer): Boolean;
    procedure MostraFormTarefa();
  end;

var
  App_PF: TUnApp_PF;

implementation

uses dmkGeral, UnMyVclEvents, UnApp_Consts, UnProjGroup_PF, UnMyJSON,
  Tarefa;

{ TUnApp_PF }

procedure TUnApp_PF.CarregaDadosDeArquivo(
  const DCP_twofish: TDCP_twofish;
  var Nome, Pasta, Executavel, Parametros: String;
  var MonitSohProprioProces, (*ParaEtapaOnDeactive,*) ExecAsAdmin, LogInMemo,
      ExeOnIni: Boolean;
  var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
  var HWNDShow(*, MaiorCodigo,*): Integer);
//var
  //Dir: String;
begin
  //Dir := CO_DIR_RAIZ_DMK + '\Afresh\Data\';
  ForceDirectories(CO_DIR_APP_DATA);
  //FDBCriptArqName := Dir + 'TarefaUnica.dmkafr';
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  _CarregaDados(DCP_twofish,
  Nome, Pasta, Executavel, Parametros, MonitSohProprioProces,
  (*ParaEtapaOnDeactive,*) ExecAsAdmin, LogInMemo, ExeOnIni, SegWaitOnOSReinit,
  SegWaitScanMonit,
  HWNDShow(*, MaiorCodigo,*));
end;

function TUnApp_PF.ExecutaAppAlvo(const Form: TForm; const ExecAsAdmin: Boolean;
  const Pasta, Executavel, Parametros: String; const EstadoJanela: Integer;
  var HProcesso: Integer): Boolean;
  //
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      HProcesso := sei.hProcess;
    end;
  end;
var
  FileName, Folder: string;
  WaitUntilTerminated, WaitUntilIdle(*, RunMinimized*): Boolean;
  ErrorCode: Integer;
  OK: Boolean;
  //EstadoJanela: Integer;
  //HWND_CmdShow: Integer;
  //
  Keys: array of Word;
  Shift: TShiftState;
  SpecialKey: Boolean;
  Retorno: Integer;
var
  Path: string;
  ExecuteResult, MouseClickEventId: Integer;
begin
  Result := False;
  //FileName  := EdPasta.Text + EdExecutavel.Text;
  FileName  := Pasta + Executavel;
  //Params    := EdParametros.Text; // '-p'; // can be empty
  Folder    := ''; //EdPasta.Text; // if empty function will extract path from FileName
  //ErrorCode := ;
  //
  WaitUntilTerminated := False;
  WaitUntilIdle       := True;//True;
  //HWND_CmdShow        := AppPF.EstadoJanelaToHWND_CmdShow(EstadoJanela);

  //if FmPrincipal.FBtAtivado = False then Exit;

  if not ExecAsAdmin then
  begin
    //Funcionando local e Server! *)
    Retorno := ShellExecute(Application.Handle, 'open', PChar(Executavel),
      PChar(Parametros), PChar(Pasta), SW_MAXIMIZE(*HWND_CmdShow*));
    Result := Retorno > 32;
  end else
  begin
    Result :=  RunAsAdmin(Form.Handle, Executavel, Pasta, Parametros, SW_MAXIMIZE);
  end;
end;

function TUnApp_PF.FinalizaProcesso(Executavel: String): Boolean;
  function KillProcess (aProcessId : Cardinal) : boolean;
  var
    hProcess : integer;
  begin
    hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
    Result:= False;
    //
    if (hProcess <>0 ) then
    begin
      Result:= TerminateProcess(hProcess, 0);
      exit;
    end;
  end;
  //
begin
  if Executavel <> '' then
    Result := KillProcess(MyVclEvents.FindProcessesByName(Executavel));
(*
    if Result then
      ShowMessage('Processo n�o finalizado.')
    else
      ShowMessage('Processo finalizado com sucesso!.');
  end;
*)
end;

function TUnApp_PF.LocalizaProcesso(const Executavel: String; var hProcess:
  Integer): Boolean;
begin
  Result := False;
  hProcess := -1;
  if Executavel <> '' then
  begin
    hProcess := MyVclEvents.FindProcessesByName(Executavel);
    Result := hProcess <> -1;
  end;
end;

procedure TUnApp_PF.MostraFormTarefa();
begin
  Application.CreateForm(TFmTarefa, FmTarefa);
  FmTarefa.ShowModal;
  FmTarefa.Destroy;
end;

procedure TUnApp_PF._CarregaDados(const DCP_twofish: TDCP_twofish; var Nome,
  Pasta, Executavel, Parametros: String; var MonitSohProprioProces,
  (*ParaEtapaOnDeactive,*) ExecAsAdmin, LogInMemo, ExeOnIni: Boolean;
  var SegWaitOnOSReinit, SegWaitScanMonit, EstadoJanela(*, MaiorCodigo,*): Integer);
var
  jsonObj, jSubObj: TJSONObject;
  //ja: TJSONArray;
  //jv: TJSONValue;
  i: Integer;
  Texto: TStringList;
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Txt, TxtCript: String;
begin
  //if not FileExists(FDBArqName) then Exit;
  if not FileExists(CO_DBCriptArqName) then Exit;
  Texto := TStringList.Create;
  try
    Txt := TFile.ReadAllText(CO_DBCriptArqName);
    Txt := ProjGroup_PF.DeCryptJSON(Txt, DCP_twofish);
    //
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
    //jsonObj := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(Txt), 0) as TJSONObject;
    if jsonObj = nil then
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        Exit;

    //jv := jsonObj.Get('Nome').JsonValue;
    Nome := MyJSON.JObjValStr(jsonObj, 'Nome');
    Pasta := MyJSON.JObjValStr(jsonObj, 'Pasta');
    Executavel := MyJSON.JObjValStr(jsonObj, 'Executavel');
    Parametros := MyJSON.JObjValStr(jsonObj, 'Parametros');
    //
    MonitSohProprioProces := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'MonitSohProprioProces'));
    //ParaEtapaOnDeactive   := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ParaEtapaOnDeactive'));
    ExecAsAdmin           := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExecAsAdmin'));
    LogInMemo             := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'LogInMemo'));
    ExeOnIni              := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExeOnIni'));
    SegWaitOnOSReinit     := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'SegWaitOnOSReinit'));
    SegWaitScanMonit      := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'SegWaitScanMonit'));

  finally
    Texto.Free;
  end;
end;

end.
