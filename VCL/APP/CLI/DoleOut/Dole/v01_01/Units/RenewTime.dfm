object FmRenewTime: TFmRenewTime
  Left = 0
  Top = 0
  Caption = 'FmRenewTime'
  ClientHeight = 195
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 385
    Height = 85
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 635
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 45
      Height = 58
      Align = alClient
      Alignment = taCenter
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -48
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 85
    Width = 385
    Height = 41
    Align = alTop
    Padding.Top = 11
    TabOrder = 1
    ExplicitLeft = 340
    ExplicitTop = 144
    ExplicitWidth = 185
    object Label2: TLabel
      Left = 1
      Top = 12
      Width = 147
      Height = 13
      Align = alClient
      Alignment = taCenter
      Caption = 'Deseja continuar usando o ....'
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 126
    Width = 385
    Height = 69
    Align = alClient
    TabOrder = 2
    ExplicitLeft = 336
    ExplicitTop = 224
    ExplicitWidth = 185
    ExplicitHeight = 41
    object Button1: TButton
      Left = 128
      Top = 13
      Width = 120
      Height = 40
      Caption = '&Sim'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Timer1: TTimer
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 188
    Top = 44
  end
end
