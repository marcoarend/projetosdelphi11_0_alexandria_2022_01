unit UnProjGroup_Consts;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, mysqlDBTables, UnMyLinguas,
  UnInternalConsts, dmkGeral, UnDmkProcFunc,
  UnDmkEnums;

type
  TUnApp_Consts = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
  end;

const
  CO_APPLICATION_NAME_VOYEZ = 'Voyez';
  CO_APPLICATION_NAME_DOLE  = 'Dole';

var
  App_Constst: TUnApp_Consts;

implementation

end.
