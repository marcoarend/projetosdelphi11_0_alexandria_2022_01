unit UnProjGroup_Vars;

interface

uses Vcl.StdCtrls, Vcl.ExtCtrls, dmkLED;


type
  TUnApp_Vars = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  VAR_SENHA_BOSS: String  = '';
  VAR_CPF_CNPJ: String = '';
  VAR_Posicao: Integer = 0;
  VAR_CodigoPIN: String = '';
  VAR_ExigeSenhaLoginApp: Boolean = False;
  //
  VAR_LaUsoApPacotes: TLabel = nil;
  VAR_LaUsoApBytes: TLabel = nil;
  VAR_LaTempoOcioso: TLabel = nil;
  VAR_LaTempoAtivo: TLabel = nil;
  VAR_LaTempoLimbo: TLabel = nil;
  VAR_LaCicloPacotes: TLabel = nil;
  VAR_LaCicloBytes: TLabel = nil;
  //
  VAR_TmEsgotou: TTimer;
  VAR_dmkLED1: TdmkLED = nil;
  VAR_dmkLED2: TdmkLED = nil;


implementation

end.
