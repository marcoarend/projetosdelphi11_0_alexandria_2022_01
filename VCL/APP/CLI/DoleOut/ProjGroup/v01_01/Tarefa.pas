unit Tarefa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons, System.JSON, System.IOUtils,
  //Vcl.Menus,
  dmkEdit, UnDmkProcFunc,
  DCPcrypt2, DCPblockciphers, DCPtwofish, DCPsha1, ShellApi, dmkCheckBox,
  Vcl.Menus;
  //Data.DB, Vcl.Menus, Vcl.Grids, Vcl.DBGrids;

type
  TFmTarefa = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Label2: TLabel;
    EdExecutavel: TdmkEdit;
    Label3: TLabel;
    EdPasta: TdmkEdit;
    RGEstadoJanela: TRadioGroup;
    Label4: TLabel;
    EdParametros: TdmkEdit;
    CkExeOnIni: TdmkCheckBox;
    CkMonitSohProprioProces: TdmkCheckBox;
    EdSegWaitOnOSReinit: TdmkEdit;
    Label5: TLabel;
    CkExecDoleAsAdmin: TdmkCheckBox;
    CkLogInMemo: TdmkCheckBox;
    EdSegWaitScanMonit: TdmkEdit;
    LaSegWaitOnOSReinit: TLabel;
    Panel4: TPanel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    LaTablPort: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    EdIntervalPacks: TdmkEdit;
    EdMaxSecPcks: TdmkEdit;
    EdPorta: TdmkEdit;
    EdCicloTimeSeg: TdmkEdit;
    EdCicloMinPacks: TdmkEdit;
    EdCicloMinBytes: TdmkEdit;
    EdMaxTempoOciosoSeg: TdmkEdit;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    EdTempoPergKillSecs: TdmkEdit;
    Label22: TLabel;
    Label23: TLabel;
    EdPackMinBytes: TdmkEdit;
    Label24: TLabel;
    CkExecTargetAsAdmin: TdmkCheckBox;
    Label7: TLabel;
    EdTmpOciPostergaSeg: TdmkEdit;
    Label9: TLabel;
    CkDesabKillSecs: TdmkCheckBox;
    CkEncerraDoleJunto: TdmkCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CkExeOnIniClick(Sender: TObject);
  private
    { Private declarations }
    //procedure Tabela_Carrega();
  public
    { Public declarations }
    FDBArqName: String;
    FMaiorCodigo: Integer;
    //
    function  Salva(): Boolean;
  end;

var
  FmTarefa: TFmTarefa;

implementation

uses
  UnInternalConsts, UnMyJson, UnMyObjects, dmkGeral, UnDmkEnums, UnMyVclEvents,
  //UnAppEnums, UnAppMainPF,
  UnProjGroup_PF, UnProjGroup_Consts,
  Principal;

{$R *.dfm}

procedure TFmTarefa.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmTarefa.BtOKClick(Sender: TObject);
begin
  if Salva() then
    Close;
end;

procedure TFmTarefa.CkExeOnIniClick(Sender: TObject);
begin
  EdSegWaitOnOSReinit.Enabled := CkExeOnIni.Checked;
  LaSegWaitOnOSReinit.Enabled := CkExeOnIni.Checked;
end;

procedure TFmTarefa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Geral.MB_Pergunta('Deseja salvar as alterações?') = ID_YES then
    Salva();
end;

procedure TFmTarefa.FormCreate(Sender: TObject);
begin
  FMaiorCodigo := 0;
  //
  EdNome.ValueVariant       := FmPrincipal.FNome;
  EdPasta.ValueVariant      := FmPrincipal.FPasta;
  EdExecutavel.ValueVariant := FmPrincipal.FExecutavel;
  EdParametros.ValueVariant := FmPrincipal.FParametros;
  RGEstadoJanela.ItemIndex  := FmPrincipal.FEstadoJanela;
  //
  CkMonitSohProprioProces.Checked  := FmPrincipal.FMonitSohProprioProces;
  CkExecDoleAsAdmin.Checked        := FmPrincipal.FExecDoleAsAdmin;
  CkExecTargetAsAdmin.Checked      := FmPrincipal.FExecTargetAsAdmin;
  CkLogInMemo.Checked              := FmPrincipal.FLogInMemo;
  CkExeOnIni.Checked               := FmPrincipal.FExeOnIni;
  EdSegWaitOnOSReinit.ValueVariant := FmPrincipal.FSegWaitOnOSReinit;
  EdSegWaitScanMonit.ValueVariant  := FmPrincipal.FSegWaitScanMonit;
  //
  EdIntervalPacks.ValueVariant     := FmPrincipal.FIntervalPacks;
  EdMaxSecPcks.ValueVariant        := FmPrincipal.FMaxSecPcks;
  EdPorta.ValueVariant             := FmPrincipal.FPorta;
  EdCicloTimeSeg.ValueVariant      := FmPrincipal.FCicloTimeSeg;
  EdCicloMinPacks.ValueVariant     := FmPrincipal.FCicloMinPacks;
  EdCicloMinBytes.ValueVariant     := FmPrincipal.FCicloMinBytes;
  EdPackMinBytes.ValueVariant      := FmPrincipal.FPackMinBytes;
  EdMaxTempoOciosoSeg.ValueVariant := FmPrincipal.FMaxTempoOciosoSeg;
  EdTempoPergKillSecs.ValueVariant := FmPrincipal.FTempoPergKillSecs;
  EdTmpOciPostergaSeg.ValueVariant := FmPrincipal.FTmpOciPostergaSeg;
  //
  CkDesabKillSecs.Checked          := FmPrincipal.FDesabKillSecs;
  CkEncerraDoleJunto.Checked       := FmPrincipal.FEncerraDoleJunto;
  //CkExecDoleAsAdmin.Enabled := Application.Name = CO_APPLICATION_NAME_DOLE;
  //
end;

function TFmTarefa.Salva(): Boolean;
  procedure SalvaEmArqDef(TxtCript, AppNome: String);
  var
    Arquivo: String;
  begin
    Arquivo := ProjGroup_PF.ObtemNomeArquivoTarefa(AppNome);
    ForceDirectories(ExtractFilePath(Arquivo));
    TFile.WriteAllText(Arquivo, TxtCript, TEncoding.ANSI);
  end;
var
  i: Integer;
  lJsonObj: TJSONObject;
  JSONColor: TJSONObject;
  //JSONArray : TJSONArray;
  Nome, Pasta, Executavel, Parametros: String;
  EstadoJanela, HWNDShow: Integer;
  TxtCript: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    lJsonObj     := TJSONObject.Create;
    //
    Nome         := EdNome.Text;
    Pasta        := EdPasta.Text;
    Executavel   := EdExecutavel.Text;
    Parametros   := EdParametros.Text;
    //LinCom       := EdLinCom.Text;
    //Processo     := EdProcesso.Text;
    EstadoJanela := RGEstadoJanela.ItemIndex;
    HWNDShow     := dmkPF.EstadoJanelaToHWND_CmdShow(EstadoJanela);
    //
    FmPrincipal.FMonitSohProprioProces := CkMonitSohProprioProces.Checked;
    FmPrincipal.FExecDoleAsAdmin       := CkExecDoleAsAdmin.Checked;
    FmPrincipal.FExecTargetAsAdmin     := CkExecTargetAsAdmin.Checked;
    FmPrincipal.FLogInMemo             := CkLogInMemo.Checked;
    FmPrincipal.FExeOnIni              := CkExeOnIni.Checked;
    FmPrincipal.FSegWaitOnOSReinit     := EdSegWaitOnOSReinit.ValueVariant;
    FmPrincipal.FSegWaitScanMonit      := EdSegWaitScanMonit.ValueVariant;
    //
    FmPrincipal.FIntervalPacks         := EdIntervalPacks.ValueVariant;
    FmPrincipal.FMaxSecPcks            := EdMaxSecPcks.ValueVariant;
    FmPrincipal.FPorta                 := EdPorta.ValueVariant;
    FmPrincipal.FCicloTimeSeg          := EdCicloTimeSeg.ValueVariant;
    FmPrincipal.FCicloMinPacks         := EdCicloMinPacks.ValueVariant;
    FmPrincipal.FCicloMinBytes         := EdCicloMinBytes.ValueVariant;
    FmPrincipal.FPackMinBytes          := EdPackMinBytes.ValueVariant;
    FmPrincipal.FMaxTempoOciosoSeg     := EdMaxTempoOciosoSeg.ValueVariant;
    FmPrincipal.FTempoPergKillSecs     := EdTempoPergKillSecs.ValueVariant;
    FmPrincipal.FDesabKillSecs         := CkDesabKillSecs.Checked;
    FmPrincipal.FEncerraDoleJunto      := CkEncerraDoleJunto.Checked;
    FmPrincipal.FTmpOciPostergaSeg     := EdTmpOciPostergaSeg.ValueVariant;
    //FmPrincipal.FSTUserName            := EdSTUserName.ValueVariant;
    //FmPrincipal.FSTUserPwd             := EdSTUserPwd.ValueVariant;
    //
    lJsonObj.AddPair('Nome', Geral.JsonText(Nome));
    lJsonObj.AddPair('Pasta', Geral.JsonText(Pasta));
    lJsonObj.AddPair('Executavel', Geral.JsonText(Executavel));
    lJsonObj.AddPair('Parametros', Geral.JsonText(Parametros));
    lJsonObj.AddPair('HWNDShow', Geral.FF0(HWNDShow));
    //lJsonObj.AddPair('Itens', Geral.FF0(MemTable.RecordCount));
    //
    lJsonObj.AddPair('MonitSohProprioProces', Geral.JsonBool(FmPrincipal.FMonitSohProprioProces));
    //lJsonObj.AddPair('ParaEtapaOnDeactive',   Geral.JsonBool(FmPrincipal.FParaEtapaOnDeactive));
    lJsonObj.AddPair('ExecDoleAsAdmin',       Geral.JsonBool(FmPrincipal.FExecDoleAsAdmin));
    lJsonObj.AddPair('ExecTargetAsAdmin',     Geral.JsonBool(FmPrincipal.FExecTargetAsAdmin));
    lJsonObj.AddPair('LogInMemo',             Geral.JsonBool(FmPrincipal.FLogInMemo));
    lJsonObj.AddPair('ExeOnIni',              Geral.JsonBool(FmPrincipal.FExeOnIni));
    lJsonObj.AddPair('SegWaitOnOSReinit',     Geral.FF0(FmPrincipal.FSegWaitOnOSReinit));
    lJsonObj.AddPair('SegWaitScanMonit',      Geral.FF0(FmPrincipal.FSegWaitScanMonit));
    //
    lJsonObj.AddPair('IntervalPacks',         Geral.FF0(FmPrincipal.FIntervalPacks));
    lJsonObj.AddPair('MaxSecPcks',            Geral.FF0(FmPrincipal.FMaxSecPcks));
    lJsonObj.AddPair('Porta',                 Geral.FF0(FmPrincipal.FPorta));
    lJsonObj.AddPair('CicloTimeSeg',          Geral.FF0(FmPrincipal.FCicloTimeSeg));
    lJsonObj.AddPair('CicloMinPacks',         Geral.FF0(FmPrincipal.FCicloMinPacks));
    lJsonObj.AddPair('CicloMinBytes',         Geral.FF0(FmPrincipal.FCicloMinBytes));
    lJsonObj.AddPair('PackMinBytes',          Geral.FF0(FmPrincipal.FPackMinBytes));
    lJsonObj.AddPair('MaxTempoOciosoSeg',     Geral.FF0(FmPrincipal.FMaxTempoOciosoSeg));
    lJsonObj.AddPair('TempoPergKillSecs',     Geral.FF0(FmPrincipal.FTempoPergKillSecs));
    lJsonObj.AddPair('DesabKillSecs',         Geral.JsonBool(FmPrincipal.FDesabKillSecs));
    lJsonObj.AddPair('EncerraDoleJunto',      Geral.JsonBool(FmPrincipal.FEncerraDoleJunto));
    lJsonObj.AddPair('TmpOciPostergaSeg',     Geral.FF0(FmPrincipal.FTmpOciPostergaSeg));
    //lJsonObj.AddPair('STUserPwd',             Geral.JsonText(FmPrincipal.FSTUserPwd));



    //
(*
    JSONArray := TJSONArray.Create();
    MemTable.First;
    while not MemTable.Eof do
    begin
      JSONColor := TJSONObject.Create();
      //
      JSONColor.AddPair('Codigo',      Geral.FF0(MemTableCodigo.Value));
      JSONColor.AddPair('Nome',        Geral.JsonText(MemTableNome.Value));
      JSONColor.AddPair('Ordem',       Geral.FF0(MemTableOrdem.Value));
      JSONColor.AddPair('SegundosA',   Geral.FFT_Dot(MemTableSegundosA.Value, 3, siPositivo));
      JSONColor.AddPair('SegundosD',   Geral.FFT_Dot(MemTableSegundosD.Value, 3, siPositivo));
      JSONColor.AddPair('TpAcao',      Geral.FF0(MemTableTpAcao.Value));
      JSONColor.AddPair('MousePosX',   Geral.FF0(MemTableMousePosX.Value));
      JSONColor.AddPair('MousePosY',   Geral.FF0(MemTableMousePosY.Value));
      JSONColor.AddPair('MouseEvent',  Geral.FF0(MemTableMouseEvent.Value));
      JSONColor.AddPair('QtdClkSmlt',  Geral.FF0(MemTableQtdClkSmlt.Value));
      JSONColor.AddPair('KeyCode',     Geral.FF0(MemTableKeyCode.Value));
      JSONColor.AddPair('KeyEvent',    Geral.FF0(MemTableKeyEvent.Value));
      JSONColor.AddPair('Texto',       Geral.JsonText(MemTableTexto.Value));
      JSONColor.AddPair('ShiftState',  Geral.FF0(MemTableShiftState.Value));
      JSONColor.AddPair('TipoTecla',   Geral.FF0(MemTableTipoTecla.Value));
      JSONColor.AddPair('TeclEsp',     Geral.FF0(MemTableTeclEsp.Value));
      JSONColor.AddPair('Letra',       Geral.JsonText(MemTableLetra.Value));

      JSONArray.Add(JSONColor);
      //
      MemTable.Next;
    end;
    lJsonObj.AddPair('TarefasIts', JSONArray);
*)
    //
    TxtCript := ProjGroup_PF.EnCryptJSON(lJsonObj.ToString, FmPrincipal.DCP_twofish1);
    SalvaEmArqDef(TxtCript, CO_APPLICATION_NAME_DOLE);
    SalvaEmArqDef(TxtCript, CO_APPLICATION_NAME_VOYEZ);
    //btnsave.Caption := 'Load Data';
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
