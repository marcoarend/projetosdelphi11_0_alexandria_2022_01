object FmTarefa: TFmTarefa
  Left = 0
  Top = 0
  Caption = 'Configura'#231#227'o de Tarefa'
  ClientHeight = 521
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 321
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 289
      Height = 13
      Caption = 'Nome da configura'#231#227'o:  exemplo: (Windows M'#233'dia Player'#174')'
    end
    object Label2: TLabel
      Left = 12
      Top = 88
      Width = 358
      Height = 13
      Caption = 
        'Nome + extens'#227'o do execut'#225'vel da tarefa alvo: (exemplo: wmplayer' +
        '.exe)'
    end
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 465
      Height = 13
      Caption = 
        'Caminho (pasta) do Execut'#225'vel da tarefa alvo: exemplo: C:\Progra' +
        'm Files\Windows Media Player'
    end
    object Label4: TLabel
      Left = 12
      Top = 128
      Width = 421
      Height = 13
      Caption = 
        'Par'#226'metros extras na execu'#231#227'o do aplicativo da tarefa alvo: (exe' +
        'mplo: -p teste -u 123)'
    end
    object Label5: TLabel
      Left = 12
      Top = 300
      Width = 523
      Height = 13
      Caption = 
        'Intervalo em segundos de an'#225'lise da atividade da tarefa alvo (in' +
        'tervalo de scaneamento do monitoramento):'
    end
    object LaSegWaitOnOSReinit: TLabel
      Left = 232
      Top = 274
      Width = 317
      Height = 13
      Caption = ' Tempo em segundos de espera ao reiniciar o sistema operacional:'
      Enabled = False
    end
    object EdNome: TdmkEdit
      Left = 12
      Top = 24
      Width = 750
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdExecutavel: TdmkEdit
      Left = 12
      Top = 104
      Width = 750
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdPasta: TdmkEdit
      Left = 12
      Top = 64
      Width = 750
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object RGEstadoJanela: TRadioGroup
      Left = 12
      Top = 168
      Width = 750
      Height = 41
      Caption = ' Estado da janela principal da tarefa ao executar: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Padr'#227'o do app'
        'Normal'
        'Maximizada'
        'Minimizada')
      TabOrder = 4
    end
    object EdParametros: TdmkEdit
      Left = 12
      Top = 144
      Width = 750
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CkExeOnIni: TdmkCheckBox
      Left = 12
      Top = 272
      Width = 213
      Height = 17
      Caption = 'Executa tarefa ao iniciar o applicativo.  '
      Checked = True
      State = cbChecked
      TabOrder = 8
      OnClick = CkExeOnIniClick
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CkMonitSohProprioProces: TdmkCheckBox
      Left = 12
      Top = 212
      Width = 337
      Height = 17
      Caption = 'Monitorar somente a inst'#226'ncia (Tarefa) iniciada pelo Dole.'
      TabOrder = 5
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object EdSegWaitOnOSReinit: TdmkEdit
      Left = 560
      Top = 270
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '600'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 600
      ValWarn = False
    end
    object CkExecDoleAsAdmin: TdmkCheckBox
      Left = 12
      Top = 232
      Width = 213
      Height = 17
      Caption = 'Executar o Dole como administrador. '
      TabOrder = 6
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CkLogInMemo: TdmkCheckBox
      Left = 12
      Top = 252
      Width = 213
      Height = 17
      Caption = 'Log de atividades na janela principal. '
      Checked = True
      State = cbChecked
      TabOrder = 7
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object EdSegWaitScanMonit: TdmkEdit
      Left = 560
      Top = 296
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '600'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 600
      ValWarn = False
    end
    object CkExecTargetAsAdmin: TdmkCheckBox
      Left = 236
      Top = 232
      Width = 230
      Height = 17
      Caption = 'Executar a tarefa como administrador. '
      TabOrder = 11
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CkDesabKillSecs: TdmkCheckBox
      Left = 236
      Top = 252
      Width = 230
      Height = 17
      Caption = 'Desabilita temporizador de t'#233'rmino.'
      TabOrder = 12
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CkEncerraDoleJunto: TdmkCheckBox
      Left = 492
      Top = 232
      Width = 230
      Height = 17
      Caption = 'Encerrar o Dole ao encerrar a tarefa.'
      TabOrder = 13
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 321
    Width = 784
    Height = 145
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 325
    ExplicitHeight = 141
    object Label6: TLabel
      Left = 12
      Top = 8
      Width = 163
      Height = 13
      Caption = 'Atualiza'#231#227'o de status da internet:'
    end
    object Label8: TLabel
      Left = 12
      Top = 36
      Width = 136
      Height = 13
      Caption = 'Espera m'#225'xima por pacotes:'
    end
    object Label10: TLabel
      Left = 12
      Top = 64
      Width = 30
      Height = 13
      Caption = 'Porta:'
    end
    object LaTablPort: TLabel
      Left = 268
      Top = 64
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label11: TLabel
      Left = 12
      Top = 92
      Width = 140
      Height = 13
      Caption = 'Ciclo de leitura em segundos:'
    end
    object Label12: TLabel
      Left = 12
      Top = 120
      Width = 119
      Height = 13
      Caption = 'M'#237'nimo pacotes por ciclo:'
    end
    object Label13: TLabel
      Left = 340
      Top = 8
      Width = 108
      Height = 13
      Caption = 'M'#237'nimo Bytes por ciclo:'
    end
    object Label14: TLabel
      Left = 340
      Top = 64
      Width = 116
      Height = 13
      Caption = 'Tempo m'#225'ximo "ocioso":'
    end
    object Label17: TLabel
      Left = 268
      Top = 8
      Width = 50
      Height = 13
      Caption = 'segundos.'
    end
    object Label18: TLabel
      Left = 268
      Top = 36
      Width = 50
      Height = 13
      Caption = 'segundos.'
    end
    object Label19: TLabel
      Left = 268
      Top = 92
      Width = 50
      Height = 13
      Caption = 'segundos.'
    end
    object Label20: TLabel
      Left = 596
      Top = 64
      Width = 50
      Height = 13
      Caption = 'segundos.'
    end
    object Label21: TLabel
      Left = 340
      Top = 92
      Width = 123
      Height = 13
      Caption = 'Temporizador de t'#233'rmino:'
    end
    object Label22: TLabel
      Left = 596
      Top = 92
      Width = 50
      Height = 13
      Caption = 'segundos.'
    end
    object Label23: TLabel
      Left = 340
      Top = 36
      Width = 134
      Height = 13
      Caption = 'Tamanho m'#237'nimo do pacote:'
    end
    object Label24: TLabel
      Left = 596
      Top = 36
      Width = 31
      Height = 13
      Caption = 'bytes.'
    end
    object Label7: TLabel
      Left = 340
      Top = 120
      Width = 135
      Height = 13
      Caption = 'Tempo ocioso p'#243's posterga:'
    end
    object Label9: TLabel
      Left = 596
      Top = 120
      Width = 50
      Height = 13
      Caption = 'segundos.'
    end
    object EdIntervalPacks: TdmkEdit
      Left = 184
      Top = 4
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '90'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 90
      ValWarn = False
    end
    object EdMaxSecPcks: TdmkEdit
      Left = 184
      Top = 32
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '90'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 90
      ValWarn = False
    end
    object EdPorta: TdmkEdit
      Left = 184
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '491'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 491
      ValWarn = False
    end
    object EdCicloTimeSeg: TdmkEdit
      Left = 184
      Top = 88
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '60'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 60
      ValWarn = False
    end
    object EdCicloMinPacks: TdmkEdit
      Left = 184
      Top = 116
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '50'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 50
      ValWarn = False
    end
    object EdCicloMinBytes: TdmkEdit
      Left = 512
      Top = 4
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '10000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 10000
      ValWarn = False
    end
    object EdMaxTempoOciosoSeg: TdmkEdit
      Left = 512
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '300'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 300
      ValWarn = False
    end
    object EdTempoPergKillSecs: TdmkEdit
      Left = 512
      Top = 88
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '10'
      ValMax = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '15'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 15
      ValWarn = False
    end
    object EdPackMinBytes: TdmkEdit
      Left = 512
      Top = 32
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '64'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 64
      ValWarn = False
    end
    object EdTmpOciPostergaSeg: TdmkEdit
      Left = 512
      Top = 116
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '10'
      ValMax = '60'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '30'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 30
      ValWarn = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 466
    Width = 784
    Height = 55
    Align = alBottom
    TabOrder = 2
    object BtOK: TBitBtn
      Left = 8
      Top = 8
      Width = 120
      Height = 40
      Caption = 'Salvar'
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BitBtn1: TBitBtn
      Left = 648
      Top = 8
      Width = 120
      Height = 40
      Caption = 'Desiste'
      TabOrder = 1
      OnClick = BitBtn1Click
    end
  end
end
