unit UnPrjGruFiles_PF;

interface


uses Winapi.Windows, Winapi.Messages, Winapi.ShellApi, Vcl.Forms,
  System.SysUtils, System.JSON, System.IOUtils, System.Classes,
  DCPtwofish, DCPsha1, UnGrl_Vars;

type
  TUnPrjGruFiles_PF = class(TObject)
  private
    { Private declarations }
    procedure _CarregaDados(const DCP_twofish: TDCP_twofish;
              var Nome, Pasta, Executavel, Parametros: String;
              var MonitSohProprioProces, (*ParaEtapaOnDeactive,*) ExecDoleAsAdmin,
                  ExecTargetAsAdmin,LogInMemo, ExeOnIni: Boolean;
              var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
              var HWNDShow, EstadoJanela,(* MaiorCodigo,*)
              IntervalPacks, MaxSecPcks, Porta, CicloTimeSeg, CicloMinPacks,
              CicloMinBytes, PackMinBytes, MaxTempoOciosoSeg, TempoPergKillSecs,
              TmpOciPostergaSeg: Integer(*;
              var  STUserName, STUserPwd: String*);
              var DesabKillSecs, EncerraDoleJunto: Boolean);
    procedure _MostraFormAgendaTarefa();
    procedure _MostraFormTarefa();
  public
    { Public declarations }
    function  CarregaDadosDeArquivo(
              const DCP_twofish: TDCP_twofish;
              var Nome, Pasta, Executavel, Parametros: String;
              var MonitSohProprioProces, (*ParaEtapaOnDeactive,*) ExecDoleAsAdmin,
                  ExecTargetAsAdmin, LogInMemo, ExeOnIni: Boolean;
              var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
              var HWNDShow, EstadoJanela,(*MaiorCodigo,*)
              IntervalPacks, MaxSecPcks, Porta, CicloTimeSeg, CicloMinPacks,
              CicloMinBytes, PackMinBytes, MaxTempoOciosoSeg, TempoPergKillSecs,
              TmpOciPostergaSeg: Integer(*;
              var  STUserName, STUserPwd: String*);
              var DesabKillSecs, EncerraDoleJunto: Boolean): Boolean;
    function  ExecutaAppAlvo(const Form: TForm;
              const ExecTargetAsAdmin: Boolean;
              const Pasta, Executavel, Parametros: String;
              const EstadoJanela: Integer;
              var HProcesso: Integer): Boolean;
    function  FinalizaProcesso(Executavel: String): Boolean;
    function  LocalizaProcesso(const Executavel: String; var hProcess: Integer): Boolean;
    procedure MostraFormTarefa_ParaBoss();
    procedure MostraFormAgendaTarefa_ParaBoss();
  end;

var
  PrjGruFiles_PF: TUnPrjGruFiles_PF;

implementation

uses dmkGeral, UnMyVclEvents, UnProjGroup_PF, UnMyJSON, UnProjGroup_Vars,
  UnDmkProcFunc, UnMyProcesses,
  AgendaTarefa, Tarefa;

{ TUnPrjGruFiles_PF }

function TUnPrjGruFiles_PF.CarregaDadosDeArquivo(
  const DCP_twofish: TDCP_twofish;
  var Nome, Pasta, Executavel, Parametros: String;
  var MonitSohProprioProces, (*ParaEtapaOnDeactive,*) ExecDoleAsAdmin,
      ExecTargetAsAdmin, LogInMemo,
      ExeOnIni: Boolean;
  var SegWaitOnOSReinit, SegWaitScanMonit: Integer;
  var HWNDShow, EstadoJanela(*, MaiorCodigo,*),
  IntervalPacks, MaxSecPcks, Porta, CicloTimeSeg, CicloMinPacks,
  CicloMinBytes, PackMinBytes, MaxTempoOciosoSeg, TempoPergKillSecs,
  TmpOciPostergaSeg: Integer(*;
  var STUserName, STUserPwd: String*);
  var DesabKillSecs, EncerraDoleJunto: Boolean): Boolean;
var
  Dir: String;
begin
  Result := False;
  //
  Dir := ExtractFileDir(ProjGroup_PF.ObtemNomeArquivoTarefa(Application.Name));
  ForceDirectories(Dir);
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Nome       := EmptyStr;
  Pasta      := EmptyStr;
  Executavel := EmptyStr;
  Parametros := EmptyStr;
  //
  _CarregaDados(DCP_twofish,
  Nome, Pasta, Executavel, Parametros, MonitSohProprioProces,
  (*ParaEtapaOnDeactive,*) ExecDOleAsAdmin, ExecTargetAsAdmin, LogInMemo,
  ExeOnIni, SegWaitOnOSReinit, SegWaitScanMonit,
  HWNDShow, EstadoJanela(*, MaiorCodigo,*),
  IntervalPacks, MaxSecPcks, Porta, CicloTimeSeg, CicloMinPacks, CicloMinBytes,
  PackMinBytes, MaxTempoOciosoSeg, TempoPergKillSecs, TmpOciPostergaSeg
  (*, STUserName, STUserPwd*), DesabKillSecs, EncerraDoleJunto);
  //
  Result := Executavel <> EmptyStr;
end;

function TUnPrjGruFiles_PF.ExecutaAppAlvo(const Form: TForm; const ExecTargetAsAdmin: Boolean;
  const Pasta, Executavel, Parametros: String; const EstadoJanela: Integer;
  var HProcesso: Integer): Boolean;
  //
  function RunAsAdmin(const Handle: Hwnd; const Exec, Dir, Params: string; const HWND_CmdShow: Integer): Boolean;
  var
    sei: TShellExecuteInfoA;
    InstApp: Integer;
  begin
    FillChar(sei, SizeOf(sei), 0);
    sei.cbSize := SizeOf(sei);
    sei.Wnd := Handle;
    sei.fMask := SEE_MASK_FLAG_DDEWAIT or SEE_MASK_FLAG_NO_UI or SEE_MASK_NOCLOSEPROCESS;
    //sei.lpVerb := 'open';
    sei.lpVerb := 'runas'; // Run as admin
    //sei.lpFile := 'executavel.exe';
    sei.lpFile := PAnsiChar(AnsiString(Exec));
    //sei.lpDirectory  := 'C:\Program Files (x86)\Dermatek\';
    sei.lpDirectory  := PAnsiChar(AnsiString(Dir));
    //sei.lpParameters := '-h www.???.com.br -u root -p teste123 -c -a "MyApp"'; //
    sei.lpParameters := PAnsiChar(AnsiString(Params));
    sei.nShow := HWND_CmdShow; //SW_SHOWNORMAL;
    Result := ShellExecuteExA(@sei);
    if Result then
    begin
      InstApp  := sei.hInstApp;
      HProcesso := sei.hProcess;
    end;
  end;
var
  FileName, Folder: string;
  WaitUntilTerminated, WaitUntilIdle(*, RunMinimized*): Boolean;
  ErrorCode: Integer;
  OK: Boolean;
  //EstadoJanela: Integer;
  //HWND_CmdShow: Integer;
  //
  Keys: array of Word;
  Shift: TShiftState;
  SpecialKey: Boolean;
  Retorno: Integer;
var
  Path: string;
  ExecuteResult, MouseClickEventId: Integer;
begin
  Result := False;
  //FileName  := EdPasta.Text + EdExecutavel.Text;
  FileName  := Pasta + Executavel;
  //Params    := EdParametros.Text; // '-p'; // can be empty
  Folder    := ''; //EdPasta.Text; // if empty function will extract path from FileName
  //ErrorCode := ;
  //
  WaitUntilTerminated := False;
  WaitUntilIdle       := True;//True;
  //HWND_CmdShow        := AppPF.EstadoJanelaToHWND_CmdShow(EstadoJanela);

  //if FmPrincipal.FBtAtivado = False then Exit;

  if not ExecTargetAsAdmin then
  begin
    //Funcionando local e Server! *)
    Retorno := ShellExecute(Application.Handle, 'open', PChar(Executavel),
      PChar(Parametros), PChar(Pasta), SW_MAXIMIZE(*HWND_CmdShow*));
    Result := Retorno > 32;
  end else
  begin
    Result :=  RunAsAdmin(Form.Handle, Executavel, Pasta, Parametros, SW_MAXIMIZE);
  end;
end;

function TUnPrjGruFiles_PF.FinalizaProcesso(Executavel: String): Boolean;
  function KillProcess (aProcessId : Cardinal) : boolean;
  var
    hProcess : integer;
  begin
    hProcess:= OpenProcess(PROCESS_ALL_ACCESS, TRUE, aProcessId);
    Result:= False;
    //
    if (hProcess <>0 ) then
    begin
      Result:= TerminateProcess(hProcess, 0);
      exit;
    end;
  end;
  //
begin
  if Executavel <> '' then
    Result := KillProcess(MyProcesses.FindProcessesByName(Executavel));
(*
    if Result then
      ShowMessage('Processo n�o finalizado.')
    else
      ShowMessage('Processo finalizado com sucesso!.');
  end;
*)
end;

function TUnPrjGruFiles_PF.LocalizaProcesso(const Executavel: String; var hProcess:
  Integer): Boolean;
begin
  Result := False;
  hProcess := -1;
  if Executavel <> '' then
  begin
    hProcess := MyProcesses.FindProcessesByName(Executavel);
    Result := hProcess <> -1;
  end;
end;

procedure TUnPrjGruFiles_PF.MostraFormAgendaTarefa_ParaBoss;
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := ProjGroup_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
    _MostraFormAgendaTarefa();
end;

procedure TUnPrjGruFiles_PF.MostraFormTarefa_ParaBoss();
var
  Continua: Boolean;
begin
  if VAR_SENHA_BOSS <> EmptyStr then
    Continua := ProjGroup_PF.LiberaAppPelaSenha(VAR_SENHA_BOSS)
  else
    Continua := True;
  if Continua then
    _MostraFormTarefa();
end;

procedure TUnPrjGruFiles_PF._MostraFormAgendaTarefa;
begin
  //ScheduleRunAtStartup();
  Application.CreateForm(TFmAgendaTarefa, FmAgendaTarefa);
  FmAgendaTarefa.ShowModal;
  FmAgendaTarefa.Destroy;
end;

procedure TUnPrjGruFiles_PF._MostraFormTarefa();
begin
  Application.CreateForm(TFmTarefa, FmTarefa);
  FmTarefa.ShowModal;
  FmTarefa.Destroy;
end;

procedure TUnPrjGruFiles_PF._CarregaDados(const DCP_twofish: TDCP_twofish; var Nome,
  Pasta, Executavel, Parametros: String; var MonitSohProprioProces,
  (*ParaEtapaOnDeactive,*) ExecDoleAsAdmin, ExecTargetAsAdmin, LogInMemo, ExeOnIni: Boolean;
  var SegWaitOnOSReinit, SegWaitScanMonit, HWNDShow, EstadoJanela,(*MaiorCodigo,*)
  IntervalPacks, MaxSecPcks, Porta, CicloTimeSeg, CicloMinPacks, CicloMinBytes,
  PackMinBytes, MaxTempoOciosoSeg, TempoPergKillSecs, TmpOciPostergaSeg: Integer(*;
  var STUserName, STUserPwd: String*); var DesabKillSecs, EncerraDoleJunto: Boolean);
var
  jsonObj, jSubObj: TJSONObject;
  //ja: TJSONArray;
  //jv: TJSONValue;
  i: Integer;
  Texto: TStringList;
var
  LJsonArr   : TJSONArray;
  LJsonValue : TJSONValue;
  LItem     : TJSONValue;
  //
  jsArray: TJSONArray;
  jsonObject: TJSONObject;
  Campo, Valor: String;
  N: Integer;
  //
  Item: TJSONObject;
  Txt, TxtCript, DBCriptArqName: String;
begin
  //if not FileExists(FDBArqName) then Exit;
  DBCriptArqName := ProjGroup_PF.ObtemNomeArquivoTarefa(Application.Name);

  if not FileExists(DBCriptArqName) then Exit;
  Texto := TStringList.Create;
  try
    Txt := TFile.ReadAllText(DBCriptArqName);
    Txt := ProjGroup_PF.DeCryptJSON(Txt, DCP_twofish);
    //
    jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Txt), 0) as TJSONObject;
    //jsonObj := TJSONObject.ParseJSONValue(TEncoding.UTF8.GetBytes(Txt), 0) as TJSONObject;
    if jsonObj = nil then
      jsonObj := TJSONObject.ParseJSONValue(TEncoding.ANSI.GetBytes(Txt), 0) as TJSONObject;
      if jsonObj = nil then
        Exit;

    //jv := jsonObj.Get('Nome').JsonValue;
    Nome := MyJSON.JObjValStr(jsonObj, 'Nome');
    Pasta := MyJSON.JObjValStr(jsonObj, 'Pasta');
    Executavel := MyJSON.JObjValStr(jsonObj, 'Executavel');
    Parametros := MyJSON.JObjValStr(jsonObj, 'Parametros');
    //
    MonitSohProprioProces := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'MonitSohProprioProces'));
    //ParaEtapaOnDeactive   := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ParaEtapaOnDeactive'));
    ExecDoleAsAdmin       := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExecDoleAsAdmin'));
    ExecTargetAsAdmin     := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExecTargetAsAdmin'));
    LogInMemo             := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'LogInMemo'));
    ExeOnIni              := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'ExeOnIni'));
    SegWaitOnOSReinit     := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'SegWaitOnOSReinit'));
    SegWaitScanMonit      := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'SegWaitScanMonit'));
    HWNDShow              := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'HWNDShow'));
    EstadoJanela          := dmkPF.HWND_CmdShowToEstadoJanela(HWNDShow);
    //
    IntervalPacks         := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'IntervalPacks'));
    MaxSecPcks            := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'MaxSecPcks'));
    Porta                 := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'Porta'));
    CicloTimeSeg          := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'CicloTimeSeg'));
    CicloMinPacks         := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'CicloMinPacks'));
    CicloMinBytes         := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'CicloMinBytes'));
    PackMinBytes          := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'PackMinBytes'));
    MaxTempoOciosoSeg     := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'MaxTempoOciosoSeg'));
    TempoPergKillSecs     := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'TempoPergKillSecs'));
    TmpOciPostergaSeg     := Geral.IMV(MyJSON.JObjValStr(jsonObj, 'TmpOciPostergaSeg'));
    if TmpOciPostergaSeg < 6 then
      TmpOciPostergaSeg := 30;
    //
    //STUserName            := MyJSON.JObjValStr(jsonObj, 'STUserName');
    //STUserPwd             := MyJSON.JObjValStr(jsonObj, 'STUserPwd');
    //
    DesabKillSecs         := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'DesabKillSecs'));
    EncerraDoleJunto      := Geral.NumStrToBool(MyJSON.JObjValStr(jsonObj, 'EncerraDoleJunto'));
  finally
    Texto.Free;
  end;
end;

end.
