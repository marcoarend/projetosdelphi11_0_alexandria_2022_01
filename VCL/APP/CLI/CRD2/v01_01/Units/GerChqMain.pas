unit GerChqMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Menus, Mask, UnDmkProcFunc, DmkDAC_PF, frxClass, frxDBSet, UnDmkEnums,
  dmkDBGrid;

type
  TTipoFormChq = (tmfChqOcor, tmfChqPror, tmfChqDevo, tmfChqPgDv, tmfChqPgOc);
  TFmGerChqMain = class(TForm)
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    BtHistorico: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    PMHistorico: TPopupMenu;
    Clientepesquisado1: TMenuItem;
    Emitentesacadopesquisado1: TMenuItem;
    Ambos2: TMenuItem;
    N5: TMenuItem;
    Clienteselecionado1: TMenuItem;
    Emitentesacadoselecionado1: TMenuItem;
    Ambos1: TMenuItem;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrLast: TmySQLQuery;
    QrLastData: TDateField;
    QrColigado: TmySQLQuery;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    DsColigado: TDataSource;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFLimite: TFloatField;
    QrEmitCPFLastAtz: TDateField;
    QrEmitCPFAcumCHComV: TFloatField;
    QrEmitCPFAcumCHComQ: TIntegerField;
    QrEmitCPFAcumCHDevV: TFloatField;
    QrEmitCPFAcumCHDevQ: TIntegerField;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    QrEmitBACCPF: TWideStringField;
    QrAntePror: TmySQLQuery;
    QrAnteProrData1: TDateField;
    QrAnteProrData2: TDateField;
    QrAnteProrData3: TDateField;
    QrAnteProrBanco: TIntegerField;
    QrAnteProrEmitente: TWideStringField;
    QrAnteProrVALORCHEQUE: TFloatField;
    QrAnteProrVALORRESSARCIDO: TFloatField;
    QrAnteProrControle: TIntegerField;
    QrAnteProrITEMLOTE: TIntegerField;
    QrAnteProrVlrCompra: TFloatField;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    DsBanco: TDataSource;
    QrBco1: TmySQLQuery;
    QrBco1Nome: TWideStringField;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    DsOcorreu: TDataSource;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrOcorP: TmySQLQuery;
    DsOcorP: TDataSource;
    QrAlinIts: TmySQLQuery;
    QrAlinItsCodigo: TIntegerField;
    QrAlinItsAlinea1: TIntegerField;
    QrAlinItsAlinea2: TIntegerField;
    QrAlinItsData1: TDateField;
    QrAlinItsData2: TDateField;
    QrAlinItsData3: TDateField;
    QrAlinItsCliente: TIntegerField;
    QrAlinItsBanco: TIntegerField;
    QrAlinItsAgencia: TIntegerField;
    QrAlinItsConta: TWideStringField;
    QrAlinItsCheque: TIntegerField;
    QrAlinItsCPF: TWideStringField;
    QrAlinItsValor: TFloatField;
    QrAlinItsTaxas: TFloatField;
    QrAlinItsLk: TIntegerField;
    QrAlinItsDataCad: TDateField;
    QrAlinItsDataAlt: TDateField;
    QrAlinItsUserCad: TIntegerField;
    QrAlinItsUserAlt: TIntegerField;
    QrAlinItsEmitente: TWideStringField;
    QrAlinItsCPF_TXT: TWideStringField;
    QrAlinItsDATA1_TXT: TWideStringField;
    QrAlinItsDATA2_TXT: TWideStringField;
    QrAlinItsDATA3_TXT: TWideStringField;
    QrAlinItsChequeOrigem: TIntegerField;
    QrAlinItsStatus: TSmallintField;
    QrAlinItsValPago: TFloatField;
    QrAlinItsMulta: TFloatField;
    QrAlinItsJurosP: TFloatField;
    QrAlinItsJurosV: TFloatField;
    QrAlinItsDesconto: TFloatField;
    QrAlinItsNOMEALINEA1: TWideStringField;
    QrAlinItsNOMEALINEA2: TWideStringField;
    QrAlinItsSALDO: TFloatField;
    QrAlinItsATUAL: TFloatField;
    QrAlinItsPgDesc: TFloatField;
    DsAlinIts: TDataSource;
    QrChqPgs: TmySQLQuery;
    DsChqPgs: TDataSource;
    QrLotPrr: TmySQLQuery;
    QrLotPrrCodigo: TIntegerField;
    QrLotPrrControle: TIntegerField;
    QrLotPrrData1: TDateField;
    QrLotPrrData2: TDateField;
    QrLotPrrData3: TDateField;
    QrLotPrrOcorrencia: TIntegerField;
    QrLotPrrLk: TIntegerField;
    QrLotPrrDataCad: TDateField;
    QrLotPrrDataAlt: TDateField;
    QrLotPrrUserCad: TIntegerField;
    QrLotPrrUserAlt: TIntegerField;
    QrLotPrrDIAS_1_3: TIntegerField;
    QrLotPrrDIAS_2_3: TIntegerField;
    DsLotPrr: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaItens: TLargeintField;
    QrSomaValor: TFloatField;
    DsSoma: TDataSource;
    GBPesq: TGroupBox;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label36: TLabel;
    Label2: TLabel;
    Label9: TLabel;
    LaColigado: TLabel;
    Label55: TLabel;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    EdBanda: TdmkEdit;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    DBEdit19: TDBEdit;
    EdCPF_1: TdmkEdit;
    EdCMC_7_1: TEdit;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    CkRepassado: TdmkCheckGroup;
    EdControle: TdmkEdit;
    EdEmitente1: TdmkEdit;
    CkAtualiza: TCheckBox;
    CkStatus: TdmkCheckGroup;
    CkProrrogado: TdmkCheckGroup;
    CkCalcFields: TCheckBox;
    BtChequeOcorr: TBitBtn;
    BtStatus: TBitBtn;
    BtQuitacao: TBitBtn;
    BtChequeDev: TBitBtn;
    BtDesDep: TBitBtn;
    BtBordero: TBitBtn;
    BtReabre: TBitBtn;
    GBData: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid3: TDBGrid;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid2: TDBGrid;
    Panel7: TPanel;
    BtReciboAntecip: TBitBtn;
    TabSheet3: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet4: TTabSheet;
    Memo1: TMemo;
    PMOcorreu: TPopupMenu;
    Incluiocorrncia1: TMenuItem;
    Alteraocorrncia1: TMenuItem;
    Excluiocorrncia1: TMenuItem;
    N3: TMenuItem;
    IncluiPagamento2: TMenuItem;
    Excluipagamento2: TMenuItem;
    N4: TMenuItem;
    Recibodopagamento2: TMenuItem;
    PMStatus: TPopupMenu;
    Incluiprorrogao1: TMenuItem;
    IncluiQuitao1: TMenuItem;
    ExcluiProrrogao1: TMenuItem;
    N6: TMenuItem;
    N0ForastatusAutomtico1: TMenuItem;
    N1Forastatus1: TMenuItem;
    N2ForastatusBaixado1: TMenuItem;
    N3ForastatusMoroso1: TMenuItem;
    PMDevolucao: TPopupMenu;
    IncluiPagamento1: TMenuItem;
    ExcluiPagamento1: TMenuItem;
    N1: TMenuItem;
    Alteradevoluo1: TMenuItem;
    Desfazdevoluo1: TMenuItem;
    N2: TMenuItem;
    RecibodoPagamento1: TMenuItem;
    PMReciboAntecip: TPopupMenu;
    Meuparaocliente1: TMenuItem;
    Clienteparamim1: TMenuItem;
    PMQuitacao: TPopupMenu;
    Quitadocumento1: TMenuItem;
    Imprimerecibodequitao1: TMenuItem;
    Panel9: TPanel;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    Panel8: TPanel;
    Label8: TLabel;
    Label6: TLabel;
    Label57: TLabel;
    DBEdit7: TDBEdit;
    DBEdit5: TDBEdit;
    EdSomaI: TdmkEdit;
    EdSomaV: TdmkEdit;
    GradeItens: TdmkDBGrid;
    Splitter1: TSplitter;
    QrOcorreuTpOcor: TSmallintField;
    RGFontes: TRadioGroup;
    QrPesqCNPJ_TXT: TWideStringField;
    QrPesqNOMESTATUS: TWideStringField;
    QrPesqTAXA_COMPRA: TFloatField;
    QrPesqTxaCompra: TFloatField;
    QrPesqDCompra: TDateField;
    QrPesqDesco: TFloatField;
    QrPesqQuitado: TIntegerField;
    QrPesqNOMECOLIGADO: TWideStringField;
    QrPesqSTATUSDEV: TSmallintField;
    QrPesqDevolucao: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrPesqNOMECLIENTE: TWideStringField;
    QrPesqProrrVz: TIntegerField;
    QrPesqProrrDd: TIntegerField;
    QrPesqEmitente: TWideStringField;
    QrPesqValQuit: TFloatField;
    QrPesqDDeposito: TDateField;
    QrPesqRisco: TFloatField;
    QrPesqNome: TWideStringField;
    QrPesqNaoDeposita: TSmallintField;
    QrPesqRepCli: TIntegerField;
    QrPesqBanco: TIntegerField;
    QrPesqDepositado: TSmallintField;
    QrPesqTEL1CLI: TWideStringField;
    QrPesqTEL1CLI_TXT: TWideStringField;
    QrPesqDocumento: TFloatField;
    QrPesqFatParcela: TIntegerField;
    QrPesqData: TDateField;
    QrPesqCredito: TFloatField;
    QrPesqVencimento: TDateField;
    QrPesqContaCorrente: TWideStringField;
    QrPesqDescricao: TWideStringField;
    QrPesqCodigo: TIntegerField;
    QrPesqCNPJCPF: TWideStringField;
    QrAnteProrDocumento: TFloatField;
    QrAnteProrContaCorrente: TWideStringField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesTe1: TWideStringField;
    QrClientesTe1_TXT: TWideStringField;
    DsClientes: TDataSource;
    QrPesqAgencia: TIntegerField;
    QrAnteProrAgencia: TIntegerField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPFatParcela: TIntegerField;
    QrOcorPPago: TFloatField;
    QrOcorPFatNum: TFloatField;
    QrOcorPMoraVal: TFloatField;
    QrOcorreuLOIS: TIntegerField;
    QrOcorreuPlaGen: TIntegerField;
    QrLastOcorFatParcela: TIntegerField;
    QrOcorPControle: TIntegerField;
    QrPesqControle: TIntegerField;
    CkCliente: TCheckBox;
    BtRecalcula: TBitBtn;
    GradeDev: TDBGrid;
    QrChqPgsCredito: TFloatField;
    QrChqPgsMoraVal: TFloatField;
    QrChqPgsDescoVal: TFloatField;
    QrChqPgsMultaVal: TFloatField;
    QrChqPgsTaxasVal: TFloatField;
    QrChqPgsVencimento: TDateField;
    QrChqPgsBanco: TIntegerField;
    QrChqPgsAgencia: TIntegerField;
    QrChqPgsContaCorrente: TWideStringField;
    QrChqPgsDocumento: TFloatField;
    QrChqPgsCNPJCPF: TWideStringField;
    QrChqPgsEmitente: TWideStringField;
    QrChqPgsData: TDateField;
    QrChqPgsFatNum: TFloatField;
    QrChqPgsFatParcela: TIntegerField;
    QrChqPgsControle: TIntegerField;
    QrChqPgsOcorreu: TIntegerField;
    QrChqPgsFatParcRef: TIntegerField;
    QrChqPgsFatGrupo: TIntegerField;
    QrChqPgsPAGOU: TFloatField;
    QrPesqTedRem_Its: TIntegerField;
    PMDiario: TPopupMenu;
    Adicionareventoaodiario1: TMenuItem;
    Gerenciardirio1: TMenuItem;
    N7: TMenuItem;
    Imprimirdiriodeitemselecionado1: TMenuItem;
    Imprimirdiriodeitenspesquisados1: TMenuItem;
    frxDsDiarioAdd: TfrxDBDataset;
    QrDiarioAdd: TmySQLQuery;
    QrDiarioAddNOME_CLI: TWideStringField;
    QrDiarioAddNOME_ENT: TWideStringField;
    QrDiarioAddNOME_DEPTO: TWideStringField;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    QrDiarioAddDiarioAss: TIntegerField;
    QrDiarioAddEntidade: TIntegerField;
    QrDiarioAddCliInt: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddDataCad: TDateField;
    QrDiarioAddDataAlt: TDateField;
    QrDiarioAddUserCad: TIntegerField;
    QrDiarioAddUserAlt: TIntegerField;
    QrDiarioAddNOME_UH_ENT: TWideStringField;
    QrDiarioAddDDeposito: TDateField;
    DsDiarioAdd: TDataSource;
    frxGER_DIARI_001_01: TfrxReport;
    QrDiarioAddDocumento: TFloatField;
    QrPesqJuridico: TSmallintField;
    N8: TMenuItem;
    Localizarremessa11: TMenuItem;
    RGJuridico: TRadioGroup;
    Label13: TLabel;
    EdCNABLot: TdmkEdit;
    N9: TMenuItem;
    EditarstatusJurdicoSelecionados1: TMenuItem;
    QrPesqTedRemCod: TIntegerField;
    RGRemessa: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtHistoricoClick(Sender: TObject);
    procedure Clientepesquisado1Click(Sender: TObject);
    procedure Emitentesacadopesquisado1Click(Sender: TObject);
    procedure Ambos2Click(Sender: TObject);
    procedure Clienteselecionado1Click(Sender: TObject);
    procedure Emitentesacadoselecionado1Click(Sender: TObject);
    procedure Ambos1Click(Sender: TObject);
    procedure QrPesqAfterClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure QrAlinItsAfterScroll(DataSet: TDataSet);
    procedure QrAlinItsBeforeClose(DataSet: TDataSet);
    procedure QrAlinItsCalcFields(DataSet: TDataSet);
    procedure QrLotPrrCalcFields(DataSet: TDataSet);
    procedure QrSomaCalcFields(DataSet: TDataSet);
    procedure EdClienteChange(Sender: TObject);
    procedure EdCPF_1Exit(Sender: TObject);
    procedure EdCPF_1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBandaChange(Sender: TObject);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCMC_7_1Change(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure EdBancoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAgenciaExit(Sender: TObject);
    procedure EdContaExit(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure CkRepassadoClick(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure CkStatusClick(Sender: TObject);
    procedure CkProrrogadoClick(Sender: TObject);
    procedure BtReciboAntecipClick(Sender: TObject);
    procedure Incluiocorrncia1Click(Sender: TObject);
    procedure Alteraocorrncia1Click(Sender: TObject);
    procedure Excluiocorrncia1Click(Sender: TObject);
    procedure IncluiPagamento2Click(Sender: TObject);
    procedure Excluipagamento2Click(Sender: TObject);
    procedure Recibodopagamento2Click(Sender: TObject);
    procedure Incluiprorrogao1Click(Sender: TObject);
    procedure IncluiQuitao1Click(Sender: TObject);
    procedure ExcluiProrrogao1Click(Sender: TObject);
    procedure N0ForastatusAutomtico1Click(Sender: TObject);
    procedure N1Forastatus1Click(Sender: TObject);
    procedure N2ForastatusBaixado1Click(Sender: TObject);
    procedure N3ForastatusMoroso1Click(Sender: TObject);
    procedure IncluiPagamento1Click(Sender: TObject);
    procedure ExcluiPagamento1Click(Sender: TObject);
    procedure Alteradevoluo1Click(Sender: TObject);
    procedure Desfazdevoluo1Click(Sender: TObject);
    procedure RecibodoPagamento1Click(Sender: TObject);
    procedure Meuparaocliente1Click(Sender: TObject);
    procedure Clienteparamim1Click(Sender: TObject);
    procedure Quitadocumento1Click(Sender: TObject);
    procedure Imprimerecibodequitao1Click(Sender: TObject);
    procedure BtChequeOcorrClick(Sender: TObject);
    procedure BtStatusClick(Sender: TObject);
    procedure BtQuitacaoClick(Sender: TObject);
    procedure BtChequeDevClick(Sender: TObject);
    procedure BtDesDepClick(Sender: TObject);
    procedure BtBorderoClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure GradeItensDblClick(Sender: TObject);
    procedure PMOcorreuPopup(Sender: TObject);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure PMStatusPopup(Sender: TObject);
    procedure PMDevolucaoPopup(Sender: TObject);
    procedure QrOcorreuAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure RGFontesClick(Sender: TObject);
    procedure QrOcorreuBeforeClose(DataSet: TDataSet);
    procedure CkClienteClick(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure Adicionareventoaodiario1Click(Sender: TObject);
    procedure Gerenciardirio1Click(Sender: TObject);
    procedure PMDiarioPopup(Sender: TObject);
    procedure Imprimirdiriodeitemselecionado1Click(Sender: TObject);
    procedure Imprimirdiriodeitenspesquisados1Click(Sender: TObject);
    procedure EditarstatusJurdicoSelecionados1Click(Sender: TObject);
    procedure PMQuitacaoPopup(Sender: TObject);
    procedure Localizarremessa11Click(Sender: TObject);
  private
    { Private declarations }
    FLct_LOIs: Integer;
    //FCPFSacado: String;
    FTempo, FUltim: TDateTime;
    FSoma_i: Integer;
    FSoma_v: Double;
    //
    procedure EmiteReciboAntecip(Tipo: Integer);
    procedure ForcaStatus(NovoStatus: Integer);
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    procedure MostraEdicao(TipoForm: TTipoFormChq; SQLType: TSQLType);
    procedure MostraGerChqDevo(SQLType: TSQLType);
    procedure MostraGerChqOcor(SQLType: TSQLType; Prorroga: Boolean);
    procedure MostraGerChqPgDv(SQLType: TSQLType);
    procedure MostraGerChqPgOc(SQLType: TSQLType);
    procedure MostraHistorico(Quem: Integer);
    procedure ReopenAlinIts(LocCod: Integer);
    procedure ReopenBanco();
    procedure ReopenLastOcor(Ocorreu: Integer);
    procedure ReopenLotPrr(Controle: Integer);
    procedure ReopenOcorP();
    procedure ReabreSomaPesq();
    function  TextoSQLPesq(TabLotX, TabLctX: String; Soma: Boolean;
              Query: TmySQLQuery): Integer;
    //Di�rio
    procedure ImprimeDiarioAdd(ID: Integer);
  public
    { Public declarations }
    FNaoDeposita: Boolean;
    FOcorP, FAlinIts, FChqPgs, FOcorreu, FLotPrr, Fq1, Fq2, Fq3, Fq0: Integer;
    FCliente: Integer;
    //
    //procedure Calcula Pagamento CHDev(Quitacao: TDateTime);
    procedure CalculaPagtoOcorP(Quitacao: TDateTime; OcorP: Integer);
    procedure Pesquisa(Forca, Reabre: Boolean);
    procedure ReopenChqPgs(LocCod: Integer);
    procedure ReopenOcorreu(Codigo: Integer);
  end;

  var
  FmGerChqMain: TFmGerChqMain;

implementation

uses UnMyObjects, Module, HistCliEmi, LotEmi, PesqCPFCNPJ, Principal, UnGOTOy,
  UMySQLModule, GerChqOcor, MyDBCheck, GerChqDevo, GerChqPgDv, GerChqPgOc,
  MyListas, ModuleLot, ModuleFin, TedC_Aux;

{$R *.DFM}

procedure TFmGerChqMain.Adicionareventoaodiario1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCNPJCPF.Value + ' - ' + QrPesqEmitente.Value + #13#10;
  //
  FmPrincipal.MostraDiarioAdd(0, QrPesqCliente.Value, QrPesqControle.Value, Texto);
end;

procedure TFmGerChqMain.Alteradevoluo1Click(Sender: TObject);
begin
  MostraEdicao(tmfChqDevo, stUpd);
end;

procedure TFmGerChqMain.Alteraocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(tmfChqOcor, stUpd);
end;

procedure TFmGerChqMain.Ambos1Click(Sender: TObject);
begin
  MostraHistorico(3);
end;

procedure TFmGerChqMain.Ambos2Click(Sender: TObject);
begin
  MostraHistorico(6);
end;

procedure TFmGerChqMain.BtBorderoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  FmPrincipal.CriaFormLot(2, 0, QrPesqCodigo.Value, QrPesqFatParcela.Value);
end;

procedure TFmGerChqMain.BtRecalculaClick(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    QrPesq.First;
    while not QrPesq.Eof do
    begin
      QrAlinIts.First;
      while not QrAlinIts.Eof do
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrLast, Dmod.MyDB, [
        'SELECT Max(Data) Data ',
        'FROM ' + CO_TabLctA,
        'WHERE FatID=' + TXT_VAR_FATID_0311,
        'AND FatParcRef=' + Geral.FF0(QrAlinItsCodigo.Value),
        '']);
        //
        DmLot.CalculaPagtoAlinIts(QrLastData.Value, QrAlinItsCodigo.Value);
        //
        QrAlinIts.Next;
      end;
      //
      QrPesq.Next;
    end;
    Pesquisa(True, False);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerChqMain.BtChequeDevClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  if (QrAlinIts.State <> dsInactive) and (QrAlinits.RecordCount = 0) and
    (QrPesqJuridico.Value = 0)
  then
    MostraEdicao(tmfChqDevo, stIns)
  else
    MyObjects.MostraPopUpDeBotao(PMDevolucao, BtChequeDev);
end;

procedure TFmGerChqMain.BtChequeOcorrClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMOcorreu, BtChequeOcorr);
end;

procedure TFmGerChqMain.BtDesDepClick(Sender: TObject);
begin
{
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE ' + CO_TabLctA);
  Dmod.QrUpdM.SQL.Add('SET AlterWeb=1, Depositado=0 ');
  Dmod.QrUpdM.SQL.Add('WHERE FatID=' + FormatFloat('0', VAR_FATID_0301));
  Dmod.QrUpdM.SQL.Add('AND FatParcela=' + FormatFloat('0', QrPesqFatParcela.Value));
  Dmod.QrUpdM.ExecSQL;
}  //
  // N�o permitir desfazer dep�sito para depositado=2 > fazer via TedC!
  //BtDesDep.Enabled := QrPesqDepositado.Value = 1;
  //
  PageControl1.ActivePageIndex := 0;
  //
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) and
    (QrPesqJuridico.Value = 0) then
  begin
    DmLot.LOI_Upd_FPC(Dmod.QrUpd, QrPesqFatParcela.Value,
      ['Depositado'], [CO_CH_DEPOSITADO_NAO]);
    BtDesDep.Enabled := False;
  end;
end;

procedure TFmGerChqMain.BtHistoricoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMHistorico, BtHistorico);
end;

procedure TFmGerChqMain.BtQuitacaoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  // N�o funciona direito
  MyObjects.MostraPopUpDeBotao(PMQuitacao, BtQuitacao);
end;

procedure TFmGerChqMain.BtReciboAntecipClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReciboAntecip, BtReciboAntecip);
end;

procedure TFmGerChqMain.BtReabreClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  Pesquisa(True, True);
end;

procedure TFmGerChqMain.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerChqMain.BtStatusClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMStatus, BtStatus);
end;

{
procedure TFmGerChqMain.Calcula Pagamento CHDev(Quitacao: TDateTime);
var
  Sit: Integer;
  Data3, Data4: String;
begin
  ReopenAlinits(QrAlinItsCodigo.Value);
(*
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := QrAlinItsCodigo.Value;
  UMyMod.AbreQuery(QrSumPg);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPg, Dmod.MyDB, [
  'SELECT SUM(MoraVal) Juros, ',
  'SUM(Credito) Pago, SUM(Desco) Desco',
  'FROM ' + CO_TabLctA,
/
  'WHERE FatID=' + TXT_VAR_FATID_0305,
  'AND FatParcRef=' + Geral.FF0(QrAlinItsCodigo.Value),
  '']);
  //
  if (QrSumPgPago.Value + QrSumPgDesco.Value) < 0.01 then Sit := 0 else if
    (QrSumPgPago.Value + QrSumPgDesco.Value) <
    (QrSumPgJuros.Value + QrAlinItsValor.Value + QrAlinItsTaxas.Value +
    QrAlinItsMulta.Value - QrAlinItsDesconto.Value)-0.009 then
    Sit := 1 else Sit := 2;
  Data4 := FormatDateTime(VAR_FORMATDATE, Quitacao);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('JurosV=:P0, ValPago=:P1, PgDesc=:P2, Status=:P3, ');
  Dmod.QrUpd.SQL.Add('Data3=:P4, Data4=:P5 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[03].AsInteger := Sit;
  Dmod.QrUpd.Params[04].AsString  := Data3;
  Dmod.QrUpd.Params[05].AsString  := Data4;
  Dmod.QrUpd.Params[06].AsInteger := QrAlinItsCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
(*
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLctA);
  Dmod.QrUpd.SQL.Add('SET AlterWeb=1, Data3="' + Data3 + '"');
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + FormatFloat('0', VAR_FATID_0301));
  Dmod.QrUpd.SQL.Add('AND FatParcela=' + FormatFloat('0', QrAlinItsChequeOrigem.Value));
  Dmod.QrUpd.ExecSQL;
*)
  // caso n�o foi desfeita a devolu��o
  if QrAlinItsChequeOrigem.Value <> 0 then
    DmLot.LOI_Upd_FPC(Dmod.QrUpd, QrAlinItsChequeOrigem.Value,
    ['Data3'], [Data3]);
  //
  ReopenAlinits(QrAlinItsCodigo.Value);
end;
}

procedure TFmGerChqMain.CalculaPagtoOcorP(Quitacao: TDateTime;
  OcorP: Integer);
var
  Sit: Integer;
begin
{
  QrSumOc.Close;
  QrSumOc.Params[0].AsInteger := OcorP;
  UMyMod.AbreQuery(QrSumOc);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOC, Dmod.MyDB, [
  'SELECT SUM(MoraVal) Juros, SUM(Credito-Debito) Pago ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(OcorP),
  '']);
  if QrSumOcPago.Value < 0.01 then
    Sit := 0
  //Est� com erro - Atualizado em 09/08/2012
  //else if QrSumOcPago.Value < (QrOcorreuValor.Value + ==>QrOcorreuTaxaV.Value<==)-0.009 then
  else if QrSumOcPago.Value < (QrOcorreuValor.Value + QrSumOcJuros.Value)-0.009 then
    Sit := 1
  else
    Sit := 2;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorP;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmGerChqMain.CkClienteClick(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.CkProrrogadoClick(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.CkRepassadoClick(Sender: TObject);
begin
  if Geral.IntInConjunto(2, CkRepassado.Value) then
  begin
    LaColigado.Enabled := True;
    EdColigado.Enabled := True;
    CBColigado.Enabled := True;
  end else begin
    LaColigado.Enabled := False;
    EdColigado.Enabled := False;
    CBColigado.Enabled := False;
  end;
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.CkStatusClick(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.Clienteparamim1Click(Sender: TObject);
begin
  EmiteReciboAntecip(1);
end;

procedure TFmGerChqMain.Clientepesquisado1Click(Sender: TObject);
begin
  MostraHistorico(4);
end;

procedure TFmGerChqMain.Clienteselecionado1Click(Sender: TObject);
begin
  MostraHistorico(1);
end;

procedure TFmGerChqMain.Desfazdevoluo1Click(Sender: TObject);
var
  Sit, Controle: Integer;
begin
  if TedC_Ax.ChequeEstaNoBanco(QrPesqTedRem_Its.Value, True) then
    Exit;
  if QrAlinIts.RecordCount = 0 then
    Geral.MensagemBox('Nenhuma devolu��o foi selecionada para exclus�o!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if QrChqPgs.RecordCount > 0 then
    Geral.MensagemBox('Exclus�o cancelada! Este cheque devolvido j� '+
    'possui pagamentos!', 'Aviso', MB_OK+MB_ICONWARNING) else
  begin
    if Geral.MensagemBox('Confirma o desfazimento da devolu��o do cheque selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLctA);
      Dmod.QrUpd.SQL.Add('SET AlterWeb=1, Devolucao=0, Data3=DDeposito ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0301);
      Dmod.QrUpd.SQL.Add('AND FatParcela=' + FormatFloat('0', QrAlinItsChequeOrigem.Value));
      Dmod.QrUpd.ExecSQL;
}
      Sit := DmLot.ObterSitOriginalDeChequeDevolvido(QrAlinItsChequeOrigem.Value);
      Controle := DmLot.QrPsqLctAControle.Value;
      //
      // Desfaz devolu��o!
      DmLot.LOI_Upd_FPC(Dmod.QrUpd, QrAlinItsChequeOrigem.Value,
      ['Sit', 'Devolucao', CO_JOKE_SQL], [Sit, 0, 'Data3=DDeposito']);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM alinits WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrAlinItsCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      //incluir na tabela de LctStep
      DmodFin.FolowStepLct(Controle, 0, CO_STEP_LCT_DESDEV);
      //
      //Calcula Pagamento CHDev(QrAlinItsData3.Value);
      DmLot.CalculaPagtoAlinIts(QrAlinItsData3.Value, QrAlinItsCodigo.Value);

      QrAlinIts.Next;
      FAlinIts := QrAlinItsCodigo.Value;
      Pesquisa(True, False);
    end;
  end;
end;

procedure TFmGerChqMain.EdAgenciaExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdBancoChange(Sender: TObject);
begin
  if not EdBanco.Focused then
    ReopenBanco();
end;

procedure TFmGerChqMain.EdBancoExit(Sender: TObject);
begin
  ReopenBanco();
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdBancoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    BtSaidaClick(Self);
end;

procedure TFmGerChqMain.EdBandaChange(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmGerChqMain.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtSaidaClick(Self);
end;

procedure TFmGerChqMain.EdChequeExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdClienteChange(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdCMC_7_1Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_1.Text;
    //EdComp.Text    := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    Pesquisa(False, False);
  end;
end;

procedure TFmGerChqMain.EdColigadoChange(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdContaExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdCPF_1Exit(Sender: TObject);
begin
  Dmod.QrLocCPF.Close;
  Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_1.Text);
  UMyMod.AbreQuery(Dmod.QrLocCPF, Dmod.MyDB);
  if Dmod.QrLocCPF.RecordCount = 0 then EdEmitente1.Text := ''
  else EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
  //
  Pesquisa(False, False);
end;

procedure TFmGerChqMain.EdCPF_1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_1.Text);
    UMyMod.AbreQuery(Dmod.QrLocCPF, Dmod.MyDB);
    case Dmod.QrLocCPF.RecordCount of
      0: Geral.MensagemBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotEmi, FmLotEmi);
        FmLotEmi.ShowModal;
        if FmLotEmi.FEmitenteNome <> '' then
          EdEmitente1.Text := FmLotEmi.FEmitenteNome;
        FmLotEmi.Destroy;
      end;
    end;
  end;
  if Key=VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then EdCPF_1.Text := VAR_CPF_PESQ;
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_1.Text);
    UMyMod.AbreQuery(Dmod.QrLocCPF, Dmod.MyDB);
    if Dmod.QrLocCPF.RecordCount = 0 then EdEmitente1.Text := ''
    else EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
  end;
end;

procedure TFmGerChqMain.EditarstatusJurdicoSelecionados1Click(Sender: TObject);
var
  Status, i, Controle: Integer;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    Status := MyObjects.SelRadioGroup('Selecione o status jur�dico', '',
                ['Sem restri��es', 'Com a��o de cobran�a'], 1, 0);
    //
    if Status > -1 then
    begin
      Screen.Cursor := crHourGlass;
      try
        if GradeItens.SelectedRows.Count > 1 then
        begin
          with GradeItens.DataSource.DataSet do
          begin
            for i:= 0 to GradeItens.SelectedRows.Count -1 do
            begin
              //GotoBookmark(pointer(GradeItens.SelectedRows.Items[i]));
              GotoBookmark(GradeItens.SelectedRows.Items[i]);
              //
              Controle := QrPesqControle.Value;
              //
              Dmod.AtualizaStatusJuridico(Status, Controle);
            end;
          end;
        end else
        begin
          Controle := QrPesqControle.Value;
          //
          Dmod.AtualizaStatusJuridico(Status, Controle);
        end;
      finally
        Screen.Cursor := crDefault;
        //
        Pesquisa(True, True);
      end;
    end;
  end;
end;

procedure TFmGerChqMain.Emitentesacadopesquisado1Click(Sender: TObject);
begin
  MostraHistorico(5);
end;

procedure TFmGerChqMain.Emitentesacadoselecionado1Click(Sender: TObject);
begin
  MostraHistorico(2);
end;

procedure TFmGerChqMain.EmiteReciboAntecip(Tipo: Integer);
var
  Texto: String;
  Valor: Double;
begin
{
  QrAntePror.Close;
  QrAntePror.Params[0].AsInteger := QrLotPrrControle.Value;
  QrAntePror. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrAntePror, Dmod.MyDB, [
  'SELECT lit.Controle ITEMLOTE, lit.VlrCompra, ',
  'prr.Controle, prr.Data1, prr.Data2, prr.Data3, ',
  'lit.Documento, lit.Banco, lit.Agencia, ',
  'lit.ContaCorrente, lit.Emitente, ',
  'lit.Credito VALORCHEQUE, oco.Valor VALORRESSARCIDO ',
  'FROM ' + CO_TabLotPrr + ' prr ',
  'LEFT JOIN ocorreu oco ON oco.Codigo=prr.Ocorrencia ',
  //'LEFT JOIN lot esits lit ON lit.Controle=prr.Codigo ',
  'LEFT JOIN ' + CO_TabLctA + ' lit ON lit.FatID=' +
  FormatFloat('0', VAR_FATID_0301) + ' AND lit.FatParcela=prr.Codigo ',
  'WHERE prr.Controle=' + FormatFloat('0', QrLotPrrControle.Value),
  '']);
  //
  Texto := 'Antecipa��o da quita��o do cheque n� ' +
    FormatFloat('000000', QrAnteProrDocumento.Value)
    +' do banco n� '+FormatFloat('000', QrAnteProrBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrAnteProrAgencia.Value) +
    ' emitida por '+ QrAnteProrEmitente.Value + ' e comprada no dia '+
    FormatDateTime('dd" de "mmmm" de "yyyy', QrAnteProrData1.Value)+
    ', sendo que o vencimento acordado na compra era para '+
    FormatDateTime('dd" de "mmmm" de "yyyy', QrAnteProrData2.Value)+
    ' e foi alterada para '+
    FormatDateTime('dd" de "mmmm" de "yyyy', QrAnteProrData3.Value);
  //
  if QrAnteProrVALORRESSARCIDO.Value < 0 then
  begin
    if Tipo = 1 then
    GOTOy.EmiteRecibo(Dmod.QrMasterDono.Value, QrPesqCliente.Value,  CO_BENEFICIARIO_0,
      -QrAnteProrVALORRESSARCIDO.Value, 0, 0, 'ACH.R-'+FormatFloat('000',
      QrAnteProrControle.Value), Texto, '', '', QrAnteProrData3.Value, 0)
    else
    begin
      Valor := Dmod.ObtemValorDeCompraRealizado(QrAnteProrITEMLOTE.Value)[0]
      + QrAnteProrVlrCompra.Value;
      GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
        QrAnteProrVALORCHEQUE.Value+Valor+QrAnteProrVALORRESSARCIDO.Value,
        0, 0, 'ACH.P-'+FormatFloat('000',
        QrAnteProrControle.Value), Texto, '', '', QrAnteProrData3.Value, 0);
    end;
  end else Geral.MensagemBox('Lan�amento deve ser de antecipa��o!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmGerChqMain.Excluiocorrncia1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da ocorr�ncia selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;
end;

procedure TFmGerChqMain.ExcluiPagamento1Click(Sender: TObject);
var
  DelFatParc: Integer;
begin
  if QrChqPgs.RecordCount = 0 then
    Geral.MensagemBox('Nenhum pagamento foi selecionado para exclus�o!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if (QrChqPgs.RecNo < QrChqPgs.RecordCount) then
    Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else if (QrChqPgsFatNum.Value >= 1) then
  begin
    if Geral.MensagemBox(
    'Somente pagamentos feitos aqui podem ser exclu�dos aqui!'
    + #13#10 +
    'Deseja abrir o lote de border� no qual o pagamento foi realizado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      FmPrincipal.CriaFormLot(2, 0, Trunc(QrChqPgsFatNum.Value),
      QrChqPgsFatParcela.Value);
  end else begin
    if Geral.MensagemBox('Confirma a exclus�o do pagamento selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      //
      DelFatParc := QrChqPgsFatParcela.Value;
      //
      if DmLot.Exclui_ChqPg(QrChqPgsControle.Value, QrChqPgsFatParcela.Value,
      QrChqPgsOcorreu.Value, QrChqPgsFatParcRef.Value, QrChqPgsFatGrupo.Value,
      False, QrChqPgsFatNum.Value) then
      begin
{
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM alin pgs WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrChqPgsFatParcela.Value;
        Dmod.QrUpd.ExecSQL;
}
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
        Dmod.QrUpd.SQL.Add('WHERE ' + FLD_CHQ_PGS + ' <> 0 AND ' +
        FLD_CHQ_PGS + '=:P0');
        Dmod.QrUpd.Params[0].AsInteger := DelFatParc;
        Dmod.QrUpd.ExecSQL;
        //
  {
        QrLast.Close;
        QrLast.Params[0].AsInteger := QrAlinItsCodigo.Value;
        UMyMod.AbreQuery(QrLast);
  }
        UnDmkDAC_PF.AbreMySQLQuery0(QrLast, Dmod.MyDB, [
        'SELECT Max(Data) Data ',
        'FROM ' + CO_TabLctA,
        'WHERE FatID=' + TXT_VAR_FATID_0311,
        'AND FatParcRef=' + Geral.FF0(QrAlinItsCodigo.Value),
        '']);
        //
        QrChqPgs.Next;
        FChqPgs := UmyMod.ProximoRegistro(QrChqPgs, 'FatParcela', QrChqPgsFatParcela.Value);
        //Calcula Pagamento CHDev(QrLastData.Value);
        DmLot.CalculaPagtoAlinIts(QrLastData.Value, QrAlinItsCodigo.Value);
        Pesquisa(True, False);
      end;
    end;
  end;
end;

procedure TFmGerChqMain.Excluipagamento2Click(Sender: TObject);
var
  Ocorreu: Integer;
begin
  ReopenLastOcor(QrOcorPOcorreu.Value);
  //
  if (QrLastOcorData.Value > QrOcorPData.Value)
  or ((QrLastOcorData.Value = QrOcorPData.Value)
  and (QrLastOcorFatParcela.Value > QrOcorPFatParcela.Value))
  then
     Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    if Geral.MensagemBox('Confirma a exclus�o deste pagamento?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorPOcorreu.Value;
        DmLot.Exclui_OcorP(QrOcorPControle.Value, QrOcorPFatParcela.Value,
          QrOcorPOcorreu.Value);
{
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorr pg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorPFatParcela.Value;
        Dmod.QrUpd.ExecSQL;
}        //
        ReopenLastOcor(Ocorreu);
        CalculaPagtoOcorP(QrLastOcorData.Value, Ocorreu);
        //
        QrOcorP.Next;
        FOcorP := QrOcorPFatParcela.Value;
        ReopenOcorreu(QrOcorreuCodigo.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
  end;
end;

procedure TFmGerChqMain.ExcluiProrrogao1Click(Sender: TObject);
var
  ProrrDd: Integer;
  Vencimento, DDeposito, Data4: String;
  Ocor, ProrrVz: Integer;
begin
  if TedC_Ax.ChequeEstaNoBanco(QrPesqTedRem_Its.Value, True) then
    Exit;
  if QrLotPrr.RecNo <> QrLotPrr.RecordCount then
  Geral.MensagemBox('Somente a �ltima prorroga��o/antecipa��o pode ser exclu�da!',
  'Aviso', MB_OK+MB_ICONWARNING) else
  begin
    if Geral.MensagemBox('Confirma a exclus�o da prorroga��o/antecipa��o?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Ocor := QrLotPrrOcorrencia.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLotPrr);
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotPrrControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenLotPrr(0);
      QrLotPrr.Last;
      ProrrDd := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData1.Value));
      if QrLotPrrData3.Value = 0 then
        DDeposito := FormatDateTime(VAR_FORMATDATE,
          UMyMod.CalculaDataDeposito(QrPesqVencimento.Value))
      else
        DDeposito := FormatDateTime(VAR_FORMATDATE, QrLotPrrData3.Value);
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1, DDeposito=:P0, ProrrVz=:P1, ');
      Dmod.QrUpd.SQL.Add('ProrrDd=:P2, Data4=:P3 WHERE Controle=:Pa');
      Dmod.QrUpd.Params[0].AsString  := DDep;
      Dmod.QrUpd.Params[1].AsInteger := QrLotPrr.RecordCount;
      Dmod.QrUpd.Params[2].AsInteger := Dias;
      Dmod.QrUpd.Params[3].AsString  := DDep;
      //
      Dmod.QrUpd.Params[4].AsInteger := QrPesqFatParcela.Value;
      Dmod.QrUpd.ExecSQL;
}
      ProrrVz := QrLotPrr.RecordCount;
      Data4 := DDeposito;
      //
      Vencimento := DDeposito;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'Vencimento', 'DDeposito', 'ProrrVz', 'ProrrDd', 'Data4'], [
      'FatID', 'FatParcela'], [
      Vencimento, DDeposito, ProrrVz, ProrrDd, Data4], [
      VAR_FATID_0301, QrPesqFatParcela.Value], True);
      //
      FLct_LOIs := QrPesqFatParcela.Value;
      FOcorreu  := QrOcorreuCodigo.Value;
      FLotPrr := QrLotPrrControle.Value;
      //
      Pesquisa(True, False);
      if Ocor <> 0 then
      begin
        if Geral.MensagemBox(
        'Deseja excluir tamb�m a ocorr�ncia da prorroga��o/antecipa��o exclu�da?',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
          Dmod.QrUpd.Params[0].AsInteger := Ocor;
          Dmod.QrUpd.ExecSQL;
          //
          if (QrOcorreu.State <> dsInactive) and (QrOcorreu.RecordCount > 0) then
          begin
            QrOcorreu.Next;
            ReopenOcorreu(QrOcorreuCodigo.Value);
          end else
            ReopenOcorreu(0);
        end;
      end;
    end;
  end;
end;

procedure TFmGerChqMain.ForcaStatus(NovoStatus: Integer);
var
  FatParcela: Integer;
begin
  if QrPesq.State = dsBrowse then
  begin
    if QrPesq.RecordCount > 0 then
    begin
      FatParcela := QrPesqFatParcela.Value;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, ['Quitado'], [
      'FatID', 'FatParcela'], [NovoStatus], [VAR_FATID_0301, FatParcela], True);
      //
      QrPesq.Close;
      UMyMod.AbreQuery(QrPesq, Dmod.MyDB);
      QrPesq.Locate('FatParcela', FatParcela, []);
    end;
  end;
end;

procedure TFmGerChqMain.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
  if FCliente > 0 then
  begin
    EdCliente.ValueVariant := FCliente;
    CBCliente.KeyValue := FCliente;
  end;
end;

procedure TFmGerChqMain.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrEmitCPF, Dmod.MyDB);
  UMyMod.AbreQuery(QrEmitBAC, Dmod.MyDB);
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigado, Dmod.MyDB);
{
  UMyMod.AbreQuery(QrOcorBank);
  UMyMod.AbreQuery(QrAlineas);
  UMyMod.AbreQuery(QrAPCD);
  PainelData.Align := alClient;
  PainelBtsE.Align := alClient;
}
  GradeItens.Align := alClient;
  DBGrid4.Align    := alClient;
  Panel5.Align     := alClient;
  Panel9.Align     := alTop;
  Panel8.Align     := alClient;
  PageControl1.ActivePageIndex := 0;
  //
  CkRepassado.Value    := 3;
  RGJuridico.ItemIndex := 2;
  RGRemessa.ItemIndex  := 2;
end;

procedure TFmGerChqMain.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerChqMain.Gerenciardirio1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCNPJCPF.Value + ' - ' + QrPesqEmitente.Value;
  //
  FmPrincipal.MostraDiarioGer(0, QrPesqCliente.Value, Geral.FF0(QrPesqControle.Value), Texto);
end;

procedure TFmGerChqMain.GradeItensDblClick(Sender: TObject);
begin
  EdControle.Text := IntToStr(QrPesqFatParcela.Value);
  //CGStatusAutoma.Value := Trunc(Power(2, CGStatusAutoma.Items.Count)-1);
  Pesquisa(True, True);
end;

procedure TFmGerChqMain.ImprimeDiarioAdd(ID: Integer);
var
  Depto: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if ID <> 0 then
      Depto := Geral.FF0(ID)
    else if Length(Depto) = 0 then
    begin
      if QrPesq.RecordCount > 40 then
      begin
        if Geral.MensagemBox('Pesquisar muitos itens pode demorar v�rios minutos.' +
          #13#10 + 'Deseja continuar assim mesmo?', 'Pergunta',
          MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
      end;
      QrPesq.First;
      while not QrPesq.Eof do
      begin
        if QrPesq.RecNo = QrPesq.RecordCount then
          Depto := Depto + Geral.FF0(QrPesqControle.Value)
        else
          Depto := Depto + Geral.FF0(QrPesqControle.Value) + ', ';
        QrPesq.Next;
      end;
    end;
    QrDiarioAdd.Close;
    QrDiarioAdd.SQL.Clear;
    QrDiarioAdd.SQL.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_UH_ENT,');
    QrDiarioAdd.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,');
    QrDiarioAdd.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,');
    QrDiarioAdd.SQL.Add('"" NOME_DEPTO, lct.Documento, lct.DDeposito, dad.*');
    QrDiarioAdd.SQL.Add('FROM diarioadd dad');
    QrDiarioAdd.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt');
    QrDiarioAdd.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade');
    QrDiarioAdd.SQL.Add('LEFT JOIN lct0001a lct ON lct.Controle=dad.Depto AND lct.FatID=' + Geral.FF0(VAR_FATID_0301));
    QrDiarioAdd.SQL.Add('WHERE dad.Depto IN(' + Depto + ')');
    QrDiarioAdd.SQL.Add('ORDER BY dad.Data, dad.Hora, NOME_UH_ENT');
    QrDiarioAdd.Open;
    //
    MyObjects.frxMostra(frxGER_DIARI_001_01, 'Pesquisa de Di�rio');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerChqMain.Imprimerecibodequitao1Click(Sender: TObject);
var
  Pagamento, Texto, Liga: String;
begin
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  UMyMod.AbreQuery(QrBco1, Dmod.MyDB);
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos ao';
  Texto := Pagamento + ' cheque n� ' + FormatFloat('000000', QrPesqDocumento.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencimento.Value);
  if QrAlinItsAlinea1.Value > 0 then
  begin
    Texto := Texto + ' e devolvido sob a al�nea n�' +
    IntToStr(QrAlinItsAlinea1.Value) + ' "' + QrAlinItsNOMEALINEA1.Value + '"';
  end;
  if QrAlinItsAlinea2.Value > 0 then
  begin
    Texto := Texto + ' e sob a al�nea n� ' +
    IntToStr(QrAlinItsAlinea2.Value) + ' "' + QrAlinItsNOMEALINEA2.Value + '"';
  end;
  if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;
  //
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrPesqValQuit.Value, 0, 0, 'CHX-'+FormatFloat('000000',
    QrPesqFatParcela.Value), Texto, '', '', Date, 0);
end;

procedure TFmGerChqMain.Imprimirdiriodeitemselecionado1Click(Sender: TObject);
begin
  ImprimeDiarioAdd(QrPesqControle.Value);
end;

procedure TFmGerChqMain.Imprimirdiriodeitenspesquisados1Click(Sender: TObject);
begin
  ImprimeDiarioAdd(0);
end;

procedure TFmGerChqMain.Incluiocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(tmfChqOcor, stIns);
end;

procedure TFmGerChqMain.IncluiPagamento1Click(Sender: TObject);
begin
  MostraEdicao(tmfChqPgDv, stIns);
end;

procedure TFmGerChqMain.IncluiPagamento2Click(Sender: TObject);
begin
  MostraEdicao(tmfChqPgOc, stIns);
end;

procedure TFmGerChqMain.Incluiprorrogao1Click(Sender: TObject);
begin
  FNaoDeposita := False;
  MostraEdicao(tmfChqPror, stIns);
end;

procedure TFmGerChqMain.IncluiQuitao1Click(Sender: TObject);
begin
  // NaoDeposita > Documento quitado?
  FNaoDeposita := True;
  MostraEdicao(tmfChqPror, stIns);
end;

procedure TFmGerChqMain.InfoTempo(Tempo: TDateTime; Texto: String;
  Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('==============================================================================');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

procedure TFmGerChqMain.Localizarremessa11Click(Sender: TObject);
var
  CNAB_Lot, FatParcela: Integer;
begin
  FatParcela := QrPesqFatParcela.Value;
  CNAB_Lot   := QrPesqTedRemCod.Value;
  //
  TedC_Ax.MostraTedCRemCab(CNAB_Lot, FatParcela);
  //
  Pesquisa(True, True);
end;

procedure TFmGerChqMain.Meuparaocliente1Click(Sender: TObject);
begin
  EmiteReciboAntecip(0);
end;

procedure TFmGerChqMain.MostraEdicao(TipoForm: TTipoFormChq; SQLType: TSQLType);
begin
  if QrPesq.State     = dsBrowse then FLct_LOIs := QrPesqFatParcela.Value;
  if QrOcorreu.State  = dsBrowse then FOcorreu  := QrOcorreuCodigo.Value;
  if QrLotPrr.State = dsBrowse then FLotPrr := QrLotPrrControle.Value;
  if QrAlinIts.State  = dsBrowse then FAlinIts  := QrAlinItsCodigo.Value;
  if QrChqPgs.State  = dsBrowse then FChqPgs  := QrChqPgsFatParcela.Value;
  //
  case TipoForm of
    tmfChqOcor: MostraGerChqOcor(SQLType, False);
    tmfChqPror: MostraGerChqOcor(SQLType, True);
    tmfChqDevo: MostraGerChqDevo(SQLType);
    tmfChqPgDv: MostraGerChqPgDv(SQLType);
    tmfChqPgOc: MostraGerChqPgOc(SQLType);
  end;
end;

procedure TFmGerChqMain.MostraGerChqDevo(SQLType: TSQLType);
begin
  if TedC_Ax.ChequeEstaNoBanco(QrPesqTedRem_Its.Value, True) then
    Exit
  else begin
    if DBCheck.CriaFm(TFmGerChqDevo, FmGerChqDevo, afmoNegarComAviso) then
    begin
      FmGerChqDevo.ImgTipo.SQLType := SQLType;
      if SQLType = stIns then
      begin
        FmGerChqDevo.EdJuros1.Text := Geral.FFT(QrPesqTAXA_COMPRA.Value, 4, siPositivo);
        FmGerChqDevo.TPData1_1.Date := QrPesqVencimento.Value;
        FmGerChqDevo.TPData1_2.Date := QrPesqVencimento.Value;
      end else
      begin
        FmGerChqDevo.EdAlinea1.ValueVariant := QrAlinItsAlinea1.Value;
        FmGerChqDevo.CBAlinea1.KeyValue := QrAlinItsAlinea1.Value;
        FmGerChqDevo.EdAlinea2.ValueVariant := QrAlinItsAlinea2.Value;
        FmGerChqDevo.CBAlinea2.KeyValue := QrAlinItsAlinea2.Value;
        FmGerChqDevo.EdTaxas1.ValueVariant := QrAlinItsTaxas.Value;
        FmGerChqDevo.EdMulta1.ValueVariant := QrAlinItsMulta.Value;
        FmGerChqDevo.EdJuros1.ValueVariant := QrAlinItsJurosP.Value;
        FmGerChqDevo.TPData1_1.Date := QrAlinItsData1.Value;
        FmGerChqDevo.TPData1_2.Date := QrAlinItsData2.Value;
      end;
      FmGerChqDevo.ShowModal;
      FmGerChqDevo.Destroy;
    end;
  end;
end;

procedure TFmGerChqMain.MostraGerChqOcor(SQLType: TSQLType; Prorroga: Boolean);
var
  Continua: Boolean;
begin
  if Prorroga then
  begin
    if TedC_Ax.ChequeEstaNoBanco(QrPesqTedRem_Its.Value, True) then
      Continua := False
    else Continua := True;
  end else Continua := True;
  if not Continua then
    Exit;
  if DBCheck.CriaFm(TFmGerChqOcor, FmGerChqOcor, afmoNegarComAviso) then
  begin
    FmGerChqOcor.ImgTipo.SQLType := SQLType;
    //
    FmGerChqOcor.FLotPrrControle := QrLotPrrControle.Value;
    FmGerChqOcor.FOcorreuCodigo := QrOcorreuCodigo.Value;
    FmGerChqOcor.FFatParcela := QrPesqFatParcela.Value;
    FmGerChqOcor.FLotPrrRecordCount := QrLotPrr.RecordCount;
    FmGerChqOcor.FForm := 'FmGerChqMain';
    FmGerChqOcor.FNaoDeposita := FNaoDeposita;
    FmGerChqOcor.FDDeposito := QrPesqDDeposito.Value;
    FmGerChqOcor.FVencimento := QrPesqVencimento.Value;
    FmGerChqOcor.FCredito:= QrPesqCredito.Value;
    FmGerChqOcor.FCliente := QrPesqCliente.Value;
    //
    if SQLType = stUpd then
    begin
      FmGerChqOcor.EdOcorrencia.Text := IntToStr(QrOcorreuOcorrencia.Value);
      FmGerChqOcor.CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
      FmGerChqOcor.TPDataO.Date := QrOcorreuDataO.Value;
      FmGerChqOcor.EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
      FmGerChqOcor.EdTaxaP.Text := Geral.FFT(QrOcorreuTaxaP.Value, 4, siPositivo);
      FmGerChqOcor.GBPror.Visible := Prorroga;
    end else
    begin
      FmGerChqOcor.EdOcorrencia.ValueVariant := 0;
      FmGerChqOcor.CBOcorrencia.KeyValue := Null;
      FmGerChqOcor.TPDataO.Date := Date;
      FmGerChqOcor.EdValor.ValueVariant := 0;
      FmGerChqOcor.EdTaxaP.ValueVariant := 0;
      //
      FmGerChqOcor.ConfiguraPr(Date);
    end;
    FmGerChqOcor.ShowModal;
    FmGerChqOcor.Destroy;
  end;
end;

procedure TFmGerChqMain.MostraGerChqPgDv(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGerChqPgDv, FmGerChqPgDv, afmoNegarComAviso) then
  begin
    FmGerChqPgDv.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
    begin
      FmGerChqPgDv.FPrimeiroPagto := QrChqPgs.RecordCount = 0;
      FmGerChqPgDv.ConfiguraPgCHDev(Date);
      FmGerChqPgDv.CalculaJurosDev();
    end else begin
      FmGerChqPgDv.FPrimeiroPagto := QrChqPgs.RecNo = 1;
    end;
    FmGerChqPgDv.ShowModal;
    FmGerChqPgDv.Destroy;
  end;
end;

procedure TFmGerChqMain.MostraGerChqPgOc(SQLType: TSQLType);
begin
  if (SQLType <> stIns) and DmLot.AvisoDeNaoUpd(3) then Exit;
  //
  if DBCheck.CriaFm(TFmGerChqPgOc, FmGerChqPgOc, afmoNegarComAviso) then
  begin
    FmGerChqPgOc.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
    begin
      FmGerChqPgOc.ConfiguraPgOC();
      FmGerChqPgOc.CalculaJurosOcor();
      FmGerChqPgOc.CalculaAPagarOcor();
    end;
    FmGerChqPgOc.ShowModal;
    FmGerChqPgOc.Destroy;
  end;
end;

procedure TFmGerChqMain.MostraHistorico(Quem: Integer);
begin
  Application.CreateForm(TFmHistCliEmi, FmHistCliEmi);
  case Quem of
    1:
    begin
      FmHistCliEmi.EdCliente.ValueVariant := QrPesqCliente.Value;
      FmHistCliEmi.CBCliente.KeyValue     := QrPesqCliente.Value;
    end;
    2: FmHistCliEmi.EdCPF1.Text           := QrPesqCNPJ_TXT.Value;
    3:
    begin
      FmHistCliEmi.EdCliente.ValueVariant := QrPesqCliente.Value;
      FmHistCliEmi.CBCliente.KeyValue     := QrPesqCliente.Value;
      FmHistCliEmi.EdCPF1.Text            := QrPesqCNPJ_TXT.Value;
    end;
    4:
    begin
      FmHistCliEmi.EdCliente.ValueVariant := EdCliente.ValueVariant;
      FmHistCliEmi.CBCliente.KeyValue     := EdCliente.ValueVariant;
    end;
    5: FmHistCliEmi.EdCPF1.Text           := EdCPF_1.Text;
    6:
    begin
      FmHistCliEmi.EdCliente.ValueVariant := EdCliente.ValueVariant;
      FmHistCliEmi.CBCliente.KeyValue     := EdCliente.ValueVariant;
      FmHistCliEmi.EdCPF1.Text            := EdCPF_1.Text;
    end;
  end;
  FmHistCliEmi.Pesquisa;
  FmHistCliEmi.ShowModal;
  FmHistCliEmi.Destroy;
end;

procedure TFmGerChqMain.N0ForastatusAutomtico1Click(Sender: TObject);
begin
  ForcaStatus(0);
end;

procedure TFmGerChqMain.N1Forastatus1Click(Sender: TObject);
begin
  ForcaStatus(-1);
end;

procedure TFmGerChqMain.N2ForastatusBaixado1Click(Sender: TObject);
begin
  ForcaStatus(-2);
end;

procedure TFmGerChqMain.N3ForastatusMoroso1Click(Sender: TObject);
begin
  ForcaStatus(-3);
end;

procedure TFmGerChqMain.Pesquisa(Forca, Reabre: Boolean);
var
  Ok: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrPesq.Close;
    if not Reabre then Exit;
    if CkStatus.Value = 0 then Exit;
    //
    GBData.Visible := False;
    //
    if Forca then Ok := 1 else Ok := 0;
    QrPesq.Close;
    QrPesq.SQL.Clear;
    Ok := Ok + TextoSQLPesq(CO_TabLotA, CO_TabLctA, False, QrPesq);
    if RGFontes.ItemIndex > 0 then
    begin
      QrPesq.SQL.Add('');
      QrPesq.SQL.Add('UNION');
      QrPesq.SQL.Add('');
      Ok := Ok + TextoSQLPesq('lot0001b', 'lct0001b', False, QrPesq);
    end;
    if RGFontes.ItemIndex > 1 then
    begin
      QrPesq.SQL.Add('');
      QrPesq.SQL.Add('UNION');
      QrPesq.SQL.Add('');
      Ok := Ok + TextoSQLPesq('lot0001d', 'lct0001d', False, QrPesq);
    end;
    if Ok > 0 then
    begin
      QrPesq.SQL.Add('');
      QrPesq.SQL.Add('ORDER BY Vencimento, DDeposito');
      //
      UMyMod.AbreQuery(QrPesq, Dmod.MyDB);
      //
      if QrPesq.RecordCount > 0 then
        GBData.Visible := True;
      if FLct_LOIs <> 0 then
      begin
        QrPesq.Locate('FatParcela', FLct_LOIs, []);
        InfoTempo(Now, 'Localiza��o de cheque na pesquisa', False);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerChqMain.PMDevolucaoPopup(Sender: TObject);
var
  Enab, Enab2, Enab3, Enab4: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrAlinIts.State <> dsInactive) and (QrAlinIts.RecordCount > 0);
  Enab3 := (QrChqPgs.State <> dsInactive) and (QrChqPgs.RecordCount > 0);
  Enab4 := (Enab) and (QrPesqJuridico.Value = 0);
  //
  IncluiPagamento1.Enabled := Enab and Enab2 and Enab4;
  ExcluiPagamento1.Enabled := Enab and Enab2 and Enab3 and Enab4;
  //
  Alteradevoluo1.Enabled := Enab and Enab2 and Enab4;
  Desfazdevoluo1.Enabled := Enab and Enab2 and Enab4;
  //
  RecibodoPagamento1.Enabled := Enab and Enab2 and Enab3;
end;

procedure TFmGerChqMain.PMDiarioPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  Adicionareventoaodiario1.Enabled := Enab;
  Gerenciardirio1.Enabled          := Enab;
  Localizarremessa11.Enabled       := Enab;
end;

procedure TFmGerChqMain.PMOcorreuPopup(Sender: TObject);
var
  Enab, Enab2, Enab3, Enab4: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrOcorreu.State <> dsInactive) and (QrOcorreu.RecordCount > 0);
  Enab3 := (QrOcorP.State <> dsInactive) and (QrOcorP.RecordCount > 0);
  Enab4 := (Enab) and (QrPesqJuridico.Value = 0);
  //
  Incluiocorrncia1.Enabled := Enab and Enab4;
  Alteraocorrncia1.Enabled := Enab and Enab2 and Enab4;
  Excluiocorrncia1.Enabled := Enab and Enab2 and Enab4;
  //
  IncluiPagamento2.Enabled := Enab and Enab2 and Enab4;
  Excluipagamento2.Enabled := Enab and Enab2 and Enab3 and Enab4;
  //
  Recibodopagamento2.Enabled := Enab and Enab2 and Enab3;
end;

procedure TFmGerChqMain.PMQuitacaoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) and
            (QrPesqJuridico.Value = 0);
  //
  Quitadocumento1.Enabled := Enab;
end;

procedure TFmGerChqMain.PMStatusPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrLotPrr.State <> dsInactive) and (QrLotPrr.RecordCount > 0);
  Enab3 := (Enab) and (QrPesqJuridico.Value = 0);
  //
  Incluiprorrogao1.Enabled := Enab and Enab3;
  IncluiQuitao1.Enabled    := Enab and Enab2 and Enab3;
  ExcluiProrrogao1.Enabled := Enab and Enab2 and Enab3;
  //
  N0ForastatusAutomtico1.Enabled := Enab and Enab3;
  N1Forastatus1.Enabled          := Enab and Enab3;
  N2ForastatusBaixado1.Enabled   := Enab and Enab3;
  N3ForastatusMoroso1.Enabled    := Enab and Enab3;
end;

procedure TFmGerChqMain.QrAlinItsAfterScroll(DataSet: TDataSet);
begin
  ReopenChqPgs(QrChqPgsFatParcela.Value);
end;

procedure TFmGerChqMain.QrAlinItsBeforeClose(DataSet: TDataSet);
begin
  QrChqPgs.Close;
end;

procedure TFmGerChqMain.QrAlinItsCalcFields(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  QrAlinItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAlinItsCPF.Value);
  QrAlinItsDATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData1.Value);
  QrAlinItsDATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData2.Value);
  QrAlinItsDATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData3.Value);
  QrAlinItsSALDO.Value := QrAlinItsValor.Value + QrAlinItsTaxas.Value +
    QrAlinItsMulta.Value + QrAlinItsJurosV.Value - QrAlinItsValPago.Value -
    QrAlinItsDesconto.Value - QrAlinItsPgDesc.Value;
  //
{ 2012-01-05
  QrAlinItsATUAL.Value := DMod.ObtemValorAtualizado(
    QrAlinItsCliente.Value, QrAlinItsStatus.Value, QrAlinItsData1.Value, Date,
    QrAlinItsData3.Value, QrAlinItsValor.Value, QrAlinItsJurosV.Value,
    QrAlinItsDesconto.Value, QrAlinItsValPago.Value + QrAlinItsPgDesc.Value,
    QrAlinItsJurosP.Value, False);
}
  // Deveria ser informado a multa e a taxa do alinits!
  QrAlinItsATUAL.Value := DMod.ObtemValorAtualizado(
    QrAlinItsCliente.Value, QrAlinItsStatus.Value, QrAlinItsData1.Value, Date,
    QrAlinItsData3.Value, QrAlinItsSALDO.Value, (*QrAlinItsJurosV.Value*)0,
    (*QrAlinItsDesconto.Value*)0, (*QrAlinItsValPago.Value + QrAlinItsPgDesc.Value*)0,
    QrAlinItsJurosP.Value, False);
// Fim 2012-01-05
  Screen.Cursor := crDefault;
end;

procedure TFmGerChqMain.QrLotPrrCalcFields(DataSet: TDataSet);
begin
  QrLotPrrDIAS_1_3.Value := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData1.Value));
  QrLotPrrDIAS_2_3.Value := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData2.Value));
end;

procedure TFmGerChqMain.QrOcorreuAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorP();
end;

procedure TFmGerChqMain.QrOcorreuBeforeClose(DataSet: TDataSet);
begin
  QrOcorP.Close;
end;

procedure TFmGerChqMain.QrOcorreuCalcFields(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  //
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcorreuCliente.Value, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
    QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
    QrOcorreuPago.Value, QrOcorreuTaxaP.Value, False);
  Screen.Cursor := crDefault;
end;

procedure TFmGerChqMain.QrPesqAfterClose(DataSet: TDataSet);
begin
  BtChequeOcorr.Enabled   := False;
  BtChequeDev.Enabled     := False;
  BtStatus.Enabled        := False;
  BtQuitacao.Enabled      := False;
end;

procedure TFmGerChqMain.QrPesqAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  InfoTempo(Now, 'Reabertura de pesquisa', True);
  //
  ReabreSomaPesq;
  //
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  BtHistorico.Enabled   := Enab;
  BtChequeOcorr.Enabled := Enab;
  BtStatus.Enabled      := Enab;
  BtQuitacao.Enabled    := Enab;
  BtChequeDev.Enabled   := Enab;
  BtDesDep.Enabled      := Enab;
  BtBordero.Enabled     := Enab;
  BtRecalcula.Enabled   := Enab; 
end;

procedure TFmGerChqMain.QrPesqAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  InfoTempo(Now, 'Rolagem da tabela', False);
  InfoTempo(Now, 'Reabertura de tabelas', FTempo=0);
  ReopenOcorreu(0);
  InfoTempo(Now, 'Ocorr�ncias', False);
  ReopenLotPrr(0);
  InfoTempo(Now, 'Prorroga��es', False);
  ReopenAlinIts(0);
  InfoTempo(Now, 'Cheques sem fundo', False);
  Screen.Cursor := crDefault;
  Memo1.Lines.Add('========================================== Fim reaberturas');
  FTempo := 0;
  //
  // N�o permitir desfazer dep�sito para depositado=2 > via TedC!
  BtDesDep.Enabled := QrPesqDepositado.Value = CO_CH_DEPOSITADO_MANUAL;
end;

procedure TFmGerChqMain.QrPesqBeforeClose(DataSet: TDataSet);
begin
  QrSoma.Close;
  //
  EdSomaI.Text := '0';
  EdSomaV.Text := '0,00';
  //
  QrAlinIts.Close;
  QrChqPgs.Close;
  QrLotPrr.Close;
  QrOcorreu.Close;
  //
  BtHistorico.Enabled   := False;
  BtChequeOcorr.Enabled := False;
  BtStatus.Enabled      := False;
  BtQuitacao.Enabled    := False;
  BtChequeDev.Enabled   := False;
  BtDesDep.Enabled      := False;
  BtBordero.Enabled     := False;
  BtRecalcula.Enabled   := False;
end;

procedure TFmGerChqMain.QrPesqCalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
begin
  if CkCalcFields.Checked then
  begin
    InfoTempo(Now, 'In�cio c�lculos autom�ticos', False);
    Screen.Cursor := crHourGlass;
    QrPesqCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesqCNPJCPF.Value);
    //
    Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesqFatParcela.Value);
    QrPesqTAXA_COMPRA.Value := QrPesqTxaCompra.Value + Taxas[0];
    //
    QrPesqNOMESTATUS.Value := MLAGeral.NomeStatusPgto3(QrPesqQuitado.Value,
      QrPesqDDeposito.Value, Date, (*QrPesqNOMECOLIGADO.Value*)'', True,
      QrPesqDevolucao.Value, QrPesqStatusDev.Value, QrPesqProrrVz.Value,
      QrPesqProrrDd.Value, QrPesqNaoDeposita.Value, QrPesqRepCli.Value);
    QrPesqTEL1CLI_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrPesqTEL1CLI.Value);
    InfoTempo(Now, 'Fim c�lculos autom�ticos', False);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerChqMain.QrSomaCalcFields(DataSet: TDataSet);
begin
  FSoma_i := 0;
  FSoma_v := 0;
  //
  while not QrSoma.Eof do
  begin
    FSoma_i := FSoma_i + QrSomaItens.Value;
    FSoma_v := FSoma_v + QrSomaValor.Value;
    QrSoma.Next;
  end;
  EdSomaI.Text := IntToStr(FSoma_i);
  EdSomaV.Text := Geral.FFT(FSoma_v, 2, siNegativo);
end;

procedure TFmGerChqMain.Quitadocumento1Click(Sender: TObject);
const
  OcorAPCD = -1;
  Desco = 0;
var
  ValQuit: Double;
  //DevolPg, OcorP, GenAPCD,
  CartDep, Localiz, AlinIts, Ocorreu: Integer;
  //
  LocFP, FatParcela, Controle, FatID_Sub, Genero, Cliente, FatParcRef: Integer;
  FatNum, ValPrinc, ValPagar, Valor, MoraVal: Double;
  DataCH, Dta: TDateTime;
  //
  Gen_0303, Gen_0304, Gen_0305, Gen_0307: Integer;
  ValTaxas, ValMulta, ValJuros, ValDesco, ValPende: Double;
begin
  if TedC_Ax.ChequeEstaNoBanco(QrPesqTedRem_Its.Value, True) then
    Exit;
  Screen.Cursor := crHourGlass;
  LocFP := QrPesqFatParcela.Value;
  try
    if ((QrOcorreu.State = dsInactive) or (QrOcorreu.RecordCount = 0))
      and ((QrAlinIts.State = dsInactive) or (QrAlinIts.RecordCount = 0)) then
    begin
      Geral.MensagemBox('N�o h� nenhuma ocorr�ncia ou devolu��o para ser quitado!',
        'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Ocorreu := QrOcorreuCodigo.Value;
    AlinIts := QrAlinItsCodigo.Value;
    ValQuit := 0;

    QrOcorreu.First;
    while not QrOcorreu.Eof do
    begin
      //Genero := QrOcorreuPlaGen.Value;
      if MyObjects.FIC(QrOcorreuPlaGen.Value = 0, nil,
      'A ocorr�ncia cadastro n� ' + Geral.FF0(QrOcorreuOcorrencia.Value) +
      ' n�o possui conta (do plano de contas) atrelada!') then Exit;
      //
      QrOcorreu.Next;
    end;
    if QrAlinItsATUAL.Value > 0 then
    begin
      ValQuit := QrAlinItsATUAL.Value;
{
      DevolPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'Alin Pgs', 'Alin Pgs', 'Codigo');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO alin pgs SET AlterWeb=1, ');
      Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2');
      Dmod.QrUpd.SQL.Add(', AlinIts=:Pa, Codigo=:Pb');
      Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpd.Params[01].AsFloat   := QrAlinItsATUAL.Value;
      Dmod.QrUpd.Params[02].AsFloat   := QrAlinItsATUAL.Value-QrAlinItsSALDO.Value;
      //
      Dmod.QrUpd.Params[03].AsInteger := QrAlinItsCodigo.Value;
      Dmod.QrUpd.Params[04].AsInteger := DevolPg;
      Dmod.QrUpd.ExecSQL;
}
      //
      FatNum := 0; // n�o tem! � no cheque!
      FatParcela := 0;
      Controle := 0;
      FatID_Sub := QrAlinItsAlinea1.Value;
        // genero verificado se = 0 em loop acima!
      Cliente := QrAlinItsCliente.Value;
      Ocorreu := OcorAPCD;
      FatParcRef := QrAlinItsCodigo.Value;
      Valor := QrAlinItsATUAL.Value-QrAlinItsSALDO.Value;
      ValPagar := QrAlinItsATUAL.Value;
      Dta := Date;
      DataCH := 0; // ?????
      //
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0305, Gen_0305, tpCred, True) then
        Exit;
      if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0307, Gen_0307, tpDeb, True) then
        Exit;
      //
      if QrChqPgs.RecordCount = 0 then
      begin
        ValTaxas := QrAlinItsTaxas.Value;
        ValMulta := QrAlinItsMulta.Value;
      end else begin
        ValTaxas := 0;
        ValMulta := 0;
      end;
      ValDesco := 0;
      ValJuros := Valor - ValTaxas - ValMulta;
      ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros;// - ValDesco;
      ValPende := 0; // Paga Tudo!
      CartDep := 0; // Pagamento � vista

{
      DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub, Genero,
      Cliente, Ocorreu, FatParcRef, FatNum, Pago, MoraVal, Desco, DataCH, Dta);
}
      DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub,
      Gen_0303, Gen_0304, Gen_0305, Gen_0307, Cliente, Ocorreu, FatParcRef,
      FatNum, ValPagar(*, MoraVal, Desco*), DataCH, Dta,
      ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende, CartDep);
      //
      //Calcula Pagamento CHDev(Date);
      DmLot.CalculaPagtoAlinIts(date, QrAlinItsCodigo.Value);
    end;
    QrOcorreu.First;
    while not QrOcorreu.Eof do
    begin
      if QrOcorreuATUALIZADO.Value > 0 then
      begin
        ValQuit := ValQuit + QrOcorreuATUALIZADO.Value;
{
        Ocor rPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'Ocor rPG', 'Ocor rPG', 'Codigo');
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocor rpg SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
        Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
        Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[01].AsFloat   := QrOcorreuATUALIZADO.Value-QrOcorreuSALDO.Value;
        Dmod.QrUpd.Params[02].AsFloat   := QrOcorreuATUALIZADO.Value;
        Dmod.QrUpd.Params[03].AsInteger := 0;
        //
        Dmod.QrUpd.Params[04].AsInteger := QrOcorreuCodigo.Value;
        Dmod.QrUpd.Params[05].AsInteger := Ocor rPg;
        Dmod.QrUpd.ExecSQL;
}
        FatID_Sub := QrOcorreuOcorrencia.Value;
        Genero := QrOcorreuPlaGen.Value;
        Cliente := QrOcorreuCliente.Value;
        Ocorreu := QrOcorreuCodigo.Value;
        FatParcRef := QrOcorreuLOIS.Value;
        //
        FatNum := 0; // n�o tem! � no cheque!
        FatParcela := 0;
        Controle := 0;
        Valor := QrOcorreuATUALIZADO.Value;
        MoraVal := QrOcorreuATUALIZADO.Value-QrOcorreuSALDO.Value;
        Dta := Date;
        //
        if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
        FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef,
        FatNum, Valor, MoraVal, Dta, 0) then
        begin
          CalculaPagtoOcorP(Date, QrOcorreuCodigo.Value);
        end;
      end;
      QrOcorreu.Next;
    end;
    Dmod.QrUpd.SQL.Clear;
{
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1, ValQuit=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[1].AsInteger := QrPesqFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'ValQuit'], ['FatID', 'FatParcela'], [
    ValQuit], [VAR_FATID_0301, QrPesqFatParcela.Value], True);
    //
    Localiz := QrPesqFatParcela.Value;
    QrPesq.Close;
    EdControle.Text := IntToStr(LocFP);
    CkStatus.SetMaxValue;
    //UMyMod.AbreQuery(QrPesq);
    Pesquisa(True, True);
    QrPesq.Locate('FatParcela', Localiz, []);
    ReopenAlinIts(AlinIts);
    ReopenOcorreu(Ocorreu);
    if Geral.MensagemBox(
    'Deseja imprimir um recibo com a soma dos recebimentos efetuados?',
    'Emiss�o de Recibo', MB_YESNO+MB_ICONQUESTION) = ID_YES then
      Imprimerecibodequitao1Click(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerChqMain.ReabreSomaPesq();
begin
  Screen.Cursor := crHourGlass;
  try
    QrSoma.Close;
    QrSoma.SQL.Clear;
    TextoSQLPesq(CO_TabLotA, CO_TabLctA, True, QrSoma);
    if RGFontes.ItemIndex > 0 then
    begin
      QrSoma.SQL.Add('');
      QrSoma.SQL.Add('UNION');
      QrSoma.SQL.Add('');
      TextoSQLPesq('lot0001b', 'lct0001b', True, QrSoma);
    end;
    if RGFontes.ItemIndex > 1 then
    begin
      QrSoma.SQL.Add('');
      QrSoma.SQL.Add('UNION');
      QrSoma.SQL.Add('');
      TextoSQLPesq('lot0001d', 'lct0001d', True, QrSoma);
    end;
    UMyMod.AbreQuery(QrSoma, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmGerChqMain.RecibodoPagamento1Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  UMyMod.AbreQuery(QrBco1, Dmod.MyDB);
  Pagamento := MLAGeral.NomeStatusPgto5(QrPesqQuitado.Value,
    QrPesqDDeposito.Value, Date, '', True, QrPesqDevolucao.Value,
    QrPesqStatusDev.Value, QrPesqProrrVz.Value, QrPesqProrrDd.Value, True, False);
  Texto := Pagamento + ' do cheque n� ' + FormatFloat('000000', QrPesqDocumento.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencimento.Value);
  if QrAlinItsAlinea1.Value > 0 then
  begin
    Texto := Texto + ' e devolvido sob a al�nea n�' +
    IntToStr(QrAlinItsAlinea1.Value) + ' "' + QrAlinItsNOMEALINEA1.Value + '"';
  end;
  if QrAlinItsAlinea2.Value > 0 then
  begin
    Texto := Texto + ' e sob a al�nea n� ' +
    IntToStr(QrAlinItsAlinea2.Value) + ' "' + QrAlinItsNOMEALINEA2.Value + '"';
  end;
  //
  //if QrLctDebito.Value > 0 then
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrChqPgsPAGOU.Value, 0, 0, 'CHD-'+FormatFloat('000',
    QrChqPgsFatParcela.Value), Texto, '', '', QrChqPgsData.Value, 0);
end;

procedure TFmGerChqMain.Recibodopagamento2Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  UMyMod.AbreQuery(QrBco1, Dmod.MyDB);
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorP.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' o cheque n� ' + FormatFloat('000000', QrPesqDocumento.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencimento.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita o cheque';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value, CO_BENEFICIARIO_0,
    QrOcorPPago.Value, 0, 0, 'CHO-'+FormatFloat('000',
    QrOcorPFatParcela.Value), Texto, '', '', QrOcorPData.Value, 0);
end;

procedure TFmGerChqMain.ReopenAlinIts(LocCod: Integer);
begin
  QrAlinIts.Close;
  QrAlinIts.SQL.Clear;
  QrAlinIts.SQL.Add('SELECT al1.Nome NOMEALINEA1, al2.Nome NOMEALINEA2, ai.*');
  QrAlinIts.SQL.Add('FROM alinits ai');
  QrAlinIts.SQL.Add('LEFT JOIN alineas al1 ON al1.Codigo=ai.Alinea1');
  QrAlinIts.SQL.Add('LEFT JOIN alineas al2 ON al2.Codigo=ai.Alinea2');
  QrAlinIts.SQL.Add('WHERE ai.ChequeOrigem=:P0');
  QrAlinIts.Params[0].AsInteger := QrPesqFatParcela.Value;
  UMyMod.AbreQuery(QrAlinIts, Dmod.MyDB);
  if LocCod > 0 then QrAlinIts.Locate('Codigo', LocCod, []);
end;

procedure TFmGerChqMain.ReopenChqPgs(LocCod: Integer);
begin
{
  QrChqPgs.Close;
  QrChqPgs.Params[0].AsInteger := QrAlinItsCodigo.Value;
  UMyMod.AbreQuery(QrChqPgs);
}
//SQL original estranha!!! 
{
  SQL Original Estranha!!!
  UnDmkDAC_PF.AbreMySQLQuery0(QrChqPgs, Dmod.MyDB, [
  'SELECT ali.Vencto, ali.DDeposito, ',
  'ali.Banco, ali.Agencia, ali.Conta, ali.Cheque, ',
  'ali.CPF, ali.Emitente, alp.* ',
  'FROM ' + CO_TabLctA + ' alp ',
  'LEFT JOIN alinits ali ON ali.Codigo=alp.FatParcela ',
  //'WHERE alp.FatID=' + TXT_VAR_FATID_0305,
  'WHERE alp.FatID=' + TXT_VAR_FATID_0311,
  'AND alp.FatParcRef=' + Geral.FF0(QrAlinItsCodigo.Value),
  'ORDER BY alp.Data',
  '']);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrChqPgs, Dmod.MyDB, [
  'SELECT ',
  '(alp.Credito + alp.MoraVal + alp.TaxasVal + ',
  '  alp.MultaVal - alp.DescoVal) PAGOU, ',
  'alp.Credito, alp.MoraVal,  alp.DescoVal, ',
  'alp.MultaVal, alp.TaxasVal, alp.Vencimento, alp.Banco, ',
  'alp.Agencia, alp.ContaCorrente, alp.Documento, ',
  'alp.CNPJCPF, alp.Emitente,  alp.Data, alp.FatNum, ',
  'alp.FatParcela,  alp.Controle, alp.Ocorreu, ',
  'alp.FatParcRef, alp.FatGrupo ',
  'FROM ' + CO_TabLctA + ' alp ',
  'WHERE alp.FatID=' + TXT_VAR_FATID_0311,
  'AND alp.FatParcRef=' + Geral.FF0(QrAlinItsCodigo.Value),
  'ORDER BY alp.Data',
  '']);
  //
  if LocCod > 0 then QrChqPgs.Locate('FatParcela', LocCod, [])
  else QrChqPgs.Locate('FatParcela', FChqPgs, []);
end;

procedure TFmGerChqMain.ReopenBanco();
begin
  QrBanco.Close;
  QrBanco.Params[0].AsString := EdBanco.Text;
  UMyMod.AbreQuery(QrBanco, Dmod.MyDB);
end;

procedure TFmGerChqMain.ReopenLastOcor(Ocorreu: Integer);
begin
{
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorPOcorreu.Value;
  UMyMod.AbreQuery(QrLastOcor);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastOcor, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(Ocorreu),
  'ORDER BY Data Desc, FatID Desc ',
  '']);
  //
end;

procedure TFmGerChqMain.ReopenLotPrr(Controle: Integer);
begin
{
  QrLotPrr.Close;
  QrLotPrr.Params[00].AsInteger := QrPesqFatParcela.Value;
  UMyMod.AbreQuery(QrLotPrr);
}

{
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotPrr, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_TabLotPrr,
  'WHERE Codigo=' + FormatFloat('0', QrPesqFatParcela.Value),
  'ORDER BY Controle ',
  '']);
  //
  if Controle <> 0 then
    QrLotPrr.Locate('Controle', Controle, [])
  else
    QrLotPrr.Locate('Controle', FLotPrr, []);
}
  DmLot.ReopenLotPrr(QrlotPrr, QrPesqFatParcela.Value, Controle, FLotPrr);
end;

procedure TFmGerChqMain.ReopenOcorreu(Codigo: Integer);
begin
{
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrPesqFatParcela.Value;
  UMyMod.AbreQuery(QrOcorreu);
}


{
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorreu, Dmod.MyDB, [
  'SELECT  ob.Nome NOMEOCORRENCIA, ob.PlaGen, ',
  'oc.' + FLD_LOIS + ' LOIS, oc.* ',
  'FROM ocorreu oc ',
  'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
  'WHERE oc.' + FLD_LOIS + '=' + FormatFloat('0', QrPesqFatParcela.Value),
  '']);
  //
  if Codigo <> 0 then
    QrOcorreu.Locate('Codigo', Codigo, [])
  else
    QrOcorreu.Locate('Codigo', FOcorreu, []);
}

  DmLot.ReopenOcorreu(QrOcorreu, QrPesqFatParcela.Value, Codigo, FOcorreu);
end;

procedure TFmGerChqMain.ReopenOcorP();
begin
{
  QrOcorP.Close;
  QrOcorP.Params[0].AsInteger := QrOcorreuCodigo.Value;
  UMyMod.AbreQuery(QrOcorP);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorP, Dmod.MyDB, [
  'SELECT lct.Ocorreu, lct.Data, lct.FatParcela, ',
  'lct.Controle, lct.FatNum, lct.MoraVal, ',
  'lct.Credito - lct.Debito Pago ',
  'FROM ' + CO_TabLctA + ' lct ',
  'WHERE lct.FatID=' + TXT_VAR_FATID_0304,
  'AND lct.Ocorreu=' + Geral.FF0(QrOcorreuCodigo.Value),
  '']);
  //
  QrOcorP.Locate('FatParcela', FOcorP, []);
end;

procedure TFmGerChqMain.RGFontesClick(Sender: TObject);
begin
  Pesquisa(False, False);
end;

function TFmGerChqMain.TextoSQLPesq(TabLotX, TabLctX: String; Soma: Boolean;
  Query: TmySQLQuery): Integer;
const
  //  Aqui ajuda a definir o movimento do cheque?
  ListaSQL: array [1..6] of String = (
  (*Devolvido*) ' (lct.Devolucao<>0 AND ai.Status in (0,1)) AND lct.Quitado>-2 ',
  (*Aberto   *) ' (lct.Devolucao=0 AND lct.DDeposito>=SYSDATE()) AND lct.Quitado>-2 ',
  (*Liquidado*) ' (lct.Devolucao=0 AND lct.DDeposito<SYSDATE()) AND lct.Quitado>-2 ',
  (*Quitado  *) ' (lct.Devolucao<>0 AND ai.Status in (2,3)) AND lct.Quitado>-2 ',
  (*Baixado  *) ' (lct.Quitado=-2) ',
  (*Moroso   *) ' (lct.Quitado=-3) ');
var
  i, m, j, Ok, Coligado, Controle, CNAB_Lot, Juridico, Remessa: Integer;
  LIGA_OR: Boolean;
begin
  Juridico := RGJuridico.ItemIndex;
  Remessa  := RGRemessa.ItemIndex;
  CNAB_Lot := EdCNABLot.ValueVariant;
  Liga_OR  := False;
  Ok       := 0;
  if Soma then
  begin
    Query.SQL.Add('SELECT COUNT(lct.FatParcela) Itens, SUM(lct.Credito) Valor ');
  end else begin
    Query.SQL.Add('SELECT lot.Codigo, lot.Cliente, lct.Documento, lct.FatParcela, ');
    Query.SQL.Add('lct.Data, lct.TxaCompra, lct.DCompra, lct.Credito, lct.Desco, ');
    Query.SQL.Add('lct.Vencimento, lct.Quitado, lct.Devolucao, ai.Status STATUSDEV, ');
    Query.SQL.Add('lct.ProrrVz, lct.ProrrDd, lct.Emitente, lct.ValQuit, ');
    Query.SQL.Add('IF(co.Tipo=0, co.RazaoSocial, co.Nome) NOMECOLIGADO,');
    Query.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, ');
    Query.SQL.Add('lct.DDeposito, lct.CNPJCPF, lct.NaoDeposita, lct.RepCli, ');
    Query.SQL.Add('lct.Banco, lct.Agencia, lct.ContaCorrente, lct.Depositado, ');
    Query.SQL.Add('IF(cl.Tipo=0, cl.ETe1, cl.PTe1) TEL1CLI, lct.Descricao, ');
    Query.SQL.Add('lct.Controle, lct.TedRem_Its, lct.Juridico, te.Codigo TedRemCod  ');
  end;
  Query.SQL.Add('FROM ' + TabLctX + ' lct');
  Query.SQL.Add('LEFT JOIN ' + TabLotX + ' lot ON lot.Codigo=lct.FatNum');
  Query.SQL.Add('LEFT JOIN tedcremits te ON te.Controle=lct.TedRem_Its ');
  Query.SQL.Add('LEFT JOIN repasits   ri ON ri.Origem=lct.FatParcela');
  Query.SQL.Add('LEFT JOIN repas      re ON re.Codigo=ri.Codigo');
  Query.SQL.Add('LEFT JOIN entidades  co ON co.Codigo=re.Coligado');
  Query.SQL.Add('LEFT JOIN entidades  cl ON cl.Codigo=lot.Cliente');
  Query.SQL.Add('LEFT JOIN alinits    ai ON ai.ChequeOrigem=lct.FatParcela');
  Query.SQL.Add('WHERE lct.FatID=301 ');
  Query.SQL.Add('AND lot.Tipo=0');
  Query.SQL.Add('AND lot.TxCompra + lot.ValValorem + ' +
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  Coligado := EdColigado.ValueVariant;
  Controle := EdControle.ValueVariant;
  case CkRepassado.Value of
    0:
    begin
      Geral.MensagemBox('Defina o status de repassado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Query.SQL.Add('AND lct.Repassado = -1000');
    end;
    1:
    begin
      if Coligado = 0 then Query.SQL.Add('AND lct.Repassado = 1')
      else begin
        Query.SQL.Add('AND (re.Coligado = '+IntToStr(Coligado)+
        ' AND lct.Repassado = 1)')
      end;
    end;
    2: Query.SQL.Add('AND lct.Repassado = 0');
    3:
    begin
      if Coligado <> 0 then
      begin
        Query.SQL.Add('AND (lct.Repassado = 0 OR (');
        Query.SQL.Add('lct.Repassado=1 AND re.Coligado = '+
        IntToStr(Coligado)+'))');
      end else begin
        // Sem SQL - vale qualquer situa��o.
      end;
    end;
  end;
  if CkCliente.Checked then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lot.Cliente=' + Geral.FF0(EdCliente.ValueVariant));
  end;
  if Geral.IMV(EdBanco.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lct.Banco='+EdBanco.Text);
  end;
  if Geral.IMV(EdAgencia.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lct.Agencia='+EdAgencia.Text);
  end;
  if Geral.IMV(EdConta.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND RIGHT(lct.ContaCorrente, 6)=RIGHT(' + EdConta.Text + ', 6)');
  end;
  if Geral.IMV(EdCheque.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lct.Documento=' + EdCheque.Text);
  end;
  if EdCPF_1.Text <> '' then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lct.CNPJCPF="' + Geral.SoNumero_TT(EdCPF_1.Text) + '"');
  end;
  if Controle <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lct.FatParcela = ' + FormatFloat('0', Controle));
  end;

  //

  (*Query.SQL.Add('AND lct.Quitado in (' + IntToStr(Fq0) + ', ' +
    IntToStr(Fq1) + ', ' +IntToStr(Fq2) + ', ' +IntToStr(Fq3) + ')');*)

  case CkProrrogado.Value of
    1: Query.SQL.Add('AND lct.ProrrVz>0');
    2: Query.SQL.Add('AND lct.ProrrVz=0');
    3: ; // Nada
  end;
  //
  case Juridico of
    0: Query.SQL.Add('AND lct.Juridico = 0');
    1: Query.SQL.Add('AND lct.Juridico = 1');
  end;
  if CNAB_Lot <> 0 then
    Query.SQL.Add('AND lct.CNAB_Lot=' + Geral.FF0(CNAB_Lot));
  case Remessa of
    0: Query.SQL.Add('AND lct.CNAB_Lot <> 0');
    1: Query.SQL.Add('AND lct.CNAB_Lot = 0');
  end;
  //
  i := 1;
  j := 1;
  m := MLAGeral.CheckGroupMaxValue(CkStatus);
  if (CkStatus.Value <> m) and (CkStatus.Value > 0) then
  begin
    Query.SQL.Add('AND (');
    while i <= m do
    begin
      if MLAGeral.IntInConjunto2(i, CkStatus.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add(ListaSQL[j]);
        LIGA_OR := True;
      end;
      j := j+ 1;
      i := i * 2;
    end;
    Query.SQL.Add(')');
  end;
  Result := Ok;
end;

{
function TFmGerChqMain.TextoSQLPesq(TabLote, TabLoteIts: String; Soma: Boolean;
  Query: TmySQLQuery): Integer;
const
  ListaSQL: array [1..6] of String = (
  (*Devolvido*) ' (li.Devolucao<>0 AND ai.Status in (0,1)) AND li.Quitado>-2 ',
  (*Aberto   *) ' (li.Devolucao=0 AND li.DDeposito>=SYSDATE()) AND li.Quitado>-2 ',
  (*Liquidado*) ' (li.Devolucao=0 AND li.DDeposito<SYSDATE()) AND li.Quitado>-2 ',
  (*Quitado  *) ' (li.Devolucao<>0 AND ai.Status in (2,3)) AND li.Quitado>-2 ',
  (*Baixado  *) ' (li.Quitado=-2) ',
  (*Moroso   *) ' (li.Quitado=-3) ');
var
  i, m, j, Ok, Coligado, Controle: Integer;
  LIGA_OR: Boolean;
begin
  Liga_OR := False;
  Ok := 0;
  if Soma then
  begin
    Query.SQL.Add('SELECT COUNT(li.Controle) Itens, SUM(li.Valor) Valor ');
  end else begin
    Query.SQL.Add('SELECT lo.Codigo, lo.Cliente, li.Cheque, li.Controle, li.Emissao, ');
    Query.SQL.Add('li.TxaCompra, li.DCompra, li.Valor, li.Desco, li.Vencto, ');
    Query.SQL.Add('li.Quitado,li.Devolucao, ai.Status STATUSDEV, li.ProrrVz, ');
    Query.SQL.Add('li.ProrrDd, li.Emitente, li.ValQuit, CASE WHEN co.Tipo=0 THEN ');
    Query.SQL.Add('co.RazaoSocial ELSE co.Nome END NOMECOLIGADO,');
    Query.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome');
    Query.SQL.Add('END NOMECLIENTE, li.DDeposito, li.CPF, li.NaoDeposita, '); // em.*
    Query.SQL.Add('li.RepCli, li.Banco, li.Agencia, li.Conta, li.Depositado, '); // em.*
    Query.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.ETe1 ELSE cl.PTe1 END TEL1CLI, ');
    Query.SQL.Add('li.ObsGerais');
  end;
  Query.SQL.Add('FROM '+tabloteits+' li');
  Query.SQL.Add('LEFT JOIN '+tablote+' lo ON lo.Codigo=li.Codigo');
  //Query.SQL.Add('LEFT JOIN emitentes em ON em.CPF=li.CPF');
  Query.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  Query.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  Query.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  Query.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  Query.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');
  Query.SQL.Add('WHERE lo.Tipo=0');
  Query.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  Coligado := EdColigado.ValueVariant;
  Controle := EdControle.ValueVariant;
  case CkRepassado.Value of
    0:
    begin
      Geral.MensagemBox('Defina o status de repassado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Query.SQL.Add('AND li.Repassado = -1000');
    end;
    1:
    begin
      if Coligado = 0 then Query.SQL.Add('AND li.Repassado = 1')
      else begin
        Query.SQL.Add('AND (re.Coligado = '+IntToStr(Coligado)+
        ' AND li.Repassado = 1)')
      end;
    end;
    2: Query.SQL.Add('AND li.Repassado = 0');
    3:
    begin
      if Coligado <> 0 then
      begin
        Query.SQL.Add('AND (li.Repassado = 0 OR (');
        Query.SQL.Add('li.Repassado=1 AND re.Coligado = '+
        IntToStr(Coligado)+'))');
      end else begin
        // Sem SQL - vale qualquer situa��o.
      end;
    end;
  end;
  if Trim(EdCliente.Text) <> '' then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lo.Cliente='+EdCliente.Text);
  end;
  if Geral.IMV(EdBanco.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Banco='+EdBanco.Text);
  end;
  if Geral.IMV(EdAgencia.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Agencia='+EdAgencia.Text);
  end;
  if Geral.IMV(EdConta.Text) <> 0 then
  begin
    Ok := Ok + 1;
    //Query.SQL.Add('AND li.Conta='+EdConta.Text);
    Query.SQL.Add('AND RIGHT(li.Conta, 6)=RIGHT('+EdConta.Text+', 6)');
  end;
  if Geral.IMV(EdCheque.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Cheque='+EdCheque.Text);
  end;
  if EdCPF_1.Text <> '' then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.CPF="'+Geral.SoNumero_TT(EdCPF_1.Text)+'"');
  end;
  if Controle <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Controle = '+IntToStr(Controle));
  end;

  //

  (*Query.SQL.Add('AND li.Quitado in (' + IntToStr(Fq0) + ', ' +
    IntToStr(Fq1) + ', ' +IntToStr(Fq2) + ', ' +IntToStr(Fq3) + ')');*)

  case CkProrrogado.Value of
    1: Query.SQL.Add('AND li.ProrrVz>0');
    2: Query.SQL.Add('AND li.ProrrVz=0');
    3: ; // Nada
  end;
  //
  i := 1;
  j := 1;
  m := MLAGeral.CheckGroupMaxValue(CkStatus);
  if (CkStatus.Value <> m) and (CkStatus.Value > 0) then
  begin
    Query.SQL.Add('AND (');
    while i <= m do
    begin
      if MLAGeral.IntInConjunto2(i, CkStatus.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add(ListaSQL[j]);
        LIGA_OR := True;
      end;
      j := j+ 1;
      i := i * 2;
    end;
    Query.SQL.Add(')');
  end;
 (* // Parei aqui
  case CkStatus.Value of
    1: // Devolvido
    begin
      Query.SQL.Add('AND ' + Devolvido);
    end;
    2: // Aberto
    begin
      Query.SQL.Add('AND ' + Aberto);
    end;
    3: // Devolvido + Aberto
    begin
      Query.SQL.Add('AND ( '+ Devolvido);
      Query.SQL.Add('OR ' + Aberto +')');
    end;
    4: // Liquidados
    begin
      Query.SQL.Add('AND ' + Liquidado);
    end;
    5: // Devolvidos + Liquidados
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado + ')');
    end;
    6: // Abertos + Liquidados
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Liquidado + ')');
    end;
    7: // Devolvidos + Liquidados + Abertos
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Aberto + ')');
    end;
    8: // Quitados(Devolv.)
    begin
      Query.SQL.Add('AND ' + Quitado);
    end;
    9: // Devolvido + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Quitado));
    end;
    10: // Aberto + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Quitado));
    end;
    11: // Aberto + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Devolvido);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    12: // Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Liquidado);
      Query.SQL.Add('OR ' + Quitado));
    end;
    13: // Devolvido + Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    14: // Aberto + Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    15: // Devolvido + Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    16: // Baixado
    begin
      Query.SQL.Add('AND ' + Baixado);
    end;
    17: // Devolvido + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Baixado));
    end;
    18: // Aberto + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Baixado));
    end;
    19: // Aberto + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Devolvido);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    20: // Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Liquidado);
      Query.SQL.Add('OR ' + Baixado));
    end;
    21: // Devolvido + Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    22: // Aberto + Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    23: // Devolvido + Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    // Parei aqui fazer ate 63
  end;*)

  (*if MLAGeral.IntInConjunto2(1, CGStatusManual.Value)
  or MLAGeral.IntInConjunto2(2, CGStatusManual.Value) then
  begin
    LIGA_OR := False;
    if (CGStatusAutoma.Value > 0) and
       (CGStatusAutoma.Value <> Trunc(Power(2, CGStatusAutoma.Items.Count)-1)) then
    begin
      Ok := Ok + 1;
      Query.SQL.Add('AND (');
      if MLAGeral.IntInConjunto2(1, CGStatusAutoma.Value) then
      begin
        Query.SQL.Add('  (li.Devolucao=0 AND li.DDeposito< CURRENT_DATE)');
        LIGA_OR := True;
      end;
      if MLAGeral.IntInConjunto2(2, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=0)');
        LIGA_OR := True;
      end;
      if MLAGeral.IntInConjunto2(4, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao=0 AND li.DDeposito>= CURRENT_DATE)');
        LIGA_OR := True;
      end;
      if MLAGeral.IntInConjunto2(8, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=1)');
        LIGA_OR := True;
      end;
      if (
           (MLAGeral.IntInConjunto2(16, CGStatusAutoma.Value))
           and
           (MLAGeral.IntInConjunto2(32, CGStatusAutoma.Value))
          ) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=2 )');
        LIGA_OR := True;
      end else
      if MLAGeral.IntInConjunto2(16, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=2 AND (ai.ValPago <= (ai.Valor+ai.Taxas+ai.Multa+ai.JurosV-ai.Desconto)))');
        LIGA_OR := True;
      end else
      if MLAGeral.IntInConjunto2(32, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=2 AND (ai.ValPago >  (ai.Valor+ai.Taxas+ai.Multa+ai.JurosV-ai.Desconto)))');
        LIGA_OR := True;
      end;
      if LIGA_OR then Query.SQL.Add('OR');
      Query.SQL.Add('  (li.Quitado in (' + IntToStr(Fq2) + ',' + IntToStr(Fq3) + '))');
      Query.SQL.Add(')');
    end;
  end;*)
  Result := Ok;
end;
}

end.
