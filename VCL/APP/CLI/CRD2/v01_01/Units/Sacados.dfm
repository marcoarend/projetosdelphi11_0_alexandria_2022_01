object FmSacados: TFmSacados
  Left = 332
  Top = 217
  Caption = 'EMI-TENTE-007 ::  Cadastro de Sacados'
  ClientHeight = 552
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GrSacados: TDBGrid
      Left = 0
      Top = 136
      Width = 1008
      Height = 193
      Align = alClient
      DataSource = DsSacados
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CNPJ_TXT'
          Title.Caption = 'CNPJ'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 186
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Risco'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TEL1_TXT'
          Title.Caption = 'Telefone'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Email'
          Title.Caption = 'E-mail'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Rua'
          Width = 95
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Numero'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compl'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bairro'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cidade'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CEP_TXT'
          Title.Caption = 'CEP'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UF'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IE'
          Title.Caption = 'Inscri'#231#227'o Estadual'
          Width = 106
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object RGOrdem1: TRadioGroup
        Left = 0
        Top = 0
        Width = 895
        Height = 72
        Align = alClient
        Caption = ' Ordem 1: '
        Columns = 7
        Items.Strings = (
          'CNPJ'
          'Nome'
          'Logradouro'
          'N'#250'mero'
          'Complemento'
          'Bairro'
          'Cidade'
          'UF'
          'CEP'
          'Telefone'
          'Risco')
        TabOrder = 0
        OnClick = RGOrdem1Click
      end
      object RGOrdem2: TRadioGroup
        Left = 895
        Top = 0
        Width = 113
        Height = 72
        Align = alRight
        Caption = ' Ordem 2: '
        Items.Strings = (
          'Crescente'
          'Decrescente')
        TabOrder = 1
        OnClick = RGOrdem2Click
      end
    end
    object StringGrid1: TStringGrid
      Left = 0
      Top = 329
      Width = 1008
      Height = 61
      Align = alBottom
      ColCount = 6
      DefaultRowHeight = 18
      TabOrder = 2
      Visible = False
    end
    object Panel2: TPanel
      Left = 0
      Top = 72
      Width = 1008
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 64
        Align = alClient
        Caption = ' Filtros: '
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 16
          Width = 31
          Height = 13
          Caption = 'Nome:'
        end
        object Label2: TLabel
          Left = 376
          Top = 16
          Width = 30
          Height = 13
          Caption = 'CNPJ:'
        end
        object Label3: TLabel
          Left = 496
          Top = 16
          Width = 24
          Height = 13
          Caption = 'CEP:'
        end
        object Label4: TLabel
          Left = 572
          Top = 16
          Width = 17
          Height = 13
          Caption = 'UF:'
        end
        object EdNome: TEdit
          Left = 12
          Top = 32
          Width = 361
          Height = 21
          TabOrder = 0
        end
        object EdCNPJ: TEdit
          Left = 376
          Top = 32
          Width = 116
          Height = 21
          TabOrder = 1
        end
        object EdCEP: TEdit
          Left = 496
          Top = 32
          Width = 72
          Height = 21
          TabOrder = 2
        end
        object EdUF: TEdit
          Left = 572
          Top = 32
          Width = 24
          Height = 21
          TabOrder = 3
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 263
        Height = 32
        Caption = 'Cadastro de Sacados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 263
        Height = 32
        Caption = 'Cadastro de Sacados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 263
        Height = 32
        Caption = 'Cadastro de Sacados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 438
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 482
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 18
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Inclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
        NumGlyphs = 2
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 114
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Altera'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
        NumGlyphs = 2
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 210
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Exclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
        NumGlyphs = 2
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 422
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Reabre'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtReabreClick
        NumGlyphs = 2
      end
    end
  end
  object DsSacados: TDataSource
    DataSet = QrSacados
    Left = 48
    Top = 12
  end
  object QrSacados: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSacadosCalcFields
    SQL.Strings = (
      'SELECT sac.Numero+0.000 Numero, sac.* '
      'FROM sacados sac')
    Left = 20
    Top = 12
    object QrSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
    object QrSacadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSacadosRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrSacadosCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrSacadosBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrSacadosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrSacadosUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSacadosCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrSacadosTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrSacadosRisco: TFloatField
      FieldName = 'Risco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacadosCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrSacadosCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrSacadosTEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 50
      Calculated = True
    end
    object QrSacadosIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrSacadosNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrSacadosEmail: TWideStringField
      FieldName = 'Email'
      Size = 255
    end
  end
end
