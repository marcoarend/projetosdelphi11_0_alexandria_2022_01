object FmLot2Dup: TFmLot2Dup
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-007 :: Lote - Duplicata'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 192
        Height = 32
        Caption = 'Lote - Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 192
        Height = 32
        Caption = 'Lote - Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 192
        Height = 32
        Caption = 'Lote - Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 622
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair4: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSair4Click
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma4: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirma4Click
      end
      object BtCalendario2: TBitBtn
        Tag = 107
        Left = 372
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'Ca&lend'#225'rio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtCalendario2Click
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 530
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 530
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 530
        Align = alClient
        TabOrder = 0
        object PainelDigitaDup: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 369
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel37: TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 205
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label66: TLabel
              Left = 3
              Top = 4
              Width = 42
              Height = 13
              Caption = 'Emiss'#227'o:'
            end
            object Label58: TLabel
              Left = 99
              Top = 44
              Width = 40
              Height = 13
              Caption = 'Sacado:'
            end
            object Label62: TLabel
              Left = 3
              Top = 84
              Width = 30
              Height = 13
              Caption = 'Bairro:'
            end
            object Label69: TLabel
              Left = 3
              Top = 124
              Width = 30
              Height = 13
              Caption = 'Prazo:'
            end
            object Label63: TLabel
              Left = 119
              Top = 84
              Width = 36
              Height = 13
              Caption = 'Cidade:'
            end
            object Label71: TLabel
              Left = 71
              Top = 124
              Width = 27
              Height = 13
              Caption = 'Total:'
            end
            object Label72: TLabel
              Left = 139
              Top = 124
              Width = 71
              Height = 13
              Caption = 'Total Taxa (%):'
            end
            object Label64: TLabel
              Left = 267
              Top = 84
              Width = 24
              Height = 13
              Caption = 'CEP:'
            end
            object Label59: TLabel
              Left = 335
              Top = 44
              Width = 57
              Height = 13
              Caption = 'Logradouro:'
            end
            object Label73: TLabel
              Left = 383
              Top = 84
              Width = 45
              Height = 13
              Caption = 'Telefone:'
            end
            object Label93: TLabel
              Left = 39
              Top = 164
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label92: TLabel
              Left = 3
              Top = 164
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label60: TLabel
              Left = 579
              Top = 44
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
            end
            object Label61: TLabel
              Left = 635
              Top = 44
              Width = 67
              Height = 13
              Caption = 'Complemento:'
            end
            object Label18: TLabel
              Left = 3
              Top = 44
              Width = 90
              Height = 13
              Caption = 'Inscri'#231#227'o Estadual:'
            end
            object Label57: TLabel
              Left = 591
              Top = 4
              Width = 109
              Height = 13
              Caption = 'CNPJ: [F8] pesq. nome'
            end
            object Label35: TLabel
              Left = 535
              Top = 4
              Width = 49
              Height = 13
              Caption = 'Tx.com %:'
            end
            object Label67: TLabel
              Left = 512
              Top = 4
              Width = 17
              Height = 13
              Caption = 'D+:'
            end
            object Label56: TLabel
              Left = 443
              Top = 4
              Width = 59
              Height = 13
              Caption = 'Vencimento:'
            end
            object Label79: TLabel
              Left = 279
              Top = 4
              Width = 49
              Height = 13
              Caption = 'Desconto:'
            end
            object Label55: TLabel
              Left = 203
              Top = 4
              Width = 27
              Height = 13
              Caption = 'Valor:'
            end
            object Label54: TLabel
              Left = 139
              Top = 4
              Width = 61
              Height = 13
              Caption = 'N'#186' duplicata:'
            end
            object Label68: TLabel
              Left = 71
              Top = 4
              Width = 39
              Height = 13
              Caption = 'Compra:'
            end
            object Label65: TLabel
              Left = 335
              Top = 84
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label204: TLabel
              Left = 223
              Top = 124
              Width = 115
              Height = 13
              Caption = 'Carteira de recebimento:'
            end
            object Label207: TLabel
              Left = 83
              Top = 164
              Width = 73
              Height = 13
              Caption = 'Conta corrente:'
              FocusControl = DBEdConta4
            end
            object Label70: TLabel
              Left = 339
              Top = 4
              Width = 52
              Height = 13
              Caption = 'Desco at'#233':'
            end
            object Label95: TLabel
              Left = 180
              Top = 164
              Width = 80
              Height = 13
              Caption = 'Nome do Banco:'
              FocusControl = DBEdit23
            end
            object Label1: TLabel
              Left = 500
              Top = 84
              Width = 31
              Height = 13
              Caption = 'E-mail:'
            end
            object EdEmiss4: TdmkEdit
              Left = 3
              Top = 20
              Width = 64
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdEmiss4Exit
            end
            object EdSacado: TdmkEdit
              Left = 99
              Top = 60
              Width = 233
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdBairro: TdmkEdit
              Left = 3
              Top = 100
              Width = 113
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 15
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdPrazo4: TdmkEdit
              Left = 3
              Top = 140
              Width = 64
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 21
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdCidade: TdmkEdit
              Left = 119
              Top = 100
              Width = 145
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 16
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdDias4: TdmkEdit
              Left = 71
              Top = 140
              Width = 64
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 22
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdTotalCompraPer4: TdmkEdit
              Left = 139
              Top = 140
              Width = 81
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 23
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdCEP: TdmkEdit
              Left = 267
              Top = 100
              Width = 65
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtCEP
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdRua: TdmkEdit
              Left = 335
              Top = 60
              Width = 241
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdTel1: TdmkEdit
              Left = 383
              Top = 100
              Width = 113
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 19
              FormatType = dmktfString
              MskType = fmtTelCurto
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdNumero: TdmkEdit
              Left = 579
              Top = 60
              Width = 53
              Height = 21
              Alignment = taRightJustify
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 13
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdCompl: TdmkEdit
              Left = 635
              Top = 60
              Width = 69
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdIE: TdmkEdit
              Left = 3
              Top = 60
              Width = 93
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdCNPJ: TdmkEdit
              Left = 591
              Top = 20
              Width = 113
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdCNPJChange
              OnExit = EdCNPJExit
              OnKeyDown = EdCNPJKeyDown
            end
            object EdTxaCompra4: TdmkEdit
              Left = 535
              Top = 20
              Width = 53
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = EdTxaCompra4Exit
            end
            object EdDMaisD: TdmkEdit
              Left = 512
              Top = 20
              Width = 20
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnExit = EdDMaisDExit
            end
            object EdVence4: TdmkEdit
              Left = 443
              Top = 20
              Width = 64
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdVence4Exit
            end
            object EdDesco4: TdmkEdit
              Left = 279
              Top = 20
              Width = 57
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = EdDesco4Exit
            end
            object EdValor4: TdmkEdit
              Left = 203
              Top = 20
              Width = 73
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = EdValor4Exit
            end
            object EdDuplicata: TdmkEdit
              Left = 139
              Top = 20
              Width = 61
              Height = 21
              Alignment = taCenter
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdDuplicataExit
            end
            object EdData4: TdmkEdit
              Left = 71
              Top = 20
              Width = 64
              Height = 21
              Alignment = taCenter
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdData4Exit
            end
            object CBUF: TComboBox
              Left = 335
              Top = 100
              Width = 45
              Height = 21
              Style = csDropDownList
              Sorted = True
              TabOrder = 18
              OnKeyDown = CBUFKeyDown
              Items.Strings = (
                'AC'
                'AL'
                'AM'
                'AP'
                'BA'
                'CE'
                'DF'
                'ES'
                'GO'
                'MA'
                'MG'
                'MS'
                'MT'
                'PA'
                'PB'
                'PE'
                'PI'
                'PR'
                'RJ'
                'RN'
                'RO'
                'RR'
                'RS'
                'SC'
                'SE'
                'SP'
                'TO')
            end
            object EdCartDep4: TdmkEditCB
              Left = 223
              Top = 140
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 24
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBCartDep4
              IgnoraDBLookupComboBox = False
            end
            object CBCartDep4: TdmkDBLookupComboBox
              Left = 271
              Top = 140
              Width = 257
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DmLot.DsCarteiras4
              TabOrder = 25
              dmkEditCB = EdCartDep4
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object DBEdBanco4: TDBEdit
              Left = 3
              Top = 180
              Width = 33
              Height = 21
              TabStop = False
              Color = clInactiveCaption
              DataField = 'Banco1'
              DataSource = DmLot.DsCarteiras4
              TabOrder = 26
              OnChange = DBEdBanco4Change
            end
            object DBEdAgencia4: TDBEdit
              Left = 39
              Top = 180
              Width = 41
              Height = 21
              TabStop = False
              Color = clInactiveCaption
              DataField = 'Agencia1'
              DataSource = DmLot.DsCarteiras4
              TabOrder = 27
            end
            object DBEdConta4: TDBEdit
              Left = 83
              Top = 180
              Width = 94
              Height = 21
              TabStop = False
              Color = clInactiveCaption
              DataField = 'Conta1'
              DataSource = DmLot.DsCarteiras4
              TabOrder = 28
            end
            object dmkEdTPDescAte: TdmkEditDateTimePicker
              Left = 339
              Top = 20
              Width = 101
              Height = 21
              Date = 40074.401972708350000000
              Time = 40074.401972708350000000
              TabOrder = 5
              OnExit = dmkEdTPDescAteExit
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
            end
            object DBEdit23: TDBEdit
              Left = 180
              Top = 180
              Width = 349
              Height = 21
              TabStop = False
              Color = clInactiveCaption
              DataField = 'Nome'
              DataSource = DmLot.DsBanco4
              TabOrder = 29
            end
            object GroupBox2: TGroupBox
              Left = 706
              Top = 0
              Width = 298
              Height = 205
              Align = alRight
              Enabled = False
              TabOrder = 30
              object Label7: TLabel
                Left = 4
                Top = 26
                Width = 45
                Height = 13
                Caption = 'Cheques:'
              end
              object Label16: TLabel
                Left = 4
                Top = 48
                Width = 53
                Height = 13
                Caption = 'Duplicatas:'
              end
              object Label111: TLabel
                Left = 60
                Top = 8
                Width = 30
                Height = 13
                Caption = 'Risco:'
              end
              object Label112: TLabel
                Left = 137
                Top = 8
                Width = 42
                Height = 13
                Caption = 'Vencido:'
              end
              object Label113: TLabel
                Left = 214
                Top = 8
                Width = 56
                Height = 13
                Caption = 'Sub-total:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label114: TLabel
                Left = 4
                Top = 70
                Width = 56
                Height = 13
                Caption = 'Sub-total:'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label115: TLabel
                Left = 4
                Top = 92
                Width = 60
                Height = 13
                Caption = 'Ocorr'#234'ncias:'
              end
              object EdDT_S: TdmkEdit
                Left = 214
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdRT_S: TdmkEdit
                Left = 60
                Top = 66
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdVT_S: TdmkEdit
                Left = 137
                Top = 66
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdST_S: TdmkEdit
                Left = 214
                Top = 66
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdCT_S: TdmkEdit
                Left = 214
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdDV_S: TdmkEdit
                Left = 137
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdCR_S: TdmkEdit
                Left = 60
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdCV_S: TdmkEdit
                Left = 137
                Top = 22
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdOA_S: TdmkEdit
                Left = 137
                Top = 88
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdTT_S: TdmkEdit
                Left = 214
                Top = 88
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdDR_S: TdmkEdit
                Left = 60
                Top = 44
                Width = 76
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
            end
            object EdEmail: TdmkEdit
              Left = 500
              Top = 100
              Width = 204
              Height = 21
              CharCase = ecLowerCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 20
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
          object PageControl6: TPageControl
            Left = 0
            Top = 205
            Width = 1004
            Height = 164
            ActivePage = TabSheet19
            Align = alClient
            TabOrder = 1
            TabStop = False
            object TabSheet19: TTabSheet
              Caption = 'Risco cheques'
              object DBGrid15: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 136
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacRiscoC
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Banco'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Agencia'
                    Title.Caption = 'Ag'#234'ncia'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DCompra'
                    Title.Caption = 'D.Compra'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Title.Alignment = taRightJustify
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DDeposito'
                    Title.Caption = 'Vencimento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Emitente'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CPF_TXT'
                    Title.Caption = 'CPF / CNPJ'
                    Width = 113
                    Visible = True
                  end>
              end
            end
            object TabSheet20: TTabSheet
              Caption = 'Cheques devolvidos'
              ImageIndex = 1
              object DBGrid16: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 136
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacCHDevA
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DATA1_TXT'
                    Title.Caption = '1'#170' Devol.'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Alinea1'
                    Title.Caption = 'Ali 1'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Emitente'
                    Title.Caption = 'Nome emitente'
                    Width = 184
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CPF_TXT'
                    Title.Caption = 'CPF / CNPJ emitente'
                    Width = 113
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Banco'
                    Title.Caption = 'Bco.'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Agencia'
                    Title.Caption = 'Ag.'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Width = 72
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Cheque'
                    Width = 45
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Taxas'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATA3_TXT'
                    Title.Caption = #218'lt.Pgto'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValPago'
                    Title.Caption = 'Pago'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ATUAL'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATA2_TXT'
                    Title.Caption = '2'#170' Devol.'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Alinea2'
                    Title.Caption = 'Ali 2'
                    Width = 24
                    Visible = True
                  end>
              end
            end
            object TabSheet21: TTabSheet
              Caption = 'Duplicatas'
              ImageIndex = 2
              object DBGrid17: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 136
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacDOpen
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Duplicata'
                    Title.Caption = 'Vencidas'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DCompra'
                    Title.Caption = 'D.Compra'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Title.Alignment = taRightJustify
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DDeposito'
                    Title.Caption = 'Vencto.'
                    Width = 52
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Emitente'
                    Width = 310
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO_DESATUALIZ'
                    Title.Caption = 'Saldo'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO_ATUALIZADO'
                    Title.Caption = 'Atualizado'
                    Width = 68
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESTATUS'
                    Title.Caption = 'Status'
                    Width = 90
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'STATUS'
                    Title.Caption = #218'ltima posi'#231#227'o no hist'#243'rico'
                    Width = 184
                    Visible = True
                  end>
              end
            end
            object TabSheet22: TTabSheet
              Caption = 'Ocorr'#234'ncias'
              ImageIndex = 3
              object DBGrid18: TDBGrid
                Left = 0
                Top = 0
                Width = 996
                Height = 136
                TabStop = False
                Align = alClient
                DataSource = DmLot.DsSacOcorA
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'TIPODOC'
                    Title.Caption = 'TD'
                    Width = 20
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ocorrencia'
                    Title.Caption = 'C'#243'd'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEOCORRENCIA'
                    Title.Caption = 'Ocorr'#234'ncia'
                    Width = 210
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DataO'
                    Title.Caption = 'Data'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Valor'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TaxaV'
                    Title.Caption = '$ Taxa'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pago'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SALDO'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Data3'
                    Title.Caption = 'Ult.Pg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ATUALIZADO'
                    Visible = True
                  end>
              end
            end
            object TabSheet28: TTabSheet
              Caption = 'Dados do lote'
              ImageIndex = 4
              object Panel45: TPanel
                Left = 0
                Top = 0
                Width = 996
                Height = 126
                Align = alTop
                BevelOuter = bvNone
                Enabled = False
                TabOrder = 0
                object Label179: TLabel
                  Left = 4
                  Top = 4
                  Width = 36
                  Height = 13
                  Caption = 'C'#243'digo:'
                  FocusControl = LM_DBEdit2
                end
                object Label180: TLabel
                  Left = 72
                  Top = 4
                  Width = 35
                  Height = 13
                  Caption = 'Cliente:'
                  FocusControl = DBEdit36
                end
                object Label181: TLabel
                  Left = 231
                  Top = 44
                  Width = 24
                  Height = 13
                  Caption = 'Lote:'
                  FocusControl = DBEdit62
                end
                object Label182: TLabel
                  Left = 292
                  Top = 44
                  Width = 68
                  Height = 13
                  Caption = 'Ad Valorem %:'
                end
                object Label183: TLabel
                  Left = 4
                  Top = 44
                  Width = 74
                  Height = 13
                  Caption = 'Valor negociad:'
                  FocusControl = DBEdit64
                end
                object Label184: TLabel
                  Left = 80
                  Top = 44
                  Width = 76
                  Height = 13
                  Caption = 'Prazo m'#233'dio dd:'
                  FocusControl = DBEdit65
                end
                object Label185: TLabel
                  Left = 156
                  Top = 44
                  Width = 68
                  Height = 13
                  Caption = 'Taxa real per.:'
                  FocusControl = DBEdit66
                end
                object Label186: TLabel
                  Left = 4
                  Top = 84
                  Width = 65
                  Height = 13
                  Caption = 'Tx. compra $:'
                  FocusControl = DBEdit67
                end
                object Label187: TLabel
                  Left = 76
                  Top = 84
                  Width = 66
                  Height = 13
                  Caption = 'Ad Valorem $:'
                  FocusControl = DBEdit68
                end
                object Label188: TLabel
                  Left = 456
                  Top = 84
                  Width = 62
                  Height = 13
                  Caption = 'Valor l'#237'quido:'
                  FocusControl = DBEdit69
                end
                object Label189: TLabel
                  Left = 380
                  Top = 44
                  Width = 26
                  Height = 13
                  Caption = 'Data:'
                end
                object Label190: TLabel
                  Left = 152
                  Top = 84
                  Width = 44
                  Height = 13
                  Caption = 'Tarifas $:'
                  FocusControl = DBEdit71
                end
                object Label191: TLabel
                  Left = 228
                  Top = 84
                  Width = 52
                  Height = 13
                  Caption = 'Pg ocor. $:'
                  FocusControl = DBEdit72
                end
                object Label192: TLabel
                  Left = 304
                  Top = 84
                  Width = 51
                  Height = 13
                  Caption = '$ CH dev.:'
                  FocusControl = DBEdit73
                end
                object Label193: TLabel
                  Left = 540
                  Top = 84
                  Width = 40
                  Height = 13
                  Caption = 'A pagar:'
                end
                object Label194: TLabel
                  Left = 380
                  Top = 84
                  Width = 52
                  Height = 13
                  Caption = '$ DU dev.:'
                  FocusControl = DBEdit75
                end
                object Label195: TLabel
                  Left = 464
                  Top = 4
                  Width = 100
                  Height = 13
                  Caption = 'Tipo de documentos:'
                  FocusControl = DBEdit76
                end
                object Label196: TLabel
                  Left = 568
                  Top = 4
                  Width = 40
                  Height = 13
                  Caption = 'Border'#244':'
                  FocusControl = DBEdit77
                end
                object Label197: TLabel
                  Left = 624
                  Top = 4
                  Width = 23
                  Height = 13
                  Caption = 'N.F.:'
                  FocusControl = DBEdit78
                end
                object Label198: TLabel
                  Left = 568
                  Top = 44
                  Width = 26
                  Height = 13
                  Caption = 'Itens:'
                  FocusControl = DBEdit79
                end
                object Label199: TLabel
                  Left = 624
                  Top = 44
                  Width = 57
                  Height = 13
                  Caption = 'Taxa / m'#234's:'
                  FocusControl = DBEdit80
                end
                object Label200: TLabel
                  Left = 612
                  Top = 84
                  Width = 24
                  Height = 13
                  Caption = 'Tipo:'
                  FocusControl = DBEdit81
                end
                object DBText3: TDBText
                  Left = 447
                  Top = 44
                  Width = 122
                  Height = 17
                  DataField = 'CAPTIONCONFCARTA'
                end
                object Label201: TLabel
                  Left = 660
                  Top = 84
                  Width = 24
                  Height = 13
                  Caption = 'CBE:'
                  FocusControl = DBEdit83
                end
                object LM_DBEdit2: TDBEdit
                  Left = 4
                  Top = 20
                  Width = 65
                  Height = 21
                  Hint = 'N'#186' do banco'
                  TabStop = False
                  DataField = 'Codigo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8281908
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ParentShowHint = False
                  ReadOnly = True
                  ShowHint = True
                  TabOrder = 0
                end
                object DBEdit36: TDBEdit
                  Left = 72
                  Top = 20
                  Width = 57
                  Height = 21
                  DataField = 'Cliente'
                  TabOrder = 1
                end
                object DBEdit37: TDBEdit
                  Left = 132
                  Top = 20
                  Width = 329
                  Height = 21
                  DataField = 'NOMECLIENTE'
                  TabOrder = 2
                end
                object DBEdit62: TDBEdit
                  Left = 231
                  Top = 60
                  Width = 58
                  Height = 21
                  DataField = 'Lote'
                  TabOrder = 3
                end
                object DBEdit63: TDBEdit
                  Left = 292
                  Top = 60
                  Width = 85
                  Height = 21
                  DataField = 'ADVALOREM_TOTAL'
                  TabOrder = 4
                end
                object DBEdit64: TDBEdit
                  Left = 4
                  Top = 60
                  Width = 72
                  Height = 21
                  DataField = 'Total'
                  TabOrder = 5
                end
                object DBEdit65: TDBEdit
                  Left = 80
                  Top = 60
                  Width = 72
                  Height = 21
                  DataField = 'Dias'
                  TabOrder = 6
                end
                object DBEdit66: TDBEdit
                  Left = 156
                  Top = 60
                  Width = 72
                  Height = 21
                  DataField = 'TAXAPERCE_TOTAL'
                  TabOrder = 7
                end
                object DBEdit67: TDBEdit
                  Left = 4
                  Top = 100
                  Width = 69
                  Height = 21
                  DataField = 'TAXAVALOR_TOTAL'
                  TabOrder = 8
                end
                object DBEdit68: TDBEdit
                  Left = 76
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'VALVALOREM_TOTAL'
                  TabOrder = 9
                end
                object DBEdit69: TDBEdit
                  Left = 456
                  Top = 100
                  Width = 81
                  Height = 21
                  DataField = 'VAL_LIQUIDO'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 10
                end
                object DBEdit70: TDBEdit
                  Left = 380
                  Top = 60
                  Width = 64
                  Height = 21
                  DataField = 'Data'
                  TabOrder = 11
                end
                object DBEdit71: TDBEdit
                  Left = 152
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'Tarifas'
                  TabOrder = 12
                end
                object DBEdit72: TDBEdit
                  Left = 228
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'OcorP'
                  TabOrder = 13
                end
                object DBEdit73: TDBEdit
                  Left = 304
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'CHDevPg'
                  TabOrder = 14
                end
                object DBEdit74: TDBEdit
                  Left = 540
                  Top = 100
                  Width = 69
                  Height = 21
                  DataField = 'A_PG_LIQ'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 15
                end
                object DBEdit75: TDBEdit
                  Left = 380
                  Top = 100
                  Width = 72
                  Height = 21
                  DataField = 'DUDevPg'
                  TabOrder = 16
                end
                object DBEdit76: TDBEdit
                  Left = 464
                  Top = 20
                  Width = 101
                  Height = 21
                  DataField = 'NOMETIPOLOTE'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 17
                end
                object DBEdit77: TDBEdit
                  Left = 568
                  Top = 20
                  Width = 52
                  Height = 21
                  DataField = 'Lote'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 18
                end
                object DBEdit78: TDBEdit
                  Left = 624
                  Top = 20
                  Width = 60
                  Height = 21
                  DataField = 'NF'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 19
                end
                object DBEdit79: TDBEdit
                  Left = 568
                  Top = 60
                  Width = 52
                  Height = 21
                  DataField = 'Itens'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 20
                end
                object DBEdit80: TDBEdit
                  Left = 624
                  Top = 60
                  Width = 61
                  Height = 21
                  DataField = 'TAXAPERCEMENSAL'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8388672
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 21
                end
                object DBEdit81: TDBEdit
                  Left = 612
                  Top = 100
                  Width = 45
                  Height = 21
                  DataField = 'NOMESPREAD'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 22
                end
                object DBEdit82: TDBEdit
                  Left = 447
                  Top = 60
                  Width = 118
                  Height = 21
                  DataField = 'NOMECOFECARTA'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 23
                end
                object DBEdit83: TDBEdit
                  Left = 660
                  Top = 100
                  Width = 25
                  Height = 21
                  DataField = 'CBE'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 24
                end
              end
            end
          end
        end
        object GradeDU: TStringGrid
          Left = 2
          Top = 384
          Width = 1004
          Height = 144
          Align = alBottom
          DefaultRowHeight = 19
          RowCount = 7
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 1
          OnDrawCell = GradeDUDrawCell
          OnKeyDown = GradeDUKeyDown
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 578
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
end
