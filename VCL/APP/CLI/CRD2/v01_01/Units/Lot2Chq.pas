unit Lot2Chq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, UnitSPC, UnDmkEnums;

type
  TFmLot2Chq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtConfirma2: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PainelBanda: TPanel;
    Painelx: TPanel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    Label94: TLabel;
    Label210: TLabel;
    Label208: TLabel;
    Label211: TLabel;
    EdBanda: TdmkEdit;
    EdRegiaoCompe: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    EdValor: TdmkEdit;
    EdVencto: TdmkEdit;
    EdCPF: TdmkEdit;
    EdEmitente: TdmkEdit;
    EdTxaCompra: TdmkEdit;
    EdDMaisC: TdmkEdit;
    EdPrazo: TdmkEdit;
    EdCompensacao: TdmkEdit;
    EdDias: TdmkEdit;
    EdTotalCompraPer: TdmkEdit;
    EdData: TdmkEdit;
    DBEdit22: TDBEdit;
    GroupBox3: TGroupBox;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    EdDR_E: TdmkEdit;
    EdDT_E: TdmkEdit;
    EdRT_E: TdmkEdit;
    EdVT_E: TdmkEdit;
    EdST_E: TdmkEdit;
    EdCT_E: TdmkEdit;
    EdDV_E: TdmkEdit;
    EdCR_E: TdmkEdit;
    EdCV_E: TdmkEdit;
    EdOA_E: TdmkEdit;
    EdTT_E: TdmkEdit;
    EdCMC_7: TdmkEdit;
    EdRealCC: TdmkEdit;
    EdTipific: TdmkEdit;
    EdStatusSPC: TdmkEdit;
    EdStatusSPC_TXT: TEdit;
    PageControl7: TPageControl;
    TabSheet23: TTabSheet;
    DBGrid19: TDBGrid;
    TabSheet24: TTabSheet;
    DBGrid20: TDBGrid;
    TabSheet25: TTabSheet;
    DBGrid21: TDBGrid;
    TabSheet26: TTabSheet;
    DBGrid22: TDBGrid;
    TabSheet27: TTabSheet;
    Panel44: TPanel;
    Label156: TLabel;
    Label157: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    Label162: TLabel;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    Label166: TLabel;
    Label167: TLabel;
    Label168: TLabel;
    Label169: TLabel;
    Label170: TLabel;
    Label171: TLabel;
    Label172: TLabel;
    Label173: TLabel;
    Label174: TLabel;
    Label175: TLabel;
    Label176: TLabel;
    Label177: TLabel;
    DBText2: TDBText;
    Label178: TLabel;
    LM_DBEdit1: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    TabSheet32: TTabSheet;
    MeSPC: TMemo;
    GradeCH: TStringGrid;
    BtCalendario: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCMC_7Change(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRegiaoCompeChange(Sender: TObject);
    procedure EdRegiaoCompeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBancoChange(Sender: TObject);
    procedure EdAgenciaChange(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdVenctoEnter(Sender: TObject);
    procedure EdVenctoExit(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdCPFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdStatusSPCChange(Sender: TObject);
    procedure EdStatusSPCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdStatusSPC_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTxaCompraExit(Sender: TObject);
    procedure EdDMaisCExit(Sender: TObject);
    procedure EdDataExit(Sender: TObject);
    procedure GradeCHDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtCalendarioClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValoresChequeAlterado();
    procedure ConsultaCNPJCPF_CH();
    procedure Desiste();
    function  LocalizaEmitente(MudaNome: Boolean): Boolean;
  public
    { Public declarations }
    procedure CalculaDiasCH();
  end;

  var
  FmLot2Chq: TFmLot2Chq;

implementation

uses UnMyObjects, Module, Lot2Cab, ModuleLot, SomaDiasCred, Principal,
  GerChqMain, UMySQLModule, ModuleLot2, LotEmi, MyListas;

{$R *.DFM}

procedure TFmLot2Chq.BtCalendarioClick(Sender: TObject);
begin
  Application.CreateForm(TFmSomaDiasCred, FmSomaDiasCred);
  FmSomaDiasCred.TPDataI.Date := Geral.ValidaDataSimples(EdData.Text, True);
  FmSomaDiasCred.TPDataF.Date := Geral.ValidaDataSimples(EdVencto.Text, True);
  FmSomaDiasCred.EdComp.ValueVariant  := EdCompensacao.ValueVariant;
  FmSomaDiasCred.EdDMais.ValueVariant := EdDMaisC.ValueVariant;
  FmSomaDiasCred.ShowModal;
  FmSomaDiasCred.Destroy;
end;

procedure TFmLot2Chq.BtConfirma2Click(Sender: TObject);
var
  Vence, Data_: TDateTime;
  Agencia, i, z, Erros, DMais, Dias, FatNum, Documento, Comp, Banco, Lote,
  Praca, FatParcela, Tipific, StatusSPC, Tipo, Carteira, Controle, Sub: Integer;
  Credito: Double;
  DDeposito, DataDoc, DCompra, Vencimento, ContaCorrente, CNPJCPF, Emitente: String;
begin
  DmLot.QrCHsDev.Close;
  DmLot.QrCHsDev.SQL.Clear;
  DmLot.QrCHsDev.SQL.Add('SELECT ai.*');
  DmLot.QrCHsDev.SQL.Add('FROM alinits ai');
  (*DmLot.QrCHsDev.SQL.Add('LEFT JOIN lot esits li ON li.Controle=ai.ChequeOrigem');
  DmLot.QrCHsDev.SQL.Add('LEFT JOIN lot es lo ON li.Codigo=lo.Codigo');*)
  DmLot.QrCHsDev.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lo ON ai.LoteOrigem=lo.Codigo');
  DmLot.QrCHsDev.SQL.Add('WHERE ai.CPF="' + Geral.SoNumero_TT(EdCPF.Text) + '"');
  if FmPrincipal.FConnections = 0 then
    DmLot.QrCHsDev.SQL.Add('AND lo.TxCompra + lo.ValValorem + 1 >= 0.01');
  UMyMod.AbreQuery(DmLot.QrCHsDev, Dmod.MyDB);
  if DmLot.QrCHsDev.RecordCount > 0 then
  begin
    PageControl7.ActivePageIndex := 1;
    if Geral.MensagemBox('Aten��o! Emitente com movimenta��o de '+
    'cheque(s) devolvido(s). Deseja aceitar assim mesmo?', 'ATEN��O!',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Application.CreateForm(TFmGerChqMain, FmGerChqMain);
      FmGerChqMain.EdCPF_1.Text := EdCPF.Text;
      FmGerChqMain.EdCPF_1Exit(Self);
      FmGerChqMain.WindowState := wsMaximized;
      FmGerChqMain.ShowModal;
      FmGerChqMain.Destroy;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    CalculaValoresChequeAlterado;
    FatNum        := FmLot2Cab.QrLotCodigo.Value;
    Erros         := 0;
    Credito       := EdValor.ValueVariant;
    Vence         := Geral.ValidaDataSimples(EdVencto.Text, True);
    Vencimento    := FormatDateTime(VAR_FORMATDATE, Vence);
    Data_         := Geral.ValidaDataSimples(EdData.Text, True);
    DCompra       := FormatDateTime(VAR_FORMATDATE, Data_);
    DataDoc       := DCompra;
    Banco         := EdBanco.ValueVariant;
    Agencia       := EdAgencia.ValueVariant;
    ContaCorrente := EdConta.Text;
    Documento     := EdCheque.ValueVariant;
    CNPJCPF       := Geral.SoNumero_TT(EdCPF.Text);
    Emitente      := EdEmitente.Text;
    Praca         := EdRegiaoCompe.ValueVariant;
    Tipific       := EdTipific.ValueVariant;
    StatusSPC     := EdStatusSPC.ValueVariant;
    Comp          := FmPrincipal.VerificaDiasCompensacao(Praca, FmLot2Cab.QrLotCBECli.Value,
                       Credito, Data_, Vence, FmLot2Cab.QrLotSCBCli.Value);
    //
    z := 9;
    for i := 1 to z do
    begin
      case i of
        1: if Credito <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o valor!', EdValor);
        2: if Vence < Data_ then Erros := Erros +
          MLAGeral.MostraErroControle('Prazo inv�lido!', EdVencto);
        //3: if Comp < 0 then Erros := Erros +
          //MLAGeral.MostraErroControle('Erro! Compensa��o negativa', EdRegiaoCompe);
        4: if Banco <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o banco!', EdBanco);
        5: if EdAgencia.ValueVariant <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a ag�ncia!', EdAgencia);
        6: if ContaCorrente = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o n�mero da conta !', EdConta);
        7: if Documento <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o n�mero do cheque!', EdCheque);
        8: if CNPJCPF = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o CPF/CNPJ!', EdCPF);
        9: if Emitente = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o emitente!', EdCPF);
      end;
      if Erros > 0 then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    DMais := EdDMaisC.ValueVariant;
    Dias := EdDias.ValueVariant;
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    //
    if ImgTipo.SQLType = stUpd then
    begin
      FatParcela := FmLot2Cab.QrLotItsFatParcela.Value;
      Tipo := FmLot2Cab.QrLotItsTipo.Value;
      Carteira := FmLot2Cab.QrLotItsCarteira.Value;
      Controle := FmLot2Cab.QrLotItsControle.Value;
      Sub := FmLot2Cab.QrLotItsSub.Value;
    end else begin
      FatParcela := 0;
      Tipo := 2;
      Carteira := CO_CARTEIRA_CHQ_BORDERO;
      Controle := 0;
      Sub := 0;
    end;

    FmLot2Cab.SQL_CH(FatParcela, Comp, Banco, Agencia, Documento,
                DMais, Dias, FatNum, Praca, Tipific, StatusSPC, ContaCorrente,
                CNPJCPF, Emitente, DataDoc, Vencimento, DCompra, DDeposito, Credito,
                ImgTipo.SQLType, FmLot2Cab.QrLotCliente.Value,
                Tipo, Carteira, Controle, Sub);
    //
    DmLot2.AtualizaEmitente(MLAGeral.FFD(Banco, 3, siPositivo), FormatFloat(
    '0000', Agencia), ContaCorrente, CNPJCPF, Emitente, -1);
    EdBanda.Text := '';
    EdBanda.SetFocus;
    if ImgTipo.SQLType = stUpd then BtSaidaClick(Self)
    else begin
      Lote := FmLot2Cab.QrLotCodigo.Value;
      FmLot2Cab.CalculaLote(Lote, True);
      FmLot2Cab.LocCod(Lote, Lote);
      if Lote <> FmLot2Cab.QrLotCodigo.Value then
      begin
        Geral.MensagemBox('Erro no refresh do lote. O aplicativo ' +
        'dever� ser encerrado!', 'Erro', MB_OK+MB_ICONERROR);
        Application.Terminate;
      end;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  FmPrincipal.AtualizaLastEditLote(DCompra);
end;

procedure TFmLot2Chq.BtSaidaClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := FmLot2Cab.QrLotCodigo.Value;
    FmLot2Cab.CalculaLote(Codigo, True);
    FmLot2Cab.LocCod(Codigo, Codigo);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  Close;
end;

procedure TFmLot2Chq.CalculaDiasCH();
var
  Comp, DMai: Integer;
  Vcto, Data: TDateTime;
begin
  Vcto := Geral.ValidaDataSimples(EdVencto.Text, True);
  Data := Geral.ValidaDataSimples(EdData.Text, True);
  DMai := EdDMaisC.ValueVariant;
  // busca CBE e SBC do Lote pois j� est� definido
  Comp := FmPrincipal.VerificaDiasCompensacao(EdregiaoCompe.ValueVariant,
          FmLot2Cab.QrLotCBE.Value, EdValor.ValueVariant, Data, Vcto,
          FmLot2Cab.QrLotSCB.Value);
  EdDias.ValueVariant := UMyMod.CalculaDias(Int(Data), Int(Vcto), DMai, Comp,
          Dmod.QrControleTipoPrazoDesc.Value, FmLot2Cab.QrLotCBE.Value);
  EdCompensacao.ValueVariant := Comp;
  EdPrazo.ValueVariant := Trunc(Int(Vcto)-Int(Data));
  //
  CalculaValoresChequeAlterado();
end;

procedure TFmLot2Chq.CalculaValoresChequeAlterado();
var
  TaxaT, Base, JuroT: Double;
  Dias, i: Integer;
begin
  FmLot2Cab.FTaxa[0] := EdTxaCompra.ValueVariant;
  for i := 1 to GradeCH.RowCount -1 do FmLot2Cab.FTaxa[i] :=
    Geral.DMV(GradeCH.Cells[1, i]);
  // zerar n�o setados
  for i := GradeCH.RowCount to 6 do FmLot2Cab.FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT + FmLot2Cab.FTaxa[i];
  Base := EdValor.ValueVariant;
  Dias := EdDias.ValueVariant;
  //juros compostos ?
  JuroT   := MLAGeral.CalculaJuroComposto(TaxaT, Dias);
  //
  EdTotalCompraPer.ValueVariant := TaxaT;
  //
  if TaxaT > FmLot2Cab.FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Base / 100;
    end else begin
      FmLot2Cab.FJuro[0] := MLAGeral.CalculaJuroComposto(FmLot2Cab.FTaxa[0], Dias);
      FmLot2Cab.FValr[0] := FmLot2Cab.FJuro[0] * Base / 100;
      JuroT := JuroT - FmLot2Cab.FJuro[0];
      TaxaT := TaxaT - FmLot2Cab.FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FmLot2Cab.FJuro[i] := 0 else
          FmLot2Cab.FJuro[i] := FmLot2Cab.FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FmLot2Cab.FValr[i] := FmLot2Cab.FJuro[i] * Base / 100;
    end;
  end else begin
    FmLot2Cab.FJuro[0] := JuroT;
    FmLot2Cab.FValr[0] := JuroT * Base / 100;
  end;
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmLot2Chq.ConsultaCNPJCPF_CH();
var
  Solicitante, RG, UF, Conta, Endereco, Telefone, CEP: String;
  Nascimento: TDateTime;
  Banco, Agencia, Cheque, DigitoCC, DigitoCH, Qtde: Integer;
  Valor, ValMin, ValMax: Double;
  //Continua: Boolean;
begin
  Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
  RG          := '';
  UF          := '';
  Nascimento  := 0;
  //
  Banco       := EdBanco.ValueVariant;
  Agencia     := EdAgencia.ValueVariant;
  Conta       := EdRealCC.Text;
  Cheque      := EdCheque.ValueVariant;
  //
  DigitoCC    := 0;
  DigitoCH    := 0;
  Qtde        := 1;
  Valor       := EdValor.ValueVariant;
  //
  Endereco    := '';
  Telefone    := '';
  CEP         := '';
  //
  ValMin      := 0;
  ValMax      := 0;
  //
  EdStatusSPC.ValueVariant :=
  UnSPC.ConsultaSPC_Rapida(DmLot.QrSPC_CliCodigo.Value, True, EdCPF.Text,
    EdEmitente.Text, DmLot.QrSPC_CliServidor.Value,
    DmLot.QrSPC_CliSPC_Porta.Value, DmLot.QrSPC_CliCodigSocio.Value,
    DmLot.QrSPC_CliSenhaSocio.Value, DmLot.QrSPC_CliModalidade.Value,
    Solicitante, FmLot2Cab.QrLotData.Value, DmLot.QrSPC_CliBAC_CMC7.Value,
    EdBanda.Text, RG, UF, Nascimento, Banco, FormatFloat('0', Agencia), Conta, DigitoCC,
    Cheque, DigitoCH, Qtde, DmLot.QrSPC_CliTipoCred.Value, Valor,
    DmLot.QrSPC_CliValAviso.Value, DmLot.QrSPC_CliVALCONSULTA.Value,
    Endereco, Telefone, CEP, ValMin, ValMax, nil, MeSPC, True);
end;

procedure TFmLot2Chq.Desiste();
var
  Codigo: Integer;
begin
  FmPrincipal.FLoteWebDone := 0;
  Codigo := FmLot2Cab.QrLotCodigo.Value;
  FmLot2Cab.CalculaLote(Codigo, False);
  FmLot2Cab.LocCod(Codigo, Codigo);
  Close;
end;

procedure TFmLot2Chq.EdAgenciaChange(Sender: TObject);
begin
  LocalizaEmitente(True);
end;

procedure TFmLot2Chq.EdBancoChange(Sender: TObject);
begin
  LocalizaEmitente(True);
  if not EdBanco.Focused then
    DmLot2.ReopenBanco(EdBanco.ValueVariant);
end;

procedure TFmLot2Chq.EdBandaChange(Sender: TObject);
begin
  BtConfirma2.Enabled := False;
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmLot2Chq.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Desiste();
end;

procedure TFmLot2Chq.EdCMC_7Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7.Text;
    EdTipific.Text       := Banda.Tipo;
    EdRegiaoCompe.Text   := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    // precisa para n�o pular direto para a data
    EdValor.Text := '';
    //
    EdValor.SetFocus;
    BtConfirma2.Enabled := True;
  end;
end;

procedure TFmLot2Chq.EdContaChange(Sender: TObject);
begin
  LocalizaEmitente(True);
end;

procedure TFmLot2Chq.EdCPFChange(Sender: TObject);
begin
  EdCR_E.ValueVariant := 0;
  EdCV_E.ValueVariant := 0;
  EdCT_E.ValueVariant := 0;
  EdDR_E.ValueVariant := 0;
  EdDV_E.ValueVariant := 0;
  EdDT_E.ValueVariant := 0;
  EdRT_E.ValueVariant := 0;
  EdVT_E.ValueVariant := 0;
  EdST_E.ValueVariant := 0;
  EdOA_E.ValueVariant := 0;
  EdTT_E.ValueVariant := 0;
  //
  DmLot.FechaTudoSac();
  EdStatusSPC.ValueVariant := -1;
end;

procedure TFmLot2Chq.EdCPFExit(Sender: TObject);
var
  //Num : String;
  CPF : String;
begin
  Screen.Cursor := crHourGlass;
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if (CPF <> '') and (CPF = Geral.FormataCNPJ_TFT(CPF)) then
  begin
    if EdEmitente.Text = '' then
    begin
      DmLot2.QrLocEmiCPF.Close;
      DmLot2.QrLocEmiCPF.Params[0].AsString := CPF;
      UMyMod.AbreQuery(DmLot2.QrLocEmiCPF, Dmod.MyDB);
      //
      if DmLot2.QrLocEmiCPF.RecordCount > 0 then
        EdEmitente.Text := DmLot2.QrLocEmiCPFNome.Value;
    end;
    DmLot.PesquisaRiscoSacado(EdCPF.Text,
      EdCR_E, EdCV_E, EdCT_E,
      EdDR_E, EdDV_E, EdDT_E,
      EdRT_E, EdVT_E, EdST_E,
      EdOA_E, EdTT_E);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmLot2Chq.EdCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF.Text);
    UMyMod.AbreQuery(Dmod.QrLocCPF, Dmod.MyDB);
    case Dmod.QrLocCPF.RecordCount of
      0: Geral.MensagemBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotEmi, FmLotEmi);
        FmLotEmi.ShowModal;
        if FmLotEmi.FEmitenteNome <> '' then
          EdEmitente.Text := FmLotEmi.FEmitenteNome;
        FmLotEmi.Destroy;
      end;
    end;
  end;
end;

procedure TFmLot2Chq.EdDataExit(Sender: TObject);
var
  Data: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdData.Text);
  Data := Geral.ValidaDataSimples(EdData.Text, False);
  if Data > 2 then
  begin
    if Data <= Date then
      if Casas < 6 then Data := IncMonth(Data, 12);
    EdData.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdData.SetFocus;
  CalculaDiasCH();
end;

procedure TFmLot2Chq.EdDMaisCExit(Sender: TObject);
begin
  CalculaDiasCH();
end;

procedure TFmLot2Chq.EdRegiaoCompeChange(Sender: TObject);
begin
  CalculaDiasCH();
end;

procedure TFmLot2Chq.EdRegiaoCompeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Desiste();
end;

procedure TFmLot2Chq.EdStatusSPCChange(Sender: TObject);
var
  Status: Integer;
begin
  Status := EdStatusSPC.ValueVariant;
  EdStatusSPC_TXT.Text := MLAGeral.StatusSPC_TXT(Status);
  EdStatusSPC_TXT.Font.Color := MLAGeral.StatusSPC_Color(Status);
end;

procedure TFmLot2Chq.EdStatusSPCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    ConsultaCNPJCPF_CH();
end;

procedure TFmLot2Chq.EdStatusSPC_TXTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    ConsultaCNPJCPF_CH();
end;

procedure TFmLot2Chq.EdTxaCompraExit(Sender: TObject);
begin
  CalculaValoresChequeAlterado;
end;

procedure TFmLot2Chq.EdValorExit(Sender: TObject);
begin
  CalculaDiasCH();
end;

procedure TFmLot2Chq.EdVenctoEnter(Sender: TObject);
begin
  if EdValor.ValueVariant < 0.01 then
    EdValor.SetFocus;
end;

procedure TFmLot2Chq.EdVenctoExit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  if EdValor.ValueVariant < 0.01 then
  begin
    EdValor.SetFocus;
    Exit;
  end;
  Casas := Length(EdVencto.Text);
  Data := Geral.ValidaDataSimples(EdVencto.Text, False);
  Emis := Geral.ValidaDataSimples(EdData.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Geral.MensagemBox('Data j� passada! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdVencto.SetFocus;
      end else begin
        EdVencto.SetFocus;
        Exit;
      end;
    end;
    EdVencto.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdVencto.SetFocus;
  CalculaDiasCH();
end;

procedure TFmLot2Chq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdBanda.SetFocus;
end;

procedure TFmLot2Chq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Chq.GradeCHDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeCH = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeCH.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeCH.Canvas.Handle, TA_LEFT);
      GradeCH.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeCH.Cells[Acol, ARow]);
      SetTextAlign(GradeCH.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeCH.Canvas.Handle, TA_RIGHT);
      GradeCH.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeCH.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeCH.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLot2Chq.GradeCHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CalculaValoresChequeAlterado;
  if key=VK_RETURN then
  begin
    if GradeCH.EditorMode = True then
    begin
      if (GradeCH.Row < GradeCH.RowCount-1) then
        GradeCH.Row := GradeCH.Row +1
      else BtConfirma2.SetFocus;
    end;
  end
end;

function TFmLot2Chq.LocalizaEmitente(MudaNome: Boolean): Boolean;
(*
var
  B,A,C: String;
*)
begin
(*
  Result := False;
  if  (Geral.IMV(EdBanco.Text) > 0)
  and (Geral.IMV(EdAgencia.Text) > 0)
  and (Geral.DMV(EdConta.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      //
      if MudaNome then
      begin
        B := EdBanco.Text;
        A := EdAgencia.Text;
        C := EdConta.Text;
        //
        DmLot2.QrLocEmiBAC.Close;
        DmLot2.QrLocEmiBAC.Params[0].AsInteger := DmLot2.QrBancoDVCC.Value;
        DmLot2.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,DmLot2.QrBancoDVCC.Value);
        UMyMod.AbreQuery(DmLot2.QrLocEmiBAC);
        //
        if DmLot2.QrLocEmiBAC.RecordCount = 0 then
        begin
          EdEmitente.Text := '';
          EdCPF.Text := '';
        end else begin
          DmLot2.QrLocEmiCPF.Close;
          DmLot2.QrLocEmiCPF.Params[0].AsString := DmLot2.QrLocEmiBACCPF.Value;
          UMyMod.AbreQuery(DmLot2.QrLocEmiCPF);
          //
          if DmLot2.QrLocEmiCPF.RecordCount = 0 then
          begin
            EdEmitente.Text := '';
            EdCPF.Text := '';
          end else begin
            EdEmitente.Text := DmLot2.QrLocEmiCPFNome.Value;
            EdCPF.Text      := Geral.FormataCNPJ_TT(DmLot2.QrLocEmiCPFCPF.Value);
            DmLot.PesquisaRiscoSacado(EdCPF.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
*)
  Result := DmLot2.LocalizaEmitente2(MudaNome, EdBanco.Text, EdAgencia.Text,
  EdConta.Text, DmLot2.QrBancoDVCC.Value, EdEmitente, EdCPF, EdRealCC);
  if EdCPF.Text <> '' then
    DmLot.PesquisaRiscoSacado(EdCPF.Text,
      EdCR_E, EdCV_E, EdCT_E,
      EdDR_E, EdDV_E, EdDT_E,
      EdRT_E, EdVT_E, EdST_E,
      EdOA_E, EdTT_E);

end;



{
104167150090010605700300005469
77.190.379/0001-74
CONGR EVANG LUTERANA SAO MARCOS

399003690099121555000362882904
029.859.889-26
CLEIDE MERLI AREND

356019850090101455800097297626
581.717.540-15
MARCO LUCIANO AREND
}

end.
