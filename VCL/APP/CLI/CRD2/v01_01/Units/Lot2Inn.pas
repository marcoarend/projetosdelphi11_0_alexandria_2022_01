unit Lot2Inn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, Menus, MyListas, UnDmkEnums;

type
  TFmLot2Inn = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Pagina: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    Panel12: TPanel;
    Splitter1: TSplitter;
    GradeC_L: TDBGrid;
    PGAbertos_CH: TPageControl;
    TabSheet3: TTabSheet;
    GradeDev: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet33: TTabSheet;
    Panel49: TPanel;
    DBGrid23: TDBGrid;
    StCPF_CH: TStaticText;
    Panel7: TPanel;
    Panel42: TPanel;
    Panel43: TPanel;
    Label29: TLabel;
    Label131: TLabel;
    LaVerSuit1: TLabel;
    BtConfirma1: TBitBtn;
    BtEntidades: TBitBtn;
    BtPagtosExclui: TBitBtn;
    EdQtdeCh: TdmkEdit;
    EdValorCH: TdmkEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    BtCuidado: TBitBtn;
    BtEntiTroca1: TBitBtn;
    BtSPC_CH: TBitBtn;
    DBGrid1: TDBGrid;
    GroupBox5: TGroupBox;
    Label30: TLabel;
    Label32: TLabel;
    Label142: TLabel;
    Label143: TLabel;
    Label144: TLabel;
    Label145: TLabel;
    Label146: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    EdDR_C: TdmkEdit;
    EdDT_C: TdmkEdit;
    EdRT_C: TdmkEdit;
    EdVT_C: TdmkEdit;
    EdST_C: TdmkEdit;
    EdCT_C: TdmkEdit;
    EdDV_C: TdmkEdit;
    EdCR_C: TdmkEdit;
    EdCV_C: TdmkEdit;
    EdOA_C: TdmkEdit;
    EdTT_C: TdmkEdit;
    DBEdit30: TDBEdit;
    GradeC: TStringGrid;
    TabSheet2: TTabSheet;
    Splitter2: TSplitter;
    PGAbertos_DU: TPageControl;
    TabSheet5: TTabSheet;
    Panel16: TPanel;
    DBGrid4: TDBGrid;
    Panel17: TPanel;
    DBGrid2: TDBGrid;
    TabSheet34: TTabSheet;
    Panel50: TPanel;
    DBGrid24: TDBGrid;
    StCPF_DU: TStaticText;
    GradeD_L: TDBGrid;
    Panel8: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    Label31: TLabel;
    Label132: TLabel;
    LaVerSuit2: TLabel;
    BtConfirma3: TBitBtn;
    BtPagtosExclui2: TBitBtn;
    EdQtdeDU: TdmkEdit;
    EdValorDU: TdmkEdit;
    DBEdit14: TDBEdit;
    DBEdit24: TDBEdit;
    BtEntidades2: TBitBtn;
    BtCuidado2: TBitBtn;
    BtEntiTroca2: TBitBtn;
    BtSPC_DU: TBitBtn;
    GradeLDU1: TDBGrid;
    GradeLDU2: TDBGrid;
    GroupBox4: TGroupBox;
    Label135: TLabel;
    Label136: TLabel;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    Label140: TLabel;
    Label141: TLabel;
    Label147: TLabel;
    Label148: TLabel;
    EdDR_D: TdmkEdit;
    EdDT_D: TdmkEdit;
    EdRT_D: TdmkEdit;
    EdVT_D: TdmkEdit;
    EdST_D: TdmkEdit;
    EdCT_D: TdmkEdit;
    EdDV_D: TdmkEdit;
    EdCR_D: TdmkEdit;
    EdCV_D: TdmkEdit;
    EdOA_D: TdmkEdit;
    EdTT_D: TdmkEdit;
    DBEdit31: TDBEdit;
    GradeD: TStringGrid;
    Panel9: TPanel;
    Label33: TLabel;
    EdCodCli: TdmkEdit;
    EdNomCli: TdmkEdit;
    Panel10: TPanel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    TPCheque: TdmkEditDateTimePicker;
    EdQtdeTO: TdmkEdit;
    EdValorTO: TdmkEdit;
    BtFechar: TBitBtn;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    PMCadastraCH: TPopupMenu;
    Emitenteselecionado1: TMenuItem;
    Todosemitentesnocadastrados1: TMenuItem;
    PMCadastraDU: TPopupMenu;
    Sacadoselecionado1: TMenuItem;
    Todossacadosnocadastrados1: TMenuItem;
    PMSPC_Import: TPopupMenu;
    VerificatodosCPFCNPJpelovalormnimoconfigurao1: TMenuItem;
    VerificatodosCPFCNPJaindanoverificados1: TMenuItem;
    N7: TMenuItem;
    VerificaCPFCNPJatual1: TMenuItem;
    ConsultaosCPFCNPJdositensselecionados1: TMenuItem;
    N6: TMenuItem;
    Abrejaneladeconsulta1: TMenuItem;
    N8: TMenuItem;
    Imprimeconsultaselecionada1: TMenuItem;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirma1Click(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure BtCuidadoClick(Sender: TObject);
    procedure BtEntiTroca1Click(Sender: TObject);
    procedure BtPagtosExcluiClick(Sender: TObject);
    procedure BtSPC_CHClick(Sender: TObject);
    procedure EdCR_CChange(Sender: TObject);
    procedure EdCV_CChange(Sender: TObject);
    procedure EdCT_CChange(Sender: TObject);
    procedure EdDR_CChange(Sender: TObject);
    procedure EdDV_CChange(Sender: TObject);
    procedure EdDT_CChange(Sender: TObject);
    procedure EdRT_CChange(Sender: TObject);
    procedure EdVT_CChange(Sender: TObject);
    procedure EdST_CChange(Sender: TObject);
    procedure EdOA_CChange(Sender: TObject);
    procedure EdTT_CChange(Sender: TObject);
    procedure GradeC_LDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeC_LTitleClick(Column: TColumn);
    procedure GradeDevDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdCodCliChange(Sender: TObject);
    procedure EdNomCliChange(Sender: TObject);
    procedure TPChequeChange(Sender: TObject);
    procedure BtFecharClick(Sender: TObject);
    procedure BtConfirma3Click(Sender: TObject);
    procedure BtEntidades2Click(Sender: TObject);
    procedure BtCuidado2Click(Sender: TObject);
    procedure BtEntiTroca2Click(Sender: TObject);
    procedure BtPagtosExclui2Click(Sender: TObject);
    procedure BtSPC_DUClick(Sender: TObject);
    procedure GradeLDU1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeLDU2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeD_LDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeD_LTitleClick(Column: TColumn);
    procedure Emitenteselecionado1Click(Sender: TObject);
    procedure Todosemitentesnocadastrados1Click(Sender: TObject);
    procedure Sacadoselecionado1Click(Sender: TObject);
    procedure Todossacadosnocadastrados1Click(Sender: TObject);
    procedure VerificatodosCPFCNPJpelovalormnimoconfigurao1Click(
      Sender: TObject);
    procedure VerificatodosCPFCNPJaindanoverificados1Click(Sender: TObject);
    procedure VerificaCPFCNPJatual1Click(Sender: TObject);
    procedure ConsultaosCPFCNPJdositensselecionados1Click(Sender: TObject);
    procedure Abrejaneladeconsulta1Click(Sender: TObject);
    procedure Imprimeconsultaselecionada1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure ExcluiImportLote(Tipo, Ordem: Integer);
    procedure IgnoraImportLote(Tipo, Ordem: Integer);
    procedure LocalizaProximoChNaoVerde();
    procedure LocalizaProximoDUNaoVerde();
    function  NaoPermiteCHs(): Boolean;
    function  NaoPermiteDUs(): Boolean;
    procedure VerificaNomeImport();
    procedure VerificaSeFecha();
  public
    { Public declarations }
    procedure HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
  end;

  var
  FmLot2Inn: TFmLot2Inn;

implementation

uses UnMyObjects, Module, ModuleLot, Principal, Lot2Cab, SPC_Pesq,
  MyDBCheck, ModuleLot2, ArquivosSCX, Lot2Cad, UMySQLModule;

{$R *.DFM}

procedure TFmLot2Inn.Abrejaneladeconsulta1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Pesq, FmSPC_Pesq, afmoNegarComAviso) then
  begin
    FmSPC_Pesq.ShowModal;
    FmSPC_Pesq.Destroy;
  end;
end;

procedure TFmLot2Inn.BtConfirma1Click(Sender: TObject);
begin
  if NaoPermiteCHs() then Exit;
  FmLot2Cab.FIncluindoCh := True;
(*
  IncluiRegistro(8, 0);
  EdCliente.Text := EdCodCli.Text;
  CBCliente.KeyValue := EdCodCli.ValueVariant;
  TPData.Date := TPCheque.Date;
*)
  FmLot2Cab.IncluiLoteCab(stIns, loteCheque, False, True,
    EdCodCli.ValueVariant, TPCheque.Date);
  //
  VerificaSeFecha();
end;

procedure TFmLot2Inn.BtConfirma3Click(Sender: TObject);
begin
  if NaoPermiteDUs() then Exit;
  FmLot2Cab.FIncluindoDU := True;
  DmLot2.FDataTransacao := TPCheque.Date;

(*
  IncluiRegistro(8, 1);
  EdCliente.Text := EdCodCli.Text;
  CBCliente.KeyValue := EdCodCli.ValueVariant;
  TPData.Date := TPCheque.Date;
*)
  FmLot2Cab.IncluiLoteCab(stIns, loteDuplicata, False, True,
    EdCodCli.ValueVariant, TPCheque.Date);
  //
  VerificaSeFecha();
end;

procedure TFmLot2Inn.BtCuidado2Click(Sender: TObject);
begin
  IgnoraImportLote(DmLot2.QrLDUsTipo.Value, DmLot2.QrLDUsOrdem.Value);
  LocalizaProximoDUNaoVerde();
end;

procedure TFmLot2Inn.BtCuidadoClick(Sender: TObject);
begin
  IgnoraImportLote(DmLot2.QrLCHsTipo.Value, DmLot2.QrLCHsOrdem.Value);
  LocalizaProximoChNaoVerde();
end;

procedure TFmLot2Inn.BtEntidades2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCadastraDU, BtEntidades2);
  LocalizaProximoDUNaoVerde();
end;

procedure TFmLot2Inn.BtEntidadesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCadastraCH, BtEntidades);
  LocalizaProximoChNaoVerde();
end;

procedure TFmLot2Inn.BtEntiTroca1Click(Sender: TObject);
begin
  Emitenteselecionado1Click(Self);
  LocalizaProximoChNaoVerde();
end;

procedure TFmLot2Inn.BtEntiTroca2Click(Sender: TObject);
begin
  FmPrincipal.AtualizaSacado(DmLot2.QrLDUsCPF.Value,
                             DmLot2.QrLDUsEmitente.Value,
                             DmLot2.QrLDUsRua.Value,
                             DmLot2.QrLDUsNumero.Value,
                             DmLot2.QrLDUsCompl.Value,
                             DmLot2.QrLDUsBirro.Value,
                             DmLot2.QrLDUsCidade.Value,
                             DmLot2.QrLDUsUF.Value,
                             Geral.SoNumero_TT(Geral.FormataCEP_NT(DmLot2.QrLDUsCEP.Value)),
                             MLAGeral.SoNumeroESinal_TT(DmLot2.QrLDUsTel1.Value),
                             DmLot2.QrLDUsIE.Value, '', -1);
  DmLot2.ReopenImportLote(DmLot2.QrLCHsOrdem.Value, DmLot2.QrLDUsOrdem.Value);
  LocalizaProximoDUNaoVerde();
end;

procedure TFmLot2Inn.BtFecharClick(Sender: TObject);
begin
  //MostraEdicao(0, CO_TRAVADO, 0);
  Application.ProcessMessages;
  FmLot2Cab.DefineRiscos(FmLot2Cab.QrLotCliente.Value);
  Close;
end;

procedure TFmLot2Inn.BtPagtosExclui2Click(Sender: TObject);
begin
  ExcluiImportLote(DmLot2.QrLDUsTipo.Value, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.BtPagtosExcluiClick(Sender: TObject);
begin
  ExcluiImportLote(DmLot2.QrLCHsTipo.Value, DmLot2.QrLCHsOrdem.Value);
end;

procedure TFmLot2Inn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2Inn.BtSPC_CHClick(Sender: TObject);
begin
  PGAbertos_CH.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMSPC_Import, BtSPC_CH);
end;

procedure TFmLot2Inn.BtSPC_DUClick(Sender: TObject);
begin
  PGAbertos_DU.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMSPC_Import, BtSPC_DU);
end;

procedure TFmLot2Inn.ConsultaosCPFCNPJdositensselecionados1Click(
  Sender: TObject);
begin
  DmLot2.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istSelecionados);
end;

procedure TFmLot2Inn.EdCodCliChange(Sender: TObject);
begin
  VerificaNomeimport();
end;

procedure TFmLot2Inn.EdCR_CChange(Sender: TObject);
begin
  if DmLot.QrRiscoTCValor.Value >= 0.01 then
  begin
    //EdCR.Font.Color := $000080FF; // Laranja intenso
    EdCR_C.Font.Color := $000080FF; // Laranja intenso
    EdCR_D.Font.Color := $000080FF; // Laranja intenso
  end else begin
    //EdCR.Font.Color := clSilver;
    EdCR_C.Font.Color := clSilver;
    EdCR_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdCT_CChange(Sender: TObject);
begin
  Exit;
  if Geral.DMV(EdCT_C.Text) >= 0.01 then
  begin
    //EdCT.Font.Color := clRed;
    EdCT_C.Font.Color := clRed;
    EdCT_D.Font.Color := clRed;
  end else begin
    //EdCT.Font.Color := clSilver;
    EdCT_C.Font.Color := clSilver;
    EdCT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdCV_CChange(Sender: TObject);
begin
  if DmLot.FCHDevOpen_Total >= 0.01 then
  begin
    //EdCV.Font.Color := clRed;
    EdCV_C.Font.Color := clRed;
    EdCV_D.Font.Color := clRed;
  end else begin
    //EdCV.Font.Color := clSilver;
    EdCV_C.Font.Color := clSilver;
    EdCV_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdDR_CChange(Sender: TObject);
begin
  if Geral.DMV(EdDR_C.Text) >= 0.01 then
  begin
    //EdDR.Font.Color := $000080FF; // Laranja intenso
    EdDR_C.Font.Color := $000080FF; // Laranja intenso
    EdDR_D.Font.Color := $000080FF; // Laranja intenso
  end else begin
    //EdDR.Font.Color := clSilver;
    EdDR_C.Font.Color := clSilver;
    EdDR_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdDT_CChange(Sender: TObject);
begin
  Exit;
  if Geral.DMV(EdDT_C.Text) >= 0.01 then
  begin
    //EdDT.Font.Color := clRed;
    EdDT_C.Font.Color := clRed;
    EdDT_D.Font.Color := clRed;
  end else begin
    //EdDT.Font.Color := clSilver;
    EdDT_C.Font.Color := clSilver;
    EdDT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdDV_CChange(Sender: TObject);
begin
  if Geral.DMV(EdDV_C.Text) >= 0.01 then
  begin
    //EdDV.Font.Color := clRed;
    EdDV_C.Font.Color := clRed;
    EdDV_D.Font.Color := clRed;
  end else begin
    //EdDV.Font.Color := clSilver;
    EdDV_C.Font.Color := clSilver;
    EdDV_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdNomCliChange(Sender: TObject);
begin
  VerificaNomeimport();
end;

procedure TFmLot2Inn.EdOA_CChange(Sender: TObject);
begin
  if DmLot.FOcorA_Total >= 0.01 then
  begin
    //EdOA.Font.Color := clRed;
    EdOA_C.Font.Color := clRed;
    EdOA_D.Font.Color := clRed;
  end else begin
    //EdOA.Font.Color := clSilver;
    EdOA_C.Font.Color := clSilver;
    EdOA_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdRT_CChange(Sender: TObject);
begin
  if Geral.DMV(EdRT_C.Text) >= 0.01 then
  begin
    //EdRT.Font.Color := $000080FF; // Laranja intenso
    EdRT_C.Font.Color := $000080FF; // Laranja intenso
    EdRT_D.Font.Color := $000080FF; // Laranja intenso
  end else begin
    //EdRT.Font.Color := clSilver;
    EdRT_C.Font.Color := clSilver;
    EdRT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdST_CChange(Sender: TObject);
begin
  if Geral.DMV(EdVT_C.Text) >= 0.01 then
  begin
    //EdVT.Font.Color := clRed;
    EdVT_C.Font.Color := clRed;
    EdVT_D.Font.Color := clRed;
  end else begin
    //EdVT.Font.Color := clSilver;
    EdVT_C.Font.Color := clSilver;
    EdVT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdTT_CChange(Sender: TObject);
begin
  if Geral.DMV(EdTT_C.Text) >= 0.01 then
  begin
    //EdTT.Font.Color := clRed;
    EdTT_C.Font.Color := clRed;
    EdTT_D.Font.Color := clRed;
  end else begin
    //EdTT.Font.Color := clSilver;
    EdTT_C.Font.Color := clSilver;
    EdTT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.EdVT_CChange(Sender: TObject);
begin
  if Geral.DMV(EdVT_C.Text) >= 0.01 then
  begin
    //EdVT.Font.Color := clRed;
    EdVT_C.Font.Color := clRed;
    EdVT_D.Font.Color := clRed;
  end else begin
    //EdVT.Font.Color := clSilver;
    EdVT_C.Font.Color := clSilver;
    EdVT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLot2Inn.Emitenteselecionado1Click(Sender: TObject);
var
  Banco, Agencia, Conta, CPF, Nome: String;
  Ordem: Integer;
begin
  Ordem    := DmLot2.QrLCHsOrdem.Value;
  Banco    := MLAGeral.FFD(DmLot2.QrLCHsBanco.Value, 3, siPositivo);
  Agencia  := MLAGeral.FFD(DmLot2.QrLCHsAgencia.Value, 4, siPositivo);
  Conta    := DmLot2.QrLCHsConta.Value;
  CPF      := Geral.SoNumero_TT(DmLot2.QrLCHsCPF.Value);
  Nome     := DmLot2.QrLCHsEmitente.Value;
  DmLot2.AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome, -1);
  FmPrincipal.AtualizaAcumulado(PB1);
  //
  DmLot2.ReopenImportLote(Ordem, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.ExcluiImportLote(Tipo, Ordem: Integer);
var
  CH, DU: Integer;
begin
  if Geral.MensagemBox('Confirma a exclus�o da seq. n� ' +
  IntToStr(Ordem) + '?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then begin
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM importlote WHERE Tipo=:P0 AND Ordem=:P1');
    Dmod.QrUpdL.Params[00].AsInteger := Tipo;
    Dmod.QrUpdL.Params[01].AsInteger := Ordem;
    Dmod.QrUpdL.ExecSQL;
    //
    if Tipo = 0 then
    begin
      CH := Ordem;
      DU := DmLot2.QrLDUsOrdem.Value;
    end else begin
      DU := Ordem;
      CH := DmLot2.QrLCHsOrdem.Value;
    end;
    DmLot2.ReopenImportLote(CH, DU);
  end;
end;

procedure TFmLot2Inn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmLot2Inn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmLot.FShowLot2Inn := False;
end;

procedure TFmLot2Inn.FormCreate(Sender: TObject);
begin
  DmLot.FShowLot2Inn := True;
  //
  Pagina.Align := alClient;
  DmLot2.ReopenImportLote(0,0);
  ArqSCX.ConfiguraGrades(GradeC, GradeD, Pagina);
end;

procedure TFmLot2Inn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Inn.GradeC_LDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor1, Cor2: TColor;
  OldAlign: Integer;
  Fon: Boolean;
begin
  if (Column.FieldName = 'Disponiv') then
  begin
    if DmLot2.QrLCHsDisponiv.Value < 0 then Cor1 := clRed
    else if DmLot2.QrLCHsDisponiv.Value < DmLot2.QrLCHsValor.Value then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else if (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA') 
  or (Column.FieldName = 'QtdeCH') then
  begin
    if DmLot2.QrLCHsOcorrA.Value > 0.009 then Cor1 := clRed
    else if DmLot2.QrLCHsQtdeCH.Value > 0 then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else begin
    case DmLot2.QrLCHsMEU_SIT.Value of
      0: Cor1 := clBlue;
      1: Cor1 := $0023AAEF; // Laranja
      2: Cor1 := clGreen;
      else Cor1 := clFuchsia;
    end;
  end;
  if DmLot2.QrLCHsValor.Value >= Dmod.QrControleCHValAlto.Value then Cor2 := clPurple else
  if DmLot2.QrLCHsOcorrA.Value > 0 then Cor2 := clRed else
  if DmLot2.QrLCHsOcorrT.Value > 0 then
  Cor2 := clFuchsia else
  if DmLot2.QrLCHsAberto.Value > 0 then Cor2 := $0023AAEF // Laranja
  else Cor2 := clGreen;
  //
  //Fon := False;
  if Cor1 = clBlack then Fon := False else Fon := True;
  //
  if (Column.FieldName = 'Valor')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor2;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Ordem')
  //or (Column.FieldName = 'Comp')
  //or (Column.FieldName = 'Banco')
  //or (Column.FieldName = 'Agencia')
  //or (Column.FieldName = 'Cheque')
  or (Column.FieldName = 'AberCH')
  or (Column.FieldName = 'Risco')
  or (Column.FieldName = 'Aberto')
  or (Column.FieldName = 'Disponiv')
  or (Column.FieldName = 'DIAS')
  or (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA')
  or (Column.FieldName = 'QtdeCH')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Conta')
  or (Column.FieldName = 'DVence')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'CPF_TXT')
  or (Column.FieldName = 'Emitente')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'NOMEDUPLICADO')
  then begin
    with GradeC_L.Canvas do
    begin
      if DmLot2.QrLCHsDuplicado.Value = 0 then Font.Color := clGreen
      else Font.Color := clRed;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'TEXTO_SPC')
  then begin
    with GradeC_L.Canvas do
    begin
      case DmLot2.QrLCHsStatusSPC.Value of
        -1: Font.Color := clGray;
         0: Font.Color := clGreen;
         1: Font.Color := clBlue;
         2: Font.Color := clRed;
        else Font.Color := clFuchsia;
      end;
      Font.Style := [fsBold];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmLot2Inn.GradeC_LTitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := DmLot2.FORDA_LCHs;
  DmLot2.FORDA_LCHs := Column.FieldName;
  if DmLot2.FORDA_LCHs = 'CPF_TXT' then DmLot2.FORDA_LCHs := 'CPF';
  if DmLot2.FORDA_LCHs = 'NOMECHPLICADO' then DmLot2.FORDA_LCHs := 'Duplicado';
  if DmLot2.FORDA_LCHs = 'DIAS' then DmLot2.FORDA_LCHs := 'DVence';
  if Antigo = DmLot2.FORDA_LCHs then
  DmLot2.FORDB_LCHs := MLAGeral.InverteOrdemAsc(DmLot2.FORDB_LCHs);
  DmLot2.ReopenImportLote(DmLot2.QrLCHsOrdem.Value, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.GradeDevDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  OldAlign: Integer;
  Fon: Boolean;
begin
  case DmLot.QrAlinItsStatus.Value of
    0: Cor := clRed;
    1: Cor := $0023AAEF; // Laranja
    2: Cor := clNavy;
    else
    Cor := clFuchsia;
  end;
  //
  Fon := False;
  //
  if (Column.FieldName = 'Alinea1')
  or (Column.FieldName = 'Alinea2')
  or (Column.FieldName = 'Banco')
  or (Column.FieldName = 'Agencia')
  or (Column.FieldName = 'Cheque')
  or (Column.FieldName = 'Valor')
  or (Column.FieldName = 'Taxas')
  or (Column.FieldName = 'ValPago')
  then begin
    with GradeDev.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Conta')
  or (Column.FieldName = 'DATA1_TXT')
  or (Column.FieldName = 'DATA2_TXT')
  or (Column.FieldName = 'DATA3_TXT')
  then begin
    with GradeDev.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'NOMECLIENTE')
  or (Column.FieldName = 'CPF_TXT')
  or (Column.FieldName = 'Emitente')
  then begin
    with GradeDev.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmLot2Inn.GradeD_LDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor1, Cor2: TColor;
  OldAlign: Integer;
  Fon: Boolean;
begin
  if (Column.FieldName = 'Disponiv') then
  begin
    if DmLot2.QrLDUsDisponiv.Value < 0 then Cor1 := clRed
    //else if (DmLot2.QrLDUsDISPONIVEL.Value - DmLot2.QrLDUsValor.Value) < 0 then Cor1 := $000080FF // Laranja intenso
    else if DmLot2.QrLDUsDisponiv.Value < DmLot2.QrLDUsValor.Value then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else if (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA')
  or (Column.FieldName = 'QtdeCH') then
  begin
    if DmLot2.QrLDUsOcorrA.Value > 0.009 then Cor1 := clRed
    else if DmLot2.QrLDUsQtdeCH.Value > 0 then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else begin
    case DmLot2.QrLDUsMEU_SIT.Value of
      0: Cor1 := clBlue;
      1: Cor1 := $0023AAEF; // Laranja
      2: Cor1 := clGreen;
      else
       Cor1 := clFuchsia;
    end;
  end;
  if DmLot2.QrLDUsValor.Value >= Dmod.QrControleCHValAlto.Value then Cor2 := clPurple else
  if DmLot2.QrLDUsOcorrA.Value > 0 then Cor2 := clRed else
  if DmLot2.QrLDUsOcorrT.Value > 0 then
  Cor2 := clFuchsia else
  if DmLot2.QrLDUsAberto.Value > 0 then Cor2 := $0023AAEF // Laranja
  else Cor2 := clGreen;
  //
  //Fon := False;
  if Cor1 = clBlack then Fon := False else Fon := True;
  //
  if (Column.FieldName = 'Valor')
  or (Column.FieldName = 'Desco')
  or (Column.FieldName = 'Bruto')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor2;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Ordem')
  //or (Column.FieldName = 'Comp')
  //or (Column.FieldName = 'Banco')
  //or (Column.FieldName = 'Agencia')
  //or (Column.FieldName = 'Cheque')
  or (Column.FieldName = 'AberCH')
  or (Column.FieldName = 'Risco')
  or (Column.FieldName = 'Aberto')
  or (Column.FieldName = 'Disponiv')
  or (Column.FieldName = 'DIAS')
  or (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA')
  or (Column.FieldName = 'QtdeCH')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Conta')
  or (Column.FieldName = 'DVence')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'CPF_TXT')
  or (Column.FieldName = 'Emitente')
  or (Column.FieldName = 'IE')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'NOMEDUPLICADO')
  then begin
    with GradeD_L.Canvas do
    begin
      if DmLot2.QrLDUsDuplicado.Value = 0 then Font.Color := clGreen
      else Font.Color := clRed;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'TEXTO_SPC')
  then begin
    with GradeD_L.Canvas do
    begin
      case DmLot2.QrLDUsStatusSPC.Value of
        -1: Font.Color := clGray;
         0: Font.Color := clGreen;
         1: Font.Color := clBlue;
         2: Font.Color := clRed;
        else Font.Color := clFuchsia;
      end;
      Font.Style := [fsBold];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmLot2Inn.GradeD_LTitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := DmLot2.FORDA_LDUs;
  DmLot2.FORDA_LDUs := Column.FieldName;
  if DmLot2.FORDA_LDUs = 'CPF_TXT' then DmLot2.FORDA_LDUs := 'CPF';
  if DmLot2.FORDA_LDUs = 'NOMEDUPLICADO' then DmLot2.FORDA_LDUs := 'Duplicado';
  if DmLot2.FORDA_LDUs = 'DIAS' then DmLot2.FORDA_LDUs := 'DVence';
  if Antigo = DmLot2.FORDA_LDUs then
  DmLot2.FORDB_LDUs := MLAGeral.InverteOrdemAsc(DmLot2.FORDB_LDUs);
  DmLot2.ReopenImportLote(DmLot2.QrLCHsOrdem.Value, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.GradeLDU1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.FieldName = 'Emitente')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsEmitente.Value <> DmLot2.QrLDUsNOME_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Rua')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsRua.Value <> DmLot2.QrLDUsRUA_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'NUMERO_TXT')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsNUMERO_TXT.Value <> DmLot2.QrLDUsNUMERO_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Compl')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsCompl.Value <> DmLot2.QrLDUsCOMPL_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Birro')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsBirro.Value <> DmLot2.QrLDUsBAIRRO_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Cidade')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsCidade.Value <> DmLot2.QrLDUsCIDADE_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'CEP_TXT')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsCEP_TXT.Value <> DmLot2.QrLDUsCEP_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Trim(Column.Field.DisplayText));
    end;
  end else if (Column.FieldName = 'UF')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsUF.Value <> DmLot2.QrLDUsUF_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Trim(Column.Field.DisplayText));
    end;
  end else if (Column.FieldName = 'TEL1_TXT')
  then begin
    with GradeLDU1.Canvas do
    begin
      if DmLot2.QrLDUsTEL1_TXT.Value <> DmLot2.QrLDUsTEL1_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end
end;

procedure TFmLot2Inn.GradeLDU2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (Column.FieldName = 'NOME_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsEmitente.Value <> DmLot2.QrLDUsNOME_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'RUA_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsRua.Value <> DmLot2.QrLDUsRUA_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'NUMERO_2_TXT')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsNUMERO_TXT.Value <> DmLot2.QrLDUsNUMERO_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'COMPL_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsCompl.Value <> DmLot2.QrLDUsCOMPL_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'BAIRRO_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsBirro.Value <> DmLot2.QrLDUsBAIRRO_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'CIDADE_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsCidade.Value <> DmLot2.QrLDUsCIDADE_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'CEP_2_TXT')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsCEP_TXT.Value <> DmLot2.QrLDUsCEP_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'UF_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsUF.Value <> DmLot2.QrLDUsUF_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'TEL1_2_TXT')
  then begin
    with GradeLDU2.Canvas do
    begin
      if DmLot2.QrLDUsTEL1_TXT.Value <> DmLot2.QrLDUsTEL1_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end
end;

procedure TFmLot2Inn.HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
begin
  case Meu_Sit of
    0:
    begin
      BtEntidades.Enabled  := TipoDoc = 0;
      BtEntidades2.Enabled := TipoDoc = 1;
      BtCuidado.Enabled    := False;
      BtEntiTroca1.Enabled := False;
    end;
    1:
    begin
      BtEntidades.Enabled  := False;
      BtEntidades2.Enabled := False;
      BtCuidado.Enabled    := True;
      BtEntiTroca1.Enabled := True;
    end;
    2:
    begin
      BtEntidades.Enabled  := False;
      BtEntidades2.Enabled := False;
      BtCuidado.Enabled    := False;
      BtEntiTroca1.Enabled := False;
    end;
    else
    begin
      BtEntidades.Enabled  := False;
      BtCuidado.Enabled    := True;
      BtEntiTroca1.Enabled := False;
    end;
  end;
end;

procedure TFmLot2Inn.IgnoraImportLote(Tipo, Ordem: Integer);
var
  CH, DU: Integer;
begin
  if Geral.MensagemBox('Deseja realmente ignorar as diferen�as da' +
    ' seq. n� '+ IntToStr(Ordem)+'?', 'Pergunta', MB_YESNOCANCEL+
    MB_ICONQUESTION) = ID_YES
  then begin
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('UPDATE importlote SET Sit=2 WHERE Tipo=:P0 AND Ordem=:P1');
    Dmod.QrUpdL.Params[00].AsInteger := Tipo;
    Dmod.QrUpdL.Params[01].AsInteger := Ordem;
    Dmod.QrUpdL.ExecSQL;
    //
    if Tipo = 0 then
    begin
      CH := Ordem;
      DU := DmLot2.QrLDUsOrdem.Value;
    end else begin
      DU := Ordem;
      CH := DmLot2.QrLCHsOrdem.Value;
    end;
    DmLot2.ReopenImportLote(CH, DU);
  end;
end;

procedure TFmLot2Inn.Imprimeconsultaselecionada1Click(Sender: TObject);
begin
  MyObjects.frxMostra(FmLot2Cab.frxSPC_Result, 'Resultado de consulta ao SPC / SERASA');
end;

procedure TFmLot2Inn.LocalizaProximoChNaoVerde();
begin
  while not DmLot2.QrLCHs.Eof do
  begin
    if DmLot2.QrLCHsMEU_SIT.Value <> 2 then Exit;
    DmLot2.QrLCHs.Next;
  end;
end;

procedure TFmLot2Inn.LocalizaProximoDUNaoVerde();
begin
  while not DmLot2.QrLDUs.Eof do
  begin
    if DmLot2.QrLDUsMEU_SIT.Value <> 2 then Exit;
    DmLot2.QrLDUs.Next;
  end;
end;

function TFmLot2Inn.NaoPermiteCHs(): Boolean;
var
  Impede, Duplic: Integer;
begin
  if FmPrincipal.FTipoImport = il4_BCO then
  begin
    if Geral.MensagemBox('Deseja confirmar sem cadastrar os CPFs ' +
    'e/ou os nomes dos emitentes n�o informados?', 'Pergunta', MB_YESNO+
    MB_ICONQUESTION) = ID_YES then
    begin
      Result := False;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    Impede := 0;
    Duplic := 0;
    DmLot2.QrLCHs.DisableControls;
    DmLot2.QrLCHs.First;
    while not DmLot2.QrLCHs.Eof do
    begin
      if DmLot2.QrLCHsMEU_SIT.Value <> 2 then Impede := Impede + 1;
      if DmLot2.QrLCHsDuplicado.Value <> 0 then Duplic := Duplic + 1;
      DmLot2.QrLCHs.Next;
    end;
    if Impede <> 0 then
    begin
      Geral.MensagemBox('H� ' + IntToStr(Impede) + ' itens impedindo ' +
      'a inclus�o destes cheques!', 'Aviso', MB_OK+MB_ICONWARNING);
      Result := True;
    end else if Duplic > 0 then begin
      if Geral.MensagemBox('H� '+IntToStr(Duplic)+' Cheques deste ' +
      'lote que j� est�o digitados! Deseja continuar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Result := True else
      Result := False;
    end else Result := False;
    DmLot2.QrLCHs.EnableControls;
    Screen.Cursor := crDefault;
  except
    DmLot2.QrLCHs.EnableControls;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TFmLot2Inn.NaoPermiteDUs(): Boolean;
var
  Impede, Duplic: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Impede := 0;
    Duplic := 0;
    DmLot2.QrLDUs.DisableControls;
    DmLot2.QrLDUs.First;
    while not DmLot2.QrLDUs.Eof do
    begin
      if DmLot2.QrLDUsMEU_SIT.Value <> 2 then Impede := Impede + 1;
      if DmLot2.QrLDUsDuplicado.Value <> 0 then Duplic := Duplic + 1;
      DmLot2.QrLDUs.Next;
    end;
    if Impede <> 0 then
    begin
      Geral.MensagemBox('H� '+IntToStr(Impede)+' itens impedindo '+
      'a inclus�o destas duplicatas!', 'Aviso', MB_OK+MB_ICONWARNING);
      Result := True;
    end else if Duplic > 0 then begin
      if Geral.MensagemBox('H� '+IntToStr(Duplic)+' duplicatas deste '+
      'lote que j� est�o digitadas! Deseja continuar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Result := True else
      Result := False;
    end else Result := False;
    DmLot2.QrLDUs.EnableControls;
    Screen.Cursor := crDefault;
  except
    DmLot2.QrLDUs.EnableControls;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLot2Inn.Sacadoselecionado1Click(Sender: TObject);
begin
  FmPrincipal.AtualizaSacado(DmLot2.QrLDUsCPF.Value, DmLot2.QrLDUsEmitente.Value, DmLot2.QrLDUsRua.Value,
    DmLot2.QrLDUsNumero.Value, DmLot2.QrLDUsCompl.Value, DmLot2.QrLDUsBirro.Value,
    DmLot2.QrLDUsCidade.Value, DmLot2.QrLDUsUF.Value, IntToStr(DmLot2.QrLDUsCEP.Value),
    MlAGeral.SoNumeroESinal_TT(DmLot2.QrLDUsTel1.Value), DmLot2.QrLDUsIE.Value, '', -1);
  //
  FmPrincipal.AtualizaAcumulado(PB1);
  DmLot2.ReopenImportLote(DmLot2.QrLCHsOrdem.Value, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.Todosemitentesnocadastrados1Click(Sender: TObject);
var
  Banco, Agencia, Conta, CPF, Nome: String;
  Ordem: Integer;
begin
  Ordem := DmLot2.QrLCHsOrdem.Value;
  DmLot2.QrLCHs.First;
  while not DmLot2.QrLCHs.Eof do
  begin
    if DmLot2.QrLCHsMEU_SIT.Value = 0 then
    begin
      Banco    := MLAGeral.FFD(DmLot2.QrLCHsBanco.Value, 3, siPositivo);
      Agencia  := MLAGeral.FFD(DmLot2.QrLCHsAgencia.Value, 4, siPositivo);
      Conta    := DmLot2.QrLCHsConta.Value;
      CPF      := Geral.SoNumero_TT(DmLot2.QrLCHsCPF.Value);
      Nome     := DmLot2.QrLCHsEmitente.Value;
      DmLot2.AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome, -1);
    end;
    DmLot2.QrLCHs.Next;
  end;
  FmPrincipal.AtualizaAcumulado(PB1);
  DmLot2.ReopenImportLote(Ordem, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.Todossacadosnocadastrados1Click(Sender: TObject);
var
  Ordem: Integer;
begin
  Ordem := DmLot2.QrLDUsOrdem.Value;
  DmLot2.QrLDUs.First;
  while not DmLot2.QrLDUs.Eof do
  begin
    if DmLot2.QrLDUsMEU_SIT.Value = 0 then
    begin
      FmPrincipal.AtualizaSacado(DmLot2.QrLDUsCPF.Value, DmLot2.QrLDUsEmitente.Value, DmLot2.QrLDUsRua.Value,
        DmLot2.QrLDUsNumero.Value, DmLot2.QrLDUsCompl.Value, DmLot2.QrLDUsBirro.Value,
        DmLot2.QrLDUsCidade.Value, DmLot2.QrLDUsUF.Value, IntToStr(DmLot2.QrLDUsCEP.Value),
        MlAGeral.SoNumeroESinal_TT(DmLot2.QrLDUsTel1.Value), DmLot2.QrLDUsIE.Value, '', -1);
    end;
    DmLot2.QrLDUs.Next;
  end;
  FmPrincipal.AtualizaAcumulado(PB1);
  DmLot2.ReopenImportLote(DmLot2.QrLDUsOrdem.Value, Ordem);
end;

procedure TFmLot2Inn.TPChequeChange(Sender: TObject);
begin
  DmLot2.ReopenImportLote(DmLot2.QrLCHsOrdem.Value, DmLot2.QrLDUsOrdem.Value);
end;

procedure TFmLot2Inn.VerificaCPFCNPJatual1Click(Sender: TObject);
begin
  DmLot2.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istAtual);
end;

procedure TFmLot2Inn.VerificaNomeImport();
begin
  DmLot.QrLocCliImp.Close;
  DmLot.QrLocCliImp.Params[0].AsInteger := EdCodCli.ValueVariant;
  UMyMod.AbreQuery(DmLot.QrLocCliImp, Dmod.MyDB);
  if DmLot.QrLocCliImpNOMECLI.Value <> EdNomCli.Text then
  begin
    EdNomCli.Font.Color := clRed;
    EdCodCli.Font.Color := clRed;
    EdNomCli.Color := clYellow;
    EdCodCli.Color := clYellow;
  end else begin
    EdNomCli.Font.Color := clBlue;
    EdCodCli.Font.Color := clBlue;
    EdNomCli.Color := clWhite;
    EdCodCli.Color := clWhite;
  end;
end;

procedure TFmLot2Inn.VerificaSeFecha();
begin
  if (DmLot2.QrLDUs.RecordCount = 0) and
  (DmLot2.QrLDUs.RecordCount = 0) then
    Close;
end;

procedure TFmLot2Inn.VerificatodosCPFCNPJaindanoverificados1Click(
  Sender: TObject);
begin
  DmLot2.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istExtra1);
end;

procedure TFmLot2Inn.VerificatodosCPFCNPJpelovalormnimoconfigurao1Click(
  Sender: TObject);
begin
  DmLot2.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istExtra2);
end;

end.
