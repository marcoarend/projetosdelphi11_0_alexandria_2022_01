unit SociosImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, frxClass, frxDBSet,
  dmkGeral, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmSociosImp = class(TForm)
    DsEmpresas: TDataSource;
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasRazaoSocial: TWideStringField;
    QrSocios: TmySQLQuery;
    DsSocios: TDataSource;
    QrSociosCodigo: TIntegerField;
    QrEmpresa: TmySQLQuery;
    DsEmpresa: TDataSource;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaRazaoSocial: TWideStringField;
    QrEmpresaCadastro: TDateField;
    QrEmpresaENatal: TDateField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaSimples: TSmallintField;
    QrEmpresaELograd: TSmallintField;
    QrEmpresaERua: TWideStringField;
    QrEmpresaECompl: TWideStringField;
    QrEmpresaEBairro: TWideStringField;
    QrEmpresaECidade: TWideStringField;
    QrEmpresaECEP: TIntegerField;
    QrEmpresaETe1: TWideStringField;
    QrEmpresaEFax: TWideStringField;
    QrEmpresaETE1_TXT: TWideStringField;
    QrEmpresaFAX_TXT: TWideStringField;
    QrEmpresaFormaSociet: TWideStringField;
    QrEmpresaCNPJ_TXT: TWideStringField;
    QrEmpresaE_LNR: TWideStringField;
    QrEmpresaNOMEUF: TWideStringField;
    QrEmpresaNUMERO_TXT: TWideStringField;
    QrEmpresaNOMELOGRAD: TWideStringField;
    QrEmpresaECEP_TXT: TWideStringField;
    QrEmpresaAtividade: TWideStringField;
    QrSociosSexo: TWideStringField;
    QrSociosPai: TWideStringField;
    QrSociosMae: TWideStringField;
    QrSociosPNatal: TDateField;
    QrSociosNOMEPUF: TWideStringField;
    QrSociosNOMEPLOGRAD: TWideStringField;
    QrSociosNOMEECIVIL: TWideStringField;
    QrSociosITEM: TIntegerField;
    QrSociosNOMESEXO: TWideStringField;
    QrSociosCidadeNatal: TWideStringField;
    QrSociosNacionalid: TWideStringField;
    QrSociosConjugeNome: TWideStringField;
    QrSociosCPF: TWideStringField;
    QrSociosRG: TWideStringField;
    QrSociosSSP: TWideStringField;
    QrSociosDataRG: TDateField;
    QrSociosPRua: TWideStringField;
    QrSociosPCompl: TWideStringField;
    QrSociosPBairro: TWideStringField;
    QrSociosPCidade: TWideStringField;
    QrSociosPCEP: TIntegerField;
    QrSociosPTe1: TWideStringField;
    QrSociosProfissao: TWideStringField;
    QrSociosCargo: TWideStringField;
    QrSociosETE1_TXT: TWideStringField;
    QrSociosCNPJ_TXT: TWideStringField;
    QrSociosE_LNR: TWideStringField;
    QrSociosNUMERO_TXT: TWideStringField;
    QrSociosECEP_TXT: TWideStringField;
    QrSociosDATARG_TXT: TWideStringField;
    QrEmpresaENATAL_TXT: TWideStringField;
    QrSociosPNATAL_TXT: TWideStringField;
    QrRepr0: TmySQLQuery;
    DsRepr0: TDataSource;
    QrRepr1: TmySQLQuery;
    DsRepr1: TDataSource;
    QrRepr0Codigo: TIntegerField;
    QrRepr0Nome: TWideStringField;
    QrRepr0Cargo: TWideStringField;
    QrSociosNOMESOCIO: TWideStringField;
    QrRepr1NOMESOCIO: TWideStringField;
    QrRepr1Codigo: TIntegerField;
    QrRepr1Sexo: TWideStringField;
    QrRepr1Pai: TWideStringField;
    QrRepr1Mae: TWideStringField;
    QrRepr1PNatal: TDateField;
    QrRepr1CidadeNatal: TWideStringField;
    QrRepr1Nacionalid: TWideStringField;
    QrRepr1ConjugeNome: TWideStringField;
    QrRepr1CPF: TWideStringField;
    QrRepr1RG: TWideStringField;
    QrRepr1SSP: TWideStringField;
    QrRepr1DataRG: TDateField;
    QrRepr1PRua: TWideStringField;
    QrRepr1PCompl: TWideStringField;
    QrRepr1PBairro: TWideStringField;
    QrRepr1PCidade: TWideStringField;
    QrRepr1PCEP: TIntegerField;
    QrRepr1PTe1: TWideStringField;
    QrRepr1Profissao: TWideStringField;
    QrRepr1Cargo: TWideStringField;
    QrRepr1NOMEPUF: TWideStringField;
    QrRepr1NOMEPLOGRAD: TWideStringField;
    QrRepr1NOMEECIVIL: TWideStringField;
    QrEmpresaENumero: TIntegerField;
    QrRepr1PNumero: TIntegerField;
    QrSociosPNumero: TIntegerField;
    frxSocios: TfrxReport;
    frxDsEmpresa: TfrxDBDataset;
    frxDsSocios: TfrxDBDataset;
    frxDsRepr0: TfrxDBDataset;
    frxDsRepr1: TfrxDBDataset;
    QrEmpresaNIRE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PainelDados: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrEmpresasAfterScroll(DataSet: TDataSet);
    procedure QrEmpresaCalcFields(DataSet: TDataSet);
    procedure QrSociosCalcFields(DataSet: TDataSet);
    procedure frxSociosGetValue(const VarName: String; var Value: Variant);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSociosImp: TFmSociosImp;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmSociosImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UMyMod.AbreQuery(QrEmpresas, Dmod.MyDB);
end;

procedure TFmSociosImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSociosImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSociosImp.BtImprimeClick(Sender: TObject);
begin
 QrEmpresa.Close;
 QrEmpresa.Params[0].AsInteger := QrEmpresasCodigo.Value;
 UMyMod.AbreQuery(QrEmpresa, Dmod.MyDB);
 //
 QrRepr0.Close;
 QrRepr0.Params[0].AsInteger := Dmod.QrControleDono.Value;
 UMyMod.AbreQuery(QrRepr0, Dmod.MyDB);
 //
 QrRepr1.Close;
 QrRepr1.Params[0].AsInteger := QrEmpresasCodigo.Value;
 UMyMod.AbreQuery(QrRepr1, Dmod.MyDB);
 //
 MyObjects.frxMostra(frxSocios, 'S�cios');
end;

procedure TFmSociosImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSociosImp.QrEmpresasAfterScroll(DataSet: TDataSet);
begin
 QrSocios.Close;
 QrSocios.Params[0].AsInteger := QrEmpresasCodigo.Value;
 UMyMod.AbreQuery(QrSocios, Dmod.MyDB);
end;

procedure TFmSociosImp.QrEmpresaCalcFields(DataSet: TDataSet);
begin
  QrEmpresaETE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEmpresaETe1.Value);
  QrEmpresaFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEmpresaEFax.Value);
  QrEmpresaCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEmpresaCNPJ.Value);
  //
  QrEmpresaNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEmpresaERua.Value, QrEmpresaENumero.Value, False);
  QrEmpresaE_LNR.Value := QrEmpresaNOMELOGRAD.Value;
  if Trim(QrEmpresaE_LNR.Value) <> '' then QrEmpresaE_LNR.Value :=
    QrEmpresaE_LNR.Value + ' ';
  QrEmpresaE_LNR.Value := QrEmpresaE_LNR.Value + QrEmpresaERua.Value;
  if Trim(QrEmpresaERua.Value) <> '' then QrEmpresaE_LNR.Value :=
    QrEmpresaE_LNR.Value + ', ' + QrEmpresaNUMERO_TXT.Value;
  //
  QrEmpresaECEP_TXT.Value :=Geral.FormataCEP_NT(QrEmpresaECEP.Value);
  //
  if QrEmpresaENatal.Value < 2 then QrEmpresaENATAL_TXT.Value := ''
  else QrEmpresaENATAL_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrEmpresaENatal.Value);
end;

procedure TFmSociosImp.QrSociosCalcFields(DataSet: TDataSet);
begin
  QrSociosITEM.Value := QrSocios.RecNo;
  //
  if QrSociosSexo.Value = 'M' then QrSociosNOMESEXO.Value := 'MASCULINO'
  else if QrSociosSexo.Value = 'F' then QrSociosNOMESEXO.Value := 'FEMININO'
  else QrSociosNOMESEXO.Value := '';
  //
  QrSociosETE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrSociosPTe1.Value);
  QrSociosCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrSociosCPF.Value);
  QrSociosNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrSociosPRua.Value, Trunc(QrSociosPNumero.Value), True);
  QrSociosE_LNR.Value := QrSociosNOMEPLOGRAD.Value;
  if Trim(QrSociosE_LNR.Value) <> '' then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ' ';
  QrSociosE_LNR.Value := QrSociosE_LNR.Value + QrSociosPRua.Value;
  if Trim(QrSociosPRua.Value) <> '' then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ', ' + QrSociosNUMERO_TXT.Value;
  //
  QrSociosECEP_TXT.Value :=Geral.FormataCEP_NT(QrSociosPCEP.Value);
  //
  if QrSociosDataRG.Value < 2 then QrSociosDATARG_TXT.Value := ''
  else QrSociosDATARG_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrSociosDataRG.Value);
  if QrSociosPNatal.Value < 2 then QrSociosPNATAL_TXT.Value := ''
  else QrSociosPNATAL_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrSociosPNatal.Value);
end;

procedure TFmSociosImp.frxSociosGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VAR_S' then
  begin
    if QrSocios.RecordCount < 2 then Value := '' else Value := 's';
  end else if VarName = 'VAR_ES' then begin
    if QrSocios.RecordCount < 2 then Value := '' else Value := 'es';
  end else if VarName = 'VAR_S2' then begin
    if QrRepr0.RecordCount < 2 then Value := '' else Value := 's';
  end else if VarName = 'VAR_IS2' then begin
    if QrRepr0.RecordCount < 2 then Value := 'l' else Value := 'is';
  end else if VarName = 'VAR_S3' then begin
    if QrRepr1.RecordCount < 2 then Value := '' else Value := 's';
  end else if VarName = 'VAR_IS3' then begin
    if QrRepr1.RecordCount < 2 then Value := 'l' else Value := 'is';
  end

end;

end.
