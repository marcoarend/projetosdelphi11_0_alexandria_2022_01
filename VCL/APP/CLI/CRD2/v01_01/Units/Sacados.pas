unit Sacados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, Grids, DBGrids,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmSacados = class(TForm)
    PainelDados: TPanel;
    DsSacados: TDataSource;
    GrSacados: TDBGrid;
    Panel1: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    QrSacados: TmySQLQuery;
    QrSacadosCNPJ: TWideStringField;
    QrSacadosNome: TWideStringField;
    QrSacadosRua: TWideStringField;
    QrSacadosCompl: TWideStringField;
    QrSacadosBairro: TWideStringField;
    QrSacadosCidade: TWideStringField;
    QrSacadosUF: TWideStringField;
    QrSacadosCEP: TIntegerField;
    QrSacadosTel1: TWideStringField;
    QrSacadosRisco: TFloatField;
    QrSacadosCNPJ_TXT: TWideStringField;
    QrSacadosCEP_TXT: TWideStringField;
    QrSacadosTEL1_TXT: TWideStringField;
    StringGrid1: TStringGrid;
    QrSacadosIE: TWideStringField;
    QrSacadosNumero: TFloatField;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EdNome: TEdit;
    EdCNPJ: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdCEP: TEdit;
    EdUF: TEdit;
    Label4: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtReabre: TBitBtn;
    QrSacadosEmail: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure QrSacadosCalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtReabreClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenSacados(CNPJ: String);
  end;

  var
  FmSacados: TFmSacados;

implementation

uses UnMyObjects, Module, SacadosEdit, UnInternalConsts, Principal, UMySQLModule;

{$R *.DFM}

procedure TFmSacados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSacados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FmPrincipal.AplicaInfoRegEdit(FmSacados, True);
end;

procedure TFmSacados.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSacados.RGOrdem1Click(Sender: TObject);
begin
  ReopenSacados(QrSacadosCNPJ.Value);
end;

procedure TFmSacados.RGOrdem2Click(Sender: TObject);
begin
  ReopenSacados(QrSacadosCNPJ.Value);
end;

procedure TFmSacados.ReopenSacados(CNPJ: String);
var
  Ordem: String;
begin
  case RGOrdem1.ItemIndex of
    00: Ordem := 'CNPJ';
    01: Ordem := 'Nome';
    02: Ordem := 'Rua';
    03: Ordem := 'Numero';
    04: Ordem := 'Compl';
    05: Ordem := 'Bairro';
    06: Ordem := 'Cidade';
    07: Ordem := 'CEP';
    08: Ordem := 'UF';
    09: Ordem := 'Tel1';
    10: Ordem := 'Risco';
    else Ordem := '';
  end;
  //
  if Ordem <> '' then
    Ordem := 'ORDER BY ' + Ordem;
  if Ordem <> '' then
  begin
    if RGOrdem2.ItemIndex = 1 then
      Ordem := Ordem + ' DESC';
  end;
  //
  QrSacados.Close;
  QrSacados.SQL.Clear;
  QrSacados.SQL.Add('SELECT sac.Numero+0.000 Numero, sac.*');
  QrSacados.SQL.Add('FROM sacados sac');
  if Length(Ordem) > 0 then
    QrSacados.SQL.Add(Ordem);
  UMyMod.AbreQuery(QrSacados, Dmod.MyDB);
  //
  if CNPJ <> '' then QrSacados.Locate('CNPJ', CNPJ, []);
end;

procedure TFmSacados.QrSacadosCalcFields(DataSet: TDataSet);
begin
  QrSacadosCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrSacadosCNPJ.Value);
  QrSacadosCEP_TXT.Value :=Geral.FormataCEP_NT(QrSacadosCEP.Value);
  QrSacadosTEL1_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrSacadosTel1.Value);
end;

procedure TFmSacados.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FmPrincipal.SalvaInfoRegEdit(FmSacados, 0, StringGrid1);
  ReopenSacados('');
end;

procedure TFmSacados.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a exclus�o do CNPJ "'+
  QrSacadosCNPJ.Value+'"?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)=
  ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM sacados WHERE CNPJ=:P0');
    Dmod.QrUpd.Params[0].AsString := QrSacadosCNPJ.Value;
    Dmod.QrUpd.ExecSQl;
    //
    QrSacados.Next;
    ReopenSacados(QrSacadosCNPJ.Value);
  end;
end;

procedure TFmSacados.BtIncluiClick(Sender: TObject);
begin
  Application.CreateForm(TFmSacadosEdit, FmSacadosEdit);
  FmSacadosEdit.ImgTipo.SQLType := stIns;
  FmSacadosEdit.ShowModal;
  FmSacadosEdit.Destroy;
end;

procedure TFmSacados.BtReabreClick(Sender: TObject);
begin
  QrSacados.Close;
  QrSacados.SQL.Clear;
  QrSacados.SQL.Add('SELECT sac.Numero+0.000 Numero, sac.*');
  QrSacados.SQL.Add('FROM sacados sac');
  QrSacados.SQL.Add('WHERE Nome <> ""');
  if Trim(EdNome.Text) <> '' then
    QrSacados.SQL.Add('AND Nome LIKE "%' + EdNome.Text + '%"');
  if Trim(EdCNPJ.Text) <> '' then
    QrSacados.SQL.Add('AND CNPJ LIKE "%' + Geral.SoNumero_TT(EdCNPJ.Text) + '%"');
  if Trim(EdCEP.Text) <> '' then
    QrSacados.SQL.Add('AND CEP LIKE "%' + Geral.SoNumero_TT(EdCEP.Text) + '%"');
  if Trim(EdUF.Text) <> '' then
    QrSacados.SQL.Add('AND UF = "' + EdUF.Text + '"');
  UMyMod.AbreQuery(QrSacados, Dmod.MyDB);
end;

procedure TFmSacados.BtAlteraClick(Sender: TObject);
var
  i: Integer;
begin
  Application.CreateForm(TFmSacadosEdit, FmSacadosEdit);
  with FmSacadosEdit do
  begin
    ImgTipo.SQLType := stUpd;
    FOrigem         := 'FmSacados';
    EdCNPJ.Text     := Geral.FormataCNPJ_TT(QrSacadosCNPJ.Value);
    EdSacado.Text   := QrSacadosNome.Value;
    EdRua.Text      := QrSacadosRua.Value;
    EdNumero.Text   := IntToStr(Trunc(QrSacadosNumero.Value));
    EdCompl.Text    := QrSacadosCompl.Value;
    EdBairro.Text   := QrSacadosBairro.Value;
    EdCidade.Text   := QrSacadosCidade.Value;
    EdCEP.Text      := Geral.FormataCEP_NT(QrSacadosCEP.Value);
    EdTel1.Text     := Geral.FormataTelefone_TT(QrSacadosTel1.Value);
    EdEmail.Text    := QrSacadosEmail.Value;
    EdRisco.Text    := Geral.FFT(QrSacadosRisco.Value, 2, siPositivo);
    //
    for i := 0 to CBUF.Items.Count-1 do
      if CBUF.Items[i] = QrSacadosUF.Value then CBUF.ItemIndex := i;
  end;
  FmSacadosEdit.ShowModal;
  FmSacadosEdit.Destroy;
end;

procedure TFmSacados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmPrincipal.SalvaInfoRegEdit(FmSacados, 1, nil);
end;

end.

