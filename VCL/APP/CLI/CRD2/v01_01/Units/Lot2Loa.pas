unit Lot2Loa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmLot2Loa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InsereItensImportLote(Pagina: TPageControl;
              GradeC, GradeD: TStringGrid; TPCheque: TdmkEditDateTimePicker);
  end;

  var
  FmLot2Loa: TFmLot2Loa;

implementation

uses UnMyObjects, Module, Principal, ModuleLot, Lot2Cab, ModuleLot2,
  MyListas, UMySQLModule;

{$R *.DFM}

procedure TFmLot2Loa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2Loa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmLot2Loa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Loa.InsereItensImportLote(Pagina: TPageControl;
GradeC, GradeD: TStringGrid; TPCheque: TdmkEditDateTimePicker);
var
  I: Integer;
  CPF, Duplicata, DEmiss, DCompra, DVence, Deposit, BAC: String;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  DmLot2.QrLCHs.Close;
  Application.ProcessMessages;
  //
  FmPrincipal.FGradeFocused := 0;
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO importlote SET Sit=0, ');
    Dmod.QrUpdL.SQL.Add('Tipo        =:P00,');
    Dmod.QrUpdL.SQL.Add('Ordem       =:P01,');
    Dmod.QrUpdL.SQL.Add('DEmiss      =:P02,');
    Dmod.QrUpdL.SQL.Add('DCompra     =:P03,');
    Dmod.QrUpdL.SQL.Add('DVence      =:P04,');
    Dmod.QrUpdL.SQL.Add('Deposito    =:P05,');
    Dmod.QrUpdL.SQL.Add('Comp        =:P06,');
    Dmod.QrUpdL.SQL.Add('Banco       =:P07,');
    Dmod.QrUpdL.SQL.Add('Agencia     =:P08,');
    Dmod.QrUpdL.SQL.Add('Conta       =:P09,');
    Dmod.QrUpdL.SQL.Add('CPF         =:P10,');
    Dmod.QrUpdL.SQL.Add('Emitente    =:P11,');
    Dmod.QrUpdL.SQL.Add('Cheque      =:P12,');
    Dmod.QrUpdL.SQL.Add('Duplicata   =:P13,');
    Dmod.QrUpdL.SQL.Add('Valor       =:P14,');
    Dmod.QrUpdL.SQL.Add('Rua         =:P15,');
    Dmod.QrUpdL.SQL.Add('Numero      =:P16,');
    Dmod.QrUpdL.SQL.Add('Compl       =:P17,');
    Dmod.QrUpdL.SQL.Add('Birro       =:P18,');
    Dmod.QrUpdL.SQL.Add('Cidade      =:P19,');
    Dmod.QrUpdL.SQL.Add('UF          =:P20,');
    Dmod.QrUpdL.SQL.Add('CEP         =:P21,');
    Dmod.QrUpdL.SQL.Add('Tel1        =:P22,');
    Dmod.QrUpdL.SQL.Add('Aberto      =:P23,');
    Dmod.QrUpdL.SQL.Add('OcorrA      =:P24,');
    Dmod.QrUpdL.SQL.Add('OcorrT      =:P25,');
    Dmod.QrUpdL.SQL.Add('QtdeCH      =:P26,');
    Dmod.QrUpdL.SQL.Add('AberCH      =:P27,');
    Dmod.QrUpdL.SQL.Add('IE          =:P28,');
    Dmod.QrUpdL.SQL.Add('Desco       =:P29,');
    Dmod.QrUpdL.SQL.Add('Bruto       =:P30,');
    Dmod.QrUpdL.SQL.Add('Duplicado   =:P31,');
    Dmod.QrUpdL.SQL.Add('Praca       =:P32,');
    Dmod.QrUpdL.SQL.Add('BAC         =:P33,');
    Dmod.QrUpdL.SQL.Add('CPF_2       =:P34,');
    Dmod.QrUpdL.SQL.Add('Nome_2      =:P35,');
    Dmod.QrUpdL.SQL.Add('RISCOEM     =:P36,');
    Dmod.QrUpdL.SQL.Add('RISCOSA     =:P37,');
    Dmod.QrUpdL.SQL.Add('Rua_2       =:P38,');
    Dmod.QrUpdL.SQL.Add('Numero_2    =:P39,');
    Dmod.QrUpdL.SQL.Add('Compl_2     =:P40,');
    Dmod.QrUpdL.SQL.Add('IE_2        =:P41,');
    Dmod.QrUpdL.SQL.Add('Bairro_2    =:P42,');
    Dmod.QrUpdL.SQL.Add('Cidade_2    =:P43,');
    Dmod.QrUpdL.SQL.Add('UF_2        =:P44,');
    Dmod.QrUpdL.SQL.Add('CEP_2       =:P45,');
    Dmod.QrUpdL.SQL.Add('TEL1_2      =:P46,');
    Dmod.QrUpdL.SQL.Add('StatusSPC   =:P47 ');
    //
    PB1.Position := 0;
    PB1.Max := GradeC.RowCount-1;
    PB1.Refresh;
    PB1.Update;
    Application.ProcessMessages;
    if (GradeC.RowCount > 2) or (Geral.DMV(GradeC.Cells[06, 1]) > 0) then
    begin
      FmPrincipal.FGradeFocused := 1;
      for i := 1 to GradeC.RowCount -1 do
      begin
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        CPF := Geral.SoNumero_TT(GradeC.Cells[09, i]);
        //
        DmLot.QrSAlin.Close;
        DmLot.QrSAlin.Params[0].AsString := CPF;
        UMyMod.AbreQuery(DmLot.QrSAlin, Dmod.MyDB);
        //
        DEmiss  := '0000-00-00';
        DCompra := FormatDateTime(VAR_FORMATDATE, TPCheque.Date);
        DVence  := FormatDateTime(VAR_FORMATDATE, Geral.ValidaDataSimples(GradeC.Cells[07, i], True));
        Deposit := '0000-00-00';
        //
{
        DmLot.QrLocCH.Close;
        DmLot.QrLocCH.Params[00].AsInteger := Geral.IMV(GradeC.Cells[02, i]);
        DmLot.QrLocCH.Params[01].AsInteger := Geral.IMV(GradeC.Cells[03, i]);
        DmLot.QrLocCH.Params[02].AsString  := GradeC.Cells[04, i];
        DmLot.QrLocCH.Params[03].AsInteger := Geral.IMV(GradeC.Cells[05, i]);
        DmLot. Open;
}
        UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrLocCH, Dmod.MyDB, [
        'SELECT COUNT(*) Cheques ',
        'FROM ' + CO_TabLctA,
        'WHERE FatID=' + Geral.FF0(VAR_FATID_0301),
        'AND Banco=' + Geral.FF0(Geral.IMV(GradeC.Cells[02, i])),
        'AND Agencia=' + Geral.FF0(Geral.IMV(GradeC.Cells[03, i])),
        'AND ContaCorrente="' + GradeC.Cells[04, i] + '" ',
        'AND Documento=' + Geral.FF0(Geral.IMV(GradeC.Cells[05, i])),
        '']);
        //
        DmLot2.QrLocEmiCPF.Close;
        DmLot2.QrLocEmiCPF.Params[0].AsString := CPF;
        UMyMod.AbreQuery(DmLot2.QrLocEmiCPF, Dmod.MyDB);
        //
        DmLot2.QrLocSacados.Close;
        DmLot2.QrLocSacados.Params[0].AsString := CPF;
        UMyMod.AbreQuery(DmLot2.QrLocSacados, Dmod.MyDB);
        //
        BAC := FormatFloat('000', Geral.IMV(GradeC.Cells[02, i])) +
               FormatFloat('0000', Geral.IMV(GradeC.Cells[03, i])) +
               GradeC.Cells[04, i];
        //
        Dmod.QrUpdL.Params[00].AsInteger := 0;
        Dmod.QrUpdL.Params[01].AsInteger := i;
        Dmod.QrUpdL.Params[02].AsString  := DEmiss;
        Dmod.QrUpdL.Params[03].AsString  := DCompra;
        Dmod.QrUpdL.Params[04].AsString  := DVence;
        Dmod.QrUpdL.Params[05].AsString  := Deposit;
        Dmod.QrUpdL.Params[06].AsInteger := FmPrincipal.VerificaDiasCompensacao(
          Geral.IMV(GradeC.Cells[01, i]), FmLot2Cab.QrLotCBECli.Value,
          Geral.DMV(GradeC.Cells[06, i]), TPCheque.Date,
          Geral.ValidaDataSimples(GradeC.Cells[07, i], True),
          FmLot2Cab.QrLotSCBCli.Value);
        Dmod.QrUpdL.Params[07].AsInteger := Geral.IMV(GradeC.Cells[02, i]);
        Dmod.QrUpdL.Params[08].AsInteger := Geral.IMV(GradeC.Cells[03, i]);
        Dmod.QrUpdL.Params[09].AsString  := GradeC.Cells[04, i];
        Dmod.QrUpdL.Params[10].AsString  := CPF;
        Dmod.QrUpdL.Params[11].AsString  := GradeC.Cells[10, i];
        Dmod.QrUpdL.Params[12].AsInteger := Geral.IMV(GradeC.Cells[05, i]);
        Dmod.QrUpdL.Params[13].AsString  := '';
        Dmod.QrUpdL.Params[14].AsFloat   := Geral.DMV(GradeC.Cells[06, i]);
        Dmod.QrUpdL.Params[15].AsString  := '';
        Dmod.QrUpdL.Params[16].AsInteger := 0;
        Dmod.QrUpdL.Params[17].AsString  := '';
        Dmod.QrUpdL.Params[18].AsString  := '';
        Dmod.QrUpdL.Params[19].AsString  := '';
        Dmod.QrUpdL.Params[20].AsString  := '';
        Dmod.QrUpdL.Params[21].AsString  := '';
        Dmod.QrUpdL.Params[22].AsString  := '';
        Dmod.QrUpdL.Params[23].AsFloat   := DmLot.RiscoSacado(CPF);
        Dmod.QrUpdL.Params[24].AsFloat   := DmLot.QrSAlinAberto.Value;
        Dmod.QrUpdL.Params[25].AsFloat   := DmLot.QrSAlinValor.Value;
        Dmod.QrUpdL.Params[26].AsInteger := DmLot.QrSAlinCheques.Value;
        Dmod.QrUpdL.Params[27].AsInteger := 0;//QrSCHOpenCheques.Value;
        Dmod.QrUpdL.Params[28].AsString  := '';
        Dmod.QrUpdL.Params[29].AsFloat   := 0;
        Dmod.QrUpdL.Params[30].AsFloat   := 0;
        Dmod.QrUpdL.Params[31].AsInteger := DmLot.QrLocCHCheques.Value;
        Dmod.QrUpdL.Params[32].AsInteger := Geral.IMV(GradeC.Cells[01, i]);
        Dmod.QrUpdL.Params[33].AsString  := BAC;
        Dmod.QrUpdL.Params[34].AsString  := DmLot2.QrLocEmiCPFCPF.Value;
        Dmod.QrUpdL.Params[35].AsString  := DmLot2.QrLocEmiCPFNome.Value;
        Dmod.QrUpdL.Params[36].AsFloat   := DmLot2.QrLocEmiCPFLimite.Value;
        Dmod.QrUpdL.Params[37].AsFloat   := DmLot2.QrLocSacadosRisco.Value;
        Dmod.QrUpdL.Params[38].AsString  := '';
        Dmod.QrUpdL.Params[39].AsInteger := 0;
        Dmod.QrUpdL.Params[40].AsString  := '';
        Dmod.QrUpdL.Params[41].AsString  := '';
        Dmod.QrUpdL.Params[42].AsString  := '';
        Dmod.QrUpdL.Params[43].AsString  := '';
        Dmod.QrUpdL.Params[44].AsString  := '';
        Dmod.QrUpdL.Params[45].AsInteger := 0;
        Dmod.QrUpdL.Params[46].AsString  := '';
        Dmod.QrUpdL.Params[47].AsInteger := -1;
        Dmod.QrUpdL.ExecSQL;
      end;
    end;
    PB1.Position := 0;
    PB1.Max := GradeD.RowCount-1;
    PB1.Refresh;
    PB1.Update;
    Application.ProcessMessages;
    if (GradeD.RowCount > 2) or (Geral.DMV(GradeD.Cells[03, 1]) > 0) then
    begin
      FmPrincipal.FGradeFocused := FmPrincipal.FGradeFocused + 2;
      for i := 1 to GradeD.RowCount -1 do
      begin
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        CPF := Geral.SoNumero_TT(GradeD.Cells[07, i]);
{
        DmLot.QrSDUOpen.Close;
        DmLot.QrSDUOpen.Params[0].AsString  := CPF;
        DmLot.QrSDUOpen. Open;
}
        DmLot.ReopenSDUOpen(CPF);
        //
        DEmiss  := FormatDateTime(VAR_FORMATDATE, Geral.ValidaDataSimples(GradeD.Cells[01, i], True));
        DCompra := FormatDateTime(VAR_FORMATDATE, TPCheque.Date);
        DVence  := FormatDateTime(VAR_FORMATDATE, Geral.ValidaDataSimples(GradeD.Cells[06, i], True));
        Deposit := '0000-00-00';
        //
{
        DmLot.QrLocDU.Close;
        DmLot.QrLocDU.Params[00].AsString  := CPF;
        DmLot.QrLocDU.Params[01].AsString  := GradeD.Cells[02, i];
        DmLot.QrLocDU. Open;
}
        Duplicata := GradeD.Cells[02, i];
        DmLot.ReopenLocDU(CPF, Duplicata);
        //
        Dmod.QrUpdL.Params[00].AsInteger := 1;
        Dmod.QrUpdL.Params[01].AsInteger := i;
        Dmod.QrUpdL.Params[02].AsString  := DEmiss;
        Dmod.QrUpdL.Params[03].AsString  := DCompra;
        Dmod.QrUpdL.Params[04].AsString  := DVence;
        Dmod.QrUpdL.Params[05].AsString  := Deposit;
        Dmod.QrUpdL.Params[06].AsInteger := 0;//Geral.IMV(GradeD.Cells[01, i]);
        Dmod.QrUpdL.Params[07].AsInteger := 0;//Geral.IMV(GradeD.Cells[02, i]);
        Dmod.QrUpdL.Params[08].AsInteger := 0;//Geral.IMV(GradeD.Cells[03, i]);
        Dmod.QrUpdL.Params[09].AsString  := '';//GradeD.Cells[04, i];
        Dmod.QrUpdL.Params[10].AsString  := CPF;
        Dmod.QrUpdL.Params[11].AsString  := GradeD.Cells[08, i];
        Dmod.QrUpdL.Params[12].AsInteger := 0;//Geral.IMV( GradeD.Cells[05, i]);
        Dmod.QrUpdL.Params[13].AsString  := GradeD.Cells[02, i];
        Dmod.QrUpdL.Params[14].AsFloat   := Geral.DMV(GradeD.Cells[03, i]);
        Dmod.QrUpdL.Params[15].AsString  := GradeD.Cells[09, i];
        Dmod.QrUpdL.Params[16].AsInteger := Geral.IMV(GradeD.Cells[10, i]);
        Dmod.QrUpdL.Params[17].AsString  := GradeD.Cells[11, i]; //Compl
        Dmod.QrUpdL.Params[18].AsString  := GradeD.Cells[12, i];
        Dmod.QrUpdL.Params[19].AsString  := GradeD.Cells[13, i];
        Dmod.QrUpdL.Params[20].AsString  := GradeD.Cells[15, i];
        Dmod.QrUpdL.Params[21].AsString  := Geral.SoNumero_TT(GradeD.Cells[14, i]);
        Dmod.QrUpdL.Params[22].AsString  := GradeD.Cells[16, i]; //Telefone
        Dmod.QrUpdL.Params[23].AsFloat   := DmLot.QrSDUOpenValor.Value;
        Dmod.QrUpdL.Params[24].AsFloat   := 0;
        Dmod.QrUpdL.Params[25].AsFloat   := 0;
        Dmod.QrUpdL.Params[26].AsInteger := 0;
        Dmod.QrUpdL.Params[27].AsInteger := 0;
        Dmod.QrUpdL.Params[28].AsString  := GradeD.Cells[17, i];
        Dmod.QrUpdL.Params[29].AsFloat   := Geral.DMV(GradeD.Cells[04, i]);
        Dmod.QrUpdL.Params[30].AsFloat   := Geral.DMV(GradeD.Cells[05, i]);
        Dmod.QrUpdL.Params[31].AsInteger := DmLot.QrLocDUDuplicatas.Value;
        Dmod.QrUpdL.Params[32].AsInteger := 0;
        Dmod.QrUpdL.Params[33].AsString  := '';

        Dmod.QrUpdL.Params[34].AsString  := DmLot2.QrLocSacadosCNPJ.Value;
        Dmod.QrUpdL.Params[35].AsString  := DmLot2.QrLocSacadosNome.Value;
        Dmod.QrUpdL.Params[36].AsFloat   := DmLot2.QrLocEmiCPFLimite.Value;
        Dmod.QrUpdL.Params[37].AsFloat   := DmLot2.QrLocSacadosRisco.Value;

        Dmod.QrUpdL.Params[38].AsString  := DmLot2.QrLocSacadosRua.Value;
        Dmod.QrUpdL.Params[39].AsInteger := DmLot2.QrLocSacadosNumero.Value;
        Dmod.QrUpdL.Params[40].AsString  := DmLot2.QrLocSacadosCompl.Value;
        Dmod.QrUpdL.Params[41].AsString  := DmLot2.QrLocSacadosIE.Value;
        Dmod.QrUpdL.Params[42].AsString  := DmLot2.QrLocSacadosBairro.Value;
        Dmod.QrUpdL.Params[43].AsString  := DmLot2.QrLocSacadosCidade.Value;
        Dmod.QrUpdL.Params[44].AsString  := DmLot2.QrLocSacadosUF.Value;
        Dmod.QrUpdL.Params[45].AsInteger := DmLot2.QrLocSacadosCEP.Value;
        Dmod.QrUpdL.Params[46].AsString  := DmLot2.QrLocSacadosTel1.Value;
        Dmod.QrUpdL.Params[47].AsInteger := -1;
        Dmod.QrUpdL.ExecSQL;
      end;
    end;
    FmPrincipal.AtualizaAcumulado(PB1);
    DmLot2.ReopenImportLote(0, 0);
    if FmPrincipal.FGradeFocused = 2 then
    begin
      if Pagina <> nil then
        Pagina.ActivePageIndex := 1
      else
        Pagina.ActivePageIndex := 0;
    end;
    Screen.Cursor := MyCursor;
  except
    raise;
    Screen.Cursor := crDefault;
  end;
end;

end.
