object FmEntiJur1Ocor: TFmEntiJur1Ocor
  Left = 482
  Top = 200
  Caption = 'CLI-JURID-003 :: Ocorr'#234'ncias Banc'#225'rias (CNAB)'
  ClientHeight = 308
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 568
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 520
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 472
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 377
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias (CNAB)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 377
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias (CNAB)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 377
        Height = 32
        Caption = 'Ocorr'#234'ncias Banc'#225'rias (CNAB)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 194
    Width = 568
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 564
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 238
    Width = 568
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 422
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 420
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn2: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn2Click
      end
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 568
    Height = 146
    Align = alClient
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 564
      Height = 129
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 28
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Ocorr'#234'ncia:'
      end
      object Label4: TLabel
        Left = 464
        Top = 8
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object SpeedButton5: TSpeedButton
        Left = 440
        Top = 24
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdOcor: TdmkEditCB
        Left = 28
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdOcorChange
        DBLookupComboBox = CBOcor
        IgnoraDBLookupComboBox = False
      end
      object CBOcor: TdmkDBLookupComboBox
        Left = 88
        Top = 24
        Width = 348
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOcorBank
        TabOrder = 1
        OnClick = CBOcorClick
        dmkEditCB = EdOcor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGFormaCNAB: TRadioGroup
        Left = 28
        Top = 49
        Width = 521
        Height = 72
        Caption = ' Forma de cobran'#231'a em arquivos remessa / retorno (CNAB240): '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o cobrar'
          'Cobrar o mesmo valor do banco'
          'Cobrar o valor base deste cadastro'
          'Cobrar o valor base do cadastro da ocorr'#234'ncia')
        TabOrder = 3
      end
      object EdBase: TdmkEdit
        Left = 464
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Base'
      'FROM ocorbank'
      'ORDER BY Nome'
      '')
    Left = 8
    Top = 8
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 36
    Top = 8
  end
end
