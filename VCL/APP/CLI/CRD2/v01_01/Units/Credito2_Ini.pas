unit Credito2_Ini;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmCredito2_Ini = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    GroupBox1: TGroupBox;
    PB1: TProgressBar;
    QrOcorreu: TmySQLQuery;
    QrOcorreuCodigo: TIntegerField;
    QrLOI: TmySQLQuery;
    QrLOICliente: TIntegerField;
    QrLOITipo: TSmallintField;
    QrLOIBanco: TIntegerField;
    QrLOIAgencia: TIntegerField;
    QrLOIConta: TWideStringField;
    QrLOICheque: TIntegerField;
    QrLOIDuplicata: TWideStringField;
    QrLOICPF: TWideStringField;
    QrLOIEmitente: TWideStringField;
    QrLOIEmissao: TDateField;
    QrLOIDDeposito: TDateField;
    QrLOIVencto: TDateField;
    QrLOIDCompra: TDateField;
    QrOcorreuLOIS: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure VerificaClienteOcorreu();

  end;

  var
  FmCredito2_Ini: TFmCredito2_Ini;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmCredito2_Ini.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCredito2_Ini.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCredito2_Ini.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
end;

procedure TFmCredito2_Ini.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCredito2_Ini.VerificaClienteOcorreu();
  procedure AbreTabela(Tabela, LOIS: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLOI, Dmod.MyDB, [
    'SELECT lot.Cliente, lot.Tipo, loi.Banco, loi.Agencia, loi.Conta, ',
    'loi.Cheque, loi.Duplicata, loi.CPF, loi.Emitente, ',
    'loi.Emissao, loi.DDeposito, loi.Vencto, loi.DCompra ',
    'FROM ' + Tabela + ' loi ',
    'LEFT JOIN ' + TAB_LOT + ' lot ON lot.Codigo=loi.Codigo ',
    'WHERE loi.Controle=' + LOIS,
    '']);
  end;
var
  Ocorreu, LOIS: String;
var
  Conta, Duplicata, CPF, Emitente, Emissao, DCompra, Vencto, DDeposito: String;
  Codigo, Cliente, TpOcor, Banco, Agencia, Cheque: Integer;
  //Valor, TaxaB, TaxaP, TaxaV, Pago: Double;
begin
  MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
  'Definindo ocorr�ncias de clientes');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET TpOcor=3 ');
  Dmod.QrUpd.SQL.Add('WHERE Cliente<>0 ');
  Dmod.QrUpd.SQL.Add('AND ' + FLD_LOIS + '=0');
  Dmod.QrUpd.ExecSQL;
  //
{
  QrOcorreu.Close;
  QrOcorreu. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorreu, Dmod.MyDB, [
  'SELECT Codigo, ' + FLD_LOIS + ' LOIS ',
  'FROM ocorreu ',
  'WHERE Cliente=0 ',
  'AND ' + FLD_LOIS + '<>0' ,
  '']);
  //
  PB1.Position := 0;
  PB1.Max := QrOcorreu.RecordCount;
  //
  while not QrOcorreu.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Inserindo dados de item de lote na ocorr�ncia n� ' +
    FormatFloat('0', QrOcorreuCodigo.Value));
    //
    Ocorreu := FormatFloat('0', QrOcorreuCodigo.Value);
    LOIS := FormatFloat('0', QrOcorreuLOIS.Value);
    //
    //QrLOI.Database := Dmod.MyDB;
    AbreTabela(TAB_LOI, LOIS);
    if QrLOI.RecordCount = 0 then
    begin
      try
        AbreTabela('lotezits', LOIS);
      except
        //
      end;
    end;
    //
    if (QrLOI.State <> dsInactive) and (QrLOI.RecordCount > 0) then
    begin
      Codigo         := QrOcorreuCodigo.Value;
      //
      Cliente        := QrLOICliente.Value;
      TpOcor         := QrLOITipo.Value + 1;
      Banco          := QrLOIBanco.Value;
      Agencia        := QrLOIAgencia.Value;
      Conta          := QrLOIConta.Value;
      Cheque         := QrLOICheque.Value;
      Duplicata      := QrLOIDuplicata.Value;
      CPF            := QrLOICPF.Value;
      Emitente       := QrLOIEmitente.Value;
      Emissao        := Geral.FDT(QrLOIEmissao.Value, 1);
      DCompra        := Geral.FDT(QrLOIDCompra.Value, 1);
      Vencto         := Geral.FDT(QrLOIVencto.Value, 1);
      DDeposito      := Geral.FDT(QrLOIDDeposito.Value, 1);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ocorreu', False, [
      'Cliente',
      'TpOcor', 'Banco', 'Agencia',
      'Conta', 'Cheque', 'Duplicata',
      'CPF', 'Emitente', 'Emissao',
      'DCompra', 'Vencto', 'DDeposito'], [
      'Codigo'], [
      Cliente,
      TpOcor, Banco, Agencia,
      Conta, Cheque, Duplicata,
      CPF, Emitente, Emissao,
      DCompra, Vencto, DDeposito], [
      Codigo], False);
    end;
    //
    QrOcorreu.Next;
  end;
end;

end.
