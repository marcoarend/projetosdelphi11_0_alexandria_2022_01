unit Composto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit;

type
  TFmComposto = class(TForm)
    PainelDados: TPanel;
    Label11: TLabel;
    EdBaseT: TdmkEdit;
    Label1: TLabel;
    EdPrazo: TdmkEdit;
    EdJuroV: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdJuroP: TdmkEdit;
    Label4: TLabel;
    EdLiqui: TdmkEdit;
    EdTaxaM: TdmkEdit;
    Label5: TLabel;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBaseTChange(Sender: TObject);
    procedure EdPrazoChange(Sender: TObject);
    procedure EdTaxaMChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmComposto: TFmComposto;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmComposto.BtOKClick(Sender: TObject);
var
  BaseT, JuroP, JuroV, Prazo, TaxaM, Liqui: Double;
begin
  BaseT := EdBaseT.ValueVariant;
  Prazo := EdPrazo.ValueVariant;
  TaxaM := EdTaxaM.ValueVariant;
  //
  JuroP := MLAGeral.CalculaJuroComposto(TaxaM, Prazo);
  JuroV := Trunc(JuroP * BaseT) / 100;
  Liqui := BaseT - JuroV;
  //
  EdJuroP.ValueVariant := JuroP;
  EdJuroV.ValueVariant := JuroV;
  EdLiqui.ValueVariant := Liqui;
end;

procedure TFmComposto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComposto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmComposto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, False, taCenter, 2, 10, 20);
end;

procedure TFmComposto.EdBaseTChange(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmComposto.EdPrazoChange(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmComposto.EdTaxaMChange(Sender: TObject);
begin
  BtOKClick(Self);
end;

end.
