object FmTaxas: TFmTaxas
  Left = 368
  Top = 194
  Caption = 'CDR-TAXAS-001 :: Cadastro de Taxas'
  ClientHeight = 552
  ClientWidth = 758
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 758
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 392
      Width = 758
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 1
      end
      object Panel3: TPanel
        Left = 364
        Top = 15
        Width = 392
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 283
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtContas: TBitBtn
          Tag = 10087
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Contas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtContasClick
        end
      end
    end
    object GBData: TGroupBox
      Left = 0
      Top = 0
      Width = 758
      Height = 369
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBText1: TDBText
        Left = 378
        Top = 16
        Width = 65
        Height = 13
        DataField = 'FORMA_TXT'
        DataSource = DsTaxas
      end
      object Label5: TLabel
        Left = 12
        Top = 60
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 72
        Top = 32
        Width = 300
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 378
        Top = 32
        Width = 80
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Valor'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 12
        Top = 99
        Width = 446
        Height = 45
        Caption = 'G'#234'nero'
        Color = clBtnFace
        Columns = 2
        DataField = 'Genero'
        DataSource = DsTaxas
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
      end
      object dmkDBCheckGroup2: TdmkDBCheckGroup
        Left = 12
        Top = 149
        Width = 446
        Height = 45
        Caption = 'Modo de reportar'
        Color = clBtnFace
        Columns = 2
        DataField = 'Mostra'
        DataSource = DsTaxas
        Items.Strings = (
          'Cont'#225'bil'
          'Extrato')
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
      end
      object dmkDBCheckGroup3: TdmkDBCheckGroup
        Left = 12
        Top = 198
        Width = 446
        Height = 45
        Caption = 'Insere autom'#225'tico'
        Color = clBtnFace
        Columns = 2
        DataField = 'Automatico'
        DataSource = DsTaxas
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 12
        Top = 248
        Width = 446
        Height = 45
        Caption = 'Forma de cobran'#231'a'
        Columns = 2
        DataField = 'Forma'
        DataSource = DsTaxas
        Items.Strings = (
          'Valor'
          'Porcentagem')
        ParentBackground = True
        TabOrder = 6
        Values.Strings = (
          '0'
          '1')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 12
        Top = 298
        Width = 446
        Height = 45
        Caption = 'Base multiplicadora da cobran'#231'a'
        Columns = 2
        DataField = 'Base'
        DataSource = DsTaxas
        Items.Strings = (
          'Valor total dos itens'
          'Contagem dos Itens')
        ParentBackground = True
        TabOrder = 7
        Values.Strings = (
          '0'
          '1')
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 12
        Top = 76
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'PlaGen'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taRightJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 68
        Top = 76
        Width = 389
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_PLAGEN'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 758
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 393
      Width = 758
      Height = 63
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 648
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdit: TGroupBox
      Left = 0
      Top = 0
      Width = 758
      Height = 365
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label7: TLabel
        Left = 12
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label9: TLabel
        Left = 72
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 377
        Top = 16
        Width = 53
        Height = 13
        Caption = '% ou Valor:'
      end
      object Label4: TLabel
        Left = 12
        Top = 60
        Width = 131
        Height = 13
        Caption = 'Conta (do plano de contas):'
      end
      object SpeedButton5: TSpeedButton
        Left = 436
        Top = 76
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 72
        Top = 32
        Width = 300
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdValor: TdmkEdit
        Left = 377
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'Valor'
        UpdCampo = 'Valor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object RGForma: TdmkRadioGroup
        Left = 12
        Top = 252
        Width = 445
        Height = 45
        Caption = ' Forma de cobran'#231'a: '
        Columns = 2
        Items.Strings = (
          'Valor'
          'Porcentagem')
        TabOrder = 8
        QryCampo = 'Forma'
        UpdCampo = 'Forma'
        UpdType = utYes
        OldValor = 0
      end
      object RGBase: TdmkRadioGroup
        Left = 12
        Top = 302
        Width = 445
        Height = 45
        Caption = ' Base da multiplicadora da cobran'#231'a: '
        Columns = 2
        Items.Strings = (
          'Valor total dos itens'
          'Contagem dos Itens')
        TabOrder = 9
        QryCampo = 'Base'
        UpdCampo = 'Base'
        UpdType = utYes
        OldValor = 0
      end
      object CGAutoma: TdmkCheckGroup
        Left = 12
        Top = 202
        Width = 445
        Height = 45
        Caption = ' Insere autom'#225'tico '
        Color = clBtnFace
        Columns = 2
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        ParentColor = False
        TabOrder = 7
        QryCampo = 'Automatico'
        UpdCampo = 'Automatico'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object CGMostra: TdmkCheckGroup
        Left = 12
        Top = 153
        Width = 445
        Height = 45
        Caption = 'Modo de reportar'
        Color = clBtnFace
        Columns = 2
        Items.Strings = (
          'Cont'#225'bil'
          'Extrato')
        ParentColor = False
        TabOrder = 6
        QryCampo = 'Mostra'
        UpdCampo = 'Mostra'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object CGGenero: TdmkCheckGroup
        Left = 12
        Top = 103
        Width = 445
        Height = 45
        Caption = 'G'#234'nero'
        Color = clBtnFace
        Columns = 2
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        ParentColor = False
        TabOrder = 5
        QryCampo = 'Genero'
        UpdCampo = 'Genero'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object EdPlaGen: TdmkEditCB
        Left = 12
        Top = 76
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PlaGen'
        UpdCampo = 'PlaGen'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPlaGen
        IgnoraDBLookupComboBox = False
      end
      object CBPlaGen: TdmkDBLookupComboBox
        Left = 68
        Top = 76
        Width = 365
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 4
        dmkEditCB = EdPlaGen
        QryCampo = 'PlaGen'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 758
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 710
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 494
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 226
        Height = 32
        Caption = 'Cadastro de Taxas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 226
        Height = 32
        Caption = 'Cadastro de Taxas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 226
        Height = 32
        Caption = 'Cadastro de Taxas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 758
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 754
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsTaxas: TDataSource
    DataSet = QrTaxas
    Left = 40
    Top = 12
  end
  object QrTaxas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTaxasBeforeOpen
    AfterOpen = QrTaxasAfterOpen
    OnCalcFields = QrTaxasCalcFields
    SQL.Strings = (
      'SELECT cta.Nome NO_PLAGEN, txa.*'
      'FROM taxas txa'
      'LEFT JOIN contas cta ON cta.Codigo=txa.PlaGen')
    Left = 12
    Top = 12
    object QrTaxasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTaxasFORMA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FORMA_TXT'
      Size = 5
      Calculated = True
    end
    object QrTaxasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTaxasGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrTaxasForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrTaxasMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrTaxasBase: TSmallintField
      FieldName = 'Base'
    end
    object QrTaxasAutomatico: TSmallintField
      FieldName = 'Automatico'
    end
    object QrTaxasValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,##0.000000'
    end
    object QrTaxasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTaxasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTaxasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTaxasPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
    object QrTaxasNO_PLAGEN: TWideStringField
      FieldName = 'NO_PLAGEN'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Panel2
    CanUpd01 = BtInclui
    CanDel01 = BtAltera
    Left = 68
    Top = 12
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 496
    Top = 192
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 524
    Top = 192
  end
end
