object FmEmitSacImport: TFmEmitSacImport
  Left = 339
  Top = 185
  Caption = 'EMI-TENTE-004 ::  Importa'#231#227'o de Emitentes'
  ClientHeight = 573
  ClientWidth = 704
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 704
    Height = 411
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Memo1: TMemo
      Left = 0
      Top = 136
      Width = 704
      Height = 275
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      ExplicitLeft = 276
      ExplicitTop = 92
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 704
      Height = 136
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 118
        Height = 13
        Caption = 'Arquivo fonte dos dados:'
      end
      object Edit1: TdmkEdit
        Left = 12
        Top = 20
        Width = 585
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object BtImportar: TBitBtn
        Tag = 19
        Left = 600
        Top = 8
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Abre'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtImportarClick
        NumGlyphs = 2
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 48
        Width = 285
        Height = 61
        Caption = ' Fonte dados: '
        TabOrder = 2
        object Label2: TLabel
          Left = 8
          Top = 16
          Width = 55
          Height = 13
          Caption = '000 - Parc.:'
        end
        object Label3: TLabel
          Left = 76
          Top = 16
          Width = 53
          Height = 13
          Caption = '001 - DIBs:'
        end
        object Label4: TLabel
          Left = 144
          Top = 16
          Width = 51
          Height = 13
          Caption = '002 - BAC:'
        end
        object Label5: TLabel
          Left = 212
          Top = 16
          Width = 61
          Height = 13
          Caption = 'Desconhec.:'
        end
        object EdFonte00: TdmkEdit
          Left = 8
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdFonte01: TdmkEdit
          Left = 76
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdFonte02: TdmkEdit
          Left = 144
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdFonte09: TdmkEdit
          Left = 212
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
      object GroupBox2: TGroupBox
        Left = 300
        Top = 48
        Width = 389
        Height = 61
        Caption = ' Andamento da importa'#231#227'o: '
        TabOrder = 3
        object Label6: TLabel
          Left = 8
          Top = 16
          Width = 47
          Height = 13
          Caption = '001 - Sim:'
        end
        object Label7: TLabel
          Left = 76
          Top = 16
          Width = 50
          Height = 13
          Caption = '001 - N'#227'o:'
        end
        object Label8: TLabel
          Left = 144
          Top = 16
          Width = 47
          Height = 13
          Caption = '002 - Sim:'
        end
        object Label9: TLabel
          Left = 212
          Top = 16
          Width = 47
          Height = 13
          Caption = '002 - N'#227'o'
        end
        object Label10: TLabel
          Left = 280
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Erro DIB:'
        end
        object Ed001s: TdmkEdit
          Left = 8
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object Ed001n: TdmkEdit
          Left = 76
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object Ed002s: TdmkEdit
          Left = 144
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object Ed002n: TdmkEdit
          Left = 212
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdDIBe: TdmkEdit
          Left = 280
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
      object PB1: TProgressBar
        Left = 0
        Top = 119
        Width = 704
        Height = 17
        Align = alBottom
        TabOrder = 4
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 704
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 656
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 608
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 303
        Height = 32
        Caption = 'Importa'#231#227'o de Emitentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 303
        Height = 32
        Caption = 'Importa'#231#227'o de Emitentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 303
        Height = 32
        Caption = 'Importa'#231#227'o de Emitentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 459
    Width = 704
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 700
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 343
        Height = 16
        Caption = 'DIB = Documento de identifica'#231#227'o banc'#225'ria: CPF ou CNPJ.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 343
        Height = 16
        Caption = 'DIB = Documento de identifica'#231#227'o banc'#225'ria: CPF ou CNPJ.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 503
    Width = 704
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 558
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 556
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 100
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Importa'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object CkVerificaCPF: TCheckBox
        Left = 8
        Top = 16
        Width = 85
        Height = 17
        Caption = 'Verificar CPF.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 540
    Top = 28
  end
  object QrEmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO emitcpf SET '
      'CPF=:P0, Nome=:P1, LastAtz=:P2, '
      'Limite=:P3')
    Left = 245
    Top = 249
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object QrBAC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'INSERT INTO emitbac SET '
      'CPF=:P0, BAC=:P1')
    Left = 273
    Top = 249
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM emitpar'
      'WHERE CPF=:P0'
      'ORDER BY Codigo')
    Left = 369
    Top = 245
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrPesqCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
