unit HistCliEmi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, Mask, DBCtrls,
  Grids, DBGrids, ComCtrls, frxDBSet, frxChBox, frxClass, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabelRotate, dmkDBEdit, dmkGeral, dmkImage,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmHistCliEmi = class(TForm)
    PainelDados: TPanel;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFNome: TWideStringField;
    DsEmitCPF: TDataSource;
    QrSomaA: TmySQLQuery;
    DsSomaA: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    QrItens: TmySQLQuery;
    DsItens: TDataSource;
    QrItensITENS: TLargeintField;
    QrItensITENS_AVencer: TLargeintField;
    QrItensITENS_Vencidos: TLargeintField;
    QrItensITENS_PgVencto: TLargeintField;
    QrItensITENS_Devolvid: TLargeintField;
    QrItensITENS_DevPgTot: TLargeintField;
    QrItensITENS_DevNaoQt: TLargeintField;
    QrItensPago: TFloatField;
    QrItensEXPIRADO: TFloatField;
    QrItensAEXPIRAR: TFloatField;
    QrItensVENCIDO: TFloatField;
    QrItensPGDDEPOSITO: TFloatField;
    QrItensPGATRAZO: TFloatField;
    QrItensPGABERTO: TFloatField;
    QrItensPERIODO: TFloatField;
    QrItensNOMECLI: TWideStringField;
    QrItensNOMEBANCO: TWideStringField;
    QrItensDDeposito: TDateField;
    QrItensNOMEEMITENTE: TWideStringField;
    QrItensNOMESTATUS: TWideStringField;
    QrSomaAValor: TFloatField;
    QrSomaAITENS: TLargeintField;
    QrSomaAITENS_AVencer: TFloatField;
    QrSomaAITENS_Vencidos: TFloatField;
    QrSomaAITENS_PgVencto: TFloatField;
    QrSomaAITENS_Devolvid: TFloatField;
    QrSomaAITENS_DevPgTot: TFloatField;
    QrSomaAITENS_DevNaoQt: TFloatField;
    QrSomaAPago: TFloatField;
    QrSomaAEXPIRADO: TFloatField;
    QrSomaAAEXPIRAR: TFloatField;
    QrSomaAVENCIDO: TFloatField;
    QrSomaAPGDDEPOSITO: TFloatField;
    QrSomaAPGATRAZO: TFloatField;
    QrSomaAPGABERTO: TFloatField;
    QrSomaAPERC_AVencer: TFloatField;
    QrSomaAPERC_Vencidos: TFloatField;
    QrSomaAPERC_PgVencto: TFloatField;
    QrSomaAPERC_Devolvid: TFloatField;
    QrSomaAPERC_DevPgTot: TFloatField;
    QrSomaAPERC_DevNaoQt: TFloatField;
    QrSomaAPERC_ITENS: TFloatField;
    QrTudo: TmySQLQuery;
    QrTudoITENS: TLargeintField;
    QrSomaAPERC_Expirado: TFloatField;
    QrSomaAPERC_AExpirar: TFloatField;
    QrSomaAPERC_Vencido: TFloatField;
    QrSomaAPERC_PgDDeposito: TFloatField;
    QrSomaAPERC_Atrazo: TFloatField;
    QrSomaAPERC_Aberto: TFloatField;
    QrTudoVALOR: TFloatField;
    QrSomaAPERC_VALOR: TFloatField;
    QrSomaB: TmySQLQuery;
    QrSomaBValor: TFloatField;
    QrSomaBITENS: TLargeintField;
    QrSomaBITENS_AVencer: TFloatField;
    QrSomaBITENS_Vencidos: TFloatField;
    QrSomaBITENS_PgVencto: TFloatField;
    QrSomaBITENS_Devolvid: TFloatField;
    QrSomaBITENS_DevPgTot: TFloatField;
    QrSomaBITENS_DevNaoQt: TFloatField;
    QrSomaBPago: TFloatField;
    QrSomaBEXPIRADO: TFloatField;
    QrSomaBAEXPIRAR: TFloatField;
    QrSomaBVENCIDO: TFloatField;
    QrSomaBPGDDEPOSITO: TFloatField;
    QrSomaBPGATRAZO: TFloatField;
    QrSomaBPGABERTO: TFloatField;
    QrSomaASOMA_I_AVencer: TFloatField;
    QrSomaASOMA_I_Vencidos: TFloatField;
    QrSomaASOMA_I_PgVencto: TFloatField;
    QrSomaASOMA_I_ITENS: TFloatField;
    QrSomaASOMA_I_Devolvid: TFloatField;
    QrSomaASOMA_I_DevPgTot: TFloatField;
    QrSomaASOMA_I_DevNaoQt: TFloatField;
    QrSomaASOMA_V_AEXPIRAR: TFloatField;
    QrSomaASOMA_V_EXPIRADO: TFloatField;
    QrSomaASOMA_V_VENCIDO: TFloatField;
    QrSomaASOMA_V_PGDDEPOSITO: TFloatField;
    QrSomaASOMA_V_PGATRAZO: TFloatField;
    QrSomaASOMA_V_PGABERTO: TFloatField;
    QrSomaASOMA_V_VALOR: TFloatField;
    QrItensBanco: TIntegerField;
    QrItensDuplicata: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    QrGruSacEmiIts: TmySQLQuery;
    QrGruSacEmiItsCodigo: TIntegerField;
    QrGruSacEmiItsCNPJ_CPF: TWideStringField;
    QrGruSacEmiItsNome: TWideStringField;
    DsGruSacEmiIts: TDataSource;
    QrPesqGru: TmySQLQuery;
    DBGrid2: TDBGrid;
    QrPesqGruCodigo: TIntegerField;
    QrGruSacEmiItsCNPJ_CPF_TXT: TWideStringField;
    QrGruSacEmiItsNOMEGRUPO: TWideStringField;
    QrItensITENS_XXX: TLargeintField;
    GroupBox1: TGroupBox;
    CkA: TCheckBox;
    CkQ: TCheckBox;
    CkR: TCheckBox;
    CkP: TCheckBox;
    QrRevenda: TmySQLQuery;
    QrRevendaPago: TFloatField;
    QrRevendaItens: TFloatField;
    DsRevenda: TDataSource;
    QrRevendaPago_Perc: TFloatField;
    QrRevendaItens_Perc: TFloatField;
    frxHistorico: TfrxReport;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxDsItens: TfrxDBDataset;
    frxDsSomaA: TfrxDBDataset;
    QrMediaDias: TmySQLQuery;
    DsMediaDias: TDataSource;
    QrMediaDiasPRAZO_MEDIO: TFloatField;
    frxDsMediaDias: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel5: TPanel;
    BtOK: TBitBtn;
    BtImprime: TBitBtn;
    BtGrupo: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    EdCPF1: TdmkEdit;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CkPeriodo: TCheckBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    CkLotNeg: TCheckBox;
    CkGrupos: TCheckBox;
    CkMediaDias: TCheckBox;
    dmkEdMediaDias: TdmkDBEdit;
    RGTipo: TRadioGroup;
    GroupBox3: TGroupBox;
    Panel2: TPanel;
    Bevel31: TBevel;
    Bevel1: TBevel;
    Bevel24: TBevel;
    Bevel23: TBevel;
    Bevel25: TBevel;
    Bevel26: TBevel;
    Bevel27: TBevel;
    Bevel28: TBevel;
    Bevel29: TBevel;
    Bevel22: TBevel;
    Bevel21: TBevel;
    Bevel20: TBevel;
    Bevel19: TBevel;
    Bevel18: TBevel;
    Bevel17: TBevel;
    Bevel16: TBevel;
    Bevel8: TBevel;
    Bevel7: TBevel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel4: TBevel;
    Bevel3: TBevel;
    Bevel2: TBevel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Bevel9: TBevel;
    Bevel10: TBevel;
    Bevel11: TBevel;
    Bevel12: TBevel;
    Bevel13: TBevel;
    Bevel14: TBevel;
    Bevel15: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    LaValores: TdmkLabelRotate;
    LaItens: TdmkLabelRotate;
    Label31: TLabel;
    Label32: TLabel;
    Bevel30: TBevel;
    Label33: TLabel;
    Bevel32: TBevel;
    Label34: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    QrItensFatParcela: TIntegerField;
    QrItensCredito: TFloatField;
    QrItensContaCorrente: TWideStringField;
    QrItensTipo: TWideStringField;
    QrItensDOCUM_TXT: TWideStringField;
    QrItensDocumento: TFloatField;
    QrItensAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCPF1Exit(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure QrItensCalcFields(DataSet: TDataSet);
    procedure EdCPF1Change(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure CkCrescenteClick(Sender: TObject);
    procedure QrSomaACalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure BtGrupoClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrGruSacEmiItsCalcFields(DataSet: TDataSet);
    procedure CkLotNegClick(Sender: TObject);
    procedure CkGruposClick(Sender: TObject);
    procedure QrGruSacEmiItsBeforeClose(DataSet: TDataSet);
    procedure QrGruSacEmiItsAfterOpen(DataSet: TDataSet);
    procedure QrItensBeforeClose(DataSet: TDataSet);
    procedure QrItensAfterOpen(DataSet: TDataSet);
    procedure CkAClick(Sender: TObject);
    procedure CkQClick(Sender: TObject);
    procedure CkRClick(Sender: TObject);
    procedure CkPClick(Sender: TObject);
    procedure QrRevendaCalcFields(DataSet: TDataSet);
    procedure frxHistoricoGetValue(const VarName: String;
      var Value: Variant);
    procedure CkMediaDiasClick(Sender: TObject);
  private
    { Private declarations }
    FOrdC1, FOrdC2, FOrdC3, FOrdS1, FOrdS2, FOrdS3, FGrupos: String;
    procedure FechaPesquisa;
    procedure MostraCkGrupos;
    procedure DesativaItens;
  public
    { Public declarations }
    procedure Pesquisa;
    procedure ReopenEmiSac(CPF: String);
  end;

  var
  FmHistCliEmi: TFmHistCliEmi;

implementation

{$R *.DFM}

uses UnMyObjects, Module, PesqCPFCNPJ, UnInternalConsts, Principal, 
  MyListas, UMySQLModule;

procedure TFmHistCliEmi.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHistCliEmi.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  MyObjects.CorIniComponente();
end;

procedure TFmHistCliEmi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmHistCliEmi.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then
    begin
      EdCPF1.Text := VAR_CPF_PESQ;
    end;
    ReopenEmiSac(VAR_CPF_PESQ);
  end;
end;

procedure TFmHistCliEmi.ReopenEmiSac(CPF: String);
var
  Grupos, CPFx: String;
begin
  QrEmitCPF.Close;
  QrGruSacEmiIts.Close;
  CPFx := Geral.SoNumero_TT(CPF);
  if Trim(CPFx) <> '' then
  begin
    QrEmitCPF.Params[0].AsString := CPFx;
    UMyMod.AbreQuery(QrEmitCPF, Dmod.MyDB);
    //
    QrPesqGru.Close;
    QrPesqGru.Params[0].AsString := CPFx;
    UMyMod.AbreQuery(QrPesqGru, Dmod.MyDB);
    if QrPesqGru.RecordCount > 0 then
    begin
      Grupos := '';
      while not QrPesqGru.Eof do
      begin
        Grupos := Grupos + ',' + IntToStr(QrPesqGruCodigo.Value);
        QrPesqGru.Next;
      end;
      Grupos[1] := '(';
      QrGruSacEmiIts.Close;
      QrGruSacEmiIts.SQL.Clear;
      QrGruSacEmiIts.SQL.Add('SELECT gse.Nome NOMEGRUPO, gei.*');
      QrGruSacEmiIts.SQL.Add('FROM grusacemiits gei');
      QrGruSacEmiIts.SQL.Add('LEFT JOIN grusacemi gse ON gse.Codigo=gei.Codigo');
      QrGruSacEmiIts.SQL.Add('WHERE gse.Codigo in '+Grupos+')');
      QrGruSacEmiIts.SQL.Add('ORDER BY NOMEGRUPO, gei.Nome');
      QrGruSacEmiIts.SQL.Add('');
      UMyMod.AbreQuery(QrGruSacEmiIts, Dmod.MyDB);
    end;
  end;
end;

procedure TFmHistCliEmi.EdCPF1Exit(Sender: TObject);
begin
  ReopenEmiSac(EdCPF1.Text);
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  FOrdC1 := 'DDeposito';
  FOrdS1 := '';
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  TPIni.Date := Date - 180;
  TPFim.Date := Date + 365;
end;

procedure TFmHistCliEmi.BtOKClick(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmHistCliEmi.Pesquisa;
const
  ItensOrdem: array[0..4] of String = ('NOMECLI', 'NOMEEMITENTE', 'NOMEBANCO',
  'Periodo', 'DDeposito');
var
  A, Q, R, P, T: Integer;
begin
  if EdCliente.ValueVariant = 0 then
    if Geral.MensagemBox('N�o foi definido nenhum cliente.'+
    #13#10+'Para pesquisar todos os clientes pode demorar v�rios minutos.'
    +#13#10+'Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    ReopenEmiSac(EdCPF1.Text);
    MostraCkGrupos;
    A := MLAGeral.BoolToInt(CkA.Checked);
    Q := MLAGeral.BoolToInt(CkQ.Checked);
    R := MLAGeral.BoolToInt(CkR.Checked);
    P := MLAGeral.BoolToInt(CkP.Checked);
    T := A + Q + R + P;
    if A = 0 then A := -1000 else A := 1;
    if Q = 0 then Q := -1000 else Q := 2;
    if R = 0 then R := -1000 else R := 4;
    if P = 0 then P := -1000 else P := 0;

    //

    FGrupos := '';
    if QrGruSacEmiIts.State = dsBrowse then
      if QrGruSacEmiIts.RecordCount > 0 then
        //if CkGrupos.Enabled then
          if CkGrupos.Checked then
    begin
      QrGruSacEmiIts.First;
      while not QrGruSacEmiIts.Eof do
      begin
        FGrupos := FGrupos + ',"' + QrGruSacEmiItsCNPJ_CPF.Value+'"';
        QrGruSacEmiIts.Next;
      end;
      if Length(Trim(FGrupos)) > 0 then FGrupos[1] := '(';
    end;
    if RGTipo.ItemIndex in ([1,2]) then
    begin
{
      QrSomaB.Close;
      QrSomaB.SQL.Clear;
      QrSomaB.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
      QrSomaB.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
      QrSomaB.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) ITENS_PgVencto,');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) +');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ITENS_Devolvid,');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ITENS_DevPgTot,');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) ITENS_DevNaoQt,');
      QrSomaB.SQL.Add('SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago,');
      QrSomaB.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
      QrSomaB.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
      QrSomaB.SQL.Add('SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)');
      QrSomaB.SQL.Add('  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0)) VENCIDO,');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),');
      QrSomaB.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO,');
      QrSomaB.SQL.Add('SUM(IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3),');
      QrSomaB.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO,');
      QrSomaB.SQL.Add('SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),');
      QrSomaB.SQL.Add('  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGABERTO');
      QrSomaB.SQL.Add('FROM lot esits loi');
      QrSomaB.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
      QrSomaB.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
      QrSomaB.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
      QrSomaB.SQL.Add('WHERE lot.Tipo=1');
      QrSomaB.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkLotNeg.Checked = False then
        QrSomaB.SQL.Add('AND lot.Codigo > 0');
      if CkPeriodo.Checked then
        QrSomaB.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrSomaB.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
        if Trim(FGrupos) <> ''  then
          QrSomaB.SQL.Add('AND loi.CNPJCPF in ' + FGrupos + ')')
        else
          QrSomaB.SQL.Add('AND loi.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
      end;
      //////////////////////////////////////////////////////////////////////////////
      QrSomaB. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrSomaB, Dmod.MyDB, [
      'SELECT SUM(lct.Credito) Valor, COUNT(lct.FatParcela) ITENS, ',
      'SUM(IF(lct.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer, ',
      'SUM(IF((lct.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos, ',
      'SUM(IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 1, 0)) ITENS_PgVencto, ',
      'SUM(IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0)) + ',
      'SUM(IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0)) ITENS_Devolvid, ',
      'SUM(IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0)) ITENS_DevPgTot, ',
      'SUM(IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0)) ITENS_DevNaoQt, ',
      'SUM((lct.TotalPg - lct.TotalJr + lct.TotalDs)) Pago, ',
      'SUM(IF(lct.DDeposito < SYSDATE(), lct.Credito, 0)) EXPIRADO, ',
      'SUM(IF(lct.DDeposito < SYSDATE(), 0, lct.Credito)) AEXPIRAR, ',
      'SUM(IF((lct.DDeposito<SYSDATE()) AND ((lct.Data3<2) ',
      '  OR (lct.DDeposito<lct.Data3)), lct.Credito, 0)) VENCIDO, ',
      'SUM(IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), ',
      '  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0)) PGDDEPOSITO, ',
      'SUM(IF(/*(lct.Quitado> 1)AND*/ (lct.DDeposito< lct.Data3), ',
      '  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0)) PGATRAZO, ',
      'SUM(IF((lct.Quitado< 2)AND (lct.DDeposito< SYSDATE()), ',
      '  lct.Credito - (lct.TotalPg - lct.TotalJr) + lct.TotalDs, 0)) PGABERTO ',
      'FROM ' + CO_TabLctA + ' lct ',
      'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum ',
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente ',
      'LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco ',
      'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
      'AND lot.Tipo=1 ',
      'AND lot.TxCompra + lot.ValValorem+ ' + IntToStr(FmPrincipal.FConnections) + ' >= 0.01',
      //
      Geral.ATS_if(CkLotNeg.Checked = False, [
      'AND lot.Codigo > 0']),
      //
      Geral.ATS_if(CkPeriodo.Checked, [dmkPF.SQL_Periodo(
      'AND lct.DDeposito ', TPIni.Date, TPFim.Date, True, True)]),
      //
      Geral.ATS_if(EdCliente.ValueVariant <> 0, [
      'AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant)]),
      //
      Geral.ATS_if((EdCPF1.Text <> '') and (Trim(FGrupos) <> ''), [
      'AND lct.CNPJCPF in ' + FGrupos + ')']),
      //
      Geral.ATS_if((EdCPF1.Text <> '') and (Trim(FGrupos) = ''), [
      'AND lct.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"']),
      //
      '']);
    end;

    //////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////

{
    QrSomaA.Close;
    QrSomaA.SQL.Clear;
    QrSomaA.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
    QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
    QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) -');
    QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
    QrSomaA.SQL.Add('1, 0)) ITENS_PgVencto,');
    QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
    QrSomaA.SQL.Add('1, 0)) ITENS_Devolvid,');
    QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),');
    QrSomaA.SQL.Add('1, 0)) ITENS_DevPgTot,');
    QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)),');
    QrSomaA.SQL.Add('1, 0)) ITENS_DevNaoQt,');
    QrSomaA.SQL.Add('SUM((chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto)) Pago,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, ');
    QrSomaA.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0)) VENCIDO,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,');
    QrSomaA.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor))) PGDDEPOSITO,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
    QrSomaA.SQL.Add('chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0)) PGATRAZO,');
    QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
    QrSomaA.SQL.Add('loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +');
    QrSomaA.SQL.Add('chd.Desconto, 0)) PGABERTO');
    QrSomaA.SQL.Add('FROM lot esits loi');
    QrSomaA.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
    QrSomaA.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
    QrSomaA.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
    QrSomaA.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
    QrSomaA.SQL.Add('WHERE lot.Tipo <> 1');
    QrSomaA.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    if CkLotNeg.Checked = False then
      QrSomaA.SQL.Add('AND lot.Codigo > 0');
    if RGTipo.ItemIndex in ([0,2]) then
    begin
      if CkPeriodo.Checked then
        QrSomaA.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
    end else QrSomaA.SQL.Add('AND loi.DDeposito < -100000');
    //////////////////////////////////////////////////////////////////////////////
    if EdCliente.ValueVariant <> 0 then
      QrSomaA.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
    if EdCPF1.Text <> '' then
    begin
      if Trim(FGrupos) <> ''  then
        QrSomaA.SQL.Add('AND loi.CPF in ' + FGrupos + ')')
      else
        QrSomaA.SQL.Add('AND loi.CPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
    end;
    QrSomaA. Open;
}
    UnDmkDAC_PF.AbreMySQLQuery0(QrSomaA, Dmod.MyDB, [
    'SELECT SUM(lct.Credito) Valor, COUNT(lct.FatParcela) ITENS, ',
    'SUM(IF(lct.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer, ',
    'SUM(IF((lct.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos, ',
    'SUM(IF((lct.DDeposito < SYSDATE()), 1, 0)) - ',
    'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), ',
    '1, 0)) ITENS_PgVencto, ',
    'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), ',
    '1, 0)) ITENS_Devolvid, ',
    'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), ',
    '1, 0)) ITENS_DevPgTot, ',
    'SUM(IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)), ',
    '1, 0)) ITENS_DevNaoQt, ',
    'SUM((chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto)) Pago, ',
    'SUM(IF(lct.DDeposito < SYSDATE(), lct.Credito, 0)) EXPIRADO, ',
    'SUM(IF(lct.DDeposito < SYSDATE(), 0, lct.Credito)) AEXPIRAR, ',
    'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, ',
    'lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito), 0)) VENCIDO, ',
    'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0, ',
    'lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito))) PGDDEPOSITO, ',
    'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, ',
    'chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0)) PGATRAZO, ',
    'SUM(IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, ',
    'lct.Credito - (chd.ValPago - chd.Taxas - chd.JurosV) + ',
    'chd.Desconto, 0)) PGABERTO ',
    'FROM ' + CO_TAbLctA + ' lct ',
    'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum ',
    'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente ',
    'LEFT JOIN alinits   chd ON chd.ChequeOrigem=lct.FatParcela ',
    'LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco ',
    'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
    'AND lot.Tipo <> 1 ',
    'AND lot.TxCompra + lot.ValValorem+ ' + IntToStr(FmPrincipal.FConnections) + ' >= 0.01',
    //
    Geral.ATS_if(CkLotNeg.Checked = False, [
    'AND lot.Codigo > 0']),
    //
    Geral.ATS_if(CkPeriodo.Checked and (RGTipo.ItemIndex in ([0,2])), [dmkPF.SQL_Periodo(
    'AND lct.DDeposito ', TPIni.Date, TPFim.Date, True, True)]),
    //
    Geral.ATS_if(not (RGTipo.ItemIndex in ([0,2])), [
    'AND lct.DDeposito < -100000']),
    //
    Geral.ATS_if(EdCliente.ValueVariant <> 0, [
    'AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant)]),
    //
    Geral.ATS_if((EdCPF1.Text <> '') and (Trim(FGrupos) <> ''), [
    'AND lct.CNPJCPF in ' + FGrupos + ')']),
    //
    Geral.ATS_if((EdCPF1.Text <> '') and (Trim(FGrupos) = ''), [
    'AND lct.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"']),
    //
    '']);

    /////////////////////////////// MEDIA DE DIAS
    if CkMediaDias.Checked = True then
    begin
{
      QrMediaDias.Close;
      QrMediaDias.SQL.Clear;
      QrMediaDias.SQL.Add('SELECT SUM(loi.Valor*loi.Dias)/SUM(loi.Valor) PRAZO_MEDIO ');
      QrMediaDias.SQL.Add('FROM lot esits loi');
      QrMediaDias.SQL.Add('LEFT JOIN lot es lot ON lot.Codigo = loi.Codigo');
      QrMediaDias.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
      // Per�odo dep�sito
      QrMediaDias.SQL.Add(dmkPF.SQL_Periodo('WHERE loi.DDeposito ', TPIni.Date, TPFim.Date, CkPeriodo.Checked, CkPeriodo.Checked));
      //
      QrMediaDias.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      //Lot es negativos
      if CkLotNeg.Checked = False then
        QrMediaDias.SQL.Add('AND lot.Codigo > 0');
      //Tipo de documento
      if RGTipo.ItemIndex <> 2 then
        QrMediaDias.SQL.Add('AND lot.Tipo ='+ IntToStr(RGTipo.ItemIndex) +'');
      // Emitente
      if EdCliente.ValueVariant <> 0 then
        QrMediaDias.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      //CPF / CNPJ Emitente
      if EdCPF1.Text <> '' then
      begin
        if Trim(FGrupos) <> ''  then
          QrMediaDias.SQL.Add('AND loi.CPF in ' + FGrupos + ')')
        else
          QrMediaDias.SQL.Add('AND loi.CPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
      end;
      // Configura��o da pesquisa
      if T <> 4 then
      begin
        QrMediaDias.SQL.Add('');
        QrMediaDias.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
        QrMediaDias.SQL.Add('IF((loi.DDeposito < SYSDATE()), 2, 0) -');
        QrMediaDias.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
        QrMediaDias.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
        QrMediaDias.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrMediaDias.SQL.Add('');
      end;

      QrMediaDias. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrMediaDias, Dmod.MyDB, [
      'SELECT SUM(lct.Credito*lct.Dias)/SUM(lct.Credito) PRAZO_MEDIO  ',
      'FROM ' + CO_TabLctA + ' lct ',
      'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo = lct.FatNum ',
      'LEFT JOIN alinits chd ON chd.ChequeOrigem=lct.FatParcela ',
      'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
      // Per�odo dep�sito
      dmkPF.SQL_Periodo('AND lct.DDeposito ',
      TPIni.Date, TPFim.Date, CkPeriodo.Checked, CkPeriodo.Checked),
      //
      'AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01 ',
      //Lot es negativos
      //
      Geral.ATS_if(CkLotNeg.Checked = False, [
        'AND lot.Codigo > 0 ']),
      //Tipo de documento
      Geral.ATS_if(RGTipo.ItemIndex <> 2, [
        'AND lot.Tipo ='+ IntToStr(RGTipo.ItemIndex) +' ']),
      // Emitente
      Geral.ATS_if(EdCliente.ValueVariant <> 0, [
        'AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant)]),
      //CPF / CNPJ Emitente
      Geral.ATS_if((EdCPF1.Text <> '') and (Trim(FGrupos) <> ''), [
        'AND lct.CNPJCPF in ' + FGrupos + ')']),
      Geral.ATS_if((EdCPF1.Text <> '') and (Trim(FGrupos) = ''), [
        'AND lct.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '" ']),
      // Configura��o da pesquisa
      Geral.ATS_if(T <> 4, [
        ' ',
        'AND (IF(lct.DDeposito > SYSDATE(), 1, 0) + ',
        'IF((lct.DDeposito < SYSDATE()), 2, 0) - ',
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) + ',
        'IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0)) ',
        ' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+') ',
        ' ']),
        '']);
    end;
    /////////////////////////////// FIM MEDIA DE DIAS

    //////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////
{
    QrItens.Close;
    QrItens.SQL.Clear;
    if RGTipo.ItemIndex in ([0,2]) then
    begin
      QrItens.SQL.Add('SELECT loi.Controle, loi.Valor, 1 ITENS,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('(IF(loi.DDeposito > SYSDATE(), 1, 0) +');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 2, 0) -');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
      QrItens.SQL.Add('ITENS_XXX,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('IF(loi.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 1, 0) -');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
      QrItens.SQL.Add('1, 0) ITENS_PgVencto,');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
      QrItens.SQL.Add('1, 0) ITENS_Devolvid,');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),');
      QrItens.SQL.Add('1, 0) ITENS_DevPgTot,');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)),');
      QrItens.SQL.Add('1, 0) ITENS_DevNaoQt,');
      QrItens.SQL.Add('(chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto) Pago,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), loi.Valor, 0) EXPIRADO,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), 0, loi.Valor) AEXPIRAR,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
      QrItens.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0) VENCIDO,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,');
      QrItens.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) PGDDEPOSITO,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
      QrItens.SQL.Add('chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0) PGATRAZO,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
      QrItens.SQL.Add('loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +');
      QrItens.SQL.Add('chd.Desconto, 0) PGABERTO, "C" Tipo,');
      QrItens.SQL.Add('MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
      QrItens.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
      QrItens.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE, ');
      QrItens.SQL.Add('loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Duplicata');
      QrItens.SQL.Add('FROM lot esits loi');
      QrItens.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
      QrItens.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
      QrItens.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
      QrItens.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
      QrItens.SQL.Add('WHERE lot.Tipo <> 1');
      if CkLotNeg.Checked = False then
        QrItens.SQL.Add('AND lot.Codigo > 0');
      QrItens.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkPeriodo.Checked then
        QrItens.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrItens.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
      if Trim(FGrupos) <> ''  then
          QrItens.SQL.Add('AND loi.CPF in ' + FGrupos + ')')
        else
          QrItens.SQL.Add('AND loi.CPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
      end;
    //////////////////////////////////////////////////////////////////////////////
      if T <> 4 then
      begin
        QrItens.SQL.Add('');
        QrItens.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
        QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 2, 0) -');
        QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
        QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
        QrItens.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrItens.SQL.Add('');
      end;
    end;
    //////////////////////////////////////////////////////////////////////////////

    if RGTipo.ItemIndex = 2 then QrItens.SQL.Add('UNION');

    //////////////////////////////////////////////////////////////////////////////

    if RGTipo.ItemIndex in ([1,2]) then
    begin
      QrItens.SQL.Add('SELECT loi.Controle, loi.Valor, 1 ITENS,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('(IF(loi.DDeposito > SYSDATE(), 1, 0) +');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )');
      QrItens.SQL.Add('ITENS_XXX,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('IF(loi.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0) ITENS_PgVencto,');
      QrItens.SQL.Add('IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0) +');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0) ITENS_Devolvid,');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0) ITENS_DevPgTot,');
      QrItens.SQL.Add('IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0) ITENS_DevNaoQt,');
      QrItens.SQL.Add('(loi.TotalPg - loi.TotalJr + loi.TotalDs) Pago,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), loi.Valor, 0) EXPIRADO,');
      QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), 0, loi.Valor) AEXPIRAR,');
      QrItens.SQL.Add('IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)');
      QrItens.SQL.Add('  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0) VENCIDO,');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),');
      QrItens.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0) PGDDEPOSITO,');
      QrItens.SQL.Add('IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3),');
      QrItens.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0) PGATRAZO,');
      QrItens.SQL.Add('IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),');
      QrItens.SQL.Add('  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0) PGABERTO, ');
      QrItens.SQL.Add('"D" Tipo, MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
      QrItens.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
      QrItens.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE,');
      QrItens.SQL.Add('loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Duplicata');
      QrItens.SQL.Add('FROM lot esits loi');
      QrItens.SQL.Add('LEFT JOIN lot es     lot ON lot.Codigo=loi.Codigo');
      QrItens.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
      QrItens.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
      QrItens.SQL.Add('WHERE lot.Tipo=1');
      QrItens.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkLotNeg.Checked = False then
        QrItens.SQL.Add('AND lot.Codigo > 0');
      if CkPeriodo.Checked then
        QrItens.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrItens.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
      if Trim(FGrupos) <> ''  then
          QrItens.SQL.Add('AND loi.CPF in ' + FGrupos + ')')
        else
          QrItens.SQL.Add('AND loi.CPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
      end;
      ////////////////////////////////////////////////////////////////////////////
      if T <> 4 then
      begin
        QrItens.SQL.Add('');
        QrItens.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
        QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +');
        QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )');
        QrItens.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrItens.SQL.Add('');
      end;
      ////////////////////////////////////////////////////////////////////////////
    end;
    QrItens.SQL.Add(MLAGeral.OrdemSQL3(FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3));
    QrItens. Open;
}
    QrItens.Close;
    QrItens.SQL.Clear;
    if RGTipo.ItemIndex in ([0,2]) then
    begin
      QrItens.SQL.Add('SELECT lct.FatParcela, lct.Credito, 1 ITENS,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('(IF(lct.DDeposito > SYSDATE(), 1, 0) +');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()), 2, 0) -');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
      QrItens.SQL.Add('ITENS_XXX,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('IF(lct.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()), 1, 0) -');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
      QrItens.SQL.Add('1, 0) ITENS_PgVencto,');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
      QrItens.SQL.Add('1, 0) ITENS_Devolvid,');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),');
      QrItens.SQL.Add('1, 0) ITENS_DevPgTot,');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)),');
      QrItens.SQL.Add('1, 0) ITENS_DevNaoQt,');
      QrItens.SQL.Add('(chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto) Pago,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE(), lct.Credito, 0) EXPIRADO,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE(), 0, lct.Credito) AEXPIRAR,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
      QrItens.SQL.Add('lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito), 0) VENCIDO,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,');
      QrItens.SQL.Add('lct.Credito - IF(lct.DDeposito < SYSDATE(), 0, lct.Credito)) PGDDEPOSITO,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
      QrItens.SQL.Add('chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0) PGATRAZO,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
      QrItens.SQL.Add('lct.Credito - (chd.ValPago - chd.Taxas - chd.JurosV) +');
      QrItens.SQL.Add('chd.Desconto, 0) PGABERTO, "C" Tipo,');
      QrItens.SQL.Add('MONTH(lct.DDeposito)+YEAR(lct.DDeposito)*100+0.00 PERIODO,');
      QrItens.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
      QrItens.SQL.Add('ban.Nome NOMEBANCO, lct.DDeposito, lct.Emitente NOMEEMITENTE, ');
      QrItens.SQL.Add('lct.Banco, lct.Agencia, lct.ContaCorrente, lct.Documento, lct.Duplicata');
      QrItens.SQL.Add('FROM ' + CO_TabLctA + ' lct');
      QrItens.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum');
      QrItens.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
      QrItens.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=lct.FatParcela');
      QrItens.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco');
      QrItens.SQL.Add('WHERE lct.FatID=301');
      QrItens.SQL.Add('AND lot.Tipo <> 1');
      if CkLotNeg.Checked = False then
        QrItens.SQL.Add('AND lot.Codigo > 0');
      QrItens.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkPeriodo.Checked then
        QrItens.SQL.Add(dmkPF.SQL_Periodo('AND lct.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrItens.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
      if Trim(FGrupos) <> ''  then
          QrItens.SQL.Add('AND lct.CNPJCPF in ' + FGrupos + ')')
        else
          QrItens.SQL.Add('AND lct.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
      end;
    //////////////////////////////////////////////////////////////////////////////
      if T <> 4 then
      begin
        QrItens.SQL.Add('');
        QrItens.SQL.Add('AND (IF(lct.DDeposito > SYSDATE(), 1, 0) +');
        QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()), 2, 0) -');
        QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
        QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
        QrItens.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrItens.SQL.Add('');
      end;
    end;
    //////////////////////////////////////////////////////////////////////////////

    if RGTipo.ItemIndex = 2 then QrItens.SQL.Add('UNION');

    //////////////////////////////////////////////////////////////////////////////

    if RGTipo.ItemIndex in ([1,2]) then
    begin
      QrItens.SQL.Add('SELECT lct.FatParcela, lct.Credito, 1 ITENS,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('(IF(lct.DDeposito > SYSDATE(), 1, 0) +');
      QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 2, 0) +');
      QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 4, 0) )');
      QrItens.SQL.Add('ITENS_XXX,');
      QrItens.SQL.Add('');
      QrItens.SQL.Add('IF(lct.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,');
      QrItens.SQL.Add('IF((lct.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,');
      QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 1, 0) ITENS_PgVencto,');
      QrItens.SQL.Add('IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0) +');
      QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0) ITENS_Devolvid,');
      QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 1, 0) ITENS_DevPgTot,');
      QrItens.SQL.Add('IF((lct.Quitado<2) AND (lct.DDeposito< SYSDATE()), 1, 0) ITENS_DevNaoQt,');
      QrItens.SQL.Add('(lct.TotalPg - lct.TotalJr + lct.TotalDs) Pago,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE(), lct.Credito, 0) EXPIRADO,');
      QrItens.SQL.Add('IF(lct.DDeposito < SYSDATE(), 0, lct.Credito) AEXPIRAR,');
      QrItens.SQL.Add('IF((lct.DDeposito<SYSDATE()) AND ((lct.Data3<2)');
      QrItens.SQL.Add('  OR (lct.DDeposito<lct.Data3)), lct.Credito, 0) VENCIDO,');
      QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3),');
      QrItens.SQL.Add('  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0) PGDDEPOSITO,');
      QrItens.SQL.Add('IF(/*(lct.Quitado> 1)AND*/ (lct.DDeposito< lct.Data3),');
      QrItens.SQL.Add('  lct.TotalPg - lct.TotalJr + lct.TotalDs, 0) PGATRAZO,');
      QrItens.SQL.Add('IF((lct.Quitado< 2)AND (lct.DDeposito< SYSDATE()),');
      QrItens.SQL.Add('  lct.Credito - (lct.TotalPg - lct.TotalJr) + lct.TotalDs, 0) PGABERTO, ');
      QrItens.SQL.Add('"D" Tipo, MONTH(lct.DDeposito)+YEAR(lct.DDeposito)*100+0.00 PERIODO,');
      QrItens.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
      QrItens.SQL.Add('ban.Nome NOMEBANCO, lct.DDeposito, lct.Emitente NOMEEMITENTE,');
      QrItens.SQL.Add('lct.Banco, lct.Agencia, lct.ContaCorrente, lct.Documento, lct.Duplicata');
      QrItens.SQL.Add('FROM ' + CO_TabLctA + ' lct');
      QrItens.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum');
      QrItens.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
      QrItens.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=lct.Banco');
      QrItens.SQL.Add('WHERE lct.FatID=301');
      QrItens.SQL.Add('AND lot.Tipo=1');
      QrItens.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkLotNeg.Checked = False then
        QrItens.SQL.Add('AND lot.Codigo > 0');
      if CkPeriodo.Checked then
        QrItens.SQL.Add(dmkPF.SQL_Periodo('AND lct.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrItens.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
      if Trim(FGrupos) <> ''  then
          QrItens.SQL.Add('AND lct.CNPJCPF in ' + FGrupos + ')')
        else
          QrItens.SQL.Add('AND lct.CNPJCPF="' + Geral.SoNumero_TT(EdCPF1.Text) + '"');
      end;
      ////////////////////////////////////////////////////////////////////////////
      if T <> 4 then
      begin
        QrItens.SQL.Add('');
        QrItens.SQL.Add('AND (IF(lct.DDeposito > SYSDATE(), 1, 0) +');
        QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 2, 0) +');
        QrItens.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 4, 0) )');
        QrItens.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrItens.SQL.Add('');
      end;
      ////////////////////////////////////////////////////////////////////////////
    end;
    QrItens.SQL.Add(MLAGeral.OrdemSQL3(FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3));
    UMyMod.AbreQuery(QrItens, Dmod.MyDB);

    //
    QrRevenda.Close;
    if RGTipo.ItemIndex in ([1,2]) then
    begin
{
      QrRevenda.SQL.Clear;
      QrRevenda.SQL.Add('SELECT SUM(adp.Pago-adp.Juros) Pago,');
      QrRevenda.SQL.Add('SUM((adp.Pago-adp.Juros)/ loi.Valor) Itens');
      QrRevenda.SQL.Add('FROM adup pgs adp');
      QrRevenda.SQL.Add('LEFT JOIN lot esits loi ON loi.Controle=adp.Lot es Its');
      QrRevenda.SQL.Add('LEFT JOIN lot es    lot ON lot.Codigo=loi.Codigo');
      QrRevenda.SQL.Add('WHERE adp.LotePg>0');
      //
      QrRevenda.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkLotNeg.Checked = False then
        QrRevenda.SQL.Add('AND lot.Codigo > 0');
      if CkPeriodo.Checked then
        QrRevenda.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrRevenda.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
      if Trim(FGrupos) <> ''  then
          QrRevenda.SQL.Add('AND loi.CPF in '+FGrupos+')')
        else
          QrRevenda.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
      end;
      ////////////////////////////////////////////////////////////////////////////
      if T <> 4 then
      begin
        QrRevenda.SQL.Add('');
        QrRevenda.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
        QrRevenda.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +');
        QrRevenda.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )');
        QrRevenda.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrRevenda.SQL.Add('');
      end;
      ////////////////////////////////////////////////////////////////////////////
      QrRevenda. Open;
}
      QrRevenda.SQL.Clear;
      QrRevenda.SQL.Add('SELECT SUM(adp.Credito-adp.MoraVal) Pago,');
      QrRevenda.SQL.Add('SUM((adp.Credito-adp.MoraVal)/ lct.Credito) Itens');
      QrRevenda.SQL.Add('FROM ' + CO_TabLctA + ' adp');
      QrRevenda.SQL.Add('LEFT JOIN ' + CO_TabLctA + ' lct ON lct.FatParcela=adp.FatParcRef');
      QrRevenda.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum');
      QrRevenda.SQL.Add('WHERE lct.FatID=' + TXT_VAR_FATID_0301);
      QrRevenda.SQL.Add('AND adp.FatID=' + TXT_VAR_FATID_0312);
      QrRevenda.SQL.Add('AND adp.FatNum>0');
      //
      QrRevenda.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
        IntToStr(FmPrincipal.FConnections)+' >= 0.01');
      if CkLotNeg.Checked = False then
        QrRevenda.SQL.Add('AND lot.Codigo > 0');
      if CkPeriodo.Checked then
        QrRevenda.SQL.Add(dmkPF.SQL_Periodo('AND lct.DDeposito ', TPIni.Date, TPFim.Date, True, True));
      //////////////////////////////////////////////////////////////////////////////
      if EdCliente.ValueVariant <> 0 then
        QrRevenda.SQL.Add('AND lot.Cliente=' + FormatFloat('0', EdCliente.ValueVariant));
      if EdCPF1.Text <> '' then
      begin
      if Trim(FGrupos) <> ''  then
          QrRevenda.SQL.Add('AND lct.CNPJCPF in '+FGrupos+')')
        else
          QrRevenda.SQL.Add('AND lct.CNPJCPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
      end;
      ////////////////////////////////////////////////////////////////////////////
      if T <> 4 then
      begin
        QrRevenda.SQL.Add('');
        QrRevenda.SQL.Add('AND (IF(lct.DDeposito > SYSDATE(), 1, 0) +');
        QrRevenda.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito>=lct.Data3), 2, 0) +');
        QrRevenda.SQL.Add('IF((lct.Quitado>1) AND (lct.DDeposito< lct.Data3), 4, 0) )');
        QrRevenda.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
        QrRevenda.SQL.Add('');
      end;
      ////////////////////////////////////////////////////////////////////////////
      UMyMod.AbreQuery(QrRevenda, Dmod.MyDB);
    end;
    // Para imprimir
    if Length(FGrupos) > 0 then FGrupos[1] := ' ';
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmHistCliEmi.CkPeriodoClick(Sender: TObject);
begin
  TPIni.Visible := CkPeriodo.Checked;
  TPFim.Visible := CkPeriodo.Checked;
  FechaPesquisa;
end;

procedure TFmHistCliEmi.QrItensCalcFields(DataSet: TDataSet);
begin
  if QrItensITENS_AVencer.Value = 1 then
    QrItensNOMESTATUS.Value := 'A vencer'
  else if QrItensITENS_PgVencto.Value = 1 then
    QrItensNOMESTATUS.Value := 'Quitado'
  else if QrItensITENS_DevPgTot.Value = 1 then
    QrItensNOMESTATUS.Value := 'Revendido'
  else
    QrItensNOMESTATUS.Value := 'Pendente';

  //////////////////////////////////////////////////////////////////////////////

  if QrItensTipo.Value = 'C' then QrItensDOCUM_TXT.Value := 'C '+
    FormatFloat('000', QrItensBanco.Value)+'/'+FormatFloat('0000',
    QrItensAgencia.Value)+'  '+QrItensContaCorrente.Value+' - '+
    FormatFloat('000000', QrItensDocumento.Value)
  else QrItensDOCUM_TXT.Value := 'D ' +
    FormatFloat('000', QrItensBanco.Value)+'/'+FormatFloat('0000',
    QrItensAgencia.Value)+' - '+QrItensDuplicata.Value;
end;

procedure TFmHistCliEmi.EdCPF1Change(Sender: TObject);
begin
  FechaPesquisa;
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.TPIniChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.TPIniClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.TPFimChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.TPFimClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.CkCrescenteClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.QrSomaACalcFields(DataSet: TDataSet);
var
  i: Double;
begin
  QrSomaASOMA_I_AVencer.Value  := QrSomaAITENS_AVencer.Value + QrSomaBITENS_AVencer.Value;
  QrSomaASOMA_I_Vencidos.Value := QrSomaAITENS_Vencidos.Value + QrSomaBITENS_Vencidos.Value;
  QrSomaASOMA_I_ITENS.Value := QrSomaAITENS.Value + QrSomaBITENS.Value;
  i := QrSomaASOMA_I_ITENS.Value / 100;
  if i > 0 then
  begin
    QrSomaAPERC_AVencer.Value  := QrSomaASOMA_I_AVencer.Value  / i;
    QrSomaAPERC_Vencidos.Value := QrSomaASOMA_I_Vencidos.Value / i;
  end else begin
    QrSomaAPERC_AVencer.Value  := 0;
    QrSomaAPERC_Vencidos.Value := 0;
  end;
  //
  QrSomaASOMA_I_PgVencto.Value := QrSomaAITENS_PgVencto.Value + QrSomaBITENS_PgVencto.Value;
  QrSomaASOMA_I_Devolvid.Value := QrSomaAITENS_Devolvid.Value + QrSomaBITENS_Devolvid.Value;
  i := QrSomaASOMA_I_Vencidos.Value / 100;
  if i > 0 then
  begin
    QrSomaAPERC_PgVencto.Value := (QrSomaASOMA_I_PgVencto.Value) / i;
    QrSomaAPERC_Devolvid.Value := (QrSomaASOMA_I_Devolvid.Value) / i;
  end else begin
    QrSomaAPERC_PgVencto.Value := 0;
    QrSomaAPERC_Devolvid.Value := 0;
  end;
  i := QrSomaASOMA_I_Devolvid.Value / 100;
  QrSomaASOMA_I_DevPgTot.Value := QrSomaAITENS_DevPgTot.Value + QrSomaBITENS_DevPgTot.Value;
  QrSomaASOMA_I_DevNaoQt.Value := QrSomaAITENS_DevNaoQt.Value + QrSomaBITENS_DevNaoQt.Value;
  if i > 0 then
  begin
    QrSomaAPERC_DevPgTot.Value := (QrSomaASOMA_I_DevPgTot.Value) / i;
    QrSomaAPERC_DevNaoQt.Value := (QrSomaASOMA_I_DevNaoQt.Value) / i;
  end else begin
    QrSomaAPERC_DevPgTot.Value := 0;
    QrSomaAPERC_DevNaoQt.Value := 0;
  end;
  {
  QrTudo.Close;
  QrTudo. Open;
  }
  UnDmkDAC_PF.AbreMySQLQuery0(QrTudo, Dmod.MyDB, [
  'SELECT COUNT(FatParcela) ITENS, ',
  'SUM(Credito) VALOR ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + FormatFloat('0', VAR_FATID_0301),
  '']);
  //
  if QrTudoITENS.Value > 0 then QrSomaAPERC_ITENS.Value :=
    QrSomaASOMA_I_ITENS.Value / QrTudoITENS.Value * 100
  else QrSomaAPERC_ITENS.Value := 0;
  //////////////////////////////////////////////////////////////////////////////
  QrSomaASOMA_V_Valor.Value := QrSomaAValor.Value + QrSomaBValor.Value;
  i := QrSomaASOMA_V_Valor.Value / 100;
  QrSomaASOMA_V_AEXPIRAR.Value := QrSomaAAEXPIRAR.Value + QrSomaBAEXPIRAR.Value;
  QrSomaASOMA_V_EXPIRADO.Value := QrSomaAEXPIRADO.Value + QrSomaBEXPIRADO.Value;
  if i > 0 then
  begin
    QrSomaAPERC_AExpirar.Value := QrSomaASOMA_V_AEXPIRAR.Value  / i;
    QrSomaAPERC_Expirado.Value := QrSomaASOMA_V_EXPIRADO.Value / i;
  end else begin
    QrSomaAPERC_AExpirar.Value := 0;
    QrSomaAPERC_Expirado.Value := 0;
  end;
  //
  i := QrSomaASOMA_V_EXPIRADO.Value / 100;
  QrSomaASOMA_V_VENCIDO.Value := QrSomaAVENCIDO.Value + QrSomaBVENCIDO.Value;
  QrSomaASOMA_V_PGDDEPOSITO.Value := QrSomaAPGDDEPOSITO.Value + QrSomaBPGDDEPOSITO.Value;
  if i > 0 then
  begin
    QrSomaAPERC_Vencido.Value     := QrSomaASOMA_V_VENCIDO.Value / i;
    QrSomaAPERC_PgDDeposito.Value := QrSomaASOMA_V_PGDDEPOSITO.Value / i;
  end else begin
    QrSomaAPERC_Vencido.Value     := 0;
    QrSomaAPERC_PgDDeposito.Value := 0;
  end;
  i := QrSomaASOMA_V_VENCIDO.Value / 100;
  QrSomaASOMA_V_PGATRAZO.Value := QrSomaAPGATRAZO.Value + QrSomaBPGATRAZO.Value;
  QrSomaASOMA_V_PGABERTO.Value := QrSomaAPGABERTO.Value + QrSomaBPGABERTO.Value;
  if i > 0 then
  begin
    QrSomaAPERC_Atrazo.Value := QrSomaASOMA_V_PGATRAZO.Value / i;
    QrSomaAPERC_Aberto.Value := QrSomaASOMA_V_PGABERTO.Value / i;
  end else begin
    QrSomaAPERC_Atrazo.Value := 0;
    QrSomaAPERC_Aberto.Value := 0;
  end;
  if QrTudoVALOR.Value > 0 then QrSomaAPERC_VALOR.Value :=
    QrSomaASOMA_V_Valor.Value / QrTudoVALOR.Value * 100
  else QrSomaAPERC_VALOR.Value := 0;
end;

procedure TFmHistCliEmi.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxHistorico, 'Hist�rico');
end;

procedure TFmHistCliEmi.RGTipoClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.FechaPesquisa;
begin
  QrItens.Close;
  QrSomaA.Close;
  QrSomaB.Close;
  // N�o pode!!! QrGruSacEmiIts.Close;
end;

procedure TFmHistCliEmi.DBGrid1TitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := FOrdC1;
  FOrdC1 := Column.FieldName;
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  if FOrdC1 = 'DOCUM_TXT' then
  begin
    FOrdC1 := 'Tipo, Banco, Agencia, Conta, Cheque, Duplicata';
  end else if FOrdC1 = 'NOMESTATUS' then
  begin
    FOrdC1 := 'ITENS_AVencer';
    FOrdC2 := 'ITENS_PgVencto';
    FOrdC3 := 'ITENS_DevPgTot';
  end else if FOrdC1 = 'Tipo' then
  begin
    FOrdC2 := 'DDeposito';
    FOrdC3 := 'Valor';
  end;
  if Antigo = FOrdC1 then
  FOrdS1 := MLAGeral.InverteOrdemAsc(FOrdS1);
  if FOrdC1 = 'ITENS_AVencer' then
  begin
    FOrdS2 := FOrdS1;
    FOrdS3 := FOrdS1;
  end;
  Pesquisa;
end;

procedure TFmHistCliEmi.BtGrupoClick(Sender: TObject);
begin
  FmPrincipal.CadastroGruSacEmi(QrGruSacEmiItsCodigo.Value);
  if Length(Trim(EdCPF1.Text)) > 0 then ReopenEmiSac(EdCPF1.Text);
  FechaPesquisa;
  Pesquisa;
end;

procedure TFmHistCliEmi.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then BtGrupo.Visible := True
  else BtGrupo.Visible := False;
end;

procedure TFmHistCliEmi.QrGruSacEmiItsCalcFields(DataSet: TDataSet);
begin
  QrGruSacEmiItsCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrGruSacEmiItsCNPJ_CPF.Value);
end;

procedure TFmHistCliEmi.CkLotNegClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.CkMediaDiasClick(Sender: TObject);
begin
  dmkEdMediaDias.Visible := CkMediaDias.Checked;
  FechaPesquisa;
end;

procedure TFmHistCliEmi.CkGruposClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.QrGruSacEmiItsBeforeClose(DataSet: TDataSet);
begin
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.QrGruSacEmiItsAfterOpen(DataSet: TDataSet);
begin
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.MostraCkGrupos;
var
  Ver: Boolean;
begin
  Ver := False;
  if QrGruSacEmiIts.State = dsBrowse then
    if QrGruSacEmiIts.RecordCount > 0 then
      if EdCPF1.Text <> '' then Ver := True;
  CkGrupos.Enabled := Ver;
  CkGrupos.Checked := CkGrupos.Enabled;
end;

procedure TFmHistCliEmi.QrItensBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmHistCliEmi.QrItensAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmHistCliEmi.CkAClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.CkQClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.CkRClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.CkPClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.DesativaItens;
begin
  BtImprime.Enabled := False;
  QrItens.Close;
end;

procedure TFmHistCliEmi.QrRevendaCalcFields(DataSet: TDataSet);
begin
  if QrSomaASOMA_I_DevPgTot.Value = 0 then QrRevendaItens_Perc.Value := 0 else
  QrRevendaItens_Perc.Value := QrRevendaItens.Value /
    QrSomaASOMA_I_DevPgTot.Value * 100;
  //
  if QrSomaASOMA_V_PGATRAZO.Value = 0 then QrRevendaPago_Perc.Value := 0 else
  QrRevendaPago_Perc.Value := QrRevendaPago.Value /
    QrSomaASOMA_V_PGATRAZO.Value * 100;
end;

procedure TFmHistCliEmi.frxHistoricoGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
     Value := MLAGeral.CampoReportTxt(CBCliente.Text, 'TODOS')
  else if AnsiCompareText(VarName, 'VARF_EMISAC') = 0 then
     Value := MLAGeral.CampoReportTxt(DBEdit1.Text, 'TODOS')
  else if AnsiCompareText(VarName, 'VARF_GRUPO') = 0 then
     Value := FGrupos
  else if AnsiCompareText(VarName, 'VAR_AVENCER') = 0 then
     Value := CkA.Checked
  else if AnsiCompareText(VarName, 'VAR_QUITADO') = 0 then
     Value := CkQ.Checked
  else if AnsiCompareText(VarName, 'VAR_REVENDIDO') = 0 then
     Value := CkR.Checked
  else if AnsiCompareText(VarName, 'VAR_PENDENTE') = 0 then
     Value := CkP.Checked
  else if AnsiCompareText(VarName, 'VAR_MEDIADIAS') = 0 then
     Value := CkMediaDias.Checked
end;

end.

