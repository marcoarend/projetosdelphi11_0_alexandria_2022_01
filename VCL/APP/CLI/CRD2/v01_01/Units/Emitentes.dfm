object FmEmitentes: TFmEmitentes
  Left = 332
  Top = 217
  Caption = 'EMI-TENTE-001 :: Cadastro de Emitentes'
  ClientHeight = 374
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 212
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GrEmitentes: TDBGrid
      Left = 0
      Top = 41
      Width = 784
      Height = 110
      Align = alClient
      DataSource = DsEmitPF
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 242
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF_TXT'
          Title.Caption = 'CPF'
          Width = 104
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Risco'
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      object RGOrdem1: TRadioGroup
        Left = 0
        Top = 0
        Width = 599
        Height = 41
        Align = alClient
        Caption = ' Ordem 1: '
        Columns = 3
        Items.Strings = (
          'Nome'
          'CPF / CNPJ'
          'Risco')
        TabOrder = 0
        OnClick = RGOrdem1Click
        ExplicitLeft = 1
        ExplicitTop = 1
        ExplicitWidth = 595
        ExplicitHeight = 39
      end
      object RGOrdem2: TRadioGroup
        Left = 599
        Top = 0
        Width = 185
        Height = 41
        Align = alRight
        Caption = ' Ordem 2: '
        Columns = 2
        Items.Strings = (
          'Crescente'
          'Decrescente')
        TabOrder = 1
        OnClick = RGOrdem2Click
        ExplicitLeft = 596
        ExplicitTop = 1
        ExplicitHeight = 39
      end
    end
    object StringGrid1: TStringGrid
      Left = 0
      Top = 151
      Width = 784
      Height = 61
      Align = alBottom
      ColCount = 6
      DefaultRowHeight = 18
      TabOrder = 2
      Visible = False
      ExplicitLeft = 1
      ExplicitTop = 150
      ExplicitWidth = 782
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 4
    ExplicitTop = 4
    ExplicitWidth = 630
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 582
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 534
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 278
        Height = 32
        Caption = 'Cadastro de Emitentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 278
        Height = 32
        Caption = 'Cadastro de Emitentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 278
        Height = 32
        Caption = 'Cadastro de Emitentes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 260
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 289
    ExplicitWidth = 630
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 304
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
      object BtInclui: TBitBtn
        Tag = 10
        Left = 18
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Inclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
        NumGlyphs = 2
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 142
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Altera'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
        NumGlyphs = 2
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 266
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Exclui'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
        NumGlyphs = 2
      end
    end
  end
  object DsEmitPF: TDataSource
    DataSet = QrEmitCPF
    Left = 220
    Top = 84
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmitCPFCalcFields
    SQL.Strings = (
      'SELECT * FROM emitcpf')
    Left = 192
    Top = 84
    object QrEmitCPFCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmitCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrEmitCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrEmitCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrEmitCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrEmitCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrEmitCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
end
