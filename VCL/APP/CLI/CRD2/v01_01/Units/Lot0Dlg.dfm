object FmLot0Dlg: TFmLot0Dlg
  Left = 419
  Top = 217
  Caption = 'BDR-GEREN-002 :: Impress'#245'es'
  ClientHeight = 318
  ClientWidth = 415
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object CLImpressoes: TCheckListBox
    Left = 0
    Top = 48
    Width = 275
    Height = 156
    Align = alClient
    ItemHeight = 13
    Items.Strings = (
      'Border'#244' Cliente'
      'Border'#244' Cont'#225'bil'
      'Aditivo + N.P. + A.P.'
      'Aditivo + N.P.'
      'Aditivo'
      'Nota Promiss'#243'ria'
      'Autoriza'#231#227'o de Protesto'
      'Carta ao Sacado'
      'Margem Coligadas')
    TabOrder = 0
    OnClick = CLImpressoesClick
  end
  object Panel1: TPanel
    Left = 275
    Top = 48
    Width = 140
    Height = 156
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object BtTodos: TBitBtn
      Tag = 127
      Left = 10
      Top = 8
      Width = 120
      Height = 40
      Caption = '&Todos'
      TabOrder = 0
      OnClick = BtTodosClick
      NumGlyphs = 2
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 10
      Top = 52
      Width = 120
      Height = 40
      Caption = '&Nenhum'
      TabOrder = 1
      OnClick = BtNenhumClick
      NumGlyphs = 2
    end
    object BtSalvar: TBitBtn
      Tag = 24
      Left = 10
      Top = 96
      Width = 120
      Height = 40
      Caption = 'Sal&va'
      TabOrder = 2
      OnClick = BtSalvarClick
      NumGlyphs = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 415
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 367
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 319
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 141
        Height = 32
        Caption = 'Impress'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 141
        Height = 32
        Caption = 'Impress'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 141
        Height = 32
        Caption = 'Impress'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 204
    Width = 415
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 411
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 248
    Width = 415
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 269
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 267
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        TabOrder = 0
        OnClick = BtImprimeClick
        NumGlyphs = 2
      end
      object BtPreview: TBitBtn
        Tag = 27
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Preview'
        Enabled = False
        TabOrder = 1
        OnClick = BtPreviewClick
        NumGlyphs = 2
      end
    end
  end
end
