unit Taxas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkRadioGroup, dmkCheckBox,
  dmkCheckGroup, dmkImage, dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc,
  DmkDAC_PF, UnDmkEnums;

type
  TFmTaxas = class(TForm)
    PainelDados: TPanel;
    DsTaxas: TDataSource;
    QrTaxas: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    QrTaxasCodigo: TIntegerField;
    QrTaxasNome: TWideStringField;
    QrTaxasGenero: TSmallintField;
    QrTaxasForma: TSmallintField;
    QrTaxasMostra: TSmallintField;
    QrTaxasBase: TSmallintField;
    QrTaxasAutomatico: TSmallintField;
    QrTaxasValor: TFloatField;
    QrTaxasLk: TIntegerField;
    QrTaxasDataCad: TDateField;
    QrTaxasDataAlt: TDateField;
    QrTaxasUserCad: TIntegerField;
    QrTaxasUserAlt: TIntegerField;
    QrTaxasAlterWeb: TSmallintField;
    QrTaxasAtivo: TSmallintField;
    QrTaxasFORMA_TXT: TWideStringField;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBData: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBEdit: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    EdValor: TdmkEdit;
    RGForma: TdmkRadioGroup;
    RGBase: TdmkRadioGroup;
    CGAutoma: TdmkCheckGroup;
    CGMostra: TdmkCheckGroup;
    CGGenero: TdmkCheckGroup;
    Label1: TLabel;
    Label2: TLabel;
    DBText1: TDBText;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    dmkDBCheckGroup2: TdmkDBCheckGroup;
    dmkDBCheckGroup3: TdmkDBCheckGroup;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdPlaGen: TdmkEditCB;
    Label4: TLabel;
    CBPlaGen: TdmkDBLookupComboBox;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrTaxasPlaGen: TIntegerField;
    Label5: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    QrTaxasNO_PLAGEN: TWideStringField;
    SpeedButton5: TSpeedButton;
    BtContas: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTaxasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTaxasBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrTaxasCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTaxas: TFmTaxas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Contas, MyDBCheck, MyListas, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTaxas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTaxas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTaxasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTaxas.DefParams;
begin
  VAR_GOTOTABELA := 'taxas';
  VAR_GOTOMYSQLTABLE := QrTaxas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cta.Nome NO_PLAGEN, txa.*');
  VAR_SQLx.Add('FROM taxas txa');
  VAR_SQLx.Add('LEFT JOIN contas cta ON cta.Codigo=txa.PlaGen');

  VAR_SQLx.Add('WHERE txa.Codigo > 0');
  //
  VAR_SQL1.Add('AND txa.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND txa.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND txa.Nome Like :P0');
  //
end;

procedure TFmTaxas.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant   := '';
        //
      end else begin
        EdCodigo.ValueVariant := QrTaxasCodigo.Value;
        EdNome.ValueVariant   := QrTaxasNome.Value;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTaxas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTaxas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTaxas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTaxas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTaxas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTaxas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTaxas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTaxas.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdPlaGen.ValueVariant;
  //
  FinanceiroJan.CadastroDeContas(Codigo);
  //
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdPlaGen, CBPlaGen, QrContas, VAR_CADASTRO);
end;

procedure TFmTaxas.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTaxas, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'taxas');
end;

procedure TFmTaxas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTaxasCodigo.Value;
  Close;
end;

procedure TFmTaxas.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EdPlaGen.ValueVariant = 0, EdPlaGen,
    'Defina uma conta (do plano de contas)!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('taxas', 'Codigo', ImgTipo.SQLType,
    QrTaxasCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(SQLType, FmTaxas, PainelEdita,
    'taxas', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    //
    {$IfDef DEFINE_VARLCT}
      if CO_DMKID_APP = 22 then //Credito2
      begin
        if (SQLType = stUpd) and (EdPlaGen.OldValor <> EdPlaGen.ValueVariant) then
        begin
          // 2011-12-24 Altera��o dos lan�amentos financeiros para a nova conta!
          MyObjects.Informa2(LaAviso1, LaAviso2, True,
            'Alterando lan�amentos financeiros (n�o encerrados)');
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
            'Genero'], [CO_JOKE_SQL, 'FatID_Sub'], [EdPlaGen.ValueVariant], [
            'FATID <> 0 AND FatID = ' + TXT_VAR_FATID_0303, Codigo], True);
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
        end;
      end;
    {$EndIf}
  end;
end;

procedure TFmTaxas.BtContasClick(Sender: TObject);
var
  Codigo, PlaGen, SubGrupo: Integer;
  Nome, Nome2, Credito, Debito: String;
begin
  if Geral.MensagemBox(
  'A a��o a seguir ir� criar contas novas no plano de contas, ' + #13#10 +
  'sendo criada uma conta para cada taxa sem conta definida!' + #13#10 +
  '' + #13#10 +
  'Antes de executar esta a��o tenha certeza que n�o existe cadastro ' + #13#10 +
  'de alguma conta que poderia ser usadas para alguma taxa!' + #13#10 +
  '' + #13#10 +
  'Deseja realmente criar as contas e atrel�-las �s taxas sem contas?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM taxas ',
    'WHERE PlaGen=0 ',
    'AND Codigo > 0 ',
    '']);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      if Geral.MensagemBox(
      'Foram localizadas ' + Geral.FF0(Dmod.QrAux.RecordCount) +
      ' taxas sem conta atrelada!' + #13#10 +
      '' + #13#10 +
      'Deseja realmente criar as contas e atrel�-las �s taxas sem contas?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrAux.First;
        while not Dmod.QrAux.Eof do
        begin
          Nome := Dmod.QrAux.FieldByName('Nome').AsString;
          Nome2 := Nome;
          Credito := 'V';
          Debito := 'F';
          Subgrupo := 0;
          //
          Codigo := UMyMod.BuscaEmLivreY_Def('contas', 'Codigo', stIns, 0);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contas', False, [
          'Nome', 'Nome2',
          'Subgrupo', 'Credito', 'Debito'], [
          'Codigo'], [
          Nome, Nome2,
          Subgrupo, Credito, Debito], [
          Codigo], False) then
          begin
            PlaGen := Codigo;
            Codigo := Dmod.QrAux.FieldByName('Codigo').AsInteger;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'taxas', False, [
            'PlaGen'], ['Codigo'], [PlaGen], [Codigo], False);
          end;
          //
          Dmod.QrAux.Next;
        end;
        //
        LocCod(QrTaxasCodigo.Value, QrTaxasCodigo.Value);
      end;
    end else Geral.MensagemBox(
      'N�o foram localizadas taxas sem conta atrelada!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmTaxas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'taxas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'taxas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'taxas', 'Codigo');
end;

procedure TFmTaxas.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrTaxas, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'taxas');
end;

procedure TFmTaxas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GBEdit.Align  := alClient;
  GBData.Align  := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmTaxas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTaxasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTaxas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTaxas.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTaxasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTaxas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTaxas.QrTaxasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTaxas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTaxas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTaxasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'taxas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTaxas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTaxas.QrTaxasBeforeOpen(DataSet: TDataSet);
begin
  QrTaxasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTaxas.QrTaxasCalcFields(DataSet: TDataSet);
begin
  QrTaxasFORMA_TXT.Value := MLAGeral.FormataTaxa(
    QrTaxasForma.Value, Dmod.QrControleMoeda.Value)+':';
end;

end.

