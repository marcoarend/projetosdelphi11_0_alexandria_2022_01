unit EmitentesEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, Db, mySQLDbTables, dmkEdit,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmEmitentesedit = class(TForm)
    Panel2: TPanel;
    Label1: TLabel;
    EdBanda: TdmkEdit;
    Label6: TLabel;
    EdComp: TdmkEdit;
    EdBanco: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdAgencia: TdmkEdit;
    Label4: TLabel;
    EdConta: TdmkEdit;
    Label5: TLabel;
    EdCheque: TdmkEdit;
    Label9: TLabel;
    EdCPF: TdmkEdit;
    Label10: TLabel;
    EdEmitente: TdmkEdit;
    EdRisco: TdmkEdit;
    Label7: TLabel;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    EdCMC_7: TdmkEdit;
    Label11: TLabel;
    EdRealCC: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    DsBanco: TDataSource;
    procedure EdBandaChange(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure ReopenBanco;
    procedure EdCMC_7Change(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimpaEdits;
    procedure LocalizaEmitente;
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
  public
    { Public declarations }
    FLaTipo: String;
    FIncluiu: Boolean;
  end;

var
  FmEmitentesedit: TFmEmitentesedit;

implementation

uses UnMyObjects, Principal, UnMLAGeral, UnInternalConsts, Emitentes, Module, ModuleLot2,
  UMySQLModule;

{$R *.DFM}

procedure TFmEmitentesedit.EdBandaChange(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmEmitentesedit.EdBancoExit(Sender: TObject);
begin
  ReopenBanco;
end;

procedure TFmEmitentesedit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then LimpaEdits;
end;

procedure TFmEmitentesedit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitentesedit.BtOKClick(Sender: TObject);
begin
  DmLot2.AtualizaEmitente(EdBanco.Text, EdAgencia.Text, EdConta.Text,
    EdCPF.Text, EdEmitente.Text, Geral.DMV(EdRisco.Text));
  FmEmitentes.ReopenEmitentes(EdBanco.Text+EdAgencia.Text+EdConta.Text);
end;

procedure TFmEmitentesedit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitentesedit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitentesedit.LimpaEdits;
begin
  Edbanda.Text   := '';
  EdComp.Text    := '000';
  EdBanco.Text   := '000';
  EdConta.Text   := '0000000000';
  EdAgencia.Text := '0000';
  EdCheque.Text  := '000000';
  //
  EdCPF.Text     := '';
  EdEmitente.Text := '';
  EdBanda.SetFocus;
end;

procedure TFmEmitentesedit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdRisco.Text := Geral.FFT(Dmod.QrControleCHRisco.Value, 2, siPositivo);
end;

procedure TFmEmitentesedit.ReopenBanco;
begin
  QrBanco.Close;
  QrBanco.Params[0].AsString := EdBanco.Text;
  UMyMod.AbreQuery(QrBanco, Dmod.MyDB);
end;

procedure TFmEmitentesedit.EdBancoChange(Sender: TObject);
begin
  if not EdBanco.Focused then ReopenBanco;
end;

procedure TFmEmitentesedit.EdCMC_7Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7.Text;
    EdComp.Text    := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    LocalizaEmitente;
    EdCPF.SetFocus;
  end;
end;

procedure TFmEmitentesEdit.LocalizaEmitente;
var
  B,A,C: String;
begin
  if  (Geral.IMV(EdBanco.Text) > 0)
  and (Geral.IMV(EdAgencia.Text) > 0)
  and (Geral.DMV(EdConta.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      B := EdBanco.Text;
      A := EdAgencia.Text;
      C := EdConta.Text;
      DmLot2.QrLocEmiBAC.Close;
      DmLot2.QrLocEmiBAC.Params[0].AsInteger := DmLot2.QrBancoDVCC.Value;
      DmLot2.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,DmLot2.QrBancoDVCC.Value);
      UMyMod.AbreQuery(DmLot2.QrLocEmiBAC, Dmod.MyDB);
      //
      if DmLot2.QrLocEmiBAC.RecordCount = 0 then
      begin
        EdEmitente.Text := '';
        EdCPF.Text := '';
      end else begin
        DmLot2.QrLocEmiCPF.Close;
        DmLot2.QrLocEmiCPF.Params[0].AsString := DmLot2.QrLocEmiBACCPF.Value;
        UMyMod.AbreQuery(DmLot2.QrLocEmiCPF, Dmod.MyDB);
        //
        if DmLot2.QrLocEmiCPF.RecordCount = 0 then
        begin
          EdEmitente.Text := '';
          EdCPF.Text := '';
        end else begin
          EdEmitente.Text := DmLot2.QrLocEmiCPFNome.Value;
          EdCPF.Text      := Geral.FormataCNPJ_TT(DmLot2.QrLocEmiCPFCPF.Value);
        end;
      end;
      Screen.Cursor := crDefault;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

function TFmEmitentesEdit.CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
begin
  EdRealCC.Text := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+EdRealCC.Text;
end;

end.
