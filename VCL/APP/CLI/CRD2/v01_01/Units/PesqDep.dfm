object FmPesqDep: TFmPesqDep
  Left = 274
  Top = 150
  Caption = 'CHQ-DEPOS-005 :: Gerencia Recebimentos'
  ClientHeight = 292
  ClientWidth = 466
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 466
    Height = 69
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 466
      Height = 76
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Valor: TLabel
        Left = 5
        Top = 20
        Width = 24
        Height = 13
        Caption = 'Valor'
      end
    end
    object CkPeriodo: TCheckBox
      Left = 92
      Top = 16
      Width = 177
      Height = 17
      Caption = 'Per'#237'odo dep'#243'sito (in'#237'cio - fim):'
      TabOrder = 1
      OnClick = CkPeriodoClick
    end
    object TPIniDep: TDateTimePicker
      Left = 92
      Top = 38
      Width = 90
      Height = 21
      CalColors.TextColor = clMenuText
      Date = 37636.777157974500000000
      Time = 37636.777157974500000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = TPIniDepClick
      OnChange = TPIniDepChange
    end
    object TPFimDep: TDateTimePicker
      Left = 188
      Top = 38
      Width = 90
      Height = 21
      Date = 37636.777203761600000000
      Time = 37636.777203761600000000
      TabOrder = 3
      OnClick = TPIniDepClick
      OnChange = TPIniDepChange
    end
    object dmkEdValor: TdmkEdit
      Left = 6
      Top = 38
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Credito'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = dmkEdValorChange
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 466
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 418
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 370
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 292
        Height = 32
        Caption = 'Gerencia Recebimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 292
        Height = 32
        Caption = 'Gerencia Recebimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 292
        Height = 32
        Caption = 'Gerencia Recebimentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 178
    Width = 466
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 462
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 222
    Width = 466
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 320
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 318
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesq: TBitBtn
        Tag = 22
        Left = 6
        Top = 5
        Width = 120
        Height = 40
        Caption = '&Pesquisar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesqClick
      end
      object BtImprimir: TBitBtn
        Tag = 5
        Left = 130
        Top = 5
        Width = 120
        Height = 40
        Caption = '&Imprimir'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimirClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 165
    Width = 466
    Height = 13
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
  end
  object CGDepositado: TdmkCheckGroup
    Left = 0
    Top = 117
    Width = 466
    Height = 48
    Align = alTop
    Caption = ' Status de dep'#243'sito: '
    Columns = 3
    Items.Strings = (
      'N'#227'o depositado'
      'Depositado Manualmente'
      'Depositado via TED')
    TabOrder = 5
    OnClick = dmkEdValorChange
    UpdType = utYes
    Value = 0
    OldValor = 0
  end
  object QrDepIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrDepItsAfterOpen
    BeforeClose = QrDepItsBeforeClose
    OnCalcFields = QrDepItsCalcFields
    SQL.Strings = (
      
        'SELECT li.FatNum, lo.Cliente, li.Banco, li.Agencia, li.ContaCorr' +
        'ente, '
      'li.Documento, lo.NF, li.Emitente, li.CNPJCPF, li.Vencimento, '
      'li.DDeposito, li.Credito, li.Descricao '
      'FROM lct0001a li '
      'LEFT JOIN lot0001a lo ON lo.Codigo = li.FatNum '
      'WHERE lo.TxCompra + lo.ValValorem + 1 >= 0.01 '
      'AND li.Depositado = 1 '
      'AND li.DDeposito BETWEEN "2011-12-01" AND "2011-12-31"'
      ''
      ''
      'ORDER BY li.Vencimento, li.Descricao')
    Left = 361
    Top = 73
    object QrDepItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrDepItsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrDepItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDepItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrDepItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrDepItsContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrDepItsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDepItsNF: TIntegerField
      FieldName = 'NF'
    end
    object QrDepItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrDepItsCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrDepItsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrDepItsDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrDepItsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrDepItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsDepIts: TDataSource
    DataSet = QrDepIts
    Left = 389
    Top = 73
  end
  object frxDepIts: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 39766.436549479200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxDepItsGetValue
    Left = 305
    Top = 73
    Datasets = <
      item
        DataSet = frxDsDepIts
        DataSetName = 'frxDsDepIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 22.299212350000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo43: TfrxMemoView
          Left = 10.204731000000000000
          Top = 1.133859000000001000
          Width = 755.906000000000000000
          Height = 18.897635350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito de cheques')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 34.007936260000010000
        Top = 185.196970000000000000
        Width = 718.110700000000000000
        DataSet = frxDsDepIts
        DataSetName = 'frxDsDepIts'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 48.755902829999990000
          Width = 22.677165350000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsDepIts."Banco">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 71.433075500000000000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Agencia'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Agencia"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 101.669295980000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."ContaCorrente"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 162.141736920000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsDepIts."Documento">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 241.511815660000000000
          Width = 207.873991340000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emitente'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDepIts."Emitente"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 449.385807000000000000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDepIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 645.921240070000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Credito'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDepIts."Credito"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 551.433051090000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Vencimento"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 600.566909360000000000
          Width = 45.354330710000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'DDeposito'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."DDeposito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 199.937012510000000000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsDepIts."NF">)]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 10.960637000000000000
          Top = 0.000051259999992226
          Width = 37.795273150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Cliente"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 10.960637000000000000
          Top = 17.007885000000020000
          Width = 60.472453150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 71.433117000000000000
          Top = 17.007885000000020000
          Width = 634.961013150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDepIts."Descricao"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 60.850433000000000000
        Top = 64.252010000000000000
        Width = 718.110700000000000000
        object Memo15: TfrxMemoView
          Left = 48.755902830000000000
          Top = 43.842547999999990000
          Width = 22.677165350000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 71.433075500000000000
          Top = 43.842547999999990000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 101.669295980000000000
          Top = 43.842547999999990000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 162.141736920000000000
          Top = 43.842547999999990000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheq.')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 241.511815660000000000
          Top = 43.842547999999990000
          Width = 207.873991340000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 449.385807000000000000
          Top = 43.842547999999990000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 645.921240070000000000
          Top = 43.842547999999990000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 551.433051090000000000
          Top = 43.842547999999990000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 600.566909360000000000
          Top = 43.842547999999990000
          Width = 45.354330710000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 199.937012510000000000
          Top = 43.842547999999990000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 10.960637000000000000
          Top = 43.842599260000000000
          Width = 37.795273150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 11.338590000000000000
          Top = 1.629932000000000000
          Width = 755.906000000000000000
          Height = 39.685050350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo de: [VARF_PERIODO]   Status: [VARF_NO_STATUS] ')
          ParentFont = False
          VAlign = vaBottom
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 279.685220000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 549.921615000000000000
          Width = 158.299258000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina[Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsDepIts: TfrxDBDataset
    UserName = 'frxDsDepIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CPF_TXT=CPF_TXT'
      'FatNum=FatNum'
      'Cliente=Cliente'
      'Banco=Banco'
      'Agencia=Agencia'
      'ContaCorrente=ContaCorrente'
      'Documento=Documento'
      'NF=NF'
      'Emitente=Emitente'
      'CNPJCPF=CNPJCPF'
      'Vencimento=Vencimento'
      'DDeposito=DDeposito'
      'Credito=Credito'
      'Descricao=Descricao')
    DataSource = DsDepIts
    BCDToCurrency = False
    Left = 333
    Top = 73
  end
end
