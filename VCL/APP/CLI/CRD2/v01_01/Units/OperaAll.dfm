object FmOperaAll: TFmOperaAll
  Left = 393
  Top = 177
  Caption = 'OPE-GEREN-001 :: Gerencia Resultados'
  ClientHeight = 532
  ClientWidth = 935
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 935
    Height = 45
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 232
      Height = 45
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 125
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object Label34: TLabel
        Left = 19
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object TPFim: TDateTimePicker
        Left = 125
        Top = 20
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 1
      end
      object TPIni: TDateTimePicker
        Left = 19
        Top = 20
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 0
      end
    end
    object RGModelo: TRadioGroup
      Left = 232
      Top = 0
      Width = 150
      Height = 45
      Align = alLeft
      Caption = ' Modelo'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Modelo A'
        'Modelo B')
      TabOrder = 1
      OnClick = RGModeloClick
    end
    object RGOrdem1: TRadioGroup
      Left = 382
      Top = 0
      Width = 110
      Height = 45
      Align = alLeft
      Caption = ' Ordem A: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Data'
        'M'#234's')
      TabOrder = 2
    end
    object RGOrdem2: TRadioGroup
      Left = 492
      Top = 0
      Width = 110
      Height = 45
      Align = alLeft
      Caption = ' Ordem B: '
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Data'
        'M'#234's')
      TabOrder = 3
    end
    object RGSintetico: TRadioGroup
      Left = 602
      Top = 0
      Width = 150
      Height = 45
      Align = alLeft
      Caption = ' Tipo: '
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Anal'#237'tico'
        'Sint'#233'tico')
      TabOrder = 4
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 93
    Width = 935
    Height = 311
    Align = alClient
    DataSource = DsLReA
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'NomeMes'
        Title.Caption = 'M'#234's'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NomeCli'
        Title.Caption = 'Cliente'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Lote'
        Title.Caption = 'Border'#244
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QtdeDocs'
        Title.Caption = 'QDcs'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MediaDoc'
        Title.Caption = 'M'#233'dia/Doc'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Total'
        Title.Caption = 'V.Negoc.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TTC'
        Title.Caption = 'F.Compra'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TVV'
        Title.Caption = 'Ad Valorem'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TTC_J'
        Title.Caption = 'Tx.per'#237'odo'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Dias'
        Title.Caption = 'Prz.m'#233'dio'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TTC_T'
        Title.Caption = 'Tx.m'#234's'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Impostos'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TRC_T'
        Title.Caption = 'Taxa real'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IOF_TOTAL'
        Title.Caption = 'IOF'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CHDevPg'
        Title.Caption = 'CH Dev.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DUDevPg'
        Title.Caption = 'DU Dev.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal01'
        Title.Caption = 'Coluna 01'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal02'
        Title.Caption = 'Coluna 02'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal03'
        Title.Caption = 'Coluna 03'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal04'
        Title.Caption = 'Coluna 04'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal05'
        Title.Caption = 'Coluna 05'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal06'
        Title.Caption = 'Coluna 06'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal07'
        Title.Caption = 'Coluna 07'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal08'
        Title.Caption = 'Coluna 08'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal09'
        Title.Caption = 'Coluna 09'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal10'
        Title.Caption = 'Coluna 10'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal11'
        Title.Caption = 'Coluna 11'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ColVal12'
        Title.Caption = 'Coluna 12'
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 935
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 887
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 839
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 255
        Height = 32
        Caption = 'Gerencia Resultados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 255
        Height = 32
        Caption = 'Gerencia Resultados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 255
        Height = 32
        Caption = 'Gerencia Resultados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 404
    Width = 935
    Height = 58
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 931
      Height = 41
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 24
        Width = 931
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 462
    Width = 935
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 789
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 787
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCalcula: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Calcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCalculaClick
      end
      object BtResultado: TBitBtn
        Tag = 5
        Left = 139
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Vizualiza'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtResultadoClick
      end
    end
  end
  object QrOper1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE'
      'FROM lot es lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'ORDER BY lo.Data, NOMECLIENTE, lo.Lote')
    Left = 68
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOper1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOper1Tipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrOper1Spread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrOper1Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOper1Lote: TIntegerField
      FieldName = 'Lote'
      Required = True
    end
    object QrOper1Data: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrOper1Total: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrOper1Dias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrOper1PeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrOper1TxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
    end
    object QrOper1ValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrOper1AdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrOper1IOC: TFloatField
      FieldName = 'IOC'
      Required = True
    end
    object QrOper1IOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrOper1Tarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrOper1CPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
    end
    object QrOper1CPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
    end
    object QrOper1TipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrOper1IRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrOper1IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrOper1ISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrOper1ISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrOper1PIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrOper1PIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrOper1PIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrOper1PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrOper1COFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrOper1COFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrOper1COFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrOper1COFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
    object QrOper1OcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrOper1MaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrOper1CHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
    end
    object QrOper1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOper1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOper1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOper1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOper1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOper1MINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrOper1MINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrOper1MINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrOper1PIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
    end
    object QrOper1COFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
    end
    object QrOper1PgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
    end
    object QrOper1DUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
    end
    object QrOper1NF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrOper1Itens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrOper1Conferido: TIntegerField
      FieldName = 'Conferido'
      Required = True
    end
    object QrOper1ECartaSac: TSmallintField
      FieldName = 'ECartaSac'
      Required = True
    end
    object QrOper1CBE: TIntegerField
      FieldName = 'CBE'
      Required = True
    end
    object QrOper1NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrOper1IOFd: TFloatField
      FieldName = 'IOFd'
    end
    object QrOper1IOFd_VAL: TFloatField
      FieldName = 'IOFd_VAL'
    end
    object QrOper1IOFv: TFloatField
      FieldName = 'IOFv'
    end
    object QrOper1IOFv_VAL: TFloatField
      FieldName = 'IOFv_VAL'
    end
  end
  object QrLoteCHDevP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (SUM(Juros) - SUM(Desco)) Juros'
      'FROM alin pgs '
      'WHERE LotePG=:P0')
    Left = 96
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoteCHDevPJuros: TFloatField
      FieldName = 'Juros'
    end
  end
  object QrLoteDUDevP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (SUM(Juros) - SUM(Desco)) Juros'
      'FROM adup pgs '
      'WHERE LotePg=:P0')
    Left = 125
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoteDUDevPJuros: TFloatField
      FieldName = 'Juros'
    end
  end
  object QrEmLo1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT el.Codigo, el.ValValorem, el.TaxaVal '
      'FROM emlot el'
      'LEFT JOIN creditoX.lot es lo'
      'ON lo.Codigo=el.Codigo'
      'WHERE lo.data BETWEEN :P0 AND :P1')
    Left = 153
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmLo1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmLo1ValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrEmLo1TaxaVal: TFloatField
      FieldName = 'TaxaVal'
      Required = True
    end
  end
  object QrLReA: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrLReABeforeClose
    AfterScroll = QrLReAAfterScroll
    OnCalcFields = QrLReACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM __2_lrea'
      'ORDER BY Data, NomeCli, Lote')
    Left = 125
    Top = 253
    object QrLReACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLReAData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLReANomeCli: TWideStringField
      FieldName = 'NomeCli'
      Size = 100
    end
    object QrLReALote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000;-000000; '
    end
    object QrLReANF: TIntegerField
      FieldName = 'NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrLReATotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATC0: TFloatField
      FieldName = 'TC0'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAVV0: TFloatField
      FieldName = 'VV0'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAETC: TFloatField
      FieldName = 'ETC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAEVV: TFloatField
      FieldName = 'EVV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATTC: TFloatField
      FieldName = 'TTC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATVV: TFloatField
      FieldName = 'TVV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReADias: TFloatField
      FieldName = 'Dias'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATTC_T: TFloatField
      FieldName = 'TTC_T'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATTC_J: TFloatField
      FieldName = 'TTC_J'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAALL_T: TFloatField
      FieldName = 'ALL_T'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAALL_J: TFloatField
      FieldName = 'ALL_J'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAddVal: TFloatField
      FieldName = 'ddVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReANomeMes: TWideStringField
      FieldName = 'NomeMes'
      Size = 30
    end
    object QrLReAMes: TIntegerField
      FieldName = 'Mes'
    end
    object QrLReAQtdeDocs: TIntegerField
      FieldName = 'QtdeDocs'
    end
    object QrLReAMediaDoc: TFloatField
      FieldName = 'MediaDoc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrLReATC1: TFloatField
      FieldName = 'TC1'
    end
    object QrLReATC2: TFloatField
      FieldName = 'TC2'
    end
    object QrLReATC3: TFloatField
      FieldName = 'TC3'
    end
    object QrLReATC4: TFloatField
      FieldName = 'TC4'
    end
    object QrLReATC5: TFloatField
      FieldName = 'TC5'
    end
    object QrLReATC6: TFloatField
      FieldName = 'TC6'
    end
    object QrLReAVV1: TFloatField
      FieldName = 'VV1'
    end
    object QrLReAVV2: TFloatField
      FieldName = 'VV2'
    end
    object QrLReAVV3: TFloatField
      FieldName = 'VV3'
    end
    object QrLReAVV4: TFloatField
      FieldName = 'VV4'
    end
    object QrLReAVV5: TFloatField
      FieldName = 'VV5'
    end
    object QrLReAVV6: TFloatField
      FieldName = 'VV6'
    end
    object QrLReAISS_Val: TFloatField
      FieldName = 'ISS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReACOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAImpostos: TFloatField
      FieldName = 'Impostos'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATRC_T: TFloatField
      FieldName = 'TRC_T'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATRC_J: TFloatField
      FieldName = 'TRC_J'
    end
    object QrLReAddLiq: TFloatField
      FieldName = 'ddLiq'
    end
    object QrLReAIOF_TOTAL: TFloatField
      FieldName = 'IOF_TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATarifas: TFloatField
      FieldName = 'Tarifas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAPgOcorren: TFloatField
      FieldName = 'PgOcorren'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReACHDevPg: TFloatField
      FieldName = 'CHDevPg'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReADUDevPg: TFloatField
      FieldName = 'DUDevPg'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColCod01: TIntegerField
      FieldName = 'ColCod01'
    end
    object QrLReAColCod02: TIntegerField
      FieldName = 'ColCod02'
    end
    object QrLReAColCod03: TIntegerField
      FieldName = 'ColCod03'
    end
    object QrLReAColCod04: TIntegerField
      FieldName = 'ColCod04'
    end
    object QrLReAColCod05: TIntegerField
      FieldName = 'ColCod05'
    end
    object QrLReAColCod06: TIntegerField
      FieldName = 'ColCod06'
    end
    object QrLReAColCod07: TIntegerField
      FieldName = 'ColCod07'
    end
    object QrLReAColCod08: TIntegerField
      FieldName = 'ColCod08'
    end
    object QrLReAColCod09: TIntegerField
      FieldName = 'ColCod09'
    end
    object QrLReAColCod10: TIntegerField
      FieldName = 'ColCod10'
    end
    object QrLReAColNom01: TWideStringField
      FieldName = 'ColNom01'
      Size = 50
    end
    object QrLReAColNom02: TWideStringField
      FieldName = 'ColNom02'
      Size = 50
    end
    object QrLReAColNom03: TWideStringField
      FieldName = 'ColNom03'
      Size = 50
    end
    object QrLReAColNom04: TWideStringField
      FieldName = 'ColNom04'
      Size = 50
    end
    object QrLReAColNom05: TWideStringField
      FieldName = 'ColNom05'
      Size = 50
    end
    object QrLReAColNom06: TWideStringField
      FieldName = 'ColNom06'
      Size = 50
    end
    object QrLReAColNom07: TWideStringField
      FieldName = 'ColNom07'
      Size = 50
    end
    object QrLReAColNom08: TWideStringField
      FieldName = 'ColNom08'
      Size = 50
    end
    object QrLReAColNom09: TWideStringField
      FieldName = 'ColNom09'
      Size = 50
    end
    object QrLReAColNom10: TWideStringField
      FieldName = 'ColNom10'
      Size = 50
    end
    object QrLReAColVal01: TFloatField
      FieldName = 'ColVal01'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal02: TFloatField
      FieldName = 'ColVal02'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal03: TFloatField
      FieldName = 'ColVal03'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal04: TFloatField
      FieldName = 'ColVal04'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal05: TFloatField
      FieldName = 'ColVal05'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal06: TFloatField
      FieldName = 'ColVal06'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal07: TFloatField
      FieldName = 'ColVal07'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal08: TFloatField
      FieldName = 'ColVal08'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal09: TFloatField
      FieldName = 'ColVal09'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal10: TFloatField
      FieldName = 'ColVal10'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColCod11: TIntegerField
      FieldName = 'ColCod11'
    end
    object QrLReAColCod12: TIntegerField
      FieldName = 'ColCod12'
    end
    object QrLReAColNom11: TWideStringField
      FieldName = 'ColNom11'
      Size = 50
    end
    object QrLReAColNom12: TWideStringField
      FieldName = 'ColNom12'
      Size = 50
    end
    object QrLReAColVal11: TFloatField
      FieldName = 'ColVal11'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColVal12: TFloatField
      FieldName = 'ColVal12'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAColTip01: TSmallintField
      FieldName = 'ColTip01'
    end
    object QrLReAColTip02: TSmallintField
      FieldName = 'ColTip02'
    end
    object QrLReAColTip03: TSmallintField
      FieldName = 'ColTip03'
    end
    object QrLReAColTip04: TSmallintField
      FieldName = 'ColTip04'
    end
    object QrLReAColTip05: TSmallintField
      FieldName = 'ColTip05'
    end
    object QrLReAColTip06: TSmallintField
      FieldName = 'ColTip06'
    end
    object QrLReAColTip07: TSmallintField
      FieldName = 'ColTip07'
    end
    object QrLReAColTip08: TSmallintField
      FieldName = 'ColTip08'
    end
    object QrLReAColTip09: TSmallintField
      FieldName = 'ColTip09'
    end
    object QrLReAColTip10: TSmallintField
      FieldName = 'ColTip10'
    end
    object QrLReAColTip11: TSmallintField
      FieldName = 'ColTip11'
    end
    object QrLReAColTip12: TSmallintField
      FieldName = 'ColTip12'
    end
  end
  object DsLReA: TDataSource
    DataSet = QrLReA
    Left = 153
    Top = 253
  end
  object QrOcoPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pg.Data, pg.Juros'
      'FROM ocor rpg pg'
      'WHERE pg.LotePG = 0'
      'AND Data=:P0')
    Left = 96
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcoPgData: TDateField
      FieldName = 'Data'
    end
    object QrOcoPgJuros: TFloatField
      FieldName = 'Juros'
    end
  end
  object QrChqPgs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Juros'
      'FROM alin pgs'
      'WHERE LotePG=0'
      'AND Data=:P0')
    Left = 124
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChqPgsData: TDateField
      FieldName = 'Data'
    end
    object QrChqPgsJuros: TFloatField
      FieldName = 'Juros'
    end
  end
  object QrDupPgs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Juros'
      'FROM adup pgs'
      'WHERE LotePG=0'
      'AND Data=:P0')
    Left = 153
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDupPgsData: TDateField
      FieldName = 'Data'
    end
    object QrDupPgsJuros: TFloatField
      FieldName = 'Juros'
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 123
    Top = 281
  end
  object QrLoteOcPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros'
      'FROM ocor rpg '
      'WHERE LotePg=:P0')
    Left = 96
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoteOcPgJuros: TFloatField
      FieldName = 'Juros'
    end
  end
  object QrLocCol: TmySQLQuery
    Database = Dmod.MyDB
    Left = 94
    Top = 281
  end
  object QrLocData: TmySQLQuery
    Database = Dmod.MyDB
    Left = 65
    Top = 281
  end
  object QrColSBor: TmySQLQuery
    Database = Dmod.MyDB
    Left = 153
    Top = 281
  end
  object frxOper3: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.568873333300000000
    ReportOptions.LastChange = 40687.658731620370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub1Total , Sub1TTC   , Sub1TRC   ,'
      '  Sub2Dias  , Sub2Total , Sub2TTC   , Sub2TRC   ,'
      '  Dias      , Total     , TTC       , TRC       ,'
      '  TotalDias , TotalTTC  , TotalTRC  , TotalTotal: Extended;'
      '    '
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TotalDias  := 0;'
      '  TotalTotal := 0;'
      '  TotalTTC   := 0;'
      '  TotalTRC   := 0;  '
      'end;'
      ''
      'procedure DadosMestre1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Sub2Dias  := Sub2Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub2Total := Sub2Total + <frxDsLReA."Total">;'
      '  Sub2TTC   := Sub2TTC   + <frxDsLReA."TTC">;'
      
        '  Sub2TRC   := Sub2TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  Sub1Dias  := Sub1Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub1Total := Sub1Total + <frxDsLReA."Total">;'
      '  Sub1TTC   := Sub1TTC   + <frxDsLReA."TTC">;'
      
        '  Sub1TRC   := Sub1TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  TotalDias  := TotalDias  + <frxDsLReA."Dias"> * <frxDsLReA."To' +
        'tal">;'
      '  TotalTotal := TotalTotal + <frxDsLReA."Total">;'
      '  TotalTTC   := TotalTTC   + <frxDsLReA."TTC">;'
      
        '  TotalTRC   := TotalTRC   + <frxDsLReA."TTC"> - <frxDsLReA."Imp' +
        'ostos">;    '
      'end;'
      ''
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  Sub1TRC   := 0;'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      '  Sub2TRC   := 0;'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub1.Text  := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TTC);'
      
        '  MemoSub1L.Text := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TRC);'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub2.Text  := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TTC);'
      
        '  MemoSub2L.Text := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TRC);  '
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSubT.Text  := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTTC);'
      
        '  MemoSubTL.Text := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTRC);'
      'end;'
      ''
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      '  //Colunas vari'#225'veis'
      '  //Coluna 01      '
      '  Memo5.Visible   := <VAR_COLUNA01>; //T'#237'tulo'
      
        '  Memo100.Visible := <VAR_COLUNA01>; //Valor                    ' +
        '                                              '
      
        '  Memo112.Visible := <VAR_COLUNA01>; //Total 1                  ' +
        '                                                             '
      
        '  Memo124.Visible := <VAR_COLUNA01>; //Total 2                  ' +
        '                                                                '
      '  Memo136.Visible := <VAR_COLUNA01>; //Total 3'
      '  //Coluna 02      '
      '  Memo33.Visible  := <VAR_COLUNA02>;'
      
        '  Memo101.Visible := <VAR_COLUNA02>;                            ' +
        '                                   '
      
        '  Memo113.Visible := <VAR_COLUNA02>;                            ' +
        '                                                   '
      
        '  Memo125.Visible := <VAR_COLUNA02>;                            ' +
        '                                                       '
      
        '  Memo137.Visible := <VAR_COLUNA02>;                            ' +
        '                                                          '
      '  //Coluna 03'
      '  Memo82.Visible  := <VAR_COLUNA03>;'
      
        '  Memo102.Visible := <VAR_COLUNA03>;                            ' +
        '                  '
      
        '  Memo114.Visible := <VAR_COLUNA03>;                            ' +
        '                                                   '
      
        '  Memo126.Visible := <VAR_COLUNA03>;                            ' +
        '                                                       '
      
        '  Memo138.Visible := <VAR_COLUNA03>;                            ' +
        '                                                          '
      '  //Coluna 04'
      '  Memo83.Visible  := <VAR_COLUNA04>;'
      
        '  Memo103.Visible := <VAR_COLUNA04>;                            ' +
        '                    '
      
        '  Memo115.Visible := <VAR_COLUNA04>;                            ' +
        '                                                   '
      
        '  Memo127.Visible := <VAR_COLUNA04>;                            ' +
        '                                                       '
      
        '  Memo139.Visible := <VAR_COLUNA04>;                            ' +
        '                                                          '
      '  //Coluna 05'
      '  Memo86.Visible  := <VAR_COLUNA05>;'
      
        '  Memo104.Visible := <VAR_COLUNA05>;                            ' +
        '                   '
      
        '  Memo116.Visible := <VAR_COLUNA05>;                            ' +
        '                                                   '
      
        '  Memo128.Visible := <VAR_COLUNA05>;                            ' +
        '                                                       '
      
        '  Memo140.Visible := <VAR_COLUNA05>;                            ' +
        '                                                          '
      '  //Coluna 06'
      '  Memo87.Visible  := <VAR_COLUNA06>;'
      
        '  Memo105.Visible := <VAR_COLUNA06>;                            ' +
        '                     '
      
        '  Memo117.Visible := <VAR_COLUNA06>;                            ' +
        '                                                   '
      
        '  Memo129.Visible := <VAR_COLUNA06>;                            ' +
        '                                                       '
      
        '  Memo141.Visible := <VAR_COLUNA06>;                            ' +
        '                                                          '
      '  //Coluna 07'
      '  Memo88.Visible  := <VAR_COLUNA07>;'
      
        '  Memo106.Visible := <VAR_COLUNA07>;                            ' +
        '                   '
      
        '  Memo118.Visible := <VAR_COLUNA07>;                            ' +
        '                                                   '
      
        '  Memo130.Visible := <VAR_COLUNA07>;                            ' +
        '                                                       '
      
        '  Memo142.Visible := <VAR_COLUNA07>;                            ' +
        '                                                          '
      '  //Coluna 08'
      '  Memo89.Visible  := <VAR_COLUNA08>;'
      
        '  Memo107.Visible := <VAR_COLUNA08>;                            ' +
        '                      '
      
        '  Memo119.Visible := <VAR_COLUNA08>;                            ' +
        '                                                   '
      
        '  Memo131.Visible := <VAR_COLUNA08>;                            ' +
        '                                                       '
      
        '  Memo143.Visible := <VAR_COLUNA08>;                            ' +
        '                                                          '
      '  //Coluna 09'
      '  Memo92.Visible  := <VAR_COLUNA09>;'
      
        '  Memo108.Visible := <VAR_COLUNA09>;                            ' +
        '               '
      
        '  Memo120.Visible := <VAR_COLUNA09>;                            ' +
        '                                                   '
      
        '  Memo132.Visible := <VAR_COLUNA09>;                            ' +
        '                                                       '
      
        '  Memo144.Visible := <VAR_COLUNA09>;                            ' +
        '                                                          '
      '  //Coluna 10'
      '  Memo95.Visible  := <VAR_COLUNA10>;'
      
        '  Memo109.Visible := <VAR_COLUNA10>;                            ' +
        '                 '
      
        '  Memo121.Visible := <VAR_COLUNA10>;                            ' +
        '                                                   '
      
        '  Memo133.Visible := <VAR_COLUNA10>;                            ' +
        '                                                       '
      
        '  Memo145.Visible := <VAR_COLUNA10>;                            ' +
        '                                                          '
      '  //Coluna 11'
      '  Memo98.Visible  := <VAR_COLUNA11>;'
      
        '  Memo110.Visible := <VAR_COLUNA11>;                            ' +
        '                     '
      
        '  Memo122.Visible := <VAR_COLUNA11>;                            ' +
        '                                                   '
      
        '  Memo134.Visible := <VAR_COLUNA11>;                            ' +
        '                                                       '
      
        '  Memo146.Visible := <VAR_COLUNA11>;                            ' +
        '                                                          '
      '  //Coluna 12'
      '  Memo99.Visible  := <VAR_COLUNA12>;'
      
        '  Memo111.Visible := <VAR_COLUNA12>;                            ' +
        '             '
      
        '  Memo123.Visible := <VAR_COLUNA12>;                            ' +
        '                                                   '
      
        '  Memo135.Visible := <VAR_COLUNA12>;                            ' +
        '                                                       '
      
        '  Memo147.Visible := <VAR_COLUNA12>;                            ' +
        '                                                          '
      'end.')
    OnGetValue = frxOper3GetValue
    OnUserFunction = frxOper3UserFunction
    Left = 154
    Top = 168
    Datasets = <
      item
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 456.993625000000000000
      PaperHeight = 209.999791670000000000
      PaperSize = 256
      LeftMargin = 19.999854166666670000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object ReportTitle1: TfrxReportTitle
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Width = 1613.858658270000000000
          Height = 17.007874020000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 17.102350000000000000
          Width = 1613.858658270000000000
          Height = 26.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 540.472790000000000000
        Width = 1613.831042265208000000
        object Memo53: TfrxMemoView
          Top = 6.172850000000000000
          Width = 1613.858658270000000000
          Height = 18.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[PAGE#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 79.023622050000000000
        Top = 98.267780000000000000
        Width = 1613.831042265208000000
        Stretched = True
        object Memo19: TfrxMemoView
          Top = 1.732220000000000000
          Width = 1613.858658270000000000
          Height = 22.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 56.692913390000000000
          Top = 51.023622050000000000
          Width = 120.944874570000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 211.653535980000000000
          Top = 51.023622050000000000
          Width = 56.692913390000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 351.496055670000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 51.023622050000000000
          Width = 56.692913390000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data oper.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 30.236220470000000000
          Width = 1613.858658270000000000
          Height = 18.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 177.637787950000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 411.968496610000000000
          Top = 51.023622050000000000
          Width = 52.913385830000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 464.881882440000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 551.811016300000000000
          Top = 51.023622050000000000
          Width = 37.795275590000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prazo'
            'M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 589.606291890000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 514.015740710000000000
          Top = 51.023622050000000000
          Width = 37.795275590000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Taxa'
            'Per'#237'odo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 302.362197400000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia'
            'Doc R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 268.346449370000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 211.653535980000000000
          Top = 65.007874020000000000
          Width = 56.692913390000000000
          Height = 14.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'R$/Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 177.637787950000000000
          Top = 65.007874020000000000
          Width = 34.015748030000000000
          Height = 14.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 623.622039920000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Impostos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 672.755898190000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'Real')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 706.772110000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IOF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 767.244590000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CH Dev.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 827.717070000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DU Dev.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 888.000000000000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom01"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 948.472440940000000000
          Top = 51.023622050000000000
          Width = 60.661417320000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom02"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 1009.133858270000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom03"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 1069.606299210000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom04"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 1130.078740160000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom05"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 1190.551181100000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom06"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 1251.023622050000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom07"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 1311.496062990000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom08"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 1371.968503940000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom09"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 1432.440944880000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom10"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 1492.913385830000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom11"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          Left = 1553.385826770000000000
          Top = 51.000000000000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."ColNom12"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 321.260050000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 56.692913390000000000
          Width = 120.944874570000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLReA."NomeCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 211.653535980000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsLReA."NF">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 351.496055670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 411.968496610000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 464.881882440000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TVV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 177.637787950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Lote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 551.811016300000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Dias"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 589.606291890000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 514.015740710000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_J"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 302.362197400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."MediaDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 268.346449370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."QtdeDocs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 623.622039920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Impostos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 672.755898190000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TRC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."IOF_TOTAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."CHDevPg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."DUDevPg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 888.188976380000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'ColVal01'
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal01"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 948.661417322834900000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal02"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 1009.133858267720000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal03"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 1069.606299210000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal04"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          Left = 1130.078740160000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal05"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          Left = 1190.551181100000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal06"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 1251.023622047240000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal07"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 1311.496062992130000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal08"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 1371.968503940000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal09"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          Left = 1432.440944880000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal10"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 1492.913385830000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal11"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          Left = 1553.385826771650000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsLReA
          DataSetName = 'frxDsLReA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."ColVal12"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897637800000000000
        Top = 498.897960000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Memo36: TfrxMemoView
          Width = 177.637787950000000000
          Height = 18.897637800000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 351.496055670000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 411.968496610000000000
          Width = 52.913385830000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 464.881882440000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 551.811016300000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubT: TfrxMemoView
          Left = 589.606291890000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 514.015740710000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 302.362197400000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 268.346449370000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 211.653535980000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 177.637787950000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 623.622039920000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubTL: TfrxMemoView
          Left = 672.755898190000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal01">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal02">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          Left = 1009.134510000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal03">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          Left = 1069.606990000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal04">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          Left = 1130.079470000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal05">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          Left = 1190.551950000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal06">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo142: TfrxMemoView
          Left = 1251.024430000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal07">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          Left = 1311.496910000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal08">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          Left = 1371.969390000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal09">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          Left = 1432.441870000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal10">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          Left = 1492.914350000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal11">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          Left = 1553.386830000000000000
          Width = 60.472440940000000000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal12">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLReA."NomeCli"'
        object Memo24: TfrxMemoView
          Width = 1009.134485590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLReA."NomeMes"'
        object Memo30: TfrxMemoView
          Left = 34.015770000000000000
          Width = 975.118715590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 362.834880000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo25: TfrxMemoView
          Left = 34.015770000000000000
          Top = 3.779530000000000000
          Width = 143.622039920000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 351.496055670000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 411.968496610000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 464.881882440000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 551.811016300000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2: TfrxMemoView
          Left = 589.606291890000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 514.015740710000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 302.362197400000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 268.346449370000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 211.653535980000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 177.637787950000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 623.622039920000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2L: TfrxMemoView
          Left = 672.755898190000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 827.717070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 888.189550000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal01">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          Left = 948.662030000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal02">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Left = 1009.134510000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal03">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 1069.606990000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal04">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          Left = 1130.079470000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal05">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Left = 1190.551950000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal06">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          Left = 1251.024430000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal07">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          Left = 1311.496910000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal08">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          Left = 1371.969390000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal09">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          Left = 1432.441870000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal10">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          Left = 1492.914350000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal11">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          Left = 1553.386830000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal12">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 411.968770000000000000
        Width = 1613.831042265208000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo27: TfrxMemoView
          Top = 3.558750000000000000
          Width = 177.637787950000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 351.496055670000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 411.968496610000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 464.881882440000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 551.811016300000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1: TfrxMemoView
          Left = 589.606291890000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 514.015740710000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 302.362197400000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 268.346449370000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 211.653535980000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 177.637787950000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 623.622039920000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1L: TfrxMemoView
          Left = 672.755898190000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 827.717070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 888.189550000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal01">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          Left = 948.662030000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal02">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          Left = 1009.134510000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal03">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          Left = 1069.606990000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal04">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          Left = 1130.079470000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal05">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo129: TfrxMemoView
          Left = 1190.551950000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal06">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          Left = 1251.024430000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal07">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo131: TfrxMemoView
          Left = 1311.496910000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal08">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo132: TfrxMemoView
          Left = 1371.969390000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal09">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo133: TfrxMemoView
          Left = 1432.441870000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal10">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          Left = 1492.914350000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal11">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo135: TfrxMemoView
          Left = 1553.386830000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ColVal12">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsLReA: TfrxDBDataset
    UserName = 'frxDsLReA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Data=Data'
      'NomeCli=NomeCli'
      'Lote=Lote'
      'NF=NF'
      'Total=Total'
      'TC0=TC0'
      'VV0=VV0'
      'ETC=ETC'
      'EVV=EVV'
      'TTC=TTC'
      'TVV=TVV'
      'Dias=Dias'
      'TTC_T=TTC_T'
      'TTC_J=TTC_J'
      'ALL_T=ALL_T'
      'ALL_J=ALL_J'
      'ddVal=ddVal'
      'NomeMes=NomeMes'
      'Mes=Mes'
      'QtdeDocs=QtdeDocs'
      'MediaDoc=MediaDoc'
      'ITEM=ITEM'
      'TC1=TC1'
      'TC2=TC2'
      'TC3=TC3'
      'TC4=TC4'
      'TC5=TC5'
      'TC6=TC6'
      'VV1=VV1'
      'VV2=VV2'
      'VV3=VV3'
      'VV4=VV4'
      'VV5=VV5'
      'VV6=VV6'
      'ISS_Val=ISS_Val'
      'PIS_Val=PIS_Val'
      'COFINS_Val=COFINS_Val'
      'Impostos=Impostos'
      'TRC_T=TRC_T'
      'TRC_J=TRC_J'
      'ddLiq=ddLiq'
      'IOF_TOTAL=IOF_TOTAL'
      'Tarifas=Tarifas'
      'PgOcorren=PgOcorren'
      'CHDevPg=CHDevPg'
      'DUDevPg=DUDevPg'
      'ColCod01=ColCod01'
      'ColCod02=ColCod02'
      'ColCod03=ColCod03'
      'ColCod04=ColCod04'
      'ColCod05=ColCod05'
      'ColCod06=ColCod06'
      'ColCod07=ColCod07'
      'ColCod08=ColCod08'
      'ColCod09=ColCod09'
      'ColCod10=ColCod10'
      'ColNom01=ColNom01'
      'ColNom02=ColNom02'
      'ColNom03=ColNom03'
      'ColNom04=ColNom04'
      'ColNom05=ColNom05'
      'ColNom06=ColNom06'
      'ColNom07=ColNom07'
      'ColNom08=ColNom08'
      'ColNom09=ColNom09'
      'ColNom10=ColNom10'
      'ColVal01=ColVal01'
      'ColVal02=ColVal02'
      'ColVal03=ColVal03'
      'ColVal04=ColVal04'
      'ColVal05=ColVal05'
      'ColVal06=ColVal06'
      'ColVal07=ColVal07'
      'ColVal08=ColVal08'
      'ColVal09=ColVal09'
      'ColVal10=ColVal10'
      'ColCod11=ColCod11'
      'ColCod12=ColCod12'
      'ColNom11=ColNom11'
      'ColNom12=ColNom12'
      'ColVal11=ColVal11'
      'ColVal12=ColVal12'
      'ColTip01=ColTip01'
      'ColTip02=ColTip02'
      'ColTip03=ColTip03'
      'ColTip04=ColTip04'
      'ColTip05=ColTip05'
      'ColTip06=ColTip06'
      'ColTip07=ColTip07'
      'ColTip08=ColTip08'
      'ColTip09=ColTip09'
      'ColTip10=ColTip10'
      'ColTip11=ColTip11'
      'ColTip12=ColTip12')
    DataSet = QrLReA
    BCDToCurrency = False
    Left = 181
    Top = 253
  end
  object frxOperaAll: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 182
    Top = 167
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxOper1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.568873333300000000
    ReportOptions.LastChange = 40604.650761226850000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub1Total , Sub1TTC   , Sub1TRC   ,'
      '  Sub2Dias  , Sub2Total , Sub2TTC   , Sub2TRC   ,'
      '  Dias      , Total     , TTC       , TRC       ,'
      '  TotalDias , TotalTTC  , TotalTRC  , TotalTotal: Extended;'
      '    '
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TotalDias  := 0;'
      '  TotalTotal := 0;'
      '  TotalTTC   := 0;'
      '  TotalTRC   := 0;  '
      'end;'
      ''
      'procedure DadosMestre1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Sub2Dias  := Sub2Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub2Total := Sub2Total + <frxDsLReA."Total">;'
      '  Sub2TTC   := Sub2TTC   + <frxDsLReA."TTC">;'
      
        '  Sub2TRC   := Sub2TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  Sub1Dias  := Sub1Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub1Total := Sub1Total + <frxDsLReA."Total">;'
      '  Sub1TTC   := Sub1TTC   + <frxDsLReA."TTC">;'
      
        '  Sub1TRC   := Sub1TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  TotalDias  := TotalDias  + <frxDsLReA."Dias"> * <frxDsLReA."To' +
        'tal">;'
      '  TotalTotal := TotalTotal + <frxDsLReA."Total">;'
      '  TotalTTC   := TotalTTC   + <frxDsLReA."TTC">;'
      
        '  TotalTRC   := TotalTRC   + <frxDsLReA."TTC"> - <frxDsLReA."Imp' +
        'ostos">;    '
      'end;'
      ''
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  Sub1TRC   := 0;'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      '  Sub2TRC   := 0;'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub1.Text  := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TTC);'
      
        '  MemoSub1L.Text := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TRC);'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub2.Text  := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TTC);'
      
        '  MemoSub2L.Text := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TRC);  '
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSubT.Text  := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTTC);'
      
        '  MemoSubTL.Text := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTRC);'
      'end;'
      ''
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      'end.')
    OnGetValue = frxOper1GetValue
    OnUserFunction = frxOper1UserFunction
    Left = 140
    Top = 336
    Datasets = <
      item
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Left = 844.724409450000000000
          Width = 164.409448820000000000
          Height = 17.007874015748030000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 17.102350000000000000
          Width = 1009.133858270000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 540.472790000000000000
        Width = 1009.134510000000000000
        object Memo31: TfrxMemoView
          Left = 877.023810000000000000
          Top = 6.172850000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 965.023810000000000000
          Top = 6.172850000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 79.023622050000000000
        Top = 98.267780000000000000
        Width = 1009.134510000000000000
        object Memo19: TfrxMemoView
          Top = 1.732220000000000000
          Width = 1009.133858267717000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 56.692913390000000000
          Top = 51.023622050000000000
          Width = 120.944874570000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 211.653535980000000000
          Top = 51.023622050000000000
          Width = 56.692913390000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 351.496055670000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 51.023622050000000000
          Width = 56.692913390000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data oper.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 820.362400000000000000
          Top = 29.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 772.362400000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 177.637787950000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 411.968496610000000000
          Top = 51.023622050000000000
          Width = 52.913385830000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 464.881882440000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 551.811016300000000000
          Top = 51.023622050000000000
          Width = 37.795275590000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prazo'
            'M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 589.606291890000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 514.015740710000000000
          Top = 51.023622050000000000
          Width = 37.795275590000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Taxa'
            'Per'#237'odo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 302.362197400000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia'
            'Doc R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 268.346449370000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 211.653535980000000000
          Top = 65.007874020000000000
          Width = 56.692913390000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'R$/Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 177.637787950000000000
          Top = 65.007874020000000000
          Width = 34.015748030000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 623.622039920000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Impostos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 672.755898190000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'Real')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 706.772110000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IOF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 767.244590000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tarifas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 827.717070000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncias')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 888.189550000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CH Dev.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 948.662030000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DU Dev.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 321.260050000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 56.692913390000000000
          Width = 120.944874570000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLReA."NomeCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 211.653535980000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsLReA."NF">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 351.496055670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 411.968496610000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 464.881882440000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TVV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 177.637787950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Lote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 551.811016300000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Dias"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 589.606291890000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 514.015740710000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_J"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 302.362197400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."MediaDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 268.346449370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."QtdeDocs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 623.622039920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Impostos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 672.755898190000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TRC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."IOF_TOTAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Tarifas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."PgOcorren"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."CHDevPg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."DUDevPg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897637800000000000
        Top = 498.897960000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Memo36: TfrxMemoView
          Width = 177.637787950000000000
          Height = 18.897637800000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 351.496055670000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 411.968496610000000000
          Width = 52.913385830000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 464.881882440000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 551.811016300000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubT: TfrxMemoView
          Left = 589.606291890000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 514.015740710000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 302.362197400000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 268.346449370000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 211.653535980000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 177.637787950000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 623.622039920000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubTL: TfrxMemoView
          Left = 672.755898190000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Tarifas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."PgOcorren">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLReA."NomeCli"'
        object Memo24: TfrxMemoView
          Width = 1009.134485590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLReA."NomeMes"'
        object Memo30: TfrxMemoView
          Left = 34.015770000000000000
          Width = 975.118715590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 362.834880000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo25: TfrxMemoView
          Left = 34.015770000000000000
          Top = 3.779530000000000000
          Width = 143.622039920000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 351.496055670000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 411.968496610000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 464.881882440000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 551.811016300000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2: TfrxMemoView
          Left = 589.606291890000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 514.015740710000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 302.362197400000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 268.346449370000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 211.653535980000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 177.637787950000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 623.622039920000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2L: TfrxMemoView
          Left = 672.755898190000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Tarifas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 827.717070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."PgOcorren">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 888.189550000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 948.662030000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 411.968770000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo27: TfrxMemoView
          Top = 3.558750000000000000
          Width = 177.637787950000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 351.496055670000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 411.968496610000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 464.881882440000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 551.811016300000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1: TfrxMemoView
          Left = 589.606291890000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 514.015740710000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 302.362197400000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 268.346449370000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 211.653535980000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 177.637787950000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 623.622039920000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1L: TfrxMemoView
          Left = 672.755898190000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Tarifas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 827.717070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."PgOcorren">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 888.189550000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 948.662030000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxOper2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.568873333300000000
    ReportOptions.LastChange = 40672.691838553240000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub1Total , Sub1TTC   , Sub1TRC   ,'
      '  Sub2Dias  , Sub2Total , Sub2TTC   , Sub2TRC   ,'
      '  Dias      , Total     , TTC       , TRC       ,'
      '  TotalDias , TotalTTC  , TotalTRC  , TotalTotal: Extended;'
      '    '
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TotalDias  := 0;'
      '  TotalTotal := 0;'
      '  TotalTTC   := 0;'
      '  TotalTRC   := 0;  '
      'end;'
      ''
      'procedure DadosMestre1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Sub2Dias  := Sub2Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub2Total := Sub2Total + <frxDsLReA."Total">;'
      '  Sub2TTC   := Sub2TTC   + <frxDsLReA."TTC">;'
      
        '  Sub2TRC   := Sub2TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  Sub1Dias  := Sub1Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub1Total := Sub1Total + <frxDsLReA."Total">;'
      '  Sub1TTC   := Sub1TTC   + <frxDsLReA."TTC">;'
      
        '  Sub1TRC   := Sub1TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  TotalDias  := TotalDias  + <frxDsLReA."Dias"> * <frxDsLReA."To' +
        'tal">;'
      '  TotalTotal := TotalTotal + <frxDsLReA."Total">;'
      '  TotalTTC   := TotalTTC   + <frxDsLReA."TTC">;'
      
        '  TotalTRC   := TotalTRC   + <frxDsLReA."TTC"> - <frxDsLReA."Imp' +
        'ostos">;    '
      'end;'
      ''
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  Sub1TRC   := 0;'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      '  Sub2TRC   := 0;'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub1.Text  := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TTC);'
      
        '  MemoSub1L.Text := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TRC);'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub2.Text  := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TTC);'
      
        '  MemoSub2L.Text := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TRC);  '
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSubT.Text  := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTTC);'
      
        '  MemoSubTL.Text := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTRC);'
      'end;'
      ''
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      'end.')
    OnGetValue = frxOper1GetValue
    OnUserFunction = frxOper1UserFunction
    Left = 168
    Top = 336
    Datasets = <
      item
        DataSet = frxDsCHDevP
        DataSetName = 'frxDsCHDevP'
      end
      item
        DataSet = frxDsCHDevPSL
        DataSetName = 'frxDsCHDevPSL'
      end
      item
        DataSet = frxDsDPago
        DataSetName = 'frxDsDPago'
      end
      item
        DataSet = frxDsDPagoSL
        DataSetName = 'frxDsDPagoSL'
      end
      item
        DataSet = frxDsLotTxs
        DataSetName = 'frxDsLotTxs'
      end
      item
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOcorP
        DataSetName = 'frxDsOcorP'
      end
      item
        DataSet = frxDsOcorPSL
        DataSetName = 'frxDsOcorPSL'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object ReportTitle1: TfrxReportTitle
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Left = 844.724409450000000000
          Width = 164.409448820000000000
          Height = 17.007874015748000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 17.102350000000000000
          Width = 1009.133858270000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 79.023622050000000000
        Top = 98.267780000000000000
        Width = 1009.134510000000000000
        object Memo19: TfrxMemoView
          Top = 1.732220000000000000
          Width = 1009.133858267720000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 56.692913390000000000
          Top = 51.023622050000000000
          Width = 120.944874570000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 211.653535980000000000
          Top = 51.023622050000000000
          Width = 56.692913390000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 351.496055670000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 51.023622050000000000
          Width = 56.692913390000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data oper.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 820.362400000000000000
          Top = 29.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 772.362400000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 177.637787950000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 411.968496610000000000
          Top = 51.023622050000000000
          Width = 52.913385830000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 464.881882440000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 551.811016300000000000
          Top = 51.023622050000000000
          Width = 37.795275590000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prazo'
            'M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 589.606291890000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 514.015740710000000000
          Top = 51.023622050000000000
          Width = 37.795275590000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Taxa'
            'Per'#237'odo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 302.362197400000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia'
            'Doc R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 268.346449370000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 211.653535980000000000
          Top = 65.007874020000000000
          Width = 56.692913390000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'R$/Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 177.637787950000000000
          Top = 65.007874020000000000
          Width = 34.015748030000000000
          Height = 14.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 623.622039920000000000
          Top = 51.023622050000000000
          Width = 49.133858270000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Impostos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 672.755898190000000000
          Top = 51.023622050000000000
          Width = 34.015748030000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'Real')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 706.772110000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IOF')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 767.244590000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tarifas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 827.717070000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncias')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 888.189550000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CH Dev.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 948.662030000000000000
          Top = 51.023622050000000000
          Width = 60.472440940000000000
          Height = 28.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DU Dev.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 321.260050000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 56.692913390000000000
          Width = 120.944874570000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLReA."NomeCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 211.653535980000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsLReA."NF">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 351.496055670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 411.968496610000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 464.881882440000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TVV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 177.637787950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Lote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 551.811016300000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Dias"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 589.606291890000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 514.015740710000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_J"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 302.362197400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."MediaDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 268.346449370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."QtdeDocs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 623.622039920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Impostos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 672.755898190000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TRC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."IOF_TOTAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Tarifas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."PgOcorren"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."CHDevPg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."DUDevPg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLReA."NomeCli"'
        object Memo24: TfrxMemoView
          Width = 1009.134485590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLReA."NomeMes"'
        object Memo30: TfrxMemoView
          Left = 34.015770000000000000
          Width = 975.118715590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        Height = 17.000000000000000000
        Top = 404.409710000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsLotTxs
        DataSetName = 'frxDsLotTxs'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo98: TfrxMemoView
          Left = 56.692950000000000000
          Width = 559.370354570000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLotTxs."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          Left = 616.063390000000000000
          Width = 26.456688030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'TAXA_STR'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLotTxs."TAXA_STR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 642.520100000000000000
          Width = 41.574808030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'TaxaTxa'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLotTxs."TaxaTxa"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 684.094930000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CALCQtd'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLotTxs."CALCQtd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'TaxaVal'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLotTxs."TaxaVal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 362.834880000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsLotTxs."Codigo"'
        object Memo99: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tarifas')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 56.692950000000000000
          Width = 559.370325280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 616.063390000000000000
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '% / R')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 642.520100000000000000
          Width = 41.574808030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Taxa')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 684.094930000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Qtd.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Top = 445.984540000000000000
        Width = 1009.134510000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 468.661720000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsOcorP."LotePg"'
        object Memo109: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncias')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 56.692950000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TD')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 147.401670000000000000
          Width = 472.441135280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Taxa $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData2: TfrxDetailData
        Height = 17.000000000000000000
        Top = 510.236550000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsOcorP
        DataSetName = 'frxDsOcorP'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo117: TfrxMemoView
          Left = 56.692950000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'TIPODOC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsOcorP."TIPODOC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'DOCUM_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsOcorP."DOCUM_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          Left = 147.401670000000000000
          Width = 472.441164570000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'NOMEOCORRENCIA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsOcorP."NOMEOCORRENCIA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Valor'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorP."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Juros'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorP."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorP."Pago"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Top = 551.811380000000000000
        Width = 1009.134510000000000000
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 574.488560000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsCHDevP."LotePG"'
        object Memo123: TfrxMemoView
          Left = 56.692950000000000000
          Width = 260.787455280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 317.480520000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          Left = 438.425480000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          Left = 472.441250000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          Left = 506.457020000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          Left = 563.149970000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo129: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo131: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CH Dev.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          Left = 767.244590000000000000
          Width = 120.944920940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Top = 657.638220000000000000
        Width = 1009.134510000000000000
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 1281.260670000000000000
        Width = 1009.134510000000000000
        object Memo31: TfrxMemoView
          Left = 877.023810000000000000
          Top = 6.172850000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 965.023810000000000000
          Top = 6.172850000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TotalPages#]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897637800000000000
        Top = 1239.685840000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Memo57: TfrxMemoView
          Left = 351.496055670000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 411.968496610000000000
          Width = 52.913385830000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 464.881882440000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 551.811016300000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubT: TfrxMemoView
          Left = 589.606291890000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 514.015740710000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 302.362197400000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 268.346449370000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 211.653535980000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 623.622039920000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubTL: TfrxMemoView
          Left = 672.755898190000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 706.772110000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Tarifas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."PgOcorren">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Width = 177.637787950000000000
          Height = 18.897637800000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 177.637787950000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 1152.756650000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo28: TfrxMemoView
          Left = 351.496055670000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 411.968496610000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 464.881882440000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 551.811016300000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2: TfrxMemoView
          Left = 589.606291890000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 514.015740710000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 302.362197400000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 268.346449370000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 211.653535980000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 623.622039920000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2L: TfrxMemoView
          Left = 672.755898190000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Tarifas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 827.717070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."PgOcorren">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 888.189550000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 948.662030000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 34.015770000000000000
          Top = 3.779530000000000000
          Width = 143.622039920000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 177.637787950000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 1103.622760000000000000
        Width = 1009.134510000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo2: TfrxMemoView
          Left = 351.496055670000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 411.968496610000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 464.881882440000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 551.811016300000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1: TfrxMemoView
          Left = 589.606291890000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 514.015740710000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 302.362197400000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 268.346449370000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 211.653535980000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 623.622039920000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1L: TfrxMemoView
          Left = 672.755898190000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."IOF_TOTAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 767.244590000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Tarifas">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 827.717070000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."PgOcorren">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 888.189550000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."CHDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 948.662030000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."DUDevPg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Top = 3.558750000000000000
          Width = 177.637787950000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 177.637787950000000000
          Top = 3.779530000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData3: TfrxDetailData
        Height = 17.000000000000000000
        Top = 616.063390000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsCHDevP
        DataSetName = 'frxDsCHDevP'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo133: TfrxMemoView
          Left = 56.692950000000000000
          Width = 260.787455280000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emitente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevP."Emitente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          Left = 317.480520000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevP."CPF_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo135: TfrxMemoView
          Left = 438.425480000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Banco'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevP."Banco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo136: TfrxMemoView
          Left = 472.441250000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Agencia'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevP."Agencia"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          Left = 506.457020000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Conta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevP."Conta"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          Left = 563.149970000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Cheque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevP."Cheque"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Valor'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCHDevP."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Juros'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCHDevP."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCHDevP."Pago"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 680.315400000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsDPago."LotePG"'
        object Memo142: TfrxMemoView
          Left = 113.385900000000000000
          Width = 385.511945280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          Left = 498.897960000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '& Desconto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo160: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo132: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DU Dev.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          Left = 767.244590000000000000
          Width = 181.417354570000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Top = 763.465060000000000000
        Width = 1009.134510000000000000
      end
      object DetailData4: TfrxDetailData
        Height = 17.000000000000000000
        Top = 721.890230000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsDPago
        DataSetName = 'frxDsDPago'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo151: TfrxMemoView
          Left = 113.385900000000000000
          Width = 385.511945280000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emitente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDPago."Emitente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo152: TfrxMemoView
          Left = 498.897960000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDPago."CPF_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo157: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Desco'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDPago."Desco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo158: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Juros'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDPago."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo159: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDPago."Pago"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo161: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Duplicata'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDPago."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader6: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 786.142240000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsOcorPSL."Data"'
        object Memo166: TfrxMemoView
          Left = 56.692950000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TD')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo167: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo168: TfrxMemoView
          Left = 147.401670000000000000
          Width = 472.441135280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo169: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo170: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Taxa $')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo171: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo165: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncias')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData6: TfrxDetailData
        Height = 17.000000000000000000
        Top = 827.717070000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsOcorPSL
        DataSetName = 'frxDsOcorPSL'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo172: TfrxMemoView
          Left = 56.692950000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'TIPODOC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsOcorPSL."TIPODOC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo173: TfrxMemoView
          Left = 90.708720000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'DOCUM_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsOcorPSL."DOCUM_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo174: TfrxMemoView
          Left = 147.401670000000000000
          Width = 472.441164570000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'NOMEOCORRENCIA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsOcorPSL."NOMEOCORRENCIA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo175: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Valor'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorPSL."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo176: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Juros'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorPSL."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo177: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorPSL."Pago"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter6: TfrxGroupFooter
        Top = 869.291900000000000000
        Width = 1009.134510000000000000
      end
      object GroupHeader7: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 891.969080000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsCHDevPSL."Data"'
        object Memo179: TfrxMemoView
          Left = 56.692950000000000000
          Width = 260.787455280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo180: TfrxMemoView
          Left = 317.480520000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo181: TfrxMemoView
          Left = 438.425480000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo182: TfrxMemoView
          Left = 472.441250000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo183: TfrxMemoView
          Left = 506.457020000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo184: TfrxMemoView
          Left = 563.149970000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo185: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo186: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo187: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo178: TfrxMemoView
          Left = 888.189550000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CH Dev.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          Left = 767.244590000000000000
          Width = 120.944920940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter7: TfrxGroupFooter
        Top = 975.118740000000000000
        Width = 1009.134510000000000000
      end
      object DetailData7: TfrxDetailData
        Height = 17.000000000000000000
        Top = 933.543910000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsCHDevPSL
        DataSetName = 'frxDsCHDevPSL'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo188: TfrxMemoView
          Left = 56.692950000000000000
          Width = 260.787455280000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emitente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Emitente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo189: TfrxMemoView
          Left = 317.480520000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevPSL."CPF_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo190: TfrxMemoView
          Left = 438.425480000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Banco'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Banco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo191: TfrxMemoView
          Left = 472.441250000000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Agencia'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Agencia"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          Left = 506.457020000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Conta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Conta"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          Left = 563.149970000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Cheque'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Cheque"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Valor'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Juros'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCHDevPSL."Pago"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader8: TfrxGroupHeader
        Height = 17.000000000000000000
        Top = 997.795920000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsDPagoSL."Data"'
        object Memo198: TfrxMemoView
          Left = 113.385900000000000000
          Width = 385.511945280000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Left = 498.897960000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo200: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '& Desconto')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo201: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo202: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo203: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Left = 948.662030000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DU Dev.')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          Left = 767.244590000000000000
          Width = 181.417400940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Color = 14803425
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter8: TfrxGroupFooter
        Top = 1080.945580000000000000
        Width = 1009.134510000000000000
      end
      object DetailData8: TfrxDetailData
        Height = 17.000000000000000000
        Top = 1039.370750000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsDPagoSL
        DataSetName = 'frxDsDPagoSL'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo204: TfrxMemoView
          Left = 113.385900000000000000
          Width = 385.511945280000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emitente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDPagoSL."Emitente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo205: TfrxMemoView
          Left = 498.897960000000000000
          Width = 120.944881890000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDPagoSL."CPF_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo206: TfrxMemoView
          Left = 668.976810000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Desco'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDPagoSL."Desco"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo207: TfrxMemoView
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Juros'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDPagoSL."Juros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo208: TfrxMemoView
          Left = 718.110700000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Pago'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDPagoSL."Pago"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo209: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Duplicata'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDPagoSL."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsLotTxs: TfrxDBDataset
    UserName = 'frxDsLotTxs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nome=Nome'
      'Codigo=Codigo'
      'Controle=Controle'
      'TaxaCod=TaxaCod'
      'TaxaTxa=TaxaTxa'
      'TaxaVal=TaxaVal'
      'TaxaQtd=TaxaQtd'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'TAXA_STR=TAXA_STR'
      'Forma=Forma'
      'SysAQtd=SysAQtd'
      'CALCQtd=CALCQtd'
      'TaxaValNEG=TaxaValNEG')
    DataSet = QrLotTxs
    BCDToCurrency = False
    Left = 340
    Top = 244
  end
  object QrLotTxs: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotTxsCalcFields
    SQL.Strings = (
      'SELECT tx.Nome, lt.* '
      'FROM lct0001a lt'
      'LEFT JOIN taxas tx ON tx.Codigo=lt.FatID_Sub'
      'WHERE lt.FatID_Sub=:P0')
    Left = 312
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotTxsTAXA_STR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TAXA_STR'
      Size = 5
      Calculated = True
    end
    object QrLotTxsCALCQtd: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALCQtd'
      Calculated = True
    end
    object QrLotTxsTaxaValNEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TaxaValNEG'
      Calculated = True
    end
    object QrLotTxsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrLotTxsTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrLotTxsMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLotTxsMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrLotTxsMoraTxa: TFloatField
      FieldName = 'MoraTxa'
    end
    object QrLotTxsQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object frxDsOcorP: TfrxDBDataset
    UserName = 'frxDsOcorP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DOCUM_TXT=DOCUM_TXT'
      'Banco=Banco'
      'Agencia=Agencia'
      'Conta=Conta'
      'Cheque=Cheque'
      'Duplicata=Duplicata'
      'Tipo=Tipo'
      'TIPODOC=TIPODOC'
      'NOMEOCORRENCIA=NOMEOCORRENCIA'
      'Codigo=Codigo'
      'Ocorreu=Ocorreu'
      'Data=Data'
      'Juros=Juros'
      'Pago=Pago'
      'LotePg=LotePg'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Ocorrencia=Ocorrencia'
      'Valor=Valor'
      'DataO=DataO'
      'SALDO=SALDO'
      'Emitente=Emitente'
      'CPF=CPF'
      'CPF_TXT=CPF_TXT'
      'Descri=Descri'
      'PagoNeg=PagoNeg'
      'SALDONEG=SALDONEG')
    DataSet = QrOcorP
    BCDToCurrency = False
    Left = 396
    Top = 244
  end
  object QrOcorP: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcorPCalcFields
    SQL.Strings = (
      'SELECT oc.DataO, oc.Ocorrencia, oc.Valor,  oc.Descri,'
      '(oc.Valor + oc.TaxaV - oc.Pago) SALDO,'
      'oc.Banco, oc.Agencia, oc.Conta, oc.Cheque,'
      'oc.Duplicata, oc.Emitente, oc.CPF,'
      'ELT(oc.TpOcor, "CH", "DU", "CL", "??") TIPODOC,'
      'ob.Nome NOMEOCORRENCIA, (op.Credito - op.Debito) PAGO'
      'FROM lct0001a op'
      'LEFT JOIN ocorreu  oc ON oc.Codigo=op.Ocorreu'
      'LEFT JOIN ocorbank ob ON ob.Codigo=oc.Ocorrencia'
      'WHERE op.FatID=304'
      'AND op.FatNum > 0'
      'AND op.FatNum=:P0'
      '')
    Left = 368
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorPDOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrOcorPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrOcorPPagoNeg: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PagoNeg'
      Calculated = True
    end
    object QrOcorPSALDONEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDONEG'
      Calculated = True
    end
    object QrOcorPBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrOcorPAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrOcorPConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrOcorPCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrOcorPDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrOcorPEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrOcorPCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrOcorPTIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrOcorPSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrOcorPPAGO: TFloatField
      FieldName = 'PAGO'
    end
    object QrOcorPDataO: TDateField
      FieldName = 'DataO'
    end
    object QrOcorPOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrOcorPValor: TFloatField
      FieldName = 'Valor'
    end
    object QrOcorPDescri: TWideStringField
      FieldName = 'Descri'
      Size = 30
    end
    object QrOcorPNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
  end
  object QrCHDevP: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCHDevPCalcFields
    SQL.Strings = (
      '/*'
      'SELECT ai.ValPago,ai.Banco, ai.Agencia, ai.Conta, ai.Cheque, '
      'ai.CPF, ai.Valor, ai.Taxas, ai.Multa, ai.Emitente, '
      'ai.Status, ai.JurosP, ai.JurosV, ai.Desconto, '
      'CASE WHEN ai.Status = 2 THEN 0 ELSE (ai.Valor + ai.Taxas +'
      'ai.Multa + ai.JurosV - ai.Desconto - ai.ValPago) END SALDO, '
      'ap.Data, ap.Juros, ap.Pago, ap.AlinIts, ap.Codigo, ap.LotePG '
      'FROM alin pgs ap'
      'LEFT JOIN alinits ai ON ai.Codigo=ap.AlinIts'
      'WHERE ap.LotePG=:P0'
      'AND ap.LotePG > 0'
      '*/'
      '  SELECT ai.ValPago,ai.Banco, ai.Agencia, ai.Conta, ai.Cheque,'
      '  ai.CPF, ai.Valor, ai.Taxas, ai.Multa, ai.Emitente,'
      '  ai.Status, ai.JurosP, ai.JurosV, ai.Desconto,'
      '  IF(ai.Status = 2, 0, (ai.Valor + ai.Taxas +'
      '  ai.Multa + ai.JurosV - ai.Desconto - ai.ValPago)) SALDO,'
      '  ap.Data, ap.MoraVal, ap.Credito, ap.FatParcRef, '
      '  ap.FatParcela, ap.FatNum'
      '  FROM lct0001a ap'
      '  LEFT JOIN alinits ai ON ai.Codigo=ap.FatParcRef'
      '  WHERE ap.FatID=305'
      '  AND ap.FatNum=:P0'
      '  AND ap.FatNum > 0')
    Left = 424
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCHDevPData: TDateField
      FieldName = 'Data'
      Origin = 'lct0001a.Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevPMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lct0001a.MoraVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lct0001a.Credito'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'alinits.Banco'
      DisplayFormat = '000'
    end
    object QrCHDevPAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'alinits.Agencia'
      DisplayFormat = '0000'
    end
    object QrCHDevPConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'alinits.Conta'
    end
    object QrCHDevPCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'alinits.Cheque'
      DisplayFormat = '000000'
    end
    object QrCHDevPCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'alinits.CPF'
      Size = 15
    end
    object QrCHDevPValor: TFloatField
      FieldName = 'Valor'
      Origin = 'alinits.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPTaxas: TFloatField
      FieldName = 'Taxas'
      Origin = 'alinits.Taxas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'alinits.Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'alinits.Emitente'
      Size = 50
    end
    object QrCHDevPStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'alinits.Status'
    end
    object QrCHDevPJurosP: TFloatField
      FieldName = 'JurosP'
      Origin = 'alinits.JurosP'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCHDevPJurosV: TFloatField
      FieldName = 'JurosV'
      Origin = 'alinits.JurosV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPDesconto: TFloatField
      FieldName = 'Desconto'
      Origin = 'alinits.Desconto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrCHDevPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrCHDevPValPago: TFloatField
      FieldName = 'ValPago'
      Origin = 'alinits.ValPago'
    end
    object QrCHDevPSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrCHDevPFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
      Origin = 'lct0001a.FatParcRef'
    end
    object QrCHDevPFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lct0001a.FatParcela'
    end
    object QrCHDevPFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lct0001a.FatNum'
    end
  end
  object frxDsCHDevP: TfrxDBDataset
    UserName = 'frxDsCHDevP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'MoraVal=MoraVal'
      'Credito=Credito'
      'Banco=Banco'
      'Agencia=Agencia'
      'Conta=Conta'
      'Cheque=Cheque'
      'CPF=CPF'
      'Valor=Valor'
      'Taxas=Taxas'
      'Multa=Multa'
      'Emitente=Emitente'
      'Status=Status'
      'JurosP=JurosP'
      'JurosV=JurosV'
      'Desconto=Desconto'
      'NOMESTATUS=NOMESTATUS'
      'CPF_TXT=CPF_TXT'
      'ValPago=ValPago'
      'SALDO=SALDO'
      'FatParcRef=FatParcRef'
      'FatParcela=FatParcela'
      'FatNum=FatNum')
    DataSet = QrCHDevP
    BCDToCurrency = False
    Left = 453
    Top = 244
  end
  object QrDPago: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDPagoCalcFields
    SQL.Strings = (
      'SELECT li.Emitente, li.CNPJCPF, li.Credito, '
      'li.Duplicata, li.Quitado, '
      'IF(li.Quitado = 2, 0, '
      '(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg)) SALDO, '
      'dp.Data, dp.MoraVal, dp.Credito PAGO, dp.FatNum, '
      'dp.FatParcela, dp.Desco '
      'FROM lct0001a dp '
      'LEFT JOIN lct0001a li ON li.FatParcela=dp.FatParcRef '
      'WHERE li.FatID=301'
      'AND dp.FatID=306'
      'AND dp.FatNum=12793'
      'AND dp.FatNum > 0 ')
    Left = 481
    Top = 244
    object QrDPagoEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lct0001a.Emitente'
      Size = 30
    end
    object QrDPagoCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lct0001a.CNPJCPF'
      Size = 15
    end
    object QrDPagoCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lct0001a.Credito'
    end
    object QrDPagoDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lct0001a.Duplicata'
      Size = 12
    end
    object QrDPagoQuitado: TIntegerField
      FieldName = 'Quitado'
      Origin = 'lct0001a.Quitado'
    end
    object QrDPagoSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrDPagoData: TDateField
      FieldName = 'Data'
      Origin = 'lct0001a.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDPagoMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lct0001a.MoraVal'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDPagoPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lct0001a.Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDPagoFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lct0001a.FatParcela'
    end
    object QrDPagoDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'lct0001a.Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDPagoNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrDPagoCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDPagoFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lct0001a.FatNum'
    end
  end
  object frxDsDPago: TfrxDBDataset
    UserName = 'frxDsDPago'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Juros=Juros'
      'Pago=Pago'
      'LotePg=LotePG'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Controle=Controle'
      'Desco=Desco'
      'Emitente=Emitente'
      'CPF=CPF'
      'Valor=Valor'
      'Duplicata=Duplicata'
      'NOMESTATUS=NOMESTATUS'
      'Quitado=Quitado'
      'CPF_TXT=CPF_TXT'
      'SALDO=SALDO')
    DataSet = QrDPago
    BCDToCurrency = False
    Left = 510
    Top = 244
  end
  object QrOcorPSL: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcorPSLCalcFields
    SQL.Strings = (
      'SELECT oc.DataO, oc.Ocorrencia, oc.Valor,  oc.Descri,'
      '(oc.Valor + oc.TaxaV - oc.Pago) SALDO,'
      'oc.Banco, oc.Agencia, oc.Conta, oc.Cheque,'
      'oc.Duplicata, oc.Emitente, oc.CPF,'
      'ELT(oc.TpOcor, "CH", "DU", "CL", "??") TIPODOC,'
      'ob.Nome NOMEOCORRENCIA, (op.Credito - op.Debito) PAGO'
      'FROM lct0001a op'
      'LEFT JOIN ocorreu  oc ON oc.Codigo=op.Ocorreu'
      'LEFT JOIN ocorbank ob ON ob.Codigo=oc.Ocorrencia'
      'WHERE op.FatID=304'
      'AND op.Data=:P0'
      'AND op.FatNum=:P1'
      'AND op.FatNum = 0')
    Left = 312
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorPSLDOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrOcorPSLCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrOcorPSLPagoNeg: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PagoNeg'
      Calculated = True
    end
    object QrOcorPSLSALDONEG: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDONEG'
      Calculated = True
    end
    object QrOcorPSLDataO: TDateField
      FieldName = 'DataO'
    end
    object QrOcorPSLOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrOcorPSLValor: TFloatField
      FieldName = 'Valor'
    end
    object QrOcorPSLDescri: TWideStringField
      FieldName = 'Descri'
      Size = 30
    end
    object QrOcorPSLSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrOcorPSLBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrOcorPSLAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrOcorPSLConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrOcorPSLCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrOcorPSLDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrOcorPSLEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrOcorPSLCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrOcorPSLTIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrOcorPSLNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorPSLPAGO: TFloatField
      FieldName = 'PAGO'
    end
  end
  object frxDsOcorPSL: TfrxDBDataset
    UserName = 'frxDsOcorPSL'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DOCUM_TXT=DOCUM_TXT'
      'Banco=Banco'
      'Agencia=Agencia'
      'Conta=Conta'
      'Cheque=Cheque'
      'Duplicata=Duplicata'
      'Tipo=Tipo'
      'TIPODOC=TIPODOC'
      'NOMEOCORRENCIA=NOMEOCORRENCIA'
      'Codigo=Codigo'
      'Ocorreu=Ocorreu'
      'Data=Data'
      'Juros=Juros'
      'Pago=Pago'
      'LotePg=LotePg'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Ocorrencia=Ocorrencia'
      'Valor=Valor'
      'DataO=DataO'
      'SALDO=SALDO'
      'Emitente=Emitente'
      'CPF=CPF'
      'CPF_TXT=CPF_TXT'
      'Descri=Descri'
      'PagoNeg=PagoNeg'
      'SALDONEG=SALDONEG')
    DataSet = QrOcorPSL
    BCDToCurrency = False
    Left = 340
    Top = 216
  end
  object QrCHDevPSL: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCHDevPSLCalcFields
    SQL.Strings = (
      'SELECT ai.ValPago,ai.Banco, ai.Agencia, ai.Conta, ai.Cheque, '
      'ai.CPF, ai.Valor, ai.Taxas, ai.Multa, ai.Emitente, '
      'ai.Status, ai.JurosP, ai.JurosV, ai.Desconto, '
      'CASE WHEN ai.Status = 2 THEN 0 ELSE (ai.Valor + ai.Taxas +'
      'ai.Multa + ai.JurosV - ai.Desconto - ai.ValPago) END SALDO, '
      'ap.Data, ap.Juros, ap.Pago, ap.AlinIts, ap.Codigo, ap.LotePG '
      'FROM alin pgs ap'
      'LEFT JOIN alinits ai ON ai.Codigo=ap.AlinIts'
      'WHERE ap.Data=:P0'
      'AND ap.LotePG=:P1'
      'AND ap.LotePG = 0')
    Left = 368
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCHDevPSLData: TDateField
      FieldName = 'Data'
      Origin = 'lct0001a.Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevPSLMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lct0001a.MoraVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lct0001a.Credito'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'alinits.Banco'
      DisplayFormat = '000'
    end
    object QrCHDevPSLAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'alinits.Agencia'
      DisplayFormat = '0000'
    end
    object QrCHDevPSLConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'alinits.Conta'
    end
    object QrCHDevPSLCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'alinits.Cheque'
      DisplayFormat = '000000'
    end
    object QrCHDevPSLCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'alinits.CPF'
      Size = 15
    end
    object QrCHDevPSLValor: TFloatField
      FieldName = 'Valor'
      Origin = 'alinits.Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLTaxas: TFloatField
      FieldName = 'Taxas'
      Origin = 'alinits.Taxas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'alinits.Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'alinits.Emitente'
      Size = 50
    end
    object QrCHDevPSLStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'alinits.Status'
    end
    object QrCHDevPSLJurosP: TFloatField
      FieldName = 'JurosP'
      Origin = 'alinits.JurosP'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCHDevPSLJurosV: TFloatField
      FieldName = 'JurosV'
      Origin = 'alinits.JurosV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLDesconto: TFloatField
      FieldName = 'Desconto'
      Origin = 'alinits.Desconto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevPSLNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrCHDevPSLCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrCHDevPSLValPago: TFloatField
      FieldName = 'ValPago'
      Origin = 'alinits.ValPago'
    end
    object QrCHDevPSLSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrCHDevPSLFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
      Origin = 'lct0001a.FatParcRef'
    end
    object QrCHDevPSLFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lct0001a.FatParcela'
    end
    object QrCHDevPSLFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lct0001a.FatNum'
    end
  end
  object QrDPagoSL: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDPagoSLCalcFields
    SQL.Strings = (
      
        'SELECT li.Emitente, li.CNPJCPF, li.Credito, li.Duplicata, li.Qui' +
        'tado, '
      'IF(li.Quitado = 2, 0, '
      
        '(li.Credito + li.TotalJr - li.TotalDs - li.TotalPg)) SALDO, dp.*' +
        ' '
      'FROM lct0001a dp'
      'LEFT JOIN lct0001a li ON li.FatParcela=dp.FatParcRef'
      'WHERE li.FatID=301'
      'AND dp.FatID=306'
      'AND dp.Data=2011-12-02'
      'AND dp.FatNum=12793'
      'AND dp.FatNum = 0 '
      ''
      ''
      '/********* FIM SQL *********/'
      ''
      '/*****Query sem parametros*******/'
      ''
      '')
    Left = 425
    Top = 216
    object QrDPagoSLNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrDPagoSLCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDPagoSLEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrDPagoSLCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrDPagoSLCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrDPagoSLDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrDPagoSLQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrDPagoSLSALDO: TFloatField
      FieldName = 'SALDO'
    end
  end
  object frxDsDPagoSL: TfrxDBDataset
    UserName = 'frxDsDPagoSL'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Juros=Juros'
      'Pago=Pago'
      'LotePg=LotePG'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Controle=Controle'
      'Desco=Desco'
      'Emitente=Emitente'
      'CPF=CPF'
      'Valor=Valor'
      'Duplicata=Duplicata'
      'NOMESTATUS=NOMESTATUS'
      'Quitado=Quitado'
      'CPF_TXT=CPF_TXT'
      'SALDO=SALDO')
    DataSet = QrDPagoSL
    BCDToCurrency = False
    Left = 454
    Top = 216
  end
  object frxDsCHDevPSL: TfrxDBDataset
    UserName = 'frxDsCHDevPSL'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'MoraVal=MoraVal'
      'Credito=Credito'
      'Banco=Banco'
      'Agencia=Agencia'
      'Conta=Conta'
      'Cheque=Cheque'
      'CPF=CPF'
      'Valor=Valor'
      'Taxas=Taxas'
      'Multa=Multa'
      'Emitente=Emitente'
      'Status=Status'
      'JurosP=JurosP'
      'JurosV=JurosV'
      'Desconto=Desconto'
      'NOMESTATUS=NOMESTATUS'
      'CPF_TXT=CPF_TXT'
      'ValPago=ValPago'
      'SALDO=SALDO'
      'FatParcRef=FatParcRef'
      'FatParcela=FatParcela'
      'FatNum=FatNum')
    DataSet = QrCHDevPSL
    BCDToCurrency = False
    Left = 397
    Top = 216
  end
end
