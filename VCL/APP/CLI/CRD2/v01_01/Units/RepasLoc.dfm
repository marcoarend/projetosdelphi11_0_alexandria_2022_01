object FmRepasLoc: TFmRepasLoc
  Left = 396
  Top = 181
  Caption = 'REP-GEREN-003 :: Localiza'#231#227'o de Repasse'
  ClientHeight = 443
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 97
    Width = 784
    Height = 232
    Align = alClient
    DataSource = DsLoc
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Total'
        Title.Caption = 'Valor'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECOLIGADO'
        Title.Caption = 'Coligado'
        Width = 477
        Visible = True
      end>
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 88
    object Label2: TLabel
      Left = 12
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Coligado:'
    end
    object Label34: TLabel
      Left = 504
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label4: TLabel
      Left = 608
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object CBColigado: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 421
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsClientes
      TabOrder = 1
      dmkEditCB = EdColigado
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdColigado: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdColigadoChange
      DBLookupComboBox = CBColigado
      IgnoraDBLookupComboBox = False
    end
    object TPIni: TDateTimePicker
      Left = 504
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 2
      OnChange = TPIniChange
    end
    object TPFim: TDateTimePicker
      Left = 608
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 3
      OnChange = TPFimChange
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = 4
    ExplicitTop = 4
    ExplicitWidth = 630
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 582
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 534
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 297
        Height = 32
        Caption = 'Localiza'#231#227'o de Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 297
        Height = 32
        Caption = 'Localiza'#231#227'o de Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 297
        Height = 32
        Caption = 'Localiza'#231#227'o de Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 329
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitTop = 289
    ExplicitWidth = 630
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 626
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 373
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 333
    ExplicitWidth = 630
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 484
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 482
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localiza'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsClientes: TDataSource
    DataSet = QrColigados
    Left = 416
    Top = 46
  end
  object QrColigados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades'
      'WHERE Fornece2="V" '
      'ORDER BY NOMEENTIDADE')
    Left = 388
    Top = 46
    object QrColigadosNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrColigadosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocAfterOpen
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECOLIGADO, '
      'CASE WHEN en.Tipo=0 THEN en.CNPJ'
      'ELSE en.CPF END CNPJCPF, re.* '
      'FROM repas re'
      'LEFT JOIN entidades en ON en.Codigo=re.Coligado'
      'WHERE re.Coligado>0'
      'ORDER BY Codigo DESC')
    Left = 40
    Top = 156
    object QrLocNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrLocCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocColigado: TIntegerField
      FieldName = 'Coligado'
      Required = True
    end
    object QrLocData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLocTotal: TFloatField
      FieldName = 'Total'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLocJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 68
    Top = 156
  end
end
