unit Decomp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit;

type
  TFmDecomp = class(TForm)
    PainelDados: TPanel;
    Label11: TLabel;
    EdJuros: TdmkEdit;
    Label1: TLabel;
    EdPrazo: TdmkEdit;
    EdCasas: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdTaxa: TdmkEdit;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmDecomp: TFmDecomp;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmDecomp.BtOKClick(Sender: TObject);
var
  Casas: Integer;
  Juros, Prazo, TaxaM: Double;
begin
  Casas := EdCasas.ValueVariant;
  Juros := EdJuros.ValueVariant;
  Prazo := EdPrazo.ValueVariant;
  //
  TaxaM := MLAGeral.DescobreJuroComposto(Juros, Prazo, Casas);
  EdTaxa.DecimalSize := Casas;
  EdTaxa.ValueVariant := TaxaM;
end;

procedure TFmDecomp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDecomp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDecomp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, False, taCenter, 2, 10, 20);
end;

end.
