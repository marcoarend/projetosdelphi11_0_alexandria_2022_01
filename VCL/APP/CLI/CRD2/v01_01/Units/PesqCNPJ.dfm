object FmPesqCNPJ: TFmPesqCNPJ
  Left = 419
  Top = 217
  Caption = 'CSE-PESQU-005 :: Pesquisa de Sacados'
  ClientHeight = 415
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 794
    Height = 61
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 188
      Top = 12
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object RGMascara: TRadioGroup
      Left = 8
      Top = 8
      Width = 177
      Height = 41
      Caption = ' M'#225'scara: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Ambos'
        'Prefixo'
        'Sufixo')
      TabOrder = 0
      OnClick = RGMascaraClick
    end
    object EdEmitente: TdmkEdit
      Left = 188
      Top = 28
      Width = 593
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnChange = EdEmitenteChange
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 109
    Width = 794
    Height = 192
    Align = alClient
    DataSource = DsSacados
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 794
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 746
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 698
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 263
        Height = 32
        Caption = 'Pesquisa de Sacados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 263
        Height = 32
        Caption = 'Pesquisa de Sacados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 263
        Height = 32
        Caption = 'Pesquisa de Sacados'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 301
    Width = 794
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 790
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 345
    Width = 794
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 648
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 646
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        TabOrder = 0
        OnClick = BtPesquisaClick
        NumGlyphs = 2
      end
      object BtConfirma: TBitBtn
        Tag = 15
        Left = 334
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Confirma'
        Enabled = False
        TabOrder = 1
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
    end
  end
  object QrSacados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sac.Numero+0.000 Numero, sac.* '
      'FROM sacados sac'
      'WHERE sac.Nome LIKE :P0'
      'ORDER BY sac.Nome')
    Left = 296
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
    object QrSacadosIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrSacadosRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrSacadosCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrSacadosBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrSacadosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrSacadosUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSacadosCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrSacadosTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrSacadosRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrSacadosNumero: TFloatField
      FieldName = 'Numero'
    end
  end
  object DsSacados: TDataSource
    DataSet = QrSacados
    Left = 324
    Top = 164
  end
end
