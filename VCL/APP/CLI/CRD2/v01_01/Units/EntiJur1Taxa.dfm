object FmEntiJur1Taxa: TFmEntiJur1Taxa
  Left = 339
  Top = 185
  Caption = 'CLI-JURID-004 :: Taxa'
  ClientHeight = 216
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 637
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 589
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 541
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 55
        Height = 32
        Caption = 'Taxa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 55
        Height = 32
        Caption = 'Taxa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 55
        Height = 32
        Caption = 'Taxa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 146
    Width = 637
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 491
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 489
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 637
    Height = 54
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 637
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 637
        Height = 54
        Align = alClient
        TabOrder = 0
        object PainelTaxas: TPanel
          Left = 2
          Top = 15
          Width = 633
          Height = 37
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label28: TLabel
            Left = 8
            Top = 8
            Width = 27
            Height = 13
            Caption = 'Taxa:'
          end
          object Label35: TLabel
            Left = 464
            Top = 8
            Width = 53
            Height = 13
            Caption = '% ou Valor:'
          end
          object SpeedButton5: TSpeedButton
            Left = 438
            Top = 4
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object EdTaxa: TdmkEditCB
            Left = 40
            Top = 4
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBTaxa
            IgnoraDBLookupComboBox = False
          end
          object CBTaxa: TdmkDBLookupComboBox
            Left = 95
            Top = 4
            Width = 340
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTaxas
            TabOrder = 1
            dmkEditCB = EdTaxa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdValor: TdmkEdit
            Left = 524
            Top = 4
            Width = 97
            Height = 21
            Alignment = taRightJustify
            MaxLength = 255
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 102
    Width = 637
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 633
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrTaxas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrTaxasAfterScroll
    SQL.Strings = (
      'SELECT * FROM taxas'
      'ORDER BY Nome')
    Left = 116
    Top = 16
    object QrTaxasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTaxasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTaxasGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrTaxasForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrTaxasMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrTaxasBase: TSmallintField
      FieldName = 'Base'
    end
    object QrTaxasAutomatico: TSmallintField
      FieldName = 'Automatico'
    end
    object QrTaxasValor: TFloatField
      FieldName = 'Valor'
    end
    object QrTaxasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsTaxas: TDataSource
    DataSet = QrTaxas
    Left = 144
    Top = 16
  end
end
