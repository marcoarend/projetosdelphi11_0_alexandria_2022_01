unit RiscoCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Grids, DBGrids, Db,
  mySQLDbTables, ComCtrls, Menus, frxClass, frxDBSet, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, dmkGeral, DmkDAC_PF,
  UnDmkEnums;

type
  TFmRiscoCli = class(TForm)
    PainelDados: TPanel;
    Panel1: TPanel;
    QrOcorA: TmySQLQuery;
    QrOcorATIPODOC: TWideStringField;
    QrOcorANOMEOCORRENCIA: TWideStringField;
    QrOcorACodigo: TIntegerField;
    QrOcorADataO: TDateField;
    QrOcorAOcorrencia: TIntegerField;
    QrOcorAValor: TFloatField;
    QrOcorALoteQuit: TIntegerField;
    QrOcorALk: TIntegerField;
    QrOcorADataCad: TDateField;
    QrOcorADataAlt: TDateField;
    QrOcorAUserCad: TIntegerField;
    QrOcorAUserAlt: TIntegerField;
    QrOcorATaxaP: TFloatField;
    QrOcorATaxaV: TFloatField;
    QrOcorAPago: TFloatField;
    QrOcorADataP: TDateField;
    QrOcorATaxaB: TFloatField;
    QrOcorAData3: TDateField;
    QrOcorAStatus: TSmallintField;
    QrOcorASALDO: TFloatField;
    QrOcorAATUALIZADO: TFloatField;
    DsOcorA: TDataSource;
    QrOcorACliente: TIntegerField;
    QrCHDevA: TmySQLQuery;
    QrCHDevADATA1_TXT: TWideStringField;
    QrCHDevADATA2_TXT: TWideStringField;
    QrCHDevADATA3_TXT: TWideStringField;
    QrCHDevACPF_TXT: TWideStringField;
    QrCHDevANOMECLIENTE: TWideStringField;
    QrCHDevACodigo: TIntegerField;
    QrCHDevAAlinea1: TIntegerField;
    QrCHDevAAlinea2: TIntegerField;
    QrCHDevAData1: TDateField;
    QrCHDevAData2: TDateField;
    QrCHDevAData3: TDateField;
    QrCHDevACliente: TIntegerField;
    QrCHDevABanco: TIntegerField;
    QrCHDevAAgencia: TIntegerField;
    QrCHDevAConta: TWideStringField;
    QrCHDevACheque: TIntegerField;
    QrCHDevACPF: TWideStringField;
    QrCHDevAValor: TFloatField;
    QrCHDevATaxas: TFloatField;
    QrCHDevALk: TIntegerField;
    QrCHDevADataCad: TDateField;
    QrCHDevADataAlt: TDateField;
    QrCHDevAUserCad: TIntegerField;
    QrCHDevAUserAlt: TIntegerField;
    QrCHDevAEmitente: TWideStringField;
    QrCHDevAChequeOrigem: TIntegerField;
    QrCHDevAStatus: TSmallintField;
    QrCHDevAValPago: TFloatField;
    QrCHDevAMulta: TFloatField;
    QrCHDevAJurosP: TFloatField;
    QrCHDevAJurosV: TFloatField;
    QrCHDevADesconto: TFloatField;
    QrCHDevASALDO: TFloatField;
    QrCHDevAATUAL: TFloatField;
    DsCHDevA: TDataSource;
    QrDOpen: TmySQLQuery;
    QrDOpenDuplicata: TWideStringField;
    QrDOpenDCompra: TDateField;
    QrDOpenDDeposito: TDateField;
    QrDOpenEmitente: TWideStringField;
    QrDOpenCliente: TIntegerField;
    QrDOpenSTATUS: TWideStringField;
    QrDOpenQuitado: TIntegerField;
    QrDOpenTotalJr: TFloatField;
    QrDOpenTotalDs: TFloatField;
    QrDOpenTotalPg: TFloatField;
    QrDOpenSALDO_DESATUALIZ: TFloatField;
    QrDOpenSALDO_ATUALIZADO: TFloatField;
    QrDOpenNOMESTATUS: TWideStringField;
    QrDOpenDDCALCJURO: TIntegerField;
    QrDOpenData3: TDateField;
    DsDOpen: TDataSource;
    QrRiscoC: TmySQLQuery;
    DsRiscoC: TDataSource;
    DsRiscoTC: TDataSource;
    QrRiscoTC: TmySQLQuery;
    QrRiscoTCValor: TFloatField;
    GroupBox1: TGroupBox;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesAdValorem: TFloatField;
    QrClientesDMaisC: TIntegerField;
    QrClientesFatorCompra: TFloatField;
    QrClientesMAIOR_T: TFloatField;
    QrClientesADVAL_T: TFloatField;
    QrClientesDMaisD: TIntegerField;
    DsClientes: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid5: TDBGrid;
    DBGrid10: TDBGrid;
    DBGrid14: TDBGrid;
    DBGrid12: TDBGrid;
    QrColigados: TmySQLQuery;
    DsColigados: TDataSource;
    QrColigadosCodigo: TIntegerField;
    QrColigadosNOMECOLIGADO: TWideStringField;
    QrDOpenBanco: TIntegerField;
    QrOcorABanco: TIntegerField;
    QrOcorAAgencia: TIntegerField;
    QrOcorACheque: TIntegerField;
    QrOcorADuplicata: TWideStringField;
    QrOcorAConta: TWideStringField;
    QrOcorAEmissao: TDateField;
    QrOcorADCompra: TDateField;
    QrOcorAVencto: TDateField;
    QrOcorADDeposito: TDateField;
    QrOcorAEmitente: TWideStringField;
    QrOcorACPF: TWideStringField;
    QrRisco1: TmySQLQuery;
    QrRisco1Tipo: TIntegerField;
    QrRisco1Banco: TIntegerField;
    QrRisco1Agencia: TIntegerField;
    QrRisco1Conta: TWideStringField;
    QrRisco1Duplicata: TWideStringField;
    QrRisco1Cheque: TIntegerField;
    QrRisco1DEmissao: TDateField;
    QrRisco1DCompra: TDateField;
    QrRisco1DVence: TDateField;
    QrRisco1DDeposito: TDateField;
    QrRisco1DDevol1: TDateField;
    QrRisco1DDevol2: TDateField;
    QrRisco1UltPagto: TDateField;
    QrRisco1Valor: TFloatField;
    QrRisco1Taxas: TFloatField;
    QrRisco1Saldo: TFloatField;
    QrRisco1Juros: TFloatField;
    QrRisco1Desco: TFloatField;
    QrRisco1Pago: TFloatField;
    QrRisco1Atual: TFloatField;
    QrRisco1Emitente: TWideStringField;
    QrRisco1CPF: TWideStringField;
    QrRisco1Alinea1: TIntegerField;
    QrRisco1Alinea2: TIntegerField;
    QrRisco1Status: TWideStringField;
    QrRisco1Historico: TWideStringField;
    QrRisco1CONTA_DUPLICATA: TWideStringField;
    QrRisco1DEMISSAO_TXT: TWideStringField;
    QrRisco1DCOMPRA_TXT: TWideStringField;
    QrRisco1DVENCE_TXT: TWideStringField;
    QrRisco1DDEPOSITO_TXT: TWideStringField;
    QrRisco1DDEVOL1_TXT: TWideStringField;
    QrRisco1DDEVOL2_TXT: TWideStringField;
    QrRisco1ULTPAGTO_TXT: TWideStringField;
    TabSheet5: TTabSheet;
    GroupBox2: TGroupBox;
    Ck10: TCheckBox;
    Ck30: TCheckBox;
    Ck20: TCheckBox;
    Ck50: TCheckBox;
    Panel2: TPanel;
    BtImprime: TBitBtn;
    BtCancela: TBitBtn;
    QrRisco1TIPODOC: TWideStringField;
    Panel3: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    QrRisco1CONTAGEM: TIntegerField;
    QrRisco1NOMECLIENTE: TWideStringField;
    QrRisco1Cliente: TIntegerField;
    BtGera: TBitBtn;
    QrRisco1Controle: TIntegerField;
    Ck40: TCheckBox;
    Panel4: TPanel;
    Panel5: TPanel;
    RGAgrupa: TRadioGroup;
    Panel6: TPanel;
    Ck60: TCheckBox;
    Ck70: TCheckBox;
    QrRisco1STATUS_HISTORICO: TWideStringField;
    QrSintetico: TmySQLQuery;
    QrAnalitico: TmySQLQuery;
    QrAnaliticoCliente: TIntegerField;
    QrAnaliticoTipo: TIntegerField;
    QrAnaliticoValor: TFloatField;
    QrAnaliticoTaxas: TFloatField;
    QrAnaliticoSaldo: TFloatField;
    QrAnaliticoJuros: TFloatField;
    QrAnaliticoDesco: TFloatField;
    QrAnaliticoPago: TFloatField;
    QrAnaliticoAtual: TFloatField;
    QrAnaliticoITENS: TLargeintField;
    QrAnaliticoDESCRITIPO: TWideStringField;
    QrAnaliticoNOMECLI: TWideStringField;
    PMImprime: TPopupMenu;
    Descritivo1: TMenuItem;
    Analtico1: TMenuItem;
    Sinttico1: TMenuItem;
    QrSinteticoCliente: TIntegerField;
    QrSinteticoTipo: TIntegerField;
    QrSinteticoValor: TFloatField;
    QrSinteticoTaxas: TFloatField;
    QrSinteticoSaldo: TFloatField;
    QrSinteticoJuros: TFloatField;
    QrSinteticoDesco: TFloatField;
    QrSinteticoPago: TFloatField;
    QrSinteticoAtual: TFloatField;
    QrSinteticoITENS: TLargeintField;
    QrSinteticoNOMECLI: TWideStringField;
    DBGrid1: TDBGrid;
    QrOcorADOCUM_TXT: TWideStringField;
    QrDOpenRepassado: TSmallintField;
    QrOcorADescri: TWideStringField;
    frxRisco1: TfrxReport;
    frxDsRisco1: TfrxDBDataset;
    frxAnalitico: TfrxReport;
    frxDsAnalitico: TfrxDBDataset;
    frxSintetico: TfrxReport;
    frxDsSintetico: TfrxDBDataset;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel8: TPanel;
    BtPesquisa: TBitBtn;
    Panel9: TPanel;
    GroupBox3: TGroupBox;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    QrOcorAMoviBank: TIntegerField;
    QrRiscoCCliente: TIntegerField;
    QrRiscoCTipo: TSmallintField;
    QrRiscoCBanco: TIntegerField;
    QrRiscoCContaCorrente: TWideStringField;
    QrRiscoCDocumento: TFloatField;
    QrRiscoCCredito: TFloatField;
    QrRiscoCDCompra: TDateField;
    QrRiscoCDDeposito: TDateField;
    QrRiscoCEmitente: TWideStringField;
    QrRiscoCCNPJCPF: TWideStringField;
    QrRiscoCData: TDateField;
    QrRiscoCVencimento: TDateField;
    QrRiscoCFatParcela: TIntegerField;
    QrRiscoCCPF_TXT: TWideStringField;
    QrDOpenFatParcela: TIntegerField;
    QrDOpenCredito: TFloatField;
    QrDOpenCNPJCPF: TWideStringField;
    QrDOpenVencimento: TDateField;
    QrDOpenData: TDateField;
    QrSinteticoDevolvido: TFloatField;
    QrSinteticoLimiCred: TFloatField;
    QrSinteticoULO: TFloatField;
    Progress1: TProgressBar;
    QrRiscoCAgencia: TIntegerField;
    QrDOpenAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrOcorAAfterOpen(DataSet: TDataSet);
    procedure QrOcorABeforeClose(DataSet: TDataSet);
    procedure QrOcorACalcFields(DataSet: TDataSet);
    procedure QrCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrCHDevABeforeClose(DataSet: TDataSet);
    procedure QrCHDevACalcFields(DataSet: TDataSet);
    procedure QrDOpenAfterOpen(DataSet: TDataSet);
    procedure QrDOpenBeforeClose(DataSet: TDataSet);
    procedure QrDOpenCalcFields(DataSet: TDataSet);
    procedure QrRiscoCCalcFields(DataSet: TDataSet);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure QrRisco1CalcFields(DataSet: TDataSet);
    procedure frRisco1GetValue(const ParName: String;
      var ParValue: Variant);
    procedure frRisco1UserFunction(const Name: String; p1, p2, p3: Variant;
      var Val: Variant);
    procedure BtGeraClick(Sender: TObject);
    procedure Descritivo1Click(Sender: TObject);
    procedure Analtico1Click(Sender: TObject);
    procedure QrAnaliticoCalcFields(DataSet: TDataSet);
    procedure Sinttico1Click(Sender: TObject);
    procedure frxRisco1GetValue(const VarName: String; var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
  private
    { Private declarations }
    FCHDevOpen_Total, FOcorA_Total: Double;
    FCliente, FColigado: Integer;
    FParar: Boolean;
    FR10, FR20, FR30, FR40, FR50, FR60, FR70: Integer;
    procedure ReopenOcorA();
    procedure ReopenCHDevA();
    procedure ReopenDOpen();
    procedure ReopenRiscoC();
    procedure DesfazPesquisa;
    function ParaImpressao: Boolean;
    function ImpressaoDeCabecalho(Tipo: Integer): String;
    function DescricaoDeDocumento(Tipo: Integer): String;
    function CondicaoDeImpressao(Tipo: Integer): String;
    function Ordem(Tipo: Integer): String;
    procedure DefineTipos;
 public
    { Public declarations }
    FEmpresa, FCliInt, FEntidade: Integer;
    //?TabLctA, ?TabLctB, ?TabLctD,
    FEntidade_TXT: String;
    FDtEncer, FDtMorto: TDateTime;
  end;

  var
  FmRiscoCli: TFmRiscoCli;

implementation

uses UnMyObjects, Module, UCreate, Principal, UMySQLModule, ModuleGeral,
  MyListas, UnInternalConsts;

{$R *.DFM}


procedure TFmRiscoCli.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRiscoCli.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmRiscoCli.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRiscoCli.BtPesquisaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  FCliente  := EdCliente.ValueVariant;
  FColigado := EdColigado.ValueVariant;
  // soma antes do �ltimo !!!!
  ReopenCHDevA();
  ReopenOcorA();
  // Normais
  ReopenRiscoC();
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenDOpen();
  BtImprime.Enabled := False;
  BtGera.Enabled := True;
  Screen.Cursor := crDefault;
end;

procedure TFmRiscoCli.QrOcorAAfterOpen(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
  while not QrOcorA.Eof do
  begin
    //if Int(Date) > QrOcorADataO.Value then
      FOcorA_Total := FOcorA_Total + QrOcorAATUALIZADO.Value;
    QrOcorA.Next;
  end;
end;

procedure TFmRiscoCli.QrOcorABeforeClose(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
end;

procedure TFmRiscoCli.QrOcorACalcFields(DataSet: TDataSet);
begin
  QrOcorASALDO.Value := QrOcorAValor.Value + QrOcorATaxaV.Value - QrOcorAPago.Value;
  //
  QrOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcorACliente.Value, 1, QrOcorADataO.Value, Date, QrOcorAData3.Value,
    QrOcorAValor.Value, QrOcorATaxaV.Value, 0 (*Desco*),
    QrOcorAPago.Value, QrOcorATaxaP.Value, False);
  //
  if QrOcorADescri.Value <> '' then
    QrOcorADOCUM_TXT.Value := QrOcorADescri.Value
  else if QrOcorATIPODOC.Value = 'CH' then
    QrOcorADOCUM_TXT.Value := FormatFloat('000', QrOcorABanco.Value)+'/'+
    FormatFloat('0000', QrOcorAAgencia.Value)+'/'+ QrOcorAConta.Value+'-'+
    FormatFloat('000000', QrOcorACheque.Value)
  else if QrOcorATIPODOC.Value = 'DU' then
    QrOcorADOCUM_TXT.Value := QrOcorADuplicata.Value
  else QrOcorADOCUM_TXT.Value := '';
end;

procedure TFmRiscoCli.ReopenOcorA();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Ocorr�ncias');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorA, Dmod.MyDB, [
  'SELECT ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC, ',
  'ob.Nome NOMEOCORRENCIA, ocr.* ',
  'FROM ocorreu ocr ',
  'LEFT JOIN ocorbank ob ON ob.Codigo   = ocr.Ocorrencia ',
  //
  Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits ri ON ri.Origem   = ocr.' + FLD_LOIS,
  'LEFT JOIN repas    re ON re.Codigo   = ri.Codigo']),
  //
  'WHERE ocr.Status<2',
  //
  Geral.ATS_if(FCliente <> 0, [
  'AND ocr.Cliente=' + FormatFloat('0', FCliente) + ' AND ocr.' + FLD_LOIS + '=0']),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  '']);
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmRiscoCli.ReopenCHDevA();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Cheques devolvidos');
  //
  QrCHDevA.Close;
  QrCHDevA.SQL.Clear;
  QrCHDevA.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrCHDevA.SQL.Add('ELSE en.Nome END NOMECLIENTE, ai.*');
  QrCHDevA.SQL.Add('FROM alinits ai');
  QrCHDevA.SQL.Add('LEFT JOIN entidades en ON en.Codigo=ai.Cliente');
  if FmPrincipal.FConnections = 0 then
  begin
    QrCHDevA.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lo ON ai.LoteOrigem=lo.Codigo');
  end;
  QrCHDevA.SQL.Add('');
  QrCHDevA.SQL.Add('');
  QrCHDevA.SQL.Add('');
  if FColigado <> 0 then
  begin
    QrCHDevA.SQL.Add('LEFT JOIN repasits ri ON ri.Origem   = ai.ChequeOrigem');
    QrCHDevA.SQL.Add('LEFT JOIN repas    re ON re.Codigo   = ri.Codigo');
  end;
  QrCHDevA.SQL.Add('WHERE ai.Status<2');
  if FmPrincipal.FConnections = 0 then
  begin
    QrCHDevA.SQL.Add('AND lo.TxCompra + lo.ValValorem + '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  end;
  if FCliente <> 0 then
  begin
    QrCHDevA.SQL.Add('AND ai.Cliente=:P0');
    QrCHDevA.Params[0].AsInteger := FCliente;
  end;
  if FColigado <> 0 then
    QrCHDevA.SQL.Add('AND re.Coligado='+IntToStr(FColigado));
  QrCHDevA.SQL.Add('ORDER BY ai.Data1, ai.Data2');
  QrCHDevA.SQL.Add('');
  UMyMod.AbreQuery(QrCHDevA, Dmod.MyDB);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmRiscoCli.ReopenDOpen();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Duplicatas abertas');
  //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDOpen, Dmod.MyDB, [
  'SELECT od.Nome STATUS, lct.FatParcela, lct.Duplicata, lct.Repassado, ',
  'lct.DCompra, lct.Credito, lct.DDeposito, lct.Emitente, lct.CNPJCPF, ',
  'lo.Cliente, lct.Quitado, lct.TotalJr, lct.TotalDs, lct.TotalPg, ',
  'lct.Vencimento, lct.Data3, lct.Banco, lct.Agencia, lct.Data ',
  'FROM ' + CO_TabLctA + ' lct ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=lct.FatNum ',
  'LEFT JOIN ocordupl  od ON od.Codigo=lct.Devolucao ',
  //
  Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits  ri ON ri.Origem=lct.FatParcela ',
  'LEFT JOIN repas     re ON re.Codigo=ri.Codigo ']),
  //
  'WHERE lct.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo=1 ',
  'AND lo.TxCompra + lo.ValValorem + ' +
        IntToStr(FmPrincipal.FConnections) + ' >= 0.01',
  'AND lct.Quitado <> 2 ',
  //
  Geral.ATS_if(FCliente <> 0, [
  'AND lo.Cliente=' + FormatFloat('0', FCliente)]),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  'ORDER BY lct.Vencimento ',
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmRiscoCli.QrCHDevAAfterOpen(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
  while not QrCHDevA.Eof do
  begin
    //if Int(Date) > QrCHDevAData1.Value then
      FCHDevOpen_Total := FCHDevOpen_Total + QrCHDevAATUAL.Value;
    QrCHDevA.Next;
  end;
end;

procedure TFmRiscoCli.QrCHDevABeforeClose(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
end;

procedure TFmRiscoCli.QrCHDevACalcFields(DataSet: TDataSet);
begin
  QrCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData1.Value);
  QrCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData2.Value);
  QrCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData3.Value);
  QrCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevACPF.Value);
  QrCHDevASALDO.Value := QrCHDevAValor.Value +QrCHDevATaxas.Value +
    QrCHDevAMulta.Value + QrCHDevAJurosV.Value - QrCHDevAValPago.Value -
    QrCHDevADesconto.Value;
  //
  QrCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrCHDevACliente.Value, QrCHDevAStatus.Value, QrCHDevAData1.Value, Date,
    QrCHDevAData3.Value, QrCHDevAValor.Value, QrCHDevAJurosV.Value,
    QrCHDevADesconto.Value, QrCHDevAValPago.Value, QrCHDevAJurosP.Value, False);
end;

procedure TFmRiscoCli.QrDOpenAfterOpen(DataSet: TDataSet);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  DR := 0; DV := 0;
  while not QrDOpen.Eof do
  begin
    if Int(Date) > QrDOpenVencimento.Value then
      DV := DV + QrDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrDOpenSALDO_ATUALIZADO.Value;
    QrDOpen.Next;
  end;
  DT := DR+DV;
  QrDOpen.First;
  CR := QrRiscoTCValor.Value;
  CV := FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FOcorA_Total;
  TT := ST + OA;
  EdDV.ValueVariant := DV;
  EdDR.ValueVariant := DR;
  EdDT.ValueVariant := DT;
  EdCV.ValueVariant := CV;
  EdCR.ValueVariant := CR;
  EdCT.ValueVariant := CT;
  EdRT.ValueVariant := RT;
  EdVT.ValueVariant := VT;
  EdST.ValueVariant := ST;
  EdOA.ValueVariant := OA;
  EdTT.ValueVariant := TT;
  //
end;

procedure TFmRiscoCli.QrDOpenBeforeClose(DataSet: TDataSet);
begin
  EdDV.ValueVariant := 0;
  EdDR.ValueVariant := 0;
  EdDT.ValueVariant := 0;
  EdCV.ValueVariant := 0;
  EdCR.ValueVariant := 0;
  EdCT.ValueVariant := 0;
  EdRT.ValueVariant := 0;
  EdVT.ValueVariant := 0;
  EdST.ValueVariant := 0;
  EdOA.ValueVariant := 0;
  EdTT.ValueVariant := 0;
end;

procedure TFmRiscoCli.QrDOpenCalcFields(DataSet: TDataSet);
begin
  QrDOpenSALDO_DESATUALIZ.Value := QrDOpenCredito.Value + QrDOpenTotalJr.Value -
  QrDOpenTotalDs.Value - QrDOpenTotalPg.Value;
  //
  QrDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrDOpenQuitado.Value,
    QrDOpenDDeposito.Value, Date, QrDOpenData3.Value, QrDOpenRepassado.Value);
  //
  QrDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrDOpenCliente.Value, QrDOpenQuitado.Value, QrDOpenVencimento.Value, Date,
    QrDOpenData3.Value, QrDOpenCredito.Value, QrDOpenTotalJr.Value, QrDOpenTotalDs.Value,
    QrDOpenTotalPg.Value, 0, True);
end;

procedure TFmRiscoCli.QrRiscoCCalcFields(DataSet: TDataSet);
begin
  QrRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRiscoCCNPJCPF.Value);
end;

procedure TFmRiscoCli.FormCreate(Sender: TObject);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  ImgTipo.SQLType := stPsq;
  //
  UCriar.GerenciaTabelaLocal('Risco', acCreate);
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigados, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmRiscoCli.EdClienteChange(Sender: TObject);
begin
  DesfazPesquisa;
end;

procedure TFmRiscoCli.DesfazPesquisa;
begin
  BtImprime.Enabled := False;
  QrOcorA.Close;
  QrCHDevA.Close;
  QrRiscoC.Close;
  QrRiscoTC.Close;
  QrDOpen.Close;
end;

procedure TFmRiscoCli.EdColigadoChange(Sender: TObject);
begin
  DesfazPesquisa;
end;

procedure TFmRiscoCli.EdEmpresaChange(Sender: TObject);
begin
{
  DModG.All_ABD(
    TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto, ?TabLctA, ?TabLctB, ?TabLctD);
}
end;

procedure TFmRiscoCli.ReopenRiscoC();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Risco cheques: Itens');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRiscoC, Dmod.MyDB, [
  'SELECT lo.Cliente, lo.Tipo, li.Banco, li.Agencia, li.ContaCorrente, ',
  'li.Documento, li.Credito, li.DCompra, li.DDeposito, ',
  'li.Emitente, li.CNPJCPF, li.Data, li.Vencimento, li.FatParcela ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  //
   Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits ri ON ri.Origem   = li.FatParcela  ',
  'LEFT JOIN repas    re ON re.Codigo   = ri.Codigo  ']),
   //
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0 ',
  'AND lo.TxCompra + lo.ValValorem + ' +
        IntToStr(FmPrincipal.FConnections) + ' >= 0.01 ',
  'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE()) ',
   //
  Geral.ATS_if(FCliente <> 0, [
  'AND lo.Cliente=' + FormatFloat('0', FCliente)]),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  'ORDER BY DDeposito ',
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Pesquisando em tabelas. Risco cheques: Totais');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRiscoTC, Dmod.MyDB, [
  'SELECT SUM(li.Credito) Valor',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  //
   Geral.ATS_if(FColigado <> 0, [
  'LEFT JOIN repasits ri ON ri.Origem   = li.FatParcela  ',
  'LEFT JOIN repas    re ON re.Codigo   = ri.Codigo  ']),
   //
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0 ',
  'AND lo.TxCompra + lo.ValValorem + ' +
        IntToStr(FmPrincipal.FConnections) + ' >= 0.01 ',
  'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE()) ',
   //
  Geral.ATS_if(FCliente <> 0, [
  'AND lo.Cliente=' + FormatFloat('0', FCliente)]),
  //
  Geral.ATS_if(FColigado <> 0, [
  'AND re.Coligado=' + FormatFloat('0', FColigado)]),
  //
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmRiscoCli.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmRiscoCli.BtCancelaClick(Sender: TObject);
begin
  FParar := True;
end;

function TFmRiscoCli.ParaImpressao: Boolean;
begin
  BtCancela.Enabled := False;
  Progress1.Visible := False;
  Result := True;
end;

procedure TFmRiscoCli.QrRisco1CalcFields(DataSet: TDataSet);
var
  Liga: String;
begin
  QrRisco1CONTA_DUPLICATA.Value :=
    QrRisco1Conta.Value + QrRisco1Duplicata.Value;
  QrRisco1DEMISSAO_TXT.Value   := Geral.FDT(QrRisco1DEmissao.Value, 3);
  QrRisco1DCOMPRA_TXT.Value    := Geral.FDT(QrRisco1DCompra.Value, 3);
  QrRisco1DVENCE_TXT.Value     := Geral.FDT(QrRisco1DVence.Value, 3);
  QrRisco1DDEPOSITO_TXT.Value  := Geral.FDT(QrRisco1DDeposito.Value, 3);
  QrRisco1DDEVOL1_TXT.Value    := Geral.FDT(QrRisco1DDevol1.Value, 3);
  QrRisco1DDEVOL2_TXT.Value    := Geral.FDT(QrRisco1DDevol2.Value, 3);
  QrRisco1ULTPAGTO_TXT.Value   := Geral.FDT(QrRisco1UltPagto.Value, 3);
////////////////////////////////////////////////////////////////////////////////
  case QrRisco1Tipo.Value of
    10: QrRisco1TIPODOC.Value := 'CH CART';
    20: QrRisco1TIPODOC.Value := 'CH DEVO';
    30: QrRisco1TIPODOC.Value := 'DU CART';
    40: QrRisco1TIPODOC.Value := 'DU DEVO';
    50: QrRisco1TIPODOC.Value := 'CL OCOR';
    60: QrRisco1TIPODOC.Value := 'CH OCOR';
    70: QrRisco1TIPODOC.Value := 'DU OCOR';
  end;
////////////////////////////////////////////////////////////////////////////////
  QrRisco1CONTAGEM.Value := 1;
////////////////////////////////////////////////////////////////////////////////
  if (QrRisco1Status.Value <> '') and (QrRisco1Historico.Value <> '') then
    Liga := ' - ' else Liga := '';
  QrRisco1STATUS_HISTORICO.Value := QrRisco1Status.Value + Liga +
    QrRisco1Historico.Value;
end;

procedure TFmRiscoCli.frRisco1GetValue(const ParName: String;
  var ParValue: Variant);
begin
  if ParName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then ParValue := ' ' else
    ParValue := CBCliente.Text;
  end else if ParName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then ParValue := 'TODOS CLIENTES' else
    ParValue := ' ';
  end else if ParName = 'VARF_QTD_CHEQUES' then ParValue := Progress1.Max
  else if ParName = 'VFR_LA1NOME' then
    ParValue := ImpressaoDeCabecalho(RGOrdem1.ItemIndex)
  else if ParName = 'VFR_LA2NOME' then
    ParValue := ImpressaoDeCabecalho(RGOrdem2.ItemIndex)
  else if ParName = 'VFR_LA3NOME' then
    ParValue := ImpressaoDeCabecalho(RGOrdem3.ItemIndex)
  else if ParName = 'VARF_SITUACOES' then
  begin
    Parvalue := ' ';
    if Ck10.Checked then ParValue := ParValue + '[' + Ck10.Caption + ']';
    if Ck20.Checked then ParValue := ParValue + '[' + Ck20.Caption + ']';
    if Ck30.Checked then ParValue := ParValue + '[' + Ck30.Caption + ']';
    if Ck40.Checked then ParValue := ParValue + '[' + Ck40.Caption + ']';
    if Ck50.Checked then ParValue := ParValue + '[' + Ck50.Caption + ']';
    if Ck60.Checked then ParValue := ParValue + '[' + Ck60.Caption + ']';
    if Ck70.Checked then ParValue := ParValue + '[' + Ck70.Caption + ']';
    //if Ck00.Checked then ParValue := ParValue + ' (Resumido)';
    if Trim(ParValue) = '' then ParValue := '(??????)';
  end
  else if ParName = 'VARF_FILTROS' then
  begin
    ParValue := '';
    if EdColigado.ValueVariant <> 0 then ParValue := ParValue +
      'Coligado: '+CBColigado.Text+ '  ';
    if ParValue <> '' then ParValue := 'FILTROS : '+ParValue;
  end;
end;

procedure TFmRiscoCli.frRisco1UserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
       if Name = 'VFR_ORD1' then Val := RGAgrupa.ItemIndex >= 1
  else if Name = 'VFR_ORD2' then Val := RGAgrupa.ItemIndex >= 2
  else if Name = 'VFR_ORD3' then Val := RGAgrupa.ItemIndex >= 3
  else if Name = 'VFR_IDX1' then Val := RGOrdem1.ItemIndex
  else if Name = 'VFR_IDX2' then Val := RGOrdem2.ItemIndex
  else if Name = 'VFR_IDX3' then Val := RGOrdem3.ItemIndex
  else if Name = 'VFR_CON1' then Val := CondicaoDeImpressao(RGOrdem1.ItemIndex)
  else if Name = 'VFR_CON2' then Val := CondicaoDeImpressao(RGOrdem2.ItemIndex)
  else if Name = 'VFR_CON3' then Val := CondicaoDeImpressao(RGOrdem3.ItemIndex)

end;

function TFmRiscoCli.ImpressaoDeCabecalho(Tipo: Integer): String;
begin
  case Tipo of
    0: Result := 'Cliente: '+QrRisco1NOMECLIENTE.Value;
    1: Result := DescricaoDeDocumento(QrRisco1Tipo.Value);
    2: Result := 'Vencimento: '+QrRisco1DVENCE_TXT.Value;
    3: Result := 'Status: '+QrRisco1Status.Value;
  end;
end;

function TFmRiscoCli.CondicaoDeImpressao(Tipo: Integer): String;
begin     
  case Tipo of
    0: Result := QuotedStr('[QrRisco1."NOMECLIENTE"]');
    1: Result := QuotedStr('[QrRisco1."TIPODOC"]');
    2: Result := QuotedStr('[QrRisco1."DVENCE_TXT"]');
    3: Result := QuotedStr('[QrRisco1."Status"]');
  end;
end;

function TFmRiscoCli.Ordem(Tipo: Integer): String;
begin
  case Tipo of
    0: Result := 'NOMECLIENTE';
    1: Result := 'Tipo';
    2: Result := 'DVence';
    3: Result := 'Status';
  end;
end;

function TFmRiscoCli.DescricaoDeDocumento(Tipo: Integer): String;
begin
  case Tipo of
    10: Result := 'Cheques em carteira';
    20: Result := 'Cheques devolvidos';
    30: Result := 'Duplicatas em carteira';
    40: Result := 'Duplicatas vencidas';
    50: Result := 'Ocorr�ncias em clientes';
    60: Result := 'Ocorr�ncias de cheques';
    70: Result := 'Ocorr�ncias de duplicatas';
    else Result := '??????????????????????????????????????????';
  end;
end;

procedure TFmRiscoCli.BtGeraClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Risco cheques');
    FParar := False;
    BtCancela.Enabled := True;
    Progress1.Position := 0;
    Progress1.Visible := True;
    Progress1.Max := QrRiscoC.RecordCount + QrCHDevA.RecordCount +
      QrDOpen.RecordCount + QrOcorA.RecordCount;
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM risco');
    Dmod.QrUpdL.ExecSQL;
    //
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO risco SET Tipo=:P0, Banco=:P1, Agencia=:P2, ');
    Dmod.QrUpdL.SQL.Add('Conta=:P3, Duplicata=:P4, Cheque=:P5, DEmissao=:P6, ');
    Dmod.QrUpdL.SQL.Add('DCompra=:P7, DVence=:P8, DDeposito=:P9, DDevol1=:P10, ');
    Dmod.QrUpdL.SQL.Add('DDevol2=:P11, UltPagto=:P12, Valor=:P13, Taxas=:P14, ');
    Dmod.QrUpdL.SQL.Add('Saldo=:P15, Pago=:P16, Atual=:P17, Emitente=:P18, ');
    Dmod.QrUpdL.SQL.Add('CPF=:P19, Alinea1=:P20, Alinea2=:P21, Status=:P22, ');
    Dmod.QrUpdL.SQL.Add('Historico=:P23, Desco=:P24, Juros=:P25, Cliente=:P26, ');
    Dmod.QrUpdL.SQL.Add('Controle=:P27');
    QrRiscoC.First;
    while not QrRiscoC.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      Dmod.QrUpdL.Params[00].AsInteger := 10;
      Dmod.QrUpdL.Params[01].AsInteger := QrRiscoCBanco.Value;
      Dmod.QrUpdL.Params[02].AsInteger := QrRiscoCAgencia.Value;
      Dmod.QrUpdL.Params[03].AsString  := QrRiscoCContaCorrente.Value;
      Dmod.QrUpdL.Params[04].AsString  := '';  //  Duplicata QrRiscoC.Value
      Dmod.QrUpdL.Params[05].AsInteger := Trunc(QrRiscoCDocumento.Value);
      Dmod.QrUpdL.Params[06].AsString  := Geral.FDT(QrRiscoCData.Value, 1);
      Dmod.QrUpdL.Params[07].AsString  := Geral.FDT(QrRiscoCDCompra.Value, 1);
      Dmod.QrUpdL.Params[08].AsString  := Geral.FDT(QrRiscoCVencimento.Value, 1);
      Dmod.QrUpdL.Params[09].AsString  := Geral.FDT(QrRiscoCDDeposito.Value, 1);
      Dmod.QrUpdL.Params[10].AsString  := '';
      Dmod.QrUpdL.Params[11].AsString  := '';
      Dmod.QrUpdL.Params[12].AsString  := '';
      Dmod.QrUpdL.Params[13].AsFloat   := QrRiscoCCredito.Value;
      Dmod.QrUpdL.Params[14].AsFloat   := 0;//Taxas.Value;
      Dmod.QrUpdL.Params[15].AsFloat   := QrRiscoCCredito.Value;//Saldo.Value;
      Dmod.QrUpdL.Params[16].AsFloat   := 0;//Pago.Value;
      Dmod.QrUpdL.Params[17].AsFloat   := QrRiscoCCredito.Value;//Atual.Value;
      Dmod.QrUpdL.Params[18].AsString  := QrRiscoCEmitente.Value;
      Dmod.QrUpdL.Params[19].AsString  := QrRiscoCCPF_TXT.Value;
      Dmod.QrUpdL.Params[20].AsInteger := 0;
      Dmod.QrUpdL.Params[21].AsInteger := 0;
      Dmod.QrUpdL.Params[22].AsString  := ''; //Status
      Dmod.QrUpdL.Params[23].AsString  := ''; //Historico
      Dmod.QrUpdL.Params[24].AsFloat   := 0;
      Dmod.QrUpdL.Params[25].AsFloat   := 0;
      Dmod.QrUpdL.Params[26].AsInteger := QrRiscoCCliente.Value;
      Dmod.QrUpdL.Params[27].AsInteger := QrRiscoCFatParcela.Value;
      Dmod.QrUpdL.ExecSQL;
      QrRiscoC.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Cheques devolvidos');
    QrCHDevA.First;
    while not QrCHDevA.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      Dmod.QrUpdL.Params[00].AsInteger := 20;
      Dmod.QrUpdL.Params[01].AsInteger := QrCHDevABanco.Value;
      Dmod.QrUpdL.Params[02].AsInteger := QrCHDevAAgencia.Value;
      Dmod.QrUpdL.Params[03].AsString  := QrCHDevAConta.Value;
      Dmod.QrUpdL.Params[04].AsString  := '';  //  Duplicata QrCHDevA.Value
      Dmod.QrUpdL.Params[05].AsInteger := QrCHDevACheque.Value;
      Dmod.QrUpdL.Params[06].AsString  := '';//Geral.FDT(QrCHDevAEmissao.Value, 1);
      Dmod.QrUpdL.Params[07].AsString  := '';//Geral.FDT(QrCHDevADCompra.Value, 1);
      Dmod.QrUpdL.Params[08].AsString  := '';//Geral.FDT(QrCHDevAVencto.Value, 1);
      Dmod.QrUpdL.Params[09].AsString  := '';//Geral.FDT(QrCHDevADDeposito.Value, 1);
      Dmod.QrUpdL.Params[10].AsString  := Geral.FDT(QrCHDevAData1.Value, 1);
      Dmod.QrUpdL.Params[11].AsString  := Geral.FDT(QrCHDevAData2.Value, 1);
      Dmod.QrUpdL.Params[12].AsString  := Geral.FDT(QrCHDevAData3.Value, 1);
      Dmod.QrUpdL.Params[13].AsFloat   := QrCHDevAValor.Value;
      Dmod.QrUpdL.Params[14].AsFloat   := QrCHDevATaxas.Value;
      Dmod.QrUpdL.Params[15].AsFloat   := QrCHDevASaldo.Value;
      Dmod.QrUpdL.Params[16].AsFloat   := QrCHDevAValPago.Value;
      Dmod.QrUpdL.Params[17].AsFloat   := QrCHDevAAtual.Value;
      Dmod.QrUpdL.Params[18].AsString  := QrCHDevAEmitente.Value;
      Dmod.QrUpdL.Params[19].AsString  := QrCHDevACPF_TXT.Value;
      Dmod.QrUpdL.Params[20].AsInteger := QrCHDevAAlinea1.Value;
      Dmod.QrUpdL.Params[21].AsInteger := QrCHDevAAlinea2.Value;
      Dmod.QrUpdL.Params[22].AsString  := ''; //Status
      Dmod.QrUpdL.Params[23].AsString  := ''; //Historico
      Dmod.QrUpdL.Params[24].AsFloat   := 0;
      Dmod.QrUpdL.Params[25].AsFloat   := 0;
      Dmod.QrUpdL.Params[26].AsInteger := QrCHDevACliente.Value;
      Dmod.QrUpdL.Params[27].AsInteger := QrCHDevACodigo.Value;
      Dmod.QrUpdL.ExecSQL;
      QrCHDevA.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Duplicatas abertas');
    QrDOpen.First;
    while not QrDOpen.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      if Int(Date) > QrDOpenVencimento.Value then
        Dmod.QrUpdL.Params[00].AsInteger := 40
      else
        Dmod.QrUpdL.Params[00].AsInteger := 30;
      //Dmod.QrUpdL.Params[00].AsInteger := 1;
      Dmod.QrUpdL.Params[01].AsInteger := QrDOpenBanco.Value;
      Dmod.QrUpdL.Params[02].AsInteger := QrDOpenAgencia.Value;
      Dmod.QrUpdL.Params[03].AsString  := '';//QrDOpenConta.Value;
      Dmod.QrUpdL.Params[04].AsString  := QrDOpenDuplicata.Value;
      Dmod.QrUpdL.Params[05].AsInteger := 0;//Cheque.Value;
      Dmod.QrUpdL.Params[06].AsString  := Geral.FDT(QrDOpenData.Value, 1);
      Dmod.QrUpdL.Params[07].AsString  := Geral.FDT(QrDOpenDCompra.Value, 1);
      Dmod.QrUpdL.Params[08].AsString  := Geral.FDT(QrDOpenVencimento.Value, 1);
      Dmod.QrUpdL.Params[09].AsString  := Geral.FDT(QrDOpenDDeposito.Value, 1);
      Dmod.QrUpdL.Params[10].AsString  := Geral.FDT(QrDOpenVencimento.Value, 1);
      Dmod.QrUpdL.Params[11].AsString  := '';//Geral.FDT(QrDOpenData2.Value, 1);
      Dmod.QrUpdL.Params[12].AsString  := Geral.FDT(QrDOpenData3.Value, 1);
      Dmod.QrUpdL.Params[13].AsFloat   := QrDOpenCredito.Value;
      Dmod.QrUpdL.Params[14].AsFloat   := 0;
      Dmod.QrUpdL.Params[15].AsFloat   := QrDOpenSALDO_DESATUALIZ.Value;
      Dmod.QrUpdL.Params[16].AsFloat   := QrDOpenTotalPg.Value;
      Dmod.QrUpdL.Params[17].AsFloat   := QrDOpenSALDO_ATUALIZADO.Value;
      Dmod.QrUpdL.Params[18].AsString  := QrDOpenEmitente.Value;
      Dmod.QrUpdL.Params[19].AsString  := Geral.FormataCNPJ_TT(QrDOpenCNPJCPF.Value);
      Dmod.QrUpdL.Params[20].AsInteger := 0;//QrDOpenAlinea1.Value;
      Dmod.QrUpdL.Params[21].AsInteger := 0;//QrDOpenAlinea2.Value;
      Dmod.QrUpdL.Params[22].AsString  := QrDOpenNOMESTATUS.Value;
      Dmod.QrUpdL.Params[23].AsString  := QrDOpenSTATUS.Value;
      Dmod.QrUpdL.Params[24].AsFloat   := QrDOpenTotalDs.Value;
      Dmod.QrUpdL.Params[25].AsFloat   := QrDOpenTotalJr.Value;
      Dmod.QrUpdL.Params[26].AsInteger := QrDOpenCliente.Value;
      Dmod.QrUpdL.Params[27].AsInteger := QrDOpenFatParcela.Value;
      Dmod.QrUpdL.ExecSQL;
      QrDOpen.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Gerando registros: Ocorr�ncias');
    QrOcorA.First;
    while not QrOcorA.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      Application.ProcessMessages;
      if FParar then if ParaImpressao then Exit;
      if QrOcorATIPODOC.Value = 'CL' then Dmod.QrUpdL.Params[00].AsInteger := 50 else
      if QrOcorATIPODOC.Value = 'CH' then Dmod.QrUpdL.Params[00].AsInteger := 60 else
      if QrOcorATIPODOC.Value = 'DU' then Dmod.QrUpdL.Params[00].AsInteger := 70;
      Dmod.QrUpdL.Params[01].AsInteger := QrOcorABanco.Value;
      Dmod.QrUpdL.Params[02].AsInteger := QrOcorAAgencia.Value;
      Dmod.QrUpdL.Params[03].AsString  := QrOcorAConta.Value;
      Dmod.QrUpdL.Params[04].AsString  := QrOcorADuplicata.Value;
      Dmod.QrUpdL.Params[05].AsInteger := QrOcorACheque.Value;
      Dmod.QrUpdL.Params[06].AsString  := Geral.FDT(QrOcorAEmissao.Value, 1);
      Dmod.QrUpdL.Params[07].AsString  := Geral.FDT(QrOcorADCompra.Value, 1);
      Dmod.QrUpdL.Params[08].AsString  := Geral.FDT(QrOcorAVencto.Value, 1);
      Dmod.QrUpdL.Params[09].AsString  := Geral.FDT(QrOcorADDeposito.Value, 1);
      Dmod.QrUpdL.Params[10].AsString  := Geral.FDT(QrOcorADataO.Value, 1);
      Dmod.QrUpdL.Params[11].AsString  := '';//Geral.FDT(QrOcorAData2.Value, 1);
      Dmod.QrUpdL.Params[12].AsString  := Geral.FDT(QrOcorAData3.Value, 1);
      Dmod.QrUpdL.Params[13].AsFloat   := QrOcorAValor.Value;
      Dmod.QrUpdL.Params[14].AsFloat   := QrOcorATaxaV.Value;
      Dmod.QrUpdL.Params[15].AsFloat   := QrOcorASALDO.Value;
      Dmod.QrUpdL.Params[16].AsFloat   := QrOcorAPago.Value;
      Dmod.QrUpdL.Params[17].AsFloat   := QrOcorAATUALIZADO.Value;
      Dmod.QrUpdL.Params[18].AsString  := QrOcorAEmitente.Value;
      Dmod.QrUpdL.Params[19].AsString  := Geral.FormataCNPJ_TT(QrOcorACPF.Value);
      Dmod.QrUpdL.Params[20].AsInteger := 0;//QrOcorAAlinea1.Value;
      Dmod.QrUpdL.Params[21].AsInteger := 0;//QrOcorAAlinea2.Value;
      Dmod.QrUpdL.Params[22].AsString  := '';//QrOcorANOMESTATUS.Value;
      Dmod.QrUpdL.Params[23].AsString  := QrOcorANOMEOCORRENCIA.Value;//QrOcorASTATUS.Value;
      Dmod.QrUpdL.Params[24].AsFloat   := 0;//QrOcorATotalDs.Value;
      Dmod.QrUpdL.Params[25].AsFloat   := 0;//QrOcorATotalJr.Value;
      Dmod.QrUpdL.Params[26].AsInteger := QrOcorACliente.Value;
      Dmod.QrUpdL.Params[27].AsInteger := QrOcorACodigo.Value;
      Dmod.QrUpdL.ExecSQL;
      QrOcorA.Next;
    end;
    Progress1.Visible := False;
    BtImprime.Enabled := True;
    BtGera.Enabled    := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  finally
    BtCancela.Enabled := False;
    Screen.Cursor     := crDefault;
  end;
end;

procedure TFmRiscoCli.Descritivo1Click(Sender: TObject);
begin
  DefineTipos;
  QrRisco1.Close;
  QrRisco1.SQL.Clear;
  QrRisco1.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrRisco1.SQL.Add('ELSE en.Nome END NOMECLIENTE, ri.*');
  QrRisco1.SQL.Add('FROM risco ri');
  QrRisco1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades en ON ri.Cliente=en.Codigo');
  QrRisco1.SQL.Add('WHERE ri.Tipo in (:P10, :P20, :P30, :P40, :P50, :P60, :P70)');
  QrRisco1.SQL.Add('ORDER BY '+Ordem(RGOrdem1.ItemIndex)+', '+
  Ordem(RGOrdem2.ItemIndex)+', '+Ordem(RGOrdem3.ItemIndex));
  QrRisco1.Params[00].AsInteger := FR10;
  QrRisco1.Params[01].AsInteger := FR20;
  QrRisco1.Params[02].AsInteger := FR30;
  QrRisco1.Params[03].AsInteger := FR40;
  QrRisco1.Params[04].AsInteger := FR50;
  QrRisco1.Params[05].AsInteger := FR60;
  QrRisco1.Params[06].AsInteger := FR70;
  UMyMod.AbreQuery(QrRisco1, Dmod.MyLocDatabase);
  MyObjects.frxMostra(frxRisco1, 'Risco Descritivo');
end;

procedure TFmRiscoCli.Analtico1Click(Sender: TObject);
begin
  DefineTipos;
  QrAnalitico.Close;
  QrAnalitico.SQL.Clear;
  QrAnalitico.SQL.Add('SELECT ri.Cliente, ri.Tipo, SUM(ri.Valor) Valor,');
  QrAnalitico.SQL.Add('SUM(ri.Taxas) Taxas, SUM(ri.Saldo) Saldo, SUM(ri.Juros) Juros,');
  QrAnalitico.SQL.Add('SUM(ri.Desco) Desco, SUM(ri.Pago) Pago, SUM(ri.Atual) Atual,');
  QrAnalitico.SQL.Add('COUNT(ri.Controle) ITENS,');
  QrAnalitico.SQL.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMECLI');
  QrAnalitico.SQL.Add('FROM risco ri');
  QrAnalitico.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades en ON en.Codigo=ri.Cliente');
  QrAnalitico.SQL.Add('WHERE ri.Tipo in (:P10, :P20, :P30, :P40, :P50, :P60, :P70)');
  QrAnalitico.SQL.Add('GROUP BY NOMECLI, ri.Tipo');
  QrAnalitico.Params[00].AsInteger := FR10;
  QrAnalitico.Params[01].AsInteger := FR20;
  QrAnalitico.Params[02].AsInteger := FR30;
  QrAnalitico.Params[03].AsInteger := FR40;
  QrAnalitico.Params[04].AsInteger := FR50;
  QrAnalitico.Params[05].AsInteger := FR60;
  QrAnalitico.Params[06].AsInteger := FR70;
  UMyMod.AbreQuery(QrAnalitico, Dmod.MyLocDatabase);
  MyObjects.frxMostra(frxAnalitico, 'Risco Anal�tico');
end;

procedure TFmRiscoCli.QrAnaliticoCalcFields(DataSet: TDataSet);
begin
  QrAnaliticoDESCRITIPO.Value := DescricaoDeDocumento(QrAnaliticoTipo.Value);
end;

procedure TFmRiscoCli.Sinttico1Click(Sender: TObject);
begin
  DefineTipos;
  QrSintetico.Close;
  QrSintetico.SQL.Clear;
  QrSintetico.SQL.Add('SELECT ri.Cliente, ri.Tipo, SUM(ri.Valor) Valor,');
  QrSintetico.SQL.Add('SUM(ri.Taxas) Taxas, SUM(ri.Saldo) Saldo, SUM(ri.Juros) Juros,');
  QrSintetico.SQL.Add('SUM(ri.Desco) Desco, SUM(ri.Pago) Pago, SUM(ri.Atual) Atual,');
  QrSintetico.SQL.Add('COUNT(ri.Controle) ITENS,');
  // 2011-11-19
  QrSintetico.SQL.Add('SUM(ri.Devolvido) Devolvido, en.LimiCred,');
  QrSintetico.SQL.Add('SUM(ri.Atual) / en.LimiCred * 100 ULO,');
  // fim 2011-11-19
  QrSintetico.SQL.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMECLI');
  QrSintetico.SQL.Add('FROM risco ri');
  QrSintetico.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades en ON en.Codigo=ri.Cliente');
  QrSintetico.SQL.Add('WHERE ri.Tipo in (:P10, :P20, :P30, :P40, :P50, :P60, :P70)');
  QrSintetico.SQL.Add('GROUP BY NOMECLI');
  QrSintetico.Params[00].AsInteger := FR10;
  QrSintetico.Params[01].AsInteger := FR20;
  QrSintetico.Params[02].AsInteger := FR30;
  QrSintetico.Params[03].AsInteger := FR40;
  QrSintetico.Params[04].AsInteger := FR50;
  QrSintetico.Params[05].AsInteger := FR60;
  QrSintetico.Params[06].AsInteger := FR70;
  UMyMod.AbreQuery(QrSintetico, Dmod.MyLocDatabase);
  MyObjects.frxMostra(frxSintetico, 'Risco Sint�tico');
end;

procedure TFmRiscoCli.DefineTipos;
begin
  FR10 := MLAGeral.BoolToInt2(Ck10.Checked, 10, -1000);
  FR20 := MLAGeral.BoolToInt2(Ck20.Checked, 20, -1000);
  FR30 := MLAGeral.BoolToInt2(Ck30.Checked, 30, -1000);
  FR40 := MLAGeral.BoolToInt2(Ck40.Checked, 40, -1000);
  FR50 := MLAGeral.BoolToInt2(Ck50.Checked, 50, -1000);
  FR60 := MLAGeral.BoolToInt2(Ck60.Checked, 60, -1000);
  FR70 := MLAGeral.BoolToInt2(Ck70.Checked, 70, -1000);
end;

procedure TFmRiscoCli.frxRisco1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then Value := ' ' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS CLIENTES' else
    Value := ' ';
  end else if VarName = 'VARF_QTD_CHEQUES' then Value := Progress1.Max
  else if VarName = 'VFR_LA1NOME' then
    Value := ImpressaoDeCabecalho(RGOrdem1.ItemIndex)
  else if VarName = 'VFR_LA2NOME' then
    Value := ImpressaoDeCabecalho(RGOrdem2.ItemIndex)
  else if VarName = 'VFR_LA3NOME' then
    Value := ImpressaoDeCabecalho(RGOrdem3.ItemIndex)
  else if VarName = 'VARF_SITUACOES' then
  begin
    Value := ' ';
    if Ck10.Checked then Value := Value + '[' + Ck10.Caption + ']';
    if Ck20.Checked then Value := Value + '[' + Ck20.Caption + ']';
    if Ck30.Checked then Value := Value + '[' + Ck30.Caption + ']';
    if Ck40.Checked then Value := Value + '[' + Ck40.Caption + ']';
    if Ck50.Checked then Value := Value + '[' + Ck50.Caption + ']';
    if Ck60.Checked then Value := Value + '[' + Ck60.Caption + ']';
    if Ck70.Checked then Value := Value + '[' + Ck70.Caption + ']';
    //if Ck00.Checked then Value := Value + ' (Resumido)';
    if Trim(Value) = '' then Value := '(??????)';
  end
  else if VarName = 'VARF_FILTROS' then
  begin
    Value := '';
    if EdColigado.ValueVariant <> 0 then Value := Value +
      'Coligado: '+CBColigado.Text+ '  ';
    //if FEmitente <> '' then Value := Value + '  {Emitente: '+ FEmitente+'}';
    //if FCPF <> '' then Value := Value + '  {CPF/CNPJ: '+
    //Geral.FormataCNPJ_TT(FCPF)+'}';
    if Value <> '' then Value := 'FILTROS : '+Value;
  end

  // user function


  else if VarName = 'VFR_ORD1' then Value := RGAgrupa.ItemIndex >= 1
  else if VarName = 'VFR_ORD2' then Value := RGAgrupa.ItemIndex >= 2
  else if VarName = 'VFR_ORD3' then Value := RGAgrupa.ItemIndex >= 3
  else if VarName = 'VFR_IDX1' then Value := RGOrdem1.ItemIndex
  else if VarName = 'VFR_IDX2' then Value := RGOrdem2.ItemIndex
  else if VarName = 'VFR_IDX3' then Value := RGOrdem3.ItemIndex
  else if VarName = 'VFR_CON1' then Value := CondicaoDeImpressao(RGOrdem1.ItemIndex)
  else if VarName = 'VFR_CON2' then Value := CondicaoDeImpressao(RGOrdem2.ItemIndex)
  else if VarName = 'VFR_CON3' then Value := CondicaoDeImpressao(RGOrdem3.ItemIndex)

end;

end.

