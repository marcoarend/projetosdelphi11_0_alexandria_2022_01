object FmOperaImp: TFmOperaImp
  Left = 419
  Top = 217
  Caption = 'OPE-GEREN-003 :: Impress'#227'o de Operac'#245'es'
  ClientHeight = 413
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 657
    Height = 234
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 597
    object Panel1: TPanel
      Left = 0
      Top = 53
      Width = 657
      Height = 137
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel1'
      TabOrder = 0
      ExplicitWidth = 597
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 432
        Height = 137
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 0
        object RGPeriodo: TRadioGroup
          Left = 0
          Top = 0
          Width = 432
          Height = 137
          Align = alClient
          Caption = ' Configura'#231#227'o inicial do per'#237'odo: '
          Items.Strings = (
            #218'ltimos sete dias.'
            'Semanal:'
            'Decendial')
          TabOrder = 0
        end
        object CBDiaSemanaI: TComboBox
          Left = 76
          Top = 61
          Width = 173
          Height = 21
          Hint = 'Nome da fonte|Seleciona o nome da fonte'
          Ctl3D = False
          DropDownCount = 10
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
        end
        object StaticText1: TStaticText
          Left = 76
          Top = 45
          Width = 52
          Height = 13
          AutoSize = False
          Caption = 'Dia inicial:'
          TabOrder = 2
        end
        object CBDiaSemanaF: TComboBox
          Left = 252
          Top = 61
          Width = 173
          Height = 21
          Hint = 'Nome da fonte|Seleciona o nome da fonte'
          Ctl3D = False
          DropDownCount = 10
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 3
        end
        object StaticText2: TStaticText
          Left = 252
          Top = 45
          Width = 45
          Height = 13
          AutoSize = False
          Caption = 'Dia final:'
          TabOrder = 4
        end
      end
      object Panel3: TPanel
        Left = 432
        Top = 0
        Width = 225
        Height = 137
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GBCorrige: TGroupBox
          Left = 5
          Top = 6
          Width = 185
          Height = 125
          TabOrder = 0
          Visible = False
          object BtCorrige: TBitBtn
            Left = 7
            Top = 75
            Width = 120
            Height = 40
            Caption = '&Corrige'
            TabOrder = 0
            OnClick = BtCorrigeClick
            NumGlyphs = 2
          end
          object CkPIS: TCheckBox
            Left = 7
            Top = 15
            Width = 97
            Height = 17
            Caption = 'PIS'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object CkCOFINS: TCheckBox
            Left = 7
            Top = 35
            Width = 97
            Height = 17
            Caption = 'COFINS'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object CkISS: TCheckBox
            Left = 7
            Top = 55
            Width = 97
            Height = 17
            Caption = 'ISS'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
        end
        object CkCorrige: TCheckBox
          Left = 20
          Top = 0
          Width = 55
          Height = 17
          Caption = 'Corrige'
          TabOrder = 1
          OnClick = CkCorrigeClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 657
      Height = 53
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 597
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label34: TLabel
        Left = 384
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label1: TLabel
        Left = 488
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 24
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CbCliente
      end
      object CbCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 24
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
      end
      object TPIni: TDateTimePicker
        Left = 384
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 2
      end
      object TPFim: TDateTimePicker
        Left = 488
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 3
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 190
      Width = 657
      Height = 44
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      ExplicitWidth = 597
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 657
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 597
    object GB_R: TGroupBox
      Left = 609
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 549
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 561
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 501
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 306
        Height = 32
        Caption = 'Impress'#227'o de Operac'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 306
        Height = 32
        Caption = 'Impress'#227'o de Operac'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 306
        Height = 32
        Caption = 'Impress'#227'o de Operac'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 282
    Width = 657
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 597
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 653
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 593
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 343
    Width = 657
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 597
    object PnSaiDesis: TPanel
      Left = 511
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 451
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 509
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 449
      object BtImprime: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtImprimeClick
        NumGlyphs = 2
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 326
    Width = 657
    Height = 17
    Align = alBottom
    TabOrder = 4
    Visible = False
    ExplicitWidth = 597
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 288
    Top = 64
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 316
    Top = 64
  end
  object QrOpera: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOperaCalcFields
    SQL.Strings = (
      'SELECT lo.*, IF(en.Tipo=0, en.RazaoSocial, en.Nome)NOMECLIENTE, '
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJCPF'
      'FROM lot es lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'ORDER BY lo.Data, NOMECLIENTE, lo.Lote')
    Left = 176
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOperaNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrOperaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOperaTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrOperaSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrOperaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOperaLote: TSmallintField
      FieldName = 'Lote'
      Required = True
    end
    object QrOperaData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrOperaTotal: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrOperaDias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrOperaPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrOperaTxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
    end
    object QrOperaValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrOperaAdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrOperaIOC: TFloatField
      FieldName = 'IOC'
      Required = True
    end
    object QrOperaCPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
    end
    object QrOperaTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrOperaIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrOperaIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrOperaISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrOperaISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrOperaPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrOperaPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrOperaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOperaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOperaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOperaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOperaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOperaTarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrOperaOcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrOperaCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrOperaCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
    object QrOperaPIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrOperaPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrOperaCOFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrOperaCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrOperaPIS_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PIS_TOTAL'
      Calculated = True
    end
    object QrOperaCOFINS_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COFINS_TOTAL'
      Calculated = True
    end
    object QrOperaIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrOperaCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
    end
    object QrOperaMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrOperaCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
    end
    object QrOperaMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrOperaMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrOperaMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrOperaPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
    end
    object QrOperaCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
    end
    object QrOperaPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
    end
    object QrOperaDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
    end
    object QrOperaNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrOperaItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrOperaCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrOperaIOFd_VAL: TFloatField
      FieldName = 'IOFd_VAL'
      Required = True
    end
    object QrOperaIOFv_VAL: TFloatField
      FieldName = 'IOFv_VAL'
      Required = True
    end
    object QrOperaTipoIOF: TSmallintField
      FieldName = 'TipoIOF'
      Required = True
    end
    object QrOperaCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrOperaIOF_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'IOF_TOTAL'
      Calculated = True
    end
  end
  object frxOperacoes: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39451.850826631900000000
    ReportOptions.LastChange = 39451.850826631900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxOperacoesGetValue
    Left = 384
    Top = 120
    Datasets = <
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOperacoes
        DataSetName = 'frxDsOperacoes'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 1122.520410000000000000
        object Memo1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 143.622140000000000000
        Top = 98.267780000000000000
        Width = 1122.520410000000000000
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 34.015770000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'RELAT'#195#8220'RIO DAS OPERA'#195#8225#195#8226'ES REALIZADAS')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 982.677800000000000000
          Height = 22.677180000000000000
          ShowHint = False
        end
        object Memo3: TfrxMemoView
          Left = 128.504020000000000000
          Top = 56.692950000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Tipo de opera'#195#167#195#163'o:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 245.669450000000000000
          Top = 56.692950000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Compra de Direitos')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 627.401980000000000000
          Top = 56.692950000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Servi'#195#167'os executados:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 759.685530000000000000
          Top = 56.692950000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'An'#195#161'lise de Riscos e Liquidez')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 75.590600000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Cliente:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 117.165430000000000000
          Top = 83.149660000000000000
          Width = 721.890230000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 884.410020000000000000
          Top = 83.149660000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 839.055660000000000000
          Top = 83.149660000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Per'#195#173'odo:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 75.590600000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Data'
            'Opera'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 124.724490000000000000
          Top = 109.606370000000000000
          Width = 102.047310000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'CNPJ ou CPF'
            'Contratante')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 226.771800000000000000
          Top = 109.606370000000000000
          Width = 215.433210000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Cliente'
            'Contratante')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 442.205010000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'N'#194#186
            'Border'#195#180)
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 491.338900000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'N'#194#186
            'N.F.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 540.472790000000000000
          Top = 109.606370000000000000
          Width = 68.031540000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Valor'
            'Negociado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 608.504330000000000000
          Top = 109.606370000000000000
          Width = 56.692950000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Fator de'
            'Compra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 665.197280000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'Ad'
            'Valorem')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 714.331170000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsControle."Moeda"]'
            'PIS')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 763.465060000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsControle."Moeda"]'
            'ISS')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 812.598950000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsControle."Moeda"]'
            'COFINS')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 861.732840000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsControle."Moeda"]'
            'IOC')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 910.866730000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsControle."Moeda"]'
            'IOF')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 960.000620000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsControle."Moeda"]'
            'IOF adicio.')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 1009.134510000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            'TOTAL'
            '[frxDsControle."Moeda"] IOF')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 302.362400000000000000
        Width = 1122.520410000000000000
        DataSet = frxDsOperacoes
        DataSetName = 'frxDsOperacoes'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 75.590600000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsOperacoes."Data"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 124.724490000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'CNPJCPF_TXT'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsOperacoes."CNPJCPF_TXT"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 226.771800000000000000
          Width = 215.433210000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NOMECLIENTE'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            '[frxDsOperacoes."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 442.205010000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'000000'#39',<frxDsOperacoes."Lote">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 491.338900000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'000000'#39',<frxDsOperacoes."NF">)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 540.472790000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Total'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."Total"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 608.504330000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'MINTC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."MINTC"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 665.197280000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'MINAV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."MINAV"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 714.331170000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'PIS_T_Val'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."PIS_T_Val"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 763.465060000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'ISS_Val'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."ISS_Val"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 812.598950000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'COFINS_T_Val'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."COFINS_T_Val"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 861.732840000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IOC_VAL'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."IOC_VAL"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 910.866730000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IOFd_VAL'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."IOFd_VAL"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 960.000620000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IOFv_VAL'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."IOFv_VAL"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 1009.134510000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'IOF_TOTAL'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsOperacoes."IOF_TOTAL"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 71.811070000000000000
        Top = 343.937230000000000000
        Width = 1122.520410000000000000
        object Memo41: TfrxMemoView
          Left = 540.472790000000000000
          Top = 3.779530000000020000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."Total">,MasterData1)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 608.504330000000000000
          Top = 3.779530000000020000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."MINTC">,MasterData1)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 665.197280000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."MINAV">,MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 714.331170000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."PIS_T_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 763.465060000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."ISS_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 812.598950000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."COFINS_T_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 861.732840000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."IOC_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 910.866730000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."IOFd_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 960.000620000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."IOFv_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 1009.134510000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsOperacoes."IOF_TOTAL">,MasterData1)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000020000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8 = (
            ' TOTAL DA PESQUISA')
          ParentFont = False
        end
      end
    end
  end
  object frxDsOperacoes: TfrxDBDataset
    UserName = 'frxDsOperacoes'
    CloseDataSource = False
    DataSet = QrOpera
    BCDToCurrency = False
    Left = 412
    Top = 120
  end
end
