unit ModuleLot;

interface

uses
  Windows, Forms, Controls, Classes, SysUtils, DB, mySQLDbTables, dmkGeral,
  dmkEdit, Variants, DmkDAC_PF, UnDmkEnums;

type
  TDmLot = class(TDataModule)
    QrSPC_Cli: TmySQLQuery;
    QrSPC_CliSPC_ValMin: TFloatField;
    QrSPC_CliSPC_ValMax: TFloatField;
    QrSPC_CliServidor: TWideStringField;
    QrSPC_CliSPC_Porta: TSmallintField;
    QrSPC_CliPedinte: TWideStringField;
    QrSPC_CliCodigSocio: TIntegerField;
    QrSPC_CliModalidade: TIntegerField;
    QrSPC_CliInfoExtra: TIntegerField;
    QrSPC_CliBAC_CMC7: TSmallintField;
    QrSPC_CliTipoCred: TSmallintField;
    QrSPC_CliSenhaSocio: TWideStringField;
    QrSPC_CliCodigo: TIntegerField;
    QrSPC_CliValAviso: TFloatField;
    QrSPC_CliVALCONSULTA: TFloatField;
    QrSum0: TmySQLQuery;
    QrSum1: TmySQLQuery;
    QrSum1TaxaVal: TFloatField;
    QrSum2: TmySQLQuery;
    QrSum2TaxaVal: TFloatField;
    QrSum3: TmySQLQuery;
    QrSum3TaxaVal: TFloatField;
    QrSum4: TmySQLQuery;
    QrSum4TaxaVal: TFloatField;
    QrSum5: TmySQLQuery;
    QrSum5TaxaVal: TFloatField;
    QrSum6: TmySQLQuery;
    QrSum6TaxaVal: TFloatField;
    QrEmLot6: TmySQLQuery;
    QrEmLot6IRRF: TFloatField;
    QrEmLot6IRRF_Val: TFloatField;
    QrEmLot6ISS: TFloatField;
    QrEmLot6ISS_Val: TFloatField;
    QrEmLot6PIS_R: TFloatField;
    QrEmLot6PIS_R_Val: TFloatField;
    QrEmLot6TaxaVal: TFloatField;
    QrEmLot6ValValorem: TFloatField;
    QrEmLot6Codigo: TIntegerField;
    QrEmLot6AdValorem: TFloatField;
    QrEmLot5: TmySQLQuery;
    QrEmLot5IRRF: TFloatField;
    QrEmLot5IRRF_Val: TFloatField;
    QrEmLot5ISS: TFloatField;
    QrEmLot5ISS_Val: TFloatField;
    QrEmLot5PIS_R: TFloatField;
    QrEmLot5PIS_R_Val: TFloatField;
    QrEmLot5TaxaVal: TFloatField;
    QrEmLot5ValValorem: TFloatField;
    QrEmLot5Codigo: TIntegerField;
    QrEmLot5AdValorem: TFloatField;
    QrEmLot4: TmySQLQuery;
    QrEmLot4IRRF: TFloatField;
    QrEmLot4IRRF_Val: TFloatField;
    QrEmLot4ISS: TFloatField;
    QrEmLot4ISS_Val: TFloatField;
    QrEmLot4PIS_R: TFloatField;
    QrEmLot4PIS_R_Val: TFloatField;
    QrEmLot4TaxaVal: TFloatField;
    QrEmLot4ValValorem: TFloatField;
    QrEmLot4Codigo: TIntegerField;
    QrEmLot4AdValorem: TFloatField;
    QrEmLot3: TmySQLQuery;
    QrEmLot3IRRF: TFloatField;
    QrEmLot3IRRF_Val: TFloatField;
    QrEmLot3ISS: TFloatField;
    QrEmLot3ISS_Val: TFloatField;
    QrEmLot3PIS_R: TFloatField;
    QrEmLot3PIS_R_Val: TFloatField;
    QrEmLot3TaxaVal: TFloatField;
    QrEmLot3ValValorem: TFloatField;
    QrEmLot3Codigo: TIntegerField;
    QrEmLot3AdValorem: TFloatField;
    QrEmLot1: TmySQLQuery;
    QrEmLot1IRRF: TFloatField;
    QrEmLot1IRRF_Val: TFloatField;
    QrEmLot1ISS: TFloatField;
    QrEmLot1ISS_Val: TFloatField;
    QrEmLot1PIS_R: TFloatField;
    QrEmLot1PIS_R_Val: TFloatField;
    QrEmLot1TaxaVal: TFloatField;
    QrEmLot1ValValorem: TFloatField;
    QrEmLot1Codigo: TIntegerField;
    QrEmLot1AdValorem: TFloatField;
    QrExEnti1: TmySQLQuery;
    QrExEnti1Codigo: TIntegerField;
    QrExEnti1Maior: TFloatField;
    QrExEnti1Menor: TFloatField;
    QrExEnti1AdVal: TFloatField;
    QrExEnti2: TmySQLQuery;
    QrExEnti2Codigo: TIntegerField;
    QrExEnti2Maior: TFloatField;
    QrExEnti2Menor: TFloatField;
    QrExEnti2AdVal: TFloatField;
    QrEmLot2: TmySQLQuery;
    QrEmLot2IRRF: TFloatField;
    QrEmLot2IRRF_Val: TFloatField;
    QrEmLot2ISS: TFloatField;
    QrEmLot2ISS_Val: TFloatField;
    QrEmLot2PIS_R: TFloatField;
    QrEmLot2PIS_R_Val: TFloatField;
    QrEmLot2TaxaVal: TFloatField;
    QrEmLot2ValValorem: TFloatField;
    QrEmLot2Codigo: TIntegerField;
    QrEmLot2AdValorem: TFloatField;
    QrExEnti3: TmySQLQuery;
    IntegerField1: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    QrExEnti3AdVal: TFloatField;
    QrExEnti4: TmySQLQuery;
    IntegerField2: TIntegerField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    QrExEnti4AdVal: TFloatField;
    QrExEnti5: TmySQLQuery;
    IntegerField3: TIntegerField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    QrExEnti5AdVal: TFloatField;
    QrExEnti6: TmySQLQuery;
    IntegerField4: TIntegerField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    QrExEnti6AdVal: TFloatField;
    QrLI: TmySQLQuery;
    QrLocDU: TmySQLQuery;
    QrLocDUDuplicatas: TLargeintField;
    QrLocCH: TmySQLQuery;
    QrLocCHCheques: TLargeintField;
    QrSumTxs: TmySQLQuery;
    QrSumTxsTaxaVal: TFloatField;
    QrLCalc: TmySQLQuery;
    QrLCalcAdValorem: TFloatField;
    QrLCalcIOC: TFloatField;
    QrLCalcCPMF: TFloatField;
    QrLCalcIRRF: TFloatField;
    QrLCalcPIS: TFloatField;
    QrLCalcPIS_R: TFloatField;
    QrLCalcCOFINS: TFloatField;
    QrLCalcCOFINS_R: TFloatField;
    QrLCalcISS: TFloatField;
    QrLCalcCliente: TIntegerField;
    QrLCalcData: TDateField;
    QrLCalcVAL_LIQUIDO: TFloatField;
    QrLCalcValValorem: TFloatField;
    QrLCalcTxCompra: TFloatField;
    QrLCalcISS_Val: TFloatField;
    QrLCalcIRRF_Val: TFloatField;
    QrLCalcPIS_R_Val: TFloatField;
    QrLCalcTotal: TFloatField;
    QrLCalcIOC_Val: TFloatField;
    QrLCalcCPMF_Val: TFloatField;
    QrLCalcTarifas: TFloatField;
    QrLCalcOcorP: TFloatField;
    QrLCalcCHDevPg: TFloatField;
    QrLCalcSobraNow: TFloatField;
    QrLCalcIOFv: TFloatField;
    QrLCalcIOFd: TFloatField;
    QrLCalcTipo: TSmallintField;
    QrLCalcSimples: TSmallintField;
    QrTxs: TmySQLQuery;
    QrDupNeg: TmySQLQuery;
    QrTaxasCli: TmySQLQuery;
    QrTaxasCliGenero: TSmallintField;
    QrTaxasCliForma: TSmallintField;
    QrTaxasCliBase: TSmallintField;
    QrTaxasCliNOMETAXA: TWideStringField;
    QrTaxasCliControle: TIntegerField;
    QrTaxasCliCliente: TIntegerField;
    QrTaxasCliLk: TIntegerField;
    QrTaxasCliDataCad: TDateField;
    QrTaxasCliDataAlt: TDateField;
    QrTaxasCliUserCad: TIntegerField;
    QrTaxasCliUserAlt: TIntegerField;
    QrTaxasCliTaxa: TIntegerField;
    QrTaxasCliValor: TFloatField;
    QrLocs: TmySQLQuery;
    QrLocsCodigo: TIntegerField;
    QrLocsTipo: TSmallintField;
    QrDupSac: TmySQLQuery;
    QrSumP: TmySQLQuery;
    QrSumPDebito: TFloatField;
    QrVerNF: TmySQLQuery;
    QrVerNFCodigo: TIntegerField;
    QrLocNF: TmySQLQuery;
    QrLocNFCodigo: TIntegerField;
    QrSOA: TmySQLQuery;
    QrSOACodigo: TIntegerField;
    QrSOADataO: TDateField;
    QrSOAOcorrencia: TIntegerField;
    QrSOAValor: TFloatField;
    QrSOALoteQuit: TIntegerField;
    QrSOATaxaB: TFloatField;
    QrSOATaxaP: TFloatField;
    QrSOATaxaV: TFloatField;
    QrSOAPago: TFloatField;
    QrSOADataP: TDateField;
    QrSOALk: TIntegerField;
    QrSOADataCad: TDateField;
    QrSOADataAlt: TDateField;
    QrSOAUserCad: TIntegerField;
    QrSOAUserAlt: TIntegerField;
    QrSOAData3: TDateField;
    QrSOAStatus: TSmallintField;
    QrSOACliente: TIntegerField;
    QrSDO: TmySQLQuery;
    QrSDOCliente: TIntegerField;
    QrSDODCompra: TDateField;
    QrSDODDeposito: TDateField;
    QrSDOQuitado: TIntegerField;
    QrSDOTotalJr: TFloatField;
    QrSDOTotalDs: TFloatField;
    QrSDOTotalPg: TFloatField;
    QrSDOData3: TDateField;
    QrTCD: TmySQLQuery;
    QrTCDValor: TFloatField;
    QrTCDCliente: TIntegerField;
    QrTCDStatus: TSmallintField;
    QrTCDData1: TDateField;
    QrTCDData3: TDateField;
    QrTCDJurosV: TFloatField;
    QrTCDDesconto: TFloatField;
    QrTCDValPago: TFloatField;
    QrTCDJurosP: TFloatField;
    QrSRS: TmySQLQuery;
    QrSRSValor: TFloatField;
    QrCart: TmySQLQuery;
    QrCartCodigo: TIntegerField;
    QrCartTipo: TIntegerField;
    QrCHsDev: TmySQLQuery;
    QrCHsDevCodigo: TIntegerField;
    QrEmLotIts: TmySQLQuery;
    QrEmLotItsCodigo: TIntegerField;
    QrEmLotItsControle: TIntegerField;
    QrEmLotItsTaxaPer: TFloatField;
    QrEmLotItsTaxaVal: TFloatField;
    QrEmLotItsJuroPer: TFloatField;
    QrSumOco: TmySQLQuery;
    QrSumOcoPAGO: TFloatField;
    QrNovoTxs: TmySQLQuery;
    QrNovoTxsCodigo: TIntegerField;
    QrNovoTxsNome: TWideStringField;
    QrNovoTxsGenero: TSmallintField;
    QrNovoTxsForma: TSmallintField;
    QrNovoTxsAutomatico: TSmallintField;
    QrNovoTxsValor: TFloatField;
    QrNovoTxsMostra: TSmallintField;
    QrNovoTxsLk: TIntegerField;
    QrNovoTxsDataCad: TDateField;
    QrNovoTxsDataAlt: TDateField;
    QrNovoTxsUserCad: TIntegerField;
    QrNovoTxsUserAlt: TIntegerField;
    QrLocOc: TmySQLQuery;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorFatParcela: TIntegerField;
    QrLotExist: TmySQLQuery;
    QrLocSaca: TmySQLQuery;
    QrLocSacaNome: TWideStringField;
    QrLocSacaCNPJ: TWideStringField;
    QrLocSacaRua: TWideStringField;
    QrLocSacaNumero: TFloatField;
    QrLocSacaCompl: TWideStringField;
    QrLocSacaBairro: TWideStringField;
    QrLocSacaCidade: TWideStringField;
    QrLocSacaCEP: TIntegerField;
    QrLocSacaTel1: TWideStringField;
    QrLocSacaUF: TWideStringField;
    QrSumCHP: TmySQLQuery;
    QrSumCHPPago: TFloatField;
    QrLastCHDV: TmySQLQuery;
    QrLastCHDVData: TDateField;
    QrSumPgCHQ: TmySQLQuery;
    QrLocPg: TmySQLQuery;
    QrLocPgFatParcela: TIntegerField;
    QrLocPgData: TDateField;
    QrLocLote: TmySQLQuery;
    QrLocLoteLote: TSmallintField;
    QrSDUOpen: TmySQLQuery;
    QrSDUOpenValor: TFloatField;
    QrSDUOpenDUPLICATAS: TLargeintField;
    QrSumDUP: TmySQLQuery;
    QrSumDUPPago: TFloatField;
    QrLastDPg: TmySQLQuery;
    QrLastDPgData: TDateField;
    QrLastDPgFatParcela: TIntegerField;
    QrPgD: TmySQLQuery;
    QrPgDJuros: TFloatField;
    QrPgDDesco: TFloatField;
    QrPgDPago: TFloatField;
    QrPgDMaxData: TDateField;
    QrSumRepCli: TmySQLQuery;
    QrSumRepCliValor: TFloatField;
    QrSAlin: TmySQLQuery;
    QrSAlinValor: TFloatField;
    QrSAlinAberto: TFloatField;
    QrSAlinCheques: TLargeintField;
    QrOcorA: TmySQLQuery;
    QrOcorAATUALIZADO: TFloatField;
    QrOcorADOCUM_TXT: TWideStringField;
    DsOcorA: TDataSource;
    QrLocCliImp: TmySQLQuery;
    QrLocCliImpCodigo: TIntegerField;
    QrLocCliImpNOMECLI: TWideStringField;
    QrLocCliImpLimiCred: TFloatField;
    DsLocCliImp: TDataSource;
    DsRepCli: TDataSource;
    QrRepCli: TmySQLQuery;
    QrRepCliSEQ: TIntegerField;
    QrDUVenc: TmySQLQuery;
    StringField9: TWideStringField;
    DateField6: TDateField;
    DateField7: TDateField;
    DsDUVenc: TDataSource;
    DsDOpen: TDataSource;
    QrDOpen: TmySQLQuery;
    QrDOpenDuplicata: TWideStringField;
    QrDOpenDCompra: TDateField;
    QrDOpenDDeposito: TDateField;
    QrDOpenEmitente: TWideStringField;
    QrDOpenCliente: TIntegerField;
    QrDOpenSTATUS: TWideStringField;
    QrDOpenQuitado: TIntegerField;
    QrDOpenTotalJr: TFloatField;
    QrDOpenTotalDs: TFloatField;
    QrDOpenTotalPg: TFloatField;
    QrDOpenSALDO_DESATUALIZ: TFloatField;
    QrDOpenSALDO_ATUALIZADO: TFloatField;
    QrDOpenNOMESTATUS: TWideStringField;
    QrDOpenDDCALCJURO: TIntegerField;
    QrDOpenData3: TDateField;
    QrDOpenRepassado: TSmallintField;
    QrDUOpen: TmySQLQuery;
    QrDUOpenDuplicata: TWideStringField;
    QrDUOpenDCompra: TDateField;
    QrDUOpenDDeposito: TDateField;
    DsDUOpen: TDataSource;
    DsCHOpen: TDataSource;
    QrCHOpen: TmySQLQuery;
    QrCHOpenBanco: TIntegerField;
    QrCHOpenDCompra: TDateField;
    QrCHOpenDDeposito: TDateField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesAdValorem: TFloatField;
    QrClientesDMaisC: TIntegerField;
    QrClientesFatorCompra: TFloatField;
    QrClientesMAIOR_T: TFloatField;
    QrClientesADVAL_T: TFloatField;
    QrClientesDMaisD: TIntegerField;
    QrClientesCBE: TIntegerField;
    QrClientesSCB: TIntegerField;
    QrClientesCPMF: TFloatField;
    QrClientesTipo: TSmallintField;
    QrClientesSimples: TSmallintField;
    DsClientes: TDataSource;
    DsCHDevT: TDataSource;
    QrCHDevT: TmySQLQuery;
    QrCHDevTABERTO: TFloatField;
    QrCHDevA: TmySQLQuery;
    QrCHDevADATA1_TXT: TWideStringField;
    QrCHDevADATA2_TXT: TWideStringField;
    QrCHDevADATA3_TXT: TWideStringField;
    QrCHDevACPF_TXT: TWideStringField;
    QrCHDevANOMECLIENTE: TWideStringField;
    QrCHDevACodigo: TIntegerField;
    QrCHDevAAlinea1: TIntegerField;
    QrCHDevAAlinea2: TIntegerField;
    QrCHDevAData1: TDateField;
    QrCHDevAData2: TDateField;
    QrCHDevAData3: TDateField;
    QrCHDevACliente: TIntegerField;
    QrCHDevABanco: TIntegerField;
    QrCHDevAAgencia: TIntegerField;
    QrCHDevAConta: TWideStringField;
    QrCHDevACheque: TIntegerField;
    QrCHDevACPF: TWideStringField;
    QrCHDevAValor: TFloatField;
    QrCHDevATaxas: TFloatField;
    QrCHDevALk: TIntegerField;
    QrCHDevADataCad: TDateField;
    QrCHDevADataAlt: TDateField;
    QrCHDevAUserCad: TIntegerField;
    QrCHDevAUserAlt: TIntegerField;
    QrCHDevAEmitente: TWideStringField;
    QrCHDevAChequeOrigem: TIntegerField;
    QrCHDevAStatus: TSmallintField;
    QrCHDevAValPago: TFloatField;
    QrCHDevAMulta: TFloatField;
    QrCHDevAJurosP: TFloatField;
    QrCHDevAJurosV: TFloatField;
    QrCHDevADesconto: TFloatField;
    QrCHDevASALDO: TFloatField;
    QrCHDevAATUAL: TFloatField;
    QrCHDevAPgDesc: TFloatField;
    DsCHDevA: TDataSource;
    QrAlinIts: TmySQLQuery;
    QrAlinItsCodigo: TIntegerField;
    QrAlinItsAlinea1: TIntegerField;
    QrAlinItsAlinea2: TIntegerField;
    QrAlinItsData1: TDateField;
    QrAlinItsData2: TDateField;
    QrAlinItsData3: TDateField;
    QrAlinItsCliente: TIntegerField;
    QrAlinItsBanco: TIntegerField;
    QrAlinItsAgencia: TIntegerField;
    QrAlinItsConta: TWideStringField;
    QrAlinItsCheque: TIntegerField;
    QrAlinItsCPF: TWideStringField;
    QrAlinItsValor: TFloatField;
    QrAlinItsTaxas: TFloatField;
    QrAlinItsLk: TIntegerField;
    QrAlinItsDataCad: TDateField;
    QrAlinItsDataAlt: TDateField;
    QrAlinItsUserCad: TIntegerField;
    QrAlinItsUserAlt: TIntegerField;
    QrAlinItsEmitente: TWideStringField;
    QrAlinItsChequeOrigem: TIntegerField;
    QrAlinItsStatus: TSmallintField;
    QrAlinItsValPago: TFloatField;
    QrAlinItsDATA1_TXT: TWideStringField;
    QrAlinItsDATA2_TXT: TWideStringField;
    QrAlinItsDATA3_TXT: TWideStringField;
    QrAlinItsNOMECLIENTE: TWideStringField;
    QrAlinItsMulta: TFloatField;
    QrAlinItsJurosP: TFloatField;
    QrAlinItsJurosV: TFloatField;
    QrAlinItsDesconto: TFloatField;
    QrAlinItsCPF_TXT: TWideStringField;
    QrTaxas: TmySQLQuery;
    QrTaxasCodigo: TIntegerField;
    QrTaxasNome: TWideStringField;
    QrTaxasGenero: TSmallintField;
    QrTaxasForma: TSmallintField;
    QrTaxasAutomatico: TSmallintField;
    QrTaxasValor: TFloatField;
    QrTaxasMostra: TSmallintField;
    QrTaxasLk: TIntegerField;
    QrTaxasDataCad: TDateField;
    QrTaxasDataAlt: TDateField;
    QrTaxasUserCad: TIntegerField;
    QrTaxasUserAlt: TIntegerField;
    DsTaxas: TDataSource;
    DsAlinIts: TDataSource;
    QrSacRiscoTC: TmySQLQuery;
    QrSacRiscoTCValor: TFloatField;
    DsSacRiscoTC: TDataSource;
    DsCarteiras4: TDataSource;
    QrCarteiras4: TmySQLQuery;
    QrCarteiras4Codigo: TIntegerField;
    QrCarteiras4Nome: TWideStringField;
    QrCarteiras4Banco1: TIntegerField;
    QrCarteiras4Agencia1: TIntegerField;
    QrCarteiras4Conta1: TWideStringField;
    QrSacOcorA: TmySQLQuery;
    QrSacOcorASALDO: TFloatField;
    QrSacOcorAATUALIZADO: TFloatField;
    DsSacOcorA: TDataSource;
    DsSacCHDevA: TDataSource;
    QrSacCHDevA: TmySQLQuery;
    QrSacCHDevADATA1_TXT: TWideStringField;
    QrSacCHDevADATA2_TXT: TWideStringField;
    QrSacCHDevADATA3_TXT: TWideStringField;
    QrSacCHDevACPF_TXT: TWideStringField;
    QrSacCHDevASALDO: TFloatField;
    QrSacCHDevAATUAL: TFloatField;
    QrBanco1: TmySQLQuery;
    QrBanco1Nome: TWideStringField;
    DsBanco1: TDataSource;
    DsBanco0: TDataSource;
    QrBanco0: TmySQLQuery;
    QrBanco0Nome: TWideStringField;
    QrRiscoTC: TmySQLQuery;
    QrRiscoTCValor: TFloatField;
    DsRiscoTC: TDataSource;
    DsRiscoC: TDataSource;
    QrRiscoC: TmySQLQuery;
    QrRiscoCBanco: TIntegerField;
    QrRiscoCDCompra: TDateField;
    QrRiscoCDDeposito: TDateField;
    QrRiscoCEmitente: TWideStringField;
    QrRiscoCCPF_TXT: TWideStringField;
    QrSacRiscoC: TmySQLQuery;
    QrSacRiscoCCPF_TXT: TWideStringField;
    DsSacRiscoC: TDataSource;
    DsSacDOpen: TDataSource;
    QrSacDOpen: TmySQLQuery;
    QrSacDOpenSALDO_DESATUALIZ: TFloatField;
    QrSacDOpenSALDO_ATUALIZADO: TFloatField;
    QrSacDOpenNOMESTATUS: TWideStringField;
    QrSacDOpenDDCALCJURO: TIntegerField;
    QrBanco4: TmySQLQuery;
    QrBanco4Nome: TWideStringField;
    DsBanco4: TDataSource;
    QrSPC_Result: TmySQLQuery;
    DsSPC_Result: TDataSource;
    QrSPC_ResultLinha: TIntegerField;
    QrSPC_ResultTexto: TWideStringField;
    QrSPC_Cfg: TmySQLQuery;
    QrSPC_CfgSPC_ValMin: TFloatField;
    QrSPC_CfgSPC_ValMax: TFloatField;
    QrSPC_CfgServidor: TWideStringField;
    QrSPC_CfgSPC_Porta: TSmallintField;
    QrSPC_CfgPedinte: TWideStringField;
    QrSPC_CfgCodigSocio: TIntegerField;
    QrSPC_CfgModalidade: TIntegerField;
    QrSPC_CfgInfoExtra: TIntegerField;
    QrSPC_CfgBAC_CMC7: TSmallintField;
    QrSPC_CfgTipoCred: TSmallintField;
    QrSPC_CfgSenhaSocio: TWideStringField;
    QrSPC_CfgCodigo: TIntegerField;
    QrSPC_CfgValAviso: TFloatField;
    QrSPC_CfgVALCONSULTA: TFloatField;
    QrILi: TmySQLQuery;
    QrILiCPF: TWideStringField;
    QrOcorASALDO: TFloatField;
    QrOcorANOMEOCORRENCIA: TWideStringField;
    QrOcorACodigo: TIntegerField;
    QrOcorACliente: TIntegerField;
    QrOcorADataO: TDateField;
    QrOcorAOcorrencia: TIntegerField;
    QrOcorAValor: TFloatField;
    QrOcorALoteQuit: TIntegerField;
    QrOcorATaxaB: TFloatField;
    QrOcorATaxaP: TFloatField;
    QrOcorATaxaV: TFloatField;
    QrOcorAPago: TFloatField;
    QrOcorADataP: TDateField;
    QrOcorAData3: TDateField;
    QrOcorAStatus: TSmallintField;
    QrOcorADescri: TWideStringField;
    QrOcorAMoviBank: TIntegerField;
    QrRiscoCContaCorrente: TWideStringField;
    QrRiscoCDocumento: TFloatField;
    QrRiscoCCredito: TFloatField;
    QrRiscoCCNPJCPF: TWideStringField;
    QrRepCliNOMECLI: TWideStringField;
    QrRepCliLote: TIntegerField;
    QrRepCliVencimento: TDateField;
    QrRepCliCredito: TFloatField;
    QrRepCliDocumento: TFloatField;
    QrRepCliBanco: TIntegerField;
    QrRepCliContaCorrente: TWideStringField;
    QrRepCliEmitente: TWideStringField;
    QrRepCliFatNum: TFloatField;
    QrRepCliFatParcela: TIntegerField;
    QrDOpenFatParcela: TIntegerField;
    QrDOpenCredito: TFloatField;
    QrDOpenCNPJCPF: TWideStringField;
    QrDOpenVencimento: TDateField;
    QrDupSacNOMEBANCO: TWideStringField;
    QrDupSacDuplicata: TWideStringField;
    QrDupSacData: TDateField;
    QrDupSacCredito: TFloatField;
    QrDupSacVencimento: TDateField;
    QrDupSacBanco: TIntegerField;
    QrLICBE: TIntegerField;
    QrLISCB: TIntegerField;
    QrLIFatNum: TFloatField;
    QrLIFatParcela: TIntegerField;
    QrLICredito: TFloatField;
    QrLIVencimento: TDateField;
    QrLIDCompra: TDateField;
    QrLIBanco: TIntegerField;
    QrLIContaCorrente: TWideStringField;
    QrLIDocumento: TFloatField;
    QrLICNPJCPF: TWideStringField;
    QrLIEmitente: TWideStringField;
    QrLIDMais: TIntegerField;
    QrLIPraca: TIntegerField;
    QrLITipific: TSmallintField;
    QrLIStatusSPC: TSmallintField;
    QrLITxaCompra: TFloatField;
    QrLIBruto: TFloatField;
    QrLIDesco: TFloatField;
    QrLIDescAte: TDateField;
    QrLICliente: TIntegerField;
    QrLIData: TDateField;
    QrLIDuplicata: TWideStringField;
    QrLICartDep: TIntegerField;
    QrLITipoLote: TSmallintField;
    QrLITipoCart: TSmallintField;
    QrLICarteira: TIntegerField;
    QrLIControle: TIntegerField;
    QrLISub: TSmallintField;
    QrSum0Valor: TFloatField;
    QrSum0VlrCompra: TFloatField;
    QrSum0Dias: TFloatField;
    QrSum0PRAZO_MEDIO: TFloatField;
    QrSum0Vencto: TDateField;
    QrSum0ITENS: TLargeintField;
    QrSacOcorATIPODOC: TWideStringField;
    QrSacOcorANOMEOCORRENCIA: TWideStringField;
    QrSacOcorACodigo: TIntegerField;
    QrSacOcorACliente: TIntegerField;
    QrSacOcorADataO: TDateField;
    QrSacOcorAOcorrencia: TIntegerField;
    QrSacOcorAValor: TFloatField;
    QrSacOcorALoteQuit: TIntegerField;
    QrSacOcorATaxaB: TFloatField;
    QrSacOcorATaxaP: TFloatField;
    QrSacOcorATaxaV: TFloatField;
    QrSacOcorAPago: TFloatField;
    QrSacOcorADataP: TDateField;
    QrSacOcorAData3: TDateField;
    QrSacOcorAStatus: TSmallintField;
    QrSacOcorADescri: TWideStringField;
    QrSacOcorAMoviBank: TIntegerField;
    QrSacOcorABanco: TIntegerField;
    QrSacOcorAAgencia: TIntegerField;
    QrSacOcorAConta: TWideStringField;
    QrSacOcorACheque: TIntegerField;
    QrSacOcorADuplicata: TWideStringField;
    QrSacOcorACPF: TWideStringField;
    QrSacOcorAEmitente: TWideStringField;
    QrSacOcorAEmissao: TDateField;
    QrSacOcorADCompra: TDateField;
    QrSacOcorAVencto: TDateField;
    QrSacOcorADDeposito: TDateField;
    QrOcorALk: TIntegerField;
    QrOcorADataCad: TDateField;
    QrOcorADataAlt: TDateField;
    QrOcorAUserCad: TIntegerField;
    QrOcorAUserAlt: TIntegerField;
    QrOcorAAlterWeb: TSmallintField;
    QrOcorAAtivo: TSmallintField;
    QrOcorABanco: TIntegerField;
    QrOcorAAgencia: TIntegerField;
    QrOcorAConta: TWideStringField;
    QrOcorACheque: TIntegerField;
    QrOcorADuplicata: TWideStringField;
    QrOcorACPF: TWideStringField;
    QrOcorAEmitente: TWideStringField;
    QrOcorAEmissao: TDateField;
    QrOcorADCompra: TDateField;
    QrOcorAVencto: TDateField;
    QrOcorADDeposito: TDateField;
    QrOcorATIPODOC: TWideStringField;
    QrOcorATpOcor: TSmallintField;
    QrSacOcorATpOcor: TSmallintField;
    QrSacCHDevANOMECLIENTE: TWideStringField;
    QrSacCHDevACodigo: TIntegerField;
    QrSacCHDevAAlinea1: TIntegerField;
    QrSacCHDevAAlinea2: TIntegerField;
    QrSacCHDevAData1: TDateField;
    QrSacCHDevAData2: TDateField;
    QrSacCHDevAData3: TDateField;
    QrSacCHDevACliente: TIntegerField;
    QrSacCHDevABanco: TIntegerField;
    QrSacCHDevAAgencia: TIntegerField;
    QrSacCHDevAConta: TWideStringField;
    QrSacCHDevACheque: TIntegerField;
    QrSacCHDevACPF: TWideStringField;
    QrSacCHDevAValor: TFloatField;
    QrSacCHDevATaxas: TFloatField;
    QrSacCHDevAMulta: TFloatField;
    QrSacCHDevAJurosP: TFloatField;
    QrSacCHDevAJurosV: TFloatField;
    QrSacCHDevADesconto: TFloatField;
    QrSacCHDevAEmitente: TWideStringField;
    QrSacCHDevAChequeOrigem: TIntegerField;
    QrSacCHDevAValPago: TFloatField;
    QrSacCHDevAPgDesc: TFloatField;
    QrSacCHDevAStatus: TSmallintField;
    QrSacCHDevADDeposito: TDateField;
    QrSacCHDevAVencto: TDateField;
    QrSacCHDevALoteOrigem: TIntegerField;
    QrSacCHDevALk: TIntegerField;
    QrSacCHDevADataCad: TDateField;
    QrSacCHDevADataAlt: TDateField;
    QrSacCHDevAUserCad: TIntegerField;
    QrSacCHDevAUserAlt: TIntegerField;
    QrSacCHDevAAlterWeb: TSmallintField;
    QrSacCHDevAData4: TDateField;
    QrSacCHDevAAtivo: TSmallintField;
    QrSacDOpenSTATUS: TWideStringField;
    QrSacDOpenFatParcela: TIntegerField;
    QrSacDOpenDuplicata: TWideStringField;
    QrSacDOpenRepassado: TSmallintField;
    QrSacDOpenDCompra: TDateField;
    QrSacDOpenCredito: TFloatField;
    QrSacDOpenDDeposito: TDateField;
    QrSacDOpenEmitente: TWideStringField;
    QrSacDOpenCNPJCPF: TWideStringField;
    QrSacDOpenCliente: TIntegerField;
    QrSacDOpenQuitado: TIntegerField;
    QrSacDOpenTotalJr: TFloatField;
    QrSacDOpenTotalDs: TFloatField;
    QrSacDOpenTotalPg: TFloatField;
    QrSacDOpenVencimento: TDateField;
    QrSacDOpenData3: TDateField;
    QrSacRiscoCTipo: TSmallintField;
    QrSacRiscoCBanco: TIntegerField;
    QrSacRiscoCContaCorrente: TWideStringField;
    QrSacRiscoCDocumento: TFloatField;
    QrSacRiscoCCredito: TFloatField;
    QrSacRiscoCDCompra: TDateField;
    QrSacRiscoCDDeposito: TDateField;
    QrSacRiscoCEmitente: TWideStringField;
    QrSacRiscoCCNPJCPF: TWideStringField;
    QrSDOCredito: TFloatField;
    QrSDOVencimento: TDateField;
    QrCHOpenContaCorrente: TWideStringField;
    QrCHOpenDocumento: TFloatField;
    QrCHOpenCredito: TFloatField;
    QrRiscoCAgencia: TIntegerField;
    QrRepCliAgencia: TIntegerField;
    QrDupSacAgencia: TIntegerField;
    QrLIAgencia: TIntegerField;
    QrSacRiscoCAgencia: TIntegerField;
    QrCHOpenAgencia: TIntegerField;
    QrDUOpenCredito: TFloatField;
    QrDUVencCredito: TFloatField;
    QrDupNegLote: TIntegerField;
    QrDupNegDCompra: TDateField;
    QrTxsBase: TSmallintField;
    QrTxsFatNum: TFloatField;
    QrTxsFatParcela: TIntegerField;
    QrTxsFatID_Sub: TIntegerField;
    QrTxsMoraVal: TFloatField;
    QrTxsMoraDia: TFloatField;
    QrTxsQtde: TFloatField;
    QrTxsTipific: TSmallintField;
    QrTaxasBase: TSmallintField;
    QrTaxasPlaGen: TIntegerField;
    QrTxsCredito: TFloatField;
    QrTxsMoraTxa: TFloatField;
    QrNovoTxsBase: TSmallintField;
    QrNovoTxsPlaGen: TIntegerField;
    QrTaxasCliPlaGen: TIntegerField;
    QrLocOcData: TDateField;
    QrOcorAPlaGen: TIntegerField;
    QrOcorALOIS: TIntegerField;
    QrAPCm1: TmySQLQuery;
    QrAPCm1PlaGen: TIntegerField;
    QrAdupIts: TmySQLQuery;
    QrAdupItsNOMESTATUS: TWideStringField;
    QrAdupItsControle: TIntegerField;
    QrAdupItsAlinea: TIntegerField;
    QrAdupItsDataA: TDateField;
    QrAdupItsLk: TIntegerField;
    QrAdupItsDataCad: TDateField;
    QrAdupItsDataAlt: TDateField;
    QrAdupItsUserCad: TIntegerField;
    QrAdupItsUserAlt: TIntegerField;
    QrAdupItsAlterWeb: TSmallintField;
    QrAdupItsAtivo: TSmallintField;
    QrLOI: TmySQLQuery;
    QrLOIVencimento: TDateField;
    QrLOICliente: TIntegerField;
    QrTeste: TmySQLQuery;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    FloatField11: TFloatField;
    QrPgDValor: TFloatField;
    QrPsqLctA: TmySQLQuery;
    QrPsqLctATipo: TSmallintField;
    QrPsqLctACarteira: TIntegerField;
    QrPsqLctAControle: TIntegerField;
    QrPsqLctAPago: TFloatField;
    QrPsqLctATpCart: TIntegerField;
    QrPsqLctACompensado: TDateField;
    QrPsqLctASit: TIntegerField;
    QrPsqLctB: TmySQLQuery;
    QrPsqLctBCodigo: TIntegerField;
    QrPsqLctBChequeOrigem: TIntegerField;
    QrPsqLctBData: TDateField;
    QrPsqLctBTipo: TSmallintField;
    QrPsqLctBCarteira: TIntegerField;
    QrPsqLctBControle: TIntegerField;
    QrPsqLctBSub: TSmallintField;
    QrPsqLctBCredito: TFloatField;
    QrPsqLctBDebito: TFloatField;
    QrPsqLctBDocumento: TFloatField;
    QrPsqLctC: TmySQLQuery;
    QrPsqLctCSit: TIntegerField;
    QrSumPgCHQPago: TFloatField;
    QrSumPgCHQTaxas: TFloatField;
    QrSumPgCHQMulta: TFloatField;
    QrSumPgCHQJuros: TFloatField;
    QrSumPgCHQDesco: TFloatField;
    QrSumPgCHQPrinc: TFloatField;
    QrLOIControle: TIntegerField;
    QrLOISub: TIntegerField;
    QrLOIDuplicata: TWideStringField;
    QrSumPgDUP: TmySQLQuery;
    QrSumPgDUPPago: TFloatField;
    QrSumPgDUPTaxas: TFloatField;
    QrSumPgDUPMulta: TFloatField;
    QrSumPgDUPJuros: TFloatField;
    QrSumPgDUPDesco: TFloatField;
    QrSumPgDUPPrinc: TFloatField;
    QrSumPgDUPMaxData: TDateField;
    QrLOICredito: TFloatField;
    QrLOIDebito: TFloatField;
    QrLot: TmySQLQuery;
    QrLotCodigo: TIntegerField;
    QrPgts: TmySQLQuery;
    QrPgtsItens: TLargeintField;
    QrLotPrr: TmySQLQuery;
    QrLotPrrCodigo: TIntegerField;
    QrLotPrrControle: TIntegerField;
    QrLotPrrData1: TDateField;
    QrLotPrrData2: TDateField;
    QrLotPrrData3: TDateField;
    QrLotPrrOcorrencia: TIntegerField;
    QrLotPrrLk: TIntegerField;
    QrLotPrrDataCad: TDateField;
    QrLotPrrDataAlt: TDateField;
    QrLotPrrUserCad: TIntegerField;
    QrLotPrrUserAlt: TIntegerField;
    QrLotPrrDIAS_1_3: TIntegerField;
    QrLotPrrDIAS_2_3: TIntegerField;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuTpOcor: TSmallintField;
    QrOcorreuLOIS: TIntegerField;
    QrOcorreuPlaGen: TIntegerField;
    QrDupsAtencao: TmySQLQuery;
    QrLocSacaEmail: TWideStringField;
    procedure QrLCalcCalcFields(DataSet: TDataSet);
    procedure QrClientesCalcFields(DataSet: TDataSet);
    procedure QrAlinItsCalcFields(DataSet: TDataSet);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrCHDevACalcFields(DataSet: TDataSet);
    procedure QrOcorACalcFields(DataSet: TDataSet);
    procedure QrCHDevAAfterScroll(DataSet: TDataSet);
    procedure QrDOpenCalcFields(DataSet: TDataSet);
    procedure QrDOpenAfterOpen(DataSet: TDataSet);
    procedure QrDOpenBeforeClose(DataSet: TDataSet);
    procedure QrRiscoCCalcFields(DataSet: TDataSet);
    procedure QrCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrCHDevABeforeClose(DataSet: TDataSet);
    procedure QrOcorAAfterOpen(DataSet: TDataSet);
    procedure QrOcorABeforeClose(DataSet: TDataSet);
    procedure QrSacRiscoCCalcFields(DataSet: TDataSet);
    procedure QrSacDOpenCalcFields(DataSet: TDataSet);
    procedure QrSacCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrSacCHDevABeforeClose(DataSet: TDataSet);
    procedure QrSacCHDevACalcFields(DataSet: TDataSet);
    procedure QrSacOcorAAfterOpen(DataSet: TDataSet);
    procedure QrSacOcorABeforeClose(DataSet: TDataSet);
    procedure QrSacOcorACalcFields(DataSet: TDataSet);
    procedure QrRepCliCalcFields(DataSet: TDataSet);
    procedure QrRepCliBeforeClose(DataSet: TDataSet);
    procedure QrRepCliAfterOpen(DataSet: TDataSet);
    procedure QrLotPrrCalcFields(DataSet: TDataSet);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FCHDevOpen_Total, FOcorA_Total: Double;
    FShowLot2Cad, FShowLot2Inn: Boolean;
    //
    function  AvisoDeNaoUpd(Index: Integer): Boolean;
    procedure ReopenSPC_Cli(Entidade: Integer);
    procedure ReopenSPC_Result(CPF: String; Linha: Integer);
    function  RiscoSacado(CPF: String): Double;
    // Risco Sacado
    procedure PesquisaRiscoSacado(CPF: String;
              CH_Risc, CH_Venc, CH_SubT,
              DU_Risc, DU_Venc, DU_SubT,
              ST_Risc, ST_Venc, ST_SubT,
              OC_Risc, TO_Tudo: TdmkEdit);
    procedure ReopenAdupIts(LOI: Integer);          
    procedure ReopenCliCHDevA(Cliente, CHDevA: Integer);
    procedure ReopenDUOpen(CNPJCPF: String);
    procedure ReopenDUVenc(CNPJCPF: String);
    procedure ReopenILi(Lote: Double);
    procedure ReopenLastCHDV(FatParcRef: Integer);
    procedure ReopenLastDPg(LOIS: Integer);
    procedure ReopenLastOcor(Ocorreu: Integer);
    procedure ReopenLCalc(Lote: Double);
    procedure ReopenLocDU(CNPJCPF, Duplicata: String);
    procedure ReopenLocLote(Cliente, Lote: Integer);
    procedure ReopenLocOc(Ocorreu: Integer);
    procedure ReopenLocPg(FatParcRef: Integer);
    procedure ReopenLocs(Lote: Double);
    procedure ReopenLOI(LOI: Integer);
    procedure ReopenNovoTxs();
    procedure ReopenOcorA(Cliente, OcorA: Integer);
    procedure ReopenPgD(FatParcela: Integer);
    procedure ReopenSacCHDevA();
    procedure ReopenSacOcorA();
    procedure ReopenSacRiscoC();
    procedure ReopenSacDOpen();
    procedure ReopenSDUOpen(CNPJCPF: String);
    procedure ReopenSumCHP(Lote: Double);
    procedure ReopenSumDUP(Lote: Double);
    procedure ReopenSumOc(OcorP: Integer);
    procedure ReopenSumOco(Lote: Double);
    procedure ReopenSumPgCHQ(FatParcRef: Integer);
    procedure ReopenSumPgDUP(FatParcRef: Integer);
    procedure ReopenSumRepCli(Lote: Double);
    procedure ReopenSumTxs(Lote: Double);
    procedure ReopenTaxas();
    procedure ReopenTxs(Lote: Double);
    procedure FechaTudoSac();
    procedure CorrigeDadosParaMigrarAoCredito2();
    // Fim Risco Sacado
    function  LOI_Upd_FPC(QrUpd: TmySQLQuery; FatParcCtrl: Integer;
              SQLCampos: array of String; ValCampos: array of Variant): Boolean;
    function  LOI_Upd_NUM(QrUpd: TmySQLQuery;
              FatID, FatNum, FatParcela, Controle: Integer;
              SQLCampos: array of String; ValCampos: array of Variant): Boolean;
    function  LOT_Ins_Upd(QrUpd: TmySQLQuery; Tipo: TSQLType; DesEncerra: Boolean;
              SQLCampos, SQLIndex: array of String;
              ValCampos, ValIndex: array of Variant;
              UserDataAlterweb: Boolean; ComplUpd: String = '';
              InfoSQLOnError: Boolean = True; InfoErro: Boolean = True): Boolean;
    function  SQL_CH(var FatParcela: Integer; const Comp, Banco: Integer;
              Agencia: String; Documento: Double; DMais, Dias: Integer; FatNum:
              Double; Praca, Tipific, StatusSPC: Integer; ContaCorrente,
              CNPJCPF, Emitente, DataDoc, Vencimento, DCompra, DDeposito: String;
              Credito: Double; SQLType: TSQLType; Cliente: Integer; TxaCompra,
              TxaAdValorem, VlrCompra, VlrAdValorem, TxaJuros: Double;
              Data3: String; TpAlin, AliIts, ChqPgs: Integer; Descricao: String;
              var TipoCart, Carteira, Controle, Sub: Integer): Boolean;
    function  SQL_Txa(QrUpd: TmySQLQuery; SQLType: TSQLType;
              var FatParcela, Controle: Integer;
              const FatID_Sub, Genero, Cliente, Tipific: Integer;
              const FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal: Double;
              const Emissao: TDateTime; FatParcRef, FatGrupo: Integer;
              Antigo: String = ''): Boolean;
    function  SQL_OcorP(QrUpd: TmySQLQuery; SQLType: TSQLType;
              var FatParcela, Controle: Integer;
              const FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef: Integer;
              const FatNum, Valor, MoraVal: Double;
              const Dta: TDateTime; FatGrupo: Integer;
              Antigo: String = ''): Boolean;
    function  InsUpd_ChqPg(QrUpd: TmySQLQuery; SQLType: TSQLType; var FatParcela,
              Controle: Integer; const FatID_Sub, Gen_0303, Gen_0304, Gen_0305,
              Gen_0307, Cliente, Ocorreu,
              FatParcRef: Integer; const FatNum, Pago(*, MoraVal, Desco*): Double;
              const DataCH, Dta: TDateTime;
              const ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende:
              Double; CartDep: Integer; Antigo: String = '';
              PermiteQuitarControleZero: Boolean = False): Boolean;
    function  InsUpd_DupPg(QrUpd: TmySQLQuery; SQLType: TSQLType; const
              LctCtrl, LctSub: Integer;  var FatParcela, Controle: Integer;
              const FatID_Sub, Gen_0303, Gen_0304, Gen_0306,
              Gen_0308, Cliente, Ocorreu,
              FatParcRef: Integer; const FatNum, Pago(*, MoraVal, Desco*): Double;
              const DataDU, Dta: TDateTime; Duplicata: String;
              const ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende:
              Double; CartDep: Integer; Antigo: String = '';
              PermiteQuitarControleZero: Boolean = False): Boolean;
{
    function  SQL_DupPg(QrUpd: TmySQLQuery; SQLType: TSQLType;
              var FatParcela, Controle: Integer;
              const FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef: Integer;
              const FatNum, Pago, MoraVal, Desco: Double;
              const DataDU, Dta: TDateTime): Boolean;
}
    function  SQL_Autom(QrUpd: TmySQLQuery; SQLType: TSQLType;
              var FatParcela, Controle: Integer;
              const FatID, Genero, Cliente: Integer;
              const FatNum, Credito, Debito: Double;
              const Dta: TDateTime): Boolean;
    procedure Exclui_OcorP(Controle, FatParcela, Ocorreu: Integer);
    function  Exclui_ChqPg(Controle, FatParcela, Ocorreu, FatParcRef,
              FatGrupo: Integer; EstaNoBordero: Boolean; FatNum: Double):
              Boolean;
    function  Exclui_DupPg(Controle, FatParcela, Ocorreu, FatParcRef,
              FatGrupo: Integer; EstaNoBordero: Boolean; FatNum: Double):
              Boolean;

{
    function  BuscaGenAPCD(const OcorAPCD: Integer; ValorAtual: Double;
              var GenAPCD: Integer): Boolean;
    function  BuscaGenAPDV(): Integer;
}
    function  DesEncerraLote(Lote: Integer): Boolean;
    function  ObterSitOriginalDeChequeDevolvido(ChequeOrigem: Integer): Integer;
    function  SQL_Extra_CH_DU(QrUpd: TmySQLQuery; SQLType: TSQLType;
              const Genero: Integer; const Debito, Credito: Double; const Compensado:
              String; const Sit: Integer; const Vencimento: String; const FatID, FatID_Sub:
              Integer; const FatNum: Double; var FatParcela: Integer; const Cliente,
              CliInt: Integer; const MoraVal: Double; const DataDoc: String; const Desco:
              Double; const FatParcRef, Ocorreu: Integer; const Data: String; const Tipo,
              Carteira: Integer; var Controle: Integer; const Sub, FatGrupo: Integer;
              Antigo: String = ''): Boolean;
    procedure CalculaPagtoAlinIts(Quitacao: TDateTime; AlinIts: Integer);
    procedure CalculaPagtoDuplicata(LOIS: Integer; ValDupl: Double);
    function  QtdDePagtosDeChqDev(AlinIts: Integer): Integer;
    //
    procedure ReopenLotPrr(QrLotPrr: TmySQLQuery; FatParcela, Controle,
              LotPrr: Integer);
    procedure ReopenOcorreu(QrOcorreu: TmySQLQuery; FatParcela, Codigo,
              Ocorreu: Integer);
    function  FazProrrogacao(const FatParcela, Cliente: Integer; const
              Vencimento, DDeposito: TDateTime; Credito: Double;
              const NaoDeposita: Boolean; const NomeForm: String;
              const SQLType: TSQLType; var NovaData: TDateTime): Boolean;

  end;

var
  DmLot: TDmLot;

implementation

uses UnMyObjects, Module, Principal, Lot2Cab, UnMLAGeral, Lot2Inn,
  MyDBCheck, Credito2_Ini, UMySQLModule, UnInternalConsts, MyListas, Lot2Cad,
  ModuleFin, GerChqOcor, OperaImp;

{$R *.dfm}

function TDmLot.AvisoDeNaoUpd(Index: Integer): Boolean;
begin
  Geral.MensagemBox(
  'Form n�o preparado para altera��o de lan�amanto!' + #13#10 +
  'AVISE A DERMATEK -> C�digo: ' + Geral.FFN(Index, 3),
  'ERRO', MB_OK+MB_ICONERROR);
  //
  Result := True;
end;

{
function TDmLot.BuscaGenAPCD(const OcorAPCD: Integer; ValorAtual: Double;
  var GenAPCD: Integer): Boolean;
begin
  //GenAPCD := - 3 9 4;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0305, GenAPCD, tpCred, True) then
    Exit;
end;
}

{
function TDmLot.BuscaGenAPDV(): Integer;
begin
  //Result := - 3 9 3;
  DModFin.ObtemGeneroDeFatID(VAR_FATID_0312, Result, tpCred, True);
end;
}

procedure TDmLot.CalculaPagtoAlinIts(Quitacao: TDateTime; AlinIts: Integer);
var
  Sit: Integer;
  Data3: String;
begin
(*
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := AlinIts;
  UMyMod.AbreQuery(QrSumPg);
*)
  ReopenSumPgCHQ(AlinIts);
  //
(*
  if (QrSumPgCHQPago.Value + QrSumPgCHQDesco.Value)< 0.01 then Sit := 0 else
    if (QrSumPgCHQPago.Value + QrSumPgCHQDesco.Value) <
    (QrSumPgCHQJuros.Value + QrCHDevAValor.Value + QrCHDevATaxas.Value +
    QrCHDevAMulta.Value - QrCHDevADesconto.Value)-0.009 then
    Sit := 1 else Sit := 2;
*)
  if QrSumPgCHQPago.Value < 0.01 then Sit := 0 else
    if QrCHDevAValor.Value - QrSumPgCHQPrinc.Value >= 0.01 then
      Sit := 1
    else
      Sit := 2;
  //
  if Sit > 1 then Data3 := Geral.FDT(Quitacao, 1)
  else Data3 := '0000-00-00';
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('JurosV=:P0, ValPago=:P1, PgDesc=:P2, Status=:P3, ');
  Dmod.QrUpd.SQL.Add('Data3=:P4, Data4=:P5 WHERE Codigo=:Pa');
  // 2011-12-30 > Compatibilidade! Soma de Juros + Multa + Taxas
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgCHQJuros.Value;// + QrSumPgCHQMulta.Value + QrSumPgCHQTaxas.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgCHQPago.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgCHQDesco.Value;
  Dmod.QrUpd.Params[03].AsInteger := Sit;
  Dmod.QrUpd.Params[04].AsString  := Data3;
  Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[06].AsInteger := AlinIts;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TDmLot.CalculaPagtoDuplicata(LOIS: Integer; ValDupl: Double);
var
  Sit: Integer;
  //Cred, Debi,
  TotalJr, TotalDs, TotalPg: Double;
  Data3, Data4: String;
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPg, Dmod.MyDB, [
  'SELECT SUM(ad.MoraVal) Juros, SUM(ad.DescoVal) Desco, ',
  'SUM(ad.Credito) Pago, li.Credito Valor, MAX(ad.Data) MaxData ',
  'FROM ' + CO_TabLctA + ' ad ',
  'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ad.FatParcRef',
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND ad.FatID=' + TXT_VAR_FATID_0312,
  'AND ad.FatParcRef=' + Geral.FF0(LOIS),
  'GROUP BY ad.FatParcRef',
  '']);
}
  ReopenSumPgDUP(LOIS);

{
  Cred := QrSumPgPago.Value + QrSumPgDesco.Value;
  Debi := QrSumPgValor.Value + QrSumPgJuros.Value;
  if QrSumPgPago.Value < 0.01 then Quitado := 0 else
  begin
    if Cred < (Debi - 0.009) then Quitado := 1 else
    begin
      if Cred > (Debi + 0.009) then Quitado := 3 else Quitado := 2;
    end;
  end;
  Data4 := Geral.FDT(QrSumPgMaxData.Value, 1);
  if Quitado > 1 then
    Data3 := Data4
  else
    Data3 := '0000-00-00';
  //
}
  if QrSumPgDUPPago.Value < 0.01 then Sit := 0 else
    if ValDupl - QrSumPgDUPPrinc.Value >= 0.01 then
      Sit := 1
    else
    if QrSumPgDUPPrinc.Value - ValDupl >= 0.01 then
      Sit := 3
    else
      Sit := 2;
  //
  if Sit > 1 then
    Data3 := Geral.FDT(QrSumPgDUPMaxData.Value, 1)
  else
    Data3 := '0000-00-00';
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 WHERE Controle=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Quitado;
  Dmod.QrUpd.Params[06].AsInteger := LOIS;
  Dmod.QrUpd.ExecSQL;
  //
}
  TotalJr := QrSumPgDUPJuros.Value;
  TotalDs := QrSumPgDUPDesco.Value;
  TotalPg := QrSumPgDUPPago.Value;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
  'TotalJr', 'TotalDs', 'TotalPg',
  'Data3', 'Data4', 'Quitado'], [
  'FatID', 'FatParcela'], [
  TotalJr, TotalDs, TotalPg,
  Data3, Data4, Sit], [
  VAR_FATID_0301, LOIS], True);
  //
end;

procedure TDmLot.CorrigeDadosParaMigrarAoCredito2();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SHOW TABLES ',
  'FROM ' + TMeuDB,
  'LIKE "ocorrpg"',
  '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SHOW TABLES ',
    'FROM ' + TMeuDB,
    'LIKE "lotesits"',
    '']);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      if DBCheck.CriaFm(TFmCredito2_Ini, FmCredito2_Ini, afmoLiberado) then
      begin
        FmCredito2_Ini.Show;
        //
        FmCredito2_Ini.VerificaClienteOcorreu();
        //
        FmCredito2_Ini.Destroy;
      end;
    end;
  end;
end;

function TDmLot.DesEncerraLote(Lote: Integer): Boolean;
begin
  Result := False;
  if Lote > 0 then
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLotA, False,
    ['LctsAuto'], ['Codigo'], [0], [Lote], False)
  else Geral.MensagemBox('Somente lotes positivos s�o reabertos!',
  'Erro', MB_OK+MB_ICONERROR);
end;

function TDmLot.Exclui_ChqPg(Controle, FatParcela, Ocorreu, FatParcRef,
  FatGrupo: Integer; EstaNoBordero: Boolean; FatNum: Double): Boolean;
begin
(*
  if EstaNoBordero or (Trunc(FatNum) >= 1) then
  begin
*)
    if FatGrupo <> 0 then
    begin
      UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
      'DELETE FROM ' + CO_TabLctA,
      'WHERE FatID <> 0 ',
      'AND FatID IN (' + TXT_FATIDs_PG_CH_DEV_ALL + ')',
      'AND FatParcRef<>0',
      'AND FatParcRef=' + Geral.FF0(FatParcRef),
      'AND FatGrupo<>0',
      'AND FatGrupo=' + Geral.FF0(FatGrupo),
      '']);
    end else begin
      // ter certeza que exclui o principal
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
      //Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0305);
      Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0311);
      Dmod.QrUpd.SQL.Add('AND FatParcela<>0');
      Dmod.QrUpd.SQL.Add('AND FatParcela=' + Geral.FF0(FatParcela));
      Dmod.QrUpd.SQL.Add('AND Ocorreu=' + Geral.FF0(Ocorreu));
      Dmod.QrUpd.SQL.Add('AND Controle=' + Geral.FF0(Controle));
      Dmod.QrUpd.ExecSQL;
      //
      Geral.MensagemBox(
      'N�o h� grupo de exclus�o! ' + #13#10 +
      'Por seguran�a s� o pagamento do valor principal foi exclu�do!' + #13#10 +
      'Exclua manualmente a tarifa, multa, juros e desconto que houver!',
      'Aviso', MB_OK+MB_ICONERROR);
    end;
    //incluir na tabela de LctStep
    DmodFin.FolowStepLct(Controle, 0, CO_STEP_LCT_DELPGC);
    Result := True;
(*
  end else
  begin
    Result := False;
    if Geral.MensagemBox(
    'Exclus�o cancelada! Este pagamento pertence ao lote de Border� n� ' +
    Geral.FFT(FatNum, 0, siNegativo) + ' !' + #13#10 + 'Deseja localizar este lote?',
    'Aviso', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      FmPrincipal.CriaFormLot(2, 0, Trunc(FatNum), FatParcela);
  end;
*)  
end;

function TDmLot.Exclui_DupPg(Controle, FatParcela, Ocorreu, FatParcRef,
  FatGrupo: Integer; EstaNoBordero: Boolean; FatNum: Double): Boolean;
begin
(*
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0312);
  Dmod.QrUpd.SQL.Add('AND FatParcela<>0');
  Dmod.QrUpd.SQL.Add('AND FatParcela=' + Geral.FF0(FatParcela));
  Dmod.QrUpd.SQL.Add('AND Ocorreu=' + Geral.FF0(Ocorreu));
  Dmod.QrUpd.SQL.Add('AND Controle=' + Geral.FF0(Controle));
  Dmod.QrUpd.ExecSQL;
*)
{ N�o tem FatNum! N�o registra pagamento no border� como no CHQ?}
  if (EstaNoBordero and (Trunc(FatNum) >= 1))
  or ((EstaNoBordero = False) and (Trunc(FatNum) = 0)) then
  begin
    if FatGrupo <> 0 then
    begin
      UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
      'DELETE FROM ' + CO_TabLctA,
      'WHERE FatID <> 0 ',
      'AND FatID IN (' + TXT_FATIDs_PG_DU_DEV_ALL + ')',
      'AND FatParcRef<>0',
      'AND FatParcRef=' + Geral.FF0(FatParcRef),
      'AND FatGrupo<>0',
      'AND FatGrupo=' + Geral.FF0(FatGrupo),
      '']);
    end else begin
      // ter certeza que exclui o principal
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
      Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0312);
      Dmod.QrUpd.SQL.Add('AND FatParcela<>0');
      Dmod.QrUpd.SQL.Add('AND FatParcela=' + Geral.FF0(FatParcela));
      Dmod.QrUpd.SQL.Add('AND Ocorreu=' + Geral.FF0(Ocorreu));
      Dmod.QrUpd.SQL.Add('AND Controle=' + Geral.FF0(Controle));
      Dmod.QrUpd.ExecSQL;
      //
      Geral.MensagemBox(
      'N�o h� grupo de exclus�o! ' + #13#10 +
      'Por seguran�a s� o pagamento do valor principal foi exclu�do!' + #13#10 +
      'Exclua manualmente a tarifa, multa, juros e desconto que houver!',
      'Aviso', MB_OK+MB_ICONERROR);
    end;
    //incluir na tabela de LctStep
    DmodFin.FolowStepLct(Controle, 0, CO_STEP_LCT_DELPGC);
    Result := True;
  end else
  begin
    Result := False;
    if Geral.MensagemBox(
    'Exclus�o cancelada! Este pagamento pertence ao lote de Border� n� ' +
    Geral.FFT(FatNum, 0, siNegativo) + ' !' + #13#10 + 'Deseja localizar este lote?',
    'Aviso', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      FmPrincipal.CriaFormLot(2, 0, Trunc(FatNum), FatParcela);
  end;
  //
end;

procedure TDmLot.Exclui_OcorP(Controle, FatParcela, Ocorreu: Integer);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0304);
  Dmod.QrUpd.SQL.Add('AND FatParcela<>0');
  Dmod.QrUpd.SQL.Add('AND FatParcela=' + Geral.FF0(FatParcela));
  Dmod.QrUpd.SQL.Add('AND Ocorreu=' + Geral.FF0(Ocorreu));
  Dmod.QrUpd.SQL.Add('AND Controle=' + Geral.FF0(Controle));
  Dmod.QrUpd.ExecSQL;
end;

function TDmLot.FazProrrogacao(const FatParcela, Cliente: Integer; const
  Vencimento, DDeposito: TDateTime; Credito: Double;
  const NaoDeposita: Boolean; const NomeForm: String;
  const SQLType: TSQLType; var NovaData: TDateTime): Boolean;
  //
  procedure MostraGerChqOcor(SQLType: TSQLType; Prorroga: Boolean);
  begin
    if DBCheck.CriaFm(TFmGerChqOcor, FmGerChqOcor, afmoNegarComAviso) then
    begin
      FmGerChqOcor.ImgTipo.SQLType := SQLType;
      //
      FmGerChqOcor.FLotPrrControle := QrLotPrrControle.Value;
      FmGerChqOcor.FOcorreuCodigo := QrOcorreuCodigo.Value;
      FmGerChqOcor.FFatParcela := FatParcela;
      FmGerChqOcor.FLotPrrRecordCount := QrLotPrr.RecordCount;
      FmGerChqOcor.FForm := NomeForm;
      FmGerChqOcor.FNaoDeposita := NaoDeposita;
      FmGerChqOcor.FDDeposito := DDeposito;
      FmGerChqOcor.FVencimento := Vencimento;
      FmGerChqOcor.FCredito:= Credito;
      FmGerChqOcor.FCliente := Cliente;
      //
      if SQLType = stUpd then
      begin
        FmGerChqOcor.EdOcorrencia.Text := IntToStr(QrOcorreuOcorrencia.Value);
        FmGerChqOcor.CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        FmGerChqOcor.TPDataO.Date := QrOcorreuDataO.Value;
        FmGerChqOcor.EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
        FmGerChqOcor.EdTaxaP.Text := Geral.FFT(QrOcorreuTaxaP.Value, 4, siPositivo);
        FmGerChqOcor.GBPror.Visible := Prorroga;
      end else
      begin
        FmGerChqOcor.EdOcorrencia.ValueVariant := 0;
        FmGerChqOcor.CBOcorrencia.KeyValue := Null;
        FmGerChqOcor.TPDataO.Date := Date;
        FmGerChqOcor.EdValor.ValueVariant := 0;
        FmGerChqOcor.EdTaxaP.ValueVariant := 0;
        //
        FmGerChqOcor.ConfiguraPr(Date);
      end;
      FmGerChqOcor.ShowModal;
      NovaData := FmGerChqOcor.FNovaData;
      FmGerChqOcor.Destroy;
    end;
  end;
begin
  ReopenLotPrr(QrLotPrr, FatParcela, 0(*Controle*), 0(*LotPrr*));
  ReopenOcorreu(QrOcorreu, FatParcela, 0(*Codigo*), 0(*Ocorreu*));
  //
  MostraGerChqOcor(SQLType, True);
  //
  Result := True;
end;

procedure TDmLot.FechaTudoSac();
begin
  QrSacOcorA.Close;
  QrSacCHDevA.Close;
  QrSacRiscoC.Close;
  QrSacRiscoTC.Close;
  QrSacDOpen.Close;
end;

function TDmLot.SQL_Extra_CH_DU(QrUpd: TmySQLQuery; SQLType: TSQLType;
  const Genero: Integer; const Debito, Credito: Double; const Compensado:
  String; const Sit: Integer; const Vencimento: String; const FatID, FatID_Sub:
  Integer; const FatNum: Double; var FatParcela: Integer; const Cliente,
  CliInt: Integer; const MoraVal: Double; const DataDoc: String; const Desco:
  Double; const FatParcRef, Ocorreu: Integer; const Data: String; const Tipo,
  Carteira: Integer; var Controle: Integer; const Sub, FatGrupo: Integer;
  Antigo: String = ''): Boolean;
begin
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      CO_TabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(FatID, CO_TabLctA);
  end;
  //
  Result := UMyMod.SQLInsUpd(QrUpd, SQLType, CO_TabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',
  'Descricao', 'SerieNF', 'NotaFiscal',*)
  'Debito', 'Credito', 'Compensado',
  (*'SerieCH', 'Documento',*) 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', 'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',*)
  'MoraVal', (*'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal',*) 'Antigo',
  (*'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag', 'Comp',
  'Praca', 'Bruto',*) 'Desco',
  (*'DCompra', 'DDeposito', 'DescAte',
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', 'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg', 'Data3', 'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste', 'Tipific', 'StatusSPC',
  'CNAB_Lot', 'MoraTxa'*) 'FatParcRef',
  'Ocorreu', 'FatGrupo'], [
  'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,
  Descricao, SerieNF, NotaFiscal,*)
  Debito, Credito, Compensado,
  (*SerieCH, Documento,*) Sit,
  Vencimento, FatID, FatID_Sub,
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,*)
  MoraVal, (*MultaVal, Protesto, *)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V, Duplicata, Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal,*) Antigo,
  (*ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, SubPgto1,
  MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Endossan, Endossad,
  Cancelado, EventosCad, Encerrado,
  ErrCtrl, IndiPag, Comp,
  Praca, Bruto,*) Desco,
  (*DCompra, DDeposito, DescAte,
  TxaCompra, TxaJuros, TxaAdValorem,
  VlrCompra, VlrAdValorem, DMais,
  Dias, Devolucao, ProrrVz,
  ProrrDd, Quitado, BcoCobra,
  AgeCobra, TotalJr, TotalDs,
  TotalPg, Data3, Data4,
  Repassado, Depositado, ValQuit,
  ValDeposito, TpAlin, AliIts,
  Alin#Pgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli,
  Teste, Tipific, StatusSPC,
  CNAB_Lot, MoraTxa*) FatParcRef,
  Ocorreu, FatGrupo], [
  Data, Tipo, Carteira, Controle, Sub], True);
end;

function TDmLot.LOI_Upd_FPC(QrUpd: TmySQLQuery; FatParcCtrl: Integer;
  SQLCampos: array of String; ValCampos: array of Variant): Boolean;
begin
  Result := False;
  if FatParcCtrl <> 0 then
  begin
    Result := UMyMod.CarregaSQLInsUpd(QrUpd, stUpd, LowerCase(CO_TabLctA), False,
    SQLCampos, ['FatID', 'FatParcela'],
    ValCampos, [VAR_FATID_0301, FatParcCtrl], True, False, '');
    if Result then
    begin
      try
        QrUpd.ExecSQL;
        Result := True;
      except
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
        raise;
      end;
    end else; // ?
  end else Geral.MensagemBox('Erro em "LOI_Upd_FPC" Controle=0!',
  'Erro', MB_OK+MB_ICONERROR);
end;

function TDmLot.LOI_Upd_NUM(QrUpd: TmySQLQuery;
  FatID, FatNum, FatParcela, Controle: Integer; SQLCampos: array of String;
  ValCampos: array of Variant): Boolean;
begin
  //Result := False;
  {
  if FatNum in ([VAR_FATID_0322, VAR_FATID_0333, VAR_FATID_0341,
  VAR_FATID_0342, VAR_FATID_0343, VAR_FATID_0344, VAR_FATID_0345,
  VAR_FATID_0361, VAR_FATID_0362, VAR_FATID_0371]) then
  }
  begin
    Result := UMyMod.CarregaSQLInsUpd(QrUpd, stUpd, LowerCase(CO_TabLctA), False,
    SQLCampos, ['FatID', 'FatNum', 'FatParcela', 'Controle'],
    ValCampos, [FatID, FatNum, FatParcela, Controle], True, False, '');
    if Result then
    begin
      try
        QrUpd.ExecSQL;
        Result := True;
      except
        UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrUpd, '', nil, True, True);
        raise;
      end;
    end else; // ?
  end{
   else Geral.MensagemBox('Erro em "LOI_Upd_NUM" FatID n�o habilitado!',
  'Erro', MB_OK+MB_ICONERROR);
  }
end;

function TDmLot.LOT_Ins_Upd(QrUpd: TmySQLQuery; Tipo: TSQLType; DesEncerra: Boolean;
  SQLCampos, SQLIndex: array of String; ValCampos, ValIndex: array of Variant;
  UserDataAlterweb: Boolean; ComplUpd: String; InfoSQLOnError,
  InfoErro: Boolean): Boolean;
begin
  UMyMod.SQLInsUpd(QrUpd, Tipo, CO_TabLotA, False,
  SQLCampos, SQLIndex, ValCampos, ValIndex,
  UserDataAlterweb, ComplUpd, InfoSQLOnError, InfoErro);
  //
  if (Tipo = stUpd) and DesEncerra then
  begin
    UMyMod.SQLInsUpd(QrUpd, Tipo, CO_TabLotA, False,
    ['LctsAuto'], SQLIndex, [0], ValIndex, False);
  end;
  //
  Result := True;
end;

function TDmLot.ObterSitOriginalDeChequeDevolvido(
  ChequeOrigem: Integer): Integer;
begin
  // Verifica se era um cheque � vista!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqLctA, Dmod.MyDB, [
  'SELECT lct.Sit, lct.Tipo, lct.Carteira, lct.Controle, ',
  'lct.Pago, lct.Compensado, car.Tipo TpCart ',
  'FROM ' + CO_TabLctA + ' lct ',
  'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira ',
  'WHERE lct.FatID=' + TXT_VAR_FATID_0301,
  'AND lct.FatParcela=' + Geral.FF0(ChequeOrigem),
  '']);
  //
  if QrPsqLctACompensado.Value > 2 then
    Result := 2
  else if QrPsqLctAPago.Value >= 0.01 then
    Result := 1
  else
    Result := 0;
  // N�o Fechar! vai ser usado dados da tabela!
end;

procedure TDmLot.PesquisaRiscoSacado(CPF: String; CH_Risc, CH_Venc, CH_SubT,
DU_Risc, DU_Venc, DU_SubT,
ST_Risc, ST_Venc, ST_SubT,
OC_Risc, TO_Tudo: TdmkEdit);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  FmPrincipal.FCPFSacado := Geral.SoNumero_TT(CPF);
  //FNomeSacado := '';
  // soma antes do �ltimo !!!!
  ReopenSacCHDevA;
  ReopenSacOcorA;
  // fim somas.
  //
  ReopenSacRiscoC;
  //
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenSacDOpen;
  //
  DR := 0; DV := 0;
  while not QrSacDOpen.Eof do
  begin
    if Int(Date) > QrSacDOpenVencimento.Value then
      DV := DV + QrSacDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrSacDOpenSALDO_ATUALIZADO.Value;
    QrSacDOpen.Next;
  end;
  DT := DR+DV;
  QrSacDOpen.First;
  CR := QrSacRiscoTCValor.Value;
  CV := FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FOcorA_Total;
  TT := ST + OA;
  //

  if CH_Risc <> nil then CH_Risc.ValueVariant := CR;
  if CH_Venc <> nil then CH_Venc.ValueVariant := CV;
  if CH_SubT <> nil then CH_SubT.ValueVariant := CT;

  if DU_Risc <> nil then DU_Risc.ValueVariant := DR;
  if DU_Venc <> nil then DU_Venc.ValueVariant := DV;
  if DU_SubT <> nil then DU_SubT.ValueVariant := DT;

  if ST_Risc <> nil then ST_Risc.ValueVariant := RT;
  if ST_Venc <> nil then ST_Venc.ValueVariant := VT;
  if ST_SubT <> nil then ST_SubT.ValueVariant := ST;

  if OC_Risc <> nil then OC_Risc.ValueVariant := OA;
  if TO_Tudo <> nil then TO_TUDO.ValueVariant := TT;
  
end;

procedure TDmLot.QrAlinItsCalcFields(DataSet: TDataSet);
begin
  QrAlinItsDATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData1.Value);
  QrAlinItsDATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData2.Value);
  QrAlinItsDATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData3.Value);
  QrAlinItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAlinItsCPF.Value);
end;

procedure TDmLot.QrCHDevAAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  FCHDevOpen_Total := 0;
  while not QrCHDevA.Eof do
  begin
    //if Int(Date) > QrCHDevAData1.Value then
      FCHDevOpen_Total := FCHDevOpen_Total + QrCHDevAATUAL.Value;
    QrCHDevA.Next;
  end;
  Enab  := (FmLot2Cab.QrLot.State <> dsInactive) and (FmLot2Cab.QrLot.RecordCount > 0) and (FmLot2Cab.QrLotLctsAuto.Value = 0);
  Enab2 := (QrCHDevA.State <> dsInactive) and (QrCHDevA. RecordCount > 0);
  //
  FmLot2Cab.BtPagamento7.Enabled := Enab and Enab2; 
end;

procedure TDmLot.QrCHDevAAfterScroll(DataSet: TDataSet);
begin
  if QrCHDevASALDO.Value > 0 then FmLot2Cab.BtPagamento7.Enabled := True
  else FmLot2Cab.BtPagamento7.Enabled := False;
end;

procedure TDmLot.QrCHDevABeforeClose(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
  //
  FmLot2Cab.BtPagamento7.Enabled := False;
end;

procedure TDmLot.QrCHDevACalcFields(DataSet: TDataSet);
begin
  QrCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData1.Value);
  QrCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData2.Value);
  QrCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrCHDevAData3.Value);
  QrCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevACPF.Value);
  QrCHDevASALDO.Value := QrCHDevAValor.Value +QrCHDevATaxas.Value +
    QrCHDevAMulta.Value + QrCHDevAJurosV.Value - QrCHDevAValPago.Value -
    QrCHDevADesconto.Value - QrCHDevAPgDesc.Value;
  //
{
  QrCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrCHDevACliente.Value, QrCHDevAStatus.Value, QrCHDevAData1.Value, Date,
    QrCHDevAData3.Value, QrCHDevAValor.Value, QrCHDevAJurosV.Value,
    QrCHDevADesconto.Value, QrCHDevAValPago.Value + QrCHDevAPgDesc.Value,
    QrCHDevAJurosP.Value, False);
}
  QrCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrCHDevACliente.Value, QrCHDevAStatus.Value, QrCHDevAData1.Value, Date,
    QrCHDevAData3.Value, QrCHDevASALDO.Value,
    0(*QrCHDevAJurosV.Value*),
    0(*QrCHDevADesconto.Value*),
    0(*QrCHDevAValPago.Value + QrCHDevAPgDesc.Value*),
    QrCHDevAJurosP.Value, False);
end;

procedure TDmLot.QrClientesAfterScroll(DataSet: TDataSet);
var
  i, Lins: Integer;
begin
  if (DmLot.FShowLot2Inn) and (DmLot.FShowLot2Cad) then
  begin
    if FmLot2Inn.Pagina.ActivePageIndex = 0 then
    begin
      FmLot2Cad.EdDMaisX.Text := Geral.FFT(QrClientesDMaisC.Value, 0, siNegativo)
    end else begin
      FmLot2Cad.EdDMaisX.Text := Geral.FFT(QrClientesDMaisD.Value, 0, siNegativo);
    end;
  end;

  ////////////////////////////////////////////////////////////////////////
  if DmLot.FShowLot2Cad then
  begin
    FmLot2Cad.EdTxaCompraX.Text := Geral.FFT(QrClientesFatorCompra.Value, 4, siPositivo);
    FmLot2Cad.GradeX1.RowCount := 2;
    MyObjects.LimpaGrade(FmLot2Cad.GradeX1, 1, 1, True);
    Lins := FmPrincipal.FMyDBs.MaxDBs +1;
    if Lins < 2 then Lins := 2;
    FmLot2Cad.GradeX1.RowCount := Lins;
    for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
    begin
      if FmPrincipal.FMyDBs.Connected[i] = '1' then
      begin
        FmLot2Cad.GradeX1.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
        FmLot2Cab.FArrayMySQLEE[i].Close;
        FmLot2Cab.FArrayMySQLEE[i].Params[0].AsInteger := QrClientesCodigo.Value;
        FmLot2Cab.FArrayMySQLEE[i]. Open;
        if FmLot2Cab.QrLotSpread.Value = 0 then
          FmLot2Cad.GradeX1.Cells[01, i+1] := Geral.FFT(
            FmLot2Cab.FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
        else
          FmLot2Cad.GradeX1.Cells[01, i+1] := Geral.FFT(
            FmLot2Cab.FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
      end;
    end;
  end;
end;

procedure TDmLot.QrClientesCalcFields(DataSet: TDataSet);
var
  AdValT, MaiorT: Double;
  i: Integer;
begin
  MaiorT := QrClientesFatorCompra.Value;
  AdValT := QrClientesFatorCompra.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs-1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      FmLot2Cab.FArrayMySQLEE[i].Close;
      FmLot2Cab.FArrayMySQLEE[i].Params[0].AsInteger := QrClientesCodigo.Value;
      FmLot2Cab.FArrayMySQLEE[i]. Open;
      MaiorT := MaiorT + FmLot2Cab.FArrayMySQLEE[i].FieldByName('Maior').AsFloat;
      AdValT := AdValT + FmLot2Cab.FArrayMySQLEE[i].FieldByName('AdVal').AsFloat;
    end;
  end;
  QrClientesMAIOR_T.Value := MaiorT;
  QrClientesADVAL_T.Value := AdValT;
  //
end;

procedure TDmLot.QrDOpenAfterOpen(DataSet: TDataSet);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
  Enab, Enab2: Boolean;
begin
  DR := 0; DV := 0;
  while not QrDOpen.Eof do
  begin
    if Int(Date) > QrDOpenVencimento.Value then
      DV := DV + QrDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrDOpenSALDO_ATUALIZADO.Value;
    QrDOpen.Next;
  end;
  DT := DR+DV;
  QrDOpen.First;
  CR := QrRiscoTCValor.Value;
  CV := FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FOcorA_Total;
  TT := ST + OA;
  FmLot2Cab.EdDV.ValueVariant := DV;
  FmLot2Cab.EdDR.ValueVariant := DR;
  FmLot2Cab.EdDT.ValueVariant := DT;
  FmLot2Cab.EdCV.ValueVariant := CV;
  FmLot2Cab.EdCR.ValueVariant := CR;
  FmLot2Cab.EdCT.ValueVariant := CT;
  FmLot2Cab.EdRT.ValueVariant := RT;
  FmLot2Cab.EdVT.ValueVariant := VT;
  FmLot2Cab.EdST.ValueVariant := ST;
  FmLot2Cab.EdOA.ValueVariant := OA;
  FmLot2Cab.EdTT.ValueVariant := TT;
  //
  if FShowLot2Inn then
  begin
    with FmLot2Inn do
    begin
      EdDV_C.ValueVariant := DV;
      EdDR_C.ValueVariant := DR;
      EdDT_C.ValueVariant := DT;
      EdCV_C.ValueVariant := CV;
      EdCR_C.ValueVariant := CR;
      EdCT_C.ValueVariant := CT;
      EdRT_C.ValueVariant := RT;
      EdVT_C.ValueVariant := VT;
      EdST_C.ValueVariant := ST;
      EdOA_C.ValueVariant := OA;
      EdTT_C.ValueVariant := TT;
      //
      EdDV_D.ValueVariant := DV;
      EdDR_D.ValueVariant := DR;
      EdDT_D.ValueVariant := DT;
      EdCV_D.ValueVariant := CV;
      EdCR_D.ValueVariant := CR;
      EdCT_D.ValueVariant := CT;
      EdRT_D.ValueVariant := RT;
      EdVT_D.ValueVariant := VT;
      EdST_D.ValueVariant := ST;
      EdOA_D.ValueVariant := OA;
      EdTT_D.ValueVariant := TT;
    end;
  end;
  Enab  := (FmLot2Cab.QrLot.State <> dsInactive) and (FmLot2Cab.QrLot.RecordCount > 0) and (FmLot2Cab.QrLotLctsAuto.Value = 0);
  Enab2 := (QrDOpen.State <> dsInactive) and (QrDOpen.RecordCount > 0);
  //
  FmLot2Cab.BtPagtosInclui9.Enabled := Enab and Enab2;
end;

procedure TDmLot.QrDOpenBeforeClose(DataSet: TDataSet);
begin
  FmLot2Cab.EdDV.ValueVariant := 0;
  FmLot2Cab.EdDR.ValueVariant := 0;
  FmLot2Cab.EdDT.ValueVariant := 0;
  FmLot2Cab.EdCV.ValueVariant := 0;
  FmLot2Cab.EdCR.ValueVariant := 0;
  FmLot2Cab.EdCT.ValueVariant := 0;
  FmLot2Cab.EdRT.ValueVariant := 0;
  FmLot2Cab.EdVT.ValueVariant := 0;
  FmLot2Cab.EdST.ValueVariant := 0;
  FmLot2Cab.EdOA.ValueVariant := 0;
  FmLot2Cab.EdTT.ValueVariant := 0;
  //
  FmLot2Cab.BtPagtosInclui9.Enabled := False;
end;

procedure TDmLot.QrDOpenCalcFields(DataSet: TDataSet);
begin
  QrDOpenSALDO_DESATUALIZ.Value := QrDOpenCredito.Value + QrDOpenTotalJr.Value -
  QrDOpenTotalDs.Value - QrDOpenTotalPg.Value;
  //
  QrDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrDOpenQuitado.Value,
    QrDOpenDDeposito.Value, Date, QrDOpenData3.Value, QrDOpenRepassado.Value);
  //
  QrDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrDOpenCliente.Value, QrDOpenQuitado.Value, QrDOpenVencimento.Value, Date,
    QrDOpenData3.Value, QrDOpenCredito.Value, QrDOpenTotalJr.Value, QrDOpenTotalDs.Value,
    QrDOpenTotalPg.Value, 0, True);
end;

procedure TDmLot.QrLCalcCalcFields(DataSet: TDataSet);
var
 VaValT, AdValT, VaTaxT, TotISS, TotIRR, TotPIS: Double;
 i: Integer;
begin
  AdValT := QrLCalcAdValorem.Value ;
  VaValT := QrLCalcValValorem.Value;
  VaTaxT := QrLCalcTxCompra.Value;
  TotISS := QrLCalcISS_Val.Value;
  TotIRR := QrLCalcIRRF_Val.Value;
  TotPIS := QrLCalcPIS_R_Val.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      AdValT := AdValT + FmLot2Cab.FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat;
      VaValT := VaValT + FmLot2Cab.FArrayMySQLLO[i].FieldByName('ValValorem').AsFloat;
      VaTaxT := VaTaxT + FmLot2Cab.FArrayMySQLLO[i].FieldByName('TaxaVal').AsFloat;
      TotISS := TotISS + FmLot2Cab.FArrayMySQLLO[i].FieldByName('ISS_Val').AsFloat;
      TotIRR := TotIRR + FmLot2Cab.FArrayMySQLLO[i].FieldByName('IRRF_Val').AsFloat;
      TotPIS := TotPIS + FmLot2Cab.FArrayMySQLLO[i].FieldByName('PIS_R_Val').AsFloat;
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrLCalcVAL_LIQUIDO.Value := QrLCalcTotal.Value - VaValT
    - VaTaxT - QrLCalcIRRF_VAL.Value - QrLCalcIOC_VAL.Value
    - QrLCalcIOFd.Value -QrLCalcIOFv.Value -
    - QrLCalcCPMF_VAL.Value - QrLCalcTarifas.Value - QrLCalcOcorP.Value -
    QrLCalcCHDevPG.Value;
  //
end;

procedure TDmLot.QrLotPrrCalcFields(DataSet: TDataSet);
begin
  QrLotPrrDIAS_1_3.Value := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData1.Value));
  QrLotPrrDIAS_2_3.Value := Trunc(Int(QrLotPrrData3.Value)-Int(QrLotPrrData2.Value));
end;

procedure TDmLot.QrOcorAAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  if QrOcorA.RecordCount > 0 then
    FmLot2Cab.BtPagamento6.Enabled := True
  else
    FmLot2Cab.BtPagamento6.Enabled := False;
  //
  FOcorA_Total := 0;
  while not QrOcorA.Eof do
  begin
    //if Int(Date) > QrOcorADataO.Value then
    FOcorA_Total := FOcorA_Total + QrOcorAATUALIZADO.Value;
    QrOcorA.Next;
  end;
  Enab  := (FmLot2Cab.QrLot.State <> dsInactive) and (FmLot2Cab.QrLot.RecordCount > 0) and (FmLot2Cab.QrLotLctsAuto.Value = 0);
  Enab2 := (QrOcorA.State <> dsInactive) and (QrOcorA.RecordCount > 0);
  //
  FmLot2Cab.BtIncluiocor.Enabled := Enab;
  FmLot2Cab.BtPagamento6.Enabled := Enab and Enab2; 
end;

procedure TDmLot.QrOcorABeforeClose(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  FOcorA_Total := 0;
  //
  Enab := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0);
  //
  if FmLot2Cab.BtIncluiocor <> nil then
    FmLot2Cab.BtIncluiocor.Enabled := Enab;
  if FmLot2Cab.BtPagamento6 <> nil then
    FmLot2Cab.BtPagamento6.Enabled := False;
end;

procedure TDmLot.QrOcorACalcFields(DataSet: TDataSet);
begin
  QrOcorASALDO.Value := QrOcorAValor.Value + QrOcorATaxaV.Value - QrOcorAPago.Value;
  //
  QrOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcorACliente.Value, 1, QrOcorADataO.Value, Date, QrOcorAData3.Value,
    QrOcorAValor.Value, QrOcorATaxaV.Value, 0 (*Desco*),
    QrOcorAPago.Value, QrOcorATaxaP.Value, False);
  //
  if QrOcorADescri.Value <> '' then
    QrOcorADOCUM_TXT.Value := QrOcorADescri.Value
  else if QrOcorATIPODOC.Value = 'CH' then
    QrOcorADOCUM_TXT.Value := FormatFloat('000', QrOcorABanco.Value)+'/'+
    FormatFloat('0000', QrOcorAAgencia.Value)+'/'+ QrOcorAConta.Value+'-'+
    FormatFloat('000000', QrOcorACheque.Value)
  else if QrOcorATIPODOC.Value = 'DU' then
    QrOcorADOCUM_TXT.Value := QrOcorADuplicata.Value
  else QrOcorADOCUM_TXT.Value := '';
end;

procedure TDmLot.QrOcorreuCalcFields(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  //
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrOcorreuCliente.Value, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
    QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
    QrOcorreuPago.Value, QrOcorreuTaxaP.Value, False);
  Screen.Cursor := crDefault;
end;

procedure TDmLot.QrRepCliAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (FmLot2Cab.QrLot.State <> dsInactive) and (FmLot2Cab.QrLot.RecordCount > 0) and (FmLot2Cab.QrLotLctsAuto.Value = 0);
  Enab2 := (QrRepCli.State <> dsInactive) and (QrRepCli.RecordCount > 0);
  //
  FmLot2Cab.BtPagTerc.Enabled := Enab;
  FmLot2Cab.BtExclui8.Enabled := Enab and Enab2;
  FmLot2Cab.BtSobra.Enabled   := Enab;
end;

procedure TDmLot.QrRepCliBeforeClose(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (FmLot2Cab.QrLot.State <> dsInactive) and (FmLot2Cab.QrLot.RecordCount > 0);
  //
  FmLot2Cab.BtPagTerc.Enabled := Enab;
  FmLot2Cab.BtExclui8.Enabled := False;
  FmLot2Cab.BtSobra.Enabled   := Enab;
end;

procedure TDmLot.QrRepCliCalcFields(DataSet: TDataSet);
begin
  QrRepCliSEQ.Value := QrRepCli.RecNo;
end;

procedure TDmLot.QrRiscoCCalcFields(DataSet: TDataSet);
begin
  QrRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRiscoCCNPJCPF.Value);
end;

procedure TDmLot.QrSacCHDevAAfterOpen(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
  while not QrSacCHDevA.Eof do
  begin
    //if Int(Date) > QrSacCHDevAData1.Value then
      FCHDevOpen_Total := FCHDevOpen_Total + QrSacCHDevAATUAL.Value;
    QrSacCHDevA.Next;
  end;
end;

procedure TDmLot.QrSacCHDevABeforeClose(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
end;

procedure TDmLot.QrSacCHDevACalcFields(DataSet: TDataSet);
begin
  QrSacCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData1.Value);
  QrSacCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData2.Value);
  QrSacCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData3.Value);
  QrSacCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacCHDevACPF.Value);
  QrSacCHDevASALDO.Value := QrSacCHDevAValor.Value +QrSacCHDevATaxas.Value +
    QrSacCHDevAMulta.Value + QrSacCHDevAJurosV.Value - QrSacCHDevAValPago.Value -
    QrSacCHDevADesconto.Value - QrSacCHDevAPgDesc.Value;
  //
  QrSacCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrSacCHDevACliente.Value, QrSacCHDevAStatus.Value, QrSacCHDevAData1.Value, Date,
    QrSacCHDevAData3.Value, QrSacCHDevAValor.Value, QrSacCHDevAJurosV.Value,
    QrSacCHDevADesconto.Value, QrSacCHDevAValPago.Value +
    QrSacCHDevAPgDesc.Value, QrSacCHDevAJurosP.Value, False);
end;

procedure TDmLot.QrSacDOpenCalcFields(DataSet: TDataSet);
begin
  QrSacDOpenSALDO_DESATUALIZ.Value := QrSacDOpenCredito.Value +
    QrSacDOpenTotalJr.Value - QrSacDOpenTotalDs.Value - QrSacDOpenTotalPg.Value;
  //
  QrSacDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(
    QrSacDOpenQuitado.Value, QrSacDOpenDDeposito.Value, Date,
    QrSacDOpenData3.Value, QrSacDOpenRepassado.Value);
  //
  QrSacDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrSacDOpenCliente.Value, QrSacDOpenQuitado.Value,
    QrSacDOpenVencimento.Value, Date,
    QrSacDOpenData3.Value, QrSacDOpenCredito.Value,
    QrSacDOpenTotalJr.Value, QrSacDOpenTotalDs.Value,
    QrSacDOpenTotalPg.Value, 0, True);
end;

procedure TDmLot.QrSacOcorAAfterOpen(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
  while not QrSacOcorA.Eof do
  begin
    //if Int(Date) > QrSacOcorADataO.Value then
      FOcorA_Total := FOcorA_Total + QrSacOcorAATUALIZADO.Value;
    QrSacOcorA.Next;
  end;
end;

procedure TDmLot.QrSacOcorABeforeClose(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
end;

procedure TDmLot.QrSacOcorACalcFields(DataSet: TDataSet);
begin
  QrSacOcorASALDO.Value := QrSacOcorAValor.Value + QrSacOcorATaxaV.Value - QrSacOcorAPago.Value;
  //
  QrSacOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrSacOcorACliente.Value, 1, QrSacOcorADataO.Value, Date, QrSacOcorAData3.Value,
    QrSacOcorAValor.Value, QrSacOcorATaxaV.Value, 0 (*Desco*),
    QrSacOcorAPago.Value, QrSacOcorATaxaP.Value, False);
end;

procedure TDmLot.QrSacRiscoCCalcFields(DataSet: TDataSet);
begin
  QrSacRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacRiscoCCNPJCPF.Value);
end;

function TDmLot.QtdDePagtosDeChqDev(AlinIts: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgts, Dmod.MyDB, [
  'SELECT COUNT(Controle) Itens ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0311,
  'AND FatParcRef=' + Geral.FF0(AlinIts), // DmLot.QrCHDevACodigo.Value
  '']);
  //
  Result := QrPgtsItens.Value;
end;

procedure TDmLot.ReopenAdupIts(LOI: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAdupIts, Dmod.MyDB, [
  'SELECT od.Nome NOMESTATUS, ad.* ',
  'FROM adupits ad ',
  'LEFT JOIN ocordupl od ON od.Codigo=ad.Alinea ',
  'WHERE ad.' + FLD_LOIS + '=' + Geral.FF0(LOI),
  'ORDER BY ad.DataA DESC ',
  '']);
end;

procedure TDmLot.ReopenCliCHDevA(Cliente, CHDevA: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHDevA, Dmod.MyDB, [
  'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial ',
  'ELSE en.Nome END NOMECLIENTE, ai.* ',
  'FROM alinits ai ',
  'LEFT JOIN entidades en ON en.Codigo=ai.Cliente ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON ai.LoteOrigem=lo.Codigo ',
  'WHERE ai.Status<2 ',
  'AND ai.Cliente=' + FormatFloat('0', Cliente),
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections) + ' >= 0.01 ',
  'ORDER BY ai.Data1, ai.Data2 ',
  '']);
  //
  if CHDevA > 0 then
    QrCHDevA.Locate('Codigo', CHDevA, []);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHDevT, Dmod.MyDB, [
  'SELECT SUM(ai.Valor + ai.Taxas + ai.Multa + ai.JurosV - ai.Desconto - ai.ValPago) ABERTO ',
  'FROM alinits ai ',
  'LEFT JOIN entidades en ON en.Codigo=ai.Cliente ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON ai.LoteOrigem=lo.Codigo ',
  'WHERE ai.Status<2 ',
  'AND ai.Cliente=' + FormatFloat('0', Cliente),
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections) + ' >= 0.01 ',
  '']);
  //
end;

procedure TDmLot.ReopenDUOpen(CNPJCPF: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDUOpen, Dmod.MyDB, [
  'SELECT li.Duplicata, li.DCompra, ',
  'li.Credito, li.DDeposito ',
  'FROM ' + CO_TabLctA + ' li ',
  'WHERE li.CNPJCPF ="' + CNPJCPF + '" ',
  'AND li.Quitado=0 ',
  'AND li.DDeposito>=CURDATE() ',
  'ORDER BY DDeposito ',
  '']);
end;

procedure TDmLot.ReopenDUVenc(CNPJCPF: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDUVenc, Dmod.MyDB, [
  'SELECT li.Duplicata, li.DCompra, ',
  'li.Credito, li.DDeposito ',
  'FROM ' + CO_TabLctA + ' li ',
  'WHERE li.CNPJCPF ="' + CNPJCPF + '" ',
  'AND li.Quitado=0 ',
  'AND li.DDeposito<CURDATE() ',
  'ORDER BY DDeposito ',
  '']);
end;

procedure TDmLot.ReopenILi(Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrILi, Dmod.MyDB, [
  'SELECT DISTINCT CPF ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND FatNum=' + Geral.FF0(Trunc(Lote)),
  '']);

end;

procedure TDmLot.ReopenLastCHDV(FatParcRef: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastCHDV, Dmod.MyDB, [
  'SELECT Max(Data) Data ',
  'FROM ' + CO_TabLctA,
  //'WHERE FatID=' + TXT_VAR_FATID_0305,
  'WHERE FatID=' + TXT_VAR_FATID_0311,
  'AND FatParcRef=' + Geral.FF0(FatParcRef),
  '']);
end;

procedure TDmLot.ReopenLastDPg(LOIS: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastDPg, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0312,
  'AND FatParcRef=' + Geral.FF0(LOIS),
  'ORDER BY Data DESC, FatParcela DESC ',
  '']);
end;

procedure TDmLot.ReopenLastOcor(Ocorreu: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLastOcor, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(Ocorreu),
  'ORDER BY Data Desc, FatID Desc ',
  '']);
end;

procedure TDmLot.ReopenLCalc(Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLCalc, Dmod.MyDB, [
  'SELECT lo.AdValorem, lo.IOC, lo.CPMF, lo.Cliente, lo.Data, ',
  'lo.IRRF, lo.PIS, lo.PIS_R, lo.COFINS, lo.COFINS_R,  lo.ISS, ',
  'lo.ValValorem, lo.TxCompra, lo.ISS_Val, lo.IRRF_Val, ',
  'lo.PIS_R_Val, lo.Total, lo.IOC_Val, lo.CPMF_Val, lo.Tarifas, ',
  'lo.OcorP, lo.CHDevPg, lo.SobraNow, lo.IOFv, lo.IOFd, ',
  'ent.Tipo, ent.Simples ',
  'FROM ' + CO_TabLotA + ' lo ',
  'LEFT JOIN entidades ent on ent.Codigo=lo.Cliente ',
  'WHERE lo.Codigo=' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenLocDU(CNPJCPF, Duplicata: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocDU, Dmod.MyDB, [
  'SELECT Count(*) Duplicatas ',
  'FROM ' + CO_TabLctA,
  'WHERE CNPJCPF="' + CNPJCPF + '" ',
  'AND Duplicata="' + Duplicata + '" ',
  '']);
end;

procedure TDmLot.ReopenLocLote(Cliente, Lote: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocLote, Dmod.MyDB, [
  'SELECT Max(Lote) Lote FROM ' + CO_TablotA,
  'WHERE Cliente=' + FormatFloat('0', Cliente),
  'AND Codigo<>' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenLocOc(Ocorreu: Integer);
begin
{
SELECT * FROM ocor rpg
WHERE Data=(
SELECT Max(Data) FROM ocor rpg
WHERE Ocorreu=:P0)
ORDER BY Codigo DESC
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocOc, Dmod.MyDB, [
  'SELECT Data FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Data=( ',
  '  SELECT MAX(Data) ',
  '  FROM ' + CO_TabLctA,
  '  WHERE FatID=' + TXT_VAR_FATID_0304,
  '  AND Ocorreu=' + FormatFloat('0', Ocorreu),
  ')',
  'ORDER BY FatParcela DESC ',
  '']);
end;

procedure TDmLot.ReopenLocPg(FatParcRef: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocPg, Dmod.MyDB, [
  'SELECT Data, FatParcela ',
  'FROM ' + CO_TabLctA,
  //'WHERE FatID=' + TXT_VAR_FATID_0305,
  'WHERE FatID=' + TXT_VAR_FATID_0311,
  'AND Data=( ',
  '  SELECT Max(Data) ',
  '  FROM ' + CO_TabLctA,
  '  WHERE FatParcRef=' + Geral.FF0(FatParcRef),
  ') ',
  'ORDER BY FatParcela DESC ',
  '']);
end;

procedure TDmLot.ReopenLocs(Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocs, Dmod.MyDB, [
  'SELECT Codigo, Tipo ',
  'FROM ' + CO_TabLotA,
  'WHERE Codigo >= ' + FormatFloat('0', Lote),
  'ORDER BY Codigo ',
  '']);
end;

procedure TDmLot.ReopenLOI(LOI: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLOI, Dmod.MyDB, [
  'SELECT loi.Vencimento, loi.Cliente, ',
  'loi.Controle, loi.Sub, loi.Duplicata, ',
  'loi.Credito, loi.Debito ',
  'FROM ' + CO_TabLctA + ' loi ',
  'WHERE loi.FatID=' + TXT_VAR_FATID_0301,
  'AND loi.FatParcela=' + Geral.FF0(LOI),
  '']);
end;

procedure TDmLot.ReopenLotPrr(QrlotPrr: TmySQLQuery; FatParcela, Controle,
  LotPrr: Integer);
begin
{
  QrLotPrr.Close;
  QrLotPrr.Params[00].AsInteger := QrPesqFatParcela.Value;
  UMyMod.AbreQuery(QrLotPrr);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotPrr, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + CO_TabLotPrr,
  'WHERE Codigo=' + FormatFloat('0', FatParcela),
  'ORDER BY Controle ',
  '']);
  //
  if Controle <> 0 then
    QrLotPrr.Locate('Controle', Controle, [])
  else
    QrLotPrr.Locate('Controle', LotPrr, []);
end;

procedure TDmLot.ReopenNovoTxs();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNovoTxs, Dmod.MyDB, [
  'SELECT * ',
  'FROM taxas ',
  'WHERE Codigo <> -1 ',  // -> Tarifa de cheque devolvido
  'ORDER BY Nome ',
  '']);
end;

procedure TDmLot.ReopenOcorA(Cliente, OcorA: Integer);
begin
{
  QrOcorA.Close;
  QrOcorA.Params[0].AsInteger := Cliente;
  QrOcorA.Params[1].AsInteger := Cliente;
  QrOcorA. Open;
  //
  if FOcorA > 0 then QrOcorA.Locate('Codigo', FOcorA, []);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorA, Dmod.MyDB, [
  'SELECT ob.Nome NOMEOCORRENCIA, ob.PlaGen, ',
  'oc.' + FLD_LOIS + ' LOIS, oc.*, ',
  'ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC ',
  'FROM ocorreu oc ',
  'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
  'WHERE oc.Status<2 ',
  'AND (oc.Valor + oc.TaxaV - oc.Pago <> 0) ',
  'AND (oc.Cliente=' + FormatFloat('0', Cliente) + ') ',
  '']);
  if OcorA > 0 then QrOcorA.Locate('Codigo', OcorA, []);
end;

procedure TDmLot.ReopenOcorreu(QrOcorreu: TmySQLQuery; FatParcela, Codigo,
Ocorreu: Integer);
begin
{
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrPesqFatParcela.Value;
  UMyMod.AbreQuery(QrOcorreu);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorreu, Dmod.MyDB, [
  'SELECT  ob.Nome NOMEOCORRENCIA, ob.PlaGen, ',
  'oc.' + FLD_LOIS + ' LOIS, oc.* ',
  'FROM ocorreu oc ',
  'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
  'WHERE oc.' + FLD_LOIS + '=' + FormatFloat('0', FatParcela),
  '']);
  //
  if Codigo <> 0 then
    QrOcorreu.Locate('Codigo', Codigo, [])
  else
    QrOcorreu.Locate('Codigo', Ocorreu, []);
end;

procedure TDmLot.ReopenPgD(FatParcela: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPgD, Dmod.MyDB, [
  'SELECT SUM(ad.MoraVal) Juros, SUM(ad.DescoVal) Desco, ',
  'SUM(ad.Credito) Pago, li.Credito Valor, MAX(ad.Data) MaxData ',
  'FROM ' + CO_TabLctA + ' ad ',
  'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ad.FatParcRef',
  'WHERE li.FatID=' + TXT_VAR_FATID_0301,
  'AND ad.FatID=' + TXT_VAR_FATID_0312,
  'AND ad.FatParcRef=' + Geral.FF0(FatParcela),
  'GROUP BY ad.FatParcRef',
  '']);
end;

procedure TDmLot.ReopenSacCHDevA();
begin
{
  QrSacCHDevA.Close;
  QrSacCHDevA.SQL.Clear;
  QrSacCHDevA.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrSacCHDevA.SQL.Add('ELSE en.Nome END NOMECLIENTE, ai.*');
  QrSacCHDevA.SQL.Add('FROM alinits ai');
  QrSacCHDevA.SQL.Add('LEFT JOIN entidades en ON en.Codigo=ai.Cliente');
  if FmPrincipal.FConnections = 0 then
  begin
    //QrSacCHDevA.SQL.Add('LEFT JOIN lot es its li ON li.Controle=ai.ChequeOrigem');
    QrSacCHDevA.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lo ON ai.LoteOrigem=lo.Codigo');
  end;
  QrSacCHDevA.SQL.Add('WHERE ai.Status<2');
  if FmPrincipal.FConnections = 0 then
  begin
    QrSacCHDevA.SQL.Add('AND lo.TxCompra + lo.ValValorem + '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  end;
  if FmPrincipal.FCPFSacado <> '' then
    QrSacCHDevA.SQL.Add('AND ai.CPF = "' + FmPrincipal.FCPFSacado + '"');
  QrSacCHDevA.SQL.Add('ORDER BY ai.Data1, ai.Data2');
  QrSacCHDevA.SQL.Add('');
  QrSacCHDevA. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSacCHDevA, Dmod.MyDB, [
  'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial',
  'ELSE en.Nome END NOMECLIENTE, ai.*',
  'FROM alinits ai',
  'LEFT JOIN entidades en ON en.Codigo=ai.Cliente',
   Geral.ATS_if(FmPrincipal.FConnections = 0,
   ['LEFT JOIN ' + CO_TabLotA + ' lo ON ai.LoteOrigem=lo.Codigo']),
  'WHERE ai.Status<2',
   Geral.ATS_if(FmPrincipal.FConnections = 0,
   ['AND lo.TxCompra + lo.ValValorem + ' +
     IntToStr(FmPrincipal.FConnections) + ' >= 0.01']),
   Geral.ATS_if(FmPrincipal.FCPFSacado <> '',
   ['AND ai.CPF = "' + FmPrincipal.FCPFSacado + '"']),
  'ORDER BY ai.Data1, ai.Data2',
  '']);
end;

procedure TDmLot.ReopenSacDOpen();
begin
{
  QrSacDOpen.Close;
  QrSacDOpen.SQL.Clear;
  QrSacDOpen.SQL.Add('SELECT od.Nome STATUS, li.FatParcela, li.Duplicata, ');
  QrSacDOpen.SQL.Add('li.Repassado, li.DCompra, li.Valor, li.DDeposito, ');
  QrSacDOpen.SQL.Add('li.Emitente, li.CNPJCPF, lo.Cliente, li.Quitado, ');
  QrSacDOpen.SQL.Add('li.TotalJr, li.TotalDs, li.TotalPg, ');
  QrSacDOpen.SQL.Add('li.Vencimento, li.Data3');
  QrSacDOpen.SQL.Add('FROM ' + CO_TabLctA + ' li');
  QrSacDOpen.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum');
  QrSacDOpen.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrSacDOpen.SQL.Add('WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301));
  QrSacDOpen.SQL.Add('AND lo.Tipo=1');
  QrSacDOpen.SQL.Add('AND lo.TxCompra + lo.ValValorem + '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacDOpen.SQL.Add('AND li.Quitado <> 2');
  if FmPrincipal.FCPFSacado <> '' then
    QrSacDOpen.SQL.Add('AND li.CNPJCPF = "' + FmPrincipal.FCPFSacado + '"');
  QrSacDOpen.SQL.Add('ORDER BY li.Vencimento');
  QrSacDOpen.SQL.Add('');
  QrSacDOpen. Open;
}

  UnDmkDAC_PF.AbreMySQLQuery0(QrSacDOPen, Dmod.MyDB, [
  'SELECT od.Nome STATUS, li.FatParcela, li.Duplicata, ',
  'li.Repassado, li.DCompra, li.Credito, li.DDeposito, ',
  'li.Emitente, li.CNPJCPF, lo.Cliente, li.Quitado, ',
  'li.TotalJr, li.TotalDs, li.TotalPg, ',
  'li.Vencimento, li.Data3',
  'FROM ' + CO_TabLctA + ' li',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
  'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao',
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo=1',
  'AND lo.TxCompra + lo.ValValorem + '+ IntToStr(FmPrincipal.FConnections)+' >= 0.01',
  'AND li.Quitado <> 2',
   Geral.ATS_if(FmPrincipal.FCPFSacado <> '',
   ['AND li.CNPJCPF = "' + FmPrincipal.FCPFSacado + '"']),
  'ORDER BY li.Vencimento',
  '']);
  //
end;

procedure TDmLot.ReopenSacOcorA();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSacOcorA, Dmod.MyDB, [
  'SELECT ELT(TpOcor, "CH", "DU", "CL", "??") TIPODOC, ',
  'ob.Nome NOMEOCORRENCIA, ocr.* ',
  'FROM ocorreu ocr ',
  'LEFT JOIN ocorbank ob ON ob.Codigo   = ocr.Ocorrencia ',
  'WHERE ocr.Status<2 ',
   Geral.ATS_if(FmPrincipal.FCPFSacado <> '',
   ['AND ocr.CPF = "' + FmPrincipal.FCPFSacado + '"']),
  '']);
end;

procedure TDmLot.ReopenSacRiscoC();
begin
{
  QrSacRiscoC.Close;
  QrSacRiscoC.SQL.Clear;
  QrSacRiscoC.SQL.Add('SELECT lo.Tipo, li.Banco, li.Agencia, ');
  QrSacRiscoC.SQL.Add('li.ContaCorrente, li.Documento, li.Valor,');
  QrSacRiscoC.SQL.Add('li.DCompra, li.DDeposito, li.Emitente, li.CNPJCPF');
  QrSacRiscoC.SQL.Add('FROM ' + CO_TabLctA + ' li');
  QrSacRiscoC.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum');
  QrSacRiscoC.SQL.Add('WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301));
  QrSacRiscoC.SQL.Add('AND lo.Tipo = 0');
  QrSacRiscoC.SQL.Add('AND lo.TxCompra + lo.ValValorem + '+
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacRiscoC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FmPrincipal.FCPFSacado <> '' then
    QrSacRiscoC.SQL.Add('AND li.CNPJCPF = "' + FmPrincipal.FCPFSacado + '"');
  QrSacRiscoC.SQL.Add('ORDER BY DDeposito');
  QrSacRiscoC. Open;
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSacRiscoC, Dmod.MyDB, [
  'SELECT lo.Tipo, li.Banco, li.Agencia, ',
  'li.ContaCorrente, li.Documento, li.Credito,',
  'li.DCompra, li.DDeposito, li.Emitente, li.CNPJCPF',
  'FROM ' + CO_TabLctA + ' li',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0',
  'AND lo.TxCompra + lo.ValValorem + ' +
  IntToStr(FmPrincipal.FConnections)+' >= 0.01',
  'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())',
  Geral.ATS_if(FmPrincipal.FCPFSacado <> '',
  ['AND li.CNPJCPF = "' + FmPrincipal.FCPFSacado + '"']),
  'ORDER BY DDeposito',
  '']);
  //
{
  QrSacRiscoTC.Close;
  QrSacRiscoTC.SQL.Clear;
  QrSacRiscoTC.SQL.Add('SELECT SUM(li.Credito) Valor');
  QrSacRiscoTC.SQL.Add('FROM ' + CO_TabLctA + ' li');
  QrSacRiscoTC.SQL.Add('LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum');
  QrSacRiscoTC.SQL.Add('WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301));
  QrSacRiscoTC.SQL.Add('AND lo.Tipo = 0');
  QrSacRiscoTC.SQL.Add('AND lo.TxCompra + lo.ValValorem + '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacRiscoTC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FmPrincipal.FCPFSacado <> '' then
    QrSacRiscoTC.SQL.Add('AND li.CNPJCPF = "' + FmPrincipal.FCPFSacado + '"');
  QrSacRiscoTC. Open;
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSacRiscoTC, Dmod.MyDB, [
  'SELECT SUM(li.Credito) Valor',
  'FROM ' + CO_TabLctA + ' li',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0',
  'AND lo.TxCompra + lo.ValValorem + ' +
  IntToStr(FmPrincipal.FConnections)+' >= 0.01',
  'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())',
  Geral.ATS_if(FmPrincipal.FCPFSacado <> '',
  ['AND li.CNPJCPF = "' + FmPrincipal.FCPFSacado + '"']),
  '']);
end;

procedure TDmLot.ReopenSDUOpen(CNPJCPF: String);
begin
  UnDmkDAC_PF.AbreMySQLQUery0(QrSDUOpen, Dmod.MyDB, [
  'SELECT SUM(li.Credito) Valor, COUNT(*) DUPLICATAS ',
  'FROM ' + CO_TabLctA + ' li ',
  'WHERE li.CNPJCPF ="' + CNPJCPF + '" ',
  'AND li.Quitado=0 ',
  'ORDER BY DDeposito ',
  '']);
end;

procedure TDmLot.ReopenSPC_Cli(Entidade: Integer);
begin
{
  QrSPC_Cli.Close;
  QrSPC_Cli.Params[0].AsInteger := Entidade;
  QrSPC_Cli. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSPC_Cli, Dmod.MyDB, [
  'SELECT sm.Valor VALCONSULTA,sc.Codigo, sp.ValMin SPC_ValMin, ',
  'sp.ValMax SPC_ValMax, sc.Servidor, sc.Porta SPC_Porta, ',
  'sc.Pedinte, sc.CodigSocio, sc.SenhaSocio, sc.Modalidade, ',
  'sc.InfoExtra, sc.BAC_CMC7, sc.TipoCred, ValAviso ',
  'FROM spc_config sc ',
  'LEFT JOIN spc_entida sp ON sp.SPC_Config=sc.Codigo ',
  'LEFT JOIN spc_modali sm ON sm.Codigo=sc.Modalidade ',
  'WHERE sp.Entidade=' + FormatFloat('0', Entidade),
  '']);
end;

procedure TDmLot.ReopenSPC_Result(CPF: String; Linha: Integer);
begin
{
  QrSPC_Result.Close;
  QrSPC_Result.Params[0].AsString := CPF;
  QrSPC_Result. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSPC_Result, Dmod.MyDB, [
  'SELECT Linha, Texto ',
  'FROM spc_result ',
  'WHERE CPFCNPJ="' + CPF + '"' ,
  'ORDER BY Linha ',
  '']);
  //
  if Linha > 0 then
    QrSPC_Result.Locate('Linha', Linha, []);
end;

procedure TDmLot.ReopenSumCHP(Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCHP, Dmod.MyDB, [
  'SELECT SUM(ap.Credito-ap.Debito) Pago ',
  'FROM ' + CO_TabLctA + ' ap ',
  'WHERE FatID IN (' + TXT_FATIDs_PG_CH_DEV_SUM + ')',
  'AND ap.FatNum=' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenSumDUP(Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumDUP, Dmod.MyDB, [
  'SELECT SUM(Credito-Debito) Pago  ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID IN (' + TXT_FATIDs_PG_DU_DEV_SUM + ')',
  'AND FatNum=' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenSumOc(OcorP: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOC, Dmod.MyDB, [
  'SELECT SUM(MoraVal) Juros, SUM(Credito-Debito) Pago ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Ocorreu=' + Geral.FF0(OcorP),
  '']);
end;

procedure TDmLot.ReopenSumOco(Lote: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOco, Dmod.MyDB, [
  'SELECT SUM(op.Credito-op.Debito) PAGO ',
  'FROM ' + CO_TabLctA + ' op ',
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND op.FatNum=' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenSumPgCHQ(FatParcRef: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPgCHQ, Dmod.MyDB, [
  'SELECT SUM(Credito-Debito) Pago, ',
  'SUM(IF(FatID=303, Credito-Debito, 0)) Taxas, ',
  'SUM(IF(FatID=304, Credito-Debito, 0)) Multa, ',
  'SUM(IF(FatID=305, Credito-Debito, 0)) Juros, ',
  'SUM(IF(FatID=307, Debito-Credito, 0)) Desco, ',
  'SUM(IF(FatID=311, Credito-Debito, 0)) Princ ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID IN (' + TXT_FATIDs_PG_CH_DEV_ALL + ') ',
  'AND FatGrupo <> 0 ',
  'AND FatParcRef=' + Geral.FF0(FatParcRef),
  '']);
  //
{
  Acho que n�o vou precisar!
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqLctC, Dmod.MyDB, [
  'SELECT Sit ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0301,
  'AND FatParcela=' + Geral.FF0(FatParcRef),
  '']);
  Result := QrPsqLctCSit.Value;
}
end;

procedure TDmLot.ReopenSumPgDUP(FatParcRef: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumPgDUP, Dmod.MyDB, [
  'SELECT SUM(Credito-Debito) Pago, ',
  'SUM(IF(FatID=303, Credito-Debito, 0)) Taxas, ',
  'SUM(IF(FatID=304, Credito-Debito, 0)) Multa, ',
  'SUM(IF(FatID=306, Credito-Debito, 0)) Juros, ',
  'SUM(IF(FatID=308, Debito-Credito, 0)) Desco, ',
  'SUM(IF(FatID=312, Credito-Debito, 0)) Princ, ',
  'MAX(Data) MaxData ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID IN (' + TXT_FATIDs_PG_DU_DEV_ALL + ') ',
  'AND FatGrupo <> 0 ',
  'AND FatParcRef=' + Geral.FF0(FatParcRef),
  '']);
  //
end;

procedure TDmLot.ReopenSumRepCli(Lote: Double);
begin
{
  DmLot.QrSumRepCli.Close;
  DmLot.QrSumRepCli.Params[0].AsFloat := Lote;
  DmLot.QrSumRepCli. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrSumRepCli, Dmod.MyDB, [
  'SELECT SUM(loi.Credito) Valor ',
  'FROM ' + CO_TabLctA + ' loi ',
  'WHERE FatID=' + TXT_VAR_FATID_0301,
  'AND loi.RepCli=' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenSumTxs(Lote: Double);
begin
  // era CO TabLotTxs
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumTxs, Dmod.MyDB, [
  'SELECT SUM(Credito) TaxaVal ',
  'FROM ' + CO_TabLctA + ' lt ',
  'WHERE FatID=' + TXT_VAR_FATID_0303,
  'AND lt.FatNum=' + FormatFloat('0', Lote),
  '']);
end;

procedure TDmLot.ReopenTaxas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTaxas, Dmod.MyDB, [
  'SELECT * ',
  'FROM taxas ',
  'ORDER BY Nome ',
  '']);
end;

procedure TDmLot.ReopenTxs(Lote: Double);
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrTxs, Dmod.MyDB, [
  'SELECT tx.Base, lt.* ',
  'FROM ' + CO TabLotTxs + ' lt ',
  'LEFT JOIN taxas tx ON tx.Codigo=lt.TaxaCod ',
  'WHERE lt.Codigo=' + FormatFloat('0', Lote),
  '']);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrTxs, Dmod.MyDB, [
  'SELECT tx.Base, ',
  'lt.FatNum, lt.FatParcela, lt.FatID_Sub, ',
  'lt.MoraTxa, lt.MoraVal, lt.MoraDia, ',
  'lt.Qtde, lt.Tipific, lt.Credito ',
  'FROM ' + CO_TabLctA + ' lt ',
  'LEFT JOIN taxas tx ON tx.Codigo=lt.FatID_Sub ',
  'WHERE lt.FatID=' + TXT_VAR_FATID_0303,
  'AND lt.FatNum=' + Geral.FF0(Trunc(Lote)),
  '']);
end;

function TDmLot.RiscoSacado(CPF: String): Double;
var
  Valor: Double;
begin

  Valor := 0;
{
  QrSRS.Close;
  QrSRS.Params[0].AsString := CPF;
  QrSRS. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSRS, Dmod.MyDB, [
  'SELECT SUM(li.Credito) Valor ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo = 0 ',
  'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE()) ',
  'AND li.CNPJCPF = "' + CPF + '" ',
  '']);
  //
{
  QrTCD.Close;
  QrTCD.Params[0].AsString := CPF;
  QrTCD. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrTCD, Dmod.MyDB, [
  'SELECT ai.Valor, ai.Cliente, ai.Status, ai.Data1, ai.Data3, ',
  'ai.JurosV, ai.Desconto, ai.ValPago, ai.JurosP ',
  'FROM alinits ai ',
  'WHERE ai.Status<2 ',
  'AND ai.CPF = "' + CPF + '" ',
  '']);
  while not QrTCD.Eof do
  begin
    Valor := Valor + DMod.ObtemValorAtualizado(
    QrTCDCliente.Value, QrTCDStatus.Value, QrTCDData1.Value, Date,
    QrTCDData3.Value, QrTCDValor.Value, QrTCDJurosV.Value,
    QrTCDDesconto.Value, QrTCDValPago.Value, QrTCDJurosP.Value, False);
    //
    QrTCD.Next;
  end;
  //
{
  QrSDO.Close;
  QrSDO.Params[0].AsString := CPF;
  QrSDO. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSDO, Dmod.MyDB, [
  'SELECT lo.Cliente, li.DCompra, li.Credito, li.DDeposito, ',
  'li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg, ',
  'li.Vencimento, li.Data3 ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao ',
  'WHERE li.FatID=' + FormatFloat('0', VAR_FATID_0301),
  'AND lo.Tipo=1 ',
  'AND li.Quitado <> 2 ',
  'AND li.CNPJCPF = "' + CPF + '" ',
  'ORDER BY li.Vencimento ',
  '']);
  while not QrSDO.Eof do
  begin
    Valor := Valor + DMod.ObtemValorAtualizado(
    QrSDOCliente.Value, QrSDOQuitado.Value, QrSDOVencimento.Value, Date,
    QrSDOData3.Value, QrSDOCredito.Value, QrSDOTotalJr.Value, QrSDOTotalDs.Value,
    QrSDOTotalPg.Value, 0, True);
    //
    QrSDO.Next;
  end;
  //
{
  QrSOA.Close;
  QrSOA.Params[0].AsString := CPF;
  QrSOA. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSOA, Dmod.MyDB, [
  'SELECT ocr.* ',
  'FROM ocorreu ocr ',
  'WHERE ocr.Status<2 ',
  'AND ocr.CPF="' + CPF + '" ',
  '']);
  while not QrSOA.Eof do
  begin
    Valor := Valor + DMod.ObtemValorAtualizado(
    QrSOACliente.Value, 1, QrSOADataO.Value, Date, QrSOAData3.Value,
    QrSOAValor.Value, QrSOATaxaV.Value, 0 (*Desco*),
    QrSOAPago.Value, QrSOATaxaP.Value, False);
    //
    QrSOA.Next;
  end;
  //
  Result := Valor + QrSRSValor.Value;
end;

function TDmLot.SQL_CH(var FatParcela: Integer; const Comp, Banco: Integer;
              Agencia: String; Documento: Double; DMais, Dias: Integer; FatNum:
              Double; Praca, Tipific, StatusSPC: Integer; ContaCorrente,
              CNPJCPF, Emitente, DataDoc, Vencimento, DCompra, DDeposito: String;
              Credito: Double; SQLType: TSQLType; Cliente: Integer; TxaCompra,
              TxaAdValorem, VlrCompra, VlrAdValorem, TxaJuros: Double; Data3:
              String; TpAlin, AliIts, ChqPgs: Integer; Descricao: String;
              var TipoCart, Carteira, Controle, Sub: Integer): Boolean;
const
  //Descricao = 'CD';
  Sit = 0;
  FatID = VAR_FATID_0301;
  FatID_Sub = 0;
  CliInt = -11;
var
  Genero: Integer;
  Data: String;
begin
  //Genero = - 3 9 8;
  Result := False;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0301, Genero, tpCred, True) then
    Exit;
  //Result := False;
  //
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      CO_TabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(FatID, CO_TabLctA);
  end;
{
  TxaCompra := FTaxa[0];
  TxaAdValorem := 0;
  VlrCompra := FValr[0];
  VlrAdValorem := 0;
  TxaJuros := FJuro[0];
  Data := DCompra;
  Data3 := DDeposito;
  //
  if SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET Quitado=0, ');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   ');
  end;
  Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
  Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
  Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
  Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Cliente=:P19, Data3=:P20, ');
  Dmod.QrUpd.SQL.Add('Tipific=:P21, StatusSPC=:P22 ');
  Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := Comp;
  Dmod.QrUpd.Params[01].AsInteger := Banco;
  Dmod.QrUpd.Params[02].AsInteger := Agencia;
  Dmod.QrUpd.Params[03].AsString  := Conta;
  Dmod.QrUpd.Params[04].AsInteger := Cheque;
  Dmod.QrUpd.Params[05].AsString  := CPF;
  Dmod.QrUpd.Params[06].AsString  := Emitente;
  Dmod.QrUpd.Params[07].AsFloat   := Valor;
  Dmod.QrUpd.Params[08].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[09].AsFloat   := FTaxa[0];
  Dmod.QrUpd.Params[10].AsFloat   := 0;
  Dmod.QrUpd.Params[11].AsFloat   := FValr[0];
  Dmod.QrUpd.Params[12].AsFloat   := 0;
  Dmod.QrUpd.Params[13].AsInteger := DMais;
  Dmod.QrUpd.Params[14].AsInteger := Dias;
  Dmod.QrUpd.Params[15].AsFloat   := FJuro[0];
  Dmod.QrUpd.Params[16].AsString  := DCompra;
  Dmod.QrUpd.Params[17].AsString  := DDeposito;
  Dmod.QrUpd.Params[18].AsInteger := Praca;
  Dmod.QrUpd.Params[19].AsInteger := Cliente;
  Dmod.QrUpd.Params[20].AsString  := DDeposito; // Quita autom�tico at� voltar
  Dmod.QrUpd.Params[21].AsInteger := Tipific;
  Dmod.QrUpd.Params[22].AsInteger := StatusSPC;
  //
  Dmod.QrUpd.Params[23].AsInteger := Codigo;
  Dmod.QrUpd.Params[24].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
}
  //  deve ser assim porque o cheque compensa autom�tico!
  Data := DDeposito;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_TabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',*)
  'Descricao', (*'SerieNF', 'NotaFiscal',
  'Debito',*) 'Credito', (*'Compensado',
  'SerieCH',*) 'Documento', 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',*)
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', (*'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',
  'MoraVal', 'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal', 'Antigo',
  'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag',*) 'Comp',
  'Praca', (*'Bruto', 'Desco',*)
  'DCompra', 'DDeposito', (*'DescAte',*)
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', (*'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg',*) 'Data3', (*'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito',*) 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, (*'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste',*) 'Tipific', 'StatusSPC'(*,
  'CNAB_Lot'*)], [
  'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,*)
  Descricao, (*SerieNF, NotaFiscal,
  Debito,*) Credito, (*Compensado,
  SerieCH,*) Documento, Sit,
  Vencimento, FatID, FatID_Sub,
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,*)
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, (*Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,
  MoraVal, MultaVal, Protesto,*)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V, Duplicata, Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal, Antigo,
  ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, SubPgto1,
  MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Endossan, Endossad,
  Cancelado, EventosCad, Encerrado,
  ErrCtrl, IndiPag,*) Comp,
  Praca, (*Bruto, Desco,*)
  DCompra, DDeposito, (*DescAte,*)
  TxaCompra, TxaJuros, TxaAdValorem,
  VlrCompra, VlrAdValorem, DMais,
  Dias, (*Devolucao, ProrrVz,
  ProrrDd, Quitado, BcoCobra,
  AgeCobra, TotalJr, TotalDs,
  TotalPg,*) Data3, (*Data4,
  Repassado, Depositado, ValQuit,
  ValDeposito,*) TpAlin, AliIts,
  ChqPgs, (*NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli,
  Teste,*) Tipific, StatusSPC(*,
  CNAB_Lot*)], [
  Data, TipoCart, Carteira, Controle, Sub], True) then
  begin
{
    DefineCompraEmpresas(FatNum, fatParcela);
    //
    FmPrincipal.FControlIts := FatParcela;
    Result := FatParcela;
}   Result := True;
  end else Result := False;
end;

function TDmLot.InsUpd_ChqPg(QrUpd: TmySQLQuery; SQLType: TSQLType; var
  FatParcela, Controle: Integer; const FatID_Sub, Gen_0303, Gen_0304, Gen_0305,
  Gen_0307, Cliente, Ocorreu, FatParcRef: Integer;
  const FatNum, Pago(*, MoraVal, Desco*): Double;
  const DataCH, Dta: TDateTime;
  const ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende: Double;
  CartDep: Integer; Antigo: String = '';
  PermiteQuitarControleZero: Boolean = False): Boolean;
const
  RecalcSdoCart = False;
  Descricao = '';
  CliInt = -11;
  Tipo = 0;
  Carteira = -3;
  Sub = 0;
  Qtde = 0; // ???
  MoraDia = 1.0; // Quantidade de cobran�a da taxa
  Tipific = 0; // por Valor e n�o por %
var
  Data, Vencimento, Compensado, DataDoc: String;
  Credito, Debito: Double;
  FatID, Sit, NewCarteira, NewTipo, NewSit: Integer;
  //
  MoraTxa, MoraVal, MultaVal, Desco: Double;
  CtrlIni, CtrlOri: Integer;
  Documento: Double;
  DataQuit: TDateTime;
  FatGrupo: Integer;
begin
  Result := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqLctB, Dmod.MyDB, [
  'SELECT ali.Codigo, ali.ChequeOrigem, lct.Data, lct.Tipo, lct.Documento, ',
  'lct.Carteira, lct.Controle, lct.Sub, lct.Credito, lct.Debito ',
  'FROM alinits ali, ' + CO_TabLctA + ' lct ',
  'WHERE lct.FatParcela=ali.ChequeOrigem ',
  'AND lct.FatID=' + TXT_VAR_FATID_0301,
  'AND ali.Codigo=' + Geral.FF0(FatParcref),
  '']);
  if not DModFin.AbreLanctosAtrelados(QrPsqLctBControle.Value,
  QrPsqLctBSub.Value, CO_TabLctA, PermiteQuitarControleZero) then
    Exit;
  //
  if not DModFin.AbreLanctoByCtrlSub(QrPsqLctBControle.Value,
  QrPsqLctBSub.Value, CO_TabLctA, PermiteQuitarControleZero) then
    Exit;
  //
  FatGrupo := UMyMod.BPGS1I32(CO_TabLctA, 'FatGrupo', '', '', tsPos, stIns, 0);
  Data := Geral.FDT(Dta, 1);
  Vencimento := Geral.FDT(DataCH, 1);
  Compensado := Data;
  DataDoc    := Data;
  //
  //MoraVal := 0;
  //Desco := 0;
  Sit := 3; // Quita��o autom�tica!
  //
  // VAR_FATID_0303
  if (ValTaxas >= 0.01) or (ValTaxas <= -0.01) then
  begin
    if (ValTaxas >= 0.01) then
    begin
      Credito := ValTaxas;
      Debito := 0;
    end else
    begin
      Credito := 0;
      Debito := -ValTaxas;
    end;

    //FatID := VAR_FATID_0303; // Taxas
    //
    //FatID_Sub := CO_COD_TAXA_PG_CHQ_DEV;
    FatParcela := 0;
    Controle := 0;
    //FatNum := NumeroDoLote;
    MoraTxa := Credito + Debito;
    MoraVal := Credito + Debito;
    //
    if DmLot.SQL_Txa(Dmod.QrUpd, SQLType,
    FatParcela, Controle, (*FatID_Sub*)CO_COD_TAXA_PG_CHQ_DEV, Gen_0303, Cliente, Tipific,
    FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal, Dta, FatParcRef, FatGrupo,
    Antigo) then ;
  end;
  //
  // VAR_FATID_0304
  if (ValMulta >= 0.01) or (ValMulta <= -0.01) then
  begin
    //FatID := VAR_FATID_0304; // Multas
    //FatID_Sub := CO_COD_OCOR_PG_CHQ_DEV; // -99: Multa de cheque devolvido
    //
    FatParcela := 0;
    Controle := 0;
    MoraVal := 0;
    //
    if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
    (*FatID_Sub*)CO_COD_OCOR_PG_CHQ_DEV, Gen_0304, Cliente, Ocorreu, FatParcRef,
    FatNum, ValMulta, MoraVal, Dta, FatGrupo, Antigo) then
    begin
      // N�o existe ocorrencia cadastrada!
      //CalculaPagtoOcorP(Date, QrOcorreuCodigo.Value);
    end;
  end;
  //
  // VAR_FATID_0305
  if (ValJuros >= 0.01) or (ValJuros <= -0.01) then
  begin
    if (ValJuros >= 0.01) then
    begin
      Credito := ValJuros;
      Debito := 0;
    end else
    begin
      Credito := 0;
      Debito := -ValJuros;
    end;

    FatID := VAR_FATID_0305; // Juros
    //
    FatParcela := 0;
    Controle := 0;
    MoraVal := ValJuros;
    Desco := 0;
    //
    SQL_Extra_CH_DU(QrUpd, SQLType, Gen_0305, Debito, Credito, Compensado, Sit,
    Vencimento, FatID, FatID_Sub, FatNum, FatParcela, Cliente, CliInt, MoraVal,
    DataDoc, Desco, FatParcRef, Ocorreu, Data, Tipo, Carteira, Controle, Sub,
    FatGrupo, Antigo);
  end;
  //
  // VAR_FATID_0307
  if (ValDesco >= 0.01) or (ValDesco <= -0.01) then
  begin
    if (ValDesco >= 0.01) then
    begin
      Credito := 0;
      Debito := ValDesco;
    end else
    begin
      Credito := -ValDesco;
      Debito := 0;
    end;

    FatID := VAR_FATID_0307; // Desconto
    //
    FatParcela := 0;
    Controle := 0;
    MoraVal := 0;
    Desco := ValDesco;
    //
    SQL_Extra_CH_DU(QrUpd, SQLType, Gen_0307, Debito, Credito, Compensado, Sit,
    Vencimento, FatID, FatID_Sub, FatNum, FatParcela, Cliente, CliInt, MoraVal,
    DataDoc, Desco, FatParcRef, Ocorreu, Data, Tipo, Carteira, Controle, Sub,
    FatGrupo, Antigo);
  end;
  //
  // Compensa cheque devolvido!
  CtrlIni := 0;
  CtrlOri := QrPsqLctBControle.Value;
  Documento := QrPsqLctBDocumento.Value;
  DataQuit := Dta;
  //Descricao := '';

  Result := DModFin.QuitaOuPgPartDeDocumento(ValPrinc, CtrlIni, CtrlOri,
    Documento, '', DataQuit, Descricao, ValTaxas, ValMulta, ValJuros, ValDesco,
    (*QrCrt*)nil, DModFin.QrUmLct, RecalcSdoCart, CO_TabLctA, True,
    VAR_FATID_0311, 0, FatNum, FatParcRef, FatParcRef, FatGrupo, ValPende,
    CartDep, Antigo, PermiteQuitarControleZero);
  //
end;

function TDmLot.InsUpd_DupPg(QrUpd: TmySQLQuery; SQLType: TSQLType; const
  LctCtrl, LctSub: Integer; var FatParcela, Controle: Integer;
  const FatID_Sub, Gen_0303, Gen_0304, Gen_0306,
  Gen_0308, Cliente, Ocorreu, FatParcRef: Integer;
  const FatNum, Pago(*, MoraVal, Desco*): Double;
  const DataDU, Dta: TDateTime; Duplicata: String;
  const ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende:
  Double; CartDep: Integer; Antigo: String = '';
  PermiteQuitarControleZero: Boolean = False): Boolean;
const
  RecalcSdoCart = False;
  Descricao = '';
  CliInt = -11;
  Tipo = 0;
  Carteira = -3;
  Sub = 0;
  Qtde = 0; // ???
  MoraDia = 1.0; // Quantidade de cobran�a da taxa
  Tipific = 0; // por Valor e n�o por %
var
  Data, Vencimento, Compensado, DataDoc: String;
  Credito, Debito: Double;
  FatID, Sit, NewCarteira, NewTipo, NewSit: Integer;
  //
  MoraTxa, MoraVal, MultaVal, Desco: Double;
  CtrlIni, CtrlOri: Integer;
  Documento: Double;
  DataQuit: TDateTime;
  FatGrupo: Integer;
begin
  Result := False;
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqLctB, Dmod.MyDB, [
  'SELECT ali.Codigo, ali.ChequeOrigem, lct.Data, lct.Tipo, lct.Documento, ',
  'lct.Carteira, lct.Controle, lct.Sub, lct.Credito, lct.Debito ',
  'FROM alinits ali, ' + CO_TabLctA + ' lct ',
  'WHERE lct.FatParcela=ali.ChequeOrigem ',
  'AND lct.FatID=' + TXT_VAR_FATID_0301,
  'AND ali.Codigo=' + Geral.FF0(FatParcref),
  '']);
}
  if not DModFin.AbreLanctosAtrelados(LctCtrl, LctSub, CO_TabLctA,
    PermiteQuitarControleZero) then
    Exit;
  //
  if not DModFin.AbreLanctoByCtrlSub(LctCtrl, LctSub, CO_TabLctA,
    PermiteQuitarControleZero) then
    Exit;
  //
  FatGrupo := UMyMod.BPGS1I32(CO_TabLctA, 'FatGrupo', '', '', tsPos, stIns, 0);
  Data := Geral.FDT(Dta, 1);
  Vencimento := Geral.FDT(DataDU, 1);
  Compensado := Data;
  DataDoc    := Data;
  //
  //MoraVal := 0;
  //Desco := 0;
  Sit := 3; // Quita��o autom�tica!
  //
  // VAR_FATID_0303
{
  if (ValTaxas >= 0.01) or (ValTaxas <= -0.01) then
  begin
    if (ValTaxas >= 0.01) then
    begin
      Credito := ValTaxas;
      Debito := 0;
    end else
    begin
      Credito := 0;
      Debito := -ValTaxas;
    end;

    //FatID := VAR_FATID_0303; // Taxas
    //
    //FatID_Sub := CO_COD_TAXA_PG_CHQ_DEV;
    FatParcela := 0;
    Controle := 0;
    //FatNum := NumeroDoLote;
    MoraTxa := Credito + Debito;
    MoraVal := Credito + Debito;
    //
    / erro: CO_COD_TAXA_PG_CHQ_DEV  ver qual � o FatIDSub
    if DmLot.SQL_Txa(Dmod.QrUpd, SQLType,
    FatParcela, Controle, (*FatID_Sub*)CO_COD_TAXA_PG_CHQ_DEV, Gen_0303, Cliente, Tipific,
    FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal, Dta, FatParcRef, FatGrupo) then ;
    /
  end;
  //
  // VAR_FATID_0304
  if (ValMulta >= 0.01) or (ValMulta <= -0.01) then
  begin
    //FatID := VAR_FATID_0304; // Multas
    //FatID_Sub := CO_COD_OCOR_PG_CHQ_DEV; // -99: Multa de cheque devolvido
    //
    FatParcela := 0;
    Controle := 0;
    MoraVal := 0;
    //
    erro: CO_COD_OCOR_PG_CHQ_DEV -> ver qual � o FatIDSub
    if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
    (*FatID_Sub*)CO_COD_OCOR_PG_CHQ_DEV, Gen_0304, Cliente, Ocorreu, FatParcRef,
    FatNum, ValMulta, MoraVal, Dta, FatGrupo) then
    begin
      // N�o existe ocorrencia cadastrada!
      //CalculaPagtoOcorP(Date, QrOcorreuCodigo.Value);
    end;
  end;
}
  //
  // VAR_FATID_0306
  if (ValJuros >= 0.01) or (ValJuros <= -0.01) then
  begin
    if (ValJuros >= 0.01) then
    begin
      Credito := ValJuros;
      Debito := 0;
    end else
    begin
      Credito := 0;
      Debito := -ValJuros;
    end;

    FatID := VAR_FATID_0306; // Juros
    //
    FatParcela := 0;
    Controle := 0;
    MoraVal := ValJuros;
    Desco := 0;
    //
    SQL_Extra_CH_DU(QrUpd, SQLType, Gen_0306, Debito, Credito, Compensado, Sit,
    Vencimento, FatID, FatID_Sub, FatNum, FatParcela, Cliente, CliInt, MoraVal,
    DataDoc, Desco, FatParcRef, Ocorreu, Data, Tipo, Carteira, Controle, Sub,
    FatGrupo);
  end;
  //
  // VAR_FATID_0308
  if (ValDesco >= 0.01) or (ValDesco <= -0.01) then
  begin
    if (ValDesco >= 0.01) then
    begin
      Credito := 0;
      Debito := ValDesco;
    end else
    begin
      Credito := -ValDesco;
      Debito := 0;
    end;

    FatID := VAR_FATID_0308; // Desconto
    //
    FatParcela := 0;
    Controle := 0;
    MoraVal := 0;
    Desco := ValDesco;
    //
    SQL_Extra_CH_DU(QrUpd, SQLType, Gen_0308, Debito, Credito, Compensado, Sit,
    Vencimento, FatID, FatID_Sub, FatNum, FatParcela, Cliente, CliInt, MoraVal,
    DataDoc, Desco, FatParcRef, Ocorreu, Data, Tipo, Carteira, Controle, Sub,
    FatGrupo);
  end;
  //
  // Compensa cheque devolvido!
  CtrlIni := 0;
  CtrlOri := LctCtrl;
  Documento := 0;
  DataQuit := Dta;
  //Descricao := '';

  Result := DModFin.QuitaOuPgPartDeDocumento(ValPrinc, CtrlIni, CtrlOri,
    Documento, Duplicata, DataQuit, Descricao, ValTaxas, ValMulta, ValJuros, ValDesco,
    (*QrCrt*)nil, DModFin.QrUmLct, RecalcSdoCart, CO_TabLctA, True,
    VAR_FATID_0312, 0, FatNum, FatParcRef, FatParcRef, FatGrupo, ValPende, CartDep);
  //
end;

function TDmLot.SQL_Autom(QrUpd: TmySQLQuery; SQLType: TSQLType;
  var FatParcela, Controle: Integer;
  const FatID, Genero, Cliente: Integer;
  const FatNum, Credito, Debito: Double;
  const Dta: TDateTime): Boolean;
const
  Sit = 3; // Quita��o autom�tica
  CliInt = -11;
  Tipo = 0;
  Sub = 0;
var
  Data, Vencimento, Compensado, DataDoc: String;
  Carteira: Integer;
begin
  if FatID = VAR_FATID_0365 then
    Carteira := CO_CARTEIRA_CHQ_REPASSA
  else
    Carteira := CO_CARTEIRA_VAL_RETIDOS;
  //
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      CO_TabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(FatID, CO_TabLctA);
  end;
  //
  Data := Geral.FDT(Dta, 1);
  Vencimento := Data;
  Compensado := Data;
  DataDoc    := Data;
  //
  if UMyMod.SQLInsUpd(QrUpd, SQLType, CO_TabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',
  'Descricao', 'SerieNF', 'NotaFiscal',*)
  'Debito', 'Credito', 'Compensado',
  (*'SerieCH', 'Documento',*) 'Sit',
  'Vencimento', 'FatID', (*'FatID_Sub',*)
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', 'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',
  'MoraVal', 'MultaVal', 'Protesto',*)
  'DataDoc'(*, 'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal', 'Antigo',
  'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag', 'Comp',
  'Praca', 'Bruto', 'Desco',
  'DCompra', 'DDeposito', 'DescAte',
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', 'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg', 'Data3', 'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste', 'Tipific', 'StatusSPC',
  'CNAB_Lot', 'MoraTxa'*)], [
  'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,
  Descricao, SerieNF, NotaFiscal,*)
  Debito, Credito, Compensado,
  (*SerieCH, Documento,*) Sit,
  Vencimento, FatID, (*FatID_Sub,*)
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,
  MoraVal, MultaVal, Protesto, *)
  DataDoc(*, CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V, Duplicata, Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal, Antigo,
  ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, SubPgto1,
  MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Endossan, Endossad,
  Cancelado, EventosCad, Encerrado,
  ErrCtrl, IndiPag, Comp,
  Praca, Bruto, Desco,
  DCompra, DDeposito, DescAte,
  TxaCompra, TxaJuros, TxaAdValorem,
  VlrCompra, VlrAdValorem, DMais,
  Dias, Devolucao, ProrrVz,
  ProrrDd, Quitado, BcoCobra,
  AgeCobra, TotalJr, TotalDs,
  TotalPg, Data3, Data4,
  Repassado, Depositado, ValQuit,
  ValDeposito, TpAlin, AliIts,
  Alin#Pgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli,
  Teste, Tipific, StatusSPC,
  CNAB_Lot, MoraTxa*)], [
  Data, Tipo, Carteira, Controle, Sub], True) then
  begin
    Result := True;
    //
  end else Result := False;
end;

{
function TDmLot.SQL_DupPg(QrUpd: TmySQLQuery; SQLType: TSQLType; var FatParcela,
  Controle: Integer; const FatID_Sub, Genero, Cliente, Ocorreu,
  FatParcRef: Integer; const FatNum, Pago, MoraVal, Desco: Double; const DataDU,
  Dta: TDateTime): Boolean;
const
  Sit = 3; // Quita��o autom�tica
  FatID = VAR_FATID_0312;
  CliInt = -11;
  Tipo = 0;
  Carteira = -3;
  Sub = 0;
var
  Data, Vencimento, Compensado, DataDoc: String;
  Credito, Debito: Double;
begin
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      CO_TabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(FatID, CO_TabLctA);
  end;
  //
  Data := Geral.FDT(Dta, 1);
  Vencimento := Geral.FDT(DataDU, 1);
  Compensado := Data;
  DataDoc    := Data;
  //
  if Pago > 0 then
  begin
    Credito := Pago;
    Debito := 0;
  end else
  begin
    Debito := -Pago;
    Credito := 0;
  end;
  if UMyMod.SQLInsUpd(QrUpd, SQLType, CO_TabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',
  'Descricao', 'SerieNF', 'NotaFiscal',*)
  'Debito', 'Credito', 'Compensado',
  (*'SerieCH', 'Documento',*) 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', 'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',*)
  'MoraVal', (*'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal', 'Antigo',
  'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag', 'Comp',
  'Praca', 'Bruto',*) 'Desco',
  (*'DCompra', 'DDeposito', 'DescAte',
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', 'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg', 'Data3', 'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste', 'Tipific', 'StatusSPC',
  'CNAB_Lot', 'MoraTxa'*) 'FatParcRef',
  'Ocorreu'], [
  'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,
  Descricao, SerieNF, NotaFiscal,*)
  Debito, Credito, Compensado,
  (*SerieCH, Documento,*) Sit,
  Vencimento, FatID, FatID_Sub, 
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,*)
  MoraVal, (*MultaVal, Protesto, *)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P, 
  ICMS_V, Duplicata, Depto, 
  DescoPor, DescoVal, DescoControle, 
  Unidade, NFVal, Antigo, 
  ExcelGru, Doc2, CNAB_Sit, 
  TipoCH, Reparcel, Atrelado, 
  PagMul, PagJur, SubPgto1, 
  MultiPgto, Protocolo, CtrlQuitPg, 
  Endossas, Endossan, Endossad, 
  Cancelado, EventosCad, Encerrado, 
  ErrCtrl, IndiPag, Comp, 
  Praca, Bruto,*) Desco,
  (*DCompra, DDeposito, DescAte, 
  TxaCompra, TxaJuros, TxaAdValorem, 
  VlrCompra, VlrAdValorem, DMais, 
  Dias, Devolucao, ProrrVz, 
  ProrrDd, Quitado, BcoCobra, 
  AgeCobra, TotalJr, TotalDs, 
  TotalPg, Data3, Data4, 
  Repassado, Depositado, ValQuit, 
  ValDeposito, TpAlin, AliIts, 
  Alin#Pgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli,
  Teste, Tipific, StatusSPC,
  CNAB_Lot, MoraTxa*) FatParcRef,
  Ocorreu], [
  Data, Tipo, Carteira, Controle, Sub], True) then
  begin
    Result := True;
    //
  end else Result := False;
end;
}

function TDmLot.SQL_Txa(QrUpd: TmySQLQuery; SQLType: TSQLType;
  var FatParcela, Controle: Integer;
  const FatID_Sub, Genero, Cliente, Tipific: Integer;
  const FatNum, Qtde, Credito, MoraTxa, MoraDia, MoraVal: Double;
  const Emissao: TDateTime; FatParcRef, FatGrupo: Integer;
  Antigo: String = ''): Boolean;
const
  Sit = 3; // Quita��o autom�tica
  FatID = VAR_FATID_0303;
  CliInt = -11;
  Tipo = 0;
  Carteira = -3;
  Sub = 0;
var
  Data, Vencimento, Compensado, DataDoc: String;
begin
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      CO_TabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(FatID, CO_TabLctA);
  end;
  //
  Data := Geral.FDT(Emissao, 1);
  Vencimento := Data;
  Compensado := Data;
  DataDoc    := Data;
  //
  if UMyMod.SQLInsUpd(QrUpd, SQLType, CO_TabLctA, False, [
  'Data', 'Tipo', 'Carteira',
  (*'Autorizacao',*) 'Genero', 'Qtde',
  (*'Descricao', 'SerieNF', 'NotaFiscal',
  'Debito',*) 'Credito', 'Compensado',
  (*'SerieCH', 'Documento',*) 'Sit',
  'Vencimento', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', 'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI',*) 'MoraDia', (*'Multa',*)
  'MoraVal', (*'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal',*) 'Antigo',
  (*'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag', 'Comp',
  'Praca', 'Bruto', 'Desco',
  'DCompra', 'DDeposito', 'DescAte',
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', 'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg', 'Data3', 'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste',*) 'Tipific', (*'StatusSPC',
  'CNAB_Lot',*) 'MoraTxa',
  'FatParcRef', 'FatGrupo'], [
  'Controle', 'Sub', 'FatID',
  'FatID_Sub', 'FatNum', 'FatParcela'], [
  Data, Tipo, Carteira,
  (*Autorizacao,*) Genero, Qtde,
  (*Descricao, SerieNF, NotaFiscal,
  Debito,*) Credito, Compensado,
  (*SerieCH, Documento,*) Sit,
  Vencimento, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI,*) MoraDia, (*Multa,*)
  MoraVal, (*MultaVal, Protesto, *)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V, Duplicata, Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal,*) Antigo,
  (*ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, SubPgto1,
  MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Endossan, Endossad,
  Cancelado, EventosCad, Encerrado,
  ErrCtrl, IndiPag, Comp,
  Praca, Bruto, Desco,
  DCompra, DDeposito, DescAte,
  TxaCompra, TxaJuros, TxaAdValorem,
  VlrCompra, VlrAdValorem, DMais,
  Dias, Devolucao, ProrrVz,
  ProrrDd, Quitado, BcoCobra,
  AgeCobra, TotalJr, TotalDs,
  TotalPg, Data3, Data4,
  Repassado, Depositado, ValQuit,
  ValDeposito, TpAlin, AliIts,
  Alin#Pgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli,
  Teste,*) Tipific, (*StatusSPC,
  CNAB_Lot,*) MoraTxa,
  FatParcRef, FatGrupo], [
  Controle, Sub, FatID,
  FatID_Sub, FatNum, FatParcela], True) then
  begin
    Result := True;
    //
  end else Result := False;
end;

function TDmLot.SQL_OcorP(QrUpd: TmySQLQuery; SQLType: TSQLType; var FatParcela,
  Controle: Integer; const FatID_Sub, Genero, Cliente, Ocorreu,
  FatParcRef: Integer; const FatNum, Valor, MoraVal: Double;
  const Dta: TDateTime; FatGrupo: Integer;
  Antigo: String = ''): Boolean;
const
  Sit = 3; // Quita��o autom�tica
  FatID = VAR_FATID_0304;
  CliInt = -11;
  Tipo = 0;
  Carteira = -3;
  Sub = 0;
var
  Data, Vencimento, Compensado, DataDoc: String;
  Credito, Debito: Double;
begin
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      CO_TabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(FatID, CO_TabLctA);
  end;
  //
  Data := Geral.FDT(Dta, 1);
  Vencimento := Data;
  Compensado := Data;
  DataDoc    := Data;
  //
  if Valor > 0 then
  begin
    Credito := Valor;
    Debito := 0;
  end else
  begin
    Debito := -Valor;
    Credito := 0;
  end;
  if UMyMod.SQLInsUpd(QrUpd, SQLType, CO_TabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',
  'Descricao', 'SerieNF', 'NotaFiscal',*)
  'Debito', 'Credito', 'Compensado',
  (*'SerieCH', 'Documento',*) 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', 'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',*)
  'MoraVal', (*'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal',*) 'Antigo',
  (*'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag', 'Comp',
  'Praca', 'Bruto', 'Desco',
  'DCompra', 'DDeposito', 'DescAte',
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', 'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg', 'Data3', 'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste', 'Tipific', 'StatusSPC',
  'CNAB_Lot', 'MoraTxa'*) 'FatParcRef',
  'Ocorreu', 'FatGrupo'], [
  'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,
  Descricao, SerieNF, NotaFiscal,*)
  Debito, Credito, Compensado,
  (*SerieCH, Documento,*) Sit,
  Vencimento, FatID, FatID_Sub, 
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,*)
  MoraVal, (*MultaVal, Protesto, *)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P, 
  ICMS_V, Duplicata, Depto, 
  DescoPor, DescoVal, DescoControle, 
  Unidade, NFVal,*) Antigo,
  (*ExcelGru, Doc2, CNAB_Sit, 
  TipoCH, Reparcel, Atrelado, 
  PagMul, PagJur, SubPgto1, 
  MultiPgto, Protocolo, CtrlQuitPg, 
  Endossas, Endossan, Endossad, 
  Cancelado, EventosCad, Encerrado, 
  ErrCtrl, IndiPag, Comp, 
  Praca, Bruto, Desco, 
  DCompra, DDeposito, DescAte, 
  TxaCompra, TxaJuros, TxaAdValorem, 
  VlrCompra, VlrAdValorem, DMais, 
  Dias, Devolucao, ProrrVz, 
  ProrrDd, Quitado, BcoCobra, 
  AgeCobra, TotalJr, TotalDs, 
  TotalPg, Data3, Data4, 
  Repassado, Depositado, ValQuit, 
  ValDeposito, TpAlin, AliIts, 
  Alin#Pgs, NaoDeposita, ReforcoCxa, 
  CartDep, Cobranca, RepCli, 
  Teste, Tipific, StatusSPC,
  CNAB_Lot, MoraTxa*) FatParcRef,
  Ocorreu, FatGrupo], [
  Data, Tipo, Carteira, Controle, Sub], True) then
  begin
    Result := True;
    //
  end else Result := False;
end;

//Cheque = 2402

end.
