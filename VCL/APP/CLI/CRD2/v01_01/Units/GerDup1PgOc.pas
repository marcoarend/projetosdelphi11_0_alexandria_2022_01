unit GerDup1PgOc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmGerDup1PgOc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    PainelOcorPg: TPanel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label85: TLabel;
    Label84: TLabel;
    TPDataBase6: TDateTimePicker;
    EdValorBase6: TdmkEdit;
    EdJurosBase6: TdmkEdit;
    EdJurosPeriodo6: TdmkEdit;
    TPPagto6: TDateTimePicker;
    EdJuros6: TdmkEdit;
    EdAPagar6: TdmkEdit;
    EdPago6: TdmkEdit;
    QrLocOc: TmySQLQuery;
    QrLocOcData: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdValorBase6Change(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    //procedure ConfirmaOcorPg();
  public
    { Public declarations }
    procedure ConfiguraPgOc();
    procedure CalculaJurosOcor();
    procedure CalculaAPagarOcor();
  end;

  var
  FmGerDup1PgOc: TFmGerDup1PgOc;

implementation

uses UnMyObjects, Module, GerDup1Main, UMySQLModule, ModuleLot, MyListas;

{$R *.DFM}

procedure TFmGerDup1PgOc.BtOKClick(Sender: TObject);
var
  FatParcela, Controle, FatID_Sub, Genero, Cliente, Ocorreu,
  FatParcRef: Integer;
  FatNum, Valor, MoraVal: Double;
  Dta: TDateTime;
begin
//  ConfirmaOcorP();
{procedure TFmGerDup1PgOc.ConfirmaOcorP();
var
  OcorP: Integer;
begin
  OcorP := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Ocor rPG', 'Ocor rPG', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ocor rpg SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
  Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto6.Date);
  Dmod.QrUpd.Params[01].AsFloat   := EdJuros6.ValueVariant;
  Dmod.QrUpd.Params[02].AsFloat   := EdPago6.ValueVariant;
  Dmod.QrUpd.Params[03].AsInteger := 0;
  //
  Dmod.QrUpd.Params[04].AsInteger := FmGerDup1Main.QrOcorreuCodigo.Value;
  Dmod.QrUpd.Params[05].AsInteger := Ocor rPg;
  Dmod.QrUpd.ExecSQL;
  //
  FmGerDup1Main.CalculaPagtoOcorP(TPPagto6.Date, FmGerDup1Main.QrOcorreuCodigo.Value);
  ///
  FmGerDup1Main.FOcorP := Ocor rPg;
  FmGerDup1Main.ReopenOcorreu(FmGerDup1Main.QrOcorreuCodigo.Value);
  //
  Close;
end;
}
  FatID_Sub := FmGerDup1Main.QrOcorreuOcorrencia.Value;
  Genero := FmGerDup1Main.QrOcorreuPlaGen.Value;
  if MyObjects.FIC(Genero=0, nil, 'A ocorr�ncia ' + Geral.FF0(
  FmGerDup1Main.QrOcorreuOcorrencia.Value) +
  ' n�o tem cadastrada sua conta do plano de de contas!') then
    Exit;
  Cliente := FmGerDup1Main.QrOcorreuCliente.Value;
  Ocorreu := FmGerDup1Main.QrOcorreuCodigo.Value;
  FatParcRef := FmGerDup1Main.QrOcorreuLOIS.Value;
  //
  FatNum := 0; // n�o tem! � na duplicata!
  FatParcela := 0;
  Controle := 0;
  Valor := Geral.DMV(EdPago6.Text);
  MoraVal := Geral.DMV(EdJuros6.Text);
  Dta := TPPagto6.Date;
  //
  if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
  FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef,
  FatNum, Valor, MoraVal, Dta, 0) then
  begin
    FmGerDup1Main.CalculaPagtoOcorP(TPPagto6.Date, FmGerDup1Main.QrOcorreuCodigo.Value);
    ///
    FmGerDup1Main.FOcorP := FatParcela;
    FmGerDup1Main.ReopenOcorreu(FmGerDup1Main.QrOcorreuCodigo.Value);
    //
    Close;
  end;
end;

procedure TFmGerDup1PgOc.BtSaidaClick(Sender: TObject);
begin
  FmGerDup1Main.ForcaOcorBank := 0;
  Close;
end;

procedure TFmGerDup1PgOc.CalculaAPagarOcor();
var
  Base, Juro: Double;
begin
  Base := EdValorBase6.ValueVariant;
  Juro := EdJuros6.ValueVariant;
  //
  EdAPagar6.ValueVariant := Base + Juro;
end;

procedure TFmGerDup1PgOc.CalculaJurosOcor();
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := EdJurosBase6.ValueVariant;
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end;
  Valor := EdValorBase6.ValueVariant;
  EdJurosPeriodo6.ValueVariant := Juros;
  EdJuros6.ValueVariant := Juros * Valor / 100;
end;

procedure TFmGerDup1PgOc.ConfiguraPgOc();
var
  ValorBase: Double;
begin
{
  QrLocOc.Close;
  QrLocOc.Params[0].AsInteger := FmGerDup1Main.QrOcorreuCodigo.Value;
  UMyMod.AbreQuery(QrLocOc);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocOc, Dmod.MyDB, [
  'SELECT Data ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Data=( ',
  '  SELECT Max(Data) ',
  '  FROM ' + CO_TabLctA,
  '  WHERE FatID=' + TXT_VAR_FATID_0304,
  '  AND Ocorreu=' + Geral.FF0(FmGerDup1Main.QrOcorreuCodigo.Value) + ') ',
  'ORDER BY FatNum DESC ',
  '']);

  TPPagto6.MinDate := 0;
  if QrLocOc.RecordCount > 0 then
  begin
    TPPagto6.Date     := Int(QrLocOcData.Value);
    TPDataBase6.Date  := Int(QrLocOcData.Value);
  end else begin
    TPPagto6.Date    := Int(FmGerDup1Main.QrOcorreuDataO.Value);
    TPDataBase6.Date := Int(FmGerDup1Main.QrOcorreuDataO.Value);
  end;
  EdJurosBase6.ValueVariant :=
    Dmod.ObtemTaxaDeCompraCliente(FmGerDup1Main.QrPesqCliente.Value);
  ValorBase := FmGerDup1Main.QrOcorreuValor.Value +
               FmGerDup1Main.QrOcorreuTaxaV.Value -
               FmGerDup1Main.QrOcorreuPago.Value;
  EdValorBase6.ValueVariant := ValorBase;
  TPPagto6.MinDate := TPPagto6.Date;
  if Date > TPPagto6.MinDate then TPPagto6.Date := Int(Date);
end;

procedure TFmGerDup1PgOc.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.ValueVariant := EdAPagar6.ValueVariant;
end;

procedure TFmGerDup1PgOc.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := EdValorBase6.ValueVariant;
  Juros := EdJuros6.ValueVariant;
  EdAPagar6.ValueVariant := VBase + Juros;
end;

procedure TFmGerDup1PgOc.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor();
  CalculaAPagarOcor();
end;

procedure TFmGerDup1PgOc.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor();
end;

procedure TFmGerDup1PgOc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmGerDup1PgOc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerDup1PgOc.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor();
end;

end.
