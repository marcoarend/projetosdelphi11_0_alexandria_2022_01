unit SacadosEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, dmkGeral, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmSacadosEdit = class(TForm)
    Panel2: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    EdCNPJ: TdmkEdit;
    EdSacado: TdmkEdit;
    EdRua: TdmkEdit;
    EdNumero: TdmkEdit;
    EdCompl: TdmkEdit;
    EdBairro: TdmkEdit;
    EdCidade: TdmkEdit;
    EdCEP: TdmkEdit;
    CBUF: TComboBox;
    EdTel1: TdmkEdit;
    Label1: TLabel;
    EdRisco: TdmkEdit;
    Label5: TLabel;
    EdIE: TdmkEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Label7: TLabel;
    EdEmail: TdmkEdit;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBUFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FLaTipo: String;
    FIncluiu: Boolean;
    FOrigem: String;
  end;

var
  FmSacadosEdit: TFmSacadosEdit;

implementation

uses UnMyObjects, Principal, UnMLAGeral, UnInternalConsts, Sacados, Module;

{$R *.DFM}

procedure TFmSacadosEdit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then Close;
end;

procedure TFmSacadosEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSacadosEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSacadosEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSacadosEdit.BtConfirmaClick(Sender: TObject);
var
  Erros: Integer;
  Numero: Integer;
  IE, CPF, Sacado, Rua, CEP, Compl, Bairro, Cidade, UF, Email: String;
  Risco: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    Erros   := 0;
    Risco   := EdRisco.ValueVariant;
    CPF     := Geral.SoNumero_TT(EdCNPJ.Text);
    Sacado  := EdSacado.Text;
    Rua     := EdRua.Text;
    Numero  := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
    Compl   := EdCompl.Text;
    Bairro  := EdBairro.Text;
    Cidade  := EdCidade.Text;
    CEP     := Geral.SoNumero_TT(EdCEP.Text);
    UF      := CBUF.Text;
    IE      := EdIE.Text;
    Email   := EdEmail.Text;
    //
    if CPF = '' then
      Erros := Erros + MLAGeral.MostraErroControle('Informe o CNPJ!', EdCNPJ);
    if Sacado = '' then
      Erros := Erros + MLAGeral.MostraErroControle('Informe o Sacado!', EdSacado);
    if Rua = '' then
      Erros := Erros + MLAGeral.MostraErroControle('Informe o logradouro!', EdRua);
    if Cidade = '' then
      Erros := Erros + MLAGeral.MostraErroControle('Informe a Cidade!', EdCidade);
    if CEP = '' then
      Erros := Erros + MLAGeral.MostraErroControle('Informe o CEP!', EdCEP);
    if Erros > 0 then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    Geral.Valida_IE(EdIE.Text, MLAGeral.GetCodigoUF_da_SiglaUF(CBUF.Text), '??');
    FmPrincipal.AtualizaSacado(CPF, Sacado, Rua, Numero, Compl, Bairro, Cidade, UF, CEP,
    MlaGeral.SoNumeroESinal_TT(EdTel1.Text), IE, Email, Risco);
    //
    if LowerCase(FOrigem) = 'fmsacados' then
      FmSacados.ReopenSacados(Geral.SoNumero_TT(EdCNPJ.Text));
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  Close;
end;

procedure TFmSacadosEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdRisco.Text := Geral.FFT(Dmod.QrControleDURisco.Value, 2, siPositivo);
end;

procedure TFmSacadosEdit.CBUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    if CBUF.DroppedDown = False then
      SendMessage(FmSacadosEdit.Handle, WM_NEXTDLGCTL, 0, 0);
end;

end.
