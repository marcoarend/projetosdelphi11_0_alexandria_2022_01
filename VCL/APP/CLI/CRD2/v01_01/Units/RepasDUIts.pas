unit RepasDUIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmRepasDUIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    BtConfirma2: TBitBtn;
    BtExclui2: TBitBtn;
    PainelItens: TPanel;
    GradeCHs: TDBGrid;
    Panel3: TPanel;
    Label11: TLabel;
    Label13: TLabel;
    Label8: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdDuplicata: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdTaxa: TdmkEdit;
    CkInap: TCheckBox;
    EdBordero: TdmkEdit;
    Memo1: TMemo;
    QrCheques: TmySQLQuery;
    QrChequesComp: TIntegerField;
    QrChequesBanco: TIntegerField;
    QrChequesAgencia: TIntegerField;
    QrChequesEmitente: TWideStringField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesTxaCompra: TFloatField;
    QrChequesTxaJuros: TFloatField;
    QrChequesTxaAdValorem: TFloatField;
    QrChequesVlrCompra: TFloatField;
    QrChequesVlrAdValorem: TFloatField;
    QrChequesDMais: TIntegerField;
    QrChequesDias: TIntegerField;
    QrChequesDuplicata: TWideStringField;
    QrChequesDevolucao: TIntegerField;
    QrChequesDesco: TFloatField;
    QrChequesQuitado: TIntegerField;
    QrChequesBruto: TFloatField;
    QrChequesCPF_TXT: TWideStringField;
    QrChequesLote: TSmallintField;
    DsCheques: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    QrChequesCredito: TFloatField;
    QrChequesFatNum: TFloatField;
    QrChequesFatParcela: TIntegerField;
    QrChequesVencimento: TDateField;
    QrChequesContaCorrente: TWideStringField;
    QrChequesCNPJCPF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure EdDuplicataExit(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdBorderoExit(Sender: TObject);
    procedure EdTaxaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CkInapClick(Sender: TObject);
    procedure QrChequesAfterClose(DataSet: TDataSet);
    procedure QrChequesAfterOpen(DataSet: TDataSet);
    procedure QrChequesCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure RepassaCheque();
    procedure ReopenDuplicatas(FatParcela: Integer);
  public
    { Public declarations }
  end;

  var
  FmRepasDUIts: TFmRepasDUIts;

implementation

uses UnMyObjects, Module, RepasDUCad, UMySQLModule, MyListas;

{$R *.DFM}

procedure TFmRepasDUIts.BtConfirma2Click(Sender: TObject);
var
  n, i, Codigo: integer;
begin
  n := GradeCHs.SelectedRows.Count;
  if n > 0 then
  begin
    if Application.MessageBox(PChar('Confirma o repasse da(s) '+IntToStr(n)+
    ' duplicata(s) selecionada(s)?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      with GradeCHs.DataSource.DataSet do
      for i:= 0 to GradeCHs.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(GradeCHs.SelectedRows.Items[i]));
        GotoBookmark(GradeCHs.SelectedRows.Items[i]);
        RepassaCheque();
      end;
    end;
  end else begin
    if Application.MessageBox('Confirma o repasse da duplicata selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then RepassaCheque;
  end;
  Codigo := FmRepasDUCad.QrRepasCodigo.Value;
  FmRepasDUCad.CalculaLote(Codigo);
  FmRepasDUCad.LocCod(Codigo, Codigo);
  EdDuplicata.Text;
  EdDuplicata.Text := '';
  ReopenDuplicatas(0);
  if ImgTipo.SQLType = stUpd then
    Close;
end;

procedure TFmRepasDUIts.BtExclui2Click(Sender: TObject);
begin
  FmRepasDUCad.Retirachequedoloteatual1Click(Self);
end;

procedure TFmRepasDUIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRepasDUIts.CkInapClick(Sender: TObject);
begin
  ReopenDuplicatas(QrChequesFatParcela.Value);
end;

procedure TFmRepasDUIts.EdBorderoExit(Sender: TObject);
begin
  ReopenDuplicatas(QrChequesFatParcela.Value);
end;

procedure TFmRepasDUIts.EdClienteChange(Sender: TObject);
begin
  EdBordero.Enabled := EdCliente.ValueVariant <> 0;
  ReopenDuplicatas(QrChequesFatParcela.Value);
end;

procedure TFmRepasDUIts.EdCNPJExit(Sender: TObject);
begin
  ReopenDuplicatas(0);
end;

procedure TFmRepasDUIts.EdDuplicataExit(Sender: TObject);
begin
  ReopenDuplicatas(0);
end;

procedure TFmRepasDUIts.EdTaxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F12 then
  begin
    if BtConfirma2.Enabled then BtConfirma2Click(Self) else
      Application.MessageBox('Nenhuma duplicata foi localizada!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      EdDuplicata.Text;
      EdDuplicata.Text := '';
  end;
end;

procedure TFmRepasDUIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  BtExclui2.Enabled := FmRepasDUCad.QrRepasIts.RecordCount > 0;
end;

procedure TFmRepasDUIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
end;

procedure TFmRepasDUIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRepasDUIts.QrChequesAfterClose(DataSet: TDataSet);
begin
  BtConfirma2.Enabled := False;
end;

procedure TFmRepasDUIts.QrChequesAfterOpen(DataSet: TDataSet);
begin
  if QrCheques.RecordCount > 0 then BtConfirma2.Enabled := True else
  BtConfirma2.Enabled := False;
end;

procedure TFmRepasDUIts.QrChequesCalcFields(DataSet: TDataSet);
begin
  QrChequesCPF_TXT.Value := Geral.FormataCNPJ_TT(QrChequesCNPJCPF.Value);
end;

procedure TFmRepasDUIts.ReopenDuplicatas(FatParcela: Integer);
var
  Cliente, Bordero: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  if EdBordero.Enabled then
    Bordero := EdBordero.ValueVariant
  else
    Bordero := 0;
{
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT lo.Lote, li.*');
  QrCheques.SQL.Add('FROM lot esits li');
  QrCheques.SQL.Add('LEFT JOIN lot es lo ON lo.Codigo=li.Codigo');
  QrCheques.SQL.Add('WHERE lo.Tipo=1');
  QrCheques.SQL.Add('AND li.DDeposito > "'+
    FormatDateTime(VAR_FORMATDATE, FmRepasDUCad.QrRepasData.Value)+'"');
  QrCheques.SQL.Add('AND li.Quitado = 0');
  QrCheques.SQL.Add('AND li.Repassado = 0');
  //
  Cliente := EdCliente.ValueVariant;
  if Cliente > 0 then
    QrCheques.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  if Trim(EdDuplicata.Text) <> '' then
    QrCheques.SQL.Add('AND li.Duplicata='+EdDuplicata.Text);
  //
  if Trim(EdCNPJ.Text) <> '' then
    QrCheques.SQL.Add('AND li.CPF='+Geral.SoNumero_TT(EdCNPJ.Text));
  //
  Cliente := EdCliente.ValueVariant;
  if Cliente <> 0 then
    QrCheques.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  if EdBordero.Enabled then
  begin
    Bordero := EdBordero.ValueVariant;
    if Bordero <> 0 then
      QrCheques.SQL.Add('AND lo.Lote='+IntToStr(Bordero));
  end;
  //
  QrCheques. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrCheques, Dmod.MyDB, [
  'SELECT lo.Lote, li.* ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND lo.Tipo=1 ',
  'AND li.DDeposito > "' + Geral.FDT(FmRepasDUCad.QrRepasData.Value, 1)+'" ',
  'AND li.Quitado = 0 ',
  'AND li.Repassado = 0 ',
  Geral.ATS_if(Cliente > 0, [
  'AND lo.Cliente=' + Geral.FF0(Cliente)]),
  Geral.ATS_if(Trim(EdDuplicata.Text) <> '', [
  'AND li.Duplicata="' + EdDuplicata.Text + '"']),
  Geral.ATS_if(Trim(EdCNPJ.Text) <> '', [
  'AND li.CNPJCPF="' + Geral.SoNumero_TT(EdCNPJ.Text) + '"']),
  Geral.ATS_if(Bordero <> 0, [
  'AND lo.Lote=' + Geral.FF0(Bordero)]),
  '']);
  //
  if FatParcela > 0 then
    QrCheques.Locate('FatParcela', FatParcela, []);
end;

procedure TFmRepasDUIts.RepassaCheque();
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, Taxa, JurosP, JurosV: Double;
begin
  Codigo := FmRepasDUCad.QrRepasCodigo.Value;
  Origem := QrChequesFatParcela.Value;
  Taxa   := Geral.DMV(EdTaxa.Text);
  Dias   := Trunc(int(QrChequesDDeposito.Value) - int(FmRepasDUCad.QrRepasData.Value));
  JurosP := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Valor  := QrChequesCredito.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO repasits SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'RepasIts', 'RepasIts', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE repasits SET ');
    Controle := FmRepasDUCad.QrRepasItsControle.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Repassado=1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False,[
  'Repassado'], ['FatID', 'FatParcela'], [
  1], [VAR_FATID_0301, Origem], True);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  FmRepasDUCad.FRepasIts := Controle;
end;

end.
