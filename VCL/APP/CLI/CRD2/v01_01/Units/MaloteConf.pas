unit MaloteConf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TFmMaloteConf = class(TForm)
    PainelDados: TPanel;
    DsTransportadoras: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    PainelConfig: TPanel;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGOQue: TRadioGroup;
    PainelConfere: TPanel;
    DBG2: TDBGrid;
    QrLot: TmySQLQuery;
    DsLot: TDataSource;
    QrLotNOMECLI: TWideStringField;
    QrLotCodigo: TIntegerField;
    QrLotCliente: TIntegerField;
    QrLotLote: TSmallintField;
    QrLotData: TDateField;
    QrLotTotal: TFloatField;
    RGStatus: TRadioGroup;
    GroupBox1: TGroupBox;
    CkPeriodo: TCheckBox;
    Label34: TLabel;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    Label2: TLabel;
    QrLoc: TmySQLQuery;
    Panel3: TPanel;
    Label36: TLabel;
    EdBanda: TdmkEdit;
    Label7: TLabel;
    EdEmitente: TdmkEdit;
    Label3: TLabel;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdConta: TdmkEdit;
    Label32: TLabel;
    EdCheque: TdmkEdit;
    EdCPF: TdmkEdit;
    Label6: TLabel;
    EdValor: TdmkEdit;
    Label11: TLabel;
    DBG1: TDBGrid;
    QrLotIts: TmySQLQuery;
    DsLotIts: TDataSource;
    QrLotItsCONF_V: TIntegerField;
    QrLotItsDADOS: TWideStringField;
    QrLotItens: TIntegerField;
    Label8: TLabel;
    QrSumM: TmySQLQuery;
    DsSumM: TDataSource;
    QrSumMVALOR: TFloatField;
    QrSumMCHEQUES: TFloatField;
    DBEdItens: TDBEdit;
    DBEdValor: TDBEdit;
    Timer1: TTimer;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCtrl: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfere: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtLimpar: TBitBtn;
    GBConf: TGroupBox;
    Panel2: TPanel;
    Panel5: TPanel;
    BtDesiste: TBitBtn;
    BtRatifica: TBitBtn;
    BtDesfaz: TBitBtn;
    QrLotItsVALORMALOTE: TFloatField;
    QrLotItsLOCCHEQUE: TIntegerField;
    QrLotItsControle: TIntegerField;
    QrLotItsBanco: TIntegerField;
    QrLotItsContaCorrente: TWideStringField;
    QrLotItsDocumento: TFloatField;
    QrLotItsFatNum: TFloatField;
    QrLotItsCNPJCPF: TWideStringField;
    QrLotItsEmitente: TWideStringField;
    QrLotItsCredito: TFloatField;
    QrLotItsFatParcela: TIntegerField;
    QrLocFatNum: TFloatField;
    QrLocFatParcela: TIntegerField;
    QrLocCNPJCPF: TWideStringField;
    QrLocEmitente: TWideStringField;
    QrLocCredito: TFloatField;
    QrLotItsAgencia: TIntegerField;
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure RGStatusClick(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure RGOQueClick(Sender: TObject);
    procedure QrLotAfterScroll(DataSet: TDataSet);
    procedure DBG1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConfereClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure QrLotItsCalcFields(DataSet: TDataSet);
    procedure BtRatificaClick(Sender: TObject);
    procedure BtDesfazClick(Sender: TObject);
    procedure DBEdItensChange(Sender: TObject);
    procedure DBEdValorChange(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    FLotCab, FLotIts: Integer;
    procedure ReopenLot();
    procedure ReopenLotIts();
    procedure CliqueItem(Tipo: Integer);
    procedure InsereMaloteConf(Localizou: Integer; Valor, Real, Codigo: Double;
              Controle: Integer; Avisa: Boolean);
    procedure RatificaLote(Status: Integer);
    procedure LocalizaItem();
    procedure VoltaABanda;
  public
    { Public declarations }
  end;

var
  FmMaloteConf: TFmMaloteConf;

implementation

uses UnMyObjects, Module, Entidades, Principal, MyVCLSkin, MyListas, ModuleLot;

{$R *.DFM}

procedure TFmMaloteConf.BtSaidaClick(Sender: TObject);
begin
  Geral.WriteAppKey('Malote\Confere\', Application.Title,
    RGOQue.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
  Close;
end;

procedure TFmMaloteConf.EdClienteChange(Sender: TObject);
begin
  ReopenLot();
end;

procedure TFmMaloteConf.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  //
  FLotCab := 0;
  FLotIts := 0;
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
  RGOQue.ItemIndex := Geral.ReadAppKey('Malote\Confere\', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
  ReopenLot();
end;

procedure TFmMaloteConf.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMaloteConf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMaloteConf.ReopenLot();
var
  Per: Boolean;
  Cli: Integer;
  Lig: String;
begin
  Cli := EdCliente.ValueVariant;
  //Lig := 'WHERE ';
{
  QrLot.Close;
  QrLot.SQL.Clear;
  QrLot.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrLot.SQL.Add('ELSE en.Nome END NOMECLI, lo.Codigo, lo.Cliente,');
  QrLot.SQL.Add('lo.Lote, lo.Data, lo.Total, lo.Itens');
  QrLot.SQL.Add('FROM lot es lo');
  QrLot.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrLot.SQL.Add('WHERE lo.Tipo=0');
  QrLot.SQL.Add('AND lo.Codigo>0');
  QrLot.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  Lig := 'AND ';
  if RGStatus.ItemIndex < 2 then
  begin
    QrLot.SQL.Add(Lig+'lo.Conferido='+IntToStr(RGStatus.ItemIndex));
    //Lig := 'AND ';
  end;
  if Cli <> 0 then
  begin
    QrLot.SQL.Add(Lig+'lo.Cliente='+IntToStr(Cli));
    //Lig := 'AND ';
  end;
  Per := CkPeriodo.Checked;
  QrLot.SQL.Add(dmkPF.SQL_Periodo(Lig+'lo.Data ',
    TPIni.Date, TPFim.Date, Per, Per));
  QrLot.SQL.Add('ORDER BY NOMECLI, Lote DESC');
  QrLot. Open;
}
  Lig := 'AND ';
  Per := CkPeriodo.Checked;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLot, Dmod.MyDB, [
  'SELECT IF(en.Tipo=0, en.RazaoSocial, ',
  'en.Nome) NOMECLI, lo.Codigo, lo.Cliente, ',
  'lo.Lote, lo.Data, lo.Total, lo.Itens ',
  'FROM ' + CO_TabLotA + ' lo ',
  'LEFT JOIN entidades en ON en.Codigo=lo.Cliente ',
  'WHERE lo.Tipo=0 ',
  'AND lo.Codigo>0 ',
  'AND lo.TxCompra + lo.ValValorem+ ' +
  Geral.FF0(FmPrincipal.FConnections) + ' >= 0.01 ',
  Geral.ATS_if(RGStatus.ItemIndex < 2, [
  Lig + 'lo.Conferido=' + Geral.FF0(RGStatus.ItemIndex)]),
  Geral.ATS_if(Cli <> 0, [
  Lig + 'lo.Cliente=' + Geral.FF0(Cli)]),
  dmkPF.SQL_Periodo(Lig + 'lo.Data ', TPIni.Date, TPFim.Date, Per, Per),
  'ORDER BY NOMECLI, Lote DESC ',
  '']);
  QrLot.Locate('Codigo', FLotCab, []);
end;

procedure TFmMaloteConf.CkPeriodoClick(Sender: TObject);
begin
  ReopenLot();
end;

procedure TFmMaloteConf.TPIniChange(Sender: TObject);
begin
  ReopenLot();
end;

procedure TFmMaloteConf.TPFimChange(Sender: TObject);
begin
  ReopenLot();
end;

procedure TFmMaloteConf.RGStatusClick(Sender: TObject);
begin
  ReopenLot();
end;

procedure TFmMaloteConf.EdValorExit(Sender: TObject);
begin
  case RGOQue.ItemIndex of
    0:
    begin
      LocalizaItem();
      if QrLoc.RecordCount > 0 then
      begin
        InsereMaloteConf(1, Geral.DMV(EdValor.Text), QrLocCredito.Value,
          QrLocFatNum.Value, QrLocFatParcela.Value, True);
        VoltaABanda;
      end;
    end;
    1: EdBanda.SetFocus;
    2:
    begin
      InsereMaloteConf(0, Geral.DMV(EdValor.Text), QrLocCredito.Value,
        QrLocFatNum.Value, -1, True);
      EdValor.Text := '';
      //EdValor.SetFocus;
    end;
  end;
end;

procedure TFmMaloteConf.EdBandaChange(Sender: TObject);
var
  CMC7: String;
  Banda: TBandaMagnetica;
  //Ban: Integer;
begin
  CMC7 := Geral.SoNumero_TT(EdBanda.Text);
  //Ban := Length(EdBanda.Text);
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    if MLAGeral.CalculaCMC7(CMC7) = 0 then
    begin
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := CMC7;
      //EdComp.Text    := Banda.Compe;
      EdBanco.Text   := Banda.Banco;
      EdAgencia.Text := Banda.Agencia;
      EdConta.Text   := Banda.Conta;
      EdCheque.Text  := Banda.Numero;
      //
      LocalizaItem();
      if QrLoc.RecordCount > 0 then
      begin
        if RGOQue.ItemIndex <> 1 then Timer1.Enabled := True
        else begin
          EdCPF.Text := Geral.FormataCNPJ_TT(QrLocCNPJCPF.Value);
          EdEmitente.Text := QrLocEmitente.Text;
          begin
            InsereMaloteConf(1, Geral.DMV(EdValor.Text), QrLocCredito.Value,
              QrLocFatNum.Value, QrLocFatParcela.Value, False);
            Timer1.Enabled := True;
          end;
        end;
      end else begin
        Beep;
        Application.MessageBox('Cheque n�o localizado!', 'Erro', MB_OK+MB_ICONERROR);  
      end;
    end;
  end;
end;

procedure TFmMaloteConf.RGOQueClick(Sender: TObject);
begin
  case RGOQue.ItemIndex of
    0: EdBanda.Enabled := True;
    1: EdBanda.Enabled := True;
    2: EdBanda.Enabled := False;
  end;
  case RGOQue.ItemIndex of
    0: EdValor.Enabled := True;
    1: EdValor.Enabled := False;
    2: EdValor.Enabled := True;
  end;
end;

procedure TFmMaloteConf.QrLotAfterScroll(DataSet: TDataSet);
begin
  ReopenLotIts();
end;

procedure TFmMaloteConf.ReopenLotIts();
begin
{
  QrLotIts.Close;
  QrLotIts.Params[0].AsInteger := QrLotCodigo.Value;
  QrLotIts. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrlotIts, Dmod.MyDB, [
  'SELECT ma.Valor VALORMALOTE, ma.Cheque LOCCHEQUE, ',
  'li.Controle, li.Banco, li.Agencia, li.ContaCorrente, ',
  'li.Documento, li.FatNum, li.FatParcela, ',
  'li.CNPJCPF, li.Emitente, li.Credito ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabMaLots + ' ma ON ma.Controle=li.FatParcela ',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND li.FatNum=' + Geral.FF0(QrLotCodigo.Value),
  '']);
  //
  QrLotIts.Locate('FatParcela', FLotIts, []);
  //
{
  QrSumM.Close;
  QrSumM.Params[0].AsInteger := QrLotCodigo.Value;
  QrSumM. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumM, Dmod.MyDB, [
  'SELECT SUM(ma.Valor) VALOR, SUM(ma.Cheque) CHEQUES ',
  'FROM ' + CO_TabMaLots + ' ma ',
  'WHERE ma.Codigo=' + Geral.FF0(QrLotCodigo.Value),
  '']);
end;

procedure TFmMaloteConf.DBG1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if DBG1.Visible then
  begin
    if Column.FieldName = 'LOCCHEQUE' then
      MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotItsLOCCHEQUE.Value);
    if Column.FieldName = 'CONF_V' then
      MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotItsCONF_V.Value);
  end;
  //
end;

procedure TFmMaloteConf.BtConfereClick(Sender: TObject);
begin
  PainelConfere.Visible  := True;
  GBConf.Visible := True;
  DBG2.Enabled           := False;
  DBG2.Align             := alTop;
  PainelConfere.Align    := alClient;
  GBCtrl.Visible := False;
  PainelConfig.Enabled   := False;
  //
  if EdBanda.Enabled then EdBanda.SetFocus else
  if EdValor.Enabled then EdValor.SetFocus;
end;

procedure TFmMaloteConf.BtDesisteClick(Sender: TObject);
begin
  GBCtrl.Visible := True;
  PainelConfere.Visible  := False;
  DBG2.Enabled           := True;
  GBConf.Visible := False;
  PainelConfere.Align    := alBottom;
  DBG2.Align             := alClient;
  PainelConfig.Enabled   := True;
end;

procedure TFmMaloteConf.DBG1CellClick(Column: TColumn);
begin
  FLotIts := QrLotItsFatParcela.Value;
  if (Column.Field.FieldName = 'LOCCHEQUE') then CliqueItem(0) else
  if (Column.Field.FieldName = 'CONF_V') then CliqueItem(1);
end;

procedure TFmMaloteConf.CliqueItem(Tipo: Integer);
var
  Localizou: Integer;
  Valor: Double;
begin
  Localizou := 0;
  Valor     := 0;
  Screen.Cursor := crHourGlass;
  case Tipo of
    0:
    begin
      Localizou := MLAGeral.IntBool_Inverte(QrLotItsLOCCHEQUE.Value);
      Valor     := QrLotItsVALORMALOTE.Value;
    end;
    1:
    begin
      Localizou := QrLotItsLOCCHEQUE.Value;
      if QrLotItsCONF_V.Value = 1 then Valor := 0 else
        Valor := QrLotItsCredito.Value;
    end;
    else Application.MessageBox(PChar('Situa��o de desconhecida!'),
    'Erro', MB_OK+MB_ICONERROR);
  end;
  InsereMaloteConf(Localizou, Valor, QrLotItsCredito.Value,
    QrLotItsFatNum.Value, QrLotItsFatParcela.Value, False);
  Screen.Cursor := crDefault;
end;

procedure TFmMaloteConf.InsereMaloteConf(Localizou: Integer; Valor, Real,
Codigo: Double; Controle: Integer; Avisa: Boolean);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabMaLots);
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Trunc(Codigo);
  Dmod.QrUpd.Params[1].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ' + CO_TabMaLots + ' SET Cheque=:P0, ');
  Dmod.QrUpd.SQL.Add('Valor=:P1, Codigo=:P2, Controle=:P3');
  Dmod.QrUpd.Params[0].AsInteger := Localizou;
  Dmod.QrUpd.Params[1].AsFloat   := Valor;
  Dmod.QrUpd.Params[2].AsInteger := Trunc(Codigo);
  Dmod.QrUpd.Params[3].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotIts();
  //
  if (int(Valor*100) <> (Real*100)) and Avisa then
  begin
    Beep;
    Application.MessageBox('Valor n�o Confere!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmMaloteConf.QrLotItsCalcFields(DataSet: TDataSet);
begin
  QrLotItsCONF_V.Value := MLAGeral.BoolToInt(QrLotItsCredito.Value =
    QrLotItsVALORMALOTE.Value);
  //
  QrLotItsDADOS.Value := MLAGeral.FFD(QrLotItsBanco.Value, 3, siPositivo)+
    ' / ' + MLAGeral.FFD(QrLotItsAgencia.Value, 4, siPositivo) +
    ' / ' + QrLotItsContaCorrente.Value +
    ' - ' + MLAGeral.FFD(QrLotItsDocumento.Value, 6, siPositivo);
end;

procedure TFmMaloteConf.BtRatificaClick(Sender: TObject);
begin
  RatificaLote(1);
  BtDesisteClick(Self);
end;                                    

procedure TFmMaloteConf.RatificaLote(Status: Integer);
begin
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot es SET AlterWeb=1,  Conferido=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Status;
  Dmod.QrUpd.Params[1].AsInteger := QrLotCodigo.Value;
  Dmod.QrUpd.ExecSQL;
}
  if DmLot.LOT_Ins_Upd(Dmod.QrUpd, stUpd, False, [
  'Conferido'], ['Codigo'], [Status], [QrLotCodigo.Value], True) then
  begin
    FLotCab := UMyMod.ProximoRegistro(QrLot, 'Codigo', QrLotCodigo.Value);
    ReopenLot();
    FmPrincipal.AtualizaSP2(1);
  end;
end;

procedure TFmMaloteConf.BtDesfazClick(Sender: TObject);
begin
  RatificaLote(0);
end;

procedure TFmMaloteConf.DBEdItensChange(Sender: TObject);
begin
  DBEdItens.Font.Color := MLAGeral.BoolToInt2(QrSumMCHEQUES.Value =
    QrLotItens.Value, clGreen, clRed);
end;

procedure TFmMaloteConf.DBEdValorChange(Sender: TObject);
begin
  DBEdValor.Font.Color := MLAGeral.BoolToInt2(QrSumMVALOR.Value =
    QrLotTotal.Value, clGreen, clRed);
end;

procedure TFmMaloteConf.LocalizaItem();
begin
{
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := QrLotCodigo.Value;
  QrLoc.Params[1].AsInteger := EdBanco.ValueVariant;
  QrLoc.Params[2].AsInteger := EdAgencia.ValueVariant;
  QrLoc.Params[3].AsString  := EdConta.Text;
  QrLoc.Params[4].AsInteger := EdCheque.ValueVariant;
  QrLoc. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
  'SELECT FatNum, FatParcela, CNPJCPF, Emitente, Credito ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND FatNum=' + Geral.FF0(QrLotCodigo.Value),
  'AND Banco=' + Geral.FF0(EdBanco.ValueVariant),
  'AND Agencia=' + Geral.FF0(EdAgencia.ValueVariant),
  'AND ContaCorrente="' + EdConta.Text + '" ',
  'AND Documento=' + Geral.FF0(EdCheque.ValueVariant),
  '']);
end;

procedure TFmMaloteConf.BtLimparClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabMaLots);
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotIts();
  Screen.Cursor := crDefault;
end;

procedure TFmMaloteConf.VoltaABanda;
begin
  EdBanda.Text := '';
  EdBanda.SetFocus;
end;

procedure TFmMaloteConf.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  case RGOque.ItemIndex of
   0: EdValor.SetFocus;
   1: VoltaABanda;
   2: EdValor.SetFocus;
  end;
end;

end.

