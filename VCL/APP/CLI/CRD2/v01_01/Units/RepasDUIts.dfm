object FmRepasDUIts: TFmRepasDUIts
  Left = 339
  Top = 185
  Caption = 'REP-GEREN-006 :: Adi'#231#227'o de Duplicata a Repasse'
  ClientHeight = 436
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 382
        Height = 32
        Caption = 'Adi'#231#227'o de Duplicata a Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 32
        Caption = 'Adi'#231#227'o de Duplicata a Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 382
        Height = 32
        Caption = 'Adi'#231#227'o de Duplicata a Repasse'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 274
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 792
      Height = 274
      Align = alClient
      TabOrder = 0
      object PainelItens: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 257
        Align = alClient
        BevelOuter = bvNone
        Color = clAppWorkSpace
        TabOrder = 0
        object GradeCHs: TDBGrid
          Left = 0
          Top = 88
          Width = 788
          Height = 169
          Align = alClient
          DataSource = DsCheques
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Lote'
              Title.Caption = 'Border'#244
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco.'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag.'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D. Compra'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'D. Dep'#243'sito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 255
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 788
          Height = 88
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label11: TLabel
            Left = 8
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label13: TLabel
            Left = 8
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
          end
          object Label8: TLabel
            Left = 124
            Top = 4
            Width = 67
            Height = 13
            Caption = 'CPF Emitente:'
          end
          object Label14: TLabel
            Left = 368
            Top = 44
            Width = 54
            Height = 13
            Caption = 'Taxa [F12]:'
          end
          object Label15: TLabel
            Left = 320
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Border'#244':'
          end
          object EdCliente: TdmkEditCB
            Left = 8
            Top = 60
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 56
            Top = 60
            Width = 261
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsClientes
            TabOrder = 3
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdDuplicata: TdmkEdit
            Left = 8
            Top = 20
            Width = 113
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdDuplicataExit
          end
          object EdCNPJ: TdmkEdit
            Left = 124
            Top = 20
            Width = 113
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdCNPJExit
          end
          object EdTaxa: TdmkEdit
            Left = 368
            Top = 60
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnKeyDown = EdTaxaKeyDown
          end
          object CkInap: TCheckBox
            Left = 436
            Top = 60
            Width = 149
            Height = 17
            Caption = 'Mostrar cheques vencidos.'
            TabOrder = 6
            Visible = False
            OnClick = CkInapClick
          end
          object EdBordero: TdmkEdit
            Left = 320
            Top = 60
            Width = 45
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnExit = EdBorderoExit
          end
          object Memo1: TMemo
            Left = 591
            Top = 0
            Width = 197
            Height = 88
            Align = alRight
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              'Ctrl + Mouse '
              'seleciona mais'
              'de uma duplicata.')
            ParentFont = False
            ReadOnly = True
            TabOrder = 7
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 322
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 366
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma2: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirma2Click
      end
      object BtExclui2: TBitBtn
        Tag = 12
        Left = 276
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExclui2Click
      end
    end
  end
  object QrCheques: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequesAfterOpen
    AfterClose = QrChequesAfterClose
    OnCalcFields = QrChequesCalcFields
    SQL.Strings = (
      'SELECT lo.Lote, li.* '
      'FROM lct0001a li '
      'LEFT JOIN lot0001a lo ON lo.Codigo=li.FatNum '
      'WHERE li.FatID=301'
      'AND lo.Tipo=1 '
      'AND li.DDeposito > "2011-12-05" '
      'AND li.Quitado = 0 '
      'AND li.Repassado = 0 '
      ''
      ''
      '')
    Left = 448
    Top = 12
    object QrChequesComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequesTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequesTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequesVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequesVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequesDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequesDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequesDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequesDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequesDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequesQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequesBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequesCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrChequesLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '0000'
    end
    object QrChequesCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrChequesFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrChequesFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrChequesVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrChequesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrChequesCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 476
    Top = 12
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 508
    Top = 12
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 536
    Top = 12
  end
end
