object FmLot2DupMu1: TFmLot2DupMu1
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-018 :: Lote - Duplicatas (Excel Modelo 1)'
  ClientHeight = 491
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 417
        Height = 32
        Caption = 'Lote - Duplicatas (Excel Modelo 1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 417
        Height = 32
        Caption = 'Lote - Duplicatas (Excel Modelo 1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 417
        Height = 32
        Caption = 'Lote - Duplicatas (Excel Modelo 1)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 316
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 316
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 70
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Arquivo:'
          end
          object SpeedButton1: TSpeedButton
            Left = 448
            Top = 0
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object Label2: TLabel
            Left = 476
            Top = 4
            Width = 147
            Height = 13
            Caption = 'Separador Duplicata x Parcela:'
          end
          object Label70: TLabel
            Left = 216
            Top = 28
            Width = 52
            Height = 13
            Caption = 'Desco at'#233':'
          end
          object Label67: TLabel
            Left = 321
            Top = 28
            Width = 17
            Height = 13
            Caption = 'D+:'
          end
          object Label35: TLabel
            Left = 344
            Top = 28
            Width = 49
            Height = 13
            Caption = 'Tx.com %:'
          end
          object Label204: TLabel
            Left = 400
            Top = 28
            Width = 115
            Height = 13
            Caption = 'Carteira de recebimento:'
          end
          object Label92: TLabel
            Left = 823
            Top = 28
            Width = 34
            Height = 13
            Caption = 'Banco:'
          end
          object Label93: TLabel
            Left = 859
            Top = 28
            Width = 42
            Height = 13
            Caption = 'Ag'#234'ncia:'
          end
          object Label95: TLabel
            Left = 664
            Top = 4
            Width = 80
            Height = 13
            Caption = 'Nome do Banco:'
            FocusControl = DBEdit23
          end
          object Label3: TLabel
            Left = 8
            Top = 28
            Width = 42
            Height = 13
            Caption = 'Emiss'#227'o:'
          end
          object Label4: TLabel
            Left = 112
            Top = 28
            Width = 39
            Height = 13
            Caption = 'Compra:'
          end
          object EdArquivo: TEdit
            Left = 48
            Top = 0
            Width = 401
            Height = 21
            TabOrder = 0
          end
          object EdSeparador: TdmkEdit
            Left = 624
            Top = 0
            Width = 29
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '/'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '/'
          end
          object dmkEdTPDescAte: TdmkEditDateTimePicker
            Left = 216
            Top = 44
            Width = 101
            Height = 21
            Date = 40074.401972708350000000
            Time = 40074.401972708350000000
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdDMaisD: TdmkEdit
            Left = 321
            Top = 44
            Width = 20
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdTxaCompra4: TdmkEdit
            Left = 344
            Top = 44
            Width = 53
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdCartDep4: TdmkEditCB
            Left = 400
            Top = 44
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBCartDep4
            IgnoraDBLookupComboBox = False
          end
          object CBCartDep4: TdmkDBLookupComboBox
            Left = 448
            Top = 44
            Width = 369
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DmLot.DsCarteiras4
            TabOrder = 6
            dmkEditCB = EdCartDep4
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object DBEdBanco4: TDBEdit
            Left = 823
            Top = 44
            Width = 33
            Height = 21
            TabStop = False
            DataField = 'Banco1'
            DataSource = DmLot.DsCarteiras4
            TabOrder = 7
          end
          object DBEdAgencia4: TDBEdit
            Left = 859
            Top = 44
            Width = 41
            Height = 21
            TabStop = False
            DataField = 'Agencia1'
            DataSource = DmLot.DsCarteiras4
            TabOrder = 8
          end
          object DBEdConta4: TDBEdit
            Left = 903
            Top = 44
            Width = 94
            Height = 21
            TabStop = False
            DataField = 'Conta1'
            DataSource = DmLot.DsCarteiras4
            TabOrder = 9
          end
          object DBEdit23: TDBEdit
            Left = 748
            Top = 0
            Width = 249
            Height = 21
            TabStop = False
            DataField = 'Nome'
            DataSource = DmLot.DsBanco4
            TabOrder = 10
          end
          object TPEmiss4: TdmkEditDateTimePicker
            Left = 8
            Top = 44
            Width = 101
            Height = 21
            Date = 40074.401972708350000000
            Time = 40074.401972708350000000
            TabOrder = 11
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object TPData4: TdmkEditDateTimePicker
            Left = 112
            Top = 44
            Width = 101
            Height = 21
            Date = 40074.401972708350000000
            Time = 40074.401972708350000000
            TabOrder = 12
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 85
          Width = 1004
          Height = 229
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          object TabSheet3: TTabSheet
            Caption = 'Coligadas'
            ExplicitLeft = 0
            ExplicitTop = 28
            ExplicitWidth = 0
            ExplicitHeight = 33
            object GradeDU: TStringGrid
              Left = 0
              Top = 0
              Width = 996
              Height = 201
              Align = alClient
              DefaultRowHeight = 19
              RowCount = 7
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
              TabOrder = 0
              OnDrawCell = GradeDUDrawCell
              OnKeyDown = GradeDUKeyDown
              ExplicitTop = 9
              ExplicitHeight = 144
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Arquivo original'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 227
            object Grade1: TStringGrid
              Left = 0
              Top = 0
              Width = 996
              Height = 201
              Align = alClient
              ColCount = 8
              DefaultColWidth = 18
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              ExplicitTop = 4
              ExplicitHeight = 149
            end
          end
          object TabSheet1: TTabSheet
            Caption = 'Extrato carregado'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade2: TStringGrid
              Left = 0
              Top = 0
              Width = 996
              Height = 201
              Align = alClient
              ColCount = 15
              DefaultColWidth = 18
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              ColWidths = (
                18
                67
                18
                18
                18
                18
                18
                18
                18
                18
                18
                18
                18
                18
                18)
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 364
    Width = 1008
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 421
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma4: TBitBtn
        Tag = 14
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirma4Click
      end
      object BitBtn1: TBitBtn
        Tag = 39
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abrir'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.xls'
    Title = 'Arquivo Excel (XLS)'
    Left = 516
    Top = 180
  end
end
