object FmDeposito: TFmDeposito
  Left = 339
  Top = 185
  Caption = 'CHQ-DEPOS-003 :: Cheques Abertos'
  ClientHeight = 563
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBG1: TDBGrid
      Left = 0
      Top = 101
      Width = 1008
      Height = 300
      Align = alClient
      DataSource = DsCheques
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBG1CellClick
      OnKeyDown = DBG1KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia'
          Title.Caption = 'Agen.'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ContaCorrente'
          Title.Caption = 'Conta corrente'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Title.Caption = 'Border'#244
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatParcela'
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 101
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel5: TPanel
        Left = 699
        Top = 0
        Width = 309
        Height = 101
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 101
          Height = 101
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object CkDescende1: TCheckBox
            Left = 4
            Top = 12
            Width = 89
            Height = 17
            Caption = 'Descendente.'
            TabOrder = 0
            OnClick = RGOrdem1Click
          end
          object CkDescende2: TCheckBox
            Left = 4
            Top = 44
            Width = 89
            Height = 17
            Caption = 'Descendente.'
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = RGOrdem1Click
          end
          object CkDescende3: TCheckBox
            Left = 4
            Top = 76
            Width = 89
            Height = 17
            Caption = 'Descendente.'
            TabOrder = 2
            OnClick = RGOrdem1Click
          end
        end
        object Panel8: TPanel
          Left = 101
          Top = 0
          Width = 208
          Height = 101
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label24: TLabel
            Left = 106
            Top = 56
            Width = 95
            Height = 13
            Caption = 'Soma selecionados:'
          end
          object Label2: TLabel
            Left = 4
            Top = 56
            Width = 84
            Height = 13
            Caption = 'Total pesquisado:'
            FocusControl = DBEdit1
          end
          object EdSoma: TdmkEdit
            Left = 106
            Top = 72
            Width = 97
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 208
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Label1: TLabel
              Left = 4
              Top = 8
              Width = 86
              Height = 13
              Caption = 'At'#233' o vencimento:'
            end
            object TPVencto: TDateTimePicker
              Left = 4
              Top = 24
              Width = 97
              Height = 21
              Date = 39140.000000000000000000
              Time = 0.395218819401634400
              TabOrder = 0
              OnChange = TPVenctoChange
            end
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 72
            Width = 97
            Height = 21
            DataField = 'VALOR'
            DataSource = DsSoma
            TabOrder = 2
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 699
        Height = 101
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object RGOrdem3: TRadioGroup
          Left = 0
          Top = 32
          Width = 699
          Height = 32
          Align = alTop
          Caption = ' Ordem 3: '
          Columns = 6
          ItemIndex = 1
          Items.Strings = (
            'Vencimento'
            'Valor'
            'Cheque'
            'BAC'
            'Emitente'
            'Cliente')
          TabOrder = 0
          OnClick = RGOrdem1Click
        end
        object RGOrdem2: TRadioGroup
          Left = 0
          Top = 64
          Width = 699
          Height = 32
          Align = alTop
          Caption = ' Ordem 2: '
          Columns = 6
          ItemIndex = 2
          Items.Strings = (
            'Vencimento'
            'Valor'
            'Cheque'
            'BAC'
            'Emitente'
            'Cliente')
          TabOrder = 1
          OnClick = RGOrdem1Click
        end
        object RGOrdem1: TRadioGroup
          Left = 0
          Top = 0
          Width = 699
          Height = 32
          Align = alTop
          Caption = ' Ordem 1: '
          Columns = 6
          ItemIndex = 0
          Items.Strings = (
            'Vencimento'
            'Valor'
            'Cheque'
            'BAC'
            'Emitente'
            'Cliente')
          TabOrder = 2
          OnClick = RGOrdem1Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 210
        Height = 32
        Caption = 'Cheques Abertos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 210
        Height = 32
        Caption = 'Cheques Abertos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 210
        Height = 32
        Caption = 'Cheques Abertos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 449
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 493
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel10: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 10
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtRefreshClick
      end
      object BtDeposito: TBitBtn
        Tag = 301
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Dep'#243'sito'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtDepositoClick
      end
    end
  end
  object QrCheques: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequesAfterOpen
    AfterClose = QrChequesAfterClose
    SQL.Strings = (
      '/*'
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Vencto <= :P0'
      '*/'
      ''
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, '
      'ent.Nome) NOMECLI, lot.Lote, loi.FatNum,'
      'loi.FatParcela, loi.Data, loi.Vencimento,'
      'loi.DCompra, loi.DDeposito, loi.Emitente,'
      'loi.CNPJCPF, loi.Credito, loi.Documento,'
      'loi.Banco, loi.Agencia, loi.ContaCorrente'
      'FROM lct0001a loi'
      'LEFT JOIN lot0001a lot ON lot.Codigo=loi.FatNum'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE loi.FatID=301'
      'AND lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Depositado = 0'
      'AND loi.Vencimento <= :P0'
      '')
    Left = 212
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequesLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000'
    end
    object QrChequesFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrChequesFatParcela: TIntegerField
      FieldName = 'FatParcela'
      DisplayFormat = '000000'
    end
    object QrChequesData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrChequesCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrChequesCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrChequesDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000'
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrChequesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 240
    Top = 68
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 332
    Top = 205
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) VALOR'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Vencto <= :P0')
    Left = 212
    Top = 97
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 241
    Top = 97
  end
  object PMDeposito: TPopupMenu
    Left = 128
    Top = 420
    object Itemselecionado1: TMenuItem
      Caption = '&Itens selecionados'
      OnClick = Itemselecionado1Click
    end
    object Determinarperodo1: TMenuItem
      Caption = '&Determinado per'#237'odo'
      OnClick = Determinarperodo1Click
    end
  end
  object QrMin: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(loi.Vencto) Vencto'
      'FROM lot esits loi'
      'LEFT JOIN lot es lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Tipo=0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Depositado = 0')
    Left = 112
    Top = 232
    object QrMinVencto: TDateField
      FieldName = 'Vencto'
    end
  end
end
