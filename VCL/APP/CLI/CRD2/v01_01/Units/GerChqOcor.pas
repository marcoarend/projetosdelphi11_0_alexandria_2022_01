unit GerChqOcor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmGerChqOcor = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    PainelOcor: TPanel;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    DsOcorBank: TDataSource;
    GBPror: TGroupBox;
    Label13: TLabel;
    TPDataN: TDateTimePicker;
    Label14: TLabel;
    EdTaxaPror: TdmkEdit;
    Label29: TLabel;
    EdJurosPeriodoN: TdmkEdit;
    Label15: TLabel;
    EdValorBase: TdmkEdit;
    Label27: TLabel;
    EdJurosPr: TdmkEdit;
    Label16: TLabel;
    TPDataI: TDateTimePicker;
    GBOcor: TGroupBox;
    Label20: TLabel;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    Label21: TLabel;
    Label22: TLabel;
    EdValor: TdmkEdit;
    Label56: TLabel;
    EdTaxaP: TdmkEdit;
    TPDataO: TDateTimePicker;
    QrLocPr: TmySQLQuery;
    QrLocPrCodigo: TIntegerField;
    QrLocPrControle: TIntegerField;
    QrLocPrData1: TDateField;
    QrLocPrData2: TDateField;
    QrLocPrData3: TDateField;
    QrLocPrOcorrencia: TIntegerField;
    QrLocPrLk: TIntegerField;
    QrLocPrDataCad: TDateField;
    QrLocPrDataAlt: TDateField;
    QrLocPrUserCad: TIntegerField;
    QrLocPrUserAlt: TIntegerField;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TPDataNChange(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure EdOcorrenciaExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdValorBaseChange(Sender: TObject);
    procedure EdTaxaProrChange(Sender: TObject);
    procedure EdJurosPrChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaAPagarPrr();
    procedure CalculaJurosPrr();
  public
    { Public declarations }
    FVencimento: TDateTime;
    FCredito: Double;
    FCliente,
    FLotPrrControle, FOcorreuCodigo, FFatParcela, FLotPrrRecordCount: Integer;
    FForm: String;
    FNaoDeposita: Boolean;
    FDDeposito, FNovaData: TDateTime;
    //
    procedure ConfiguraPr(Data: TDateTime);
  end;

  var
  FmGerChqOcor: TFmGerChqOcor;

implementation

uses UnMyObjects, Module, GerChqMain, UMySQLModule, OcorBank, MyDBCheck,
  MyListas;

{$R *.DFM}

procedure TFmGerChqOcor.BtOKClick(Sender: TObject);
var
  Ocorr, Prorr: Integer;
  Data: String;
  //
  DDeposito, Data3, Vencimento: String;
  ProrrVz, ProrrDd, NaoDeposita, Quitado, FatParcela: Integer;
begin
  Prorr := FLotPrrControle; //FmGerChqMain.QrLotPrrControle.Value;
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
    Ocorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Ocorreu', 'Ocorreu', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
    Ocorr := FOcorreuCodigo; //FmGerChqMain.QrOcorreuCodigo.Value;
  end;
  Dmod.QrUpd.SQL.Add(FLD_LOIS + '=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, Cliente=:P4, ');
  Dmod.QrUpd.SQL.Add('TaxaP=:P5 ');
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  end else begin
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  end;
  Dmod.QrUpd.Params[00].AsInteger := FFatParcela; //FmGerChqMain.QrPesqFatParcela.Value;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
  Dmod.QrUpd.Params[02].AsInteger := EdOcorrencia.ValueVariant;
  Dmod.QrUpd.Params[03].AsFloat   := EdValor.ValueVariant;
  Dmod.QrUpd.Params[04].AsInteger := FCliente;
  Dmod.QrUpd.Params[05].AsFloat   := EdTaxaP.ValueVariant;
  Dmod.QrUpd.Params[06].AsInteger := Ocorr;
  Dmod.QrUpd.ExecSQL;
  //
  if GBPror.Visible then
  begin
    Data := FormatDateTime(VAR_FORMATDATE, TPDataN.Date);
    Dmod.QrUpd.SQL.Clear;
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO ' + CO_TabLotPrr + ' SET ');
      Prorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        CO_TabLotPrr, CO_TabLotPrr, 'Controle');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLotPrr + ' SET ');
      Prorr := FLotPrrControle; //FmGerChqMain.QrLotPrrControle.Value;
    end;
    Dmod.QrUpd.SQL.Add('Codigo=:P0, Data1=:P1, Data2=:P2, Data3=:P3, ');
    Dmod.QrUpd.SQL.Add('Ocorrencia=:P4');
    if ImgTipo.SQLType = stIns then
    begin
      Dmod.QrUpd.SQL.Add(', Controle=:Pa');
    end else begin
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
    end;
    Dmod.QrUpd.Params[00].AsInteger := FFatParcela;//FmGerChqMain.QrPesqFatParcela.Value;
    Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
    Dmod.QrUpd.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
    Dmod.QrUpd.Params[03].AsString  := Data;
    Dmod.QrUpd.Params[04].AsInteger := Ocorr;
    //
    Dmod.QrUpd.Params[05].AsInteger := Prorr;
    Dmod.QrUpd.ExecSQL;
    //
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
    Dmod.QrUpd.SQL.Add('ProrrDd=:P2, NaoDeposita=:P3, Data3=:P4, Quitado=:P5  ');  //Parei aqui
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
    Dmod.QrUpd.Params[0].AsString  := Data;
    Dmod.QrUpd.Params[1].AsInteger := FmGerChqMain.QrLotPrr.RecordCount+1;
    Dmod.QrUpd.Params[2].AsInteger := Trunc(Int(TPDataN.Date)-Int(TPDataI.Date));
    Dmod.QrUpd.Params[3].AsInteger := MLAGeral.BoolToInt(FmGerChqMain.FNaoDeposita);
    Dmod.QrUpd.Params[4].AsString  := Data;
    Dmod.QrUpd.Params[5].AsInteger := -1; // prorrogação
    //
    Dmod.QrUpd.Params[6].AsInteger := FmGerChqMain.QrPesqFatParcela.Value;
/
    Dmod.QrUpd.ExecSQL;
}
    DDeposito := Data;
    Vencimento := DDeposito;
    ProrrVz := FLotPrrRecordCount; //FmGerChqMain.QrLotPrr.RecordCount + 1;
    ProrrDd := Trunc(Int(TPDataN.Date)-Int(TPDataI.Date));
    NaoDeposita := MLAGeral.BoolToInt(FNaoDeposita(*FmGerChqMain.FNaoDeposita*));
    Data3 := Data;
    Quitado := -1; // Prorrogação
    //
    FatParcela := FFatParcela; //FmGerChqMain.QrPesqFatParcela.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
    'Vencimento', 'DDeposito', 'ProrrVz',
    'ProrrDd', 'NaoDeposita', 'Data3',
    'Quitado'
    ], ['FatID', 'FatParcela'], [
    Vencimento, DDeposito, ProrrVz,
    ProrrDd, NaoDeposita, Data3,
    Quitado
    ], [VAR_FATID_0301, FatParcela], True) then
      FNovaData := TPDataN.Date;
    //
    //
  end;
  if FForm = 'FmGerChqMain' then
  begin
    FmGerChqMain.FOcorreu  := Ocorr;
    FmGerChqMain.FLotPrr := Prorr;
    //
    FmGerChqMain.Pesquisa(True, False);
  end;
  //
  Close;
end;

procedure TFmGerChqOcor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerChqOcor.CalculaAPagarPrr();
begin
  EdValor.Text := EdJurosPr.Text;
end;

procedure TFmGerChqOcor.CalculaJurosPrr();
var
  Prazo, Praz2: Integer;
  Taxa, Juros, Valor: Double;
begin
  Prazo := Trunc(Int(TPDataN.Date) - Int(FDDeposito(*FmGerChqMain.QrPesqDDeposito.Value*)));
  if Prazo < 0 then Praz2 := -Prazo else Praz2 := Prazo;
  begin
    Taxa  := EdTaxaPror.ValueVariant;
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Praz2);
  end;
  Valor := EdValorBase.ValueVariant;
  EdJurosPeriodoN.ValueVariant := Juros;
  Juros := Juros * Valor / 100;
  if Prazo < 0 then Juros := Juros * -1;
  EdJurosPr.ValueVariant := Juros;
end;

procedure TFmGerChqOcor.ConfiguraPr(Data: TDateTime);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrlocPr, Dmod.MyDB, [
  'SELECT * FROM ' + CO_TabLotPrr,
  'WHERE Data3=( ',
  'SELECT Max(Data3) FROM ' + CO_TabLotPrr,
  'WHERE Codigo=' + FormatFloat('0',
  FFatParcela) +') ',
  'ORDER BY Controle DESC ',
  '']);
  //
  TPDataN.MinDate := 0;
  if QrLocPr.RecordCount > 0 then
  begin
    TPDataN.Date := Int(QrLocPrData3.Value);
    TPDataO.Date := Int(QrLocPrData3.Value);
    TPDataI.Date := Int(QrLocPrData1.Value);
  end else begin
    TPDataN.Date := Int(FVencimento);
    TPDataO.Date := Date;//Int(QrPesqVencimento.Value);
    TPDataI.Date := Int(FVencimento);
  end;
  EdTaxaPror.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(FCliente), 6, siPositivo);
  EdValorBase.Text := Geral.FFT(FCredito, 2, siPositivo);
  // Permitir adiantamento
  //TPDataN.MinDate := TPDataO.Date;
  if Int(Data) > TPDataN.Date then TPDataN.Date := Int(Data);
  //TPdataN.SetFocus;
  CalculaJurosPrr();
end;

procedure TFmGerChqOcor.EdJurosPrChange(Sender: TObject);
begin
  CalculaAPagarPrr();
end;

procedure TFmGerChqOcor.EdOcorrenciaExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (GBPror.Visible = False) then
  EdValor.ValueVariant := QrOcorBankBase.Value;
end;

procedure TFmGerChqOcor.EdTaxaProrChange(Sender: TObject);
begin
  CalculaJurosPrr();
end;

procedure TFmGerChqOcor.EdValorBaseChange(Sender: TObject);
begin
  CalculaAPagarPrr();
end;

procedure TFmGerChqOcor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerChqOcor.FormCreate(Sender: TObject);
begin
  FNovaData := 0;
  TPDataN.Date := Date;
  TPDataI.Date := Date;
  TPDataO.Date := Date;
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
  ImgTipo.SQLType := stNil;
end;

procedure TFmGerChqOcor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerChqOcor.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmOcorBank, FmOcorBank, afmoNegarComAviso) then
  begin
    FmOcorBank.ShowModal;
    FmOcorBank.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdOcorrencia, CBOcorrencia, QrOcorBank, VAR_CADASTRO);
  end;
end;

procedure TFmGerChqOcor.TPDataIChange(Sender: TObject);
begin
  CalculaJurosPrr();
end;

procedure TFmGerChqOcor.TPDataNChange(Sender: TObject);
begin
  CalculaJurosPrr();
end;

end.
