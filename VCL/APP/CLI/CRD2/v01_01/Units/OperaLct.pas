unit OperaLct;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkImage, Grids, DBGrids, TypInfo, Menus, dmkEditDateTimePicker,
  Printers, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmOperaLct = class(TForm)
    PainelDados: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    Panel4: TPanel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPIni: TdmkEditDateTimePicker;
    Label34: TLabel;
    Label1: TLabel;
    TPFim: TdmkEditDateTimePicker;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    QrCtaFat0: TmySQLQuery;
    QrCtaFat0FatID: TIntegerField;
    PB1: TProgressBar;
    QrCtaFat1: TmySQLQuery;
    QrCtaFat1Codigo: TIntegerField;
    QrCtaFat1Sigla: TWideStringField;
    QrCtaFat1Ordem: TIntegerField;
    QrFlds1: TmySQLQuery;
    QrFlds1FatID: TIntegerField;
    QrNiv1: TmySQLQuery;
    DsNiv1: TDataSource;
    QrPsq1: TmySQLQuery;
    QrCtaFat2Ger: TmySQLQuery;
    DsCtaFat2Ger: TDataSource;
    QrCtaFat2GerCodigo: TIntegerField;
    QrCtaFat2GerNome: TWideStringField;
    QrCtaFat2Its: TmySQLQuery;
    QrCtaFat2ItsDC: TWideStringField;
    QrConfere: TmySQLQuery;
    QrConfereFatNum: TFloatField;
    QrConfereLote: TIntegerField;
    QrConfereData: TDateField;
    QrConfereCodCli: TIntegerField;
    QrConfereNomCli: TWideStringField;
    QrConfereEntradas: TFloatField;
    QrConfereSaidas: TFloatField;
    QrConfereErro: TFloatField;
    frxConfere0: TfrxReport;
    frxDsConfere: TfrxDBDataset;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrCtaFat2ItsFatID: TIntegerField;
    frxSintetico: TfrxReport;
    frxDsNiv1: TfrxDBDataset;
    frxConfere1: TfrxReport;
    QrCtaFat0ChkFator: TFloatField;
    QrCtaFat0Ger: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsCtaFat0Ger: TDataSource;
    PageControl1: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    EdPercZoom: TdmkEdit;
    EdTamFonte: TdmkEdit;
    BtImprime1: TBitBtn;
    CkDesign: TCheckBox;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    EdCtaFat2Ger: TdmkEditCB;
    CBCtaFat2Ger: TdmkDBLookupComboBox;
    CkSoErrConf: TCheckBox;
    GradeA: TDBGrid;
    Panel5: TPanel;
    BtGera: TBitBtn;
    BtBordero: TBitBtn;
    PageControl3: TPageControl;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Label7: TLabel;
    EdCtaFat0Ger: TdmkEditCB;
    CBCtaFat0Ger: TdmkDBLookupComboBox;
    BtImprime0: TBitBtn;
    BtImprime2: TBitBtn;
    DBGrid1: TDBGrid;
    QrArrec: TmySQLQuery;
    DsArrec: TDataSource;
    frxDsArrec: TfrxDBDataset;
    QrArrecNO_FatID: TWideStringField;
    QrArrecFatID: TIntegerField;
    QrArrecValor: TFloatField;
    frxArrec: TfrxReport;
    RGOrdem: TRadioGroup;
    CkBorderos: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtGeraClick(Sender: TObject);
    procedure QrNiv1AfterOpen(DataSet: TDataSet);
    procedure QrNiv1BeforeClose(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure BtImprime1Click(Sender: TObject);
    procedure frxConfere0GetValue(const VarName: string; var Value: Variant);
    procedure EdPercZoomChange(Sender: TObject);
    procedure BtImprime2Click(Sender: TObject);
    procedure RGOrdemClick(Sender: TObject);
    procedure BtBorderoClick(Sender: TObject);
    procedure BtImprime0Click(Sender: TObject);
    procedure EdCtaFat0GerChange(Sender: TObject);
  private
    { Private declarations }
    FArrFatID, FArrNiv1Codi: array of Integer;
    FArrFator: array of Double;
    FArrNiv1Flds: array of String;
    FOLC_1, FOLC_2: String;
    FFatIDs, FCliente, FEmpresa: Integer;
    //
    //procedure ExecutaSQLItem(Item: Integer);
    procedure ExecutaSQLFatID(Item: Integer);
    procedure FechaPesquisa();
    procedure ImprimeOperaSintetico();
    procedure ReacertaFrxDataSets();
    function  Zum(Inteiro: Integer): Integer;
    procedure AdicionaMemos(const Band: TfrxBand; const T, M:
              Integer; Titulo: String; var TamFix: Integer);

  public
    { Public declarations }
    FEnableBtBordero: Boolean;
  end;

  var
  FmOperaLct: TFmOperaLct;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, MyListas, ModuleGeral,
  UCreateCredito, Principal;

procedure TFmOperaLct.BtImprime0Click(Sender: TObject);
begin
  if CkBorderos.Checked then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrArrec, Dmod.MyDB, [
    'SELECT cf0.Nome NO_FatID, lct.FatID, ',
    'SUM(lct.Credito - lct.Debito) Valor ',
    'FROM ' + CO_TabLctA + ' lct ',
    'LEFT JOIN ctafat0its c0i ON lct.FatID=c0i.FatNiv0 ',
    'LEFT JOIN ctafat0 cf0 ON cf0.FatID=c0i.FatNiv0 ',
    'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum ',
    'WHERE c0i.Codigo=' + Geral.FF0(EdCtaFat0Ger.ValueVariant),
    dmkPF.SQL_Periodo('AND lot.Data ', TPIni.Date, TPFim.Date, True, True),
    'GROUP BY lct.FatID ',
    'ORDER BY lct.FatID ',
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrArrec, Dmod.MyDB, [
    'SELECT cf0.Nome NO_FatID, lct.FatID, ',
    'SUM(lct.Credito - lct.Debito) Valor ',
    'FROM ' + CO_TabLctA + ' lct ',
    'LEFT JOIN ctafat0its c0i ON lct.FatID=c0i.FatNiv0 ',
    'LEFT JOIN ctafat0 cf0 ON cf0.FatID=c0i.FatNiv0 ',
    'WHERE c0i.Codigo=' + Geral.FF0(EdCtaFat0Ger.ValueVariant),
    dmkPF.SQL_Periodo('AND lct.Data ', TPIni.Date, TPFim.Date, True, True),
    'GROUP BY lct.FatID ',
    'ORDER BY lct.FatID ',
    '']);
  end;
  //
  MyObjects.frxMostra(frxArrec, 'Arrecada��es em border�s');
end;

procedure TFmOperaLct.BtImprime1Click(Sender: TObject);
begin
  ImprimeOperaSintetico();
end;

procedure TFmOperaLct.BtImprime2Click(Sender: TObject);
var
  Creditos, Debitos, Ordem: String;
begin
  if MyObjects.FIC(EdCtaFat2Ger.ValueVariant = 0, EdCtaFat2Ger,
  'Informe a "Configura��o do relat�rio de confer�ncia" antes de selecionar esta impress�o!')
  then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Creditos := '0';
    Debitos := '0';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCtaFat2Its, Dmod.MyDB, [
    'SELECT DISTINCT cf0.FatID, cfi.DC ',
    'FROM ctafat2its cfi ',
    'LEFT JOIN ctafat0 cf0 ON cf0.FatNiv2=cfi.FatNiv2 ',
    'WHERE cfi.Codigo=' + Geral.FF0(EdCtaFat2Ger.ValueVariant),
    'ORDER BY DC, FatNiv1 ',
    '']);
    QrCtaFat2Its.First;
    while not QrCtaFat2Its.Eof do
    begin
      if Uppercase(QrCtaFat2ItsDC.Value) = 'C' then
        Creditos := Creditos + ' + Fld' + Geral.FFN(QrCtaFat2ItsFatID.Value, 4)
      else
      if Uppercase(QrCtaFat2ItsDC.Value) = 'D' then
        Debitos := Debitos + ' + Fld' + Geral.FFN(QrCtaFat2ItsFatID.Value, 4)
      else
        Geral.MensagemBox('"DC" n�o definido para o "CtaFat2Its" n�mero ' +
          Geral.FF0(QrCtaFat2ItsFatID.Value), 'ERRO', MB_OK+MB_ICONERROR);
      //
      QrCtaFat2Its.Next;
    end;
    //
    FOLC_2 := UCriarCredito.RecriaTempTableNovo(ntrtt_OLC_2_, DModG.QrUpdPID1);
    case RGOrdem.ItemIndex of
      0: Ordem := 'ORDER BY NomCli, Data, Lote, FatNum ';
      1: Ordem := 'ORDER BY FatNum';
      else Ordem := 'ORDER BY ? ? ? ?';
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrConfere, DModG.MyPID_DB, [
    'INSERT INTO ' + FOLC_2,
    'SELECT FatNum, Lote, Data, CodCli, NomCli, ',
    'SUM(' + Creditos + ') Entradas, ',
    'SUM(' + Debitos  + ') Saidas, ',
    'SUM(' + Creditos + ' + ' + Debitos + ') Erro, ',
    '1 Ativo ',
    'FROM ' + FOLC_1,
    'GROUP BY FatNum, Lote, Data, CodCli; ',
    'SELECT * FROM ' + FOLC_2,
    Geral.ATS_if(CkSoErrConf.Checked,
    ['WHERE (Erro >= 0.01 OR Erro <= -0.01)']),
    Ordem,
    '']);
    //
    case RGOrdem.ItemIndex of
      0: MyObjects.frxMostra(frxConfere0, 'Confer�ncia de Opera��es');
      1: MyObjects.frxMostra(frxConfere1, 'Confer�ncia de Opera��es');
    end;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOperaLct.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOperaLct.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOperaLct.EdCtaFat0GerChange(Sender: TObject);
begin
  BtImprime0.Enabled := EdCtaFat0Ger.ValueVariant <> 0;
end;

procedure TFmOperaLct.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> 0 then
    FEmpresa := DModG.QrEmpresasCodigo.Value
  else
    FEmpresa := 0;
  //
  FechaPesquisa();
end;

procedure TFmOperaLct.EdPercZoomChange(Sender: TObject);
begin
  EdTamFonte.ValueVariant :=                              // + 20%
    Int(GradeA.Font.Size * EdPercZoom.ValueVariant / 100) * 1.2;
end;

procedure TFmOperaLct.ExecutaSQLFatID(Item: Integer);
var
  I: Integer;
  Txts: String;
begin
  SetLength(Txts, FFatIDs);
  //
  Txts := '';
  for I := 0 to FFatIDs - 1 do
  begin
    if I = Item then
    begin
      Txts := Txts + '(SUM(lct.Credito-lct.Debito) * ' +
      Geral.FFT_Dot(FArrFator[I], 8, siNegativo) + ') Fld' +
      Geral.FFN(FArrFatID[I], 4) + ', ' + #13#10
    end else
      Txts := Txts + '0 Fld' + Geral.FFN(FArrFatID[I], 4) + ', ' + #13#10;
  end;
  //
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
  'INSERT INTO ' + FOLC_1,
  'SELECT lct.FatNum, lot.Lote, ',
  'lot.Data, lot.Cliente CodCli, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NomCli, ',
  'ct0.FatNiv1 Nivel1, ct0.FatNiv2 Nivel2, ',
  Txts, '1 Ativo ',
  'FROM ' + TMeuDB + '.' + CO_TabLctA + ' lct ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_TabLotA + ' lot ON lot.Codigo=lct.FatNum ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=lot.Cliente ',
  'LEFT JOIN Creditor.ctafat0 ct0 ON ct0.FatID=lct.FatID ',
  'WHERE lct.FatNum <> 0 ',
  'AND lct.CliInt=' + Geral.FF0(FEmpresa),
  Geral.ATS_If(FCliente <> 0, [
  'AND lct.Cliente=' + Geral.FF0(FCliente)]),
  'AND lct.FatID = ' + Geral.FF0(FArrFatID[Item]),
  dmkPF.SQL_Periodo('AND lot.Data ', TPIni.Date, TPFim.Date, True, True),
  'GROUP BY lct.FatNum, lot.Lote, lot.Data ',
  '']);
end;

procedure TFmOperaLct.FechaPesquisa();
begin
  QrNiv1.Close;
end;

procedure TFmOperaLct.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOperaLct.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOperaLct.frxConfere0GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS' else
    Value := CBCliente.Text;
  end else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    Geral.FDT(TPIni.Date, 2) + CO_ATE + Geral.FDT(TPFim.Date, 2);
end;

procedure TFmOperaLct.ImprimeOperaSintetico();
const
  PageBanLef = 100;
  PageBanWid = 0;
  PagePagFot = 100;
var
  Page: TfrxReportPage;
  Head: TfrxPageHeader;
  Shape: TfrxShapeView;
  Memo: TfrxMemoView;
  Line01: TfrxLineView;
  Band: TfrxMasterData;
  Summ: TfrxReportSummary;
  Foot: TfrxPageFooter;

  BANLEF, BANTOP, BANWID, BANHEI,
  MEMLEF, MEMTOP, MEMWID, MEMHEI: Integer;

  //GH: TfrxGroupHeader;
  //GF: TfrxGroupFooter;
  //RS: TfrxReportSummary;
  L, T, W, H, Z, M, X, Y, (*U,*) Posicao, TamFix, i(*, n*): Integer;
  Fld, Campo(*, Somas, Titulo*): String;
  AlinhaH: TfrxHAlign;
  //Somar: Boolean;
  Fator: Double;
  //
begin
  Fator := VAR_frCM / (EdPercZoom.ValueVariant / 100);
  // Configura frxReport
  frxSintetico.DataSets.Add(frxDsnIV1);
  //frxSintetico.DataSets.Items[0] := frxDsnIV1;
  //
  // Configura p�gina
  while frxSintetico.PagesCount > 0 do
    frxSintetico.Pages[0].Free;
  Page := TfrxReportPage.Create(frxSintetico);
  Page.CreateUniqueName;
  Page.Height       := 210;
{
  if EdLarguraFolha.ValueVariant > 680 then
  begin
}
    Page.Orientation  := poLandscape;
    X := 7700;                 // 2010-07-02 -> Original = 8700
    Page.LeftMargin   :=  20;  // 2010-07-02 -> Original = 10
    Page.RightMargin  :=  10;
    Page.TopMargin    :=  15;
    Page.BottomMargin :=  15;
{
  end else begin
    Page.Orientation  := poPortrait;
    X := 0;
    Page.LeftMargin   :=  15;
    Page.RightMargin  :=  15;
    Page.TopMargin    :=  10;
    Page.BottomMargin :=  10;
  end;
}
  //
  //////////////////////////////////////////////////////////////////////////////
  // Configura Page Header
  //
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  5000      / VAR_frCM);  // 2010-07-02 -> Original = 4500
  Head := TfrxPageHeader.Create(Page);
  Head.CreateUniqueName;
  Head.SetBounds(L, T, W, H);
  //////////////////////////////////////////////////////////////////////////////
  ///  Shape
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(     0      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  1000      / VAR_frCM);
  Shape := TfrxShapeView.Create(Head);
  Shape.CreateUniqueName;
  Shape.Visible := True;
  Shape.SetBounds(L, T, W, H);
  Shape.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  Shape.Frame.Width := 0.1;
  Shape.Shape := skRoundRectangle;
  //
  // Nome Empresa
  L :=  Round(   200      / VAR_frCM);
  T :=  Round(     0      / VAR_frCM);
  W :=  Round((17600 + X) / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := DModG.QrDonoNOMEDONO.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // Data, hora impress�o
  L :=  Round(   200      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round(  3800      / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'Impresso em [Date], [Time]';
  Memo.VAlign      := vaCenter;
  //
  // Nome Relat�rio
  L :=  Round(  4000      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((10000 + X) / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'RELAT�RIO SINT�TICO DE OPERA��ES';
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // P�gina ? de ?
  L :=  Round((14000 + X) / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round(  3800      / VAR_frCM);
  H :=  Round(   500      / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Univers Light Condensed';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := 'P�gina [Page] de [TotalPages]';
  Memo.HAlign      := haRight;
  Memo.VAlign      := vaCenter;
  //
  // Linha divis�ria shape
  L :=  Round(     0      / VAR_frCM);
  T :=  Round(   500      / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(     0      / VAR_frCM);
  Line01 := TfrxLineView.Create(Head);
  Line01.CreateUniqueName;
  Line01.SetBounds(L, T, W, H);
  Line01.Visible     := True;
  Line01.Frame.Width := 0.1;
  //
  // Nome Cliente
  L :=  Round(    0       / VAR_frCM);
  T :=  Round( 1200       / VAR_frCM);
  W :=  Round((18000 + X) / VAR_frCM);
  H :=  Round(  500       / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 8;
  Memo.Memo.Text   := '[VARF_CLIENTE]';//DmCond.QrCondNOMECLI.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  //
  // Per�odo do relat�rio (M�s e ano)
  L :=  Round(     0       / VAR_frCM);
  T :=  Round(  1800       / VAR_frCM);
  W :=  Round((18000 + X)  / VAR_frCM);
  H :=  Round(   500       / VAR_frCM);
  Memo := TfrxMemoView.Create(Head);
  Memo.CreateUniqueName;
  Memo.SetBounds(L, T, W, H);
  Memo.Visible     := True;
  Memo.Font.Name   := 'Arial';
  Memo.Font.Size   := 10;
  Memo.Memo.Text   := '[VARF_PERIODO]';//FmCondGer.QrPrevPERIODO_TXT.Value;
  Memo.Font.Style  := [fsBold];
  Memo.HAlign      := haCenter;
  Memo.VAlign      := vaCenter;
  Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
  Memo.Frame.Width := 0.1;

  Posicao := Trunc(Head.Top + Head.Height + 1);
  //H :=  THackDBGrid(GradeA).RowHeights[1];
  H :=  Zum(THackDBGrid(GradeA).DefaultRowHeight);
{
  // GroupHeader 1
  if RGAgrup.ItemIndex > 0 then
  begin
    T := Posicao;
    GH := TfrxGroupHeader.Create(Page);
    GH.CreateUniqueName;
    GH.SetBounds(0, T, 0, H);
    GH.Condition := 'frxDsnIV1."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"';
    //
    L :=  Round(     0       / Fator);
    //T :=  Round(  1800       / Fator);
    Z :=  Round( 2500 / Fator);
    W :=  Round((18000 + X)  / Fator);
    //H :=  Round(   500       / Fator);
    Memo := TfrxMemoView.Create(GH);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, 0, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Arial';
    Memo.Font.Size   := 10;
    Memo.Memo.Text   := OrdemPor_Tit[RGOrdem1.ItemIndex] + '[frxDsnIV1."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"]';
    Memo.Font.Style  := [fsBold];
(*
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
*)
    //
    Posicao := Trunc(GH.Top + GH.Height + 1);
  end;

  // GroupHeader 2
  if RGAgrup.ItemIndex > 1 then
  begin
    T := Posicao;
    GH := TfrxGroupHeader.Create(Page);
    GH.CreateUniqueName;
    GH.SetBounds(0, T, 0, H);
    GH.Condition := 'frxDsnIV1."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"';
    //
    L :=  Round(     0       / Fator);
    //T :=  Round(  1800       / Fator);
    Z :=  Round( 2500 / Fator);
    W :=  Round((18000 + X)  / Fator);
    //H :=  Round(   500       / Fator);
    Memo := TfrxMemoView.Create(GH);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, 0, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Arial';
    Memo.Font.Size   := 10;
    Memo.Memo.Text   := OrdemPor_Tit[RGOrdem2.ItemIndex] + '[frxDsnIV1."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"]';
    Memo.Font.Style  := [fsBold];
(*
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
*)
    //
    Posicao := Trunc(GH.Top + GH.Height + 1);
  end;
}
  // Master data para linhas de dados
  //H :=  Zum(GradeA.DefaultRowHeight);
  T := Posicao;
  Band := TfrxMasterData.Create(Page);
  Band.CreateUniqueName;
  Band.SetBounds(0, T, 0, H);
  Band.DataSet     := frxDsNiv1;
  //
  //Posicao := Trunc(Band.Top + Band.Height + 1);
  // Linha de totais dos valores
  Y := H + Round(200/Fator);
  Summ := TfrxReportSummary.Create(Page);
  Summ.CreateUniqueName;
  Summ.SetBounds(0, 0, 0, Y);
  //
  // Dados do relat�rio
  //H := H;
  L := 0;
  T := 0;
  W := 0;
  H :=  Zum(THackDBGrid(GradeA).DefaultRowHeight);
  //
  for I := 1 to THackDBGrid(GradeA).ColCount -1 do
  begin
    Fld := GradeA.Columns[I-1].FieldName;
    // Dados das colunas
    if THackDBGrid(GradeA).ColWidths[i-1] < 5 then
      Campo := ''
    else begin
      if Uppercase(Copy(Fld, 1, 3)) = Uppercase('Fld') then
        Campo := '[FormatFloat(''#,###,##0.00;-#,###,##0.00; '',<frxDsNiv1."' +
        Fld + '">)]'
      else
        Campo := '[frxDsNiv1."' + Fld + '"]';
    end;
    //
    case I of
      0..4: AlinhaH := haCenter;
      5: AlinhaH := haLeft;
      else AlinhaH := haRight;
    end;
    //
    L :=  L + W;
    W :=  Zum(THackDBGrid(GradeA).ColWidths[i]);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, T, W, H);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    Memo.Memo.Text   := Campo;
    Memo.HAlign      := AlinhaH;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Rotation    := 0;

    // T�tulos das colunas
    Z :=  Round( 2500 / VAR_frCM); // original 2500 / Fator
    M :=  Round( 2500 / Fator);
    Memo := TfrxMemoView.Create(Head);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, Z, W, M);
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    if THackDBGrid(GradeA).ColWidths[i-1] < 5 then
      Memo.Memo.Text   := ''
    else
      Memo.Memo.Text   := GradeA.Columns[I-1].Title.Caption;

    // Exporta��o para PDF correta:
    Memo.HAlign      := haCenter;
    Memo.VAlign      := vaCenter;
    //

    Memo.Frame.Typ   := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Rotation    := 90;
    //
  end;
  //

  AdicionaMemos(Summ, 250, 0, 'TOTAIS: ', TamFix);
  //
{
  // GroupFooter 2
  H :=  Zum(GradeA.DefaultRowHeight);
  if RGAgrup.ItemIndex > 1 then
  begin
    T := Posicao;
    GF := TfrxGroupFooter.Create(Page);
    GF.CreateUniqueName;
    GF.SetBounds(0, T, 0, H);
    //
    L :=  Round(     0       / Fator);
    //T :=  Round(  1800       / Fator);
    Z :=  Round( 2500 / Fator);
    W :=  TamFix;//Round((18000 + X)  / Fator);
    //H :=  Round(   500       / Fator);

    Titulo := 'Total de ' + OrdemPor_Tit[RGOrdem2.ItemIndex] +
      '[frxDsnIV1."' + OrdemPor_frx[RGOrdem2.ItemIndex] + '"]';
    AdicionaMemos(GF, 0, 0, Titulo, TamFix);

    //
    Posicao := Trunc(GF.Top + GF.Height + 1);
  end;

{
  // GroupFooter 1
  if RGAgrup.ItemIndex > 0 then
  begin
    T := Posicao;
    GF := TfrxGroupFooter.Create(Page);
    GF.CreateUniqueName;
    GF.SetBounds(0, T, 0, H);
    //
    L :=  Round(     0       / Fator);
    //T :=  Round(  1800       / Fator);
    Z :=  Round( 2500 / Fator);
    W :=  TamFix;//Round((18000 + X)  / Fator);
    //H :=  Round(   500       / Fator);

    Titulo := 'Total de ' + OrdemPor_Tit[RGOrdem1.ItemIndex] +
      '[frxDsnIV1."' + OrdemPor_frx[RGOrdem1.ItemIndex] + '"]';
    AdicionaMemos(GF, 0, 0, Titulo, TamFix);

    //
    Posicao := Trunc(GF.Top + GF.Height + 1);
  end;
}

  BANLEF := Trunc(PageBanLef / VAR_DOTIMP);
  BANTOP := Trunc(269 / VAR_DOTIMP);
  BANWID := Trunc(PageBanWid / VAR_DOTIMP);
  BANHEI := Trunc(PagePagFot / VAR_DOTIMP);
  Foot := TfrxPageFooter.Create(Page);
  Foot.CreateUniqueName;
  Foot.SetBounds(BANLEF, BANTOP, BANWID, BANHEI);
  //
  ReacertaFrxDataSets();
  if CkDesign.Checked then
    frxSintetico.DesignReport
  else
    MyObjects.frxMostra(frxSintetico, 'Relat�rio de arrecada��es');
end;

procedure TFmOperaLct.FormCreate(Sender: TObject);
{
var
  Cam: String;
  i, f, k, t: Integer;
  Data: TDateTime;
  Ano, Mes, Dia, DiaI, DiaF: Word;
}
begin
  FEnableBtBordero := True;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrCtaFat2Ger, Dmod.MyDB);
  UMyMod.AbreQuery(QrCtaFat0Ger, Dmod.MyDB);
  //
  QrNiv1.Close;
  QrNiv1.Database := DModG.MyPID_DB;
  //
  QrConfere.Close;
  QrConfere.Database := DModG.MyPID_DB;
  //
  TPIni.Date := Date - 90;
  TPFim.Date := Date;
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmOperaLct.AdicionaMemos(const Band: TfrxBand; const T, M: Integer;
  Titulo: String; var TamFix: Integer);
const
  AlinhaH = haRight;
var
  Memo: TfrxMemoView;
  L, W, H, (*Posicao, Z, X,*) Y(*, U*): Integer;
  I(*, C, N*): Integer;
  Fld, Campo(*, Somas*): String;
  //AlinhaH: TfrxHAlign;
  //Somar: Boolean;
  Bordas: set of TfrxFrameType;
begin
  L := 0;
  W := 0;
  H :=  Zum(THackDBGrid(GradeA).DefaultRowHeight);
  //
  for I := 1 to THackDBGrid(GradeA).ColCount -1 do
  begin
    Bordas := [];
    Campo := ' ';
    Fld := GradeA.Columns[I-1].FieldName;
    // Dados das colunas
    if THackDBGrid(GradeA).ColWidths[i-1] >= 5 then
    begin
      if Uppercase(Copy(Fld, 1, 3)) = Uppercase('Fld') then
      begin
        Campo := '[FormatFloat(''#,###,##0.00;-#,###,##0.00; '',SUM(<frxDsNiv1."' +
        Fld + '">))]';
        Bordas := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
      end else
      begin
        if I = 1 then
          Campo := 'TOTAL';
      end;
    end;
    //
    L :=  L + W;
    W :=  Zum(THackDBGrid(GradeA).ColWidths[i]);

    Y := Round(T / VAR_frCM);
    Memo := TfrxMemoView.Create(Band);
    Memo.CreateUniqueName;
    Memo.SetBounds(L, Y, W, H);
    if THackDBGrid(GradeA).ColWidths[i] < 5 then
      Memo.Memo.Text   := ''
    else
      Memo.Memo.Text   := Campo;
    //end;
    Memo.Visible     := True;
    Memo.Font.Name   := 'Univers Light Condensed';
    Memo.Font.Size   := EdTamFonte.ValueVariant;
    Memo.HAlign      := AlinhaH;
    Memo.VAlign      := vaCenter;
    Memo.Frame.Typ   := Bordas;
    Memo.Frame.Width := 0.1;
    Memo.WordWrap    := False;
    Memo.Font.Style  := [fsBold];
  end;
end;

procedure TFmOperaLct.BtBorderoClick(Sender: TObject);
begin
  if FEnableBtBordero then
    FmPrincipal.CriaFormLot(2, 0, Trunc(QrNiv1.FieldByName('FatNum').AsFloat), 0)
  else Geral.MensagemBox(
  'A janela de gerenciamento de border�s n�o pode ser criada porque j� existe!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmOperaLct.BtGeraClick(Sender: TObject);
var
  Temp, Num, Txts,Flds, Ordem: String;
  I: Integer;
begin
  FCliente := EdCliente.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtaFat0, DModG.MyPID_DB, [
  'SELECT FatID, ChkFator ',
  'FROM ctafat0 ',
  'ORDER BY FatID ',
  '']);
  FFatIDs := QrCtaFat0.RecordCount;
  Flds := '';
  SetLength(FArrFatID, FFatIDs);
  SetLength(FArrFator, FFatIDs);
  QrCtaFat0.First;
  while not QrCtaFat0.Eof do
  begin
    FArrFator[QrCtaFat0.RecNo - 1] := QrCtaFat0ChkFator.Value;
    FArrFatID[QrCtaFat0.RecNo - 1] := QrCtaFat0FatID.Value;
    Flds := Flds + 'Fld' + Geral.FFN(QrCtaFat0FatID.Value, 4) +
      '              double(15,2) NOT NULL DEFAULT "0.00"       , ' + #13#10;
    //
    QrCtaFat0.Next;
  end;
  //
  FOLC_1 := '_OLC_1_';
  UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
  'DROP TABLE IF EXISTS ' + FOLC_1 + ';                          ',
  'CREATE TABLE ' + FOLC_1 + ' (                                 ',
  '  FatNum               double(15,0) NOT NULL DEFAULT "0"          , ',
  '  Lote                 int(11)      NOT NULL DEFAULT "0"          , ',
  '  Data                 date         NOT NULL DEFAULT "0000-00-00" , ',
  '  CodCli               int(11)      NOT NULL DEFAULT "0"          , ',
  '  NomCli               varchar(100) NOT NULL                      , ',
  '  Nivel1               int(11)      NOT NULL DEFAULT "0"          , ',
  '  Nivel2               int(11)      NOT NULL DEFAULT "0"          , ',
  Flds,
  '  Ativo                tinyint(1)   NOT NULL DEFAULT "0"            ',
  ') TYPE=MyISAM                                                       ',
  ';']);
  //
  Txts := '';
  PB1.Position := 0;
  PB1.Max := FFatIDs;
  for I := 0 to FFatIDs - 1 do
  begin
    Num := Geral.FFN(FArrFatID[I], 4);
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Somando valores da conta de faturamento ' + Num);
    //
    Txts := Txts + 'SUM(Fld' + Num + ') Fld' + Num;
    if I = FFatIDs - 1 then
      Txts := Txts + ',' + #13#10
    else
      Txts := Txts + #13#10;
    //
    ExecutaSQLFatID(I);
  end;
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtaFat1, Dmod.MyDB, [
  'SELECT Codigo, Sigla, Ordem ',
  'FROM ctafat1 ',
  'ORDER BY Ordem ',
  '']);
  SetLength(FArrNiv1Codi, QrCtaFat1.RecordCount);
  SetLength(FArrNiv1Flds, QrCtaFat1.RecordCount);
  //
  QrCtaFat1.First;
  while not QrCtaFat1.Eof do
  begin
    FArrNiv1Codi[QrCtaFat1.RecNo - 1] := QrCtaFat1Codigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrFlds1, Dmod.MyDB, [
    'SELECT FatID ',
    'FROM ctafat0 ',
    'WHERE FatNiv1=' + Geral.FF0(QrCtaFat1Codigo.Value),
    '']);
    Temp := '';
    QrFlds1.First;
    while not QrFlds1.Eof do
    begin
      Temp := Temp + ' SUM(Fld' + Geral.FFN(QrFlds1FatID.Value, 4) + ') +';
      //
      QrFlds1.Next;
    end;
    //
    if Length(Temp) > 1 then
    begin
      Temp := Copy(Temp, 1, Length(Temp) - 1);
      Temp := Temp + 'Fld' + Geral.FFN(QrCtaFat1Codigo.Value, 4)
      + ', ';
    end;
    FArrNiv1Flds[QrCtaFat1.RecNo -1] := Temp;
    //
    QrCtaFat1.Next;
  end;
  //
  case RGOrdem.ItemIndex of
    0: Ordem := 'ORDER BY NomCli, CodCli, Lote, FatNum';
    1: Ordem := 'ORDER BY FatNum';
    else Ordem := 'ORDER BY ? ? ? ?';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, DModG.MyPID_DB, [
  'SELECT FatNum, Lote, Data, CodCli, NomCli, ',
  Geral.ATS(FArrNiv1Flds),
  'Ativo',
  'FROM ' + FOLC_1,
  'GROUP BY FatNum, Lote, Data, CodCli ',
  Ordem,
  '']);
end;

procedure TFmOperaLct.QrNiv1AfterOpen(DataSet: TDataSet);
var
  I: Integer;
  X, Y, Fld: String;
begin
  for I := 0 to QrNiv1.Fields.Count - 1 do
  begin
    X := LowerCase(QrNiv1.Fields[I].FieldName);
    //
    if X = 'fatnum' then
      Y := 'Lote'
    else
    if X = 'lote' then
      Y := 'Border�'
    else
    if X = 'data' then
    begin
      Y := 'Data';
      SetPropValue(QrNiv1.Fields[I], 'DisplayFormat', 'dd/mm/yy');
    end else
    if X = 'codcli' then
      Y := 'Cliente'
    else
    if X = 'nomcli' then
      Y := 'Nome do cliente'
    else
    if X = 'ativo' then
      //Y := 'Ativo'
      QrNiv1.Fields[I].Visible := False
    else
    if Copy(X, 1, 3) = 'fld' then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, Dmod.MyDB, [
      'SELECT Sigla ',
      'FROM ctafat1 ',
      'WHERE Codigo=' + Copy(X, 4),
      '']);
      //
      Y := QrPsq1.Fields[0].AsString;
      SetPropValue(QrNiv1.Fields[I], 'DisplayFormat',
        '#,###,###,##0.00;-#,###,###,##0.00; ');
      //
      if UpperCase(Y) = 'LIVRE' then
        QrNiv1.Fields[I].Visible := False
    end
    else
      Y := QrNiv1.Fields[I].DisplayLabel;
    //
    QrNiv1.Fields[I].DisplayLabel := Y;
  end;

  //

  for I := 0 to GradeA.Columns.Count - 1 do
  begin
    Fld := Uppercase(GradeA.Columns[I].FieldName);
    if Fld = Uppercase('FatNum') then
      GradeA.Columns[I].Width := 56;
    if Fld = Uppercase('Lote') then
      GradeA.Columns[I].Width := 56;
    if Fld = Uppercase('Data') then
      GradeA.Columns[I].Width := 56;
    if Fld = Uppercase('NomCLi') then
      GradeA.Columns[I].Width := 300;
  end;

  //

  BtImprime1.Enabled := QrNiv1.RecordCount > 0;
  BtImprime2.Enabled := QrNiv1.RecordCount > 0;

end;

procedure TFmOperaLct.QrNiv1BeforeClose(DataSet: TDataSet);
begin
  BtImprime1.Enabled := False;
  BtImprime2.Enabled := False;
end;

procedure TFmOperaLct.RGOrdemClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOperaLct.ReacertaFrxDataSets();
begin
  //
end;

procedure TFmOperaLct.TPFimChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOperaLct.TPFimClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOperaLct.TPIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmOperaLct.TPIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

function TFmOperaLct.Zum(Inteiro: Integer): Integer;
begin
  Result := Trunc(Inteiro * EdPercZoom.ValueVariant / 100);
end;

end.

