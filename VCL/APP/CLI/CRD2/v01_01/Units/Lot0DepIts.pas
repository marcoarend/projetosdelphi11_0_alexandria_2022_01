unit Lot0DepIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkEditDateTimePicker, mySQLDbTables;

type
  TFmLot0DepIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LaCarteira: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCarteira: Integer;
    FDescricao, FData: String;
  end;

  var
  FmLot0DepIts: TFmLot0DepIts;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmLot0DepIts.BtOKClick(Sender: TObject);
begin
  FCarteira  := EdCarteira.ValueVariant;
  FData      := Geral.FDT(TPData.Date, 1);
  FDescricao := EdDescricao.ValueVariant;
  //
  if MyObjects.FIC(FCarteira = 0, EdCarteira, 'Defina a carteira!') then Exit;
  if MyObjects.FIC(TPData.Date = 0, TPData, 'Defina a data!') then Exit;
  //
  Close;
end;

procedure TFmLot0DepIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot0DepIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot0DepIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmLot0DepIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
