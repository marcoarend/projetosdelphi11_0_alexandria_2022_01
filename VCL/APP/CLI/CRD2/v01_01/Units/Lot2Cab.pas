unit Lot2Cab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, frxRich,
  frxClass, frxDBSet, ComCtrls, dmkEditDateTimePicker, Grids, DBGrids, Menus,
  Variants, MyListas, UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, DMKpnfsConversao;

type
  TFmLot2Cab = class(TForm)
    PnDados: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtNovo: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrLot: TmySQLQuery;
    QrLotCodigo: TIntegerField;
    QrLotTipo: TSmallintField;
    QrLotCliente: TIntegerField;
    QrLotLote: TSmallintField;
    QrLotData: TDateField;
    QrLotTotal: TFloatField;
    QrLotDias: TFloatField;
    QrLotPeCompra: TFloatField;
    QrLotTxCompra: TFloatField;
    QrLotAdValorem: TFloatField;
    QrLotIOC: TFloatField;
    QrLotCPMF: TFloatField;
    QrLotLk: TIntegerField;
    QrLotDataCad: TDateField;
    QrLotDataAlt: TDateField;
    QrLotUserCad: TIntegerField;
    QrLotUserAlt: TIntegerField;
    QrLotNOMECLIENTE: TWideStringField;
    QrLotTipoAdV: TIntegerField;
    QrLotADVALOREM_TOTAL: TFloatField;
    QrLotVALVALOREM_TOTAL: TFloatField;
    QrLotValValorem: TFloatField;
    QrLotSpread: TSmallintField;
    QrLotTAXAVALOR_TOTAL: TFloatField;
    QrLotTAXAPERCE_TOTAL: TFloatField;
    QrLotTAXAPERCE_PROPR: TFloatField;
    QrLotIOC_VAL: TFloatField;
    QrLotCPMF_VAL: TFloatField;
    QrLotSUB_TOTAL: TFloatField;
    QrLotVAL_LIQUIDO: TFloatField;
    QrLotTOT_ISS: TFloatField;
    QrLotTOT_IRR: TFloatField;
    QrLotISS: TFloatField;
    QrLotISS_Val: TFloatField;
    QrLotPIS_R: TFloatField;
    QrLotPIS_R_Val: TFloatField;
    QrLotIRRF: TFloatField;
    QrLotIRRF_Val: TFloatField;
    QrLotVAL_LIQUIDO_MEU: TFloatField;
    QrLotSUB_TOTAL_MEU: TFloatField;
    QrLotCNPJCPF: TWideStringField;
    QrLotCNPJ_CPF_TXT: TWideStringField;
    QrLotTAXA_AM_TXT: TWideStringField;
    QrLotADVAL_TXT: TWideStringField;
    QrLotTarifas: TFloatField;
    QrLotOcorP: TFloatField;
    QrLotSUB_TOTAL_2: TFloatField;
    QrLotCOFINS_R: TFloatField;
    QrLotCOFINS_R_Val: TFloatField;
    QrLotPIS: TFloatField;
    QrLotCOFINS: TFloatField;
    QrLotMEU_PISCOFINS_VAL: TFloatField;
    QrLotPIS_Val: TFloatField;
    QrLotCOFINS_Val: TFloatField;
    QrLotMEU_PISCOFINS: TFloatField;
    QrLotMaxVencto: TDateField;
    QrLotSUB_TOTAL_3: TFloatField;
    QrLotCHDevPg: TFloatField;
    QrLotMINTC: TFloatField;
    QrLotMINAV: TFloatField;
    QrLotMINTC_AM: TFloatField;
    QrLotPIS_T_Val: TFloatField;
    QrLotCOFINS_T_Val: TFloatField;
    QrLotPgLiq: TFloatField;
    QrLotA_PG_LIQ: TFloatField;
    QrLotDUDevPg: TFloatField;
    QrLotNOMETIPOLOTE: TWideStringField;
    QrLotNF: TIntegerField;
    QrLotTAXAPERCEMENSAL: TFloatField;
    QrLotItens: TIntegerField;
    QrLotConferido: TIntegerField;
    QrLotECartaSac: TSmallintField;
    QrLotNOMECOFECARTA: TWideStringField;
    QrLotCAPTIONCONFCARTA: TWideStringField;
    QrLotNOMESPREAD: TWideStringField;
    QrLotLimiCred: TFloatField;
    QrLotCBECli: TIntegerField;
    QrLotSCBCli: TIntegerField;
    QrLotCBE: TIntegerField;
    QrLotSCB: TIntegerField;
    QrLotSALDOAPAGAR: TFloatField;
    QrLotQuantN1: TFloatField;
    QrLotQuantI1: TIntegerField;
    QrLotAllQuit: TSmallintField;
    QrLotSobraIni: TFloatField;
    QrLotSobraNow: TFloatField;
    QrLotIOC_VALNEG: TFloatField;
    QrLotCPMF_VALNEG: TFloatField;
    QrLotTAXAVALOR_TOTALNEG: TFloatField;
    QrLotVAVALOREM_TOTALNEG: TFloatField;
    QrLotCHDevPgNEG: TFloatField;
    QrLotDUDevPgNEG: TFloatField;
    QrLotIOFd_VAL: TFloatField;
    QrLotIOFv_VAL: TFloatField;
    QrLotNOMEFANCLIENTE: TWideStringField;
    QrLotWebCod: TIntegerField;
    QrLotAlterWeb: TSmallintField;
    QrLotIOFd: TFloatField;
    QrLotIOFv: TFloatField;
    QrLotTipoIOF: TSmallintField;
    QrLotAtivo: TSmallintField;
    QrLotIts: TmySQLQuery;
    QrLotItsCPF_TXT: TWideStringField;
    QrLotItsStatusSPC_TXT: TWideStringField;
    QrLotTxs: TmySQLQuery;
    QrLotTxsTAXA_STR: TWideStringField;
    QrLotTxsCALCQtd: TFloatField;
    QrLotTxsTaxaValNEG: TFloatField;
    QrCHDevP: TmySQLQuery;
    QrOcorP: TmySQLQuery;
    QrOcorPPAGONEG: TFloatField;
    QrOcorPSALDONEG: TFloatField;
    QrJuros0: TmySQLQuery;
    QrJuros0Praca: TIntegerField;
    QrJuros0Banco: TIntegerField;
    QrJuros0Emitente: TWideStringField;
    QrJuros0TP: TIntegerField;
    QrJuros0Dias: TIntegerField;
    QrJuros0VlrCompra: TFloatField;
    QrJuros0VlrOutras: TFloatField;
    QrJuros0VlrTotal: TFloatField;
    QrJuros0CPF_TXT: TWideStringField;
    QrJuros0ITEM: TIntegerField;
    QrJuros0DDeposito: TDateField;
    DsLot: TDataSource;
    DsLotIts: TDataSource;
    DsLotTxs: TDataSource;
    DsCHDevP: TDataSource;
    DsOcorP: TDataSource;
    frxDsLot: TfrxDBDataset;
    frxDsLotIts: TfrxDBDataset;
    frxDsLotTxs: TfrxDBDataset;
    frxDsCHDevP: TfrxDBDataset;
    frxDsOcorP: TfrxDBDataset;
    frxDsJuros0: TfrxDBDataset;
    frxDsSPC_Result: TfrxDBDataset;
    frxDsBordero: TfrxDBDataset;
    frxDsOcorOP: TfrxDBDataset;
    frxDsDPago: TfrxDBDataset;
    frxDsAditiv2: TfrxDBDataset;
    frxDsContrat: TfrxDBDataset;
    frxDsPagtos: TfrxDBDataset;
    frxDsSaCart: TfrxDBDataset;
    frxDsNF: TfrxDBDataset;
    QrSumOP: TmySQLQuery;
    QrSumOPOcorrencias: TLargeintField;
    QrSumOPPago: TFloatField;
    QrSumOPPagoNeg: TFloatField;
    QrDPago: TmySQLQuery;
    QrDPagoNOMESTATUS: TWideStringField;
    QrDPagoCPF_TXT: TWideStringField;
    QrContrat: TmySQLQuery;
    QrContratContrato: TIntegerField;
    QrContratDataC: TDateField;
    QrContratLimite: TFloatField;
    QrContratNOME11: TWideStringField;
    QrContratNOME12: TWideStringField;
    QrContratNOME21: TWideStringField;
    QrContratNOME22: TWideStringField;
    QrPagtos: TmySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosSub: TSmallintField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosDebito: TFloatField;
    QrPagtosCredito: TFloatField;
    QrPagtosCompensado: TDateField;
    QrPagtosDocumento: TFloatField;
    QrPagtosSit: TIntegerField;
    QrPagtosVencimento: TDateField;
    QrPagtosFatID: TIntegerField;
    QrPagtosFatID_Sub: TIntegerField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosBanco: TIntegerField;
    QrPagtosCliente: TIntegerField;
    QrPagtosDataDoc: TDateField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrPagtosCNPJCPF: TWideStringField;
    QrPagtosEmitente: TWideStringField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosNOMEFORNECEI: TWideStringField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    QrPagtosSEQ: TIntegerField;
    QrPagtosNome: TWideStringField;
    QrPagtosCPF: TWideStringField;
    QrPagtosCPF_TXT: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosNOMETIPODOC: TWideStringField;
    QrPagtosAgencia: TIntegerField;
    QrPagtosControle: TIntegerField;
    QrPagtosFatNum: TFloatField;
    QrSaCart: TmySQLQuery;
    QrSaCartNUMERO_TXT: TWideStringField;
    QrSaCartLNR: TWideStringField;
    QrSaCartLN2: TWideStringField;
    QrSaCartCUC: TWideStringField;
    QrSaCartCEP_TXT: TWideStringField;
    DsDPago: TDataSource;
    DsPagtos: TDataSource;
    frxJuros0: TfrxReport;
    frxBorProFis0_A: TfrxReport;
    frxBorTotCliE2: TfrxReport;
    frxBorTotCliBP2: TfrxReport;
    frxRichObject1: TfrxRichObject;
    frxBorTotCli2: TfrxReport;
    frxAditivo_AP_NP: TfrxReport;
    frxBorProFis1_A: TfrxReport;
    frxBorTotCliE3: TfrxReport;
    frxBorTotCliBP3: TfrxReport;
    Timer1: TTimer;
    frxBorTotCli3: TfrxReport;
    frxBorTotCli2S: TfrxReport;
    frxCartaSacadoTudo: TfrxReport;
    frxBorTotCliE0: TfrxReport;
    frxBorTotCliBP0: TfrxReport;
    frxBorTotCli0: TfrxReport;
    frxBorTotCli3S: TfrxReport;
    frxCartaSacadoVersoTudo: TfrxReport;
    frxBorTotCliE1: TfrxReport;
    frxBorTotCliBP1: TfrxReport;
    frxBorTotCli1: TfrxReport;
    frxBorTotCli0S: TfrxReport;
    frxProtesto: TfrxReport;
    frxColigadas0: TfrxReport;
    frxBorTotCli1S: TfrxReport;
    frxCartaSacadoDados: TfrxReport;
    frxColigadas1: TfrxReport;
    frxAditivo_NP: TfrxReport;
    frxAditivo: TfrxReport;
    frxSPC_Result: TfrxReport;
    frxPromissoria: TfrxReport;
    frxCartaSacadoVersoDados: TfrxReport;
    frxAditivo_NP_AP: TfrxReport;
    frxNF_Dados: TfrxReport;
    frxReport1: TfrxReport;
    frxNF_Tudo: TfrxReport;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label21: TLabel;
    Label3: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label74: TLabel;
    Label90: TLabel;
    Label110: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label134: TLabel;
    DBText1: TDBText;
    Label155: TLabel;
    DBLaCPMF: TLabel;
    GroupBox1: TGroupBox;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    Label151: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdCodigo: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    EdDBValLiq: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit32: TDBEdit;
    EdDBAPGLIQ: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdNF: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdCPMF: TDBEdit;
    PgPrincipal: TPageControl;
    TabSheet7: TTabSheet;
    Splitter3: TSplitter;
    Panel51: TPanel;
    DBGrid25: TDBGrid;
    StCPF_LI: TStaticText;
    Panel52: TPanel;
    Panel6: TPanel;
    BtInclui: TBitBtn;
    BtAltera2: TBitBtn;
    BtExclui2: TBitBtn;
    BtImportaBCO: TBitBtn;
    BitBtn7: TBitBtn;
    BtWeb: TBitBtn;
    BtSPC_LI: TBitBtn;
    GradeCHE: TDBGrid;
    GradeDUP: TDBGrid;
    TabSheet8: TTabSheet;
    DBGrid7: TDBGrid;
    Panel13: TPanel;
    BtInclui5: TBitBtn;
    BtAltera5: TBitBtn;
    BtExclui5: TBitBtn;
    TabSheet6: TTabSheet;
    PageControl3: TPageControl;
    TabSheet9: TTabSheet;
    Panel23: TPanel;
    DBGrid5: TDBGrid;
    Panel26: TPanel;
    BtPagamento6: TBitBtn;
    BtIncluiocor: TBitBtn;
    TabSheet10: TTabSheet;
    Panel19: TPanel;
    BtPagtosExclui6: TBitBtn;
    DBGrid6: TDBGrid;
    TabSheet11: TTabSheet;
    PageControl4: TPageControl;
    TabSheet12: TTabSheet;
    Panel31: TPanel;
    Panel32: TPanel;
    BtPagamento7: TBitBtn;
    DBGrid10: TDBGrid;
    TabSheet13: TTabSheet;
    Panel33: TPanel;
    BtExclui7: TBitBtn;
    DBGrid11: TDBGrid;
    TabSheet17: TTabSheet;
    DBGrid14: TDBGrid;
    TabSheet14: TTabSheet;
    PageControl5: TPageControl;
    TabSheet16: TTabSheet;
    Panel35: TPanel;
    BtPagtosInclui9: TBitBtn;
    DBGrid12: TDBGrid;
    TabSheet18: TTabSheet;
    DBGrid13: TDBGrid;
    Panel36: TPanel;
    BtPagtosExclui9: TBitBtn;
    TabSheet15: TTabSheet;
    Panel34: TPanel;
    BtPagtosInclui8: TBitBtn;
    BtPagtosAltera8: TBitBtn;
    BtPagtosExclui8: TBitBtn;
    BtCheque: TBitBtn;
    GridPagtos: TDBGrid;
    TabSheet29: TTabSheet;
    Panel46: TPanel;
    BtPagTerc: TBitBtn;
    BtExclui8: TBitBtn;
    BtSobra: TBitBtn;
    DBG1: TDBGrid;
    TabSheet30: TTabSheet;
    Memo1: TMemo;
    Panel47: TPanel;
    BitBtn5: TBitBtn;
    TabSheet31: TTabSheet;
    Panel48: TPanel;
    Label205: TLabel;
    Label206: TLabel;
    DBEdit13: TDBEdit;
    DBEdit84: TDBEdit;
    PMBordero: TPopupMenu;
    Total1: TMenuItem;
    Novo2: TMenuItem;
    Sobras1: TMenuItem;
    NoMostrarDias1: TMenuItem;
    MostrarDias1: TMenuItem;
    FormatoBP1: TMenuItem;
    Contabilidade1: TMenuItem;
    AditivoNPAP1: TMenuItem;
    AditivoAPNP1: TMenuItem;
    AditivoNP1: TMenuItem;
    Aditivo1: TMenuItem;
    Promissria1: TMenuItem;
    AutorizaodeProtesto1: TMenuItem;
    CartaaoSacado1: TMenuItem;
    Dasos1: TMenuItem;
    Tudo1: TMenuItem;
    N5: TMenuItem;
    Enviaaocliente1: TMenuItem;
    N3: TMenuItem;
    Coligadas1: TMenuItem;
    MargemtOtal1: TMenuItem;
    N4: TMenuItem;
    NotaFiscal1: TMenuItem;
    Preencher1: TMenuItem;
    Semomeuendereo1: TMenuItem;
    Comomeuendereo1: TMenuItem;
    TudoFolhaembranco1: TMenuItem;
    N9: TMenuItem;
    ResultadodaconsultaaoSPC1: TMenuItem;
    PMNovo: TPopupMenu;
    BorderauxdeCheques1: TMenuItem;
    BorderauxdeDuplicatas1: TMenuItem;
    N2: TMenuItem;
    PMAltera: TPopupMenu;
    Cabealhodoborderatual1: TMenuItem;
    N1: TMenuItem;
    Todositensaomesmotempo1: TMenuItem;
    Diversositens1: TMenuItem;
    Carteiradedepsito1: TMenuItem;
    PMAltera2: TPopupMenu;
    Alteraototaldodocumentoatual1: TMenuItem;
    Alteraodadatadodepsito1: TMenuItem;
    PMSPC_LotIts: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    PMIncluiOcor: TPopupMenu;
    Cliente1: TMenuItem;
    Cheque1: TMenuItem;
    Duplicata1: TMenuItem;
    PMPagamento7: TPopupMenu;
    Detalhado1: TMenuItem;
    Prconfigurado1: TMenuItem;
    PMCheque: TPopupMenu;
    Nominal1: TMenuItem;
    Portador1: TMenuItem;
    PMPagto: TPopupMenu;
    Novo1: TMenuItem;
    Rpido1: TMenuItem;
    PMExcluiPagto: TPopupMenu;
    Pagamento1: TMenuItem;
    Todospagamentosdestebordero1: TMenuItem;
    PMPagTerc: TPopupMenu;
    Multiplaseleo1: TMenuItem;
    Leitora1: TMenuItem;
    LaAviso3: TLabel;
    LaAviso4: TLabel;
    SbImprime: TBitBtn;
    BtCheckImp: TBitBtn;
    BtRefresh: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    BtSinaleira: TBitBtn;
    BtRiscoSac: TBitBtn;
    BtHistorico: TBitBtn;
    GroupBox6: TGroupBox;
    BtOcorencias: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn6: TBitBtn;
    PMHistorico: TPopupMenu;
    Clienteselecionado1: TMenuItem;
    Emitentesacadoselecionado1: TMenuItem;
    Ambos1: TMenuItem;
    PMRefresh: TPopupMenu;
    EsteBorder1: TMenuItem;
    DesteBorderemdiante1: TMenuItem;
    QrLotItsFatParcela: TIntegerField;
    QrLotItsComp: TIntegerField;
    QrLotItsBanco: TIntegerField;
    QrLotItsContaCorrente: TWideStringField;
    QrLotItsDocumento: TFloatField;
    QrLotItsEmitente: TWideStringField;
    QrLotItsCredito: TFloatField;
    QrLotItsVencimento: TDateField;
    QrLotItsDDeposito: TDateField;
    QrLotItsTxaCompra: TFloatField;
    QrLotItsTxaAdValorem: TFloatField;
    QrLotItsVlrCompra: TFloatField;
    QrLotItsVlrAdValorem: TFloatField;
    QrLotItsDMais: TIntegerField;
    QrLotItsDias: TIntegerField;
    QrLotItsCNPJCPF: TWideStringField;
    QrLotItsTxaJuros: TFloatField;
    QrLotItsDCompra: TDateField;
    QrLotItsDuplicata: TWideStringField;
    QrLotItsData: TDateField;
    QrLotItsDevolucao: TIntegerField;
    QrLotItsDesco: TFloatField;
    QrLotItsQuitado: TIntegerField;
    QrLotItsBruto: TFloatField;
    QrLotItsPraca: TIntegerField;
    QrLotItsBcoCobra: TIntegerField;
    QrLotItsAgeCobra: TIntegerField;
    QrLotItsCartDep: TIntegerField;
    QrLotItsDescAte: TDateField;
    QrLotItsTipific: TSmallintField;
    QrLotItsStatusSPC: TSmallintField;
    QrLotItsFatNum: TFloatField;
    QrOcorPDataO: TDateField;
    QrOcorPOcorrencia: TIntegerField;
    QrOcorPValor: TFloatField;
    QrOcorPDescri: TWideStringField;
    QrOcorPSALDO: TFloatField;
    QrOcorPNOMEOCORRENCIA: TWideStringField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPBanco: TIntegerField;
    QrOcorPContaCorrente: TWideStringField;
    QrOcorPDocumento: TFloatField;
    QrOcorPDuplicata: TWideStringField;
    QrOcorPEmitente: TWideStringField;
    QrOcorPCNPJCPF: TWideStringField;
    QrOcorPTipo: TSmallintField;
    QrOcorPTIPODOC: TWideStringField;
    QrOcorPDOCUM_TXT: TWideStringField;
    QrOcorPCPF_TXT: TWideStringField;
    QrSaCartFatNum: TFloatField;
    QrSaCartNumero: TFloatField;
    QrSaCartCNPJ: TWideStringField;
    QrSaCartIE: TWideStringField;
    QrSaCartNome: TWideStringField;
    QrSaCartRua: TWideStringField;
    QrSaCartCompl: TWideStringField;
    QrSaCartBairro: TWideStringField;
    QrSaCartCidade: TWideStringField;
    QrSaCartUF: TWideStringField;
    QrSaCartCEP: TIntegerField;
    QrSaCartTel1: TWideStringField;
    QrSaCartRisco: TFloatField;
    QrJuros0Vencimento: TDateField;
    QrJuros0ContaCorrente: TWideStringField;
    QrJuros0Documento: TFloatField;
    QrJuros0CNPJCPF: TWideStringField;
    QrJuros0Credito: TFloatField;
    QrJuros0FatParcela: TIntegerField;
    QrLotItsTipo: TSmallintField;
    QrLotItsCarteira: TIntegerField;
    QrLotItsControle: TIntegerField;
    QrLotItsSub: TSmallintField;
    BitBtn1: TBitBtn;
    QrLotItsAgencia: TIntegerField;
    QrOcorPAgencia: TIntegerField;
    QrJuros0Agencia: TIntegerField;
    QrPagtosAgencia_1: TIntegerField;
    N6: TMenuItem;
    Emitecheque1: TMenuItem;
    QrPagtosSerieCH: TWideStringField;
    QrLotTxsFatNum: TFloatField;
    QrLotTxsFatParcela: TIntegerField;
    QrLotTxsFatID_Sub: TIntegerField;
    QrLotTxsNome: TWideStringField;
    QrLotTxsMoraVal: TFloatField;
    QrLotTxsMoraDia: TFloatField;
    QrLotTxsQtde: TFloatField;
    QrLotTxsTipific: TSmallintField;
    QrLotTxsCredito: TFloatField;
    QrLotTxsMoraTxa: TFloatField;
    QrLotTxsControle: TIntegerField;
    QrLotTxsData: TDateField;
    QrOcorPFatParcela: TIntegerField;
    QrOcorPMoraVal: TFloatField;
    QrOcorPPAGO: TFloatField;
    QrOcorPFatNum: TFloatField;
    QrOcorPControle: TIntegerField;
    QrDPagoEmitente: TWideStringField;
    QrDPagoCNPJCPF: TWideStringField;
    QrDPagoCredito: TFloatField;
    QrDPagoDuplicata: TWideStringField;
    QrDPagoQuitado: TIntegerField;
    QrDPagoSALDO: TFloatField;
    QrDPagoFatParcRef: TIntegerField;
    QrDPagoData: TDateField;
    QrDPagoFatParcela: TIntegerField;
    QrDPagoFatNum: TFloatField;
    QrDPagoMoraVal: TFloatField;
    QrDPagoControle: TIntegerField;
    QrDPagoOcorreu: TIntegerField;
    QrLotLctsAuto: TSmallintField;
    GroupBox2: TGroupBox;
    LaTitulo2A: TLabel;
    LaTitulo2B: TLabel;
    LaTitulo2C: TLabel;
    BtEncerra: TBitBtn;
    QrLotTxsVALOR: TFloatField;
    QrPagtosGenero: TIntegerField;
    QrPagtosCliInt: TIntegerField;
    BitBtn8: TBitBtn;
    BtFinancas: TBitBtn;
    BtRecalculaCHs: TBitBtn;
    QrOcorPTipoIns: TIntegerField;
    QrOcorPLOIS: TIntegerField;
    QrOcorPDataO2: TDateField;
    QrDPagoFatGrupo: TIntegerField;
    QrDPagoDescoVal: TFloatField;
    QrDPagoPago: TFloatField;
    QrCHDevPPAGOU: TFloatField;
    QrCHDevPCredito: TFloatField;
    QrCHDevPMoraVal: TFloatField;
    QrCHDevPDescoVal: TFloatField;
    QrCHDevPMultaVal: TFloatField;
    QrCHDevPTaxasVal: TFloatField;
    QrCHDevPVencimento: TDateField;
    QrCHDevPData: TDateField;
    QrCHDevPFatNum: TFloatField;
    QrCHDevPFatParcela: TIntegerField;
    QrCHDevPControle: TIntegerField;
    QrCHDevPOcorreu: TIntegerField;
    QrCHDevPFatParcRef: TIntegerField;
    QrCHDevPFatGrupo: TIntegerField;
    QrCHDevPCPF_TXT: TWideStringField;
    QrCHDevPValor: TFloatField;
    QrCHDevPTaxas: TFloatField;
    QrCHDevPMulta: TFloatField;
    QrCHDevPJurosV: TFloatField;
    QrCHDevPValPago: TFloatField;
    QrCHDevPDesconto: TFloatField;
    QrCHDevPPgDesc: TFloatField;
    QrCHDevPStatus: TSmallintField;
    QrCHDevPSALDO: TFloatField;
    QrCHDevPNOMESTATUS: TWideStringField;
    QrCHDevPBanco: TIntegerField;
    QrCHDevPAgencia: TIntegerField;
    QrCHDevPConta: TWideStringField;
    QrCHDevPCheque: TIntegerField;
    QrCHDevPCPF: TWideStringField;
    QrCHDevPEmitente: TWideStringField;
    frxCartaSacadoTudo2: TfrxReport;
    frxCartaEmitCH: TfrxReport;
    QrSaEmitCH: TmySQLQuery;
    QrSaEmitCHEmitente: TWideStringField;
    QrSaEmitCHCPF: TWideStringField;
    frxDsSaEmitCH: TfrxDBDataset;
    CartaaoEmitentedoCheque1: TMenuItem;
    ModeloA1: TMenuItem;
    ModeloB1: TMenuItem;
    QrSaEmitCHCodigo: TFloatField;
    QrLotSUB_TOTAL_SCOL: TFloatField;
    QrLotSUB_TOTAL_2_SCOL: TFloatField;
    QrLotSUB_TOTAL_3_SCOL: TFloatField;
    QrLotVAL_LIQUIDO_SCOL: TFloatField;
    QrLotSALDOAPAGAR_SCOL: TFloatField;
    QrLotItsFatID: TIntegerField;
    BitBtn9: TBitBtn;
    QrContratTestem1Nome: TWideStringField;
    QrContratTestem2Nome: TWideStringField;
    QrContratNOME13: TWideStringField;
    QrContratNOME14: TWideStringField;
    QrContratFiador11: TIntegerField;
    QrContratFiador12: TIntegerField;
    QrContratFiador13: TIntegerField;
    QrContratFiador14: TIntegerField;
    QrContratFiador21: TIntegerField;
    QrContratFiador22: TIntegerField;
    QrFiadores: TmySQLQuery;
    frxDsFiadores: TfrxDBDataset;
    QrFiadoresNome: TWideStringField;
    QrFiadoresConjugeNome: TWideStringField;
    QrTestemunhas: TmySQLQuery;
    QrTestemunhasTipo: TIntegerField;
    QrTestemunhasNome: TWideStringField;
    QrTestemunhasCNPJ: TWideStringField;
    frxDsTestemunhas: TfrxDBDataset;
    QrPagtosNOMECLIENTE: TWideStringField;
    BorderCliente1: TMenuItem;
    NFSeMaring1: TMenuItem;
    BtNFSe: TBitBtn;
    QrLotItsJuridico: TSmallintField;
    N7: TMenuItem;
    RemessaCNAB1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrLotAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLotBeforeOpen(DataSet: TDataSet);
    procedure BtNovoClick(Sender: TObject);
    procedure frxAditivo_NPGetValue(const VarName: string; var Value: Variant);
    procedure frxBorTotCli2GetValue(const VarName: string; var Value: Variant);
    function frxBorTotCli2UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure frxNF_TudoGetValue(const VarName: string; var Value: Variant);
    procedure frxCartaSacadoVersoTudoGetValue(const VarName: string;
      var Value: Variant);
    procedure frxCartaSacadoDadosGetValue(const VarName: string;
      var Value: Variant);
    procedure QrLotAfterScroll(DataSet: TDataSet);
    procedure QrLotBeforeClose(DataSet: TDataSet);
    procedure QrLotCalcFields(DataSet: TDataSet);
    procedure QrLotItsAfterOpen(DataSet: TDataSet);
    procedure QrLotItsAfterScroll(DataSet: TDataSet);
    procedure QrLotItsCalcFields(DataSet: TDataSet);
    procedure QrLotTxsCalcFields(DataSet: TDataSet);
    procedure QrCHDevPAfterOpen(DataSet: TDataSet);
    procedure QrCHDevPCalcFields(DataSet: TDataSet);
    procedure QrOcorPAfterOpen(DataSet: TDataSet);
    procedure QrOcorPCalcFields(DataSet: TDataSet);
    procedure QrJuros0CalcFields(DataSet: TDataSet);
    procedure QrSumOPCalcFields(DataSet: TDataSet);
    procedure QrDPagoAfterOpen(DataSet: TDataSet);
    procedure QrDPagoBeforeClose(DataSet: TDataSet);
    procedure QrDPagoCalcFields(DataSet: TDataSet);
    procedure QrPagtosAfterOpen(DataSet: TDataSet);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure QrSaCartAfterScroll(DataSet: TDataSet);
    procedure QrSaCartCalcFields(DataSet: TDataSet);
    procedure EdCRChange(Sender: TObject);
    procedure EdDRChange(Sender: TObject);
    procedure EdRTChange(Sender: TObject);
    procedure EdOAChange(Sender: TObject);
    procedure EdCVChange(Sender: TObject);
    procedure EdDVChange(Sender: TObject);
    procedure EdVTChange(Sender: TObject);
    procedure EdCTChange(Sender: TObject);
    procedure EdDTChange(Sender: TObject);
    procedure EdSTChange(Sender: TObject);
    procedure EdTTChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Novo2Click(Sender: TObject);
    procedure NoMostrarDias1Click(Sender: TObject);
    procedure MostrarDias1Click(Sender: TObject);
    procedure FormatoBP1Click(Sender: TObject);
    procedure Contabilidade1Click(Sender: TObject);
    procedure AditivoNPAP1Click(Sender: TObject);
    procedure AditivoAPNP1Click(Sender: TObject);
    procedure AditivoNP1Click(Sender: TObject);
    procedure Aditivo1Click(Sender: TObject);
    procedure Promissria1Click(Sender: TObject);
    procedure AutorizaodeProtesto1Click(Sender: TObject);
    procedure Dasos1Click(Sender: TObject);
    procedure Enviaaocliente1Click(Sender: TObject);
    procedure Coligadas1Click(Sender: TObject);
    procedure MargemtOtal1Click(Sender: TObject);
    procedure Semomeuendereo1Click(Sender: TObject);
    procedure Comomeuendereo1Click(Sender: TObject);
    procedure TudoFolhaembranco1Click(Sender: TObject);
    procedure ResultadodaconsultaaoSPC1Click(Sender: TObject);
    procedure BtCheckImpClick(Sender: TObject);
    procedure BorderauxdeCheques1Click(Sender: TObject);
    procedure BorderauxdeDuplicatas1Click(Sender: TObject);
    procedure BtFTPClick(Sender: TObject);
    procedure BtImportaBCOClick(Sender: TObject);
    procedure Cabealhodoborderatual1Click(Sender: TObject);
    procedure Diversositens1Click(Sender: TObject);
    procedure Carteiradedepsito1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrLotItsBeforeClose(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAltera2Click(Sender: TObject);
    procedure Alteraototaldodocumentoatual1Click(Sender: TObject);
    procedure Alteraodadatadodepsito1Click(Sender: TObject);
    procedure BtExclui2Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BtSPC_LIClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure BtWebClick(Sender: TObject);
    procedure BtInclui5Click(Sender: TObject);
    procedure BtAltera5Click(Sender: TObject);
    procedure BtExclui5Click(Sender: TObject);
    procedure BtIncluiocorClick(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure Cheque1Click(Sender: TObject);
    procedure Duplicata1Click(Sender: TObject);
    procedure BtPagamento6Click(Sender: TObject);
    procedure Detalhado1Click(Sender: TObject);
    procedure Prconfigurado1Click(Sender: TObject);
    procedure BtPagamento7Click(Sender: TObject);
    procedure BtExclui7Click(Sender: TObject);
    procedure BtPagtosInclui9Click(Sender: TObject);
    procedure BtPagtosExclui9Click(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure Nominal1Click(Sender: TObject);
    procedure Portador1Click(Sender: TObject);
    procedure BtPagtosInclui8Click(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure Rpido1Click(Sender: TObject);
    procedure BtPagtosExclui8Click(Sender: TObject);
    procedure Pagamento1Click(Sender: TObject);
    procedure Todospagamentosdestebordero1Click(Sender: TObject);
    procedure BtPagTercClick(Sender: TObject);
    procedure Multiplaseleo1Click(Sender: TObject);
    procedure Leitora1Click(Sender: TObject);
    procedure BtExclui8Click(Sender: TObject);
    procedure BtSobraClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtSinaleiraClick(Sender: TObject);
    procedure BtRiscoSacClick(Sender: TObject);
    procedure BtHistoricoClick(Sender: TObject);
    procedure BtOcorenciasClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Clienteselecionado1Click(Sender: TObject);
    procedure Emitentesacadoselecionado1Click(Sender: TObject);
    procedure Ambos1Click(Sender: TObject);
    procedure EsteBorder1Click(Sender: TObject);
    procedure DesteBorderemdiante1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure PMRefreshPopup(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Emitecheque1Click(Sender: TObject);
    procedure BtPagtosExclui6Click(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtPagtosAltera8Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure QrPagtosBeforeClose(DataSet: TDataSet);
    procedure BtFinancasClick(Sender: TObject);
    procedure BtRecalculaCHsClick(Sender: TObject);
    procedure QrSaEmitCHAfterOpen(DataSet: TDataSet);
    procedure QrSaEmitCHAfterScroll(DataSet: TDataSet);
    procedure CartaaoEmitentedoCheque1Click(Sender: TObject);
    procedure ModeloA1Click(Sender: TObject);
    procedure ModeloB1Click(Sender: TObject);
    procedure PMBorderoPopup(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BorderCliente1Click(Sender: TObject);
    procedure QrLotTxsAfterOpen(DataSet: TDataSet);
    procedure QrLotTxsBeforeClose(DataSet: TDataSet);
    procedure QrOcorPBeforeClose(DataSet: TDataSet);
    procedure QrCHDevPBeforeClose(DataSet: TDataSet);
    procedure NFSeMaring1Click(Sender: TObject);
    procedure BtNFSeClick(Sender: TObject);
    procedure RemessaCNAB1Click(Sender: TObject);
  private
    FLogo1Path, FTbFiadores, FTbTestemunha: String;

    //FMostra, FCasasAdValorem: Integer;
    FDadosJurosColigadas: MyArray07ArrayR03;
    FContaST, FOrelha,
    FOcorA, FDPago: Integer;
    FEscapeEnabed: Boolean;
    FTempo, FUltim: TDateTime;
    FLogo1Exists,
    FMEU_ENDERECO, FImprimindoColigadas: Boolean;
    //
    procedure ReopenFiadores();
    procedure ReopenTestemunhas();
    procedure AtualizaImpressaoDeCarta(Lote, Tipo: Integer);
    procedure CalculaPagtoLotIts(LOIS: Integer);
    procedure CalculaValoresCH_DU_Refresh(Controle, Dias: Integer;
              TxaCompra, Valor: Double);

    function DefineDataSetsNF(frx: TfrxReport): Boolean;
    procedure ExecutaListaDeImpressoes();

    function GetTitle(TipoLote: Integer): String;

    procedure ExcluiChequeSelecionado();
    procedure ExcluiItemPagamento();
    procedure ExcluiTodasParcelas();
    procedure ImprimeAditivoNPNovo();
    procedure ImprimeAditivoAPNPNovo();
    procedure ImprimeAditivoNPAPNovo();
    procedure ImprimeAditivoNovo();
    procedure ImprimeAutorizacaodeProtestoNovo();
    procedure ImprimeBorderoClienteBP();
    procedure ImprimeBorderoClienteE2();
    procedure ImprimeBorderoClienteSobra(Dias: Boolean);
    procedure ImprimeBorderoContabilNovo();
    procedure ImprimeCartaAoSacadoDados();
    procedure ImprimeCartaAoSacadoTudoNovo(Modelo: Integer);
    procedure ImprimeMargemColigadasNovo();
    procedure ImprimeNF(MeuEndereco: Boolean);
    procedure ImprimeNotaPromissoriaNovo();
    procedure IncluiPagamento();
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    procedure MostraHistorico(Quem: Integer);
    procedure PagaChequeDevolvido();
    procedure RefreshCheque();
    procedure RefreshDuplicata();
    procedure RefreshEmBorderoCH(Lote: Integer; Unico: Boolean);
    procedure RefreshEmBorderoDU(Lote: Integer; Unico: Boolean);
    function  ReopenContrat(): Boolean;
    procedure ReopenDOpen(Cliente: Integer);
    procedure ReopenDPago();
    procedure ReopenLI(Lote: Integer);
    procedure ReopenIts();
    procedure ReopenRepCli();
    procedure ReopenRiscoC(Cliente: Integer);
    function  ReopenSaCart(SelType: TSelType): Boolean;
    procedure ReopenTxs();
    procedure VerificaCPFCNP_XX_LOIS(SelType: TSelType);

    // Procedures do form
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure DefParams;
    procedure MostraLot2Chq(SQLType: TSQLType);
    procedure MostraLot2Dup(SQLType: TSQLType);
    procedure MostraLot2Txa(SQLType: TSQLType);
    procedure Va(Para: TVaiPara);
    procedure ReopenDupSac(FatNum: Double; CNPJCPF, TabLct: String);
    function  LiberaStatusJuridico(Juridico: Integer): Boolean;
  public
    { Public declarations }
    FThisFatID_302, FThisFatID_301: String;
    FCliInt, FEntidade: Integer;
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtEncer, FDtMorto: TDateTime;
    //
    FTaxa, FJuro, FValr: array[0..6] of Double;
    {
    FLoteMSN,
    }
    FCHDevA, FCHDevP, FOcorP, FControlTxs, FConferido, FRelatorios, FImprime,
    FTipoLote: Integer;
    FMudouSaldo, FIncluindoCH, FIncluindoDU: Boolean;
    //
    FArrayMySQLEE: array[0..5] of TmySQLQuery;
    FArrayMySQLSI: array[0..5] of TmySQLQuery;
    FArrayMySQLLO: array[0..5] of TmySQLQuery;
    //
    procedure AtualizaPagamento(Lote, Cliente: Integer; Data: TDateTime;
              Liquido: Double);
    procedure CalculaLote(Lote: Double; AvisaLoteAntigo: Boolean);
    procedure CalculaPagtoOcorP(Quitacao: TDateTime; OcorP: Integer);
    procedure DefineCompraEmpresas(FatNum: Double; FatParcela: Integer);
    procedure DefineRiscos(Cliente: Integer);
    function  GetTipoBorderoAtual(): TLoteBordero;
    procedure IncluiLoteCab(SQLType: TSQLType; TipoBordero: TLoteBordero;
              AlteraTodos, Importando: Boolean;
              Cliente: Integer = 0; Data: TDateTime = 0);
    procedure LocCod(Atual, Codigo: Integer);
    procedure RecalculaBordero(Lote: Integer);
    procedure ReopenCHDevA(Cliente: Integer);
    procedure ReopenCHDevP();
    procedure ReopenOcorP();
    procedure ReopenPagtos();
    procedure ReopenSumOP(FatNum: Double);
{
    function  SQL_CH(Controle, Comp, Banco, Agencia, Cheque, DMais, Dias,
              Codigo, Praca, Tipific, StatusSPC: Integer; Conta, CPF, Emitente,
              Vencto, DCompra, DDeposito: String; Valor: Double; SQLType:
              TSQLType; Cliente: Integer): Integer;
}
    function  SQL_CH(var FatParcela: Integer; const Comp, Banco, Agencia:
              Integer; Documento: Double; DMais, Dias: Integer; FatNum:
              Double; Praca, Tipific, StatusSPC: Integer; ContaCorrente, CNPJCPF,
              Emitente, DataDoc, Vencimento, DCompra, DDeposito: String;
              Credito: Double; SQLType: TSQLType; Cliente: Integer;
              var TipoCart, Carteira, Controle, Sub: Integer): Integer;
{
    function SQL_DU(Codigo, Controle, DMais, Dias, Banco, Agencia: Integer;
             Valor, Desco, Bruto: Double; Duplic, CPF, Sacado, Vencto, DCompra,
             DDeposito, Emissao, DescAte: String; SQLType: TSQLType; Cliente,
             CartDep: Integer): Integer;
}
    function SQL_DU(const FatNum: Double; var FatParcela: Integer;
             const DMais, Dias, Banco, Agencia: Integer;
             const Credito, Desco, Bruto: Double;
             const Duplicata, CNPJCPF, Emitente, Vencimento, DCompra,
             DDeposito, Emissao, DescAte: String;
             SQLType: TSQLType;
             const Cliente, CartDep: Integer;
             var TipoCart, Carteira, Controle, Sub: Integer): Integer;
    function ReopenEmitCH(SelType: TSelType): Boolean;
  end;

var
  FmLot2Cab: TFmLot2Cab;
const
  FFormatFloat = '00000';

implementation

uses ArquivosSCX, Module, UnMyObjects, Principal, ModuleLot, ModuleGeral,
  QuaisItens, MyDBCheck, Lot0Dlg, Lot2Cad, Taxas, ModuleLot2, GetData,
  Lot0CartDep, Lot2Chq, Lot2Dup, SPC_Pesq, UnitSPC, Lot2Web, Lot2Txa,
  GerDup1Main, GerChqMain, GerCliMain, Lot2Oco, Lot2PCD, Lot0Pg,
  UnInternalConsts3, UnPagtos, Lot2RepCliMul, RiscoCli, EntiJur1, RiscoAll_1,
  Lot0Loc, HistCliEmi, UnFinanceiro, UnMyPrinters, ModuleFin, OperaLct,
  ModuleLct2, Lot2DupMu1, UCreate, Lot2CabImp, NFSe_PF_0201, NFSe_PF_0000;

{$R *.DFM}

procedure TFmLot2Cab.Leitora1Click(Sender: TObject);
begin
  (*Application.CreateForm(TFmLot1RepCliLei, FmLot1RepCliLei);
  FmLot1RepCliLei.FRepCli := QrLotCodigo.Value;
  FmLot1RepCliLei.FAcao   := 0;
  FmLot1RepCliLei.ShowModal;
  FmLot1RepCliLei.Destroy;
  //
  CalculaLote(QrLotCodigo.Value);
  LocCod(QrLotCodigo.Value, QrLotCodigo.Value); *)
end;

procedure TFmLot2Cab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmLot2Cab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrLotCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmLot2Cab.DefParams;
begin
  VAR_GOTOTABELA := CO_TabLotA;
  VAR_GOTOMYSQLTABLE := QrLot;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := ' TxCompra+ValValorem+NF+'+
                   IntToStr(FmPrincipal.FConnections)+'<>0';

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('ELSE en.Nome END NOMECLIENTE, ');
  VAR_SQLx.Add('CASE WHEN en.Tipo=0 THEN en.CNPJ');
  VAR_SQLx.Add('ELSE en.CPF END CNPJCPF, ');
  VAR_SQLx.Add('CASE WHEN en.Tipo=0 THEN en.Fantasia ');
  VAR_SQLx.Add('ELSE en.Apelido END NOMEFANCLIENTE, ');
  VAR_SQLx.Add('lo.*, en.LimiCred, ');
  VAR_SQLx.Add('en.CBE CBECli, en.SCB SCBCli, en.QuantN1, en.QuantI1');
  VAR_SQLx.Add('FROM ' + CO_TabLotA + ' lo');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  VAR_SQLx.Add('WHERE lo.TxCompra+lo.ValValorem+lo.NF+'+
    IntToStr(FmPrincipal.FConnections)+'<>0');
  //
  VAR_SQL1.Add('AND lo.Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmLot2Cab.DesteBorderemdiante1Click(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

procedure TFmLot2Cab.Detalhado1Click(Sender: TObject);
var
  ValorBase: Double;
begin
{
  DmLot.QrLocPg.Close;
  DmLot.QrLocPg.Params[0].AsInteger := DmLot.QrCHDevACodigo.Value;
  UMyMod.AbreQuery(DmLot.QrLocPg);
}
  DmLot.ReopenLocPg(DmLot.QrCHDevACodigo.Value);

  if DBCheck.CriaFm(TFmLot2PCD, FmLot2PCD, afmoNegarComAviso) then
  begin
    FmLot2PCD.ImgTipo.SQLType := stIns;
    //
    with FmLot2PCD do
    begin
      TPPagto7.MinDate := 0;
      if DmLot.QrLocPg.RecordCount > 0 then
      begin
        TPDataBase7.Date := Int(DmLot.QrLocPgData.Value);
        TPPagto7.Date    := Int(DmLot.QrLocPgData.Value);
      end else begin
        TPDataBase7.Date := Int(DmLot.QrCHDevAData1.Value);
        TPPagto7.Date    := Int(DmLot.QrCHDevAData1.Value);
      end;
      EdJurosBase7.ValueVariant := DmLot.QrCHDevAJurosP.Value;
      ValorBase := DmLot.QrCHDevAValor.Value +DmLot.QrCHDevATaxas.Value +
        DmLot.QrCHDevAMulta.Value + DmLot.QrCHDevAJurosV.Value - DmLot.QrCHDevAValPago.Value -
        DmLot.QrCHDevADesconto.Value - DmLot.QrCHDevAPgDesc.Value;
      EdValorBase7.ValueVariant := ValorBase;
      TPPagto7.MinDate := TPPagto7.Date;
      if Int(Date) > TPPagto7.Date then
        TPPagto7.Date := Int(Date);
      //
      CalculaJurosCHDev();
      CalculaAPagarCHDev();
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.Diversositens1Click(Sender: TObject);
begin
  IncluiLoteCab(stUpd, GetTipoBorderoAtual(), True, False);
end;

procedure TFmLot2Cab.Duplicata1Click(Sender: TObject);
begin
  Application.CreateForm(TFmGerDup1Main, FmGerDup1Main);
  FmGerDup1Main.EdCliente.ValueVariant := QrLotCliente.Value;
  FmGerDup1Main.CBCliente.KeyValue     := QrLotCliente.Value;
  FmGerDup1Main.ShowModal;
  FmGerDup1Main.Destroy;
  //
  DmLot.ReopenOcorA(QrLotCliente.Value, FOcorA);
end;

procedure TFmLot2Cab.EdCRChange(Sender: TObject);
begin
  if DmLot.QrRiscoTCValor.Value >= 0.01 then
    EdCR.Font.Color := $000080FF // Laranja intenso
  else
    EdCR.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdCTChange(Sender: TObject);
begin
  Exit;
  if EdCT.ValueVariant >= 0.01 then
    EdCT.Font.Color := clRed
  else
    EdCT.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdCVChange(Sender: TObject);
begin
  if DmLot.FCHDevOpen_Total >= 0.01 then
    EdCV.Font.Color := clRed
  else
    EdCV.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdDRChange(Sender: TObject);
begin
  if EdDR.ValueVariant >= 0.01 then
    EdDR.Font.Color := $000080FF // Laranja intenso
  else
    EdDR.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdDTChange(Sender: TObject);
begin
  Exit;
  if EdDT.ValueVariant >= 0.01 then
    EdDT.Font.Color := clRed
  else
    EdDT.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdDVChange(Sender: TObject);
begin
  if EdDV.ValueVariant >= 0.01 then
    EdDV.Font.Color := clRed
  else
    EdDV.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdOAChange(Sender: TObject);
begin
  if DmLot.FOcorA_Total >= 0.01 then
    EdOA.Font.Color := clRed
  else
    EdOA.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdRTChange(Sender: TObject);
begin
  if EdRT.ValueVariant >= 0.01 then
    EdRT.Font.Color := $000080FF // Laranja intenso
  else
    EdRT.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdSTChange(Sender: TObject);
begin
  Exit;
  if EdST.ValueVariant >= 0.01 then
    EdST.Font.Color := clRed
  else
    EdST.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdTTChange(Sender: TObject);
begin
  if EdTT.ValueVariant >= 0.01 then
    EdTT.Font.Color := clRed
  else
    EdTT.Font.Color := clSilver;
end;

procedure TFmLot2Cab.EdVTChange(Sender: TObject);
begin
  if EdVT.ValueVariant >= 0.01 then
    EdVT.Font.Color := clRed
  else
    EdVT.Font.Color := clSilver;
end;

procedure TFmLot2Cab.Emitecheque1Click(Sender: TObject);
begin
  MyPrinters.EmiteCheque(
    QrPagtosControle.Value, QrPagtosSub.Value, QrPagtosBanco1.Value,
    0, '', 0, '', QrPagtosSerieCH.Value, QrPagtosDocumento.Value,
    QrPagtosData.Value, QrPagtosCarteira.Value,
    'NOMECLIENTE', GridPagtos, FTabLctA, -1, 0, 0);
end;

procedure TFmLot2Cab.Emitentesacadoselecionado1Click(Sender: TObject);
begin
  MostraHistorico(2);
end;

procedure TFmLot2Cab.Enviaaocliente1Click(Sender: TObject);
begin
  //EnviaBorderoCliente2();
end;

procedure TFmLot2Cab.EsteBorder1Click(Sender: TObject);
begin
  RecalculaBordero(QrLotCodigo.Value);
end;

procedure TFmLot2Cab.ExcluiChequeSelecionado();
var
  I: Integer;
begin
  {//N�o usar no Credito2
  if UFinanceiro.ExcluiLct_FatParcela(QrLotIts, QrLotItsFatID.Value,
    QrLotItsFatNum.Value, QrLotItsFatParcela.Value, QrLotItsCarteira.Value,
    FTabLctA) then
  }
  UFinanceiro.ExcluiLct_Unico(FTabLctA, QrLotIts.Database, QrLotItsData.Value,
    QrLotItsTipo.Value,  QrLotItsCarteira.Value, QrLotItsControle.Value,
    QrLotItsSub.Value, dmkPF.MotivDel_ValidaCodigo(300), True);
  //
  for I := 0 to FmPrincipal.FMyDBs.MaxDBs - 1 do
  begin
    if FmPrincipal.FMyDBs.Connected[I] = '1' then
    begin
      Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[I];
      Dmod.QrUpd_.SQL.Clear;
      Dmod.QrUpd_.SQL.Add('DELETE FROM emlotits WHERE Controle=:P0');
      //
      Dmod.QrUpd_.Params[00].AsInteger := QrLotItsFatParcela.Value;
      Dmod.QrUpd_.ExecSQL;
    end;
  end;
end;

procedure TFmLot2Cab.ExcluiItemPagamento();
var
  FatNum: Double;
  //FatID, FatParcela: Integer;
begin
  if QrPagtos.RecordCount = 0 then
  begin
    Geral.MensagemBox('N�o h� pagamento cadastrado',
    'Exclus�o de Pagamentos', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  //FatParcela := QrPagtosFatParcela.Value;
  FatNum := QrPagtosFatNum.Value;
  //FatID := QrPagtosFatID.Value;
  {
  if UFinanceiro.ExcluiLct_FatParcela(QrPagtos, FatID, FatNum, FatParcela,
  QrPagtosCarteira.Value, FTabLctA) then
  }
  UFinanceiro.ExcluiLct_Unico(FTabLctA, QrPagtos.Database, QrPagtosData.Value,
    QrPagtosTipo.Value, QrPagtosCarteira.Value, QrPagtosControle.Value,
    QrPagtosSub.Value, dmkPF.MotivDel_ValidaCodigo(300), True);
  //    
  DmLot.DesEncerraLote(QrLotCodigo.Value);
  CalculaLote(FatNum, True);
  LocCod(Trunc(FatNum), Trunc(FatNum));
end;

procedure TFmLot2Cab.ExcluiTodasParcelas();
var
  FatNum: Double;
  FatID, Carteira: Integer;
begin
  if QrPagtos.RecordCount = 0 then
  begin
    Geral.MensagemBox('N�o h� pagamento cadastrado',
    'Exclus�o de Pagamentos', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  FatNum := QrPagtosFatNum.Value;
  FatID := QrPagtosFatID.Value;
  Carteira := QrPagtosCarteira.Value;
  //
  if UFinanceiro.ExcluiLct_FatNum(QrPagtos, FatID, FatNum, FEntidade,
  Carteira, dmkPF.MotivDel_ValidaCodigo(300), True, FTabLctA) then
  begin
    DmLot.DesEncerraLote(QrLotCodigo.Value);
    CalculaLote(FatNum, True);
    LocCod(Trunc(FatNum), Trunc(FatNum));
  end;
end;

procedure TFmLot2Cab.ExecutaListaDeImpressoes();
begin
  Screen.Cursor := crHourGlass;
  try
    if MLAGeral.IntInConjunto2(002, FRelatorios) then ImprimeBorderoClienteE2;
    if MLAGeral.IntInConjunto2(004, FRelatorios) then ImprimeBorderoContabilNovo;
    if MLAGeral.IntInConjunto2(008, FRelatorios) then ImprimeAditivoNPAPNovo;
    if MLAGeral.IntInConjunto2(016, FRelatorios) then ImprimeAditivoNPNovo;
    if MLAGeral.IntInConjunto2(032, FRelatorios) then ImprimeAditivoNovo;
    if MLAGeral.IntInConjunto2(064, FRelatorios) then ImprimeNotaPromissoriaNovo;
    if MLAGeral.IntInConjunto2(128, FRelatorios) then ImprimeAutorizacaodeProtestoNovo;
    if MLAGeral.IntInConjunto2(512, FRelatorios) then ImprimeMargemColigadasNovo;
    // cuidado com envelopes !!!
    if MLAGeral.IntInConjunto2(256, FRelatorios) then ImprimeCartaAoSacadoDados;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2Cab.MargemtOtal1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
{
  QrJuros0.Close;
  QrJuros0.Params[0].AsInteger := QrLotCodigo.Value;
  QrJuros0. Open;
  Screen.Cursor := crDefault;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrJuros0, Dmod.MyDB, [
  'SELECT Vencimento, Praca, Banco, Agencia, ',
  'ContaCorrente, Documento, CNPJCPF, Emitente, ',
  'Credito, Comp TP, Dias, VlrCompra, FatParcela, ',
  'DDeposito ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + FThisFatID_301,
  'AND FatNum=' + FormatFloat('0', QrLotCodigo.Value),
  'ORDER BY Vencimento, FatParcela ',
  '']);
  //
  MyObjects.frxMostra(frxJuros0, 'Margem Total');
end;

procedure TFmLot2Cab.MenuItem1Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LOIS(istExtra2);
end;

procedure TFmLot2Cab.MenuItem2Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LOIS(istExtra1);
end;

procedure TFmLot2Cab.MenuItem4Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LOIS(istAtual);
end;

procedure TFmLot2Cab.MenuItem5Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LOIS(istSelecionados);
end;

procedure TFmLot2Cab.MenuItem7Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Pesq, FmSPC_Pesq, afmoNegarComAviso) then
  begin
    FmSPC_Pesq.ShowModal;
    FmSPC_Pesq.Destroy;
  end;
end;

procedure TFmLot2Cab.MenuItem9Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSPC_Result, 'Resultado de consulta ao SPC / SERASA');
end;

procedure TFmLot2Cab.ModeloA1Click(Sender: TObject);
begin
 ImprimeCartaAoSacadoTudoNovo(1);
end;

procedure TFmLot2Cab.ModeloB1Click(Sender: TObject);
begin
  ImprimeCartaAoSacadoTudoNovo(2);
end;

procedure TFmLot2Cab.MostraHistorico(Quem: Integer);
begin
  Application.CreateForm(TFmHistCliEmi, FmHistCliEmi);
  if Quem in ([1,3]) then
  begin
    FmHistCliEmi.EdCliente.ValueVariant := QrLotCliente.Value;
    FmHistCliEmi.CBCliente.KeyValue := QrLotCliente.Value;
  end;
  if Quem in ([2,3]) then
  begin
{??? Precisa?
    if PainelImporta.Visible then
    begin
      if Pagina.ActivePageIndex = 0 then
        FmHistCliEmi.EdCPF1.Text := Geral.FormataCNPJ_TT(DmLot1.QrLCHsCPF.Value)
      else FmHistCliEmi.EdCPF1.Text := Geral.FormataCNPJ_TT(DmLot1.QrLDUsCPF.Value)
    end else FmHistCliEmi.EdCPF1.Text := Geral.FormataCNPJ_TT(QrLotItsCNPJCPF.Value);
}
  end;
  FmHistCliEmi.Pesquisa;
  FmHistCliEmi.ShowModal;
  FmHistCliEmi.Destroy;
end;

procedure TFmLot2Cab.MostraLot2Chq(SQLType: TSQLType);
var
  I, Lins: Integer;
  Taxas: MyArrayR07;
begin
  if DBCheck.CriaFm(TFmLot2Chq, FmLot2Chq, afmoNegarComAviso) then
  begin
    with FmLot2Chq do
    begin
      ImgTipo.SQLType := SQLType;
      //
      GradeCH.RowCount := 2;
      MyObjects.LimpaGrade(GradeCH, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeCH.RowCount := Lins;
      if SQLType = stIns then
      begin
        EdBanda.ValueVariant := '';
        EdTipific.ValueVariant := 0;
        EdRegiaoCompe.ValueVariant := 0;
        EdBanco.ValueVariant := 0;
        EdAgencia.ValueVariant := 0;
        EdConta.ValueVariant := '';
        EdCheque.ValueVariant := 0;
        EdValor.ValueVariant := 0;
        EdVencto.ValueVariant := '000000';
        EdData.ValueVariant := FormatDateTime('dd/mm/yyyy', QrLotData.Value);
        EdCPF.ValueVariant := '';
        EdEmitente.ValueVariant := '';
        EdTxaCompra.ValueVariant := DmLot.QrClientesFatorCompra.Value;
        EdDMaisC.ValueVariant := DmLot.QrClientesDMaisC.Value;
        EdStatusSPC.ValueVariant := -1;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeCH.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            FArrayMySQLEE[i].Close;
            FArrayMySQLEE[i].Params[0].AsInteger := QrLotCliente.Value;
            FArrayMySQLEE[i]. Open;
            //
            if QrLotSpread.Value = 0 then
              GradeCH.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
            else
              GradeCH.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
          end;
         end;
      end else begin
        EdBanda.ValueVariant := '';
        EdTipific.ValueVariant     := QrLotItsTipific.Value;
        EdRegiaoCompe.ValueVariant := QrLotItsPraca.Value;
        EdBanco.ValueVariant       := QrLotItsBanco.Value;
        EdAgencia.ValueVariant     := QrLotItsAgencia.Value;
        EdConta.ValueVariant       := QrLotItsContaCorrente.Value;
        EdCheque.ValueVariant      := QrLotItsDocumento.Value;
        EdValor.ValueVariant       := QrLotItsCredito.Value;
        EdVencto.ValueVariant      := FormatDateTime('dd/mm/yyyy', QrLotItsVencimento.Value);
        EdData.ValueVariant        := FormatDateTime('dd/mm/yyyy', QrLotItsDCompra.Value);
        EdCPF.ValueVariant         := Geral.FormataCNPJ_TT(QrLotItsCNPJCPF.Value);
        EdEmitente.ValueVariant    := QrLotItsEmitente.Value;
        EdStatusSPC.ValueVariant   := QrLotItsStatusSPC.Value;
        //
        EdTxaCompra.ValueVariant   := QrLotItsTxaCompra.Value;
        EdDMaisC.ValueVariant      := QrLotItsDMais.Value;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          Taxas := Dmod.ObtemTaxaDeCompraRealizada(QrLotItsFatParcela.Value);
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeCH.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            GradeCH.Cells[01, i+1] := Geral.FFT(Taxas[i+1], 6, siPositivo);
          end;
        end;
        //EdValor.SetFocus;
        DmLot.PesquisaRiscoSacado(EdCPF.Text,
          EdCR, EdCV, EdCT,
          EdDR, EdDV, EdDT,
          EdRT, EdVT, EdST,
          EdOA, EdTT);
        FmLot2Chq.CalculaDiasCH();
        BtConfirma2.Enabled := True;
      end;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.MostraLot2Dup(SQLType: TSQLType);
var
  I, Lins: Integer;
  //Taxas: MyArrayR07;
begin
  if DBCheck.CriaFm(TFmLot2Dup, FmLot2Dup, afmoNegarComAviso) then
  begin
    with FmLot2Dup do
    begin
      ImgTipo.SQLType    := SQLType;
      FmLot2Dup.FTabLctA := FmLot2Cab.FTabLctA;
      //
      GradeDU.RowCount := 2;
      MyObjects.LimpaGrade(GradeDU, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeDU.RowCount := Lins;
      if SQLType = stIns then
      begin
        EdEmiss4.ValueVariant       := FormatDateTime('dd/mm/yyyy', QrLotData.Value);
        EdData4.ValueVariant        := FormatDateTime('dd/mm/yyyy', QrLotData.Value);
        EdDuplicata.ValueVariant    := '';
        EdValor4.ValueVariant       := 0;
        EdDesco4.ValueVariant       := 0;
        dmkEdTPDescAte.Date         := 0;
        EdVence4.ValueVariant       := '000000';
        EdTxaCompra4.ValueVariant   := DmLot.QrClientesFatorCompra.Value;
        EdDMaisD.ValueVariant       := DmLot.QrClientesDMaisD.Value;
        EdSacado.ValueVariant       := '';
        EdCNPJ.ValueVariant         := '';
        EdRua.ValueVariant          := '';
        EdNumero.ValueVariant       := '';
        EdBairro.ValueVariant       := '';
        EdCidade.ValueVariant       := '';
        EdCEP.ValueVariant          := '';
        //CBUF.ValueVariant           := '';
        EdTel1.ValueVariant         := '';
        EdCartDep4.ValueVariant     := Dmod.QrControleCartDuplic.Value;
        CBCartDep4.KeyValue         := Dmod.QrControleCartDuplic.Value;
        //
        //EdEmiss4.SetFocus;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeDU.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            FArrayMySQLEE[i].Close;
            FArrayMySQLEE[i].Params[0].AsInteger := QrLotCliente.Value;
            FArrayMySQLEE[i]. Open;
            if QrLotSpread.Value = 0 then
              GradeDU.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
            else
              GradeDU.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
          end;
        end;
      end else begin
        EdEmiss4.ValueVariant       := FormatDateTime('dd/mm/yyyy', QrLotItsData.Value);
        EdData4.ValueVariant        := FormatDateTime('dd/mm/yyyy', QrLotItsDCompra.Value);
        EdDuplicata.ValueVariant    := QrLotItsDuplicata.Value;
        EdValor4.ValueVariant       := QrLotItsBruto.Value;
        EdDesco4.ValueVariant       := QrLotItsDesco.Value;
        dmkEdTPDescAte.Date         := QrLotItsDescAte.Value;
        EdVence4.ValueVariant       := FormatDateTime('dd/mm/yyyy', QrLotItsVencimento.Value);
        EdTxaCompra4.ValueVariant   := QrLotItsTxaCompra.Value;
        EdDMaisD.ValueVariant       := QrLotItsDMais.Value;
        EdSacado.ValueVariant       := QrLotItsEmitente.Value;
        EdCNPJ.ValueVariant         := QrLotItsCPF_TXT.Value;
        EdCartDep4.ValueVariant     := IntToStr(QrLotItsCartDep.Value);
        CBCartDep4.KeyValue         := QrLotItsCartDep.Value;
        DmLot2.LocalizaSacado(True, EdCNPJ, EdSacado, EdRua, EdNumero,
          EdCompl, EdBairro, EdCidade, EdCEP, EdTel1, EdEmail, CBUF);
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            DmLot.QrEmLotIts.Close;
            DmLot.QrEmLotIts.Database := Dmod.FArrayMySQLBD[i];
            DmLot.QrEmLotIts.Params[0].AsInteger := QrLotItsFatParcela.Value;
            DmLot.QrEmLotIts. Open;
            GradeDU.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            GradeDU.Cells[01, i+1] := Geral.FFT(
              DmLot.QrEmLotItsTaxaPer.Value, 4, siPositivo);
          end;
        end;
        FmLot2Dup.CalculaDiasDU();
      end;
      FmLot2Dup.ShowModal;
      FmLot2Dup.Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.MostraLot2Txa(SQLType: TSQLType);
{
var
  I, Lins: Integer;
  Taxas: MyArrayR07;
}
begin
  FOrelha := 1;
  //
  if DBCheck.CriaFm(TFmLot2Txa, FmLot2Txa, afmoNegarComAviso) then
  begin
    with FmLot2Txa do
    begin
      ImgTipo.SQLType := SQLType;
      //
      if SQLType = stIns then
      begin
        EdTaxaCod5.ValueVariant := 0;
        CBTaxaCod5.KeyValue     := NULL;
        EdTaxaTxa5.ValueVariant := 0;
        EdTaxaQtd5.ValueVariant := 0;
        RGForma.ItemIndex := 0;
        TPData.Date := QrLotData.Value;
      end else begin
        EdTaxaCod5.ValueVariant := QrLotTxsFatID_Sub.Value;
        CBTaxaCod5.KeyValue     := QrLotTxsFatID_Sub.Value;
        EdTaxaTxa5.ValueVariant := QrLotTxsMoraTxa.Value;
        RGForma.ItemIndex       := QrLotTxsTipific.Value;
        EdTaxaQtd5.ValueVariant := QrLotTxsMoraDia.Value;
        TPData.Date             := QrLotTxsData.Value;
      end;
      FmLot2Txa.ShowModal;
      FmLot2Txa.Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.MostrarDias1Click(Sender: TObject);
begin
  ImprimeBorderoClienteSobra(True);
end;

procedure TFmLot2Cab.Multiplaseleo1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLot2RepCliMul, FmLot2RepCliMul);
  FmLot2RepCliMul.FRepCli := QrLotCodigo.Value;
  FmLot2RepCliMul.ShowModal;
  FmLot2RepCliMul.Destroy;
  //
  CalculaLote(QrLotCodigo.Value, True);
  LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
end;

procedure TFmLot2Cab.NFSeMaring1Click(Sender: TObject);
var
  NumNF, Codigo: Integer;
  SerieNF: String;
  TotNF: Double;
begin
  (*
  if (Pos('[VAL_FATCOMPR]', Dmod.QrControleNFLinha1.Value) > 0) or
    (Pos('[VAL_FATCOMPR]', Dmod.QrControleNFLinha2.Value) > 0) or
    (Pos('[VAL_FATCOMPR]', Dmod.QrControleNFLinha3.Value) > 0)
  then
    TotNF := QrLotMINAV.Value + QrLotMINTC.Value
  else
    TotNF := QrLotMINAV.Value;
  *)
  TotNF := QrLotMINAV.Value;
  //
  UnNFSe_PF_0201.MostraFormNFSe(stIns, VAR_FilialUnica, QrLotCliente.Value, 0,
    0, 0, '', True, 0, 0, fgnLoteRPS, nil, TotNF, SerieNF, NumNF, nil);
  //
  if (SerieNF <> '') and (NumNF <> 0) then
  begin
    Codigo := QrLotCodigo.Value;
    //
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLotA, False,
      ['SerNF', 'NF'], ['Codigo'], [SerieNF, NumNF], [Codigo], True)
    then
      Geral.MB_Info('Falha ao gerar NFS-e!')
    else
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLot2Cab.Nominal1Click(Sender: TObject);
begin
  FmPrincipal.ImprimeChequeSimples(Geral.FFT(QrPagtosDebito.Value, 2, siPositivo),
    QrPagtosNome.Value, DModG.QrDonoCIDADE.Value, QrPagtosData.Value, True);
end;

procedure TFmLot2Cab.NoMostrarDias1Click(Sender: TObject);
begin
  ImprimeBorderoClienteSobra(False);
end;

procedure TFmLot2Cab.Novo1Click(Sender: TObject);
var
  Valor: Double;
begin
  Valor := QrLotVAL_LIQUIDO.Value - QrLotPgLiq.Value;
  if Valor <=0 then Valor := QrLotA_PG_LIQ.Value;
  Application.CreateForm(TFmLot0Pg, FmLot0Pg);
  FmLot0Pg.FValor      := Valor;
  FmLot0Pg.FCliente    := QrLotCliente.Value;
  FmLot0Pg.FFatNum     := QrLotCodigo.Value;
  FmLot0Pg.FFatParcela := QrPagtos.RecNo + 1;
  FmLot0Pg.TPData.Date := QrLotData.Value;
  FmLot0Pg.EdTxt.Text  := 'CD';
  FmLot0Pg.FNomeCreditado := QrLotNOMECLIENTE.Value;
  FmLot0Pg.FCNPJCreditado := QrLotCNPJCPF.Value;
  FmLot0Pg.ReopenCreditados;
  FmLot0Pg.EdValor.ValueVariant := Valor;
  //
  FmLot0Pg.ShowModal;
  FmLot0Pg.Destroy;
  if FMudouSaldo then
  begin
    DmLot.DesEncerraLote(QrLotCodigo.Value);
    CalculaLote(QrLotCodigo.Value, True);
  end;
end;

procedure TFmLot2Cab.Novo2Click(Sender: TObject);
begin
  ImprimeBorderoClienteE2();
end;

procedure TFmLot2Cab.PagaChequeDevolvido();
{
var
  Codigo: Integer;
}
const
  OcorAPCD = -1;
  //Desco = 0;
var
{
  ValQuit: Double;
  Localiz, DevolPg, OcorP, AlinIts,
}
  Ocorreu, Gen_0303, Gen_0304, Gen_0305, Gen_0307: Integer;
  //LocFP,
  CartDep, FatParcela, Controle, FatID_Sub, Cliente, FatParcRef: Integer;
  //MoraVal,
  FatNum, ValPagar, ValPrinc, 
  ValTaxas, ValMulta, ValJuros, ValDesco, ValPende: Double;
  DataCH, Dta: TDateTime;
begin
{
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Alin Pgs', 'Alin Pgs', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO alin pgs SET AlterWeb=1, LotePg='+IntToStr(QrLotCodigo.Value)+',');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2');
  Dmod.QrUpd.SQL.Add(', AlinIts=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrLotData.Value);
  Dmod.QrUpd.Params[01].AsFloat   := DmLot.QrCHDevAATUAL.Value-DmLot.QrCHDevASALDO.Value;
  Dmod.QrUpd.Params[02].AsFloat   := DmLot.QrCHDevAATUAL.Value;
  Dmod.QrUpd.Params[03].AsInteger := DmLot.QrCHDevACodigo.Value;
  Dmod.QrUpd.Params[04].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
}
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0303, Gen_0303, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0304, Gen_0304, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0305, Gen_0305, tpCred, True) then
    Exit;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0307, Gen_0307, tpDeb, True) then
    Exit;
  //
  FatNum := QrLotCodigo.Value;
  FatParcela := 0;
  Controle := 0;
  FatID_Sub := DmLot.QrCHDevAAlinea1.Value;
  Cliente := DmLot.QrCHDevACliente.Value;
  Ocorreu := OcorAPCD;
  FatParcRef := DmLot.QrCHDevACodigo.Value;
  //Valor := DmLot.QrCHDevAATUAL.Value-DmLot.QrCHDevASALDO.Value;
  Dta := QrlotData.Value;
  DataCH := 0; // ?????
  //
  ValPagar := DmLot.QrCHDevAATUAL.Value;
  if DmLot.QtdDePagtosDeChqDev(DmLot.QrCHDevACodigo.Value) = 0 then
  begin
    ValTaxas := DmLot.QrCHDevATaxas.Value;
    ValMulta := DmLot.QrCHDevAMulta.Value;
  end else begin
    ValTaxas := 0;
    ValMulta := 0;
  end;
  ValDesco := 0; // N�o tem desconto!
  ValJuros := ValPagar - ValTaxas - ValMulta - DmLot.QrCHDevAValor.Value;
  ValPrinc := ValPagar - ValTaxas - ValMulta - ValJuros;// - ValDesco;
  ValPende := 0;  // Paga Tudo???
  CartDep := 0;
  //
{
  DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub,
  Gen_0303, Gen_0304, Gen_0305, Gen_0307,
  Cliente, Ocorreu, FatParcRef, FatNum, Pago, (*MoraVal, Desco,*) DataCH, Dta);
}
  if DmLot.InsUpd_ChqPg(Dmod.QrUpd, stIns, FatParcela, Controle, FatID_Sub,
  Gen_0303, Gen_0304, Gen_0305, Gen_0307,
  Cliente, Ocorreu, FatParcRef,
  FatNum, ValPagar(*, MoraVal, Desco*), DataCH, Dta,
  ValPrinc, ValTaxas, ValMulta, ValJuros, ValDesco, ValPende, CartDep) then
  begin
    DmLot.CalculaPagtoAlinIts(QrLotData.Value, DmLot.QrCHDevACodigo.Value);
    FCHDevA := DmLot.QrCHDevACodigo.Value;
    FCHDevP := FatParcela;
  end;
end;

procedure TFmLot2Cab.Pagamento1Click(Sender: TObject);
begin
  ExcluiItemPagamento();
end;

procedure TFmLot2Cab.PMBorderoPopup(Sender: TObject);
var
  OK: Boolean;
begin
  if QrLotMINAV.Value + QrLotMINTC.Value>=0.01 then
  begin
    Contabilidade1.Visible := True;
    MargemtOtal1.Visible   := True;
  end else begin
    Contabilidade1.Visible := False;
    MargemtOtal1.Visible   := False;
  end;
  OK := MLAGeral.IntToBool(QrLotTipo.Value);
  AditivoNPAP1.Visible         := OK;
  AutorizaodeProtesto1.Visible := OK;
  CartaaoSacado1.Visible       := OK;
  CartaaoEmitentedoCheque1.Visible := not OK;
end;

procedure TFmLot2Cab.PMRefreshPopup(Sender: TObject);
begin
  EsteBorder1.Enabled :=
    (QrLot.State <> dsInactive) and (QrLotCodigo.Value <> 0);
  DesteBorderemdiante1.Enabled := EsteBorder1.Enabled;
end;

procedure TFmLot2Cab.Portador1Click(Sender: TObject);
begin
  FmPrincipal.ImprimeChequeSimples(Geral.FFT(QrPagtosDebito.Value, 2, siPositivo),
    QrPagtosNome.Value, DModG.QrDonoCIDADE.Value, QrPagtosData.Value, False);
end;

procedure TFmLot2Cab.Prconfigurado1Click(Sender: TObject);
var
  i: integer;
begin
  if DBGrid10.SelectedRows.Count > 0 then
  begin
    if Geral.MensagemBox('Confirma o pagamento de todos cheques selecionados?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      with DBGrid10.DataSource.DataSet do
      for i:= 0 to DBGrid10.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGrid10.SelectedRows.Items[i]));
        GotoBookmark(DBGrid10.SelectedRows.Items[i]);
        PagaChequeDevolvido();
      end;
    end;
  end else PagaChequeDevolvido();
  CalculaLote(QrLotCodigo.Value, False);
  LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
  ReopenCHDevA(QrLotCliente.Value);
  ReopenCHDevP;
end;

procedure TFmLot2Cab.Promissria1Click(Sender: TObject);
begin
  ImprimeNotaPromissoriaNovo();
end;

procedure TFmLot2Cab.Cabealhodoborderatual1Click(Sender: TObject);
begin
  IncluiLoteCab(stUpd, GetTipoBorderoAtual(), False, False);
end;

procedure TFmLot2Cab.CalculaLote(Lote: Double; AvisaLoteAntigo: Boolean);
var
  CPMF, IOFd, IOCt, IOFDt, IOFVt, IRRF, IRRF_Val, ISS, ISS_Val,
  PIS, PIS_VAL, PIS_R, PIS_R_Val,
  AdValor, TaxaValX, Valor, TaxaT, COFINS, COFINS_VAL, COFINS_R, COFINS_R_VAL,
  Dias, Total, TaxaVal0, AdValMin, MINTxCompraVal, MINTxCompraPer, MINAV,
  MINTC, MINTC_AM, PIS_T_VAL, COFINS_T_VAL, Liquido, TaxaMedia,
  SobraIni, APagar, PgLiq: Double;
  TipoIOF, i: Integer;
  MaxVencto, Data, LoteTxt: String;
begin
  LoteTxt := FormatFloat('0', Lote);
  //
  // Usado no lugar de QrLot
{
  DmLot.QrLCalc.Close;
  DmLot.QrLCalc.Params[0].AsFloat := Lote;
  DmLot.QrLCalc. Open;
}
  DmLot.ReopenLCalc(Lote);
  Data := Geral.FDT(DmLot.QrLCalcdata.Value, 1);
  //
{
  DmLot.QrSum0.Close;
  DmLot.QrSum0.Params[0].AsFloat := Lote;
  DmLot.QrSum0. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrSum0, Dmod.MyDB, [
  'SELECT SUM(Credito) Valor, SUM(VlrCompra) VlrCompra, ',
  'SUM(Dias) Dias, SUM(Credito*Dias)/SUM(Credito) PRAZO_MEDIO, ',
  'MAX(Vencimento) Vencto, COUNT(FatNum) ITENS ',
  'FROM ' + FTabLctA,
  'WHERE FatID=' + FThisFatID_301,
  'AND FatNum=' + FormatFloat('0', Lote),
  '']);
  //
  Dias := DmLot.QrSum0PRAZO_MEDIO.Value;
  Total := DmLot.QrSum0Valor.Value;
  TaxaVal0 := DmLot.QrSum0VlrCompra.Value;
  MaxVencto := FormatDateTime(VAR_FORMATDATE, DmLot.QrSum0Vencto.Value);
  //
  {
  DmLot.QrTxs.Close;
  DmLot.QrTxs.Params[0].AsFloat := Lote;
  DmLot.QrTxs. Open;
  }
  DmLot.ReopenTxs(Lote);
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + CO TabLotTxs);
  Dmod.QrUpd.SQL.Add('SET AlterWeb=1, TaxaVal=:P0, SysAQtd=:P1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
}
  while not DmLot.QrTxs.Eof do
  begin
    if (DmLot.QrTxsTipific.Value = 1) or (DmLot.QrTxsBase.Value = 1) then
    begin
      if DmLot.QrTxsMoraDia.Value <> 0 then
        TaxaT := DmLot.QrTxsMoraTxa.Value * DmLot.QrTxsMoraDia.Value
      else
        if DmLot.QrSum0ITENS.Value <> 0 then
          TaxaT := DmLot.QrTxsMoraTxa.Value * DmLot.QrSum0ITENS.Value
        else
          TaxaT := DmLot.QrTxsMoraTxa.Value;
      case DmLot.QrTxsBase.Value of
        0:
        begin
          if DmLot.QrTxsTipific.Value = 1 then
            Valor := Int((TaxaT * Total)+0.51) / 100
          else Valor := TaxaT;
        end;
        1: Valor := Int((TaxaT * 100)+0.51) / 100;
        else Valor := 0;
      end;
{
      Dmod.QrUpd.Params[0].AsFloat   := Valor;
      Dmod.QrUpd.Params[1].AsFloat   := DmLot.QrSum0ITENS.Value;
      //
      Dmod.QrUpd.Params[2].AsInteger := DmLot.QrTxsControle.Value;
      Dmod.QrUpd.ExecSQL;
}
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False, [
      'MoraVal', 'Credito', 'Qtde'], [
      'FatID', 'FatParcela'], [
      Valor, Valor, DmLot.QrSum0ITENS.Value], [
      VAR_FATID_0303, DmLot.QrTxsFatParcela.Value], True);
    end;
    DmLot.QrTxs.Next;
  end;
  //
  IRRF     := DmLot.QrLCalcIRRF.Value;
  PIS      := DmLot.QrLCalcPIS.Value;
  PIS_R    := DmLot.QrLCalcPIS_R.Value;
  COFINS   := DmLot.QrLCalcCOFINS.Value;
  COFINS_R := DmLot.QrLCalcCOFINS_R.Value;
  ISS      := DmLot.QrLCalcISS.Value;
  //
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1'  then
    begin
      FArrayMySQLSI[i].Close;
      FArrayMySQLSI[i].Params[0].AsFloat := Lote;
      FArrayMySQLSI[i]. Open;
      //
      AdValor      := Total * FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat / 100;
      TaxaValX     := FArrayMySQLSI[i].FieldByName('TaxaVal').AsFloat;
      IRRF_Val     := Trunc(IRRF * AdValor+0.5)/100;
      ISS_Val      := Trunc(ISS * AdValor+0.5)/100;
      PIS_Val      := Trunc(PIS * (AdValor+TaxaValX)+0.5)/100;
      PIS_R_Val    := Trunc(PIS_R * (AdValor+TaxaValX)+0.5)/100;
      COFINS_Val   := Trunc(COFINS * (AdValor+TaxaValX)+0.5)/100;
      COFINS_R_Val := Trunc(COFINS_R * (AdValor+TaxaValX)+0.5)/100;
      //
      Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
      Dmod.QrUpd_.SQL.Clear;
      Dmod.QrUpd_.SQL.Add('UPDATE emlot SET AlterWeb=1, ValValorem=:P0, TaxaVal=:P1, ');
      Dmod.QrUpd_.SQL.Add('IRRF=:P2, IRRF_Val=:P3, ISS=:P4, ISS_Val=:P5, ');
      Dmod.QrUpd_.SQL.Add('PIS_R=:P6, PIS_R_Val=:P7, COFINS_R=:P8, ');
      Dmod.QrUpd_.SQL.Add('COFINS_R_VAL=:P9, PIS=:P10, PIS_VAL=:P11, ');
      Dmod.QrUpd_.SQL.Add('COFINS=:P12, COFINS_VAL=:P13 ');
      Dmod.QrUpd_.SQL.Add('WHERE Codigo=:Pa');
      //
      Dmod.QrUpd_.Params[00].AsFloat := AdValor;
      Dmod.QrUpd_.Params[01].AsFloat := TaxaValX;
      Dmod.QrUpd_.Params[02].AsFloat := IRRF;
      Dmod.QrUpd_.Params[03].AsFloat := IRRF_Val;
      Dmod.QrUpd_.Params[04].AsFloat := ISS;
      Dmod.QrUpd_.Params[05].AsFloat := ISS_Val;
      Dmod.QrUpd_.Params[06].AsFloat := PIS_R;
      Dmod.QrUpd_.Params[07].AsFloat := PIS_R_Val;
      Dmod.QrUpd_.Params[08].AsFloat := COFINS_R;
      Dmod.QrUpd_.Params[09].AsFloat := COFINS_R_Val;
      Dmod.QrUpd_.Params[10].AsFloat := PIS;
      Dmod.QrUpd_.Params[11].AsFloat := PIS_Val;
      Dmod.QrUpd_.Params[12].AsFloat := COFINS;
      Dmod.QrUpd_.Params[13].AsFloat := COFINS_Val;
      ////
      Dmod.QrUpd_.Params[14].AsFloat := Lote;
      Dmod.QrUpd_.ExecSQL;
    end;
  end;
  //
  if Dmod.QrControleTipoAdValorem.Value = 0 then
  begin
    AdValor := DmLot.QrLCalcAdValorem.Value * Total / 100;
  end else AdValor := DmLot.QrLCalcAdValorem.Value;
  //////////////////////////////////////////////////////////////////////////////
  CPMF := DmLot.QrLCalcCPMF.Value * Total;
  CPMF := Int(CPMF + 0.999999) / 100;
  //
  {
  DmLot.QrSumTxs.Close;
  DmLot.QrSumTxs.Params[0].AsFloat := Lote;
  DmLot.QrSumTxs. Open;
  }
  DmLot.ReopenSumTxs(Lote);
  //
  {
  DmLot.QrSumOco.Close;
  DmLot.QrSumOco.Params[0].AsFloat := Lote;
  DmLot.QrSumOco. Open;
  }
  DmLot.ReopenSumOco(Lote);
  //
  {
  DmLot.QrSumCHP.Close;
  DmLot.QrSumCHP.Params[0].AsFloat := Lote;
  DmLot.QrSumCHP. Open;
  }
  DmLot.ReopenSumCHP(Lote);
  //
  {
  DmLot.QrSumDUP.Close;
  DmLot.QrSumDUP.Params[0].AsFloat := Lote;
  DmLot.QrSumDUP. Open;
  }
  DmLot.ReopenSumDUP(Lote);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrSumP, Dmod.MyDB, [
  'SELECT SUM(Debito) Debito ',
  'FROM ' + FTabLctA,
  'WHERE FatID IN (' + FThisFatID_302 + ') ',
  'AND FatNum=' + LoteTXT,
  '']);
  //
  DmLot.ReopenSumRepCli(Lote);
  //////////////////////////////////////////////////////////////////////////////

  ///////// M�nimos ///////////////////
  if (Total >= 0.01) and (TaxaVal0+AdValor>=0.01) then
  begin
    if Dmod.QrControleAdValMinTip.Value = 0 then
      AdValMin :=
        Trunc(Dmod.QrControleAdValMinVal.Value * Total) / 100
    else AdValMin := Dmod.QrControleAdValMinVal.Value;
    if AdValMin > AdValor then
      MINAV := Trunc(AdValMin*100)/100
    else
      MINAV := Trunc(AdValor*100)/100;
    //
    MINTxCompraPer := MLAGeral.CalculaJuroComposto(
      Dmod.QrControleFatorCompraMin.Value, Dias);
    MINTxCompraVal := Trunc(MINTxCompraPer * Total) / 100;
    if MINTxCompraVal > TaxaVal0 then MINTC := Round(MINTxCompraVal*100)/100
    else MINTC := Round(TaxaVal0*100)/100;
    //
    MINTC_AM := MLAGeral.DescobreJuroComposto(MINTC/Total*100, Dias, 2);
  end else begin
    MINAV := 0;
    MINTC := 0;
    MINTC_AM := 0;
  end;
  IOCt := DmLot.QrLCalcIOC.Value * Dias;
  IOCt := Int((IOCt * Total)+ 0.999999) / 100;

  FmPrincipal.ObtemTipoEPercDeIOF2008(DmLot.QrLCalcTipo.Value, DmLot.QrLCalcSimples.Value, Total,
    IOFd, TipoIOF);
  IOFd := IOFd / 365;

  IOFdt := IOFd * Dias;
  IOFdt := Int((IOFdt * Total)+ 0.999999) / 100;

  IOFvt := Int((DmLot.QrLCalcIOFv.Value * (Total - MINTC))+ 0.999999) / 100;

  IRRF_Val  := Trunc((IRRF * MINAV) + 0.4)/100;
  ////////////////////////////////////////////////////////////////////////////
  ISS_Val      := Trunc(ISS * MINAV+0.5)/100;
  PIS_Val      := Trunc(PIS * (MINAV+MINTC)+0.5)/100;
  PIS_R_Val    := Trunc(PIS_R * (MINAV+MINTC)+0.5)/100;
  PIS_T_Val    := Trunc((PIS+PIS_R) * (MINAV+MINTC)+0.5)/100;
  COFINS_Val   := Trunc(COFINS * (MINAV+MINTC)+0.5)/100;
  COFINS_R_Val := Trunc(COFINS_R * (MINAV+MINTC)+0.5)/100;
  COFINS_T_Val := Trunc((COFINS+COFINS_R) * (MINAV+MINTC)+0.5)/100;
  //
  // Sobras
  {
  DmLot2.QrSobraIni.Close;
  DmLot2.QrSobraIni.Params[00].AsInteger := DmLot.QrLCalcCliente.Value;
  DmLot2.QrSobraIni.Params[01].AsString  := Data;
  DmLot2.QrSobraIni.Params[02].AsString  := Data;
  DmLot2.QrSobraIni.Params[03].AsFloat   := Lote;
  DmLot2.QrSobraIni. Open;
  }
  DmLot2.ReopenSobraIni(DmLot.QrLCalcCliente.Value, Data, Lote);
  //
  // Lotes -> L�quido?
  APagar := Total - MINAV - MINTC - IOCt - IOFDt - IOFVt;
  PGLiq  := DmLot.QrSumPDebito.Value + DmLot.QrSumRepCliValor.Value;
  //
  SobraIni := DmLot2.QrSobraIniSobraNow.Value;
  // Fim Sobras
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLotA);
  Dmod.QrUpd.SQL.Add('SET AlterWeb=1, LctsAuto=0, ');
  Dmod.QrUpd.SQL.Add('Total=:P0, Dias=:P1, TxCompra=:P2, ValValorem=:P3,');
  Dmod.QrUpd.SQL.Add('IRRF=:P4, IRRF_Val=:P5, ISS=:P6, ISS_Val=:P7, ');
  Dmod.QrUpd.SQL.Add('PIS_R=:P8, PIS_R_Val=:P9, Tarifas=:P10, OcorP=:P11, ');
  Dmod.QrUpd.SQL.Add('PIS=:P12, PIS_Val=:P13, COFINS=:P14, COFINS_VAL=:P15, ');
  Dmod.QrUpd.SQL.Add('COFINS_R=:P16, COFINS_R_VAL=:P17, IOC_VAL=:P18, ');
  Dmod.QrUpd.SQL.Add('CPMF_VAL=:P19, MaxVencto=:P20, CHDevPg=:P21, ');
  Dmod.QrUpd.SQL.Add('MINAV=:P22, MINTC=:P23, MINTC_AM=:P24, PIS_T_Val=:P25, ');
  Dmod.QrUpd.SQL.Add('COFINS_T_Val=:P26, PgLiq=:P27, DUDevPg=:P28, ');
  Dmod.QrUpd.SQL.Add('Itens=:P29, SobraIni=:P30, ');
  Dmod.QrUpd.SQL.Add('IOFd_Val=:P31, IOFv_Val=:P32, IOFd=:P33, TipoIOF=:P34 ');
  //
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa ');
  //
  Dmod.QrUpd.Params[00].AsFloat  := Total;
  Dmod.QrUpd.Params[01].AsFloat  := Dias;
  Dmod.QrUpd.Params[02].AsFloat  := TaxaVal0;
  Dmod.QrUpd.Params[03].AsFloat  := AdValor;
  //
  Dmod.QrUpd.Params[04].AsFloat := IRRF;
  Dmod.QrUpd.Params[05].AsFloat := IRRF_Val;
  Dmod.QrUpd.Params[06].AsFloat := ISS;
  Dmod.QrUpd.Params[07].AsFloat := ISS_Val;
  Dmod.QrUpd.Params[08].AsFloat := PIS_R;
  Dmod.QrUpd.Params[09].AsFloat := PIS_R_Val;
  Dmod.QrUpd.Params[10].AsFloat := DmLot.QrSumTxsTaxaVal.Value;
  Dmod.QrUpd.Params[11].AsFloat := DmLot.QrSumOcoPAGO.Value;
  Dmod.QrUpd.Params[12].AsFloat := PIS;
  Dmod.QrUpd.Params[13].AsFloat := PIS_VAL;
  Dmod.QrUpd.Params[14].AsFloat := COFINS;
  Dmod.QrUpd.Params[15].AsFloat := COFINS_VAL;
  Dmod.QrUpd.Params[16].AsFloat := COFINS_R;
  Dmod.QrUpd.Params[17].AsFloat := COFINS_R_VAL;
  Dmod.QrUpd.Params[18].AsFloat := IOCt;
  Dmod.QrUpd.Params[19].AsFloat := CPMF;
  Dmod.QrUpd.Params[20].AsString  := MaxVencto;
  Dmod.QrUpd.Params[21].AsFloat   := DmLot.QrSumCHPPago.Value;
  Dmod.QrUpd.Params[22].AsFloat   := MINAV;
  Dmod.QrUpd.Params[23].AsFloat   := MINTC;
  Dmod.QrUpd.Params[24].AsFloat   := MINTC_AM;
  Dmod.QrUpd.Params[25].AsFloat   := PIS_T_VAL;
  Dmod.QrUpd.Params[26].AsFloat   := COFINS_T_VAL;
  Dmod.QrUpd.Params[27].AsFloat   := PgLiq;
  Dmod.QrUpd.Params[28].AsFloat   := DmLot.QrSumDUPPago.Value;
  Dmod.QrUpd.Params[29].AsInteger := DmLot.QrSum0ITENS.Value;
  Dmod.QrUpd.Params[30].AsFloat   := SobraIni;
  Dmod.QrUpd.Params[31].AsFloat   := IOFDt;
  Dmod.QrUpd.Params[32].AsFloat   := IOFVt;
  Dmod.QrUpd.Params[33].AsFloat   := IOFd;
  Dmod.QrUpd.Params[34].AsFloat   := TipoIOF;
  //
  Dmod.QrUpd.Params[35].AsFloat   := Lote;
  Dmod.QrUpd.ExecSQL;
  //LocCod(Codigo, Codigo);
{
  DmLot.QrLCalc.Close;
  DmLot.QrLCalc.Params[0].AsFloat := Lote;
  DmLot.QrLCalc. Open;
}
  DmLot.ReopenLCalc(Lote);
  //
  if AvisaLoteAntigo then
  begin
{
    DmLot2.QrOutros.Close;
    DmLot2.QrOutros.Params[00].AsInteger := DmLot.QrLCalcCliente.Value;
    DmLot2.QrOutros.Params[01].AsString  := Data;
    DmLot2.QrOutros.Params[02].AsString  := Data;
    DmLot2.QrOutros.Params[03].AsFloat   := Lote;
    UMyMod.AbreQuery(DmLot2.QrOutros);
}
    DmLot2.ReopenOutros(DmLot.QrLCalcCliente.Value, Data, Lote);
    //
    if DmLot2.QrOutros.RecordCount > 0 then
      MyObjects.frxMostra(DmLot2.frxOutros, 'Border�s a recalcular');
  end;
  //
end;

procedure TFmLot2Cab.CalculaPagtoLotIts(LOIS: Integer);
var
  Sit: Integer;
  Cred, Debi: Double;
  Data3, Data4: String;
begin
{
  DmLot.QrPgD.Close;
  DmLot.QrPgD.Params[0].AsInteger := LOIS;
  DmLot.QrPgD. Open;
}
  DmLot.ReopenPgD(LOIS);
  //
  Cred := DmLot.QrPgDPago.Value + DmLot.QrPgDDesco.Value;
  Debi := DmLot.QrPgDValor.Value + DmLot.QrPgDJuros.Value;
  if DmLot.QrPgDPago.Value < 0.01 then Sit := 0 else
  begin
    if Cred < (Debi - 0.009) then Sit := 1 else
    begin
      if Cred > (Debi + 0.009) then Sit := 3 else Sit := 2;
    end;
  end;
  //

  //Data := FormatDateTime(VAR_FORMATDATE, DmLot.QrPgDMaxData.Value);
  // novo 2007 08 21
  Data4 := FormatDateTime(VAR_FORMATDATE, DmLot.QrPgDMaxData.Value);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  // fim novo
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET AlterWeb=1,   ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 ');
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + FThisFatID_301);
  Dmod.QrUpd.SQL.Add('AND FatParcela=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := DmLot.QrPgDJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := DmLot.QrPgDDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := DmLot.QrPgDPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Sit;
  //
  Dmod.QrUpd.Params[06].AsInteger := LOIS;
  Dmod.QrUpd.ExecSQL;
  //
  FmPrincipal.AtualizaLastEditLote(Data4);
end;

procedure TFmLot2Cab.CalculaPagtoOcorP(Quitacao: TDateTime;
  OcorP: Integer);
var
  Sit: Integer;
  Valor, TaxaV: Double;
begin
{
  DmLot.QrSumOc.Close;
  DmLot.QrSumOc.Params[0].AsInteger := OcorP;
  UMyMod.AbreQuery(DmLot.QrSumOc);
}
  DmLot.ReopenSumOc(OcorP);
  //
  Sit := 0;
  if DmLot.QrSumOcPago.Value <> 0 then
  begin
    Valor := DmLot.QrOcorAValor.Value;
    TaxaV := DmLot.QrOcorATaxaV.Value;
    //
    if Valor = 0 then
      Valor := QrOcorPValor.Value;
    if TaxaV = 0 then
      TaxaV := QrOcorPMoraVal.Value;
    //
    if (DmLot.QrSumOcPago.Value < (Valor + TaxaV)-0.009)
//   or (DmLot.QrSumOcPago.Value > (DmLot.QrOcorAValor.Value + DmLot.QrOcorATaxaV.Value)+0.009) n�o funcionou
    then Sit := 1 else Sit := 2;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := DmLot.QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := DmLot.QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorP;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmLot2Cab.CalculaValoresCH_DU_Refresh(Controle, Dias: Integer;
  TxaCompra, Valor: Double);
var
  TaxaT, JuroT: Double;
  i: Integer;
  Taxas: MyArrayR07;
begin
  Taxas := Dmod.ObtemTaxaDeCompraRealizada(Controle);
  FTaxa[0] := TxaCompra;
  TaxaT    := TxaCompra;
  for i := 1 to 6 do
  begin
    FTaxa[i] := Taxas[i];
    TaxaT := TaxaT + Taxas[i];
  end;
  //
  //juros compostos ?
  JuroT   := MLAGeral.CalculaJuroComposto(TaxaT, Dias);
  //
  if TaxaT > FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end else begin
      FJuro[0] := MLAGeral.CalculaJuroComposto(FTaxa[0], Dias);
      FValr[0] := FJuro[0] * Valor / 100;
      JuroT := JuroT - FJuro[0];
      TaxaT := TaxaT - FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end;
  end else begin
    FJuro[0] := JuroT;
    FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLot2Cab.CartaaoEmitentedoCheque1Click(Sender: TObject);
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(5);
  if ReopenEmitCH(istPergunta) then
  begin
    MyObjects.frxMostra(frxCartaEmitCH, 'Carta ao emitente do cheque');
    AtualizaImpressaoDeCarta(QrLotCodigo.Value, 0);
  end;
end;

procedure TFmLot2Cab.Carteiradedepsito1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLot0CartDep, FmLot0CartDep);
  if QrLotIts.State = dsBrowse then
  begin
    FmLot0CartDep.EdCartDep.ValueVariant := QrLotItsCartDep.Value;
    FmLot0CartDep.CBCartDep.KeyValue := QrLotItsCartDep.Value;
  end;
  FmLot0CartDep.ShowModal;
  FmLot0CartDep.Destroy;
end;

procedure TFmLot2Cab.Cheque1Click(Sender: TObject);
begin
  Application.CreateForm(TFmGerChqMain, FmGerChqMain);
  FmGerChqMain.FCliente := QrLotCliente.Value;
  FmGerChqMain.ShowModal;
  FmGerChqMain.Destroy;
  //
  DmLot.ReopenOcorA(QrLotCliente.Value, FOcorA);
end;

procedure TFmLot2Cab.Cliente1Click(Sender: TObject);
begin
  Application.CreateForm(TFmGerCliMain, FmGerCliMain);
  FmGerCliMain.FDataLot := QrLotData.Value;
  FmGerCliMain.FCliente := QrLotCliente.Value;
  FmGerCliMain.ShowModal;
  FmGerCliMain.Destroy;
  //
  DmLot.ReopenOcorA(QrLotCliente.Value, FOcorA);
end;

procedure TFmLot2Cab.Clienteselecionado1Click(Sender: TObject);
begin
  MostraHistorico(1);
end;

procedure TFmLot2Cab.Coligadas1Click(Sender: TObject);
begin
  ImprimeMargemColigadasNovo();
end;

procedure TFmLot2Cab.Comomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(True);
end;

procedure TFmLot2Cab.Contabilidade1Click(Sender: TObject);
begin
  ImprimeBorderoContabilNovo();
end;

procedure TFmLot2Cab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  // Demora muito!!! Va(vpLast);
end;

procedure TFmLot2Cab.RecalculaBordero(Lote: Integer);
begin
  ReopenLI(Lote);
  //
  if DmLot.QrLITipoLote.Value = 0 then
    RefreshEmBorderoCH(Lote, True)
  else
    RefreshEmBorderoDU(Lote, True);
end;

procedure TFmLot2Cab.RefreshCheque();
var
  Vence, Data_: TDateTime;
  DMais, Dias, FatParcela, Comp, Banco, Praca, Tipific, StatusSPC, Cliente,
  Agencia, TipoCart, Carteira, Controle, Sub: Integer;
  Documento, FatNum, Credito: Double;
  DDeposito, DataDoc, DCompra, Vencimento, ContaCorrente,
  CNPJCPF, Emitente: String;
begin
  FatParcela    := DmLot.QrLIFatParcela.Value;
  FatNum        := DmLot.QrLIFatNum.Value;
  Credito       := DmLot.QrLICredito.Value;
  Vence         := DmLot.QrLIVencimento.Value;
  Vencimento    := FormatDateTime(VAR_FORMATDATE, Vence);
  Data_         := DmLot.QrLIDCompra.Value;
  DCompra       := FormatDateTime(VAR_FORMATDATE, Data_);
  DataDoc       := DCompra; // 2011-12-24
  Banco         := DmLot.QrLIBanco.Value;
  Agencia       := DmLot.QrLIAgencia.Value;
  ContaCorrente := DmLot.QrLIContaCorrente.Value;
  Documento     := DmLot.QrLIDocumento.Value;
  CNPJCPF       := DmLot.QrLICNPJCPF.Value;
  Emitente      := DmLot.QrLIEmitente.Value;
  DMais         := DmLot.QrLIDMais.Value;
  Praca         := DmLot.QrLIPraca.Value;
  Tipific       := DmLot.QrLITipific.Value;
  StatusSPC     := DmLot.QrLIStatusSPC.Value;
  Comp          := FmPrincipal.VerificaDiasCompensacao(Praca,
                   DmLot.QrLICBE.Value, Credito, Data_, Vence,
                   DmLot.QrLISCB.Value);
  Dias          := UMyMod.CalculaDias(Data_, Vence, DMais, Comp,
                   Dmod.QrControleTipoPrazoDesc.Value, DmLot.QrLICBE.Value);
  DDeposito     := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
  Cliente       := DmLot.QrLICliente.Value;
  TipoCart      := DmLot.QrLITipoCart.Value;
  Carteira      := DmLot.QrLICarteira.Value;
  Controle      := DmLot.QrLIControle.Value;
  Sub           := DmLot.QrLISub.Value;
  //
  CalculaValoresCH_DU_Refresh(FatParcela, Dias, DmLot.QrLITxaCompra.Value, Credito);
  //
  SQL_CH(FatParcela, Comp, Banco, Agencia, Documento, DMais, Dias, FatNum,
    Praca, Tipific, StatusSPC, ContaCorrente, CNPJCPF, Emitente, DataDoc,
    Vencimento, DCompra, DDeposito, Credito, stUpd, Cliente, TipoCart, Carteira,
    Controle, Sub);
end;

procedure TFmLot2Cab.RefreshDuplicata();
var
  TipoCart, Carteira, Controle, Sub,
  Agencia, FatParcela, DMais, Dias, Banco, CartDep, Cliente: Integer;
  FatNum, Valor, Desco, Bruto: Double;
  Duplic, CPF, Sacado, Vencto, DCompra, DDeposito, Emissao, DescAte: String;
  Vence, Data_, Emiss_: TDateTime;
begin
  FatParcela := DmLot.QrLIFatParcela.Value;
  FatNum     := DmLot.QrLIFatNum.Value;
  Bruto   := DmLot.QrLIBruto.Value;
  Desco   := DmLot.QrLIDesco.Value;
  DescAte := Geral.FDT(DmLot.QrLIDescAte.Value, 1);
  Valor   := Bruto-Desco;
  Vence   := DmLot.QrLIVencimento.Value;
  Vencto  := FormatDateTime(VAR_FORMATDATE, Vence);
  Data_   := DmLot.QrLIDCompra.Value;
  DCompra := FormatDateTime(VAR_FORMATDATE, Data_);
  Emiss_  := DmLot.QrLIData.Value;
  Emissao := FormatDateTime(VAR_FORMATDATE, Emiss_);
  Duplic  := DmLot.QrLIDuplicata.Value;
  CPF     := DmLot.QrLICNPJCPF.Value;
  Sacado  := DmLot.QrLIEmitente.Value;
  Banco   := DmLot.QrLIBanco.Value;
  Agencia := DmLot.QrLIAgencia.Value;
  CartDep := DmLot.QrLICartDep.Value;
  //
  Cliente  := DmLot.QrLICliente.Value;
  TipoCart := DmLot.QrLITipoCart.Value;
  Carteira := DmLot.QrLICarteira.Value;
  Controle := DmLot.QrLIControle.Value;
  Sub      := DmLot.QrLISub.Value;
  //
  DMais := DmLot.QrLIDMais.Value;
  Dias := UMyMod.CalculaDias(Data_, Vence, DMais, 0(*Comp*),
    Dmod.QrControleTipoPrazoDesc.Value, 0(*CBE*));
  DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
  //
  CalculaValoresCH_DU_Refresh(FatParcela, Dias, DmLot.QrLITxaCompra.Value, Valor);
  //
  SQL_DU(FatNum, FatParcela, DMais, Dias, Banco, Agencia, Valor, Desco, Bruto,
    Duplic, CPF, Sacado, Vencto, DCompra, DDeposito, Emissao, DescAte,
    stUpd, Cliente, CartDep, TipoCart, Carteira, Controle, Sub);
  FmPrincipal.AtualizaLastEditLote(DCompra);
end;

procedure TFmLot2Cab.RefreshEmBorderoCH(Lote: Integer; Unico: Boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    if DmLot.QrLI.RecordCount > 0 then
    begin
      ProgressBar1.Position := 0;
      ProgressBar1.Max := DmLot.QrLI.RecordCount;
      if Unico then ProgressBar1.Visible := True;
      while not DmLot.QrLI.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position + 1;
        Update;
        Application.ProcessMessages;
        RefreshCheque();
        DmLot.QrLI.Next;
      end;
    end;
    //
    CalculaLote(Lote, True);
    if Unico then
    begin
      ProgressBar1.Visible := False;
      LocCod(Lote, Lote);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2Cab.RefreshEmBorderoDU(Lote: Integer; Unico: Boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    if DmLot.QrLI.RecordCount > 0 then
    begin
      ProgressBar1.Max := DmLot.QrLI.RecordCount;
      ProgressBar1.Position := 0;
      if Unico then ProgressBar1.Visible := True;
      while not DmLot.QrLI.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position + 1;
        Update;
        Application.ProcessMessages;
        RefreshDuplicata;
        DmLot.QrLI.Next;
      end;
    end;
    //
    CalculaLote(Lote, True);
    if Unico then
    begin
      ProgressBar1.Visible := False;
      LocCod(Lote, Lote);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2Cab.RemessaCNAB1Click(Sender: TObject);
var
  Bordero: Integer;
begin
  if (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) then
    Bordero := QrLotCodigo.Value
  else
    Bordero := 0;
  //
  FmPrincipal.MostraRemessaCNABNovo(0, 0, Bordero);
end;

procedure TFmLot2Cab.ReopenCHDevA(Cliente: Integer);
begin
{
  DmLot.QrCHDevA.Close;
  DmLot.QrCHDevA.Params[0].AsInteger := Cliente;
  DmLot.QrCHDevA.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLot.QrCHDevA. Open;
  //
  DmLot.QrCHDevT.Close;
  DmLot.QrCHDevT.Params[0].AsInteger := Cliente;
  DmLot.QrCHDevT.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLot.QrCHDevT. Open;
}
  DmLot.ReopenCliCHDevA(Cliente, FCHDevA);
end;

procedure TFmLot2Cab.ReopenCHDevP();
begin
{
  QrCHDevP.Close;
  QrCHDevP.Params[0].AsInteger := QrLotCodigo.Value;
  UMyMod.AbreQuery(QrCHDevP);
}
{
SELECT ai.ValPago,ai.Banco, ai.Agencia, ai.Conta, ai.Cheque,
ai.CPF, ai.Valor, ai.Taxas, ai.Multa, ai.Emitente,
ai.Status, ai.JurosP, ai.JurosV, ai.Desconto,
CASE WHEN ai.Status = 2 THEN 0 ELSE (ai.Valor + ai.Taxas +
ai.Multa + ai.JurosV - ai.Desconto - ai.ValPago) END SALDO,
ap.Data, ap.Juros, ap.Pago, ap.AlinIts, ap.Codigo
FROM alin pgs ap
LEFT JOIN alinits ai ON ai.Codigo=ap.AlinIts
WHERE ap.LotePG=:P0

}
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHDevP, Dmod.MyDB, [
  'SELECT ai.ValPago, ai.Banco, ai.Agencia, ai.Conta, ai.Cheque, ',
  'ai.CPF, ai.Valor, ai.Taxas, ai.Multa, ai.Emitente, ',
  'ai.Status, ai.JurosP, ai.JurosV, ai.Desconto, ai.PgDesc, ',
  'IF(ai.Status = 2, 0, (ai.Valor + ai.Taxas + ',
  'ai.Multa + ai.JurosV - ai.Desconto - ai.ValPago)) SALDO, ',
  'ap.Data, ap.MoraVal, ap.Credito, ap.FatParcRef, ap.FatNum, ',
  'ap.FatParcela, ap.Controle, ap.Ocorreu, ap.FatGrupo',
  'FROM ' + CO_TabLctA + ' ap ',
  'LEFT JOIN alinits ai ON ai.Codigo=ap.FatParcRef ',
  //'WHERE ap.FatID=' + TXT_VAR_FATID_0305,
  'WHERE ap.FatID=' + TXT_VAR_FATID_0311,
  'AND ap.FatNum=' + Geral.FF0(QrLotCodigo.Value),
  '']);
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCHDevP, Dmod.MyDB, [
  'SELECT (alp.Credito + alp.MoraVal + alp.TaxasVal + ',
  'alp.MultaVal - alp.DescoVal) PAGOU,  alp.Credito, ',
  'alp.MoraVal, alp.DescoVal, alp.MultaVal, alp.TaxasVal, ',
  'alp.Vencimento, alp.Data, alp.FatNum, alp.FatParcela, ',
  'alp.Controle, alp.Ocorreu, alp.FatParcRef, alp.FatGrupo, ',
  'ali.Valor, ali.Taxas, ali.Multa, ali.JurosV, ali.ValPago, ',
  'ali.Desconto, ali.PgDesc, ali.Status, ali.Banco, ',
  'ali.Agencia, ali.Conta, ali.Cheque, ali.CPF, ali.Emitente ',
  'FROM ' + CO_TabLctA + ' alp ',
  'LEFT JOIN alinits ali ON ali.Codigo=alp.FatParcRef ',
  'WHERE alp.FatID=' + TXT_VAR_FATID_0311,
  'AND alp.FatNum=' + Geral.FF0(QrLotCodigo.Value),
  'ORDER BY alp.Data',
  '']);
  //
  if FCHDevP > 0 then
    QrCHDevP.Locate('FatParcela', FCHDevP, []);
end;

function TFmLot2Cab.ReopenContrat(): Boolean;
begin
  QrContrat.Close;
  QrContrat.Params[0].AsInteger := QrLotCliente.Value;
  QrContrat.Params[1].AsInteger := QrLotCliente.Value;
  UMyMod.AbreQuery(QrContrat, Dmod.MyDB);
  if QrContrat.RecordCount = 0 then
  begin
    Result := False;
    Geral.MensagemBox('Cliente sem contrato!', 'Aviso', MB_OK+MB_ICONWARNING);
  end else Result := True;
end;

procedure TFmLot2Cab.ReopenDOpen(Cliente: Integer);
begin
{
  DmLot.QrDOpen.Close;
  DmLot.QrDOpen.Params[0].AsInteger := Cliente;
  DmLot.QrDOpen.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLot.QrDOpen. Open;

SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado,
li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,
lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,
li.Vencto, li.Data3
FROM lot esits li
LEFT JOIN lot es     lo ON lo.Codigo=li.Codigo
LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao
WHERE lo.Tipo=1
AND li.Quitado <> 2
AND lo.Cliente=:P0
AND lo.TxCompra+lo.ValValorem+ :P1 >= 0.01
ORDER BY li.Vencto
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrDOpen, Dmod.MyDB, [
  'SELECT od.Nome STATUS, li.FatParcela, li.Duplicata, li.Repassado, ',
  'li.DCompra, li.Credito, li.DDeposito, li.Emitente, li.CNPJCPF, ',
  'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg, ',
  'li.Vencimento, li.Data3 ',
  'FROM ' + FTabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao ',
  'WHERE li.FatID=' + FThisFatID_301,
  'AND lo.Tipo=1 ',
  'AND li.Quitado NOT IN (-2, 2, 3) ',
  'AND lo.Cliente=' + FormatFloat('0', Cliente),
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections) + ' >= 0.01 ',
  'ORDER BY li.Vencimento ',
  '']);
end;

procedure TFmLot2Cab.ReopenDPago();
begin
{
  QrDPago.Close;
  QrDPago.Params[0].AsInteger := QrLotCodigo.Value;
  QrDPago. Open;
  //
  if FDPago > 0 then QrDPago.Locate('Controle', FDPago, []);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrDPago, Dmod.MyDB, [
  'SELECT li.Emitente, li.CNPJCPF, ',
  'li.Duplicata, li.Quitado, IF(li.Quitado = 2, 0, ',
  '(li.Credito+li.TotalJr-li.TotalDs-li.TotalPg)) SALDO, ',
  'dp.FatParcRef, dp.Data, dp.FatParcela, dp.FatNum, ',
  'dp.MoraVal, dp.DescoVal, dp.Credito, ',
  'dp.Credito + dp.MoraVal - dp.DescoVal PAGO, ',
  'dp.Controle, dp.Ocorreu, dp.FatGrupo ',
  'FROM ' + CO_TabLctA + ' dp, ' + CO_TabLctA + ' li ',
  'WHERE dp.FatID=' + TXT_VAR_FATID_0312,
  'AND dp.FatNum=' + Geral.FF0(QrLotCodigo.Value),
  'AND li.FatID=' + TXT_VAR_FATID_0301,
  'AND li.FatParcela=dp.FatParcRef ',
  '']);
  if FDPago > 0 then QrDPago.Locate('FatParcela', FDPago, []);
end;

procedure TFmLot2Cab.ReopenIts();
begin
{
  QrLotIts.Close;
  QrLotIts.Params[0].AsInteger := QrLotCodigo.Value;
  QrLotIts. Open;
}
  if QrLotCodigo.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLotIts, Dmod.MyDB, [
    'SELECT Tipo, Carteira, Controle, Sub, ',
    'FatNum, FatParcela, Comp, Banco, Agencia, ',
    'ContaCorrente, Documento, Emitente, Credito, ',
    'Vencimento, DDeposito, TxaCompra, TxaAdValorem, ',
    'VlrCompra, VlrAdValorem, DMais, Dias, ',
    'CNPJCPF, TxaJuros, DCompra, Duplicata, Data, ',
    'Devolucao, Desco, Quitado, Bruto, Praca, ',
    'BcoCobra, AgeCobra, CartDep, DescAte, Tipific, ',
    'StatusSPC, FatID, Juridico ',
    'FROM ' + FtabLctA,
    'WHERE FatID=' + FThisFatID_301,
    'AND FatNum=' + FormatFloat('0', QrLotCodigo.Value),
    '']);
    //
    if FmPrincipal.FControlIts <> 0 then
      QrLotIts.Locate('FatParcela', FmPrincipal.FControlIts, []);
  end else
    QrLotIts.Close;
end;

procedure TFmLot2Cab.ReopenLI(Lote: Integer);
begin
{
  DmLot.QrLI.Close;
  DmLot.QrLI.Params[0].AsInteger := Lote;
  DmLot.QrLI. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrLI, Dmod.MyDB, [
  'SELECT lo.Tipo TipoLote, lo.CBE, lo.SCB, li.FatNum, li.FatParcela, ',
  'li.Credito, li.Vencimento, li.DCompra, li.Banco, ',
  'li.Agencia, li.ContaCorrente, li.Documento, li.CNPJCPF, ',
  'li.Emitente, li.DMais, li.Praca, li.Tipific, li.StatusSPC, ',
  'li.TxaCompra, li.Bruto, li.Desco, li.DescAte, li.Cliente, ',
  'li.Data, li.Duplicata, li.CartDep, li.Tipo TipoCart, li.Carteira, ',
  'li.Controle, li.Sub ',
  'FROM ' + FTabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum ',
  'WHERE li.FatID=' + FThisFatID_301,
  'AND lo.Codigo=' + FormatFloat('0', Lote),
  '']);
end;

procedure TFmLot2Cab.ReopenOcorP();
begin
{
  QrOcorP.Close;
  QrOcorP.Params[0].AsInteger := QrLotCodigo.Value;
  QrOcorP. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOcorP, QrOcorP.Database, [
  'DROP TABLE IF EXISTS ocorp; ',
  'CREATE TABLE ocorp ',
  'SELECT IF(oc.' + FLD_LOIS + '<>0, 1, 2) TipoIns, ',
  'IF(oc.' + FLD_LOIS + '<>0, oc.' + FLD_LOIS + ', op.FatParcRef) LOIS, ',
  'oc.DataO, op.Data DataO2, ',
  'IF(oc.' + FLD_LOIS + '<>0, oc.Ocorrencia, op.FatID_Sub) Ocorrencia, ',
  'oc.Valor, oc.Descri, (oc.Valor + oc.TaxaV - oc.Pago) SALDO, ',
  'ob.Nome NOMEOCORRENCIA, op.Controle, op.FatParcela, op.Ocorreu, ',
  'op.Data, op.MoraVal, op.Credito - op.Debito PAGO, op.FatNum ',
  'FROM ' + FTabLctA + ' op ',
  'LEFT JOIN ' + TMeuDB + '.ocorreu  oc ON oc.Codigo=op.Ocorreu ',
  'LEFT JOIN ' + TMeuDB + '.ocorbank ob ON ob.Codigo = IF(oc.' +
  FLD_LOIS + '<>0, oc.Ocorrencia, op.FatID_Sub) ',
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND op.FatNum=' + FormatFloat('0', QrLotCodigo.Value) + '; ',
  'SELECT ocp.*, ',
  'loi.Banco, loi.Agencia, loi.ContaCorrente, loi.Documento, ',
  'loi.Duplicata, loi.Emitente, loi.CNPJCPF, lot.Tipo, ',
  'IF(lot.Tipo=0, "CH", "DU") TIPODOC ',
  'FROM ocorp ocp ',
  'LEFT JOIN ' + FTabLctA + ' loi ON ocp.LOIS = loi.FatParcela ',
  '  AND loi.FatID=' + FThisFatID_301,
  'LEFT JOIN ' + TMeuDB + '.' + CO_TabLotA +  ' lot ON lot.Codigo = loi.FatNum; ',

  '']);
  //
  if FOcorP > 0 then
    QrOcorP.Locate('FatParcela', FOcorP, []);
end;

procedure TFmLot2Cab.ReopenPagtos();
const
  DataI = 0;
  DataF = 0;
begin
  UMyMod.AbreSQL_ABD(QrPagtos, '', FTabLctA, FTabLctB, FTabLctD,
  '', '', '', DataI, DataF, FDtEncer, FDtMorto, [
  'SELECT cr.*, la.*, ca.Nome NOMECARTEIRA, ',
  'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI, ',
  'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ',
  'ELSE cl.Nome END NOMECLIENTE, ',
  'ca.Tipo CARTEIRATIPO ',
  'FROM ' + FTabLctA + ' la ',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
  'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI ',
  'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente ',
  'LEFT JOIN creditados cr ON ',
  '          cr.Banco=la.Banco AND ',
  '          cr.Agencia=la.Agencia AND ',
  '          cr.Conta=la.ContaCorrente ',
  'WHERE FatID IN (' + FThisFatID_302 + ') ',
  'AND FatNum=' + FormatFloat('0', QrLotCodigo.Value) ,
  ''], [
  'ORDER BY la.FatParcela, la.Vencimento ',
  ''], tetNaoExclui, 'Lot2Cab.ReopenPagtos()');
end;

procedure TFmLot2Cab.ReopenRepCli();
begin
{
  DmLot.QrRepCli.Close;
  DmLot.QrRepCli.Params[0].AsInteger := QrLotCodigo.Value;
  DmLot.QrRepCli. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrRepCli, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, ',
  'lot.Lote, loi.Vencimento, loi.Credito, loi.Documento, loi.Banco, ',
  'loi.Agencia, loi.ContaCorrente, loi.Emitente, loi.FatNum, ',
  'loi.FatParcela ',
  'FROM ' + FTabLctA + ' loi ',
  'LEFT JOIN ' + CO_TabLotA + ' lot ON lot.Codigo=loi.FatNum ',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente ',
  'WHERE loi.FatID=' + FThisFatID_301,
  'AND loi.RepCli=' + FormatFloat('0', QrLotCodigo.Value),
  'ORDER BY loi.Credito DESC ',
  '']);
end;

procedure TFmLot2Cab.ReopenRiscoC(Cliente: Integer);
begin
{
  DmLot.QrRiscoC.Close;
  DmLot.QrRiscoC.Params[0].AsInteger := Cliente;
  DmLot.QrRiscoC.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLot.QrRiscoC. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrRiscoC, Dmod.MyDB, [
  'SELECT lo.Tipo, li.Banco, li.Agencia, li.ContaCorrente, ',
  'li.Documento, li.Credito, li.DCompra, li.DDeposito, ',
  'li.Emitente, li.CNPJCPF ',
  'FROM ' + FTabLctA + ' li, ' + CO_TabLotA + ' lo ',
  'WHERE li.FatID=' + FThisFatID_301,
  'AND lo.Codigo=li.FatNum ',
  'AND lo.Cliente=' + FormatFloat('0', Cliente),
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections) + ' >= 0.01 ',
  'AND (li.Devolucao=0) AND (DDeposito>=SYSDATE()) ',
  'AND lo.Tipo = 0 ',
  'ORDER BY DDeposito ',
  '']);
  //
{
  DmLot.QrRiscoTC.Close;
  DmLot.QrRiscoTC.Params[0].AsInteger := Cliente;
  DmLot.QrRiscoTC.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLot.QrRiscoTC. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrRiscoTC, Dmod.MyDB, [
  'SELECT SUM(li.Credito) Valor ',
  'FROM ' + FTabLctA + ' li, ' + CO_TabLotA + ' lo ',
  'WHERE li.FatID=' + FThisFatID_301,
  'AND lo.Codigo=li.FatNum ',
  'AND lo.Cliente=' + FormatFloat('0', Cliente),
  'AND lo.TxCompra + lo.ValValorem + ' +
  FormatFloat('0', FmPrincipal.FConnections) + ' >= 0.01 ',
  'AND (li.Devolucao=0) AND (DDeposito>=SYSDATE()) ',
  'AND lo.Tipo = 0 ',
  '']);
end;

function TFmLot2Cab.ReopenSaCart(SelType: TSelType): Boolean;
var
  Quais: TSelType;
  i: Integer;
  Grade: TDBGrid;
  Txt: String;
begin
  Quais := SelType;
  if SelType = istPergunta then
  begin
    if DBCheck.CriaFm(TFmQuaisItens, FmQuaisItens, afmoNegarComAviso) then
    begin
      FmQuaisItens.ShowModal;
      Quais := FmQuaisItens.FEscolha;
      FmQuaisItens.Destroy;
    end;
  end;
  QrSaCart.Close;
  QrSaCart.SQL.Clear;
  QrSaCart.SQL.Add('SELECT li.FatNum, sa.Numero+0.000 Numero, sa.*');
  QrSaCart.SQL.Add('FROM ' + FTabLctA + ' li');
  QrSaCart.SQL.Add('LEFT JOIN sacados sa ON sa.CNPJ=li.CNPJCPF');
  QrSaCart.SQL.Add('WHERE li.FatID=' + FThisFatID_301);
  if QrLotTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
  if (Quais = istAtual) or (
    (Quais = istSelecionados) and (Grade.SelectedRows.Count < 2)) then
      QrSaCart.SQL.Add('AND li.FatParcela=' +
      FormatFloat('0', QrLotItsFatParcela.Value)) else
  if Quais = istSelecionados then
  begin
    Txt := '';
    with Grade.DataSource.DataSet do
    for i := 0 to Grade.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
      GotoBookmark(Grade.SelectedRows.Items[i]);
      if Txt <> '' then Txt := Txt + ',';
      Txt := Txt + FormatFloat('0', QrLotItsFatParcela.Value);
    end;
    QrSaCart.SQL.Add('AND li.FatParcela in (' + Txt + ')');
  end else
  if Quais = istTodos then QrSaCart.SQL.Add('AND li.FatNum=' +
      FormatFloat('0', QrLotCodigo.Value))
  else begin
    Result := False;
    if Quais <> istNenhum then Geral.MensagemBox(
    '"SelType" n�o definido em "ReopenSaCart"!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  UMyMod.AbreQuery(QrSaCart, Dmod.MyDB);
  Result := True;
end;

procedure TFmLot2Cab.ReopenSumOP(FatNum: Double);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumOP, Dmod.MyDB, [
  'SELECT COUNT(oc.Ocorrencia) Ocorrencias, SUM(op.Credito-Debito) Pago ',
  'FROM ' + CO_TabLctA +' op ',
  'LEFT JOIN ocorreu oc ON oc.Codigo=op.Ocorreu ',
  'WHERE op.FatID=' + TXT_VAR_FATID_0304,
  'AND op.FatNum=' + Geral.FF0(Trunc(FatNum)),
  '']);
end;

procedure TFmLot2Cab.ReopenTestemunhas;
begin
  FTbTestemunha := UCriar.RecriaTempTable('Testemunha', DModG.QrUpdPID1, False);
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+ FTbTestemunha +' SET Nome=:P0, Tipo=:P1');
  if Length(QrContratTestem1Nome.Value) > 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsString := QrContratTestem1Nome.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 1;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if Length(QrContratTestem2Nome.Value) > 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsString  := QrContratTestem2Nome.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 2;
    DModG.QrUpdPID1.ExecSQL;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrTestemunhas, DModG.MyPID_DB, [
  'SELECT Nome, CNPJ, Tipo ',
  'FROM ' + FTbTestemunha + ' ',
  'ORDER BY Tipo',
  '']);
end;

procedure TFmLot2Cab.ReopenTxs();
begin
{
  QrLotTxs.Close;
  QrLotTxs.Params[0].AsInteger := QrLotCodigo.Value;
  QrLotTxs. Open;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotTxs, Dmod.MyDB, [
  'SELECT tx.Nome, lt.* ',
  'FROM ' + CO TabLotTxs + ' lt ',
  'LEFT JOIN taxas tx ON tx.Codigo=lt.TaxaCod ',
  'WHERE lt.Codigo=' + FormatFloat('0', QrLotCodigo.Value),
  '']);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotTxs, Dmod.MyDB, [
  'SELECT lt.Credito-lt.Debito VALOR, ' ,
  'tx.Nome, lt.Controle, lt.Data, ',
  'lt.FatNum, lt.FatParcela, lt.FatID_Sub, ',
  'lt.MoraTxa, lt.MoraVal, lt.MoraDia, ',
  'lt.Qtde, lt.Tipific, lt.Credito ',
  'FROM ' + CO_TabLctA + ' lt ',
  'LEFT JOIN taxas tx ON tx.Codigo=lt.FatID_Sub ',
  'WHERE lt.FatID=' + TXT_VAR_FATID_0303,
  'AND lt.FatNum=' + Geral.FF0(QrLotCodigo.Value),
  '']);
  //
  if FControlTxs <> 0 then
    QrLotTxs.Locate('FatParcela', FControlTxs, []);
end;

procedure TFmLot2Cab.ResultadodaconsultaaoSPC1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSPC_Result, 'Resultado de consulta ao SPC / SERASA');
end;

procedure TFmLot2Cab.Rpido1Click(Sender: TObject);
begin
  AtualizaPagamento(QrLotCodigo.Value, QrLotCliente.Value, QrLotData.Value,
    QrLotVAL_LIQUIDO.Value);
end;

procedure TFmLot2Cab.Dasos1Click(Sender: TObject);
begin
  ImprimeCartaAoSacadoDados();
end;

procedure TFmLot2Cab.DefineCompraEmpresas(FatNum: Double; FatParcela: Integer);
var
  I: Integer;
begin
  for I := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[I] = '1' then
    begin
     Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('DELETE FROM emlotits WHERE Controle=:P0');
     Dmod.QrUpd_.Params[00].AsInteger := FatParcela;
     Dmod.QrUpd_.ExecSQL;
     //
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('INSERT INTO emlotits SET TaxaPer=:P0, TaxaVal=:P1, ');
     Dmod.QrUpd_.SQL.Add('JuroPer=:P2, Codigo=:Pa, Controle=:Pb');
     //
     Dmod.QrUpd_.Params[00].AsFloat   := FTaxa[I + 1];
     Dmod.QrUpd_.Params[01].AsFloat   := FValr[I + 1];
     Dmod.QrUpd_.Params[02].AsFloat   := FJuro[I + 1];
     //
     Dmod.QrUpd_.Params[03].AsFloat   := FatNum;
     Dmod.QrUpd_.Params[04].AsInteger := FatParcela;
     Dmod.QrUpd_.ExecSQL;
     //
    end;
  end;
end;

function TFmLot2Cab.DefineDataSetsNF(frx: TfrxReport): Boolean;
var
  I: Integer;
begin
  //Result := False;
  //
  Dmod.frxDsControle.DataSet  := Dmod.QrControle;
  DmodG.frxDsDono.DataSet     := DmodG.QrDono;
  DModG.frxDsEndereco.DataSet := DModG.QrEndereco;
  frxDsNF.DataSet             := QrLot;
  //
  frx.Datasets.Clear;
  frx.DataSets.Add(Dmod.frxDsControle);
  frx.DataSets.Add(DmodG.frxDsDono);
  frx.DataSets.Add(DModG.frxDsEndereco);
  frx.DataSets.Add(frxDsNF);
  //
  for I := 0 to frx.Datasets.Count -1 do
    frx.DataSets.Items[I].DataSet.Enabled := True;
  //
  Result := True;
end;

procedure TFmLot2Cab.DefineONomeDoForm;
begin
end;

procedure TFmLot2Cab.DefineRiscos(Cliente: Integer);
begin
  InfoTempo(Now, 'IN�CIO DEFINI��O DE RISCOS', True);
  //
  // soma antes do �ltimo !!!!
  ReopenCHDevA(Cliente);
  InfoTempo(Now, 'Cheques devolvidos abertos', False);
  DmLot.ReopenOcorA(Cliente, FOcorA);
  InfoTempo(Now, 'Ocorrr�ncias abertas', False);
  // fim somas.
  //
  // Normais
  ReopenIts();
  InfoTempo(Now, 'Itens do border�', False);
  ReopenTxs();
  InfoTempo(Now, 'Taxas do border�', False);
  ReopenOcorP();
  InfoTempo(Now, 'Ocorr�ncias pagas', False);
  ReopenCHDevP();
  InfoTempo(Now, 'Cheques devolvidos pagos', False);
  ReopenPagtos();
  InfoTempo(Now, 'Pagamentos ao cliente', False);
  ReopenDPago();
  InfoTempo(Now, 'Duplicatas vencidas pagas', False);
  ReopenRiscoC(Cliente);
  InfoTempo(Now, 'Risco cliente', False);
  ReopenRepCli();
  InfoTempo(Now, 'Pagamentos com cheques de terceiros', False);
  DmLot.ReopenSPC_Cli(QrLotCliente.Value);
  InfoTempo(Now, 'Configura��es de consulta ao SPC', False);
  //
  // precisa ser o �ltimo !!!! para Ed?.ValueVariant
  ReopenDOpen(Cliente);
  InfoTempo(Now, 'Duplicatas vencidas abertas', False);
  //
  Memo1.Lines.Add('');
  Memo1.Lines.Add('==============================================================================');
  Memo1.Lines.Add('');
end;

procedure TFmLot2Cab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmLot2Cab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmLot2Cab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmLot2Cab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

function TFmLot2Cab.SQL_CH(var FatParcela: Integer; const Comp, Banco, Agencia:
              Integer; Documento: Double; DMais, Dias: Integer; FatNum:
              Double; Praca, Tipific, StatusSPC: Integer; ContaCorrente, CNPJCPF,
              Emitente, DataDoc, Vencimento, DCompra, DDeposito: String; Credito: Double;
              SQLType: TSQLType; Cliente: Integer; var TipoCart, Carteira, Controle,
              Sub: Integer): Integer;
const
  Descricao = 'CD';
  Sit = 0;
  FatID = VAR_FATID_0301;
  FatID_Sub = 0;
  CliInt = -11;
var
  TxaCompra, TxaAdValorem, VlrCompra, VlrAdValorem, TxaJuros: Double;
  Data_, Data3, Data: String;
  Genero: Integer;
begin
  Result := 0;
  //Genero = - 3 9 8;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0301, Genero, tpCred, True) then
    Exit;
  //
  Dmod.QrUpd.SQL.Clear;
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      FTabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(VAR_FATID_0301, FTabLctA);
  end;
{

    Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET Quitado=0, ');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   ');
  end;
  Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
  Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
  Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
  Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Cliente=:P19, Data3=:P20, ');
  Dmod.QrUpd.SQL.Add('Tipific=:P21, StatusSPC=:P22 ');
  if SQLType = stIns then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := Comp;
  Dmod.QrUpd.Params[01].AsInteger := Banco;
  Dmod.QrUpd.Params[02].AsInteger := Agencia;
  Dmod.QrUpd.Params[03].AsString  := Conta;
  Dmod.QrUpd.Params[04].AsInteger := Cheque;
  Dmod.QrUpd.Params[05].AsString  := CPF;
  Dmod.QrUpd.Params[06].AsString  := Emitente;
  Dmod.QrUpd.Params[07].AsFloat   := Valor;
  Dmod.QrUpd.Params[08].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[09].AsFloat   := FTaxa[0];
  Dmod.QrUpd.Params[10].AsFloat   := 0;
  Dmod.QrUpd.Params[11].AsFloat   := FValr[0];
  Dmod.QrUpd.Params[12].AsFloat   := 0;
  Dmod.QrUpd.Params[13].AsInteger := DMais;
  Dmod.QrUpd.Params[14].AsInteger := Dias;
  Dmod.QrUpd.Params[15].AsFloat   := FJuro[0];
  Dmod.QrUpd.Params[16].AsString  := DCompra;
  Dmod.QrUpd.Params[17].AsString  := DDeposito;
  Dmod.QrUpd.Params[18].AsInteger := Praca;
  Dmod.QrUpd.Params[19].AsInteger := Cliente;
  Dmod.QrUpd.Params[20].AsString  := DDeposito; // Quita autom�tico at� voltar
  Dmod.QrUpd.Params[21].AsInteger := Tipific;
  Dmod.QrUpd.Params[22].AsInteger := StatusSPC;
  //
  Dmod.QrUpd.Params[23].AsInteger := Codigo;
  Dmod.QrUpd.Params[24].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
}
  TxaCompra := FTaxa[0];
  TxaAdValorem := 0;
  VlrCompra := FValr[0];
  VlrAdValorem := 0;
  TxaJuros := FJuro[0];
  Data_ := DCompra;
  Data3 := DDeposito;
  //  deve ser assim porque o cheque compensa autom�tico!
  Data := DDeposito;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',*)
  'Descricao', (*'SerieNF', 'NotaFiscal',
  'Debito',*) 'Credito', (*'Compensado',
  'SerieCH',*) 'Documento', 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',*)
  'Emitente', 'Banco', 'Agencia',
  'ContaCorrente', 'CNPJCPF', (*'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',
  'MoraVal', 'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V', 'Duplicata', 'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal', 'Antigo',
  'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag',*) 'Comp',
  'Praca', (*'Bruto', 'Desco',*)
  'DCompra', 'DDeposito', (*'DescAte',*)
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', (*'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg',*) 'Data3', (*'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',
  'CartDep', 'Cobranca', 'RepCli',
  'Teste',*) 'Tipific', 'StatusSPC', (*,
  'CNAB_Lot'*) 'Data'], [
  'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,*)
  Descricao, (*SerieNF, NotaFiscal,
  Debito,*) Credito, (*Compensado,
  SerieCH,*) Documento, Sit,
  Vencimento, FatID, FatID_Sub,
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,*)
  Emitente, Banco, Agencia,
  ContaCorrente, CNPJCPF, (*Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,
  MoraVal, MultaVal, Protesto,*)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V, Duplicata, Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal, Antigo,
  ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, SubPgto1,
  MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Endossan, Endossad,
  Cancelado, EventosCad, Encerrado,
  ErrCtrl, IndiPag,*) Comp,
  Praca, (*Bruto, Desco,*)
  DCompra, DDeposito, (*DescAte,*)
  TxaCompra, TxaJuros, TxaAdValorem,
  VlrCompra, VlrAdValorem, DMais,
  Dias, (*Devolucao, ProrrVz,
  ProrrDd, Quitado, BcoCobra,
  AgeCobra, TotalJr, TotalDs,
  TotalPg,*) Data3, (*Data4,
  Repassado, Depositado, ValQuit,
  ValDeposito, TpAlin, AliIts,
  Alin#Pgs, NaoDeposita, ReforcoCxa,
  CartDep, Cobranca, RepCli,
  Teste,*) Tipific, StatusSPC, (*,
  CNAB_Lot*) Data], [
  TipoCart, Carteira, Controle, Sub], True) then
  begin
    DefineCompraEmpresas(FatNum, fatParcela);
    //
    FmPrincipal.FControlIts := FatParcela;
    Result := FatParcela;
  end else Result := 0;
end;

function TFmLot2Cab.SQL_DU(const FatNum: Double; var FatParcela: Integer;
             const DMais, Dias, Banco, Agencia: Integer;
             const Credito, Desco, Bruto: Double;
             const Duplicata, CNPJCPF, Emitente, Vencimento, DCompra,
             DDeposito, Emissao, DescAte: String;
             SQLType: TSQLType;
             const Cliente, CartDep: Integer;
             var TipoCart, Carteira, Controle, Sub: Integer): Integer;
const
  Descricao = 'CD';
  Sit = 0;
  FatID = VAR_FATID_0301;
  FatID_Sub = 0;
  CliInt = -11;
var
  TxaCompra, TxaAdValorem, VlrCompra, VlrAdValorem, TxaJuros: Double;
  Data, Data3, DataDoc: String;
  Genero: Integer;
begin
  Result := 0;
  //Genero = - 3 9 8;
  if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0301, Genero, tpCred, True) then
    Exit;
  Dmod.QrUpd.SQL.Clear;
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
      FTabLctA, LAN_CTOS, 'Controle');
    FatParcela := UMyMod.BuscaProximaFatParcelaDeFatID(VAR_FATID_0301, FTabLctA);
  end;
{
  if SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Lot esIts', 'Lot esIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lot esits SET ');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   ');
  end;
  Dmod.QrUpd.SQL.Add('Duplicata=:P0, CPF=:P1, Emitente=:P2, Valor=:P3, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P4, TxaCompra=:P5, VlrCompra=:P6, DMais=:P7, ');
  Dmod.QrUpd.SQL.Add('Dias=:P8, TxaJuros=:P9, DCompra=:P10, DDeposito=:P11, ');
  Dmod.QrUpd.SQL.Add('Emissao=:P12, Desco=:P13, Bruto=:P14, Banco=:P15, ');
  Dmod.QrUpd.SQL.Add('Agencia=:P16, Cliente=:P17, CartDep=:P18, DescAte=:P19 ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsString  := Duplic;
  Dmod.QrUpd.Params[01].AsString  := CPF;
  Dmod.QrUpd.Params[02].AsString  := Sacado;
  Dmod.QrUpd.Params[03].AsFloat   := Valor;
  Dmod.QrUpd.Params[04].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[05].AsFloat   := FTaxa[0];
  Dmod.QrUpd.Params[06].AsFloat   := FValr[0];
  Dmod.QrUpd.Params[07].AsInteger := DMais;
  Dmod.QrUpd.Params[08].AsInteger := Dias;
  Dmod.QrUpd.Params[09].AsFloat   := FJuro[0];
  Dmod.QrUpd.Params[10].AsString  := DCompra;
  Dmod.QrUpd.Params[11].AsString  := DDeposito;
  Dmod.QrUpd.Params[12].AsString  := Emissao;
  Dmod.QrUpd.Params[13].AsFloat   := Desco;
  Dmod.QrUpd.Params[14].AsFloat   := Bruto;
  Dmod.QrUpd.Params[15].AsInteger := Banco;
  Dmod.QrUpd.Params[16].AsInteger := Agencia;
  Dmod.QrUpd.Params[17].AsInteger := Cliente;
  Dmod.QrUpd.Params[18].AsInteger := CartDep;
  Dmod.QrUpd.Params[19].AsString  := DescAte;
  //
  Dmod.QrUpd.Params[20].AsInteger := Codigo;
  Dmod.QrUpd.Params[21].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
}
  //
  TxaCompra := FTaxa[0];
  TxaAdValorem := 0;
  VlrCompra := FValr[0];
  VlrAdValorem := 0;
  TxaJuros := FJuro[0];
  //Data := DCompra;
  Data := Emissao;
  //Desabilitado em 01/08/2013 Ini Motivo: Estava criando um data de quita��o para um documento em aberto
  //Data3 := DDeposito;
  Data3 := '0000-00-00';
  //Desabilitado em 01/08/2013 Fim
  DataDoc := Data;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabLctA, False, [
  (*'Autorizacao',*) 'Genero', (*'Qtde',*)
  'Descricao', (*'SerieNF', 'NotaFiscal',
  'Debito',*) 'Credito', (*'Compensado',
  'SerieCH', 'Documento',*) 'Sit',
  'Vencimento', 'FatID', 'FatID_Sub',
  'FatNum', 'FatParcela', (*'ID_Pgto',
  'ID_Quit', 'ID_Sub', 'Fatura',*)
  'Emitente', 'Banco', 'Agencia',
  (*'ContaCorrente',*) 'CNPJCPF', (*'Local',
  'Cartao', 'Linha', 'OperCount',
  'Lancto', 'Pago', 'Mez',
  'Fornecedor',*) 'Cliente', 'CliInt',
  (*'ForneceI', 'MoraDia', 'Multa',
  'MoraVal', 'MultaVal', 'Protesto',*)
  'DataDoc', (*'CtrlIni', 'Nivel',
  'Vendedor', 'Account', 'ICMS_P',
  'ICMS_V',*) 'Duplicata', (*'Depto',
  'DescoPor', 'DescoVal', 'DescoControle',
  'Unidade', 'NFVal', 'Antigo',
  'ExcelGru', 'Doc2', 'CNAB_Sit',
  'TipoCH', 'Reparcel', 'Atrelado',
  'PagMul', 'PagJur', 'SubPgto1',
  'MultiPgto', 'Protocolo', 'CtrlQuitPg',
  'Endossas', 'Endossan', 'Endossad',
  'Cancelado', 'EventosCad', 'Encerrado',
  'ErrCtrl', 'IndiPag', 'Comp',
  'Praca',*) 'Bruto', 'Desco',
  'DCompra', 'DDeposito', 'DescAte',
  'TxaCompra', 'TxaJuros', 'TxaAdValorem',
  'VlrCompra', 'VlrAdValorem', 'DMais',
  'Dias', (*'Devolucao', 'ProrrVz',
  'ProrrDd', 'Quitado', 'BcoCobra',
  'AgeCobra', 'TotalJr', 'TotalDs',
  'TotalPg',*) 'Data3', (*'Data4',
  'Repassado', 'Depositado', 'ValQuit',
  'ValDeposito', 'TpAlin', 'AliIts',
  FLD_CHQ_PGS, 'NaoDeposita', 'ReforcoCxa',*)
  'CartDep', (*, 'Cobranca', 'RepCli',
  'Teste', 'Tipific', 'StatusSPC',
  'CNAB_Lot'*) 'Data'], [
  'Tipo', 'Carteira', 'Controle', 'Sub'], [
  (*Autorizacao,*) Genero, (*Qtde,*)
  Descricao, (*SerieNF, NotaFiscal,
  Debito,*) Credito, (*Compensado,
  SerieCH, Documento,*) Sit,
  Vencimento, FatID, FatID_Sub,
  FatNum, FatParcela, (*ID_Pgto,
  ID_Quit, ID_Sub, Fatura,*)
  Emitente, Banco, Agencia,
  (*ContaCorrente,*) CNPJCPF, (*Local,
  Cartao, Linha, OperCount,
  Lancto, Pago, Mez,
  Fornecedor,*) Cliente, CliInt,
  (*ForneceI, MoraDia, Multa,
  MoraVal, MultaVal, Protesto,*)
  DataDoc, (*CtrlIni, Nivel,
  Vendedor, Account, ICMS_P,
  ICMS_V,*) Duplicata, (*Depto,
  DescoPor, DescoVal, DescoControle,
  Unidade, NFVal, Antigo,
  ExcelGru, Doc2, CNAB_Sit,
  TipoCH, Reparcel, Atrelado,
  PagMul, PagJur, SubPgto1,
  MultiPgto, Protocolo, CtrlQuitPg,
  Endossas, Endossan, Endossad,
  Cancelado, EventosCad, Encerrado,
  ErrCtrl, IndiPag, Comp,
  Praca,*) Bruto, Desco,
  DCompra, DDeposito, DescAte,
  TxaCompra, TxaJuros, TxaAdValorem,
  VlrCompra, VlrAdValorem, DMais,
  Dias, (*Devolucao, ProrrVz,
  ProrrDd, Quitado, BcoCobra,
  AgeCobra, TotalJr, TotalDs,
  TotalPg,*) Data3, (*Data4,
  Repassado, Depositado, ValQuit,
  ValDeposito, TpAlin, AliIts,
  Alin#Pgs, NaoDeposita, ReforcoCxa,*)
  CartDep, (*, Cobranca, RepCli,
  Teste, Tipific, StatusSPC,
  CNAB_Lot*) Data], [
  TipoCart, Carteira, Controle, Sub], True) then
  begin
    DefineCompraEmpresas(FatNum, FatParcela);
    //
    FmPrincipal.FControlIts := FatParcela;
    Result := FatParcela;
  end;
end;

procedure TFmLot2Cab.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FEscapeEnabed := False;
{
  DmLot.QrLocs.Close;
  DmLot.QrLocs.Params[0].AsInteger := QrLotCodigo.Value;
  DmLot.QrLocs. Open;
}
  DmLot.ReopenLocs(QrLotCodigo.Value);
  //
  ProgressBar2.Position := 0;
  ProgressBar2.Visible := True;
  ProgressBar2.Max := DmLot.QrLocs.RecordCount;
  ProgressBar1.Visible := True;
  while not DmLot.QrLocs.Eof do
  begin
    ProgressBar1.Position := 0;
    ProgressBar2.Position := ProgressBar2.Position +1;
    ProgressBar2.Update;
    Application.ProcessMessages;
    ReopenLI(DmLot.QrLocsCodigo.Value);
{
    DmLot.QrLI.Close;
    DmLot.QrLI.Params[0].AsInteger := DmLot.QrLocsCodigo.Value;
    DmLot.QrLI. Open;
}
    if DmLot.QrLocsTipo.Value = 0 then
      RefreshEmBorderoCH(DmLot.QrLocsCodigo.Value, False)
    else
      RefreshEmBorderoDU(DmLot.QrLocsCodigo.Value, False);
    Application.ProcessMessages;
    if FEscapeEnabed then
    begin
      Geral.MensagemBox('Processo abortado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    DmLot.QrLocs.Next;
  end;
  LocCod(DmLot.QrLocsCodigo.Value, DmLot.QrLocsCodigo.Value);
  ProgressBar1.Visible := False;
  ProgressBar2.Visible := False;
end;

procedure TFmLot2Cab.Todospagamentosdestebordero1Click(Sender: TObject);
begin
  ExcluiTodasParcelas();
end;

procedure TFmLot2Cab.TudoFolhaembranco1Click(Sender: TObject);
begin
  if DefineDataSetsNF(frxNF_Tudo) then
  begin
    GOTOy.EnderecoDeEntidade(QrLotCliente.Value, 0);
    MyObjects.frxMostra(frxNF_Tudo, 'Nota Fiscal n� '+IntToStr(QrLotNF.Value));
  end;
end;

procedure TFmLot2Cab.Aditivo1Click(Sender: TObject);
begin
  ImprimeAditivoNovo();
end;

procedure TFmLot2Cab.AditivoAPNP1Click(Sender: TObject);
begin
  ImprimeAditivoAPNPNovo();
end;

procedure TFmLot2Cab.AditivoNP1Click(Sender: TObject);
begin
  ImprimeAditivoNPNovo();
end;

procedure TFmLot2Cab.AditivoNPAP1Click(Sender: TObject);
begin
  ImprimeAditivoNPAPNovo();
end;

procedure TFmLot2Cab.Alteraodadatadodepsito1Click(Sender: TObject);
  function AlteraDepositoDocAtual(): Boolean;
  begin
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(VAR_GETDATA, 1);
    Dmod.QrUpd.Params[01].AsInteger := QrLotItsFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
    Result := true;
  end;
var
  Grade: TDBGrid;
  i: Integer;
begin
  Application.CreateForm(TFmGetData, FmGetData);
  FmGetData.TPData.Date := Date;
  FmGetData.ShowModal;
  FmGetData.Destroy;
  if VAR_GETDATA = 0 then Exit;
  //
  if QrLotTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA + ' SET DDeposito=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + FThisFatID_301);
  Dmod.QrUpd.SQL.Add('AND FatParcela=:P1');
  if Grade.SelectedRows.Count > 0 then
  begin
    with Grade.DataSource.DataSet do
    for i:= 0 to Grade.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
      GotoBookmark(Grade.SelectedRows.Items[i]);
      AlteraDepositoDocAtual();
    end;
  end else AlteraDepositoDocAtual();
  ReopenIts();
  //
  Geral.MensagemBox('As altera��es da data de dep�sito ser�o ' +
  'desfeitas caso o item for re-editado!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmLot2Cab.Alteraototaldodocumentoatual1Click(Sender: TObject);
begin
  FmPrincipal.FControlIts := QrLotItsFatParcela.Value;
  if QrLotTipo.Value = 0 then
    MostraLot2Chq(stUpd)
  else
    MostraLot2Dup(stUpd);
end;

procedure TFmLot2Cab.Ambos1Click(Sender: TObject);
begin
  MostraHistorico(3);
end;

procedure TFmLot2Cab.AtualizaImpressaoDeCarta(Lote, Tipo: Integer);
var
  Campo: String;
begin
  if Tipo = 0 then
    Campo := 'ECartaSac' //Cheques
  else
    Campo := 'ECartaEmCH';//Duplicatas
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLotA);
  Dmod.QrUpd.SQL.Add('SET AlterWeb=1, /*LctsAuto=0,*/ ECartaSac=ECartaSac+1');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=' + IntToStr(Lote));
  Dmod.QrUpd.ExecSQL;
  FmPrincipal.AtualizaSP2(2);
end;

procedure TFmLot2Cab.AtualizaPagamento(Lote, Cliente: Integer;
  Data: TDateTime; Liquido: Double);
var
  //Controle,
  Codigo: Integer;
  DataStr: String;
begin
  if Dmod.QrControleCartEspecie.Value <> 0 then
  begin
    DataStr := Geral.FDT(Data, 1);
    Codigo := QrLotCodigo.Value;
    //
    UFinanceiro.LancamentoDefaultVARS;
    // - 3 9 7
    if not DModFin.ObtemGeneroDeFatID(VAR_FATID_0302, FLAN_Genero, tpDeb, True) then
      Exit;
    //
    FLAN_Data         := DataStr;
    FLAN_Tipo         := DmLot.QrCartTipo.Value;
    FLAN_Carteira     := Dmod.QrControleCartEspecie.Value;
    FLAN_Debito       := QrLotA_PG_LIQ.Value;
    FLAN_Vencimento   := DataStr;
    FLAN_Descricao    := '';
    FLAN_Sit          := 2;
    FLAN_Cliente      := Cliente;
    FLAN_DataDoc      := Geral.FDT(QrLotData.Value, 1);
    FLAN_FatID        := VAR_FATID_0302;
    FLAN_FatNum       := Codigo;
    FLAN_FatParcela   := 999;
    //
    FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
          'Controle', FTabLctA, LAN_CTOS, 'Controle');
    VAR_LANCTO2 := FLAN_Controle;
    //
    if UFinanceiro.InsereLancamento(FTabLctA) then
    begin
      DmLot.DesEncerraLote(QrLotCodigo.Value);
      CalculaLote(Codigo, True);
      LocCod(Codigo, Codigo);
    end;
  end else Geral.MensagemBox('Carteira para pagamento r�pido n�o definida.',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmLot2Cab.AutorizaodeProtesto1Click(Sender: TObject);
begin
  ImprimeAutorizacaodeProtestoNovo();
end;

function TFmLot2Cab.LiberaStatusJuridico(Juridico: Integer): Boolean;
begin
  if Juridico <> 0 then
  begin
    Geral.MB_Aviso('A��o abortada!' + sLineBreak + 'Item atual possui status jur�dico!');
    Result := False;
  end else
    Result := True;
end;

procedure TFmLot2Cab.BitBtn1Click(Sender: TObject);
begin
{
  FmPrincipal.FCPFSacado := '58171754015';
  DmLot.ReopenSacCHDevA();
  DmLot.ReopenSacDOpen();
  DmLot.ReopenSacRiscoC();
  DmLot.RiscoSacado('08311918000163');
  DmLot.RiscoSacado('17274516949');
}
end;

procedure TFmLot2Cab.BitBtn2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerCliMain, FmGerCliMain, afmoNegarComAviso) then
  begin
    FmGerCliMain.EdCliente.ValueVariant := QrLotCliente.Value;
    FmGerCliMain.CBCliente.KeyValue := QrLotCliente.Value;
    FmGerCliMain.ShowModal;
    FmGerCliMain.Destroy;
  end;
end;

procedure TFmLot2Cab.BitBtn3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerChqMain, FmGerChqMain, afmoNegarComAviso) then
  begin
    FmGerChqMain.ShowModal;
    FmGerChqMain.Destroy;
  end;
end;

procedure TFmLot2Cab.BitBtn4Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGerDup1Main, FmGerDup1Main, afmoNegarComAviso) then
  begin
    FmGerDup1Main.ShowModal;
    FmGerDup1Main.Destroy;
  end;
end;

procedure TFmLot2Cab.BitBtn5Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
end;

procedure TFmLot2Cab.BitBtn6Click(Sender: TObject);
begin
  FmPrincipal.MostraOpcoesCreditoX();
end;

procedure TFmLot2Cab.BitBtn7Click(Sender: TObject);
var
  Emitente, CPF, Banco, Agencia, Conta: String;
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FmPrincipal.FControlIts := QrLotItsFatParcela.Value;
    Emitente := '';
    CPF      := '';
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA);
    Dmod.QrUpd.SQL.Add('SET AlterWeb=1, Emitente=:P0, CPF=:P1 ');
    Dmod.QrUpd.SQL.Add('WHERE FatID=' + FThisFatID_301);
    Dmod.QrUpd.SQL.Add('AND FatParcela=:P2');
    QrLotIts.First;
    while not QrLotIts.Eof do
    begin
      if (QrLotItsCNPJCPF.Value = '') or (QrLotItsEmitente.Value = '') then
      begin
        Banco   := MLAGeral.FFD(QrLotItsBanco.Value, 3, siPositivo);
        Agencia := MLAGeral.FFD(QrLotItsAgencia.Value, 4, siPositivo);
        Conta   := QrLotItsContaCorrente.Value;
        if DmLot2.ObtemEmitente(Banco, Agencia, Conta, Emitente, CPF, False, nil) then
        begin
          Dmod.QrUpd.Params[00].AsString  := Emitente;
          Dmod.QrUpd.Params[01].AsString  := CPF;
          Dmod.QrUpd.Params[02].AsInteger := QrLotItsFatParcela.Value;
          Dmod.QrUpd.ExecSQL;
        end;
      end;
      QrLotIts.Next;
    end;
    ReopenIts;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLot2Cab.BitBtn8Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOperaLct, FmOperaLct, afmoNegarComAviso) then
  begin
    FmOperaLct.FEnableBtBordero := False;
    FmOperaLct.ShowModal;
    FmOperaLct.Destroy;
  end;
end;

procedure TFmLot2Cab.BitBtn9Click(Sender: TObject);
var
  I, Lins: Integer;
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;
  //
  if DBCheck.CriaFm(TFmLot2DupMu1, FmLot2DupMu1, afmoNegarComAviso) then
  begin
    with FmLot2DupMu1 do
    begin
      ImgTipo.SQLType := stIns;
      //
      GradeDU.RowCount := 2;
      MyObjects.LimpaGrade(GradeDU, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeDU.RowCount := Lins;
      //if SQLType = stIns then
      begin
        TPEmiss4.Date               := QrLotData.Value;
        TPData4.Date                := QrLotData.Value;
        dmkEdTPDescAte.Date         := 0;
        EdTxaCompra4.ValueVariant   := DmLot.QrClientesFatorCompra.Value;
        EdDMaisD.ValueVariant       := DmLot.QrClientesDMaisD.Value;
        EdCartDep4.ValueVariant     := Dmod.QrControleCartDuplic.Value;
        CBCartDep4.KeyValue         := Dmod.QrControleCartDuplic.Value;
        //
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeDU.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            FArrayMySQLEE[i].Close;
            FArrayMySQLEE[i].Params[0].AsInteger := QrLotCliente.Value;
            FArrayMySQLEE[i]. Open;
            if QrLotSpread.Value = 0 then
              GradeDU.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
            else
              GradeDU.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
          end;
        end;
      end;
      FmLot2DupMu1.ShowModal;
      FmLot2DupMu1.Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.BorderauxdeCheques1Click(Sender: TObject);
begin
  FConferido := 1;
  FmPrincipal.FTipoImport := il1_Man;
  IncluiLoteCab(stIns, loteCheque, False, False);
end;

procedure TFmLot2Cab.BorderauxdeDuplicatas1Click(Sender: TObject);
begin
  FmPrincipal.FTipoImport := il1_Man;
  FConferido := 1;
  IncluiLoteCab(stIns, loteDuplicata, False, False);
end;

procedure TFmLot2Cab.BorderCliente1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLot2CabImp, FmLot2CabImp, afmoNegarComAviso) then
  begin
    FmLot2CabImp.ShowModal;
    FmLot2CabImp.Destroy;
  end;
end;

procedure TFmLot2Cab.BtAltera2Click(Sender: TObject);
begin
  if not LiberaStatusJuridico(QrLotItsJuridico.Value) then Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMAltera2, BtAltera2);
end;

procedure TFmLot2Cab.BtAltera5Click(Sender: TObject);
begin
  FControlTxs := QrLotTxsFatParcela.Value;
  MostraLot2Txa(stUpd);
end;

procedure TFmLot2Cab.BtAlteraClick(Sender: TObject);
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;
  //  
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmLot2Cab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrLotCodigo.Value;
  Close;
end;

procedure TFmLot2Cab.BtSinaleiraClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRiscoCli, FmRiscoCli, afmoNegarComAviso) then
  begin
    FmRiscoCli.EdCliente.ValueVariant := QrLotCliente.Value;
    FmRiscoCli.CBCliente.KeyValue := QrLotCliente.Value;
    FmRiscoCli.ShowModal;
    FmRiscoCli.Destroy;
  end;
end;

procedure TFmLot2Cab.BtSobraClick(Sender: TObject);
var
  Sobra: String;
begin
  {
  // N�o sugerir nada! Valor cont�bil e real podem diferir!
  //Sobra := Geral.FFT(QrLotSALDOAPAGAR.Value, 2, siNegativo);
  Sobra := '0,00';
  }
  //Verifica se valor cont�bil e real s�o iguais
  if QrLotSALDOAPAGAR.Value = QrLotSALDOAPAGAR_SCOL.Value then
    Sobra := Geral.FFT(QrLotSALDOAPAGAR_SCOL.Value, 2, siNegativo)
  else
  begin
    Sobra := '0,00';
    Geral.MensagemBox('Valor calculado: ' +
      Geral.FFT(QrLotSALDOAPAGAR.Value, 2, siNegativo) + #13#10 +
      'N�o confere com o valor cont�bil: ' +
      Geral.FFT(QrLotSALDOAPAGAR_SCOL.Value, 2, siNegativo), 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
  if InputQuery('Sobra de Border�', 'Informe o valor da sobra:', Sobra) then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + CO_TabLotA);
    Dmod.QrUpd.SQL.Add('SET AlterWeb=1, LctsAuto=0, ');
    Dmod.QrUpd.SQL.Add('SobraNow=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(Sobra);
    Dmod.QrUpd.Params[01].AsInteger := QrLotCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    CalculaLote(QrLotCodigo.Value, True);
    LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
  end;
end;

procedure TFmLot2Cab.BtSPC_LIClick(Sender: TObject);
begin
  if (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) then
    MyObjects.MostraPopUpDeBotao(PMSPC_LotIts, BtSPC_LI);
end;

procedure TFmLot2Cab.BtWebClick(Sender: TObject);
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;
  //
  FmPrincipal.FLoteWeb := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  'Verificando lote no servidor web!');
  DmLot2.BaixaLotWeb();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  'Abrindo tabela de lote baixados da web!');
  DmLot2.QrLLC.Close;
  UMyMod.AbreQuery(DmLot2.QrLLC, Dmod.MyDB);
  //
  if DmLot2.QrLLC.RecordCount > 0 then
  begin
    Application.CreateForm(TFmLot2Web, FmLot2Web);
    FmLot2Web.ShowModal;
    FmLot2Web.Destroy;
  end;
  //
  if DmLot2.QrLLC.Locate('Codigo', FmPrincipal.FLoteWeb, []) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando lote web!');
    //
    FmPrincipal.Importar(il5_Web, 0);
  end else begin
    if FmPrincipal.FLoteWeb > 0 then Geral.MensagemBox('O lote web '+
      IntToStr(FmPrincipal.FLoteWeb)+' n�o foi localizado!',
      'Aviso', MB_OK+MB_ICONWARNING);
  end;
  DmLot2.QrLLC.Close;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmLot2Cab.BtEncerraClick(Sender: TObject);
var
  Codigo: Integer;
  Text: String;
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;  
  //
  if QrLotLctsAuto.Value = 1 then
  begin
    Codigo := QrLotCodigo.Value;
    //
    if Geral.MensagemBox('Deseja desfazer o encerramento do border� ID n�mero ' +
      Geral.FF0(Codigo) + '?', 'Aviso', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
      //
      DmLot.DesEncerraLote(QrLotCodigo.Value);
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    end;
  end else
  begin
    CalculaLote(QrLotCodigo.Value, False);
    LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    Text := Geral.FFT(QrLotVAL_LIQUIDO.Value +
      QrLotSobraIni.Value - QrLotSobraNow.Value - QrLotPgLiq.Value, 2, siNegativo);
    //
    if (Text = '0,00') or (Text = '-0,00') then
    begin
      DmLot2.LancamentosAutomaticosBordero(QrLotCodigo.Value);
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    end else Geral.MensagemBox('N�o � poss�vel encerrar a opera��o!' + #13#10 +
      'H� uma diverg�ncia de $ ' + Text, 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmLot2Cab.BtExclui2Click(Sender: TObject);
var
  i: integer;
  Exclui: Boolean;
  Grade: TDBGrid;
  x: String;
begin
  if not LiberaStatusJuridico(QrLotItsJuridico.Value) then Exit;
  //
  Exclui := False;
  Screen.Cursor := crHourGlass;
  try
    if QrLotTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
    if Grade.SelectedRows.Count > 0 then
    begin
      if QrLotTipo.Value = 0 then
        x := 'todos cheques selecionados'
      else
        x := 'todas duplicatas selecionadas';
      if Geral.MensagemBox('Confirma a exclus�o de ' + x + '?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          Excluichequeselecionado;
        end;
        Exclui := True;
      end;
    end else begin
      if QrLotTipo.Value = 0 then
        x := 'do cheque ' +FormatFloat('0', QrLotItsDocumento.Value)
      else
        x := 'da duplicata ' +QrLotItsDuplicata.Value;
      if Geral.MensagemBox('Confirma a exclus�o ' + x + '?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Excluichequeselecionado;
        Exclui := True;
      end;
    end;
    if Exclui then
    begin
      QrLotIts.Next;
      FmPrincipal.FControlIts := QrLotItsFatParcela.Value;
      CalculaLote(QrLotCodigo.Value, True);
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLot2Cab.BtExclui5Click(Sender: TObject);
begin
  FOrelha     := 1;
  FControlTxs := QrLotTxsFatParcela.Value;
  if Geral.MensagemBox('Confirma a exclus�o da taxa selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO TabLotTxs);
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLotTxsFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
}
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLctA);
    Dmod.QrUpd.SQL.Add('WHERE FatID=' + TXT_VAR_FATID_0303);
    Dmod.QrUpd.SQL.Add('AND FatNum=' + Geral.FF0(Trunc(QrLotTxsFatNum.Value)));
    Dmod.QrUpd.SQL.Add('AND FatParcela<>0');
    Dmod.QrUpd.SQL.Add('AND FatParcela=' + Geral.FF0(QrLotTxsFatParcela.Value));
    //Dmod.QrUpd.SQL.Add('AND Controle=' + Geral.FF0(QrLotTxsControle.Value));
    Dmod.QrUpd.ExecSQL;
    //
    QrLotTxs.Next;
    FControlTxs := QrLotTxsFatParcela.Value;
    CalculaLote(QrLotCodigo.Value, True);
    LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
  end;
end;

procedure TFmLot2Cab.BtExclui7Click(Sender: TObject);
var
  AlinIts: Integer;
begin
{
  DmLot.QrLastCHDV.Close;
  DmLot.QrLastCHDV.Params[0].AsInteger := QrCHDevPFatParcRef.Value;
  UMyMod.AbreQuery(DmLot.QrLastCHDV);
}
  DmLot.ReopenLastCHDV(QrCHDevPFatParcRef.Value);
  //
  if DmLot.QrLastCHDVData.Value > QrCHDevPData.Value then
     Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    if Geral.MensagemBox(
    'Confirma a exclus�o do pagamento selecionado, e seus ' + #13#10 +
    'atrelamentos de taxa, multa, juros e desconto?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      AlinIts := QrCHDevPFatParcRef.Value;
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM alin pgs WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrCHDevPFatParcela.Value;
      Dmod.QrUpd.ExecSQL;
}
      if DmLot.Exclui_ChqPg(QrCHDevPControle.Value, QrCHDevPFatParcela.Value,
      QrCHDevPOcorreu.Value, QrCHDevPFatParcRef.Value, QrCHDevPFatGrupo.Value,
      True, QrCHDevPFatNum.Value) then
      begin
        CalculaLote(QrLotCodigo.Value, False);
  {
        DmLot.QrLastCHDV.Close;
        DmLot.QrLastCHDV.Params[0].AsInteger := AlinIts;
        UMyMod.AbreQuery(DmLot.QrLastCHDV);
  }
        DmLot.ReopenLastCHDV(AlinIts);
        //
        DmLot.CalculaPagtoAlinIts(DmLot.QrLastCHDVData.Value, AlinIts);
        //
        QrCHDevP.Next;
        FCHDevP := QrCHDevPFatParcela.Value;
        LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
        ReopenCHDevP;
        ReopenCHDevA(QrLotCliente.Value);
      end;
    end;
  end;
end;

procedure TFmLot2Cab.BtExclui8Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do item n� ' +
  IntToStr(DmLot.QrRepCliSEQ.Value) + ' de cheques de terceiros?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA);
    Dmod.QrUpd.SQL.Add('SET AlterWeb=1, RepCli = 0');
    Dmod.QrUpd.SQL.Add('WHERE FatID=' + FThisFatID_301);
    Dmod.QrUpd.SQL.Add('AND FatParcela=:P0');
    Dmod.QrUpd.Params[00].AsInteger := DmLot.QrRepCliFatParcela.Value;
    Dmod.QrUpd.ExecSQL;
    //
    CalculaLote(QrLotCodigo.Value, True);
    LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
  end;
end;

procedure TFmLot2Cab.BtExcluiClick(Sender: TObject);
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;  
  //
  if QrPagtos.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui'+
    'pagamentos!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DmLot.QrRepCli.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui pagamentos!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrDPago.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui' +
    ' duplicatas pagas nele!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrCHDevP.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui' +
    ' cheques pagos nele!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrOcorP.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui' +
    ' ocorr�ncias pagas nele!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrLotTxs.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui' +
    ' taxas inseridas nele!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if (QrLotIts.RecordCount = 0) and (QrLotCodigo.Value > 0) then
  begin
    if Geral.MensagemBox('Confirma a exclus�o do Border� - (' +
    'controle ' + IntToStr(QrLotCodigo.Value) + ')?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ' + CO_TabLotA);
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    end;
  end else
  begin
    Geral.MensagemBox('Este lote n�o pode ser exclu�do pois possui' +
      ' itens cadastrados nele!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
end;

procedure TFmLot2Cab.BtFinancasClick(Sender: TObject);
begin
  DmLct2.GerenciaEmpresa(nil, nil);
end;

procedure TFmLot2Cab.BtFTPClick(Sender: TObject);
begin
  //Importar(FmPrincipal.FDirR, il3_FTP);
end;

procedure TFmLot2Cab.BtHistoricoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMHistorico, BtHistorico);
end;

procedure TFmLot2Cab.BtImportaBCOClick(Sender: TObject);
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;
  //
  FmPrincipal.Importar(il4_BCO, QrLotItsFatParcela.Value);
end;

procedure TFmLot2Cab.BtInclui5Click(Sender: TObject);
begin
  FControlTxs := QrLotTxsFatParcela.Value;
  MostraLot2Txa(stIns);
end;

procedure TFmLot2Cab.BtIncluiClick(Sender: TObject);
begin
  FmPrincipal.FControlIts := QrLotItsFatParcela.Value;
  if QrLotCodigo.Value < 1 then BtInclui.Enabled := False
  else begin
    if QrLotTipo.Value = 0 then
      MostraLot2Chq(stIns)
    else
      MostraLot2Dup(stIns);
  end;
end;

procedure TFmLot2Cab.BtIncluiocorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIncluiOcor, BtIncluiOcor);
end;

procedure TFmLot2Cab.BtNFSeClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmLot2Cab.BtNovoClick(Sender: TObject);
begin
  if DModFin.FaltaPlaGenTaxas(LaAviso1, LaAviso2) > 0 then Exit;
  if DModFin.FaltaPlaGenOcorP(LaAviso1, LaAviso2) > 0 then Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMNovo, BtNovo);
end;

procedure TFmLot2Cab.BtOcorenciasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiJur1, FmEntiJur1, afmoNegarComAviso) then
  begin
    FmEntiJur1.ShowModal;
    FmEntiJur1.Destroy;
  end;
end;

procedure TFmLot2Cab.BtPagamento6Click(Sender: TObject);
var
  ValorBase: Double;
begin
  if DBCheck.CriaFm(TFmLot2Oco, FmLot2Oco, afmoNegarComAviso) then
  begin
    FmLot2Oco.ImgTipo.SQLType := stIns;
    //
{
    DmLot.QrLocOc.Close;
    DmLot.QrLocOc.Params[0].AsInteger := DmLot.QrOcorACodigo.Value;
    UMyMod.AbreQuery(DmLot.QrLocOc);
}
    DmLot.ReopenLocOc(DmLot.QrOcorACodigo.Value);
    //
    with FmLot2Oco do
    begin
      TPPagto6.MinDate := 0;
      if DmLot.QrLocOc.RecordCount > 0 then
      begin
        TPPagto6.Date     := Int(DmLot.QrLocOcData.Value);
        TPDataBase6.Date  := Int(DmLot.QrLocOcData.Value);
      end else begin
        TPPagto6.Date    := Int(DmLot.QrOcorADataO.Value);
        TPDataBase6.Date := Int(DmLot.QrOcorADataO.Value);
      end;
      EdJurosBase6.ValueVariant :=
        Dmod.ObtemTaxaDeCompraCliente(QrLotCliente.Value);
      ValorBase := DmLot.QrOcorAValor.Value + DmLot.QrOcorATaxaV.Value - DmLot.QrOcorAPago.Value;
      EdValorBase6.ValueVariant := ValorBase;
      TPPagto6.MinDate := Int(TPPagto6.Date);
      if Date > TPPagto6.MinDate then
        TPPagto6.Date := Int(Date);
      FmLot2Oco.CalculaJurosOcor();
      FmLot2Oco.CalculaAPagarOcor();
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.BtPagamento7Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagamento7, BtPagamento7);
end;

procedure TFmLot2Cab.BtPagTercClick(Sender: TObject);
begin
  Multiplaseleo1Click(Self);
end;

procedure TFmLot2Cab.BtPagtosAltera8Click(Sender: TObject);
var
  FatID_Sub, CliInt, Genero, Terceiro, Cod, Carteira: Integer;
  Valor: Double;
begin
  IC3_ED_FatNum := QrLotCodigo.Value;
  IC3_ED_NF := 0;
  IC3_ED_Data := QrLotData.Value;
  IC3_ED_Vencto := QrLotData.Value;
  //
  Cod       := QrLotCodigo.Value;
  Terceiro  := QrLotCliente.Value;
  //
  Valor     := QrPagtosDebito.Value;
  Genero    := QrPagtosGenero.Value;
  CliInt    := QrPagtosCliInt.Value;
  FatID_Sub := QrPagtosFatID_Sub.Value;
  Carteira  := QrPagtosCarteira.Value;
  //
  UPagtos.Pagto(QrPagtos, tpDeb, Cod, Terceiro, VAR_FATID_0302, Genero, CO_GenCtb_0, stUpd,
  'Pagamento de L�quido de Cliente', Valor, VAR_USUARIO, FatID_Sub, CliInt,
  mmNenhum, 0, 0, True, False, Carteira, 0, 0, 0, 0, CO_TabLctA);
  //
  DmLot.DesEncerraLote(Cod);
  LocCod(Cod, Cod);
end;

procedure TFmLot2Cab.BtPagtosExclui6Click(Sender: TObject);
var
  Ocorreu: Integer;
begin
{
  DmLot.QrLastOcor.Close;
  DmLot.QrLastOcor.Params[0].AsInteger := QrOcorPOcorreu.Value;
  DmLot.QrLastOcor.Open;
}
  DmLot.ReopenLastOcor(QrOcorPOcorreu.Value);
  //
  if (DmLot.QrLastOcorData.Value > QrOcorPData.Value)
  or ((DmLot.QrLastOcorData.Value = QrOcorPData.Value)
  and (DmLot.QrLastOcorFatParcela.Value > QrOcorPFatParcela.Value))
  then
     Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    if Geral.MensagemBox('Confirma a exclus�o deste pagamento?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorPOcorreu.Value;
{
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocor rpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorPFatParcela.Value;
        Dmod.QrUpd.ExecSQL;
}
        DmLot.Exclui_OcorP(QrOcorPControle.Value, QrOcorPFatParcela.Value,
          QrOcorPOcorreu.Value);
        //
        CalculaLote(QrLotCodigo.Value, False);
{
        DmLot.QrLastOcor.Close;
        DmLot.QrLastOcor.Params[0].AsInteger := Ocorreu;
        DmLot.QrLastOcor.Open;
}
        DmLot.ReopenLastOcor(Ocorreu);
        CalculaPagtoOcorP(DmLot.QrLastOcorData.Value, Ocorreu);
        //
        QrOcorP.Next;
        FOcorP := QrOcorPFatParcela.Value;
        LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
        ReopenOcorP();
        DmLot.ReopenOcorA(QrLotCliente.Value, FOcorA);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmLot2Cab.BtPagtosExclui8Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExcluiPagto, BtPagtosExclui8);
end;

procedure TFmLot2Cab.BtPagtosExclui9Click(Sender: TObject);
var
  LOIS: Integer;
begin
{
  DmLot.QrLastDPg.Close;
  DmLot.QrLastDPg.Params[0].AsInteger := QrDPagoLOIS.Value;
  DmLot.QrLastDPg. Open;
}
  DmLot.ReopenLastDPg(QrDPagoFatParcRef.Value);
  //
  if (DmLot.QrLastDPgData.Value > QrDPagoData.Value)
  or ((DmLot.QrLastDPgData.Value = QrDPagoData.Value) and
      (DmLot.QrLastDPgFatParcela.Value > QrDPagoFatParcela.Value))
  then
     Geral.MensagemBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Aviso', MB_OK+MB_ICONWARNING)
  else begin
    if Geral.MensagemBox('Confirma a exclus�o do pagamento selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      LOIS    := QrDPagoFatParcRef.Value;
      FOrelha := 4;
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM adup pgs WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrDPagoFatParcela.Value;
      Dmod.QrUpd.ExecSQL;
}
{
      DmLot.Exclui_DupPg(QrDPagoControle.Value, QrDPagoFatParcela.Value,
        QrDPagoOcorreu.Value);
}
      DmLot.Exclui_DupPg(QrDPagoControle.Value,
        QrDPagoFatParcela.Value, QrDPagoOcorreu.Value,
        QrDPagoFatParcRef.Value, QrDPagoFatGrupo.Value,
        True, QrDPagoFatNum.Value);
      //
      CalculaLote(QrLotCodigo.Value, True);
{
      DmLot.QrLastDPg.Close;
      DmLot.QrLastDPg.Params[0].AsInteger := LOIS;
      DmLot.QrLastDPg. Open;
}
      DmLot.ReopenLastDPg(QrDPagoFatParcRef.Value);
      CalculaPagtoLotIts(LOIS);
      //
      QrDPago.Next;
      FDPago := QrDPagoFatParcela.Value;
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
      ReopenDOpen(QrLotCliente.Value);
      ReopenDPago;
    end;
  end;
end;

procedure TFmLot2Cab.BtPagtosInclui8Click(Sender: TObject);
begin
  if Dmod.QrControleCartEspecie.Value = 0 then
  begin
    Geral.MensagemBox(
    'A "Carteira para pagamento r�pido" n�o foi definida nas op��es do aplicativo!'
    + #13#10 + 'Ser� aberta uma janela gen�rica para a inclus�o do pagamento!',
    'Aviso', MB_OK+MB_ICONINFORMATION);
    //
    IncluiPagamento();
  end else
    MyObjects.MostraPopUpDeBotao(PMPagto, BtPagtosInclui8);
end;

procedure TFmLot2Cab.BtPagtosInclui9Click(Sender: TObject);
begin
  Application.CreateForm(TFmGerDup1Main, FmGerDup1Main);
  //
  FmGerDup1Main.PainelPesq.Enabled     := False;
  FmGerDup1Main.EdCliente.ValueVariant := QrLotCliente.Value;
  FmGerDup1Main.CBCliente.KeyValue     := QrLotCliente.Value;
  FmGerDup1Main.EdDuplicata.Text       := DmLot.QrDOpenDuplicata.Value;
  FmGerDup1Main.EdCPF.Text             := Geral.FormataCNPJ_TT(DmLot.QrDOpenCNPJCPF.Value);
  FmGerDup1Main.EdEmitente.Text        := DmLot.QrDOpenEmitente.Value;
  FmGerDup1Main.FLotePgOrigem          := QrLotCodigo.Value;
  FmGerDup1Main.FCallFromLot           := True;
  //
  FmGerDup1Main.ReabrirTabelas;
  FmGerDup1Main.PreparaDuplicataPg(3, FmGerDup1Main.QrPesqFatParcela.Value);
  if FmGerDup1Main.ForcaOcorBank > 0 then
  begin
    FmGerDup1Main.ShowModal;
  end;
  FmGerDup1Main.Destroy;
  //
  FOrelha := 4;
  //
  CalculaLote(QrLotCodigo.Value, True);
  LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
end;

procedure TFmLot2Cab.BtRecalculaCHsClick(Sender: TObject);
{
var
  Codigo: Integer;
}
begin
{ que besteira!!!
  if DmLot.QrCHDevA.RecordCount > 0 then
  begin
    Codigo := DmLot.QrCHDevACodigo.Value;
    //
    DmLot.QrCHDevA.First;
    while not DmLot.QrCHDevA.Eof do
    begin
      CalculaJurosCHDev();
      CalculaAPagarCHDev();
      //
      DmLot.QrCHDevA.Next;
    end;
  end else
  Geral.MensagemBox('N�o h� itens a recalcular!', 'Aviso', MB_OK+MB_ICONWARNING);
}
end;

procedure TFmLot2Cab.BtRefreshClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRefresh, BtRefresh);
end;

procedure TFmLot2Cab.BtRiscoSacClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmRiscoAll_1, FmRiscoAll_1, afmoNegarComAviso) then
  begin
{??? Precisa?
    if PainelImporta.Visible then
    begin
      if Pagina.ActivePageIndex = 0 then
        FmRiscoAll_1.EdCPF.Text := Geral.FormataCNPJ_TT(DmLot1.QrLCHsCPF.Value)
      else FmRiscoAll_1.EdCPF.Text := Geral.FormataCNPJ_TT(DmLot1.QrLDUsCPF.Value)
    end else FmRiscoAll_1.EdCPF.Text := Geral.FormataCNPJ_TT(QrLotItsCNPJCPF.Value);
    FmRiscoAll_1.BtPesquisaClick(Self);
}
    FmRiscoAll_1.ShowModal;
    FmRiscoAll_1.Destroy;
  end;
end;

procedure TFmLot2Cab.FormCreate(Sender: TObject);
{
var
  Cam: String;
}
begin
  GradeCHE.Align := alClient;
  GradeDUP.Align := alClient;
  //
  Dmod.ReopenControle;
  FThisFatID_302 := FormatFloat('0', VAR_FATID_0302);
  FThisFatID_301 := FormatFloat('0', VAR_FATID_0301);
  //
  FEntidade := DModG.QrEmpresasCodigo.Value;
  FCliInt := DModG.QrEmpresasFilial.Value;
  //
  if (FCliInt <> 1) or (FEntidade <> -11) then
  begin
    Geral.MensagemBox('O Aplicativo ser� finalizado!' + #13#10 +
    'Filial: ' + IntToStr(FCliInt) + ' > Esperado: 1' + #13#10 +
    'Entidade: ' + IntToStr(FEntidade) + ' > Esperado: -11',
    'ERRO', MB_OK+MB_ICONERROR);
    //
    Halt(0);
  end;
  DModG.Def_EM_ABD(TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto,
        FTabLctA, FTabLctB, FTabLctD);
  //
  ImgTipo.SQLType := stLok;
  //
  QrOcorP.Database := DModG.MyPID_DB;
  //
  DmLot.QrExEnti1.Database := Dmod.MyDB1;
  DmLot.QrEmLot1.Database := Dmod.MyDB1;
  DmLot.QrSum1.Database := Dmod.MyDB1;
  //
  DmLot.QrExEnti2.Database := Dmod.MyDB2;
  DmLot.QrEmLot2.Database := Dmod.MyDB2;
  DmLot.QrSum2.Database := Dmod.MyDB2;
  //
  DmLot.QrExEnti3.Database := Dmod.MyDB3;
  DmLot.QrEmLot3.Database := Dmod.MyDB3;
  DmLot.QrSum3.Database := Dmod.MyDB3;
  //
  DmLot.QrExEnti4.Database := Dmod.MyDB4;
  DmLot.QrEmLot4.Database := Dmod.MyDB4;
  DmLot.QrSum4.Database := Dmod.MyDB4;
  //
  DmLot.QrExEnti5.Database := Dmod.MyDB5;
  DmLot.QrEmLot5.Database := Dmod.MyDB5;
  DmLot.QrSum5.Database := Dmod.MyDB5;
  //
  DmLot.QrExEnti6.Database := Dmod.MyDB6;
  DmLot.QrEmLot6.Database := Dmod.MyDB6;
  DmLot.QrSum6.Database := Dmod.MyDB6;
  //
{???
  if FmPrincipal.FConnections < FmPrincipal.FMyDBs.MaxDBs then
  begin
    Total1.Visible := False;
    Contabilidade1.Caption := '&Border�';
    BtCheckImp.Visible := False;
    //
    DBLaCPMF.Visible := False;
    DBEdCPMF.Visible := True;
    DBEdNF.Width := DBEdNF.Width + DBEdCPMF.Width + 4;
    //
  end else begin
    Total1.Visible := True;
    Contabilidade1.Caption := 'Border� &Cont�bil';
    BtCheckImp.Visible := True;
  end;
  Cam := Application.Title + '\XYConfig\PGAbertos';
  PGAbertos_CH.Height :=
  Geral.ReadAppKey('Altura',Cam,ktInteger,PGAbertos_CH.Height,HKEY_LOCAL_MACHINE);
}
  //
  FIncluindoCh := False;
  FIncluindoDU := False;
  //
  FArrayMySQLEE[00] := DmLot.QrExEnti1;
  FArrayMySQLEE[01] := DmLot.QrExEnti2;
  FArrayMySQLEE[02] := DmLot.QrExEnti3;
  FArrayMySQLEE[03] := DmLot.QrExEnti4;
  FArrayMySQLEE[04] := DmLot.QrExEnti5;
  FArrayMySQLEE[05] := DmLot.QrExEnti6;
  //
  FArrayMySQLLO[00] := DmLot.QrEmLot1;
  FArrayMySQLLO[01] := DmLot.QrEmLot2;
  FArrayMySQLLO[02] := DmLot.QrEmLot3;
  FArrayMySQLLO[03] := DmLot.QrEmLot4;
  FArrayMySQLLO[04] := DmLot.QrEmLot5;
  FArrayMySQLLO[05] := DmLot.QrEmLot6;
  //
  FArrayMySQLSI[00] := DmLot.QrSum1;
  FArrayMySQLSI[01] := DmLot.QrSum2;
  FArrayMySQLSI[02] := DmLot.QrSum3;
  FArrayMySQLSI[03] := DmLot.QrSum4;
  FArrayMySQLSI[04] := DmLot.QrSum5;
  FArrayMySQLSI[05] := DmLot.QrSum6;
  //
  GradeCHE.Align        := alClient;
  GradeDUP.Align        := alClient;
  DBGrid5.Align         := alClient;
  DBGrid10.Align        := alClient;
  Panel23.Align         := alClient;
{
  PainelEdita.Align     := alClient;
  PainelDados.Align     := alClient;
  PainelBanda.Align     := alClient;
  PainelPdxAdVal.Align  := alClient;
  PainelDigitaDup.Align := alClient;
  Pagina.Align          := alClient;
  Panel15.Align         := alClient;
  //
  GradeCH.ColCount := 2;
  GradeCH.RowCount := 2;
  GradeCH.ColWidths[00] := 240;
  GradeCH.ColWidths[01] := 80;
  GradeCH.Cells[00,00] := 'Coligada';
  GradeCH.Cells[01,00] := '% Compra';
  //
  GradeDU.ColCount := 2;
  GradeDU.RowCount := 2;
  GradeDU.ColWidths[00] := 240;
  GradeDU.ColWidths[01] := 80;
  GradeDU.Cells[00,00] := 'Coligada';
  GradeDU.Cells[01,00] := '% Compra';
  //
  ArqSCX.ConfiguraGrades(GradeC, GradeD, Pagina);
  //
  PgAbertos_DU.ActivePageIndex := 0;
}
  //
  CriaOForm;
  DmLot.QrClientes.Close;
  UMyMod.AbreQuery(DmLot.QrClientes, Dmod.MyDB);
  DmLot.QrCarteiras4.Close;
  UMyMod.AbreQuery(DmLot.QrCarteiras4, Dmod.MyDB);
  //
  PgPrincipal.ActivePageIndex  := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  //PageControl6.ActivePageIndex := 0;
  PgPrincipal.Align            := alClient;
  //
  //DmLot.ReopenTaxas();
  //
  DmLot2.FORDA_LCHs := 'il.Ordem';
  DmLot2.FORDB_LCHs := '';
  DmLot2.FORDA_LDUs := 'il.Ordem';
  DmLot2.FORDB_LDUs := '';
  //
  FLogo1Path := Geral.ReadAppKey('Logo1', Application.Title, ktString, '',
    HKEY_LOCAL_MACHINE);
  FLogo1Exists := FileExists(FLogo1Path);
  FmPrincipal.FLoteWeb := 0;
  FmPrincipal.FLoteWebDone := 0;
end;

procedure TFmLot2Cab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrLotCodigo.Value, LaRegistro.Caption);
end;

procedure TFmLot2Cab.SbImprimeClick(Sender: TObject);
begin
  if (QrLot.State = dsInactive) or (QrLot.RecordCount = 0) then Exit;  
  //
  FImprime := 2;
  MyObjects.MostraPopUpDeBotao(PMBordero, SbImprime);
end;

procedure TFmLot2Cab.SbNomeClick(Sender: TObject);
begin
  FmPrincipal.FLoteLoc := 0;
  if DBCheck.CriaFm(TFmLot0Loc, FmLot0Loc, afmoNegarComAviso) then
  begin
    FmLot0Loc.FFormCall := 1;
    FmLot0Loc.ShowModal;
    FmLot0Loc.Destroy;
    //
    if FmPrincipal.FLoteLoc <> 0 then
    LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
  end;
end;

procedure TFmLot2Cab.BtCheckImpClick(Sender: TObject);
begin
  FImprime := 0;
  //
  if DBCheck.CriaFm(TFmLot0Dlg, FmLot0Dlg, afmoNegarComAviso) then
  begin
    FmLot0Dlg.ShowModal;
    try
      if FImprime > 0 then
      begin
        FmLot0Dlg.Hide;
        ExecutaListaDeImpressoes();
      end;
    finally
      FmLot0Dlg.Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmLot2Cab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmLot2Cab.QrCHDevPAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) and (QrLotLctsAuto.Value = 0);
  Enab2 := (QrCHDevP.State <> dsInactive) and (QrCHDevP.RecordCount > 0);
  //
  BtExclui7.Enabled := Enab and Enab2;
end;

procedure TFmLot2Cab.QrCHDevPBeforeClose(DataSet: TDataSet);
begin
  BtExclui7.Enabled := False;
end;

procedure TFmLot2Cab.QrCHDevPCalcFields(DataSet: TDataSet);
begin
  // Precisa atrelar com AlinIts, para saber o saldo e o status
  QrCHDevPSALDO.Value := QrCHDevPValor.Value + QrCHDevPTaxas.Value +
    QrCHDevPMulta.Value + QrCHDevPJurosV.Value - QrCHDevPValPago.Value -
    QrCHDevPDesconto.Value - QrCHDevPPgDesc.Value;
  QrCHDevPNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrCHDevPStatus.Value);
  //
  QrCHDevPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevPCPF.Value);
end;

procedure TFmLot2Cab.QrDPagoAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) and (QrLotLctsAuto.Value = 0);
  Enab2 := (QrDPago.State <> dsInactive) and (QrDPago.RecordCount > 0);
  //
  BtPagtosExclui9.Enabled := Enab and Enab2;
end;

procedure TFmLot2Cab.QrDPagoBeforeClose(DataSet: TDataSet);
begin
  BtPagtosExclui9.Enabled := False;
end;

procedure TFmLot2Cab.QrDPagoCalcFields(DataSet: TDataSet);
begin
  QrDPagoNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrDPagoQuitado.Value);
  QrDPagoCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDPagoCNPJCPF.Value);
end;

procedure TFmLot2Cab.QrJuros0CalcFields(DataSet: TDataSet);
var
  Total: MyArrayR07;
begin
  Total := Dmod.ObtemValorDeCompraRealizado(QrJuros0FatParcela.Value);
  QrJuros0VlrOutras.Value := Total[0];
  QrJuros0VlrTotal.Value := Total[0] + QrJuros0VlrCompra.Value;
  //
  QrJuros0CPF_TXT.Value := Geral.FormataCNPJ_TT(QrJuros0CNPJCPF.Value);
  //
  QrJuros0ITEM.Value := 1;
end;

procedure TFmLot2Cab.QrLotAfterOpen(DataSet: TDataSet);
var
  Enab, Encerr: Boolean;
begin
  InfoTempo(Now, 'Lote reaberto', False);
  //
  //Enab := GOTOy.BtEnabled(QrLotCodigo.Value, False); 
  Enab   := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0);
  Encerr := QrLotLctsAuto.Value = 0;
  //
  BtAltera.Enabled   := Enab and Encerr;
  BtExclui.Enabled   := Enab and Encerr;
  SbImprime.Enabled  := Enab;
  BtCheckImp.Enabled := Enab;
  BtInclui.Enabled   := Geral.IntToBool_0(QrLotLote.Value);
  BtEncerra.Enabled  := True;//QrLotLctsAuto.Value = 0;
end;

procedure TFmLot2Cab.QrLotAfterScroll(DataSet: TDataSet);
var
  CorA, CorC: TColor;
  Titulo2: String;
begin
  if QrLotTipo.Value = 0 then
  begin
    GradeCHE.Visible := True;
    GradeDUP.Visible := False;
  end else begin
    GradeDUP.Visible := True;
    GradeCHE.Visible := False;
  end;
  //
  //InfoTempo(Now, 'Rolagem', False);  // n�o precisa. � zero.
  DefineRiscos(QrLotCliente.Value);
  Memo1.Lines.Add('========================================== Fim reaberturas');
  PgPrincipal.ActivePageIndex := FOrelha;
  //
  if QrLotLctsAuto.Value = 0 then
  begin
    CorA := $00D5DCFF;
    CorC := $003535FF;
    Titulo2 := 'Aberto!';
  end else begin
    CorA := $00FFD9C6;
    CorC := $00FF5A00;
    Titulo2 := 'Encerrado';
  end;
  LaTitulo2A.Font.Color := CorA;
  //LaTitulo2B.Font.Color := CorB;
  LaTitulo2C.Font.Color := CorC;
  //
  LaTitulo2A.Caption := Titulo2;
  LaTitulo2B.Caption := Titulo2;
  LaTitulo2C.Caption := Titulo2;
  //
  {
  LaTitulo2A.Visible := True;
  LaTitulo2B.Visible := True;
  LaTitulo2C.Visible := True;
  }
end;

procedure TFmLot2Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2Cab.FormatoBP1Click(Sender: TObject);
begin
  ImprimeBorderoClienteBP();
end;

procedure TFmLot2Cab.SbQueryClick(Sender: TObject);
begin
  SbNomeClick(Self);
end;

procedure TFmLot2Cab.Semomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(False);
end;

procedure TFmLot2Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Cab.frxAditivo_NPGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_HOJE' then Value := FormatDateTime(VAR_FORMATDATE2, Date)
  else if VarName = 'VARF_CHQS' then Value := QrLotIts.RecordCount
  else if VarName = 'VARF_MOEDA' then Value := Dmod.QrControleMoeda.Value
  else if VarName = 'Coligada0' then Value := Dmod.QrMasterEm.Value
  else if VarName = 'Coligada1' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 1 then
    begin
      if FmPrincipal.FMyDBs.Connected[0] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[0]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada2' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 2 then
    begin
      if FmPrincipal.FMyDBs.Connected[1] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[1]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada3' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 3then
    begin
      if FmPrincipal.FMyDBs.Connected[2] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[2]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada4' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 4 then
    begin
      if FmPrincipal.FMyDBs.Connected[3] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[3]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada5' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 5 then
    begin
      if FmPrincipal.FMyDBs.Connected[4] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[4]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada6' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 6 then
    begin
      if FmPrincipal.FMyDBs.Connected[5] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[5]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'TaxaJuros0' then Value := QrLotItsTxaCompra.Value
  else if VarName = 'TaxaJuros1' then Value := FDadosJurosColigadas[1][0]
  else if VarName = 'TaxaJuros2' then Value := FDadosJurosColigadas[2][0]
  else if VarName = 'TaxaJuros3' then Value := FDadosJurosColigadas[3][0]
  else if VarName = 'TaxaJuros4' then Value := FDadosJurosColigadas[4][0]
  else if VarName = 'TaxaJuros5' then Value := FDadosJurosColigadas[5][0]
  else if VarName = 'TaxaJuros6' then Value := FDadosJurosColigadas[6][0]
  else if VarName = 'TaxaJuros7' then Value := FDadosJurosColigadas[0][0]

  else if VarName = 'JurosPeriodo0' then Value := QrLotItsTxaJuros.Value
  else if VarName = 'JurosPeriodo1' then Value := FDadosJurosColigadas[1][1]
  else if VarName = 'JurosPeriodo2' then Value := FDadosJurosColigadas[2][1]
  else if VarName = 'JurosPeriodo3' then Value := FDadosJurosColigadas[3][1]
  else if VarName = 'JurosPeriodo4' then Value := FDadosJurosColigadas[4][1]
  else if VarName = 'JurosPeriodo5' then Value := FDadosJurosColigadas[5][1]
  else if VarName = 'JurosPeriodo6' then Value := FDadosJurosColigadas[6][1]
  else if VarName = 'JurosPeriodo7' then Value := FDadosJurosColigadas[0][1]

  else if VarName = 'ValorJuros0' then Value := QrLotItsVlrCompra.Value
  else if VarName = 'ValorJuros1' then Value := FDadosJurosColigadas[1][2]
  else if VarName = 'ValorJuros2' then Value := FDadosJurosColigadas[2][2]
  else if VarName = 'ValorJuros3' then Value := FDadosJurosColigadas[3][2]
  else if VarName = 'ValorJuros4' then Value := FDadosJurosColigadas[4][2]
  else if VarName = 'ValorJuros5' then Value := FDadosJurosColigadas[5][2]
  else if VarName = 'ValorJuros6' then Value := FDadosJurosColigadas[6][2]
  else if VarName = 'ValorJuros7' then Value := FDadosJurosColigadas[0][2]
  else if VarName = 'VARF_TOTAL_EXTENSO' then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(QrLotTotal.Value, 2, siPositivo))
  else if VarName = 'VARF_CIDADE_E_DATA' then
    Value := DModG.QrDonoCIDADE.Value
    + ', '+FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLotData.Value)
  else if VarName = 'VARF_ENDERECO_CLIENTE' then Value :=
    GOTOy.EnderecoDeEntidade(QrLotCliente.Value, 0)
  else if VarName = 'NUM_CONT' then
    Value := QrContratContrato.Value
  else if VarName = 'DATA_CON' then Value :=
    FormatDateTime(VAR_FORMATDATE2, QrContratDataC.Value)
  else if VarName = 'LIMITE  ' then Value := QrContratLimite.Value
  else if VarName = 'ADITIVO ' then Value := QrLotLote.Value
  else if VarName = 'PLU1_TIT' then
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := '' else Value := 's'
  end
  else if VarName = 'PLU2_TIT' then
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := 'i' else Value := 'ram'
  end
  else if VarName = 'PLU3_TIT' then
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := '�' else Value := '�o'
  end
  else if VarName = 'PLU4_TIT' then
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := '' else Value := 'em'
  end
  else if VarName = 'PLU5_TIT' then                     
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := 'l' else Value := 'is'
  end
  else if VarName = 'PLU6_TIT' then                     
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := '�o' else Value := '�es'
  end
  else if VarName = 'PLU7_TIT' then
  begin
    if DmLot.QrDupSac.RecordCount < 2 then Value := '' else Value := 'es'
  end
  else if VarName = 'NOME_SAC' then
    Value := QrSaCartNome.Value
  else if VarName = 'NOME_EMIT' then
    Value := QrSaEmitCHEmitente.Value
  else if VarName = 'DUPLISAC' then
  begin
    DmLot.QrDupSac.First;
    Value :=
    'N� Duplicata  Emitida em   Valor de R$   Vencimento Banco e ag�ncia para pagamento'+Chr(10)+Chr(13)+
    '------------------------------------------------------------------------------------------------'+Chr(10)+Chr(13);
    while not DmLot.QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(DmLot.QrDupSacDuplicata.Value,' ', 14, taLeftJustify, True)+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLot.QrDupSacData.Value), ' ', 11, taLeftJustify, True)+
      Geral.CompletaString(Geral.FFT(DmLot.QrDupSacCredito.Value, 2, siPositivo), ' ', 13, taRightJustify, True)+ '   '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLot.QrDupSacVencimento.Value), ' ', 10, taLeftJustify, True)+ ' '+
      Geral.CompletaString(MLAGeral.FFD(DmLot.QrDupSacBanco.Value, 3, siPositivo), ' ', 3, taRightJustify, True)+'/'+
      Geral.CompletaString(MLAGeral.FFD(DmLot.QrDupSacAgencia.Value, 4, siPositivo), ' ', 4, taRightJustify, True) + ' ' +
      Geral.CompletaString(DmLot.QrDupSacNOMEBANCO.Value, ' ', 35, taLeftJustify, True)+
      Chr(10)+Chr(13);
      //
      DmLot.QrDupSac.Next;
    end;
  end
  else if VarName = 'NOME_CLI' then Value := QrLotNOMECLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_NOTEOF') = 0 then
  begin
    if QrSaCart.RecNo < QrSaCart.RecordCount then Value := True else Value := False;
  end
  else if AnsiCompareText(VarName, 'VARF_NOTEOF2') = 0 then
  begin
    if QrSaEmitCH.RecNo < QrSaEmitCH.RecordCount then Value := True else Value := False;
  end  
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := DModG.QrDonoCIDADE.Value + ', ' +
    FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLotData.Value)
  else FmPrincipal.TextosCartasGetValue(VarName, Value);
end;

procedure TFmLot2Cab.frxBorTotCli2GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_HOJE' then
  begin
    Value := FormatDateTime(VAR_FORMATDATE2, Date);
    FContaST := 0;
  end
  else if VarName = 'VARF_CHQS' then Value := QrLotIts.RecordCount
  else if VarName = 'VARF_MOEDA' then Value := Dmod.QrControleMoeda.Value
  else if VarName = 'VAR_CONTAST' then
  begin
    FContaST := FContaST +1;
    Value := FContaST;// repasse
  end else if VarName = 'VARF_CHDEV' then Value := QrCHDevP.RecordCount
  else if VarName = 'VARF_DUDEV' then Value := QrDPago.RecordCount
  else if VarName = 'VARF_NOMEEMP' then
  begin
    if Dmod.QrControleCodCliRel.Value = 1 then
      Value := FormatFloat('0', QrLotcliente.Value) + ' - '
    else
      Value := '';
    case Dmod.QrControleTipNomeEmp.Value of
      0: Value := Value + QrLotNOMECLIENTE.Value;
      1: Value := Value + QrLotNOMEFANCLIENTE.Value;
    end;
  end;
end;

function TFmLot2Cab.frxBorTotCli2UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = 'VARF_CHDEV' then Params := QrCHDevP.RecordCount
  else if MethodName = 'VARF_DUDEV' then Params := QrDPago.RecordCount
end;

procedure TFmLot2Cab.frxCartaSacadoDadosGetValue(const VarName: string;
  var Value: Variant);
var
  Altura: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_CSD_AltuHeader') = 0 then
    Value := Int(Dmod.QrControleCSD_TopoCidata.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuCidata') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoDestin.Value - Dmod.QrControleCSD_TopoCidata.Value;
    Value := Int(Altura / VAR_frCM)
  end
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuDestin') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoDuplic.Value - Dmod.QrControleCSD_TopoDestin.Value;
    Value := Int(Altura / VAR_frCM)
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuDuplic') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoClient.Value - Dmod.QrControleCSD_TopoDuplic.Value;
    Value := Int(Altura / VAR_frCM)
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuFooter') = 0 then
  begin
    Altura := 29700 - 500 - 4500 - Dmod.QrControleCSD_TopoClient.Value;
    Value := Int(Altura / VAR_frCM)
  //
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuHeade2') = 0 then
    Value := Int(Dmod.QrControleCSD_TopoDesti2.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuDesti2') = 0 then
  begin
    Altura := 26000 (*- 4500*) - Dmod.QrControleCSD_TopoDesti2.Value;
    Value := Int(Altura / VAR_frCM)
  //
  end else if AnsiCompareText(VarName, 'VARF_CSD_MEsqCidata') = 0 then
    Value := Dmod.QrControleCSD_MEsqCidata.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDestin') = 0 then
    Value := Dmod.QrControleCSD_MEsqDestin.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDuplic') = 0 then
    Value := Dmod.QrControleCSD_MEsqDuplic.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqClient') = 0 then
    Value := Dmod.QrControleCSD_MEsqClient.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDesti2') = 0 then
    Value := Dmod.QrControleCSD_MEsqDesti2.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := DModG.QrDonoCIDADE.Value + ', '+
    FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLotData.Value)
  else if AnsiCompareText(VarName, 'DUPLISACDADOS') = 0  then
  begin
    DmLot.QrDupSac.First;
    Value := '';
    //'N� Duplicata  Emitida em   Valor de R$   Vencimento Banco e ag�ncia para pagamento'+Chr(10)+Chr(13)+
    //'------------------------------------------------------------------------------------------------'+Chr(10)+Chr(13);
    while not DmLot.QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(DmLot.QrDupSacDuplicata.Value,' ', 16, taLeftJustify, True)+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLot.QrDupSacData.Value), ' ', 10, taLeftJustify, True)+
      Geral.CompletaString(Geral.FFT(DmLot.QrDupSacCredito.Value, 2, siPositivo), ' ', 14, taRightJustify, True)+ '     '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLot.QrDupSacVencimento.Value), ' ', 10, taLeftJustify, True)+ ' '+
      '    ' +
      Geral.CompletaString(MLAGeral.FFD(DmLot.QrDupSacBanco.Value, 3, siPositivo), ' ', 3, taRightJustify, True)+'/'+
      Geral.CompletaString(MLAGeral.FFD(DmLot.QrDupSacAgencia.Value, 4, siPositivo), ' ', 4, taRightJustify, True)+ ' ' +
      Geral.CompletaString(DmLot.QrDupSacNOMEBANCO.Value, ' ', 35, taLeftJustify, True)
      +Chr(10)+Chr(13);
      //
      DmLot.QrDupSac.Next;
    end;
  end
  else if AnsiCompareText(VarName, 'NOME_CLI') = 0 then Value := QrLotNOMECLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_NOTEOF') = 0 then
  begin
    if QrSaCart.RecNo < QrSaCart.RecordCount then Value := True else Value := False;
  end
  else ;
end;

procedure TFmLot2Cab.frxCartaSacadoVersoTudoGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_COLORENV') = 0 then
  begin
    if Dmod.QrControleColoreEnvelope.Value = 1 then Value := $EBEBEB
    else Value := $FFFFFF;
  end
  else ;
end;

procedure TFmLot2Cab.frxNF_TudoGetValue(const VarName: string;
  var Value: Variant);
var
  Txt: String;
begin
  if AnsiCompareText(VarName, 'VARF_NFLINHA1') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha1.Value,
      '[TAXA_ADVAL]', QrLotADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[VAL_FATCOMPR]',
      Geral.FFT(QrLotMINTC.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[VAL_ADVAL]',
      Geral.FFT(QrLotMINAV.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA2') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha2.Value,
      '[TAXA_ADVAL]', QrLotADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[VAL_FATCOMPR]',
      Geral.FFT(QrLotMINTC.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[VAL_ADVAL]',
      Geral.FFT(QrLotMINAV.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA3') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha3.Value,
      '[TAXA_ADVAL]', QrLotADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[VAL_FATCOMPR]',
      Geral.FFT(QrLotMINTC.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[VAL_ADVAL]',
      Geral.FFT(QrLotMINAV.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_TOTNF') = 0 then
  begin
    if (Pos('[VAL_FATCOMPR]', Dmod.QrControleNFLinha1.Value) > 0) or
      (Pos('[VAL_FATCOMPR]', Dmod.QrControleNFLinha2.Value) > 0) or
      (Pos('[VAL_FATCOMPR]', Dmod.QrControleNFLinha3.Value) > 0)
    then
      Value := QrLotMINAV.Value + QrLotMINTC.Value
    else
      Value := QrLotMINAV.Value; 
  end
  else if AnsiCompareText(VarName, 'IMPOSTO') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFAliq.Value, '[IMPOSTO]',
      Geral.FFT(QrLotISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'AlturaInicial') = 0 then
  begin
    Value := Int(Dmod.QrControleNF_Cabecalho.Value / VAR_frCM);//37.7953);
  end
  else if AnsiCompareText(VarName, 'VARF_MEUENDERECO') = 0 then
  begin
    if FMEU_ENDERECO then Value := DModG.QrDonoE_CUC.Value
    else Value := ' ';
  end
end;

function TFmLot2Cab.GetTipoBorderoAtual(): TLoteBordero;
begin
  case QrLotTipo.Value of
    0: Result := loteCheque;
    1: Result := loteDuplicata;
    else
    begin
      Result := loteUnknow;
      Geral.MensagemBox('Tipo de documento n�o definido!',
      'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

function TFmLot2Cab.GetTitle(TipoLote: Integer): String;
begin
  case TipoLote of
    0: Result := 'Cheques';
    1: Result := 'Duplicatas';
    else Result := '???'
  end;
end;

procedure TFmLot2Cab.ImprimeAditivoAPNPNovo();
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  Dmod.VerificaDefinicaoDeTextoPadrao(3);
  ReopenContrat();
  ReopenSaCart(istTodos);
  //
  ReopenFiadores();
  ReopenTestemunhas();
  //
  case FImprime of
    1: MyObjects.frxImprime(frxAditivo_AP_NP, 'Aditivo + Autoriza��o de protesto + Nota Promoss�ria');
    2: MyObjects.frxMostra(frxAditivo_AP_NP, 'Aditivo + Autoriza��o de protesto + Nota Promoss�ria');
  end;
end;

procedure TFmLot2Cab.ImprimeAditivoNovo();
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  ReopenContrat;
  //
  ReopenFiadores();
  ReopenTestemunhas();
  //
  frxAditivo.PrepareReport;
  case FImprime of
    1: frxAditivo.Print;//PreparedReport('', 1, True, frAll);
    2: MyObjects.frxMostra(frxAditivo, 'Aditivo');
  end;
end;

procedure TFmLot2Cab.ImprimeAditivoNPAPNovo();
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  Dmod.VerificaDefinicaoDeTextoPadrao(3);
  ReopenContrat;
  ReopenSaCart(istTodos);
  //
  ReopenFiadores();
  ReopenTestemunhas();
  //
  case FImprime of
    1: MyObjects.frxImprime(frxAditivo_NP_AP, 'Aditivo + Nota promiss�ria + Autoriza��o de protesto');
    2: MyObjects.frxMostra(frxAditivo_NP_AP, 'Aditivo + Nota promiss�ria + Autoriza��o de protesto');
  end;
end;

procedure TFmLot2Cab.ImprimeAditivoNPNovo();
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  ReopenContrat;
  //
  ReopenFiadores();
  ReopenTestemunhas();
  //
  frxAditivo_NP.PrepareReport;
  case FImprime of
    1: frxAditivo_NP.Print;//PreparedReport('', 1, True, frAll);
    2: MyObjects.frxMostra(frxAditivo_NP, 'Aditivo + NP');
  end;
end;

procedure TFmLot2Cab.ImprimeAutorizacaodeProtestoNovo();
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(3);
  ReopenSaCart(istTodos);
  //
  case FImprime of
    1: MyObjects.frxImprime(frxProtesto, 'Autoriza��o de Protesto');
    2: MyObjects.frxMostra(frxProtesto, 'Autoriza��o de Protesto');
  end;
end;

procedure TFmLot2Cab.ImprimeBorderoClienteBP();
begin
  FContaST := 0;
{
  QrSumOP.Close;
  QrSumOP.Params[0].AsInteger := QrLotCodigo.Value;
  UMyMod.AbreQuery(QrSumOP);
}
  ReopenSumOP(QrLotCodigo.Value);
  //
  if QrLotMINAV.Value+QrLotMINTC.Value>=0.01 then
  begin
    if QrLotTipo.Value = 0 then
    begin
      frxBorTotCliE2.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE2.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE2.Print; //PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP2, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end else begin
      frxBorTotCliE3.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE3.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE3.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP3, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end;
  end else begin
    if QrLotTipo.Value = 0 then
    begin
      frxBorTotCliE0.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE0.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE0.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP0, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end else begin
      frxBorTotCliE1.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE1.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE1.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP1, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end;
  end;
end;

procedure TFmLot2Cab.ImprimeBorderoClienteE2();
begin
  FContaST := 0;
{
  QrSumOP.Close;
  QrSumOP.Params[0].AsInteger := QrLotCodigo.Value;
  UMyMod.AbreQuery(QrSumOP);
}
  ReopenSumOP(QrLotCodigo.Value);
  //
  if QrLotMINAV.Value+QrLotMINTC.Value>=0.01 then
  begin
    if QrLotTipo.Value = 0 then
    begin
      frxBorTotCliE2.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE2.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE2.Print; //PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE2, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end else begin
      frxBorTotCliE3.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE3.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE3.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE3, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end;
  end else begin
    if QrLotTipo.Value = 0 then
    begin
      frxBorTotCliE0.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE0.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE0.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE0, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end else begin
      frxBorTotCliE1.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE1.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE1.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE1, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end;
  end;
end;

procedure TFmLot2Cab.ImprimeBorderoClienteSobra(Dias: Boolean);
begin
  FContaST := 0;
{
  QrSumOP.Close;
  QrSumOP.Params[0].AsInteger := QrLotCodigo.Value;
  UMyMod.AbreQuery(QrSumOP);
}
  ReopenSumOP(QrLotCodigo.Value);
  //
  if QrLotMINAV.Value+QrLotMINTC.Value>=0.01 then
  begin
    if QrLotTipo.Value = 0 then
    begin
      frxBorTotCli2S.Variables['VARF_DIAS']  := MLAGeral.BoolToInt(Dias);
      frxBorTotCli2S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli2S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli2S.Print; //PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli2S, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end else begin
      frxBorTotCli3S.Variables['VARF_DIAS']  := MLAGeral.BoolToInt(Dias);
      frxBorTotCli3S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli3S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli3S.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli3S, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end;
  end else begin
    if QrLotTipo.Value = 0 then
    begin
      frxBorTotCli0S.Variables['VARF_DIAS']  := MLAGeral.BoolToInt(Dias);
      frxBorTotCli0S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli0S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli0S.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli0S, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end else begin
      frxBorTotCli1S.Variables['VARF_DIAS']  := MLAGeral.BoolToInt(Dias);
      frxBorTotCli1S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli1S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli1S.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli1S, 'Border� cliente n� '+IntToStr(QrLotLote.Value));
      end;
    end;
  end;
end;

procedure TFmLot2Cab.ImprimeBorderoContabilNovo();
begin
  if QrLotMINAV.Value+QrLotMINTC.Value>=0.01 then
  begin
    if QrLotTipo.Value = 0 then
    begin
      case FImprime of
        1: MyObjects.frxImprime(frxBorProFis0_A, 'Border� Cont�bil');
        2: MyObjects.frxMostra(frxBorProFis0_A, 'Border� Cont�bil');
      end;
    end else begin
      case FImprime of
        1: MyObjects.frxImprime(frxBorProFis1_A, 'Border� Cont�bil');
        2: MyObjects.frxMostra(frxBorProFis1_A, 'Border� Cont�bil');
      end;
    end;
  end;
end;

procedure TFmLot2Cab.ImprimeCartaAoSacadoDados();
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(4);
  if ReopenSaCart(istPergunta) then
  begin
    //frCartaSacadoDados.PrepareReport;
    case FImprime of
      1: MyObjects.frxImprime(frxCartaSacadoDados, 'Carta ao sacado (frente)');//.PrintPreparedReport('', 1, True, frAll);
      2: MyObjects.frxMostra(frxCartaSacadoDados, 'Carta ao sacado (frente)');
      //2: frCartaSacadoDados.ShowPreparedReport;
    end;
    if Dmod.QrControleEnvelopeSacado.Value = 1 then
    begin
      DModG.QrDono.Close;
      UMyMod.AbreQuery(DModG.QrDono, Dmod.MyDB);
      //frCartaSacadoVersoDados.PrepareReport;
      //frCartaSacadoVersoDados.ShowPreparedReport;
      MyObjects.frxMostra(frxCartaSacadoVersoDados, 'Carta ao sacado (verso)');
    end;
    AtualizaImpressaoDeCarta(QrLotCodigo.Value, 1);
  end;
end;

procedure TFmLot2Cab.ImprimeCartaAoSacadoTudoNovo(Modelo: Integer);
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(4);
  if ReopenSaCart(istPergunta) then
  begin
    case FImprime of
      1:
      begin
        if Modelo = 1 then
          MyObjects.frxImprime(frxCartaSacadoTudo, 'Carta ao sacado')
        else
          MyObjects.frxImprime(frxCartaSacadoTudo2, 'Carta ao sacado');
      end;
      2:
      begin
        if Modelo = 1 then
          MyObjects.frxMostra(frxCartaSacadoTudo, 'Carta ao sacado')
        else
          MyObjects.frxMostra(frxCartaSacadoTudo2, 'Carta ao sacado')
      end;
    end;
    if Dmod.QrControleEnvelopeSacado.Value = 1 then
    begin
      DModG.QrDono.Close;
      UMyMod.AbreQuery(DModG.QrDono, Dmod.MyDB);
      MyObjects.frxMostra(frxCartaSacadoVersoTudo, 'Verso da carta ao sacado');
    end;
    AtualizaImpressaoDeCarta(QrLotCodigo.Value, 1);
  end;
end;

procedure TFmLot2Cab.ImprimeMargemColigadasNovo();
begin
  FImprimindoColigadas := True;
  FDadosJurosColigadas := Dmod.ObtemEmLotIts(QrLotItsFatParcela.Value,
    QrLotItsTxaCompra.Value, QrLotItsTxaJuros.Value,
    QrLotItsVlrCompra.Value);
  if QrLotTipo.Value = 0 then
  begin
    case FImprime of
      1: MyObjects.frxImprime(frxColigadas0, 'Margem de Coligadas');
      2: MyObjects.frxMostra(frxColigadas0, 'Margem de Coligadas');
    end;
  end else begin
    case FImprime of
      1: MyObjects.frxImprime(frxColigadas1, 'Margem de Coligadas');
      2: MyObjects.frxMostra(frxColigadas1, 'Margem de Coligadas');
    end;
  end;
  FImprimindoColigadas := False;
end;

procedure TFmLot2Cab.ImprimeNF(MeuEndereco: Boolean);
begin
  if DefineDataSetsNF(frxNF_Dados) then
  begin
    FMEU_ENDERECO := MeuEndereco;
    GOTOy.EnderecoDeEntidade(QrLotCliente.Value, 0);
    MyObjects.frxMostra(frxNF_Dados, 'Nota Fiscal n� '+IntToStr(QrLotNF.Value));
  end;
end;

procedure TFmLot2Cab.ImprimeNotaPromissoriaNovo();
begin
  frxPromissoria.PrepareReport;
  case FImprime of
    1: frxPromissoria.Print;//PreparedReport('', 1, True, frAll);
    2: MyObjects.frxMostra(frxPromissoria, 'Nota promiss�ria');
  end;
end;

procedure TFmLot2Cab.IncluiLoteCab(SQLType: TSQLType; TipoBordero:
TLoteBordero; AlteraTodos, Importando: Boolean;
Cliente: Integer; Data: TDateTime);
var
  I, Lins: Integer;
begin
  if DBCheck.CriaFm(TFmLot2Cad, FmLot2Cad, afmoNegarComAviso) then
  begin
    with FmLot2Cad do
    begin
      ImgTipo.SQLType := SQLType;
      FTipoBordero := TipoBordero;
      FAlteraTodos := AlteraTodos;
      if AlteraTodos then
        MyObjects.Informa2(FmLot2Cad.LaAviso1, FmLot2Cad.LaAviso2, False,
'Ao confirmar, o D+ e a taxa de compra de todos itens deste border� ser�o alterados!'
        );
      PainelItens.Visible := True;
      //
      if Importando then
      begin
        EdLote.ValueVariant      := 0;
        EdAdValorem.ValueVariant := 0;
        EdNF.ValueVariant        := 0;
        EdCBE.ValueVariant       := 0;
        CkSCB.Checked            := False;
        EdNF.Enabled             := True;
        GradeAV.RowCount         := 2;
        MyObjects.LimpaGrade(GradeAV, 1, 1, True);
        Lins := FmPrincipal.FMyDBs.MaxDBs +1;
        if Lins < 2 then Lins := 2;
        GradeAV.RowCount := Lins;
        //EdCodigo.ValueVariant := Codigo;
        EdPIS.ValueVariant  := Dmod.QrControlePIS.Value;
        EdPIS_R.ValueVariant  := Dmod.QrControlePIS_R.Value;
        EdCOFINS.ValueVariant  := Dmod.QrControleCOFINS.Value;
        EdCOFINS_R.ValueVariant  := Dmod.QrControleCOFINS_R.Value;
        EdISS.ValueVariant    := Dmod.QrControleISS.Value;
        EdIRRF.ValueVariant   := 0;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
          end;
        end;
        //EdCliente.SetFocus;
        if FIncluindoDU then
        begin
          EdBanco0.ValueVariant := 0;
          EdAgencia0.ValueVariant := 0;
          PainelBco0.Visible := True;
        end;
      end else begin
        EdCliente.ValueVariant   := 0;
        CBCliente.KeyValue       := Null;
        EdLote.ValueVariant      := 0;
        EdAdValorem.ValueVariant := 0;
        GradeAV.RowCount := 2;
        MyObjects.LimpaGrade(GradeAV, 1, 1, True);
        Lins := FmPrincipal.FMyDBs.MaxDBs +1;
        if Lins < 2 then Lins := 2;
        GradeAV.RowCount := Lins;
        if SQLType = stIns then
        begin
          LaCartDep1.Visible := False;
          EdCartDep1.Visible := False;
          CBCartDep1.Visible := False;
          //
          if FTipoLote = 0 then
          begin
            EdCartDep1.ValueVariant := Dmod.QrControleCartCheques.Value;
            CBCartDep1.KeyValue     := Dmod.QrControleCartCheques.Value;
          end else
          begin
            EdCartDep1.ValueVariant := Dmod.QrControleCartDuplic.Value;
            CBCartDep1.KeyValue     := Dmod.QrControleCartDuplic.Value;
          end;
          //
          //EdCodigo.ValueVariant   := Codigo;
          EdPIS.ValueVariant      := Dmod.QrControlePIS.Value;
          EdPIS_R.ValueVariant    := Dmod.QrControlePIS_R.Value;
          EdCOFINS.ValueVariant   := Dmod.QrControleCOFINS.Value;
          EdCOFINS_R.ValueVariant := Dmod.QrControleCOFINS_R.Value;
          EdISS.ValueVariant      := Dmod.QrControleISS.Value;
          EdCPMF.ValueVariant     := Dmod.QrControleCPMF.Value;
          EdIRRF.ValueVariant     := 0;
          EdNF.ValueVariant       := 0;
          EdCBE.ValueVariant      := 0;
          CkSCB.Checked           := False;
          EdNF.Enabled            := True;
          for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
            if FmPrincipal.FMyDBs.Connected[i] = '1' then
              GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];

        end else begin
          LaCartDep1.Visible := False;
          EdCartDep1.Visible := False;
          CBCartDep1.Visible := False;
          //
          EdCodigo.ValueVariant      := QrLotCodigo.Value;
          EdCliente.ValueVariant     := IntToStr(QrLotCliente.Value);
          CBCliente.KeyValue         := QrLotCliente.Value;
          EdLote.ValueVariant        := IntToStr(QrLotLote.Value);
          TPData.Date                := QrLotData.Value;
          RGTipo.ItemIndex           := 0;
          //
          EdPIS.ValueVariant       := QrLotPIS.Value;
          EdPIS_R.ValueVariant     := QrLotPIS_R.Value;
          EdCOFINS.ValueVariant    := QrLotCOFINS.Value;
          EdCOFINS_R.ValueVariant  := QrLotCOFINS_R.Value;
          EdISS.ValueVariant       := QrLotISS.Value;
          EdCPMF.ValueVariant      := QrLotCPMF.Value;
          EdIRRF.ValueVariant      := QrLotIRRF.Value;
          EdNF.ValueVariant        := QrLotNF.Value;
          EdCBE.ValueVariant       := QrLotCBE.Value;
          CkSCB.Checked    := Geral.IntToBool_0(QrLotSCB.Value);
          if QrLotNF.Value = 0 then
            EdNF.ReadOnly := False
          else
            EdNF.ReadOnly := True;
          //
          EdAdValorem.DecimalSize  := 6;//FCasasAdValorem; // evitar erro (altera�ao de taxa!)
          EdAdValorem.ValueVariant := QrLotAdValorem.Value;
          //
          for I := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
          begin
            if FmPrincipal.FMyDBs.Connected[I] = '1' then
            begin
              GradeAV.Cells[00, I + 1] := FmPrincipal.FMyDBs.IDName[i];
              GradeAV.Cells[01, I + 1] := Geral.FFT(
                FArrayMySQLLO[I].FieldByName('AdValorem').AsFloat, 6, siPositivo);
            end;
          end;
        end;
        if Cliente <> 0 then
        begin
          EdCliente.ValueVariant := Cliente;
          CBCliente.KeyValue     := Cliente;
        end;
        if Data > 2 then
          TPData.Date := Data;
      end;
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmLot2Cab.IncluiPagamento();
var
  Terceiro, Cod : Integer;
  Valor: Double;
  Continua: Integer;
begin
  IC3_ED_FatNum := QrLotCodigo.Value;
  IC3_ED_NF := 0;
  IC3_ED_Data := QrLotData.Value;
  IC3_ED_Vencto := QrLotData.Value;
  //
  Cod       := QrLotCodigo.Value;
  Terceiro  := QrLotCliente.Value;
  //
  {
  //Valor := QrLotA_PG_LIQ.Value;
  // Valor cont�bil e real podem diferir! Obrigar o cliente a definir o valor!
  Valor := 0;//QrLotVAL_LIQUIDO.Value + QrLotSobraIni.Value - QrLotPgLiq.Value;
  }
  //Verifica se valor cont�bil e real s�o iguais
  if QrLotSALDOAPAGAR.Value = QrLotSALDOAPAGAR_SCOL.Value then
    Valor := QrLotSALDOAPAGAR_SCOL.Value
  else
    Valor := 0;
  Continua := ID_YES;
  if Valor < 0 then
    Continua := Geral.MensagemBox('O valor a pagar n�o � positivo!' + #13#10 +
    'Deseja continuar assim mesmo?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
  //
  if Continua = ID_YES then
  begin
    UPagtos.PgFatID(QrPagtos, tpDeb, Cod, Terceiro, VAR_FATID_0302, 0, stIns,
    'Pagamento de L�quido de Cliente', Valor, VAR_USUARIO, 0, FEntidade, mmNenhum,
    0, 0, True, False, 0, 0, 0, 0, FTabLctA);
    //
    DmLot.DesEncerraLote(QrLotCodigo.Value);
    LocCod(Cod, Cod);
  end;
end;

procedure TFmLot2Cab.InfoTempo(Tempo: TDateTime; Texto: String;
  Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('==============================================================================');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

procedure TFmLot2Cab.QrLotBeforeClose(DataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do FArrayMySQLLO[i].Close;
  FOrelha := PgPrincipal.ActivePageIndex;
  PgPrincipal.ActivePageIndex := 7;
  Application.ProcessMessages;
  //
  LaTitulo2A.Caption := '...';
  LaTitulo2B.Caption := '...';
  LaTitulo2C.Caption := '...';
  {
  LaTitulo2A.Visible := False;
  LaTitulo2B.Visible := False;
  LaTitulo2C.Visible := False;
  }
  //
  BtAltera.Enabled   := False;
  BtExclui.Enabled   := False;
  SbImprime.Enabled  := False;
  BtCheckImp.Enabled := False;
  BtEncerra.Enabled  := False;
end;

procedure TFmLot2Cab.QrLotBeforeOpen(DataSet: TDataSet);
var
  i: Integer;
begin
  QrLotCodigo.DisplayFormat := FFormatFloat;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    FArrayMySQLLO[i].Close;
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      FArrayMySQLLO[i].Params[0].AsInteger := QrLot.Params[0].AsInteger;
      FArrayMySQLLO[i]. Open;
    end;
  end;
end;

procedure TFmLot2Cab.QrLotCalcFields(DataSet: TDataSet);
var
 VaValT, AdValT, VaTaxT, TotISS, TotIRR, TotPIS: Double;
 i: Integer;
begin
  QrLotNOMETIPOLOTE.Value := GetTitle(QrLotTipo.Value);
  //
  QrLotCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotCNPJCPF.Value);
  AdValT := QrLotAdValorem.Value ;
  VaValT := QrLotValValorem.Value;
  VaTaxT := QrLotTxCompra.Value;
  TotISS := QrLotISS_Val.Value;
  TotIRR := QrLotIRRF_Val.Value;
  TotPIS := QrLotPIS_R_Val.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      AdValT := AdValT + FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat;
      VaValT := VaValT + FArrayMySQLLO[i].FieldByName('ValValorem').AsFloat;
      VaTaxT := VaTaxT + FArrayMySQLLO[i].FieldByName('TaxaVal').AsFloat;
      TotISS := TotISS + FArrayMySQLLO[i].FieldByName('ISS_Val').AsFloat;
      TotIRR := TotIRR + FArrayMySQLLO[i].FieldByName('IRRF_Val').AsFloat;
      TotPIS := TotPIS + FArrayMySQLLO[i].FieldByName('PIS_R_Val').AsFloat;
    end;
  end;
  //
  VaValT := Geral.RoundC(VaValT, 2);
  VaTaxT := Geral.RoundC(VaTaxT, 2);
  // IMPOSTOS
  QrLotTOT_ISS.Value  := TotISS;
  QrLotTOT_IRR.Value := TotIRR;
  //
  QrLotADVALOREM_TOTAL.Value := AdValT;
  QrLotVALVALOREM_TOTAL.Value := VaValT;
  QrLotTAXAVALOR_TOTAL.Value := VaTaxT;
  //
  if QrLotTotal.Value = 0 then
  begin
    QrLotTAXAPERCE_TOTAL.Value := 0;
    QrLotTAXAPERCE_PROPR.Value := 0;
    QrLotTAXAPERCEMENSAL.Value := 0;
  end else begin
    QrLotTAXAPERCE_TOTAL.Value := VaTaxT / QrLotTotal.Value * 100;
    QrLotTAXAPERCE_PROPR.Value := QrLotTxCompra.Value / QrLotTotal.Value * 100;
    QrLotTAXAPERCEMENSAL.Value := MLAGeral.DescobreJuroComposto(
      QrLotTAXAPERCE_TOTAL.Value, QrLotDias.Value, 2);
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL.Value := Round((QrLotTotal.Value - QrLotVALVALOREM_TOTAL.Value -
    QrLotTAXAVALOR_TOTAL.Value - QrLotIRRF_VAL.Value - QrLotIOC_VAL.Value -
    QrLotIOFd_VAL.Value - QrLotIOFv_VAL.Value - QrLotCPMF_VAL.Value) * 100)/100;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL_2.Value := QrLotSUB_TOTAL.Value - QrLotTarifas.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL_3.Value := QrLotSUB_TOTAL_2.Value - QrLotOcorP.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotVAL_LIQUIDO.Value := Round((QrLotSUB_TOTAL_3.Value -
    QrLotCHDevPG.Value - QrLotDUDevPG.Value)*100)/100;
  //
  QrLotSALDOAPAGAR.Value := QrLotVAL_LIQUIDO.Value - QrLotPgLiq.Value
   + QrLotSobraIni.Value;
  //
  // SEM COLIGADAS IN�CIO //////////////////////////////////////////////////////
  QrLotSUB_TOTAL_SCOL.Value := Round((QrLotTotal.Value - QrLotValValorem.Value -
    QrLotTxCompra.Value - QrLotIRRF_VAL.Value - QrLotIOC_VAL.Value -
    QrLotIOFd_VAL.Value - QrLotIOFv_VAL.Value - QrLotCPMF_VAL.Value) * 100)/100;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL_2_SCOL.Value := QrLotSUB_TOTAL_SCOL.Value - QrLotTarifas.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotSUB_TOTAL_3_SCOL.Value := QrLotSUB_TOTAL_2_SCOL.Value - QrLotOcorP.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotVAL_LIQUIDO_SCOL.Value := Round((QrLotSUB_TOTAL_3_SCOL.Value -
    QrLotCHDevPG.Value - QrLotDUDevPG.Value)*100)/100;
  //
  QrLotSALDOAPAGAR_SCOL.Value := QrLotVAL_LIQUIDO_SCOL.Value - QrLotPgLiq.Value
   + QrLotSobraIni.Value;
  // SEM COLIGADAS FIM /////////////////////////////////////////////////////////
  //
  QrLotMEU_PISCOFINS_VAL.Value := QrLotPIS_Val.Value + QrLotPIS_R_Val.Value +
    QrLotCOFINS_Val.Value + QrLotCOFINS_R_Val.Value;
  QrLotMEU_PISCOFINS.Value := QrLotPIS.Value + QrLotPIS_R.Value +
    QrLotCOFINS.Value + QrLotCOFINS_R.Value;

  ///////// M�nimos ///////////////////
  if QrLotTotal.Value > 0 then
  begin
    QrLotADVAL_TXT.Value := Geral.FFT(QrLotMINAV.Value /
      QrLotTotal.Value * 100, 2, siPositivo);
  end else begin
    QrLotADVAL_TXT.Value := '0,00';
  end;
  QrLotTAXA_AM_TXT.Value := Geral.FFT(QrLotMINTC_AM.Value, 2, siPositivo);
  //
  QrLotSUB_TOTAL_MEU.Value := QrLotTotal.Value -
    QrLotMINAV.Value - QrLotMINTC.Value;
  QrLotVAL_LIQUIDO_MEU.Value := QrLotSUB_TOTAL_MEU.Value -
    QrLotIOC_VAL.Value - QrLotIOFd_VAL.Value - QrLotIOFv_VAL.Value;
  //
  QrLotA_PG_LIQ.Value := QrLotVAL_LIQUIDO_MEU.Value - QrLotPgLiq.Value;
  ///////// Fim M�nimos ///////////////////
  if QrLotTipo.Value = 0 then
  begin
    QrLotCAPTIONCONFCARTA.Value := 'Malote conferido:';
    if QrLotConferido.Value > 0 then
      QrLotNOMECOFECARTA.Value := 'SIM'
    else QrLotNOMECOFECARTA.Value := 'N�O';
  end else begin
    QrLotCAPTIONCONFCARTA.Value := 'Carta sacado impressa:';
    if QrLotECartaSac.Value > 0 then
      QrLotNOMECOFECARTA.Value := 'SIM'
    else QrLotNOMECOFECARTA.Value := 'N�O';
  end;
  if QrLotSpread.Value = 0 then QrLotNOMESPREAD.Value := '+'
  else QrLotNOMESPREAD.Value := '-';
  if QrLotSCB.Value = 0 then
    QrLotNOMESPREAD.Value := 'CCB' + QrLotNOMESPREAD.Value
  else
    QrLotNOMESPREAD.Value := 'SCB' + QrLotNOMESPREAD.Value;
  // Valores Negativos - Ponto de vista cliente da factoring
  QrLotIOC_VALNEG.Value         := - QrLotIOC_VAL.Value;
  QrLotCPMF_VALNEG.Value        := - QrLotCPMF_VAL.Value;
  QrLotTAXAVALOR_TOTALNEG.Value := - QrLotTAXAVALOR_TOTAL.Value;
  QrLotVAVALOREM_TOTALNEG.Value := - QrLotVALVALOREM_TOTAL.Value;
  QrLotCHDevPgNEG.Value         := - QrLotCHDevPg.Value;
  QrLotDUDevPgNEG.Value         := - QrLotDUDevPg.Value;
end;

procedure TFmLot2Cab.QrLotItsAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) and (QrLotLctsAuto.Value = 0);
  Enab2 := (QrLotIts.State <> dsInactive) and (QrLotIts.RecordCount > 0);
  //
  BitBtn9.Enabled      := Enab;
  BtInclui.Enabled     := Enab;
  BtAltera2.Enabled    := Enab and Enab2;
  BtExclui2.Enabled    := Enab and Enab2;
  BitBtn7.Enabled      := Enab and Enab2;
  BtSPC_LI.Enabled     := Enab and Enab2;
  BtImportaBCO.Enabled := Enab;
  BtWeb.Enabled        := Enab;
end;

procedure TFmLot2Cab.QrLotItsAfterScroll(DataSet: TDataSet);
begin
  if FImprimindoColigadas then
    FDadosJurosColigadas := Dmod.ObtemEmLotIts(QrLotItsFatParcela.Value,
      QrLotItsTxaCompra.Value, QrLotItsTxaJuros.Value,
      QrLotItsVlrCompra.Value);
  //
  DmLot.ReopenSPC_Result(QrLotItsCNPJCPF.Value, 0);
end;

procedure TFmLot2Cab.QrLotItsBeforeClose(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0);
  // 
  BitBtn9.Enabled      := False;
  BtInclui.Enabled     := Enab;
  BtAltera2.Enabled    := False;
  BtExclui2.Enabled    := False;
  BitBtn7.Enabled      := False;
  BtSPC_LI.Enabled     := False;
  BtImportaBCO.Enabled := Enab;
  BtWeb.Enabled        := Enab;
end;

procedure TFmLot2Cab.QrLotItsCalcFields(DataSet: TDataSet);
begin
  QrLotItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotItsCNPJCPF.Value);
  QrLotItsStatusSPC_TXT.Value := MLAGeral.StatusSPC_TXT(QrLotItsStatusSPC.Value);
end;

procedure TFmLot2Cab.QrLotTxsAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) and (QrLotLctsAuto.Value = 0);
  Enab2 := (QrLotTxs.State <> dsInactive) and (QrLotTxs.RecordCount > 0);
  //
  BtInclui5.Enabled := Enab;
  BtAltera5.Enabled := Enab and Enab2;
  BtExclui5.Enabled := Enab and Enab2;
end;

procedure TFmLot2Cab.QrLotTxsBeforeClose(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0);
  //
  BtInclui5.Enabled := Enab;
  BtAltera5.Enabled := False;
  BtExclui5.Enabled := False;  
end;

procedure TFmLot2Cab.QrLotTxsCalcFields(DataSet: TDataSet);
begin
  QrLotTxsTAXA_STR.Value := MLAGeral.FormataTaxa(
    QrLotTxsTipific.Value, Dmod.QrControleMoeda.Value);
  //
  if QrLotTxsMoraDia.Value > 0 then
    QrLotTxsCALCQtd.Value := QrLotTxsMoraDia.Value
  else
    if QrLotTxsQtde.Value > 0 then
      QrLotTxsCALCQtd.Value := QrLotTxsQtde.Value
    else
      if QrLotTxsMoraTxa.Value > 0 then
        QrLotTxsCALCQtd.Value := 1
      else
        QrLotTxsCALCQtd.Value := 0;
  //
  QrLotTxsTaxaValNEG.Value := - QrLotTxsVALOR.Value;

end;

procedure TFmLot2Cab.QrOcorPAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  if QrOcorP.RecordCount > 0 then
  begin
    BtPagtosExclui6.Enabled := True;
  end else begin
    BtPagtosExclui6.Enabled := False;
  end;
  Enab := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) and (QrLotLctsAuto.Value = 0);
  Enab2 := (QrOcorP.State <> dsInactive) and (QrOcorP.RecordCount > 0);  
  //
  BtPagtosExclui6.Enabled := Enab and Enab2;                                      
end;

procedure TFmLot2Cab.QrOcorPBeforeClose(DataSet: TDataSet);
begin
  BtPagtosExclui6.Enabled := False;
end;

procedure TFmLot2Cab.QrOcorPCalcFields(DataSet: TDataSet);
begin
  if QrOcorPDescri.Value <> '' then
    QrOcorPDOCUM_TXT.Value := QrOcorPDescri.Value
  else if QrOcorPTIPODOC.Value = 'CH' then
    QrOcorPDOCUM_TXT.Value := FormatFloat('000', QrOcorPBanco.Value) + '/' +
    FormatFloat('0000', QrOcorPAgencia.Value) +
    '/' + QrOcorPContaCorrente.Value + '-' +
    FormatFloat('000000', QrOcorPDocumento.Value)
  else if QrOcorPTIPODOC.Value = 'DU' then
    QrOcorPDOCUM_TXT.Value := QrOcorPDuplicata.Value
  else QrOcorPDOCUM_TXT.Value := '';
  //
  QrOcorPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrOcorPCNPJCPF.Value);
  //
  QrOcorPPagoNeg.Value := - QrOcorPPago.Value;
  QrOcorPSALDONEG.Value := - QrOcorPSALDO.Value;
end;

procedure TFmLot2Cab.QrPagtosAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0) and (QrLotLctsAuto.Value = 0);
  Enab2 := (QrPagtos.State <> dsInactive) and (QrPagtos.RecordCount > 0);
  //
  BtCheque.Enabled        := Enab and Enab2; 
  BtPagtosInclui8.Enabled := Enab;
  BtPagtosAltera8.Enabled := Enab and Enab2;
  BtPagtosExclui8.Enabled := Enab and Enab2;
end;

procedure TFmLot2Cab.QrPagtosBeforeClose(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  Enab := (QrLot.State <> dsInactive) and (QrLot.RecordCount > 0);
  //
  BtCheque.Enabled        := False;
  BtPagtosInclui8.Enabled := Enab;
  BtPagtosAltera8.Enabled := False;
  BtPagtosExclui8.Enabled := False;
end;

procedure TFmLot2Cab.QrPagtosCalcFields(DataSet: TDataSet);
begin
  QrPagtosSEQ.Value := QrPagtos.RecNo;
  QrPagtosCPF_TXT.Value := Geral.FormataCNPJ_TT(QrPagtosCPF.Value);
  case QrPagtosTipoDoc.Value of
    0: QrPagtosNOMETIPODOC.Value := '';
    1: QrPagtosNOMETIPODOC.Value := 'Cheque';
    2: QrPagtosNOMETIPODOC.Value := 'DOC';
    3: QrPagtosNOMETIPODOC.Value := 'TED';
    4: QrPagtosNOMETIPODOC.Value := 'Esp�cie';
    else QrPagtosNOMETIPODOC.Value := '***DESCONHECIDO***';
  end;
end;

procedure TFmLot2Cab.QrSaCartAfterScroll(DataSet: TDataSet);
begin
{
  DmLot.QrDupSac.Close;
  DmLot.QrDupSac.Params[0].AsFloat   := QrSaCartFatNum.Value;
  DmLot.QrDupSac.Params[1].AsString  := QrSaCartCNPJ.Value;
  DmLot.QrDupSac. Open;
}
  ReopenDupSac(QrSaCartFatNum.Value, QrSaCartCNPJ.Value, FTabLctA);
end;

procedure TFmLot2Cab.QrSaCartCalcFields(DataSet: TDataSet);
begin
  QrSaCartNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrSaCartRua.Value, Trunc(QrSaCartNumero.Value), False);
  QrSaCartLNR.Value := '';//QrSaCartNOMELOGRAD.Value;
  if Trim(QrSaCartLNR.Value) <> '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' ';
  QrSaCartLNR.Value := QrSaCartLNR.Value + QrSaCartRua.Value;
  if Trim(QrSaCartRua.Value) <> '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ', ' + QrSaCartNUMERO_TXT.Value;
  if Trim(QrSaCartCompl.Value) <>  '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' ' + QrSaCartCompl.Value;
  if Trim(QrSaCartBairro.Value) <>  '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' - ' + QrSaCartBairro.Value;
  //
  QrSaCartLN2.Value := '';
  if Trim(QrSaCartCidade.Value) <>  '' then QrSaCartLN2.Value :=
    QrSaCartLN2.Value + QrSaCartCIDADE.Value;
  QrSaCartLN2.Value := QrSaCartLN2.Value + ' - '+QrSaCartUF.Value;
  if QrSaCartCEP.Value > 0 then QrSaCartLN2.Value :=
    QrSaCartLN2.Value + '     CEP ' + Geral.FormataCEP_NT(QrSaCartCEP.Value);
  //
  QrSaCartCUC.Value := QrSaCartLNR.Value+ '   ' +QrSaCartLN2.Value;
  //
  QrSaCartCEP_TXT.Value := Geral.FormataCEP_NT(QrSaCartCEP.Value);
  //
end;

procedure TFmLot2Cab.QrSaEmitCHAfterOpen(DataSet: TDataSet);
begin
  ReopenDupSac(QrSaEmitCHCodigo.Value, QrSaEmitCHCPF.Value, FTabLctA);
end;

procedure TFmLot2Cab.QrSaEmitCHAfterScroll(DataSet: TDataSet);
begin
  ReopenDupSac(QrSaEmitCHCodigo.Value, QrSaEmitCHCPF.Value, FTabLctA);
end;

procedure TFmLot2Cab.QrSumOPCalcFields(DataSet: TDataSet);
begin
  QrSumOPPagoNeg.Value := - QrSumOPPago.Value;
end;

procedure TFmLot2Cab.VerificaCPFCNP_XX_LOIS(SelType: TSelType);
  procedure VerificaCPF_Atual(const MinMax: Boolean; var Contagem: Integer;
  AvisaValor: Boolean);
  var
    TipoDoc, Solicitante, RG, UF, Endereco, Telefone, CEP, Banda: String;
    Nascimento: TDateTime;
    DigitoCC, DigitoCH, Qtde, Agencia: Integer;
    //
    StatusSPC: Integer;
    ValMax, ValMin, Cheque: Double;
  begin
    TipoDoc := MLAGeral.ObtemTipoDocTxtDeCPFCNPJ(QrLotItsCNPJCPF.Value);
    StCPF_LI.Caption := 'Aguarde... consultando ' + TipoDoc +
      Geral.FormataCNPJ_TT(QrLotItsCNPJCPF.Value);
    StCPF_LI.Update;
    //
    Application.ProcessMessages;
    //
    Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
    RG          := '';
    UF          := '';
    Nascimento  := 0;
    //
    //MLAGeral.GeraCMC7_30(Comp, Banco, Agencia, Conta, Cheque, Tipif, Banda);
    Banda       := '';
    //
    DigitoCC    := 0;
    DigitoCH    := 0;
    Qtde        := 1;
    //
    Endereco    := '';
    Telefone    := '';
    CEP         := '';
    //
    if MinMax then
    begin
      ValMin := DmLot.QrSPC_CfgSPC_ValMin.Value;
      ValMax := DmLot.QrSPC_CfgSPC_ValMax.Value;
    end else begin
      ValMin := 0;
      ValMax := 0;
    end;
    Agencia := QrLotItsAgencia.Value;
    Cheque := QrLotItsDocumento.Value;
    StatusSPC :=
    UnSPC.ConsultaSPC_Rapida(DmLot.QrSPC_CfgCodigo.Value, True, QrLotItsCNPJCPF.Value,
      QrLotItsEmitente.Value, DmLot.QrSPC_CfgServidor.Value,
      DmLot.QrSPC_CfgSPC_Porta.Value, DmLot.QrSPC_CfgCodigSocio.Value,
      DmLot.QrSPC_CfgSenhaSocio.Value, DmLot.QrSPC_CfgModalidade.Value,
      Solicitante, QrLotItsDCompra.Value, DmLot.QrSPC_CfgBAC_CMC7.Value,
      Banda, RG, UF, Nascimento, QrLotItsBanco.Value, FormatFloat('0', Agencia),
      QrLotItsContaCorrente.Value, DigitoCC, Cheque, DigitoCH, Qtde,
      DmLot.QrSPC_CfgTipoCred.Value, QrLotItsCredito.Value,
      DmLot.QrSPC_CfgValAviso.Value, DmLot.QrSPC_CfgVALCONSULTA.Value,
      Endereco, Telefone, CEP, ValMax, ValMin, nil, nil, AvisaValor);
    Dmod.QrUpd.Params[00].AsInteger := StatusSPC;
    Dmod.QrUpd.Params[01].AsFloat   := QrLotItsFatNum.Value;
    Dmod.QrUpd.Params[02].AsString  := QrLotItsCNPJCPF.Value;
    Dmod.QrUpd.ExecSQL;
    //
    if StatusSPC = 2 then Contagem := Contagem + 1;
  end;
var
  Controle, i, Conta: Integer;
  Grade: TDBGrid;
begin
  DmLot.QrSPC_Cfg.Close;
  DmLot.QrSPC_Cfg.Params[0].AsInteger := QrLotCliente.Value;
  UMyMod.AbreQuery(DmLot.QrSPC_Cfg, Dmod.MyDB);
  //
  if DmLot.QrSPC_CfgCodigo.Value = 0 then
  begin
    Geral.MensagemBox('O cliente ' + IntToStr(QrLotCliente.Value) +
    ' - ' + QrLotNOMECLIENTE.Value +
    ' n�o possui configura��o de pesquisa ao SPC em seu cadastro!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  {
  case Pagina of
    0: VerificaCPFCNP_CH_Importacao(SelType);
    1: VerificaCPFCNP_DU_Importacao(SelType);
    else Geral.MensagemBox('Aba n�o definida para consulta de CPF ' +
    '/CNPJ na importa��o de lote!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
  }
  StCPF_LI.Visible := True;
  Controle := QrLotItsFatParcela.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctA);
  Dmod.QrUpd.SQL.Add('SET StatusSPC=:P0');
  Dmod.QrUpd.SQL.Add('WHERE FatID=' + FThisFatID_301);
  Dmod.QrUpd.SQL.Add('AND FatNum=:P1 AND CPF=:P2');
  //
  Conta := 0;
  case SelType of
    istAtual: VerificaCPF_Atual(False, Conta, True);
    istExtra1, istExtra2:
    begin
{
      DmLot.QrILi.Close;
      DmLot.QrILi.Params[0].AsFloat := QrLotItsFatNum.Value;
      DmLot.QrILi. Open;
}
      DmLot.ReopenILi(QrLotItsFatNum.Value);
      //
      if UnSPC.AceitaValorSPC(DmLot.QrSPC_CfgCodigo.Value,
      DmLot.QrSPC_CfgVALCONSULTA.Value, DmLot.QrILi.RecordCount,
      DmLot.QrSPC_CfgValAviso.Value) =
      ID_YES then
      begin
        ProgressBar1.Position := 0;
        ProgressBar1.Max := DmLot.QrILi.RecordCount;
        ProgressBar1.Visible := True;
        //
        while not DmLot.QrILi.Eof do
        begin
          ProgressBar1.Position := ProgressBar1.Position + 1;
          ProgressBar1.Update;
          Application.ProcessMessages;
          //
          if QrLotIts.Locate('CPF', DmLot.QrILiCPF.Value, [])  then
          begin
            if SelType = istExtra1 then
              VerificaCPF_Atual(False, Conta, False)
            else
              VerificaCPF_Atual(True, Conta, False);
          end;
          //
          DmLot.QrILi.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      if QrLotTipo.Value = 0 then
        Grade := GradeCHE
      else
        Grade := GradeDUP;
      if Grade.SelectedRows.Count > 0 then
      begin
        //
        if UnSPC.AceitaValorSPC(DmLot.QrSPC_CfgCodigo.Value,
        DmLot.QrSPC_CfgVALCONSULTA.Value, DmLot.QrILi.RecordCount,
        DmLot.QrSPC_CfgValAviso.Value) =
        ID_YES then
        begin
          ProgressBar1.Position := 0;
          ProgressBar1.Max := Grade.SelectedRows.Count;
          ProgressBar1.Visible := True;
          //
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count-1 do
          begin
            ProgressBar1.Position := ProgressBar1.Position + 1;
            ProgressBar1.Update;
            Application.ProcessMessages;
            //
            //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
            GotoBookmark(Grade.SelectedRows.Items[i]);
            VerificaCPF_Atual(False, Conta, False);
          end;
        end;
      end else VerificaCPF_Atual(False, Conta, True);
    end;
  end;
  //
  FmPrincipal.FControlIts := Controle;
  ReopenIts;
  StCPF_LI.Visible := False;
  ProgressBar1.Visible := False;
  case Conta of
    0: ; // Nada
    1: Geral.MensagemBox('Foi localizado um documento com informa��es!',
    'Aviso', MB_OK+MB_ICONWARNING);
    else Geral.MensagemBox('Foram localizados ' + IntToStr(Conta) +
    ' documentos com informa��es!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TFmLot2Cab.ReopenEmitCH(SelType: TSelType): Boolean;
var
  Quais: TSelType;
  i: Integer;
  Grade: TDBGrid;
  Txt: String;
begin
  Quais := SelType;
  if SelType = istPergunta then
  begin
    if DBCheck.CriaFm(TFmQuaisItens, FmQuaisItens, afmoNegarComAviso) then
    begin
      FmQuaisItens.ShowModal;
      Quais := FmQuaisItens.FEscolha;
      FmQuaisItens.Destroy;
    end;
  end;
  QrSaEmitCH.Close;
  QrSaEmitCH.SQL.Clear;
  QrSaEmitCH.SQL.Add('SELECT li.FatNum Codigo, li.Emitente, li.CNPJCPF CPF');
  QrSaEmitCH.SQL.Add('FROM '+ FtabLctA +' li');
  QrSaEmitCH.SQL.Add('');
  if QrLotTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
  if (Quais = istAtual) or (
    (Quais = istSelecionados) and (Grade.SelectedRows.Count < 2)) then
      QrSaEmitCH.SQL.Add('WHERE li.Controle=' +
      FormatFloat('0', QrLotItsControle.Value)) else
  if Quais = istSelecionados then
  begin
    Txt := '';
    with Grade.DataSource.DataSet do
    for i := 0 to Grade.SelectedRows.Count-1 do
    begin
      //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
      GotoBookmark(Grade.SelectedRows.Items[i]);
      if Txt <> '' then Txt := Txt + ',';
      Txt := Txt + FormatFloat('0', QrLotItsControle.Value);
    end;
    QrSaEmitCH.SQL.Add('WHERE li.Controle in (' + Txt + ')');
  end else
  if Quais = istTodos then QrSaEmitCH.SQL.Add('WHERE li.FatNum=' +
      FormatFloat('0', QrLotCodigo.Value))
  else begin
    Result := False;
    if Quais <> istNenhum then Geral.MensagemBox(
    '"SelType" n�o definido em "ReopenSaCart"!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QrSaEmitCH.SQL.Add('GROUP BY li.CNPJCPF');
  QrSaEmitCH.Open;
  Result := True;
end;

procedure TFmLot2Cab.ReopenFiadores;
begin
  FTbFiadores := UCriar.RecriaTempTable('Fiadores', DModG.QrUpdPID1, False);
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+ FTbFiadores +' SET Codigo=:P0, Tipo=:P1');
  if QrContratFiador11.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratFiador11.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 1;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratFiador12.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratFiador12.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 2;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratFiador21.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratFiador21.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 3;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratFiador22.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratFiador22.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 4;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratFiador13.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratFiador13.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 5;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratFiador14.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratFiador14.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 6;
    DModG.QrUpdPID1.ExecSQL;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrFiadores, DModG.MyPID_DB, [
  'SELECT en.Nome, en.ConjugeNome ',
  'FROM '+ FTbFiadores +' fi',
  'LEFT JOIN ' + TMeuDB + '.entidades   en ON fi.Codigo=en.Codigo',
  'ORDER BY fi.Tipo',
  '']);
end;

procedure TFmLot2Cab.ReopenDupSac(FatNum: Double; CNPJCPF, TabLct: String);
begin
  if Length(TabLct) > 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmLot.QrDupSac, Dmod.myDB, [
    'SELECT ba.Nome NOMEBANCO, li.Duplicata, li.Data, ',
    'li.Credito, li.Vencimento, li.Banco, li.Agencia ',
    'FROM ' + TabLct + ' li ',
    'LEFT JOIN bancos ba ON ba.Codigo=li.Banco ',
    'WHERE li.FatNum=' + FormatFloat('0', FatNum),
    'AND li.CNPJCPF="' + CNPJCPF + '"',
    '']);
  end;
end;

end.

