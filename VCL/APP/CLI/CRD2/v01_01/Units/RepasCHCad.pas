unit RepasCHCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, dmkGeral, DBGrids, Menus, frxClass, frxDBSet, Variants,
  dmkEditDateTimePicker, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkImage,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums;

type
  TTipoGera = (tgEnvio, tgTeste, tgSoDados);
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TFmRepasCHCad = class(TForm)
    PainelDados: TPanel;
    DsRepas: TDataSource;
    QrRepas: TmySQLQuery;
    PainelEdita: TPanel;
    QrColigado: TmySQLQuery;
    DsColigado: TDataSource;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    QrRepasCodigo: TIntegerField;
    QrRepasData: TDateField;
    QrRepasTotal: TFloatField;
    QrRepasJurosV: TFloatField;
    QrRepasColigado: TIntegerField;
    QrRepasNOMECOLIGADO: TWideStringField;
    QrRepasSALDO: TFloatField;
    QrRepasIts: TmySQLQuery;
    DsRepasIts: TDataSource;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMCheque: TPopupMenu;
    Adicionachequeaoloteatual1: TMenuItem;
    Alterarepassedochequeatual1: TMenuItem;
    Retirachequedoloteatual1: TMenuItem;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    QrSum: TmySQLQuery;
    QrSumValor: TFloatField;
    QrSumJurosV: TFloatField;
    QrRepasFatorCompra: TFloatField;
    QrRepasItsBanco: TIntegerField;
    QrRepasItsAgencia: TIntegerField;
    QrRepasItsEmitente: TWideStringField;
    QrRepasItsOrigem: TIntegerField;
    QrRepasItsDias: TIntegerField;
    QrRepasItsTaxa: TFloatField;
    QrRepasItsJurosP: TFloatField;
    QrRepasItsJurosV: TFloatField;
    QrRepasItsCPF_TXT: TWideStringField;
    QrRepasItsDDeposito: TDateField;
    QrRepasItsLIQUIDO: TFloatField;
    PMImprime: TPopupMenu;
    QrLI: TmySQLQuery;
    frxRepasIts: TfrxReport;
    frxDsRepasIts: TfrxDBDataset;
    frxDsRepas: TfrxDBDataset;
    QrConfigBB: TmySQLQuery;
    QrConfigBBCodigo: TIntegerField;
    QrConfigBBNome: TWideStringField;
    QrConfigBBConvenio: TIntegerField;
    QrConfigBBCarteira: TWideStringField;
    QrConfigBBVariacao: TWideStringField;
    QrConfigBBSiglaEspecie: TWideStringField;
    QrConfigBBMoeda: TWideStringField;
    QrConfigBBAceite: TSmallintField;
    QrConfigBBProtestar: TSmallintField;
    QrConfigBBMsgLinha1: TWideStringField;
    QrConfigBBPgAntes: TSmallintField;
    QrConfigBBMultaCodi: TSmallintField;
    QrConfigBBMultaDias: TSmallintField;
    QrConfigBBMultaValr: TFloatField;
    QrConfigBBMultaPerc: TFloatField;
    QrConfigBBMultaTiVe: TSmallintField;
    QrConfigBBImpreLoc: TSmallintField;
    QrConfigBBModalidade: TIntegerField;
    QrConfigBBclcAgencNr: TWideStringField;
    QrConfigBBclcAgencDV: TWideStringField;
    QrConfigBBclcContaNr: TWideStringField;
    QrConfigBBclcContaDV: TWideStringField;
    QrConfigBBcedAgencNr: TWideStringField;
    QrConfigBBcedAgencDV: TWideStringField;
    QrConfigBBcedContaNr: TWideStringField;
    QrConfigBBcedContaDV: TWideStringField;
    QrConfigBBEspecie: TSmallintField;
    QrConfigBBCorrido: TSmallintField;
    QrConfigBBBanco: TIntegerField;
    QrConfigBBIDEmpresa: TWideStringField;
    QrConfigBBProduto: TWideStringField;
    QrConfigBBNOMEBANCO: TWideStringField;
    QrConfigBBInfoCovH: TSmallintField;
    QrConfigBBCarteira240: TWideStringField;
    QrConfigBBCadastramento: TWideStringField;
    QrConfigBBTradiEscrit: TWideStringField;
    QrConfigBBDistribuicao: TWideStringField;
    QrConfigBBAceite240: TWideStringField;
    QrConfigBBProtesto: TWideStringField;
    QrConfigBBProtestodd: TIntegerField;
    QrConfigBBBaixaDevol: TWideStringField;
    QrConfigBBBaixaDevoldd: TIntegerField;
    QrConfigBBLk: TIntegerField;
    QrConfigBBDataCad: TDateField;
    QrConfigBBDataAlt: TDateField;
    QrConfigBBUserCad: TIntegerField;
    QrConfigBBUserAlt: TIntegerField;
    QrConfigBBEmisBloqueto: TWideStringField;
    QrConfigBBEspecie240: TWideStringField;
    QrConfigBBJuros240Cod: TWideStringField;
    QrConfigBBJuros240Qtd: TFloatField;
    QrConfigBBContrOperCred: TIntegerField;
    QrConfigBBReservBanco: TWideStringField;
    QrConfigBBReservEmprs: TWideStringField;
    QrConfigBBLH_208_33: TWideStringField;
    QrConfigBBSQ_233_008: TWideStringField;
    QrConfigBBTL_124_117: TWideStringField;
    QrConfigBBSR_208_033: TWideStringField;
    QrRepasConfigBB: TIntegerField;
    QrRepasHora: TTimeField;
    QrRepasDataS: TDateField;
    QrRepasHoraS: TTimeField;
    QrConfigs: TmySQLQuery;
    QrConfigsCodigo: TIntegerField;
    QrConfigsNome: TWideStringField;
    DsConfigs: TDataSource;
    Memo1: TMemo;
    Splitter1: TSplitter;
    QrRepasAgencRecNu: TIntegerField;
    QrRepasAgencRecDV: TWideStringField;
    QrRepasItsPraca: TIntegerField;
    QrRepasItsTipific: TSmallintField;
    PMGera: TPopupMenu;
    Somentecheques1: TMenuItem;
    CabealhochequesArquivodiretoaobanco1: TMenuItem;
    QrConfigBBDiretorio: TWideStringField;
    QrRepasMyDATAG: TWideStringField;
    QrRepasMyDATAS: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    BtImpCalculado: TBitBtn;
    BtImpPesquisa: TBitBtn;
    GBEdita: TGroupBox;
    Label9: TLabel;
    Label75: TLabel;
    Label3: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdCodigo: TdmkEdit;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    TPHora: TDateTimePicker;
    CBConfigBB: TdmkDBLookupComboBox;
    EdConfigBB: TdmkEditCB;
    EdAgencRecNu: TdmkEdit;
    EdAgencRecDV: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtReceitas: TBitBtn;
    BtCheque: TBitBtn;
    BtImportar: TBitBtn;
    BtGera: TBitBtn;
    GBDados: TGroupBox;
    PainelData: TPanel;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit6: TDBEdit;
    GroupBox2: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    GroupBox4: TGroupBox;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    DBEdCodigo: TDBEdit;
    Label1: TLabel;
    GradeRepas: TDBGrid;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Progress: TProgressBar;
    QrRepasItsContaCorrente: TWideStringField;
    QrRepasItsDocumento: TFloatField;
    QrRepasItsCNPJCPF: TWideStringField;
    QrRepasItsVencimento: TDateField;
    QrRepasItsCodigo: TIntegerField;
    QrRepasItsControle: TIntegerField;
    QrRepasItsValor: TFloatField;
    QrLITIPOLOTE: TSmallintField;
    QrLIRepassado: TSmallintField;
    QrLIFatParcela: TIntegerField;
    QrLIDDeposito: TDateField;
    QrLICredito: TFloatField;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtReceitasClick(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrRepasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrRepasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRepasBeforeOpen(DataSet: TDataSet);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Adicionachequeaoloteatual1Click(Sender: TObject);
    procedure Retirachequedoloteatual1Click(Sender: TObject);
    procedure QrRepasItsCalcFields(DataSet: TDataSet);
    procedure BtImpCalculadoClick(Sender: TObject);
    procedure BtImpPesquisaClick(Sender: TObject);
    procedure QrRepasItsAfterOpen(DataSet: TDataSet);
    procedure BtExclui2Click(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure frxRepasItsGetValue(const VarName: String;
      var Value: Variant);
    procedure BtGeraClick(Sender: TObject);
    procedure Somentecheques1Click(Sender: TObject);
    procedure CabealhochequesArquivodiretoaobanco1Click(Sender: TObject);
    procedure QrRepasCalcFields(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
  private
    FItensBloqueados, FItensImportados: Integer;
    FItens: Boolean;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenRepasIts;
    procedure ImportaItemDeLote(Taxa: Double);
    procedure RecalculaCheque;
    //  Gera��o de arquivo remessa
    procedure GeraArquivoRemessa(Tipo: TTipoGera);
    procedure AdicionaAoMemo(Memo: TMemo; Texto: String;
              MesmoSeTextoVazio: Boolean; Tamanho: Integer);
    procedure VerificaSomaArray(MaxS: Integer; Tam: array of Integer);
    function GeraHeaderArquivo(Tipo: TTipoGera): String;
    function GeraHeaderLote(Tipo: TTipoGera): String;
    function GeraDetalheN(Tipo: TTipoGera; Sequencia: Integer): String;
    function GeraTrailerLote(Tipo: TTipoGera; Registros: Integer;
             ValorTotal: Double): String;
    function GeraTrailerArquivo(Tipo: TTipoGera; Registros: Integer): String;
    function CompletaString(Texto, Compl: String; Tamanho: Integer;
             Alinhamento: dceAlinha): String;
    function AjustaString(Texto, Compl: String; Tamanho: Integer;
             Alinhamento: dceAlinha): String;
    procedure VerificaTamanhoTxts(Txt: array of String;
              Tam: array of Integer; MaxS: Integer);
    //  FIM da gera��o de arquivo remessa
  public
    { Public declarations }
    FRepasIts: Integer;
    //
    procedure CalculaLote(Lote: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmRepasCHCad: TFmRepasCHCad;
const
  FFormatFloat = '00000';
  FArquivoSalvaArq = 'CH_CNAB240.txt';
  FTamCNAB = 240;

implementation

uses UnMyObjects, Module, Principal, RepasImp, Lot0Loc, GetPercent, RepasLoc, 
  MyListas, MyDBCheck, RepasCHIts, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmRepasCHCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmRepasCHCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrRepasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmRepasCHCad.DefParams;
begin
  VAR_GOTOTABELA := 'Repas';
  VAR_GOTOMYSQLTABLE := QrRepas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := ' Tipo=0';

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('ELSE en.Nome END NOMECOLIGADO, en.FatorCompra, ');
  VAR_SQLx.Add('re.*, (re.Total-re.JurosV) SALDO');
  VAR_SQLx.Add('FROM repas re');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=re.Coligado');
  VAR_SQLx.Add('WHERE re.Tipo = 0');
  //
  VAR_SQL1.Add('AND re.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END) LIKE :P0');
  //
end;

procedure TFmRepasCHCad.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if not Mostra then
  begin
    PainelDados.Visible    := True;
    PainelEdita.Visible    := False;
    Memo1.Visible          := True;
    //
    CalculaLote(Codigo);
  end
  else
  begin
    PainelEdita.Visible    := True;
    PainelDados.Visible    := False;
    Memo1.Visible          := False;
    //
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant := Codigo;
      EdColigado.Text     := '';
      CBColigado.KeyValue := NULL;
      EdConfigBB.Text     := '';
      CBConfigBB.KeyValue := NULL;
      TPData.Date         := Date;
      TPHora.Time         := Now();
      EdAgencRecNu.Text   := '00000';
      EdAgencRecDV.Text   := '';
    end else begin
      EdCodigo.Text       := DBEdCodigo.Text;
      EdColigado.Text     := IntToStr(QrRepasColigado.Value);
      CBColigado.KeyValue := QrRepasColigado.Value;
      EdConfigBB.Text     := IntToStr(QrRepasConfigBB.Value);
      CBConfigBB.KeyValue := QrRepasConfigBB.Value;
      TPData.Date         := QrRepasData.Value;
      TPHora.Time         := QrRepasHora.Value;
      EdAgencRecNu.Text   := FormatFloat('00000', QrRepasAgencRecNu.Value);
      EdAgencRecDV.Text   := QrRepasAgencRecDV.Value;
    end;
    EdColigado.SetFocus;
  end;
  //
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmRepasCHCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmRepasCHCad.AlteraRegistro;
var
  Repas : Integer;
begin
  Repas := QrRepasCodigo.Value;
  if QrRepasCodigo.Value = 0 then
  begin
    Geral.MensagemBox('N�o dados selecionados para editar', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  if not UMyMod.SelLockY(Repas, Dmod.MyDB, 'Repas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Repas, Dmod.MyDB, 'Repas', 'Codigo');
      MostraEdicao(True, stIns, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmRepasCHCad.IncluiRegistro;
var
  Cursor : TCursor;
  Repas : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Repas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Repas', 'Repas', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Repas))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, Repas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmRepasCHCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmRepasCHCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmRepasCHCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmRepasCHCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmRepasCHCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmRepasCHCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmRepasCHCad.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdColigado.ValueVariant;
  //
  DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrColigado, Dmod.MyDB);
    //
    EdColigado.ValueVariant := VAR_CADASTRO;
    CBColigado.KeyValue     := VAR_CADASTRO;
    EdColigado.SetFocus;
  end;
end;

procedure TFmRepasCHCad.BtReceitasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtReceitas);
end;

procedure TFmRepasCHCad.BtGeraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGera, BtGera);
end;

procedure TFmRepasCHCad.GeraArquivoRemessa(Tipo: TTipoGera);
var
 DataS, HoraS, Arquivo: String;
 Codigo, NumLote, Sequencia, SequenciaT, Registros: Integer;
 ValorTotal: Double;
begin
  if QrRepasConfigBB.Value = 0 then
  begin
    Geral.MensagemBox('Configura��o de remessa n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  DataS   := Geral.FDT(Date, 1);
  HoraS   := Geral.FDT(Now(), 100);
  Codigo  := QrRepasCodigo.Value;
  QrConfigBB.Close;
  QrConfigBB.Params[0].AsInteger := QrRepasConfigBB.Value;
  UMyMod.AbreQuery(QrConfigBB, Dmod.MyDB);
  if QrConfigBBProtestar.Value = 0 then
  begin
    Geral.MensagemBox('Configura��o de dias para protesto inv�lido ('+
    IntToStr(QrConfigBBProtestar.Value)+').'+
    Chr(13)+Chr(10)+'Fa�a a corre��o em Op��es -> Cobran�a de t�tulos',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if (QrConfigBBBanco.Value < 1) or (QrConfigBBBanco.Value > 999) then
  begin
    Geral.MensagemBox('C�digo de banco inv�lido ('+
    FormatFloat('000', QrConfigBBBanco.Value)+').'+
    Chr(13)+Chr(10)+'Fa�a a corre��o em Op��es -> Cobran�a de t�tulos',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Memo1.Lines.Clear;
  if Tipo <> tgSoDados then
    AdicionaAoMemo(Memo1, GeraHeaderArquivo(Tipo), False, FTamCNAB);
  NumLote := 1;
  Sequencia  := 0;
  SequenciaT := 0;
  if Tipo <> tgSoDados then
    AdicionaAoMemo(Memo1, GeraHeaderLote(Tipo), False, FTamCNAB);
  ValorTotal := 0;
  QrRepasIts.First;
  while not QrRepasIts.Eof do
  begin
    Sequencia := Sequencia + 1;
    ValorTotal := ValorTotal + QrRepasItsValor.Value;
    AdicionaAoMemo(Memo1, GeraDetalheN(Tipo, Sequencia), False, FTamCNAB);
    //
    QrRepasIts.Next;
  end;
  if Tipo <> tgSoDados then
    AdicionaAoMemo(Memo1, GeraTrailerLote(Tipo, Sequencia, ValorTotal), False, FTamCNAB);
  SequenciaT := SequenciaT + Sequencia;
  //
  //
  //           detalhes      Lot es         Arquivo
  Registros := SequenciaT + (NumLote * 2) + 2;
  if Tipo <> tgSoDados then
    AdicionaAoMemo(Memo1, GeraTrailerArquivo(Tipo, Registros), False, FTamCNAB);
  //
  Arquivo := QrConfigBBDiretorio.Value;
  if Trim(Arquivo) = '' then
  begin
    Geral.MensagemBox('Diret�rio de remessa n�o definido!'+
    Chr(13)+Chr(10)+'Defina em "Cadastros" -> "Configura��o de arquivos CNAB" -> ' +
    Chr(13)+Chr(10)+'Aba "Dados 1" em "Diret�rio de arquivos cobran�a - envio".',
    'Aviso', MB_OK+MB_ICONWARNING);
    Geral.MensagemBox('O arquivo n�o pode ser salvo!'+Chr(13)+Chr(10)+
    Arquivo, 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  ForceDirectories(ExtractFileDir(Arquivo));
  if Arquivo[Length(Arquivo)] <> '\' then
    Arquivo := Arquivo + '\';
  Arquivo := Arquivo + FormatFloat('000000', Codigo) + '_' + FArquivoSalvaArq;
  //
  //
  if MLAGeral.ExportaMemoToFileExt(Memo1, Arquivo, True, False, True, 10, Null) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'Repas', False, [
      'DataS', 'HoraS'], ['Codigo'], [DataS, HoraS], [Codigo], True) then
    begin
      Geral.MensagemBox('Arquivo salvo com sucesso!'+Chr(13)+Chr(10)+
      Arquivo, 'Arquivo Salvo', MB_OK+MB_ICONINFORMATION);
      LocCod(Codigo, Codigo);
    end;
  end else
    Geral.MensagemBox('O arquivo n�o pode ser salvo!'+Chr(13)+Chr(10)+
    Arquivo, 'ERRO', MB_OK+MB_ICONERROR);
end;

procedure TFmRepasCHCad.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmRepasCHCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrRepasCodigo.Value;
  Close;
end;

procedure TFmRepasCHCad.BtConfirmaClick(Sender: TObject);
var
  AgencRecNu, Coligado, Codigo, ConfigBB: Integer;
  AgencRecDV, Data, Hora: String;
begin
  Coligado := EdColigado.ValueVariant;
  if Coligado = 0 then
  begin
    Geral.MensagemBox('Defina o coligado!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := EdCodigo.ValueVariant;
  {
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO repas SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE repas SET ');
  Dmod.QrUpdU.SQL.Add('Coligado=:P0, Data=:P1, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsInteger := Coligado;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  }
  ConfigBB   := EdConfigBB.ValueVariant;
  Data       := Geral.FDT(TPData.Date, 1);
  Hora       := Geral.FDT(TPHora.Time, 100);
  AgencRecNu := EdAgencRecNu.ValueVariant;
  AgencRecDV := EdAgencRecDV.Text;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'repas', False,
  ['Coligado', 'Data', 'ConfigBB', 'Hora', 'AgencRecNu', 'AgencRecDV'], ['Codigo'],
  [Coligado, Data, ConfigBB, Hora, AgencRecNu, AgencRecDV], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
    FRepasIts := QrRepasItsControle.Value;
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo,Codigo);
    //
    Progress.Max := QrRepasIts.RecordCount;
    Progress.Position := 0;
    Progress.Visible := True;
    QrRepasIts.First;
    while not QrRepasIts.Eof do
    begin
      Progress.Position := Progress.Position + 1;
      RecalculaCheque;
      QrRepasIts.Next;
    end;
    CalculaLote(Codigo);
    LocCod(Codigo,Codigo);
    Progress.Position := 0;
    Progress.Visible := False;
  end;
end;

procedure TFmRepasCHCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Repas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  MostraEdicao(False, stLok, 0);
end;

procedure TFmRepasCHCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align    := alClient;
  PainelDados.Align    := alClient;
  GBEdita.Align    := alClient;
  GBDados.Align    := alClient;
  GradeRepas.Align     := alClient;
  LaRegistro.Align     := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrColigado, Dmod.MyDB);
  UMyMod.AbreQuery(QrConfigs, Dmod.MyDB);
end;

procedure TFmRepasCHCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrRepasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmRepasCHCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmRepasCHCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmRepasCHCad.QrRepasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmRepasCHCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Repas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmRepasCHCad.QrRepasAfterScroll(DataSet: TDataSet);
begin
  ReopenRepasIts;
  Memo1.Lines.Clear;
end;

procedure TFmRepasCHCad.SbQueryClick(Sender: TObject);
begin
  Application.CreateForm(TFmRepasLoc, FmRepasLoc);
  FmRepasLoc.ShowModal;
  FmRepasLoc.Destroy;
  if FmPrincipal.FLoteLoc <> 0 then
    LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
end;

procedure TFmRepasCHCad.Somentecheques1Click(Sender: TObject);
begin
 GeraArquivoRemessa(tgSoDados);
end;

procedure TFmRepasCHCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRepasCHCad.QrRepasBeforeOpen(DataSet: TDataSet);
begin
  QrRepasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmRepasCHCad.QrRepasCalcFields(DataSet: TDataSet);
begin
  if QrRepasData.Value = 0 then
    QrRepasMyDataG.Value := '00/00/0000'
  else QrRepasMyDataG.Value := Geral.FDT(QrRepasData.Value, 2);
  if QrRepasDataS.Value = 0 then
    QrRepasMyDataS.Value := '00/00/0000'
  else QrRepasMyDataS.Value := Geral.FDT(QrRepasDataS.Value, 2);
end;

procedure TFmRepasCHCad.ReopenRepasIts;
begin
{
  QrRepasIts.Close;
  QrRepasIts.Params[0].AsInteger := QrRepasCodigo.Value;
  QrRepasIts. Open;
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrRepasIts, Dmod.MyDB, [
  'SELECT li.Banco, li.Agencia, li.ContaCorrente, li.Documento, ',
  'li.Emitente, li.CNPJCPF, li.DDeposito, li.Vencimento, ',
  '(ri.Valor - ri.JurosV) LIQUIDO, li.Praca, li.Tipific, ',
  'ri.* ',
  'FROM repasits ri',
  'LEFT JOIN ' + CO_TabLctA + ' li ON li.FatParcela=ri.Origem',
  'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
  'AND ri.Codigo=' + Geral.FF0(QrRepasCodigo.Value),
  '']);

  //
  if FRepasIts <> 0 then QrRepasIts.Locate('Controle', FRepasIts, []);
end;

procedure TFmRepasCHCad.Incluinovolote1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmRepasCHCad.Alteraloteatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmRepasCHCad.Adicionachequeaoloteatual1Click(Sender: TObject);
begin
  FItens := True;
  if DBCheck.CriaFm(TFmRepasCHIts, FmRepasCHIts, afmoNegarComAviso) then
  begin
    FmRepasCHIts.ImgTipo.SQLType := stIns;
    FmRepasCHIts.EdTaxa.ValueVariant := QrRepasFatorCompra.Value +
      Dmod.ObtemExEntiMaior(QrRepasColigado.Value);
    //
    FmRepasCHIts.ShowModal;
    FmRepasCHIts.Destroy;
  end;
  FItens := False;
end;

procedure TFmRepasCHCad.RecalculaCheque;
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, Taxa, JurosP, JurosV: Double;
begin
  Codigo := QrRepasCodigo.Value;
  Origem := QrRepasItsOrigem.Value;
  Taxa   := QrRepasItsTaxa.Value;
  Dias   := Trunc(int(QrRepasItsDDeposito.Value) - int(QrRepasData.Value));
  JurosP := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Valor  := QrRepasItsValor.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Controle := QrRepasItsControle.Value;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE repasits SET ');
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  //
  Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
end;

procedure TFmRepasCHCad.ImportaItemDeLote(Taxa: Double);
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, JurosP, JurosV: Double;
begin
  if QrLIRepassado.Value > 0 then
  begin
    FItensBloqueados := FItensBloqueados + 1;
    Exit;
  end;
  FItensImportados := FItensImportados + 1;
  Codigo := QrRepasCodigo.Value;
  Origem := QrLIFatParcela.Value;
  Dias   := Trunc(int(QrLIDDeposito.Value) - int(QrRepasData.Value));
  JurosP := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Valor  := QrLICredito.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
  'RepasIts', 'RepasIts', 'Codigo');
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO repasits SET ');
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
{
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Repassado=1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
}
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False,[
  'Repassado'], ['FatID', 'FatParcela'], [
  1], [VAR_FATID_0301, Origem], True);
  //
end;

procedure TFmRepasCHCad.CalculaLote(Lote: Integer);
begin
 QrSum.Close;
 QrSum.Params[0].AsInteger := lote;
 UMyMod.AbreQuery(QrSum, Dmod.MyDB);
 //
 Dmod.QrUpd.SQL.Clear;
 Dmod.QrUpd.SQL.Add('UPDATE repas SET Total=:P0, JurosV=:P1');
 Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
 Dmod.QrUpd.SQL.Add('');
 Dmod.QrUpd.Params[00].AsFloat   := QrSumValor.Value;
 Dmod.QrUpd.Params[01].AsFloat   := QrSumJurosV.Value;
 //
 Dmod.QrUpd.Params[02].AsInteger := Lote;
 Dmod.QrUpd.ExecSQL;
end;

procedure TFmRepasCHCad.Retirachequedoloteatual1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a retirada do cheque selecionado '+
  'do lote atual?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lot esits SET AlterWeb=1,   Repassado=0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrRepasItsOrigem.Value;
    Dmod.QrUpd.ExecSQL;
}
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TabLctA, False,[
    'Repassado'], ['FatID', 'FatParcela'], [
    0], [VAR_FATID_0301, QrRepasItsOrigem.Value], True);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM repasits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrRepasItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrRepasIts.Next;
    FRepasIts := QrRepasItsControle.Value;
    CalculaLote(QrRepasCodigo.Value);
    LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
  end;
end;

procedure TFmRepasCHCad.QrRepasItsCalcFields(DataSet: TDataSet);
begin
  QrRepasItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRepasItsCNPJCPF.Value);
end;

procedure TFmRepasCHCad.BtImpCalculadoClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxRepasIts, 'Repasse de cheques');
end;

procedure TFmRepasCHCad.BtImpPesquisaClick(Sender: TObject);
begin
  Application.CreateForm(TFmRepasImp, FmRepasImp);
  FmRepasImp.ShowModal;
  FmRepasImp.Destroy;
end;

procedure TFmRepasCHCad.QrRepasItsAfterOpen(DataSet: TDataSet);
begin
  if FItens then
    QrRepasIts.Last;
end;

procedure TFmRepasCHCad.BtExclui2Click(Sender: TObject);
begin
  Retirachequedoloteatual1Click(Self);
end;

procedure TFmRepasCHCad.BtImportarClick(Sender: TObject);
var
  Taxa: Double;
  Texto1, Texto2: String;
begin
  if Geral.MensagemBox('Todos cheques do border� a ser selecionado '+
  'ser�o incorporados ao lote de repasse atual. Para incorporar o border� a ser '+
  'selecionado em um lote novo, desista desta opera��o e crie um lote antes '+
  'da importa��o. Deseja continuar assim mesmo?', 'Aviso', MB_YESNOCANCEL+MB_ICONWARNING) = ID_YES then
  begin
    FmPrincipal.FLoteLoc := 0;
    Application.CreateForm(TFmLot0Loc, FmLot0Loc);
    FmLot0Loc.FFormCall := 1;
    FmLot0Loc.ShowModal;
    FmLot0Loc.Destroy;
    if FmPrincipal.FLoteLoc <> 0 then
    begin
{
      QrLI.Close;
      QrLI.Params[0].AsInteger := FmPrincipal.FLoteLoc;
      QrLI. Open;
}
      UnDmkDAC_PF.AbreMySQLQuery0(QrLI, Dmod.MyDB, [
      'SELECT lo.Tipo TIPOLOTE, li.Repassado, li.FatParcela, ',
      'li.DDeposito, li.Credito ',
      'FROM ' + CO_TabLctA + ' li',
      'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo=li.FatNum',
      'WHERE li.FatID=' + Geral.FF0(VAR_FATID_0301),
      'AND lo.Codigo=' + Geral.FF0(FmPrincipal.FLoteLoc),
      '']);
      //
      if QrLI.RecordCount = 0 then
      begin
        Geral.MensagemBox('O border� selecionado n�o cont�m nenhum cheque!',
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end else begin
        FItensBloqueados := 0;
        FItensImportados := 0;
        Taxa := MLAGeral.GetPercent(TfmGetPercent, FmGetPercent, 'Taxa de Repasse',
          'Taxa de repasse:', QrRepasFatorCompra.Value+
          Dmod.ObtemExEntiMaior(QrRepasColigado.Value), 6, siPositivo);
        while not QrLI.Eof do
        begin
          ImportaItemDeLote(Taxa);
          QrLI.Next;
        end;
        CalculaLote(QrRepasCodigo.Value);
        LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
        FRepasIts := QrRepasItsControle.Value;
        case FItensImportados of
          0: Texto1 := 'Nenhum cheque foi importado!';
          1: Texto1 := 'Um cheque foi importado!';
          else Texto1 := IntToStr(FItensImportados)+ ' cheques foram importados!';
        end;
        case FItensBloqueados of
          0: Texto2 := 'Nenhum cheque foi descartado!';
          1: Texto2 := 'Um cheque foi descartado!';
          else Texto2 := IntToStr(FItensBloqueados)+ ' cheques foram '+
          'descartados por j� estarem repassados!';
        end;
        Geral.MensagemBox(Texto1+Chr(13)+Chr(10)+Chr(13)+Chr(10)+
          Texto2, 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
end;

procedure TFmRepasCHCad.CabealhochequesArquivodiretoaobanco1Click(Sender: TObject);
begin
  GeraArquivoRemessa(tgEnvio);
end;

procedure TFmRepasCHCad.frxRepasItsGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_QTD_CHEQUES' then Value := QrRepasIts.RecordCount;
end;

////////////////////////////////////////////////////////////////////////////////
/////////  R E M E S S A  //////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure TFmRepasCHCad.AdicionaAoMemo(Memo: TMemo; Texto: String;
  MesmoSeTextoVazio: Boolean; Tamanho: Integer);
var
  Leng: Integer;
begin
  if (MesmoSeTextoVazio = False) and (Trim(Texto) = '') then Exit;
  Memo.Lines.Add(Texto);
  Leng := Length(Texto);
  if Leng <> Tamanho then
    Geral.MensagemBox('Erro. A linha '+IntToStr(Memo.Lines.Count)+
    ' tem '+IntToStr(Length(Texto))+' caracteres, quando deveria ter '+
    IntToStr(Tamanho)+'!', 'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmRepasCHCad.VerificaSomaArray(MaxS: Integer; Tam: array of Integer);
var
  c, i: Integer;
begin
  c := 0;
  for i := 0 to MaxS - 1 do
    c := c + Tam[i];
  if c <> FTamCNAB then
    Geral.MensagemBox('Configura��o de array de segmentos inv�lida!',
    'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmRepasCHCad.VerificaTamanhoTxts(Txt: array of String;
  Tam: array of Integer; MaxS: Integer);
var
  i: Integer;
begin
  for i := 0 to MaxS - 1 do
  begin
    if Length(Txt[i]) <> Tam[i] then
      Geral.MensagemBox('O segmento '+IntToStr(i+1)+' deveria ter '+
      IntToStr(Tam[i])+ ' caracteres, mas possui '+IntToStr(Length(Txt[i]))+'!',
      'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmRepasCHCad.AjustaString(Texto, Compl: String; Tamanho: Integer;
  Alinhamento: dceAlinha): String;
var
  Txt: String;
  Direita: Boolean;
begin
  Direita := True;
  Texto := Geral.SemAcento(Texto);
  Texto := Geral.Maiusculas(Texto, False);
  Txt := CompletaString(Texto, Compl, Tamanho, Alinhamento);
  while Length(Txt) > Tamanho do
  begin
    case Alinhamento of
      posEsquerda: Txt := Copy(Txt, 1, Length(Txt)-1);
      posCentro  :
      begin
        if Direita then
          Txt := Copy(Txt, 2, Length(Txt)-1)
        else
          Txt := Copy(Txt, 1, Length(Txt)-1);
        Direita := not Direita;
      end;
      posDireita: Txt := Copy(Txt, 2, Length(Txt)-1);
    end;
  end;
  Result := Txt;
end;

function TFmRepasCHCad.CompletaString(Texto, Compl: String; Tamanho: Integer;
  Alinhamento: dceAlinha): String;
var
  Txt: String;
  Direita: Boolean;
begin
  Direita := True;
  Txt := Texto;
  while Length(Txt) < Tamanho do
  begin
    case Alinhamento of
      posEsquerda: Txt := Txt + Compl;
      posCentro  :
      begin
        if Direita then
          Txt := Txt + Compl
        else
          Txt := Compl + Txt;
        Direita := not Direita;
      end;
      posDireita:  Txt := Compl + Txt;
    end;
  end;
  Result := Txt;
end;

function TFmRepasCHCad.GeraHeaderArquivo(Tipo: TTipoGera): String;
const
  MaxS = 28;
var
  i: Integer;
  Txt: array[1..MaxS] of String;
  Tam: array[1..MaxS] of Integer;
  cpf, Convenio: String;
begin
  for i := 1 to MaxS do Txt[i] := '';
  Tam[01] := 003; Tam[02] := 004; Tam[03] := 001; Tam[04] := 009;
  Tam[05] := 001; Tam[06] := 014; Tam[07] := 009; Tam[08] := 011;
  Tam[09] := 005; Tam[10] := 001; Tam[11] := 012; Tam[12] := 001;
  Tam[13] := 001; Tam[14] := 030; Tam[15] := 030; Tam[16] := 010;
  Tam[17] := 001; Tam[18] := 008; Tam[19] := 006; Tam[20] := 006;
  Tam[21] := 003; Tam[22] := 005; Tam[23] := 020; Tam[24] := 020;
  Tam[25] := 019; Tam[26] := 002; Tam[27] := 008;
  //
  VerificaSomaArray(MaxS, Tam);
(*
01. HEADER DE ARQUIVO   - REGISTRO 0
    -----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !54 !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - ! 0000        ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO HEADER DE ARQUIVO     !  8!  8!  1! - ! 0           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !  9! 17!  9! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[01] := FormatFloat('000', QrConfigBBBanco.Value);
  Txt[02] := '0000';
  Txt[03] := '0';
  Txt[04] := AjustaString(' ', ' ', 009, posEsquerda);
(*
!TIPO DE INSCRICAO DA EMPRESA   ! 18! 18!  1! - !1-CPF, 2-CGC ! 6 !             5
!-------------------------------!---!---!---!---!-------------!---!
!N  DE INSCRICAO DA EMPRESA     ! 19! 32! 14! - !NUMERICO     ! 6 !             6
!-------------------------------!---!---!---!---!-------------!---!
*)
  case DModG.QrDonoTipo.Value of
    0: Txt[05] := '2';
    1: Txt[05] := '1';
    else Txt[05] := '0'
  end;
  cpf := Geral.SoNumero_TT(DModG.QrDonoCNPJ_CPF.Value);
  Txt[06] := AjustaString(cpf, '0', 14, posDireita);
(*
!CODIGO DO CONVENIO NO BANCO    ! 33! 41! 09! - !ALFANUMERICO !   !             7
!-------------------------------!---!---!---!---!-------------!---!
!USO RESERVADO DO BANCO         ! 42! 52! 11! - !BRANCOS      !   !             8
*)
  if QrConfigBBInfoCovH.Value = 1 then
  Convenio :=
    AjustaString(FormatFloat('0', QrConfigBBConvenio.Value), '0', 009, posDireita)
  else Convenio := '';
  Txt[07] := AjustaString(Convenio, ' ', 009, posEsquerda);
  Txt[08] := AjustaString(''      , ' ', 011, posEsquerda);
(*
!AGENCIA MANTENEDORA DA CONTA   ! 53! 57!  5! - !NUMERICO     !6/8!             9
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  ! 58! 58!  1! - !ALFANUMERICO !6/8!             10
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA CONTA CORRENTE       ! 59! 70! 12! - !NUMERICO     !6/8!             11
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA CONTA    ! 71! 71!  1! - !ALFANUMERICO !6/8!             12
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AG/CONTA ! 72! 72!  1! - !ALFANUMERICO !6/8!             13
!-------------------------------!---!---!---!---!-------------!---!
!NOME DA EMPRESA                ! 73!102! 30! - !ALFANUMERICO !   !             14
!-------------------------------!---!---!---!---!-------------!---!
!NOME DO BANCO                  !103!132! 30! - !ALFANUMERICO !   !             15
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !133!142! 10! - !BRANCOS      !   !             16
!-----------------------------------------------------------------!
*)
  Txt[09] := AjustaString(QrConfigBBclcAgencNr.Value, '0', 005, posDireita);
  Txt[10] := AjustaString(QrConfigBBcedAgencDV.Value, ' ', 001, posDireita);
  Txt[11] := AjustaString(QrConfigBBcedContaNr.Value, '0', 012, posDireita);
  Txt[12] := AjustaString(QrConfigBBcedContaDV.Value, ' ', 001, posDireita);
  Txt[13] := AjustaString(''                        , ' ', 001, posDireita);
  Txt[14] := AjustaString(DModG.QrDonoNOMEDONO.Value , ' ', 030, posEsquerda);
  Txt[15] := AjustaString(QrConfigBBNOMEBANCO.Value , ' ', 030, posEsquerda);
  Txt[16] := AjustaString(''                        , ' ', 010, posEsquerda);
(*
!CODIGO REMESSA / RETORNO       !143!143!  1! - !1-REM e 2-RET!   !             17
!-------------------------------!---!---!---!---!-------------!---!
!DATA DE GERACAO DO ARQUIVO     !144!151!  8! - !NUM/DDMMAAAA/!   !             18
!-------------------------------!---!---!---!---!-------------!---!
!HORA DE GERACAO DO ARQUIVO     !152!157!  6! - !NUM /HHMMSS/ !   !             19
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO ARQUIVO       !158!163!  6! - !NUMERICO     !17 !             20
!-------------------------------!---!---!---!---!-------------!---!
!N DA VERSAO DO LAYOUT DO ARQUIV!164!166!  3! - ! 030         ! 9 !             21
!-------------------------------!---!---!---!---!-------------!---!
!DENSIDADE DE GRAVACAO DO ARQUIV!167!171!  5! - !NUMERICO/BPI/!   !             22
*)
  Txt[17] := '1';
  Txt[18] := FormatDateTime('DDMMYYYY', QrRepasData.Value);
  Txt[19] := FormatDateTime('HHNNSS', QrRepasHora.Value);
  Txt[20] := AjustaString(FormatFloat('0', QrRepasCodigo.Value), '0', 006, posDireita);
  Txt[21] := '030';
  Txt[22] := '00000';
(*
!-------------------------------!---!---!---!---!-------------!---!
!PARA USO RESERVADO DO BANCO    !172!191! 20! - !ALFANUMERICO !   !             23
!-------------------------------!---!---!---!---!-------------!---!
!PARA USO RESERVADO DA EMPRESA  !192!211! 20! - !ALFANUMERICO !   !             24
*-----------------------------------------------------------------*
!USO EXCLUSIVO FEBRABAN/CNAB    !212!230! 19! - !BRANCOS      !   !             25
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO RETORNO              !231!232! 10! - !ALFANUMERICO !53 !             26
*-----------------------------------------------------------------*
!CODIGOS DAS OCORRENCIAS        !233!240! 10! - !ALFANUMERICO !53 !             27
*-----------------------------------------------------------------*
*)
  Txt[23] := AjustaString(QrConfigBBReservBanco.Value , ' ', 020, posDireita);
  Txt[24] := AjustaString(QrConfigBBReservEmprs.Value , 'X', 020, posDireita); 
  Txt[25] := AjustaString(''                        , ' ', 019, posDireita);
  Txt[26] := AjustaString(''                        , ' ', 002, posDireita);
  Txt[27] := AjustaString(''                        , ' ', 008, posDireita);
  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
  VerificaTamanhoTxts(Txt, Tam, MaxS);
end;

function TFmRepasCHCad.GeraHeaderLote(Tipo: TTipoGera): String;
const
  MaxS = 35;
var
  i: Integer;
  Txt: array[1..MaxS] of String;
  Tam: array[1..MaxS] of Integer;
  cpf, Convenio: String;
begin
  for i := 1 to MaxS do Txt[i] := '';
  Tam[01] := 003; Tam[02] := 004; Tam[03] := 001; Tam[04] := 001;
  Tam[05] := 002; Tam[06] := 002; Tam[07] := 003; Tam[08] := 001;
  Tam[09] := 001; Tam[10] := 014; Tam[11] := 009; Tam[12] := 011;
  Tam[13] := 005; Tam[14] := 001; Tam[15] := 006; Tam[16] := 005;
  Tam[17] := 001; Tam[18] := 012; Tam[19] := 001; Tam[20] := 001;
  Tam[21] := 030; Tam[22] := 008; Tam[23] := 006; Tam[24] := 010;
  Tam[25] := 004; Tam[26] := 030; Tam[27] := 005; Tam[28] := 015;
  Tam[29] := 020; Tam[30] := 005; Tam[31] := 003; Tam[32] := 002;
  Tam[33] := 008; Tam[34] := 002; Tam[35] := 008;
  //
  VerificaSomaArray(MaxS, Tam);
(*
HEADER DE LOTE  -  REGISTRO 1
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !             1
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !             2
!-------------------------------!---!---!---!---!------------ !---!
!REGISTRO HEADER DO LOTE        !  8!  8!  1! - ! 1           ! 2 !             3
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE OPERACAO               !  9!  9!  1! - !ALFANUMERICO ! 3 !             4
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE SERVICO                ! 10! 11!  2! - ! 07          ! 4 !             5
!-------------------------------!---!---!---!---!-------------!---!
!FORMA DE LANCAMENTO            ! 12! 13!  2! - ! 01          !   !             6
!-------------------------------!---!---!---!---!-------------!---!
!N  DA VERSAO DO LAYOUT DO LOTE ! 14! 16!  3! - !'020'        !45 !             7
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 17! 17!  1! - !BRANCOS      !   !             8
!-----------------------------------------------------------------!
*)
  Txt[01] := FormatFloat('000', QrConfigBBBanco.Value);
  Txt[02] := FormatFloat('0000', 1);
  Txt[03] := '1';
  Txt[04] := 'R';
  Txt[05] := '07';
  Txt[06] := '01';
  Txt[07] := '020';
  Txt[08] := ' ';
(*
!TIPO DE INSCRICAO DA EMPRESA   ! 18! 18!  1! - !1-CPF, 2-CGC !   !             9
!-------------------------------!---!---!---!---!-------------!---!
!N  DE INSCRICAO DA EMPRESA     ! 19! 32! 14! - !NUMERICO     !   !             10
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO CONVENIO NO BANCO    ! 33! 41! 09! - !ALFANUMERICO ! 7 !             11
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO DO BANCO         ! 42! 52! 11! - !BRANCOS      !   !             12
!-------------------------------!---!---!---!---!-------------!---!
*)
  case DModG.QrDonoTipo.Value of
    0: Txt[09] := '2';
    1: Txt[09] := '1';
    else Txt[09] := '0';
  end;
  cpf := Geral.SoNumero_TT(DModG.QrDonoCNPJ_CPF.Value);
  Txt[10] := AjustaString(cpf, '0', 14, posDireita);
  //
  Convenio :=
    AjustaString(FormatFloat('0', QrConfigBBConvenio.Value), '0', 009, posDireita) +
    AjustaString(         QrConfigBBProduto. Value , '0', 004, posDireita) +
    AjustaString(         QrConfigBBCarteira.Value , '0', 002, posDireita) +
    AjustaString(         QrConfigBBVariacao.Value , '0', 003, posDireita);
  Txt[11] := AjustaString(Convenio, ' ', 009, posEsquerda);
  Txt[12] := AjustaString(''      , ' ', 011, posEsquerda);
(*
!AGENCIA RECEPTORA              ! 53! 57!  5! _ !NUMERICO     !   !             13
!-------------------------------!---!---!---!---!-------------!---!
!DV DA AGENCIA RECEPTORA        ! 58! 58!  1! - !ALFANUMERICO !   !             14
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA REMESSA              ! 59! 64!  6! _ !NUMERICO     !   !             15
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[13] := AjustaString(FormatFloat('0', QrRepasAgencRecNu.Value)  , '0', 005, posDireita);
  Txt[14] := AjustaString(                 QrRepasAgencRecDV.Value   , ' ', 001, posDireita);
  Txt[15] := AjustaString(FormatFloat('0', QrRepasCodigo.Value)      , '0', 006, posDireita);
(*
!AGENCIA MANTENEDORA DA CONTA   ! 65! 69!  5! _ !NUMERICO     !   !             16
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  ! 70! 70!  1! - !ALFANUMERICO !   !             17
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA CONTA CORRENTE       ! 71! 82! 12! _ !NUMERICO     !   !             18
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA CONTA    ! 83! 83!  1! _ !ALFANUMERICO !   !             19
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AG/CONTA ! 84! 84!  1! _ !ALFANUMERICO !   !             20
!-------------------------------!---!---!---!---!-------------!---!
!NOME DA EMPRESA                ! 85!114! 30! - !ALFANUMERICO !   !             21
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[16] := AjustaString(QrConfigBBclcAgencNr.Value, '0', 005, posDireita);
  Txt[17] := AjustaString(QrConfigBBcedAgencDV.Value, ' ', 001, posEsquerda);
  Txt[18] := AjustaString(QrConfigBBcedContaNr.Value, '0', 012, posDireita);
  Txt[19] := AjustaString(QrConfigBBcedContaDV.Value, ' ', 001, posEsquerda);
  Txt[20] := AjustaString(''                        , ' ', 001, posEsquerda);
  Txt[21] := AjustaString(DModG.QrDonoNOMEDONO.Value , ' ', 030, posEsquerda);
(*
!-------------------------------!---!---!---!---!-------------!---!
!DATA DA REMESSA DO ARQUIVO     !115!122!  8! - !DDMMAAAA     !   !             22
!-------------------------------!---!---!---!---!-------------!---!
!N�MERO REMESSA ORIGEM (ARQ.RET)!123!128!  6! - !             !   !             23
!-------------------------------!---!---!---!---!-------------!---!
!CAMPO LIVRE                    !129!138! 10! - !             !   !             24
!-------------------------------!---!---!---!---!-------------!---!
!INFORMA��O 1 - MENSAGEM        !139!142!  4! - !             !   !             25
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[22] := FormatDateTime('DDMMYYYY', QrRepasData.Value);
  Txt[23] := AjustaString(''                          , '0', 006, posDireita);
  Txt[24] := AjustaString(''                          , ' ', 010, posEsquerda);
  Txt[25] := AjustaString(''                          , ' ', 004, posEsquerda);
(*
!-------------------------------!---!---!---!---!-------------!---!
!LOGRADOURO - RUA,AV,P�A,ETC    !143!172! 30! - !             !   !             26
!-------------------------------!---!---!---!---!-------------!---!
!N�MERO                         !173!177!  5! - !             !   !             27
!-------------------------------!---!---!---!---!-------------!---!
!COMPLEMENTO                    !178!192! 15! - !             !   !             28
!-------------------------------!---!---!---!---!-------------!---!
!CIDADE                         !193!212! 20! - !             !   !             29
!-------------------------------!---!---!---!---!-------------!---!
!CEP                            !213!217!  5! - !             !   !             30
!-------------------------------!---!---!---!---!-------------!---!
!COMPLEMENTO CEP                !218!220!  3! - !             !   !             31
!-------------------------------!---!---!---!---!-------------!---!
!ESTADO                         !221!222!  2! - !             !   !             32
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[26] := AjustaString(''                          , ' ', 030, posEsquerda);
  Txt[27] := AjustaString(''                          , ' ', 005, posEsquerda);
  Txt[28] := AjustaString(''                          , ' ', 015, posEsquerda);
  Txt[29] := AjustaString(''                          , ' ', 020, posEsquerda);
  Txt[30] := AjustaString(''                          , ' ', 005, posEsquerda);
  Txt[31] := AjustaString(''                          , ' ', 003, posEsquerda);
  Txt[32] := AjustaString(''                          , ' ', 002, posEsquerda);
(*
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO DA FEBRABAN      !223!230!  8! - !ALFANUMERICO !   !             33
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO RETORNO              !231!232! 10! - !ALFANUMERICO !55 !             34
*-----------------------------------------------------------------*
!CODIGOS DAS OCORRENCIAS        !233!240! 10! - !ALFANUMERICO !56 !             35
*-----------------------------------------------------------------*
*)
  Txt[33] := AjustaString(''                        , ' ', 008, posDireita);
  Txt[34] := AjustaString(''                        , ' ', 002, posDireita);
  Txt[35] := AjustaString(''                        , ' ', 008, posDireita);
  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
  VerificaTamanhoTxts(Txt, Tam, MaxS);
end;

function TFmRepasCHCad.GeraDetalheN(Tipo: TTipoGera; Sequencia: Integer): String;
const
  MaxS = 24;
var
  Txt: array[1..MaxS] of String;
  Tam: array[1..MaxS] of Integer;
  i: Integer;
  CMC7: String;
begin
  for i := 1 to MaxS do Txt[i] := '';
  Tam[01] := 003; Tam[02] := 004; Tam[03] := 001; Tam[04] := 005;
  Tam[05] := 001; Tam[06] := 001; Tam[07] := 002; Tam[08] := 034;
  Tam[09] := 014; Tam[10] := 015; Tam[11] := 008; Tam[12] := 010;
  Tam[13] := 010; Tam[14] := 003; Tam[15] := 003; Tam[16] := 005;
  Tam[17] := 005; Tam[18] := 012; Tam[19] := 015; Tam[20] := 015;
  Tam[21] := 015; Tam[22] := 049; Tam[23] := 002; Tam[24] := 008;
  //
  VerificaSomaArray(MaxS, Tam);
(*
REMESSA
DETALHE -  REGISTRO 3  -  SEGMENTO N  /OBRIGATORIO/
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !             1
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     !   !             2
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE DE LOTE       !  8!  8!  1! - !Igual a 3    !   !             3
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !   !             4
!-------------------------------!---!---!---!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - !Igual a N    !   !             5
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE MOVIMENTO              ! 15! 15!  1! - ! 0=INCLUS�O  !   !             6
!-------------------------------!---!---!---!---!-------------!---!
!C�DIGO DA INSTRU��O P/ MOVIM.  ! 16! 17!  2! - ! 02 FIXO     !   !             7
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[01] := FormatFloat('000', QrConfigBBBanco.Value);
  Txt[02] := FormatFloat('0000', 1);
  Txt[03] := '3';
  Txt[04] := FormatFloat('00000', Sequencia);
  Txt[05] := 'N';
  Txt[06] := '1';
  Txt[07] := '02';

(*
!-----------------------------------------------------------------!
!DADOS DO CMC7                  ! 18! 51! 34! _ !ALFANUMERICO !   !             8
!-------------------------------!---!---!---!---!-------------!---!
!CNPJ OU CPF DO EMITENTE        ! 52! 65! 14! _ !ALFANUMERICO !   !             9
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DO CHEQUE                ! 66! 80! 13! 2 !NUMERICO     !   !             10
!-------------------------------!---!---!---!---!-------------!---!
!DATA DE "BOM PARA" P/ COMPE    ! 81! 88!  8! - !NUMERICO     !   !             11
!-------------------------------!---!---!---!---!-------------!---!
*)
  MLAGeral.GeraCMC7_34(QrRepasItsPraca.Value, QrRepasItsBanco.Value,
    QrRepasItsAgencia.Value, QrRepasItsContaCorrente.Value,
    Trunc(QrRepasItsDocumento.Value), QrRepasItsTipific.Value, CMC7);

  Txt[08] := CMC7;
  Txt[09] := AjustaString(QrRepasItsCNPJCPF.Value       , ' ', 014, posEsquerda);
  Txt[10] := MLAGeral.FTX(QrRepasItsValor.Value     , 013, 002, siPositivo);
  Txt[11] := FormatDateTime('DDMMYYYY', QrRepasItsVencimento.Value);

(*
!-----------------------------------------------------------------!
!USO LIVRE PARA O CLIENTE       ! 89! 98! 10! _ !ALFANUMERICO !   !             12
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO DO BANCO         ! 99!108! 10! _ !ALFANUMERICO !   !             13
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[12] := AjustaString(''                        , ' ', 010, posEsquerda);
  Txt[13] := AjustaString(''                        , ' ', 010, posEsquerda);
(*
!-----------------------------------------------------------------!
!CODIGO COMPE DO ACOLHIMENTO    !109!111!  3! _ !FIXO "000"   !   !             14
!-------------------------------!---!---!---!---!-------------!---!
!BANCO DO REMETENTE             !112!114!  3! _ !FIXO "001"   !   !             15
!-------------------------------!---!---!---!---!-------------!---!
!AG�NCIA DE ENTREGA SEM O DV    !115!119!  5! _ !NUMERICO     !   !             16
!-------------------------------!---!---!---!---!-------------!---!
!AG�NCIA DO DEPOSITANTE SEM O DV!120!125!  5! _ !NUMERICO     !   !             17
!-------------------------------!---!---!---!---!-------------!---!
!CONTA DO DEPOSITANTE SEM O DV  !125!136! 12! _ !NUMERICO     !   !             18
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[14] := '000';
  Txt[15] := '001';
  Txt[16] := AjustaString(FormatFloat('0', QrRepasAgencRecNu.Value), '0', 005, posDireita);
  Txt[17] := AjustaString(QrConfigBBcedAgencNr.Value,                '0', 005, posDireita);
  Txt[18] := AjustaString(QrConfigBBcedContaNr.Value,                ' ', 012, posEsquerda);
(*
!-----------------------------------------------------------------!
!JUROS SOBRE DESCONTO           !137!151! 13! 2 !ALFANUMERICO !   !             19
!-------------------------------!---!---!---!---!-------------!---!
!IOF SOBRE DESCONTO             !152!166! 13! 2 !ALFANUMERICO !   !             20
!-------------------------------!---!---!---!---!-------------!---!
!OUTROS ENCARGOS SOBRE DESCONTO !167!181! 13! 2 !ALFANUMERICO !   !             21
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !182!230! 49! - !ALFANUMERICO !   !             22
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO RETORNO              !231!232! 10! - !ALFANUMERICO !55 !             23
*-----------------------------------------------------------------*
!CODIGOS DAS OCORRENCIAS        !233!240! 10! - !ALFANUMERICO !56 !             24
*-----------------------------------------------------------------*
*)
  Txt[19] := AjustaString(''                        , ' ', 015, posEsquerda);
  Txt[20] := AjustaString(''                        , ' ', 015, posEsquerda);
  Txt[21] := AjustaString(''                        , ' ', 015, posEsquerda);
  Txt[22] := AjustaString(''                        , ' ', 049, posEsquerda);
  Txt[23] := AjustaString(''                        , ' ', 002, posDireita);
  Txt[24] := AjustaString(''                        , ' ', 008, posDireita);
  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
  VerificaTamanhoTxts(Txt, Tam, MaxS);
end;

function TFmRepasCHCad.GeraTrailerLote(Tipo: TTipoGera; Registros:
  Integer; ValorTotal: Double): String;
const
  MaxS = 14;
var
  i: Integer;
  Txt: array[1..MaxS] of String;
  Tam: array[1..MaxS] of Integer;
begin
  for i := 1 to MaxS do Txt[i] := '';
  Tam[01] := 003; Tam[02] := 004; Tam[03] := 001; Tam[04] := 009;
  Tam[05] := 006; Tam[06] := 018; Tam[07] := 006; Tam[08] := 052;
  Tam[09] := 018; Tam[10] := 018; Tam[11] := 018; Tam[12] := 006;
  Tam[13] := 018; Tam[14] := 063;
  //
  VerificaSomaArray(MaxS, Tam);
(*
TRAILER DE LOTE - REGISTRO 5
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !             1
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !             2
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO TRAILER DO LOTE       !  8!  8!  1! - ! 5           ! 2 !             3
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !  9! 17!  9! - !BRANCOS      !   !             4
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[01] := FormatFloat('000', QrConfigBBBanco.Value);
  Txt[02] := FormatFloat('0000', 1);
  Txt[03] := '5';
  Txt[04] := AjustaString('', ' ', 009, posEsquerda);
(*
!QUANTIDADE DE REGISTROS DO LOTE! 18! 23!  6! - !NUMERICO     !38 !             5
!-------------------------------!---!---!---!---!-------------!---!
!-----------------!-----------------------------------------------!
!/38/             !Somatoria dos registros do lote, incluindo     !
!QUANTIDADE DE RE-!Header e Trailer.                              !
!GISTRO DE LOTE   !                                               !
!-----------------!-----------------------------------------------!
*)
  Txt[05] := AjustaString(FormatFloat('0', Registros+2), '0', 006, posDireita);
(*
!SOMATORIO DO VALOR DOS CHEQUES ! 24! 41! 16! 2 !NUMERICO     !   !             6
!-------------------------------!---!---!---!---!-------------!---!
!SOMATORIO DA QUANT. DE CHEQUES ! 42! 47!  6!   !NUMERICO     !   !             7
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 48! 99! 52! - !BRANCOS      !   !             8
*-----------------------------------------------------------------*
*)
  Txt[06] := MLAGeral.FTX(ValorTotal  , 016, 002, siPositivo);
  Txt[07] := AjustaString(FormatFloat('0', Registros), '0', 006, posDireita);
  Txt[08] := AjustaString(''   , ' ', 052, posEsquerda);
(*
!-----------------------------------------------------------------!
!TOTAL JUROS SOBRE DESCONTO     !100!117! 16! 2 !ALFANUMERICO !   !             9
!-------------------------------!---!---!---!---!-------------!---!
!TOTAL IOF SOBRE DESCONTO       !118!135! 16! 2 !ALFANUMERICO !   !             10
!-------------------------------!---!---!---!---!-------------!---!
!TOTAL OUTROS ENCARGOS S/ DESC. !136!153! 16! 2 !ALFANUMERICO !   !             11
!-------------------------------!---!---!---!---!-------------!---!
!QUANTIDADE CHEQUES PROCESSADOS !154!159!  6! - !ALFANUMERICO !   !             12
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DOS CHEQUES PROCESSADOS  !160!177! 16! 2 !ALFANUMERICO !   !             13
*-----------------------------------------------------------------*
!USO EXCLUSIVO FEBRABAN/CNAB    !178!240! 63! - !ALFANUMERICO !   !             14
*-----------------------------------------------------------------*
*)
  Txt[09] := AjustaString(''                        , ' ', 018, posEsquerda);
  Txt[10] := AjustaString(''                        , ' ', 018, posEsquerda);
  Txt[11] := AjustaString(''                        , ' ', 018, posEsquerda);
  Txt[12] := AjustaString(''                        , ' ', 006, posEsquerda);
  Txt[13] := AjustaString(''                        , ' ', 018, posDireita);
  Txt[14] := AjustaString(''                        , ' ', 063, posDireita);
  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
  VerificaTamanhoTxts(Txt, Tam, MaxS);
end;

function TFmRepasCHCad.GeraTrailerArquivo(Tipo: TTipoGera; Registros:
  Integer): String;
const
  MaxS = 08;
var
  i: Integer;
  Txt: array[1..MaxS] of String;
  Tam: array[1..MaxS] of Integer;
begin
  for i := 1 to MaxS do Txt[i] := '';
  Tam[01] := 003; Tam[02] := 004; Tam[03] := 001; Tam[04] := 009;
  Tam[05] := 006; Tam[06] := 006; Tam[07] := 006; Tam[08] := 205;
  //
  VerificaSomaArray(MaxS, Tam);
(*
TRAILER DE ARQUIVO  -  REGISTRO  9
 *----------------------------------------------------------------*
 *-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-! !
                                !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !             1
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - ! 9999        ! 1 !             2
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO TRAILER DE ARQUIVO    !  8!  8!  1! - ! 9           ! 2 !             3
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !  9! 17!  9! - !BRANCOS      !   !             4
!-------------------------------!---!---!---!---!-------------!---!
*)
  Txt[01] := FormatFloat('000', QrConfigBBBanco.Value);
  Txt[02] := '9999';
  Txt[03] := '9';
  Txt[04] := AjustaString('', ' ', 009, posEsquerda);
(*
!QUANTID. DE LOT ES DO ARQUIVO  ! 18! 23!  6! - !NUM. REGIST. !   ! !           5
                                !   !   !   !   !TIPO - 1     !   !
!-------------------------------!---!---!---!---!-------------!---!
!QUANTID. DE REGISTROS DO ARQUIV! 24! 29!  6! - !NUM.REG.TIPOS!   ! !           6
                                !   !   !   !   ! 0+1+3+5+9   !   !
!-------------------------------!---!---!---!---!-------------!---!
!QTDADE DE CONTAS P/CONC.- LOTS ! 30! 35!  6! - !NUM.REG.     !   ! !           7
                                !   !   !   !   !TIPO-1 OPER-E! E !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 36!240!205! - !BRANCOS      !   !             8
*-----------------------------------------------------------------*
*)
  Txt[05] := AjustaString(FormatFloat('0', 1        ), '0', 006, posDireita);
  Txt[06] := AjustaString(FormatFloat('0', Registros), '0', 006, posDireita);
  Txt[07] := AjustaString(''                 , '1', 006, posDireita);
  Txt[08] := AjustaString('0'                , ' ', 205, posEsquerda);
  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
  VerificaTamanhoTxts(Txt, Tam, MaxS);
end;

////////////////////////////////////////////////////////////////////////////////
/////////  F I M    R E M E S S A  /////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

end.

