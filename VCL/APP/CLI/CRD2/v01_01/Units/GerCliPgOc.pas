unit GerCliPgOc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  DmkDAC_PF, dmkImage, UnDmkEnums;

type
  TFmGerCliPgOc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GBBase: TGroupBox;
    Label86: TLabel;
    TPDataBase6: TDateTimePicker;
    Label87: TLabel;
    EdValorBase6: TdmkEdit;
    Label88: TLabel;
    EdJurosBase6: TdmkEdit;
    Label89: TLabel;
    EdJurosPeriodo6: TdmkEdit;
    GroupBox2: TGroupBox;
    Label82: TLabel;
    TPPagto6: TDateTimePicker;
    Label83: TLabel;
    EdJuros6: TdmkEdit;
    Label85: TLabel;
    EdAPagar6: TdmkEdit;
    Label84: TLabel;
    EdPago6: TdmkEdit;
    QrLocOc: TmySQLQuery;
    QrLocOcData: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValorBase6Change(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalculaAPagarOcor();
    procedure CalculaJurosOcor();
    procedure ConfiguraPgOc(Data: TDateTime);

  end;

  var
  FmGerCliPgOc: TFmGerCliPgOc;

implementation

uses UnMyObjects, Module, UMySQLModule, GerCliMain, MyListas, ModuleLot;

{$R *.DFM}

procedure TFmGerCliPgOc.BtOKClick(Sender: TObject);
var
  FatParcela, Controle, FatID_Sub, Genero, Cliente, Ocorreu,
  FatParcRef: Integer;
  FatNum, Valor, MoraVal: Double;
  Dta: TDateTime;
begin
  Screen.Cursor := crHourGlass;
{
  FmGerCliMain.FOcor rPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Ocor rPG', 'Ocor rPG', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ocor rpg SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
  Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto6.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros6.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago6.Text);
  Dmod.QrUpd.Params[03].AsInteger := 0;
  //
  Dmod.QrUpd.Params[04].AsInteger := FmGerCliMain.FOcorreu;
  Dmod.QrUpd.Params[05].AsInteger := FmGerCliMain.FOcor rPg;
  Dmod.QrUpd.ExecSQL;
  //
}
  FatID_Sub := FmGerCliMain.QrOcorreuOcorrencia.Value;
  Genero := FmGerCliMain.QrOcorreuPlaGen.Value;
  if MyObjects.FIC(Genero=0, nil, 'A ocorr�ncia ' + Geral.FF0(
  FmGerCliMain.QrOcorreuOcorrencia.Value) +
  ' n�o tem cadastrada sua conta do plano de de contas!') then
    Exit;
  Cliente := FmGerCliMain.QrOcorreuCliente.Value;
  Ocorreu := FmGerCliMain.FOcorreu;
  FatParcRef := FmGerCliMain.QrOcorreuLOIS.Value;
  //
  FatNum := 0; // n�o tem! � no Cliente!
  FatParcela := 0;
  Controle := 0;
  Valor := Geral.DMV(EdPago6.Text);
  MoraVal := Geral.DMV(EdJuros6.Text);
  Dta := TPPagto6.Date;
  //
  if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
  FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef,
  FatNum, Valor, MoraVal, Dta, 0) then
  begin
    FmGerCliMain.CalculaPagtoOcorP(TPPagto6.Date, FmGerCliMain.FOcorreu);
    //
    FmGerCliMain.ReopenOcorreu(FmGerCliMain.FOcorreu);
    //
    Screen.Cursor := crDefault;
    //
    Close;
  end;
end;

procedure TFmGerCliPgOc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGerCliPgOc.CalculaAPagarOcor();
var
  Base, Juro: Double;
begin
  Base := EdValorBase6.ValueVariant;
  Juro := EdJuros6.ValueVariant;
  //
  EdAPagar6.ValueVariant := Base + Juro;
end;

procedure TFmGerCliPgOc.CalculaJurosOcor();
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := EdJurosBase6.ValueVariant;
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end;
  Valor := EdValorBase6.ValueVariant;
  EdJurosPeriodo6.ValueVariant := Juros;
  EdJuros6.ValueVariant := Juros * Valor / 100;
end;

procedure TFmGerCliPgOc.ConfiguraPgOc(Data: TDateTime);
var
  ValorBase: Double;
begin
{
  QrLocOc.Close;
  QrLocOc.Params[0].AsInteger := FmGerCliMain.QrOcorreuCodigo.Value;
  UMyMod.AbreQuery(QrLocOc);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocOc, Dmod.MyDB, [
  'SELECT Data ',
  'FROM ' + CO_TabLctA,
  'WHERE FatID=' + TXT_VAR_FATID_0304,
  'AND Data=( ',
  '  SELECT Max(Data) ',
  '  FROM ' + CO_TabLctA,
  '  WHERE FatID=' + TXT_VAR_FATID_0304,
  '  AND Ocorreu=' + Geral.FF0(FmGerCliMain.QrOcorreuCodigo.Value) + ') ',
  'ORDER BY FatNum DESC ',
  '']);
  //
  TPPagto6.MinDate := 0;
  if QrLocOc.RecordCount > 0 then
  begin
    TPPagto6.Date     := Int(QrLocOcData.Value);
    TPDataBase6.Date  := Int(QrLocOcData.Value);
  end else begin
    TPPagto6.Date    := Int(FmGerCliMain.QrOcorreuDataO.Value);
    TPDataBase6.Date := Int(FmGerCliMain.QrOcorreuDataO.Value);
  end;
  EdJurosBase6.ValueVariant :=
    Dmod.ObtemTaxaDeCompraCliente(FmGerCliMain.EdCliente.ValueVariant);
  ValorBase :=
    FmGerCliMain.QrOcorreuValor.Value +
    FmGerCliMain.QrOcorreuTaxaV.Value -
    FmGerCliMain.QrOcorreuPago.Value;
  EdValorBase6.ValueVariant := ValorBase;
  TPPagto6.MinDate := TPPagto6.Date;
  if Date > TPPagto6.MinDate then TPPagto6.Date := Int(Date);
end;

procedure TFmGerCliPgOc.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.Text := EdAPagar6.Text;
end;

procedure TFmGerCliPgOc.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := Geral.DMV(EdValorBase6.Text);
  Juros := Geral.DMV(EdJuros6.Text);
  EdAPagar6.Text := Geral.FFT(VBase+Juros, 2, siNegativo);
end;

procedure TFmGerCliPgOc.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor();
  CalculaAPagarOcor();
end;

procedure TFmGerCliPgOc.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor();
end;

procedure TFmGerCliPgOc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGerCliPgOc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
end;

procedure TFmGerCliPgOc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGerCliPgOc.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor();
end;

end.
