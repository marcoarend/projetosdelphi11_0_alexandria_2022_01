unit GruSacEmi;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, UnDmkProcFunc, UnDmkEnums;

type
  TFmGruSacEmi = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrGruSacEmi: TmySQLQuery;
    QrGruSacEmiLk: TIntegerField;
    QrGruSacEmiDataCad: TDateField;
    QrGruSacEmiDataAlt: TDateField;
    QrGruSacEmiUserCad: TIntegerField;
    QrGruSacEmiUserAlt: TIntegerField;
    QrGruSacEmiCodigo: TSmallintField;
    QrGruSacEmiNome: TWideStringField;
    DsGruSacEmi: TDataSource;
    QrGruSacEmiIts: TmySQLQuery;
    QrGruSacEmiItsCodigo: TIntegerField;
    QrGruSacEmiItsCNPJ_CPF: TWideStringField;
    QrGruSacEmiItsNome: TWideStringField;
    QrGruSacEmiItsCNPJ_CPF_TXT: TWideStringField;
    DsGruSacEmiIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    N1: TMenuItem;
    Nomeiagrupo1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGruSacEmiAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGruSacEmiBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGruSacEmiAfterScroll(DataSet: TDataSet);
    procedure QrGruSacEmiItsCalcFields(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure Nomeiagrupo1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraGruSacEmiIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenGruSacEmiIts();

  end;

var
  FmGruSacEmi: TFmGruSacEmi;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, GruSacEmiIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGruSacEmi.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGruSacEmi.MostraGruSacEmiIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGruSacEmiIts, FmGruSacEmiIts, afmoNegarComAviso) then
  begin
    FmGruSacEmiIts.ImgTipo.SQLType := SQLType;
    if SQLType = stIns then
      FmGruSacEmiIts.EdCPF1.ReadOnly := False
    else
    begin
      FmGruSacEmiIts.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrGruSacEmiItsCNPJ_CPF.Value);
      FmGruSacEmiIts.EdNomeEmiSac.Text := QrGruSacEmiItsNome.Value;
      FmGruSacEmiIts.EdCPF1.ReadOnly := True;
    end;
    FmGruSacEmiIts.ShowModal;
    FmGruSacEmiIts.Destroy;
  end;
end;

procedure TFmGruSacEmi.Nomeiagrupo1Click(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
begin
  Nome := QrGruSacEmiItsNome.Value;
  Codigo := QrGruSacEmiCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'grusacemi', False, [
  'Nome'], ['Codigo'], [Nome], [Codigo], False) then
    LocCod(Codigo, Codigo);
end;

procedure TFmGruSacEmi.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrGruSacEmi);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrGruSacEmi, QrGruSacEmiIts);
end;

procedure TFmGruSacEmi.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrGruSacEmi);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrGruSacEmiIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrGruSacEmiIts);
  //
  MyObjects.HabilitaMenuItemItsDel(Nomeiagrupo1, QrGruSacEmiIts);
end;

procedure TFmGruSacEmi.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruSacEmiCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGruSacEmi.DefParams;
begin
  VAR_GOTOTABELA := 'grusacemi';
  VAR_GOTOMYSQLTABLE := QrGruSacEmi;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM grusacemi');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGruSacEmi.ItsAltera1Click(Sender: TObject);
begin
  MostraGruSacEmiIts(stUpd);
end;

procedure TFmGruSacEmi.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmGruSacEmi.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGruSacEmi.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGruSacEmi.ItsExclui1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma do sacado / emitente do grupo atual?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM grusacemiits WHERE CNPJ_CPF=:P0');
    Dmod.QrUpd.SQL.Add('AND Codigo=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[0].AsString  := QrGruSacEmiItsCNPJ_CPF.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrGruSacEmiItsCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenGruSacEmiIts;
  end;
end;

procedure TFmGruSacEmi.ReopenGruSacEmiIts();
begin
  QrGruSacEmiIts.Close;
  QrGruSacEmiIts.Params[0].AsInteger := QrGruSacEmiCodigo.Value;
  UMyMod.AbreQuery(QrGruSacEmiIts, Dmod.MyDB);
end;


procedure TFmGruSacEmi.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGruSacEmi.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGruSacEmi.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGruSacEmi.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGruSacEmi.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGruSacEmi.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGruSacEmi.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGruSacEmiCodigo.Value;
  Close;
end;

procedure TFmGruSacEmi.ItsInclui1Click(Sender: TObject);
begin
  MostraGruSacEmiIts(stIns);
end;

procedure TFmGruSacEmi.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGruSacEmi, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'grusacemi');
end;

procedure TFmGruSacEmi.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('grusacemi', 'Codigo', ImgTipo.SQLType,
    QrGruSacEmiCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmGruSacEmi, PnEdita,
    'grusacemi', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmGruSacEmi.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'grusacemi', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'grusacemi', 'Codigo');
end;

procedure TFmGruSacEmi.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmGruSacEmi.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmGruSacEmi.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmGruSacEmi.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruSacEmiCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGruSacEmi.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGruSacEmi.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGruSacEmiCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGruSacEmi.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGruSacEmi.QrGruSacEmiAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGruSacEmi.QrGruSacEmiAfterScroll(DataSet: TDataSet);
begin
  ReopenGruSacEmiIts();
end;

procedure TFmGruSacEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrGruSacEmiCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmGruSacEmi.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGruSacEmiCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'grusacemi', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGruSacEmi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGruSacEmi.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGruSacEmi, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'grusacemi');
end;

procedure TFmGruSacEmi.QrGruSacEmiBeforeOpen(DataSet: TDataSet);
begin
  QrGruSacEmiCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGruSacEmi.QrGruSacEmiItsCalcFields(DataSet: TDataSet);
begin
  QrGruSacEmiItsCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrGruSacEmiItsCNPJ_CPF.Value);
end;

end.

