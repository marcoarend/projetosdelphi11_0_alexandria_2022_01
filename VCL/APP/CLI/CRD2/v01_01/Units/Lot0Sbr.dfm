object FmLot0Sbr: TFmLot0Sbr
  Left = 339
  Top = 185
  Caption = 'BDR-GEREN-017 :: Controle de Sobras de Border'#244's'
  ClientHeight = 403
  ClientWidth = 913
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 913
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 865
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 817
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 391
        Height = 32
        Caption = 'Controle de Sobras de Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 32
        Caption = 'Controle de Sobras de Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 391
        Height = 32
        Caption = 'Controle de Sobras de Border'#244's'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 913
    Height = 228
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 913
      Height = 228
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 913
        Height = 228
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 276
    Width = 913
    Height = 57
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 909
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 23
        Width = 909
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 333
    Width = 913
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 767
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 765
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 319
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtBordero: TBitBtn
        Tag = 116
        Left = 261
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Border'#244
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtBorderoClick
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 48
    Width = 913
    Height = 228
    Align = alClient
    DataSource = DsSobraCli
    TabOrder = 4
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Cliente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NomCli'
        Title.Caption = 'Nome do cliente'
        Width = 355
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Lote'
        Title.Caption = 'Border'#244
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SobraIni'
        Title.Caption = 'Sobra ini.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LotAnt'
        Title.Caption = 'Border'#244' ant.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CodAnt'
        Title.Caption = 'Lote ant.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SobraAnt'
        Title.Caption = 'Sobra ant.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SobraErr'
        Title.Caption = 'Erro'
        Visible = True
      end>
  end
  object QrLots: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Cliente, Lote, Data, Codigo,'
      'SobraIni, SobraNow'
      'FROM creditor.lot0001a'
      'WHERE SobraIni <> 0'
      'OR SobraNow <> 0'
      'ORDER BY Cliente, Data, Codigo;')
    Left = 120
    Top = 116
    object QrLotsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrLotsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotsData: TDateField
      FieldName = 'Data'
    end
    object QrLotsNomCli: TWideStringField
      FieldName = 'NomCli'
      Size = 100
    end
    object QrLotsSobraAnt: TFloatField
      FieldName = 'SobraAnt'
    end
    object QrLotsSobraIni: TFloatField
      FieldName = 'SobraIni'
    end
    object QrLotsSobraNow: TFloatField
      FieldName = 'SobraNow'
    end
    object QrLotsSobraErr: TFloatField
      FieldName = 'SobraErr'
    end
    object QrLotsChecado: TIntegerField
      FieldName = 'Checado'
    end
    object QrLotsCodAnt: TIntegerField
      FieldName = 'CodAnt'
    end
    object QrLotsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLotsLotAnt: TIntegerField
      FieldName = 'LotAnt'
    end
  end
  object QrSobraCli: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrSobraCliAfterOpen
    BeforeClose = QrSobraCliBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM _sobr_cli_'
      'ORDER BY Cliente, Data, Codigo;'
      '')
    Left = 148
    Top = 116
    object QrSobraCliCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSobraCliLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrSobraCliCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSobraCliData: TDateField
      FieldName = 'Data'
    end
    object QrSobraCliNomCli: TWideStringField
      FieldName = 'NomCli'
      Size = 100
    end
    object QrSobraCliSobraAnt: TFloatField
      FieldName = 'SobraAnt'
    end
    object QrSobraCliSobraIni: TFloatField
      FieldName = 'SobraIni'
    end
    object QrSobraCliSobraNow: TFloatField
      FieldName = 'SobraNow'
    end
    object QrSobraCliSobraErr: TFloatField
      FieldName = 'SobraErr'
    end
    object QrSobraCliChecado: TIntegerField
      FieldName = 'Checado'
    end
    object QrSobraCliCodAnt: TIntegerField
      FieldName = 'CodAnt'
    end
    object QrSobraCliAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSobraCliLotAnt: TIntegerField
      FieldName = 'LotAnt'
    end
  end
  object DsSobraCli: TDataSource
    DataSet = QrSobraCli
    Left = 176
    Top = 116
  end
  object frxBDR_GEREN_017_001: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 204
    Top = 116
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsSobraCli
        DataSetName = 'frxDsSobraCli'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsSobraCli
        DataSetName = 'frxDsSobraCli'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSobraCli."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 56.692852360000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSobraCli."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 461.102503780000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSobraCli."SobraAnt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 192.755744410000000000
          Width = 68.031491180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSobraCli."SobraIni"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 589.606301650000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSobraCli."SobraErr"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 124.724490000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Lote'
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSobraCli."Lote"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 325.039580000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'CodAnt'
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSobraCli."CodAnt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 393.071217640000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsSobraCli
          DataSetName = 'frxDsSobraCli'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSobraCli."LotAnt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object TfrxGroupHeader
        Height = 56.692940240000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSobraCli."Cliente"'
        object Memo10: TfrxMemoView
          Top = 41.574815350000000000
          Width = 56.692913385826770000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 56.692852360000000000
          Top = 41.574815350000000000
          Width = 68.031496062992130000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 461.102503780000000000
          Top = 41.574815350000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sobra final')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 192.755737090000000000
          Top = 41.574815350000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sobra inic.')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 589.606338270000000000
          Top = 26.456695350000000000
          Width = 94.488188980000000000
          Height = 30.236230240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = -0.000048820000000000
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSobraCli."Cliente"] - [frxDsSobraCli."NomCli"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 124.724490000000000000
          Top = 41.574815350000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 325.039482360000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 393.071120000000000000
          Top = 41.574830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Top = 26.456710000000000000
          Width = 260.787570000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote com a diferen'#231'a inicial')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 325.039580000000000000
          Top = 26.456710000000000000
          Width = 204.094620000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote anterior (com valores de saldos)')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object TfrxGroupFooter
        Height = 18.897637800000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 589.606301650000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSobraCli."SobraErr">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Width = 581.575140000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Total per'#237'odo: [frxDsSobraCli."NomCli"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 83.149660000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Diferen'#231'as de Saldo Anterior X Saldo Inicial')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 559.370440000000000000
          Top = 18.897650000000000000
          Width = 113.385836540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente: TODOS')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: TUDO N'#195'O ARQUIVADO')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 404.409710000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 22.677180000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Left = 589.606301650000000000
          Top = 3.779527560000000000
          Width = 94.488188980000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSobraCli."SobraErr">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Top = 3.779530000000000000
          Width = 604.252320000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total geral per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsSobraCli: TfrxDBDataset
    UserName = 'frxDsSobraCli'
    CloseDataSource = False
    DataSet = QrSobraCli
    BCDToCurrency = False
    Left = 232
    Top = 116
  end
  object PMBordero: TPopupMenu
    Left = 260
    Top = 116
    object Lotedasobraanterior1: TMenuItem
      Caption = 'Lote da sobra &anterior'
      OnClick = Lotedasobraanterior1Click
    end
    object Lotedasobrainicial1: TMenuItem
      Caption = 'Lote da sobra &inicial'
      OnClick = Lotedasobrainicial1Click
    end
  end
end
