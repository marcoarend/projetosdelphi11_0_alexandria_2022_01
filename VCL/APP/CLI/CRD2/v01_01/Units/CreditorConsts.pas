unit CreditorConsts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  (*DBTables,*) StdCtrls, comctrls, mySQLDbTables, dmkGeral;

type
  TCreditorConsts = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetaVariaveisIniciais;
    procedure AvisaSetarOpcoes;
  end;

const
  CRD_CAMINHO_REGEDIT = 'Dermatek\Cred' + 'itor';
  CRD_MAX_EMPRESAS    = 6;
var
  CRD_Consts: TCreditorConsts;

implementation

uses UnMyObjects, UnMLAGeral, Principal;

procedure TCreditorConsts.AvisaSetarOpcoes;
var
  Caminho: String;
begin
  Caminho := CRD_CAMINHO_REGEDIT+'\Opcoes';
  if not Geral.ReadAppKey('JaSetado', Caminho, ktBoolean, False,
  HKEY_LOCAL_MACHINE) then
  begin
    if Application.MessageBox(PChar('O aplicativo detectou que as op��es do '+
    'aplicativo n�o foram configuradas ainda. Deseja configur�-las agora?'),
    'Aviso de Op��es', MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) =
    ID_YES then FmPrincipal.MostraOpcoesCreditoX();
  end;
end;

procedure TCreditorConsts.SetaVariaveisIniciais;
begin
  //
end;

end.
 
