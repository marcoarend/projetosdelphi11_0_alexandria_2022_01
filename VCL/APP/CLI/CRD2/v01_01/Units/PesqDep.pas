unit PesqDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, DBCtrls, Db, mySQLDbTables,
  Grids, DBGrids, dmkEdit, ComCtrls, Variants, Menus, frxClass, frxDBSet,
  dmkImage, dmkCheckGroup, DmkDAC_PF, UnDmkEnums;

type
  TFmPesqDep = class(TForm)
    Panel1: TPanel;
    QrDepIts: TmySQLQuery;
    DsDepIts: TDataSource;
    Panel3: TPanel;
    Valor: TLabel;
    frxDepIts: TfrxReport;
    frxDsDepIts: TfrxDBDataset;
    CkPeriodo: TCheckBox;
    TPIniDep: TDateTimePicker;
    TPFimDep: TDateTimePicker;
    dmkEdValor: TdmkEdit;
    QrDepItsCPF_TXT: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtPesq: TBitBtn;
    BtImprimir: TBitBtn;
    QrDepItsFatNum: TFloatField;
    QrDepItsCliente: TIntegerField;
    QrDepItsBanco: TIntegerField;
    QrDepItsAgencia: TIntegerField;
    QrDepItsContaCorrente: TWideStringField;
    QrDepItsDocumento: TFloatField;
    QrDepItsNF: TIntegerField;
    QrDepItsEmitente: TWideStringField;
    QrDepItsCNPJCPF: TWideStringField;
    QrDepItsVencimento: TDateField;
    QrDepItsDDeposito: TDateField;
    QrDepItsCredito: TFloatField;
    QrDepItsDescricao: TWideStringField;
    Panel2: TPanel;
    CGDepositado: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure frxDepItsGetValue(const ParName: string; var ParValue: Variant);
    procedure BtImprimirClick(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure QrDepItsCalcFields(DataSet: TDataSet);
    procedure QrDepItsBeforeClose(DataSet: TDataSet);
    procedure QrDepItsAfterOpen(DataSet: TDataSet);
    procedure dmkEdValorChange(Sender: TObject);
    procedure TPIniDepChange(Sender: TObject);
    procedure TPIniDepClick(Sender: TObject);
  private
    { Private declarations }
    procedure FechaPesq();
  public
    { Public declarations }
  end;

  var
  FmPesqDep: TFmPesqDep;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, MyDBCheck, MyListas,
  TedC_Aux, UnDmkProcFunc;

{$R *.DFM}

procedure TFmPesqDep.BtImprimirClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxDepIts, 'Dep�sito de cheques');
end;

procedure TFmPesqDep.BtPesqClick(Sender: TObject);
var
  Valor : Double;
  Periodo : Boolean;
  Depositado: String;
begin
  if CkPeriodo.Checked = false then
    if Application.MessageBox('N�o foi definido nenhum per�odo. '+
    #13#10+'A pesquisa poder� demorar. Deseja continuar assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;
  if MyObjects.FIC(CGDepositado.Value = 0, CGDepositado,
  'Informe o status de dep�sito') then
    Exit;

  case CGDepositado.Value of
    1: Depositado := '0';
    2: Depositado := '1';
    3: Depositado := '0,1';
    4: Depositado := '2';
    5: Depositado := '0,2';
    6: Depositado := '1,2';
    7: Depositado := '0,1,2';
    else
    begin
      Geral.MensagemBox('Status de dep�sito n�o implementado!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  //
  Periodo := CkPeriodo.Checked;
  Valor   := dmkEdValor.ValueVariant;
  //
{
  QrDepIts.Close;
  QrDepIts.SQL.Clear;
  QrDepIts.SQL.Add('SELECT li.Codigo, lo.Cliente, li.Banco, li.Agencia, li.Conta,');
  QrDepIts.SQL.Add('li.Cheque, lo.NF, li.Emitente, li.CPF, li.Vencto,');
  QrDepIts.SQL.Add('li.DDeposito, li.Valor, li.ObsGerais');
  QrDepIts.SQL.Add('FROM lot esits li');
  QrDepIts.SQL.Add('LEFT JOIN lot es lo ON lo.Codigo = li.Codigo');
  QrDepIts.SQL.Add('WHERE lo.TxCompra+lo.ValValorem+ 1 >= 0.01');
  QrDepIts.SQL.Add('AND li.Depositado = 1');
  //
  if (Periodo = true) then
    QrDepIts.SQL.Add('AND li.DDeposito BETWEEN "' + Geral.FDT(TPIniDep.Date, 1) + '" AND "' + Geral.FDT(TPFimDep.Date, 1) +'"');
  //
  if Valor <> 0 then
    QrDepIts.SQL.Add('AND li.Valor =' + FloatToStr(Valor));
  //
  QrDepIts.SQL.Add('ORDER BY li.Vencto, li.ObsGerais');
  QrDepIts. Open;
}

  UnDmkDAC_PF.AbreMySQLQuery0(QrDepIts, Dmod.MyDB, [
  'SELECT li.FatNum, lo.Cliente, li.Banco, li.Agencia, li.ContaCorrente, ',
  'li.Documento, lo.NF, li.Emitente, li.CNPJCPF, li.Vencimento, ',
  'li.DDeposito, li.Credito, li.Descricao ',
  'FROM ' + CO_TabLctA + ' li ',
  'LEFT JOIN ' + CO_TabLotA + ' lo ON lo.Codigo = li.FatNum ',
  'WHERE lo.TxCompra + lo.ValValorem + 1 >= 0.01 ',
  //'AND li.Depositado = ' + Geral.FF0(CO_CH_DEPOSITADO_MANUAL),
  'AND li.Depositado IN (' + Depositado + ')',
  Geral.ATS_if(Periodo = True, [
  'AND li.DDeposito BETWEEN "' + Geral.FDT(TPIniDep.Date, 1) +
  '" AND "' + Geral.FDT(TPFimDep.Date, 1) + '"']),
  Geral.ATS_if(Valor <> 0, [
  'AND li.Credito =' + Geral.FFT_Dot(Valor, 2, siNegativo)]),
  'ORDER BY li.Vencimento, li.Descricao',
  '']);
  //
  Screen.Cursor := crDefault;
  //
end;

procedure TFmPesqDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqDep.CkPeriodoClick(Sender: TObject);
begin
  TPIniDep.Visible := CkPeriodo.Checked;
  TPFimDep.Visible := CkPeriodo.Checked;
  QrDepIts.Close;
end;

procedure TFmPesqDep.dmkEdValorChange(Sender: TObject);
begin
  FechaPesq();
end;

procedure TFmPesqDep.FechaPesq();
begin
  QrDepIts.Close;
end;

procedure TFmPesqDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqDep.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  CkPeriodo.Checked := True;
  TPIniDep.Date := Geral.PrimeiroDiaDoMes(Date);
  TPFimDep.Date := Geral.UltimoDiaDoMes(Date);
  CGDepositado.Value := 6; // CO_CH_DEPOSITADO_MANUAL + CO_CH_DEPOSITADO_TED
  dmkEdValor.ValueVariant := 0;
end;

procedure TFmPesqDep.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;


procedure TFmPesqDep.frxDepItsGetValue(const ParName: string;
  var ParValue: Variant);
begin
  if ParName = 'VARF_PERIODO' then ParValue := dmkPF.PeriodoImp(TPIniDep.Date,
  TPFimDep.Date, 0, 0, True, True, False, False, '', '')
  else
  if ParName = 'VARF_NO_STATUS' then
  begin
    case CGDepositado.Value of
      1: ParValue := 'N�o depositado';
      2: ParValue := 'Depositado manualmente';
      3: ParValue := 'N�o depositado ou depositado manualmente';
      4: ParValue := 'Depositado via TED';
      5: ParValue := 'N�o depositado ou depositado via TED';
      6: ParValue := 'Depositado manualmente ou depositado via TED';
      7: ParValue := 'N�o depositado + depositado manualmente + depositado via TED';
      else ParValue := '? ? ? ? ?';
    end;
  end
end;

procedure TFmPesqDep.QrDepItsAfterOpen(DataSet: TDataSet);
begin
  BtImprimir.Enabled := True;
end;

procedure TFmPesqDep.QrDepItsBeforeClose(DataSet: TDataSet);
begin
  BtImprimir.Enabled := False;
end;

procedure TFmPesqDep.QrDepItsCalcFields(DataSet: TDataSet);
begin
  QrDepItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDepItsCNPJCPF.Value);
end;

procedure TFmPesqDep.TPIniDepChange(Sender: TObject);
begin
  FechaPesq();
end;

procedure TFmPesqDep.TPIniDepClick(Sender: TObject);
begin
  FechaPesq();
end;

end.

