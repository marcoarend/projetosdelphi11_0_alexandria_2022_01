object FmGerDup1Ocor: TFmGerDup1Ocor
  Left = 339
  Top = 185
  Caption = 'DUP-CNTRL-003 :: Ocorr'#234'ncia em Duplicata'
  ClientHeight = 311
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 567
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 519
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 471
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 300
        Height = 32
        Caption = 'Ocorr'#234'ncia em Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 300
        Height = 32
        Caption = 'Ocorr'#234'ncia em Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 300
        Height = 32
        Caption = 'Ocorr'#234'ncia em Duplicata'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 567
    Height = 149
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 567
      Height = 149
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 567
        Height = 149
        Align = alClient
        TabOrder = 0
        object PainelOcor: TPanel
          Left = 2
          Top = 15
          Width = 563
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          Color = clAppWorkSpace
          TabOrder = 0
          object GBPror: TGroupBox
            Left = 0
            Top = 0
            Width = 563
            Height = 61
            Align = alTop
            Caption = ' Prorroga'#231#227'o: '
            TabOrder = 0
            object Label13: TLabel
              Left = 12
              Top = 16
              Width = 84
              Height = 13
              Caption = 'Novo resgate em:'
            end
            object Label14: TLabel
              Left = 108
              Top = 16
              Width = 78
              Height = 13
              Caption = 'Taxa juros base:'
            end
            object Label29: TLabel
              Left = 192
              Top = 16
              Width = 79
              Height = 13
              Caption = '% Juros per'#237'odo:'
            end
            object Label15: TLabel
              Left = 276
              Top = 16
              Width = 62
              Height = 13
              Caption = '$ Valor base:'
            end
            object Label27: TLabel
              Left = 368
              Top = 16
              Width = 37
              Height = 13
              Caption = '$ Juros:'
            end
            object Label16: TLabel
              Left = 460
              Top = 16
              Width = 72
              Height = 13
              Caption = '1'#186' Vencimento:'
            end
            object TPDataN: TDateTimePicker
              Left = 12
              Top = 32
              Width = 93
              Height = 21
              Date = 38711.818866817100000000
              Time = 38711.818866817100000000
              TabOrder = 0
              OnChange = TPDataNChange
            end
            object EdTaxaPror: TdmkEdit
              Left = 108
              Top = 32
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdTaxaProrChange
            end
            object EdJurosPeriodoN: TdmkEdit
              Left = 192
              Top = 32
              Width = 81
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdValorBase: TdmkEdit
              Left = 276
              Top = 32
              Width = 88
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdValorBaseChange
            end
            object EdJurosPr: TdmkEdit
              Left = 368
              Top = 32
              Width = 88
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnChange = EdJurosPrChange
            end
            object TPDataI: TDateTimePicker
              Left = 460
              Top = 32
              Width = 93
              Height = 21
              Date = 38711.818866817100000000
              Time = 38711.818866817100000000
              Enabled = False
              TabOrder = 5
              TabStop = False
              OnChange = TPDataIChange
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 61
            Width = 563
            Height = 64
            Align = alTop
            Caption = ' Ocorr'#234'ncia: '
            TabOrder = 1
            object Label20: TLabel
              Left = 8
              Top = 16
              Width = 55
              Height = 13
              Caption = 'Ocorr'#234'ncia:'
            end
            object Label21: TLabel
              Left = 360
              Top = 16
              Width = 29
              Height = 13
              Caption = 'Data: '
            end
            object Label22: TLabel
              Left = 468
              Top = 16
              Width = 27
              Height = 13
              Caption = 'Valor:'
            end
            object SpeedButton1: TSpeedButton
              Left = 336
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton1Click
            end
            object EdOcorrencia: TdmkEditCB
              Left = 8
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnExit = EdOcorrenciaExit
              DBLookupComboBox = CBOcorrencia
            end
            object CBOcorrencia: TdmkDBLookupComboBox
              Left = 64
              Top = 32
              Width = 269
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOcorBank
              TabOrder = 1
              dmkEditCB = EdOcorrencia
              UpdType = utYes
            end
            object TPDataO: TDateTimePicker
              Left = 360
              Top = 32
              Width = 105
              Height = 21
              Date = 38711.818866817100000000
              Time = 38711.818866817100000000
              TabOrder = 2
            end
            object EdValor: TdmkEdit
              Left = 468
              Top = 32
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 197
    Width = 567
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 563
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 241
    Width = 567
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 421
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 419
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorbank'
      'ORDER BY Nome')
    Left = 368
    Top = 16
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 396
    Top = 16
  end
  object QrLocPr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lot esprr'
      'WHERE Data3=('
      'SELECT Max(Data3) FROM lot esprr'
      'WHERE Codigo=:P0)'
      'ORDER BY Controle DESC')
    Left = 20
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocPrData1: TDateField
      FieldName = 'Data1'
    end
    object QrLocPrData2: TDateField
      FieldName = 'Data2'
    end
    object QrLocPrData3: TDateField
      FieldName = 'Data3'
    end
    object QrLocPrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLocPrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
end
