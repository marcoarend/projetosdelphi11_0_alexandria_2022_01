unit LocPag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Grids, DBGrids, Db,
  mySQLDbTables, DBCtrls, frxClass, frxDBSet, dmkGeral, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmLocPag = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    EdMinimo: TdmkEdit;
    CkMinimo: TCheckBox;
    CkMaximo: TCheckBox;
    EdMaximo: TdmkEdit;
    GroupBox2: TGroupBox;
    TPIniA: TDateTimePicker;
    TPFimA: TDateTimePicker;
    CkIniA: TCheckBox;
    CkFimA: TCheckBox;
    GroupBox3: TGroupBox;
    CkCheque: TCheckBox;
    EdCheque: TdmkEdit;
    DBGrid1: TDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqData: TDateField;
    QrPesqDebito: TFloatField;
    QrPesqDocumento: TFloatField;
    QrPesqVencimento: TDateField;
    QrPesqCarteira: TIntegerField;
    QrPesqBanco: TIntegerField;
    QrPesqContaCorrente: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqNOMECLI: TWideStringField;
    QrPesqNOMECARTEIRA: TWideStringField;
    Panel3: TPanel;
    LaCliente: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLI: TWideStringField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasCodigo: TIntegerField;
    QrPesqLote: TIntegerField;
    QrPesqTipo: TSmallintField;
    frxReport1: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    QrPesqNF: TIntegerField;
    QrPesqFatNum: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    CkAutomatico: TCheckBox;
    BtCancela: TBitBtn;
    BitBtn1: TBitBtn;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPesqAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdMinimoExit(Sender: TObject);
    procedure EdMaximoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CkMinimoClick(Sender: TObject);
    procedure CkMaximoClick(Sender: TObject);
    procedure TPIniAChange(Sender: TObject);
    procedure CkFimAClick(Sender: TObject);
    procedure TPFimAChange(Sender: TObject);
    procedure CkChequeClick(Sender: TObject);
    procedure CkIniAClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxReport1GetValue(const VarName: String;
      var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
  private
    { Private declarations }
    FRegistryPath: String;
    //FEmpresa,
    FCliInt, FEntidade: Integer;
    //FEntidade_TXT,
    FATID_0302_Txt,
    FTabLctA, FTabLctB, FTabLctD,
    FTabLotA, FTabLotB, FTabLotD: String;
    FDtEncer, FDtMorto: TDateTime;
    procedure Pesquisa(Forca: Boolean);
  public
    { Public declarations }
  end;

  var
  FmLocPag: TFmLocPag;

implementation

uses UnMyObjects, Module, Principal, UnInternalConsts, ModuleGeral,
  UMySQLModule;

{$R *.DFM}

procedure TFmLocPag.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocPag.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
end;

procedure TFmLocPag.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLocPag.EdMinimoExit(Sender: TObject);
begin
  if Geral.DMV(EdMaximo.Text) < Geral.DMV(EdMinimo.Text) then
  EdMaximo.Text := EdMinimo.Text;
  Pesquisa(False);
end;

procedure TFmLocPag.EdMaximoExit(Sender: TObject);
begin
  Pesquisa(False);  
end;

procedure TFmLocPag.FormCreate(Sender: TObject);
begin
  FATID_0302_Txt := FormatFloat('0', VAR_FATID_0302);
  //
  ImgTipo.SQLType := stPsq;
  //
  FRegistryPath := Application.Title+'\LocalizaPagto';
  CkAutomatico.Checked := Geral.ReadAppKey('Automatico', FRegistryPath,
    ktBoolean, True, HKEY_LOCAL_MACHINE);
  TPIniA.Date := Date - 30;
  TPFimA.Date := Date;
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmLocPag.EdChequeExit(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.EdClienteChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.EdEmpresaChange(Sender: TObject);
begin
  DModG.All_ABD(
    TMeuDB, FEntidade, FCliInt, FDtEncer, FDtMorto, FTabLctA, FTabLctB, FTabLctD);
  DModG.Def_EM_xID(FTabLctA, FTabLctB, FTabLctD, 'lot', FTabLotA, FTabLotB, FTabLotD);
end;

procedure TFmLocPag.EdCarteiraChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.BtOKClick(Sender: TObject);
begin
  Pesquisa(True);
end;

procedure TFmLocPag.Pesquisa(Forca: Boolean);
var
  Minimo, Maximo: Double;
  //Carteira, Cliente: Integer;
  DataI, DataF: TDateTime;
begin
  if (not Forca) and (not CkAutomatico.Checked) then Exit;
  if MyObjects.FIC(FEntidade = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  //
  Minimo := EdMinimo.ValueVariant;
  Maximo := EdMaximo.ValueVariant;
  //Carteira := EdCarteira.ValueVariant;
  //Cliente := EdCliente.ValueVariant;
  if CkMaximo.Checked and CkMinimo.Checked then
  begin
    if (Maximo < Minimo) then
    begin
      Geral.MensagemBox('Valor m�ximo n�o pode ser menor que m�nimo!',
        'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    DataI := MLAGeral.EscolhaDe2Dta(CkIniA.Checked,  TPIniA.Date, 0);
    DataF := MLAGeral.EscolhaDe2Dta(CkFimA.Checked,  TPFimA.Date, 0);
    //
    UMyMod.AbreSQL_ABD(QrPesq, ''(*'_PESQ_LOC_PAG_'*), FTabLctA, FTabLctB,
    FTabLctD, FTabLotA, FTabLotB, FTabLotD, DataI, DataF, FDtEncer, FDtMorto, [
    'SELECT la.Data, la.Debito, la.Documento, la.Vencimento,',
    'la.Carteira, la.FatNum, la.Banco, la.Agencia,',
    'la.ContaCorrente, la.Cliente, CASE WHEN cl.Tipo=0',
    'THEN cl.RazaoSocial ELSE cl.Nome END NOMECLI,',
    'ca.Nome NOMECARTEIRA, lo.Lote, lo.Tipo, lo.NF',
    'FROM ' + LCT_MASK  + ' la',
    'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
    'LEFT JOIN ' + _ID_MASK + ' lo ON lo.Codigo=la.FatNum',
    'WHERE la.FatID IN (' + FATID_0302_Txt + ')',
    dmkPF.SQL_Periodo('AND la.Data ', TPIniA.Date, TPFimA.Date,
      CkIniA.Checked, CkFimA.Checked),
    MLAGeral.SQL_CodTxt('AND la.Documento ', EdCheque.Text,
      True, True, not CkCheque.Checked),
    MLAGeral.SQL_CodTxt('AND la.Cliente ', EdCliente.Text,
      False, True, False),
    MLAGeral.SQL_CodTxt('AND la.Carteira ', EdCarteira.Text,
      False, True, False),
    MLAGeral.SQL_Valores('AND la.Debito ', Minimo, Maximo,
      CkMinimo.Checked, CkMaximo.Checked),
    ''], [
    'ORDER BY Data DESC, FatNum',
    ''], tetNaoExclui, 'ExpContabExp.ReopenPagErr()');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocPag.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKey('Automatico', FRegistryPath, CkAutomatico.Checked,
    ktBoolean, HKEY_LOCAL_MACHINE);
end;

procedure TFmLocPag.CkMinimoClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkMaximoClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.TPIniAChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkFimAClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.TPFimAChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkChequeClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkIniAClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.BtCancelaClick(Sender: TObject);
begin
  FmPrincipal.CriaFormLot(2, QrPesqTipo.Value, Trunc(QrPesqFatNum.Value), 0);
end;

procedure TFmLocPag.DBGrid1DblClick(Sender: TObject);
begin
  if QrPesq.State = dsBrowse then
    FmPrincipal.CriaFormLot(2, QrPesqTipo.Value, Trunc(QrPesqFatNum.Value), 0);
end;

procedure TFmLocPag.BitBtn1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxReport1, 'Pesquisa de Pagamentos');
end;

procedure TFmLocPag.frxReport1GetValue(const VarName: String;
  var Value: Variant);
var
  IniA, FimA: String;
begin
  if VarName = 'INTERVALO_VALORES' then
  begin
    if CkMinimo.Checked and CkMaximo.Checked then Value := 'M�nimo = '+
      EdMinimo.Text + ' e M�ximo = ' + EdMaximo.Text
    else if CkMinimo.Checked then Value := 'M�nimo = '+ EdMinimo.Text
    else if CkMaximo.Checked then Value := 'M�ximo = '+ EdMaximo.Text
    else Value := 'Qualquer'
  end else if VarName = 'INTERVALO_DATAS' then
  begin
    IniA := Geral.FDT(TPIniA.Date, 2);
    FimA := Geral.FDT(TPFimA.Date, 2);
    if CkIniA.Checked and CkFimA.Checked then Value := 'In�cio = '+
      IniA + ' e Final = ' + FimA
    else if CkIniA.Checked then Value := 'In�cio = '+ IniA
    else if CkFimA.Checked then Value := 'Final = '+ FimA
    else Value := 'Qualquer'
  end else if VarName = 'BENEFICIARIO' then
    Value := MLAGeral.CampoReportTxt(CBCliente.Text, 'Todos')
  else if VarName = 'CARTEIRA' then
    Value := MLAGeral.CampoReportTxt(CBCarteira.Text, 'Todas')
  else if VarName = 'CHEQUE' then
    Value := MLAGeral.CampoReportTxt(EdCheque.Text, 'Todas')
end;

end.

