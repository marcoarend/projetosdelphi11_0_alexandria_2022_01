unit EmitSacExport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, dmkGeral, dmkEdit,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmEmitSacExport = class(TForm)
    Panel1: TPanel;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFLastAtz: TDateField;
    Memo1: TMemo;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    QrEmitBACCPF: TWideStringField;
    EdPath: TdmkEdit;
    BtAbrir: TBitBtn;
    EdBackFile: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEmitSacExport: TFmEmitSacExport;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmEmitSacExport.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitSacExport.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmitSacExport.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  EdPath.Text := Geral.ReadAppKey('BackUpPath', 'Dermatek', ktString, '',
    HKEY_LOCAL_MACHINE);
end;

procedure TFmEmitSacExport.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmitSacExport.BtOKClick(Sender: TObject);
var
  DIB, Txt, Dta, Path: String;
begin
  Path := Trim(EdPath.Text);
  if Trim(EdPath.Text) = '' then
  begin
    Application.MessageBox('Caminho n�o informado!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if not DirectoryExists(Path) then ForceDirectories(Path);
  if not DirectoryExists(Path) then
  begin
    Application.MessageBox('Caminho n�o pode ser criado!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Path := dmkPF.CaminhoArquivo(Path, 'EmiSac_' +
    dmkPF.ObtemDataStr(Date, False)+'.', '.txt');

  //

  Screen.Cursor := crHourGlass;
  Memo1.Lines.Clear;
  DIB := Geral.CompletaString(DModG.QrDonoCNPJ_CPF.Value,     ' ', 15, taLeftJustify, True);
  Txt := Geral.CompletaString(DModG.QrDonoNOMEDONO.Value,    ' ', 100, taLeftJustify, True);
  Memo1.Lines.Add('000' + DIB + Txt);

  //

  QrEmitCPF.Close;
  UMyMod.AbreQuery(QrEmitCPF, Dmod.MyDB);
  while not QrEmitCPF.Eof do
  begin
    DIB := Geral.CompletaString(QrEmitCPFCPF.Value,     ' ', 15, taLeftJustify, True);
    Txt := Geral.CompletaString(QrEmitCPFNome.Value,    ' ', 50, taLeftJustify, True);
    Dta := FormatDateTime(VAR_FORMATDATE, QrEmitCPFLastAtz.Value);
    Memo1.Lines.Add('001' + DIB + Txt + Dta);
    QrEmitCPF.Next;
  end;

  //

  QrEmitBAC.Close;
  UMyMod.AbreQuery(QrEmitBAC, Dmod.MyDB);
  while not QrEmitBAC.Eof do
  begin
    DIB := Geral.CompletaString(QrEmitBACCPF.Value, ' ', 15, taLeftJustify, True);
    Txt := Geral.CompletaString(QrEmitBACBAC.Value, ' ', 20, taLeftJustify, True);
    Memo1.Lines.Add('002' + DIB + Txt);
    QrEmitBAC.Next;
  end;

  //


  if MLAGeral.ExportaMemoToFile(Memo1, Path, True, False, False) then
    Application.MessageBox(PChar('Arquivo salvo em "' + Path +
    '"!'), 'Informa��o', MB_OK+MB_ICONINFORMATION)
  else
    Application.MessageBox('Arquivo n�o pode ser salvo!', 'Erro', MB_OK+MB_ICONERROR);


  Screen.Cursor := crDefault;

end;

procedure TFmEmitSacExport.BtAbrirClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', Label2.Caption, '', [], Arquivo) then
  begin
    EdPath.Text := ExtractFileDir(Arquivo) + '\';
    Geral.WriteAppKey('BackUpPath', 'Dermatek', EdPath.Text, ktString,
    HKEY_LOCAL_MACHINE);
  end;
end;

end.
