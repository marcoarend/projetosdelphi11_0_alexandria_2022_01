unit PesqCNPJ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkGeral, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmPesqCNPJ = class(TForm)
    Panel1: TPanel;
    RGMascara: TRadioGroup;
    Label1: TLabel;
    EdEmitente: TdmkEdit;
    DBGrid1: TDBGrid;
    QrSacados: TmySQLQuery;
    DsSacados: TDataSource;
    QrSacadosCNPJ: TWideStringField;
    QrSacadosIE: TWideStringField;
    QrSacadosNome: TWideStringField;
    QrSacadosRua: TWideStringField;
    QrSacadosCompl: TWideStringField;
    QrSacadosBairro: TWideStringField;
    QrSacadosCidade: TWideStringField;
    QrSacadosUF: TWideStringField;
    QrSacadosCEP: TIntegerField;
    QrSacadosTel1: TWideStringField;
    QrSacadosRisco: TFloatField;
    QrSacadosNumero: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtPesquisa: TBitBtn;
    BtConfirma: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPesqCNPJ: TFmPesqCNPJ;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule;

{$R *.DFM}

procedure TFmPesqCNPJ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCNPJ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPesqCNPJ.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
end;

procedure TFmPesqCNPJ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPesqCNPJ.BtPesquisaClick(Sender: TObject);
var
  Emitente: String;
begin
  //if Trim(EdEmitente.Text) <> '' then
  //begin
    Emitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then Emitente := '%'+Emitente;
    if RGMascara.ItemIndex in ([0,2]) then Emitente := Emitente+'%';
 // end else Emitente := '';
  //////////////////////////////////////////////////////////////////////////////
  QrSacados.Close;
  QrSacados.Params[0].AsString := Emitente;
  UMyMod.AbreQuery(QrSacados, Dmod.MyDB);
  //
  BtPesquisa.Enabled := False;
  BtConfirma.Enabled := True;
  //configurar grade
end;

procedure TFmPesqCNPJ.RGMascaraClick(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCNPJ.EdEmitenteChange(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCNPJ.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmPesqCNPJ.BtConfirmaClick(Sender: TObject);
begin
  VAR_CPF_PESQ := Geral.FormataCNPJ_TT(QrSacadosCNPJ.Value);
  Close;
end;

end.
