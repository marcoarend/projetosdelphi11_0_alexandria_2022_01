unit CartDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Variants, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCartDep = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    DsCarteiras: TDataSource;
    EdCarteira: TdmkEditCB;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmCartDep: TFmCartDep;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmCartDep.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stNil;
  //
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
end;

procedure TFmCartDep.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartDep.BtOKClick(Sender: TObject);
begin
  if EdCarteira.ValueVariant <> 0 then
  begin
    VAR_CARTDEP := EdCarteira.ValueVariant;
    Close;
  end else Geral.MensagemBox('Defina a carteira de dep�sito', 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

procedure TFmCartDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
