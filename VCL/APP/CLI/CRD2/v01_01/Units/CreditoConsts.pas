unit CreditoConsts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  (*DBTables,*) StdCtrls, comctrls, mySQLDbTables, dmkGeral;

type
  TCreditoConsts = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetaVariaveisIniciais;
    procedure AvisaSetarOpcoes;
  end;

const
  CRD_CAMINHO_REGEDIT = 'Dermatek\Cred' + 'itor';
  CRD_MAX_EMPRESAS    = 6;
var
  CRD_Consts: TCreditoConsts;

implementation

uses UnMyObjects, UnMLAGeral, Principal;

procedure TCreditoConsts.AvisaSetarOpcoes;
var
  Caminho: String;
begin
  Caminho := CRD_CAMINHO_REGEDIT+'\Opcoes';
  //
  if not Geral.ReadAppKeyLM('JaSetado', Caminho, ktBoolean, False) then
  begin
    if Geral.MB_Pergunta('O aplicativo detectou que as op��es do ' +
      'aplicativo n�o foram configuradas ainda.' + sLineBreak +
      'Deseja configur�-las agora?') = ID_YES
    then
      FmPrincipal.MostraOpcoesCreditoX();
  end;
end;

procedure TCreditoConsts.SetaVariaveisIniciais;
begin
  //
end;

end.
 
