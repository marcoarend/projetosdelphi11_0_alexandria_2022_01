object FmOperaCol: TFmOperaCol
  Left = 393
  Top = 177
  Caption = 'OPE-GEREN-002 :: Impress'#227'o de Operac'#245'es Com Coligadas'
  ClientHeight = 548
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 133
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 4
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object Label34: TLabel
      Left = 580
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label1: TLabel
      Left = 684
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object EdCliente: TdmkEditCB
      Left = 8
      Top = 20
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdClienteChange
      DBLookupComboBox = CbCliente
      IgnoraDBLookupComboBox = False
    end
    object CbCliente: TdmkDBLookupComboBox
      Left = 68
      Top = 20
      Width = 509
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECLIENTE'
      ListSource = DsClientes
      TabOrder = 1
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPIni: TDateTimePicker
      Left = 580
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 2
      OnChange = TPIniChange
    end
    object TPFim: TDateTimePicker
      Left = 684
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 3
      OnChange = TPFimChange
    end
    object RGOrdem1: TRadioGroup
      Left = 8
      Top = 48
      Width = 89
      Height = 80
      Caption = ' Ordem A: '
      ItemIndex = 2
      Items.Strings = (
        'Cliente'
        'Data'
        'M'#234's')
      TabOrder = 4
      OnClick = RGOrdem1Click
    end
    object RGOrdem2: TRadioGroup
      Left = 100
      Top = 48
      Width = 89
      Height = 80
      Caption = ' Ordem B: '
      ItemIndex = 1
      Items.Strings = (
        'Cliente'
        'Data'
        'M'#234's')
      TabOrder = 5
      OnClick = RGOrdem1Click
    end
    object RGOrdem3: TRadioGroup
      Left = 192
      Top = 48
      Width = 89
      Height = 80
      Caption = ' Ordem C: '
      ItemIndex = 0
      Items.Strings = (
        'Cliente'
        'Data'
        'M'#234's')
      TabOrder = 6
      OnClick = RGOrdem1Click
    end
    object RGGrupos: TRadioGroup
      Left = 284
      Top = 48
      Width = 105
      Height = 80
      Caption = ' Estilo: '
      ItemIndex = 0
      Items.Strings = (
        'Listagem'
        'Agrupamentos'
        'Conjuntos (2)')
      TabOrder = 7
      OnClick = RGOrdem1Click
    end
    object RGSintetico: TRadioGroup
      Left = 392
      Top = 48
      Width = 89
      Height = 80
      Caption = ' Tipo: '
      ItemIndex = 0
      Items.Strings = (
        'Anal'#237'tico'
        'Sint'#233'tico')
      TabOrder = 8
    end
    object GroupBox1: TGroupBox
      Left = 485
      Top = 48
      Width = 300
      Height = 80
      Caption = ' Ordem definida: '
      TabOrder = 9
      object LaN1: TLabel
        Left = 5
        Top = 16
        Width = 16
        Height = 13
        Caption = '1. '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clActiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object La1: TLabel
        Left = 23
        Top = 16
        Width = 93
        Height = 13
        Caption = 'Valor negociado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clActiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaN2: TLabel
        Left = 5
        Top = 32
        Width = 12
        Height = 13
        Caption = '2.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object La2: TLabel
        Left = 23
        Top = 32
        Width = 24
        Height = 13
        Caption = 'M'#234's'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object La3: TLabel
        Left = 23
        Top = 48
        Width = 28
        Height = 13
        Caption = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaN3: TLabel
        Left = 5
        Top = 48
        Width = 12
        Height = 13
        Caption = '3.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object La4: TLabel
        Left = 23
        Top = 64
        Width = 40
        Height = 13
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LaN4: TLabel
        Left = 5
        Top = 64
        Width = 12
        Height = 13
        Caption = '4.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 181
    Width = 792
    Height = 242
    Align = alClient
    DataSource = DsLReA
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'NomeMes'
        Title.Caption = 'M'#234's'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NomeCli'
        Title.Caption = 'Cliente'
        Width = 111
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Lote'
        Title.Caption = 'Border'#244
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NF'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QtdeDocs'
        Title.Caption = 'QDcs'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MediaDoc'
        Title.Caption = 'M'#233'dia/Doc'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Total'
        Title.Caption = 'V.Negoc.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TTC'
        Title.Caption = 'F.Compra'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TVV'
        Title.Caption = 'Ad Valorem'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TTC_J'
        Title.Caption = 'Tx.per'#237'odo'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Dias'
        Title.Caption = 'Prz.m'#233'dio'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TTC_T'
        Title.Caption = 'Tx.m'#234's'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Impostos'
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 500
        Height = 32
        Caption = 'Impress'#227'o de Operac'#245'es Com Coligadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 500
        Height = 32
        Caption = 'Impress'#227'o de Operac'#245'es Com Coligadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 500
        Height = 32
        Caption = 'Impress'#227'o de Operac'#245'es Com Coligadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 423
    Width = 792
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 21
        Width = 788
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 478
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCalcula: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Calcula'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCalculaClick
      end
      object BtResultado: TBitBtn
        Tag = 5
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Vizualiza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtResultadoClick
      end
      object BitBtn1: TBitBtn
        Tag = 263
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Con&figura'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 636
    Top = 248
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 664
    Top = 248
  end
  object QrEmLot: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT el.* '
      'FROM emlot el'
      'WHERE el.Codigo=:P0')
    Left = 576
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLotCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmLotAdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrEmLotValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrEmLotTaxaVal: TFloatField
      FieldName = 'TaxaVal'
      Required = True
    end
    object QrEmLotIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrEmLotIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrEmLotISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrEmLotISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrEmLotPIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrEmLotPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrEmLotPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrEmLotPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrEmLotCOFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrEmLotCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrEmLotCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrEmLotCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
  end
  object QrOper1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE'
      'FROM lot es lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'ORDER BY lo.Data, NOMECLIENTE, lo.Lote')
    Left = 524
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOper1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOper1Tipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrOper1Spread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrOper1Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOper1Lote: TIntegerField
      FieldName = 'Lote'
      Required = True
    end
    object QrOper1Data: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrOper1Total: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrOper1Dias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrOper1PeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrOper1TxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
    end
    object QrOper1ValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrOper1AdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrOper1IOC: TFloatField
      FieldName = 'IOC'
      Required = True
    end
    object QrOper1IOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrOper1Tarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrOper1CPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
    end
    object QrOper1CPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
    end
    object QrOper1TipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrOper1IRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrOper1IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrOper1ISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrOper1ISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrOper1PIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrOper1PIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrOper1PIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrOper1PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrOper1COFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrOper1COFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrOper1COFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrOper1COFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
    object QrOper1OcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrOper1MaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrOper1CHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
    end
    object QrOper1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOper1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOper1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOper1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOper1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOper1MINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrOper1MINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrOper1MINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrOper1PIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
    end
    object QrOper1COFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
    end
    object QrOper1PgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
    end
    object QrOper1DUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
    end
    object QrOper1NF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrOper1Itens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrOper1Conferido: TIntegerField
      FieldName = 'Conferido'
      Required = True
    end
    object QrOper1ECartaSac: TSmallintField
      FieldName = 'ECartaSac'
      Required = True
    end
    object QrOper1CBE: TIntegerField
      FieldName = 'CBE'
      Required = True
    end
    object QrOper1NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object QrEmLo1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT el.Codigo, el.ValValorem, el.TaxaVal '
      'FROM emlot el'
      'LEFT JOIN "TMeuDBx".lot es lo'
      'ON lo.Codigo=el.Codigo'
      'WHERE lo.data BETWEEN :P0 AND :P1')
    Left = 548
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmLo1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmLo1ValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrEmLo1TaxaVal: TFloatField
      FieldName = 'TaxaVal'
      Required = True
    end
  end
  object QrLReA: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrLReACalcFields
    SQL.Strings = (
      'SELECT * FROM lrea'
      'ORDER BY Data, NomeCli, Lote')
    Left = 176
    Top = 364
    object QrLReACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLReAData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLReANomeCli: TWideStringField
      FieldName = 'NomeCli'
      Size = 100
    end
    object QrLReALote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000;-000000; '
    end
    object QrLReANF: TIntegerField
      FieldName = 'NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrLReATotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATC0: TFloatField
      FieldName = 'TC0'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAVV0: TFloatField
      FieldName = 'VV0'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAETC: TFloatField
      FieldName = 'ETC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAEVV: TFloatField
      FieldName = 'EVV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATTC: TFloatField
      FieldName = 'TTC'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATVV: TFloatField
      FieldName = 'TVV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReADias: TFloatField
      FieldName = 'Dias'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATTC_T: TFloatField
      FieldName = 'TTC_T'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrLReATTC_J: TFloatField
      FieldName = 'TTC_J'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAALL_T: TFloatField
      FieldName = 'ALL_T'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAALL_J: TFloatField
      FieldName = 'ALL_J'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAddVal: TFloatField
      FieldName = 'ddVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReANomeMes: TWideStringField
      FieldName = 'NomeMes'
      Size = 30
    end
    object QrLReAMes: TIntegerField
      FieldName = 'Mes'
    end
    object QrLReAQtdeDocs: TIntegerField
      FieldName = 'QtdeDocs'
    end
    object QrLReAMediaDoc: TFloatField
      FieldName = 'MediaDoc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrLReATC1: TFloatField
      FieldName = 'TC1'
    end
    object QrLReATC2: TFloatField
      FieldName = 'TC2'
    end
    object QrLReATC3: TFloatField
      FieldName = 'TC3'
    end
    object QrLReATC4: TFloatField
      FieldName = 'TC4'
    end
    object QrLReATC5: TFloatField
      FieldName = 'TC5'
    end
    object QrLReATC6: TFloatField
      FieldName = 'TC6'
    end
    object QrLReAVV1: TFloatField
      FieldName = 'VV1'
    end
    object QrLReAVV2: TFloatField
      FieldName = 'VV2'
    end
    object QrLReAVV3: TFloatField
      FieldName = 'VV3'
    end
    object QrLReAVV4: TFloatField
      FieldName = 'VV4'
    end
    object QrLReAVV5: TFloatField
      FieldName = 'VV5'
    end
    object QrLReAVV6: TFloatField
      FieldName = 'VV6'
    end
    object QrLReAISS_Val: TFloatField
      FieldName = 'ISS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReACOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReAImpostos: TFloatField
      FieldName = 'Impostos'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLReATRC_T: TFloatField
      FieldName = 'TRC_T'
    end
    object QrLReATRC_J: TFloatField
      FieldName = 'TRC_J'
    end
    object QrLReAddLiq: TFloatField
      FieldName = 'ddLiq'
    end
  end
  object DsLReA: TDataSource
    DataSet = QrLReA
    Left = 204
    Top = 364
  end
  object frxOper2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.568873333300000000
    ReportOptions.LastChange = 39718.568873333300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub1Total , Sub1TTC   , Sub1TRC   ,'
      '  Sub2Dias  , Sub2Total , Sub2TTC   , Sub2TRC   ,'
      '  Dias      , Total     , TTC       , TRC       ,'
      '  TotalDias , TotalTTC  , TotalTRC  , TotalTotal: Extended;'
      '    '
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TotalDias  := 0;'
      '  TotalTotal := 0;'
      '  TotalTTC   := 0;'
      '  TotalTRC   := 0;  '
      'end;'
      ''
      'procedure DadosMestre1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Sub2Dias  := Sub2Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub2Total := Sub2Total + <frxDsLReA."Total">;'
      '  Sub2TTC   := Sub2TTC   + <frxDsLReA."TTC">;'
      
        '  Sub2TRC   := Sub2TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  Sub1Dias  := Sub1Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub1Total := Sub1Total + <frxDsLReA."Total">;'
      '  Sub1TTC   := Sub1TTC   + <frxDsLReA."TTC">;'
      
        '  Sub1TRC   := Sub1TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  TotalDias  := TotalDias  + <frxDsLReA."Dias"> * <frxDsLReA."To' +
        'tal">;'
      '  TotalTotal := TotalTotal + <frxDsLReA."Total">;'
      '  TotalTTC   := TotalTTC   + <frxDsLReA."TTC">;'
      
        '  TotalTRC   := TotalTRC   + <frxDsLReA."TTC"> - <frxDsLReA."Imp' +
        'ostos">;    '
      'end;'
      ''
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  Sub1TRC   := 0;'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      '  Sub2TRC   := 0;'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub1.Text  := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TTC);'
      
        '  MemoSub1L.Text := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TRC);'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub2.Text  := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TTC);'
      
        '  MemoSub2L.Text := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TRC);  '
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSubT.Text  := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTTC);'
      
        '  MemoSubTL.Text := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTRC);'
      'end;'
      ''
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLReA."NomeCli"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      ''
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLReA."NomeCli"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      'end.')
    OnGetValue = frxOper2GetValue
    OnUserFunction = frxOper2UserFunction
    Left = 148
    Top = 336
    Datasets = <
      item
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 2.000000000000000000
          Top = 17.102350000000000000
          Width = 720.000000000000000000
          Height = 26.000000000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 570.709030000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 582.220470000000000000
          Top = 6.172850000000040000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 670.220470000000000000
          Top = 6.172850000000040000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 111.000000000000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          Left = 6.000000000000000000
          Top = 1.732220000000000000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES REALIZADAS COM AS COLIGADAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 56.692913385826800000
          Top = 77.732220000000000000
          Width = 132.283464566929000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 222.992125984252000000
          Top = 77.732220000000000000
          Width = 56.692913385826800000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 362.834645669291000000
          Top = 77.732220000000000000
          Width = 60.472440944881890000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 77.732220000000000000
          Width = 56.692913385826800000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data oper.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 54.000000000000000000
          Top = 29.732220000000000000
          Width = 412.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 6.000000000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 518.000000000000000000
          Top = 29.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 470.000000000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 188.976377952756000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 423.307086614173000000
          Top = 77.732220000000000000
          Width = 52.913385826771700000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 476.220472440945000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 563.149606299213000000
          Top = 77.732220000000000000
          Width = 37.795275590551200000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prazo'
            'M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 600.944881889764000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 525.354330708661000000
          Top = 77.732220000000000000
          Width = 37.795275590551200000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Taxa'
            'Per'#237'odo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 313.700787401575000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia'
            'Doc R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 279.685039370079000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 222.992125984252000000
          Top = 91.732220000000000000
          Width = 56.692913385826800000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'R$/Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 188.976377952756000000
          Top = 91.732220000000000000
          Width = 34.015748031496100000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 634.960629921260000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Impostos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 684.094488188976000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'Real')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 56.692913385826800000
          Width = 132.283464566929000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLReA."NomeCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 222.992125980000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsLReA."NF">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TVV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Lote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Dias"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 600.944881890000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_J"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."MediaDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."QtdeDocs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Impostos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 684.094488190000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TRC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897637795275600000
        Top = 529.134200000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Memo36: TfrxMemoView
          Width = 188.976377952756000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 563.149606300000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubT: TfrxMemoView
          Left = 600.944881889764000000
          Width = 34.015748031496100000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 222.992125980000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubTL: TfrxMemoView
          Left = 684.094488188976000000
          Width = 34.015748031496100000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLReA."NomeCli"'
        object Memo24: TfrxMemoView
          Width = 718.110675590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLReA."NomeMes"'
        object Memo30: TfrxMemoView
          Left = 34.015770000000000000
          Width = 684.094905590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo25: TfrxMemoView
          Left = 34.015770000000000000
          Top = 3.779530000000020000
          Width = 154.960629921260000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 362.834645670000000000
          Top = 3.779530000000020000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 423.307086614173000000
          Top = 3.779530000000020000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 476.220472440945000000
          Top = 3.779530000000020000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 563.149606299213000000
          Top = 3.779530000000020000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2: TfrxMemoView
          Left = 600.944881889764000000
          Top = 3.779530000000020000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 525.354330708661000000
          Top = 3.779529999999970000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 313.700787400000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 279.685039370000000000
          Top = 3.779530000000020000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 222.992125984252000000
          Top = 3.779530000000020000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 188.976377950000000000
          Top = 3.779530000000020000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 634.960629920000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2L: TfrxMemoView
          Left = 684.094488188976000000
          Top = 3.779530000000020000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo27: TfrxMemoView
          Top = 3.558749999999980000
          Width = 188.976377952756000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 362.834645670000000000
          Top = 3.779530000000020000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 423.307086614173000000
          Top = 3.779530000000020000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 476.220472440945000000
          Top = 3.779530000000020000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 563.149606299213000000
          Top = 3.779530000000020000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1: TfrxMemoView
          Left = 600.944881889764000000
          Top = 3.779530000000020000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 525.354330708661000000
          Top = 3.779529999999970000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 313.700787400000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 279.685039370000000000
          Top = 3.779530000000020000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 222.992125984252000000
          Top = 3.779530000000020000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 188.976377950000000000
          Top = 3.779530000000020000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 634.960629920000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1L: TfrxMemoView
          Left = 684.094488188976000000
          Top = 3.779530000000020000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsLReA: TfrxDBDataset
    UserName = 'frxDsLReA'
    CloseDataSource = False
    DataSet = QrLReA
    BCDToCurrency = False
    Left = 148
    Top = 364
  end
  object frxOper3: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.568873333300000000
    ReportOptions.LastChange = 39718.568873333300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub1Total , Sub1TTC   , Sub1TRC   ,'
      '  Sub2Dias  , Sub2Total , Sub2TTC   , Sub2TRC   ,'
      '  Dias      , Total     , TTC       , TRC       ,'
      '  TotalDias , TotalTTC  , TotalTRC  , TotalTotal: Extended;'
      '    '
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TotalDias  := 0;'
      '  TotalTotal := 0;'
      '  TotalTTC   := 0;'
      '  TotalTRC   := 0;  '
      'end;'
      ''
      'procedure DadosMestre1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Sub2Dias  := Sub2Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub2Total := Sub2Total + <frxDsLReA."Total">;'
      '  Sub2TTC   := Sub2TTC   + <frxDsLReA."TTC">;'
      
        '  Sub2TRC   := Sub2TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  Sub1Dias  := Sub1Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub1Total := Sub1Total + <frxDsLReA."Total">;'
      '  Sub1TTC   := Sub1TTC   + <frxDsLReA."TTC">;'
      
        '  Sub1TRC   := Sub1TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  TotalDias  := TotalDias  + <frxDsLReA."Dias"> * <frxDsLReA."To' +
        'tal">;'
      '  TotalTotal := TotalTotal + <frxDsLReA."Total">;'
      '  TotalTTC   := TotalTTC   + <frxDsLReA."TTC">;'
      
        '  TotalTRC   := TotalTRC   + <frxDsLReA."TTC"> - <frxDsLReA."Imp' +
        'ostos">;    '
      'end;'
      ''
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  Sub1TRC   := 0;'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      '  Sub2TRC   := 0;'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub1.Text  := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TTC);'
      
        '  MemoSub1L.Text := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TRC);'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub2.Text  := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TTC);'
      
        '  MemoSub2L.Text := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TRC);  '
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSubT.Text  := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTTC);'
      
        '  MemoSubTL.Text := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTRC);'
      'end;'
      ''
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLReA."NomeCli"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      ''
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLReA."NomeCli"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      'end.')
    OnGetValue = frxOper2GetValue
    OnUserFunction = frxOper2UserFunction
    Left = 176
    Top = 336
    Datasets = <
      item
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 2.000000000000000000
          Top = 17.102350000000000000
          Width = 720.000000000000000000
          Height = 26.000000000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 563.149970000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 582.220470000000000000
          Top = 6.172850000000040000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 670.220470000000000000
          Top = 6.172850000000040000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 111.000000000000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          Left = 6.000000000000000000
          Top = 1.732220000000000000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES REALIZADAS COM AS COLIGADAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 56.692913385826800000
          Top = 77.732220000000000000
          Width = 132.283464566929000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 222.992125984252000000
          Top = 77.732220000000000000
          Width = 56.692913385826800000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 362.834645669291000000
          Top = 77.732220000000000000
          Width = 60.472440944881890000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 77.732220000000000000
          Width = 56.692913385826800000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data oper.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 54.000000000000000000
          Top = 29.732220000000000000
          Width = 412.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 6.000000000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 518.000000000000000000
          Top = 29.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 470.000000000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 188.976377952756000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 423.307086614173000000
          Top = 77.732220000000000000
          Width = 52.913385826771700000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 476.220472440945000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 563.149606299213000000
          Top = 77.732220000000000000
          Width = 37.795275590551200000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prazo'
            'M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 600.944881889764000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 525.354330708661000000
          Top = 77.732220000000000000
          Width = 37.795275590551200000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Taxa'
            'Per'#237'odo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 313.700787401575000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia'
            'Doc R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 279.685039370079000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 222.992125984252000000
          Top = 91.732220000000000000
          Width = 56.692913385826800000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'R$/Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 188.976377952756000000
          Top = 91.732220000000000000
          Width = 34.015748031496100000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 634.960629921260000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Impostos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 684.094488188976000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'Real')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 56.692913385826800000
          Width = 132.283464566929000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLReA."NomeCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 222.992125980000000000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsLReA."NF">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TVV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Lote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Dias"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 600.944881890000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_J"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."MediaDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."QtdeDocs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Impostos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 684.094488190000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TRC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897637795275600000
        Top = 521.575140000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Memo36: TfrxMemoView
          Width = 188.976377952756000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubT: TfrxMemoView
          Left = 600.944881889764000000
          Width = 34.015748031496100000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 222.992125980000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubTL: TfrxMemoView
          Left = 684.094488188976000000
          Width = 34.015748031496100000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLReA."NomeCli"'
        object Memo24: TfrxMemoView
          Width = 718.110675590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLReA."NomeMes"'
      end
      object GF2: TfrxGroupFooter
        Height = 18.440940000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo25: TfrxMemoView
          Width = 188.976399920000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2: TfrxMemoView
          Left = 600.944881889764000000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 222.992125984252000000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2L: TfrxMemoView
          Left = 684.094488188976000000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 434.645950000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo27: TfrxMemoView
          Top = 3.558749999999980000
          Width = 188.976377952756000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 362.834645670000000000
          Top = 3.779530000000020000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 423.307086614173000000
          Top = 3.779530000000020000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 476.220472440945000000
          Top = 3.779530000000020000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 563.149606299213000000
          Top = 3.779530000000020000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1: TfrxMemoView
          Left = 600.944881889764000000
          Top = 3.779530000000020000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 525.354330708661000000
          Top = 3.779529999999970000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 313.700787400000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 279.685039370000000000
          Top = 3.779530000000020000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 222.992125984252000000
          Top = 3.779530000000020000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 188.976377950000000000
          Top = 3.779530000000020000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 634.960629920000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1L: TfrxMemoView
          Left = 684.094488188976000000
          Top = 3.779530000000020000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxOper4: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.568873333300000000
    ReportOptions.LastChange = 39718.568873333300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub1Total , Sub1TTC   , Sub1TRC   ,'
      '  Sub2Dias  , Sub2Total , Sub2TTC   , Sub2TRC   ,'
      '  Dias      , Total     , TTC       , TRC       ,'
      '  TotalDias , TotalTTC  , TotalTRC  , TotalTotal: Extended;'
      '    '
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  TotalDias  := 0;'
      '  TotalTotal := 0;'
      '  TotalTTC   := 0;'
      '  TotalTRC   := 0;  '
      'end;'
      ''
      'procedure DadosMestre1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Sub2Dias  := Sub2Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub2Total := Sub2Total + <frxDsLReA."Total">;'
      '  Sub2TTC   := Sub2TTC   + <frxDsLReA."TTC">;'
      
        '  Sub2TRC   := Sub2TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  Sub1Dias  := Sub1Dias  + <frxDsLReA."Dias"> * <frxDsLReA."Tota' +
        'l">;'
      '  Sub1Total := Sub1Total + <frxDsLReA."Total">;'
      '  Sub1TTC   := Sub1TTC   + <frxDsLReA."TTC">;'
      
        '  Sub1TRC   := Sub1TRC   + <frxDsLReA."TTC"> - <frxDsLReA."Impos' +
        'tos">;'
      '  //'
      
        '  TotalDias  := TotalDias  + <frxDsLReA."Dias"> * <frxDsLReA."To' +
        'tal">;'
      '  TotalTotal := TotalTotal + <frxDsLReA."Total">;'
      '  TotalTTC   := TotalTTC   + <frxDsLReA."TTC">;'
      
        '  TotalTRC   := TotalTRC   + <frxDsLReA."TTC"> - <frxDsLReA."Imp' +
        'ostos">;    '
      'end;'
      ''
      'procedure GH1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  Sub1TRC   := 0;'
      'end;'
      ''
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      '  Sub2TRC   := 0;'
      'end;'
      ''
      'procedure GF1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub1.Text  := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TTC);'
      
        '  MemoSub1L.Text := CalculaJuroMes_frxReport(Sub1Dias, Sub1Total' +
        ', Sub1TRC);'
      'end;'
      ''
      'procedure GF2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSub2.Text  := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TTC);'
      
        '  MemoSub2L.Text := CalculaJuroMes_frxReport(Sub2Dias, Sub2Total' +
        ', Sub2TRC);  '
      'end;'
      ''
      'procedure ReportSummary1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MemoSubT.Text  := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTTC);'
      
        '  MemoSubTL.Text := CalculaJuroMes_frxReport(TotalDias, TotalTot' +
        'al, TotalTRC);'
      'end;'
      ''
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if GH1.Visible then'
      '    if <VFR_ORD2> > 0 then'
      '      GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsLReA."NomeCli"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      ''
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      ''
      '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsLReA."NomeCli"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsLReA."Data"'#39';'
      '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsLReA."NomeMes"'#39';'
      '  //'
      'end.')
    OnGetValue = frxOper2GetValue
    OnUserFunction = frxOper2UserFunction
    Left = 204
    Top = 336
    Datasets = <
      item
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 2.000000000000000000
          Top = 17.102350000000000000
          Width = 720.000000000000000000
          Height = 26.000000000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Dmod.QrMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 517.795610000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 582.220470000000000000
          Top = 6.172850000000040000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 670.220470000000000000
          Top = 6.172850000000040000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 111.000000000000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          Left = 6.000000000000000000
          Top = 1.732220000000000000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES REALIZADAS COM AS COLIGADAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 222.992125984252000000
          Top = 77.732220000000000000
          Width = 56.692913385826800000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 362.834645669291000000
          Top = 77.732220000000000000
          Width = 60.472440944881890000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 77.732220000000000000
          Width = 188.976463390000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_TITULO1]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 54.000000000000000000
          Top = 29.732220000000000000
          Width = 412.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 6.000000000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 518.000000000000000000
          Top = 29.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 470.000000000000000000
          Top = 29.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 188.976377952756000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 423.307086614173000000
          Top = 77.732220000000000000
          Width = 52.913385826771700000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 476.220472440945000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 563.149606299213000000
          Top = 77.732220000000000000
          Width = 37.795275590551200000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Prazo'
            'M'#233'dio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 600.944881889764000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'M'#234's')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 525.354330708661000000
          Top = 77.732220000000000000
          Width = 37.795275590551200000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '% Taxa'
            'Per'#237'odo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 313.700787401575000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia'
            'Doc R$')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 279.685039370079000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 222.992125984252000000
          Top = 91.732220000000000000
          Width = 56.692913385826800000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'R$/Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 188.976377952756000000
          Top = 91.732220000000000000
          Width = 34.015748031496100000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 634.960629921260000000
          Top = 77.732220000000000000
          Width = 49.133858267716500000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Impostos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 684.094488188976000000
          Top = 77.732220000000000000
          Width = 34.015748031496100000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Taxa'
            'Real')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 0.377952755905512000
        Top = 332.598640000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsLReA
        DataSetName = 'frxDsLReA'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 56.692913385826800000
          Width = 132.283464566929000000
          Height = 0.377952755905512000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsLReA."NomeCli"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 222.992125980000000000
          Width = 56.692913390000000000
          Height = 0.377952755905512000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsLReA."NF">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Width = 56.692913385826800000
          Height = 0.377952755905512000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLReA."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TVV"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Lote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Dias"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 600.944881890000000000
          Width = 34.015748030000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TTC_J"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."MediaDoc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."QtdeDocs"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."Impostos"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 684.094488190000000000
          Width = 34.015748030000000000
          Height = 0.377952755905512000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLReA."TRC_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 18.897637795275600000
        Top = 476.220780000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'ReportSummary1OnBeforePrint'
        object Memo36: TfrxMemoView
          Width = 188.976377950000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubT: TfrxMemoView
          Left = 600.944881889764000000
          Width = 34.015748031496100000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 222.992125980000000000
          Width = 56.692913390000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSubTL: TfrxMemoView
          Left = 684.094488188976000000
          Width = 34.015748031496100000
          Height = 18.897637795275600000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsLReA."NomeCli"'
        object Memo24: TfrxMemoView
          Width = 718.110675590000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 0.377952755905512000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsLReA."NomeMes"'
      end
      object GF2: TfrxGroupFooter
        Height = 18.440940000000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo25: TfrxMemoView
          Width = 188.976399920000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2: TfrxMemoView
          Left = 600.944881889764000000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 222.992125984252000000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub2L: TfrxMemoView
          Left = 684.094488188976000000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 17.000000000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo27: TfrxMemoView
          Width = 188.976377952756000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 362.834645670000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 423.307086614173000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TTC">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 476.220472440945000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."TVV">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 563.149606299213000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."ddVal">) / SUM(<frxDsLReA."Total">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1: TfrxMemoView
          Left = 600.944881889764000000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 525.354330708661000000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."TTC">) / SUM(<frxDsLReA."Total">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 313.700787400000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."QtdeDocs">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 279.685039370000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."QtdeDocs">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 222.992125984252000000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(SUM(<frxDsLReA."Total">) / SUM(<frxDsLReA."ITEM">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 188.976377950000000000
          Width = 34.015748030000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."ITEM">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 634.960629920000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLReA."Impostos">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MemoSub1L: TfrxMemoView
          Left = 684.094488188976000000
          Width = 34.015748031496100000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
