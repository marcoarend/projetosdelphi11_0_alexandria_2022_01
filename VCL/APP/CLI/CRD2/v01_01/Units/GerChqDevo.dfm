object FmGerChqDevo: TFmGerChqDevo
  Left = 339
  Top = 185
  Caption = 'CHQ-CNTRL-003 :: Devolu'#231#227'o de Cheque'
  ClientHeight = 326
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 269
        Height = 32
        Caption = 'Devolu'#231#227'o de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 269
        Height = 32
        Caption = 'Devolu'#231#227'o de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 269
        Height = 32
        Caption = 'Devolu'#231#227'o de Cheque'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 624
    Height = 164
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 164
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 624
        Height = 164
        Align = alClient
        TabOrder = 0
        object Label17: TLabel
          Left = 12
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Alinea 1:'
        end
        object Label18: TLabel
          Left = 12
          Top = 56
          Width = 41
          Height = 13
          Caption = 'Alinea 2:'
        end
        object Label19: TLabel
          Left = 500
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label23: TLabel
          Left = 500
          Top = 56
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label24: TLabel
          Left = 12
          Top = 100
          Width = 38
          Height = 13
          Caption = '$ Multa:'
        end
        object Label25: TLabel
          Left = 204
          Top = 100
          Width = 78
          Height = 13
          Caption = '% Taxa de juros:'
        end
        object Label26: TLabel
          Left = 108
          Top = 100
          Width = 41
          Height = 13
          Caption = '$ Taxas:'
        end
        object SpeedButton1: TSpeedButton
          Left = 476
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object SpeedButton2: TSpeedButton
          Left = 476
          Top = 72
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdAlinea1: TdmkEditCB
          Left = 12
          Top = 32
          Width = 45
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBAlinea1
          IgnoraDBLookupComboBox = False
        end
        object EdAlinea2: TdmkEditCB
          Left = 12
          Top = 72
          Width = 45
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBAlinea2
          IgnoraDBLookupComboBox = False
        end
        object CBAlinea2: TdmkDBLookupComboBox
          Left = 60
          Top = 72
          Width = 413
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAlinea2
          TabOrder = 4
          dmkEditCB = EdAlinea2
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBAlinea1: TdmkDBLookupComboBox
          Left = 60
          Top = 32
          Width = 413
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAlinea1
          TabOrder = 1
          dmkEditCB = EdAlinea1
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object TPData1_1: TdmkEditDateTimePicker
          Left = 500
          Top = 32
          Width = 112
          Height = 21
          Date = 38698.785142685200000000
          Time = 38698.785142685200000000
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPData1_2: TdmkEditDateTimePicker
          Left = 500
          Top = 72
          Width = 112
          Height = 21
          Date = 38698.785142685200000000
          Time = 38698.785142685200000000
          TabOrder = 5
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdMulta1: TdmkEdit
          Left = 12
          Top = 116
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdJuros1: TdmkEdit
          Left = 204
          Top = 116
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdTaxas1: TdmkEdit
          Left = 108
          Top = 116
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 212
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 256
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrAlinea1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM alineas'
      'ORDER BY Nome')
    Left = 308
    Top = 12
    object QrAlinea1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlinea1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAlinea1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlinea1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlinea1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlinea1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlinea1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsAlinea1: TDataSource
    DataSet = QrAlinea1
    Left = 336
    Top = 12
  end
  object DsAlinea2: TDataSource
    DataSet = QrAlinea2
    Left = 336
    Top = 40
  end
  object QrAlinea2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM alineas'
      'ORDER BY Nome')
    Left = 308
    Top = 40
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Lk'
    end
    object DateField1: TDateField
      FieldName = 'DataCad'
    end
    object DateField2: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField3: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField4: TIntegerField
      FieldName = 'UserAlt'
    end
  end
end
