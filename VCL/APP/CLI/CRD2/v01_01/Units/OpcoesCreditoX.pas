unit OpcoesCreditoX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Registry, Grids, Db,
  mySQLDbTables, DBCtrls, ExtDlgs, ComCtrls, dmkGeral, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmOpcoesCreditoX = class(TForm)
    PainelDados: TPanel;
    QrAditivo2: TmySQLQuery;
    DsAditivo2: TDataSource;
    OpenDialog1: TOpenDialog;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrAditivo3: TmySQLQuery;
    DsAditivo3: TDataSource;
    QrAditivo4: TmySQLQuery;
    DsAditivo4: TDataSource;
    OpenPictureDialog1: TOpenPictureDialog;
    QrocorBank: TmySQLQuery;
    QrocorBankCodigo: TIntegerField;
    QrocorBankNome: TWideStringField;
    QrocorBankLk: TIntegerField;
    QrocorBankDataCad: TDateField;
    QrocorBankDataAlt: TDateField;
    QrocorBankUserCad: TIntegerField;
    QrocorBankUserAlt: TIntegerField;
    QrocorBankBase: TFloatField;
    DsOcorBank: TDataSource;
    DBmysql: TmySQLDatabase;
    QrMysql: TmySQLQuery;
    QrOcorDupl: TmySQLQuery;
    DsOcorDupl: TDataSource;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    TbControle: TmySQLTable;
    TbControleCartSacPe: TWideMemoField;
    DsControle: TDataSource;
    QrColigEsp: TmySQLQuery;
    DsColigEsp: TDataSource;
    QrColigEspNOMECOLIG: TWideStringField;
    QrColigEspCodigo: TIntegerField;
    QrCartDupl: TmySQLQuery;
    DsCartDupl: TDataSource;
    DsCartCH: TDataSource;
    QrCartCH: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    EdCPMF: TdmkEdit;
    EdIOF_ME: TdmkEdit;
    RGAdValorem: TRadioGroup;
    RGTipoPrazoDesc: TRadioGroup;
    EdAdValoremV: TdmkEdit;
    EdAdValoremP: TdmkEdit;
    EdRegiaoCompe: TdmkEdit;
    EdPIS_R: TdmkEdit;
    EdPIS: TdmkEdit;
    EdCOFINS_R: TdmkEdit;
    EdCOFINS: TdmkEdit;
    EdISS: TdmkEdit;
    EdChequeMaior: TdmkEdit;
    EdCHValAlto: TdmkEdit;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    EdDUValAlto: TdmkEdit;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    StaticText9: TStaticText;
    StaticText10: TStaticText;
    StaticText11: TStaticText;
    StaticText12: TStaticText;
    StaticText13: TStaticText;
    StaticText14: TStaticText;
    StaticText15: TStaticText;
    EdDURisco: TdmkEdit;
    EdCHRisco: TdmkEdit;
    GroupBox1: TGroupBox;
    StaticText16: TStaticText;
    StaticText17: TStaticText;
    EdFatorCompraMin: TdmkEdit;
    EdAdValMinVal: TdmkEdit;
    RGAdValMinTip: TRadioGroup;
    RGCalcMyJuro: TRadioGroup;
    RGImpreCheque: TRadioGroup;
    RGTaxasAutom: TRadioGroup;
    CkEnvelopeSacado: TCheckBox;
    CkColoreEnvelope: TCheckBox;
    EdCMC: TdmkEdit;
    CkCPMF_Padrao: TCheckBox;
    StaticText18: TStaticText;
    StaticText19: TStaticText;
    EdMinhaCompe: TdmkEdit;
    StaticText20: TStaticText;
    EdOutraCompe: TdmkEdit;
    StaticText21: TStaticText;
    EdCBEMinimo: TdmkEdit;
    RGSBCPadrao: TRadioGroup;
    StaticText22: TStaticText;
    EdIOF_PJ: TdmkEdit;
    StaticText23: TStaticText;
    EdIOF_PF: TdmkEdit;
    StaticText24: TStaticText;
    EdIOF_Ex: TdmkEdit;
    StaticText25: TStaticText;
    EdIOF_Li: TdmkEdit;
    RGTipNomeEmp: TRadioGroup;
    RGCodCliRel: TRadioGroup;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label75: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label44: TLabel;
    Label9: TLabel;
    Label47: TLabel;
    EdCartEspecie: TdmkEditCB;
    CBCartEspecie: TdmkDBLookupComboBox;
    EdProtAtu: TdmkEditCB;
    CBProtAtu: TdmkDBLookupComboBox;
    EdAditAtu: TdmkEditCB;
    CBAditAtu: TdmkDBLookupComboBox;
    EdCartAtu: TdmkEditCB;
    CBCartAtu: TdmkDBLookupComboBox;
    EdImportPath: TdmkEdit;
    EdLogo1Path: TdmkEdit;
    EdNFAliq: TdmkEdit;
    EdNFLei: TdmkEdit;
    EdNFLinha1: TdmkEdit;
    EdNFLinha2: TdmkEdit;
    EdNFLinha3: TdmkEdit;
    EdColigEsp: TdmkEditCB;
    CBColigEsp: TdmkDBLookupComboBox;
    CBCartCheques: TdmkDBLookupComboBox;
    EdCartCheques: TdmkEditCB;
    EdCartDuplic: TdmkEditCB;
    CBCartDuplic: TdmkDBLookupComboBox;
    TabSheet3: TTabSheet;
    Label22: TLabel;
    Label31: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdContabIOF: TdmkEdit;
    EdContabAdV: TdmkEdit;
    EdContabFaC: TdmkEdit;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdmsnID: TdmkEdit;
    EdmsnPW: TdmkEdit;
    CkmsnCA: TCheckBox;
    GroupBox4: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    EdFTP_Pwd: TdmkEdit;
    EdFTP_Hos: TdmkEdit;
    EdFTP_Log: TdmkEdit;
    EdFTP_Lix: TdmkEdit;
    EdFTP_Min: TdmkEdit;
    CkFTP_Aut: TCheckBox;
    EdddArqMorto: TdmkEdit;
    EdPathBBRet: TdmkEdit;
    TabSheet4: TTabSheet;
    Label43: TLabel;
    GroupBox5: TGroupBox;
    Label23: TLabel;
    EdNF_Cabecalho: TdmkEdit;
    GroupBox6: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    EdCSD_MEsqCidata: TdmkEdit;
    EdCSD_MEsqDestin: TdmkEdit;
    EdCSD_MEsqDuplic: TdmkEdit;
    EdCSD_MEsqClient: TdmkEdit;
    EdCSD_TopoCidata: TdmkEdit;
    EdCSD_TopoDestin: TdmkEdit;
    EdCSD_TopoDuplic: TdmkEdit;
    EdCSD_TopoClient: TdmkEdit;
    EdCSD_MEsqDesti2: TdmkEdit;
    EdCSD_TopoDesti2: TdmkEdit;
    MeCartSacPe: TMemo;
    TabSheet5: TTabSheet;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    CkLiqOcoCDi: TCheckBox;
    EdLiqOcoCOc: TdmkEditCB;
    CBLiqOcoCOc: TdmkDBLookupComboBox;
    CkLiqOcoShw: TCheckBox;
    GroupBox8: TGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    EdLiqStaAnt: TdmkEditCB;
    CBLiqStaAnt: TdmkDBLookupComboBox;
    EdLiqStaVct: TdmkEditCB;
    CBLiqStaVct: TdmkDBLookupComboBox;
    EdLiqStaVcd: TdmkEditCB;
    CBLiqStaVcd: TdmkDBLookupComboBox;
    TabSheet7: TTabSheet;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    EdWeb_Host: TdmkEdit;
    EdWeb_User: TdmkEdit;
    EdWeb_Pwd: TdmkEdit;
    EdWeb_DB: TdmkEdit;
    EdWeb_Page: TdmkEdit;
    RGWeb_MySQL: TRadioGroup;
    GroupBox7: TGroupBox;
    Label39: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    EdWeb_FTPh: TdmkEdit;
    EdWeb_FTPu: TdmkEdit;
    EdWeb_FTPs: TdmkEdit;
    BtCriaBD: TBitBtn;
    GroupBox9: TGroupBox;
    Label45: TLabel;
    Label46: TLabel;
    EdNewWebMins: TdmkEdit;
    CkNewWebScan: TCheckBox;
    TabSheet8: TTabSheet;
    Panel3: TPanel;
    GroupBox10: TGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    EdIOF_F_PJ: TdmkEdit;
    EdIOF_D_PJ: TdmkEdit;
    CkIOF_Max_Usa_PJ: TCheckBox;
    EdIOF_Max_Per_PJ: TdmkEdit;
    GroupBox11: TGroupBox;
    Label50: TLabel;
    Label51: TLabel;
    EdIOF_F_PF: TdmkEdit;
    EdIOF_D_PF: TdmkEdit;
    CkIOF_Max_Usa_PF: TCheckBox;
    EdIOF_Max_Per_PF: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    CBCartEmiCH: TdmkDBLookupComboBox;
    EdCartEmiCH: TdmkEditCB;
    QrAditivo5: TmySQLQuery;
    DsAditivo5: TDataSource;
    procedure BtOKClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdImportPathKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGImpreChequeClick(Sender: TObject);
    procedure EdLogo1PathKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFTP_LixKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPathBBRetKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdWeb_HostChange(Sender: TObject);
    procedure BtCriaBDClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;


  var
  FmOpcoesCreditoX: TFmOpcoesCreditoX;

implementation


uses UnMyObjects, Module, CreditoConsts, Principal, UnInternalConsts, PertoChek,
UMySQLModule;

{$R *.DFM}

procedure TFmOpcoesCreditoX.BtOKClick(Sender: TObject);
var
  Caminho: String;
  //
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE Controle SET CPMF=:P0, IOF_Ex=:P1, ChequeMaior=:P2, ');
  Dmod.QrUpd.SQL.Add('ISS=:P3, COFINS=:P4, COFINS_R=:P5, PIS=:P6, PIS_R=:P7, ');
  Dmod.QrUpd.SQL.Add('RegiaoCompe=:P8, AdValoremP=:P9, AdValoremV=:P10, ');
  Dmod.QrUpd.SQL.Add('TipoAdValorem=:P11, TipoPrazoDesc=:P12, CHValAlto=:P13, ');
  Dmod.QrUpd.SQL.Add('DUValAlto=:P14, CHRisco=:P15, DURisco=:P16, AditAtu=:P17,');
  Dmod.QrUpd.SQL.Add('ImportPath=:P18, FatorCompraMin=:P19, AdValMinVal=:P20, ');
  Dmod.QrUpd.SQL.Add('AdValMinTip=:P21, CalcMyJuro=:P22, CartEspecie=:P23, ');
  Dmod.QrUpd.SQL.Add('CartAtu=:P24, ProtAtu=:P25, TaxasAutom=:P26, ');
  Dmod.QrUpd.SQL.Add('EnvelopeSacado=:P27, ColoreEnvelope=:P28, ');
  Dmod.QrUpd.SQL.Add('ContabIOF=:P29, ContabAdV=:P30, ContabFaC=:P31, ');
  Dmod.QrUpd.SQL.Add('CMC=:P32, msnID=:P33, msnPW=AES_ENCRYPT(:P34, :P35), ');
  Dmod.QrUpd.SQL.Add('msnCA=:P36, NFLinha1=:P37, NFLinha2=:P38, ');
  Dmod.QrUpd.SQL.Add('NFLinha3=:P39, NFAliq=:P40, NFLei=:P41, ');
  Dmod.QrUpd.SQL.Add('FTP_Pwd=:P42, FTP_Hos=:P43, FTP_Log=:P44, FTP_Lix=:P45, ');
  Dmod.QrUpd.SQL.Add('FTP_Min=:P46, FTP_Aut=:P47, CPMF_Padrao=:P48, ');
  Dmod.QrUpd.SQL.Add('ddArqMorto=:P49, NF_Cabecalho=:P50, ');
  Dmod.QrUpd.SQL.Add('CSD_TopoCidata=:P51, CSD_TopoDestin=:P52, ');
  Dmod.QrUpd.SQL.Add('CSD_TopoDuplic=:P53, CSD_TopoClient=:P54, ');
  Dmod.QrUpd.SQL.Add('CSD_MEsqCidata=:P55, CSD_MEsqDestin=:P56, ');
  Dmod.QrUpd.SQL.Add('CSD_MEsqDuplic=:P57, CSD_MEsqClient=:P58, ');
  Dmod.QrUpd.SQL.Add('CSD_TopoDesti2=:P59, CSD_MEsqDesti2=:P60, ');
  Dmod.QrUpd.SQL.Add('MinhaCompe=:P61, OutraCompe=:P62, CBEMinimo=:P63, ');
  Dmod.QrUpd.SQL.Add('SBCPadrao=:P64, IOF_Li=:P65, PathBBRet=:P66, ');
  Dmod.QrUpd.SQL.Add('LiqOcoCDi=:P67, LiqOcoShw=:P68, LiqOcoCOc=:P69, ');
  Dmod.QrUpd.SQL.Add('Web_Page=:P70, Web_Host=:P71, Web_User=:P72, ');
  Dmod.QrUpd.SQL.Add('Web_Pwd=:P73, Web_DB=:P74, Web_FTPu=:P75, ');
  Dmod.QrUpd.SQL.Add('Web_FTPs=:P76, Web_FTPh=:P77, Web_MySQL=:P78, ');
  Dmod.QrUpd.SQL.Add('LiqStaAnt=:P79, LiqStaVct=:P80, LiqStaVcd=:P81, ');
  Dmod.QrUpd.SQL.Add('CartSacPe=:P82, ColigEsp=:P83, ');
  Dmod.QrUpd.SQL.Add('NewWebScan=:P84, NewWebMins=:P85, IOF_PF=:P86, ');
  Dmod.QrUpd.SQL.Add('IOF_PJ=:P87, IOF_ME=:P88, TipNomeEmp=:P89, ');
  Dmod.QrUpd.SQL.Add('CodCliRel=:P90, CartCheques=:P91, CartDuplic=:P92, ');
  Dmod.QrUpd.SQL.Add('IOF_F_PJ=:P93, IOF_D_PJ=:P94, IOF_Max_Per_PJ=:P95, ');
  Dmod.QrUpd.SQL.Add('IOF_Max_Usa_PJ=:P96, IOF_F_PF=:P97, IOF_D_PF=:P98, ');
  Dmod.QrUpd.SQL.Add('IOF_Max_Per_PF=:P99, IOF_Max_Usa_PF=:P100, CartEmiCH=:P101');
  //
  Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(EdCPMF.Text);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdIOF_Ex.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdChequeMaior.Text);
  Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdISS.Text);
  Dmod.QrUpd.Params[04].AsFloat   := Geral.DMV(EdCOFINS.Text);
  Dmod.QrUpd.Params[05].AsFloat   := Geral.DMV(EdCOFINS_R.Text);
  Dmod.QrUpd.Params[06].AsFloat   := Geral.DMV(EdPIS.Text);
  Dmod.QrUpd.Params[07].AsFloat   := Geral.DMV(EdPIS_R.Text);
  Dmod.QrUpd.Params[08].AsInteger := EdRegiaoCompe.ValueVariant;
  Dmod.QrUpd.Params[09].AsFloat   := EdAdValoremP.ValueVariant;
  Dmod.QrUpd.Params[10].AsFloat   := EdAdValoremV.ValueVariant;
  Dmod.QrUpd.Params[11].AsInteger := RGAdValorem.ItemIndex;
  Dmod.QrUpd.Params[12].AsInteger := RGTipoPrazoDesc.ItemIndex;
  Dmod.QrUpd.Params[13].AsFloat   := Geral.DMV(EdCHValAlto.Text);
  Dmod.QrUpd.Params[14].AsFloat   := Geral.DMV(EdDUValAlto.Text);
  Dmod.QrUpd.Params[15].AsFloat   := Geral.DMV(EdCHRisco.Text);
  Dmod.QrUpd.Params[16].AsFloat   := Geral.DMV(EdDURisco.Text);
  Dmod.QrUpd.Params[17].AsInteger := EdAditAtu.ValueVariant;
  Dmod.QrUpd.Params[18].AsString  := EdImportPath.Text;
  Dmod.QrUpd.Params[19].AsFloat   := Geral.DMV(EdFatorCompraMin.Text);
  Dmod.QrUpd.Params[20].AsFloat   := Geral.DMV(EdAdValMinVal.Text);
  Dmod.QrUpd.Params[21].AsInteger := RGAdValMinTip.ItemIndex;
  Dmod.QrUpd.Params[22].AsInteger := RGCalcMyJuro.ItemIndex;
  Dmod.QrUpd.Params[23].AsInteger := EdCartEspecie.ValueVariant;
  Dmod.QrUpd.Params[24].AsInteger := EdCartAtu.ValueVariant;
  Dmod.QrUpd.Params[25].AsInteger := EdProtAtu.ValueVariant;
  Dmod.QrUpd.Params[26].AsInteger := RGTaxasAutom.ItemIndex;
  Dmod.QrUpd.Params[27].AsInteger := MLAGeral.BoolToInt(CkEnvelopeSacado.Checked);
  Dmod.QrUpd.Params[28].AsInteger := MLAGeral.BoolToInt(CkColoreEnvelope.Checked);
  Dmod.QrUpd.Params[29].AsString  := EdContabIOF.Text;
  Dmod.QrUpd.Params[30].AsString  := EdContabAdV.Text;
  Dmod.QrUpd.Params[31].AsString  := EdContabFaC.Text;
  Dmod.QrUpd.Params[32].AsString  := EdCMC.Text;
  Dmod.QrUpd.Params[33].AsString  := EdmsnID.Text;
  Dmod.QrUpd.Params[34].AsString  := EdmsnPW.Text;
  Dmod.QrUpd.Params[35].AsString  := CO_USERSPNOW;
  Dmod.QrUpd.Params[36].AsInteger := MLAGeral.BoolToInt(CkmsnCA.Checked);
  Dmod.QrUpd.Params[37].AsString  := EdNFLinha1.Text;
  Dmod.QrUpd.Params[38].AsString  := EdNFLinha2.Text;
  Dmod.QrUpd.Params[39].AsString  := EdNFLinha3.Text;
  Dmod.QrUpd.Params[40].AsString  := EdNFAliq.Text;
  Dmod.QrUpd.Params[41].AsString  := EdNFLei.Text;
  Dmod.QrUpd.Params[42].AsString  := EdFTP_Pwd.Text;
  Dmod.QrUpd.Params[43].AsString  := EdFTP_Hos.Text;
  Dmod.QrUpd.Params[44].AsString  := EdFTP_Log.Text;
  Dmod.QrUpd.Params[45].AsString  := EdFTP_Lix.Text;
  Dmod.QrUpd.Params[46].AsInteger := EdFTP_Min.ValueVariant;
  Dmod.QrUpd.Params[47].AsInteger := MLAGeral.BoolToInt(CkFTP_Aut.Checked);
  Dmod.QrUpd.Params[48].AsInteger := MLAGeral.BoolToInt(CkCPMF_Padrao.Checked);
  Dmod.QrUpd.Params[49].AsInteger := EdddArqMorto.ValueVariant;
  Dmod.QrUpd.Params[50].AsInteger := EdNF_Cabecalho.ValueVariant;
  //
  Dmod.QrUpd.Params[51].AsInteger := EdCSD_TopoCidata.ValueVariant;
  Dmod.QrUpd.Params[52].AsInteger := EdCSD_TopoDestin.ValueVariant;
  Dmod.QrUpd.Params[53].AsInteger := EdCSD_TopoDuplic.ValueVariant;
  Dmod.QrUpd.Params[54].AsInteger := EdCSD_TopoClient.ValueVariant;
  Dmod.QrUpd.Params[55].AsInteger := EdCSD_MEsqCidata.ValueVariant;
  Dmod.QrUpd.Params[56].AsInteger := EdCSD_MEsqDestin.ValueVariant;
  Dmod.QrUpd.Params[57].AsInteger := EdCSD_MEsqDuplic.ValueVariant;
  Dmod.QrUpd.Params[58].AsInteger := EdCSD_MEsqClient.ValueVariant;
  Dmod.QrUpd.Params[59].AsInteger := EdCSD_TopoDesti2.ValueVariant;
  Dmod.QrUpd.Params[60].AsInteger := EdCSD_MEsqDesti2.ValueVariant;
  //
  Dmod.QrUpd.Params[61].AsInteger := EdMinhaCompe.ValueVariant;
  Dmod.QrUpd.Params[62].AsInteger := EdOutraCompe.ValueVariant;
  Dmod.QrUpd.Params[63].AsInteger := EdCBEMinimo.ValueVariant;
  Dmod.QrUpd.Params[64].AsInteger := RGSBCPadrao.ItemIndex;
  //
  Dmod.QrUpd.Params[65].AsFloat   := Geral.DMV(EdIOF_Li.Text);
  Dmod.QrUpd.Params[66].AsString  := EdPathBBRet.Text;
  //
  Dmod.QrUpd.Params[67].AsInteger := MLAGeral.BoolToInt(CkLiqOcoCDi.Checked);
  Dmod.QrUpd.Params[68].AsInteger := MLAGeral.BoolToInt(CkLiqOcoShw.Checked);
  Dmod.QrUpd.Params[69].AsInteger := EdLiqOcoCOc.ValueVariant;
  //
  Dmod.QrUpd.Params[70].AsString  := EdWeb_Page.Text;
  Dmod.QrUpd.Params[71].AsString  := EdWeb_Host.Text;
  Dmod.QrUpd.Params[72].AsString  := EdWeb_User.Text;
  Dmod.QrUpd.Params[73].AsString  := EdWeb_Pwd.Text;
  Dmod.QrUpd.Params[74].AsString  := EdWeb_DB.Text;
  Dmod.QrUpd.Params[75].AsString  := EdWeb_FTPu.Text;
  Dmod.QrUpd.Params[76].AsString  := EdWeb_FTPs.Text;
  Dmod.QrUpd.Params[77].AsString  := EdWeb_FTPh.Text;
  Dmod.QrUpd.Params[78].AsInteger := RGWeb_MySQL.ItemIndex;
  //
  Dmod.QrUpd.Params[79].AsInteger := EdLiqStaAnt.ValueVariant;
  Dmod.QrUpd.Params[80].AsInteger := EdLiqStaVct.ValueVariant;
  Dmod.QrUpd.Params[81].AsInteger := EdLiqStaVcd.ValueVariant;
  //
  Dmod.QrUpd.Params[82].AsString  := MeCartsacPe.Text;
  //
  Dmod.QrUpd.Params[83].AsInteger := EdColigEsp.ValueVariant;
  //
  Dmod.QrUpd.Params[84].AsInteger := MLAGeral.BoolToInt(CkNewWebScan.Checked);
  Dmod.QrUpd.Params[85].AsInteger := EdNewWebMins.ValueVariant;
  //
  Dmod.QrUpd.Params[86].AsFloat   := Geral.DMV(EdIOF_PF.Text);
  Dmod.QrUpd.Params[87].AsFloat   := Geral.DMV(EdIOF_PJ.Text);
  Dmod.QrUpd.Params[88].AsFloat   := Geral.DMV(EdIOF_ME.Text);
  Dmod.QrUpd.Params[89].AsInteger := RGTipNomeEmp.ItemIndex;
  Dmod.QrUpd.Params[90].AsInteger := RGCodCliRel.ItemIndex;
  //
  Dmod.QrUpd.Params[91].AsInteger := EdCartCheques.ValueVariant;
  Dmod.QrUpd.Params[92].AsInteger := EdCartDuplic.ValueVariant;
  //
  Dmod.QrUpd.Params[93].AsFloat := EdIOF_F_PJ.ValueVariant;
  Dmod.QrUpd.Params[94].AsFloat := EdIOF_D_PJ.ValueVariant;
  Dmod.QrUpd.Params[95].AsFloat := EdIOF_Max_Per_PJ.ValueVariant;
  Dmod.QrUpd.Params[96].AsInteger := MLAGeral.BTI(CkIOF_Max_Usa_PJ.Checked);
  //
  Dmod.QrUpd.Params[97].AsFloat := EdIOF_F_PF.ValueVariant;
  Dmod.QrUpd.Params[98].AsFloat := EdIOF_D_PF.ValueVariant;
  Dmod.QrUpd.Params[99].AsFloat := EdIOF_Max_Per_PF.ValueVariant;
  Dmod.QrUpd.Params[100].AsInteger := MLAGeral.BTI(CkIOF_Max_Usa_PF.Checked);
  Dmod.QrUpd.Params[101].AsInteger := EdCartEmiCH.ValueVariant;
  //
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrControle.Close;
  UMyMod.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  //
  Geral.WriteAppKey('Logo1', Application.Title, EdLogo1Path.Text, ktString,
    HKEY_LOCAL_MACHINE);
  //
  Caminho := CRD_CAMINHO_REGEDIT+'\Opcoes';
  Geral.WriteAppKey('JaSetado', Caminho, True, ktBoolean, HKEY_LOCAL_MACHINE);
  //
  Geral.WriteAppKey('ImpCheque', 'Dermatek', VAR_IMPCHEQUE, ktInteger,
    HKEY_LOCAL_MACHINE);
  //PertoChekP.VerificaPertoChek;
  //
  Close;
end;

procedure TFmOpcoesCreditoX.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesCreditoX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesCreditoX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesCreditoX.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
  TbControle. Open;
  UMyMod.AbreQuery(QrCartCH, Dmod.MyDB);
  UMyMod.AbreQuery(QrCartDupl, Dmod.MyDB);
  //
  EdCPMF.Text               := Geral.FFT(Dmod.QrControleCPMF.Value, 4, siPositivo);
  EdIOF_Ex.Text             := Geral.FFT(Dmod.QrControleIOF_Ex.Value, 4, siPositivo);
  EdIOF_PF.Text             := Geral.FFT(Dmod.QrControleIOF_PF.Value, 4, siPositivo);
  EdIOF_PJ.Text             := Geral.FFT(Dmod.QrControleIOF_PJ.Value, 4, siPositivo);
  EdIOF_ME.Text             := Geral.FFT(Dmod.QrControleIOF_ME.Value, 4, siPositivo);
  EdIOF_Li.Text             := Geral.FFT(Dmod.QrControleIOF_Li.Value, 2, siPositivo);
  EdChequeMaior.Text        := Geral.FFT(Dmod.QrControleChequeMaior.Value, 2, siPositivo);
  EdISS.Text                := Geral.FFT(Dmod.QrControleISS.Value, 4, siPositivo) ;
  EdCOFINS.Text             := Geral.FFT(Dmod.QrControleCOFINS.Value, 4, siPositivo);
  EdCOFINS_R.Text           := Geral.FFT(Dmod.QrControleCOFINS_R.Value, 4, siPositivo);
  EdPIS.Text                := Geral.FFT(Dmod.QrControlePIS.Value, 4, siPositivo);
  EdPIS_R.Text              := Geral.FFT(Dmod.QrControlePIS_R.Value, 4, siPositivo);
  EdRegiaoCompe.Text        := MLAGeral.FFD(Dmod.QrControleRegiaoCompe.Value, 3, siPositivo);
  EdMinhaCompe.Text         := MLAGeral.FFD(Dmod.QrControleMinhaCompe.Value, 0, siPositivo);
  EdOutraCompe.Text         := MLAGeral.FFD(Dmod.QrControleOutraCompe.Value, 0, siPositivo);
  EdCBEMinimo.Text          := MLAGeral.FFD(Dmod.QrControleCBEMinimo.Value, 0, siPositivo);
  RGSBCPadrao.ItemIndex     := Dmod.QrControleSBCPadrao.Value;
  EdAdValoremP.Text         := Geral.FFT(Dmod.QrControleAdValoremP.Value, 4, siPositivo);
  EdAdValoremV.Text         := Geral.FFT(Dmod.QrControleAdValoremV.Value, 2, siPositivo);
  RGAdValorem.ItemIndex     := Dmod.QrControleTipoAdValorem.Value;
  RGTipoPrazoDesc.ItemIndex := Dmod.QrControleTipoPrazoDesc.Value;
  RGImpreCheque.ItemIndex   := VAR_IMPCHEQUE;
  EdCHValAlto.Text          := Geral.FFT(Dmod.QrControleCHValAlto.Value, 2, siPositivo);
  EdDUValAlto.Text          := Geral.FFT(Dmod.QrControleDUValAlto.Value, 2, siPositivo);
  EdCHRisco.Text            := Geral.FFT(Dmod.QrControleCHRisco.Value, 2, siPositivo);
  EdDURisco.Text            := Geral.FFT(Dmod.QrControleDURisco.Value, 2, siPositivo);
  EdAditAtu.Text            := IntToStr(Dmod.QrControleAditAtu.Value);
  CBAditAtu.KeyValue        := Dmod.QrControleAditAtu.Value;
  EdCartAtu.Text            := IntToStr(Dmod.QrControleCartAtu.Value);
  CBCartAtu.KeyValue        := Dmod.QrControleCartAtu.Value;
  EdProtAtu.Text            := IntToStr(Dmod.QrControleProtAtu.Value);
  CBProtAtu.KeyValue        := Dmod.QrControleProtAtu.Value;
  EdCartEspecie.Text        := IntToStr(Dmod.QrControleCartEspecie.Value);
  CBCartEspecie.KeyValue    := Dmod.QrControleCartEspecie.Value;
  EdImportPath.Text         := Dmod.QrControleImportPath.Value;
  EdAdValMinVal.Text        := Geral.FFT(Dmod.QrControleAdValMinVal.Value, 2, siPositivo);
  EdFatorCompraMin.Text     := Geral.FFT(Dmod.QrControleFatorCompraMin.Value, 2, siPositivo);
  RGAdValMinTip.ItemIndex   := Dmod.QrControleAdValMinTip.Value;
  RGCalcMyJuro.ItemIndex    := Dmod.QrControleCalcMyJuro.Value;
  RGTaxasAutom.ItemIndex    := Dmod.QrControleTaxasAutom.Value;
  CkEnvelopeSacado.Checked  := Geral.IntToBool_0(Dmod.QrControleEnvelopeSacado.Value);
  CkColoreEnvelope.Checked  := Geral.IntToBool_0(Dmod.QrControleColoreEnvelope.Value);
  //
  EdContabIOF.Text := Dmod.QrControleContabIOF.Value;
  EdContabAdV.Text := Dmod.QrControleContabAdV.Value;
  EdContabFaC.Text := Dmod.QrControleContabFaC.Value;
  //
  EdCMC.Text        := Dmod.QrControleCMC.Value;
  EdmsnID.Text      := Dmod.QrControlemsnID.Value;
  EdmsnPW.Text      := Dmod.QrControlemsnPW.Value;
  CkmsnCA.Checked   := MLAGeral.IntToBool(Dmod.QrControlemsnCA.Value);
  EdFTP_Pwd.Text    := Dmod.QrControleFTP_Pwd.Value;
  EdFTP_Hos.Text    := Dmod.QrControleFTP_Hos.Value;
  EdFTP_Log.Text    := Dmod.QrControleFTP_Log.Value;
  EdFTP_Lix.Text    := Dmod.QrControleFTP_Lix.Value;
  EdFTP_Min.Text    := IntToStr(Dmod.QrControleFTP_Min.Value);
  CkFTP_Aut.Checked := MLAGeral.IntToBool(Dmod.QrControleFTP_Aut.Value);
  //
  EdNFLinha1.Text     := Dmod.QrControleNFLinha1.Value;
  EdNFLinha2.Text     := Dmod.QrControleNFLinha2.Value;
  EdNFLinha3.Text     := Dmod.QrControleNFLinha3.Value;
  EdNFAliq.Text       := Dmod.QrControleNFAliq.Value;
  EdNFLei.Text        := Dmod.QrControleNFLei.Value;
  EdddArqMorto.Text   := IntToStr(Dmod.QrControleddArqMorto.Value);
  EdNF_Cabecalho.Text := IntToStr(Dmod.QrControleNF_Cabecalho.Value);
  //
  EdCSD_TopoCidata.Text := IntToStr(Dmod.QrControleCSD_TopoCidata.Value);
  EdCSD_TopoDestin.Text := IntToStr(Dmod.QrControleCSD_TopoDestin.Value);
  EdCSD_TopoDuplic.Text := IntToStr(Dmod.QrControleCSD_TopoDuplic.Value);
  EdCSD_TopoClient.Text := IntToStr(Dmod.QrControleCSD_TopoClient.Value);
  EdCSD_TopoDesti2.Text := IntToStr(Dmod.QrControleCSD_TopoDesti2.Value);
  //
  EdCSD_MEsqCidata.Text := IntToStr(Dmod.QrControleCSD_MEsqCidata.Value);
  EdCSD_MEsqDestin.Text := IntToStr(Dmod.QrControleCSD_MEsqDestin.Value);
  EdCSD_MEsqDuplic.Text := IntToStr(Dmod.QrControleCSD_MEsqDuplic.Value);
  EdCSD_MEsqClient.Text := IntToStr(Dmod.QrControleCSD_MEsqClient.Value);
  EdCSD_MEsqDesti2.Text := IntToStr(Dmod.QrControleCSD_MEsqDesti2.Value);
  //
  CkCPMF_Padrao.Checked := MLAGeral.IntToBool(Dmod.QrControleCPMF_Padrao.Value);
  //
  EdPathBBRet.Text := Dmod.QrControlePathBBRet.Value;
  //
  CBLiqOcoCOc.KeyValue := Dmod.QrControleLiqOcoCOc.Value;
  EdLiqOcoCOc.Text     := IntToStr(Dmod.QrControleLiqOcoCOc.Value);
  CkLiqOcoCDi.Checked  := Geral.IntToBool_0(Dmod.QrControleLiqOcoCDi.Value);
  CkLiqOcoShw.Checked  := Geral.IntToBool_0(Dmod.QrControleLiqOcoShw.Value);
  //
  EdWeb_Page.Text       := Dmod.QrControleWeb_Page.Value;
  EdWeb_Host.Text       := Dmod.QrControleWeb_Host.Value;
  edWeb_User.Text       := Dmod.QrControleWeb_User.Value;
  EdWeb_Pwd.Text        := Dmod.QrControleWeb_Pwd.Value;
  EdWeb_DB.Text         := Dmod.QrControleWeb_DB.Value;
  EdWeb_FTPu.Text       := Dmod.QrControleWeb_FTPu.Value;
  EdWeb_FTPs.Text       := Dmod.QrControleWeb_FTPs.Value;
  EdWeb_FTPh.Text       := Dmod.QrControleWeb_FTPh.Value;
  RGWeb_MySQL.ItemIndex := Dmod.QrControleWeb_MySQL.Value;
  //
  EdLiqStaAnt.Text     := IntToStr(Dmod.QrControleLiqStaAnt.Value);
  CBLiqStaAnt.KeyValue := Dmod.QrControleLiqStaAnt.Value;
  EdLiqStaVct.Text     := IntToStr(Dmod.QrControleLiqStaVct.Value);
  CBLiqStaVct.KeyValue := Dmod.QrControleLiqStaVct.Value;
  EdLiqStaVcd.Text     := IntToStr(Dmod.QrControleLiqStaVcd.Value);
  CBLiqStaVcd.KeyValue := Dmod.QrControleLiqStaVcd.Value;
  //
  MeCartSacPe.Text := Dmod.QrControleCartSacPe.Value;
  //
  CkNewWebScan.Checked := Geral.IntToBool_0(Dmod.QrControleNewWebScan.Value);
  EdNewWebMins.Text    := IntToStr(Dmod.QrControleNewWebMins.Value);
  //
  RGTipNomeEmp.ItemIndex := Dmod.QrControleTipNomeEmp.Value;
  RGCodCliRel.ItemIndex  := Dmod.QrControleCodCliRel.Value;
  //
  EdCartCheques.Text     := FormatFloat('0', Dmod.QrControleCartCheques.Value);
  CBCartCheques.KeyValue := Dmod.QrControleCartCheques.Value;
  EdCartDuplic.Text      := FormatFloat('0', Dmod.QrControleCartDuplic.Value);
  CBCartDuplic.KeyValue  := Dmod.QrControleCartDuplic.Value;
  //
  EdCartEmiCH.ValueVariant := Dmod.QrControleCartEmiCH.Value;
  CBCartEmiCH.KeyValue     := Dmod.QrControleCartEmiCH.Value;
  //
  UMyMod.AbreQuery(QrAditivo2, Dmod.MyDB);
  UMyMod.AbreQuery(QrAditivo3, Dmod.MyDB);
  UMyMod.AbreQuery(QrAditivo4, Dmod.MyDB);
  UMyMod.AbreQuery(QrAditivo5, Dmod.MyDB);
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
  UMyMod.AbreQuery(QrOcorDupl, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigEsp, Dmod.MyDB);
  //
  EdLogo1Path.Text := Geral.ReadAppKey('Logo1', Application.Title, ktString, '',
    HKEY_LOCAL_MACHINE);

  EdColigEsp.Text := IntToStr(Dmod.QrControleColigEsp.Value);
  CBColigEsp.KeyValue := Dmod.QrControleColigEsp.Value;
  //
  PageControl1.ActivePageIndex := 0;
  //
  EdIOF_F_PJ.ValueVariant       := Dmod.QrControleIOF_F_PJ.Value;
  EdIOF_D_PJ.ValueVariant       := Dmod.QrControleIOF_D_PJ.Value;
  EdIOF_Max_Per_PJ.ValueVariant := Dmod.QrControleIOF_Max_Per_PJ.Value;
  CkIOF_Max_Usa_PJ.Checked      := Dmod.QrControleIOF_Max_Usa_PJ.Value = 1;
  //
  EdIOF_F_PF.ValueVariant       := Dmod.QrControleIOF_F_PF.Value;
  EdIOF_D_PF.ValueVariant       := Dmod.QrControleIOF_D_PF.Value;
  EdIOF_Max_Per_PF.ValueVariant := Dmod.QrControleIOF_Max_Per_PF.Value;
  CkIOF_Max_Usa_PF.Checked      := Dmod.QrControleIOF_Max_Usa_PF.Value = 1;
  //
end;

procedure TFmOpcoesCreditoX.EdImportPathKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenDialog1.Execute then EdImportPath.Text :=
      ExtractFilePath(OpenDialog1.FileName);
  end;
end;

procedure TFmOpcoesCreditoX.RGImpreChequeClick(Sender: TObject);
begin
  VAR_IMPCHEQUE := RGImpreCheque.ItemIndex;

end;

procedure TFmOpcoesCreditoX.EdLogo1PathKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if OpenPictureDialog1.Execute then EdLogo1Path.Text :=
      OpenPictureDialog1.FileName;
  end;
end;

procedure TFmOpcoesCreditoX.EdFTP_LixKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  MyObjects.DefineDiretorio(Self, EdFTP_Lix);
end;

procedure TFmOpcoesCreditoX.EdPathBBRetKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  MyObjects.DefineDiretorio(Self, EdPathBBRet);
end;

procedure TFmOpcoesCreditoX.EdWeb_HostChange(Sender: TObject);
var
  IP: String;
begin
  IP := Trim(EdWeb_Host.Text);
  if (IP = '127.0.0.1') or (Uppercase(IP) = 'LOCALHOST') then
    BtCriaBD.Enabled := True else BtCriaBD.Enabled := False;
end;

procedure TFmOpcoesCreditoX.BtCriaBDClick(Sender: TObject);
var
  DB, ID, PW: String;
begin
  DB := Trim(EdWeb_DB.Text);
  ID := Trim(EdWeb_User.Text);
  PW := Trim(EdWeb_Pwd.Text);
  //
  if MyObjects.FIC(DB = '', EdWeb_DB, 'Banco de dados n�o definido!') then Exit;
  if MyObjects.FIC(ID = '', EdWeb_User, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC(PW = '', EdWeb_Pwd, 'Senha n�o definida!') then Exit;
  //
  try
    // Cria banco de dados WEB local
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('CREATE DATABASE '+DB);
    Dmod.QrUpd.ExecSQL;

    // Cria usuario sem nenhum privil�gio
    DBmysql.Close;
    DBmysql.UserPassword := VAR_BDSENHA;
    //
    try
    QrMysql.SQL.Clear;
    QrMysql.SQL.Add('INSERT INTO User SET ');
    QrMysql.SQL.Add('Host = "localhost",'); // deve ser substituido por %
    QrMysql.SQL.Add('User = "'+ID+'",');
    QrMysql.SQL.Add('Password = PASSWORD("'+PW+'")');
    QrMysql.ExecSQL;
    except
    end;
    //
    //
    QrMysql.SQL.Clear;
    QrMysql.SQL.Add('INSERT INTO db SET ');
    QrMysql.SQL.Add('Host = "localhost",'); // deve ser substituido por %
    QrMysql.SQL.Add('DB = "'+DB+'",');
    QrMysql.SQL.Add('User = "'+ID+'",');
    QrMysql.SQL.Add('Select_priv           = "Y",');
    QrMysql.SQL.Add('Insert_priv           = "Y",');
    QrMysql.SQL.Add('Update_priv           = "Y",');
    QrMysql.SQL.Add('Delete_priv           = "Y",');
    QrMysql.SQL.Add('Create_priv           = "Y",');
    QrMysql.SQL.Add('Drop_priv             = "Y",');
    QrMysql.SQL.Add('Grant_priv            = "Y",');
    QrMysql.SQL.Add('References_priv       = "Y",');
    QrMysql.SQL.Add('Index_priv            = "Y",');
    QrMysql.SQL.Add('Alter_priv            = "Y",');
    QrMysql.SQL.Add('Create_tmp_table_priv = "Y",');
    QrMysql.SQL.Add('Lock_tables_priv      = "Y"');
    QrMysql.ExecSQL;
    //
    QrMysql.SQL.Clear;
    QrMysql.SQL.Add('FLUSH PRIVILEGES');
    QrMysql.ExecSQL;
    //
    Application.MessageBox('Banco de Dados criado com sucesso!', 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
    //
    BtOKClick(Self);
    Application.Terminate;
  except
    raise;
  end;
end;

end.
