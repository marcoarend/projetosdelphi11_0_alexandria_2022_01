unit Lot2Oco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmLot2Oco = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel20: TPanel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    EdJuros6: TdmkEdit;
    EdPago6: TdmkEdit;
    EdAPagar6: TdmkEdit;
    TPPagto6: TdmkEditDateTimePicker;
    Panel22: TPanel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    EdValorBase6: TdmkEdit;
    EdJurosBase6: TdmkEdit;
    EdJurosPeriodo6: TdmkEdit;
    TPDataBase6: TdmkEditDateTimePicker;
    DBGrid8: TDBGrid;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure EdValorBase6Change(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalculaJurosOcor();
    procedure CalculaAPagarOcor();
  end;

  var
  FmLot2Oco: TFmLot2Oco;

implementation

uses UnMyObjects, Module, Lot2Cab, UMySQLModule, ModuleLot;

{$R *.DFM}

procedure TFmLot2Oco.BtOKClick(Sender: TObject);
{
var
  Ocorreu, LotePg, Codigo: Integer;
  Data: String;
  Juros, Pago: Double;
begin
  Ocorreu := DmLot.QrOcorACodigo.Value;
  Data := Geral.FDT(TPPagto6.Date, 1);
  Juros := EdJuros6.ValueVariant;
  Pago := EdPago6.ValueVariant;
  LotePg := FmLot2Cab.QrLotCodigo.Value;
  Codigo := UMyMod.BuscaEmLivreY_Def('ocor rpg', 'Codigo', ImgTipo.SQLType, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ocor rpg', False, [
  'Ocorreu', 'Data', 'Juros',
  'Pago', 'LotePg'], [
  'Codigo'], [
  Ocorreu, Data, Juros,
  Pago, LotePg], [
  Codigo], True) then
  begin
    with FmLot2Cab do
    begin
      FOcorP := Codigo;
      ReopenOcorP();
      //
      CalculaPagtoOcorP(TPPagto6.Date, DmLot.QrOcorACodigo.Value);
      ///
      FOcorP := DmLot.QrOcorACodigo.Value;
      CalculaLote(QrLotCodigo.Value, False);
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    end;
    //
    Close;
  end;
}
var
  FatParcela, Controle, FatID_Sub, Genero, Cliente, Ocorreu,
  FatParcRef: Integer;
  FatNum, Valor, MoraVal: Double;
  Dta: TDateTime;
begin
  Genero := DmLot.QrOcorAPlaGen.Value;
  if MyObjects.FIC(Genero=0, nil, 'A ocorr�ncia ' + Geral.FF0(
  DmLot.QrOcorAOcorrencia.Value) +
  ' n�o tem cadastrada sua conta do plano de de contas!') then
    Exit;
  Screen.Cursor := crHourGlass;
  FatID_Sub := DmLot.QrOcorAOcorrencia.Value;
  Cliente := DmLot.QrOcorACliente.Value;
  Ocorreu := DmLot.QrOcorACodigo.Value;
  FatParcRef := DmLot.QrOcorALOIS.Value;
  //
  FatNum := FmLot2Cab.QrLotCodigo.Value; // Aleluia!
  FatParcela := 0;
  Controle := 0;
  Valor := Geral.DMV(EdPago6.Text);
  MoraVal := Geral.DMV(EdJuros6.Text);
  Dta := TPPagto6.Date;
  //
  if DmLot.SQL_OcorP(Dmod.QrUpd, stIns, FatParcela, Controle,
  FatID_Sub, Genero, Cliente, Ocorreu, FatParcRef,
  FatNum, Valor, MoraVal, Dta, 0) then
  begin
    with FmLot2Cab do
    begin
      FOcorP := FatParcela;
      ReopenOcorP();
      //
      CalculaPagtoOcorP(TPPagto6.Date, DmLot.QrOcorACodigo.Value);
      ///
      FOcorP := DmLot.QrOcorACodigo.Value;
      CalculaLote(QrLotCodigo.Value, False);
      LocCod(QrLotCodigo.Value, QrLotCodigo.Value);
    end;
    //
    Close;
  end;
end;

procedure TFmLot2Oco.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLot2Oco.CalculaAPagarOcor();
var
  Base, Juro: Double;
begin
  Base := EdValorBase6.ValueVariant;
  Juro := EdJuros6.ValueVariant;
  //
  EdAPagar6.ValueVariant := Base + Juro;
end;

procedure TFmLot2Oco.CalculaJurosOcor();
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase6.Text);
    Juros := MLAGeral.CalculaJuroComposto(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase6.Text);
  EdJurosPeriodo6.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros6.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmLot2Oco.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.Text := EdAPagar6.Text;
end;

procedure TFmLot2Oco.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := Geral.DMV(EdValorBase6.Text);
  Juros := Geral.DMV(EdJuros6.Text);
  EdAPagar6.Text := Geral.FFT(VBase+Juros, 2, siNegativo);
end;

procedure TFmLot2Oco.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor();
  CalculaAPagarOcor();
end;

procedure TFmLot2Oco.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor();
end;

procedure TFmLot2Oco.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLot2Oco.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLot2Oco.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor();
end;

end.
