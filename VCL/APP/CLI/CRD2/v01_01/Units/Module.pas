unit Module;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, (*DBTables,*) UnInternalConsts, UnMLAGeral, UMySQLModule, mySQLDbTables,
  UnGOTOy, Winsock, MySQLBatch, (*DBIProcs,*) frxClass, frxDBSet, dmkGeral, ABSMain,
  UnDmkProcFunc, UnDmkEnums, UnGrl_Vars;

type
  TDmod = class(TDataModule)
    QrFields: TMySQLQuery;
    QrMaster: TMySQLQuery;
    QrRecCountX: TMySQLQuery;
    QrRecCountXRecord: TIntegerField;
    QrUser: TMySQLQuery;
    QrSB: TMySQLQuery;
    QrSBCodigo: TIntegerField;
    QrSBNome: TWideStringField;
    DsSB: TDataSource;
    QrSB2: TMySQLQuery;
    QrSB2Codigo: TFloatField;
    QrSB2Nome: TWideStringField;
    DsSB2: TDataSource;
    QrSB3: TMySQLQuery;
    QrSB3Codigo: TDateField;
    QrSB3Nome: TWideStringField;
    DsSB3: TDataSource;
    QrDuplicIntX: TMySQLQuery;
    QrDuplicIntXINTEIRO1: TIntegerField;
    QrDuplicIntXINTEIRO2: TIntegerField;
    QrDuplicIntXCODIGO: TIntegerField;
    QrDuplicStrX: TMySQLQuery;
    QrDuplicStrXNOME: TWideStringField;
    QrDuplicStrXCODIGO: TIntegerField;
    QrDuplicStrXANTERIOR: TIntegerField;
    QrInsLogX: TMySQLQuery;
    QrDelLogX: TMySQLQuery;
    QrDataBalY: TmySQLQuery;
    QrDataBalYData: TDateField;
    QrSenha: TMySQLQuery;
    QrSenhaLogin: TWideStringField;
    QrSenhaNumero: TIntegerField;
    QrSenhaSenha: TWideStringField;
    QrSenhaPerfil: TIntegerField;
    QrSenhaLk: TIntegerField;
    QrUserLogin: TWideStringField;
    QrMaster2: TMySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrProduto: TMySQLQuery;
    QrVendas: TMySQLQuery;
    QrEntrada: TMySQLQuery;
    QrEntradaPecas: TFloatField;
    QrEntradaValor: TFloatField;
    QrVendasPecas: TFloatField;
    QrVendasValor: TFloatField;
    QrUpdM: TmySQLQuery;
    QrUpdU: TmySQLQuery;
    QrUpdL: TmySQLQuery;
    QrUpdY: TmySQLQuery;
    QrLivreY: TmySQLQuery;
    QrLivreYCodigo: TIntegerField;
    QrMaster2Em: TWideStringField;
    QrMaster2CNPJ: TWideStringField;
    QrMaster2Monitorar: TSmallintField;
    QrMaster2Distorcao: TIntegerField;
    QrMaster2DataI: TDateField;
    QrMaster2DataF: TDateField;
    QrMaster2Hoje: TDateField;
    QrMaster2Hora: TTimeField;
    QrMaster2MasLogin: TWideStringField;
    QrMaster2MasSenha: TWideStringField;
    QrMaster2MasAtivar: TWideStringField;
    QrCountY: TmySQLQuery;
    QrCountYRecord: TIntegerField;
    QrLocY: TmySQLQuery;
    QrLocYRecord: TIntegerField;
    QrIdx: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrSQL: TmySQLQuery;
    MyDB: TmySQLDatabase;
    MyLocDatabase: TmySQLDatabase;
    QrSB4: TmySQLQuery;
    DsSB4: TDataSource;
    QrSB4Codigo: TWideStringField;
    QrSB4Nome: TWideStringField;
    QrPriorNext: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrSoma1: TmySQLQuery;
    QrSoma1TOTAL: TFloatField;
    QrSoma1Qtde: TFloatField;
    QrControle: TmySQLQuery;
    QrControleSoMaiusculas: TWideStringField;
    QrControleMoeda: TWideStringField;
    QrControleUFPadrao: TIntegerField;
    QrControleCidade: TWideStringField;
    QrEstoque: TmySQLQuery;
    QrEstoqueTipo: TSmallintField;
    QrEstoqueQtde: TFloatField;
    QrEstoqueValorCus: TFloatField;
    QrPeriodoBal: TmySQLQuery;
    QrPeriodoBalPeriodo: TIntegerField;
    QrMin: TmySQLQuery;
    QrMinPeriodo: TIntegerField;
    QrBalancosIts: TmySQLQuery;
    QrBalancosItsEstqQ: TFloatField;
    QrBalancosItsEstqV: TFloatField;
    QrEstqperiodo: TmySQLQuery;
    QrBalancosItsEstqQ_G: TFloatField;
    QrBalancosItsEstqV_G: TFloatField;
    QrEstqperiodoTipo: TSmallintField;
    QrEstqperiodoQtde: TFloatField;
    QrEstqperiodoValorCus: TFloatField;
    QrControleCartVen: TIntegerField;
    QrControleCartCom: TIntegerField;
    QrControleCartDeS: TIntegerField;
    QrControleCartReS: TIntegerField;
    QrControleCartDeG: TIntegerField;
    QrControleCartReG: TIntegerField;
    QrControleCartCoE: TIntegerField;
    QrControleCartCoC: TIntegerField;
    QrControleCartEmD: TIntegerField;
    QrControleCartEmA: TIntegerField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrProdutoCodigo: TIntegerField;
    QrProdutoControla: TWideStringField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrPerfis: TmySQLQuery;
    QrPerfisLibera: TSmallintField;
    QrPerfisJanela: TWideStringField;
    QrControleTravaCidade: TSmallintField;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TIntegerField;
    QrControleEntraSemValor: TIntegerField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroNOMEpUF: TWideStringField;
    QrTerceiroNOMEeUF: TWideStringField;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroIEST: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroELograd: TSmallintField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECidade: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroEte2: TWideStringField;
    QrTerceiroEte3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEmail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPLograd: TSmallintField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCidade: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPte2: TWideStringField;
    QrTerceiroPte3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEmail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroCLograd: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCidade: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLLograd: TSmallintField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCidade: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroAccount: TIntegerField;
    QrTerceiroLogo2: TBlobField;
    QrTerceiroConjugeNome: TWideStringField;
    QrTerceiroConjugeNatal: TDateField;
    QrTerceiroNome1: TWideStringField;
    QrTerceiroNatal1: TDateField;
    QrTerceiroNome2: TWideStringField;
    QrTerceiroNatal2: TDateField;
    QrTerceiroNome3: TWideStringField;
    QrTerceiroNatal3: TDateField;
    QrTerceiroNome4: TWideStringField;
    QrTerceiroNatal4: TDateField;
    QrTerceiroCreditosI: TIntegerField;
    QrTerceiroCreditosL: TIntegerField;
    QrTerceiroCreditosF2: TFloatField;
    QrTerceiroCreditosD: TDateField;
    QrTerceiroCreditosU: TDateField;
    QrTerceiroCreditosV: TDateField;
    QrTerceiroMotivo: TIntegerField;
    QrTerceiroQuantI1: TIntegerField;
    QrTerceiroQuantI2: TIntegerField;
    QrTerceiroQuantI3: TIntegerField;
    QrTerceiroQuantI4: TIntegerField;
    QrTerceiroQuantN1: TFloatField;
    QrTerceiroQuantN2: TFloatField;
    QrTerceiroAgenda: TWideStringField;
    QrTerceiroSenhaQuer: TWideStringField;
    QrTerceiroSenha1: TWideStringField;
    QrTerceiroLimiCred: TFloatField;
    QrTerceiroDesco: TFloatField;
    QrTerceiroCasasApliDesco: TSmallintField;
    QrTerceiroTempD: TFloatField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TIntegerField;
    QrTerceiroUserAlt: TIntegerField;
    QrTerceiroCPF_Pai: TWideStringField;
    QrTerceiroSSP: TWideStringField;
    QrTerceiroCidadeNatal: TWideStringField;
    QrTerceiroUFNatal: TSmallintField;
    QlLocal: TMySQLBatchExecute;
    QrControleMyPagTip: TIntegerField;
    QrControleMyPagCar: TIntegerField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrControleCPMF: TFloatField;
    QrControleChequeMaior: TFloatField;
    QrControleISS: TFloatField;
    QrControleCOFINS: TFloatField;
    QrControleCOFINS_R: TFloatField;
    QrControlePIS: TFloatField;
    QrControlePIS_R: TFloatField;
    QrControleRegiaoCompe: TIntegerField;
    QrControleTipoAdValorem: TSmallintField;
    QrControleAdValoremP: TFloatField;
    QrControleAdValoremV: TFloatField;
    QrControleTipoPrazoDesc: TSmallintField;
    MyDB1: TmySQLDatabase;
    MyDB2: TmySQLDatabase;
    MyDB3: TmySQLDatabase;
    MyDB4: TmySQLDatabase;
    MyDB5: TmySQLDatabase;
    MyDB6: TmySQLDatabase;
    QrUpd_: TmySQLQuery;
    QrExEnti1: TmySQLQuery;
    QrExEnti1Codigo: TIntegerField;
    QrExEnti1Maior: TFloatField;
    QrExEnti1Menor: TFloatField;
    QrExEnti2: TmySQLQuery;
    QrExEnti2Codigo: TIntegerField;
    QrExEnti2Maior: TFloatField;
    QrExEnti2Menor: TFloatField;
    QrExEnti3: TmySQLQuery;
    IntegerField1: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    QrExEnti4: TmySQLQuery;
    IntegerField2: TIntegerField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    QrExEnti5: TmySQLQuery;
    IntegerField3: TIntegerField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    QrExEnti6: TmySQLQuery;
    IntegerField4: TIntegerField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    QrExEnti1AdVal: TFloatField;
    QrExEnti2AdVal: TFloatField;
    QrExEnti3AdVal: TFloatField;
    QrExEnti4AdVal: TFloatField;
    QrExEnti5AdVal: TFloatField;
    QrExEnti6AdVal: TFloatField;
    QrControleCHValAlto: TFloatField;
    QrControleDUValAlto: TFloatField;
    QrControleCHRisco: TFloatField;
    QrControleDURisco: TFloatField;
    QrEmLotIts: TmySQLQuery;
    QrEmLotItsCodigo: TIntegerField;
    QrEmLotItsControle: TIntegerField;
    QrEmLotItsTaxaPer: TFloatField;
    QrEmLotItsJuroPer: TFloatField;
    QrEmLotItsTaxaVal: TFloatField;
    QrControleDono: TIntegerField;
    QrDono: TmySQLQuery;
    QrDonoCodigo: TIntegerField;
    QrDonoCadastro: TDateField;
    QrDonoNOMEDONO: TWideStringField;
    QrDonoCNPJ_CPF: TWideStringField;
    QrDonoIE_RG: TWideStringField;
    QrDonoNIRE_: TWideStringField;
    QrDonoRUA: TWideStringField;
    QrDonoCOMPL: TWideStringField;
    QrDonoBAIRRO: TWideStringField;
    QrDonoCIDADE: TWideStringField;
    QrDonoNOMELOGRAD: TWideStringField;
    QrDonoNOMEUF: TWideStringField;
    QrDonoPais: TWideStringField;
    QrDonoLograd: TLargeintField;
    QrDonoTE1: TWideStringField;
    QrDonoFAX_TXT: TWideStringField;
    QrDonoCNPJ_TXT: TWideStringField;
    QrDonoE_LNR: TWideStringField;
    QrDonoNUMERO_TXT: TWideStringField;
    QrDonoECEP_TXT: TWideStringField;
    QrDonoTE1_TXT: TWideStringField;
    QrDonoFAX: TWideStringField;
    QrDonoCEP: TLargeintField;
    QrDonoENatal: TDateField;
    QrDonoPNatal: TDateField;
    QrDonoTipo: TSmallintField;
    QrDonoNATAL_TXT: TWideStringField;
    QrEndereco: TmySQLQuery;
    QrEnderecoCodigo: TIntegerField;
    QrEnderecoCadastro: TDateField;
    QrEnderecoNOMEDONO: TWideStringField;
    QrEnderecoCNPJ_CPF: TWideStringField;
    QrEnderecoIE_RG: TWideStringField;
    QrEnderecoNIRE_: TWideStringField;
    QrEnderecoRUA: TWideStringField;
    QrEnderecoCOMPL: TWideStringField;
    QrEnderecoBAIRRO: TWideStringField;
    QrEnderecoCIDADE: TWideStringField;
    QrEnderecoNOMELOGRAD: TWideStringField;
    QrEnderecoNOMEUF: TWideStringField;
    QrEnderecoPais: TWideStringField;
    QrEnderecoLograd: TLargeintField;
    QrEnderecoTipo: TSmallintField;
    QrEnderecoCEP: TLargeintField;
    QrEnderecoTE1: TWideStringField;
    QrEnderecoFAX: TWideStringField;
    QrEnderecoENatal: TDateField;
    QrEnderecoPNatal: TDateField;
    QrEnderecoECEP_TXT: TWideStringField;
    QrEnderecoNUMERO_TXT: TWideStringField;
    QrEnderecoE_ALL: TWideStringField;
    QrEnderecoCNPJ_TXT: TWideStringField;
    QrEnderecoFAX_TXT: TWideStringField;
    QrEnderecoTE1_TXT: TWideStringField;
    QrEnderecoNATAL_TXT: TWideStringField;
    QrControleAditAtu: TIntegerField;
    QrAditiv2: TmySQLQuery;
    QrAditiv2Texto: TWideMemoField;
    QrDonoE_CUC: TWideStringField;
    QrControleImportPath: TWideStringField;
    QrControleAdValMinVal: TFloatField;
    QrControleAdValMinTip: TSmallintField;
    QrControleFatorCompraMin: TFloatField;
    QrControleCalcMyJuro: TIntegerField;
    QrControleCartEspecie: TIntegerField;
    QrControleCartAtu: TIntegerField;
    QrControleProtAtu: TIntegerField;
    QrAditiv3: TmySQLQuery;
    QrAditiv4: TmySQLQuery;
    QrAditiv3Texto: TWideMemoField;
    QrAditiv4Texto: TWideMemoField;
    QrEnti: TmySQLQuery;
    QrEntiFatorCompra: TFloatField;
    QrMaster2SolicitaSenha: TSmallintField;
    QrMaster2Limite: TSmallintField;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrDonoEContato: TWideStringField;
    QrDonoECel: TWideStringField;
    QrDonoCEL_TXT: TWideStringField;
    QrControleTaxasAutom: TIntegerField;
    QrEnderecoRespons1: TWideStringField;
    QrControleIdleMinutos: TIntegerField;
    QrControleCorRecibo: TIntegerField;
    QrControleEnvelopeSacado: TSmallintField;
    QrDonoE_LN2: TWideStringField;
    QrControleColoreEnvelope: TSmallintField;
    QrSSit: TmySQLQuery;
    QrSSitSitSenha: TSmallintField;
    QrSenhas: TmySQLQuery;
    QrSenhasSenhaNew: TWideStringField;
    QrSenhasSenhaOld: TWideStringField;
    QrSenhaslogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasPerfil: TIntegerField;
    QrBSit: TmySQLQuery;
    QrBSitSitSenha: TSmallintField;
    QrBoss: TmySQLQuery;
    QrBossMasSenha: TWideStringField;
    QrBossMasLogin: TWideStringField;
    QrBossEm: TWideStringField;
    QrBossCNPJ: TWideStringField;
    QrBossMasZero: TWideStringField;
    QrControleMyPgParc: TIntegerField;
    QrControleMyPgQtdP: TIntegerField;
    QrControleMyPgPeri: TIntegerField;
    QrControleMyPgDias: TIntegerField;
    QrLocCPF: TmySQLQuery;
    DsLocCPF: TDataSource;
    QrEmLot: TmySQLQuery;
    QrEmLotCodigo: TIntegerField;
    QrEmLotAdValorem: TFloatField;
    QrEmLotValValorem: TFloatField;
    QrEmLotTaxaVal: TFloatField;
    QrEmLotIRRF: TFloatField;
    QrEmLotIRRF_Val: TFloatField;
    QrEmLotISS: TFloatField;
    QrEmLotISS_Val: TFloatField;
    QrEmLotPIS: TFloatField;
    QrEmLotPIS_Val: TFloatField;
    QrEmLotPIS_R: TFloatField;
    QrEmLotPIS_R_Val: TFloatField;
    QrEmLotCOFINS: TFloatField;
    QrEmLotCOFINS_Val: TFloatField;
    QrEmLotCOFINS_R: TFloatField;
    QrEmLotCOFINS_R_Val: TFloatField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLastEditLote: TDateField;
    QrControleContabIOF: TWideStringField;
    QrControleContabAdV: TWideStringField;
    QrControleContabFaC: TWideStringField;
    QrControleLogoNF: TWideStringField;
    frxDsEndereco: TfrxDBDataset;
    QrDonoAPELIDO: TWideStringField;
    QrEnderecoE_MIN: TWideStringField;
    QrControleCMC: TWideStringField;
    frxDsControle: TfrxDBDataset;
    QrControlemsnID: TWideStringField;
    QrControlemsnPW: TWideStringField;
    QrControlemsnCA: TIntegerField;
    QrControleNFLinha1: TWideStringField;
    QrControleNFLinha2: TWideStringField;
    QrControleNFLinha3: TWideStringField;
    QrControleNFAliq: TWideStringField;
    QrControleNFLei: TWideStringField;
    QrMSNsIts: TmySQLQuery;
    QrMSNsItsCodigo: TIntegerField;
    QrMSNsItsLinha: TIntegerField;
    QrMSNsItsTexto: TWideMemoField;
    QrMSNsItsRecebido: TFloatField;
    QrMSNs: TmySQLQuery;
    QrMSNsCodigo: TIntegerField;
    QrMSNsCliente: TIntegerField;
    QrMSNsRecebido: TFloatField;
    QrMSNsVersao: TWideStringField;
    QrMSNsID_Envio: TWideStringField;
    QrMSNsItens: TIntegerField;
    QrMSNsStatus: TIntegerField;
    QrMSNsUltimo: TIntegerField;
    QrMSNsDataV: TDateField;
    QrMSNsNomeInfo: TWideStringField;
    QrControleFTP_Pwd: TWideStringField;
    QrControleFTP_Hos: TWideStringField;
    QrControleFTP_Log: TWideStringField;
    QrControleFTP_Lix: TWideStringField;
    QrControleFTP_Min: TSmallintField;
    QrControleFTP_Aut: TSmallintField;
    QrCliente: TmySQLQuery;
    QrClientePastaTxtFTP: TWideStringField;
    QrClientePastaPwdFTP: TWideStringField;
    QrControleCPMF_Padrao: TSmallintField;
    QrControleddArqMorto: TIntegerField;
    QrLocCPFCPF: TWideStringField;
    QrLocCPFNome: TWideStringField;
    QrLocCPFLimite: TFloatField;
    QrLocCPFLastAtz: TDateField;
    QrLocCPFAcumCHComV: TFloatField;
    QrLocCPFAcumCHComQ: TIntegerField;
    QrLocCPFAcumCHDevV: TFloatField;
    QrLocCPFAcumCHDevQ: TIntegerField;
    QrControleNF_Cabecalho: TIntegerField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterLogo2: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterENumero: TIntegerField;
    QrMasterEBairro: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TSmallintField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroPNumero: TIntegerField;
    QrTerceiroCNumero: TIntegerField;
    QrTerceiroLNumero: TIntegerField;
    QrDonoNUMERO: TFloatField;
    QrEnderecoNUMERO: TFloatField;
    QrControleCSD_TopoCidata: TIntegerField;
    QrControleCSD_TopoDestin: TIntegerField;
    QrControleCSD_TopoDuplic: TIntegerField;
    QrControleCSD_TopoClient: TIntegerField;
    QrControleCSD_MEsqCidata: TIntegerField;
    QrControleCSD_MEsqDestin: TIntegerField;
    QrControleCSD_MEsqDuplic: TIntegerField;
    QrControleCSD_MEsqClient: TIntegerField;
    QrControleCSD_TopoDesti2: TIntegerField;
    QrControleCSD_MEsqDesti2: TIntegerField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleMinhaCompe: TIntegerField;
    QrControleOutraCompe: TIntegerField;
    QrControleCBEMinimo: TIntegerField;
    QrControleSBCPadrao: TIntegerField;
    QrMaster2Licenca: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrControlePathBBRet: TWideStringField;
    QrControleLiqOcoCDi: TSmallintField;
    QrControleLiqOcoCOc: TIntegerField;
    QrControleLiqOcoShw: TSmallintField;
    QrControleWeb_Page: TWideStringField;
    QrControleWeb_Host: TWideStringField;
    QrControleWeb_User: TWideStringField;
    QrControleWeb_Pwd: TWideStringField;
    QrControleWeb_DB: TWideStringField;
    QrControleWeb_FTPu: TWideStringField;
    QrControleWeb_FTPs: TWideStringField;
    QrControleWeb_FTPh: TWideStringField;
    MyDBn: TmySQLDatabase;
    QrControleWeb_MySQL: TSmallintField;
    QrWeb: TmySQLQuery;
    QrControleLiqStaAnt: TSmallintField;
    QrControleLiqStaVct: TSmallintField;
    QrControleLiqStaVcd: TSmallintField;
    QrSenhasFuncionario: TIntegerField;
    QrControleCartSacPe: TWideMemoField;
    QrControleColigEsp: TIntegerField;
    QrControleNewWebScan: TIntegerField;
    QrControleNewWebMins: TIntegerField;
    QrUpdZ: TmySQLQuery;
    QrControleIOF_PF: TFloatField;
    QrControleIOF_PJ: TFloatField;
    QrControleIOF_ME: TFloatField;
    QrControleIOF_Ex: TFloatField;
    QrControleIOF_Li: TFloatField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrDonoRespons1: TWideStringField;
    QrDonoRespons2: TWideStringField;
    frxDsAditiv4: TfrxDBDataset;
    frxDsMaster: TfrxDBDataset;
    frxDsAditiv3: TfrxDBDataset;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrControleLastBco: TIntegerField;
    QrControleVerBcoTabs: TIntegerField;
    QrControleLogoBig1: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrSenhasIP_Default: TWideStringField;
    QrUpd2: TmySQLQuery;
    QrControleTipNomeEmp: TSmallintField;
    QrEnderecoNO_TIPO_DOC: TWideStringField;
    QrControleCorrDtaVct: TIntegerField;
    QrControleCNPJ: TWideStringField;
    QrControleCartCheques: TIntegerField;
    QrControleCartDuplic: TIntegerField;
    QrControleCodCliRel: TIntegerField;
    QrControleIOF_F_PJ: TFloatField;
    QrControleIOF_D_PJ: TFloatField;
    QrControleIOF_Max_Per_PJ: TFloatField;
    QrControleIOF_Max_Usa_PJ: TSmallintField;
    QrControleIOF_F_PF: TFloatField;
    QrControleIOF_D_PF: TFloatField;
    QrControleIOF_Max_Per_PF: TFloatField;
    QrControleIOF_Max_Usa_PF: TSmallintField;
    QrMasterUsaAccMngr: TSmallintField;
    QrControleOrcaLFeed: TIntegerField;
    QrControleCartEmiCH: TIntegerField;
    frxDsAditiv5: TfrxDBDataset;
    QrAditiv5: TmySQLQuery;
    QrAditiv5Texto: TWideMemoField;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrControleKndJrs: TIntegerField;
    frxDsDono: TfrxDBDataset;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure TbMovCalcFields(DataSet: TDataSet);
    procedure TbMovBeforePost(DataSet: TDataSet);
    procedure TbMovBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DbPdxAfterDisconnect(Sender: TObject);
    procedure QrDonoCalcFields(DataSet: TDataSet);
    procedure QrEnderecoCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FProdDelete: Integer;
    FArrayMySQLBD: array[0..5] of TmySQLDatabase;
    FArrayMySQLEE: array[0..5] of TmySQLQuery;

    function  TabelasQueNaoQueroCriar(): String;
    procedure VerificaSenha(Index: Integer; Login, Senha: String);
    procedure ConectaMeuDB;
    procedure VerificaDefinicaoDeTextoPadrao(TipoDeTexto: Integer);
    procedure ReopenCliente(Cliente: Integer);

    function Privilegios(Usuario : Integer) : Boolean;
    function ConcatenaRegistros(Matricula: Integer): String;
    function ObtemExEntiMaior(Entidade: Integer): Double;
    function ObtemTaxaDeCompraCliente(Entidade: Integer): Double;
    function ObtemTaxaDeCompraRealizada(Controle: Integer): MyArrayR07;
    function ObtemValorDeCompraRealizado(Controle: Integer): MyArrayR07;
    function ObtemEmLotIts(Controle: Integer; tp0, jp0, tv0: Double):
             MyArray07ArrayR03;
    function ObtemValorAtualizado(Entidade, Quitado: Integer; Vencto, DataCalc,
             DtUltimoPg: TDateTime; Valor, Juros, Desco, Pago, Taxa: Double;
             JuroCliente: Boolean): Double;
    function ObtemValorAtualizado2(Entidade: Integer; Dias, Valor, Taxa:
             Double; TaxaCliente: Boolean): Double;
    function ObtemValoresDeLote(Codigo: Integer): MyArrayD02;
    function DatabaseColigadas(Indice: Integer): TmySQLDatabase;
    procedure InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
              DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
              CliInt: Integer; Campos: String; DescrSubstit: Boolean);
    function  FazProrrogacao(const FatParcela, Cliente: Integer; const
              Vencimento, DDeposito: TDateTime; Credito: Double;
              const NaoDeposita: Boolean; const NomeForm: String;
              const SQLType: TSQLType; var NovaData: TDateTime): Boolean;
    procedure ReopenControle();
    procedure RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
    function  CalculaJurosDesconto(TaxaM: Double; Prazo: Double): Double;
    procedure AtualizaStatusJuridico(Status, Controle: Integer);
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure ReopenDono();
  end;

var
  Dmod: TDmod;
  FVerifi: Boolean;
  //
  QvEntra3: TmySQLQuery;
  QvVenda3: TmySQLQuery;
  QvDevol3: TmySQLQuery;
  QvRecom3: TmySQLQuery;

implementation

uses UnMyObjects, Principal, SenhaBoss, Servidor, VerifiDB, MyDBCheck, ZCF2,
  InstallMySQL41, Credito_DMK, MyListas, BancosDados, ModuleGeral, VerifiDBy,
  PertoChek, ServerConnect, VerifiDBi, ModuleFin, ModuleTedC, VerifiDBTerceiros,
  ModuleLot, UnLic_Dmk, DmkDAC_PF;

{$R *.DFM}

procedure TDMod.DataModuleCreate(Sender: TObject);
var
  i, Versao, VerZero, Resp: Integer;
  BD, IP, PW, ID: String;
  Verifica, Existe, VerificaDBTerc: Boolean;
begin
  VerificaDBTerc := False;
  //
  VAR_GOTOMySQLDBNAME := MyDB;
  //
  MyDB.LoginPrompt := False;
  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;
  MyDBn.LoginPrompt := False;
  MyDB1.LoginPrompt := False;
  MyDB2.LoginPrompt := False;
  MyDB3.LoginPrompt := False;
  MyDB4.LoginPrompt := False;
  MyDB5.LoginPrompt := False;
  MyDB6.LoginPrompt := False;
  //
  Existe := False;
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  VAR_IP := Geral.ReadAppKey('IPServer', Application.Title, ktString,
    CO_VAZIO, HKEY_LOCAL_MACHINE);
  if not VAR_SERVIDOR3 in [1,2,3] then VAR_SERVIDOR3 := 2;
  if not (VAR_SERVIDOR in [1,2, VAR_SERVIDOR3]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end else if VAR_SERVIDOR = 3 then
  begin
    //Application.MessageBox('Servidor = 3 desabilitado!', 'Erro', MB_OK+MB_ICONERROR);
    //Exit;
    Application.CreateForm(TFmServerConnect, FmServerConnect);
    FmServerConnect.ShowModal;
    FmServerConnect.Destroy;
  end;
  if VAR_SERVIDOR = 0 then
  begin
    ZZTerminate := True;
    Application.Terminate;
    Exit;
  end;
  if not MLAGeral.VerificaServidorCliente then exit;

  /////////////////////////////////////////////////////////////////////////////

  FArrayMySQLBD[00] := MyDB1;
  FArrayMySQLBD[01] := MyDB2;
  FArrayMySQLBD[02] := MyDB3;
  FArrayMySQLBD[03] := MyDB4;
  FArrayMySQLBD[04] := MyDB5;
  FArrayMySQLBD[05] := MyDB6;
  //
  FArrayMySQLEE[00] := QrExEnti1;
  FArrayMySQLEE[01] := QrExEnti2;
  FArrayMySQLEE[02] := QrExEnti3;
  FArrayMySQLEE[03] := QrExEnti4;
  FArrayMySQLEE[04] := QrExEnti5;
  FArrayMySQLEE[05] := QrExEnti6;
  //
  if MyDB.Connected then
    Application.MessageBox('MyDB est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Application.MessageBox('MyLocDataBase est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyDBn.Connected then
    Application.MessageBox('MyDBn est� conectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  VAR_BDSENHA := 'wkljweryhvbirt';
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  //////////////////////////////////////////////////////////////////////////////
  if not dmkPF.ExecutavelEstaInstalado('MySQL Server') then Exit;
  if not GOTOy.OMySQLEstaInstalado(FmCredito_DMK.LaAviso1,
  FmCredito_DMK.LaAviso2, FmCredito_DMK.ProgressBar1) then
  begin
    //raise EAbort.Create('');
    //
    MyObjects.Informa2(FmCredito_DMK.LaAviso1, FmCredito_DMK.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    FmCredito_DMK.BtEntra.Visible := False;
    Exit;
  end;
  //////////////////////////////////////////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  //////////////////////////////////////////////////////////////////////////////
  ZZDB.DatabaseName := TMeuDB;
  ZZDB.UserPassword := VAR_BDSENHA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE user SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then ShowMessage('Banco de dados teste n�o se conecta!');
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  //
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Application.MessageBox(PChar('M�quina cliente sem rede.'), 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.MessageBox(PChar('Teste'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  //
  //  Conex�o na base de dados principal
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host := VAR_IP;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  //
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql', VAR_IP, VAR_PORTA,  VAR_SQLUSER,
  VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
  ConectaMeuDB;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'), 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      (*QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      MyDB.DatabaseName := TMeuDB;*)
      Application.CreateForm(TFmBancoDados, FmBancoDados);
      if FmBancoDados.EdPathMyDB.Enabled then FmBancoDados.EdPathMyDB.Focused;
      FmBancoDados.BtAbrir.Enabled := True;
      FmBancoDados.ShowModal;
      FmBancoDados.Destroy;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  UMyMod.AbreQuery(QrAuxL, MyLocDatabase);
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Application.MessageBox(PChar('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?'),
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Application.MessageBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;

  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_VERSAO then Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Application.MessageBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;

  //
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox(PChar('Imposs�vel criar Modulo de dados Geral!' + #13#10 +
    'Verifique se o Datamodule "MyPID_DB" j� est� ativo antes da cria��o!'),
    'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

  //

  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo Financeiro'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

  //  

  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    UMyMod.AbreQuery(QrMaster, MyDB);
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
        //
        VerificaDBTerc := True;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;

  if VerificaDBTerc then
  begin
    Application.CreateForm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros);
    FmVerifiDBTerceiros.FVerifi := True;
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.FVerifi := False;
    FmVerifiDBTerceiros.Destroy;
  end;

  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  MyList.ConfiguracoesIniciais(0, VAR_BaseDBName);
  with FmPrincipal do
  begin
    FConnections := 0;
    FMyDBs.Reload;
    for i := 0 to FMyDBs.MaxDBs-1 do
    begin
      //t := IntToStr(i+1);
      if (FMyDBs.BDName[i] <> '') (*and (FMyDBs.FolderExist[i] = '1')*) then
      begin
        FMyDBs.Connected[i] := '0';
        FArrayMySQLBD[i].DatabaseName := FMyDBs.BDName[i];
        FArrayMySQLBD[i].Host         := MyDB.Host;
        FArrayMySQLBD[i].UserPassword := MyDB.UserPassword;
        FArrayMySQLBD[i].Connected := False;
        try
          FArrayMySQLBD[i].Connected := True;
          FMyDBs.Connected[i] := '1';
          FConnections := FConnections + 1;
        except
          ;
        end;
        if Verifica and (FMyDBs.Connected[i] = '1') then
        begin
          Application.CreateForm(TFmVerifiDBy, FmVerifiDBy);
          with FmVerifiDBy do
          begin
            FMySQLDB := FArrayMySQLBD[i];
            BtSair.Enabled := False;
            FVerifi := True;
            ShowModal;
            FVerifi := False;
            Destroy;
          end;
        end;
      end;
    end;
    VAR_BDsEXTRAS     := ' :: '+IntToStr(FConnections);
    if VAR_STDATABASES <> nil then VAR_STDATABASES.Text :=
      Dmod.MyDB.DatabaseName+' :: '+Dmod.MyLocDataBase.DataBaseName+VAR_BDsEXTRAS;
    with FmCredito_DMK do
    begin
      if FConnections = 0 then
      begin
{
        EdLED.LED.ActiveColor :=   clRed;
        EdLED.LED.InActiveColor := clMaroon;
}
        LaTitulo1C.Font.Color := clRed;
        LaTitulo1A.Font.Color := clMaroon;
      end else begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SHOW DATABASES');
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
        while not Dmod.QrAux.Eof do
        begin
          // Verifica se pelo menos o primeiro banco de dados de coligado existe
          if Uppercase(Dmod.QrAux.FieldByName('Database').AsString ) =
          Uppercase(FArrayMySQLBD[0].DatabaseName) then
          begin
            Existe := True;
            Break;
          end else Dmod.QrAux.Next;
        end;
        //
        if not Existe then
        begin
          LaTitulo1C.Font.Color := $000080FF;
          LaTitulo1A.Font.Color := $00006464;
          FConnections := 0;
        end else begin
          // Verifica se h� tabelas no primeiro banco de dados de coligado
          Dmod.QrAux.Close;
          Dmod.QrAux.SQL.Clear;
          Dmod.QrAux.SQL.Add('SHOW TABLES FROM ' + FArrayMySQLBD[0].DatabaseName);
          UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
          if Dmod.QrAux.RecordCount = 0 then
          begin
            LaTitulo1C.Font.Color := clYellow;
            LaTitulo1A.Font.Color := $00006464;
            //
            FmPrincipal.VerificaBDsTerceiros();
            //
            Halt(0);
          end else begin
            LaTitulo1C.Font.Color := $004CB122; // verde normal
            LaTitulo1A.Font.Color := $001DE6B5; // verde limao
            //
            FmPrincipal.AGBEuroExport.Visible := True;
            FmPrincipal.AGBResultados1.Visible := True;
            FmPrincipal.AGBResultados2.Visible := True;
            FmPrincipal.AGBInadimplencia1.Visible := True;
            FmPrincipal.AGBInadimplencia2.Visible := True;
            FmPrincipal.AGBEvolucapi1.Visible := True;
            FmPrincipal.AGBEvolucapi2.Visible := True;
          end;
        end;
      end;
      LaTitulo1C.Caption := IntToStr(FConnections);
      LaTitulo1A.Caption := IntToStr(FConnections);
      LaTitulo1B.Caption := IntToStr(FConnections);
    end;
  end;
  // Conex�o na base de dados na url
  // S� pode ser ap�s abrir a tabela controle!
  // ver se est� ativado s� ent�o fazer !
  if (Dmod.QrControleWeb_MySQL.Value > 0) and
  not FmCredito_Dmk.CkNaoTemInternet.Checked then
  begin
    try
      MyObjects.Informa2(FmCredito_DMK.LaAviso1, FmCredito_DMK.LaAviso2, True,
        'Tentando conectar na base de dados remota');
      //
      IP := Dmod.QrControleWeb_Host.Value;
      ID := Dmod.QrControleWeb_User.Value;
      PW := Dmod.QrControleWeb_Pwd.Value;
      BD := Dmod.QrControleWeb_DB.Value;
      //
      if MyObjects.FIC(IP = '', nil, 'Host n�o definido!') then Exit;
      if MyObjects.FIC(ID = '', nil, 'Usu�rio n�o definido!') then Exit;
      if MyObjects.FIC(PW = '', nil, 'Senha n�o definida!') then Exit;
      if MyObjects.FIC(BD = '', nil, 'Banco de dados n�o definido!') then Exit;
      //
      MyDBn.Connected    := False;
      MyDBn.UserPassword := PW;
      MyDBn.UserName     := ID;
      MyDBn.Host         := IP;
      MyDBn.DatabaseName := BD; // J� deve existir (configurado pelo hospedeiro)
      MyDBn.Connected    := True;
      //
      if Dmod.QrControleWeb_MySQL.Value = 2 then
      begin
        MyDBn.Connect;
        if MyDBn.Connected and Verifica then
        begin
          Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
          with FmVerifiDbi do
          begin
            BtSair.Enabled := False;
            FVerifi := True;
            ShowModal;
            FVerifi := False;
            Destroy;
          end;
        end;
      end;
      MyObjects.Informa2(FmCredito_DMK.LaAviso1, FmCredito_DMK.LaAviso2, False,
        '...');
    except
      Geral.MensagemBox('N�o foi poss�vel conectar na base de dados remota!',
      'Aviso', MB_OK+MB_ICONWARNING);
      //
      MyObjects.Informa2(FmCredito_DMK.LaAviso1, FmCredito_DMK.LaAviso2, False,
      'N�o foi poss�vel conectar na base de dados remota!');

    end;
  end;

  //

  try
    Application.CreateForm(TDmTedC, DmTedC);
  except
    Application.MessageBox('Imposs�vel criar o m�dulo TED Cheque', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;

  //
  Lic_Dmk.LiberaUso5;
  if ZZTerminate then Exit;
end;

procedure TDmod.RecalcSaldoCarteira(Tipo, Carteira: Integer; Localiza: Byte);
var
  Saldo: Double;
begin
  if Tipo < 2 then
  begin
    QrSomaM.Close;
    QrSomaM.SQL.Clear;
    QrSomaM.SQL.Add('SELECT Inicial Valor FROM Carteiras');
    QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Codigo=:P1');
    QrSomaM.Params[0].AsInteger := Tipo;
    QrSomaM.Params[1].AsInteger := Carteira;
    QrSomaM.Open;
    Saldo := QrSomaMValor.Value;
  end else Saldo := 0;
  QrSomaM.Close;
  QrSomaM.SQL.Clear;
  if Tipo = 2 then begin
    QrSomaM.SQL.Add('SELECT SUM(IF(Sit=0, (Credito-Debito),');
    QrSomaM.SQL.Add('IF(Sit=1, (Credito-Debito+Pago), 0))) Valor FROM Lanctos');
  end else
    QrSomaM.SQL.Add('SELECT (SUM(Credito) - SUM(Debito)) Valor FROM Lanctos');
  QrSomaM.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
  QrSomaM.Params[0].AsInteger := Tipo;
  QrSomaM.Params[1].AsInteger := Carteira;
  QrSomaM.Open;
  Saldo := Saldo + QrSomaMValor.Value;
  QrSomaM.Close;

  QrUpdM.Close;
  QrUpdM.SQL.Clear;
  QrUpdM.SQL.Add('UPDATE Carteiras SET Saldo=:P0');
  QrUpdM.SQL.Add('WHERE Tipo=:P1 AND Codigo=:P2');
  QrUpdM.Params[0].AsFloat := Saldo;
  QrUpdM.Params[1].AsInteger := Tipo;
  QrUpdM.Params[2].AsInteger := Carteira;
  QrUpdM.ExecSQL;
(*  if Localiza = 3 then
  begin
    if Dmod.QrLanctos.State in [dsBrowse] then
    if (Dmod.QrLanctosTipo.Value = Tipo)
    and (Dmod.QrLanctosCarteira.Value = Carteira) then
    Localiza := 1;
    Dmod.DefParams;
  end;
  if Localiza = 1 then GOTOx.LocalizaCodigo(Carteira, Carteira);*)
end;

procedure TDMod.VerificaSenha(Index: Integer; Login, Senha: String);
begin
  if (Senha = VAR_BOSS) or (Senha = CO_MASTER) then VAR_SENHARESULT := 2
  else
  begin
    if Index = 7 then
    begin
(*      QrSenha.Close;
      QrSenha.Params[0].AsString := Login;
      QrSenha.Params[1].AsString := Senha;
      UMyMod.AbreQuery(QrSenha);
      if QrSenha.RecordCount > 0 then
      begin
        QrPerfis.Close;
        QrPerfis.Params[0].AsInteger := QrSenhaPerfilW.Value;
        UMyMod.AbreQuery(QrPerfis);
        if QrPerfisVendas.Value = 'V' then VAR_SENHARESULT := 2
        else MessageBox(0,'Acesso negado. Senha Inv�lida',
        'Permiss�o por senha', MB_OK+MB_ICONEXCLAMATION);
        QrPerfis.Close;
      end else ShowMessage('Login inv�lido.');
      QrSenha.Close;                      *)
    end else ShowMessage('Em constru��o');
  end;
end;

procedure TDMod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  ShowMessage('CNPJ n�o definido ou Empresa n�o definida. '+Chr(13)+Chr(10)+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  //
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
  //QrControle.Close;
end;

procedure TDMod.QrMasterCalcFields(DataSet: TDataSet);
begin
(*  case QrMasterTipo.Value of
    0: QrMasterEm.Value := QrMasterRazaoSocial.Value;
    1: QrMasterEm.Value := QrMasterNome.Value;
    else QrMasterEm.Value := QrMasterRazaoSocial.Value;
  end;*)
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.AtualizaStatusJuridico(Status, Controle: Integer);
begin
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE ' + CO_TabLctA + ' SET Juridico="' + Geral.FF0(Status) + '"',
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
  except
    Geral.MB_Erro('Falha ao atualizar status jur�dico!');
  end;
end;

function TDmod.CalculaJurosDesconto(TaxaM, Prazo: Double): Double;
begin
  Result := 0;
  case QrControleKndJrs.Value of
    0: Result := MLAGeral.CalculaJuroSimples(TaxaM, Prazo);
    1: Result := MLAGeral.CalculaJuroComposto(TaxaM, Prazo);
    else
    begin
      raise Exception.Create('Tipo de c�lculo de juros n�o implementado!');
    end;
  end;
end;

function TDmod.ConcatenaRegistros(Matricula: Integer): String;
(*var
  Registros, Conta: Integer;
  Liga: String;*)
begin
(*  Qr??.Close;
  Qr??.Params[0].AsInteger := Matricula;
  UMyMod.AbreQuery(Qr??);
  //
  Registros := MLAGeral.Registros(Qr??);
  if Registros > 1 then Result := 'Pagamento de '
  else if Registros = 1 then Result := 'Pagamento de ';
  Result := Result + Qr??NOMECURSO.Value;
  Qr??.Next;
  Conta := 1;
  Liga := ', ';
  while not Qr??.Eof do
  begin
    Conta := Conta + 1;
    if Conta = Registros then Liga := ' e ';
    Result := Result + Liga + Qr??NOMECURSO.Value;
    Qr?
    ?.Next;
  end;*)
end;


procedure TDMod.TbMovCalcFields(DataSet: TDataSet);
begin
(*  TbMovSUBT.Value := TbMovQtde.Value * TbMovPreco.Value;
  TbMovTOTA.Value := TbMovSUBT.Value - TbMovDesco.Value;
  case TbMovTipo.Value of
    -1: TbMovNOMETIPO.Value := 'Venda';
     1: TbMovNOMETIPO.Value := 'Compra';
    else TbMovNOMETIPO.Value := 'INDEFINIDO';
  end;
  case TbMovENTITIPO.Value of
    0: TbMovNOMEENTIDADE.Value := TbMovENTIRAZAO.Value;
    1: TbMovNOMEENTIDADE.Value := TbMovENTINOME.Value;
    else TbMovNOMEENTIDADE.Value := 'INDEFINIDO';
  end;
  if TbMovESTQQ.Value <> 0 then
  TbMovCUSTOPROD.Value := TbMovESTQV.Value / TbMovESTQQ.Value else
  TbMovCUSTOPROD.Value := 0;*)
end;

procedure TDMod.TbMovBeforePost(DataSet: TDataSet);
begin
(*  case TbMovTipo.Value of
   -1:
    begin
      TbMovTotalV.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
      TbMovTotalC.Value := TbMovQtde.Value * TbMovCUSTOPROD.Value;
    end;
    1:
    begin
      TbMovTotalV.Value := 0;
      TbMovTotalC.Value := (TbMovQtde.Value * TbMovPreco.Value) - TbMovDesco.Value;
    end;
    else begin
      //
    end;
  end;*)
end;

function TDmod.TabelasQueNaoQueroCriar(): String;
begin
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  Result := Lowercase('"' + TAB_LOT + '", "' + TAB_LOT +
  'its", "lotez", "lotezits", "' + TAB_LOT + 'txs", "' + CO_TabOcoP + '", "' +
  CO_TabPgCH + '", "' + CO_TabPgDU + '","' + LAN_CTOS + '"');
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
  // lanctoz deixar criar para colocar exclu�dos!!!!!!!!!!!!!
end;

procedure TDMod.TbMovBeforeDelete(DataSet: TDataSet);
begin
  //FProdDelete := TbMovProduto.Value;
end;

procedure TDMod.DataModuleDestroy(Sender: TObject);
begin
  // 2011-10-11 - Desabilitei
  //WSACleanup;
  // Fim 2011-10-11 
end;

function TDmod.Privilegios(Usuario : Integer) : Boolean;
begin
  Result := False;
  FM_MASTER := 'F';
  QrPerfis.Params[0].AsInteger := Usuario;
  UMyMod.AbreQuery(QrPerfis, MyDB);
  if QrPerfis.RecordCount > 0 then
  begin
    Result := True;
    //FM_EQUIPAMENTOS             := QrPerfisEquipamentos.Value;
    //FM_MARCAS          := QrPerfisMarcas.Value;
    //FM_MODELOS                := QrPerfisModelos.Value;
    //FM_SERVICOS            := QrPerfisServicos.Value;
    //FM_PRODUTOS            := QrPerfisProdutos.Value;
  end;
  QrPerfis.Close;
end;

procedure TDMod.DbPdxAfterDisconnect(Sender: TObject);
begin
  ShowMessage('Paradox desconectou!');
end;

function TDmod.FazProrrogacao(const FatParcela, Cliente: Integer; const
  Vencimento, DDeposito: TDateTime; Credito: Double;
  const NaoDeposita: Boolean; const NomeForm: String;
  const SQLType: TSQLType; var NovaData: TDateTime): Boolean;

begin
  Result := DmLot.FazProrrogacao(FatParcela, Cliente, Vencimento, DDeposito,
  Credito, NaoDeposita, NomeForm, SQLType, NovaData);
end;

procedure TDMod.ConectaMeuDB;
var
  BD: String;
begin
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  UMyMod.AbreQuery(QrAux, MyDB);
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
end;

function TDMod.ObtemValorAtualizado(Entidade, Quitado: Integer; Vencto, DataCalc,
DtUltimoPg: TDateTime; Valor, Juros, Desco, Pago, Taxa: Double; JuroCliente: Boolean): Double;
var
  Praz, Juro, Real: Double;
  DataU: TDateTime;
begin
  Real := Valor + Juros - Desco - Pago;
  if Quitado < 2 then // n�o vai calcular pago a maior
  begin
    if DataCalc > Vencto then
    begin
      if DataCalc > DtUltimoPg then
      begin
        if DtUltimoPg > 1 then DataU := DtUltimoPg
        else DataU := Vencto;
        if DataU < 3 then
        begin
          // Parei Aqui - Erro ver por que
          DataU := Vencto;
        end;
        if DataU = 0 then
          Praz := 0
        else
          Praz := Trunc(DataCalc - DataU);
        if JuroCliente then Taxa := Dmod.ObtemTaxaDeCompraCliente(Entidade);
        Juro := MLAGeral.CalculaJuroComposto(Taxa, Praz);
        try
          // mudar para:
          //Result := Geral.RoundC((Real * (100 + Juro)) / 100, 2)
          Result := Trunc(Real * (100+Juro)+0.5)/100;
        except
          Result := Real;
        end;
      end else Result := Real;
    end else Result := Real;
  end else Result := Real;
end;

function TDMod.ObtemValorAtualizado2(Entidade: Integer; Dias, Valor, Taxa:
 Double; TaxaCliente: Boolean): Double;
var
  Juro: Double;
begin
  Taxa := Dmod.ObtemTaxaDeCompraCliente(Entidade);
  Juro := MLAGeral.CalculaJuroComposto(Taxa, Dias);
  Result := Trunc(Valor * (100 + Juro) + 0.5) / 100;
end;

function TDMod.ObtemTaxaDeCompraCliente(Entidade: Integer): Double;
begin
  QrEnti.Close;
  QrEnti.Params[0].AsInteger := Entidade;
  UMyMod.AbreQuery(QrEnti, MyDB);
  //
  Result := QrEntiFatorCompra.Value + ObtemExEntiMaior(Entidade);
end;

function TDMod.ObtemExEntiMaior(Entidade: Integer): Double;
var
  i: Integer;
  v: Double;
begin
  v := 0;
  //FmPrincipal.FMyDBs.Reload;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs-1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      FArrayMySQLEE[i].Close;
      FArrayMySQLEE[i].Params[0].AsInteger := Entidade;
      UMyMod.AbreQuery(FArrayMySQLEE[i], FArrayMySQLEE[i].Database);
      v := v + FArrayMySQLEE[i].FieldByName('Maior').AsFloat;
    end;
  end;
  Result := v;
end;

function TDMod.ObtemTaxaDeCompraRealizada(Controle: Integer): MyArrayR07;
var
  i, j: Integer;
  v: Double;
begin
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  v := 0;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLotIts.Close;
      QrEmLotIts.Database := Dmod.FArrayMySQLBD[i];
      QrEmLotIts.Params[0].AsInteger := Controle;
      UMyMod.AbreQuery(QrEmLotIts, Dmod.FArrayMySQLBD[i]);
      Result[i+1] := QrEmLotItsTaxaPer.Value;
      v := v + QrEmLotItsTaxaPer.Value;
    end else Result[i+1] := 0;
  end;
  Result[0] := v;
  for i := j+1 to 5 do Result[i+1] := 0;
end;

function TDMod.ObtemValorDeCompraRealizado(Controle: Integer): MyArrayR07;
var
  i, j: Integer;
  v: Double;
begin
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  v := 0;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLotIts.Close;
      QrEmLotIts.Database := Dmod.FArrayMySQLBD[i];
      QrEmLotIts.Params[0].AsInteger := Controle;
      UMyMod.AbreQuery(QrEmLotIts, Dmod.FArrayMySQLBD[i]);
      Result[i+1] := QrEmLotItsTaxaVal.Value;
      v := v + QrEmLotItsTaxaVal.Value;
    end else Result[i+1] := 0;
  end;
  Result[0] := v;
  for i := j+1 to 5 do Result[i+1] := 0;
end;

function TDMod.ObtemValoresDeLote(Codigo: Integer): MyArrayD02;
var
  i, j: Integer;
  v: Double;
begin
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  v := 0;
  Result[1] := 0;
  Result[2] := 0;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLot.Close;
      QrEmLot.Database := Dmod.FArrayMySQLBD[i];
      QrEmLot.Params[0].AsInteger := Codigo;
      UMyMod.AbreQuery(QrEmLot, Dmod.FArrayMySQLBD[i]);
      Result[1] := Result[1] + QrEmLotTaxaVal.Value;
      Result[2] := Result[2] + QrEmLotAdValorem.Value;
      v := v + QrEmLotTaxaVal.Value;
    end else Result[i+1] := 0;
  end;
end;

function TDMod.ObtemEmLotIts(Controle: Integer; tp0, jp0, tv0: Double):
  MyArray07ArrayR03;
var
  i, j: Integer;
  tp, jp, tv: Double;
begin
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  tp := 0;
  jp := 0;
  tv := 0;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLotIts.Close;
      QrEmLotIts.Database := Dmod.FArrayMySQLBD[i];
      QrEmLotIts.Params[0].AsInteger := Controle;
      UMyMod.AbreQuery(QrEmLotIts, Dmod.FArrayMySQLBD[i]);
      tp := tp + QrEmLotItsTaxaPer.Value;
      jp := jp + QrEmLotItsJuroPer.Value;
      tv := tv + QrEmLotItsTaxaVal.Value;
      Result[i+1][0] := QrEmLotItsTaxaPer.Value;
      Result[i+1][1] := QrEmLotItsJuroPer.Value;
      Result[i+1][2] := QrEmLotItsTaxaVal.Value;
    end else
    begin
       Result[i+1][0] := 0;
       Result[i+1][1] := 0;
       Result[i+1][2] := 0;
    end;
  end;
  Result[0][0] := tp+tp0;
  Result[0][1] := jp+jp0;
  Result[0][2] := tv+tv0;
  for i := j+1 to 5 do
  begin
    Result[i+1][0] := 0;
    Result[i+1][1] := 0;
    Result[i+1][2] := 0;
  end;
end;

procedure TDMod.QrDonoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrDonoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoTe1.Value);
  QrDonoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoFax.Value);
  QrDonoCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrDonoECel.Value);
  QrDonoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrDonoCNPJ_CPF.Value);
  //
  QrDonoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrDonoRUA.Value, Trunc(QrDonoNumero.Value), False);
  QrDonoE_LNR.Value := QrDonoNOMELOGRAD.Value;
  if Trim(QrDonoE_LNR.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ';
  QrDonoE_LNR.Value := QrDonoE_LNR.Value + QrDonoRua.Value;
  if Trim(QrDonoRua.Value) <> '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ', ' + QrDonoNUMERO_TXT.Value;
  if Trim(QrDonoCompl.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' ' + QrDonoCompl.Value;
  if Trim(QrDonoBairro.Value) <>  '' then QrDonoE_LNR.Value :=
    QrDonoE_LNR.Value + ' - ' + QrDonoBairro.Value;
  //
  QrDonoE_LN2.Value := '';
  if Trim(QrDonoCidade.Value) <>  '' then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + ' - '+QrDonoCIDADE.Value;
  QrDonoE_LN2.Value := QrDonoE_LN2.Value + ' - '+QrDonoNOMEUF.Value;
  if QrDonoCEP.Value > 0 then QrDonoE_LN2.Value :=
    QrDonoE_LN2.Value + '  CEP ' + Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  QrDonoE_CUC.Value := QrDonoE_LNR.Value + QrDonoE_LN2.Value;
  //
  QrDonoECEP_TXT.Value := Geral.FormataCEP_NT(QrDonoCEP.Value);
  //
  if QrDonoTipo.Value = 0 then Natal := QrDonoENatal.Value
  else Natal := QrDonoPNatal.Value;
  if Natal < 2 then QrDonoNATAL_TXT.Value := ''
  else QrDonoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.QrEnderecoCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrEnderecoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoTe1.Value);
  QrEnderecoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrEnderecoFax.Value);
  QrEnderecoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEnderecoCNPJ_CPF.Value);
  //
  QrEnderecoNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrEnderecoRUA.Value, Trunc(QrEnderecoNumero.Value), False);
  QrEnderecoE_ALL.Value := QrEnderecoNOMELOGRAD.Value;
  if Trim(QrEnderecoE_ALL.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ';
  QrEnderecoE_ALL.Value := QrEnderecoE_ALL.Value + QrEnderecoRua.Value;
  if Trim(QrEnderecoRua.Value) <> '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNUMERO_TXT.Value;
  if Trim(QrEnderecoCompl.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' ' + QrEnderecoCompl.Value;
    //
  QrEnderecoE_MIN.Value := QrEnderecoE_ALL.Value;
    //
  if Trim(QrEnderecoBairro.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', Bairro ' + QrEnderecoBairro.Value;
  if QrEnderecoCEP.Value > 0 then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - CEP ' +Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  if Trim(QrEnderecoCidade.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ' - ' + QrEnderecoCidade.Value;
  if Trim(QrEnderecoNOMEUF.Value) <>  '' then QrEnderecoE_ALL.Value :=
    QrEnderecoE_ALL.Value + ', ' + QrEnderecoNOMEUF.Value;
  //
  QrEnderecoECEP_TXT.Value :=Geral.FormataCEP_NT(QrEnderecoCEP.Value);
  //
  if QrEnderecoTipo.Value = 0 then
  begin
    Natal := QrEnderecoENatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CNPJ';
  end else begin
    Natal := QrEnderecoPNatal.Value;
    QrEnderecoNO_TIPO_DOC.Value := 'CPF';
  end;
  if Natal < 2 then QrEnderecoNATAL_TXT.Value := ''
  else QrEnderecoNATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FmPrincipal.FEntInt := QrControleDono.Value;
  FmPrincipal.TimerIdle.Interval := QrControleIdleMinutos.Value * 60000;
  //
  FmPrincipal.FImportPath := QrControleImportPath.Value;
  //
  QrAditiv2.Close;
  QrAditiv2.Params[0].AsInteger := QrControleAditAtu.Value;
  UMyMod.AbreQuery(QrAditiv2, MyDB);
  //
  QrAditiv3.Close;
  QrAditiv3.Params[0].AsInteger := QrControleProtAtu.Value;
  UMyMod.AbreQuery(QrAditiv3, MyDB);
  //
  QrAditiv4.Close;
  QrAditiv4.Params[0].AsInteger := QrControleCartAtu.Value;
  UMyMod.AbreQuery(QrAditiv4, MyDB);
  //
  QrAditiv5.Close;
  QrAditiv5.Params[0].AsInteger := QrControleCartEmiCH.Value;
  UMyMod.AbreQuery(QrAditiv5, MyDB);
  //
{
  QrDono.Close;
  UMyMod.AbreQuery(QrDono);
}

  //
  VAR_MEULOGOPATH := QrControleMeuLogoPath.Value;
  if FileExists(VAR_MEULOGOPATH) then VAR_MEULOGOEXISTE := True
  else VAR_MEULOGOEXISTE := False;
  //
  VAR_LOGONFPATH := QrControleLogoNF.Value;
  if FileExists(VAR_LOGONFPATH) then VAR_LOGONFEXISTE := True
  else VAR_LOGONFEXISTE := False;
  //
  VAR_DBWEB := QrControleWeb_DB.Value;
  //
  (*if QrControleNewWebScan.Value = 1 then
  begin
    FmPrincipal.Timer5.Interval := QrControleNewWebMins.Value * 60000;
    FmPrincipal.Timer5.Enabled := MyDBn.Connected;
  end;*)
end;

procedure TDmod.VerificaDefinicaoDeTextoPadrao(TipoDeTexto: Integer);
var
  Texto: String;
  Item: Integer;
begin
  case TipoDeTexto of
    2: Item := QrControleAditAtu.Value;
    3: Item := QrControleProtAtu.Value;
    4: Item := QrControleCartAtu.Value;
    5: Item := QrControleCartEmiCH.Value;
    else Item := 0;
  end;
  if Item < 1 then
  begin
    case TipoDeTexto of
      2: Texto := 'o aditivo';
      3: Texto := 'a autoriza��o de protesto';
      4: Texto := 'a carta ao sacado';
      5: Texto := 'a carta ao emitente do cheque';
      else Texto := '????????';
    end;
    Application.MessageBox(PChar('N�o foi definido o texto padr�o para '+
      Texto+'!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmod.ReopenCliente(Cliente: Integer);
begin
  QrCliente.Close;
  QrCliente.Params[0].AsInteger := Cliente;
  UMyMod.AbreQuery(QrCliente, MyDB);
end;

procedure TDmod.ReopenControle();
begin
  QrControle.Close;
  //QrControle.Params[0].AsString := CO_USERSPNOW;
  UMyMod.AbreQuery(QrControle, MyDB);
end;

procedure TDmod.ReopenDono;
begin
 UnDmkDAC_PF.AbreMySQLQuery0(QrDono, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, ECel, ',
    'Respons1, Respons2, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome    END NOMEDONO, ',
    'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido  END APELIDO, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF     END CNPJ_CPF, ',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG      END IE_RG, ',
    'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""         END NIRE_, ',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua    END RUA, ',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END + 0.000 NUMERO, ',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl  END COMPL, ',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIRRO, ',
    'IF(mu.Nome <> "", mu.Nome, CASE WHEN en.Tipo=0 THEN en.ECidade ELSE en.PCidade END) CIDADE, ',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOMELOGRAD, ',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOMEUF, ',
    'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais, ',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END + 0.000 Lograd, ',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END + 0.000 CEP, ',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1, ',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX ',
    'FROM controle co ',
    'LEFT JOIN entidades en ON en.Codigo=co.Dono ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    '']);
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  //Compatibilidade
end;

function TDmod.DatabaseColigadas(Indice: Integer): TmySQLDatabase;
begin
  case Indice of
    0: Result := MyDB1;
    1: Result := MyDB2;
    2: Result := MyDB3;
    3: Result := MyDB4;
    4: Result := MyDB5;
    5: Result := MyDB6;
    else Result := nil;
  end;
end;

procedure TDmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01: TmySQLQuery;
DataIDate, DataFDate: TDateTime; DataITrue, DataFTrue: Boolean;
CliInt: Integer; Campos: String; DescrSubstit: Boolean);
begin
  Application.MessageBox('Este balancete n�o possui dados industriais ' +
  'adicionais neste aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

end.

