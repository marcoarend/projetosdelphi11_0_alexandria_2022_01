object FmOpcoesCreditoX: TFmOpcoesCreditoX
  Left = 283
  Top = 181
  Caption = 'FER-OPCAO-002 :: Op'#231#245'es Espec'#237'ficas do Aplicativo'
  ClientHeight = 696
  ClientWidth = 808
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 808
    Height = 534
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 806
      Height = 532
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Border'#244
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 798
          Height = 504
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object EdCPMF: TdmkEdit
            Left = 200
            Top = 4
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdIOF_ME: TdmkEdit
            Left = 200
            Top = 52
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object RGAdValorem: TRadioGroup
            Left = 576
            Top = 44
            Width = 205
            Height = 40
            Caption = ' Ad Valorem padr'#227'o: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Percentual (%)'
              'Valor fixo ($)')
            TabOrder = 24
          end
          object RGTipoPrazoDesc: TRadioGroup
            Left = 576
            Top = 85
            Width = 205
            Height = 58
            Caption = ' Forma de c'#225'lculo de vencimento: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Normal'
              'N'#227'o pula'
              'Pula n'#227'o '#250'teis')
            TabOrder = 25
          end
          object EdAdValoremV: TdmkEdit
            Left = 480
            Top = 76
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 15
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdAdValoremP: TdmkEdit
            Left = 480
            Top = 100
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 16
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdRegiaoCompe: TdmkEdit
            Left = 480
            Top = 148
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 18
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdPIS_R: TdmkEdit
            Left = 200
            Top = 268
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdPIS: TdmkEdit
            Left = 200
            Top = 244
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdCOFINS_R: TdmkEdit
            Left = 200
            Top = 220
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdCOFINS: TdmkEdit
            Left = 200
            Top = 196
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdISS: TdmkEdit
            Left = 200
            Top = 172
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdChequeMaior: TdmkEdit
            Left = 200
            Top = 148
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdCHValAlto: TdmkEdit
            Left = 480
            Top = 4
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText1: TStaticText
            Left = 296
            Top = 6
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Alerta cheque valor alto'
            ParentBiDiMode = False
            TabOrder = 28
          end
          object StaticText2: TStaticText
            Left = 296
            Top = 30
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Alerta duplicata valor alto'
            ParentBiDiMode = False
            TabOrder = 29
          end
          object EdDUValAlto: TdmkEdit
            Left = 480
            Top = 28
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 13
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText3: TStaticText
            Left = 16
            Top = 6
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' CPMF (%)'
            ParentBiDiMode = False
            TabOrder = 30
          end
          object StaticText4: TStaticText
            Left = 16
            Top = 54
            Width = 181
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' IOF pessoa jur'#237'dica ME- EPP (%ano)'
            ParentBiDiMode = False
            TabOrder = 31
          end
          object StaticText5: TStaticText
            Left = 16
            Top = 150
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Menor cheque Maior ($)'
            ParentBiDiMode = False
            TabOrder = 32
          end
          object StaticText6: TStaticText
            Left = 16
            Top = 174
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' ISS (%)'
            ParentBiDiMode = False
            TabOrder = 33
          end
          object StaticText7: TStaticText
            Left = 16
            Top = 198
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' COFINS (%)'
            ParentBiDiMode = False
            TabOrder = 34
          end
          object StaticText8: TStaticText
            Left = 16
            Top = 222
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' COFINS (%) a recolher'
            ParentBiDiMode = False
            TabOrder = 35
          end
          object StaticText9: TStaticText
            Left = 16
            Top = 246
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' PIS (%)'
            ParentBiDiMode = False
            TabOrder = 36
          end
          object StaticText10: TStaticText
            Left = 16
            Top = 270
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' PIS (%) a recolher'
            ParentBiDiMode = False
            TabOrder = 37
          end
          object StaticText11: TStaticText
            Left = 296
            Top = 150
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Regi'#227'o de compensa'#231#227'o (cheques)'
            ParentBiDiMode = False
            TabOrder = 38
          end
          object StaticText12: TStaticText
            Left = 296
            Top = 54
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Ad Valorem (%)'
            ParentBiDiMode = False
            TabOrder = 39
          end
          object StaticText13: TStaticText
            Left = 296
            Top = 78
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Ad Valorem ($)'
            ParentBiDiMode = False
            TabOrder = 40
          end
          object StaticText14: TStaticText
            Left = 296
            Top = 102
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Risco emitente cheque (padr'#227'o)'
            ParentBiDiMode = False
            TabOrder = 41
          end
          object StaticText15: TStaticText
            Left = 296
            Top = 126
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Risco sacado (padr'#227'o)'
            ParentBiDiMode = False
            TabOrder = 42
          end
          object EdDURisco: TdmkEdit
            Left = 480
            Top = 124
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 17
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdCHRisco: TdmkEdit
            Left = 480
            Top = 52
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 14
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object GroupBox1: TGroupBox
            Left = 16
            Top = 316
            Width = 365
            Height = 73
            Caption = ' Cobran'#231'a m'#237'nima: '
            TabOrder = 27
            object StaticText16: TStaticText
              Left = 8
              Top = 22
              Width = 120
              Height = 17
              AutoSize = False
              BiDiMode = bdLeftToRight
              BorderStyle = sbsSunken
              Caption = ' Ad Valorem (% ou $)'
              ParentBiDiMode = False
              TabOrder = 3
            end
            object StaticText17: TStaticText
              Left = 8
              Top = 46
              Width = 120
              Height = 17
              AutoSize = False
              BiDiMode = bdLeftToRight
              BorderStyle = sbsSunken
              Caption = ' Fator de Compra (%)'
              ParentBiDiMode = False
              TabOrder = 4
            end
            object EdFatorCompraMin: TdmkEdit
              Left = 133
              Top = 44
              Width = 92
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdAdValMinVal: TdmkEdit
              Left = 133
              Top = 20
              Width = 92
              Height = 21
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object RGAdValMinTip: TRadioGroup
              Left = 230
              Top = 15
              Width = 133
              Height = 56
              Align = alRight
              Caption = ' Ad Valorem padr'#227'o: '
              ItemIndex = 0
              Items.Strings = (
                'Percentual (%)'
                'Valor fixo ($)')
              TabOrder = 2
            end
          end
          object RGCalcMyJuro: TRadioGroup
            Left = 576
            Top = 4
            Width = 205
            Height = 40
            Caption = ' Calculo da minha taxa de juro: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Precisa'
              'Rateio')
            TabOrder = 23
          end
          object RGImpreCheque: TRadioGroup
            Left = 384
            Top = 316
            Width = 133
            Height = 73
            Caption = ' Impressora de Cheques: '
            ItemIndex = 0
            Items.Strings = (
              'Nenhuma (ou Texto)'
              'PertoChek Paralela'
              'PertoChek Serial')
            TabOrder = 43
            OnClick = RGImpreChequeClick
          end
          object RGTaxasAutom: TRadioGroup
            Left = 520
            Top = 316
            Width = 129
            Height = 73
            Caption = ' Taxas autom'#225'ticas: '
            ItemIndex = 0
            Items.Strings = (
              'Cadastro das taxas'
              'Cadastro do cliente')
            TabOrder = 44
          end
          object CkEnvelopeSacado: TCheckBox
            Left = 264
            Top = 392
            Width = 213
            Height = 17
            Caption = 'Imprime envelope para carta ao sacado.'
            TabOrder = 45
          end
          object CkColoreEnvelope: TCheckBox
            Left = 484
            Top = 392
            Width = 269
            Height = 17
            Caption = 'Colore envelope para carta ao sacado.'
            TabOrder = 46
          end
          object EdCMC: TdmkEdit
            Left = 480
            Top = 220
            Width = 93
            Height = 21
            TabOrder = 21
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object CkCPMF_Padrao: TCheckBox
            Left = 16
            Top = 392
            Width = 241
            Height = 17
            Caption = 'Utiliza o CPMF padr'#227'o para todos os clientes.'
            TabOrder = 47
          end
          object StaticText18: TStaticText
            Left = 296
            Top = 222
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = 'Meu C.M.C.'
            ParentBiDiMode = False
            TabOrder = 48
          end
          object StaticText19: TStaticText
            Left = 296
            Top = 174
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = 'Dias padr'#227'o comp. minha regi'#227'o'
            ParentBiDiMode = False
            TabOrder = 49
          end
          object EdMinhaCompe: TdmkEdit
            Left = 480
            Top = 172
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 19
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText20: TStaticText
            Left = 296
            Top = 198
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = 'Dias padr'#227'o comp. outras regi'#245'es'
            ParentBiDiMode = False
            TabOrder = 50
          end
          object EdOutraCompe: TdmkEdit
            Left = 480
            Top = 196
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 20
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText21: TStaticText
            Left = 296
            Top = 246
            Width = 181
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = 'CBE padr'#227'o m'#237'nimo:'
            ParentBiDiMode = False
            TabOrder = 51
          end
          object EdCBEMinimo: TdmkEdit
            Left = 480
            Top = 244
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 22
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object RGSBCPadrao: TRadioGroup
            Left = 576
            Top = 144
            Width = 205
            Height = 40
            Caption = ' Compensa'#231#227'o banc'#225'ria padr'#227'o: '
            Columns = 3
            ItemIndex = 2
            Items.Strings = (
              'Sim'
              'N'#227'o'
              'Cadastro')
            TabOrder = 26
          end
          object StaticText22: TStaticText
            Left = 16
            Top = 78
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' IOF pessoa jur'#237'dica outros (% ano)'
            ParentBiDiMode = False
            TabOrder = 52
          end
          object EdIOF_PJ: TdmkEdit
            Left = 200
            Top = 76
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText23: TStaticText
            Left = 16
            Top = 102
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' IOF pessoa fisica (% ano)'
            ParentBiDiMode = False
            TabOrder = 53
          end
          object EdIOF_PF: TdmkEdit
            Left = 200
            Top = 100
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText24: TStaticText
            Left = 16
            Top = 126
            Width = 180
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' IOF adicional (%)'
            ParentBiDiMode = False
            TabOrder = 54
          end
          object EdIOF_Ex: TdmkEdit
            Left = 200
            Top = 124
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object StaticText25: TStaticText
            Left = 16
            Top = 30
            Width = 181
            Height = 17
            AutoSize = False
            BiDiMode = bdLeftToRight
            BorderStyle = sbsSunken
            Caption = ' Limite IOF reduzido para ME-EPP ($)'
            ParentBiDiMode = False
            TabOrder = 55
          end
          object EdIOF_Li: TdmkEdit
            Left = 200
            Top = 28
            Width = 92
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object RGTipNomeEmp: TRadioGroup
            Left = 576
            Top = 185
            Width = 205
            Height = 58
            Caption = 'Nome da empresa no border'#244
            ItemIndex = 0
            Items.Strings = (
              'Raz'#227'o Social'
              'Nome Fantasia')
            TabOrder = 56
          end
          object RGCodCliRel: TRadioGroup
            Left = 576
            Top = 244
            Width = 205
            Height = 40
            Caption = 'C'#243'digo do cliente nos relat'#243'rios'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 57
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Miscel'#226'nea'
        ImageIndex = 1
        object Label2: TLabel
          Left = 8
          Top = 8
          Width = 151
          Height = 13
          Caption = 'Carteira para pagamento r'#225'pido:'
        end
        object Label3: TLabel
          Left = 8
          Top = 48
          Width = 195
          Height = 13
          Caption = 'Texto padr'#227'o de autoriza'#231#227'o de protesto:'
        end
        object Label75: TLabel
          Left = 8
          Top = 92
          Width = 115
          Height = 13
          Caption = 'Texto padr'#227'o de aditivo:'
        end
        object Label4: TLabel
          Left = 8
          Top = 132
          Width = 169
          Height = 13
          Caption = 'Carta padr'#227'o para aviso ao sacado:'
        end
        object Label1: TLabel
          Left = 432
          Top = 8
          Width = 353
          Height = 13
          Caption = 
            'Caminho padr'#227'o dos arquivos de importa'#231#227'o de cheques e duplicata' +
            's [F4]:'
        end
        object Label5: TLabel
          Left = 432
          Top = 48
          Width = 310
          Height = 13
          Caption = 
            'Caminho do logo (m'#225'x. 64 X 168) para relat'#243'rios espec'#237'ficos [F4]' +
            ': '
        end
        object Label15: TLabel
          Left = 432
          Top = 92
          Width = 136
          Height = 13
          Caption = 'N.F. - Texto sobre o imposto.'
        end
        object Label16: TLabel
          Left = 432
          Top = 132
          Width = 160
          Height = 13
          Caption = 'N.F. Texto sobre a lei (permiss'#227'o):'
        end
        object Label12: TLabel
          Left = 8
          Top = 216
          Width = 146
          Height = 13
          Caption = 'Linha 1 - Texto descri'#231#227'o N.F.:'
        end
        object Label13: TLabel
          Left = 8
          Top = 256
          Width = 146
          Height = 13
          Caption = 'Linha 2 - Texto descri'#231#227'o N.F.:'
        end
        object Label14: TLabel
          Left = 8
          Top = 296
          Width = 146
          Height = 13
          Caption = 'Linha 3 - Texto descri'#231#227'o N.F.:'
        end
        object Label44: TLabel
          Left = 8
          Top = 336
          Width = 86
          Height = 13
          Caption = 'Coligado especial:'
        end
        object Label9: TLabel
          Left = 8
          Top = 380
          Width = 202
          Height = 13
          Caption = 'Carteira padr'#227'o para border'#244's de cheques:'
        end
        object Label47: TLabel
          Left = 400
          Top = 380
          Width = 209
          Height = 13
          Caption = 'Carteira padr'#227'o para border'#244's de duplicatas:'
        end
        object Label53: TLabel
          Left = 8
          Top = 173
          Width = 228
          Height = 13
          Caption = 'Carta padr'#227'o para aviso ao emitente do cheque:'
        end
        object EdCartEspecie: TdmkEditCB
          Left = 8
          Top = 24
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBCartEspecie
          IgnoraDBLookupComboBox = False
        end
        object CBCartEspecie: TdmkDBLookupComboBox
          Left = 56
          Top = 24
          Width = 368
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCarteiras
          TabOrder = 1
          dmkEditCB = EdCartEspecie
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdProtAtu: TdmkEditCB
          Left = 8
          Top = 64
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBProtAtu
          IgnoraDBLookupComboBox = False
        end
        object CBProtAtu: TdmkDBLookupComboBox
          Left = 56
          Top = 64
          Width = 368
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Titulo'
          ListSource = DsAditivo3
          TabOrder = 3
          dmkEditCB = EdProtAtu
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAditAtu: TdmkEditCB
          Left = 8
          Top = 108
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBAditAtu
          IgnoraDBLookupComboBox = False
        end
        object CBAditAtu: TdmkDBLookupComboBox
          Left = 56
          Top = 108
          Width = 368
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Titulo'
          ListSource = DsAditivo2
          TabOrder = 5
          dmkEditCB = EdAditAtu
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCartAtu: TdmkEditCB
          Left = 8
          Top = 148
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBCartAtu
          IgnoraDBLookupComboBox = False
        end
        object CBCartAtu: TdmkDBLookupComboBox
          Left = 56
          Top = 148
          Width = 368
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Titulo'
          ListSource = DsAditivo4
          TabOrder = 7
          dmkEditCB = EdCartAtu
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdImportPath: TdmkEdit
          Left = 432
          Top = 24
          Width = 356
          Height = 21
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdImportPathKeyDown
        end
        object EdLogo1Path: TdmkEdit
          Left = 432
          Top = 64
          Width = 356
          Height = 21
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdLogo1PathKeyDown
        end
        object EdNFAliq: TdmkEdit
          Left = 432
          Top = 108
          Width = 356
          Height = 21
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdImportPathKeyDown
        end
        object EdNFLei: TdmkEdit
          Left = 432
          Top = 148
          Width = 356
          Height = 21
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdImportPathKeyDown
        end
        object EdNFLinha1: TdmkEdit
          Left = 4
          Top = 232
          Width = 780
          Height = 21
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdImportPathKeyDown
        end
        object EdNFLinha2: TdmkEdit
          Left = 4
          Top = 272
          Width = 780
          Height = 21
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdImportPathKeyDown
        end
        object EdNFLinha3: TdmkEdit
          Left = 4
          Top = 312
          Width = 780
          Height = 21
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdImportPathKeyDown
        end
        object EdColigEsp: TdmkEditCB
          Left = 8
          Top = 352
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 15
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBColigEsp
          IgnoraDBLookupComboBox = False
        end
        object CBColigEsp: TdmkDBLookupComboBox
          Left = 56
          Top = 352
          Width = 728
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECOLIG'
          ListSource = DsColigEsp
          TabOrder = 16
          dmkEditCB = EdColigEsp
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBCartCheques: TdmkDBLookupComboBox
          Left = 56
          Top = 396
          Width = 336
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCartCH
          TabOrder = 18
          dmkEditCB = EdCartCheques
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCartCheques: TdmkEditCB
          Left = 8
          Top = 396
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 17
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBCartCheques
          IgnoraDBLookupComboBox = False
        end
        object EdCartDuplic: TdmkEditCB
          Left = 400
          Top = 396
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 19
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBCartDuplic
          IgnoraDBLookupComboBox = False
        end
        object CBCartDuplic: TdmkDBLookupComboBox
          Left = 448
          Top = 396
          Width = 336
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCartDupl
          TabOrder = 20
          dmkEditCB = EdCartDuplic
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBCartEmiCH: TdmkDBLookupComboBox
          Left = 60
          Top = 189
          Width = 364
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Titulo'
          ListSource = DsAditivo5
          TabOrder = 21
          dmkEditCB = EdCartEmiCH
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCartEmiCH: TdmkEditCB
          Left = 8
          Top = 189
          Width = 48
          Height = 21
          Alignment = taRightJustify
          TabOrder = 22
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBCartEmiCH
          IgnoraDBLookupComboBox = False
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Exporta / importa'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label22: TLabel
          Left = 4
          Top = 228
          Width = 251
          Height = 13
          Caption = 'Limite de dias para armazenamento no arquivo morto:'
        end
        object Label31: TLabel
          Left = 4
          Top = 248
          Width = 214
          Height = 13
          Caption = 'Diret'#243'rio de arquivos cobran'#231'a - retorno  [F4]:'
        end
        object GroupBox2: TGroupBox
          Left = 4
          Top = 4
          Width = 369
          Height = 61
          Caption = ' Identificador para exporta'#231#227'o de dados para contabilidade: '
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 16
            Width = 20
            Height = 13
            Caption = 'IOF:'
          end
          object Label7: TLabel
            Left = 120
            Top = 16
            Width = 54
            Height = 13
            Caption = 'AdValorem:'
          end
          object Label8: TLabel
            Left = 228
            Top = 16
            Width = 80
            Height = 13
            Caption = 'Fator de compra:'
          end
          object EdContabIOF: TdmkEdit
            Left = 12
            Top = 32
            Width = 104
            Height = 21
            MaxLength = 20
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdContabAdV: TdmkEdit
            Left = 120
            Top = 32
            Width = 104
            Height = 21
            MaxLength = 20
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdContabFaC: TdmkEdit
            Left = 228
            Top = 32
            Width = 104
            Height = 21
            MaxLength = 20
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
        object GroupBox3: TGroupBox
          Left = 376
          Top = 4
          Width = 413
          Height = 61
          Caption = ' Windows messenger: '
          TabOrder = 1
          object Label10: TLabel
            Left = 12
            Top = 16
            Width = 64
            Height = 13
            Caption = 'Identifica'#231#227'o:'
          end
          object Label11: TLabel
            Left = 196
            Top = 16
            Width = 34
            Height = 13
            Caption = 'Senha:'
          end
          object EdmsnID: TdmkEdit
            Left = 12
            Top = 32
            Width = 181
            Height = 21
            MaxLength = 255
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdmsnPW: TdmkEdit
            Left = 197
            Top = 32
            Width = 80
            Height = 21
            Alignment = taCenter
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings'
            Font.Style = []
            MaxLength = 255
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object CkmsnCA: TCheckBox
            Left = 284
            Top = 36
            Width = 121
            Height = 17
            Caption = 'Conectar autom'#225'tico.'
            TabOrder = 2
          end
        end
        object GroupBox4: TGroupBox
          Left = 4
          Top = 68
          Width = 785
          Height = 153
          Caption = ' FTP: '
          TabOrder = 2
          object Label17: TLabel
            Left = 216
            Top = 20
            Width = 133
            Height = 13
            Caption = 'Senha FTP j'#225' criptografada:'
          end
          object Label18: TLabel
            Left = 12
            Top = 20
            Width = 48
            Height = 13
            Caption = 'FTP Host:'
          end
          object Label19: TLabel
            Left = 12
            Top = 64
            Width = 60
            Height = 13
            Caption = 'FTP user ID:'
          end
          object Label20: TLabel
            Left = 216
            Top = 64
            Width = 257
            Height = 13
            Caption = 'Diret'#243'rio de arquivos scx baixados e processados [F4]:'
          end
          object Label21: TLabel
            Left = 12
            Top = 108
            Width = 314
            Height = 13
            Caption = 
              'Intervalo em minutos nas pesquisas por novos lote(s) no Host FTP' +
              ':'
          end
          object EdFTP_Pwd: TdmkEdit
            Left = 216
            Top = 36
            Width = 561
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdFTP_Hos: TdmkEdit
            Left = 12
            Top = 36
            Width = 201
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdFTP_Log: TdmkEdit
            Left = 12
            Top = 80
            Width = 201
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdFTP_Lix: TdmkEdit
            Left = 216
            Top = 80
            Width = 561
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdFTP_LixKeyDown
          end
          object EdFTP_Min: TdmkEdit
            Left = 328
            Top = 104
            Width = 53
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object CkFTP_Aut: TCheckBox
            Left = 404
            Top = 108
            Width = 229
            Height = 17
            Caption = 'Conencta autom'#225'tico ao iniciar o aplicativo.'
            TabOrder = 5
          end
        end
        object EdddArqMorto: TdmkEdit
          Left = 260
          Top = 224
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '30'
          ValMax = '3653'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '30'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 30
        end
        object EdPathBBRet: TdmkEdit
          Left = 4
          Top = 264
          Width = 785
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnKeyDown = EdPathBBRetKeyDown
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Impress'#245'es'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label43: TLabel
          Left = 4
          Top = 228
          Width = 136
          Height = 13
          Caption = 'Rodap'#233' da carta ao sacado:'
        end
        object GroupBox5: TGroupBox
          Left = 4
          Top = 4
          Width = 369
          Height = 37
          Caption = ' Nota Fiscal:  '
          TabOrder = 0
          object Label23: TLabel
            Left = 12
            Top = 16
            Width = 206
            Height = 13
            Caption = 'Margem superior (cent'#233'simos de milimetros):'
          end
          object EdNF_Cabecalho: TdmkEdit
            Left = 220
            Top = 12
            Width = 61
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object GroupBox6: TGroupBox
          Left = 4
          Top = 44
          Width = 369
          Height = 181
          Caption = ' Carta ao sacado (s'#243' preenchimento de dados): '
          TabOrder = 1
          object Label24: TLabel
            Left = 12
            Top = 38
            Width = 69
            Height = 13
            Caption = 'Cidade e data:'
          end
          object Label25: TLabel
            Left = 12
            Top = 62
            Width = 85
            Height = 13
            Caption = 'Destinat'#225'rio texto:'
          end
          object Label26: TLabel
            Left = 12
            Top = 86
            Width = 53
            Height = 13
            Caption = 'Duplicatas:'
          end
          object Label27: TLabel
            Left = 12
            Top = 110
            Width = 80
            Height = 13
            Caption = 'Nome do cliente:'
          end
          object Label28: TLabel
            Left = 112
            Top = 20
            Width = 64
            Height = 13
            Caption = 'Margem esq.:'
          end
          object Label29: TLabel
            Left = 188
            Top = 20
            Width = 28
            Height = 13
            Caption = 'Topo:'
          end
          object Label30: TLabel
            Left = 12
            Top = 134
            Width = 94
            Height = 13
            Caption = 'Destinat'#225'rio correio:'
          end
          object EdCSD_MEsqCidata: TdmkEdit
            Left = 112
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_MEsqDestin: TdmkEdit
            Left = 112
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_MEsqDuplic: TdmkEdit
            Left = 112
            Top = 84
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_MEsqClient: TdmkEdit
            Left = 112
            Top = 108
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_TopoCidata: TdmkEdit
            Left = 188
            Top = 36
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_TopoDestin: TdmkEdit
            Left = 188
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_TopoDuplic: TdmkEdit
            Left = 188
            Top = 84
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_TopoClient: TdmkEdit
            Left = 188
            Top = 108
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_MEsqDesti2: TdmkEdit
            Left = 112
            Top = 132
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCSD_TopoDesti2: TdmkEdit
            Left = 188
            Top = 132
            Width = 72
            Height = 21
            Alignment = taRightJustify
            MaxLength = 20
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object MeCartSacPe: TMemo
          Left = 4
          Top = 244
          Width = 769
          Height = 89
          TabOrder = 2
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'CNAB240'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 798
          Height = 504
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet6: TTabSheet
            Caption = 'Retorno'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object CkLiqOcoCDi: TCheckBox
              Left = 4
              Top = 4
              Width = 401
              Height = 17
              Caption = 
                'Cobrar diferen'#231'a quando o t'#237'tulo for pago a menor sob a ocorr'#234'nc' +
                'ia banc'#225'ria*:'
              TabOrder = 0
            end
            object EdLiqOcoCOc: TdmkEditCB
              Left = 4
              Top = 22
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBLiqOcoCOc
              IgnoraDBLookupComboBox = False
            end
            object CBLiqOcoCOc: TdmkDBLookupComboBox
              Left = 56
              Top = 22
              Width = 341
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOcorBank
              TabOrder = 2
              dmkEditCB = EdLiqOcoCOc
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CkLiqOcoShw: TCheckBox
              Left = 3
              Top = 48
              Width = 377
              Height = 17
              Caption = 'Exibir mensagem sobre a cobran'#231'a acima.'
              TabOrder = 3
            end
            object GroupBox8: TGroupBox
              Left = 4
              Top = 68
              Width = 393
              Height = 153
              Caption = 
                ' Liquida'#231#227'o de documento (status que substitui o informado pelo ' +
                'banco): '
              TabOrder = 4
              object Label40: TLabel
                Left = 12
                Top = 20
                Width = 134
                Height = 13
                Caption = 'Liquidado antecipadamente:'
              end
              object Label41: TLabel
                Left = 12
                Top = 60
                Width = 122
                Height = 13
                Caption = 'Liquidado no vencimento:'
              end
              object Label42: TLabel
                Left = 12
                Top = 100
                Width = 142
                Height = 13
                Caption = 'Liquidado ap'#243's o vencimento:'
              end
              object EdLiqStaAnt: TdmkEditCB
                Left = 12
                Top = 36
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqStaAnt
                IgnoraDBLookupComboBox = False
              end
              object CBLiqStaAnt: TdmkDBLookupComboBox
                Left = 64
                Top = 36
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 1
                dmkEditCB = EdLiqStaAnt
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqStaVct: TdmkEditCB
                Left = 12
                Top = 76
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqStaVct
                IgnoraDBLookupComboBox = False
              end
              object CBLiqStaVct: TdmkDBLookupComboBox
                Left = 64
                Top = 76
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 3
                dmkEditCB = EdLiqStaVct
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqStaVcd: TdmkEditCB
                Left = 12
                Top = 116
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqStaVcd
                IgnoraDBLookupComboBox = False
              end
              object CBLiqStaVcd: TdmkDBLookupComboBox
                Left = 64
                Top = 116
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 5
                dmkEditCB = EdLiqStaVcd
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Web'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label32: TLabel
          Left = 12
          Top = 44
          Width = 42
          Height = 13
          Caption = 'Servidor:'
        end
        object Label33: TLabel
          Left = 12
          Top = 84
          Width = 39
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object Label34: TLabel
          Left = 12
          Top = 124
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object Label35: TLabel
          Left = 12
          Top = 164
          Width = 81
          Height = 13
          Caption = 'Banco de dados:'
        end
        object Label36: TLabel
          Left = 12
          Top = 4
          Width = 43
          Height = 13
          Caption = 'Site (Url):'
          Enabled = False
        end
        object EdWeb_Host: TdmkEdit
          Left = 12
          Top = 60
          Width = 201
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnChange = EdWeb_HostChange
        end
        object EdWeb_User: TdmkEdit
          Left = 12
          Top = 100
          Width = 201
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdWeb_Pwd: TdmkEdit
          Left = 12
          Top = 140
          Width = 201
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdWeb_DB: TdmkEdit
          Left = 12
          Top = 180
          Width = 201
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdWeb_Page: TdmkEdit
          Left = 12
          Top = 20
          Width = 201
          Height = 21
          Enabled = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object RGWeb_MySQL: TRadioGroup
          Left = 220
          Top = 16
          Width = 229
          Height = 121
          Caption = ' MySQL no meu site: '
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o possuo, ou n'#227'o tenho acesso direto.'
            'Conectar somente quando eu necessitar.'
            'Conectar automaticamente ao iniciar.')
          TabOrder = 5
        end
        object GroupBox7: TGroupBox
          Left = 456
          Top = 16
          Width = 221
          Height = 185
          Caption = ' Possuo MySQL sem acesso direto: '
          TabOrder = 6
          object Label39: TLabel
            Left = 8
            Top = 20
            Width = 48
            Height = 13
            Caption = 'Host FTP:'
          end
          object Label37: TLabel
            Left = 8
            Top = 60
            Width = 62
            Height = 13
            Caption = 'Usu'#225'rio FTP:'
          end
          object Label38: TLabel
            Left = 8
            Top = 100
            Width = 57
            Height = 13
            Caption = 'Senha FTP:'
          end
          object EdWeb_FTPh: TdmkEdit
            Left = 8
            Top = 36
            Width = 201
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdWeb_FTPu: TdmkEdit
            Left = 8
            Top = 76
            Width = 201
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdWeb_FTPs: TdmkEdit
            Left = 8
            Top = 116
            Width = 201
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
        object BtCriaBD: TBitBtn
          Tag = 10001
          Left = 14
          Top = 204
          Width = 195
          Height = 40
          Caption = 'Criar &Localmente'
          Enabled = False
          TabOrder = 7
          OnClick = BtCriaBDClick
          NumGlyphs = 2
        end
        object GroupBox9: TGroupBox
          Left = 220
          Top = 144
          Width = 229
          Height = 57
          Caption = '    '
          TabOrder = 8
          object Label45: TLabel
            Left = 12
            Top = 24
            Width = 74
            Height = 13
            Caption = 'Verifica a cada '
          end
          object Label46: TLabel
            Left = 140
            Top = 24
            Width = 39
            Height = 13
            Caption = 'minutos.'
          end
          object EdNewWebMins: TdmkEdit
            Left = 88
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '1'
            ValMax = '100000'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
          end
        end
        object CkNewWebScan: TCheckBox
          Left = 232
          Top = 144
          Width = 105
          Height = 17
          Caption = 'Novos lote(s) web: '
          TabOrder = 9
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Simula Parcela'
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 798
          Height = 504
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label52: TLabel
            Left = 8
            Top = 128
            Width = 157
            Height = 13
            Caption = '*: % m'#225'ximo de cobran'#231'a de IOF:'
          end
          object GroupBox10: TGroupBox
            Left = 8
            Top = 4
            Width = 180
            Height = 121
            Caption = ' IOF Pessoa Jur'#237'dica: '
            TabOrder = 0
            object Label48: TLabel
              Left = 24
              Top = 36
              Width = 50
              Height = 13
              Caption = '% IOF fixo:'
            end
            object Label49: TLabel
              Left = 24
              Top = 60
              Width = 48
              Height = 13
              Caption = '% IOF dia:'
            end
            object EdIOF_F_PJ: TdmkEdit
              Left = 100
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdIOF_D_PJ: TdmkEdit
              Left = 100
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object CkIOF_Max_Usa_PJ: TCheckBox
              Left = 24
              Top = 82
              Width = 73
              Height = 17
              Caption = '% M'#225'ximo*:'
              TabOrder = 2
            end
            object EdIOF_Max_Per_PJ: TdmkEdit
              Left = 100
              Top = 80
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object GroupBox11: TGroupBox
            Left = 192
            Top = 4
            Width = 180
            Height = 121
            Caption = ' IOF Pessoa F'#237'sica: '
            TabOrder = 1
            object Label50: TLabel
              Left = 24
              Top = 36
              Width = 50
              Height = 13
              Caption = '% IOF fixo:'
            end
            object Label51: TLabel
              Left = 24
              Top = 60
              Width = 48
              Height = 13
              Caption = '% IOF dia:'
            end
            object EdIOF_F_PF: TdmkEdit
              Left = 100
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdIOF_D_PF: TdmkEdit
              Left = 100
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object CkIOF_Max_Usa_PF: TCheckBox
              Left = 24
              Top = 82
              Width = 73
              Height = 17
              Caption = '% M'#225'ximo*:'
              TabOrder = 2
            end
            object EdIOF_Max_Per_PF: TdmkEdit
              Left = 100
              Top = 80
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 808
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 760
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 712
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 626
    Width = 808
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 662
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 660
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 11
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 582
    Width = 808
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 804
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrAditivo2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartas'
      'WHERE Tipo=2'
      '')
    Left = 16
    Top = 453
  end
  object DsAditivo2: TDataSource
    DataSet = QrAditivo2
    Left = 44
    Top = 453
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.scx'
    Filter = 'Arquivos Suit Cash|*.scx'
    Left = 596
    Top = 12
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM carteiras'
      'WHERE Codigo <> 0'
      'ORDER BY Nome')
    Left = 16
    Top = 417
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 44
    Top = 417
  end
  object QrAditivo3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartas'
      'WHERE Tipo=3'
      '')
    Left = 144
    Top = 421
  end
  object DsAditivo3: TDataSource
    DataSet = QrAditivo3
    Left = 172
    Top = 421
  end
  object QrAditivo4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartas'
      'WHERE Tipo=4'
      '')
    Left = 144
    Top = 457
  end
  object DsAditivo4: TDataSource
    DataSet = QrAditivo4
    Left = 172
    Top = 457
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 625
    Top = 13
  end
  object QrocorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorbank'
      'ORDER BY Nome'
      '')
    Left = 536
    Top = 13
    object QrocorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrocorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrocorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrocorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrocorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrocorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrocorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrocorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrocorBank
    Left = 564
    Top = 13
  end
  object DBmysql: TmySQLDatabase
    DatabaseName = 'mysql'
    UserName = 'root'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=mysql'
      'UID=root')
    DatasetOptions = []
    Left = 321
    Top = 317
  end
  object QrMysql: TmySQLQuery
    Database = DBmysql
    Left = 349
    Top = 317
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ocordupl'
      'ORDER BY Nome')
    Left = 441
    Top = 117
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 469
    Top = 117
  end
  object TbControle: TmySQLTable
    Database = Dmod.MyDB
    TableName = 'controle'
    Left = 669
    Top = 53
    object TbControleCartSacPe: TWideMemoField
      FieldName = 'CartSacPe'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsControle: TDataSource
    DataSet = TbControle
    Left = 697
    Top = 53
  end
  object QrColigEsp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome'
      'END NOMECOLIG, Codigo '
      'FROM entidades'
      'WHERE Fornece2="V"'
      'ORDER BY NOMECOLIG')
    Left = 333
    Top = 389
    object QrColigEspNOMECOLIG: TWideStringField
      FieldName = 'NOMECOLIG'
      Size = 100
    end
    object QrColigEspCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsColigEsp: TDataSource
    DataSet = QrColigEsp
    Left = 361
    Top = 389
  end
  object QrCartDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM carteiras'
      'WHERE Codigo <> 0'
      'ORDER BY Nome')
    Left = 624
    Top = 225
  end
  object DsCartDupl: TDataSource
    DataSet = QrCartDupl
    Left = 652
    Top = 225
  end
  object DsCartCH: TDataSource
    DataSet = QrCartCH
    Left = 652
    Top = 193
  end
  object QrCartCH: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM carteiras'
      'WHERE Codigo <> 0'
      'ORDER BY Nome')
    Left = 624
    Top = 193
  end
  object QrAditivo5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartas'
      'WHERE Tipo=6')
    Left = 672
    Top = 337
  end
  object DsAditivo5: TDataSource
    DataSet = QrAditivo5
    Left = 700
    Top = 337
  end
end
