object FmGerChqMain: TFmGerChqMain
  Left = 339
  Top = 185
  Caption = 'CHQ-CNTRL-001 :: Gerenciamento de Cheques'
  ClientHeight = 704
  ClientWidth = 1054
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 590
    Width = 1054
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 0
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1050
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 634
    Width = 1054
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 920
      Top = 15
      Width = 132
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 4
        Top = 3
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 918
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtChequeOcorr: TBitBtn
        Tag = 138
        Left = 4
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Ocorr'#234'ncia'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtChequeOcorrClick
      end
      object BtStatus: TBitBtn
        Tag = 139
        Left = 116
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Status'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtStatusClick
      end
      object BtQuitacao: TBitBtn
        Tag = 171
        Left = 228
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Quita'#231#227'o'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtQuitacaoClick
      end
      object BtChequeDev: TBitBtn
        Tag = 136
        Left = 340
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Devolu'#231#227'o'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtChequeDevClick
      end
      object BtDesDep: TBitBtn
        Tag = 304
        Left = 452
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = 'D&esfazer '#13#10'dep'#243'sito'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtDesDepClick
      end
      object BtBordero: TBitBtn
        Tag = 116
        Left = 564
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Border'#244
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtBorderoClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 676
        Top = 4
        Width = 112
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = BtReabreClick
      end
      object BtRecalcula: TBitBtn
        Tag = 10086
        Left = 788
        Top = 4
        Width = 112
        Height = 40
        Caption = 'Recalcula'
        Enabled = False
        TabOrder = 7
        OnClick = BtRecalculaClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1054
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1006
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 105
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtHistorico: TBitBtn
        Tag = 259
        Left = 7
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Hist'#243'rico'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 0
        OnClick = BtHistoricoClick
      end
    end
    object GB_M: TGroupBox
      Left = 105
      Top = 0
      Width = 901
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 340
        Height = 32
        Caption = 'Gerenciamento de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 340
        Height = 32
        Caption = 'Gerenciamento de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 340
        Height = 32
        Caption = 'Gerenciamento de Cheques'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBPesq: TGroupBox
    Left = 0
    Top = 48
    Width = 1054
    Height = 210
    Align = alTop
    Caption = ' Pesquisa: '
    TabOrder = 3
    object Label38: TLabel
      Left = 300
      Top = 56
      Width = 34
      Height = 13
      Caption = 'Banco:'
    end
    object Label39: TLabel
      Left = 336
      Top = 56
      Width = 42
      Height = 13
      Caption = 'Ag'#234'ncia:'
    end
    object Label40: TLabel
      Left = 380
      Top = 56
      Width = 73
      Height = 13
      Caption = 'Conta corrente:'
    end
    object Label41: TLabel
      Left = 460
      Top = 56
      Width = 54
      Height = 13
      Caption = 'N'#186' cheque:'
    end
    object Label36: TLabel
      Left = 12
      Top = 56
      Width = 143
      Height = 13
      Caption = 'Leitura pela banda magn'#233'tica:'
    end
    object Label2: TLabel
      Left = 376
      Top = 16
      Width = 347
      Height = 13
      Caption = 
        'CPF / CNPJ [F8 pesquisa por nome] - [F4 pesquisa CPF / CNPJ digi' +
        'tado]:'
    end
    object Label9: TLabel
      Left = 520
      Top = 56
      Width = 80
      Height = 13
      Caption = 'Nome do Banco:'
    end
    object LaColigado: TLabel
      Left = 96
      Top = 96
      Width = 168
      Height = 13
      Caption = 'Coligado (quando repassado = sim):'
    end
    object Label55: TLabel
      Left = 380
      Top = 96
      Width = 38
      Height = 13
      Caption = 'Meu ID:'
    end
    object Label13: TLabel
      Left = 448
      Top = 177
      Width = 66
      Height = 13
      Caption = 'Lote remessa:'
    end
    object EdBanco: TdmkEdit
      Left = 300
      Top = 72
      Width = 33
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 3
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdBancoChange
      OnExit = EdBancoExit
      OnKeyDown = EdBancoKeyDown
    end
    object EdAgencia: TdmkEdit
      Left = 336
      Top = 72
      Width = 41
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 4
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdAgenciaExit
    end
    object EdConta: TdmkEdit
      Left = 380
      Top = 72
      Width = 77
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 10
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0000000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdContaExit
    end
    object EdCheque: TdmkEdit
      Left = 460
      Top = 72
      Width = 57
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnExit = EdChequeExit
    end
    object EdBanda: TdmkEdit
      Left = 12
      Top = 72
      Width = 285
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 34
      ParentFont = False
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnChange = EdBandaChange
      OnKeyDown = EdBandaKeyDown
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 32
      Width = 49
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 64
      Top = 32
      Width = 309
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECLIENTE'
      ListSource = DsClientes
      TabOrder = 6
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object DBEdit19: TDBEdit
      Left = 520
      Top = 72
      Width = 269
      Height = 21
      TabStop = False
      DataField = 'Nome'
      DataSource = DsBanco
      TabOrder = 7
    end
    object EdCPF_1: TdmkEdit
      Left = 376
      Top = 32
      Width = 113
      Height = 21
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtCPFJ
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdCPF_1Exit
      OnKeyDown = EdCPF_1KeyDown
    end
    object EdCMC_7_1: TEdit
      Left = 88
      Top = 72
      Width = 121
      Height = 21
      MaxLength = 30
      TabOrder = 9
      Visible = False
      OnChange = EdCMC_7_1Change
    end
    object EdColigado: TdmkEditCB
      Left = 96
      Top = 112
      Width = 49
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdColigadoChange
      DBLookupComboBox = CBColigado
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBColigado: TdmkDBLookupComboBox
      Left = 148
      Top = 112
      Width = 229
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECOLIGADO'
      ListSource = DsColigado
      TabOrder = 11
      dmkEditCB = EdColigado
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CkRepassado: TdmkCheckGroup
      Left = 12
      Top = 93
      Width = 81
      Height = 60
      Caption = ' Repassado: '
      ItemIndex = 0
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 12
      OnClick = CkRepassadoClick
      UpdType = utYes
      Value = 1
      OldValor = 0
    end
    object EdControle: TdmkEdit
      Left = 380
      Top = 112
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEmitente1: TdmkEdit
      Left = 492
      Top = 32
      Width = 297
      Height = 21
      Enabled = False
      TabOrder = 14
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdCPF_1Exit
      OnKeyDown = EdCPF_1KeyDown
    end
    object CkAtualiza: TCheckBox
      Left = 96
      Top = 136
      Width = 97
      Height = 17
      Caption = 'Atualiza saldos.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
    end
    object CkStatus: TdmkCheckGroup
      Left = 456
      Top = 96
      Width = 249
      Height = 61
      Caption = ' Status: '
      Columns = 2
      Items.Strings = (
        'Devolvido'
        'Aberto'
        'Liquidado'
        'Revendido'
        'Baixado'
        'Moroso')
      TabOrder = 16
      OnClick = CkStatusClick
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object CkProrrogado: TdmkCheckGroup
      Left = 708
      Top = 96
      Width = 81
      Height = 61
      Caption = ' Prorrogado: '
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 17
      OnClick = CkProrrogadoClick
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object CkCalcFields: TCheckBox
      Left = 196
      Top = 136
      Width = 117
      Height = 17
      Caption = 'Campos calculados.'
      Checked = True
      State = cbChecked
      TabOrder = 18
      Visible = False
    end
    object RGFontes: TRadioGroup
      Left = 796
      Top = 12
      Width = 150
      Height = 145
      Caption = ' Fontes dos dados: '
      ItemIndex = 0
      Items.Strings = (
        'Apenas ativo'
        'Ativo + Env'#233's'
        'Ativo + Env'#233's + Morto')
      TabOrder = 19
      OnClick = RGFontesClick
    end
    object CkCliente: TCheckBox
      Left = 12
      Top = 14
      Width = 97
      Height = 17
      Caption = 'Cliente:'
      Checked = True
      State = cbChecked
      TabOrder = 20
      OnClick = CkClienteClick
    end
    object RGJuridico: TRadioGroup
      Left = 12
      Top = 159
      Width = 430
      Height = 42
      Caption = ' Status jur'#237'dico: '
      Columns = 3
      Items.Strings = (
        'Sem restri'#231#245'es'
        'Com a'#231#227'o de cobran'#231'a'
        'Ambos')
      TabOrder = 21
    end
    object EdCNABLot: TdmkEdit
      Left = 519
      Top = 174
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 22
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGRemessa: TRadioGroup
      Left = 615
      Top = 159
      Width = 331
      Height = 42
      Caption = ' Remessa:'
      Columns = 3
      Items.Strings = (
        'Com remessa'
        'Sem remessa'
        'Ambos')
      TabOrder = 23
    end
  end
  object GBData: TGroupBox
    Left = 0
    Top = 258
    Width = 1054
    Height = 332
    Align = alClient
    Caption = ' Dados pesquisasdos: '
    TabOrder = 4
    object Splitter1: TSplitter
      Left = 2
      Top = 221
      Width = 1050
      Height = 10
      Cursor = crVSplit
      Align = alTop
      Color = clHighlight
      ParentColor = False
    end
    object PageControl1: TPageControl
      Left = 2
      Top = 231
      Width = 1050
      Height = 99
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Ocorr'#234'ncias  '
        object DBGrid3: TDBGrid
          Left = 772
          Top = 0
          Width = 270
          Height = 71
          Align = alRight
          DataSource = DsOcorP
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Lote pgto'
              Width = 48
              Visible = True
            end>
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 772
          Height = 71
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 772
            Height = 71
            TabStop = False
            Align = alClient
            DataSource = DsOcorreu
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Controle'
                Width = 47
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataO'
                Title.Caption = 'Data'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEOCORRENCIA'
                Title.Caption = 'Ocorr'#234'ncia'
                Width = 166
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Title.Caption = 'Saldo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATUALIZADO'
                Title.Caption = 'Atualizado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TaxaP'
                Title.Caption = 'Taxa Atz'
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Prorroga'#231#227'o  / Antecipa'#231#227'o'
        ImageIndex = 1
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 931
          Height = 71
          TabStop = False
          Align = alClient
          DataSource = DsLotPrr
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data1'
              Title.Caption = 'D.Original'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data2'
              Title.Caption = 'A.Anterior'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data3'
              Title.Caption = 'N.Vencto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIAS_2_3'
              Title.Caption = 'Dias'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIAS_1_3'
              Title.Caption = 'Acumul.'
              Width = 48
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 931
          Top = 0
          Width = 111
          Height = 71
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object BtReciboAntecip: TBitBtn
            Left = 11
            Top = 36
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Recibo'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtReciboAntecipClick
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Devolu'#231#227'o  '
        ImageIndex = 2
        object DBGrid4: TDBGrid
          Left = 0
          Top = 40
          Width = 1042
          Height = 31
          TabStop = False
          Align = alClient
          DataSource = DsChqPgs
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PAGOU'
              Title.Caption = 'Recebido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Principal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TaxasVal'
              Title.Caption = 'Tarifas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DescoVal'
              Title.Caption = 'Desconto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Border'#244
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatParcela'
              Title.Caption = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF'
              Width = 114
              Visible = True
            end>
        end
        object GradeDev: TDBGrid
          Left = 0
          Top = 0
          Width = 1042
          Height = 40
          TabStop = False
          Align = alTop
          DataSource = DsAlinIts
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA1_TXT'
              Title.Caption = '1'#170' Devol.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Alinea1'
              Title.Caption = 'A1'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA2_TXT'
              Title.Caption = '2'#170' Devol.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Alinea2'
              Title.Caption = 'A2'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Taxas'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA3_TXT'
              Title.Caption = #218'lt.Pgto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PgDesc'
              Title.Caption = 'Pg. Desc.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValPago'
              Title.Caption = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ATUAL'
              Title.Caption = 'Atual'
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Tempos de tabelas'
        ImageIndex = 3
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 1042
          Height = 71
          Align = alClient
          TabOrder = 0
        end
      end
    end
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 1050
      Height = 206
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel9'
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 162
        Width = 1050
        Height = 44
        Align = alBottom
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 0
        object Label3: TLabel
          Left = 92
          Top = 1
          Width = 50
          Height = 13
          Caption = 'Opera'#231#227'o:'
          FocusControl = DBEdit2
        end
        object Label4: TLabel
          Left = 160
          Top = 1
          Width = 27
          Height = 13
          Caption = 'Valor:'
          FocusControl = DBEdit3
        end
        object Label5: TLabel
          Left = 8
          Top = 1
          Width = 80
          Height = 13
          Caption = 'Taxa de compra:'
          FocusControl = DBEdit4
        end
        object Label7: TLabel
          Left = 244
          Top = 1
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
          FocusControl = DBEdit6
        end
        object Label10: TLabel
          Left = 312
          Top = 1
          Width = 33
          Height = 13
          Caption = 'Status:'
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 508
          Top = 1
          Width = 44
          Height = 13
          Caption = 'Coligado:'
          FocusControl = DBEdit8
        end
        object DBEdit2: TDBEdit
          Left = 92
          Top = 17
          Width = 64
          Height = 21
          DataField = 'DCompra'
          DataSource = DsPesq
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 160
          Top = 17
          Width = 80
          Height = 21
          DataField = 'Credito'
          DataSource = DsPesq
          TabOrder = 1
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 17
          Width = 80
          Height = 21
          DataField = 'TAXA_COMPRA'
          DataSource = DsPesq
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 244
          Top = 17
          Width = 64
          Height = 21
          DataField = 'Vencimento'
          DataSource = DsPesq
          TabOrder = 3
        end
        object DBEdit1: TDBEdit
          Left = 312
          Top = 17
          Width = 193
          Height = 21
          DataField = 'NOMESTATUS'
          DataSource = DsPesq
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit8: TDBEdit
          Left = 508
          Top = 17
          Width = 279
          Height = 21
          DataField = 'NOMECOLIGADO'
          DataSource = DsPesq
          TabOrder = 5
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 137
        Width = 1050
        Height = 25
        Align = alBottom
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 5
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdit7
        end
        object Label6: TLabel
          Left = 560
          Top = 5
          Width = 29
          Height = 13
          Caption = 'Itens: '
        end
        object Label57: TLabel
          Left = 644
          Top = 6
          Width = 39
          Height = 13
          Caption = 'Total $: '
        end
        object DBEdit7: TDBEdit
          Left = 48
          Top = 1
          Width = 393
          Height = 21
          DataField = 'NOMECLIENTE'
          DataSource = DsPesq
          TabOrder = 0
        end
        object DBEdit5: TDBEdit
          Left = 440
          Top = 1
          Width = 117
          Height = 21
          DataField = 'TEL1CLI_TXT'
          DataSource = DsPesq
          TabOrder = 1
        end
        object EdSomaI: TdmkEdit
          Left = 588
          Top = 1
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdSomaV: TdmkEdit
          Left = 688
          Top = 1
          Width = 99
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GradeItens: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 1050
        Height = 137
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 274
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ_TXT'
            Title.Caption = 'CNPJ/CPF'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juridico'
            Title.Caption = 'Jur'#237'dico'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DCompra'
            Title.Caption = 'Data C.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DDeposito'
            Title.Caption = 'Dep'#243'sito'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ContaCorrente'
            Title.Caption = 'Conta corrente'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Title.Caption = 'Cheque'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Risco'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ProrrVz'
            Title.Caption = 'Pror. Vz'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ProrrDd'
            Title.Caption = 'Pror. Dd'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TedRem_Its'
            Title.Caption = 'Lote rem.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatParcela'
            Title.Caption = 'ID Item lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID Lct'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsPesq
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        PopupMenu = PMDiario
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = GradeItensDblClick
        FieldsCalcToOrder.Strings = (
          'Nome=Controle'
          'CNPJ_TXT=CNPJCPF'
          'Risco=Controle')
        Columns = <
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 274
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJ_TXT'
            Title.Caption = 'CNPJ/CPF'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Juridico'
            Title.Caption = 'Jur'#237'dico'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DCompra'
            Title.Caption = 'Data C.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DDeposito'
            Title.Caption = 'Dep'#243'sito'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ContaCorrente'
            Title.Caption = 'Conta corrente'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Documento'
            Title.Caption = 'Cheque'
            Width = 43
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Risco'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ProrrVz'
            Title.Caption = 'Pror. Vz'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ProrrDd'
            Title.Caption = 'Pror. Dd'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TedRem_Its'
            Title.Caption = 'Lote rem.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatParcela'
            Title.Caption = 'ID Item lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID Lct'
            Visible = True
          end>
      end
    end
  end
  object PMHistorico: TPopupMenu
    Left = 113
    Top = 9
    object Clientepesquisado1: TMenuItem
      Caption = '&1. Cliente pesquisado'
      OnClick = Clientepesquisado1Click
    end
    object Emitentesacadopesquisado1: TMenuItem
      Caption = '&2. Emitente/sacado pesquisado'
      OnClick = Emitentesacadopesquisado1Click
    end
    object Ambos2: TMenuItem
      Caption = '&3. Ambos'
      OnClick = Ambos2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Clienteselecionado1: TMenuItem
      Caption = '&4. Cliente selecionado'
      OnClick = Clienteselecionado1Click
    end
    object Emitentesacadoselecionado1: TMenuItem
      Caption = '&5. Emitente/sacado selecionado'
      OnClick = Emitentesacadoselecionado1Click
    end
    object Ambos1: TMenuItem
      Caption = '&6. Ambos'
      OnClick = Ambos1Click
    end
  end
  object QrSumPg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago, SUM(Desco) Desco'
      'FROM alin pgs'
      'WHERE AlinIts=:P0')
    Left = 569
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
  end
  object QrLast: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT Max(Data) Data'
      'FROM alin pgs'
      'WHERE AlinIts=:P0'
      '*/'
      ''
      'SELECT Max(Data) Data '
      'FROM lct0001a'
      'WHERE FatID=305'
      'AND FatParcRef=:P0'
      '')
    Left = 625
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastData: TDateField
      FieldName = 'Data'
    end
  end
  object QrColigado: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 236
    Top = 141
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 264
    Top = 141
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    AfterClose = QrPesqAfterClose
    AfterScroll = QrPesqAfterScroll
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT lot.Codigo, lot.Cliente, lct.Documento, lct.FatParcela, '
      'lct.Data, lct.TxaCompra, lct.DCompra, lct.Credito, lct.Desco, '
      
        'lct.Vencimento, lct.Quitado, lct.Devolucao, ai.Status STATUSDEV,' +
        ' '
      'lct.ProrrVz, lct.ProrrDd, lct.Emitente, lct.ValQuit, '
      'IF(co.Tipo=0, co.RazaoSocial, co.Nome) NOMECOLIGADO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE, '
      'lct.DDeposito, lct.CNPJCPF, lct.NaoDeposita, lct.RepCli, '
      'lct.Banco, lct.Agencia, lct.ContaCorrente, lct.Depositado, '
      'IF(cl.Tipo=0, cl.ETe1, cl.PTe1) TEL1CLI, lct.Descricao,'
      'lct.Controle'
      'FROM lct0001a lct'
      'LEFT JOIN lot0001a lot ON lot.Codigo=lct.FatNum'
      'LEFT JOIN repasits  ri ON ri.Origem=lct.FatParcela'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'LEFT JOIN entidades cl ON cl.Codigo=lot.Cliente'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=lct.FatParcela'
      'WHERE lct.FatID=301 '
      'AND lot.Tipo=0'
      '')
    Left = 296
    Top = 260
    object QrPesqCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqNOMESTATUS: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 255
      Calculated = True
    end
    object QrPesqTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object QrPesqTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrPesqDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrPesqNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Origin = 'NOMECOLIGADO'
      Size = 100
    end
    object QrPesqSTATUSDEV: TSmallintField
      FieldName = 'STATUSDEV'
      Origin = 'alinits.Status'
    end
    object QrPesqDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      DisplayFormat = '0;-0; '
    end
    object QrPesqNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesqProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrPesqProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrPesqValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrPesqDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqRisco: TFloatField
      FieldKind = fkLookup
      FieldName = 'Risco'
      LookupDataSet = QrEmitCPF
      LookupKeyFields = 'CPF'
      LookupResultField = 'Limite'
      KeyFields = 'CNPJCPF'
      Lookup = True
    end
    object QrPesqNome: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Nome'
      LookupDataSet = QrEmitCPF
      LookupKeyFields = 'CPF'
      LookupResultField = 'Nome'
      KeyFields = 'CNPJCPF'
      Size = 50
      Lookup = True
    end
    object QrPesqNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrPesqRepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrPesqDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrPesqTEL1CLI: TWideStringField
      FieldName = 'TEL1CLI'
    end
    object QrPesqTEL1CLI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1CLI_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPesqData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPesqDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPesqCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqTedRem_Its: TIntegerField
      FieldName = 'TedRem_Its'
    end
    object QrPesqJuridico: TSmallintField
      FieldName = 'Juridico'
      MaxValue = 1
    end
    object QrPesqTedRemCod: TIntegerField
      FieldName = 'TedRemCod'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 324
    Top = 260
  end
  object QrEmitCPF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitcpf')
    Left = 296
    Top = 288
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmitCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrEmitCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrEmitCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrEmitCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrEmitCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrEmitCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
  object QrEmitBAC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitbac')
    Left = 324
    Top = 288
    object QrEmitBACBAC: TWideStringField
      FieldName = 'BAC'
    end
    object QrEmitBACCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrAntePror: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lit.Controle ITEMLOTE, lit.VlrCompra, '
      'prr.Controle, prr.Data1, prr.Data2, prr.Data3, '
      'lit.Documento, lit.Banco, lit.Agencia,'
      'lit.ContaCorrente, lit.Emitente,'
      'lit.Credito VALORCHEQUE, oco.Valor VALORRESSARCIDO'
      'FROM lot esprr prr'
      'LEFT JOIN ocorreu oco ON oco.Codigo=prr.Ocorrencia'
      'LEFT JOIN lct0001a lit ON lit.FatID=301'
      '  AND lit.FatParcela=prr.Codigo'
      'WHERE prr.Controle=:P0'
      ''
      '')
    Left = 316
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAnteProrData1: TDateField
      FieldName = 'Data1'
      Required = True
    end
    object QrAnteProrData2: TDateField
      FieldName = 'Data2'
      Required = True
    end
    object QrAnteProrData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrAnteProrBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lct0001a.Banco'
    end
    object QrAnteProrEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lct0001a.Emitente'
      Size = 30
    end
    object QrAnteProrVALORCHEQUE: TFloatField
      FieldName = 'VALORCHEQUE'
      Origin = 'lct0001a.Credito'
    end
    object QrAnteProrVALORRESSARCIDO: TFloatField
      FieldName = 'VALORRESSARCIDO'
      Origin = 'ocorreu.Valor'
    end
    object QrAnteProrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAnteProrITEMLOTE: TIntegerField
      FieldName = 'ITEMLOTE'
      Origin = 'lct0001a.Controle'
      Required = True
    end
    object QrAnteProrVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Origin = 'lct0001a.VlrCompra'
    end
    object QrAnteProrDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lct0001a.Documento'
    end
    object QrAnteProrContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lct0001a.ContaCorrente'
      Size = 15
    end
    object QrAnteProrAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object QrBanco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos'
      'WHERE Codigo=:P0')
    Left = 356
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 384
    Top = 288
  end
  object QrBco1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 428
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBco1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrOcorreu: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOcorreuBeforeClose
    AfterScroll = QrOcorreuAfterScroll
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      '/*'
      'SELECT lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lot es its li ON oc.Lot esIts = li.Controle'
      'LEFT JOIN lot es lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      '*/'
      'SELECT  ob.Nome NOMEOCORRENCIA, ob.PlaGen, '
      'oc.Lot esIts LOIS, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Lot esIts=:P0'
      ''
      ''
      ''
      '')
    Left = 113
    Top = 317
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ocorreu.Codigo'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Origin = 'ocorreu.DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Origin = 'ocorreu.Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Origin = 'ocorreu.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Origin = 'ocorreu.LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ocorreu.Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ocorreu.DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ocorreu.DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ocorreu.UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ocorreu.UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Origin = 'ocorreu.TaxaP'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Origin = 'ocorreu.TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Origin = 'ocorreu.Pago'
      Required = True
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Origin = 'ocorreu.DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Origin = 'ocorreu.TaxaB'
      Required = True
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Origin = 'ocorreu.Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'ocorreu.Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'ocorreu.Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuTpOcor: TSmallintField
      FieldName = 'TpOcor'
    end
    object QrOcorreuLOIS: TIntegerField
      FieldName = 'LOIS'
    end
    object QrOcorreuPlaGen: TIntegerField
      FieldName = 'PlaGen'
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 141
    Top = 317
  end
  object QrSumOc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocor rpg'
      'WHERE Ocorreu=:P0'
      '*/'
      ''
      'SELECT SUM(MoraVal) Juros, SUM(Credito-Debito) Pago'
      'FROM lct0001a'
      'WHERE FatID=304'
      'AND Ocorreu=:P0')
    Left = 369
    Top = 373
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastOcor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT Data, Codigo'
      'FROM ocor rpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc'
      '*/'
      ''
      'SELECT Data, FatParcela'
      'FROM lct0001a'
      'WHERE FatID=304 '
      'AND Ocorreu=:P0'
      'ORDER BY Data Desc, FatID Desc'
      '')
    Left = 397
    Top = 373
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object QrOcorP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.Ocorreu, lct.Data, lct.FatParcela,'
      'lct.Controle, lct.FatNum, lct.MoraVal,'
      'lct.Credito - lct.Debito Pago'
      'FROM lct0001a lct'
      'WHERE lct.FatID=304 '
      'AND lct.Ocorreu>0')
    Left = 441
    Top = 374
    object QrOcorPOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorPData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorPFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrOcorPPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorPFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrOcorPMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorPControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsOcorP: TDataSource
    DataSet = QrOcorP
    Left = 469
    Top = 374
  end
  object QrAlinIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrAlinItsBeforeClose
    AfterScroll = QrAlinItsAfterScroll
    OnCalcFields = QrAlinItsCalcFields
    SQL.Strings = (
      'SELECT al1.Nome NOMEALINEA1, al2.Nome NOMEALINEA2, ai.*'
      'FROM alinits ai'
      'LEFT JOIN alineas al1 ON al1.Codigo=ai.Alinea1'
      'LEFT JOIN alineas al2 ON al2.Codigo=ai.Alinea2'
      'WHERE ai.ChequeOrigem=:P0')
    Left = 633
    Top = 321
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAlinItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlinItsAlinea1: TIntegerField
      FieldName = 'Alinea1'
      DisplayFormat = '00;-00; '
    end
    object QrAlinItsAlinea2: TIntegerField
      FieldName = 'Alinea2'
      DisplayFormat = '00;-00; '
    end
    object QrAlinItsData1: TDateField
      FieldName = 'Data1'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinItsData2: TDateField
      FieldName = 'Data2'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinItsData3: TDateField
      FieldName = 'Data3'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAlinItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrAlinItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrAlinItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrAlinItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrAlinItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrAlinItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsTaxas: TFloatField
      FieldName = 'Taxas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlinItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlinItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlinItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlinItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAlinItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrAlinItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrAlinItsDATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Size = 12
      Calculated = True
    end
    object QrAlinItsDATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Size = 12
      Calculated = True
    end
    object QrAlinItsDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 12
      Calculated = True
    end
    object QrAlinItsChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrAlinItsStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrAlinItsValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsNOMEALINEA1: TWideStringField
      FieldName = 'NOMEALINEA1'
      Size = 250
    end
    object QrAlinItsNOMEALINEA2: TWideStringField
      FieldName = 'NOMEALINEA2'
      Size = 250
    end
    object QrAlinItsSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrAlinItsATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrAlinItsPgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsAlinIts: TDataSource
    DataSet = QrAlinIts
    Left = 661
    Top = 321
  end
  object QrChqPgs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '/*'
      'SELECT ali.Vencto, ali.DDeposito,'
      'ali.Banco, ali.Agencia, ali.Conta, ali.Cheque,'
      'ali.CPF, ali.Emitente, alp.*'
      'FROM alin pgs alp'
      'LEFT JOIN alinits ali ON ali.Codigo=alp.Codigo'
      'WHERE alp.AlinIts=:P0'
      'ORDER BY alp.Data'
      '*/'
      ''
      
        'SELECT (alp.Credito + alp.MoraVal + alp.TaxasVal + alp.MultaVal ' +
        '- alp.DescoVal) PAGOU, '
      
        'alp.Credito, alp.MoraVal,  alp.DescoVal, alp.MultaVal, alp.Taxas' +
        'Val, '
      'alp.Vencimento, alp.Banco, alp.Agencia, alp.ContaCorrente, '
      
        'alp.Documento,  alp.CNPJCPF, alp.Emitente,  alp.Data, alp.FatNum' +
        ', '
      
        'alp.FatParcela,  alp.Controle, alp.Ocorreu, alp.FatParcRef, alp.' +
        'FatGrupo '
      'FROM lct0001a alp '
      'WHERE alp.FatID=0311'
      'AND alp.FatParcRef=10'
      'ORDER BY alp.Data'
      '')
    Left = 689
    Top = 321
    object QrChqPgsCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrChqPgsMoraVal: TFloatField
      FieldName = 'MoraVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrChqPgsDescoVal: TFloatField
      FieldName = 'DescoVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrChqPgsMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrChqPgsTaxasVal: TFloatField
      FieldName = 'TaxasVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrChqPgsVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChqPgsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrChqPgsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrChqPgsContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrChqPgsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrChqPgsCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrChqPgsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrChqPgsData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChqPgsFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrChqPgsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrChqPgsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrChqPgsOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrChqPgsFatParcRef: TIntegerField
      FieldName = 'FatParcRef'
    end
    object QrChqPgsFatGrupo: TIntegerField
      FieldName = 'FatGrupo'
    end
    object QrChqPgsPAGOU: TFloatField
      FieldName = 'PAGOU'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsChqPgs: TDataSource
    DataSet = QrChqPgs
    Left = 717
    Top = 321
  end
  object QrLotPrr: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotPrrCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM lot esprr'
      'WHERE Codigo=:P0'
      'ORDER BY Controle')
    Left = 537
    Top = 371
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotPrrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotPrrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotPrrData1: TDateField
      FieldName = 'Data1'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotPrrData2: TDateField
      FieldName = 'Data2'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotPrrData3: TDateField
      FieldName = 'Data3'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotPrrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLotPrrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotPrrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotPrrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotPrrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotPrrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotPrrDIAS_1_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_1_3'
      Calculated = True
    end
    object QrLotPrrDIAS_2_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_2_3'
      Calculated = True
    end
  end
  object DsLotPrr: TDataSource
    DataSet = QrLotPrr
    Left = 565
    Top = 371
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSomaCalcFields
    SQL.Strings = (
      'SELECT COUNT(Controle) Itens, SUM(Valor) Valor'
      'FROM lot esits')
    Left = 356
    Top = 260
    object QrSomaItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 384
    Top = 260
  end
  object PMOcorreu: TPopupMenu
    OnPopup = PMOcorreuPopup
    Left = 44
    Top = 621
    object Incluiocorrncia1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia'
      OnClick = Incluiocorrncia1Click
    end
    object Alteraocorrncia1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia'
      OnClick = Alteraocorrncia1Click
    end
    object Excluiocorrncia1: TMenuItem
      Caption = '&Exclui ocorr'#234'ncia'
      OnClick = Excluiocorrncia1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object IncluiPagamento2: TMenuItem
      Caption = 'Inclui &Pagamento'
      OnClick = IncluiPagamento2Click
    end
    object Excluipagamento2: TMenuItem
      Caption = 'E&xclui pagamento'
      OnClick = Excluipagamento2Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Recibodopagamento2: TMenuItem
      Caption = '&Recibo do pagamento'
      OnClick = Recibodopagamento2Click
    end
  end
  object PMStatus: TPopupMenu
    OnPopup = PMStatusPopup
    Left = 152
    Top = 621
    object Incluiprorrogao1: TMenuItem
      Caption = '&Inclui prorroga'#231#227'o / Antecipa'#231#227'o'
      OnClick = Incluiprorrogao1Click
    end
    object IncluiQuitao1: TMenuItem
      Caption = 'Inclui &Quita'#231#227'o (Cheque sai do dep'#243'sito)'
      OnClick = IncluiQuitao1Click
    end
    object ExcluiProrrogao1: TMenuItem
      Caption = '&Exclui Prorroga'#231#227'o / Antecipa'#231#227'o / Quita'#231#227'o'
      OnClick = ExcluiProrrogao1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N0ForastatusAutomtico1: TMenuItem
      Caption = '&0. For'#231'a status Autom'#225'tico'
      OnClick = N0ForastatusAutomtico1Click
    end
    object N1Forastatus1: TMenuItem
      Caption = '&1. For'#231'a status Prorrogado '
      OnClick = N1Forastatus1Click
    end
    object N2ForastatusBaixado1: TMenuItem
      Caption = '&2. For'#231'a status Baixado'
      OnClick = N2ForastatusBaixado1Click
    end
    object N3ForastatusMoroso1: TMenuItem
      Caption = '&3. For'#231'a status Moroso'
      OnClick = N3ForastatusMoroso1Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object EditarstatusJurdicoSelecionados1: TMenuItem
      Caption = 'Editar status &Jur'#237'dico (Selecionados)'
      OnClick = EditarstatusJurdicoSelecionados1Click
    end
  end
  object PMDevolucao: TPopupMenu
    OnPopup = PMDevolucaoPopup
    Left = 381
    Top = 620
    object IncluiPagamento1: TMenuItem
      Caption = '&Inclui Pagamento'
      OnClick = IncluiPagamento1Click
    end
    object ExcluiPagamento1: TMenuItem
      Caption = '&Exclui Pagamento'
      OnClick = ExcluiPagamento1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Alteradevoluo1: TMenuItem
      Caption = '&Altera devolu'#231#227'o'
      OnClick = Alteradevoluo1Click
    end
    object Desfazdevoluo1: TMenuItem
      Caption = '&Desfaz devolu'#231#227'o'
      OnClick = Desfazdevoluo1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object RecibodoPagamento1: TMenuItem
      Caption = 'Recibo do Pagamento'
      OnClick = RecibodoPagamento1Click
    end
  end
  object PMReciboAntecip: TPopupMenu
    Left = 493
    Top = 617
    object Meuparaocliente1: TMenuItem
      Caption = '&Meu para o cliente'
      OnClick = Meuparaocliente1Click
    end
    object Clienteparamim1: TMenuItem
      Caption = '&Cliente para mim'
      OnClick = Clienteparamim1Click
    end
  end
  object PMQuitacao: TPopupMenu
    OnPopup = PMQuitacaoPopup
    Left = 276
    Top = 623
    object Quitadocumento1: TMenuItem
      Caption = '&Quita documento'
      OnClick = Quitadocumento1Click
    end
    object Imprimerecibodequitao1: TMenuItem
      Caption = '&Imprime recibo de quita'#231#227'o'
      OnClick = Imprimerecibodequitao1Click
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, '
      'CASE WHEN Tipo=0 THEN ETe1'
      'ELSE PTe1 END Te1, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 176
    Top = 64
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrClientesTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 204
    Top = 64
  end
  object PMDiario: TPopupMenu
    OnPopup = PMDiarioPopup
    Left = 808
    Top = 320
    object Adicionareventoaodiario1: TMenuItem
      Caption = '&Adicionar evento ao di'#225'rio'
      OnClick = Adicionareventoaodiario1Click
    end
    object Gerenciardirio1: TMenuItem
      Caption = 'Gerenciar di'#225'rio'
      OnClick = Gerenciardirio1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Imprimirdiriodeitemselecionado1: TMenuItem
      Caption = '&Imprimir di'#225'rio de item selecionado'
      OnClick = Imprimirdiriodeitemselecionado1Click
    end
    object Imprimirdiriodeitenspesquisados1: TMenuItem
      Caption = 'Imprimir di'#225'rio de itens &pesquisados'
      OnClick = Imprimirdiriodeitenspesquisados1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Localizarremessa11: TMenuItem
      Caption = '&Localizar remessa'
      OnClick = Localizarremessa11Click
    end
  end
  object frxDsDiarioAdd: TfrxDBDataset
    UserName = 'frxDsDiarioAdd'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOME_CLI=NOME_CLI'
      'NOME_ENT=NOME_ENT'
      'NOME_DEPTO=NOME_DEPTO'
      'Codigo=Codigo'
      'Nome=Nome'
      'DiarioAss=DiarioAss'
      'Entidade=Entidade'
      'CliInt=CliInt'
      'Depto=Depto'
      'Data=Data'
      'Hora=Hora'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'NOME_UH_ENT=NOME_UH_ENT'
      'Documento=Documento'
      'DDeposito=DDeposito')
    DataSet = QrDiarioAdd
    BCDToCurrency = False
    
    Left = 828
    Top = 8
  end
  object QrDiarioAdd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CONCAT(dpt.Unidade, " - ", '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOME_UH_ENT,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_ENT,'
      '"" NOME_DEPTO, lct.Documento, lct.DDeposito, dad.*'
      'FROM diarioadd dad'
      'LEFT JOIN entidades cli ON cli.Codigo=dad.CliInt'
      'LEFT JOIN entidades ent ON ent.Codigo=dad.Entidade'
      
        'LEFT JOIN lct0001a lct ON lct.Controle=dad.Depto AND lct.FatID=3' +
        '01')
    Left = 856
    Top = 8
    object QrDiarioAddNOME_CLI: TWideStringField
      FieldName = 'NOME_CLI'
      Size = 100
    end
    object QrDiarioAddNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrDiarioAddNOME_DEPTO: TWideStringField
      FieldName = 'NOME_DEPTO'
      Size = 10
    end
    object QrDiarioAddCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAddNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioAddDiarioAss: TIntegerField
      FieldName = 'DiarioAss'
    end
    object QrDiarioAddEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrDiarioAddCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDiarioAddDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDiarioAddData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrDiarioAddDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioAddUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDiarioAddUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDiarioAddNOME_UH_ENT: TWideStringField
      FieldName = 'NOME_UH_ENT'
      Size = 113
    end
    object QrDiarioAddDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDiarioAddDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsDiarioAdd: TDataSource
    DataSet = QrDiarioAdd
    Left = 884
    Top = 8
  end
  object frxGER_DIARI_001_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 39722.438154294000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 800
    Top = 8
    Datasets = <
      item
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      Frame.Typ = []
      MirrorMode = []
      object TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 117.165430000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsDiarioAdd."NOME_UH_ENT"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsDiarioAdd."NOME_UH_ENT"]')
          ParentFont = False
        end
      end
      object TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 3.779530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Atividades Reportadas no Di'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 313.700990000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 162.519790000000000000
        Width = 680.315400000000000000
        Child = frxGER_DIARI_001_01.Child1
        DataSet = frxDsDiarioAdd
        DataSetName = 'frxDsDiarioAdd'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015721180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015672360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Data"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 128.503873540000000000
          Width = 30.236178980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Hora:')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740064720000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Hora"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 253.228510000000000000
          Width = 98.267731180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero do cheque:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496192360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."Documento"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 64.251961180000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236452360000000000
          Width = 94.488188980000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDiarioAdd."DDeposito"]')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        Stretched = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Rich1: TfrxRichView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'Nome'
          DataSet = frxDsDiarioAdd
          DataSetName = 'frxDsDiarioAdd'
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31393034317D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
      end
    end
  end
end
