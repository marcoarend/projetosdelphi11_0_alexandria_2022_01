unit LotEmi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmLotEmi = class(TForm)
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmitenteNome: String;
  end;

  var
  FmLotEmi: TFmLotEmi;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmLotEmi.BtOKClick(Sender: TObject);
begin
  FEmitenteNome := Dmod.QrLocCPFNome.Value;
  Close;
end;

procedure TFmLotEmi.BtSaidaClick(Sender: TObject);
begin
  FEmitenteNome := '';
  Close;
end;

procedure TFmLotEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLotEmi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLotEmi.DBGrid1DblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmLotEmi.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  //
  FEmitenteNome := '';
end;

end.
